CREATE OR REPLACE PACKAGE BODY Rpm_Cand_Rule_Sql AS
--------------------------------------------------------
FUNCTION CHECK_CONDITION_0 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,   
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.item  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.item  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.item >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.item  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.item <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.item != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_0',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_0;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_1 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_dept          ITEM_MASTER.dept%TYPE;
L_class         ITEM_MASTER.CLASS%TYPE;
L_subclass      ITEM_MASTER.SUBCLASS%TYPE;
L_value         RPM_CONDITION.string_value%TYPE;
L_position      NUMBER;

BEGIN

   L_value := I_value;

   L_position := INSTR(L_value,';');
   L_dept := TO_NUMBER(SUBSTR(L_value,1,L_position-1));
   L_value := SUBSTR(L_value,L_position+1);
   L_position := INSTR(L_value,';');
   L_class := TO_NUMBER(SUBSTR(L_value,1,L_position-1));
   L_subclass := SUBSTR(L_value,L_position+1);

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item   = i.item
        AND i.dept    = L_dept
        AND i.CLASS   = L_class
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.SUBCLASS  = L_subclass) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.SUBCLASS  > L_subclass) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.SUBCLASS >= L_subclass) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.SUBCLASS  < L_subclass) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.SUBCLASS <= L_subclass) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.SUBCLASS != L_subclass)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_1',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_1;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_2 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_dept          ITEM_MASTER.dept%TYPE;
L_class         ITEM_MASTER.CLASS%TYPE;
L_value         RPM_CONDITION.string_value%TYPE;
L_position      NUMBER;

BEGIN

   L_value := I_value;

   L_position := INSTR(L_value,';');
   L_dept := TO_NUMBER(SUBSTR(L_value,1,L_position-1));
   L_class := SUBSTR(L_value,L_position+1);

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item   = i.item
        AND i.dept    = L_dept
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.CLASS  = L_class) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.CLASS  > L_class) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.CLASS >= L_class) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.CLASS  < L_class) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.CLASS <= L_class) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.CLASS != L_class)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_2',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_2;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_3 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_value NUMBER;

BEGIN

   IF I_value = 'true' THEN
      L_value := 1;
   ELSE
      L_value := 0;
   END IF;

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.current_clearance_ind  = L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.current_clearance_ind  > L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.current_clearance_ind >= L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.current_clearance_ind  < L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.current_clearance_ind <= L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.current_clearance_ind != L_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_3',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_3;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_4 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND DECODE(i.markup_calc_type,
                   Rpm_Ext_Sql.COST_MARKUP, nvl(il.current_cost,0),
                   nvl(il.current_regular_retail,0)) ! = 0
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND
                 DECODE(i.markup_calc_type,
                       Rpm_Ext_Sql.COST_MARKUP, (il.current_regular_retail - il.current_cost) / il.current_cost,
                       (il.current_regular_retail - il.current_cost) /il.current_regular_retail )  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND
                 DECODE(i.markup_calc_type,
                       Rpm_Ext_Sql.COST_MARKUP, (il.current_regular_retail - il.current_cost) / il.current_cost,
                       (il.current_regular_retail - il.current_cost) /il.current_regular_retail)  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND
                 DECODE(i.markup_calc_type,
                       Rpm_Ext_Sql.COST_MARKUP, (il.current_regular_retail - il.current_cost) / il.current_cost,
                       (il.current_regular_retail - il.current_cost) /il.current_regular_retail) >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND
                 DECODE(i.markup_calc_type,
                       Rpm_Ext_Sql.COST_MARKUP, (il.current_regular_retail - il.current_cost) / il.current_cost,
                       (il.current_regular_retail - il.current_cost) /il.current_regular_retail)  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND
                 DECODE(i.markup_calc_type,
                       Rpm_Ext_Sql.COST_MARKUP, (il.current_regular_retail - il.current_cost) / il.current_cost,
                       (il.current_regular_retail - il.current_cost) /il.current_regular_retail) <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND
                 DECODE(i.markup_calc_type,
                       Rpm_Ext_Sql.COST_MARKUP, (il.current_regular_retail - il.current_cost) / il.current_cost,
                       (il.current_regular_retail - il.current_cost) /il.current_regular_retail) != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_4',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_4;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_6 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.dept  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.dept  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.dept >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.dept  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.dept <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.dept != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_6',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_6;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_7 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.diff_1  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.diff_1  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.diff_1 >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.diff_1  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.diff_1 <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.diff_1 != I_value)
            );

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.diff_2  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.diff_2  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.diff_2 >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.diff_2  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.diff_2 <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.diff_2 != I_value)
            );

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.diff_3  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.diff_3  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.diff_3 >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.diff_3  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.diff_3 <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.diff_3 != I_value)
            );

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.diff_4  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.diff_4  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.diff_4 >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.diff_4  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.diff_4 <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.diff_4 != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_7',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_7;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_8 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN


   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.first_received_date  = TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.first_received_date  > TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.first_received_date >= TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.first_received_date  < TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.first_received_date <= TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.first_received_date != TO_DATE(I_value,'MM-DD-YYYY'))
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_8',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_8;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_9 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT /*+ full(il) */ il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            SKULIST_DETAIL sl
      WHERE il.item    = i.item
        AND sl.skulist = I_value
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND sl.item  = i.item) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND sl.item  > i.item) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND sl.item >= i.item) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND sl.item  < i.item) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND sl.item <= i.item) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND sl.item != i.item)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_9',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_9;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_10 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN


   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.last_received_date  = TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.last_received_date  > TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.last_received_date >= TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.last_received_date  < TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.last_received_date <= TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.last_received_date != TO_DATE(I_value,'MM-DD-YYYY'))
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_10',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_10;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_11 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.basis_clear_mkdn_nbr  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.basis_clear_mkdn_nbr  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.basis_clear_mkdn_nbr >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.basis_clear_mkdn_nbr  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.basis_clear_mkdn_nbr <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.basis_clear_mkdn_nbr != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_11',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_11;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_12 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.margin_mkt_basket_code  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.margin_mkt_basket_code  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.margin_mkt_basket_code >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.margin_mkt_basket_code  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.margin_mkt_basket_code <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.margin_mkt_basket_code != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_12',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_12;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_14 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.retail_label_value  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.retail_label_value  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.retail_label_value >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.retail_label_value  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.retail_label_value <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.retail_label_value != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_14',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_14;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_15 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.package_uom  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.package_uom  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.package_uom >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.package_uom  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.package_uom <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.package_uom != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_15',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_15;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_16 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.package_size  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.package_size  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.package_size >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.package_size  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.package_size <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.package_size != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_16',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_16;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_17 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_season_id     ITEM_SEASONS.season_id%TYPE;
L_phase_id      ITEM_SEASONS.phase_id%TYPE;
L_value         RPM_CONDITION.string_value%TYPE;
L_position      NUMBER;

BEGIN

   L_value := I_value;

   L_position := INSTR(L_value,';');
   L_season_id := TO_NUMBER(SUBSTR(L_value,1,L_position-1));
   L_phase_id := SUBSTR(L_value,L_position+1);

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            ITEM_SEASONS s
      WHERE il.item = i.item
        AND i.season_phase_boolean = 1
        AND s.season_id = L_season_id
        AND i.item = s.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND s.phase_id  = L_phase_id) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND s.phase_id  > L_phase_id) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND s.phase_id >= L_phase_id) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND s.phase_id  < L_phase_id) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND s.phase_id <= L_phase_id) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND s.phase_id != L_phase_id)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_17',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_17;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_18 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.projected_sales  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.projected_sales  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.projected_sales >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.projected_sales  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.projected_sales <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.projected_sales != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_18',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_18;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_19 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_value NUMBER;

BEGIN

   IF I_value = 'true' THEN
      L_value := 1;
   ELSE
      L_value := 0;
   END IF;

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.promo_boolean  = L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.promo_boolean  > L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.promo_boolean >= L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.promo_boolean  < L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.promo_boolean <= L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.promo_boolean != L_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_19',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_19;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_20 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_value NUMBER;

BEGIN

   IF I_value = 'true' THEN
      L_value := 1;
   ELSE
      L_value := 0;
   END IF;
   

   
   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND nvl(il.replenish_ind,0)  = L_value) OR
			  (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.replenish_ind  		 > L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.replenish_ind 		 >= L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.replenish_ind  		 < L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.replenish_ind 		 <= L_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL  		   = I_operator AND nvl(il.replenish_ind,0)  != L_value)
			 );
   
   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_20',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_20;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_21 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_uda_id      UDA_ITEM_LOV.uda_id%TYPE;
L_uda_value   UDA_ITEM_LOV.uda_value%TYPE;
L_value       RPM_CONDITION.string_value%TYPE;
L_position    NUMBER;

BEGIN

   L_value := I_value;

   L_position := INSTR(L_value,';');
   L_uda_id := TO_NUMBER(SUBSTR(L_value,1,L_position-1));
   L_uda_value := SUBSTR(L_value,L_position+1);

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u
      WHERE il.item  = i.item
        AND u.item   = i.item
        AND u.uda_id = L_uda_id
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND u.uda_value  = L_uda_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND u.uda_value  > L_uda_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND u.uda_value >= L_uda_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND u.uda_value  < L_uda_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND u.uda_value <= L_uda_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND u.uda_value != L_uda_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_21',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_21;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_22 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_uda_id      UDA_ITEM_DATE.uda_id%TYPE;

CURSOR C_UDA_ID IS
       SELECT la.listable_key 
        FROM RPM_LIST_VALUE la, RPM_CONDITION c
         WHERE la.CONDITION_ID = c.CONDITION_ID 
             AND c.child_condition_id  = I_condition_id;
BEGIN
   
   OPEN C_UDA_ID;
   FETCH C_UDA_ID INTO L_uda_id;
   CLOSE C_UDA_ID;
   

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_DATE u
      WHERE il.item  = i.item
        AND u.item   = i.item
        AND u.uda_id = L_uda_id
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND u.uda_date  = TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND u.uda_date  > TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND u.uda_date >= TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND u.uda_date  < TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND u.uda_date <= TO_DATE(I_value,'MM-DD-YYYY')) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND u.uda_date != TO_DATE(I_value,'MM-DD-YYYY'))
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_22',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_22;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_23 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.retail_label_type  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.retail_label_type  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.retail_label_type >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.retail_label_type  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.retail_label_type <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.retail_label_type != I_value)
            );

   RETURN 1;
EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_23',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_23;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_24 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            ITEM_SEASONS s
      WHERE il.item = i.item
        AND i.season_phase_boolean = 1
        AND i.item = s.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND s.season_id  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND s.season_id  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND s.season_id >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND s.season_id  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND s.season_id <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND s.season_id != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_24',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_24;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_25 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            RPM_DEPT_AGGREGATION da
      WHERE il.item = i.item
        AND i.dept  = da.dept
        AND DECODE(da.include_wh_on_hand,
               1,nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0),
               0,nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0)) != 0
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0))) = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0))) > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0))) >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0))) < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0))) <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.seasonal_sales,0) / (nvl(il.seasonal_sales,0) + nvl(il.location_inventory,0))) != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_25',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_25;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_26 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            RPM_DEPT_AGGREGATION da
      WHERE il.item = i.item
        AND i.dept  = da.dept
        AND DECODE(da.include_wh_on_hand,
               1,nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0),
               0,nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0)) != 0
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0))) = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0))) > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0))) >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0))) < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0))) <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND
                   DECODE(da.include_wh_on_hand,
                          1,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0) + nvl(i.ref_wh_inventory,0)),
                          0,nvl(il.historical_sales_units,0) / (nvl(il.historical_sales_units,0) + nvl(il.location_inventory,0))) != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_26',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_26;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_27 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.location_stock  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.location_stock  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.location_stock >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.location_stock  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.location_stock <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.location_stock != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_27',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_27;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_28 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.location_on_order  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.location_on_order  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.location_on_order >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.location_on_order  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.location_on_order <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.location_on_order != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_28',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_28;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_29 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.vpn  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.vpn  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.vpn >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.vpn  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.vpn <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.vpn != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_29',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_29;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_30 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
       		RPM_ME_ITEM_GTT i,
       		SUPS s
      WHERE il.item = i.item
      	AND s.supplier  = i.primary_supplier 
      	AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND s.supplier_parent  = I_value) OR
	          (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND s.supplier_parent  > I_value) OR
	          (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND s.supplier_parent >= I_value) OR
	          (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND s.supplier_parent  < I_value) OR
	          (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND s.supplier_parent <= I_value) OR
	          (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND s.supplier_parent != I_value)
      		);

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_30',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_30;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_31 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.location_stock + i.ref_wh_stock  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.location_stock + i.ref_wh_stock  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.location_stock + i.ref_wh_stock >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.location_stock + i.ref_wh_stock  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.location_stock + i.ref_wh_stock <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.location_stock + i.ref_wh_stock != I_value)
            );

   RETURN 1;
EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_31',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_31;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_32 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.location_on_order + i.ref_wh_on_order  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.location_on_order + i.ref_wh_on_order  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.location_on_order + i.ref_wh_on_order >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.location_on_order + i.ref_wh_on_order  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.location_on_order + i.ref_wh_on_order <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.location_on_order + i.ref_wh_on_order != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_32',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_32;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_33 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT /*+ USE_NL(i, u, il) ORDERED */ il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u,
            RPM_ME_CONSOLIDATE_ITEMLOC_GTT il
      WHERE I_operator = Rpm_Ext_Cand_Rule_Sql.EQUAL
        AND il.item = i.item
        AND u.item  = i.item
        AND u.uda_id  = I_value
      UNION ALL
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u
      WHERE I_operator = Rpm_Ext_Cand_Rule_Sql.GREATER_THAN
        AND il.item = i.item
        AND u.item  = i.item
        AND u.uda_id  > I_value
      UNION ALL
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u
      WHERE I_operator = Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL
        AND il.item = i.item
        AND u.item  = i.item
        AND u.uda_id >= I_value
      UNION ALL
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u
      WHERE I_operator = Rpm_Ext_Cand_Rule_Sql.LESS_THAN
        AND il.item = i.item
        AND u.item  = i.item
        AND u.uda_id  < I_value
      UNION ALL
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u
      WHERE I_operator = Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL
        AND il.item = i.item
        AND u.item  = i.item
        AND u.uda_id <= I_value
      UNION ALL
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_LOV u
      WHERE I_operator = Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL
        AND il.item = i.item
        AND u.item  = i.item
        AND u.uda_id != I_value;

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_33',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_33;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_34 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_DATE u
      WHERE il.item = i.item
        AND u.item  = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND u.uda_id  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND u.uda_id  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND u.uda_id >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND u.uda_id  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND u.uda_id <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND u.uda_id != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_34',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_34;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_35 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i,
            UDA_ITEM_FF u
      WHERE il.item = i.item
        AND u.item  = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND u.uda_id  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND u.uda_id  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND u.uda_id >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND u.uda_id  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND u.uda_id <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND u.uda_id != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_35',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_35;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_36 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.ref_wh_stock  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.ref_wh_stock  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.ref_wh_stock >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.ref_wh_stock  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.ref_wh_stock <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.ref_wh_stock != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_36',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_36;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_37 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.ref_wh_on_order  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.ref_wh_on_order  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.ref_wh_on_order >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.ref_wh_on_order  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.ref_wh_on_order <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.ref_wh_on_order != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_37',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_37;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_38 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND il.wks_of_sales_exp  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND il.wks_of_sales_exp  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND il.wks_of_sales_exp >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND il.wks_of_sales_exp  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND il.wks_of_sales_exp <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND il.wks_of_sales_exp != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_38',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_38;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_39 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_vdate DATE := Get_Vdate;

BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND
                  FLOOR((L_vdate - il.first_received_date) / 7)  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND
                  FLOOR((L_vdate - il.first_received_date) / 7)  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND
                  FLOOR((L_vdate - il.first_received_date) / 7) >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND
                  FLOOR((L_vdate - il.first_received_date) / 7)  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND
                  FLOOR((L_vdate - il.first_received_date) / 7) <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND
                  FLOOR((L_vdate - il.first_received_date) / 7) != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_39',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_39;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_40 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS

L_vdate DATE := Get_Vdate;

BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND
                  FLOOR((L_vdate - il.last_received_date) / 7)  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND
                  FLOOR((L_vdate - il.last_received_date) / 7)  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND
                  FLOOR((L_vdate - il.last_received_date) / 7) >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND
                  FLOOR((L_vdate - il.last_received_date) / 7)  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND
                  FLOOR((L_vdate - il.last_received_date) / 7) <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND
                  FLOOR((L_vdate - il.last_received_date) / 7) != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_40',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_40;
--------------------------------------------------------
FUNCTION CHECK_CONDITION_41 (O_error_msg         OUT  VARCHAR2,
                            I_condition_id   IN      RPM_CONDITION.condition_id%TYPE,
                            I_operator       IN      RPM_CONDITION.OPERATOR%TYPE,
                            I_value          IN      RPM_CONDITION.string_value%TYPE)
RETURN NUMBER IS
BEGIN

   INSERT INTO RPM_ME_IL_CAND_COND_GTT (
                  item,
                  location,
                  condition_id)
     SELECT il.item,
            il.location,
            I_condition_id
       FROM RPM_ME_CONSOLIDATE_ITEMLOC_GTT il,
            RPM_ME_ITEM_GTT i
      WHERE il.item = i.item
        AND (
              (Rpm_Ext_Cand_Rule_Sql.EQUAL                 = I_operator AND i.comp_mkt_basket_code  = I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN          = I_operator AND i.comp_mkt_basket_code  > I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.GREATER_THAN_OR_EQUAL = I_operator AND i.comp_mkt_basket_code >= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN             = I_operator AND i.comp_mkt_basket_code  < I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.LESS_THAN_OR_EQUAL    = I_operator AND i.comp_mkt_basket_code <= I_value) OR
              (Rpm_Ext_Cand_Rule_Sql.NOT_EQUAL             = I_operator AND i.comp_mkt_basket_code != I_value)
            );

   RETURN 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_CAND_RULE_SQL.CHECK_CONDITION_41',
                                        TO_CHAR(SQLCODE));
      RETURN 0;
END CHECK_CONDITION_41;
--------------------------------------------------------


END;
/

