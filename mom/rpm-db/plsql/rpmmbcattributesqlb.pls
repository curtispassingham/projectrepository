CREATE OR REPLACE PACKAGE BODY RPM_MBC_ATTRIBUTE_SQL AS
--------------------------------------------------------

   ITEM_LEVEL_ITEM                  CONSTANT VARCHAR2(2) := 'I';
   ITEM_LEVEL_ITEMLIST              CONSTANT VARCHAR2(2) := 'IL';

   ITEM_TYPE_ITEM                   CONSTANT VARCHAR2(2) := 'I';
   ITEM_TYPE_PARENT                 CONSTANT VARCHAR2(2) := 'P';
   ITEM_TYPE_PARENT_DIFF            CONSTANT VARCHAR2(2) := 'PD';

   TRAN_ITEM                        CONSTANT NUMBER(1)   := 1;
   PARENT_ITEM                      CONSTANT NUMBER(1)   := 2;


  FUNCTION FILTER_ITEM(O_error_msg     IN OUT VARCHAR2,
                     I_mbc_criteria  IN     OBJ_MBC_SEARCH_REC,
                     IO_item_from    OUT    VARCHAR2,
                     IO_item_where   OUT    VARCHAR2)                   
                                                               
  RETURN BOOLEAN;
  
  FUNCTION FILTER_ZONE(O_error_msg     IN OUT VARCHAR2,
                    I_mbc_criteria    IN    OBJ_MBC_SEARCH_REC,
                    IO_zone_where    OUT    VARCHAR2)
  RETURN BOOLEAN;
------------------------------------------------------------------

PROCEDURE SEARCH(O_return_code          OUT NUMBER,
                 O_error_msg            OUT VARCHAR2,
                 O_lc_tbl              OUT OBJ_MBC_ATTRIBUTE_TBL,
                 O_lc_count            OUT NUMBER,
                 I_lc_criterias        IN  OBJ_MBC_SEARCH_TBL)
IS

   ---
   L_criterias       OBJ_MBC_SEARCH_TBL;
   L_criteria        OBJ_MBC_SEARCH_REC;
   ---
   L_select          VARCHAR2(4000) := NULL;
   L_base_from       VARCHAR2(8000) := NULL;
   L_base_where      VARCHAR2(4000) := NULL;
   L_item_from       VARCHAR2(4000) := NULL;
   L_item_where      VARCHAR2(4000) := NULL;
   L_loc_from        VARCHAR2(4000) := NULL;
   L_zone_where      VARCHAR2(4000) := NULL;
   ---
   L_from            VARCHAR2(4000) := NULL;
   L_where           VARCHAR2(4000) := NULL;
   --
   TYPE MBCCursorType IS REF CURSOR;        -- define weak REF CURSOR type
   il_lc             MBCCursorType;         -- declare cursor variable

BEGIN

   
   DELETE FROM rpm_pc_search_num_tbl;
   DELETE FROM rpm_pc_search_char_tbl;
   DELETE FROM rpm_pc_search_merch_tbl;   
   
   L_criterias := I_lc_criterias;
   L_criteria  := L_criterias(1);  
   
   IF l_criteria.dept_class_subclasses IS NOT NULL AND 
      l_criteria.dept_class_subclasses.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_merch_tbl
      SELECT 'DEPT_CLASS_SUBCLASSES',
             dcs.*
      FROM TABLE(CAST(l_criteria.dept_class_subclasses AS obj_dept_class_subclass_tbl)) dcs;
   
   END IF;
   
   IF l_criteria.items IS NOT NULL AND 
      l_criteria.items.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_char_tbl
      SELECT 'ITEMS',
              i.*
      FROM TABLE(CAST(l_criteria.items AS obj_varchar_id_table)) i;
   
   END IF;
   
   IF l_criteria.diff_ids IS NOT NULL AND 
      l_criteria.diff_ids.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_char_tbl
      SELECT 'DIFF_IDS',
              di.*
      FROM TABLE(CAST(l_criteria.diff_ids AS obj_varchar_id_table)) di;
   
   END IF;
   
   IF l_criteria.zones IS NOT NULL AND 
      l_criteria.zones.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_num_tbl
      SELECT 'ZONES',
              z.*
      FROM TABLE(CAST(l_criteria.zones AS obj_numeric_id_table)) z;
   
   END IF;  

   
   IF l_criteria.margin_mbc IS NOT NULL AND 
      l_criteria.margin_mbc.COUNT > 0 THEN     
         
      INSERT INTO rpm_pc_search_char_tbl
      SELECT 'MARGIN_MBC',
              mm.*
      FROM TABLE(CAST(l_criteria.margin_mbc AS obj_varchar_id_table)) mm;
   
   END IF;
   
   IF l_criteria.competitive_mbc IS NOT NULL AND 
      l_criteria.competitive_mbc.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_char_tbl
      SELECT 'COMPETITIVE_MBC',
              cm.*
      FROM TABLE(CAST(l_criteria.competitive_mbc AS obj_varchar_id_table)) cm;
   
   END IF;


   L_select :=
      'SELECT NEW obj_mbc_attribute_rec(';

   L_select :=  L_select ||
                             'rma.mbc_attribute_id, '||            -- MBC_ATTRIBUTE_ID
                             'rma.item, '||                        -- ITEM
                             'im.item_desc, ' ||                   -- ITEM_DESCRIPTION
                             'im.item_parent, '||                  -- ITEM_PARENT
                             'im2.item_desc, '||                   -- ITEM_PARENT_DESC
                             'rzg.zone_group_display_id, '||       -- ZONE_GROUP_DISPLAY_ID
                             'rzg.name, '||                        -- ZONE_GROUP_NAME
                             'rz.zone_display_id,'||               -- ZONE_DISPLAY_ID
                             'rz.name,'||                          -- ZONE_NAME
			     'rma.margin_mbc,' ||                  -- MARGIN_MBC
                             '(select name from rpm_mbc_lov_values where mkt_basket_code = rma.MARGIN_MBC), '||        -- MARGIN_MBC_DESC
                             'rma.competitive_mbc, '||                                                                 -- COMPETITIVE_MBC_CODE
                             '(select name from rpm_mbc_lov_values where mkt_basket_code = rma.COMPETITIVE_MBC), '||   -- MBC_ATTRIBUTE_ID_DESC
                             'rz.zone_id )';                       -- ZONE_ID

   L_base_from  := ' FROM ' ||
                           'rpm_mbc_attribute rma, '||
                           'item_master im, '||
                           'item_master im2,'||
                           'rpm_zone rz ,'||
                           'rpm_zone_group rzg';

   L_base_where := 'WHERE rma.item = im.item ' ||
                   'AND im.item_parent = im2.item(+) ' ;

   IF L_criteria.margin_mbc IS NOT NULL THEN
        L_base_where := L_base_where || ' AND rma.margin_mbc IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''MARGIN_MBC'') ';
   END IF;

   IF L_criteria.competitive_mbc IS NOT NULL THEN
        L_base_where := L_base_where || ' AND rma.competitive_mbc IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''COMPETITIVE_MBC'') ';
   END IF;

   -- Filter the Item Criteria
   IF filter_item(O_error_msg,
                  L_criteria,
                  L_item_from,
                  L_item_where) = FALSE THEN
      O_return_code := 0;
      RETURN;
   END IF;

   IF filter_zone(O_error_msg,
                 L_criteria,
                 L_zone_where) = FALSE THEN
      O_return_code := 0;
      RETURN;
   END IF;


   L_from   := L_base_from||L_item_from;
   L_where  := L_base_where||L_item_where||L_zone_where;  
 	  
   OPEN il_lc FOR L_select||' '||L_from||' '||L_where;

   FETCH il_lc BULK COLLECT INTO O_lc_tbl;
   
   O_lc_count := O_lc_tbl.COUNT();   
   
   CLOSE il_lc;

   O_return_code := 1;


EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_MBC_ATTRIBUTE_SQL.SEARCH',
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;

END SEARCH;
-----------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION FILTER_ITEM(O_error_msg     IN OUT VARCHAR2,
                     I_mbc_criteria  IN     OBJ_MBC_SEARCH_REC,
                     IO_item_from    OUT    VARCHAR2,
                     IO_item_where   OUT    VARCHAR2)
RETURN BOOLEAN IS
   ---
   --ITEM_LEVEL_ITEM                  CONSTANT VARCHAR2(2) := 'I';
   --ITEM_LEVEL_ITEMLIST              CONSTANT VARCHAR2(2) := 'IL';

   --ITEM_TYPE_ITEM                   CONSTANT VARCHAR2(2) := 'I';
   --ITEM_TYPE_PARENT                 CONSTANT VARCHAR2(2) := 'P';
   --ITEM_TYPE_PARENT_DIFF            CONSTANT VARCHAR2(2) := 'PD';

   --TRAN_ITEM                        CONSTANT NUMBER(1)   := 1;
   --PARENT_ITEM                      CONSTANT NUMBER(1)   := 2;
   ---

BEGIN

   --This function gets you to transaction level items (im.item) on item_master that meet the criteria.

   --SEARCH CRITERIA ITEM FILTERS
     --item_itemlist_ind               VARCHAR2(1),
     --item_level_ind                  VARCHAR2(1),
     --exclusive_item_level_ind        VARCHAR2(1),
     --dept_class_subclasses           obj_dept_class_subclass_tbl,
     --items                           obj_varchar_id_table,
     --diff_type                       VARCHAR2(6),
     --diff_ids                        obj_varchar_id_table,
   --
   IO_item_from  := NULL;
   IO_item_where := NULL;

   -- if item
   IF I_mbc_criteria.item_itemlist_ind = ITEM_LEVEL_ITEM THEN

      -- tran level
      IF I_mbc_criteria.item_level_ind = ITEM_TYPE_ITEM THEN
         IF I_mbc_criteria.items IS NOT NULL THEN

            IO_item_where := IO_item_where ||
                             'AND '||
                             'im.item IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''ITEMS'') ';

         ELSIF I_mbc_criteria.dept_class_subclasses IS NOT NULL THEN
            
            IO_item_where := IO_item_where ||
                             'AND (im.dept, im.class, im.subclass) IN (' ||
                             'SELECT cri.dept, NVL(cri.class,sub.class), NVL(cri.subclass, sub.subclass) ' ||
                             'FROM rpm_pc_search_merch_tbl cri, '||
                             'subclass sub '||
                             'where sub.dept = cri.dept and sub.class = NVL(cri.class,sub.class) AND sub.subclass = NVL(cri.subclass, sub.subclass) '||
                             'AND cri.criteria_type = ''DEPT_CLASS_SUBCLASSES'') ';
         END IF;

      -- parent
      ELSIF I_mbc_criteria.item_level_ind = ITEM_TYPE_PARENT THEN
         IF I_mbc_criteria.items IS NOT NULL THEN
            IO_item_where := IO_item_where ||
                             'AND '||
                             'im.item_parent IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''ITEMS'') ';

         ELSIF I_mbc_criteria.dept_class_subclasses IS NOT NULL THEN

            IO_item_where := IO_item_where || ' AND (im.dept, im.class, im.subclass) IN (' ||
                             'SELECT cri.dept, NVL(cri.class,sub.class), NVL(cri.subclass, sub.subclass) ' ||
                             'FROM rpm_pc_search_merch_tbl cri, '||
                             'subclass sub '||
                             'WHERE sub.dept = cri.dept AND sub.class = NVL(cri.class,sub.class) AND sub.subclass = NVL(cri.subclass, sub.subclass) '||
                             'AND cri.criteria_type = ''DEPT_CLASS_SUBCLASSES'') ';
         END IF;

         IO_item_where := IO_item_where || ' AND im.status = ''A'' ';
         IO_item_where := IO_item_where || ' AND im.sellable_ind = ''Y'' ';

      -- parent diff
      ELSIF I_mbc_criteria.item_level_ind = ITEM_TYPE_PARENT_DIFF THEN
         IF I_mbc_criteria.items IS NOT NULL AND
         I_mbc_criteria.diff_ids IS NOT NULL THEN
            IO_item_where := IO_item_where ||
                 'AND ' ||
                 'im.item_parent IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''ITEMS'') '||
                 'AND (im.diff_1 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') '||
                 'OR im.diff_2 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') '||
                 'OR im.diff_3 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') '||
                 'OR im.diff_4 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'')) ';
         ELSIF I_mbc_criteria.items IS NOT NULL THEN
            IO_item_where := IO_item_where ||
                 'AND '||
                 'im.item_parent IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''ITEMS'') '||
                 'AND (im.diff_1 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') '||
                 'OR im.diff_2 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') '||
                 'OR im.diff_3 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') '||
                 'OR im.diff_4 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''')) ';               
         ELSIF I_mbc_criteria.items IS NULL AND
         I_mbc_criteria.diff_ids IS NOT NULL AND
         I_mbc_criteria.dept_class_subclasses IS NOT NULL THEN

            IO_item_where := IO_item_where ||
                'AND ( '||
                '(im.dept, im.class, im.subclass) IN '||
                '(SELECT cri.dept, NVL(cri.class,sub.class), NVL(cri.subclass, sub.subclass) ' ||
                'FROM rpm_pc_search_merch_tbl cri, '||
                'subclass sub '||
                'WHERE sub.dept = cri.dept AND sub.class = NVL(cri.class,sub.class) '||
                'AND sub.subclass = NVL(cri.subclass, sub.subclass) '||
                'AND cri.criteria_type = ''DEPT_CLASS_SUBCLASSES'')) ';

            IO_item_where := IO_item_where ||
                'AND ( '||
                '(im.diff_1 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') ' ||
                'OR im.diff_2 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') ' ||
                'OR im.diff_3 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') ' ||
                'OR im.diff_4 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'')) '||
                ') ';
         ELSIF I_mbc_criteria.items IS NULL AND
         I_mbc_criteria.diff_ids IS NOT NULL AND
         I_mbc_criteria.dept_class_subclasses IS NULL THEN
            IO_item_where := IO_item_where ||
                'AND ( '||
                '(im.diff_1 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') ' ||
                'OR im.diff_2 IN (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') ' ||
                'OR im.diff_3 in (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') ' ||
                'OR im.diff_4 in (SELECT varchar_id FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'')) '||
                ') ';
         ELSIF I_mbc_criteria.dept_class_subclasses IS NOT NULL THEN

            IO_item_where := IO_item_where ||
                'AND ('||
                'im.dept, im.class, im.subclass) IN '||
                '(SELECT cri.dept, NVL(cri.class,sub.class), NVL(cri.subclass, sub.subclass) ' ||
                'FROM rpm_pc_search_merch_tbl cri, '||
                'subclass sub '||
                'WHERE sub.dept = cri.dept AND sub.class = NVL(cri.class,sub.class) '||
                'AND sub.subclass = NVL(cri.subclass, sub.subclass) '||
                'AND cri.criteria_type = ''DEPT_CLASS_SUBCLASSES'') ';

            IO_item_where := IO_item_where ||
                'AND '||
                '(im.diff_1 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') ' ||
                'OR im.diff_2 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') ' ||
                'or im.diff_3 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') ' ||
                'or im.diff_4 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''')) ';

         ELSE
            IO_item_where := IO_item_where ||
                'AND '||
                '(im.diff_1 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') ' ||
                'OR im.diff_2 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') ' ||
                'or im.diff_3 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''') ' ||
                'or im.diff_4 IN (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_mbc_criteria.diff_type||''')) ';
         END IF;

         IO_item_where := IO_item_where || ' AND im.status = ''A'' ';
         IO_item_where := IO_item_where || ' AND im.sellable_ind = ''Y'' ';

      END IF;
   --else item list
   ELSIF I_mbc_criteria.item_itemlist_ind = ITEM_LEVEL_ITEMLIST THEN
      IF I_mbc_criteria.item_list_id IS NOT NULL THEN
         IO_item_from := IO_item_from || ' , skulist_detail sd ';

         IO_item_where := IO_item_where ||
                         ' AND im.item = sd.item ' ||
                         ' AND sd.skulist = '''||I_mbc_criteria.item_list_id||''' ';
      END IF;
   END IF;



   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_CHANGE_SQL.FILTER_ITEM',
                                        TO_CHAR(SQLCODE));
      RETURN FALSE;

END FILTER_ITEM;
-----------------------------------------
FUNCTION FILTER_ZONE(O_error_msg     IN OUT VARCHAR2,
                    I_mbc_criteria    IN    OBJ_MBC_SEARCH_REC,
                    IO_zone_where    OUT    VARCHAR2)
RETURN BOOLEAN IS

BEGIN


  IO_zone_where := NULL;

   -- zone
   IF I_mbc_criteria.zones IS NOT NULL THEN
      IO_zone_where := IO_zone_where ||
         'AND (rma.zone_id IN (SELECT numeric_id FROM rpm_pc_search_num_tbl WHERE criteria_type = ''ZONES'')) ';

  -- zone group
  ELSIF I_mbc_criteria.zone_group IS NOT NULL THEN
      IO_zone_where := IO_zone_where ||
                      'AND (rma.zone_id IN (' ||
                      'SELECT zone_id FROM  '||
                      'rpm_zone rz '||
                      'WHERE rz.zone_group_id = '''||I_mbc_criteria.zone_group||''')) '||
                      'AND rzg.zone_group_id = '''||I_mbc_criteria.zone_group||'''';

  END IF;
  IO_zone_where :=IO_zone_where||'  AND rma.zone_id = rz.zone_id '
  	                       ||'  AND rz.zone_group_id = rzg.zone_group_id ';

  RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_MBC_ATTRIBUTE_SQL.FILTER_ZONE',
                                        TO_CHAR(SQLCODE));
      RETURN FALSE;

END FILTER_ZONE;
-----------------------------------------------------------------------------------------------------

END RPM_MBC_ATTRIBUTE_SQL;
/

