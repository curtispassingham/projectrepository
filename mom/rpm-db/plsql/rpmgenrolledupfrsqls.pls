CREATE OR REPLACE PACKAGE RPM_GENERATE_ROLLUP_FR_SQL AS
--------------------------------------------------------------------------------
FUNCTION GENERATE_ROLLUP_FR_FOR_PZG_UPD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION GENERATE_ROLLUP_FR_BY_ITEMS(O_error_msg    OUT VARCHAR2,
                                     I_items     IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION GENERATE_ROLLUP_FR_FOR_NIL(O_error_msg        OUT VARCHAR2,
                                    I_thread_number IN     NUMBER,
                                    I_process_id    IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------------------------------
END RPM_GENERATE_ROLLUP_FR_SQL;
/
