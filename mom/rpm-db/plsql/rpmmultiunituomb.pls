CREATE OR REPLACE PACKAGE BODY RPM_CC_MULTI_UNIT_UOM AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_MULTI_UNIT_UOM.VALIDATE';

   L_error_key    VARCHAR2(255)            := NULL;
   L_error_string VARCHAR2(255)            := NULL;
   L_error_rec    CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl    CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_multi_unit_retail RPM_FUTURE_RETAIL.MULTI_UNIT_RETAIL%TYPE := NULL;

   cursor C_CHECK is
      select price_event_id,
             item,
             location,
             action_date,
             future_retail_id,
             selling_retail,
             selling_uom,
             multi_unit_retail,
             multi_selling_uom,
             multi_units,
             'CONFLICT' type
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id NOT IN (select ccet.price_event_id
                                          from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and(   (    selling_uom     = multi_selling_uom
                 and selling_retail  > multi_unit_retail)
             or (    selling_uom     = multi_selling_uom
                 and selling_retail <= (multi_unit_retail/multi_units)))
      union all
      select price_event_id,
             item,
             location,
             action_date,
             future_retail_id,
             selling_retail,
             selling_uom,
             multi_unit_retail,
             multi_selling_uom,
             multi_units,
             'UOM' type
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id NOT IN (select ccet.price_event_id
                                          from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and selling_uom       is NOT NULL
         and multi_selling_uom is NOT NULL
         and selling_uom       != multi_selling_uom
      order by type;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999, NULL, NULL, NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      for rec IN C_CHECK loop

         if rec.type = 'CONFLICT' then
            if rec.selling_retail > rec. multi_unit_retail then
               L_error_string := 'multi_unit_retail_less_than_unit_retail';
            else
               L_error_string := 'multi_unit_retail_more_than_unit_retail';
            end if;

            L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                    rec.future_retail_id,
                                                    RPM_CONSTANTS.CONFLICT_ERROR,
                                                    L_error_string);

            if IO_error_table is NULL then
               IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
            else
               IO_error_table.EXTEND;
               IO_error_table(IO_error_table.COUNT) := L_error_rec;
            end if;

         elsif rec.type = 'UOM' then

            if UOM_SQL.CONVERT(L_error_key,
                               L_multi_unit_retail,
                               rec.multi_selling_uom,
                               rec.multi_unit_retail,
                               rec.selling_uom,
                               rec.item,
                               NULL,                      --supplier
                               NULL) = FALSE then         --origin country

               L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       L_error_key);

               IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

               return 0;

            end if;

            if rec.selling_retail > L_multi_unit_retail then

               L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                       rec.future_retail_id,
                                                       RPM_CONSTANTS.CONFLICT_ERROR,
                                                       'multi_unit_retail_less_than_unit_retail');
               if IO_error_table is NULL then
                  IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
               else
                  IO_error_table.EXTEND;
                  IO_error_table(IO_error_table.COUNT) := L_error_rec;
               end if;

            elsif rec.selling_retail <= (L_multi_unit_retail/rec.multi_units) then

               L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                       rec.future_retail_id,
                                                       RPM_CONSTANTS.CONFLICT_ERROR,
                                                       'multi_unit_retail_more_than_unit_retail');
               if IO_error_table is NULL then

                  IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

               else

                  IO_error_table.EXTEND;
                  IO_error_table(IO_error_table.COUNT) := L_error_rec;

               end if;

            end if;

         end if;

      end loop;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));

      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

