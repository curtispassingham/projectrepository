CREATE OR REPLACE PACKAGE BODY RPM_CONFLICT_LIBRARY AS
--------------------------------------------------------
FUNCTION APPLY_RETAIL_CHANGE_WITH_GUIDE(IO_error_table   IN OUT OBJ_VARCHAR_DESC_TABLE,
                                        I_retail         IN     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                                        I_change_type    IN     RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE,
                                        I_change_amount  IN     RPM_PRICE_CHANGE.CHANGE_AMOUNT%TYPE,
                                        I_change_percent IN     RPM_PRICE_CHANGE.CHANGE_PERCENT%TYPE,
                                        I_price_guide_id IN     RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE,
                                        O_retail            OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CONFLICT_LIBRARY.APPLY_RETAIL_CHANGE_WITH_GUIDE';

   L_error_msg VARCHAR2(255) := NULL;
   L_retail    RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE;

BEGIN

   if APPLY_RETAIL_CHANGE(IO_error_table,
                          I_retail,
                          I_change_type,
                          I_change_amount,
                          I_change_percent,
                          O_retail) = 0 then
      return 0;
   end if;

   L_retail := O_retail;

   if APPLY_PRICE_GUIDE(IO_error_table,
                        L_retail,
                        I_price_guide_id,
                        O_retail) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then

      L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      IO_error_table := OBJ_VARCHAR_DESC_TABLE(L_error_msg);

      return 0;

END APPLY_RETAIL_CHANGE_WITH_GUIDE;
--------------------------------------------------------

FUNCTION APPLY_RETAIL_CHANGE(IO_error_table   IN OUT OBJ_VARCHAR_DESC_TABLE,
                             I_retail         IN     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                             I_change_type    IN     RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE,
                             I_change_amount  IN     RPM_PRICE_CHANGE.CHANGE_AMOUNT%TYPE,
                             I_change_percent IN     RPM_PRICE_CHANGE.CHANGE_PERCENT%TYPE,
                             O_retail            OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CONFLICT_LIBRARY.APPLY_RETAIL_CHANGE';

   L_error_msg VARCHAR2(255) := NULL;

BEGIN

   if I_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
      O_retail := I_change_amount;
      return 1;
   end if;

   if I_retail is NULL then
      return 0;
   end if;

   if I_change_type is NULL or
      I_change_type = RPM_CONSTANTS.RETAIL_NONE then
      ---
      O_retail := I_retail;

   elsif I_change_type = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE then
      ---
      O_retail := I_retail + I_change_amount;

   elsif I_change_type = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE then
      ---
      O_retail := I_retail * (1 + I_change_percent);

   else
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      IO_error_table := OBJ_VARCHAR_DESC_TABLE(L_error_msg);

      return 0;

END APPLY_RETAIL_CHANGE;
--------------------------------------------------------

FUNCTION APPLY_PRICE_GUIDE(IO_error_table   IN OUT OBJ_VARCHAR_DESC_TABLE,
                           I_retail         IN     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                           I_price_guide_id IN     RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE,
                           O_retail            OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CONFLICT_LIBRARY.APPLY_PRICE_GUIDE';

   L_error_msg VARCHAR2(255) := NULL;

   cursor C_GUIDE is
      select NEW_PRICE_VAL
        from rpm_price_guide_interval
       where price_guide_id = I_price_guide_id
         and I_retail       BETWEEN from_price_val and to_price_val;

BEGIN

   open C_GUIDE;
   fetch C_GUIDE into O_retail;

   if C_GUIDE%NOTFOUND then

      O_retail := I_retail;
   end if;

   close C_GUIDE;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      IO_error_table := OBJ_VARCHAR_DESC_TABLE(L_error_msg);

      return 0;

END APPLY_PRICE_GUIDE;
--------------------------------------------------------
END RPM_CONFLICT_LIBRARY;
/

