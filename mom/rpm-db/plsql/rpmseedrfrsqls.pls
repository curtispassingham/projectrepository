CREATE OR REPLACE PACKAGE RPM_SEED_RFR_SQL AS

FUNCTION INIT(O_error_msg    OUT VARCHAR2,
              O_max_thread_no    OUT NUMBER)
RETURN NUMBER;

---------------------------------------------------------

FUNCTION PROCESS_THREAD(O_error_msg     OUT VARCHAR2,
                        I_thread_num IN     NUMBER)
RETURN NUMBER;

---------------------------------------------------------

FUNCTION TRUNC_RPM_DC_ITEM_LOC(O_error_msg     OUT VARCHAR2)
RETURN NUMBER;

---------------------------------------------------------

FUNCTION TRUNC_TMP_TABLES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER;

---------------------------------------------------------
END RPM_SEED_RFR_SQL;
/