CREATE OR REPLACE PACKAGE BODY RPM_CAPTURE_GTT_SQL AS

--------------------------------------------------------------------------------

LP_capture_gtt_data NUMBER(1) := 0;

LP_bulk_cc_pe_id        NUMBER(10) := NULL;
LP_parent_thread_number NUMBER(10) := NULL;
LP_thread_number        NUMBER(10) := NULL;
LP_chunk_number         NUMBER(10) := NULL;

LP_generic_capture_start_point NUMBER(10) := NULL;
LP_end_of_generic_start_point  NUMBER(10) := NULL;

LP_rfr_gtt_capt_start_point    NUMBER(10) := NULL;
LP_rpile_gtt_capt_start_point  NUMBER(10) := NULL;
LP_cspfr_gtt_capt_start_point  NUMBER(10) := NULL;
LP_clr_gtt_capt_start_point    NUMBER(10) := NULL;
LP_rile_gtt_capt_start_point   NUMBER(10) := NULL;

LP_capture_after_start_point VARCHAR2(1) := NULL;

LP_rfr_gtt_data_captured   NUMBER(1) := 0;
LP_rpile_gtt_data_captured NUMBER(1) := 0;
LP_cspfr_gtt_data_captured NUMBER(1) := 0;
LP_clr_gtt_data_captured   NUMBER(1) := 0;
LP_rfile_gtt_data_captured NUMBER(1) := 0;

--------------------------------------------------------------------------------

FUNCTION CAPTURE_GTT_DATA(O_error_message        OUT VARCHAR2,
                          I_capture_date_time IN     TIMESTAMP,
                          I_rfr_data          IN     OBJ_GTT_CAPT_RFR_TBL,
                          I_rpile_data        IN     OBJ_GTT_CAPT_RPILE_TBL,
                          I_cspfr_data        IN     OBJ_GTT_CAPT_CSPFR_TBL,
                          I_clr_data          IN     OBJ_GTT_CAPT_CLR_TBL,
                          I_rfr_il_expl_data  IN     OBJ_GTT_CAPT_RFILEXPL_TBL)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION INIT_VARS(O_error_message           OUT VARCHAR2,
                   I_user_id              IN     VARCHAR2,
                   I_bulk_cc_pe_id        IN     NUMBER,
                   I_parent_thread_number IN     NUMBER,
                   I_thread_number        IN     NUMBER,
                   I_chunk_number         IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_CAPTURE_GTT_SQL.INIT_VARS';

   -- The DECODE(DECODE()) logic below is in place to account for scenarios where a generic starting point is not
   -- exactly matched up with a statement that inserts/updates GTT data (ie when capturing RPILE data and starting
   -- with ROLL_FORWARD, the first roll forward statement is to update the timeline_seq on RFR_GTT - nothing on RPILE).
   -- There would never be a time where the first roll forward statement that impacts RPILE will be the first statement
   -- in roll forward to touch any GTT that data is being captured for.  Thus, the adjustment by table...

   cursor C_DATA is
      select -- generic capture start point
             DECODE(capture_start_point,
                    'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR,
                    'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET,
                    'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_ROLL_FORWARD,
                    'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                    'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
             -- end of generic start point
             DECODE(capture_start_point,
                    'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET_PARENT,
                    'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_REMOVE_EVENT,
                    'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_RETAIL,
                    'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                    'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
             -- rfr gtt capt start point
             DECODE(capture_rfr_gtt_data,
                    'Y', DECODE(capture_start_point,
                                'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR,
                                'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_MERGE_INTO_TIMELINE,
                                'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_ROLL_FORWARD,
                                'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                                'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
                    NULL),
             -- rpile gtt capt start point
             DECODE(capture_rpile_gtt_data,
                    'Y', DECODE(capture_start_point,
                                'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE_NON_PR,
                                'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO,
                                'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_RF_PROMO_RETAIL,
                                'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                                'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
                    NULL),
             -- cspfr gtt capt start point
             DECODE(capture_cspfr_gtt_data,
                    'Y', DECODE(capture_start_point,
                                'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_NON_PR,
                                'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO,
                                'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_UOM,
                                'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                                'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
                    NULL),
             -- clr gtt capt start point
             DECODE(capture_clr_gtt_data,
                    'Y', DECODE(capture_start_point,
                                'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR,
                                'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET,
                                'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_ROLL_FORWARD,
                                'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                                'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
                    NULL),
             -- rile gtt capt start point
             DECODE(capture_rfr_il_expl_gtt_data,
                    'Y', DECODE(capture_start_point,
                                'POP_GTT',             RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR,
                                'MERGE_INTO_TIMELINE', RPM_CONSTANTS.CAPT_GTT_MERGE_INTO_TIMELINE,
                                'ROLL_FORWARD',        RPM_CONSTANTS.CAPT_GTT_ROLL_FORWARD,
                                'PAYLOAD_POP',         RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                                'RFR_PURGE',           RPM_CONSTANTS.CAPT_GTT_RFR_PURGE),
                    NULL),
             capture_data_after_start_point,
             -- capture gtt data ind
             1
        from rpm_config_gtt_capture
       where process_name            = 'CONFLICT_CHECKING'
         and enable_gtt_capture      = 'Y'
         and NVL(user_id, I_user_id) = I_user_id;

BEGIN

   -- Set the main variable to not capture data in case there's a session being reused and
   -- the C_DATA cursor doesn't return a record so that we don't pull back data in a case
   -- where we shouldn't be
   LP_capture_gtt_data := 0;

   LP_rfr_gtt_data_captured := 0;
   LP_rpile_gtt_data_captured := 0;
   LP_cspfr_gtt_data_captured := 0;
   LP_clr_gtt_data_captured := 0;
   LP_rfile_gtt_data_captured := 0;

   LP_bulk_cc_pe_id := I_bulk_cc_pe_id;
   LP_parent_thread_number := I_parent_thread_number;
   LP_thread_number := I_thread_number;
   LP_chunk_number := I_chunk_number;

   open C_DATA;
   fetch C_DATA into LP_generic_capture_start_point,
                     LP_end_of_generic_start_point,
                     LP_rfr_gtt_capt_start_point,
                     LP_rpile_gtt_capt_start_point,
                     LP_cspfr_gtt_capt_start_point,
                     LP_clr_gtt_capt_start_point,
                     LP_rile_gtt_capt_start_point,
                     LP_capture_after_start_point,
                     LP_capture_gtt_data;
   close C_DATA;

   -- if all tables are configured to NOT capture data, but the enable flag is on, we will
   -- not capture GTT data
   if LP_rfr_gtt_capt_start_point is NULL and
      LP_rpile_gtt_capt_start_point is NULL and
      LP_cspfr_gtt_capt_start_point is NULL and
      LP_clr_gtt_capt_start_point is NULL and
      LP_rile_gtt_capt_start_point is NULL then
      ---
      LP_capture_gtt_data := 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END INIT_VARS;

--------------------------------------------------------------------------------

FUNCTION PROCESS_GTT_DATA_CAPTURE(O_error_message     OUT VARCHAR2,
                                  I_step_indicator IN     NUMBER,
                                  I_rfr_ind        IN     NUMBER,
                                  I_rpile_ind      IN     NUMBER,
                                  I_cspfr_ind      IN     NUMBER,
                                  I_clr_ind        IN     NUMBER,
                                  I_rfr_ile_ind    IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE';

   L_capture_datetime TIMESTAMP                 := SYSTIMESTAMP;
   L_rfr_data         OBJ_GTT_CAPT_RFR_TBL      := NULL;
   L_rpile_data       OBJ_GTT_CAPT_RPILE_TBL    := NULL;
   L_cspfr_data       OBJ_GTT_CAPT_CSPFR_TBL    := NULL;
   L_clr_data         OBJ_GTT_CAPT_CLR_TBL      := NULL;
   L_rfr_il_expl_data OBJ_GTT_CAPT_RFILEXPL_TBL := NULL;

   cursor C_RFR_DATA is
      select OBJ_GTT_CAPT_RFR_REC(price_event_id,
                                  future_retail_id,
                                  item,
                                  dept,
                                  class,
                                  subclass,
                                  zone_node_type,
                                  location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_exception_parent_id,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  on_simple_promo_ind,
                                  on_complex_promo_ind,
                                  promo_comp_msg_type,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  pc_msg_type,
                                  pc_selling_retail_ind,
                                  pc_multi_unit_ind,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  clear_msg_type,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  timeline_seq,
                                  rfr_rowid,
                                  item_parent,
                                  diff_id,
                                  zone_id,
                                  max_hier_level,
                                  cur_hier_level,
                                  original_fr_id,
                                  step_identifier)
        from rpm_future_retail_gtt
       where LP_rfr_gtt_capt_start_point = I_step_indicator
          or (    LP_capture_after_start_point = 'Y'
              and LP_rfr_gtt_capt_start_point  < I_step_indicator
              and NVL(step_identifier, -1)     = I_step_indicator)
          or (    LP_capture_after_start_point   = 'N'
              and LP_end_of_generic_start_point >= I_step_indicator
              and NVL(step_identifier, -1)       = I_step_indicator)
          or LP_rfr_gtt_data_captured = 0;

   cursor C_RPILE_DATA is
      select OBJ_GTT_CAPT_RPILE_REC(price_event_id,
                                    promo_item_loc_expl_id,
                                    item,
                                    dept,
                                    class,
                                    subclass,
                                    location,
                                    promo_id,
                                    promo_display_id,
                                    promo_secondary_ind,
                                    promo_comp_id,
                                    comp_display_id,
                                    promo_dtl_id,
                                    type,
                                    customer_type,
                                    detail_secondary_ind,
                                    detail_start_date,
                                    detail_end_date,
                                    detail_apply_to_code,
                                    detail_change_type,
                                    detail_change_amount,
                                    detail_change_currency,
                                    detail_change_percent,
                                    detail_change_selling_uom,
                                    detail_price_guide_id,
                                    exception_parent_id,
                                    promo_comp_msg_type,
                                    deleted_ind,
                                    rpile_rowid,
                                    zone_node_type,
                                    zone_id,
                                    item_parent,
                                    diff_id,
                                    max_hier_level,
                                    cur_hier_level,
                                    original_pile_id,
                                    timebased_dtl_ind,
                                    step_identifier)
        from rpm_promo_item_loc_expl_gtt
       where LP_rpile_gtt_capt_start_point = I_step_indicator
          or (    LP_capture_after_start_point = 'Y'
              and LP_rpile_gtt_capt_start_point  < I_step_indicator
              and NVL(step_identifier, -1)     = I_step_indicator)
          or (    LP_capture_after_start_point   = 'N'
              and LP_end_of_generic_start_point >= I_step_indicator
              and NVL(step_identifier, -1)       = I_step_indicator)
          or LP_rpile_gtt_data_captured = 0;

   cursor C_CSPFR_DATA is
      select OBJ_GTT_CAPT_CSPFR_REC(price_event_id,
                                    cust_segment_promo_id,
                                    item,
                                    zone_node_type,
                                    location,
                                    action_date,
                                    customer_type,
                                    dept,
                                    promo_retail,
                                    promo_retail_currency,
                                    promo_uom,
                                    complex_promo_ind,
                                    promo_comp_msg_type,
                                    cspfr_rowid,
                                    zone_id,
                                    item_parent,
                                    diff_id,
                                    max_hier_level,
                                    cur_hier_level,
                                    original_cs_promo_id,
                                    step_identifier)
        from rpm_cust_segment_promo_fr_gtt
       where LP_cspfr_gtt_capt_start_point = I_step_indicator
          or (    LP_capture_after_start_point = 'Y'
              and LP_cspfr_gtt_capt_start_point  < I_step_indicator
              and NVL(step_identifier, -1)     = I_step_indicator)
          or (    LP_capture_after_start_point   = 'N'
              and LP_end_of_generic_start_point >= I_step_indicator
              and NVL(step_identifier, -1)       = I_step_indicator)
          or LP_cspfr_gtt_data_captured = 0;

   cursor C_CLR_DATA is
      select OBJ_GTT_CAPT_CLR_REC(price_event_id,
                                  clearance_id,
                                  clearance_display_id,
                                  state,
                                  reason_code,
                                  exception_parent_id,
                                  reset_ind,
                                  item,
                                  diff_id,
                                  zone_id,
                                  location,
                                  zone_node_type,
                                  effective_date,
                                  out_of_stock_date,
                                  reset_date,
                                  change_type,
                                  change_amount,
                                  change_currency,
                                  change_percent,
                                  change_selling_uom,
                                  price_guide_id,
                                  vendor_funded_ind,
                                  funding_type,
                                  funding_amount,
                                  funding_amount_currency,
                                  funding_percent,
                                  supplier,
                                  deal_id,
                                  deal_detail_id,
                                  partner_type,
                                  partner_id,
                                  create_date,
                                  create_id,
                                  approval_date,
                                  approval_id,
                                  lock_version,
                                  rc_rowid,
                                  skulist,
                                  step_identifier)
        from rpm_clearance_gtt
       where LP_clr_gtt_capt_start_point = I_step_indicator
          or (    LP_capture_after_start_point = 'Y'
              and LP_clr_gtt_capt_start_point  < I_step_indicator
              and NVL(step_identifier, -1)     = I_step_indicator)
          or (    LP_capture_after_start_point   = 'N'
              and LP_end_of_generic_start_point >= I_step_indicator
              and NVL(step_identifier, -1)       = I_step_indicator)
          or LP_clr_gtt_data_captured = 0;

   cursor C_FRILEXPL_DATA is
      select OBJ_GTT_CAPT_RFILEXPL_REC(price_event_id,
                                       future_retail_id,
                                       item,
                                       dept,
                                       class,
                                       subclass,
                                       zone_node_type,
                                       location,
                                       action_date,
                                       selling_retail,
                                       selling_retail_currency,
                                       selling_uom,
                                       multi_units,
                                       multi_unit_retail,
                                       multi_unit_retail_currency,
                                       multi_selling_uom,
                                       clear_exception_parent_id,
                                       clear_retail,
                                       clear_retail_currency,
                                       clear_uom,
                                       simple_promo_retail,
                                       simple_promo_retail_currency,
                                       simple_promo_uom,
                                       complex_promo_uom,
                                       promo_comp_msg_type,
                                       price_change_id,
                                       price_change_display_id,
                                       pc_exception_parent_id,
                                       pc_change_type,
                                       pc_change_amount,
                                       pc_change_currency,
                                       pc_change_percent,
                                       pc_change_selling_uom,
                                       pc_null_multi_ind,
                                       pc_multi_units,
                                       pc_multi_unit_retail,
                                       pc_multi_unit_retail_currency,
                                       pc_multi_selling_uom,
                                       pc_price_guide_id,
                                       pc_msg_type,
                                       pc_selling_retail_ind,
                                       pc_multi_unit_ind,
                                       clearance_id,
                                       clearance_display_id,
                                       clear_mkdn_index,
                                       clear_start_ind,
                                       clear_change_type,
                                       clear_change_amount,
                                       clear_change_currency,
                                       clear_change_percent,
                                       clear_change_selling_uom,
                                       clear_price_guide_id,
                                       clear_msg_type,
                                       loc_move_from_zone_id,
                                       loc_move_to_zone_id,
                                       location_move_id,
                                       promo_dtl_id,
                                       type,
                                       customer_type,
                                       detail_start_date,
                                       detail_end_date,
                                       promo_retail,
                                       promo_uom,
                                       promo_id,
                                       exception_parent_id,
                                       step_identifier)
        from rpm_fr_item_loc_expl_gtt
       where LP_rile_gtt_capt_start_point = I_step_indicator
          or (    LP_capture_after_start_point = 'Y'
              and LP_rile_gtt_capt_start_point  < I_step_indicator
              and NVL(step_identifier, -1)     = I_step_indicator)
          or (    LP_capture_after_start_point   = 'N'
              and LP_end_of_generic_start_point >= I_step_indicator
              and NVL(step_identifier, -1)       = I_step_indicator)
          or LP_rfile_gtt_data_captured = 0;

BEGIN

   -- if we're not capturing data or
   --    we're not far enough into the processing or
   --    we're not capturing data beyond the start point, just return success

   if LP_capture_gtt_data = 0 or
      I_step_indicator < LP_generic_capture_start_point or
      (LP_capture_after_start_point = 'N' and
       LP_generic_capture_start_point > I_step_indicator) then
      ---
      return 1;
   end if;

   -- Only open the necessary cursors based on the input indicators and config

   if I_rfr_ind = 1 and
      LP_rfr_gtt_capt_start_point is NOT NULL and
      (I_step_indicator BETWEEN LP_generic_capture_start_point and LP_end_of_generic_start_point or
       (LP_capture_after_start_point = 'Y' and
        LP_rfr_gtt_capt_start_point  < I_step_indicator)) then
      ---
      open C_RFR_DATA;
   end if;

   if I_rpile_ind = 1 and
      LP_rpile_gtt_capt_start_point is NOT NULL and
      (I_step_indicator BETWEEN LP_generic_capture_start_point and LP_end_of_generic_start_point or
       (LP_capture_after_start_point = 'Y' and
        LP_rpile_gtt_capt_start_point  < I_step_indicator)) then
      ---
      open C_RPILE_DATA;
   end if;

   if I_cspfr_ind = 1 and
      LP_cspfr_gtt_capt_start_point is NOT NULL and
      (I_step_indicator BETWEEN LP_generic_capture_start_point and LP_end_of_generic_start_point or
       (LP_capture_after_start_point = 'Y' and
        LP_cspfr_gtt_capt_start_point  < I_step_indicator)) then
      ---
      open C_CSPFR_DATA;
   end if;

   if I_clr_ind = 1 and
      LP_clr_gtt_capt_start_point is NOT NULL and
      (I_step_indicator BETWEEN LP_generic_capture_start_point and LP_end_of_generic_start_point or
       (LP_capture_after_start_point = 'Y' and
        LP_clr_gtt_capt_start_point  < I_step_indicator)) then
      ---
      open C_CLR_DATA;
   end if;

   if I_rfr_ile_ind = 1 and
      LP_rile_gtt_capt_start_point is NOT NULL and
      (I_step_indicator BETWEEN LP_generic_capture_start_point and LP_end_of_generic_start_point or
       (LP_capture_after_start_point = 'Y' and
        LP_rile_gtt_capt_start_point  < I_step_indicator)) then
      ---
      open C_FRILEXPL_DATA;
   end if;

   -- Loop until EVERY cursor is done pulling data.  If one runs out of data before others, the
   -- the fetch will result in a collection that has zero records in it and the autonomous logic
   -- will do nothing with it - even if the cursor has multiple iterations after running out of
   -- data to pull from the GTT.
   loop

      if C_RFR_DATA%ISOPEN then
         fetch C_RFR_DATA BULK COLLECT into L_rfr_data limit RPM_CONSTANTS.CAPT_GTT_QUERY_LUW;

         if L_rfr_data.COUNT > 0 then
            LP_rfr_gtt_data_captured := 1;
         end if;

      end if;

      if C_RPILE_DATA%ISOPEN then
         fetch C_RPILE_DATA BULK COLLECT INTO L_rpile_data limit RPM_CONSTANTS.CAPT_GTT_QUERY_LUW;

         if L_rpile_data.COUNT > 0 then
            LP_rpile_gtt_data_captured := 1;
         end if;

      end if;

      if C_CSPFR_DATA%ISOPEN then
         fetch C_CSPFR_DATA BULK COLLECT INTO L_cspfr_data limit RPM_CONSTANTS.CAPT_GTT_QUERY_LUW;

         if L_cspfr_data.COUNT > 0 then
            LP_cspfr_gtt_data_captured := 1;
         end if;

      end if;

      if C_CLR_DATA%ISOPEN then
         fetch C_CLR_DATA BULK COLLECT INTO L_clr_data limit RPM_CONSTANTS.CAPT_GTT_QUERY_LUW;

         if L_clr_data.COUNT > 0 then
            LP_clr_gtt_data_captured := 1;
         end if;

      end if;

      if C_FRILEXPL_DATA%ISOPEN then
         fetch C_FRILEXPL_DATA BULK COLLECT INTO L_rfr_il_expl_data limit RPM_CONSTANTS.CAPT_GTT_QUERY_LUW;

         if L_rfr_il_expl_data.COUNT > 0 then
            LP_rfile_gtt_data_captured := 1;
         end if;

      end if;

      if CAPTURE_GTT_DATA(O_error_message,
                          L_capture_datetime,
                          L_rfr_data,
                          L_rpile_data,
                          L_cspfr_data,
                          L_clr_data,
                          L_rfr_il_expl_data) = 0 then
         return 0;
      end if;

      -- Exit the loop once all data that is to be captured has been captured for every table
      -- where the input indicator is set
      exit when (I_rfr_ind = 0 or
                 L_rfr_data is NULL or
                 L_rfr_data.COUNT < RPM_CONSTANTS.CAPT_GTT_QUERY_LUW) and
                (I_rpile_ind = 0 or
                 L_rpile_data is NULL or
                 L_rpile_data.COUNT < RPM_CONSTANTS.CAPT_GTT_QUERY_LUW) and
                (I_cspfr_ind = 0 or
                 L_cspfr_data is NULL or
                 L_cspfr_data.COUNT < RPM_CONSTANTS.CAPT_GTT_QUERY_LUW) and
                (I_clr_ind = 0 or
                 L_clr_data is NULL or
                 L_clr_data.COUNT < RPM_CONSTANTS.CAPT_GTT_QUERY_LUW) and
                (I_rfr_ile_ind = 0 or
                 L_rfr_il_expl_data is NULL or
                 L_rfr_il_expl_data.COUNT < RPM_CONSTANTS.CAPT_GTT_QUERY_LUW);

   end loop;

   -- If a cursor was opened before the loop - close it here...

   if C_RFR_DATA%ISOPEN then
      close C_RFR_DATA;
   end if;

   if C_RPILE_DATA%ISOPEN then
      close C_RPILE_DATA;
   end if;

   if C_CSPFR_DATA%ISOPEN then
      close C_CSPFR_DATA;
   end if;

   if C_CLR_DATA%ISOPEN then
      close C_CLR_DATA;
   end if;

   if C_FRILEXPL_DATA%ISOPEN then
      close C_FRILEXPL_DATA;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END PROCESS_GTT_DATA_CAPTURE;

--------------------------------------------------------------------------------

FUNCTION CAPTURE_GTT_DATA(O_error_message        OUT VARCHAR2,
                          I_capture_date_time IN     TIMESTAMP,
                          I_rfr_data          IN     OBJ_GTT_CAPT_RFR_TBL,
                          I_rpile_data        IN     OBJ_GTT_CAPT_RPILE_TBL,
                          I_cspfr_data        IN     OBJ_GTT_CAPT_CSPFR_TBL,
                          I_clr_data          IN     OBJ_GTT_CAPT_CLR_TBL,
                          I_rfr_il_expl_data  IN     OBJ_GTT_CAPT_RFILEXPL_TBL)
RETURN NUMBER IS

   PRAGMA AUTONOMOUS_TRANSACTION;

   L_program VARCHAR2(40) := 'RPM_CAPTURE_GTT_SQL.CAPTURE_GTT_DATA';

BEGIN

   if I_rfr_data is NOT NULL and
      I_rfr_data.COUNT > 0 then
      ---
      forall i IN 1..I_rfr_data.COUNT
         insert into rpm_rfr_gtt_data_capture
            (bulk_cc_pe_id,
             parent_thread_number,
             thread_number,
             chunk_number,
             dt_time,
             price_event_id,
             future_retail_id,
             item,
             dept,
             class,
             subclass,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_exception_parent_id,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             on_simple_promo_ind,
             on_complex_promo_ind,
             promo_comp_msg_type,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             pc_msg_type,
             pc_selling_retail_ind,
             pc_multi_unit_ind,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             clear_msg_type,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             timeline_seq,
             rfr_rowid,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level,
             original_fr_id,
             step_identifier)
      values (LP_bulk_cc_pe_id,
              LP_parent_thread_number,
              LP_thread_number,
              LP_chunk_number,
              I_capture_date_time,
              I_rfr_data(i).price_event_id,
              I_rfr_data(i).future_retail_id,
              I_rfr_data(i).item,
              I_rfr_data(i).dept,
              I_rfr_data(i).class,
              I_rfr_data(i).subclass,
              I_rfr_data(i).zone_node_type,
              I_rfr_data(i).location,
              I_rfr_data(i).action_date,
              I_rfr_data(i).selling_retail,
              I_rfr_data(i).selling_retail_currency,
              I_rfr_data(i).selling_uom,
              I_rfr_data(i).multi_units,
              I_rfr_data(i).multi_unit_retail,
              I_rfr_data(i).multi_unit_retail_currency,
              I_rfr_data(i).multi_selling_uom,
              I_rfr_data(i).clear_exception_parent_id,
              I_rfr_data(i).clear_retail,
              I_rfr_data(i).clear_retail_currency,
              I_rfr_data(i).clear_uom,
              I_rfr_data(i).simple_promo_retail,
              I_rfr_data(i).simple_promo_retail_currency,
              I_rfr_data(i).simple_promo_uom,
              I_rfr_data(i).on_simple_promo_ind,
              I_rfr_data(i).on_complex_promo_ind,
              I_rfr_data(i).promo_comp_msg_type,
              I_rfr_data(i).price_change_id,
              I_rfr_data(i).price_change_display_id,
              I_rfr_data(i).pc_exception_parent_id,
              I_rfr_data(i).pc_change_type,
              I_rfr_data(i).pc_change_amount,
              I_rfr_data(i).pc_change_currency,
              I_rfr_data(i).pc_change_percent,
              I_rfr_data(i).pc_change_selling_uom,
              I_rfr_data(i).pc_null_multi_ind,
              I_rfr_data(i).pc_multi_units,
              I_rfr_data(i).pc_multi_unit_retail,
              I_rfr_data(i).pc_multi_unit_retail_currency,
              I_rfr_data(i).pc_multi_selling_uom,
              I_rfr_data(i).pc_price_guide_id,
              I_rfr_data(i).pc_msg_type,
              I_rfr_data(i).pc_selling_retail_ind,
              I_rfr_data(i).pc_multi_unit_ind,
              I_rfr_data(i).clearance_id,
              I_rfr_data(i).clearance_display_id,
              I_rfr_data(i).clear_mkdn_index,
              I_rfr_data(i).clear_start_ind,
              I_rfr_data(i).clear_change_type,
              I_rfr_data(i).clear_change_amount,
              I_rfr_data(i).clear_change_currency,
              I_rfr_data(i).clear_change_percent,
              I_rfr_data(i).clear_change_selling_uom,
              I_rfr_data(i).clear_price_guide_id,
              I_rfr_data(i).clear_msg_type,
              I_rfr_data(i).loc_move_from_zone_id,
              I_rfr_data(i).loc_move_to_zone_id,
              I_rfr_data(i).location_move_id,
              I_rfr_data(i).lock_version,
              I_rfr_data(i).timeline_seq,
              I_rfr_data(i).rfr_rowid,
              I_rfr_data(i).item_parent,
              I_rfr_data(i).diff_id,
              I_rfr_data(i).zone_id,
              I_rfr_data(i).max_hier_level,
              I_rfr_data(i).cur_hier_level,
              I_rfr_data(i).original_fr_id,
              I_rfr_data(i).step_identifier);

   end if;

   if I_rpile_data is NOT NULL and
      I_rpile_data.COUNT > 0 then
      ---
      forall i IN 1..I_rpile_data.COUNT
         insert into rpm_rpile_gtt_data_capture
            (bulk_cc_pe_id,
             parent_thread_number,
             thread_number,
             chunk_number,
             dt_time,
             price_event_id,
             promo_item_loc_expl_id,
             item,
             dept,
             class,
             subclass,
             location,
             promo_id,
             promo_display_id,
             promo_secondary_ind,
             promo_comp_id,
             comp_display_id,
             promo_dtl_id,
             type,
             customer_type,
             detail_secondary_ind,
             detail_start_date,
             detail_end_date,
             detail_apply_to_code,
             detail_change_type,
             detail_change_amount,
             detail_change_currency,
             detail_change_percent,
             detail_change_selling_uom,
             detail_price_guide_id,
             exception_parent_id,
             promo_comp_msg_type,
             deleted_ind,
             rpile_rowid,
             zone_node_type,
             zone_id,
             item_parent,
             diff_id,
             max_hier_level,
             cur_hier_level,
             original_pile_id,
             timebased_dtl_ind,
             step_identifier)
      values (LP_bulk_cc_pe_id,
              LP_parent_thread_number,
              LP_thread_number,
              LP_chunk_number,
              I_capture_date_time,
              I_rpile_data(i).price_event_id,
              I_rpile_data(i).promo_item_loc_expl_id,
              I_rpile_data(i).item,
              I_rpile_data(i).dept,
              I_rpile_data(i).class,
              I_rpile_data(i).subclass,
              I_rpile_data(i).location,
              I_rpile_data(i).promo_id,
              I_rpile_data(i).promo_display_id,
              I_rpile_data(i).promo_secondary_ind,
              I_rpile_data(i).promo_comp_id,
              I_rpile_data(i).comp_display_id,
              I_rpile_data(i).promo_dtl_id,
              I_rpile_data(i).type,
              I_rpile_data(i).customer_type,
              I_rpile_data(i).detail_secondary_ind,
              I_rpile_data(i).detail_start_date,
              I_rpile_data(i).detail_end_date,
              I_rpile_data(i).detail_apply_to_code,
              I_rpile_data(i).detail_change_type,
              I_rpile_data(i).detail_change_amount,
              I_rpile_data(i).detail_change_currency,
              I_rpile_data(i).detail_change_percent,
              I_rpile_data(i).detail_change_selling_uom,
              I_rpile_data(i).detail_price_guide_id,
              I_rpile_data(i).exception_parent_id,
              I_rpile_data(i).promo_comp_msg_type,
              I_rpile_data(i).deleted_ind,
              I_rpile_data(i).rpile_rowid,
              I_rpile_data(i).zone_node_type,
              I_rpile_data(i).zone_id,
              I_rpile_data(i).item_parent,
              I_rpile_data(i).diff_id,
              I_rpile_data(i).max_hier_level,
              I_rpile_data(i).cur_hier_level,
              I_rpile_data(i).original_pile_id,
              I_rpile_data(i).timebased_dtl_ind,
              I_rpile_data(i).step_identifier);

   end if;

   if I_cspfr_data is NOT NULL and
      I_cspfr_data.COUNT > 0 then
      ---
      forall i IN 1..I_cspfr_data.COUNT
         insert into rpm_cspfr_gtt_data_capture
            (bulk_cc_pe_id,
             parent_thread_number,
             thread_number,
             chunk_number,
             dt_time,
             price_event_id,
             cust_segment_promo_id,
             item,
             zone_node_type,
             location,
             action_date,
             customer_type,
             dept,
             promo_retail,
             promo_retail_currency,
             promo_uom,
             complex_promo_ind,
             promo_comp_msg_type,
             cspfr_rowid,
             zone_id,
             item_parent,
             diff_id,
             max_hier_level,
             cur_hier_level,
             original_cs_promo_id,
             step_identifier)
      values (LP_bulk_cc_pe_id,
              LP_parent_thread_number,
              LP_thread_number,
              LP_chunk_number,
              I_capture_date_time,
              I_cspfr_data(i).price_event_id,
              I_cspfr_data(i).cust_segment_promo_id,
              I_cspfr_data(i).item,
              I_cspfr_data(i).zone_node_type,
              I_cspfr_data(i).location,
              I_cspfr_data(i).action_date,
              I_cspfr_data(i).customer_type,
              I_cspfr_data(i).dept,
              I_cspfr_data(i).promo_retail,
              I_cspfr_data(i).promo_retail_currency,
              I_cspfr_data(i).promo_uom,
              I_cspfr_data(i).complex_promo_ind,
              I_cspfr_data(i).promo_comp_msg_type,
              I_cspfr_data(i).cspfr_rowid,
              I_cspfr_data(i).zone_id,
              I_cspfr_data(i).item_parent,
              I_cspfr_data(i).diff_id,
              I_cspfr_data(i).max_hier_level,
              I_cspfr_data(i).cur_hier_level,
              I_cspfr_data(i).original_cs_promo_id,
              I_cspfr_data(i).step_identifier);

   end if;

   if I_clr_data is NOT NULL and
      I_clr_data.COUNT > 0 then
      ---
      forall i IN 1..I_clr_data.COUNT
         insert into rpm_clr_gtt_data_capture
            (bulk_cc_pe_id,
             parent_thread_number,
             thread_number,
             chunk_number,
             dt_time,
             price_event_id,
             clearance_id,
             clearance_display_id,
             state,
             reason_code,
             exception_parent_id,
             reset_ind,
             item,
             diff_id,
             zone_id,
             location,
             zone_node_type,
             effective_date,
             out_of_stock_date,
             reset_date,
             change_type,
             change_amount,
             change_currency,
             change_percent,
             change_selling_uom,
             price_guide_id,
             vendor_funded_ind,
             funding_type,
             funding_amount,
             funding_amount_currency,
             funding_percent,
             supplier,
             deal_id,
             deal_detail_id,
             partner_type,
             partner_id,
             create_date,
             create_id,
             approval_date,
             approval_id,
             lock_version,
             rc_rowid,
             skulist,
             step_identifier)
      values (LP_bulk_cc_pe_id,
              LP_parent_thread_number,
              LP_thread_number,
              LP_chunk_number,
              I_capture_date_time,
              I_clr_data(i).price_event_id,
              I_clr_data(i).clearance_id,
              I_clr_data(i).clearance_display_id,
              I_clr_data(i).state,
              I_clr_data(i).reason_code,
              I_clr_data(i).exception_parent_id,
              I_clr_data(i).reset_ind,
              I_clr_data(i).item,
              I_clr_data(i).diff_id,
              I_clr_data(i).zone_id,
              I_clr_data(i).location,
              I_clr_data(i).zone_node_type,
              I_clr_data(i).effective_date,
              I_clr_data(i).out_of_stock_date,
              I_clr_data(i).reset_date,
              I_clr_data(i).change_type,
              I_clr_data(i).change_amount,
              I_clr_data(i).change_currency,
              I_clr_data(i).change_percent,
              I_clr_data(i).change_selling_uom,
              I_clr_data(i).price_guide_id,
              I_clr_data(i).vendor_funded_ind,
              I_clr_data(i).funding_type,
              I_clr_data(i).funding_amount,
              I_clr_data(i).funding_amount_currency,
              I_clr_data(i).funding_percent,
              I_clr_data(i).supplier,
              I_clr_data(i).deal_id,
              I_clr_data(i).deal_detail_id,
              I_clr_data(i).partner_type,
              I_clr_data(i).partner_id,
              I_clr_data(i).create_date,
              I_clr_data(i).create_id,
              I_clr_data(i).approval_date,
              I_clr_data(i).approval_id,
              I_clr_data(i).lock_version,
              I_clr_data(i).rc_rowid,
              I_clr_data(i).skulist,
              I_clr_data(i).step_identifier);

   end if;

   if I_rfr_il_expl_data is NOT NULL and
      I_rfr_il_expl_data.COUNT > 0 then
      ---
      forall i IN 1..I_rfr_il_expl_data.COUNT
         insert into rpm_frile_gtt_data_capture
            (bulk_cc_pe_id,
             parent_thread_number,
             thread_number,
             chunk_number,
             dt_time,
             price_event_id,
             future_retail_id,
             item,
             dept,
             class,
             subclass,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_exception_parent_id,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             complex_promo_uom,
             promo_comp_msg_type,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             pc_msg_type,
             pc_selling_retail_ind,
             pc_multi_unit_ind,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             clear_msg_type,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             promo_dtl_id,
             type,
             customer_type,
             detail_start_date,
             detail_end_date,
             promo_retail,
             promo_uom,
             promo_id,
             exception_parent_id,
             step_identifier)
      values (LP_bulk_cc_pe_id,
              LP_parent_thread_number,
              LP_thread_number,
              LP_chunk_number,
              I_capture_date_time,
              I_rfr_il_expl_data(i).price_event_id,
              I_rfr_il_expl_data(i).future_retail_id,
              I_rfr_il_expl_data(i).item,
              I_rfr_il_expl_data(i).dept,
              I_rfr_il_expl_data(i).class,
              I_rfr_il_expl_data(i).subclass,
              I_rfr_il_expl_data(i).zone_node_type,
              I_rfr_il_expl_data(i).location,
              I_rfr_il_expl_data(i).action_date,
              I_rfr_il_expl_data(i).selling_retail,
              I_rfr_il_expl_data(i).selling_retail_currency,
              I_rfr_il_expl_data(i).selling_uom,
              I_rfr_il_expl_data(i).multi_units,
              I_rfr_il_expl_data(i).multi_unit_retail,
              I_rfr_il_expl_data(i).multi_unit_retail_currency,
              I_rfr_il_expl_data(i).multi_selling_uom,
              I_rfr_il_expl_data(i).clear_exception_parent_id,
              I_rfr_il_expl_data(i).clear_retail,
              I_rfr_il_expl_data(i).clear_retail_currency,
              I_rfr_il_expl_data(i).clear_uom,
              I_rfr_il_expl_data(i).simple_promo_retail,
              I_rfr_il_expl_data(i).simple_promo_retail_currency,
              I_rfr_il_expl_data(i).simple_promo_uom,
              I_rfr_il_expl_data(i).complex_promo_uom,
              I_rfr_il_expl_data(i).promo_comp_msg_type,
              I_rfr_il_expl_data(i).price_change_id,
              I_rfr_il_expl_data(i).price_change_display_id,
              I_rfr_il_expl_data(i).pc_exception_parent_id,
              I_rfr_il_expl_data(i).pc_change_type,
              I_rfr_il_expl_data(i).pc_change_amount,
              I_rfr_il_expl_data(i).pc_change_currency,
              I_rfr_il_expl_data(i).pc_change_percent,
              I_rfr_il_expl_data(i).pc_change_selling_uom,
              I_rfr_il_expl_data(i).pc_null_multi_ind,
              I_rfr_il_expl_data(i).pc_multi_units,
              I_rfr_il_expl_data(i).pc_multi_unit_retail,
              I_rfr_il_expl_data(i).pc_multi_unit_retail_currency,
              I_rfr_il_expl_data(i).pc_multi_selling_uom,
              I_rfr_il_expl_data(i).pc_price_guide_id,
              I_rfr_il_expl_data(i).pc_msg_type,
              I_rfr_il_expl_data(i).pc_selling_retail_ind,
              I_rfr_il_expl_data(i).pc_multi_unit_ind,
              I_rfr_il_expl_data(i).clearance_id,
              I_rfr_il_expl_data(i).clearance_display_id,
              I_rfr_il_expl_data(i).clear_mkdn_index,
              I_rfr_il_expl_data(i).clear_start_ind,
              I_rfr_il_expl_data(i).clear_change_type,
              I_rfr_il_expl_data(i).clear_change_amount,
              I_rfr_il_expl_data(i).clear_change_currency,
              I_rfr_il_expl_data(i).clear_change_percent,
              I_rfr_il_expl_data(i).clear_change_selling_uom,
              I_rfr_il_expl_data(i).clear_price_guide_id,
              I_rfr_il_expl_data(i).clear_msg_type,
              I_rfr_il_expl_data(i).loc_move_from_zone_id,
              I_rfr_il_expl_data(i).loc_move_to_zone_id,
              I_rfr_il_expl_data(i).location_move_id,
              I_rfr_il_expl_data(i).promo_dtl_id,
              I_rfr_il_expl_data(i).type,
              I_rfr_il_expl_data(i).customer_type,
              I_rfr_il_expl_data(i).detail_start_date,
              I_rfr_il_expl_data(i).detail_end_date,
              I_rfr_il_expl_data(i).promo_retail,
              I_rfr_il_expl_data(i).promo_uom,
              I_rfr_il_expl_data(i).promo_id,
              I_rfr_il_expl_data(i).exception_parent_id,
              I_rfr_il_expl_data(i).step_identifier);

   end if;

   commit;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      rollback;

      return 0;

END CAPTURE_GTT_DATA;

--------------------------------------------------------------------------------

FUNCTION PURGE_CAPTURE_GTT_DATA(O_error_message    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CAPTURE_GTT_SQL.PURGE_CAPTURE_GTT_DATA';

BEGIN

   EXECUTE IMMEDIATE 'truncate table rpm_rfr_gtt_data_capture';
   EXECUTE IMMEDIATE 'truncate table rpm_rpile_gtt_data_capture';
   EXECUTE IMMEDIATE 'truncate table rpm_cspfr_gtt_data_capture';
   EXECUTE IMMEDIATE 'truncate table rpm_clr_gtt_data_capture';
   EXECUTE IMMEDIATE 'truncate table rpm_frile_gtt_data_capture';

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return 0;

END PURGE_CAPTURE_GTT_DATA;

--------------------------------------------------------------------------------

END RPM_CAPTURE_GTT_SQL;
/
