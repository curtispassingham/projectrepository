CREATE OR REPLACE PACKAGE RPM_UOM_VALIDATION_SQL AS
--------------------------------------------------------
FUNCTION SAME_SELLING_UOM(O_cc_error_tbl               IN OUT CONFLICT_CHECK_ERROR_TBL,
                          O_unique_uom                    OUT RPM_FUTURE_RETAIL.SELLING_UOM%TYPE,
                          I_merch_nd_zone_nd_date_tbl  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                          I_price_event_type           IN     VARCHAR2  DEFAULT NULL,
                          I_price_event_id             IN     NUMBER    DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------
END;
/