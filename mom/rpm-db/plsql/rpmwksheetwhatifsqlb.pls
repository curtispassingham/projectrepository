CREATE OR REPLACE PACKAGE BODY RPM_WORKSHEET_WHATIF_SQL AS
--------------------------------------------------------------------------------------
LP_workspace_id     NUMBER(20)    := NULL;
LP_default_tax_type VARCHAR2(6)   := NULL;
LP_error_flag       NUMBER(1)     := NULL;
LP_error_msg        VARCHAR2(250) := NULL;

--------------------------------------------------------------------------------------
FUNCTION APPLY_CLEARANCE(O_error_msg           IN OUT VARCHAR2,
                         I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                         I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                         I_whatif_strategy_clr IN     OBJ_WHATIF_STRATEGY_CLR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------------
FUNCTION APPLY_MARGIN(O_error_msg           IN OUT VARCHAR2,
                      I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                      I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                      I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------------
FUNCTION APPLY_MAINT_MARGIN(O_error_msg           IN OUT VARCHAR2,
                            I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                            I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                            I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------------
FUNCTION APPLY_COMPETITIVE(O_error_msg           IN OUT VARCHAR2,
                           I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                           I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                           I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------------
FUNCTION APPLY_PRICE_GUIDE(I_selling_retail IN     NUMBER,
                           I_price_guide_id IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------------

FUNCTION APPLY_STRATEGY(O_error_msg           IN OUT VARCHAR2,
                        I_workspace_id        IN     NUMBER,
                        I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                        I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                        I_whatif_strategy_clr IN     OBJ_WHATIF_STRATEGY_CLR_TBL,
                        I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(100) := 'RPM_WORKSHEET_WHATIF_SQL.APPLY_STRATEGY';
   L_whatif_strategy        OBJ_WHATIF_STRATEGY_REC;
   L_default_tax_type       VARCHAR2(6);

   cursor C_OPT is
   select default_tax_type
     from system_options;

BEGIN

   LP_workspace_id   := I_workspace_id;

   open C_OPT;
   fetch C_OPT
    into LP_default_tax_type;
   close C_OPT;

   -- Assume it will only process one type of strategy at a time
   L_whatif_strategy := I_whatif_strategy(1);

   if L_whatif_strategy.strategy_type = CLEARANCE then
      if APPLY_CLEARANCE(O_error_msg,
                         I_whatif_rollup_id,
                         I_whatif_strategy,
                         I_whatif_strategy_clr) = 0 then
         return 0;
      end if;
   elsif L_whatif_strategy.strategy_type = MARGIN then
      if APPLY_MARGIN(O_error_msg,
                      I_whatif_rollup_id,
                      I_whatif_strategy,
                      I_whatif_strategy_dtl) = 0 then
         return 0;
      end if;
   elsif L_whatif_strategy.strategy_type = MAINTAIN_MARGIN then
      if APPLY_MAINT_MARGIN(O_error_msg,
                            I_whatif_rollup_id,
                            I_whatif_strategy,
                            I_whatif_strategy_dtl) = 0 then
         return 0;
      end if;
   elsif L_whatif_strategy.strategy_type = COMPETITIVE then
      if APPLY_COMPETITIVE(O_error_msg,
                           I_whatif_rollup_id,
                           I_whatif_strategy,
                           I_whatif_strategy_dtl) = 0 then
         return 0;
      end if;
   end if;

   merge into rpm_worksheet_zone_workspace target
   using (select /*+ CARDINALITY (ids, 100) */
                 rzw.workspace_id,
                 rzw.worksheet_item_data_id,
                 avg(rilw.proposed_retail) proposed_retail,
                 case max(nvl(rilw.proposed_retail,0))
                 when min(nvl(rilw.proposed_retail,0))
                    then 0
                    else 1
                 end as proposed_retail_flag,
                 sum(rilw.proposed_retail_std) proposed_retail_std,
                 max(rilw.proposed_clear_mkdn_nbr) proposed_clear_mkdn_nbr,
                 case max(nvl(rilw.proposed_clear_mkdn_nbr,0))
                 when min(nvl(rilw.proposed_clear_mkdn_nbr,0))
                    then 0
                    else 1
                 end as proposed_clear_mkdn_nbr_flag,
                 min(nvl(rilw.new_clear_ind,0)) new_clear_ind,
                 case max(nvl(rilw.new_clear_ind, 0))
                 when min(nvl(rilw.new_clear_ind, 0))
                    then 0
                    else 1
                 end as new_clear_ind_flag,
                 min(rilw.proposed_retail_uom) proposed_retail_uom,
                 case max(nvl(rilw.proposed_retail_uom,0))
                 when min(nvl(rilw.proposed_retail_uom,0))
                    then 0
                    else 1
                 end as proposed_retail_uom_flag,
                 avg(rilw.new_retail) new_retail,
                 case max(nvl(rilw.new_retail, 0))
                 when min(nvl(rilw.new_retail, 0))
                    then 0
                    else 1
                 end as new_retail_flag,
                 min (case
                      when rilw.new_retail is null
                         then 0
                         else 1
                      end) as new_retail_is_set,
                 min (case
                      when rilw.new_retail is null and rilw.new_multi_unit_retail is null
                        then 0
                        else 1
                     end) as either_retail_is_set,
                 case max(rilw.action_flag)
                 when 0
                    then sum((rilw.projected_sales * rilw.new_retail)-(rilw.basis_regular_retail * rilw.projected_sales))
                    else sum(case rilw.action_flag
                             when 1
                                then (rilw.projected_sales * rilw.new_retail)-(rilw.basis_regular_retail * rilw.projected_sales)
                                else 0
                             end)
                 end as v_sales_ch_amt_ncurrent,
                 sum((rilw.projected_sales * rilw.new_retail)-(rilw.basis_regular_retail * rilw.projected_sales)) as sales_change_amount_ncurrent,
                 avg(rilw.new_retail_std) new_retail_std,
                 sum(case
                     when rilw.new_retail_std is not null
                        then rilw.new_retail_std
                        else
                           case
                           when rilw.current_clearance_ind = 1
                              then
                                 case
                                 when rzw.basis_zl_clear_retail_uom = rzw.standard_uom
                                    then rzw.basis_zl_clear_retail
                                    else null
                                 end
                              else
                                 case
                                 when rzw.basis_zl_regular_retail_uom = rzw.standard_uom
                                    then rzw.basis_zl_regular_retail
                                    else null
                                 end
                           end
                     end) as sum_nmp_retail,
                 avg(case
                     when rilw.new_retail_std is not null
                        then rilw.new_retail_std
                        else
                           case
                           when rilw.current_clearance_ind = 1
                              then
                                 case
                                 when rzw.basis_zl_clear_retail_uom = rzw.standard_uom
                                    then rzw.basis_zl_clear_retail
                                    else null
                                 end
                              else
                                 case
                                 when rzw.basis_zl_regular_retail_uom = rzw.standard_uom
                                    then rzw.basis_zl_regular_retail
                                    else null
                                 end
                           end
                     end) as avg_nmp_retail,
                 max(rilw.new_clear_mkdn_nbr) new_clear_mkdn_nbr,
                 case max(nvl(rilw.new_clear_mkdn_nbr, 0))
                 when min(nvl(rilw.new_clear_mkdn_nbr, 0))
                   then 0
                    else 1
                 end as new_clear_mkdn_nbr_flag,
                 min(rilw.new_retail_uom) new_retail_uom,
                 case max(nvl(rilw.new_retail_uom,0))
                 when min(nvl(rilw.new_retail_uom,0))
                    then 0
                    else 1
                 end as new_retail_uom_flag,
                 min(rilw.new_out_of_stock_date) new_out_of_stock_date,
                 case max(nvl(rilw.new_out_of_stock_date, sysdate))
                 when min(nvl(rilw.new_out_of_stock_date, sysdate))
                    then 0
                    else 1
                 end as new_out_of_stock_date_flag,
                 min(rilw.new_reset_date) new_reset_date,
                 case max(nvl(rilw.new_reset_date, sysdate))
                 when min(nvl(rilw.new_reset_date, sysdate))
                    then 0
                    else 1
                 end as new_reset_date_flag
            from table(cast(I_whatif_rollup_id as OBJ_WHATIF_ROLLUP_ID_TBL)) ids,
                 rpm_worksheet_zone_workspace rzw,
                 rpm_worksheet_il_workspace rilw
           where rzw.workspace_id = LP_workspace_id
             and rzw.item = NVL(ids.item, rzw.item)
             and rzw.zone_id = ids.zone_id
             and ((rzw.item_parent is NULL and ids.item_parent is NULL) or
                  (rzw.item_parent = ids.item_parent))
             and ((rzw.link_code is NULL and ids.link_code is NULL) or
                  (rzw.link_code = ids.link_code))
             and ((ids.diff_id is NULL) or
                  (rzw.diff_type_1 = ids.diff_type and rzw.diff_1 = ids.diff_id) or
                  (rzw.diff_type_2 = ids.diff_type and rzw.diff_2 = ids.diff_id) or
                  (rzw.diff_type_3 = ids.diff_type and rzw.diff_3 = ids.diff_id) or
                  (rzw.diff_type_4 = ids.diff_type and rzw.diff_4 = ids.diff_id))
             and rzw.basis_zl_base_cost is NOT NULL
             and rilw.workspace_id = rzw.workspace_id
             and rilw.worksheet_item_data_id = rzw.worksheet_item_data_id
      group by rzw.workspace_id,
               rzw.dept,
               rzw.worksheet_status_id,
               rzw.worksheet_item_data_id,
               rilw.link_code,
               rzw.item,
               rzw.zone_id) source
   on (    target.workspace_id = source.workspace_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then
   update set
      target.proposed_retail = source.proposed_retail,
      target.proposed_retail_flag = source.proposed_retail_flag,
      target.proposed_retail_std = source.proposed_retail_std,
      target.proposed_clear_mkdn_nbr = source.proposed_clear_mkdn_nbr,
      target.proposed_clear_mkdn_nbr_flag = source.proposed_clear_mkdn_nbr_flag,
      target.new_clear_ind = source.new_clear_ind,
      target.new_clear_ind_flag = source.new_clear_ind_flag,
      target.proposed_retail_uom = source.proposed_retail_uom,
      target.proposed_retail_uom_flag = source.proposed_retail_uom_flag,
      target.new_retail = source.new_retail,
      target.new_retail_flag = source.new_retail_flag,
      target.new_retail_is_set = source.new_retail_is_set,
      target.either_retail_is_set = source.either_retail_is_set,
      target.v_sales_ch_amt_ncurrent = source.v_sales_ch_amt_ncurrent,
      target.sales_change_amount_ncurrent = source.sales_change_amount_ncurrent,
      target.new_retail_std = source.new_retail_std,
      target.sum_nmp_retail = source.sum_nmp_retail,
      target.avg_nmp_retail = source.avg_nmp_retail,
      target.new_clear_mkdn_nbr = source.new_clear_mkdn_nbr,
      target.new_clear_mkdn_nbr_flag = source.new_clear_mkdn_nbr_flag,
      target.new_retail_uom = source.new_retail_uom,
      target.new_retail_uom_flag = source.new_retail_uom_flag,
      target.new_out_of_stock_date = source.new_out_of_stock_date,
      target.new_out_of_stock_date_flag = source.new_out_of_stock_date_flag,
      target.new_reset_date = source.new_reset_date,
      target.new_reset_date_flag = source.new_reset_date_flag;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END APPLY_STRATEGY;

--------------------------------------------------------------------------------------
FUNCTION APPLY_CLEARANCE(O_error_msg           IN OUT VARCHAR2,
                         I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                         I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                         I_whatif_strategy_clr IN     OBJ_WHATIF_STRATEGY_CLR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_WORKSHEET_WHATIF_SQL.APPLY_CLEARANCE';

   L_price_guide_id     NUMBER(20) := I_whatif_strategy(1).price_guide_id;
   L_clr_markdown_basis NUMBER(1)  := I_whatif_strategy(1).clr_markdown_basis;
   L_markdown_count     NUMBER(2)  := I_whatif_strategy_clr.COUNT;

BEGIN

   merge into rpm_worksheet_il_workspace target
   using (select /*+ CARDINALITY (ids, 100) */
                 rzw.workspace_id,
                 rzw.worksheet_item_data_id,
                 rilw.worksheet_item_loc_data_id,
                 case
                    when L_clr_markdown_basis = LAST_CLEARANCE_PRICE then
                       case
                          when NVL(rilw.current_clearance_ind, 0) = 1 and
                             rzw.basis_zl_clear_retail is NOT NULL and
                             rzw.basis_zl_clear_retail_uom is NOT NULL then
                             rzw.basis_zl_clear_retail
                          else
                             rzw.basis_zl_regular_retail
                       end
                    else
                       case
                          when rzw.standard_uom != rzw.basis_zl_regular_retail_uom then
                             UOM_CONVERT_VALUE(rzw.item,
                                               rzw.basis_zl_regular_retail,
                                               rzw.basis_zl_regular_retail_uom,
                                               rzw.standard_uom)
                          else
                             rzw.basis_zl_regular_retail
                       end
                 end use_retail,
                 case
                    when L_clr_markdown_basis = LAST_CLEARANCE_PRICE then
                       NVL(rzw.basis_zl_clear_retail_uom, rzw.basis_zl_regular_retail_uom)
                    else
                       rzw.basis_zl_regular_retail_uom
                 end proposed_uom,
                 NVL(rilw.basis_clear_mkdn_nbr, 0) markdown_number
            from table(cast(I_whatif_rollup_id as OBJ_WHATIF_ROLLUP_ID_TBL)) ids,
                 rpm_worksheet_zone_workspace rzw,
                 rpm_worksheet_il_workspace rilw
           where rzw.workspace_id             = LP_workspace_id
             and rzw.item                     = NVL(ids.item, rzw.item)
             and rzw.zone_id                  = ids.zone_id
             and (   (    rzw.item_parent    is NULL
                      and ids.item_parent    is NULL)
                  or (    rzw.item_parent     = ids.item_parent))
             and (   (    rzw.link_code      is NULL
                      and ids.link_code      is NULL)
                  or (    rzw.link_code       = ids.link_code))
             and (   (ids.diff_id            is NULL)
                  or (    rzw.diff_type_1     = ids.diff_type
                      and rzw.diff_1          = ids.diff_id)
                  or (    rzw.diff_type_2     = ids.diff_type
                      and rzw.diff_2          = ids.diff_id)
                  or (    rzw.diff_type_3     = ids.diff_type
                      and rzw.diff_3          = ids.diff_id)
                  or (    rzw.diff_type_4     = ids.diff_type
                      and rzw.diff_4          = ids.diff_id))
             and rzw.basis_zl_base_cost      is NOT NULL
             and rilw.workspace_id            = rzw.workspace_id
             and rilw.worksheet_item_data_id  = rzw.worksheet_item_data_id) source
   on (    target.workspace_id               = source.workspace_id
       and target.worksheet_item_data_id     = source.worksheet_item_data_id
       and target.worksheet_item_loc_data_id = source.worksheet_item_loc_data_id)
   when MATCHED then
      update
         set proposed_retail         = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                          ---
                                          CALC_CLEAR_RETAIL(source.use_retail,
                                                            source.markdown_number + 1,
                                                            I_whatif_strategy_clr,
                                                            L_price_guide_id)
                                          else
                                             target.proposed_retail
                                       end,
             proposed_retail_std     = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             CALC_CLEAR_RETAIL(source.use_retail,
                                                               source.markdown_number + 1,
                                                               I_whatif_strategy_clr,
                                                               L_price_guide_id)
                                          else
                                             target.proposed_retail_std
                                       end,
             proposed_clear_mkdn_nbr = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             source.markdown_number + 1
                                          else
                                             target.proposed_clear_mkdn_nbr
                                       end,
             proposed_clear_ind      = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             1
                                          else
                                             target.proposed_clear_ind
                                       end,
             new_clear_ind           = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             1
                                          else
                                             target.new_clear_ind
                                       end,
             proposed_retail_uom     = source.proposed_uom,
             new_retail              = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             CALC_CLEAR_RETAIL(source.use_retail,
                                                               source.markdown_number + 1,
                                                               I_whatif_strategy_clr,
                                                               L_price_guide_id)
                                          else
                                             target.proposed_retail
                                       end,
             new_retail_std          = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             CALC_CLEAR_RETAIL(source.use_retail,
                                                               source.markdown_number + 1,
                                                               I_whatif_strategy_clr,
                                                               L_price_guide_id)
                                          else
                                             target.proposed_retail_std
                                       end,
             new_clear_mkdn_nbr      = case
                                          when source.use_retail is NOT NULL and
                                               source.markdown_number < L_markdown_count then
                                             ---
                                             source.markdown_number + 1
                                          else
                                             target.proposed_clear_mkdn_nbr
                                       end,
             new_retail_uom         = source.proposed_uom,
             new_out_of_stock_date  = target.proposed_out_of_stock_date,
             new_reset_date         = target.proposed_reset_date;

   if LP_error_flag = 1 then
      O_error_msg := LP_error_msg;
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END APPLY_CLEARANCE;

--------------------------------------------------------------------------------------
FUNCTION APPLY_MARGIN(O_error_msg           IN OUT VARCHAR2,
                      I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                      I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                      I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_WORKSHEET_WHATIF_SQL.APPLY_MARGIN';

   L_price_guide_id NUMBER(20) := I_whatif_strategy(1).price_guide_id;

BEGIN

   merge into rpm_worksheet_il_workspace target
   using (select /*+ CARDINALITY (ids, 100), CARDINALITY(dtl 100) */
                 rzw.workspace_id,
                 rzw.worksheet_item_data_id,
                 rilw.worksheet_item_loc_data_id,
                 rilw.item,
                 rilw.current_clearance_ind,
                 rilw.clear_boolean,
                 rilw.proposed_clear_ind,
                 rilw.basis_regular_retail_uom,
                 rzw.basis_zl_regular_retail,
                 rzw.basis_zl_base_cost,
                 rzw.markup_calc_type,
                 rzw.standard_uom,
                 dtl.percent,
                 dtl.from_percent,
                 dtl.to_percent,
                 CALC_MARGIN_RETAIL(rzw.basis_zl_base_cost,
                                    rzw.basis_zl_regular_retail,
                                    rzw.markup_calc_type,
                                    dtl.percent,
                                    dtl.from_percent,
                                    dtl.to_percent,
                                    L_price_guide_id,
                                    rzw.retail_include_vat_ind,
                                    rilw.vat_rate,
                                    rilw.vat_value) proposed_retail
            from table(cast(I_whatif_rollup_id as OBJ_WHATIF_ROLLUP_ID_TBL)) ids,
                 table(cast(I_whatif_strategy_dtl as OBJ_WHATIF_STRATEGY_DTL_TBL)) dtl,
                 rpm_worksheet_zone_workspace rzw,
                 rpm_worksheet_il_workspace rilw
           where rzw.workspace_id          = LP_workspace_id
             and rzw.item                  = NVL(ids.item, rzw.item)
             and rzw.zone_id               = ids.zone_id
             and (   (    rzw.item_parent is NULL
                      and ids.item_parent is NULL)
                  or (rzw.item_parent      = ids.item_parent))
             and (   (    rzw.link_code   is NULL
                      and ids.link_code   is NULL)
                  or (rzw.link_code        = ids.link_code))
             and (   (ids.diff_id         is NULL)
                  or (    rzw.diff_type_1  = ids.diff_type
                      and rzw.diff_1       = ids.diff_id)
                  or (    rzw.diff_type_2  = ids.diff_type
                      and rzw.diff_2       = ids.diff_id)
                  or (    rzw.diff_type_3  = ids.diff_type
                      and rzw.diff_3       = ids.diff_id)
                  or (    rzw.diff_type_4  = ids.diff_type
                      and rzw.diff_4       = ids.diff_id))
             and rzw.basis_zl_base_cost              is NOT NULL
             and rilw.workspace_id                    = rzw.workspace_id
             and rilw.worksheet_item_data_id          = rzw.worksheet_item_data_id
             and NVL(rilw.current_clearance_ind, 0)  != 1
             and NVL(rilw.clear_boolean, 0)          != 1
             and (   (    rzw.margin_mkt_basket_code is NULL
                      and dtl.mkt_basket_code        is NULL)
                  or (rzw.margin_mkt_basket_code      = dtl.mkt_basket_code))) source
   on (    target.workspace_id               = source.workspace_id
       and target.worksheet_item_data_id     = source.worksheet_item_data_id
       and target.worksheet_item_loc_data_id = source.worksheet_item_loc_data_id)
   when MATCHED then
   update
      set proposed_clear_ind  = case
                                   when source.current_clearance_ind = 1 or
                                        source.clear_boolean = 1 then
                                      1
                                   else
                                      0
                                end,
          new_clear_ind       = case
                                   when source.current_clearance_ind = 1 or
                                        source.clear_boolean = 1 then
                                      1
                                   else
                                      0
                                end,
          proposed_retail_uom = source.basis_regular_retail_uom,
          proposed_retail     = NVL(source.proposed_retail, target.proposed_retail),
          proposed_retail_std = case
                                   when source.proposed_retail is NULL then
                                      target.proposed_retail_std
                                   else
                                      UOM_CONVERT_VALUE(source.item,
                                                        source.proposed_retail,
                                                        source.basis_regular_retail_uom,
                                                        source.standard_uom)
                                end,
          new_retail          = NVL(source.proposed_retail, target.new_retail),
          new_retail_std      = case
                                   when source.proposed_retail is NULL then
                                      target.new_retail_std
                                   else
                                      UOM_CONVERT_VALUE(source.item,
                                                        source.proposed_retail,
                                                        source.basis_regular_retail_uom,
                                                        source.standard_uom)
                                end,
          new_retail_uom      = source.basis_regular_retail_uom;

   if LP_error_flag = 1 then
      O_error_msg := LP_error_msg;
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END APPLY_MARGIN;

--------------------------------------------------------------------------------------
FUNCTION APPLY_MAINT_MARGIN(O_error_msg            IN OUT VARCHAR2,
                            I_whatif_rollup_id     IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                            I_whatif_strategy      IN     OBJ_WHATIF_STRATEGY_TBL,
                            I_whatif_strategy_dtl  IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(100) := 'RPM_WORKSHEET_WHATIF_SQL.APPLY_MAINT_MARGIN';

BEGIN

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END APPLY_MAINT_MARGIN;

--------------------------------------------------------------------------------------
FUNCTION APPLY_COMPETITIVE(O_error_msg           IN OUT VARCHAR2,
                           I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                           I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                           I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_WORKSHEET_WHATIF_SQL.APPLY_COMPETITIVE';

BEGIN

   merge into rpm_worksheet_il_workspace target
   using (select /*+ CARDINALITY (ids, 100) */
                 rzw.workspace_id,
                 rzw.worksheet_item_data_id,
                 rilw.worksheet_item_loc_data_id,
                 rilw.item,
                 rilw.current_clearance_ind,
                 rilw.clear_boolean,
                 rilw.proposed_clear_ind,
                 rilw.basis_regular_retail_uom,
                 rzw.basis_zl_regular_retail,
                 rzw.basis_zl_base_cost,
                 rzw.markup_calc_type,
                 rzw.standard_uom,
                 CALC_COMPETITIVE_RETAIL(I_whatif_strategy_dtl,
                                         I_whatif_strategy(1).compete_type,
                                         rilw.item,
                                         rzw.basis_zl_regular_retail,
                                         rilw.basis_regular_retail,
                                         rilw.basis_regular_retail_uom,
                                         rzw.primary_comp_retail,
                                         rzw.primary_comp_retail_uom,
                                         rzw.comp_mkt_basket_code,
                                         I_whatif_strategy(1).price_guide_id) proposed_retail
            from table(cast(I_whatif_rollup_id as OBJ_WHATIF_ROLLUP_ID_TBL)) ids,
                 rpm_worksheet_zone_workspace rzw,
                 rpm_worksheet_il_workspace rilw
           where rzw.workspace_id                    = LP_workspace_id
             and rzw.item                            = NVL(ids.item, rzw.item)
             and rzw.zone_id                         = ids.zone_id
             and (   (    rzw.item_parent is NULL
                      and ids.item_parent is NULL)
                  or rzw.item_parent     = ids.item_parent)
             and (   (    rzw.link_code   is NULL
                      and ids.link_code   is NULL)
                  or rzw.link_code       = ids.link_code)
             and (   (    ids.diff_id     is NULL)
                  or (    rzw.diff_type_1 = ids.diff_type
                      and rzw.diff_1      = ids.diff_id)
                  or (    rzw.diff_type_2 = ids.diff_type
                      and rzw.diff_2      = ids.diff_id)
                  or (    rzw.diff_type_3 = ids.diff_type
                      and rzw.diff_3      = ids.diff_id)
                  or (    rzw.diff_type_4 = ids.diff_type
                      and rzw.diff_4      = ids.diff_id))
             and rzw.basis_zl_base_cost              is NOT NULL
             and rilw.workspace_id                   = rzw.workspace_id
             and rilw.worksheet_item_data_id         = rzw.worksheet_item_data_id
             and NVL(rilw.current_clearance_ind, 0) != 1
             and NVL(rilw.clear_boolean, 0)         != 1) source
   on (    target.workspace_id               = source.workspace_id
       and target.worksheet_item_data_id     = source.worksheet_item_data_id
       and target.worksheet_item_loc_data_id = source.worksheet_item_loc_data_id
       and source.proposed_retail            is NOT NULL)
   when MATCHED then
      update
         set target.proposed_clear_ind  = case
                                             when source.current_clearance_ind = 1 or
                                                  source.clear_boolean = 1 then
                                                1
                                             else
                                                0
                                          end,
             target.new_clear_ind       = case
                                             when source.current_clearance_ind = 1 or
                                                  source.clear_boolean = 1 then
                                                1
                                             else
                                                0
                                          end,
             target.proposed_retail_uom = source.basis_regular_retail_uom,
             target.proposed_retail     = NVL(source.proposed_retail, target.proposed_retail),
             target.proposed_retail_std = case
                                             when source.proposed_retail is NULL then
                                                target.proposed_retail_std
                                             else
                                                UOM_CONVERT_VALUE(source.item,
                                                                  source.proposed_retail,
                                                                  source.basis_regular_retail_uom,
                                                                  source.standard_uom)
                                          end,
             target.new_retail          = NVL(source.proposed_retail, target.new_retail),
             target.new_retail_std      = case
                                             when source.proposed_retail is NULL then
                                                target.new_retail_std
                                             else
                                                UOM_CONVERT_VALUE(source.item,
                                                                  source.proposed_retail,
                                                                  source.basis_regular_retail_uom,
                                                                  source.standard_uom)
                                          end,
             target.new_retail_uom      = source.basis_regular_retail_uom;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END APPLY_COMPETITIVE;

--------------------------------------------------------------------------------------
FUNCTION APPLY_PRICE_GUIDE(I_selling_retail    IN     NUMBER,
                           I_price_guide_id    IN     NUMBER)
RETURN NUMBER IS

   cursor C_GUIDE is
      select NEW_PRICE_VAL
        from rpm_price_guide_interval
       where price_guide_id = I_price_guide_id
         and ROUND(I_selling_retail,2) between from_price_val and to_price_val;

   L_new_retail                NUMBER;

BEGIN

   if I_price_guide_id is NULL then
      L_new_retail := I_selling_retail;
   else
      open C_GUIDE;

      fetch C_GUIDE
       into L_new_retail;

      if C_GUIDE%NOTFOUND then
         L_new_retail := I_selling_retail;
      end if;

      close C_GUIDE;
   end if;

   return L_new_retail;

END APPLY_PRICE_GUIDE;

--------------------------------------------------------------------------------------
FUNCTION UOM_CONVERT_VALUE (I_item                   IN     item_master.item%TYPE,
                            I_input_value            IN     item_loc.unit_retail%TYPE,
                            I_from_uom               IN     item_master.standard_uom%TYPE,
                            I_to_uom                 IN     item_master.standard_uom%TYPE)
RETURN NUMBER IS

   L_error_msg                   VARCHAR2(250);
   L_output_value                NUMBER(20,4);

BEGIN

   if RPM_WRAPPER.UOM_CONVERT_VALUE(L_error_msg,
                                    L_output_value,
                                    I_item,
                                    I_input_value,
                                    I_from_uom,
                                    I_to_uom) = 0 then
      return I_input_value;
   end if;

   return L_output_value;

END UOM_CONVERT_VALUE;
--------------------------------------------------------------------------------------
FUNCTION CALC_CLEAR_RETAIL(I_basis_retail        IN     NUMBER,
                           I_markdown_number     IN     NUMBER,
                           I_whatif_strategy_clr IN     OBJ_WHATIF_STRATEGY_CLR_TBL,
                           I_price_guide_id      IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_WORKSHEET_WHATIF_SQL.CALC_CLEAR_RETAIL';

   L_proposed_retail NUMBER(20,4) := NULL;

BEGIN

   -- The check below is to guard against invalid input values.
   -- The function will not fail and doesn't indicate an error to the calling function.
   -- It will simply return zero to avoid any errors that is caused by an empty collection.

   if I_whatif_strategy_clr is NULL or
      I_whatif_strategy_clr.COUNT = 0 then

      LP_error_msg := 'INVALID INPUT - I_whatif_strategy_clr collection is empty';
      LP_error_flag := 1;

      return 0;

   elsif I_markdown_number NOT BETWEEN I_whatif_strategy_clr.FIRST and I_whatif_strategy_clr.LAST - 1 then

      -- The check below is to guard against subscription beyond count error.
      -- The function will not fail and doesn't indicate an error to the calling function.
      -- It will simply return zero to avoid subscription beyond count error when the index is bigger than it's collection.

      LP_error_msg := 'INVALID MARKDOWN NUMBER';
      LP_error_flag := 1;

      return 0;

   end if;

   L_proposed_retail := I_basis_retail * (1 - I_whatif_strategy_clr(I_markdown_number + 1).markdown_percent/100);

   return APPLY_PRICE_GUIDE(L_proposed_retail,
                            I_price_guide_id);

EXCEPTION

   when OTHERS then
      LP_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CALC_CLEAR_RETAIL;

-------------------------------------------------------------------------------
FUNCTION CALC_MARGIN_RETAIL(I_basis_zl_base_cost      IN     NUMBER,
                            I_basis_zl_regular_retail IN     NUMBER,
                            I_markup_calc_type        IN     NUMBER,
                            I_percent                 IN     NUMBER,
                            I_from_percent            IN     NUMBER,
                            I_to_percent              IN     NUMBER,
                            I_price_guide_id          IN     NUMBER,
                            I_retail_include_vat_ind  IN     VARCHAR2,
                            I_vat_rate                IN     NUMBER,
                            I_vat_value               IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_WORKSHEET_WHATIF_SQL.CALC_MARGIN_RETAIL';

   L_proposed_retail NUMBER(20,4) := NULL;
   L_basis_percent   NUMBER(20,4) := NULL;

BEGIN

   -- The check below is to guard against divisor by zero error.
   -- The function will not fail and also doesn't indicate an error to the calling function.
   -- It will simply return zero to avoid divisor by zero error.

   if I_basis_zl_regular_retail = 0 or
      I_basis_zl_base_cost = 0 then

      LP_error_flag := 1;
      LP_error_msg := 'INVALID INPUT - The base_cost and regular_retail should not be zero';

      return 0;

   end if;

   if I_markup_calc_type = 1 then
      L_basis_percent := (1 - I_basis_zl_base_cost/I_basis_zl_regular_retail) * 100;
   else
      L_basis_percent := ((I_basis_zl_regular_retail - I_basis_zl_base_cost)/I_basis_zl_base_cost) * 100;
   end if;

   if I_from_percent is NOT NULL and
      I_to_percent is NOT NULL and
      L_basis_percent >= I_from_percent and
      L_basis_percent <= I_to_percent then
      ---
      L_proposed_retail := NULL;

   else

      if I_markup_calc_type = 1 then
         L_proposed_retail := I_basis_zl_base_cost / (1 - I_percent/100);

      else
         L_proposed_retail := I_basis_zl_base_cost * (1 + I_percent/100);

      end if;

      if NVL(I_retail_include_vat_ind, 'N') = 'Y' then

         if LP_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then
            L_proposed_retail := (L_proposed_retail + NVL(I_vat_value, 0)) * (1 + NVL(I_vat_rate, 0)/100);

         elsif LP_default_tax_type = RPM_CONSTANTS.SVAT_TAX_TYPE then
            L_proposed_retail := (L_proposed_retail) * (1 + NVL(I_vat_rate, 0)/100);

         end if;

      end if;

   end if;

   if L_proposed_retail is NOT NULL then

      return APPLY_PRICE_GUIDE(L_proposed_retail,
                               I_price_guide_id);
   end if;

   return L_proposed_retail;

EXCEPTION

   when OTHERS then
      LP_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CALC_MARGIN_RETAIL;

-------------------------------------------------------------------------------
FUNCTION CALC_COMPETITIVE_RETAIL(I_whatif_strategy_dtl      IN     OBJ_WHATIF_STRATEGY_DTL_TBL,
                                 I_compete_type             IN     NUMBER,
                                 I_item                     IN     VARCHAR2,
                                 I_basis_zl_regular_retail  IN     NUMBER,
                                 I_basis_regular_retail     IN     NUMBER,
                                 I_basis_regular_retail_uom IN     VARCHAR2,
                                 I_primary_comp_retail      IN     NUMBER,
                                 I_primary_comp_retail_uom  IN     VARCHAR2,
                                 I_mkt_basket_code          IN     VARCHAR2,
                                 I_price_guide_id           IN     NUMBER)
RETURN NUMBER IS

   L_competitor_retail    NUMBER(20,4) := NULL;
   L_proposed_retail      NUMBER(20,4) := NULL;
   L_basis_retail         NUMBER(20,4) := NULL;
   L_from_percent         NUMBER(20,4) := NULL;
   L_to_percent           NUMBER(20,4) := NULL;
   L_percent              NUMBER(20,4) := NULL;
   L_from_retail          NUMBER(20,4) := NULL;
   L_to_retail            NUMBER(20,4) := NULL;
   L_mkt_basket_code      VARCHAR2(24) := NULL;
   L_by_code_compete_type NUMBER(1)    := NULL;

BEGIN

   L_basis_retail := NVL(I_basis_zl_regular_retail, I_basis_regular_retail);

   if I_primary_comp_retail is NOT NULL then
      L_competitor_retail := UOM_CONVERT_VALUE (I_item,
                                                I_primary_comp_retail,
                                                I_primary_comp_retail_uom,
                                                I_basis_regular_retail_uom);
   else
      L_competitor_retail := NULL;
   end if;

   if I_compete_type IN (1,
                         2) and
      I_whatif_strategy_dtl is NOT NULL and
      I_whatif_strategy_dtl.COUNT > 0 then

      L_from_percent := I_whatif_strategy_dtl(1).from_percent;
      L_to_percent := I_whatif_strategy_dtl(1).to_percent;
      L_percent := I_whatif_strategy_dtl(1).percent;

   end if;

   if I_compete_type = 0 then -- PriceStrategyCompeteTypeCode.MATCH

      if L_competitor_retail is NULL then
         L_proposed_retail := NULL;

      elsif L_basis_retail = L_competitor_retail then
         L_proposed_retail := NULL;

      else
         L_proposed_retail := L_competitor_retail;

      end if;

   elsif I_compete_type = 2 then -- PriceStrategyCompeteTypeCode.PRICE_BELOW

      L_from_retail := L_competitor_retail * (1 - L_to_percent / 100);
      L_to_retail := L_competitor_retail * (1 - L_from_percent / 100);

      if L_competitor_retail is NULL then
         L_proposed_retail := NULL;

      elsif L_to_percent is NULL or
         L_basis_retail > L_to_retail or
         L_from_percent is NULL or
         L_basis_retail < L_from_retail then
         ---
         L_proposed_retail := L_competitor_retail * (1 - L_percent / 100);

      else
         L_proposed_retail := NULL;

      end if;

   elsif I_compete_type = 1 then -- PriceStrategyCompeteTypeCode.PRICE_ABOVE

      L_from_retail := L_competitor_retail * (1 + L_from_percent / 100);
      L_to_retail := L_competitor_retail * (1 + L_to_percent / 100);

      if L_competitor_retail is NULL then
         L_proposed_retail := NULL;

      elsif L_to_percent is NULL or
         L_basis_retail > L_to_retail or
         L_from_percent is NULL or
         L_basis_retail < L_from_retail then
         ---
         L_proposed_retail := L_competitor_retail * (1 + L_percent / 100);

      else
         L_proposed_retail := NULL;

      end if;

   elsif I_compete_type = 3 then -- PriceStrategyCompeteTypeCode.PRICE_BY_CODE

      L_proposed_retail := NULL;

      if I_whatif_strategy_dtl is NOT NULL and
         I_whatif_strategy_dtl.COUNT > 0 then

         for i IN 1..I_whatif_strategy_dtl.COUNT loop

            L_from_percent := I_whatif_strategy_dtl(1).from_percent;
            L_to_percent := I_whatif_strategy_dtl(1).to_percent;
            L_percent := I_whatif_strategy_dtl(1).percent;
            L_mkt_basket_code := I_whatif_strategy_dtl(1).mkt_basket_code;
            L_by_code_compete_type := I_whatif_strategy_dtl(1).compete_type;

            if I_mkt_basket_code = L_mkt_basket_code then

               if L_by_code_compete_type = 0 then -- PriceStrategyCompeteTypeCode.MATCH

                  if L_competitor_retail is NULL then
                     L_proposed_retail := NULL;

                  elsif L_basis_retail = L_competitor_retail then
                     L_proposed_retail := NULL;

                  else
                     L_proposed_retail := L_competitor_retail;

                  end if;

               elsif L_by_code_compete_type = 2 then -- PriceStrategyCompeteTypeCode.PRICE_BELOW

                  L_from_retail := L_competitor_retail * (1 - L_to_percent / 100);
                  L_to_retail := L_competitor_retail * (1 - L_from_percent / 100);

                  if L_competitor_retail is NULL then
                     L_proposed_retail := NULL;

                  elsif L_to_percent is NULL or
                     L_basis_retail > L_to_retail or
                     L_from_percent is NULL or
                     L_basis_retail < L_from_retail then
                     ---
                     L_proposed_retail := L_competitor_retail * (1 - L_percent / 100);

                  else
                     L_proposed_retail := NULL;

                  end if;

               elsif L_by_code_compete_type = 1 then -- PriceStrategyCompeteTypeCode.PRICE_ABOVE

                  L_from_retail := L_competitor_retail * (1 + L_from_percent / 100);
                  L_to_retail := L_competitor_retail * (1 + L_to_percent / 100);

                  if L_competitor_retail is NULL then
                     L_proposed_retail := NULL;

                  elsif L_to_percent is NULL or
                     L_basis_retail > L_to_retail or
                     L_from_percent is NULL or
                     L_basis_retail < L_from_retail then
                     ---
                     L_proposed_retail := L_competitor_retail * (1 + L_percent / 100);

                  else
                     L_proposed_retail := NULL;

                  end if;
               end if;
            end if;
         end loop;
      end if;

   end if;

   if L_proposed_retail is NOT NULL then

      return APPLY_PRICE_GUIDE(L_proposed_retail,
                               I_price_guide_id);
   end if;

   return L_proposed_retail;

END CALC_COMPETITIVE_RETAIL;

--------------------------------------------------------------------------------------
END RPM_WORKSHEET_WHATIF_SQL;
/
