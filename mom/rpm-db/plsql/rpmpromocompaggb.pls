CREATE OR REPLACE PACKAGE BODY RPM_PROM_COMP_AGG_SQL AS

--------------------------------------------------------------------------------
--                            PRIVATE GLOBALS                                 --
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------

FUNCTION GET_PCPD_IND(O_error_message         OUT VARCHAR2,
                      I_pc_processing_days IN     NUMBER)
RETURN BOOLEAN;

FUNCTION GET_TYPES(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION GET_FULL_ACCESS_INFO(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION GET_CONFLICT_IND(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION GET_TIMEBASED_IND(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION GET_VALUES(O_error_message         OUT VARCHAR2,
                    I_pc_processing_days IN     NUMBER,
                    I_user_id            IN     VARCHAR2,
                    I_pca_tbl            IN OUT OBJ_RPM_PROM_COMP_AGG_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_PROM_COMP_AGG_SQL.GET_VALUES';

   L_pcpd_pca_tbl        OBJ_RPM_PROM_COMP_AGG_TBL;
   L_types_pca_tbl       OBJ_RPM_PROM_COMP_AGG_TBL;
   L_full_access_pca_tbl OBJ_RPM_PROM_COMP_AGG_TBL;
   L_conflict_pca_tbl    OBJ_RPM_PROM_COMP_AGG_TBL;

   L_prom_comp_agg_SEQ NUMBER;

   L_truncate_dept_tbl    VARCHAR2(45) := 'truncate table rpm_dept_class_subclass_tbl';
   L_truncate_zone_tbl    VARCHAR2(45) := 'truncate table rpm_zone_locs_tbl';
   L_truncate_agg_tbl     VARCHAR2(45) := 'truncate table rpm_prom_comp_agg_tbl';
   L_truncate_agg_dtl_tbl VARCHAR2(45) := 'truncate table rpm_prom_comp_agg_dtl_tbl';

   cursor C_ROLE is
   select role_id
     from rsm_user_role
    where user_id = I_user_id;

   cursor C_FINAL is
      select OBJ_RPM_PROM_COMP_AGG_REC(promo_comp_id,
                                       type,
                                       pcpd_ind,
                                       full_access_ind,
                                       states,
                                       conflict_ind,
                                       timebased_ind)
        from (select rpca1.promo_comp_id,
                     rpca3.type,
                     NVL(rpca2.pcpd_ind, 0) pcpd_ind,
                     NVL(rpca4.full_access_ind, 1) full_access_ind,
                     (CAST(MULTISET(select distinct rpd.state
                                      from rpm_promo_dtl rpd
                                     where rpd.promo_comp_id = rpca1.promo_comp_id) as OBJ_VARCHAR_DESC_TABLE)) states,
                     NVL(rpca5.conflict_ind, 0) conflict_ind,
                     NVL(rpca6.conflict_ind, 0) timebased_ind
                from (select promo_comp_id
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 1) rpca1,
                     (select promo_comp_id,
                             pcpd_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 2) rpca2,
                     (select promo_comp_id,
                             type
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 3) rpca3,
                     (select promo_comp_id,
                             full_access_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 4) rpca4,
                     (select promo_comp_id,
                             conflict_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 5) rpca5,
                     (select promo_comp_id,
                             conflict_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 6) rpca6,
                     rpm_promo_comp pc
               where rpca1.promo_comp_id = rpca5.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca4.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca3.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca2.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca6.promo_comp_id (+)
                 and rpca1.promo_comp_id = pc.promo_comp_id
               union all
              select rpca1.promo_comp_id,
                     rpca3.type,
                     NVL(rpca2.pcpd_ind, 0) pcpd_ind,
                     NVL(rpca4.full_access_ind, 1) full_access_ind,
                     (CAST(MULTISET(select distinct rpd.state
                                      from rpm_promo_dtl_hist rpd
                                     where rpd.promo_comp_id = rpca1.promo_comp_id) as OBJ_VARCHAR_DESC_TABLE)) states,
                     NVL(rpca5.conflict_ind, 0) conflict_ind,
                     NVL(rpca6.conflict_ind, 0) timebased_ind
                from (select promo_comp_id
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 1) rpca1,
                     (select promo_comp_id,
                             pcpd_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 2) rpca2,
                     (select promo_comp_id,
                             type
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 3) rpca3,
                     (select promo_comp_id,
                             full_access_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 4) rpca4,
                     (select promo_comp_id,
                             conflict_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 5) rpca5,
                     (select promo_comp_id,
                             conflict_ind
                        from rpm_prom_comp_agg_tbl
                       where agg_type = 6) rpca6,
                     rpm_promo_comp_hist pch
               where rpca1.promo_comp_id = rpca5.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca4.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca3.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca2.promo_comp_id (+)
                 and rpca1.promo_comp_id = rpca6.promo_comp_id (+)
                 and rpca1.promo_comp_id = pch.promo_comp_id);

BEGIN

   EXECUTE IMMEDIATE L_truncate_dept_tbl;
   EXECUTE IMMEDIATE L_truncate_zone_tbl;
   EXECUTE IMMEDIATE L_truncate_agg_tbl;
   EXECUTE IMMEDIATE L_truncate_agg_dtl_tbl;

   for rec IN C_ROLE loop

      insert into rpm_dept_class_subclass_tbl
         (dept,
          class,
          subclass)
      select sc.dept,
             sc.class,
             sc.subclass
        from rsm_role_hierarchy_perm rhp,
             rsm_hierarchy_type ht,
            (select id,
                    id parent_id,
                    key_value dept,
                    NULL class,
                    NULL subclass
               from rsm_hierarchy_permission hp
              where reference_class = RPM_CONSTANTS.RSM_REF_CLASS_DEPT
                and child_id        is NULL
              union
             select hp1.id,
                    hp2.id parent_id,
                    SUBSTR(hp1.key_value, 1, INSTR(hp1.key_value, ';') - 1) dept,
                    SUBSTR(hp1.key_value, INSTR(hp1.key_value, ';') + 1) class,
                    NULL subclass
               from rsm_hierarchy_permission hp1,
                    rsm_hierarchy_permission hp2
              where hp1.reference_class = RPM_CONSTANTS.RSM_REF_CLASS_CLASS
                and hp1.child_id        is NULL
                and hp2.child_id        = hp1.id
              union
             select hp1.id,
                    hp3.id parent_id,
                    SUBSTR(hp1.key_value, 1, INSTR(hp1.key_value, ';') - 1) dept,
                    SUBSTR(hp1.key_value, INSTR(hp1.key_value, ';') + 1, INSTR(hp1.key_value, ';',-1) - INSTR(hp1.key_value, ';') - 1) class,
                    SUBSTR(hp1.key_value, INSTR(hp1.key_value, ';', -1) + 1) subclass
               from rsm_hierarchy_permission hp1,
                    rsm_hierarchy_permission hp2,
                    rsm_hierarchy_permission hp3
              where hp1.reference_class = RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS
                and hp1.child_id        is NULL
                and hp2.child_id        = hp1.id
                and hp3.child_id        = hp2.id) merch,
             subclass sc
       where rhp.role_id     = rec.role_id
         and ht.id           = rhp.hierarchy_type_id
         and ht.external_id  = RPM_CONSTANTS.RSM_MERCH_HIER
         and merch.parent_id = rhp.parent_id
         and sc.dept         = merch.dept
         and sc.class        = NVL(merch.class, sc.class)
         and sc.subclass     = NVL(merch.subclass, sc.subclass);

      insert into rpm_zone_locs_tbl
         (zone_group_id,
          zone_id,
          location_id)
      select org.zone_group_id,
             org.zone_id,
             org.location
        from rsm_role_hierarchy_perm rhp,
             rsm_hierarchy_type ht,
            (select id,
                    id parent_id,
                    TO_NUMBER(key_value) zone_group_id,
                    rz.zone_id,
                    NULL location
               from rsm_hierarchy_permission hp,
                    rpm_zone rz
              where reference_class  = 'com.retek.rpm.domain.zonestructure.bo.ZoneGroupImpl'
                and child_id         is NULL
                and rz.zone_group_id = TO_NUMBER(key_value)
              union
             select hp1.id,
                    hp2.id parent_id,
                    TO_NUMBER(hp2.key_value) zone_group_id,
                    TO_NUMBER(hp1.key_value) zone_id,
                    NULL location
               from rsm_hierarchy_permission hp1,
                    rsm_hierarchy_permission hp2
              where hp1.reference_class = 'com.retek.rpm.domain.zonestructure.bo.ZoneImpl'
                and hp1.child_id        is NULL
                and hp2.child_id        = hp1.id
              union
             select hp1.id,
                    hp3.id parent_id,
                    TO_NUMBER(hp3.key_value) zone_group_id,
                    TO_NUMBER(hp3.key_value) zone_id,
                    TO_NUMBER(hp1.key_value) location
               from rsm_hierarchy_permission hp1,
                    rsm_hierarchy_permission hp2,
                    rsm_hierarchy_permission hp3
              where hp1.reference_class = 'com.retek.rpm.domain.location.bo.LocationImpl'
                and hp1.child_id        is NULL
                and hp2.child_id        = hp1.id
                and hp3.child_id        = hp2.id) org
       where rhp.role_id    = rec.role_id
         and ht.id          = rhp.hierarchy_type_id
         and ht.external_id = 'locationHierarchy'
         and org.parent_id  = rhp.parent_id;

   end loop;

   if I_pca_tbl is NOT NULL and
      I_pca_tbl.COUNT > 0 then

      for i IN 1..I_pca_tbl.COUNT loop

         select rpm_prom_comp_agg_SEQ.NEXTVAL
           into L_prom_comp_agg_SEQ
           from dual;

         insert into rpm_prom_comp_agg_tbl
            (rpm_prom_com_agg_id,
             promo_comp_id,
             type,
             pcpd_ind,
             full_access_ind,
             conflict_ind,
             agg_type)
         values (L_prom_comp_agg_SEQ,
                 I_pca_tbl(i).promo_comp_id,
                 I_pca_tbl(i).type,
                 I_pca_tbl(i).pcpd_ind,
                 I_pca_tbl(i).full_access_ind,
                 I_pca_tbl(i).conflict_ind,
                 1);

          if I_pca_tbl(i).detail_states is NOT NULL and
             I_pca_tbl(i).detail_states.COUNT > 0 then

             for j IN 1..I_pca_tbl(i).detail_states.COUNT loop

                insert into rpm_prom_comp_agg_dtl_tbl
                   (rpm_prom_com_agg_id,
                    detail_state)
                values(L_prom_comp_agg_SEQ,
                       I_pca_tbl(i).detail_states(j));
             end loop;
          end if;

      end loop;

   end if;

   if GET_PCPD_IND(O_error_message,
                   I_pc_processing_days) = FALSE then
      return 0;
   end if;

   if GET_TYPES(O_error_message) = FALSE then
      return 0;
   end if;

   if GET_FULL_ACCESS_INFO(O_error_message) = FALSE then
      return 0;
   end if;

   if GET_CONFLICT_IND(O_error_message) = FALSE then
      return 0;
   end if;

   if GET_TIMEBASED_IND(O_error_message) = FALSE then
      return 0;
   end if;

   open C_FINAL;
   fetch C_FINAL bulk collect into I_pca_tbl;
   close C_FINAL;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END GET_VALUES;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--                         PRIVATE PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION GET_PCPD_IND(O_error_message         OUT VARCHAR2,
                      I_pc_processing_days IN     NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'RPM_PROM_COMP_AGG_SQL.GET_PCPD_IND';
   L_vdate     DATE          := GET_VDATE;

BEGIN

   insert into rpm_prom_comp_agg_tbl
   select rpm_prom_comp_agg_seq.nextval,
          rpca.promo_comp_id,
          NULL,
          1,
          NULL,
          NULL,
          2
     from rpm_prom_comp_agg_tbl rpca
    where rpca.agg_type = 1
      and EXISTS (select 'x'
                    from rpm_promo_dtl rpd
                   where rpca.promo_comp_id    = rpd.promo_comp_id
                     and trunc(rpd.start_date) < L_vdate + I_pc_processing_days
                     and rownum                = 1);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PCPD_IND;
--------------------------------------------------------------------------------

FUNCTION GET_TYPES(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'RPM_PROM_COMP_AGG_SQL.GET_TYPES';

BEGIN

   insert into rpm_prom_comp_agg_tbl
      select RPM_PROM_COMP_AGG_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.type,
             t.pcpd_ind,
             t.full_access_ind,
             t.conflict_ind,
             3
        from (select rpca.promo_comp_id,
                     rpc.type,
                     NULL pcpd_ind,
                     NULL full_access_ind,
                     NULL conflict_ind
                from rpm_prom_comp_agg_tbl rpca,
                     rpm_promo_comp rpc
               where rpca.agg_type     = 1
                 and rpc.promo_comp_id = rpca.promo_comp_id
              union all
              select rpca.promo_comp_id,
                     rpc.type,
                     NULL pcpd_ind,
                     NULL full_access_ind,
                     NULL conflict_ind
                from rpm_prom_comp_agg_tbl rpca,
                     rpm_promo_comp_hist rpc
               where rpca.agg_type     = 1
                 and rpc.promo_comp_id = rpca.promo_comp_id) t;
   --
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_TYPES;
--------------------------------------------------------------------------------

FUNCTION GET_FULL_ACCESS_INFO(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_PROM_COMP_AGG_SQL.GET_FULL_ACCESS_INFO';

BEGIN

   insert into rpm_prom_comp_agg_tbl
      (rpm_prom_com_agg_id,
       promo_comp_id,
       full_access_ind,
       agg_type)
   select RPM_PROM_COMP_AGG_SEQ.NEXTVAL,
          t.promo_comp_id,
          0 full_access_ind,
          4
     from (select rpca.promo_comp_id
             from rpm_prom_comp_agg_tbl rpca,
                  rpm_promo_comp rpc
            where rpca.agg_type     = 3
              and rpc.promo_comp_id = rpca.promo_comp_id
              and NOT EXISTS (select 'x'
                                from rpm_zone_locs_tbl oh,
                                     rpm_promo_dtl rpd,
                                     rpm_promo_zone_location rpzl
                               where EXISTS (select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node rpdm,
                                                    item_master im
                                              where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                                and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                                and im.item           = rpdm.item
                                                and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and mh.subclass       = im.subclass
                                                and mh.class          = im.class
                                                and mh.dept           = im.dept
                                                and rownum            = 1
                                             union all
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node rpdm
                                              where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                                and rpdm.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                                                and mh.dept           = rpdm.dept
                                                and mh.class          = NVL(rpdm.class, mh.class)
                                                and mh.subclass       = NVL(rpdm.subclass, mh.subclass)
                                                and rownum            = 1
                                             union all
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node rpdm,
                                                    rpm_promo_dtl_skulist pds,
                                                    item_master im
                                              where rpdm.promo_dtl_id       = rpd.promo_dtl_id
                                                and rpdm.merch_type         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                                and pds.price_event_id      = rpdm.promo_dtl_id
                                                and pds.skulist             = rpdm.skulist
                                                and (   (    pds.item_level = RPM_CONSTANTS.IL_ITEM_LEVEL
                                                         and pds.item       = im.item)
                                                     or (    pds.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                                         and pds.item       = im.item_parent))
                                                and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and mh.subclass             = im.subclass
                                                and mh.class                = im.class
                                                and mh.dept                 = im.dept
                                                and rownum                  = 1
                                             union all
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node rpdm,
                                                    rpm_merch_list_detail rmld,
                                                    item_master im
                                              where rpdm.promo_dtl_id       = rpd.promo_dtl_id
                                                and rpdm.merch_type         = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                                and rmld.merch_list_id      = rpdm.price_event_itemlist
                                                and rmld.item               = im.item
                                                and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and mh.subclass             = im.subclass
                                                and mh.class                = im.class
                                                and mh.dept                 = im.dept
                                                and rownum                  = 1
                                             union all  -- storewide - location level
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node rpdm,
                                                    rpm_promo_zone_location rpzl,
                                                    rpm_item_loc ril,
                                                    item_master im
                                              where rpdm.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpdm.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                                and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                                and ril.loc             = rpzl.location
                                                and ril.item            = im.item
                                                and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                and mh.subclass         = im.subclass
                                                and mh.class            = im.class
                                                and mh.dept             = im.dept
                                                and rownum              = 1
                                             union all  -- storewide - zone level
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node rpdm,
                                                    rpm_promo_zone_location rpzl,
                                                    rpm_zone_location rzl,
                                                    rpm_item_loc ril,
                                                    item_master im
                                              where rpdm.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpdm.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                                and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                and rpzl.zone_id        = rzl.zone_id
                                                and ril.loc             = rzl.location
                                                and ril.item            = im.item
                                                and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                and mh.subclass         = im.subclass
                                                and mh.class            = im.class
                                                and mh.dept             = im.dept
                                                and rownum              = 1)
                                 and rpd.promo_comp_id  = rpca.promo_comp_id
                                 and oh.zone_id         = NVL(rpzl.zone_id, oh.zone_id)
                                 and (   oh.location_id is NULL
                                      or oh.location_id = NVL(rpzl.location, oh.location_id))
                                 and rpzl.promo_dtl_id  = rpd.promo_dtl_id
                                 and rownum             = 1)
           union all
           select rpca.promo_comp_id
             from rpm_prom_comp_agg_tbl rpca,
                  rpm_promo_comp_hist rpch
            where rpca.agg_type      = 3
              and rpca.promo_comp_id = rpch.promo_comp_id
              and NOT EXISTS (select 'x'
                                from rpm_zone_locs_tbl oh,
                                     rpm_promo_dtl_hist rpd,
                                     rpm_promo_zone_location_hist rpzl
                               where EXISTS (select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node_hist rpdm,
                                                    item_master im
                                              where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                                and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                                and im.item           = rpdm.item
                                                and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and mh.subclass       = im.subclass
                                                and mh.class          = im.class
                                                and mh.dept           = im.dept
                                                and rownum            = 1
                                             union all
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node_hist rpdm
                                              where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                                and rpdm.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                                                and mh.dept           = rpdm.dept
                                                and mh.class          = NVL(rpdm.class, mh.class)
                                                and mh.subclass       = NVL(rpdm.subclass, mh.subclass)
                                                and rownum            = 1
                                             union all
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node_hist rpdm,
                                                    rpm_promo_dtl_skulist_hist pds,
                                                    item_master im
                                              where rpdm.promo_dtl_id       = rpd.promo_dtl_id
                                                and rpdm.merch_type         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                                and pds.price_event_id      = rpdm.promo_dtl_id
                                                and pds.skulist             = rpdm.skulist
                                                and (   (    pds.item_level = RPM_CONSTANTS.IL_ITEM_LEVEL
                                                         and pds.item       = im.item)
                                                     or (    pds.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                                         and pds.item       = im.item_parent))
                                                and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and mh.subclass             = im.subclass
                                                and mh.class                = im.class
                                                and mh.dept                 = im.dept
                                                and rownum                  = 1
                                             union all
                                             select 'x'
                                               from rpm_promo_dtl_merch_node_hist rpdm,
                                                    rpm_merch_list_detail rmld,
                                                    item_master im,
                                                    rpm_dept_class_subclass_tbl mh
                                              where rpdm.promo_dtl_id        = rpd.promo_dtl_id
                                                and rpdm.merch_type          = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                                and rmld.merch_list_id       = rpdm.price_event_itemlist
                                                and rmld.item                = im.item
                                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and mh.subclass              = im.subclass
                                                and mh.class                 = im.class
                                                and mh.dept                  = im.dept
                                                and rownum                   = 1
                                             union all  -- storewide - location level
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node_hist rpdm,
                                                    rpm_promo_zone_location_hist rpzl,
                                                    rpm_item_loc ril,
                                                    item_master im
                                              where rpdm.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpdm.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                                and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                                and ril.loc             = rpzl.location
                                                and ril.item            = im.item
                                                and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                and mh.subclass         = im.subclass
                                                and mh.class            = im.class
                                                and mh.dept             = im.dept
                                                and rownum              = 1
                                             union all  -- storewide - zone level
                                             select 'x'
                                               from rpm_dept_class_subclass_tbl mh,
                                                    rpm_promo_dtl_merch_node_hist rpdm,
                                                    rpm_promo_zone_location_hist rpzl,
                                                    rpm_zone_location rzl,
                                                    rpm_item_loc ril,
                                                    item_master im
                                              where rpdm.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpdm.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                                and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                                and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                and rpzl.zone_id        = rzl.zone_id
                                                and ril.loc             = rzl.location
                                                and ril.item            = im.item
                                                and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                and mh.subclass         = im.subclass
                                                and mh.class            = im.class
                                                and mh.dept             = im.dept
                                                and rownum              = 1)
                                 and rpd.promo_comp_id   = rpca.promo_comp_id
                                 and oh.zone_id          = NVL(rpzl.zone_id, oh.zone_id)
                                 and (   oh.location_id  is NULL
                                      or oh.location_id  = NVL(rpzl.location, oh.location_id))
                                 and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                 and rownum              = 1)) t;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_FULL_ACCESS_INFO;
--------------------------------------------------------------------------------

FUNCTION GET_CONFLICT_IND(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(40) := 'RPM_PROM_COMP_AGG_SQL.GET_CONFLICT_IND';

BEGIN

   insert into rpm_prom_comp_agg_tbl(rpm_prom_com_agg_id,
                                     promo_comp_id,
                                     type,
                                     pcpd_ind,
                                     full_access_ind,
                                     conflict_ind,
                                     agg_type)
   select RPM_PROM_COMP_AGG_SEQ.NEXTVAL,
          t.promo_comp_id,
          t.type,
          t.pcpd_ind,
          t.full_access_ind,
          t.conflict_ind,
          5
     from (select rpca.promo_comp_id,
                  NULL type,
                  NULL pcpd_ind,
                  NULL full_access_ind,
                  1    conflict_ind
             from rpm_prom_comp_agg_tbl rpca
            where rpca.agg_type = 1
              and EXISTS (select 'x'
                            from rpm_promo_dtl rpd,
                                 rpm_con_check_err rcce
                           where rpca.promo_comp_id = rpd.promo_comp_id
                             and rpd.promo_dtl_id   = rcce.ref_id
                             and rcce.ref_class     like 'com.oracle.retail.rpm.promotions.bo.%PromotionComponentDetailImpl%'
                             and rownum             = 1) ) t;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_CONFLICT_IND;
--------------------------------------------------------------------------------

FUNCTION GET_TIMEBASED_IND(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(45) := 'RPM_PROM_COMP_AGG_SQL.GET_TIMEBASED_IND';

BEGIN

   insert into rpm_prom_comp_agg_tbl(rpm_prom_com_agg_id,
                                     promo_comp_id,
                                     type,
                                     pcpd_ind,
                                     full_access_ind,
                                     conflict_ind,
                                     agg_type)
   select RPM_PROM_COMP_AGG_SEQ.NEXTVAL,
          t.promo_comp_id,
          t.type,
          t.pcpd_ind,
          t.full_access_ind,
          t.conflict_ind,
          6
     from (select rpca.promo_comp_id,
                  NULL type,
                  NULL pcpd_ind,
                  NULL full_access_ind,
                  1    conflict_ind
             from rpm_prom_comp_agg_tbl rpca
            where rpca.agg_type = 1
              and EXISTS (select 'x'
                            from rpm_promo_dtl rpd
                           where rpd.promo_comp_id     = rpca.promo_comp_id
                             and rpd.timebased_dtl_ind = 1
                             and rownum                = 1)
           union all
           select rpca.promo_comp_id,
                  NULL type,
                  NULL pcpd_ind,
                  NULL full_access_ind,
                  1    conflict_ind
             from rpm_prom_comp_agg_tbl rpca
            where rpca.agg_type = 1
              and EXISTS (select 'x'
                            from rpm_promo_dtl_hist rpd
                           where rpd.promo_comp_id     = rpca.promo_comp_id
                             and rpd.timebased_dtl_ind = 1
                             and rownum                = 1)) t;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_TIMEBASED_IND;
--------------------------------------------------------------------------------

END RPM_PROM_COMP_AGG_SQL;
/

