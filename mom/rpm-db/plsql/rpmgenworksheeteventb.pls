CREATE OR REPLACE PACKAGE BODY RPM_GENERATE_WS_PRICE_EVENTS AS
--------------------------------------------------------
LP_vdate DATE := GET_VDATE;

--PROTOTYPES
--------------------------------------------------------

FUNCTION POPULATE_WORKSPACE(O_error_msg                 OUT VARCHAR2,
                            I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                            I_workspace_id           IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION PUSH_PRICE_CHANGES(O_error_msg               OUT VARCHAR2,
                            I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                            I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                            I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;
---
FUNCTION PUSH_CLEARANCES(O_error_msg               OUT VARCHAR2,
                         I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                         I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;
---
FUNCTION PUSH_WORKSHEET(O_error_msg               OUT VARCHAR2,
                        I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                        I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                        I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;
---
FUNCTION PUSH_WORKSHEET_NOT_AUTOAPPV(O_error_msg        OUT VARCHAR2,
                                     I_workspace_id  IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION RECORD_CONFLICT_ERROR(O_error_msg              OUT VARCHAR2,
                               I_future_retail_id  IN       RPM_FUTURE_RETAIL.FUTURE_RETAIL_ID%TYPE,
                               I_con_check_err_id  IN       RPM_CON_CHECK_ERR.CON_CHECK_ERR_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION RECORD_AUTO_APPROVE_ERROR(O_error_msg               OUT VARCHAR2,
                                   I_price_change_id      IN     RPM_PRICE_CHANGE.PRICE_CHANGE_ID%TYPE,
                                   I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                                   I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;
---
FUNCTION CREATE_PRICE_CHANGES(O_error_msg                 OUT VARCHAR2,
                              IO_bulk_cc_pe_id         IN OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                              I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                              I_cc_request_group_id    IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE,
                              I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                              I_workspace_id           IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE,
                              I_sec_bulk_cc_pe_id      IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                              I_primary_ind            IN     VARCHAR2,
                              I_action                 IN     VARCHAR2,
                              I_user_id                IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                              I_auto_approve_ind       IN     NUMBER)
RETURN BOOLEAN;
---

FUNCTION CREATE_CLEARANCES(O_error_msg                 OUT VARCHAR2,
                           IO_clr_bulk_cc_pe_id     IN OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                           I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                           I_cc_request_group_id    IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE,
                           I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                           I_workspace_id           IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE,
                           I_action                 IN     VARCHAR2,
                           I_user_id                IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION PROPOSE_UOM_PKG_CALL(O_error_msg    IN OUT  VARCHAR2,
                              I_workspace_id IN      RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION AREA_DIFF_PROPOSE(O_error_msg                  OUT VARCHAR2,
                           O_secondary_worksheet_ids    OUT OBJ_NUMERIC_ID_TABLE,
                           I_worksheet_status_ids    IN     OBJ_NUMERIC_ID_TABLE,
                           I_workspace_id            IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION CLEAN_UP(O_error_msg              OUT VARCHAR2,
                  O_group_complete_ind     OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                  I_cc_request_group_id IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION SPLIT_INPUT_PRIM_SEC(O_error_msg                OUT VARCHAR2,
                              I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE,
                              O_primary_worksheet_ids    OUT OBJ_NUMERIC_ID_TABLE,
                              O_secondary_worksheet_ids  OUT OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;

--------------------------------------------------------
--INTERNAL PROCEDURES
--------------------------------------------------------

FUNCTION POPULATE_WORKSPACE(O_error_msg                 OUT VARCHAR2,
                            I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                            I_workspace_id           IN     rpm_worksheet_item_gen_ws.workspace_id%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RPM_GENERATE_WS_PRICE_EVENTS.POPULATE_WORKSPACE';

BEGIN

   insert into rpm_worksheet_item_gen_ws (
                 worksheet_item_data_id,
                 workspace_id,
                 worksheet_status_id,
                 proposed_strategy_id,
                 state,
                 item,
                 dept,
                 standard_uom,
                 item_parent,
                 primary_comp_retail,
                 primary_comp_store,
                 primary_comp_retail_uom,
                 primary_comp_retail_std,
                 primary_multi_units,
                 primary_multi_unit_retail,
                 primary_multi_unit_retail_uom,
                 zone_group_id,
                 zone_id,
                 currency,
                 original_retail,
                 margin_mkt_basket_code,
                 comp_mkt_basket_code,
                 margin_mbc_name,
                 competitive_mbc_name,
                 basis_zl_regular_retail,
                 basis_zl_regular_retail_uom,
                 basis_zl_regular_retail_std,
                 basis_zl_clear_retail,
                 basis_zl_clear_retail_uom,
                 basis_zl_clear_retail_std,
                 basis_zl_clear_mkdn_nbr,
                 basis_zl_multi_units,
                 basis_zl_multi_unit_retail,
                 basis_zl_multi_uom,
                 proposed_zl_reset_date,
                 proposed_zl_out_of_stock_date,
                 action_flag,
                 primary_comp_boolean,
                 area_diff_prim_id,
                 area_diff_id)
          select /*+ ordered */
                 wd.worksheet_item_data_id,
                 I_workspace_id,
                 wd.worksheet_status_id,
                 wd.proposed_strategy_id,
                 wd.state,
                 wd.item,
                 wd.dept,
                 wd.standard_uom,
                 wd.item_parent,
                 wd.primary_comp_retail,
                 wd.primary_comp_store,
                 wd.primary_comp_retail_uom,
                 wd.primary_comp_retail_std,
                 wd.primary_multi_units,
                 wd.primary_multi_unit_retail,
                 wd.primary_multi_unit_retail_uom,
                 wd.zone_group_id,
                 wd.zone_id,
                 wd.currency,
                 wd.original_retail,
                 wd.margin_mkt_basket_code,
                 wd.comp_mkt_basket_code,
                 wd.margin_mbc_name,
                 wd.competitive_mbc_name,
                 wd.basis_zl_regular_retail,
                 wd.basis_zl_regular_retail_uom,
                 wd.basis_zl_regular_retail_std,
                 wd.basis_zl_clear_retail,
                 wd.basis_zl_clear_retail_uom,
                 wd.basis_zl_clear_retail_std,
                 wd.basis_zl_clear_mkdn_nbr,
                 wd.basis_zl_multi_units,
                 wd.basis_zl_multi_unit_retail,
                 wd.basis_zl_multi_uom,
                 wd.proposed_zl_reset_date,
                 wd.proposed_zl_out_of_stock_date,
                 wd.action_flag,
                 wd.primary_comp_boolean,
                 wd.area_diff_prim_id,
                 wd.area_diff_id
            from (select value(ids) wsid
                    from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids
                  union
                  select worksheet_status_id wsid
                    from rpm_worksheet_status
                   where area_diff_prim_ws_id IN (select value(ids)
                                                    from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids)) input,
                 rpm_worksheet_status wss,
                 rpm_worksheet_item_data wd
           where wss.worksheet_status_id = input.wsid
             and wd.worksheet_status_id  = wss.worksheet_status_id
             and wd.dept                 = wss.dept;

   insert into rpm_worksheet_item_loc_gen_ws (
                 worksheet_item_data_id,
                 worksheet_item_loc_data_id,
                 workspace_id,
                 worksheet_status_id,
                 proposed_strategy_id,
                 item,
                 dept,
                 il_primary_supp,
                 il_primary_cntry,
                 item_parent,
                 zone_id,
                 location,
                 loc_type,
                 current_regular_retail,
                 current_regular_retail_uom,
                 current_regular_retail_std,
                 current_clear_retail,
                 current_clear_retail_uom,
                 current_clear_retail_std,
                 current_multi_units,
                 current_multi_unit_retail,
                 current_multi_unit_uom,
                 link_code,
                 link_code_display_id,
                 basis_regular_retail,
                 basis_regular_retail_uom,
                 basis_regular_retail_std,
                 basis_clear_retail,
                 basis_clear_retail_uom,
                 basis_clear_retail_std,
                 basis_multi_units,
                 basis_multi_unit_retail,
                 basis_multi_unit_uom,
                 proposed_retail,
                 proposed_retail_uom,
                 proposed_retail_std,
                 proposed_clear_mkdn_nbr,
                 proposed_clear_ind,
                 proposed_reset_date,
                 proposed_out_of_stock_date,
                 action_flag,
                 effective_date,
                 new_retail,
                 new_retail_uom,
                 new_multi_unit_ind,
                 new_multi_units,
                 new_multi_unit_retail,
                 new_multi_unit_uom,
                 new_retail_std,
                 new_clear_mkdn_nbr,
                 new_clear_ind,
                 new_reset_date,
                 new_out_of_stock_date,
                 new_item_loc_boolean,
                 original_effective_date)
          select /*+ ordered */
                 wd.worksheet_item_data_id,
                 wd.worksheet_item_loc_data_id,
                 I_workspace_id,
                 wd.worksheet_status_id,
                 wd.proposed_strategy_id,
                 wd.item,
                 wd.dept,
                 wd.il_primary_supp,
                 wd.il_primary_cntry,
                 wd.item_parent,
                 wd.zone_id,
                 wd.location,
                 wd.loc_type,
                 wd.current_regular_retail,
                 wd.current_regular_retail_uom,
                 wd.current_regular_retail_std,
                 wd.current_clear_retail,
                 wd.current_clear_retail_uom,
                 wd.current_clear_retail_std,
                 wd.current_multi_units,
                 wd.current_multi_unit_retail,
                 wd.current_multi_unit_uom,
                 wd.link_code,
                 wd.link_code_display_id,
                 wd.basis_regular_retail,
                 wd.basis_regular_retail_uom,
                 wd.basis_regular_retail_std,
                 wd.basis_clear_retail,
                 wd.basis_clear_retail_uom,
                 wd.basis_clear_retail_std,
                 wd.basis_multi_units,
                 wd.basis_multi_unit_retail,
                 wd.basis_multi_unit_uom,
                 wd.proposed_retail,
                 wd.proposed_retail_uom,
                 wd.proposed_retail_std,
                 wd.proposed_clear_mkdn_nbr,
                 wd.proposed_clear_ind,
                 wd.proposed_reset_date,
                 wd.proposed_out_of_stock_date,
                 wd.action_flag,
                 wd.effective_date,
                 wd.new_retail,
                 wd.new_retail_uom,
                 wd.new_multi_unit_ind,
                 wd.new_multi_units,
                 wd.new_multi_unit_retail,
                 wd.new_multi_unit_uom,
                 wd.new_retail_std,
                 wd.new_clear_mkdn_nbr,
                 wd.new_clear_ind,
                 wd.new_reset_date,
                 wd.new_out_of_stock_date,
                 wd.new_item_loc_boolean,
                 wd.original_effective_date
            from (select value(ids) wsid
                    from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids
                  union
                  select worksheet_status_id wsid
                    from rpm_worksheet_status
                   where area_diff_prim_ws_id IN (select value(ids)
                                                    from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids)) input,
                 rpm_worksheet_status wss,
                 rpm_worksheet_item_loc_data wd
           where wss.worksheet_status_id = input.wsid
             and wd.worksheet_status_id  = wss.worksheet_status_id
             and wd.dept                 = wss.dept;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_WORKSPACE;

--------------------------------------------------------

FUNCTION PUSH_PRICE_CHANGES(O_error_msg               OUT VARCHAR2,
                            I_bulk_cc_pe_id        IN     rpm_bulk_cc_pe.bulk_cc_pe_id%TYPE,
                            I_parent_thread_number IN     rpm_bulk_cc_pe_thread.parent_thread_number%TYPE,
                            I_thread_number        IN     rpm_bulk_cc_pe_thread.thread_number%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_WS_PRICE_EVENTS.PUSH_PRICE_CHANGES';

BEGIN

   insert into rpm_price_change (price_change_id,
                                 price_change_display_id,
                                 state,
                                 reason_code,
                                 exception_parent_id,
                                 item,
                                 diff_id,
                                 zone_id,
                                 location,
                                 zone_node_type,
                                 link_code,
                                 effective_date,
                                 change_type,
                                 change_amount,
                                 change_currency,
                                 change_percent,
                                 change_selling_uom,
                                 null_multi_ind,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_unit_retail_currency,
                                 multi_selling_uom,
                                 price_guide_id,
                                 vendor_funded_ind,
                                 funding_type,
                                 funding_amount,
                                 funding_amount_currency,
                                 funding_percent,
                                 deal_id,
                                 deal_detail_id,
                                 create_date,
                                 create_id,
                                 approval_date,
                                 approval_id,
                                 area_diff_parent_id,
                                 ignore_constraints,
                                 lock_version)
      select pc.price_change_id,
             pc.price_change_display_id,
             pc.state,
             pc.reason_code,
             pc.exception_parent_id,
             pc.item,
             pc.diff_id,
             pc.zone_id,
             pc.location,
             pc.zone_node_type,
             pc.link_code,
             pc.effective_date,
             pc.change_type,
             pc.change_amount,
             pc.change_currency,
             pc.change_percent,
             pc.change_selling_uom,
             pc.null_multi_ind,
             pc.multi_units,
             pc.multi_unit_retail,
             pc.multi_unit_retail_currency,
             pc.multi_selling_uom,
             pc.price_guide_id,
             pc.vendor_funded_ind,
             pc.funding_type,
             pc.funding_amount,
             pc.funding_amount_currency,
             pc.funding_percent,
             pc.deal_id,
             pc.deal_detail_id,
             pc.create_date,
             pc.create_id,
             pc.approval_date,
             pc.approval_id,
             pc.area_diff_parent_id,
             pc.ignore_constraints,
             pc.lock_version
        from rpm_bulk_cc_wksht_pc pc,
             rpm_bulk_cc_pe_thread cc
       where cc.bulk_cc_pe_id        = I_bulk_cc_pe_id
         and cc.parent_thread_number = I_parent_thread_number
         and cc.thread_number        = I_thread_number
         and cc.price_event_id       = pc.price_change_id;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END PUSH_PRICE_CHANGES;

--------------------------------------------------------

FUNCTION PUSH_CLEARANCES(O_error_msg               OUT VARCHAR2,
                         I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                         I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_GENERATE_WS_PRICE_EVENTS.PUSH_CLEARANCES';

BEGIN

   insert into rpm_clearance (clearance_id,
                              clearance_display_id,
                              state,
                              reason_code,
                              exception_parent_id,
                              item,
                              diff_id,
                              zone_id,
                              location,
                              zone_node_type,
                              effective_date,
                              out_of_stock_date,
                              reset_date,
                              change_type,
                              change_amount,
                              change_currency,
                              change_percent,
                              change_selling_uom,
                              price_guide_id,
                              vendor_funded_ind,
                              funding_type,
                              funding_amount,
                              funding_amount_currency,
                              funding_percent,
                              supplier,
                              deal_id,
                              deal_detail_id,
                              partner_type,
                              partner_id,
                              create_date,
                              create_id,
                              approval_date,
                              approval_id,
                              lock_version)
      select cl.clearance_id,
             cl.clearance_display_id,
             cl.state,
             cl.reason_code,
             cl.exception_parent_id,
             cl.item,
             cl.diff_id,
             cl.zone_id,
             cl.location,
             cl.zone_node_type,
             cl.effective_date,
             cl.out_of_stock_date,
             cl.reset_date,
             cl.change_type,
             cl.change_amount,
             cl.change_currency,
             cl.change_percent,
             cl.change_selling_uom,
             cl.price_guide_id,
             cl.vendor_funded_ind,
             cl.funding_type,
             cl.funding_amount,
             cl.funding_amount_currency,
             cl.funding_percent,
             cl.supplier,
             cl.deal_id,
             cl.deal_detail_id,
             cl.partner_type,
             cl.partner_id,
             cl.create_date,
             cl.create_id,
             cl.approval_date,
             cl.approval_id,
             cl.lock_version
        from rpm_bulk_cc_wksht_clr cl,
             rpm_bulk_cc_pe_thread cc
       where cc.bulk_cc_pe_id        = I_bulk_cc_pe_id
         and cc.parent_thread_number = I_parent_thread_number
         and cc.thread_number        = I_thread_number
         and cc.price_event_id       = cl.clearance_id;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END PUSH_CLEARANCES;

--------------------------------------------------------

FUNCTION PUSH_WORKSHEET(O_error_msg        OUT VARCHAR2,
                        I_bulk_cc_pe_id IN     rpm_bulk_cc_pe.bulk_cc_pe_id%TYPE,
                        I_parent_thread_number IN     rpm_bulk_cc_pe_thread.parent_thread_number%TYPE,
                        I_thread_number IN     rpm_bulk_cc_pe_thread.thread_number%TYPE)
RETURN BOOLEAN IS

BEGIN

   merge into rpm_worksheet_item_loc_data wd
   using (select wsiloc.proposed_retail,
                 wsiloc.proposed_retail_uom,
                 wsiloc.proposed_retail_std,
                 wsiloc.new_retail,
                 wsiloc.new_retail_uom,
                 wsiloc.new_retail_std,
                 wsiloc.action_flag,
                 wsiloc.worksheet_item_data_id,
                 wsiloc.worksheet_item_loc_data_id
            from rpm_worksheet_item_loc_gen_ws wsiloc,
                 rpm_bulk_cc_pe bulk,
                 rpm_bulk_cc_pe_thread thread
           where bulk.bulk_cc_pe_id          = I_bulk_cc_pe_id
             and thread.bulk_cc_pe_id        = bulk.bulk_cc_pe_id
             and thread.parent_thread_number = I_parent_thread_number
             and thread.thread_number        = I_thread_number
             and nvl(to_char(thread.link_code), thread.item) = nvl(wsiloc.link_code, wsiloc.item)
             and thread.zone_node_id         = wsiloc.zone_id
             and wsiloc.workspace_id          = bulk.worksheet_workspace_id) use_this
   on (    wd.worksheet_item_data_id = use_this.worksheet_item_data_id
       and wd.worksheet_item_loc_data_id = use_this.worksheet_item_loc_data_id)
   when matched then update
      set proposed_retail     = use_this.proposed_retail,
          proposed_retail_uom = use_this.proposed_retail_uom,
          proposed_retail_std = use_this.proposed_retail,
          new_retail          = use_this.new_retail,
          new_retail_uom      = use_this.new_retail_uom,
          new_retail_std      = use_this.new_retail,
          action_flag         = use_this.action_flag;

   merge into rpm_worksheet_item_data wd
   using (select distinct wsitem.state,
                 wsitem.action_flag,
                 wsitem.worksheet_item_data_id
            from rpm_worksheet_item_gen_ws wsitem,
                 rpm_worksheet_item_loc_gen_ws wsiloc,
                 rpm_bulk_cc_pe bulk,
                 rpm_bulk_cc_pe_thread thread
           where bulk.bulk_cc_pe_id            = I_bulk_cc_pe_id
             and thread.bulk_cc_pe_id          = bulk.bulk_cc_pe_id
             and thread.parent_thread_number   = I_parent_thread_number
             and thread.thread_number          = I_thread_number
             and nvl(to_char(thread.link_code), thread.item) = nvl(wsiloc.link_code, wsiloc.item)
             and thread.zone_node_id           = wsiloc.zone_id
             and wsiloc.workspace_id           = bulk.worksheet_workspace_id
             and wsitem.workspace_id           = wsiloc.workspace_id
             and wsitem.worksheet_item_data_id = wsiloc.worksheet_item_data_id) use_this
   on (    wd.worksheet_item_data_id = use_this.worksheet_item_data_id)
   when matched then update
      set state               = use_this.state,
          action_flag         = use_this.action_flag;

   merge into rpm_worksheet_zone_data wd
   using (select distinct wsitem.state,
                 wsitem.action_flag,
                 wsitem.worksheet_item_data_id,
                 wsitem.worksheet_status_id
            from rpm_worksheet_item_gen_ws wsitem,
                 rpm_worksheet_item_loc_gen_ws wsiloc,
                 rpm_bulk_cc_pe bulk,
                 rpm_bulk_cc_pe_thread thread
           where bulk.bulk_cc_pe_id            = I_bulk_cc_pe_id
             and thread.bulk_cc_pe_id          = bulk.bulk_cc_pe_id
             and thread.parent_thread_number   = I_parent_thread_number
             and thread.thread_number          = I_thread_number
             and nvl(to_char(thread.link_code), thread.item) = nvl(wsiloc.link_code, wsiloc.item)
             and thread.zone_node_id           = wsiloc.zone_id
             and wsiloc.workspace_id           = bulk.worksheet_workspace_id
             and wsitem.workspace_id           = wsiloc.workspace_id
             and wsitem.worksheet_item_data_id = wsiloc.worksheet_item_data_id) use_this
   on (    wd.worksheet_status_id = use_this.worksheet_status_id
       and wd.worksheet_item_data_id = use_this.worksheet_item_data_id)
   when matched then update
      set state               = use_this.state,
          action_flag         = use_this.action_flag;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.PUSH_WORKSHEET',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END PUSH_WORKSHEET;

--------------------------------------------------------

FUNCTION PUSH_WORKSHEET_NOT_AUTOAPPV(O_error_msg        OUT VARCHAR2,
                                     I_workspace_id  IN     rpm_worksheet_item_gen_ws.workspace_id%TYPE)
RETURN BOOLEAN IS

BEGIN

   merge into rpm_worksheet_item_data wd
   using (select wsitem.state,
                 wsitem.action_flag,
                 wsitem.worksheet_item_data_id
            from rpm_worksheet_item_gen_ws wsitem
           where wsitem.workspace_id = I_workspace_id) use_this
   on (wd.worksheet_item_data_id = use_this.worksheet_item_data_id)
   when matched then update
      set state               = use_this.state,
          action_flag         = use_this.action_flag;

   merge into rpm_worksheet_item_loc_data wd
   using (select wsiloc.proposed_retail,
                 wsiloc.proposed_retail_uom,
                 wsiloc.proposed_retail_std,
                 wsiloc.new_retail,
                 wsiloc.new_retail_uom,
                 wsiloc.new_retail_std,
                 wsiloc.action_flag,
                 wsiloc.worksheet_item_loc_data_id
            from rpm_worksheet_item_loc_gen_ws wsiloc
           where wsiloc.workspace_id = I_workspace_id) use_this
   on (wd.worksheet_item_loc_data_id = use_this.worksheet_item_loc_data_id)
   when matched then update
      set proposed_retail     = use_this.proposed_retail,
          proposed_retail_uom = use_this.proposed_retail_uom,
          proposed_retail_std = use_this.proposed_retail,
          new_retail          = use_this.new_retail,
          new_retail_uom      = use_this.new_retail_uom,
          new_retail_std      = use_this.new_retail,
          action_flag         = use_this.action_flag;

   merge into rpm_worksheet_zone_data wd
   using (select wsitem.state,
                 wsitem.action_flag,
                 wsitem.worksheet_item_data_id,
                 wsitem.worksheet_status_id
            from rpm_worksheet_item_gen_ws wsitem
           where wsitem.workspace_id = I_workspace_id) use_this
   on (    wd.worksheet_status_id = use_this.worksheet_status_id
       and wd.worksheet_item_data_id = use_this.worksheet_item_data_id)
   when matched then update
      set state               = use_this.state,
          action_flag         = use_this.action_flag;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.PUSH_WORKSHEET_NOT_AUTOAPPV',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END PUSH_WORKSHEET_NOT_AUTOAPPV;

--------------------------------------------------------

FUNCTION RECORD_CONFLICT_ERROR(O_error_msg           OUT VARCHAR2,
                               I_future_retail_id IN     RPM_FUTURE_RETAIL.FUTURE_RETAIL_ID%TYPE,
                               I_con_check_err_id IN     RPM_CON_CHECK_ERR.CON_CHECK_ERR_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_GENERATE_WS_PRICE_EVENTS.RECORD_CONFLICT_ERROR';

BEGIN

   insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                         con_check_err_id,
                                         ref_class,
                                         ref_id,
                                         ref_display_id,
                                         ref_secondary_display_id,
                                         effective_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_unit_uom)
                                  select RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                                         I_con_check_err_id,
                                         'com.retek.rpm.domain.pricechange.bo.StandardPriceChange',
                                         price_change_id,
                                         price_change_display_id,
                                         NULL,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom
                                    from rpm_future_retail fr
                                   where future_retail_id = I_future_retail_id
                                     and price_change_id  is NOT NULL;

   insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                         con_check_err_id,
                                         ref_class,
                                         ref_id,
                                         ref_display_id,
                                         ref_secondary_display_id,
                                         effective_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_unit_uom)
                                  select RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                                         I_con_check_err_id,
                                         'com.retek.rpm.domain.pricechange.bo.ClearancePriceChange',
                                         clearance_id,
                                         clearance_display_id,
                                         NULL,
                                         action_date,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom
                                    from rpm_future_retail fr
                                   where future_retail_id = I_future_retail_id
                                     and clearance_id     is NOT NULL;

   insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                         con_check_err_id,
                                         ref_class,
                                         ref_id,
                                         ref_display_id,
                                         ref_secondary_display_id,
                                         effective_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_unit_uom)
                                  select RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                                         I_con_check_err_id,
                                         'com.oracle.retail.rpm.promotions.bo.PromotionComponentDetail',
                                         rpile.promo_dtl_id,
                                         rpile.promo_display_id,
                                         rpile.comp_display_id,
                                         fr.action_date,
                                         fr.simple_promo_retail,
                                         fr.simple_promo_retail_currency,
                                         fr.simple_promo_uom,
                                         fr.multi_units,
                                         fr.multi_unit_retail,
                                         fr.multi_unit_retail_currency,
                                         fr.multi_selling_uom
                                    from rpm_future_retail fr,
                                         rpm_promo_item_loc_expl rpile
                                   where fr.future_retail_id       = I_future_retail_id
                                     and rpile.dept                = fr.dept
                                     and rpile.item                = fr.item
                                     and rpile.location            = fr.location
                                     and fr.action_date           >= rpile.detail_start_date
                                     and fr.action_date           <= NVL(rpile.detail_end_date, fr.action_date)
                                     and rpile.detail_change_type != RPM_CONSTANTS.RETAIL_EXCLUDE;

   insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                         con_check_err_id,
                                         ref_class,
                                         ref_id,
                                         ref_display_id,
                                         ref_secondary_display_id,
                                         effective_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_unit_uom)
                                  select RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                                         I_con_check_err_id,
                                         'com.oracle.retail.rpm.promotions.bo.PromotionComponentDetail',
                                         rpile.promo_dtl_id,
                                         rpile.promo_display_id,
                                         rpile.comp_display_id,
                                         fr.action_date,
                                         fr.simple_promo_retail,
                                         fr.simple_promo_retail_currency,
                                         fr.simple_promo_uom,
                                         fr.multi_units,
                                         fr.multi_unit_retail,
                                         fr.multi_unit_retail_currency,
                                         fr.multi_selling_uom
                                    from rpm_future_retail fr,
                                         rpm_promo_item_loc_expl rpile
                                   where fr.future_retail_id      = I_future_retail_id
                                     and rpile.dept               = fr.dept
                                     and rpile.item               = fr.item
                                     and rpile.location           = fr.location
                                     and fr.action_date          >= rpile.detail_start_date
                                     and fr.action_date          <= NVL(rpile.detail_end_date, fr.action_date)
                                     and rpile.detail_change_type = RPM_CONSTANTS.RETAIL_EXCLUDE;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END RECORD_CONFLICT_ERROR;

--------------------------------------------------------

FUNCTION RECORD_AUTO_APPROVE_ERROR(O_error_msg               OUT VARCHAR2,
                                   I_price_change_id      IN     rpm_price_change.price_change_id%TYPE,
                                   I_bulk_cc_pe_id        IN     rpm_bulk_cc_pe.bulk_cc_pe_id%TYPE,
                                   I_thread_number        IN     rpm_bulk_cc_pe_thread.thread_number%TYPE)
RETURN BOOLEAN IS

BEGIN

   merge into rpm_maint_margin_err mme
   using ( select distinct rws.review_date              end_date,
                  decode(rws.subclass,
                         null,
                         decode(rws.class, null, 3, 2),
                         1)                             merch_type,
                  rws.dept                              dept,
                  rws.class                             class,
                  rws.subclass                          subclass,
                  1                                     zone_node_type,
                  rws.zone_id                           zone_id,
                  null                                  location
             from rpm_worksheet_status rws,
                  rpm_worksheet_item_loc_gen_ws wsiloc,
                  rpm_bulk_cc_pe bulk,
                  rpm_bulk_cc_pe_thread thd
            where bulk.bulk_cc_pe_id          = I_bulk_cc_pe_id
              and bulk.bulk_cc_pe_id          = thd.bulk_cc_pe_id
              and thd.thread_number           = I_thread_number
              and bulk.worksheet_workspace_id = wsiloc.workspace_id
              and nvl(wsiloc.link_code, wsiloc.item) =  nvl(to_char(thd.link_code), thd.item)
              and wsiloc.zone_id              = thd.zone_node_id
              and rws.worksheet_status_id     = wsiloc.worksheet_status_id) use
   on (    mme.dept      = use.dept
       and mme.class     = use.class
       and mme.subclass  = use.subclass
       and mme.zone_id   = use.zone_id
       and mme.end_date  = use.end_date)
   when matched then
      update set mme.lock_version = null
   when not matched then
      insert (maint_margin_err_id,
              end_date,
              merch_type,
              dept,
              class,
              subclass,
              zone_node_type,
              zone_id,
              location,
              lock_version)
       values (rpm_maint_margin_err_seq.nextval,
               use.end_date,
               use.merch_type,
               use.dept,
               use.class,
               use.subclass,
               use.zone_node_type,
               use.zone_id,
               use.location,
               null);
/*is: get header sequence value in a variable */
   insert into rpm_maint_margin_err_dtl (maint_margin_err_dtl_id,
                                         maint_margin_err_id,
                                         price_change_display_id,
                                         effective_date,
                                         item_id,
                                         item_desc,
                                         zone_node_type,
                                         zone_id,
                                         location,
                                         lock_version)
                                  select rpm_maint_margin_err_dtl_seq.nextval,
                                         rpm_maint_margin_err_seq.currval,
                                         pc.price_change_display_id,
                                         pc.effective_date,
                                         pc.item,
                                         im.item_desc,
                                         1,
                                         pc.zone_id,
                                         null,
                                         null
                                    from rpm_price_change pc,
                                         item_master im
                                  where pc.price_change_id = I_price_change_id
                                    and pc.item = im.item;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.RECORD_AUTO_APPROVE_ERROR',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END RECORD_AUTO_APPROVE_ERROR;

--------------------------------------------------------

FUNCTION CREATE_PRICE_CHANGES(O_error_msg                 OUT VARCHAR2,
                              IO_bulk_cc_pe_id         IN OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                              I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                              I_cc_request_group_id    IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE,
                              I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                              I_workspace_id           IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE,
                              I_sec_bulk_cc_pe_id      IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                              I_primary_ind            IN     VARCHAR2,
                              I_action                 IN     VARCHAR2,
                              I_user_id                IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                              I_auto_approve_ind       IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_GENERATE_WS_PRICE_EVENTS.CREATE_PRICE_CHANGES';

   L_reason_code RPM_CODES.CODE_ID%TYPE              := NULL;

   L_count NUMBER := 0;

   cursor C_PE is
      select COUNT(1)
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id = IO_bulk_cc_pe_id;

   cursor C_REASON_CODE is
      select code_id
        from rpm_codes
       where code_type = RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE
         and code      = 'WS_GEN';

BEGIN

   open C_REASON_CODE;
   fetch C_REASON_CODE into L_reason_code;
   close C_REASON_CODE;

   insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                               price_event_type,
                               persist_ind,
                               start_state,
                               end_state,
                               user_name,
                               emergency_perm,
                               secondary_bulk_cc_pe_id,
                               secondary_ind,
                               asynch_ind,
                               cc_request_group_id,
                               auto_clean_ind,
                               thread_processor_class,
                               status,
                               need_il_explode_ind,
                               worksheet_workspace_id)
                       values (IO_bulk_cc_pe_id,
                               RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                               DECODE(I_action,
                                      'S', 'N',
                                      'Y'), --persist_ind,
                               RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE, --start_state,
                               DECODE(I_action,
                                      'S', RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE,
                                      RPM_CONSTANTS.PC_APPROVED_STATE_CODE), --end_state,
                               I_user_id,                    --user_name,
                               1,                            --emergency_perm,
                               I_sec_bulk_cc_pe_id,          --secondary_bulk_cc_pe_id,
                               DECODE(I_sec_bulk_cc_pe_id,
                                      NULL, 0,
                                      1),                    --secondary_ind,
                               1,                            --asynch_ind,
                               I_cc_request_group_id,        --cc_request_group_id,
                               0,                            --auto_clean_ind,
                               I_thread_processor_class,     --thread_processor_class,
                               'I',                          --status,
                               0,                            --need_il_explode_ind,
                               I_workspace_id);              --worksheet_workspace_id,

   --non link codes
   insert all
      when pe_rownum = 1 then
          into rpm_bulk_cc_wksht_pc (price_change_id,
                                     price_change_display_id,
                                     state,
                                     reason_code,
                                     item,
                                     zone_id,
                                     zone_node_type,
                                     effective_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_selling_uom,
                                     null_multi_ind,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_unit_retail_currency,
                                     multi_selling_uom,
                                     vendor_funded_ind,
                                     create_date,
                                     create_id,
                                     ignore_constraints,
                                     BULK_CC_PE_ID,
                                     worksheet_status_id,
                                     primary_secondary_ind)
                             values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                                     RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                                     state,
                                     reason_code,
                                     item,
                                     zone_id,
                                     zone_node_type,
                                     effective_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_selling_uom,
                                     null_multi_ind,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_unit_retail_currency,
                                     multi_selling_uom,
                                     vendor_funded_ind,
                                     create_date,
                                     create_id,
                                     ignore_constraints,
                                     IO_bulk_cc_pe_id,
                                     worksheet_status_id,
                                     I_primary_ind)
      when pe_rownum = 1 then
         into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                     price_event_id,
                                     price_event_display_id,
                                     price_event_type,
                                     rank,
                                     merch_node_type,
                                     item,
                                     zone_node_type,
                                     zone_node_id)
                             values (IO_bulk_cc_pe_id,
                                     RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                                     RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                                     thread_price_event_type,
                                     thread_rank,
                                     thread_merch_node_type,
                                     thread_item,
                                     thread_zone_node_type,
                                     thread_zone_node_id)
            select --rpm_price_change
                   RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE   state,
                   L_reason_code                           reason_code,
                   w.item                                  item,
                   w.zone_id                               zone_id,
                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE       zone_node_type,
                   w.effective_date                        effective_date,
                   RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                   w.new_retail                            change_amount,
                   igen.currency                           change_currency,
                   w.new_retail_uom                        change_selling_uom,
                   NVL(w.new_multi_unit_ind, 0)            null_multi_ind,
                   w.new_multi_units                       multi_units,
                   w.new_multi_unit_retail                 multi_unit_retail,
                   igen.currency                           multi_unit_retail_currency,
                   w.new_multi_unit_uom                    multi_selling_uom,
                   0                                       vendor_funded_ind,
                   LP_vdate                                create_date,
                   I_user_id                               create_id,
                   0                                       ignore_constraints,
                   --rpm_bulk_cc_pe_thread
                   RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE      thread_price_event_type,
                   1                                       thread_rank,
                   RPM_CONSTANTS.ITEM_LEVEL_ITEM           thread_merch_node_type,
                   w.item                                  thread_item,
                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE       thread_zone_node_type,
                   w.zone_id                               thread_zone_node_id,
                   --rpm_bulk_cc_pe_il
                   w.dept                                  il_dept,
                   w.link_code                             il_link_code,
                   w.item                                  il_item,
                   w.location                              il_location,
                   w.worksheet_status_id,
                   ROW_NUMBER() OVER (PARTITION BY w.worksheet_status_id,
                                                   w.item,
                                                   w.zone_id
                                          ORDER BY NULL) pe_rownum
              from rpm_worksheet_item_gen_ws igen,
                   rpm_worksheet_item_loc_gen_ws w,
                   rpm_worksheet_status ws
             where igen.workspace_id                  = I_workspace_id
               and (   (    I_action                  = 'S'
                        and igen.action_flag          = TAKE
                        and igen.state                IN (WS_STATE_INT_IN_PROGRESS,
                                                          WS_STATE_INT_SUBMITTED))
                    or (    I_action                  = 'A'
                        and igen.action_flag          = TAKE
                        and igen.state                IN (WS_STATE_INT_IN_PROGRESS,
                                                          WS_STATE_INT_SUBMITTED))
                    or (    I_auto_approve_ind        = 1
                        and igen.proposed_strategy_id IN (select strategy_id
                                                            from rpm_strategy_maint_margin
                                                           where auto_approve = 1)
                        and igen.action_flag          != DONT_TAKE
                        and igen.state                IN (WS_STATE_INT_NEW,
                                                          WS_STATE_INT_IN_PROGRESS,
                                                          WS_STATE_INT_PENDING,
                                                          WS_STATE_INT_SUBMITTED,
                                                          WS_STATE_INT_SUBMIT_REJECTED,
                                                          WS_STATE_INT_UPDATED)))
               and w.workspace_id            = igen.workspace_id
               and w.worksheet_item_data_id  = igen.worksheet_item_data_id
               and NVL(w.new_retail, 0)      != NVL(igen.basis_zl_regular_retail, 0)
               and NVL(w.new_clear_ind, 0)   = 0
               and w.link_code               is NULL
               and ws.worksheet_status_id    = igen.worksheet_status_id
               and ws.worksheet_status_id    IN (select value(ids)
                                                   from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   --link codes
   insert all
      when pe_rownum = 1 then
         into rpm_bulk_cc_wksht_pc (price_change_id,
                                    price_change_display_id,
                                    state,
                                    reason_code,
                                    zone_id,
                                    zone_node_type,
                                    link_code,
                                    effective_date,
                                    change_type,
                                    change_amount,
                                    change_currency,
                                    change_selling_uom,
                                    null_multi_ind,
                                    multi_units,
                                    multi_unit_retail,
                                    multi_unit_retail_currency,
                                    multi_selling_uom,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    ignore_constraints,
                                    bulk_cc_pe_id,
                                    worksheet_status_id,
                                    primary_secondary_ind)
                            values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                                    RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                                    state,
                                    reason_code,
                                    zone_id,
                                    zone_node_type,
                                    link_code,
                                    effective_date,
                                    change_type,
                                    change_amount,
                                    change_currency,
                                    change_selling_uom,
                                    null_multi_ind,
                                    multi_units,
                                    multi_unit_retail,
                                    multi_unit_retail_currency,
                                    multi_selling_uom,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    ignore_constraints,
                                    IO_bulk_cc_pe_id,
                                    worksheet_status_id,
                                    I_primary_ind)
      when pe_rownum = 1 then
         into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                     price_event_id,
                                     price_event_display_id,
                                     price_event_type,
                                     rank,
                                     merch_node_type,
                                     item,
                                     link_code,
                                     zone_node_type,
                                     zone_node_id)
                             values (IO_bulk_cc_pe_id,
                                     RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                                     RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                                     thread_price_event_type,
                                     thread_rank,
                                     thread_merch_node_type,
                                     thread_item,
                                     thread_link_code,
                                     thread_zone_node_type,
                                     thread_zone_node_id)
            select --rpm_price_change
                   RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE   state,
                   L_reason_code                           reason_code,
                   w.zone_id                               zone_id,
                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE       zone_node_type,
                   w.link_code                             link_code,
                   w.effective_date                        effective_date,
                   RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                   w.new_retail                            change_amount,
                   igen.currency                           change_currency,
                   w.new_retail_uom                        change_selling_uom,
                   NVL(w.new_multi_unit_ind, 0)            null_multi_ind,
                   w.new_multi_units                       multi_units,
                   w.new_multi_unit_retail                 multi_unit_retail,
                   NVL2(w.new_multi_unit_retail, igen.currency, NULL) multi_unit_retail_currency,
                   w.new_multi_unit_uom                   multi_selling_uom,
                   2                                      vendor_funded_ind,
                   LP_vdate                               create_date,
                   I_user_id                              create_id,
                   0                                      ignore_constraints,
                   --rpm_bulk_cc_pe_thread
                   RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE     thread_price_event_type,
                   1                                      thread_rank,
                   RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM     thread_merch_node_type,
                   w.item                                 thread_item,
                   w.link_code                            thread_link_code,
                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE      thread_zone_node_type,
                   w.zone_id                              thread_zone_node_id,
                   --rpm_bulk_cc_pe_il
                   w.dept                                 il_dept,
                   w.link_code                            il_link_code,
                   w.item                                 il_item,
                   w.location                             il_location,
                   w.worksheet_status_id,
                   ROW_NUMBER() OVER (PARTITION BY w.worksheet_status_id,
                                                   w.link_code,
                                                   w.zone_id
                                          ORDER BY NULL) pe_rownum
              from rpm_worksheet_item_gen_ws igen,
                   rpm_worksheet_item_loc_gen_ws w,
                   rpm_worksheet_status ws
             where igen.workspace_id                   = I_workspace_id
               and (   (    I_action                   = 'S'
                        and igen.action_flag           = TAKE
                        and igen.state                IN (WS_STATE_INT_IN_PROGRESS,
                                                          WS_STATE_INT_SUBMITTED))
                    or (    I_action                   = 'A'
                        and igen.action_flag           = TAKE
                        and igen.state                IN (WS_STATE_INT_IN_PROGRESS,
                                                          WS_STATE_INT_SUBMITTED))
                    or (    I_auto_approve_ind         = 1
                        and igen.proposed_strategy_id IN (select strategy_id
                                                            from rpm_strategy_maint_margin
                                                           where auto_approve = 1)
                        and igen.action_flag          != DONT_TAKE
                        and igen.state                IN (WS_STATE_INT_NEW,
                                                          WS_STATE_INT_IN_PROGRESS,
                                                          WS_STATE_INT_PENDING,
                                                          WS_STATE_INT_SUBMITTED,
                                                          WS_STATE_INT_SUBMIT_REJECTED,
                                                          WS_STATE_INT_UPDATED)))
               and w.workspace_id            = igen.workspace_id
               and w.worksheet_item_data_id  = igen.worksheet_item_data_id
               and NVL(w.new_retail, 0)      != NVL(igen.basis_zl_regular_retail, 0)
               and NVL(w.new_clear_ind, 0)   = 0
               and w.link_code               is NOT NULL
               and ws.worksheet_status_id    = igen.worksheet_status_id
               and ws.worksheet_status_id    IN (select value(ids)
                                                   from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   --- Non Link Code
   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    merch_level_type,
                                    link_code,
                                    thread_number,
                                    pe_merch_level)
      select distinct
             IO_bulk_cc_pe_id,
             pc.price_change_id,
             1, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             w.item,
             case
                when im.item_parent is NULL and
                     im.item_level = im.tran_level then
                   NULL
                else
                   im.diff_1
             end,
             RPM_CONSTANTS.ITEM_MERCH_TYPE, -- merch_level_type
             w.link_code,
             1, -- thread_number,
             RPM_CONSTANTS.ITEM_MERCH_TYPE -- pe_merch_level
        from rpm_worksheet_item_loc_gen_ws w,
             rpm_bulk_cc_wksht_pc pc,
             item_master im
       where pc.bulk_cc_pe_id       = IO_bulk_cc_pe_id
         and pc.worksheet_status_id = w.worksheet_status_id
         and pc.zone_id             = w.zone_id
         and pc.link_code           is NULL
         and pc.item                = w.item
         and im.item                = pc.item;

   insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                        price_event_id,
                                        itemloc_id,
                                        location,
                                        zone_node_type,
                                        zone_id)
      -- Locations of the Zone
      select distinct
             IO_bulk_cc_pe_id,
             pc.price_change_id,
             1, -- itemloc_id
             w.location,
             w.loc_type,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   w.zone_id
                else
                   NULL
             end
        from rpm_worksheet_item_loc_gen_ws w,
             rpm_bulk_cc_wksht_pc pc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where pc.bulk_cc_pe_id       = IO_bulk_cc_pe_id
         and pc.worksheet_status_id = w.worksheet_status_id
         and pc.zone_id             = w.zone_id
         and pc.link_code           is NULL
         and pc.item                = w.item
         and im.item                = w.item
         and rz.zone_id             = w.zone_id
         and rmrde.dept             = im.dept
         and rmrde.class            = im.class
         and rmrde.subclass         = im.subclass
      union all
      -- Zone of the Locations for PZG
      select distinct
             IO_bulk_cc_pe_id,
             pc.price_change_id,
             1,
             w.zone_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
             NULL -- zone_id
        from rpm_worksheet_item_loc_gen_ws w,
             rpm_bulk_cc_wksht_pc pc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where pc.bulk_cc_pe_id         = IO_bulk_cc_pe_id
         and pc.worksheet_status_id   = w.worksheet_status_id
         and pc.zone_id               = w.zone_id
         and rz.zone_id               = w.zone_id
         and pc.link_code             is NULL
         and pc.item                  = w.item
         and im.item                  = w.item
         and w.dept                   = rmrde.dept
         and rmrde.dept               = im.dept
         and rmrde.class              = im.class
         and rmrde.subclass           = im.subclass
         and rmrde.regular_zone_group = rz.zone_group_id;

   --- Link Code
   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                merch_level_type,
                                link_code,
                                thread_number,
                                pe_merch_level)
                        values (IO_bulk_cc_pe_id,
                                price_event_id,
                                1,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                RPM_CONSTANTS.ITEM_MERCH_TYPE,
                                link_code,
                                1, -- thread_number,
                                RPM_CONSTANTS.ITEM_MERCH_TYPE)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location,
                                    zone_node_type)
                            values (IO_bulk_cc_pe_id,
                                    price_event_id,
                                    1,
                                    location,
                                    zone_node_type)
         select pc.price_change_id price_event_id,
                im.dept,
                im.class,
                im.subclass,
                w.item,
                case
                   when im.item_parent is NULL and
                        im.item_level = im.tran_level then
                      NULL
                   else
                      im.diff_1
                end diff_id,
                w.location,
                w.loc_type zone_node_type,
                w.link_code
           from rpm_worksheet_item_loc_gen_ws w,
                rpm_bulk_cc_wksht_pc pc,
                item_master im
          where pc.bulk_cc_pe_id       = IO_bulk_cc_pe_id
            and pc.worksheet_status_id = w.worksheet_status_id
            and pc.zone_id             = w.zone_id
            and pc.link_code           is NOT NULL
            and TO_CHAR(pc.link_code)  = w.link_code
            and im.item                = w.item;

   --clear out the ID if no price changes are generated
   open C_PE;
   fetch C_PE into L_count;
   close C_PE;

   if L_count = 0 then

      delete
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = IO_bulk_cc_pe_id;

      --don't point to an empty bulk cc ID
      if I_primary_ind = 'S' then
         update rpm_bulk_cc_pe
            set secondary_bulk_cc_pe_id = NULL,
                secondary_ind           = 0
          where secondary_bulk_cc_pe_id = IO_bulk_cc_pe_id;

      end if;

      IO_bulk_cc_pe_id := NULL;

   end if;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CREATE_PRICE_CHANGES;

--------------------------------------------------------

FUNCTION CREATE_CLEARANCES(O_error_msg                 OUT VARCHAR2,
                           IO_clr_bulk_cc_pe_id     IN OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                           I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                           I_cc_request_group_id    IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE,
                           I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                           I_workspace_id           IN     RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE,
                           I_action                 IN     VARCHAR2,
                           I_user_id                IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_WS_PRICE_EVENTS.CREATE_CLEARANCES';

   L_reason_code RPM_CODES.CODE_ID%TYPE := NULL;
   L_count       NUMBER                 := 0;

   cursor C_PE is
      select COUNT(1)
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id = IO_clr_bulk_cc_pe_id;

BEGIN

   select code_id
     into L_reason_code
     from rpm_codes
    where code_type = RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE
      and code      = 'WS_GEN';

   insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                               price_event_type,
                               persist_ind,
                               start_state,
                               end_state,
                               user_name,
                               emergency_perm,
                               secondary_ind,
                               asynch_ind,
                               cc_request_group_id,
                               auto_clean_ind,
                               thread_processor_class,
                               status,
                               need_il_explode_ind,
                               worksheet_workspace_id)
                       values (IO_clr_bulk_cc_pe_id,
                               RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                               DECODE(I_action,
                                      'S', 'N',
                                      'Y'), --persist_ind,
                               RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE, --start_state
                               DECODE(I_action,
                                      'S', RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE,
                                      RPM_CONSTANTS.PC_APPROVED_STATE_CODE), --end_state,
                               I_user_id,                    --user_name,
                               1,                            --emergency_perm,
                               0,                            --secondary_ind,
                               1,                            --asynch_ind,
                               I_cc_request_group_id,
                               0,                            --auto_clean_ind,
                               I_thread_processor_class,
                               'I',                          --status,
                               0,                            --need_il_explode_ind,
                               I_workspace_id);              --worksheet_workspace_id

   insert all
      when pe_rownum = 1 then
         into rpm_bulk_cc_wksht_clr (clearance_id,
                                     clearance_display_id,
                                     state,
                                     reason_code,
                                     item,
                                     zone_id,
                                     zone_node_type,
                                     effective_date,
                                     out_of_stock_date,
                                     reset_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_selling_uom,
                                     vendor_funded_ind,
                                     create_date,
                                     create_id,
                                     bulk_cc_pe_id,
                                     worksheet_status_id,
                                     primary_secondary_ind)
                             values (RPM_CLEARANCE_SEQ.NEXTVAL,
                                     RPM_CLEARANCE_DISPLAY_SEQ.NEXTVAL,
                                     state,
                                     reason_code,
                                     item,
                                     zone_id,
                                     zone_node_type,
                                     effective_date,
                                     out_of_stock_date,
                                     reset_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_selling_uom,
                                     vendor_funded_ind,
                                     create_date,
                                     create_id,
                                     IO_clr_bulk_cc_pe_id,
                                     worksheet_status_id,
                                     'P')
      when pe_rownum = 1 then
         into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                     price_event_id,
                                     price_event_display_id,
                                     price_event_type,
                                     rank,
                                     merch_node_type,
                                     item,
                                     zone_node_type,
                                     zone_node_id)
                             values (IO_clr_bulk_cc_pe_id,
                                     RPM_CLEARANCE_SEQ.NEXTVAL,
                                     RPM_CLEARANCE_DISPLAY_SEQ.NEXTVAL,
                                     thread_price_event_type,
                                     thread_rank,
                                     thread_merch_node_type,
                                     thread_item,
                                     thread_zone_node_type,
                                     thread_zone_node_id)
            select RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE   state,
                   L_reason_code                           reason_code,
                   w.item                                  item,
                   w.zone_id                               zone_id,
                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE       zone_node_type,
                   w.effective_date                        effective_date,
                   w.new_out_of_stock_date                 out_of_stock_date,
                   w.new_reset_date                        reset_date,
                   RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                   w.new_retail                            change_amount,
                   igen.currency                           change_currency,
                   w.new_retail_uom                        change_selling_uom,
                   0                                       vendor_funded_ind,
                   LP_vdate                                create_date,
                   I_user_id                               create_id,
                   RPM_CONSTANTS.PE_TYPE_CLEARANCE         thread_price_event_type,
                   1                                       thread_rank,
                   RPM_CONSTANTS.ITEM_LEVEL_ITEM           thread_merch_node_type,
                   w.item                                  thread_item,
                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE       thread_zone_node_type,
                   w.zone_id                               thread_zone_node_id,
                   w.worksheet_status_id,
                   ROW_NUMBER() OVER (PARTITION BY w.worksheet_status_id,
                                                   w.item,
                                                   w.zone_id
                                          ORDER BY NULL) pe_rownum
              from rpm_worksheet_item_gen_ws igen,
                   rpm_worksheet_item_loc_gen_ws w,
                   rpm_worksheet_status ws,
                   table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids
             where igen.workspace_id        = I_workspace_id
               and (   (    I_action = 'S'
                        and igen.action_flag = TAKE
                        and igen.state IN (WS_STATE_INT_IN_PROGRESS,
                                           WS_STATE_INT_SUBMITTED))
                    or (    I_action = 'A'
                        and igen.action_flag = TAKE
                        and igen.state IN (WS_STATE_INT_IN_PROGRESS,
                                           WS_STATE_INT_SUBMITTED)))
               and w.workspace_id           = igen.workspace_id
               and w.worksheet_item_data_id = igen.worksheet_item_data_id
               and w.new_clear_ind          = 1
               and w.worksheet_status_id    = ws.worksheet_status_id
               and ws.worksheet_status_id   = igen.worksheet_status_id
               and ws.worksheet_status_id   = VALUE(ids);

   insert into rpm_bulk_cc_pe_item
      (bulk_cc_pe_id,
       price_event_id,
       itemloc_id,
       dept,
       class,
       subclass,
       item,
       diff_id,
       merch_level_type,
       link_code,
       thread_number,
       pe_merch_level)
   select distinct IO_clr_bulk_cc_pe_id,
          clr.clearance_id,
          1,
          im.dept,
          im.class,
          im.subclass,
          w.item,
          case
             when im.item_parent is NULL and
                  im.item_level = im.tran_level then
                NULL
             else
                im.diff_1
          end,
          RPM_CONSTANTS.ITEM_MERCH_TYPE,
          w.link_code,
          1,
          RPM_CONSTANTS.ITEM_MERCH_TYPE
     from rpm_worksheet_item_loc_gen_ws w,
          rpm_bulk_cc_wksht_clr clr,
          item_master im
    where clr.bulk_cc_pe_id       = IO_clr_bulk_cc_pe_id
      and clr.worksheet_status_id = w.worksheet_status_id
      and clr.item                = w.item
      and clr.zone_id             = w.zone_id
      and clr.item                = w.item
      and im.item                 = clr.item;

   insert into rpm_bulk_cc_pe_location
      (bulk_cc_pe_id,
       price_event_id,
       itemloc_id,
       location,
       zone_node_type,
       zone_id)
   -- Locations for the zone id
   select distinct IO_clr_bulk_cc_pe_id,
          clr.clearance_id,
          1 itemloc_id,
          w.location,
          w.loc_type,
          case
             when rmrde.regular_zone_group = rz.zone_group_id then
                w.zone_id
             else
                NULL
          end
     from rpm_worksheet_item_loc_gen_ws w,
          rpm_bulk_cc_wksht_clr clr,
          item_master im,
          rpm_zone rz,
          rpm_merch_retail_def_expl rmrde
    where clr.bulk_cc_pe_id       = IO_clr_bulk_cc_pe_id
      and clr.worksheet_status_id = w.worksheet_status_id
      and clr.item                = w.item
      and clr.zone_id             = w.zone_id
      and clr.item                = w.item
      and im.item                 = w.item
      and rz.zone_id              = w.zone_id
      and rmrde.dept              = im.dept
      and rmrde.class             = im.class
      and rmrde.subclass          = im.subclass
   union all
   -- zone id
   select distinct
          IO_clr_bulk_cc_pe_id              bulk_cc_pe_id,
          clr.clearance_id                  price_event_id,
          1                                 itemloc_id,
          w.zone_id                         location,
          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
          NULL                              zone_id
     from rpm_worksheet_item_loc_gen_ws w,
          rpm_bulk_cc_wksht_clr clr,
          item_master im,
          rpm_zone rz,
          rpm_merch_retail_def_expl rmrde
    where clr.bulk_cc_pe_id        = IO_clr_bulk_cc_pe_id
      and clr.worksheet_status_id  = w.worksheet_status_id
      and clr.item                 = w.item
      and im.item                  = w.item
      and clr.zone_id              = w.zone_id
      and rz.zone_id               = w.zone_id
      and w.dept                   = rmrde.dept
      and rmrde.dept               = im.dept
      and rmrde.class              = im.class
      and rmrde.subclass           = im.subclass
      and rmrde.regular_zone_group = rz.zone_group_id;

   --clear out the ID if no clearances are generated
   open C_PE;
   fetch C_PE into L_count;
   close C_PE;

   if L_count = 0 then

      delete
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = IO_clr_bulk_cc_pe_id;

      IO_clr_bulk_cc_pe_id := NULL;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_CLEARANCES;

--------------------------------------------------------

FUNCTION PROPOSE_UOM_PKG_CALL(O_error_msg    IN OUT  VARCHAR2,
                              I_workspace_id IN      rpm_worksheet_item_gen_ws.workspace_id%TYPE)
RETURN BOOLEAN IS

   TYPE t_item                         IS TABLE OF rpm_worksheet_item_data.item%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_proposed_retail_std          IS TABLE OF rpm_worksheet_item_loc_data.proposed_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_worksheet_item_data_id       IS TABLE OF rpm_worksheet_item_loc_data.worksheet_item_data_id%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_worksheet_item_loc_data_id   IS TABLE OF rpm_worksheet_item_loc_data.worksheet_item_loc_data_id%TYPE INDEX BY BINARY_INTEGER;

   a_item                          t_item;
   a_proposed_retail_std           t_proposed_retail_std;
   a_worksheet_item_data_id        t_worksheet_item_data_id;
   a_worksheet_item_loc_data_id    t_worksheet_item_loc_data_id;

   a_update_size                       BINARY_INTEGER := 0;
   update_count                        BINARY_INTEGER := 0;

   cursor C_ITEM_LOC_CONV is
      select wd.worksheet_item_data_id,
             wd.worksheet_item_loc_data_id,
             gtt.item,
             iwd.standard_uom,
             wd.il_primary_supp,
             wd.il_primary_cntry,
             --
             wd.proposed_retail,
             wd.proposed_retail_uom,
             wd.proposed_retail_std
        from rpm_area_diff_ws_approve_gtt gtt,
             rpm_worksheet_item_gen_ws iwd,
             rpm_worksheet_item_loc_gen_ws wd
       where gtt.worksheet_item_data_id = iwd.worksheet_item_data_id
         and gtt.worksheet_item_data_id = wd.worksheet_item_data_id
         and gtt.worksheet_item_loc_data_id = wd.worksheet_item_loc_data_id
         and wd.proposed_retail    is not null
         and iwd.standard_uom     != wd.proposed_retail_uom
         and wd.workspace_id       = I_workspace_id
         and iwd.workspace_id      = I_workspace_id;

BEGIN

/*is: getting called for each loc in the zone */

   FOR rec in C_ITEM_LOC_CONV LOOP

      a_update_size := a_update_size + 1;

      a_item(a_update_size) := rec.item;
      a_proposed_retail_std(a_update_size) := null;
      a_worksheet_item_data_id(a_update_size) := rec.worksheet_item_data_id;
      a_worksheet_item_loc_data_id(a_update_size) := rec.worksheet_item_loc_data_id;

      if UOM_SQL.CONVERT(O_error_msg,
                         a_proposed_retail_std(a_update_size),
                         rec.proposed_retail_uom,
                         rec.proposed_retail,
                         rec.standard_uom,
                         rec.item,
                         rec.il_primary_supp,
                         rec.il_primary_cntry) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   if a_update_size > 0 then

      FORALL update_count IN 1..a_update_size
         update rpm_worksheet_item_loc_gen_ws set
                proposed_retail_std = a_proposed_retail_std(update_count),
                new_retail_std      = a_proposed_retail_std(update_count)
          where worksheet_item_data_id = a_worksheet_item_data_id(update_count)
            and worksheet_item_loc_data_id = a_worksheet_item_loc_data_id(update_count)
            and workspace_id      = I_workspace_id;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.PROPOSE_UOM_PKG_CALL',
                                        to_char(SQLCODE));
      return FALSE;

END PROPOSE_UOM_PKG_CALL;

--------------------------------------------------------

FUNCTION AREA_DIFF_PROPOSE(O_error_msg                  OUT VARCHAR2,
                           O_secondary_worksheet_ids    OUT OBJ_NUMERIC_ID_TABLE,
                           I_worksheet_status_ids    IN     OBJ_NUMERIC_ID_TABLE,
                           I_workspace_id            IN     rpm_worksheet_item_gen_ws.workspace_id%TYPE)
RETURN BOOLEAN IS

L_dynamic_area_diff_ind rpm_system_options.dynamic_area_diff_ind%TYPE :=null;

cursor c_secondary_worksheets is
   select distinct worksheet_status_id
     from rpm_area_diff_ws_approve_gtt
    where auto_approve_ind = 1;

BEGIN

   --propose retail for non-dynamic area diffs
   if RPM_SYSTEM_OPTIONS_SQL.GET_DYNAMIC_AREA_DIFF_IND(L_dynamic_area_diff_ind,
                                                       O_error_msg) = FALSE then
      return FALSE;
   end if;

   delete from rpm_area_diff_ws_approve_gtt;

   insert into rpm_area_diff_ws_approve_gtt
   (
      worksheet_status_id,
      worksheet_item_data_id,
      worksheet_item_loc_data_id,
      item,
      link_code,
      location,
      zone_id,
      currency_rtl_dec,
      basis_zl_regular_retail,
      basis_zl_regular_retail_uom,
      comp_mkt_basket_code,
      proposed_retail,
      proposed_retail_uom,
      area_diff_id,
      auto_approve_ind,
      price_guide_id
   )  --With Link codes
   select second_ws.worksheet_status_id,
          second_w.worksheet_item_data_id,
          second_wd.worksheet_item_loc_data_id,
          second_wd.item,
          second_wd.link_code,
          second_wd.location,
          second_wd.zone_id,
          c.currency_rtl_dec,
          second_w.basis_zl_regular_retail,
          second_w.basis_zl_regular_retail_uom,
          second_w.comp_mkt_basket_code,
          primary_changes.change_amount,
          primary_changes.change_selling_uom,
          second_w.area_diff_id,
          ad.auto_approve_ind,
          ad.price_guide_id
     from rpm_worksheet_status second_ws,
          rpm_worksheet_item_gen_ws second_w,
          rpm_worksheet_item_loc_gen_ws second_wd,
          rpm_bulk_cc_wksht_pc primary_changes,
          rpm_area_diff ad,
          rpm_codes rc,
          currencies c
    where second_ws.area_diff_prim_ws_id in(select * from table(cast(I_worksheet_status_ids as obj_numeric_id_table)))
      and second_ws.worksheet_status_id  = second_w.worksheet_status_id
      and second_ws.dept                 = second_w.dept
      and second_w.workspace_id          = I_workspace_id
      and second_wd.workspace_id         = second_w.workspace_id
      and second_wd.worksheet_item_data_id = second_w.worksheet_item_data_id
      and second_ws.area_diff_prim_ws_id = primary_changes.worksheet_status_id
      and nvl(second_wd.link_code, second_wd.item)   = nvl(rc.code, primary_changes.item)
      and 'P'                            = primary_changes.primary_secondary_ind
      and ((L_dynamic_area_diff_ind = 1 and second_w.state in(1,2,3,4))
           or
           (L_dynamic_area_diff_ind = 0 and second_w.state in(0)))
      and second_w.area_diff_id         = ad.area_diff_id
      and second_w.currency             = c.currency_code
      and primary_changes.link_code = rc.code_id
      and rc.code_type = 1
    union all   --Without Link codes
   select second_ws.worksheet_status_id,
          second_w.worksheet_item_data_id,
          second_wd.worksheet_item_loc_data_id,
          second_wd.item,
          second_wd.link_code,
          second_wd.location,
          second_wd.zone_id,
          c.currency_rtl_dec,
          second_w.basis_zl_regular_retail,
          second_w.basis_zl_regular_retail_uom,
          second_w.comp_mkt_basket_code,
          primary_changes.change_amount,
          primary_changes.change_selling_uom,
          second_w.area_diff_id,
          ad.auto_approve_ind,
          ad.price_guide_id
     from rpm_worksheet_status second_ws,
          rpm_worksheet_item_gen_ws second_w,
          rpm_worksheet_item_loc_gen_ws second_wd,
          rpm_bulk_cc_wksht_pc primary_changes,
          rpm_area_diff ad,
          currencies c
    where second_ws.area_diff_prim_ws_id in(select * from table(cast(I_worksheet_status_ids as obj_numeric_id_table)))
      and second_ws.worksheet_status_id  = second_w.worksheet_status_id
      and second_ws.dept                 = second_w.dept
      and second_w.workspace_id          = I_workspace_id
      and second_wd.workspace_id         = second_w.workspace_id
      and second_wd.worksheet_item_data_id = second_w.worksheet_item_data_id
      and second_ws.area_diff_prim_ws_id = primary_changes.worksheet_status_id
      and second_wd.item                 = primary_changes.item
      and 'P'                            = primary_changes.primary_secondary_ind
      and ((L_dynamic_area_diff_ind = 1 and second_w.state in(1,2,3,4))
           or
           (L_dynamic_area_diff_ind = 0 and second_w.state in(0)))
      and second_w.area_diff_id          = ad.area_diff_id
      and second_ws.currency             = c.currency_code;

   --not dynamic, propose retial for 2nd area --assumes that the zl and propose retails are constant across the zone
   if L_dynamic_area_diff_ind = 0 then

      delete rpm_me_proposed_gtt;

      insert into rpm_me_proposed_gtt
           (item,
            location,
            proposed_retail,
            proposed_retail_uom,
            proposed_clear_ind,
            auto_approved_ind)
       select pg.item,
              pg.location,
              nvl(pgi.new_price_val, pg.proposed_retail) proposed_retail,
              pg.proposed_retail_uom,
              pg.proposed_clear_ind,
              pg.auto_approve_ind
         from (
         select distinct ad.item,
                ad.location,
                first_value(round(pr, currency_rtl_dec)) over (partition by ad.item, ad.location order by pr asc) proposed_retail,
                first_value(pru) over (partition by ad.item, ad.location order by pr asc) proposed_retail_uom,
                0 proposed_clear_ind,
                ad.price_guide_id,
                ad.auto_approve_ind
           from (   select gtt.item,
                           gtt.location,
                           gtt.currency_rtl_dec,
                           --
                           decode(rad.percent_apply_type,
                                  0, gtt.proposed_retail * (1 + rad.percent / 100),
                                  1, gtt.proposed_retail * (1 - rad.percent / 100),
                                  2, gtt.proposed_retail) pr,
                           --
                           gtt.proposed_retail_uom pru,
                           gtt.worksheet_item_data_id,
                           gtt.worksheet_item_loc_data_id,
                           gtt.price_guide_id,
                           gtt.auto_approve_ind
                      from rpm_area_diff_ws_approve_gtt gtt,
                           rpm_area_diff rad
                     where rad.area_diff_id = gtt.area_diff_id
                 union all
                    select gtt.item,
                           gtt.location,
                           gtt.currency_rtl_dec,
                           decode(rad.compete_type,
                                  RPM_EXT_SQL.COMP_PRICE_ABOVE, gtt.proposed_retail * (1 + rad.percent / 100),
                                  RPM_EXT_SQL.COMP_MATCH, gtt.proposed_retail,
                                  RPM_EXT_SQL.COMP_PRICE_BELOW, gtt.proposed_retail * (1 - rad.percent / 100)) pr,
                           iwd.basis_zl_regular_retail_uom pru,
                           wd.worksheet_item_data_id,
                           wd.worksheet_item_loc_data_id,
                           gtt.price_guide_id,
                           gtt.auto_approve_ind
                      from rpm_worksheet_item_gen_ws iwd,
                           rpm_worksheet_item_loc_gen_ws wd,
                           rpm_area_diff_ws_approve_gtt gtt,
                            (select decode(reg.compete_type, RPM_EXT_SQL.COMP_PRICE_CODE,
                                           code.percent, reg.percent) percent,
                                    decode(reg.compete_type, RPM_EXT_SQL.COMP_PRICE_CODE,
                                           code.from_percent, reg.from_percent) from_percent,
                                    decode(reg.compete_type, RPM_EXT_SQL.COMP_PRICE_CODE,
                                           code.to_percent, reg.to_percent) to_percent,
                                    decode(reg.compete_type, RPM_EXT_SQL.COMP_PRICE_CODE,
                                           code.compete_type, reg.compete_type) compete_type,
                               radm.mkt_basket_code,
                               reg.area_diff_id
                               from rpm_area_diff_comp reg,
                                    rpm_area_diff_mbc radm,
                               rpm_area_diff_comp code
                              where reg.area_diff_comp_id = radm.area_diff_comp_id (+)
                                and radm.area_diff_mbc_id = code.area_diff_mbc_id(+)
                                and reg.area_diff_mbc_id  is null) rad
                      where gtt.worksheet_item_data_id     = wd.worksheet_item_data_id
                        and gtt.worksheet_item_loc_data_id = wd.worksheet_item_loc_data_id
                        and wd.workspace_id                = I_workspace_id
                        and iwd.worksheet_item_data_id     = wd.worksheet_item_data_id
                        and iwd.workspace_id                = I_workspace_id
                        and gtt.area_diff_id           = rad.area_diff_id
                        and (rad.to_percent is null
                             or (rad.compete_type = RPM_EXT_SQL.COMP_PRICE_ABOVE
                                 and not iwd.basis_zl_regular_retail between
                                     iwd.primary_comp_retail * ( 1 + rad.from_percent / 100) and
                                     iwd.primary_comp_retail * ( 1 + rad.to_percent / 100)
                                 )
                             or (rad.compete_type = RPM_EXT_SQL.COMP_MATCH)
                             or (rad.compete_type = RPM_EXT_SQL.COMP_PRICE_BELOW
                                 and not iwd.basis_zl_regular_retail between
                                     iwd.primary_comp_retail * ( 1 - rad.from_percent / 100) and
                                     iwd.primary_comp_retail * ( 1 - rad.to_percent / 100)
                                )
                             )
                         and iwd.primary_comp_retail             is not null
                         and nvl(iwd.comp_mkt_basket_code, -999) = nvl(rad.mkt_basket_code, nvl(iwd.comp_mkt_basket_code, -999))
                                  ) ad) pg,
            rpm_price_guide_interval pgi
      where pgi.price_guide_id(+)  = pg.price_guide_id
        and pgi.from_price_val(+) <= pg.proposed_retail
        and pgi.to_price_val(+)   >= pg.proposed_retail;

      merge into rpm_worksheet_item_loc_gen_ws mergeinto
      using(
       select item,
              location,
              proposed_retail,
              proposed_retail_uom,
              proposed_clear_ind,
              auto_approved_ind
         from rpm_me_proposed_gtt) use
      on (    mergeinto.item         = use.item
          and mergeinto.location     = use.location
          and mergeinto.workspace_id = I_workspace_id)
      when matched then
      update
         set proposed_retail     = use.proposed_retail,
             proposed_retail_uom = use.proposed_retail_uom,
             proposed_retail_std = use.proposed_retail,
             new_retail          = use.proposed_retail,
             new_retail_uom      = use.proposed_retail_uom,
             new_retail_std      = use.proposed_retail;

      merge into rpm_worksheet_item_gen_ws mergeinto
      using(
       select distinct item,
              auto_approved_ind
         from rpm_me_proposed_gtt) use
      on (    mergeinto.item         = use.item
          and mergeinto.workspace_id = I_workspace_id)
      when matched then
      update
         set state               = decode(use.auto_approved_ind, 1, WS_STATE_INT_IN_PROGRESS, WS_STATE_INT_NEW),
             action_flag         = decode(use.auto_approved_ind, 1, TAKE, UNDECIDED);

      if PROPOSE_UOM_PKG_CALL(O_error_msg,
                              I_workspace_id) = FALSE then
         return FALSE;
      end if;

   else

      update rpm_worksheet_item_gen_ws
         set action_flag = TAKE,
             state       = WS_STATE_INT_IN_PROGRESS
       where worksheet_item_data_id in (select distinct worksheet_item_data_id
                                          from rpm_area_diff_ws_approve_gtt
                                         where auto_approve_ind = 1)
         and workspace_id = I_workspace_id;

      update rpm_worksheet_item_loc_gen_ws
         set action_flag = TAKE
       where (worksheet_item_data_id, worksheet_item_loc_data_id)  in (select worksheet_item_data_id,
                                                                              worksheet_item_loc_data_id
                                                                         from rpm_area_diff_ws_approve_gtt
                                                                        where auto_approve_ind = 1)
         and workspace_id = I_workspace_id;

   end if;

   if PUSH_WORKSHEET_NOT_AUTOAPPV(O_error_msg,
                                  I_workspace_id) = FALSE then
      return FALSE;
   end if;

   open c_secondary_worksheets;
   fetch c_secondary_worksheets bulk collect into O_secondary_worksheet_ids;
   close c_secondary_worksheets;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.AREA_DIFF_PROPOSE',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END AREA_DIFF_PROPOSE;

-------------------------------------------------------------------

FUNCTION FINISH_WORKSHEETS(O_error_msg               OUT VARCHAR2,
                           I_worksheet_status_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_return                  BINARY_INTEGER;

   L_state_bitmap            RPM_WORKSHEET_STATUS.STATE_BITMAP%TYPE := null;
   L_stock_on_hand           RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE := null;
   L_price_change_amount     RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE := null;
   L_sales_amount            RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE := null;
   L_sales_amount_ex_vat     RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE := null;
   L_sales_cost              RPM_WORKSHEET_STATUS.SALES_COST%TYPE := null;
   L_sales_change_amount     RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE := null;
   L_item_count              RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE := null;

   /* this will potentially do extra calls to rollup for non-auto approve secondary areas */
   cursor c_worksheet_status is
      select ws.worksheet_status_id,
             max(nvl(wild.conflict_boolean,0)) conflict_boolean,
             max(nvl(wid.constraint_boolean,0)) constraint_boolean
        from rpm_worksheet_status ws,
             rpm_worksheet_item_data wid,
             rpm_worksheet_item_loc_data wild
       where ws.worksheet_status_id in(select * from table(cast(I_worksheet_status_ids as obj_numeric_id_table)))
         and ws.worksheet_status_id = wid.worksheet_status_id
         and wild.worksheet_item_data_id = wid.worksheet_item_data_id
       group by ws.worksheet_status_id
      union all
      select ws.worksheet_status_id,
             max(nvl(wild.conflict_boolean,0)) conflict_boolean,
             max(nvl(wid.constraint_boolean,0)) constraint_boolean
        from rpm_worksheet_status ws,
             rpm_worksheet_item_data wid,
             rpm_worksheet_item_loc_data wild
       where ws.area_diff_prim_ws_id in(select * from table(cast(I_worksheet_status_ids as obj_numeric_id_table)))
         and ws.worksheet_status_id = wid.worksheet_status_id
         and wild.worksheet_item_data_id = wid.worksheet_item_data_id
       group by ws.worksheet_status_id;

BEGIN

   FOR rec in c_worksheet_status LOOP

      L_return :=  RPM_EXT_SQL.WORKSHEET_ROLLUP(O_error_msg,
                                                rec.worksheet_status_id,
                                                L_state_bitmap,
                                                L_stock_on_hand,
                                                L_price_change_amount,
                                                L_sales_amount,
                                                L_sales_amount_ex_vat,
                                                L_sales_cost,
                                                L_sales_change_amount,
                                                L_item_count);
      if L_return = 0 then
         return FALSE;
      end if;

      update rpm_worksheet_status
         set state_bitmap        = L_state_bitmap,
             stock_on_hand       = L_stock_on_hand,
             price_change_amount = L_price_change_amount,
             sales_amount        = L_sales_amount,
             sales_amount_ex_vat = L_sales_amount_ex_vat,
             sales_cost          = L_sales_cost,
             sales_change_amount = L_sales_change_amount,
             item_count          = L_item_count,
             conflict_ind        = rec.conflict_boolean,
             constraint_ind      = rec.constraint_boolean,
             ws_lock_owner       = null,
             ws_lock_date        = null,
             ws_lock_workspace_id= null
       where worksheet_status_id = rec.worksheet_status_id;

   END LOOP;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.FINISH_WORKSHEETS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END FINISH_WORKSHEETS;

--------------------------------------------------------

--------------------------------------------------------
--PUBLIC PROCEDURES
--------------------------------------------------------

FUNCTION GET_WORKSHEETS_TO_APPROVE(O_error_msg            OUT VARCHAR2,
                                   O_worksheet_status_ids OUT OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_vdate DATE := get_vdate;

   cursor C_GET_WORKSHEETS is
      select distinct rws.worksheet_status_id
        from rpm_calendar_period rcp,
             rpm_strategy rs,
             rpm_strategy_maint_margin rmm,
             rpm_worksheet_item_data rwd,
             rpm_worksheet_status rws
       where rcp.end_date             = get_vdate
         and rcp.calendar_id          = rs.current_calendar_id
         and rs.copied_strategy_id    is not null
         and rs.strategy_id           = rmm.strategy_id
         and rmm.auto_approve         = 1
         and rwd.proposed_strategy_id = rmm.strategy_id
         and rwd.dept                 = rs.dept
         and rwd.worksheet_status_id  = rws.worksheet_status_id;

BEGIN

   open C_GET_WORKSHEETS;
   fetch C_GET_WORKSHEETS bulk collect into O_worksheet_status_ids;
   close C_GET_WORKSHEETS;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.GET_WORKSHEETS_TO_APPROVE',
                                        TO_CHAR(SQLCODE));
      return 0;
END GET_WORKSHEETS_TO_APPROVE;

--------------------------------------------------------

FUNCTION SPLIT_INPUT_PRIM_SEC(O_error_msg                OUT VARCHAR2,
                              I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE,
                              O_primary_worksheet_ids    OUT OBJ_NUMERIC_ID_TABLE,
                              O_secondary_worksheet_ids  OUT OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

  cursor c_primary is
    select value(ids) wid
      from table(cast(I_worksheet_status_ids as obj_numeric_id_table)) ids
    MINUS
    select ws.worksheet_status_id
      from table(cast(I_worksheet_status_ids as obj_numeric_id_table)) prim_ids,
           table(cast(I_worksheet_status_ids as obj_numeric_id_table)) sec_ids,
           rpm_worksheet_status ws
     where ws.worksheet_status_id  = value(prim_ids)
       and ws.area_diff_prim_ws_id = value(sec_ids);

   cursor c_secondary is
    select ws.worksheet_status_id
      from table(cast(I_worksheet_status_ids as obj_numeric_id_table)) prim_ids,
           table(cast(I_worksheet_status_ids as obj_numeric_id_table)) sec_ids,
           rpm_worksheet_status ws
     where ws.worksheet_status_id  = value(prim_ids)
       and ws.area_diff_prim_ws_id = value(sec_ids);

BEGIN

   open c_primary;
   fetch c_primary bulk collect into O_primary_worksheet_ids;
   close c_primary;

   open c_secondary;
   fetch c_secondary bulk collect into O_secondary_worksheet_ids;
   close c_secondary;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.SPLIT_INPUT_PRIM_SEC',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END SPLIT_INPUT_PRIM_SEC;

--------------------------------------------------------
FUNCTION GENERATE(O_error_msg                 OUT VARCHAR2,
                  O_pc_bulk_cc_pe_id          OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                  O_clr_bulk_cc_pe_id         OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                  I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                  I_cc_request_group_id    IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE,
                  I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                  I_action                 IN     VARCHAR2,
                  I_user_id                IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                  I_auto_approve_ind       IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_GENERATE_WS_PRICE_EVENTS.GENERATE';

   L_primary_worksheet_ids      OBJ_NUMERIC_ID_TABLE := NULL;
   L_secondary_worksheet_ids    OBJ_NUMERIC_ID_TABLE := NULL;
   L_auto_approve_secondary_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_combined_secondary_ids     OBJ_NUMERIC_ID_TABLE := NULL;

   L_pc_bulk_cc_pe_id  RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE := RPM_BULK_CC_PE_SEQ.NEXTVAL;
   L_sec_bulk_cc_pe_id RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE := RPM_BULK_CC_PE_SEQ.NEXTVAL;
   L_clr_bulk_cc_pe_id RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   L_worksheet_workspace_id RPM_WORKSHEET_ITEM_GEN_WS.WORKSPACE_ID%TYPE := RPM_WORKSHEET_WORKSPACE_SEQ.NEXTVAL;

  cursor C_COMBINE is
     select VALUE(ids) wid
       from table(cast(L_secondary_worksheet_ids as OBJ_NUMERIC_ID_TABLE)) ids
     union all
     select VALUE(ids) wid
       from table(cast(L_auto_approve_secondary_ids as OBJ_NUMERIC_ID_TABLE)) ids;

BEGIN

   if RPM_SYSTEM_OPTIONS_SQL.RESET_GLOBALS(O_error_msg) = FALSE then
      return 0;
   end if;

   if SPLIT_INPUT_PRIM_SEC(O_error_msg,
                           I_worksheet_status_ids,
                           L_primary_worksheet_ids,
                           L_secondary_worksheet_ids) = FALSE THEN
      return 0;
   end if;

   if POPULATE_WORKSPACE(O_error_msg,
                         I_worksheet_status_ids,
                         L_worksheet_workspace_id) = FALSE THEN
      return 0;
   end if;

   --create the price changes -- only send primary areas from input
   if CREATE_PRICE_CHANGES(O_error_msg,
                           L_pc_bulk_cc_pe_id,
                           I_thread_processor_class,
                           I_cc_request_group_id,
                           L_primary_worksheet_ids,
                           L_worksheet_workspace_id,
                           L_sec_bulk_cc_pe_id,
                           'P', --I_primary_ind,
                           I_action,
                           I_user_id,
                           I_auto_approve_ind) = FALSE then
      return 0;
   end if;

   if I_action = 'A' and
      L_pc_bulk_cc_pe_id is NOT NULL then

      --propose retails for secondary worksheets if dynamic
      -- area diff option is not being used -- only send primary areas from input
      if AREA_DIFF_PROPOSE(O_error_msg,
                           L_auto_approve_secondary_ids,
                           L_primary_worksheet_ids,
                           L_worksheet_workspace_id) = FALSE then
         return 0;
      end if;

      --combine input secondaries with secondaries returned from AREA_DIFF_PROPOSE
      open C_COMBINE;
      fetch C_COMBINE BULK COLLECT into L_combined_secondary_ids;
      close C_COMBINE;

      --create price changes for auto-approve secondary worksheets
      if CREATE_PRICE_CHANGES(O_error_msg,
                              L_sec_bulk_cc_pe_id,
                              I_thread_processor_class,
                              I_cc_request_group_id,
                              L_combined_secondary_ids,
                              L_worksheet_workspace_id,
                              NULL,
                              'S', --I_primary_ind,
                              I_action,
                              I_user_id,
                              I_auto_approve_ind) = FALSE then
         return 0;
      end if;

   end if;

   --create the clearances --use unfiltered worksheet status list
   if CREATE_CLEARANCES(O_error_msg,
                        L_clr_bulk_cc_pe_id,
                        I_thread_processor_class,
                        I_cc_request_group_id,
                        I_worksheet_status_ids,
                        L_worksheet_workspace_id,
                        I_action,
                        I_user_id) = FALSE then
      return 0;
   end if;

   O_pc_bulk_cc_pe_id  := L_pc_bulk_cc_pe_id;
   O_clr_bulk_cc_pe_id := L_clr_bulk_cc_pe_id;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END GENERATE;

--------------------------------------------------------

FUNCTION BEGIN_THREAD_PROCESS(O_error_msg               OUT VARCHAR2,
                              I_bulk_cc_pe_id        IN     rpm_bulk_cc_pe.bulk_cc_pe_id%TYPE,
                              I_parent_thread_number IN     rpm_bulk_cc_pe_thread.parent_thread_number%TYPE,
                              I_thread_number        IN     rpm_bulk_cc_pe_thread.thread_number%TYPE)
RETURN NUMBER IS

BEGIN

   if PUSH_PRICE_CHANGES(O_error_msg,
                         I_bulk_cc_pe_id,
                         I_parent_thread_number,
                         I_thread_number) = FALSE then
      return 0;
   end if;

   if PUSH_CLEARANCES(O_error_msg,
                      I_bulk_cc_pe_id,
                      I_parent_thread_number,
                      I_thread_number) = FALSE then
      return 0;
   end if;

   if PUSH_WORKSHEET(O_error_msg,
                     I_bulk_cc_pe_id,
                     I_parent_thread_number,
                     I_thread_number) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.BEGIN_THREAD_PROCESS',
                                        TO_CHAR(SQLCODE));
      return 0;
END BEGIN_THREAD_PROCESS;

--------------------------------------------------------

FUNCTION CLEAN_UP(O_error_msg              OUT VARCHAR2,
                  O_group_complete_ind     OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                  I_cc_request_group_id IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_GENERATE_WS_PRICE_EVENTS.CLEAN_UP';

   L_worksheet_ids    OBJ_NUMERIC_ID_TABLE := NULL;
   L_count            NUMBER               := 0;

   cursor C_GET_WS_STATUS is
      select distinct pc.worksheet_status_id
        from rpm_bulk_cc_pe_thread thread,
             rpm_bulk_cc_wksht_pc pc
       where thread.bulk_cc_pe_id  IN (select bulk_cc_pe_id
                                         from rpm_bulk_cc_pe
                                        where cc_request_group_id = I_cc_request_group_id)
         and thread.price_event_id = pc.price_change_id
      union all
      select distinct clr.worksheet_status_id
        from rpm_bulk_cc_pe_thread thread,
             rpm_bulk_cc_wksht_clr clr
       where thread.bulk_cc_pe_id  IN (select bulk_cc_pe_id
                                         from rpm_bulk_cc_pe
                                        where cc_request_group_id = I_cc_request_group_id)
         and thread.price_event_id = clr.clearance_id;

   cursor C_GROUP_COMPLETE is
      select COUNT(*)
        from rpm_bulk_cc_pe
       where cc_request_group_id = I_cc_request_group_id
         and status              = 'I';

BEGIN

   open C_GROUP_COMPLETE;
   fetch C_GROUP_COMPLETE into L_count;
   close C_GROUP_COMPLETE;

   --return when this group is entirely done
   if L_count = 0 then

      open C_GET_WS_STATUS;
      fetch C_GET_WS_STATUS BULK COLLECT into L_worksheet_ids;
      close C_GET_WS_STATUS;

      if FINISH_WORKSHEETS(O_error_msg,
                           L_worksheet_ids) = FALSE then
         return FALSE;
      end if;

      insert into rpm_conflict_check_result
         (conflict_check_result_id,
          event_type,
          result,
          results_date,
          user_id,
          event_id,
          promo_comp_id,
          promo_comp_detail_id,
          merch_type,
          dept,
          class,
          subclass,
          zone_id,
          error_string)
      select /*+ ORDERED */
             RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
             3,                 -- BackgroundConflictCheckEventTypeCode.WORKSHEET_RESULT
             case
                when rws.conflict_ind = 1 then
                   1
                else
                   0
             end,
             GET_VDATE,
             t.user_name,
             rws.worksheet_status_id,
             NULL,
             NULL,
             NULL,
             rws.dept,
             rws.class,
             rws.subclass,
             rws.zone_id,
             NULL
        from (select /*+ CARDINALITY (ids 1000) */
                     distinct
                     rpe.user_name,
                     wsiloc.worksheet_status_id
                from rpm_bulk_cc_pe rpe,
                     rpm_worksheet_item_loc_gen_ws wsiloc,
                     table(cast(L_worksheet_ids as obj_numeric_id_table)) ids
               where rpe.cc_request_group_id = I_cc_request_group_id
                 and wsiloc.workspace_id        = rpe.worksheet_workspace_id
                 and wsiloc.worksheet_status_id = value(ids)) t,
             rpm_worksheet_status rws
       where rws.worksheet_status_id = t.worksheet_status_id;

      delete from rpm_bulk_cc_wksht_pc
       where bulk_cc_pe_id IN (select inner.bulk_cc_pe_id
                                 from rpm_bulk_cc_pe inner
                                where inner.cc_request_group_id = I_cc_request_group_id);

      delete from rpm_bulk_cc_wksht_clr
       where bulk_cc_pe_id IN (select inner.bulk_cc_pe_id
                                 from rpm_bulk_cc_pe inner
                                where inner.cc_request_group_id = I_cc_request_group_id);

      delete from rpm_worksheet_item_loc_gen_ws
       where workspace_id IN (select worksheet_workspace_id
                                from rpm_bulk_cc_pe
                               where cc_request_group_id = I_cc_request_group_id);

      delete from rpm_worksheet_item_gen_ws
       where workspace_id IN (select worksheet_workspace_id
                                from rpm_bulk_cc_pe
                               where cc_request_group_id = I_cc_request_group_id);

      delete from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id IN (select inner.bulk_cc_pe_id
                                 from rpm_bulk_cc_pe inner
                                where inner.cc_request_group_id = I_cc_request_group_id);

      delete from rpm_bulk_cc_pe_location
       where bulk_cc_pe_id IN (select inner.bulk_cc_pe_id
                                 from rpm_bulk_cc_pe inner
                                where inner.cc_request_group_id = I_cc_request_group_id);

      delete from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id IN (select inner.bulk_cc_pe_id
                                 from rpm_bulk_cc_pe inner
                                where inner.cc_request_group_id = I_cc_request_group_id);

      delete from rpm_bulk_cc_pe
       where bulk_cc_pe_id IN (select inner.bulk_cc_pe_id
                                 from rpm_bulk_cc_pe inner
                                where inner.cc_request_group_id = I_cc_request_group_id);

      O_group_complete_ind := 1;

   else

      O_group_complete_ind := 0;

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END CLEAN_UP;

--------------------------------------------------------

FUNCTION CLEAN_UP_WORKSHEET_BULK_CC(O_error_msg              OUT VARCHAR2,
                                    O_group_complete_ind     OUT NUMBER,
                                    I_bulk_cc_pe_id      IN      rpm_bulk_cc_pe.bulk_cc_pe_id%TYPE)
RETURN NUMBER IS

   L_worksheet_ids    OBJ_NUMERIC_ID_TABLE := null;
   L_cc_request_group_id  rpm_bulk_cc_pe.cc_request_group_id%TYPE := null;
   L_count            NUMBER := 0;

   cursor c_lock_group is
      select 'x'
        from rpm_bulk_cc_pe
       where cc_request_group_id in (select inner.cc_request_group_id
                                       from rpm_bulk_cc_pe inner
                                      where inner.bulk_cc_pe_id = I_bulk_cc_pe_id)
      for update of status;

   cursor c_group_id is
      select cc_request_group_id
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

BEGIN

   open c_lock_group;
   close c_lock_group;

   open c_group_id;
   fetch c_group_id into L_cc_request_group_id;
   close c_group_id;

   if CLEAN_UP(O_error_msg,
               O_group_complete_ind,
               L_cc_request_group_id) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.CLEAN_UP_WORKSHEET_BULK_CC',
                                        TO_CHAR(SQLCODE));
      return 0;
END CLEAN_UP_WORKSHEET_BULK_CC;

--------------------------------------------------------

FUNCTION CLEAN_UP_WKSHT_BULK_CC_BATCH(O_error_msg              OUT VARCHAR2,
                                      O_group_complete_ind     OUT NUMBER,
                                      O_conflicts_exist        OUT NUMBER,
                                      O_errors_exist           OUT NUMBER,
                                      I_cc_request_group_id IN     rpm_bulk_cc_pe.cc_request_group_id%TYPE)
RETURN NUMBER IS

   L_worksheet_ids    OBJ_NUMERIC_ID_TABLE := null;
   L_count            NUMBER := 0;

   cursor c_get_status is
      select max(decode(pe.status, 'E', 1, 0)),
             max(thread.has_conflict)
        from rpm_bulk_cc_pe pe,
             rpm_bulk_cc_pe_thread thread
       where pe.cc_request_group_id = I_cc_request_group_id
         and pe.bulk_cc_pe_id       = thread.bulk_cc_pe_id;

BEGIN

   open c_get_status;
   fetch c_get_status into O_errors_exist,
                           O_conflicts_exist;
   close c_get_status;

   if CLEAN_UP(O_error_msg,
               O_group_complete_ind,
               I_cc_request_group_id) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_WS_PRICE_EVENTS.CLEAN_UP_WKSHT_BULK_CC_BATCH',
                                        TO_CHAR(SQLCODE));
      return 0;
END CLEAN_UP_WKSHT_BULK_CC_BATCH;

--------------------------------------------------------

FUNCTION END_THREAD_PROCESS(O_error_msg               OUT VARCHAR2,
                            I_cc_error_tbl         IN     CONFLICT_CHECK_ERROR_TBL,
                            I_bulk_cc_pe_id        IN     rpm_bulk_cc_pe.bulk_cc_pe_id%TYPE,
                            I_parent_thread_number IN     rpm_bulk_cc_pe_thread.parent_thread_number%TYPE,
                            I_thread_number        IN     rpm_bulk_cc_pe_thread.thread_number%TYPE,
                            I_auto_approve_ind     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_GENERATE_WS_PRICE_EVENTS.END_THREAD_PROCESS';

   L_vdate                 DATE        := GET_VDATE;
   L_approve_ind           VARCHAR2(1) := NULL;

   L_price_event_type      RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE := NULL;
   L_user_name             RPM_BULK_CC_PE.USER_NAME%TYPE        := NULL;

   L_dynamic_area_diff_ind RPM_SYSTEM_OPTIONS.DYNAMIC_AREA_DIFF_IND%TYPE :=null;

   L_cc_error_tbl          CONFLICT_CHECK_ERROR_TBL := NULL;

   L_conflict_pe_ids       OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE(-9999999999);

   cursor C_BULK_TARGET is
      select DECODE(end_state,
                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, 'A',
                    'S'),
             price_event_type,
             user_name
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_CONFLICT_PE is
      select distinct
             price_event_id
        from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL));

BEGIN

   --propose retail for non-dynamic area diffs
   if RPM_SYSTEM_OPTIONS_SQL.GET_DYNAMIC_AREA_DIFF_IND(L_dynamic_area_diff_ind,
                                                       O_error_msg) = FALSE then
      return 0;
   end if;

   open C_BULK_TARGET;
   fetch C_BULK_TARGET into L_approve_ind,
                            L_price_event_type,
                            L_user_name;
   close C_BULK_TARGET;

   if I_cc_error_tbl is NOT NULL and
      I_cc_error_tbl.COUNT > 0 then
      --
      open C_CONFLICT_PE;
      fetch C_CONFLICT_PE BULK COLLECT into L_conflict_pe_ids;
      close c_conflict_pe;
      --
   end if;

   if (L_approve_ind = 'A' or
       L_approve_ind = 'S') and
      I_cc_error_tbl is NOT NULL and
      I_cc_error_tbl.COUNT > 0 then

      for i IN 1..I_cc_error_tbl.COUNT loop

         if I_auto_approve_ind = 1 then

            if RECORD_AUTO_APPROVE_ERROR(O_error_msg,
                                         I_cc_error_tbl(i).price_event_id,
                                         I_bulk_cc_pe_id,
                                         I_thread_number) = FALSE then
               return 0;
            end if;
         end if;

     end loop;

     if RPM_BULK_CC_THREADING_SQL.POPULATE_CC_ERROR_TABLE(L_cc_error_tbl,
                                                          I_cc_error_tbl,
                                                          L_price_event_type) = 0 then
        if L_cc_error_tbl is NOT NULL and
           L_cc_error_tbl.COUNT > 0 then
           ---
           O_error_msg := L_cc_error_tbl(1).error_string;
        end if;

        return 0;
     end if;
   end if;

   if L_approve_ind = 'A' then

      if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         update rpm_price_change
            set state         = RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                approval_id   = L_user_name,
                approval_date = L_vdate
          where price_change_id IN (select price_event_id
                                      from rpm_bulk_cc_pe_thread
                                     where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                       and parent_thread_number = I_parent_thread_number
                                       and thread_number        = I_thread_number
                                       and price_event_id       NOT IN (select value(ids)
                                                                          from table(cast(L_conflict_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids));
      end if;

      if L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then
         update rpm_clearance
            set state         = RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                approval_id   = L_user_name,
                approval_date = L_vdate
          where clearance_id IN (select price_event_id
                                   from rpm_bulk_cc_pe_thread
                                  where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                    and parent_thread_number = I_parent_thread_number
                                    and thread_number        = I_thread_number
                                    and price_event_id       NOT IN (select value(ids)
                                                                       from table(cast(L_conflict_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids));
      end if;

   else -- 'pricechange.state.submitted'

      if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         delete
           from rpm_price_change
          where price_change_id IN (select price_event_id
                                      from rpm_bulk_cc_pe_thread
                                     where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                       and parent_thread_number = I_parent_thread_number
                                       and thread_number        = I_thread_number);
      end if;

      if L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

         delete
           from rpm_clearance
          where clearance_id IN (select price_event_id
                                   from rpm_bulk_cc_pe_thread
                                  where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                    and parent_thread_number = I_parent_thread_number
                                    and thread_number        = I_thread_number);
      end if;

   end if;

   --update worksheet_data rows with new state
   delete numeric_id_gtt;

   insert into numeric_id_gtt(numeric_id)
      select /*+ ORDERED */ distinct wsiloc.worksheet_item_data_id
        from rpm_bulk_cc_pe grp,
             rpm_bulk_cc_pe_thread thread,
             (select price_change_id price_event_id,
                     item,
                     link_code,
                     zone_id
                from rpm_bulk_cc_wksht_pc
               where bulk_cc_pe_id = I_bulk_cc_pe_id
              union all
              select clearance_id price_event_id,
                     item,
                     NULL link_code,
                     zone_id
                from rpm_bulk_cc_wksht_clr
               where bulk_cc_pe_id = I_bulk_cc_pe_id) pc,
             rpm_worksheet_item_loc_gen_ws wsiloc
       where grp.bulk_cc_pe_id                  = I_bulk_cc_pe_id
         and thread.bulk_cc_pe_id               = grp.bulk_cc_pe_id
         and thread.thread_number               = I_thread_number
         and thread.parent_thread_number        = I_parent_thread_number
         and thread.price_event_id              NOT IN (select value(ids)
                                                          from table(cast(L_conflict_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and thread.price_event_id              = pc.price_event_id
         and wsiloc.workspace_id                = grp.worksheet_workspace_id
         and nvl(wsiloc.link_code, wsiloc.item) = NVL(TO_CHAR(pc.link_code), pc.item)
         and wsiloc.zone_id                     = pc.zone_id;

   update rpm_worksheet_item_data wid
      set state              = DECODE(L_approve_ind,
                                      'A', WS_STATE_INT_APPROVED,
                                      'S', WS_STATE_INT_SUBMITTED),
          constraint_boolean = 0
    where worksheet_item_data_id IN (select numeric_id
                                       from numeric_id_gtt);

   update rpm_worksheet_zone_data wid
      set state              = DECODE(L_approve_ind,
                                      'A', WS_STATE_INT_APPROVED,
                                      'S', WS_STATE_INT_SUBMITTED),
          constraint_boolean = 0
    where worksheet_item_data_id IN (select numeric_id
                                       from numeric_id_gtt);

   update rpm_worksheet_item_loc_data wid
      set conflict_boolean   = 0
    where worksheet_item_data_id IN (select numeric_id
                                       from numeric_id_gtt);

   --update failed worksheet_data rows
   delete gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date
      (number_1,
       number_2,
       varchar2_1)
      select distinct wsiloc.worksheet_item_data_id,
             wsiloc.worksheet_item_loc_data_id,
             err.message_key
        from rpm_bulk_cc_pe grp,
             rpm_bulk_cc_pe_thread thread,
             (select price_change_id price_event_id,
                     item,
                     link_code,
                     zone_id
                from rpm_bulk_cc_wksht_pc
               where bulk_cc_pe_id = I_bulk_cc_pe_id
              union all
              select clearance_id price_event_id,
                     item,
                     NULL link_code,
                     zone_id
                from rpm_bulk_cc_wksht_clr
               where bulk_cc_pe_id = I_bulk_cc_pe_id) pc,
             rpm_worksheet_item_loc_gen_ws wsiloc,
             rpm_con_check_err err
       where grp.bulk_cc_pe_id                  = I_bulk_cc_pe_id
         and thread.bulk_cc_pe_id               = grp.bulk_cc_pe_id
         and thread.thread_number               = I_thread_number
         and thread.parent_thread_number        = I_parent_thread_number
         and thread.price_event_id              IN (select value(ids)
                                                      from table(cast(L_conflict_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and thread.price_event_id              = pc.price_event_id
         and wsiloc.workspace_id                = grp.worksheet_workspace_id
         and nvl(wsiloc.link_code, wsiloc.item) = NVL(TO_CHAR(pc.link_code), pc.item)
         and pc.zone_id                         = wsiloc.zone_id
         and pc.price_event_id                  = err.ref_id
         and err.item                           = wsiloc.item
         and err.location                       = DECODE(err.zone_node_type,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, wsiloc.zone_id,
                                                         wsiloc.location);

   merge into rpm_worksheet_item_data wid
   using (select distinct
                 number_1 worksheet_item_data_id,
                 varchar2_1 message_key
            from gtt_num_num_str_str_date_date) use_this
      on (wid.worksheet_item_data_id = use_this.worksheet_item_data_id)
   when MATCHED then
      update
         set constraint_boolean = DECODE(use_this.message_key,
                                         'promotion_constraint_violation', 1,
                                         0);

   merge into rpm_worksheet_item_loc_data wid
   using (select distinct
                 number_1 worksheet_item_data_id,
                 number_2 worksheet_item_loc_data_id,
                 varchar2_1 message_key
            from gtt_num_num_str_str_date_date) use_this
      on (    wid.worksheet_item_data_id     = use_this.worksheet_item_data_id
          and wid.worksheet_item_loc_data_id = use_this.worksheet_item_loc_data_id)
   when MATCHED then
      update
         set conflict_boolean = DECODE(use_this.message_key,
                                       'promotion_constraint_violation', 0,
                                       1);

   merge into rpm_worksheet_zone_data wzd
   using (select rwi.dept,
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,
                 rwil.link_code,
                 rwi.item,
                 rwi.zone_id,
                 MAX(NVL(conflict_boolean, 0)) conflict_boolean,
                 case MAX(NVL(conflict_boolean, 0))
                    when MIN(NVL(conflict_boolean, 0)) then
                       0
                    else
                       1
                 end as conflict_boolean_flag,
                 MAX(NVL(constraint_boolean, 0)) constraint_boolean,
                 case MAX(NVL(constraint_boolean, 0))
                    when MIN(NVL(constraint_boolean, 0)) then
                       0
                    else
                       1
                 end as constraint_boolean_flag
            from (select distinct number_1 worksheet_item_data_id
                    from gtt_num_num_str_str_date_date) t,
                 rpm_worksheet_item_data rwi,
                 rpm_worksheet_item_loc_data rwil
           where rwi.worksheet_item_data_id  = t.worksheet_item_data_id
             and rwil.dept                   = rwi.dept
             and rwil.worksheet_status_id    = rwi.worksheet_status_id
             and rwil.worksheet_item_data_id = rwi.worksheet_item_data_id
           group by rwi.dept,
                    rwi.worksheet_status_id,
                    rwi.worksheet_item_data_id,
                    rwil.link_code,
                    rwi.item,
                    rwi.zone_id) use_this
      on (    wzd.worksheet_status_id    = use_this.worksheet_status_id
          and wzd.worksheet_item_data_id = use_this.worksheet_item_data_id)
   when MATCHED then
      update
         set wzd.conflict_boolean        = use_this.conflict_boolean,
             wzd.conflict_boolean_flag   = use_this.conflict_boolean_flag,
             wzd.constraint_boolean      = use_this.constraint_boolean,
             wzd.constraint_boolean_flag = use_this.constraint_boolean_flag;

   --back off secondary area if the primary area failed
   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select secondary.price_change_id
        from rpm_bulk_cc_pe_thread thread,
             rpm_bulk_cc_wksht_pc pc,
             rpm_bulk_cc_pe grp,
             rpm_bulk_cc_wksht_pc secondary
       where grp.bulk_cc_pe_id                                 = I_bulk_cc_pe_id
         and thread.bulk_cc_pe_id                              = grp.bulk_cc_pe_id
         and thread.thread_number                              = I_thread_number
         and thread.parent_thread_number                       = I_parent_thread_number
         and thread.has_conflict                               = 1
         and pc.price_change_id                                = thread.price_event_id
         and pc.primary_secondary_ind                          = 'P'
         and secondary.bulk_cc_pe_id                           = grp.secondary_bulk_cc_pe_id
         and secondary.primary_secondary_ind                   = 'S'
         and NVL(TO_CHAR(secondary.link_code), secondary.item) = NVL(TO_CHAR(pc.link_code), pc.item);

   delete from rpm_worksheet_item_gen_ws
    where workspace_id IN (select worksheet_workspace_id
                             from rpm_bulk_cc_pe
                            where bulk_cc_pe_id = I_bulk_cc_pe_id)
      and (item,
           zone_id) IN (select item,
                               zone_id
                          from rpm_bulk_cc_wksht_pc
                         where price_change_id IN (select numeric_id
                                                     from numeric_id_gtt));

   delete from rpm_worksheet_item_loc_gen_ws
    where workspace_id IN (select worksheet_workspace_id
                             from rpm_bulk_cc_pe
                            where bulk_cc_pe_id = I_bulk_cc_pe_id)
      and (item,
           zone_id) IN (select item,
                               zone_id
                          from rpm_bulk_cc_wksht_pc
                         where price_change_id IN (select numeric_id
                                                     from numeric_id_gtt));

   delete
     from rpm_price_change
    where price_change_id IN (select numeric_id
                                from numeric_id_gtt);

   delete
     from rpm_bulk_cc_pe_item
    where price_event_id IN (select numeric_id
                               from numeric_id_gtt);

   delete
     from rpm_bulk_cc_pe_location
    where price_event_id IN (select numeric_id
                               from numeric_id_gtt);

   delete
     from rpm_bulk_cc_pe_thread
    where price_event_id IN (select numeric_id
                               from numeric_id_gtt);

   delete
     from rpm_bulk_cc_wksht_pc
    where price_change_id IN (select numeric_id
                               from numeric_id_gtt);
   --end back off secondary area if the primary area failed

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END END_THREAD_PROCESS;

---------------------------------------------

END RPM_GENERATE_WS_PRICE_EVENTS;
/
