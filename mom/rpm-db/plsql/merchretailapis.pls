CREATE OR REPLACE PACKAGE MERCH_RETAIL_API_SQL IS

 MAX_UNIT_RETAIL           CONSTANT NUMBER(20,2) := 9999999999999999.99;
 MIN_UNIT_RETAIL           CONSTANT NUMBER(20,2) := -9999999999999999.99;
 COST_MARKUP_CALC_TYPE     CONSTANT NUMBER(1)    := 0;
 RETAIL_MARKUP_CALC_TYPE   CONSTANT NUMBER(1)    := 1;
 MAX_MARKUP_PERCENT        CONSTANT NUMBER(5,2)  := 1.00;

--------------------------------------------------------------------------------

FUNCTION GET_MERCH_PRICING_DEFS(O_error_message            OUT VARCHAR2,
                                I_dept                  IN     rpm_merch_retail_def.dept%TYPE,
                                I_class                 IN     rpm_merch_retail_def.class%TYPE,
                                I_subclass              IN     rpm_merch_retail_def.subclass%TYPE,
                                O_regular_zone_group       OUT rpm_merch_retail_def.regular_zone_group%TYPE,
                                O_clearance_zone_group     OUT rpm_merch_retail_def.clearance_zone_group%TYPE,
                                O_markup_calc_type         OUT rpm_merch_retail_def.markup_calc_type%TYPE,
                                O_markup_percent           OUT rpm_merch_retail_def.markup_percent%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------

PROCEDURE MERCH_PRICING_DEFS_EXIST (O_error_message          OUT  VARCHAR2,
                                    O_success                OUT  VARCHAR2,
                                    I_dept                IN      DEPS.DEPT%TYPE,
                                    I_class               IN      CLASS.CLASS%TYPE,
                                    I_subclass            IN      SUBCLASS.SUBCLASS%TYPE);

--------------------------------------------------------------------------------

FUNCTION GET_RPM_ITEM_ZONE_PRICE(O_error_message               OUT VARCHAR2,
                                 I_item                     IN     rpm_item_zone_price.item%TYPE,
                                 I_zone_id                  IN     rpm_item_zone_price.zone_id%TYPE,
                                 O_standard_retail             OUT rpm_item_zone_price.standard_retail%TYPE,
                                 O_standard_retail_currency    OUT rpm_item_zone_price.standard_retail_currency%TYPE,
                                 O_standard_uom                OUT rpm_item_zone_price.standard_uom%TYPE,
                                 O_selling_retail              OUT rpm_item_zone_price.selling_retail%TYPE,
                                 O_selling_retail_currency     OUT rpm_item_zone_price.selling_retail_currency%TYPE,
                                 O_selling_uom                 OUT rpm_item_zone_price.selling_uom%TYPE,
                                 O_multi_units                 OUT rpm_item_zone_price.multi_units%TYPE,
                                 O_multi_unit_retail           OUT rpm_item_zone_price.multi_unit_retail%TYPE,
                                 O_multi_unit_retail_currency  OUT rpm_item_zone_price.multi_unit_retail_currency%TYPE,
                                 O_multi_selling_uom           OUT rpm_item_zone_price.multi_selling_uom%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------

PROCEDURE GET_ITEM_PRICING_INFO (O_error_message          OUT  VARCHAR2,
                                 O_success                OUT  VARCHAR2,
                                 O_item_pricing_table     OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                 I_item_parent         IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_item                IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_dept                IN      DEPS.DEPT%TYPE,
                                 I_class               IN      CLASS.CLASS%TYPE,
                                 I_subclass            IN      SUBCLASS.SUBCLASS%TYPE,
                                 I_cost_currency_code  IN      RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE,
                                 I_cost                IN      NUMBER);

--------------------------------------------------------------------------------

PROCEDURE GET_ZONE_LOCATIONS (O_error_message       OUT  VARCHAR2,
                              O_success             OUT  VARCHAR2,
                              O_zone_locs_table     OUT  NOCOPY OBJ_ZONE_LOCS_TBL,
                              I_zone_group_id    IN      RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                              I_zone_id          IN      RPM_ZONE.ZONE_ID%TYPE);

--------------------------------------------------------------------------------

PROCEDURE SET_ITEM_PRICING_INFO (O_error_message          OUT  VARCHAR2,
                                 O_success                OUT  VARCHAR2,
                                 I_item_pricing_table  IN      OBJ_ITEM_PRICING_TBL);

--------------------------------------------------------------------------------

PROCEDURE SET_CHILD_ITEM_PRICING_INFO (O_error_message                OUT  VARCHAR2,
                                       O_success                      OUT  VARCHAR2,
                                       I_parent_item               IN      ITEM_MASTER.ITEM%TYPE,
                                       I_zone_id                   IN   RPM_ITEM_ZONE_PRICE.ZONE_ID%TYPE,
                                       I_child_item_pricing_table  IN      OBJ_CHILD_ITEM_PRICING_TBL);

--------------------------------------------------------------------------------

PROCEDURE LOCK_PRICING_RECORDS (O_error_message     OUT  VARCHAR2,
                                O_success           OUT  VARCHAR2,
                                I_item           IN      ITEM_MASTER.ITEM%TYPE);

--------------------------------------------------------------------------------

PROCEDURE BUILD_ITEM_PRICING_OBJECT (O_error_message           OUT  VARCHAR2,
                                     O_success                 OUT  VARCHAR2,
                                     O_item_pricing_table      OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                     I_item                 IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                     I_dept                 IN      DEPS.DEPT%TYPE,
                                     I_class                IN      CLASS.CLASS%TYPE,
                                     I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
                                     I_price_currency_code  IN      RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE,
                                     I_retail_price         IN      NUMBER);

--------------------------------------------------------------------------------

PROCEDURE CHECK_RETAIL_EXISTS (O_error_message     OUT  VARCHAR2,
                               O_success           OUT  VARCHAR2,
                               O_exists            OUT  VARCHAR2,
                               I_item           IN      ITEM_MASTER.ITEM%TYPE);

--------------------------------------------------------------------------------

PROCEDURE COMPARE_RETAIL_DEF (O_error_message           OUT  VARCHAR2,
                              O_success                 OUT  VARCHAR2,
                              O_equal                   OUT  VARCHAR2,
                              I_new_dept             IN      DEPS.DEPT%TYPE,
                              I_new_class            IN      CLASS.CLASS%TYPE,
                              I_new_subclass         IN     SUBCLASS.SUBCLASS%TYPE,
                              I_old_dept             IN      DEPS.DEPT%TYPE,
                              I_old_class            IN      CLASS.CLASS%TYPE,
                              I_old_subclass         IN      SUBCLASS.SUBCLASS%TYPE);

--------------------------------------------------------------------------------

PROCEDURE DELETE_RPM_ITEM_ZONE_PRICE (O_error_message           OUT  VARCHAR2,
                                      O_success                 OUT  VARCHAR2,
                                      I_item                 IN      RPM_ITEM_ZONE_PRICE.ITEM%TYPE);

--------------------------------------------------------------------------------

FUNCTION GET_NON_RANGED_ITEM_RETAIL (O_error_message         OUT  VARCHAR2,
                                     O_selling_retail        OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                                     O_selling_uom           OUT  RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                                     O_multi_units           OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNITS%TYPE,
                                     O_multi_unit_retail     OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL%TYPE,
                                     O_multi_selling_uom     OUT  RPM_ITEM_ZONE_PRICE.MULTI_SELLING_UOM%TYPE,
                                     O_currency_code         OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                                     I_item               IN      ITEM_MASTER.ITEM%TYPE,
                                     I_dept               IN      ITEM_MASTER.DEPT%TYPE,
                                     I_class              IN      ITEM_MASTER.CLASS%TYPE,
                                     I_subclass           IN      ITEM_MASTER.SUBCLASS%TYPE,
                                     I_location           IN      ITEM_LOC.LOC%TYPE,
                                     I_loc_type           IN      ITEM_LOC.LOC_TYPE%TYPE,
                                     I_unit_cost          IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                                     I_currency           IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION GET_BASE_ZONE_RETAIL (O_error_message         OUT  VARCHAR2,
                               O_zone_group            OUT  RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE,
                               O_base_zone             OUT  RPM_ZONE.ZONE_ID%TYPE,
                               O_selling_retail        OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                               O_selling_uom           OUT  RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                               O_multi_units           OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNITS%TYPE,
                               O_multi_unit_retail     OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL%TYPE,
                               O_multi_selling_uom     OUT  RPM_ITEM_ZONE_PRICE.MULTI_SELLING_UOM%TYPE,
                               O_currency_code         OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                               I_item               IN      ITEM_MASTER.ITEM%TYPE,
                               I_dept               IN      ITEM_MASTER.DEPT%TYPE,
                               I_class              IN      ITEM_MASTER.CLASS%TYPE,
                               I_subclass           IN      ITEM_MASTER.SUBCLASS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------

PROCEDURE GET_AREA_DIFF_PRICE (O_error_message          OUT  VARCHAR2,
                               O_success                OUT  VARCHAR2,
                               O_selling_retail         OUT  RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                               I_zone_group_id       IN      RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                               I_item                IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                               I_sec_zone_id         IN      RPM_ZONE.ZONE_ID%TYPE,
                               I_sec_currency_code   IN      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                               I_prm_zone_id         IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                               I_prm_selling_retail  IN      RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                               I_prm_selling_uom     IN      RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                               I_prm_currency_code   IN      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE);

--------------------------------------------------------------------------------

FUNCTION GET_PRICING_ZONE_ID (O_error_message IN OUT VARCHAR2,
                              O_zone_ids      IN OUT OBJ_ZONE_TBL,
                              I_new_loc       IN     ITEM_LOC.LOC%TYPE,
                              I_new_loc_type  IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_zone_id       IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------------------
FUNCTION SET_AREA_DIFF_PRICE(O_error_msg             OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_pricing_table    OUT OBJ_ITEM_PRICING_TBL,
                             I_item_pricing_table IN     OBJ_ITEM_PRICING_TBL)

RETURN BOOLEAN;

------------------------------------------------------------------------------------
FUNCTION GET_REASON_CODE_DESC(O_error_msg           OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_reason_code_id   IN      RPM_CODES.CODE_ID%TYPE)
RETURN VARCHAR2;

------------------------------------------------------------------------------------
FUNCTION GET_REASON_CODE_DESC(I_reason_code_id   IN      RPM_CODES.CODE_ID%TYPE)
RETURN VARCHAR2;

------------------------------------------------------------------------------------
END MERCH_RETAIL_API_SQL;
/

