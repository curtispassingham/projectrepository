CREATE OR REPLACE PACKAGE BODY RPM_EXT_DASHBOARD_DATA_SQL AS

   LP_vdate DATE := GET_VDATE;

----------------------------------------------------------------------

FUNCTION INITIALIZE_THREADS(O_error_message       OUT VARCHAR2,
                            O_max_thread          OUT NUMBER,
                            I_thread_luw_count IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_EXT_DASHBOARD_DATA_SQL.INITIALIZE_THREADS';

BEGIN

   EXECUTE IMMEDIATE 'truncate table rpm_promo_dashboard';
   EXECUTE IMMEDIATE 'truncate table rpm_promo_dashboard_thread';

   insert into rpm_promo_dashboard_thread (promo_dtl_id,
                                           thread_number)
      select distinct promo_dtl_id,
             thread_num
        from (select rpd.promo_dtl_id,
                     -- Rank the details based on merch node data so that larger
                     -- details are processed by themselves and in earlier threads
                     DECODE(rpdmn.merch_type,
                            RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM, 1,
                            RPM_CONSTANTS.DEPT_LEVEL_ITEM, 2,
                            RPM_CONSTANTS.CLASS_LEVEL_ITEM, 2,
                            RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM, 2,
                            RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM, 3,
                            RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL, 4,
                            5) merch_rank
                from rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_disc_ladder rpddl
               where rpd.state                        = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                 and NVL(rpd.end_date, LP_vdate + 1) >= LP_vdate + 1
                 and rpd.promo_dtl_id                 = rpdmn.promo_dtl_id
                 and rpdmn.promo_dtl_list_id          = rpddl.promo_dtl_list_id
                 and rpddl.change_type               != RPM_CONSTANTS.RETAIL_EXCLUDE
              union all
              select rpd.promo_dtl_id,
                     -- Rank the details based on merch node data so that larger
                     -- details are processed by themselves and in earlier threads
                     DECODE(rpdmn.merch_type,
                            RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM, 1,
                            RPM_CONSTANTS.DEPT_LEVEL_ITEM, 2,
                            RPM_CONSTANTS.CLASS_LEVEL_ITEM, 2,
                            RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM, 2,
                            RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM, 3,
                            RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL, 4,
                            5) merch_rank
                from rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_disc_ladder rpddl
               where rpd.state                = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.start_date           BETWEEN LP_vdate + 1 AND LP_vdate + 29
                 and rpd.promo_dtl_id         = rpdmn.promo_dtl_id
                 and rpdmn.promo_dtl_list_id  = rpddl.promo_dtl_list_id
                 and rpddl.change_type       != RPM_CONSTANTS.RETAIL_EXCLUDE) t
       model
          dimension by (ROW_NUMBER() OVER (ORDER BY t.merch_rank,
                                                    t.promo_dtl_id) rn)
          measures (t.promo_dtl_id,
                    t.merch_rank,
                    1 running_total,
                    1 thread_num)
          rules (running_total[rn] =
                    case
                       -- For details with potentially larger datasets, process each
                       -- in their own thread
                       when merch_rank[CV(rn)] IN (1, 2, 3, 4) then
                          I_thread_luw_count
                       -- Ensure that details with multiple merch nodes (MB, TXN) are
                       -- processed in the same thread and not split up
                       when promo_dtl_id[CV(rn) - 1] = promo_dtl_id[CV(rn)] then
                          running_total[CV(rn) - 1]
                       when running_total[CV(rn) - 1] + 1 > I_thread_luw_count then
                          1
                       else
                          running_total[CV(rn) - 1] + 1
                    end,
                 thread_num[rn] =
                    case
                       when CV(rn) = 1 then
                          1
                       -- For details with potentially larger datasets, process each
                       -- in their own thread
                       when merch_rank[CV(rn)] IN (1, 2, 3, 4) then
                          thread_num[CV(rn) - 1] + 1
                       -- Ensure that details with multiple merch nodes (MB, TXN) are
                       -- processed in the same thread and not split up
                       when promo_dtl_id[CV(rn) - 1] = promo_dtl_id[CV(rn)] then
                          thread_num[CV(rn) - 1]
                       when running_total[CV(rn)] = 1 then
                          thread_num[CV(rn) - 1] + 1
                       else
                          thread_num[CV(rn) - 1]
                    end);

   select MAX(thread_number)
     into O_max_thread
     from rpm_promo_dashboard_thread;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;


END INITIALIZE_THREADS;

----------------------------------------------------------------------

FUNCTION STAGE_PROMO_DATA(O_error_message    OUT VARCHAR2,
                          I_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_EXT_DASHBOARD_DATA_SQL.STAGE_PROMO_DATA';

BEGIN

   -- Find all Exceptions/Exclusions for the details in the thread and
   -- explode to item-loc-parent-detail level

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 location,
                                 dept)
      with promo_dtl as
         (select rpdt.promo_dtl_id,
                 rpdmn.merch_type,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.skulist,
                 rpdmn.price_event_itemlist,
                 rpzl.location,
                 rpzl.zone_id,
                 rpzl.zone_node_type,
                 rpd.sys_generated_exclusion
            from rpm_promo_dashboard_thread rpdt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_zone_location rpzl
           where rpdt.thread_number      = I_thread_number
             and rpd.exception_parent_id = rpdt.promo_dtl_id
             and rpd.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE)
             and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
             and rpd.promo_dtl_id        = rpzl.promo_dtl_id
             and rpzl.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE))
      -- parent loc
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             item_master im
       where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and im.item_parent     = rpd.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- parent zone
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             item_master im,
             rpm_zone_location rzl
       where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent     = rpd.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id        = rpd.zone_id
         and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- parent-diff loc
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             item_master im
       where rpd.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and im.item_parent     = rpd.item
         and im.item_level      = im.tran_level
         and rpd.diff_id        IN (im.diff_1,
                                    im.diff_2,
                                    im.diff_3,
                                    im.diff_4)
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- parent-diff zone
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             item_master im,
             rpm_zone_location rzl
       where rpd.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent     = rpd.item
         and im.item_level      = im.tran_level
         and rpd.diff_id        IN (im.diff_1,
                                    im.diff_2,
                                    im.diff_3,
                                    im.diff_4)
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id        = rpd.zone_id
         and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- item zone
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             item_master im,
             rpm_zone_location rzl
       where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item            = rpd.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id        = rpd.zone_id
         and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- item loc
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             item_master im
       where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and im.item            = rpd.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- skulist loc - parent items
      select /*+ USE_NL(rpds, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_promo_dtl_skulist rpds,
             item_master im
       where NVL(rpd.sys_generated_exclusion, 0) = 0
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpds.skulist                        = rpd.skulist
         and rpds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and rpds.item                           = im.item_parent
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- skulist loc - tran items
      select /*+ USE_NL(rpds, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_promo_dtl_skulist rpds,
             item_master im
       where NVL(rpd.sys_generated_exclusion, 0) = 0
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpds.skulist                        = rpd.skulist
         and rpds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
         and rpds.item                           = im.item
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- skulist zone - parent items
      select /*+ USE_NL(rpds, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_promo_dtl_skulist rpds,
             item_master im,
             rpm_zone_location rzl
       where NVL(rpd.sys_generated_exclusion, 0) = 0
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpds.skulist                        = rpd.skulist
         and rpds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and rpds.item                           = im.item_parent
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id                         = rpd.zone_id
         and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- skulist zone - tran items
      select /*+ USE_NL(rpds, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_promo_dtl_skulist rpds,
             item_master im,
             rpm_zone_location rzl
       where NVL(rpd.sys_generated_exclusion, 0) = 0
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpds.skulist                        = rpd.skulist
         and rpds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
         and rpds.item                           = im.item
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id                         = rpd.zone_id
         and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- peil loc - parent items
      select rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail rmld,
             item_master im
       where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rmld.merch_list_id = rpd.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item          = im.item_parent
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- peil loc - tran items
      select rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail rmld,
             item_master im
       where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rmld.merch_list_id = rpd.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item          = im.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- peil zone - parent items
      select rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail rmld,
             item_master im,
             rpm_zone_location rzl
       where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id = rpd.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item          = im.item_parent
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id        = rpd.zone_id
         and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- peil zone - tran items
      select rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail rmld,
             item_master im,
             rpm_zone_location rzl
       where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id = rpd.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item          = im.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id        = rpd.zone_id
         and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- merch_list zone - parent items
      select /*+ USE_NL(mld, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone_location rzl
       where NVL(rpd.sys_generated_exclusion, 0) = 1
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and mld.merch_list_id                   = rpd.skulist
         and mld.merch_level                     = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent                      = mld.item
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id                         = rpd.zone_id
         and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- merch_list zone - parent/diff items
      select /*+ USE_NL(mld, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone_location rzl
       where NVL(rpd.sys_generated_exclusion, 0) = 1
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and mld.merch_list_id                   = rpd.skulist
         and mld.merch_level                     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item_parent                      = mld.item
         and im.diff_1                           = mld.diff_id
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id                         = rpd.zone_id
         and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- merch_list zone - tran items
      select /*+ USE_NL(mld, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone_location rzl
       where NVL(rpd.sys_generated_exclusion, 0) = 1
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and mld.merch_list_id                   = rpd.skulist
         and mld.merch_level                     = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                             = mld.item
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id                         = rpd.zone_id
         and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- merch_list loc - parent items
      select /*+ USE_NL(mld, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail mld,
             item_master im
       where NVL(rpd.sys_generated_exclusion, 0) = 1
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and mld.merch_list_id                   = rpd.skulist
         and mld.merch_level                     = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent                      = mld.item
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- merch_list loc - parent/diff items
      select /*+ USE_NL(mld, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail mld,
             item_master im
       where NVL(rpd.sys_generated_exclusion, 0) = 1
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and mld.merch_list_id                   = rpd.skulist
         and mld.merch_level                     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item_parent                      = mld.item
         and im.diff_1                           = mld.diff_id
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- merch_list loc - tran items
      select /*+ USE_NL(mld, im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             rpm_merch_list_detail mld,
             item_master im
       where NVL(rpd.sys_generated_exclusion, 0) = 1
         and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and mld.merch_list_id                   = rpd.skulist
         and mld.merch_level                     = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                             = mld.item
         and im.item_level                       = im.tran_level
         and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- merch hier loc
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rpd.location,
             im.dept
        from promo_dtl rpd,
             item_master im
       where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                    RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                    RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and im.dept            = rpd.dept
         and im.class           = NVL(rpd.class, im.class)
         and im.subclass        = NVL(rpd.subclass, im.subclass)
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- merch hier zone
      select /*+ USE_NL(im) ORDERED */
             rpd.promo_dtl_id,
             im.item,
             rzl.location,
             im.dept
        from promo_dtl rpd,
             item_master im,
             rpm_zone_location rzl
       where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                    RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                    RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.dept            = rpd.dept
         and im.class           = NVL(rpd.class, im.class)
         and im.subclass        = NVL(rpd.subclass, im.subclass)
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rzl.zone_id        = rpd.zone_id
         and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE;

   -- Explode the details for the thread to the item-loc-detail level
   -- and join (outter) to the exploded exeception/exclusion data.
   -- Whatever does not have a record for the child data is part of
   -- the event so that goes into the dashboard table.

   insert into rpm_promo_dashboard (promo_dtl_id,
                                    start_date,
                                    end_date,
                                    item,
                                    location,
                                    comp_type)
      with promo_dtl as
         (select rpdt.promo_dtl_id,
                 rpdmn.merch_type,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.skulist,
                 rpdmn.price_event_itemlist,
                 rpzl.location,
                 rpzl.zone_id,
                 rpzl.zone_node_type,
                 rpd.start_date,
                 rpd.end_date,
                 rpc.type comp_type
            from rpm_promo_dashboard_thread rpdt,
                 rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_zone_location rpzl
           where rpdt.thread_number = I_thread_number
             and rpd.promo_dtl_id   = rpdt.promo_dtl_id
             and rpd.state          IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                        RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                        RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                        RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                        RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                        RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE)
            and rpd.promo_comp_id   = rpc.promo_comp_id
            and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
            and rpd.promo_dtl_id    = rpzl.promo_dtl_id
            and rpzl.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE)),
      ril as
         (select distinct item,
                 location,
                 price_event_id
            from rpm_item_loc_gtt)
      select promo_dtl_id,
             start_date,
             end_date,
             item,
             location,
             comp_type
        from (-- parent loc
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and im.item_parent     = rpd.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- parent zone
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and im.item_parent     = rpd.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- parent-diff loc
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and im.item_parent     = rpd.item
                 and im.item_level      = im.tran_level
                 and rpd.diff_id        IN (im.diff_1,
                                            im.diff_2,
                                            im.diff_3,
                                            im.diff_4)
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- parent-diff zone
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and im.item_parent     = rpd.item
                 and im.item_level      = im.tran_level
                 and rpd.diff_id        IN (im.diff_1,
                                            im.diff_2,
                                            im.diff_3,
                                            im.diff_4)
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- item zone
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpd.item           = im.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and rpd.item           = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- item loc
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.item           = im.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and rpd.item           = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- skulist loc - parent items
              select /*+ USE_NL(rpds, im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_promo_dtl_skulist rpds,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpds.skulist       = rpd.skulist
                 and rpds.item_level    = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and rpds.item          = im.item_parent
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- skulist loc - tran items
              select /*+ USE_NL(rpds, im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_promo_dtl_skulist rpds,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpds.skulist       = rpd.skulist
                 and rpds.item_level    = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and rpds.item          = im.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- skulist zone - parent items
              select /*+ USE_NL(rpds, im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_promo_dtl_skulist rpds,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpds.skulist       = rpd.skulist
                 and rpds.item_level    = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and rpds.item          = im.item_parent
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- skulist zone - tran items
              select /*+ USE_NL(rpds, im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_promo_dtl_skulist rpds,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpds.skulist       = rpd.skulist
                 and rpds.item_level    = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and rpds.item          = im.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- peil loc - parent items
              select rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rmld.merch_list_id = rpd.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item          = im.item_parent
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- peil loc - tran items
              select rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rmld.merch_list_id = rpd.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item          = im.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- peil zone - parent items
              select rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rmld.merch_list_id = rpd.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item          = im.item_parent
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- peil zone - tran items
              select rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rmld.merch_list_id = rpd.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item          = im.item
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- merch hier loc
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     ril
               where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                            RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                            RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and im.dept            = rpd.dept
                 and im.class           = NVL(rpd.class, im.class)
                 and im.subclass        = NVL(rpd.subclass, im.subclass)
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rpd.location       = ril.location (+)
              union all
              -- merch hier zone
              select /*+ USE_NL(im) ORDERED */
                     rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_master im,
                     rpm_zone_location rzl,
                     ril
               where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                            RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                            RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and im.dept            = rpd.dept
                 and im.class           = NVL(rpd.class, im.class)
                 and im.subclass        = NVL(rpd.subclass, im.subclass)
                 and im.item_level      = im.tran_level
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rzl.zone_id        = rpd.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and rzl.location       = ril.location (+)
              union all
              -- storewide loc
              select rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rpd.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     item_loc il,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and il.loc             = rpd.location
                 and il.item            = im.item
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and il.loc             = ril.location (+)
              union all
              -- storewide zone
              select rpd.promo_dtl_id,
                     rpd.start_date,
                     rpd.end_date,
                     im.item,
                     rzl.location,
                     rpd.comp_type,
                     ril.price_event_id ril_peid
                from promo_dtl rpd,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     ril
               where rpd.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpd.zone_id        = rzl.zone_id
                 and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and il.loc             = rzl.location
                 and il.item            = im.item
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpd.promo_dtl_id   = ril.price_event_id (+)
                 and im.item            = ril.item (+)
                 and il.loc             = ril.location (+))
       where ril_peid is NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END STAGE_PROMO_DATA;

----------------------------------------------------------------------

END RPM_EXT_DASHBOARD_DATA_SQL;
/
