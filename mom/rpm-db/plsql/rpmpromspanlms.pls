CREATE OR REPLACE PACKAGE RPM_CC_PROM_SPAN_LM AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION VALIDATE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                  I_promo_rec    IN     OBJ_RPM_CC_PROMO_REC)
RETURN NUMBER;

FUNCTION VALIDATE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_promo_ids        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

--------------------------------------------------------------------------------

END RPM_CC_PROM_SPAN_LM;
/

