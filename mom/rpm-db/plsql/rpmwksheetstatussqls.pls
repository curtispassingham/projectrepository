CREATE OR REPLACE PACKAGE RPM_WORKSHEET_STATUS_SQL AS

--------------------------------------------------------------------------------------
FUNCTION DELETE_ACTION(O_error_msg             IN OUT VARCHAR2,
                       I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

--------------------------------------------------------------------------------------
FUNCTION RESET_ACTION(O_error_msg             IN OUT VARCHAR2,
                      I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

--------------------------------------------------------------------------------------
FUNCTION REJECT_ACTION(O_error_msg             IN OUT VARCHAR2,
                       I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

--------------------------------------------------------------------------------------
FUNCTION APPROVE_DELETE_ACTION(O_error_msg             IN OUT VARCHAR2,
                               I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

-------------------------------------------------------------------------------

END RPM_WORKSHEET_STATUS_SQL;
/
