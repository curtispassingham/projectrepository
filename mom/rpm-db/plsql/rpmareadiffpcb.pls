CREATE OR REPLACE PACKAGE BODY RPM_AREA_DIFF_PRICE_CHANGE AS

--------------------------------------------------------
--PROTOTYPES
--------------------------------------------------------

   LP_vdate DATE := GET_VDATE;

   cursor C_AD_CODE_TYPE is
      select code_id
        from rpm_codes
       where code = 'AREADIFF';

   cursor C_ADAA_CODE_TYPE is
      select code_id
        from rpm_codes
       where code = 'AREADIFFAA';

   cursor C_NO_EXCLUSION is
      select OBJ_NUM_NUM_STR_REC(price_change_id,
                                 area_diff_id,
                                 zone_id)
        from (-- Transaction Level Item
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from rpm_area_diff_pc_approve_gtt gtt,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and im.item                  = rpc.item
                 and im.item_level            = im.tran_level
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade
                                  where rade.area_diff_id = gtt.area_diff_id
                                    and rade.item_id      = im.item)
              union all
              -- Parent Level Item and Parent Diff Level Item
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from rpm_area_diff_pc_approve_gtt gtt,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and im.item                  = rpc.item
                 and im.item_level + 1        = im.tran_level
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade,
                                        item_master im1
                                  where rade.area_diff_id = gtt.area_diff_id
                                    and im1.item_parent   = rpc.item
                                    and (   rpc.diff_id         is NULL
                                         or (    rpc.diff_id    is NOT NULL
                                             and (   im1.diff_1 = rpc.diff_id
                                                  or im1.diff_2 = rpc.diff_id
                                                  or im1.diff_3 = rpc.diff_id
                                                  or im1.diff_4 = rpc.diff_id)))
                                    and rade.item_id      = im1.item)
              union all
              -- Item Lists
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from (select price_change_id,
                             area_diff_id,
                             zone_id
                        from (select price_change_id,
                                     area_diff_id,
                                     zone_id,
                                     RANK() OVER (PARTITION BY price_change_id,
                                                               area_diff_id,
                                                               zone_id
                                                      ORDER BY dept,
                                                               class,
                                                               subclass) rnk
                                from rpm_area_diff_pc_approve_gtt
                               where rownum > 0)
                       where rnk = 1) gtt,
                     rpm_price_change rpc
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade,
                                        rpm_price_change_skulist rpcs
                                  where rpc.skulist         = rpcs.skulist
                                    and rpc.price_change_id = rpcs.price_event_id
                                    and rpcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                                    and rade.area_diff_id   = gtt.area_diff_id
                                    and rade.item_id        = rpcs.item
                                 union all
                                 select 1
                                   from rpm_area_diff_exclude rade,
                                        item_master im,
                                        rpm_price_change_skulist rpcs
                                  where rpc.skulist         = rpcs.skulist
                                    and rpc.price_change_id = rpcs.price_event_id
                                    and rpcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                    and im.item_parent      = rpcs.item
                                    and rade.area_diff_id   = gtt.area_diff_id
                                    and rade.item_id        = im.item)
              union all
              -- PEILs
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from (select price_change_id,
                             area_diff_id,
                             zone_id
                        from (select price_change_id,
                                     area_diff_id,
                                     zone_id,
                                     RANK() OVER (PARTITION BY price_change_id,
                                                               area_diff_id,
                                                               zone_id
                                                      ORDER BY dept,
                                                               class,
                                                               subclass) rnk
                                from rpm_area_diff_pc_approve_gtt
                               where rownum > 0)
                       where rnk = 1) gtt,
                     rpm_price_change rpc
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.skulist              is NULL
                 and rpc.link_code            is NULL
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade,
                                        rpm_merch_list_detail rmld
                                  where rmld.merch_list_id = rpc.price_event_itemlist
                                    and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                    and rade.area_diff_id  = gtt.area_diff_id
                                    and rade.item_id       = rmld.item
                                 union all
                                 select 1
                                   from rpm_area_diff_exclude rade,
                                        item_master im,
                                        rpm_merch_list_detail rmld
                                  where rmld.merch_list_id = rpc.price_event_itemlist
                                    and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                    and rade.area_diff_id  = gtt.area_diff_id
                                    and im.item_parent     = rmld.item
                                    and rade.item_id       = im.item)
              union all
              -- Link Code Level
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from rpm_area_diff_pc_approve_gtt gtt,
                     rpm_price_change rpc,
                     rpm_link_code_attribute rlca
               where rpc.price_change_id = gtt.price_change_id
                 and rpc.link_code       is NOT NULL
                 and rlca.link_code      = rpc.link_code
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade
                                  where rade.area_diff_id = gtt.area_diff_id
                                    and rade.item_id      = rlca.item));

   cursor C_HAS_EXCLUSION is
   -- Tran Level Item if excluded means no Area Diff Price Change should be created
   -- If an item in the Link Code is excluded then the whole Link Code will be excluded
      select OBJ_NUM_NUM_NUM_STR_REC(price_change_id,
                                     area_diff_id,
                                     zone_id,
                                     NULL)
        from (-- Parent Level Item and Parent Diff Level Item
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from rpm_area_diff_pc_approve_gtt gtt,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and im.item                  = rpc.item
                 and im.item_level + 1        = im.tran_level
                 and EXISTS (select 1
                               from rpm_area_diff_exclude rade,
                                    item_master im1
                              where rade.area_diff_id = gtt.area_diff_id
                                and im1.item_parent   = rpc.item
                                and (   rpc.diff_id        is NULL
                                     or (    rpc.diff_id   is NOT NULL
                                         and (   im1.diff_1 = rpc.diff_id
                                              or im1.diff_2 = rpc.diff_id
                                              or im1.diff_3 = rpc.diff_id
                                              or im1.diff_4 = rpc.diff_id)))
                                and rade.item_id      = im1.item)
              -- Skulist
              union all
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from (select price_change_id,
                             area_diff_id,
                             zone_id
                        from (select price_change_id,
                                     area_diff_id,
                                     zone_id,
                                     RANK() OVER (PARTITION BY price_change_id,
                                                               area_diff_id,
                                                               zone_id
                                                      ORDER BY dept,
                                                               class,
                                                               subclass) rnk
                                from rpm_area_diff_pc_approve_gtt
                               where rownum > 0)
                       where rnk = 1) gtt,
                     rpm_price_change rpc
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and EXISTS (select 1
                               from rpm_area_diff_exclude rade,
                                    rpm_price_change_skulist rpcs
                              where rpcs.skulist        = rpc.skulist
                                and rpcs.price_event_id = rpc.price_change_id
                                and rpcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                                and rade.area_diff_id   = gtt.area_diff_id
                                and rade.item_id        = rpcs.item
                             union all
                             select 1
                               from rpm_area_diff_exclude rade,
                                    item_master im,
                                    rpm_price_change_skulist rpcs
                              where rpcs.skulist        = rpc.skulist
                                and rpcs.price_event_id = rpc.price_change_id
                                and rpcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                and im.item_parent      = rpcs.item
                                and rade.area_diff_id   = gtt.area_diff_id
                                and rade.item_id        = im.item)
              -- PEIL
              union all
              select gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id
                from (select price_change_id,
                             area_diff_id,
                             zone_id
                        from (select price_change_id,
                                     area_diff_id,
                                     zone_id,
                                     RANK() OVER (PARTITION BY price_change_id,
                                                               area_diff_id,
                                                               zone_id
                                                      ORDER BY dept,
                                                               class,
                                                               subclass) rnk
                                from rpm_area_diff_pc_approve_gtt
                               where rownum > 0)
                       where rnk = 1) gtt,
                     rpm_price_change rpc
               where rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.link_code            is NULL
                 and EXISTS (select 1
                               from rpm_area_diff_exclude rade,
                                    rpm_merch_list_detail rmld
                              where rmld.merch_list_id = rpc.price_event_itemlist
                                and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rade.area_diff_id  = gtt.area_diff_id
                                and rade.item_id       = rmld.item
                             union all
                             select 1
                               from rpm_area_diff_exclude rade,
                                    item_master im,
                                    rpm_merch_list_detail rmld
                              where rmld.merch_list_id = rpc.price_event_itemlist
                                and rade.area_diff_id  = gtt.area_diff_id
                                and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and im.item_parent     = rmld.item
                                and rade.item_id       = im.item));

   cursor C_ITEMS_IN_LISTS_NOT_EXCLUDED (I_has_exclusion IN     OBJ_NUM_NUM_NUM_STR_TBL) is
   -- Finds which items are not affected by an exclusion in a price change where
   -- an exclusion is present
      with gtt as (select price_change_id,
                          area_diff_id,
                          zone_id
                     from (select price_change_id,
                                  area_diff_id,
                                  zone_id,
                                  RANK() OVER (PARTITION BY price_change_id,
                                                            area_diff_id,
                                                            zone_id
                                                   ORDER BY dept,
                                                            class,
                                                            subclass) rnk
                             from rpm_area_diff_pc_approve_gtt)
                    where rnk = 1)
      select OBJ_NUM_NUM_NUM_STR_REC(price_change_id,
                                     area_diff_id,
                                     zone_id,
                                     item)

        from (-- Tran Items - Skulist
              select /*+ CARDINALITY (ads 10) */
                     gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id,
                     rpcs.item
                from table(cast(I_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                     gtt,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rpcs
               where gtt.price_change_id      = ads.number_1
                 and gtt.area_diff_id         = ads.number_2
                 and gtt.zone_id              = ads.number_3
                 and rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and rpcs.skulist             = rpc.skulist
                 and rpcs.price_event_id      = rpc.price_change_id
                 and rpcs.item_level          = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade
                                  where rade.area_diff_id = gtt.area_diff_id
                                    and rade.item_id      = rpcs.item)
              union all
              -- Parent Items - Skulist
              select /*+ CARDINALITY (ads 10) */
                     gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id,
                     rpcs.item
                from table(cast(I_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                     gtt,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rpcs
               where gtt.price_change_id      = ads.number_1
                 and gtt.area_diff_id         = ads.number_2
                 and gtt.zone_id              = ads.number_3
                 and rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and rpcs.skulist             = rpc.skulist
                 and rpcs.price_event_id      = rpc.price_change_id
                 and rpcs.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and NOT EXISTS (select 1
                                   from item_master im,
                                        rpm_area_diff_exclude rade
                                  where im.item_parent    = rpcs.item
                                    and rade.area_diff_id = gtt.area_diff_id
                                    and rade.item_id      = im.item)
              union all
              -- Tran Items - PEIL
              select /*+ CARDINALITY (ads 10) */
                     gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id,
                     rmld.item
                from table(cast(I_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                     gtt,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld
               where gtt.price_change_id      = ads.number_1
                 and gtt.area_diff_id         = ads.number_2
                 and gtt.zone_id              = ads.number_3
                 and rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.link_code            is NULL
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and NOT EXISTS (select 1
                                   from rpm_area_diff_exclude rade
                                  where rade.area_diff_id = gtt.area_diff_id
                                    and rade.item_id      = rmld.item)
              union all
              -- Parent Items - PEIL
              select /*+ CARDINALITY (ads 10) */
                     gtt.price_change_id,
                     gtt.area_diff_id,
                     gtt.zone_id,
                     rmld.item
                from table(cast(I_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                     gtt,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld
               where gtt.price_change_id      = ads.number_1
                 and gtt.area_diff_id         = ads.number_2
                 and gtt.zone_id              = ads.number_3
                 and rpc.price_change_id      = gtt.price_change_id
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.link_code            is NULL
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and NOT EXISTS (select 1
                                   from item_master im,
                                        rpm_area_diff_exclude rade
                                  where im.item_parent    = rmld.item
                                    and rade.area_diff_id = gtt.area_diff_id
                                    and rade.item_id      = im.item));

FUNCTION FILTER_PCS(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                    I_primary_pc_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;

FUNCTION CREATE_AREA_DIFF_PC(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                             I_primary_pc_ids IN     OBJ_NUMERIC_ID_TABLE,
                             I_user_name      IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION CHUNK_CREATE_AREA_DIFF_PC(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_bulk_cc_pe_id    IN     NUMBER,
                                   I_user_name        IN     VARCHAR2,
                                   I_pe_sequence_id   IN     NUMBER,
                                   I_pe_thread_number IN     NUMBER)
RETURN BOOLEAN;

--------------------------------------------------------
--INTERNAL PROCEDURES
--------------------------------------------------------

FUNCTION FILTER_PCS(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                    I_primary_pc_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_AREA_DIFF_PRICE_CHANGE.FILTER_PCS';

   L_zone_ranging NUMBER(1) := NULL;

BEGIN

   insert into rpm_area_diff_pc_approve_gtt (price_change_id,
                                             area_diff_id,
                                             zone_id,
                                             dept,
                                             class,
                                             subclass,
                                             price_guide_id,
                                             auto_approve_ind,
                                             percent_apply_type,
                                             percent,
                                             pc_currency_code,
                                             pc_exchange_rate,
                                             ad_currency_code,
                                             ad_exchange_rate,
                                             currency_rtl_dec)
      -- Items
      select distinct /*+ CARDINALITY (ids 10) */
             pc.price_change_id,
             ad.area_diff_id,
             ad.zone_hier_id,
             im.dept,
             im.class,
             im.subclass,
             ad.price_guide_id,
             ad.auto_approve_ind,
             ad.percent_apply_type,
             ad.percent,
             pc_zone.currency_code,
             FIRST_VALUE(pc_curr.exchange_rate) OVER (PARTITION BY pc_curr.currency_code
                                                          ORDER BY pc_curr.effective_date desc),
             ad_zone.currency_code,
             FIRST_VALUE(ad_curr.exchange_rate) OVER (PARTITION BY ad_curr.currency_code
                                                          ORDER BY ad_curr.effective_date desc),
             c.currency_rtl_dec
        from table(cast(I_primary_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change pc,
             item_master im,
             rpm_area_diff_prim_expl adp,
             rpm_area_diff ad,
             rpm_zone pc_zone,
             rpm_zone ad_zone,
             currency_rates pc_curr,
             currency_rates ad_curr,
             currencies c
       where pc.price_change_id     = value(ids)
         and pc.reason_code         NOT IN (REASON_CODE_SYSTEM_CODE,
                                            REASON_CODE_WORKSHEET_CODE)
         and pc.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pc.zone_id             = adp.zone_hier_id
         and pc.item                = im.item
         and im.dept                = adp.dept
         and im.class               = adp.class
         and im.subclass            = adp.subclass
         and adp.area_diff_prim_id  = ad.area_diff_prim_id
         and pc.zone_id             = pc_zone.zone_id
         and pc_zone.currency_code  = pc_curr.currency_code
         and pc_curr.exchange_type  = 'C'
         and pc_curr.effective_date <= pc.effective_date
         and ad.zone_hier_id        = ad_zone.zone_id
         and ad_zone.currency_code  = ad_curr.currency_code
         and ad_curr.exchange_type  = 'C'
         and ad_curr.effective_date <= pc.effective_date
         and ad_curr.currency_code  = c.currency_code
      union all
      -- Item Lists
      select distinct /*+ CARDINALITY (ids 10) */
             pc.price_change_id,
             ad.area_diff_id,
             ad.zone_hier_id,
             im.dept,
             im.class,
             im.subclass,
             ad.price_guide_id,
             ad.auto_approve_ind,
             ad.percent_apply_type,
             ad.percent,
             pc_zone.currency_code,
             FIRST_VALUE(pc_curr.exchange_rate) OVER (PARTITION BY pc_curr.currency_code
                                                          ORDER BY pc_curr.effective_date desc),
             ad_zone.currency_code,
             FIRST_VALUE(ad_curr.exchange_rate) OVER (PARTITION BY ad_curr.currency_code
                                                          ORDER BY ad_curr.effective_date desc),
             c.currency_rtl_dec
        from table(cast(I_primary_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change pc,
             rpm_price_change_skulist rpcs,
             item_master im,
             rpm_area_diff_prim_expl adp,
             rpm_area_diff ad,
             rpm_zone pc_zone,
             rpm_zone ad_zone,
             currency_rates pc_curr,
             currency_rates ad_curr,
             currencies c
       where pc.price_change_id      = value(ids)
         and pc.skulist              is NOT NULL
         and pc.price_event_itemlist is NULL
         and pc.link_code            is NULL
         and pc.reason_code          NOT IN (REASON_CODE_SYSTEM_CODE,
                                             REASON_CODE_WORKSHEET_CODE)
         and pc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pc.skulist              = rpcs.skulist
         and rpcs.item               = im.item
         and pc.zone_id              = adp.zone_hier_id
         and im.dept                 = adp.dept
         and im.class                = adp.class
         and im.subclass             = adp.subclass
         and adp.area_diff_prim_id   = ad.area_diff_prim_id
         and pc.zone_id              = pc_zone.zone_id
         and pc_zone.currency_code   = pc_curr.currency_code
         and pc_curr.exchange_type   = 'C'
         and pc_curr.effective_date <= pc.effective_date
         and ad.zone_hier_id         = ad_zone.zone_id
         and ad_zone.currency_code   = ad_curr.currency_code
         and ad_curr.exchange_type   = 'C'
         and ad_curr.effective_date <= pc.effective_date
         and ad_curr.currency_code   = c.currency_code
      union all
      -- PEILS
      select distinct /*+ CARDINALITY (ids 10) */
             pc.price_change_id,
             ad.area_diff_id,
             ad.zone_hier_id,
             im.dept,
             im.class,
             im.subclass,
             ad.price_guide_id,
             ad.auto_approve_ind,
             ad.percent_apply_type,
             ad.percent,
             pc_zone.currency_code,
             FIRST_VALUE(pc_curr.exchange_rate) OVER (PARTITION BY pc_curr.currency_code
                                                          ORDER BY pc_curr.effective_date desc),
             ad_zone.currency_code,
             FIRST_VALUE(ad_curr.exchange_rate) OVER (PARTITION BY ad_curr.currency_code
                                                          ORDER BY ad_curr.effective_date desc),
             c.currency_rtl_dec
        from table(cast(I_primary_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change pc,
             rpm_merch_list_detail rmld,
             item_master im,
             rpm_area_diff_prim_expl adp,
             rpm_area_diff ad,
             rpm_zone pc_zone,
             rpm_zone ad_zone,
             currency_rates pc_curr,
             currency_rates ad_curr,
             currencies c
       where pc.price_change_id      = value(ids)
         and pc.skulist              is NULL
         and pc.price_event_itemlist is NOT NULL
         and pc.link_code            is NULL
         and pc.reason_code          NOT IN (REASON_CODE_SYSTEM_CODE,
                                             REASON_CODE_WORKSHEET_CODE)
         and pc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pc.price_event_itemlist = rmld.merch_list_id
         and rmld.item               = im.item
         and pc.zone_id              = adp.zone_hier_id
         and im.dept                 = adp.dept
         and im.class                = adp.class
         and im.subclass             = adp.subclass
         and adp.area_diff_prim_id   = ad.area_diff_prim_id
         and pc.zone_id              = pc_zone.zone_id
         and pc_zone.currency_code   = pc_curr.currency_code
         and pc_curr.exchange_type   = 'C'
         and pc_curr.effective_date  <= pc.effective_date
         and ad.zone_hier_id         = ad_zone.zone_id
         and ad_zone.currency_code   = ad_curr.currency_code
         and ad_curr.exchange_type   = 'C'
         and ad_curr.effective_date  <= pc.effective_date
         and ad_curr.currency_code   = c.currency_code
      union all
      -- This query is for link code price changes
      -- From the inside out
       -- 1 - Explode the price chagnes to the item/loc level from the link_code/zone level
       -- 2 - Get the primary area diffs that cover the item locs
       -- 3 - Ensure that every primary area diff covers every item loc from the price change
      --Next ensure that the secondary area item/locs are all in covered by the hierarchy of the area diff
      select distinct inline2.price_change_id,
             ad.area_diff_id,
             ad.zone_hier_id,
             inline2.dept,
             inline2.class,
             inline2.subclass,
             ad.price_guide_id,
             ad.auto_approve_ind,
             ad.percent_apply_type,
             ad.percent,
             pc_zone.currency_code,
             FIRST_VALUE(pc_curr.exchange_rate) OVER (PARTITION BY pc_curr.currency_code
                                                          ORDER BY pc_curr.effective_date desc),
             ad_zone.currency_code,
             FIRST_VALUE(ad_curr.exchange_rate) OVER (PARTITION BY ad_curr.currency_code
                                                          ORDER BY ad_curr.effective_date desc),
             c.currency_rtl_dec
        from rpm_area_diff ad,
             rpm_zone pc_zone,
             rpm_zone ad_zone,
             currency_rates pc_curr,
             currency_rates ad_curr,
             currencies c,
            (select distinct inline.price_change_id,
                    inline.link_code,
                    inline.zone_id,
                    inline.effective_date,
                    NVL(adp.area_diff_prim_id, -999) area_diff_prim_id,
                    inline.dept,
                    inline.class,
                    inline.subclass,
                    COUNT(distinct (inline.item||'~ ~'||inline.location)) OVER (PARTITION BY inline.price_change_id,
                                                                                             adp.area_diff_prim_id) ad_item_loc_count,
                    COUNT(distinct (inline.item||'~ ~'||inline.location)) OVER (PARTITION BY inline.price_change_id) item_loc_count
               from rpm_area_diff_prim adp,
                    (select distinct /*+ CARDINALITY (ids 10) */
                            pc.price_change_id,
                            pc.link_code,
                            pc.zone_id,
                            pc.effective_date,
                            gtt.location,
                            gtt.item,
                            gtt.dept,
                            gtt.class,
                            gtt.subclass
                       from table(cast(I_primary_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            rpm_price_change pc,
                            rpm_future_retail_gtt gtt
                      where pc.price_change_id = value(ids)
                        and pc.reason_code     NOT IN (REASON_CODE_SYSTEM_CODE,
                                                       REASON_CODE_WORKSHEET_CODE)
                        and pc.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and pc.link_code       is NOT NULL
                        and gtt.price_event_id = pc.price_change_id) inline
              where inline.zone_id  = adp.zone_hier_id (+)
                and inline.dept     = adp.dept (+)
                and inline.class    = NVL(adp.class (+), inline.class)
                and inline.subclass = NVL(adp.subclass (+), inline.subclass)) inline2
       where inline2.ad_item_loc_count = inline2.item_loc_count
         and inline2.area_diff_prim_id = ad.area_diff_prim_id
         and NOT EXISTS (select 'x'
                           from rpm_link_code_attribute lca,
                                item_master im,
                                rpm_zone_location zl
                          where lca.link_code = inline2.link_code
                            and zl.zone_id    = ad.zone_hier_id
                            and lca.location  = zl.location
                            and im.item       = lca.item
                            and NOT (    im.dept     = inline2.dept
                                     and im.class    = inline2.class
                                     and im.subclass = inline2.subclass))
         and inline2.zone_id         = pc_zone.zone_id
         and pc_zone.currency_code   = pc_curr.currency_code
         and pc_curr.exchange_type   = 'C'
         and pc_curr.effective_date <= inline2.effective_date
         and ad.zone_hier_id         = ad_zone.zone_id
         and ad_zone.currency_code   = ad_curr.currency_code
         and ad_curr.exchange_type   = 'C'
         and ad_curr.effective_date <= inline2.effective_date
         and ad_curr.currency_code   = c.currency_code;

   select zone_ranging into L_zone_ranging
     from rpm_system_options;

   if L_zone_ranging = 1 then

      -- Remove potential area diffs where there are no item/locs ranged
      -- to the new area diff zone and the zone ranging system option is = 1
      delete
        from rpm_area_diff_pc_approve_gtt
       where price_change_id IN (-- Transaction Level Item
                                 select gtt.price_change_id
                                   from rpm_area_diff_pc_approve_gtt gtt,
                                        rpm_price_change rpc,
                                        item_master im
                                  where rpc.price_change_id      = gtt.price_change_id
                                    and rpc.skulist              is NULL
                                    and rpc.price_event_itemlist is NULL
                                    and rpc.link_code            is NULL
                                    and im.item                  = rpc.item
                                    and im.item_level            = im.tran_level
                                    and NOT EXISTS (select 1
                                                      from rpm_item_loc ril,
                                                           rpm_zone_location rzl
                                                     where ril.dept    = im.dept
                                                       and ril.item    = im.item
                                                       and rzl.zone_id = gtt.zone_id
                                                       and ril.loc     = rzl.location)
                                 union all
                                 -- Parent Level Item and Parent Diff Level Item
                                 select gtt.price_change_id
                                   from rpm_area_diff_pc_approve_gtt gtt,
                                        rpm_price_change rpc,
                                        item_master im
                                  where rpc.price_change_id      = gtt.price_change_id
                                    and rpc.skulist              is NULL
                                    and rpc.price_event_itemlist is NULL
                                    and rpc.link_code            is NULL
                                    and im.item                  = rpc.item
                                    and im.item_level + 1        = im.tran_level
                                    and NOT EXISTS (select 1
                                                      from rpm_item_loc ril,
                                                           rpm_zone_location rzl,
                                                           item_master im1
                                                     where ril.dept        = im1.dept
                                                       and im1.item_parent = rpc.item
                                                       and (   rpc.diff_id         is NULL
                                                            or (    rpc.diff_id    is NOT NULL
                                                                and (   im1.diff_1 = rpc.diff_id
                                                                     or im1.diff_2 = rpc.diff_id
                                                                     or im1.diff_3 = rpc.diff_id
                                                                     or im1.diff_4 = rpc.diff_id)))
                                                       and ril.item        = im1.item
                                                       and rzl.zone_id     = gtt.zone_id
                                                       and ril.loc         = rzl.location)
                                 union all
                                 -- Item Lists
                                 select gtt.price_change_id
                                   from rpm_area_diff_pc_approve_gtt gtt,
                                        rpm_price_change rpc
                                  where rpc.price_change_id      = gtt.price_change_id
                                    and rpc.skulist              is NOT NULL
                                    and rpc.price_event_itemlist is NULL
                                    and rpc.link_code            is NULL
                                    and NOT EXISTS (select 1
                                                      from rpm_price_change_skulist rpcs,
                                                           rpm_item_loc ril,
                                                           rpm_zone_location rzl,
                                                           item_master im
                                                     where rpc.price_change_id = rpcs.price_event_id
                                                       and rpc.skulist         = rpcs.skulist
                                                       and rpcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                                                       and rpcs.item           = im.item
                                                       and ril.dept            = im.dept
                                                       and ril.item            = im.item
                                                       and rzl.zone_id         = gtt.zone_id
                                                       and ril.loc             = rzl.location
                                                    union all
                                                    select 1
                                                      from rpm_price_change_skulist rpcs,
                                                           rpm_item_loc ril,
                                                           rpm_zone_location rzl1,
                                                           item_master im1
                                                     where rpc.price_change_id = rpcs.price_event_id
                                                       and rpc.skulist         = rpcs.skulist
                                                       and rpcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                                       and im1.item_parent     = rpcs.item
                                                       and ril.dept            = im1.dept
                                                       and ril.item            = im1.item
                                                       and rzl1.zone_id        = gtt.zone_id
                                                       and ril.loc             = rzl1.location)
                                 union all
                                 -- PEILs
                                 select gtt.price_change_id
                                   from rpm_area_diff_pc_approve_gtt gtt,
                                        rpm_price_change rpc
                                  where rpc.price_change_id      = gtt.price_change_id
                                    and rpc.price_event_itemlist is NOT NULL
                                    and rpc.skulist              is NULL
                                    and rpc.link_code            is NULL
                                    and NOT EXISTS (select 1
                                                      from rpm_merch_list_detail rmld,
                                                           rpm_item_loc ril,
                                                           rpm_zone_location rzl,
                                                           item_master im
                                                     where rpc.price_event_itemlist = rmld.merch_list_id
                                                       and ril.dept                 = im.dept
                                                       and im.item                  = rmld.item
                                                       and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                                       and ril.item                 = im.item
                                                       and rzl.zone_id              = gtt.zone_id
                                                       and ril.loc                  = rzl.location
                                                    union all
                                                    select 1
                                                      from rpm_merch_list_detail rmld,
                                                           rpm_item_loc ril,
                                                           rpm_zone_location rzl1,
                                                           item_master im1
                                                     where rpc.price_event_itemlist = rmld.merch_list_id
                                                       and ril.dept                 = im1.dept
                                                       and im1.item_parent          = rmld.item
                                                       and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                                       and ril.item                 = im1.item
                                                       and rzl1.zone_id             = gtt.zone_id
                                                       and ril.loc                  = rzl1.location));

   end if;

   -- make sure one price change only uses one class_vat_ind
   delete
     from rpm_area_diff_pc_approve_gtt
    where price_change_id = (select price_change_id
                               from (select gtt.price_change_id,
                                            RANK() OVER (PARTITION BY gtt.price_change_id,
                                                                      gtt.area_diff_id,
                                                                      gtt.zone_id
                                                             ORDER BY c.class_vat_ind) rnk
                                       from rpm_area_diff_pc_approve_gtt gtt,
                                            class c
                                      where gtt.dept  = c.dept
                                        and gtt.class = c.class)
                       where rnk > 1);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return FALSE;

END FILTER_PCS;

--------------------------------------------------------

FUNCTION CREATE_AREA_DIFF_PC(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                             I_primary_pc_ids IN     OBJ_NUMERIC_ID_TABLE,
                             I_user_name      IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program                     VARCHAR2(50)            := 'RPM_AREA_DIFF_PRICE_CHANGE.CREATE_AREA_DIFF_PC';
   L_ad_code                     RPM_CODES.CODE_ID%TYPE  := NULL;
   L_adaa_code                   RPM_CODES.CODE_ID%TYPE  := NULL;
   L_ignore_constraints          NUMBER                  := 0;
   L_no_exclusion                OBJ_NUM_NUM_STR_TBL     := NULL;
   L_has_exclusion               OBJ_NUM_NUM_NUM_STR_TBL := NULL;
   L_items_in_lists_not_excluded OBJ_NUM_NUM_NUM_STR_TBL := NULL;

BEGIN

   open C_AD_CODE_TYPE;
   fetch C_AD_CODE_TYPE into L_ad_code;
   close C_AD_CODE_TYPE;

   open C_ADAA_CODE_TYPE;
   fetch C_ADAA_CODE_TYPE into L_adaa_code;
   close C_ADAA_CODE_TYPE;

   select def_wksht_promo_const_ind into L_ignore_constraints
     from rpm_system_options_def;

   open C_NO_EXCLUSION;
   fetch C_NO_EXCLUSION BULK COLLECT into L_no_exclusion;
   close C_NO_EXCLUSION;

   delete rpm_id_date_gtt;

   if L_no_exclusion is NOT NULL and
      L_no_exclusion.COUNT > 0 then

      insert all
         when 1 = 1 then
             into rpm_price_change (price_change_id,
                                    price_change_display_id,
                                    state,
                                    reason_code,
                                    item,
                                    diff_id,
                                    zone_id,
                                    zone_node_type,
                                    link_code,
                                    effective_date,
                                    change_type,
                                    change_amount,
                                    change_currency,
                                    change_selling_uom,
                                    null_multi_ind,
                                    price_guide_id,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    area_diff_parent_id,
                                    ignore_constraints)
                            values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                                    RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                                    state,
                                    reason_code,
                                    item,
                                    diff_id,
                                    zone_id,
                                    zone_node_type,
                                    link_code,
                                    effective_date,
                                    change_type,
                                    change_amount,
                                    change_currency,
                                    change_selling_uom,
                                    null_multi_ind,
                                    price_guide_id,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    area_diff_parent_id,
                                    ignore_constraints)
         when auto_approve_ind = 1 then
            into numeric_id_gtt (numeric_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         when 2 = 2 then
            into rpm_id_date_gtt (price_event_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         select /*+ CARDINALITY (ads 10) */
                DECODE(new_pc_ids.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(new_pc_ids.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                new_pc_ids.item,
                pc.diff_id,
                new_pc_ids.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.link_code,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(new_pc_ids.percent_apply_type,
                             0, (case
                                    when new_pc_ids.item_level = new_pc_ids.tran_level then
                                       inner.selling_retail_tran
                                    else
                                       inner.selling_retail_parent
                                 end) * (1 + new_pc_ids.percent / 100),
                             1, (case
                                    when new_pc_ids.item_level = new_pc_ids.tran_level then
                                       inner.selling_retail_tran
                                    else
                                       inner.selling_retail_parent
                                 end) * (1 - new_pc_ids.percent / 100),
                             2, (case
                                    when new_pc_ids.item_level = new_pc_ids.tran_level then
                                       inner.selling_retail_tran
                                    else
                                       inner.selling_retail_parent
                                 end)) *
                                       new_pc_ids.ad_exchange_rate / new_pc_ids.pc_exchange_rate, new_pc_ids.currency_rtl_dec) change_amount,
                new_pc_ids.ad_currency_code change_currency,
                inner.selling_uom change_selling_uom,
                0 null_multi_ind,
                new_pc_ids.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate create_date,
                pc.create_id,
                new_pc_ids.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                new_pc_ids.auto_approve_ind
           from (with gtt as
                    (select price_change_id,
                            area_diff_id,
                            zone_id,
                            auto_approve_ind,
                            price_guide_id,
                            ad_currency_code,
                            ad_exchange_rate,
                            pc_exchange_rate,
                            currency_rtl_dec,
                            percent,
                            percent_apply_type,
                            dept,
                            class,
                            subclass
                       from (select price_change_id,
                                    area_diff_id,
                                    zone_id,
                                    auto_approve_ind,
                                    price_guide_id,
                                    ad_currency_code,
                                    ad_exchange_rate,
                                    pc_exchange_rate,
                                    currency_rtl_dec,
                                    percent,
                                    percent_apply_type,
                                    dept,
                                    class,
                                    subclass,
                                    RANK() OVER (PARTITION BY price_change_id,
                                                              area_diff_id,
                                                              zone_id,
                                                              dept,
                                                              class,
                                                              subclass
                                                     ORDER BY rowid) rnk
                               from rpm_area_diff_pc_approve_gtt
                              where rownum > 0)
                      where rnk = 1)
                (-- Items
                 select /*+ CARDINALITY (ids 10) */
                        rpc.price_change_id,
                        rpc.item,
                        gtt.area_diff_id,
                        gtt.zone_id,
                        gtt.auto_approve_ind,
                        gtt.price_guide_id,
                        gtt.ad_currency_code,
                        gtt.ad_exchange_rate,
                        gtt.pc_exchange_rate,
                        gtt.currency_rtl_dec,
                        gtt.percent,
                        gtt.percent_apply_type,
                        im.item_level,
                        im.tran_level
                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL)) ads,
                        rpm_price_change rpc,
                        gtt,
                        item_master im
                  where rpc.price_change_id      = ads.number_1
                    and rpc.item                 is NOT NULL
                    and rpc.skulist              is NULL
                    and rpc.price_event_itemlist is NULL
                    and rpc.link_code            is NULL
                    and gtt.price_change_id      = rpc.price_change_id
                    and ads.number_2             = gtt.area_diff_id
                    and im.item                  = rpc.item
                    and im.dept                  = gtt.dept
                    and im.class                 = gtt.class
                    and im.subclass              = gtt.subclass
                 union all
                 -- Item Lists
                 select /*+ CARDINALITY (ids 10) */
                        rpc.price_change_id,
                        rpcs.item,
                        gtt.area_diff_id,
                        gtt.zone_id,
                        gtt.auto_approve_ind,
                        gtt.price_guide_id,
                        gtt.ad_currency_code,
                        gtt.ad_exchange_rate,
                        gtt.pc_exchange_rate,
                        gtt.currency_rtl_dec,
                        gtt.percent,
                        gtt.percent_apply_type,
                        im.item_level,
                        im.tran_level
                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL)) ads,
                        rpm_price_change rpc,
                        rpm_price_change_skulist rpcs,
                        gtt,
                        item_master im
                  where rpc.price_change_id         = ads.number_1
                    and rpc.skulist                 is NOT NULL
                    and rpc.price_event_itemlist    is NULL
                    and rpc.item                    is NULL
                    and rpc.link_code               is NULL
                    and rpc.sys_generated_exclusion = 0
                    and rpcs.skulist                = rpc.skulist
                    and rpcs.price_event_id         = rpc.price_change_id
                    and gtt.price_change_id         = rpc.price_change_id
                    and ads.number_2                = gtt.area_diff_id
                    and im.item                     = rpcs.item
                    and im.dept                     = gtt.dept
                    and im.class                    = gtt.class
                    and im.subclass                 = gtt.subclass
                 union all
                 -- PEILs
                 select /*+ CARDINALITY (ids 10) */
                        rpc.price_change_id,
                        rmld.item,
                        gtt.area_diff_id,
                        gtt.zone_id,
                        gtt.auto_approve_ind,
                        gtt.price_guide_id,
                        gtt.ad_currency_code,
                        gtt.ad_exchange_rate,
                        gtt.pc_exchange_rate,
                        gtt.currency_rtl_dec,
                        gtt.percent,
                        gtt.percent_apply_type,
                        im.item_level,
                        im.tran_level
                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL)) ads,
                        rpm_price_change rpc,
                        rpm_merch_list_detail rmld,
                        gtt,
                        item_master im
                  where rpc.price_change_id      = ads.number_1
                    and rpc.price_event_itemlist is NOT NULL
                    and rpc.skulist              is NULL
                    and rpc.item                 is NULL
                    and rpc.link_code            is NULL
                    and rmld.merch_list_id       = rpc.price_event_itemlist
                    and gtt.price_change_id      = rpc.price_change_id
                    and ads.number_2             = gtt.area_diff_id
                    and im.item                  = rmld.item
                    and im.dept                  = gtt.dept
                    and im.class                 = gtt.class
                    and im.subclass              = gtt.subclass)) new_pc_ids,
                rpm_price_change pc,
                (select fr.price_change_id,
                        fr.item,
                        fr.location,
                        im.item_parent,
                        MIN(fr.selling_uom) OVER (PARTITION BY fr.price_change_id,
                                                               fr.item) selling_uom,
                        AVG(fr.selling_retail) OVER (PARTITION BY fr.price_change_id,
                                                                  fr.item) selling_retail_tran,
                        AVG(fr.selling_retail) OVER (PARTITION BY fr.price_change_id,
                                                                  im.item_parent) selling_retail_parent,
                        RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                         ORDER BY im.item) parent_rank,
                        RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                         ORDER BY fr.location) loc_rank
                   from (select fr.price_change_id,
                                fr.item,
                                fr.location,
                                fr.selling_retail,
                                fr.selling_uom,
                                RANK() OVER (PARTITION BY fr.price_change_id,
                                                          fr.item,
                                                          fr.location
                                                 ORDER BY fr.promo_dtl_id) fr_rank
                           from (select /*+ CARDINALITY (ads 10) */
                                        distinct number_1 price_change_id
                                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL))) ads,
                                rpm_fr_item_loc_expl_gtt fr
                          where fr.price_change_id = ads.price_change_id) fr,
                        item_master im
                  where fr.fr_rank  = 1
                    and fr.item     = im.item) inner
          where pc.price_change_id    = new_pc_ids.price_change_id
            and inner.price_change_id = new_pc_ids.price_change_id
            and inner.loc_rank        = 1
            and (   (    inner.item_parent = new_pc_ids.item
                     and inner.parent_rank = 1)
                 or (    inner.item        = new_pc_ids.item));

   end if;

   open C_HAS_EXCLUSION;
   fetch C_HAS_EXCLUSION BULK COLLECT into L_has_exclusion;
   close C_HAS_EXCLUSION;

   if L_has_exclusion is NOT NULL and
      L_has_exclusion.COUNT > 0 then

      open C_ITEMS_IN_LISTS_NOT_EXCLUDED(L_has_exclusion);
      fetch C_ITEMS_IN_LISTS_NOT_EXCLUDED BULK COLLECT into L_items_in_lists_not_excluded;
      close C_ITEMS_IN_LISTS_NOT_EXCLUDED;

      -- If a Parent Item, Parent Diff, Item List, or PEIL Price Change has an Area Diff Exclusion,
      -- then the Area Differential Price Change will be created at the Tran Item Level

      insert all
         when 1 = 1 then
             into rpm_price_change (price_change_id,
                                    price_change_display_id,
                                    state,
                                    reason_code,
                                    item,
                                    zone_id,
                                    zone_node_type,
                                    effective_date,
                                    change_type,
                                    change_amount,
                                    change_currency,
                                    change_selling_uom,
                                    null_multi_ind,
                                    price_guide_id,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    area_diff_parent_id,
                                    ignore_constraints)
                            values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                                    RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                                    state,
                                    reason_code,
                                    item,
                                    zone_id,
                                    zone_node_type,
                                    effective_date,
                                    change_type,
                                    change_amount,
                                    change_currency,
                                    change_selling_uom,
                                    null_multi_ind,
                                    price_guide_id,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    area_diff_parent_id,
                                    ignore_constraints)
         when auto_approve_ind = 1 then
            into numeric_id_gtt (numeric_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         when 2 = 2 then
            into rpm_id_date_gtt (price_event_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         with gtt as
            (select price_change_id,
                    area_diff_id,
                    zone_id,
                    auto_approve_ind,
                    price_guide_id,
                    ad_currency_code,
                    ad_exchange_rate,
                    pc_exchange_rate,
                    currency_rtl_dec,
                    percent,
                    percent_apply_type,
                    dept,
                    class,
                    subclass
               from (select price_change_id,
                            area_diff_id,
                            zone_id,
                            auto_approve_ind,
                            price_guide_id,
                            ad_currency_code,
                            ad_exchange_rate,
                            pc_exchange_rate,
                            currency_rtl_dec,
                            percent,
                            percent_apply_type,
                            dept,
                            class,
                            subclass,
                            RANK() OVER (PARTITION BY price_change_id,
                                                      area_diff_id,
                                                      zone_id,
                                                      dept,
                                                      class,
                                                      subclass
                                             ORDER BY rowid) rnk
                       from rpm_area_diff_pc_approve_gtt)
              where rnk = 1),
         inner as
            (select fr.price_change_id,
                    fr.item,
                    fr.location,
                    im.item_parent,
                    MIN(fr.selling_uom) OVER (PARTITION BY fr.price_change_id,
                                                           fr.item) selling_uom,
                    AVG(fr.selling_retail) OVER (PARTITION BY fr.price_change_id,
                                                              fr.item) selling_retail_tran,
                    AVG(fr.selling_retail) OVER (PARTITION BY fr.price_change_id,
                                                              im.item_parent) selling_retail_parent,
                    RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                     ORDER BY im.item) parent_rank,
                    RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                     ORDER BY fr.location) loc_rank
               from (select fr.price_change_id,
                            fr.item,
                            fr.location,
                            fr.selling_retail,
                            fr.selling_uom,
                            RANK() OVER (PARTITION BY fr.price_change_id,
                                                      fr.item,
                                                      fr.location
                                             ORDER BY fr.promo_dtl_id) fr_rank
                       from (select /*+ CARDINALITY (ads 10) */
                                    number_1 price_change_id,
                                    RANK() OVER (PARTITION BY number_1
                                                     ORDER BY number_2,
                                                              string) ads_rank
                               from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL))) ads,
                            rpm_fr_item_loc_expl_gtt fr
                      where ads_rank           = 1
                        and fr.price_change_id = ads.price_change_id) fr,
                    item_master im
              where fr.fr_rank = 1
                and fr.item    = im.item)
         -- Parent Items
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                im.item                                           item,
                gtt.zone_id                                       zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date                                 effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.selling_retail_tran * (1 + gtt.percent / 100),
                             1, inner.selling_retail_tran * (1 - gtt.percent / 100),
                             2, inner.selling_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.selling_uom                                 change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id                                price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id                                      create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind                              auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.price_change_id      = inner.price_change_id
            and pc.skulist              is NULL
            and pc.price_event_itemlist is NULL
            and pc.link_code            is NULL
            and im.item_parent          = pc.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and inner.loc_rank          = 1
            and inner.item              = im.item
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and (   pc.diff_id is NULL
                 or (    pc.diff_id is NOT NULL
                     and (   im.diff_1 = pc.diff_id
                          or im.diff_2 = pc.diff_id
                          or im.diff_3 = pc.diff_id
                          or im.diff_4 = pc.diff_id)))
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
         union all
         -- Item Lists Parent Items
         select /*+ CARDINALITY (ads 10) */
                distinct DECODE(gtt.auto_approve_ind,
                                1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                                RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.selling_retail_tran * (1 + gtt.percent / 100),
                             1, inner.selling_retail_tran * (1 - gtt.percent / 100),
                             2, inner.selling_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code change_currency,
                inner.selling_uom change_selling_uom,
                0 null_multi_ind,
                gtt.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate,
                pc.create_id,
                gtt.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_price_change_skulist rpcs,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NOT NULL
            and pc.price_event_itemlist is NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.skulist              = rpcs.skulist
            and pc.price_change_id      = rpcs.price_event_id
            and rpcs.item_level         = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
            and im.item_parent          = rpcs.item
            and inner.loc_rank          = 1
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rpcs.item)
         union all
         --Item Lists Tran Items
         select /*+ CARDINALITY (ads 10) */
                distinct DECODE(gtt.auto_approve_ind,
                                1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                                RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.selling_retail_tran * (1 + gtt.percent / 100),
                             1, inner.selling_retail_tran * (1 - gtt.percent / 100),
                             2, inner.selling_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code change_currency,
                inner.selling_uom change_selling_uom,
                0 null_multi_ind,
                gtt.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate,
                pc.create_id,
                gtt.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_price_change_skulist rpcs,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NOT NULL
            and pc.price_event_itemlist is NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.skulist              = rpcs.skulist
            and pc.price_change_id      = rpcs.price_event_id
            and rpcs.item_level         = RPM_CONSTANTS.IL_ITEM_LEVEL
            and im.item                 = rpcs.item
            and inner.loc_rank          = 1
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rpcs.item)
         union all
         -- PEIL Parent Items
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.selling_retail_tran * (1 + gtt.percent / 100),
                             1, inner.selling_retail_tran * (1 - gtt.percent / 100),
                             2, inner.selling_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code change_currency,
                inner.selling_uom change_selling_uom,
                0 null_multi_ind,
                gtt.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate create_date,
                pc.create_id,
                gtt.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_merch_list_detail rmld,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NULL
            and pc.price_event_itemlist is NOT NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
            and im.item_parent          = rmld.item
            and inner.loc_rank          = 1
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rmld.item)
         union all
         --PEILS Tran Items
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.selling_retail_tran * (1 + gtt.percent / 100),
                             1, inner.selling_retail_tran * (1 - gtt.percent / 100),
                             2, inner.selling_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code change_currency,
                inner.selling_uom change_selling_uom,
                0 null_multi_ind,
                gtt.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate create_date,
                pc.create_id,
                gtt.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_merch_list_detail rmld,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NULL
            and pc.price_event_itemlist is NOT NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
            and im.item                 = rmld.item
            and inner.loc_rank          = 1
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rmld.item)
         union all
         -- Insert items that are unaffected by any area diff exclusions
         -- (avoid breaking them down to tran items)
         select /*+ CARDINALITY (ids 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                ids.string                                          item,
                gtt.zone_id                                       zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date                                 effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, DECODE(im.item_level - im.tran_level,
                                       0, inner.selling_retail_tran,
                                       inner.selling_retail_parent) * (1 + gtt.percent / 100),
                             1, DECODE(im.item_level - im.tran_level,
                                       0, inner.selling_retail_tran,
                                       inner.selling_retail_parent) * (1 - gtt.percent / 100),
                             2, DECODE(im.item_level - im.tran_level,
                                       0, inner.selling_retail_tran,
                                       inner.selling_retail_parent)) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.selling_uom                                 change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id                                price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id                                      create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind                              auto_approve_ind
           from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids,
                gtt,
                rpm_price_change pc,
                item_master im,
                rpm_zone_location rzl,
                inner
          where ids.number_1       = gtt.price_change_id
            and ids.number_2       = gtt.area_diff_id
            and ids.number_3       = gtt.zone_id
            and pc.price_change_id = gtt.price_change_id
            and ids.string         = im.item
            and pc.price_change_id = inner.price_change_id
            and im.dept            = gtt.dept
            and im.class           = gtt.class
            and im.subclass        = gtt.subclass
            and pc.zone_id         = rzl.zone_id
            and rzl.location       = inner.location
            and inner.loc_rank     = 1
            and (   (    inner.item_parent = im.item
                     and inner.parent_rank = 1)
                 or (    inner.item        = im.item));

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return FALSE;

END CREATE_AREA_DIFF_PC;

--------------------------------------------------------
--PUBLIC PROCEDURES
--------------------------------------------------------

FUNCTION APPLY_AREA_DIFFS_TO_PC(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                                I_primary_pc_ids IN    OBJ_NUMERIC_ID_TABLE,
                                I_user_name      IN    VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_AREA_DIFF_PRICE_CHANGE.APPLY_AREA_DIFFS_TO_PC';

BEGIN

   delete from numeric_id_gtt;

   if FILTER_PCS(O_cc_error_tbl,
                 I_primary_pc_ids) = FALSE then
      return FALSE;
   end if;

   if CREATE_AREA_DIFF_PC(O_cc_error_tbl,
                          I_primary_pc_ids,
                          I_user_name) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return FALSE;
END APPLY_AREA_DIFFS_TO_PC;

--------------------------------------------------------

FUNCTION UNAPPROVE_AREA_DIFF_PC(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                                O_secondary_ind              OUT NUMBER,
                                I_primary_pc_ids          IN     OBJ_NUMERIC_ID_TABLE,
                                I_secondary_bulk_cc_pe_id IN     NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)           := 'RPM_AREA_DIFF_PRICE_CHANGE.UNAPPROVE_AREA_DIFF_PC';
   L_ad_code   RPM_CODES.CODE_ID%TYPE := NULL;
   L_adaa_code RPM_CODES.CODE_ID%TYPE := NULL;

   L_pc_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor DELETE_PC_IDS is
      select /*+ CARDINALITY(ids 10) */
             rpc.price_change_id
        from table(cast(I_primary_pc_ids as obj_numeric_id_table)) ids,
             rpm_price_change rpc
       where rpc.area_diff_parent_id = value(ids)
         and rpc.reason_code         IN (L_ad_code,
                                         L_adaa_code)
         and rpc.state               = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE;

BEGIN

   O_secondary_ind := 0;

   delete from numeric_id_gtt;
   delete from rpm_id_date_gtt;

   select code_id into L_ad_code
     from rpm_codes
    where code = 'AREADIFF';

   select code_id into L_adaa_code
     from rpm_codes
    where code = 'AREADIFFAA';

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
   select /*+ CARDINALITY(ids 10) */
          I_secondary_bulk_cc_pe_id,
          rpc.price_change_id,
          RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
     from table(cast(I_primary_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
          rpm_price_change rpc
    where rpc.area_diff_parent_id = value(ids)
      and rpc.reason_code         = L_adaa_code
      and rpc.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE;

   if SQL%ROWCOUNT > 0 then
      O_secondary_ind := 1;
   end if;

   open DELETE_PC_IDS;
   fetch DELETE_PC_IDS BULK COLLECT into L_pc_ids;
   close DELETE_PC_IDS;

   if L_pc_ids.COUNT > 0 then

      if DELETE_AREA_DIFF_PC(O_cc_error_tbl,
                             L_pc_ids) = FALSE then
         return FALSE;
      end if;

   end if;

   -- detach price changes generated from area diffs from their driving price change
   merge into rpm_price_change target
   using (select /*+ CARDINALITY(ids 10) */
                 rpc.price_change_id
            from table(cast(I_primary_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_price_change rpc
           where rpc.area_diff_parent_id = value(ids)
             and rpc.reason_code         IN (L_ad_code,
                                             L_adaa_code)
             and link_code               is NULL) source
      on (target.price_change_id = source.price_change_id)
    when MATCHED then
       update
          set area_diff_parent_id = NULL;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
   return FALSE;

END UNAPPROVE_AREA_DIFF_PC;

--------------------------------------------------------

FUNCTION DELETE_AREA_DIFF_PC(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                             I_unapproved_pc_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)           := 'RPM_AREA_DIFF_PRICE_CHANGE.DELETE_AREA_DIFF_PC';
   L_ad_code   RPM_CODES.CODE_ID%TYPE := NULL;
   L_adaa_code RPM_CODES.CODE_ID%TYPE := NULL;

BEGIN

   select code_id into L_ad_code
     from rpm_codes
    where code = 'AREADIFF';

   select code_id into L_adaa_code
     from rpm_codes
    where code = 'AREADIFFAA';

   delete
     from rpm_price_change rpc
    where rpc.price_change_id IN (select /*+ CARDINALITY(ids 10) */
                                         value(ids)
                                    from table(cast(I_unapproved_pc_ids as obj_numeric_id_table)) ids)
      and rpc.reason_code     IN (L_ad_code,
                                  L_adaa_code);

  return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return FALSE;

END DELETE_AREA_DIFF_PC;

--------------------------------------------------------

FUNCTION CHUNK_CREATE_AREA_DIFF_PC(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_bulk_cc_pe_id    IN     NUMBER,
                                   I_user_name        IN     VARCHAR2,
                                   I_pe_sequence_id   IN     NUMBER,
                                   I_pe_thread_number IN     NUMBER)
RETURN BOOLEAN IS

   L_program                     VARCHAR2(70)            := 'RPM_AREA_DIFF_PRICE_CHANGE.CHUNK_CREATE_AREA_DIFF_PC';
   L_ad_code                     RPM_CODES.CODE_ID%TYPE  := NULL;
   L_adaa_code                   RPM_CODES.CODE_ID%TYPE  := NULL;
   L_ignore_constraints          NUMBER                  := 0;
   L_no_exclusion                OBJ_NUM_NUM_STR_TBL     := NULL;
   L_has_exclusion               OBJ_NUM_NUM_NUM_STR_TBL := NULL;
   L_items_in_lists_not_excluded OBJ_NUM_NUM_NUM_STR_TBL := NULL;

BEGIN

   open C_AD_CODE_TYPE;
   fetch C_AD_CODE_TYPE into L_ad_code;
   close C_AD_CODE_TYPE;

   open C_ADAA_CODE_TYPE;
   fetch C_ADAA_CODE_TYPE into L_adaa_code;
   close C_ADAA_CODE_TYPE;

   select def_wksht_promo_const_ind into L_ignore_constraints
     from rpm_system_options_def;

   open C_NO_EXCLUSION;
   fetch C_NO_EXCLUSION BULK COLLECT into L_no_exclusion;
   close C_NO_EXCLUSION;

   delete from numeric_id_gtt;

   delete from rpm_id_date_gtt;

   if L_no_exclusion is NOT NULL and
      L_no_exclusion.COUNT > 0 then

      insert all
         when 1 = 1 then
            into rpm_price_change (price_change_id,
                                   price_change_display_id,
                                   state,
                                   reason_code,
                                   item,
                                   diff_id,
                                   zone_id,
                                   zone_node_type,
                                   link_code,
                                   effective_date,
                                   change_type,
                                   change_amount,
                                   change_currency,
                                   change_selling_uom,
                                   null_multi_ind,
                                   price_guide_id,
                                   vendor_funded_ind,
                                   create_date,
                                   create_id,
                                   area_diff_parent_id,
                                   ignore_constraints)
               values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                       RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                       state,
                       reason_code,
                       item,
                       diff_id,
                       zone_id,
                       zone_node_type,
                       link_code,
                       effective_date,
                       change_type,
                       change_amount,
                       change_currency,
                       change_selling_uom,
                       null_multi_ind,
                       price_guide_id,
                       vendor_funded_ind,
                       create_date,
                       create_id,
                       area_diff_parent_id,
                       ignore_constraints)
         when auto_approve_ind = 1 then
            into numeric_id_gtt (numeric_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         when 2 = 2 then
            into rpm_id_date_gtt (price_event_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         select /*+ CARDINALITY (ads 10) */
                DECODE(new_pc_ids.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(new_pc_ids.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                new_pc_ids.item,
                pc.diff_id,
                new_pc_ids.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.link_code,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                -- A case statement decides whether the item being passed in is a parent or tran item
                -- depending on what is used, the appropriate avg retail value will be used
                TRUNC(DECODE(new_pc_ids.percent_apply_type,
                             0, (case
                                    when new_pc_ids.item_level = new_pc_ids.tran_level then
                                       inner.avg_retail_tran
                                    else
                                       inner.avg_retail_parent
                                 end) * (1 + new_pc_ids.percent / 100),
                             1, (case
                                    when new_pc_ids.item_level = new_pc_ids.tran_level then
                                       inner.avg_retail_tran
                                    else
                                       inner.avg_retail_parent
                                 end) * (1 - new_pc_ids.percent / 100),
                             2, (case
                                    when new_pc_ids.item_level = new_pc_ids.tran_level then
                                       inner.avg_retail_tran
                                    else
                                       inner.avg_retail_parent
                                 end)) *
                      new_pc_ids.ad_exchange_rate / new_pc_ids.pc_exchange_rate, new_pc_ids.currency_rtl_dec) change_amount,
                new_pc_ids.ad_currency_code change_currency,
                inner.min_uom change_selling_uom,
                0 null_multi_ind,
                new_pc_ids.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate create_date,
                pc.create_id,
                new_pc_ids.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                new_pc_ids.auto_approve_ind
           from (with gtt as
                    (select price_change_id,
                            area_diff_id,
                            zone_id,
                            auto_approve_ind,
                            price_guide_id,
                            ad_currency_code,
                            ad_exchange_rate,
                            pc_exchange_rate,
                            currency_rtl_dec,
                            percent,
                            percent_apply_type,
                            dept,
                            class,
                            subclass
                       from (select price_change_id,
                                    area_diff_id,
                                    zone_id,
                                    auto_approve_ind,
                                    price_guide_id,
                                    ad_currency_code,
                                    ad_exchange_rate,
                                    pc_exchange_rate,
                                    currency_rtl_dec,
                                    percent,
                                    percent_apply_type,
                                    dept,
                                    class,
                                    subclass,
                                    RANK() OVER (PARTITION BY price_change_id,
                                                              area_diff_id,
                                                              zone_id,
                                                              dept,
                                                              class,
                                                              subclass
                                                     ORDER BY rowid) rnk
                               from rpm_area_diff_pc_approve_gtt
                              where rownum > 0)
                      where rnk = 1)
                (-- Items
                 select /*+ CARDINALITY (ids 10) */
                        rpc.price_change_id,
                        rpc.item,
                        gtt.area_diff_id,
                        gtt.zone_id,
                        gtt.auto_approve_ind,
                        gtt.price_guide_id,
                        gtt.ad_currency_code,
                        gtt.ad_exchange_rate,
                        gtt.pc_exchange_rate,
                        gtt.currency_rtl_dec,
                        gtt.percent,
                        gtt.percent_apply_type,
                        im.item_level,
                        im.tran_level
                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL)) ads,
                        rpm_price_change rpc,
                        gtt,
                        item_master im
                  where rpc.price_change_id      = ads.number_1
                    and rpc.item                 is NOT NULL
                    and rpc.skulist              is NULL
                    and rpc.price_event_itemlist is NULL
                    and rpc.link_code            is NULL
                    and gtt.price_change_id      = rpc.price_change_id
                    and ads.number_2             = gtt.area_diff_id
                    and im.item                  = rpc.item
                    and im.dept                  = gtt.dept
                    and im.class                 = gtt.class
                    and im.subclass              = gtt.subclass
                 union all
                 -- Item Lists
                 select /*+ CARDINALITY (ids 10) */
                        rpc.price_change_id,
                        rpcs.item,
                        gtt.area_diff_id,
                        gtt.zone_id,
                        gtt.auto_approve_ind,
                        gtt.price_guide_id,
                        gtt.ad_currency_code,
                        gtt.ad_exchange_rate,
                        gtt.pc_exchange_rate,
                        gtt.currency_rtl_dec,
                        gtt.percent,
                        gtt.percent_apply_type,
                        im.item_level,
                        im.tran_level
                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL)) ads,
                        rpm_price_change rpc,
                        rpm_price_change_skulist rpcs,
                        gtt,
                        item_master im
                  where rpc.price_change_id         = ads.number_1
                    and rpc.skulist                 is NOT NULL
                    and rpc.price_event_itemlist    is NULL
                    and rpc.item                    is NULL
                    and rpc.link_code               is NULL
                    and rpc.sys_generated_exclusion = 0
                    and rpcs.skulist                = rpc.skulist
                    and rpcs.price_event_id         = rpc.price_change_id
                    and gtt.price_change_id         = rpc.price_change_id
                    and ads.number_2                = gtt.area_diff_id
                    and im.item                     = rpcs.item
                    and im.dept                     = gtt.dept
                    and im.class                    = gtt.class
                    and im.subclass                 = gtt.subclass
                 union all
                 -- PEILs
                 select /*+ CARDINALITY (ids 10) */
                        rpc.price_change_id,
                        rmld.item,
                        gtt.area_diff_id,
                        gtt.zone_id,
                        gtt.auto_approve_ind,
                        gtt.price_guide_id,
                        gtt.ad_currency_code,
                        gtt.ad_exchange_rate,
                        gtt.pc_exchange_rate,
                        gtt.currency_rtl_dec,
                        gtt.percent,
                        gtt.percent_apply_type,
                        im.item_level,
                        im.tran_level
                   from table(cast(L_no_exclusion as OBJ_NUM_NUM_STR_TBL)) ads,
                        rpm_price_change rpc,
                        rpm_merch_list_detail rmld,
                        gtt,
                        item_master im
                  where rpc.price_change_id      = ads.number_1
                    and rpc.price_event_itemlist is NOT NULL
                    and rpc.skulist              is NULL
                    and rpc.item                 is NULL
                    and rpc.link_code            is NULL
                    and rmld.merch_list_id       = rpc.price_event_itemlist
                    and gtt.price_change_id      = rpc.price_change_id
                    and ads.number_2             = gtt.area_diff_id
                    and im.item                  = rmld.item
                    and im.dept                  = gtt.dept
                    and im.class                 = gtt.class
                    and im.subclass              = gtt.subclass)) new_pc_ids,
                rpm_price_change pc,
                (-- This calculates the avg retail across all chunks
                 -- The reason for these subqueries is to find the average
                 -- retails of different chunks where each chunk may have a
                 -- different number of records tied to it and each average
                 -- tied to that number of recs for that chunk may be different
                 -- as well. (a piecewise average) That way, no matter how the
                 -- recs are divided up per chunk, they will be proprotionally correct
                 select num_tran/denom_tran avg_retail_tran,
                        num_parent/denom_parent avg_retail_parent,
                        price_change_id,
                        item,
                        item_parent,
                        min_uom,
                        parent_rank
                   -- This is calculating the components necessary to do the
                   -- division between the num and denom above
                   from (select distinct SUM(selling_retail_tran * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                                 item) num_tran,
                                SUM(selling_retail_parent * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                          item_parent) num_parent,
                                SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                  item) denom_tran,
                                SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                  item_parent) denom_parent,
                                price_change_id,
                                item,
                                item_parent,
                                min_uom,
                                parent_rank
                           -- The average retail has been broken into two calculations
                           -- 1 for each individual tran item, 1 between all items with the same parent
                           from (select fr.price_change_id,
                                        fr.item,
                                        im.item_parent,
                                        fr.rec_count,
                                        fr.bulk_cc_pe_id,
                                        MIN(fr.min_uom) OVER (PARTITION BY fr.price_change_id,
                                                                           fr.item) min_uom,
                                        AVG(fr.avg_retail) OVER (PARTITION BY fr.price_change_id,
                                                                              fr.item) selling_retail_tran,
                                        AVG(fr.avg_retail) OVER (PARTITION BY fr.price_change_id,
                                                                              im.item_parent) selling_retail_parent,
                                        RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                                         ORDER BY im.item) parent_rank,
                                        RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                                         ORDER BY fr.location) loc_rank
                                   from (select bulk_cc_pe_id,
                                                avg_retail,
                                                min_uom,
                                                price_change_id,
                                                rec_count,
                                                item,
                                                location
                                           from rpm_chunk_area_diff
                                          where bulk_cc_pe_id = I_bulk_cc_pe_id) fr,
                                        item_master im
                                  where fr.item = im.item)
                          where loc_rank = 1)) inner
          where pc.price_change_id = new_pc_ids.price_change_id
            and pc.price_change_id = inner.price_change_id
            and (   (    inner.item_parent = new_pc_ids.item
                     and inner.parent_rank = 1)
                 or (    inner.item        = new_pc_ids.item));

   end if;

   open C_HAS_EXCLUSION;
   fetch C_HAS_EXCLUSION BULK COLLECT into L_has_exclusion;
   close C_HAS_EXCLUSION;

   if L_has_exclusion is NOT NULL and
      L_has_exclusion.COUNT > 0 then

      open C_ITEMS_IN_LISTS_NOT_EXCLUDED(L_has_exclusion);
      fetch C_ITEMS_IN_LISTS_NOT_EXCLUDED BULK COLLECT into L_items_in_lists_not_excluded;
      close C_ITEMS_IN_LISTS_NOT_EXCLUDED;

      -- If a Parent Item, Parent Diff, Item List, or PEIL Price Change has an Area Diff Exclusion,
      -- then the Area Differential Price Change will be created at the Tran Item Level

      insert all
         when 1 = 1 then
            into rpm_price_change (price_change_id,
                                   price_change_display_id,
                                   state,
                                   reason_code,
                                   item,
                                   zone_id,
                                   zone_node_type,
                                   effective_date,
                                   change_type,
                                   change_amount,
                                   change_currency,
                                   change_selling_uom,
                                   null_multi_ind,
                                   price_guide_id,
                                   vendor_funded_ind,
                                   create_date,
                                   create_id,
                                   area_diff_parent_id,
                                   ignore_constraints)
               values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                       RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                       state,
                       reason_code,
                       item,
                       zone_id,
                       zone_node_type,
                       effective_date,
                       change_type,
                       change_amount,
                       change_currency,
                       change_selling_uom,
                       null_multi_ind,
                       price_guide_id,
                       vendor_funded_ind,
                       create_date,
                       create_id,
                       area_diff_parent_id,
                       ignore_constraints)
         when auto_approve_ind = 1 then
            into numeric_id_gtt (numeric_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         when 2 = 2 then
            into rpm_id_date_gtt (price_event_id)
               values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
            with gtt as
               (select price_change_id,
                       area_diff_id,
                       zone_id,
                       auto_approve_ind,
                       price_guide_id,
                       ad_currency_code,
                       ad_exchange_rate,
                       pc_exchange_rate,
                       currency_rtl_dec,
                       percent,
                       percent_apply_type,
                       dept,
                       class,
                       subclass
                  from (select price_change_id,
                               area_diff_id,
                               zone_id,
                               auto_approve_ind,
                               price_guide_id,
                               ad_currency_code,
                               ad_exchange_rate,
                               pc_exchange_rate,
                               currency_rtl_dec,
                               percent,
                               percent_apply_type,
                               dept,
                               class,
                               subclass,
                               RANK() OVER (PARTITION BY price_change_id,
                                                         area_diff_id,
                                                         zone_id,
                                                         dept,
                                                         class,
                                                         subclass
                                                ORDER BY rowid) rnk
                          from rpm_area_diff_pc_approve_gtt)
                 where rnk = 1),
            inner as
               (-- This calculates the avg retail across all chunks
                -- The reason for these subqueries is to find the average
                -- retails of different chunks where each chunk may have a
                -- different number of records tied to it and each average
                -- tied to that number of recs for that chunk may be different
                -- as well. (a piecewise average) That way, no matter how the
                select num_tran/denom_tran avg_retail_tran,
                       price_change_id,
                       item,
                       min_uom
                  -- This is calculating the components necessary to do the
                  -- division between the num and denom above
                  from (select distinct SUM(selling_retail_tran * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                                item) num_tran,
                               SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                 item) denom_tran,
                               price_change_id,
                               item,
                               min_uom
                          -- The average retail is only necessary at the tran level for parent items
                          -- that have tran item exclusions within them
                          from (select price_change_id,
                                       item,
                                       rec_count,
                                       bulk_cc_pe_id,
                                       MIN(min_uom) OVER (PARTITION BY price_change_id,
                                                                       item) min_uom,
                                       AVG(avg_retail) OVER (PARTITION BY price_change_id,
                                                                          item) selling_retail_tran,
                                       RANK() OVER (PARTITION BY item
                                                        ORDER BY location) loc_rank
                                  from (select bulk_cc_pe_id,
                                               avg_retail,
                                               min_uom,
                                               price_change_id,
                                               rec_count,
                                               item,
                                               location
                                          from rpm_chunk_area_diff
                                         where bulk_cc_pe_id = I_bulk_cc_pe_id))
                         where loc_rank = 1))
         -- Items
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code) reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.avg_retail_tran * (1 + gtt.percent / 100),
                             1, inner.avg_retail_tran * (1 - gtt.percent / 100),
                             2, inner.avg_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code change_currency,
                inner.min_uom change_selling_uom,
                0 null_multi_ind,
                gtt.price_guide_id,
                0 vendor_funded_ind,
                LP_vdate create_date,
                pc.create_id,
                gtt.price_change_id area_diff_parent_id,
                L_ignore_constraints ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.price_change_id      = inner.price_change_id
            and pc.skulist              is NULL
            and pc.price_event_itemlist is NULL
            and pc.link_code            is NULL
            and im.item_parent          = pc.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and inner.item              = im.item
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and (   pc.diff_id         is NULL
                 or (    pc.diff_id    is NOT NULL
                     and (   im.diff_1 = pc.diff_id
                          or im.diff_2 = pc.diff_id
                          or im.diff_3 = pc.diff_id
                          or im.diff_4 = pc.diff_id)))
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
         union all
         -- Inserts the remaining tran items from parent items belonging in an Item List
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.avg_retail_tran * (1 + gtt.percent / 100),
                             1, inner.avg_retail_tran * (1 - gtt.percent / 100),
                             2, inner.avg_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.min_uom                                     change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_price_change_skulist rpcs,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NOT NULL
            and pc.price_event_itemlist is NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.skulist              = rpcs.skulist
            and pc.price_change_id      = rpcs.price_event_id
            and rpcs.item_level         = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
            and im.item_parent          = rpcs.item
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rpcs.item)
         union all
         --Item Lists for Tran Items
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.avg_retail_tran * (1 + gtt.percent / 100),
                             1, inner.avg_retail_tran * (1 - gtt.percent / 100),
                             2, inner.avg_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.min_uom                                     change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_price_change_skulist rpcs,
                item_master im,
                inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NOT NULL
            and pc.price_event_itemlist is NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.skulist              = rpcs.skulist
            and pc.price_change_id      = rpcs.price_event_id
            and rpcs.item_level         = RPM_CONSTANTS.IL_ITEM_LEVEL
            and im.item                 = rpcs.item
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rpcs.item)
         union all
         -- Inserts the remaining tran items from parent items belonging in a PEIL
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.avg_retail_tran * (1 + gtt.percent / 100),
                             1, inner.avg_retail_tran * (1 - gtt.percent / 100),
                             2, inner.avg_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.min_uom                                     change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id                                price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_merch_list_detail rmld,
                item_master im,
                (-- This calculates the avg retail across all chunks
                 -- The reason for these subqueries is to find the average
                 -- retails of different chunks where each chunk may have a
                 -- different number of records tied to it and each average
                 -- tied to that number of recs for that chunk may be different
                 -- as well. (a piecewise average) That way, no matter how the
                 -- recs are divided up per chunk, they will be proprotionally correct
                 select num_tran/denom_tran avg_retail_tran,
                        price_change_id,
                        item,
                        min_uom
                   -- This is calculating the components necessary to do the
                   -- division between the num and denom above
                   from (select distinct SUM(selling_retail_tran * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                                 item) num_tran,
                                SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                  item) denom_tran,
                                price_change_id,
                                item,
                                min_uom
                           -- The average retail has been broken into two calculations
                           -- 1 for each individual tran item, 1 between all items with the same parent
                           from (select price_change_id,
                                        item,
                                        rec_count,
                                        bulk_cc_pe_id,
                                        MIN(min_uom) OVER (PARTITION BY price_change_id,
                                                                        item) min_uom,
                                        AVG(avg_retail) OVER (PARTITION BY price_change_id,
                                                                           item) selling_retail_tran,
                                        RANK() OVER (PARTITION BY item
                                                         ORDER BY location) loc_rank
                                   from (select bulk_cc_pe_id,
                                                avg_retail,
                                                min_uom,
                                                price_change_id,
                                                rec_count,
                                                item,
                                                location
                                           from rpm_chunk_area_diff
                                          where bulk_cc_pe_id = I_bulk_cc_pe_id))
                          where loc_rank = 1)) inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NULL
            and pc.price_event_itemlist is NOT NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
            and im.item_parent          = rmld.item
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rmld.item)
         union all
         --PEILS for Tran Items
         select /*+ CARDINALITY (ads 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                im.item,
                gtt.zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, inner.avg_retail_tran * (1 + gtt.percent / 100),
                             1, inner.avg_retail_tran * (1 - gtt.percent / 100),
                             2, inner.avg_retail_tran) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.min_uom                                     change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id                                price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind
           from table(cast(L_has_exclusion as OBJ_NUM_NUM_NUM_STR_TBL)) ads,
                gtt,
                rpm_price_change pc,
                rpm_merch_list_detail rmld,
                item_master im,
                (-- This calculates the avg retail across all chunks
                 -- The reason for these subqueries is to find the average retails of different chunks
                 -- where each chunk may have a different number of records tied to it and each average
                 -- tied to that number of recs for that chunk may be different as well.
                 select num_tran/denom_tran avg_retail_tran,
                        price_change_id,
                        item,
                        min_uom
                   from (select distinct SUM(selling_retail_tran * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                                 item) num_tran,
                                SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                  item) denom_tran,
                                price_change_id,
                                item,
                                min_uom
                           from (select price_change_id,
                                        item,
                                        rec_count,
                                        bulk_cc_pe_id,
                                        MIN(min_uom) OVER (PARTITION BY price_change_id,
                                                                        item) min_uom,
                                        AVG(avg_retail) OVER (PARTITION BY price_change_id,
                                                                           item) selling_retail_tran,
                                        RANK() OVER (PARTITION BY item
                                                         ORDER BY location) loc_rank
                                   from (select bulk_cc_pe_id,
                                                avg_retail,
                                                min_uom,
                                                price_change_id,
                                                rec_count,
                                                item,
                                                location
                                           from rpm_chunk_area_diff
                                          where bulk_cc_pe_id = I_bulk_cc_pe_id))
                          where loc_rank = 1)) inner
          where gtt.price_change_id     = ads.number_1
            and gtt.area_diff_id        = ads.number_2
            and gtt.zone_id             = ads.number_3
            and pc.price_change_id      = gtt.price_change_id
            and pc.item                 is NULL
            and pc.skulist              is NULL
            and pc.price_event_itemlist is NOT NULL
            and pc.link_code            is NULL
            and pc.price_change_id      = inner.price_change_id
            and pc.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
            and im.item                 = rmld.item
            and inner.item              = im.item
            and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.item_level           = im.tran_level
            and im.dept                 = gtt.dept
            and im.class                = gtt.class
            and im.subclass             = gtt.subclass
            and NOT EXISTS (select 1
                              from rpm_area_diff_exclude rade
                             where rade.area_diff_id = gtt.area_diff_id
                               and rade.item_id      = im.item)
            and NOT EXISTS (select /*+ CARDINALITY (ids 10) */
                                   1
                              from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids
                             where ids.number_1 = ads.number_1
                               and ids.number_2 = ads.number_2
                               and ids.number_3 = ads.number_3
                               and ids.string   = rmld.item)
         union all
         -- Insert items that are unaffected by any area diff
         -- exclusions (avoid breaking them down to tran items)
         select /*+ CARDINALITY (ids 10) */
                DECODE(gtt.auto_approve_ind,
                       1, RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE,
                       RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) state,
                DECODE(gtt.auto_approve_ind,
                       1, L_adaa_code,
                       L_ad_code)                                 reason_code,
                ids.string                                          item,
                gtt.zone_id                                       zone_id,
                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE                 zone_node_type,
                pc.effective_date                                 effective_date,
                RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE           change_type,
                -- apply area diff rate, do currency conversion, round to curreny decimal places
                TRUNC(DECODE(gtt.percent_apply_type,
                             0, DECODE(im.item_level - im.tran_level,
                                       0, inner.avg_retail_tran,
                                       inner.avg_retail_parent) * (1 + gtt.percent / 100),
                             1, DECODE(im.item_level - im.tran_level,
                                       0, inner.avg_retail_tran,
                                       inner.avg_retail_parent) * (1 - gtt.percent / 100),
                             2, DECODE(im.item_level - im.tran_level,
                                       0, inner.avg_retail_tran,
                                       inner.avg_retail_parent)) *
                      gtt.ad_exchange_rate / gtt.pc_exchange_rate, gtt.currency_rtl_dec) change_amount,
                gtt.ad_currency_code                              change_currency,
                inner.min_uom                                     change_selling_uom,
                0                                                 null_multi_ind,
                gtt.price_guide_id                                price_guide_id,
                0                                                 vendor_funded_ind,
                LP_vdate                                          create_date,
                pc.create_id                                      create_id,
                gtt.price_change_id                               area_diff_parent_id,
                L_ignore_constraints                              ignore_constraints,
                gtt.auto_approve_ind                              auto_approve_ind
           from table(cast(L_items_in_lists_not_excluded as OBJ_NUM_NUM_NUM_STR_TBL)) ids,
                gtt,
                rpm_price_change pc,
                item_master im,
                rpm_zone_location rzl,
                (-- This calculates the avg retail across all chunks
                 -- The reason for these subqueries is to find the average retails of different chunks
                 -- where each chunk may have a different number of records tied to it and each average
                 -- tied to that number of recs for that chunk may be different as well.
                 select num_tran/denom_tran avg_retail_tran,
                        num_parent/denom_parent avg_retail_parent,
                        price_change_id,
                        item,
                        item_parent,
                        location,
                        min_uom,
                        parent_rank
                   from (select distinct SUM(selling_retail_tran * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                                 item) num_tran,
                                SUM(selling_retail_parent * rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                                          item_parent) num_parent,
                                SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                  item) denom_tran,
                                SUM(rec_count) OVER (PARTITION BY bulk_cc_pe_id,
                                                                  item_parent) denom_parent,
                                price_change_id,
                                item,
                                item_parent,
                                location,
                                min_uom,
                                parent_rank
                           from (select fr.price_change_id,
                                        fr.item,
                                        fr.location,
                                        im.item_parent,
                                        fr.rec_count,
                                        fr.bulk_cc_pe_id,
                                        MIN(fr.min_uom) OVER (PARTITION BY fr.price_change_id,
                                                                           fr.item) min_uom,
                                        AVG(fr.avg_retail) OVER (PARTITION BY fr.price_change_id,
                                                                              fr.item) selling_retail_tran,
                                        AVG(fr.avg_retail) OVER (PARTITION BY fr.price_change_id,
                                                                              im.item_parent) selling_retail_parent,
                                        RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                                         ORDER BY im.item) parent_rank,
                                        RANK() OVER (PARTITION BY NVL(im.item_parent, im.item)
                                                         ORDER BY fr.location) loc_rank
                                   from (select bulk_cc_pe_id,
                                                avg_retail,
                                                min_uom,
                                                price_change_id,
                                                rec_count,
                                                item,
                                                location
                                           from rpm_chunk_area_diff
                                          where bulk_cc_pe_id = I_bulk_cc_pe_id) fr,
                                        item_master im
                                  where fr.item = im.item)
                          where loc_rank = 1)) inner
          where ids.number_1       = gtt.price_change_id
            and ids.number_2       = gtt.area_diff_id
            and ids.number_3       = gtt.zone_id
            and pc.price_change_id = gtt.price_change_id
            and ids.string         = im.item
            and pc.price_change_id = inner.price_change_id
            and im.dept            = gtt.dept
            and im.class           = gtt.class
            and im.subclass        = gtt.subclass
            and pc.zone_id         = rzl.zone_id
            and rzl.location       = inner.location
            and (   (    inner.item_parent = im.item
                     and inner.parent_rank = 1)
                 or (    inner.item        = im.item));

   end if;

   delete
     from rpm_chunk_area_diff
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return FALSE;

END CHUNK_CREATE_AREA_DIFF_PC;

--------------------------------------------------------

FUNCTION CHUNK_APPLY_AREA_DIFFS_TO_PC(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_primary_pc_id    IN     NUMBER,
                                      I_bulk_cc_pe_id    IN     NUMBER,
                                      I_user_name        IN     VARCHAR2,
                                      I_pe_sequence_id   IN     NUMBER,
                                      I_pe_thread_number IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'RPM_AREA_DIFF_PRICE_CHANGE.APPLY_AREA_DIFFS_TO_PC';

BEGIN

   if FILTER_PCS(O_cc_error_tbl,
                 OBJ_NUMERIC_ID_TABLE(I_primary_pc_id)) = FALSE then
      return FALSE;
   end if;

   if CHUNK_CREATE_AREA_DIFF_PC(O_cc_error_tbl,
                                I_bulk_cc_pe_id,
                                I_user_name,
                                I_pe_sequence_id,
                                I_pe_thread_number) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return FALSE;

END CHUNK_APPLY_AREA_DIFFS_TO_PC;

--------------------------------------------------------

END RPM_AREA_DIFF_PRICE_CHANGE;
/