CREATE OR REPLACE PACKAGE RPM_PROMO_STATE_CHANGE_SQL AS

/* Function UPD_CANCELLED_PROMO_DTL_END_DT update the END_DATE of the ACTIVE promotion component
   detail when the end state of the selected promotion component from UI is CANCEL.   
*/
FUNCTION UPD_CANCELLED_PROMO_DTL_END_DT(O_error_msg        OUT VARCHAR2,
                                        I_promo_level   IN     NUMBER,
                                        I_child_ids_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

-- Function to check whether there is an active promo exception or exclusion
FUNCTION CHK_ACTIVE_EXCP_EXCL(O_error_msg             OUT VARCHAR2,
                              O_exception_details     OUT OBJ_NUM_STR_STR_TBL,
                              I_dtl_ids_tbl        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

END RPM_PROMO_STATE_CHANGE_SQL;
/