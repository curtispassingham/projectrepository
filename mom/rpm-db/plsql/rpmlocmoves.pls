CREATE OR REPLACE PACKAGE RPM_LOC_MOVE_SQL AS
-----------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS(O_error_msg                 OUT VARCHAR2,
                            O_lm_bulkid_petype_obj      OUT OBJ_NUM_NUM_STR_TBL,
                            I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------
FUNCTION RECORD_ERROR(O_error_msg                OUT VARCHAR2,
                      I_bulk_cc_pe_id        IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                      I_thread_number        IN      RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                      I_parent_thread_number IN      RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                      I_price_event_id       IN      RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE,
                      I_item                 IN      RPM_FUTURE_RETAIL.ITEM%TYPE,
                      I_location             IN      RPM_FUTURE_RETAIL.LOCATION%TYPE,
                      I_fail_string          IN      RPM_STAGE_ITEM_LOC.ERROR_MSG%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------
FUNCTION RESTART_PROCESS(O_error_msg                 OUT VARCHAR2,
                         O_bulk_cc_pe_ids            OUT OBJ_NUMERIC_ID_TABLE,
                         I_max_retry_number       IN     NUMBER,
                         I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------
FUNCTION INCREMENT_ERROR(O_error_msg                OUT VARCHAR2,
                         I_bulk_cc_pe_id        IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                         I_thread_number        IN      RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                         I_parent_thread_number IN      RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_price_event_id       IN      RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE,
                         I_item                 IN      RPM_FUTURE_RETAIL.ITEM%TYPE,
                         I_location             IN      RPM_FUTURE_RETAIL.LOCATION%TYPE,
                         I_fail_string          IN      RPM_STAGE_ITEM_LOC.ERROR_MSG%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_ERROR(O_error_msg                OUT VARCHAR2,
                      I_bulk_cc_pe_id        IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                      I_thread_number        IN      RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                      I_parent_thread_number IN      RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                      I_price_event_id       IN      RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE,
                      I_item                 IN      RPM_FUTURE_RETAIL.ITEM%TYPE,
                      I_location             IN      RPM_FUTURE_RETAIL.LOCATION%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------
FUNCTION RESOLVE_ACTIVE_PROMOTIONS(O_error_msg             OUT VARCHAR2,
                                   O_bulk_su_cc_pe_id      OUT NUMBER,
                                   O_bulk_tu_cc_pe_id      OUT NUMBER,
                                   O_bulk_bu_cc_pe_id      OUT NUMBER,
                                   O_bulk_txp_cc_pe_id     OUT NUMBER,
                                   O_bulk_csu_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctu_cc_pe_id     OUT NUMBER,
                                   O_bulk_ccu_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctxp_cc_pe_id    OUT NUMBER,
                                   I_user_name          IN     VARCHAR2,
                                   I_location_move_id   IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------
FUNCTION RESOLVE_FROM_ZONE_OVERLAPS (O_error_msg             OUT VARCHAR2,
                                     O_bulk_sp_cc_pe_id      OUT NUMBER,
                                     O_bulk_tp_cc_pe_id      OUT NUMBER,
                                     O_bulk_bg_cc_pe_id      OUT NUMBER,
                                     O_bulk_txp_cc_pe_id     OUT NUMBER,
                                     O_bulk_csp_cc_pe_id     OUT NUMBER,
                                     O_bulk_ctp_cc_pe_id     OUT NUMBER,
                                     O_bulk_cbg_cc_pe_id     OUT NUMBER,
                                     O_bulk_ctxp_cc_pe_id    OUT NUMBER,
                                     I_user_name          IN     VARCHAR2,
                                     I_location_move_id   IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------
FUNCTION RESOLVE_TO_ZONE_OVERLAPS (O_error_msg             OUT VARCHAR2,
                                   O_bulk_sp_cc_pe_id      OUT NUMBER,
                                   O_bulk_tp_cc_pe_id      OUT NUMBER,
                                   O_bulk_bg_cc_pe_id      OUT NUMBER,
                                   O_bulk_txp_cc_pe_id     OUT NUMBER,
                                   O_bulk_csp_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctp_cc_pe_id     OUT NUMBER,
                                   O_bulk_cbg_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctxp_cc_pe_id    OUT NUMBER,
                                   I_user_name          IN     VARCHAR2,
                                   I_location_move_id   IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------
FUNCTION INIT_BULK_CC_PE(O_error_msg            OUT VARCHAR2,
                         O_bulk_cc_pe_id        OUT NUMBER,
                         I_price_event_type  IN     VARCHAR2,
                         I_user_name         IN     VARCHAR2,
                         I_location_move_id  IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                         I_process           IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION GENERATE_PRICE_CHANGE(O_error_msg                 OUT VARCHAR2,
                               O_bulk_cc_pe_id             OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                               I_loc_move_id            IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                               I_user_name              IN     RPM_BULK_CC_PE.USER_NAME%TYPE,
                               I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_ID(I_is_next   IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION SCHEDULE_LOCATION_MOVE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                                I_loc_move_id  IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                                I_rib_trans_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                I_persist_ind  IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION COMPLETE_LOCATION_MOVE(O_error_msg      OUT VARCHAR2,
                                I_loc_move_id IN     NUMBER)
RETURN NUMBER;
----------------------------------------------------------------------------
END RPM_LOC_MOVE_SQL;
/

