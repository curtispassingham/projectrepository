CREATE OR REPLACE PACKAGE RPM_CC_CL_PROM_OV AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------

END RPM_CC_CL_PROM_OV;
/

