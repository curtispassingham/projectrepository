CREATE OR REPLACE PACKAGE BODY MERCH_NOTIFY_API_SQL AS

-----------------------------------------------------------------------------------------
--function definitions
-----------------------------------------------------------------------------------------
FUNCTION NEW_DEPARTMENT( O_error_msg   OUT VARCHAR2,
                         I_dept_id     IN  NUMBER)
RETURN BOOLEAN IS

L_program   VARCHAR2(64) := 'MERCH_NOTIFY_API_SQL.NEW_DEPARTMENT';
L_dept_exists_flag       VARCHAR2(1) := NULL;

cursor C_DEPT_EXISTS is
      select 'x'
        from RPM_DEPT_AGGREGATION
       where dept = I_dept_id;

BEGIN

-- Validate Input Parameters

if I_dept_id is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_dept_id', L_program, NULL);
      return FALSE;
end if;

-- The MERCH_NOTIFY_API_SQL.NEW_DEPARTMENT function will verify that the department
-- is not already represented in the RPM_DEPT_AGGREGATION table before attempting to insert a new record.

   --- check if department already exists

   open C_DEPT_EXISTS;
   fetch C_DEPT_EXISTS into L_dept_exists_flag;

   if C_DEPT_EXISTS%NOTFOUND then

   --- if it doesn't exist create new department aggregation record

      INSERT INTO RPM_DEPT_AGGREGATION(DEPT_AGGREGATION_ID,
      				       DEPT,
      				       LOWEST_STRATEGY_LEVEL,
      				       WORKSHEET_LEVEL,
      				       HISTORICAL_SALES_LEVEL,
      				       REGULAR_SALES_IND,
      				       CLEARANCE_SALES_IND,
      				       PROMOTIONAL_SALES_IND,
      				       INCLUDE_WH_ON_HAND,
      				       INCLUDE_WH_ON_ORDER,
      				       PRICE_CHANGE_AMOUNT_CALC_TYPE,
      				       RETAIL_CHG_HIGHLIGHT_DAYS,
      				       COST_CHG_HIGHLIGHT_DAYS,
      				       PEND_COST_CHG_WINDOW_DAYS,
      				       PEND_COST_CHG_HIGHLIGHT_DAYS,
      				       LOCK_VERSION)
      			        VALUES(RPM_DEPT_AGGREGATION_SEQ.NEXTVAL,
				       I_dept_id,
				       0,
				       0,
				       0,
				       0,
				       0,
				       0,
				       0,
				       0,
				       0,
				       0,
				       0,
				       999,
				       0,
				       NULL);
   end if;

   close C_DEPT_EXISTS;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;

END NEW_DEPARTMENT;
-----------------------------------------------------------------------------------------
FUNCTION DELETE_DEPARTMENT(O_error_msg   OUT VARCHAR2,
                           I_dept_id     IN  NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MERCH_NOTIFY_API_SQL.DELETE_DEPARTMENT';

BEGIN

-- Validate Input Parameters

   if I_dept_id is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'I_dept_id',
                                        L_program,
                                        NULL);
      return FALSE;
   end if;

   delete
     from rpm_dept_aggregation
    where dept = I_dept_id;

   --Clearance default strategy
   delete
     from rpm_strategy_cl_dflt_dtl
    where strategy_id IN (select strategy_id
    		                from rpm_strategy
    		               where dept = I_dept_id);

   delete
     from rpm_strategy_cl_dflt
    where strategy_id IN (select strategy_id
    		                from rpm_strategy
    		               where dept = I_dept_id);

   --Clearance Strategy
   delete
     from rpm_strategy_clearance_mkdn
    where strategy_id IN (select strategy_id
                            from rpm_strategy
                           where dept = I_dept_id);

   delete
     from rpm_strategy_clearance
    where strategy_id IN (select strategy_id
                            from rpm_strategy
                           where dept = I_dept_id);

  --Margin Strategy
   delete
     from rpm_strategy_detail
    where strategy_id IN (select strategy_id
                            from rpm_strategy
                           where dept = I_dept_id);

   delete
     from rpm_strategy_margin
    where strategy_id IN (select strategy_id
                            from rpm_strategy
                           where dept = I_dept_id);

  --Maintain Margin strategy
   delete
     from rpm_strategy_maint_margin
    where strategy_id IN (select strategy_id
                            from rpm_strategy
                           where dept = I_dept_id);

   --Competitive strategy
   delete
     from rpm_strategy_competitive
    where strategy_id IN (select strategy_id
                            from rpm_strategy
                           where dept = I_dept_id);

   --Maintain Primary Zone Group
   delete
     from rpm_merch_retail_def
    where dept = I_dept_id;

   --Maintain Price Guide
   delete
     from rpm_price_guide_dept
    where dept = I_dept_id;

   delete
     from rpm_price_guide_interval
    where price_guide_id NOT IN (select price_guide_id
    		                       from rpm_price_guide_dept);

   delete
     from rpm_price_guide
    where price_guide_id NOT IN (select price_guide_id
    		                       from rpm_price_guide_dept);

   delete
     from rpm_strategy
    where dept = I_dept_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END DELETE_DEPARTMENT;
-----------------------------------------------------------------------------------------
FUNCTION ITEM_RECLASS(O_error_msg    OUT VARCHAR2,
                      I_item_id      IN  VARCHAR2,
                      I_reclass_date IN DATE,
                      I_new_dept     IN NUMBER,
                      I_new_class    IN NUMBER,
                      I_new_subclass IN NUMBER)
RETURN BOOLEAN IS

L_program   VARCHAR2(64) := 'MERCH_NOTIFY_API_SQL.ITEM_RECLASS';

O_return_code NUMBER;

BEGIN

-- Validate Input Parameters

  if I_item_id is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_item_id', L_program, NULL);
      return FALSE;
  elsif I_reclass_date is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_reclass_date', L_program, NULL);
      return FALSE;
  elsif I_new_dept is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_new_dept', L_program, NULL);
      return FALSE;
  elsif I_new_class is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_new_class', L_program, NULL);
      return FALSE;
  elsif I_new_subclass is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_reclass_date', L_program, NULL);
      return FALSE;
  end if;


  -- The following procedure to stage item reclass data into the existing RPM_ITEM_MODIFICATION table

  RPM_ITEM_RECLASS_SQL.POPULATE_RPM_ITEM_MOD(O_return_code,
                                                 O_error_msg,
                                                 I_item_id,
                                                 I_reclass_date,
                                                 I_new_dept,
                                                 I_new_class,
                                                 I_new_subclass);

  if O_return_code = 1 then
     return TRUE;
  else
     return FALSE;
  end if;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;

END ITEM_RECLASS;
-----------------------------------------------------------------------------------------
FUNCTION NEW_LOCATION(O_error_msg        OUT VARCHAR2,
                      I_new_location     IN  NUMBER,
                      I_new_loc_type     IN  VARCHAR2,
                      I_pricing_location IN  NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'MERCH_NOTIFY_API_SQL.NEW_LOCATION';

   L_loc_id                    RPM_ZONE_LOCATION.LOC_TYPE%TYPE                   := NULL;
   L_currency_code             VARCHAR2(3)                                       := NULL;
   L_store_name                STORE.STORE_NAME%TYPE                             := NULL;
   L_wh_name                   WH.WH_NAME%TYPE                                   := NULL;
   L_stockholding_ind          WH.STOCKHOLDING_IND%TYPE                          := NULL;
   L_recognize_wh_as_locations RPM_SYSTEM_OPTIONS.RECOGNIZE_WH_AS_LOCATIONS%TYPE := NULL;
   L_exists                    VARCHAR2(1)                                       := NULL;

   cursor C_VAL_NEW_STORE is
      select currency_code,
             store_name,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loc_type
        from store
       where store = I_new_location;

   cursor C_VAL_NEW_WH is
      select currency_code,
             wh_name,
             stockholding_ind,
             recognize_wh_as_locations,
             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loc_type
        from wh,
             rpm_system_options
       where wh = I_new_location;

   cursor C_RPM_ZONE_LOC_EXISTS is
      select 'x'
        from rpm_zone_location
       where location = I_new_location
         and loc_type = L_loc_id;

   cursor C_ZONE_INFO is
      select rz.zone_id,
             rz.currency_code currency_code,
             rzg.zone_group_id zone_group_id
        from rpm_zone rz,
             rpm_zone_group rzg,
             rpm_zone_location rzl
       where rz.zone_group_id     = rzg.zone_group_id
         and rz.zone_id           = rzl.zone_id
         and rzl.location         = I_pricing_location
         and (    I_new_loc_type  = RPM_CONSTANTS.LOCATION_TYPE_STORE
              or (EXISTS (select 'x'
                            from rpm_zone_group_type rzgt
                           where rz.zone_group_id = rzgt.zone_group_id
                             and I_new_loc_type   = RPM_CONSTANTS.LOCATION_TYPE_WH
                             and type             IN (RPM_CONSTANTS.CLEARANCE_ZONE_GROUP_TYPE,
                                                      RPM_CONSTANTS.REGULAR_ZONE_GROUP_TYPE))));

BEGIN

   -- Validate Input Parameters

   if I_new_location is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'I_new_location',
                                        L_program,
                                        NULL);
      return FALSE;
   elsif I_new_loc_type NOT IN (RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH) or
         I_new_loc_type is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                        NULL,
                                        NULL,
                                        NULL);
      return FALSE;
   elsif I_pricing_location is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'I_pricing_location',
                                        L_program,
                                        NULL);
      return FALSE;
   end if;

   -- Validate the passed in store parameter value exists

   if I_new_loc_type = RPM_CONSTANTS.LOCATION_TYPE_STORE then
      open C_VAL_NEW_STORE;
      fetch C_VAL_NEW_STORE into L_currency_code,
                                 L_store_name,
                                 L_loc_id;

      if C_VAL_NEW_STORE%NOTFOUND then
         O_error_msg := SQL_LIB.CREATE_MSG('INV_LOC',
                                           NULL,
                                           NULL,
                                           NULL);
         close C_VAL_NEW_STORE;
         return FALSE;
      end if;

      close C_VAL_NEW_STORE;
   end if;

   -- Validate the passed in WH parameter value exists

   if I_new_loc_type = RPM_CONSTANTS.LOCATION_TYPE_WH then
      open C_VAL_NEW_WH;
      fetch C_VAL_NEW_WH into L_currency_code,
                              L_wh_name,
                              L_stockholding_ind,
                              L_recognize_wh_as_locations,
                              L_loc_id;

      if C_VAL_NEW_WH%NOTFOUND then
         O_error_msg := SQL_LIB.CREATE_MSG('INV_LOC',
                                           NULL,
                                           NULL,
                                           NULL);
         close C_VAL_NEW_WH;
         return FALSE;
      end if;

      close C_VAL_NEW_WH;
   end if;

   --If the New Location Type is Warehouse ("W") and the system option recognize warehouses as locations
   --(rpm_system_options.recognize_wh_as_locations) is "0" (false) then exit

   if I_new_loc_type = RPM_CONSTANTS.LOCATION_TYPE_WH and
      L_recognize_wh_as_locations = 0 then
      return TRUE;
   end if;

   -- If the New Location Id/Type already exists in RPM_ZONE_LOCATION then exit
   open C_RPM_ZONE_LOC_EXISTS;
   fetch C_RPM_ZONE_LOC_EXISTS into L_exists;
   close C_RPM_ZONE_LOC_EXISTS;

   if L_exists = 'x' then
      return TRUE;
   end if;

   for rec IN C_ZONE_INFO
      LOOP

      -- if the currency of the new location equals the currency of the pricing location
      -- then add the new location to the zone containing the pricing location
      -- by inserting a new record into rpm_zone_location

      if L_currency_code = rec.currency_code then
         insert into rpm_zone_location(zone_location_id,
                                       zone_id,
                                       location,
                                       loc_type)
                                values(RPM_ZONE_LOCATION_SEQ.NEXTVAL,
                                       rec.zone_id,
                                       I_new_location,
                                       L_loc_id);

      end if;

      --if the currency of the new location does not match the currency of the pricing location
      --then add the new location to a new zone by first inserting a record into rpm_zone and
      --then inserting a record into rpm_zone_location:

      if L_currency_code != rec.currency_code then
         insert into rpm_zone(zone_id,
                              zone_display_id,
                              zone_group_id,
                              name,
                              currency_code,
                              base_ind)
                      values (RPM_ZONE_SEQ.NEXTVAL,
                              RPM_ZONE_DISPLAY_ID_SEQ.NEXTVAL,
                              rec.zone_group_id,
                              DECODE(I_new_loc_type,
                                     RPM_CONSTANTS.LOCATION_TYPE_STORE, L_store_name,
                                     RPM_CONSTANTS.LOCATION_TYPE_WH, L_wh_name),
                              L_currency_code,
                              0);

         insert into rpm_zone_location(zone_location_id,
                                       zone_id,
                                       location,
                                       loc_type)
                                values(RPM_ZONE_LOCATION_SEQ.NEXTVAL,
                                       RPM_ZONE_SEQ.CURRVAL,
                                       I_new_location,
                                       L_loc_id);
      end if;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return FALSE;

END NEW_LOCATION;

-----------------------------------------------------------------------------------------
END;
/