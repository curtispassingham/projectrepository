CREATE OR REPLACE PACKAGE BODY RPM_MARGIN_VISIBILITY_SQL AS

FUNCTION EXPL_SIMPLE_PROMO_IL(O_error_msg            OUT VARCHAR2,
                              I_obj_rpm_promo_tbl IN     OBJ_RPM_PROMO_TBL)
RETURN NUMBER;

FUNCTION EXPL_PRICE_CHANGE_CLEAR_IL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

FUNCTION EXPL_PRICE_EVENT(O_error_msg          OUT VARCHAR2,
                          I_mv_criteria_tbl IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER;

FUNCTION EXPL_FR_TO_IL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

----------------------------------------------------------------------------------
-- Package level variables...
COST_AND_RETAIL_MARKUP VARCHAR2(1) := 'B';

----------------------------------------------------------------------------------

FUNCTION GET_MARKUP_CHANGE_DATES(O_error_msg            OUT  VARCHAR2,
                                 I_obj_rpm_promo_tbl IN      OBJ_RPM_PROMO_TBL,
                                 I_event_type        IN      VARCHAR2,
                                 O_marg_vis_tbl         OUT  OBJ_MARGIN_VISIBILITY_TBL)
RETURN NUMBER IS

   L_program      VARCHAR2(75)              := 'RPM_MARGIN_VISIBILITY_SQL.GET_MARKUP_CHANGE_DATES';
   L_marg_vis_tbl OBJ_MARGIN_VISIBILITY_TBL := OBJ_MARGIN_VISIBILITY_TBL();

   cursor C_GET_DATES is
      select item,
             location,
             event_id,
             MIN(action_date) action_date
        from (select item,
                     location,
                     event_id,
                     MAX(action_date) action_date
                from (select /*+ index(fr, RPM_FUTURE_RETAIL_I1) */
                             fr.action_date action_date,
                             case
                                when il.parent_id is NULL then
                                   il.item
                                else
                                   il.parent_id
                             end item,
                             case
                                when il.zone_id is NULL then
                                   il.location
                                else
                                   il.zone_id
                             end location,
                             il.event_id
                        from rpm_mv_item_loc_expl_gtt il,
                             rpm_future_retail_gtt fr
                       where il.item             = fr.item
                         and il.location         = fr.location
                         and il.dept             = fr.dept
                         and fr.action_date     <= il.effective_date
                         and fr.price_change_id is NOT NULL
                      union all
                      select fc.active_date action_date,
                             case
                                when il.parent_id is NULL then
                                   il.item
                                else
                                   il.parent_id
                             end item,
                             case
                                when il.zone_id is NULL then
                                   il.location
                                else
                                   il.zone_id
                             end location,
                             il.event_id
                        from rpm_mv_item_loc_expl_gtt il,
                             rpm_future_cost fc
                       where il.item         = fc.item
                         and il.location     = fc.location
                         and fc.active_date <= il.effective_date) t1
               group by item,
                        location,
                        event_id
              union
              select item,
                     location,
                     event_id,
                     MIN(action_date) action_date
                from (select /*+ index(fr, RPM_FUTURE_RETAIL_I1) */
                             fr.action_date action_date,
                             case
                                when il.parent_id is NULL then
                                   il.item
                                else
                                   il.parent_id
                             end item,
                             case
                                when il.zone_id is NULL then
                                   il.location
                                else
                                   il.zone_id
                             end location,
                             il.event_id
                        from rpm_mv_item_loc_expl_gtt il,
                             rpm_future_retail_gtt fr
                       where il.item            = fr.item
                         and il.location        = fr.location
                         and il.dept            = fr.dept
                         and fr.action_date     > il.effective_date
                         and fr.price_change_id is NOT NULL
                         and not exists (select /*+ index(fr2,RPM_FUTURE_RETAIL_I1) */ 'x'
                                           from rpm_future_retail fr2
                                          where fr2.item            = il.item
                                            and fr2.location        = il.location
                                            and fr2.dept            = il.dept
                                            and fr2.price_change_id is NOT NULL
                                            and fr2.action_date     <= il.effective_date
                                         union all
                                         select 'x'
                                           from rpm_future_cost fc2
                                          where fc2.item         = il.item
                                            and fc2.location     = il.location
                                            and fc2.active_date <= il.effective_date)
                      union all
                      select fc.active_date action_date,
                             case
                                when il.parent_id is NULL then
                                   il.item
                                else
                                   il.parent_id
                             end item,
                             case
                                when il.zone_id is NULL then
                                   il.location
                                else
                                   il.zone_id
                             end location,
                             il.event_id
                        from rpm_mv_item_loc_expl_gtt il,
                             rpm_future_cost fc
                       where il.item        = fc.item
                         and il.location    = fc.location
                         and fc.active_date > il.effective_date
                         and not exists (select /*+ index(fr2,RPM_FUTURE_RETAIL_I1) */ 'x'
                                           from rpm_future_retail fr2
                                          where fr2.item            = il.item
                                            and fr2.location        = il.location
                                            and fr2.dept            = il.dept
                                            and fr2.price_change_id is NOT NULL
                                            and fr2.action_date     <= il.effective_date
                                         union all
                                         select 'x'
                                           from rpm_future_cost fc2
                                          where fc2.item         = il.item
                                            and fc2.location     = il.location
                                            and fc2.active_date <= il.effective_date)) t2
               group by item,
                        location,
                        event_id)
       group by item,
                location,
                event_id;

BEGIN

   if I_event_type = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION then

      if EXPL_SIMPLE_PROMO_IL(O_error_msg,
                              I_obj_rpm_promo_tbl) = 0 then
         return 0;
      end if;

   elsif I_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if EXPL_PRICE_CHANGE_CLEAR_IL(O_error_msg) = 0 then
         return 0;
      end if;

   elsif I_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      if EXPL_PRICE_CHANGE_CLEAR_IL(O_error_msg) = 0 then
         return 0;
      end if;

   end if;

   if EXPL_FR_TO_IL(O_error_msg) = 0 then
      return 0;
   end if;

   for rec IN C_GET_DATES loop
      L_marg_vis_tbl.EXTEND;
      L_marg_vis_tbl(L_marg_vis_tbl.COUNT) := NEW OBJ_MARGIN_VISIBILITY_REC(rec.item,
                                                                            rec.location,
                                                                            rec.event_id,
                                                                            rec.action_date);
   end loop;

   O_marg_vis_tbl := L_marg_vis_tbl;

   delete
     from rpm_mv_item_loc_expl_gtt;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
   return 0;

END GET_MARKUP_CHANGE_DATES;
----------------------------------------------------------------------------------

FUNCTION GET_MARKUP_DETAILS(O_error_msg              OUT VARCHAR2,
                            I_mv_criteria_tbl     IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                            O_mv_markup_chg_dates    OUT OBJ_MV_MARKUP_CHG_DATES_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_MARGIN_VISIBILITY_SQL.GET_MARKUP_DETAILS';

   L_num_past_markup_events   NUMBER := 1;
   L_num_future_markup_events NUMBER := 3;
   L_effective_date           DATE   := NULL;

   L_class_level_vat_ind SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE := NULL;
   L_default_tax_type    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE    := NULL;

   L_tax_calc_tbl   OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_custom_tax_ind NUMBER           := NULL;

   -- The cursor below only needs to check at the zone level because all vat_rate information
   -- at the location level is handle in the rpmmarkupcalcb.pls
   cursor C_CHECK_CUSTOM is
      select 1
        from rpm_mv_cost_retail_changes_gtt crc,
             rpm_zone_location rzl,
             (select s.store loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE zone_node_type
                from store s,
                     vat_region vr
               where s.vat_region     = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM
              union all
              select w.wh loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                from wh w,
                     vat_region vr
               where w.vat_region     = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM) locs
       where crc.zone_id  is NOT NULL
         and crc.zone_id  = rzl.zone_id
         and rzl.loc_type = locs.zone_node_type
         and rzl.location = locs.loc
         and rownum       = 1;

   cursor C_GET_MARKUP_CHG_DATES is
      select NEW OBJ_MV_MARKUP_CHG_DATES_REC(parent_id,
                                             item_id,
                                             loc_level,
                                             DECODE(loc_level,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                    loc_zone_id),
                                             cost_change_date,
                                             retail_change_date,
                                             cost,
                                             retail,
                                             markup_percent,
                                             clean_ind,
                                             price_change_display_id,
                                             cost_currency,
                                             retail_currency)
        from (select parent_id,
                     item_id,
                     loc_level,
                     loc_zone_id,
                     cost_change_date,
                     retail_change_date,
                     cost,
                     retail,
                     markup_percent,
                     clean_ind,
                     action_date,
                     price_change_display_id,
                     future_rank,
                     cost_currency,
                     retail_currency
                from (select distinct crc.parent_id,
                             case
                                when crc.parent_id is NULL then
                                   ilm.item
                                else
                                   NULL
                             end item_id,
                             case
                                when crc.zone_id is NULL then
                                   case crc.loc_type
                                      when RPM_CONSTANTS.LOCATION_TYPE_STORE then
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                      else
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE
                                   end
                                else
                                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             end loc_level,
                             case
                                when crc.zone_id is NULL then
                                   crc.location
                                else
                                   crc.zone_id
                             end loc_zone_id,
                             case
                                when crc.change_type IN (RPM_CONSTANTS.COST_MARKUP,
                                                         COST_AND_RETAIL_MARKUP) then
                                   ilm.action_date
                                else
                                   NULL
                             end cost_change_date,
                             case
                                when crc.change_type IN (RPM_CONSTANTS.RETAIL_MARKUP,
                                                         COST_AND_RETAIL_MARKUP) then
                                   ilm.action_date
                                else
                                   NULL
                            end retail_change_date,
                            ilm.cost,
                            ilm.retail,
                            ilm.markup_percent,
                            crc.clean_ind,
                            crc.action_date,
                            rpc.price_change_display_id,
                            DENSE_RANK() OVER (ORDER BY crc.action_date asc) future_rank,
                            ilm.cost_currency cost_currency,
                            ilm.retail_currency retail_currency
                       from rpm_mv_cost_retail_changes_gtt crc,
                            rpm_il_markup_gtt ilm,
                            rpm_price_change rpc
                      where (   crc.item      = ilm.item
                             or crc.parent_id = ilm.item)
                        and (   crc.location  = ilm.location
                             or crc.zone_id   = ilm.location)
                        and TRUNC(crc.action_date)  = TRUNC(ilm.action_date)
                        and ilm.dept                = crc.dept
                        and rpc.price_change_id (+) = crc.event_id
                        and crc.action_date         >= L_effective_date)
               where future_rank <= L_num_future_markup_events
              union all
              select parent_id,
                     item_id,
                     loc_level,
                     loc_zone_id,
                     cost_change_date,
                     retail_change_date,
                     cost,
                     retail,
                     markup_percent,
                     clean_ind,
                     action_date,
                     price_change_display_id,
                     past_rank,
                     cost_currency,
                     retail_currency
                from (select distinct crc.parent_id,
                             case
                                when crc.parent_id is NULL then
                                   ilm.item
                                else
                                   NULL
                             end item_id,
                             case
                                when crc.zone_id is NULL then
                                   case crc.loc_type
                                      when RPM_CONSTANTS.LOCATION_TYPE_STORE then
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                      else
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE
                                   end
                                else
                                   RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             end loc_level,
                             case
                                when crc.zone_id is NULL then
                                   crc.location
                                else
                                   crc.zone_id
                             end loc_zone_id,
                             case
                                when crc.change_type IN (RPM_CONSTANTS.COST_MARKUP,
                                                         COST_AND_RETAIL_MARKUP) then
                                   ilm.action_date
                                else
                                   NULL
                             end cost_change_date,
                             case
                                when crc.change_type IN (RPM_CONSTANTS.RETAIL_MARKUP,
                                                         COST_AND_RETAIL_MARKUP) then
                                   ilm.action_date
                                else
                                   NULL
                             end retail_change_date,
                             ilm.cost,
                             ilm.retail,
                             ilm.markup_percent,
                             crc.clean_ind,
                             crc.action_date,
                             rpc.price_change_display_id,
                             DENSE_RANK() OVER (ORDER BY crc.action_date desc) past_rank,
                             ilm.cost_currency cost_currency,
                             ilm.retail_currency retail_currency
                        from rpm_mv_cost_retail_changes_gtt crc,
                             rpm_il_markup_gtt ilm,
                             rpm_price_change rpc
                       where (   crc.item      = ilm.item
                              or crc.parent_id = ilm.item)
                         and (   crc.location  = ilm.location
                              or crc.zone_id   = ilm.location)
                        and TRUNC(crc.action_date)  = TRUNC(ilm.action_date)
                        and ilm.dept                = crc.dept
                        and rpc.price_change_id (+) = crc.event_id
                        and crc.action_date         < L_effective_date)
               where past_rank <= L_num_past_markup_events) t,
             rpm_zone rz
       where rz.zone_id (+) = t.loc_zone_id
       order by action_date desc;

BEGIN

   O_mv_markup_chg_dates := NEW OBJ_MV_MARKUP_CHG_DATES_TBL();

   select past_markup_events_disp_cnt
     into L_num_past_markup_events
     from rpm_system_options;

   select default_tax_type,
          class_level_vat_ind
     into L_default_tax_type,
          L_class_level_vat_ind
     from system_options;

   if EXPL_PRICE_EVENT(O_error_msg,
                       I_mv_criteria_tbl) != 1 then
      return 0;
   end if;

   if EXPL_FR_TO_IL(O_error_msg) != 1 then
      return 0;
   end if;

   L_effective_date := I_mv_criteria_tbl(1).effective_date;

   delete
     from rpm_mv_cost_retail_changes_gtt;

   delete
     from rpm_mv_min_retail_il_gtt;

   -- If there are any price events in the MV window that are above the tran/loc level, get
   -- the minimum retail data for those price events.  The item/loc with the minimum retail
   -- value will be used later for tax information when working in a GTAX env.

   insert into rpm_mv_min_retail_il_gtt (item,
                                         location,
                                         loc_type,
                                         selling_retail,
                                         action_date)
      select item,
             location,
             loc_type,
             selling_retail,
             action_date
        from (select item,
                     location,
                     loc_type,
                     selling_retail,
                     action_date,
                     ROW_NUMBER() OVER (PARTITION BY selling_retail
                                            ORDER BY item,
                                                     location) rank
                from (select MIN(rfrg.selling_retail) OVER (PARTITION BY rfrg.price_change_id,
                                                                         NVL(il.parent_id, il.zone_id),
                                                                         rfrg.action_date) min_selling_retail,
                             rfrg.selling_retail,
                             rfrg.item,
                             rfrg.location,
                             il.loc_type,
                             rfrg.action_date
                        from rpm_future_retail_gtt rfrg,
                             rpm_mv_item_loc_expl_gtt il
                       where rfrg.item            = il.item
                         and rfrg.location        = il.location
                         and rfrg.price_change_id is NOT NULL)
              where selling_retail = min_selling_retail)
      where rank = 1;

   -- If there are any price events in the MV window that are above the tran/loc level, get
   -- the max cost data for those price events.

   insert into rpm_mv_event_max_cost_gtt (event_id,
                                          max_cost,
                                          currency_code)
      select price_change_id,
             pricing_cost,
             currency_code
        from (select MAX(pricing_cost) OVER (PARTITION BY price_change_id) max_cost,
                     price_change_id,
                     pricing_cost,
                     currency_code
                from (select RANK() OVER (PARTITION BY fc.item,
                                                       fc.location
                                              ORDER BY fc.active_date desc) rank,
                             fc.pricing_cost,
                             pc.price_change_id,
                             fc.currency_code
                        from rpm_future_cost fc,
                             rpm_price_change pc,
                             rpm_future_retail_gtt rfr
                       where rfr.price_change_id is NOT NULL
                         and rfr.price_change_id = pc.price_change_id
                         and (   pc.zone_id      is NOT NULL
                              or pc.item         != rfr.item)
                         and fc.item             = rfr.item
                         and fc.location         = rfr.location
                         and fc.active_date      <= pc.effective_date)
               where rank = 1)
       where max_cost = pricing_cost;

   -- Pull out the max number of retail changes from future cost
   -- and future retail and hold onto them.

   insert into rpm_mv_cost_retail_changes_gtt (item,
                                               parent_id,
                                               location,
                                               loc_type,
                                               action_date,
                                               dept,
                                               retail,
                                               retail_currency,
                                               selling_uom,
                                               change_type,
                                               zone_id,
                                               event_id,
                                               clean_ind,
                                               min_retail_item,
                                               min_retail_loc,
                                               min_retail_loc_type,
                                               max_cost,
                                               max_cost_currency)
      select item,
             parent_id,
             location,
             loc_type,
             action_date,
             dept,
             retail,
             retail_currency,
             selling_uom,
             change_type,
             zone_id,
             event_id,
             'Y',
             min_retail_item,
             min_retail_loc,
             min_retail_loc_type,
             max_cost,
             max_cost_currency
        from (select item,
                     parent_id,
                     location,
                     loc_type,
                     action_date,
                     dept,
                     retail,
                     retail_currency,
                     selling_uom,
                     change_type,
                     zone_id,
                     event_id,
                     DENSE_RANK() OVER (ORDER BY t.action_date asc) fr_future_events_rank,
                     NULL fr_past_events_rank,
                     min_retail_item,
                     min_retail_loc,
                     min_retail_loc_type,
                     max_cost,
                     max_cost_currency
               from (select distinct
                            case
                               when rfr.item = rpc.item or
                                    rpc.link_code is NOT NULL then
                                  ---
                                  rfr.item
                               else
                                  NULL
                            end item,
                            case
                               when rfr.item = rpc.item or
                                    rpc.link_code is NOT NULL then
                                  ---
                                  NULL
                               else
                                  rpc.item
                            end parent_id,
                            case
                               when rpc.zone_id is NULL then
                                  il.location
                               else
                                  NULL
                            end location,
                            case
                               when rpc.zone_id is NULL then
                                  il.loc_type
                               else
                                  NULL
                            end loc_type,
                            min_retail_il.action_date,
                            il.dept,
                            min_retail_il.selling_retail retail,
                            FIRST_VALUE(rfr.selling_retail_currency) OVER (PARTITION BY rfr.price_change_id,
                                                                                        rfr.action_date) retail_currency,
                            FIRST_VALUE(rfr.selling_uom) OVER (PARTITION BY rfr.price_change_id,
                                                                            rfr.action_date) selling_uom,
                            RPM_CONSTANTS.RETAIL_MARKUP change_type,
                            case
                               when rpc.zone_id is NULL then
                                  NULL
                               else
                                  rpc.zone_id
                            end zone_id,
                            rfr.price_change_id event_id,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                   ---
                                   min_retail_il.item
                               else
                                  NULL
                            end min_retail_item,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                   ---
                                   min_retail_il.location
                               else
                                  NULL
                            end min_retail_loc,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                   ---
                                   min_retail_il.loc_type
                               else
                                  NULL
                            end min_retail_loc_type,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                  ---
                                  max_cost.max_cost
                               else
                                  NULL
                            end max_cost,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                  ---
                                  max_cost.currency_code
                               else
                                  NULL
                            end max_cost_currency
                       from rpm_mv_item_loc_expl_gtt il,
                            rpm_future_retail_gtt rfr,
                            rpm_price_change rpc,
                            rpm_mv_min_retail_il_gtt min_retail_il,
                            rpm_mv_event_max_cost_gtt max_cost
                      where il.event_id         = rfr.price_event_id
                        and il.item             = rfr.item
                        and il.location         = rfr.location
                        and il.dept             = rfr.dept
                        and rfr.action_date     > il.effective_date -- Future Retail changes
                        and rfr.item            = NVL(min_retail_il.item, rpc.item)
                        and rfr.location        = NVL(min_retail_il.location, rpc.location)
                        and rfr.action_date     = NVL(min_retail_il.action_date, rpc.effective_date)
                        and rpc.price_change_id = max_cost.event_id (+)
                        and rfr.price_change_id is NOT NULL
                        and rfr.price_change_id = rpc.price_change_id) t
              union all
              select item,
                     parent_id,
                     location,
                     loc_type,
                     action_date,
                     dept,
                     retail,
                     retail_currency,
                     selling_uom,
                     change_type,
                     zone_id,
                     event_id,
                     NULL fr_future_events_rank,
                     DENSE_RANK() OVER (ORDER BY t.action_date desc) fr_past_events_rank,
                     min_retail_item,
                     min_retail_loc,
                     min_retail_loc_type,
                     max_cost,
                     max_cost_currency
               from (select distinct
                            case
                               when rfr.item = rpc.item or
                                    rpc.link_code is NOT NULL then
                                  rfr.item
                               else
                                  NULL
                            end item,
                            case
                               when rfr.item = rpc.item or
                                    rpc.link_code is NOT NULL then
                                  NULL
                               else
                                  rpc.item
                            end parent_id,
                            case
                               when rpc.zone_id is NULL then
                                  il.location
                               else
                                  NULL
                            end location,
                            case
                               when rpc.zone_id is NULL then
                                  il.loc_type
                               else
                                  NULL
                            end loc_type,
                            rfr.action_date,
                            il.dept,
                            min_retail_il.selling_retail retail,
                            FIRST_VALUE(rfr.selling_retail_currency) OVER (PARTITION BY rfr.price_change_id,
                                                                                        rfr.action_date) retail_currency,
                            FIRST_VALUE(rfr.selling_uom) OVER (PARTITION BY rfr.price_change_id,
                                                                            rfr.action_date) selling_uom,
                            RPM_CONSTANTS.RETAIL_MARKUP change_type,
                            case
                               when rpc.zone_id is NULL then
                                  null
                               else
                                  rpc.zone_id
                            end zone_id,
                            rfr.price_change_id event_id,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                   ---
                                   min_retail_il.item
                               else
                                  NULL
                            end min_retail_item,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                   ---
                                   min_retail_il.location
                               else
                                  NULL
                            end min_retail_loc,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                   ---
                                   min_retail_il.loc_type
                               else
                                  NULL
                            end min_retail_loc_type,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                  ---
                                  max_cost.max_cost
                               else
                                  NULL
                            end max_cost,
                            case
                               when rpc.zone_id is NOT NULL or
                                    rfr.item != rpc.item then
                                  ---
                                  max_cost.currency_code
                               else
                                  NULL
                            end max_cost_currency
                       from rpm_mv_item_loc_expl_gtt il,
                            rpm_future_retail_gtt rfr,
                            rpm_price_change rpc,
                            rpm_mv_min_retail_il_gtt min_retail_il,
                            rpm_mv_event_max_cost_gtt max_cost
                      where il.event_id         = rfr.price_event_id
                        and il.item             = rfr.item
                        and il.location         = rfr.location
                        and il.dept             = rfr.dept
                        and rfr.action_date    <= il.effective_date -- Past Retail changes
                        and rfr.item            = NVL(min_retail_il.item, rpc.item)
                        and rfr.location        = NVL(min_retail_il.location, rpc.location)
                        and rfr.action_date     = NVL(min_retail_il.action_date, rpc.effective_date)
                        and rpc.price_change_id = max_cost.event_id (+)
                        and rfr.price_change_id is NOT NULL
                        and rfr.price_change_id = rpc.price_change_id) t)
              where fr_future_events_rank <= L_num_future_markup_events
                 or fr_past_events_rank   <= L_num_past_markup_events;

   -- Set the clean ind appropriately while we only have
   -- retail changes in the table...

   update rpm_mv_cost_retail_changes_gtt crc
      set clean_ind = 'N'
    where EXISTS (select 'x'
                    from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                   where t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                     and crc.zone_id      is NULL)
       or EXISTS (select 'x'
                    from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                   where t.merch_type  IN (RPM_CONSTANTS.PARENT_MERCH_TYPE,
                                           RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE)
                     and crc.parent_id is NULL);

   -- merge any cost changes into the crc table.  If a cost change occurs
   -- on the same date as a retail change, just update the change type, else
   -- insert a new record for the cost change.

   merge into rpm_mv_cost_retail_changes_gtt target
   using (select item,
                 location,
                 loc_type,
                 cost,
                 cost_currency,
                 action_date,
                 dept,
                 parent_id,
                 zone_id,
                 change_type,
                 retail,
                 retail_currency,
                 selling_uom,
                 fc_future_events_rank,
                 fc_past_events_rank
            from (-- Get Future Cost Changes
                  select distinct fc.item,
                         fc.location,
                         fc.loc_type,
                         fc.pricing_cost cost,
                         fc.currency_code cost_currency,
                         fc.active_date action_date,
                         il.dept,
                         il.parent_id,
                         il.zone_id,
                         RPM_CONSTANTS.COST_MARKUP change_type,
                         FIRST_VALUE(rfr.selling_retail) OVER (PARTITION BY rfr.item,
                                                                            rfr.location
                                                                   ORDER BY rfr.action_date desc) retail,
                         FIRST_VALUE(rfr.selling_retail_currency) OVER (PARTITION BY rfr.item,
                                                                                     rfr.location
                                                                            ORDER BY rfr.action_date desc) retail_currency,
                         FIRST_VALUE(rfr.selling_uom) OVER (PARTITION BY rfr.item,
                                                                         rfr.location
                                                                ORDER BY rfr.action_date desc) selling_uom,
                         DENSE_RANK() OVER (PARTITION BY fc.item,
                                                         fc.location
                                                ORDER BY fc.active_date asc) fc_future_events_rank,
                         NULL fc_past_events_rank
                    from rpm_future_cost fc,
                         rpm_mv_item_loc_expl_gtt il,
                         rpm_future_retail_gtt rfr
                   where il.item            = fc.item
                     and il.location        = fc.location
                     and fc.active_date     > il.effective_date  --Future Cost Changes
                     and rfr.price_event_id = il.event_id
                     and rfr.item           = il.item
                     and rfr.location       = il.location
                     and il.dept            = rfr.dept
                     and rfr.action_date   <= fc.active_date
                  union all
                  -- Get Past Cost Changes
                  select distinct fc.item,
                         fc.location,
                         fc.loc_type,
                         fc.pricing_cost cost,
                         fc.currency_code cost_currency,
                         fc.active_date action_date,
                         il.dept,
                         il.parent_id,
                         il.zone_id,
                         RPM_CONSTANTS.COST_MARKUP change_type,
                         FIRST_VALUE(rfr.selling_retail) OVER (PARTITION BY rfr.item,
                                                                            rfr.location
                                                                   ORDER BY rfr.action_date desc) retail,
                         FIRST_VALUE(rfr.selling_retail_currency) OVER (PARTITION BY rfr.item,
                                                                                     rfr.location
                                                                            ORDER BY rfr.action_date desc) retail_currency,
                         FIRST_VALUE(rfr.selling_uom) OVER (PARTITION BY rfr.item,
                                                                         rfr.location
                                                                ORDER BY rfr.action_date desc) selling_uom,
                         NULL fc_future_events_rank,
                         DENSE_RANK() OVER (PARTITION BY fc.item,
                                                         fc.location
                                                ORDER BY fc.active_date desc) fc_past_events_rank
                    from rpm_future_cost fc,
                         rpm_mv_item_loc_expl_gtt il,
                         rpm_future_retail_gtt rfr
                   where il.item            = fc.item
                     and il.location        = fc.location
                     and fc.active_date    <= il.effective_date  --Past Cost Changes
                     and rfr.price_event_id = il.event_id
                     and rfr.item           = il.item
                     and rfr.location       = il.location
                     and il.dept            = rfr.dept
                     and rfr.action_date   <= fc.active_date)
           where fc_future_events_rank <= L_num_future_markup_events
              or fc_past_events_rank   <= L_num_past_markup_events) source
      on (    NVL(target.item, target.parent_id)   = NVL(source.item, source.parent_id)
          and NVL(target.location, target.zone_id) = NVL(source.location, source.zone_id)
          and target.action_date                   = source.action_date)
   when MATCHED then
      update
         set target.change_type   = COST_AND_RETAIL_MARKUP,
             target.cost          = source.cost,
             target.cost_currency = source.cost_currency
   when NOT MATCHED then
      insert (item,
              parent_id,
              location,
              loc_type,
              action_date,
              dept,
              retail,
              retail_currency,
              selling_uom,
              change_type,
              cost,
              cost_currency,
              zone_id,
              clean_ind)
      values (source.item,
              NULL, --parent_id
              source.location,
              source.loc_type,
              source.action_date,
              source.dept,
              source.retail,
              source.retail_currency,
              source.selling_uom,
              source.change_type,
              source.cost,
              source.cost_currency,
              NULL, --zone_id
              'Y'); --clean_ind

   delete
     from rpm_il_markup_gtt;

   -- insert records into the rpm_il_markup_gtt table for processing by the rpm_markup_calc_sql package.
   -- if we have a parent item situation, the cost will be set to the max cost for all the children of
   -- of that parent so that the markup % is the lowest it can be (the min retail was already pulled
   -- into the rpm_mv_cost_retail_changes_gtt table earlier).
   -- Also, if dealing with a zone situation, the vat rate should be set so that it just comes from one
   -- of the locations in the zone for the item being populated.

   if L_default_tax_type != RPM_CONSTANTS.GTAX_TAX_TYPE then

      open C_CHECK_CUSTOM;
      fetch C_CHECK_CUSTOM into L_custom_tax_ind;
      close C_CHECK_CUSTOM;

      if NVL(L_custom_tax_ind, 0) = 1 then

         select OBJ_TAX_CALC_REC(item, -- I_item
                                 NULL, -- I_pack_ind
                                 location, -- I_from_entity
                                 DECODE(loc_type,
                                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_from_entity_type
                                 location, -- I_to_entity
                                 DECODE(loc_type,
                                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_to_entity_type
                                 action_date,     -- I_effective_from_date
                                 retail,          -- I_amount
                                 retail_currency, -- I_amount_curr
                                 NULL,            -- I_amount_tax_incl_ind
                                 NULL,            -- I_origin_country_id
                                 NULL,            -- O_cum_tax_pct
                                 NULL,            -- O_cum_tax_value
                                 NULL,            -- O_total_tax_amount
                                 NULL,            -- O_total_tax_amount_curr
                                 NULL,            -- O_total_recover_amount
                                 NULL,            -- O_total_recover_amount_curr
                                 NULL,            -- O_tax_detail_tbl
                                 'MARKUPCALC',    -- I_tran_type
                                 action_date,     -- I_tran_date
                                 NULL,            -- I_tran_id
                                 'R')             -- I_cost_retail_ind
            BULK COLLECT into L_tax_calc_tbl
           from (select item,
                        rzl.location,
                        DECODE (rzl.loc_type,
                                RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                        action_date,
                        retail,
                        retail_currency
                  from rpm_mv_cost_retail_changes_gtt gtt,
                       rpm_zone_location rzl
                 where gtt.zone_id is NOT NULL
                   and gtt.zone_id = rzl.zone_id);

         if TAX_SQL.CALC_RETAIL_TAX(O_error_msg,
                                    L_tax_calc_tbl) = FALSE then
            return 0;
         end if;

      end if;

      insert into rpm_il_markup_gtt (item,
                                     location,
                                     loc_type,
                                     action_date,
                                     dept,
                                     retail,
                                     retail_currency,
                                     cost,
                                     cost_currency,
                                     selling_uom,
                                     vat_rate)
         select distinct
                NVL(crc.item, crc.parent_id),
                NVL(crc.location, crc.zone_id),
                NVL(crc.loc_type, 'Z'),
                crc.action_date,
                crc.dept,
                crc.retail,
                crc.retail_currency,
                case
                   when crc.parent_id is NOT NULL or
                        crc.zone_id is NOT NULL then
                      --
                      (select MAX(fc.pricing_cost)
                         from rpm_future_cost fc,
                              rpm_mv_item_loc_expl_gtt il
                        where fc.item        = il.item
                          and fc.location    = il.location
                          and fc.loc_type    = il.loc_type
                          and fc.active_date <= crc.action_date)
                   else
                      NULL
                end cost,
                case
                   when crc.parent_id is NOT NULL or
                        crc.zone_id is NOT NULL then
                      ---
                      (select distinct FIRST_VALUE(fc.currency_code) OVER (PARTITION BY fc.item,
                                                                                        fc.location
                                                                               ORDER BY fc.active_date)
                         from rpm_future_cost fc,
                              rpm_mv_item_loc_expl_gtt il
                        where fc.item        = il.item
                          and fc.location    = il.location
                          and fc.loc_type    = il.loc_type
                          and fc.active_date <= crc.action_date)
                   else
                      NULL
                end cost_currency,
                crc.selling_uom,
                case
                   when L_default_tax_type = RPM_CONSTANTS.SALES_TAX_TYPE then
                      NULL
                   when NVL(L_custom_tax_ind, 0) = 0 and
                        crc.zone_id is NOT NULL then
                      --
                      (select distinct FIRST_VALUE(vi.vat_rate) OVER (PARTITION BY crc.zone_id,
                                                                                   im.item
                                                                          ORDER BY vi.active_date,
                                                                                   rzl.location desc) vat_rate
                         from (select store loc,
                                      0 zone_node_type,
                                      vat_region
                                 from store s
                               union all
                               select wh loc,
                                      2 zone_node_type,
                                      vat_region
                                 from wh) locs,
                              vat_item vi,
                              item_master im,
                              class c,
                              rpm_zone_location rzl
                        where rzl.loc_type    = locs.zone_node_type
                          and crc.zone_id     = rzl.zone_id
                          and (   im.item     = crc.item
                               or im.item     = crc.parent_id)
                          and vi.item         = im.item
                          and locs.loc        = rzl.location
                          and im.class        = c.class
                          and vi.vat_type     IN (RPM_CONSTANTS.VAT_TYPE_RETAIL,
                                                  RPM_CONSTANTS.VAT_TYPE_BOTH)
                          and vi.vat_region   = locs.vat_region
                          and vi.active_date <= crc.action_date
                          and (   L_class_level_vat_ind      = 'N'
                               or (    L_class_level_vat_ind = 'Y'
                                   and c.class_vat_ind       = 'Y')))
                   when NVL(L_custom_tax_ind, 0) = 1 and
                        crc.zone_id is NOT NULL then
                      --
                      (select vat_rate
                         from (select /*+ CARDINALITY(tax_tbl 10) */
                                      tax_tbl.O_cum_tax_pct vat_rate,
                                      ROW_NUMBER() OVER (PARTITION BY crc.item,
                                                                      crc.zone_id
                                                             ORDER BY crc.action_date) rank
                                 from table(cast(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl,
                                      rpm_zone_location rzl
                                where crc.zone_id     is NOT NULL
                                  and crc.item        = tax_tbl.I_item
                                  and crc.action_date = tax_tbl.I_effective_from_date
                                  and rzl.zone_id     = crc.zone_id
                                  and rzl.location    = tax_tbl.I_from_entity
                                  and rzl.loc_type    = DECODE(tax_tbl.I_from_entity_type,
                                                               RPM_CONSTANTS.TAX_LOC_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE))

                        where rank = 1)
                   else
                      NULL
                end vat_rate
           from rpm_mv_cost_retail_changes_gtt crc;

   else

      insert into rpm_il_markup_gtt (item,
                                     location,
                                     loc_type,
                                     action_date,
                                     dept,
                                     retail,
                                     retail_currency,
                                     cost,
                                     cost_currency,
                                     selling_uom,
                                     vat_rate,
                                     vat_value)
         -- item/loc
         select item,
                location,
                loc_type,
                action_date,
                dept,
                retail,
                retail_currency,
                NULL, -- cost
                NULL, -- cost_currency
                selling_uom,
                cum_tax_pct, -- vat_rate
                cum_tax_value -- vat_value
           from (select distinct mcrc.item item,
                        mcrc.location,
                        mcrc.loc_type,
                        mcrc.action_date,
                        mcrc.dept,
                        mcrc.retail,
                        mcrc.retail_currency,
                        NULL,
                        NULL,
                        mcrc.selling_uom,
                        gt.effective_from_date,
                        gt.cum_tax_pct, -- vat_rate
                        gt.cum_tax_value, -- vat_value
                        FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.item,
                                                                                gt.loc
                                                                       ORDER BY gt.effective_from_date desc) max_date
                   from gtax_item_rollup gt,
                        rpm_mv_cost_retail_changes_gtt mcrc
                  where mcrc.item                   is NOT NULL
                    and mcrc.location               is NOT NULL
                    and gt.item (+)                 =  NVL(mcrc.min_retail_item, mcrc.item)
                    and gt.loc  (+)                 =  NVL(mcrc.min_retail_loc, mcrc.location)
                    -- we cannot simply say gt.to_entity_type = gt.from_entity_type below due to the outter join needed...
                    and gt.loc_type (+)             = DECODE(NVL(mcrc.min_retail_loc_type, mcrc.loc_type),
                                                             RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                             RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                    and gt.effective_from_date (+) <= mcrc.action_date)
          where NVL(effective_from_date, L_effective_date) = NVL(max_date, L_effective_date)
         union all
         -- parent/loc
         select parent_id,
                location,
                loc_type,
                action_date,
                dept,
                retail,
                retail_currency,
                pricing_cost,
                cost_currency,
                selling_uom,
                cum_tax_pct vat_rate,
                cum_tax_value vat_value
           from (select distinct mcrc.parent_id,
                        mcrc.location,
                        mcrc.loc_type,
                        mcrc.action_date,
                        mcrc.dept,
                        mcrc.retail,
                        mcrc.retail_currency,
                        mcrc.max_cost pricing_cost,
                        mcrc.max_cost_currency cost_currency,
                        mcrc.selling_uom,
                        gt.effective_from_date,
                        gt.cum_tax_pct, -- vat_rate
                        gt.cum_tax_value, -- vat_value
                        FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.item,
                                                                                gt.loc
                                                                       ORDER BY gt.effective_from_date desc) max_date
                   from gtax_item_rollup gt,
                        rpm_mv_cost_retail_changes_gtt mcrc
                  where mcrc.parent_id              is NOT NULL
                    and mcrc.location               is NOT NULL
                    and gt.item (+)                 = NVL(mcrc.min_retail_item, mcrc.item)
                    and gt.loc (+)                  = NVL(mcrc.min_retail_loc, mcrc.location)
                    -- we cannot simply say gt.to_entity_type = gt.from_entity_type below due to the outter join needed...
                    and gt.loc_type (+)             = DECODE(NVL(mcrc.min_retail_loc_type, mcrc.loc_type),
                                                             RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                             RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                    and gt.effective_from_date (+) <= mcrc.action_date)
          where NVL(effective_from_date, L_effective_date) = NVL(max_date, L_effective_date)
         union all
         -- item/zone
         select item,
                zone_id,
                'Z' loc_type,
                action_date,
                dept,
                retail,
                retail_currency,
                pricing_cost,
                cost_currency,
                selling_uom,
                cum_tax_pct,
                cum_tax_value
           from (select distinct mcrc.item,
                        mcrc.zone_id,
                        'Z' loc_type,
                        mcrc.action_date,
                        mcrc.dept,
                        mcrc.retail,
                        mcrc.retail_currency,
                        mcrc.max_cost pricing_cost,
                        mcrc.max_cost_currency cost_currency,
                        mcrc.selling_uom,
                        gt.effective_from_date,
                        gt.cum_tax_pct, -- vat_rate
                        gt.cum_tax_value, -- vat_value
                        FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.item,
                                                                                gt.loc
                                                                       ORDER BY gt.effective_from_date desc) max_date
                   from gtax_item_rollup gt,
                        rpm_mv_cost_retail_changes_gtt mcrc
                  where mcrc.item                    is NOT NULL
                    and mcrc.zone_id                 is NOT NULL
                    and gt.item (+)                  = NVL(mcrc.min_retail_item, mcrc.item)
                    and gt.loc (+)                   = NVL(mcrc.min_retail_loc, mcrc.location)
                    and gt.effective_from_date (+)  <= mcrc.action_date
                    and gt.loc_type (+)              = DECODE(NVL(mcrc.min_retail_loc_type, mcrc.loc_type),
                                                              RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                              RPM_CONSTANTS.TAX_LOC_TYPE_WH))
          where NVL(effective_from_date, L_effective_date) = NVL(max_date, L_effective_date)
         union all
         -- parent/zone
         select parent_id,
                zone_id,
                'Z' loc_type,
                action_date,
                dept,
                retail,
                retail_currency,
                MAX(pricing_cost) pricing_cost,
                cost_currency,
                selling_uom,
                cum_tax_pct,
                cum_tax_value
           from (select distinct mcrc.parent_id,
                        mcrc.zone_id,
                        'Z' loc_type,
                        mcrc.action_date,
                        mcrc.dept,
                        mcrc.retail,
                        mcrc.retail_currency,
                        mcrc.max_cost pricing_cost,
                        mcrc.max_cost_currency cost_currency,
                        mcrc.selling_uom,
                        gt.effective_from_date,
                        gt.cum_tax_pct, -- vat_rate
                        gt.cum_tax_value, -- vat_value
                        FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.item,
                                                                                gt.loc
                                                                       ORDER BY gt.effective_from_date desc) max_date
                   from gtax_item_rollup gt,
                        rpm_mv_cost_retail_changes_gtt mcrc
                  where mcrc.parent_id              is NOT NULL
                    and mcrc.zone_id                is NOT NULL
                    and gt.item (+)                 = NVL(mcrc.min_retail_item, mcrc.item)
                    and gt.loc (+)                  = NVL(mcrc.min_retail_loc, mcrc.location)
                    and gt.loc_type (+)             = DECODE(NVL(mcrc.min_retail_loc_type, mcrc.loc_type),
                                                             RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                             RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                    and gt.effective_from_date (+) <= mcrc.action_date)
          where NVL(effective_from_date, L_effective_date) = NVL(max_date, L_effective_date)
          group by parent_id,
                   zone_id,
                   action_date,
                   dept,
                   retail,
                   retail_currency,
                   pricing_cost,
                   cost_currency,
                   selling_uom,
                   cum_tax_pct,
                   cum_tax_value;

   end if;

   if RPM_MARKUP_CALC_SQL.CALCULATE_MARKUP(O_error_msg) != 1 then
      return 0;
   end if;

   open C_GET_MARKUP_CHG_DATES;
   fetch C_GET_MARKUP_CHG_DATES BULK COLLECT into O_mv_markup_chg_dates;
   close C_GET_MARKUP_CHG_DATES;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END GET_MARKUP_DETAILS;
----------------------------------------------------------------------------------

FUNCTION EXPL_SIMPLE_PROMO_IL(O_error_msg            OUT VARCHAR2,
                              I_obj_rpm_promo_tbl IN     OBJ_RPM_PROMO_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_MARGIN_VISIBILITY_SQL.EXPL_SIMPLE_PROMO_IL';

BEGIN

   insert into rpm_mv_item_loc_expl_gtt (event_id,
                                         dept,
                                         item,
                                         parent_id,
                                         diff_id,
                                         location,
                                         loc_type,
                                         effective_date)
       --   Item Loc Level
       select /*+ CARDINALITY(promo_dtl 10) */
              promo_dtl.rpm_promo_comp_detail_id,
              im.dept,
              im.item,
              NULL parent_id,
              NULL diff_id,
              promo_dtl.location,
              RPM_CONSTANTS.LOCATION_TYPE_STORE,
              promo_dtl.start_date
         from table(cast(I_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) promo_dtl,
              item_master im
        where promo_dtl.merch_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and promo_dtl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
          and promo_dtl.state          IN ('state.worksheet',
                                           'state.submitted')
          and im.item                  = promo_dtl.item
          and im.item_level            = im.tran_level
          and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
       union all
       -- Item Parent Loc Level
       select /*+ CARDINALITY(promo_dtl 10) */
              promo_dtl.rpm_promo_comp_detail_id,
              im.dept,
              im.item,
              promo_dtl.item parent_id,
              NULL diff_id,
              promo_dtl.location,
              RPM_CONSTANTS.LOCATION_TYPE_STORE,
              promo_dtl.start_date
         from table(cast(I_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) promo_dtl,
              item_master im
        where promo_dtl.merch_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and promo_dtl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
          and promo_dtl.state          IN ('state.worksheet',
                                           'state.submitted')
          and im.item_parent           = promo_dtl.item
          and im.item_level            = im.tran_level
          and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
       union all
       -- Parent Diff Loc Level
       select /*+ CARDINALITY(promo_dtl 10) */
              promo_dtl.rpm_promo_comp_detail_id,
              im.dept,
              im.item,
              promo_dtl.item parent_id,
              promo_dtl.diff_id diff_id,
              promo_dtl.location,
              RPM_CONSTANTS.LOCATION_TYPE_STORE,
              promo_dtl.start_date
         from table(cast(I_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) promo_dtl,
              item_master im
        where promo_dtl.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and promo_dtl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
          and promo_dtl.state          IN ('state.worksheet',
                                           'state.submitted')
          and im.item_parent           = promo_dtl.item
          and im.item_level            = im.tran_level
          and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and (   im.diff_1 = promo_dtl.diff_id
               or im.diff_2 = promo_dtl.diff_id
               or im.diff_3 = promo_dtl.diff_id
               or im.diff_4 = promo_dtl.diff_id)
       -- Item Zone Level
       union all
       select /*+ CARDINALITY(promo_dtl 10) */
              promo_dtl.rpm_promo_comp_detail_id,
              im.dept,
              im.item,
              NULL parent_id,
              NULL diff_id,
              rzl.location,
              RPM_CONSTANTS.LOCATION_TYPE_STORE,
              promo_dtl.start_date
         from table(cast(I_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) promo_dtl,
              rpm_zone_location rzl,
              item_master im
        where promo_dtl.merch_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and promo_dtl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and promo_dtl.state          IN ('state.worksheet',
                                           'state.submitted')
          and rzl.zone_id              = promo_dtl.zone_id
          and rzl.loc_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
          and im.item                  = promo_dtl.item
          and im.item_level            = im.tran_level
          and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
       union all
       -- Item Parent Zone Level
       select /*+ CARDINALITY(promo_dtl 10) */
              promo_dtl.rpm_promo_comp_detail_id,
              im.dept,
              im.item,
              promo_dtl.item parent_id,
              NULL diff_id,
              rzl.location,
              RPM_CONSTANTS.LOCATION_TYPE_STORE,
              promo_dtl.start_date
         from table(cast(I_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) promo_dtl,
              rpm_zone_location rzl,
              item_master im
        where promo_dtl.merch_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and promo_dtl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and promo_dtl.state          IN ('state.worksheet',
                                           'state.submitted')
          and rzl.zone_id              = promo_dtl.zone_id
          and rzl.loc_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
          and im.item_parent           = promo_dtl.item
          and im.item_level            = im.tran_level
          and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
       union all
       -- Parent Diff Zone Level
       select /*+ CARDINALITY(promo_dtl 10) */
              promo_dtl.rpm_promo_comp_detail_id,
              im.dept,
              im.item,
              promo_dtl.item parent_id,
              promo_dtl.diff_id diff_id,
              rzl.location,
              RPM_CONSTANTS.LOCATION_TYPE_STORE,
              promo_dtl.start_date
         from table(cast(I_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) promo_dtl,
              rpm_zone_location rzl,
              item_master im
        where promo_dtl.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and promo_dtl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and promo_dtl.state          IN ('state.worksheet',
                                           'state.submitted')
          and rzl.zone_id              = promo_dtl.zone_id
          and rzl.loc_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
          and im.item_parent           = promo_dtl.item
          and im.item_level            = im.tran_level
          and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and (   im.diff_1 = promo_dtl.diff_id
               or im.diff_2 = promo_dtl.diff_id
               or im.diff_3 = promo_dtl.diff_id
               or im.diff_4 = promo_dtl.diff_id);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
   return 0;

END EXPL_SIMPLE_PROMO_IL;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION EXPL_PRICE_CHANGE_CLEAR_IL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_MARGIN_VISIBILITY_SQL.EXPL_PRICE_CHANGE_CLEAR_IL';

BEGIN

   insert into rpm_mv_item_loc_expl_gtt
      (event_id,
       dept,
       item,
       diff_id,
       location,
       loc_type,
       effective_date,
       parent_id,
       zone_id)
    -- item - loc level
    select rpc.price_change_id,
           im.dept,
           im.item,
           NULL diff_id,
           rpc.location,
           DECODE(rpc.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           rpc.effective_date,
           NULL parent_id,
           NULL zone_id
      from item_master im,
           rpm_pc_rec_gtt rpc
     where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and im.item            = rpc.item
       and rpc.diff_id        is NULL
       and im.item_level      = im.tran_level
       and rpc.state          IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE)
    -- item parent - loc level
    union all
    select rpc.price_change_id,
           im.dept,
           im.item,
           NULL diff_id,
           rpc.location,
           DECODE(rpc.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           rpc.effective_date,
           rpc.item parent_id,
           NULL zone_id
      from item_master im,
           rpm_pc_rec_gtt rpc
     where rpc.diff_id        is NULL
       and rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and im.item_parent     = rpc.item
       and rpc.diff_id        is NULL
       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
       and rpc.state          IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE)
    -- item parent/diff - loc level
    union all
    select rpc.price_change_id,
           im.dept,
           im.item,
           rpc.diff_id diff_id,
           rpc.location,
           DECODE(rpc.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           rpc.effective_date,
           rpc.item parent_id,
           NULL zone_id
      from item_master im,
           rpm_pc_rec_gtt rpc
     where rpc.diff_id        is NOT NULL
       and rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and im.item_parent     = rpc.item
       and (   im.diff_1 = rpc.diff_id
            or im.diff_2 = rpc.diff_id
            or im.diff_3 = rpc.diff_id
            or im.diff_4 = rpc.diff_id)
       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
       and rpc.state          IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE)
    -- item - zone level
    union all
    select rpc.price_change_id,
           im.dept,
           im.item,
           NULL diff_id,
           rzl.location,
           DECODE(rpc.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           rpc.effective_date,
           NULL parent_id,
           rpc.zone_id zone_id
      from item_master im,
           rpm_zone_location rzl,
           rpm_pc_rec_gtt rpc
     where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and im.item            = rpc.item
       and rzl.zone_id        = rpc.zone_id
       and rpc.diff_id        is NULL
       and im.item_level      = im.tran_level
       and rpc.state          IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE)
    -- item parent - zone level
    union all
    select rpc.price_change_id,
           im.dept,
           im.item,
           NULL diff_id,
           rzl.location,
           DECODE(rpc.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           rpc.effective_date,
           rpc.item parent_id,
           rpc.zone_id zone_id
      from item_master im,
           rpm_zone_location rzl,
           rpm_pc_rec_gtt rpc
     where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and im.item_parent     = rpc.item
       and rzl.zone_id        = rpc.zone_id
       and rpc.diff_id        is NULL
       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
       and rpc.state          IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE)
    -- item parent diff - zone level
    union all
    select rpc.price_change_id,
           im.dept,
           im.item,
           rpc.diff_id diff_id,
           rzl.location,
           DECODE(rpc.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           rpc.effective_date,
           rpc.item parent_id,
           rpc.zone_id zone_id
      from item_master im,
           rpm_zone_location rzl,
           rpm_pc_rec_gtt rpc
     where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and im.item_parent     = rpc.item
       and rpc.diff_id        is NOT NULL
       and (   im.diff_1 = rpc.diff_id
            or im.diff_2 = rpc.diff_id
            or im.diff_3 = rpc.diff_id
            or im.diff_4 = rpc.diff_id)
       and rzl.zone_id        = rpc.zone_id
       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
       and rpc.state          IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END EXPL_PRICE_CHANGE_CLEAR_IL;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION EXPL_PRICE_EVENT(O_error_msg          OUT VARCHAR2,
                          I_mv_criteria_tbl IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER IS

   L_program  VARCHAR2(50) := 'RPM_MARGIN_VISIBILITY.EXPL_PRICE_EVENT';

BEGIN

   delete
     from rpm_mv_item_loc_expl_gtt;

   insert into rpm_mv_item_loc_expl_gtt
      (event_id,
       dept,
       item,
       parent_id,
       location,
       loc_type,
       zone_id,
       effective_date)
    ---
    -- item - loc level
    ---
    select 9999,
           im.dept,
           im.item,
           NULL parent_id,
           t.location,
           DECODE(t.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           NULL zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im
     where t.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and t.merch_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
       and im.item          = t.item
       and t.diff_id        is NULL
       and im.item_level    = im.tran_level
    ---
    -- item parent - loc level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           t.item parent_id,
           t.location,
           DECODE(t.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           NULL zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im
     where t.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and t.merch_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
       and im.item_parent   = t.item
       and t.diff_id        is NULL
       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
    ---
    -- item parent/diff - loc level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           t.item parent_id,
           t.location,
           DECODE(t.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           NULL zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im
     where t.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and t.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
       and im.item_parent   = t.item
       and t.diff_id        is NOT NULL
       and (   im.diff_1 = t.diff_id
            or im.diff_2 = t.diff_id
            or im.diff_3 = t.diff_id
            or im.diff_4 = t.diff_id)
       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
    ---
    -- item heir - loc level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           NULL parent_id,
           t.location,
           DECODE(t.zone_node_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           NULL zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im
     where t.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and im.dept          = t.dept
       and im.class         = NVL(t.class, im.class)
       and im.subclass      = NVL(t.subclass, im.subclass)
       and im.item_level    = im.tran_level
       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
       and t.merch_type     IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
    ---
    -- item - zone level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           NULL parent_id,
           rzl.location,
           DECODE(rzl.loc_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           t.zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im,
           rpm_zone_location rzl
     where t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and t.merch_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
       and im.item          = t.item
       and rzl.zone_id      = t.zone_id
       and t.diff_id        is NULL
       and im.item_level    = im.tran_level
    ---
    -- item parent - zone level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           t.item parent_id,
           rzl.location,
           DECODE(rzl.loc_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           t.zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im,
           rpm_zone_location rzl
     where t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and t.merch_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
       and im.item_parent   = t.item
       and rzl.zone_id      = t.zone_id
       and t.diff_id        is NULL
       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
    ---
    -- item parent/diff - zone level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           t.item parent_id,
           rzl.location,
           DECODE(rzl.loc_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           t.zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im,
           rpm_zone_location rzl
     where t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and t.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
       and im.item_parent   = t.item
       and t.diff_id        is NOT NULL
       and (   im.diff_1 = t.diff_id
            or im.diff_2 = t.diff_id
            or im.diff_3 = t.diff_id
            or im.diff_4 = t.diff_id)
       and rzl.zone_id      = t.zone_id
       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
    ---
    -- item heir - zone level
    ---
    union all
    select 9999,
           im.dept,
           im.item,
           NULL parent_id,
           rzl.location,
           DECODE(rzl.loc_type,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH),
           t.zone_id,
           t.effective_date
      from table(cast(I_mv_criteria_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
           item_master im,
           rpm_zone_location rzl
     where t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
       and rzl.zone_id      = t.zone_id
       and im.dept          = t.dept
       and im.class         = NVL(t.class, im.class)
       and im.subclass      = NVL(t.subclass, im.subclass)
       and im.item_level    = im.tran_level
       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
       and t.merch_type     IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                RPM_CONSTANTS.SUBCLASS_MERCH_TYPE);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
   return 0;

END EXPL_PRICE_EVENT;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION EXPL_FR_TO_IL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_MARGIN_VISIBILITY_SQL.EXPL_FR_TO_IL';

BEGIN

   delete from rpm_future_retail_gtt;

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       diff_id,
       item_parent,
       zone_node_type,
       location,
       zone_id,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       lock_version,
       rfr_rowid,
       timeline_seq,
       on_simple_promo_ind,
       on_complex_promo_ind,
       max_hier_level,
       cur_hier_level)
   select event_id,
          future_retail_id,
          dept,
          class,
          subclass,
          item,
          diff_id,
          NULL item_parent,
          zone_node_type,
          location,
          NULL zone_id,
          action_date,
          selling_retail,
          selling_retail_currency,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_unit_retail_currency,
          multi_selling_uom,
          clear_retail,
          clear_retail_currency,
          clear_uom,
          simple_promo_retail,
          simple_promo_retail_currency,
          simple_promo_uom,
          price_change_id,
          price_change_display_id,
          pc_exception_parent_id,
          pc_change_type,
          pc_change_amount,
          pc_change_currency,
          pc_change_percent,
          pc_change_selling_uom,
          pc_null_multi_ind,
          pc_multi_units,
          pc_multi_unit_retail,
          pc_multi_unit_retail_currency,
          pc_multi_selling_uom,
          pc_price_guide_id,
          clearance_id,
          clearance_display_id,
          clear_mkdn_index,
          clear_start_ind,
          clear_change_type,
          clear_change_amount,
          clear_change_currency,
          clear_change_percent,
          clear_change_selling_uom,
          clear_price_guide_id,
          loc_move_from_zone_id,
          loc_move_to_zone_id,
          location_move_id,
          NULL lock_version,
          NULL rfr_rowid,
          NULL timeline_seq,
          on_simple_promo_ind,
          on_complex_promo_ind,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
     from (select t.event_id,
                  t.to_item item,
                  t.to_diff_id diff_id,
                  t.to_location location,
                  t.to_zone_node_type zone_node_type,
                  fr.future_retail_id,
                  fr.dept,
                  fr.class,
                  fr.subclass,
                  fr.action_date,
                  fr.selling_retail,
                  fr.selling_retail_currency,
                  fr.selling_uom,
                  fr.multi_units,
                  fr.multi_unit_retail,
                  fr.multi_unit_retail_currency,
                  fr.multi_selling_uom,
                  fr.clear_retail,
                  fr.clear_retail_currency,
                  fr.clear_uom,
                  fr.simple_promo_retail,
                  fr.simple_promo_retail_currency,
                  fr.simple_promo_uom,
                  fr.price_change_id,
                  fr.price_change_display_id,
                  fr.pc_exception_parent_id,
                  fr.pc_change_type,
                  fr.pc_change_amount,
                  fr.pc_change_currency,
                  fr.pc_change_percent,
                  fr.pc_change_selling_uom,
                  fr.pc_null_multi_ind,
                  fr.pc_multi_units,
                  fr.pc_multi_unit_retail,
                  fr.pc_multi_unit_retail_currency,
                  fr.pc_multi_selling_uom,
                  fr.pc_price_guide_id,
                  fr.clearance_id,
                  fr.clearance_display_id,
                  fr.clear_mkdn_index,
                  fr.clear_start_ind,
                  fr.clear_change_type,
                  fr.clear_change_amount,
                  fr.clear_change_currency,
                  fr.clear_change_percent,
                  fr.clear_change_selling_uom,
                  fr.clear_price_guide_id,
                  fr.loc_move_from_zone_id,
                  fr.loc_move_to_zone_id,
                  fr.location_move_id,
                  fr.on_simple_promo_ind,
                  fr.on_complex_promo_ind,
                  fr.max_hier_level,
                  t.fr_rank,
                  MAX(t.fr_rank) OVER (PARTITION BY t.to_item,
                                                    NVL(t.to_location, fr.location),
                                                    NVL(t.to_zone_node_type, fr.zone_node_type)) max_fr_rank
             from (-- Get IL timelines from existing PZ timelines
                   select distinct
                          0 fr_rank,
                          mileg.event_id,
                          im.dept,
                          im.item_parent from_item,
                          NULL from_diff_id,
                          rz.zone_id from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          im.item to_item,
                          im.diff_1 to_diff_id,
                          rzl.location to_location,
                          rzl.loc_type to_zone_node_type
                     from rpm_mv_item_loc_expl_gtt mileg,
                          item_master im,
                          rpm_merch_retail_def_expl rmrde,
                          rpm_zone_location rzl,
                          rpm_zone rz
                    where im.item                  = mileg.item
                      and im.item_level            = im.tran_level
                      and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and im.item_parent           is NOT NULL
                      and rmrde.dept               = im.dept
                      and rmrde.class              = im.class
                      and rmrde.subclass           = im.subclass
                      and rmrde.regular_zone_group = rz.zone_group_id
                      and rz.zone_id               = rzl.zone_id
                      and rzl.location             = mileg.location
                      and rzl.loc_type             = DECODE(mileg.loc_type,
                                                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                   -- Get IL timelines from existing PDZ timelines
                   union all
                   select distinct
                          1 fr_rank,
                          mileg.event_id,
                          im.dept,
                          im.item_parent from_item,
                          im.diff_1 from_diff_id,
                          rz.zone_id from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          im.item to_item,
                          im.diff_1 to_diff_id,
                          rzl.location to_location,
                          rzl.loc_type to_zone_node_type
                     from rpm_mv_item_loc_expl_gtt mileg,
                          item_master im,
                          rpm_merch_retail_def_expl rmrde,
                          rpm_zone_location rzl,
                          rpm_zone rz
                    where im.item                  = mileg.item
                      and im.item_level            = im.tran_level
                      and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and im.item_parent           is NOT NULL
                      and rmrde.dept               = im.dept
                      and rmrde.class              = im.class
                      and rmrde.subclass           = im.subclass
                      and rmrde.regular_zone_group = rz.zone_group_id
                      and rz.zone_id               = rzl.zone_id
                      and rzl.location             = mileg.location
                      and rzl.loc_type             = DECODE(mileg.loc_type,
                                                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                   -- Get IL timelines from existing PL timelines
                   union all
                   select distinct
                          1 fr_rank,
                          mileg.event_id,
                          im.dept,
                          im.item_parent from_item,
                          NULL from_diff_id,
                          mileg.location from_location,
                          DECODE(mileg.loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) from_zone_node_type,
                          im.item to_item,
                          im.diff_1 to_diff_id,
                          mileg.location to_location,
                          DECODE(mileg.loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) to_zone_node_type
                     from rpm_mv_item_loc_expl_gtt mileg,
                          item_master im
                    where im.item         = mileg.item
                      and im.item_level   = im.tran_level
                      and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and im.item_parent  is NOT NULL
                   -- Get IL timelines from existing IZ timelines
                   union all
                   select distinct
                          2 fr_rank,
                          mileg.event_id,
                          im.dept,
                          im.item from_item,
                          DECODE(im.item_parent,
                                 NULL, NULL,
                                 im.diff_1) from_diff_id,
                          rzl.zone_id from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          im.item to_item,
                          DECODE(im.item_parent,
                                 NULL, NULL,
                                 im.diff_1) to_diff_id,
                          rzl.location to_location,
                          rzl.loc_type to_zone_node_type
                     from rpm_mv_item_loc_expl_gtt mileg,
                          item_master im,
                          rpm_merch_retail_def_expl rmrde,
                          rpm_zone_location rzl,
                          rpm_zone rz
                    where im.item                  = mileg.item
                      and im.item_level            = im.tran_level
                      and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and im.item_parent           is NOT NULL
                      and rmrde.dept               = im.dept
                      and rmrde.class              = im.class
                      and rmrde.subclass           = im.subclass
                      and rmrde.regular_zone_group = rz.zone_group_id
                      and rz.zone_id               = rzl.zone_id
                      and rzl.location             = mileg.location
                      and rzl.loc_type             = DECODE(mileg.loc_type,
                                                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                   -- Get IL timelines from existing PDL timelines
                   union all
                   select distinct
                          2 fr_rank,
                          mileg.event_id,
                          im.dept,
                          im.item_parent from_item,
                          im.diff_1 from_diff_id,
                          mileg.location from_location,
                          DECODE(mileg.loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) from_zone_node_type,
                          im.item to_item,
                          im.diff_1 to_diff_id,
                          mileg.location to_location,
                          DECODE(mileg.loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) to_zone_node_type
                     from rpm_mv_item_loc_expl_gtt mileg,
                          item_master im
                    where im.item         = mileg.item
                      and im.item_level   = im.tran_level
                      and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and im.item_parent  is NOT NULL
                   -- Get IL timelines from existing IL timelines
                   union all
                   select distinct
                          3 fr_rank,
                          mileg.event_id,
                          im.dept,
                          im.item from_item,
                          DECODE(im.item_parent,
                                 NULL, NULL,
                                 im.diff_1) from_diff_id,
                          mileg.location from_location,
                          DECODE(mileg.loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) from_zone_node_type,
                          im.item to_item,
                          DECODE(im.item_parent,
                                 NULL, NULL,
                                 im.diff_1) to_diff_id,
                          mileg.location to_location,
                          DECODE(mileg.loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) to_zone_node_type
                     from rpm_mv_item_loc_expl_gtt mileg,
                          item_master im
                    where im.item         = mileg.item
                      and im.item_level   = im.tran_level
                      and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t,
                  rpm_future_retail fr,
                  rpm_item_loc ril
            where t.dept                      = fr.dept
              and t.from_item                 = fr.item
              and NVL(t.from_diff_id, '-999') = NVL(fr.diff_id, '-999')
              and fr.location                 = t.from_location
              and fr.zone_node_type           = t.from_zone_node_type
              and t.dept                      = ril.dept
              and t.to_item                   = ril.item
              and t.to_location               = ril.loc) fr_il
    where fr_rank = max_fr_rank;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END EXPL_FR_TO_IL;

-----------------------------------------------------------------------------------------------------------------------

END RPM_MARGIN_VISIBILITY_SQL;
/
