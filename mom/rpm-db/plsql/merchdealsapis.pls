CREATE OR REPLACE PACKAGE MERCH_DEALS_API_SQL IS

--------------------------------------------------------------------------------
-- This package is owned by the pricing system.  It will interface with
-- the merchandising system through PM_DEALS_API_SQL package calls.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
PROCEDURE CREATE_DEAL(O_return_code    IN OUT VARCHAR2,
                      O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_deal_ids_table    OUT DEAL_IDS_TBL,
                      I_deal_head_rec  IN     DEAL_HEAD_REC);

--------------------------------------------------------------------------------
PROCEDURE NEW_DEAL_COMP(O_return_code       IN OUT VARCHAR2,
                        O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_deal_ids_table       OUT DEAL_IDS_TBL,
                        I_deal_detail_table IN     DEAL_DETAIL_TBL);

--------------------------------------------------------------------------------
END MERCH_DEALS_API_SQL;
/

