CREATE OR REPLACE PACKAGE RPM_FINANCE_PROMO_SQL AS

FUNCTION PROCESS_PRICE_EVENTS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION PROCESS_UPD_FIN_PROMO_EVENTS(O_cc_error_tbl           OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids     IN     OBJ_NUMERIC_ID_TABLE,
                                      I_new_promo_end_date  IN    DATE)
RETURN NUMBER;

FUNCTION UPDATE_FINANCE_PROMOTION(O_cc_error_tbl           OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids     IN     OBJ_NUMERIC_ID_TABLE,
                                  I_rib_trans_id        IN     NUMBER,
                                  I_persist_ind         IN     VARCHAR2)
RETURN NUMBER;

END RPM_FINANCE_PROMO_SQL;
/