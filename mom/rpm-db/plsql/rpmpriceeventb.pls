CREATE OR REPLACE PACKAGE BODY RPM_PRICE_EVENT_SQL AS

--------------------------------------------------------------------------------
--                            PRIVATE GLOBALS                                 --
--------------------------------------------------------------------------------

   LP_vdate           DATE       := GET_VDATE;
   LP_emergency_ind   NUMBER     := 0;
   LP_ot_pe_thread_id NUMBER(15) := -9999999999;
   LP_pc_pe_thread_id NUMBER(15) := -9898989898;
   LP_cl_pe_thread_id NUMBER(15) := -9797979797;
   LP_sp_pe_thread_id NUMBER(15) := -9696969696;
   
   --Instrumentation
   LP_return_code NUMBER         := NULL;
   LP_error_msg   VARCHAR2(2000) := NULL;

--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------
FUNCTION EXECUTE_PRICE_CHANGES(O_error_message          OUT VARCHAR2,
                               I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                               I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION EXECUTE_PRICE_CHANGES(O_error_message          OUT VARCHAR2,
                               I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                               I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION EXECUTE_AFFECTED_PROMOS(O_error_message          OUT VARCHAR2,
                                 I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                                 I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_AFFECTED_PROMOS(O_error_message          OUT VARCHAR2,
                                 I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                                 I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_CLEARANCES(O_error_message          OUT VARCHAR2,
                            I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                            I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_CLEARANCES(O_error_message          OUT VARCHAR2,
                            I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                            I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_PROMOS(O_error_message          OUT VARCHAR2,
                        I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                        I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_PROMOS(O_error_message          OUT VARCHAR2,
                        I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                        I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_EVENT_FULL_PROCESS(O_error_message         OUT VARCHAR2,
                                    I_pc_tbl             IN     OBJ_PRICE_CHANGE_TBL,
                                    I_pc_il_tbl          IN     OBJ_PRICE_CHANGE_IL_TBL,
                                    I_change_event_state IN     BOOLEAN)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_PRICE_CHANGES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_CLEARANCES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_SIMPLE_PROMO(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg    OUT VARCHAR2,
                      I_pc_il_tbl IN     OBJ_PRICE_CHANGE_IL_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_PE_THREAD_EXEC(O_error_msg         OUT VARCHAR2,
                                 I_pc_tbl         IN     OBJ_PRICE_CHANGE_TBL,
                                 I_effective_date IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION PROCESS_ITEM_LOC_LUWS_IN_RMS(O_error_message       OUT VARCHAR2,
                                      I_thd_luw_tbl      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program       VARCHAR2(50)          := 'RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_LUWS_IN_RMS';
   L_status_code   VARCHAR2(1)           := NULL;
   L_pc_tbl        OBJ_PRICEEVENT_IL_TBL := NULL;

   cursor C_LUWS is
      select OBJ_PRICEEVENT_IL_REC(item,
                                   loc,
                                   loc_type,
                                   selling_unit_retail,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_selling_uom,
                                   prom_event,
                                   price_chg_type,
                                   currency_code,
                                   old_selling_unit_retail,
                                   old_selling_uom,
                                   vendor_funded_ind,
                                   funding_type,
                                   funding_amount,
                                   funding_amount_currency,
                                   funding_percent,
                                   deal_id,
                                   deal_detail_id,
                                   active_date,
                                   is_on_clearance,
                                   pc_reason_code,
                                   change_type,
                                   change_amount,
                                   clear_ind,
                                   primary_cntry,
                                   primary_supp,
                                   il_rowid,
                                   current_unit_retail,
                                   price_event_id,
                                   dept,
                                   class,
                                   subclass,
                                   standard_uom,
                                   pack_ind,
                                   sellable_ind,
                                   orderable_ind,
                                   pack_type,
                                   orig_event_type,
                                   current_selling_retail,
                                   current_selling_uom,
                                   current_clear_retail,
                                   current_clear_uom,
                                   current_simple_promo_retail,
                                   current_simple_promo_uom,
                                   is_on_simple_promo)
        from (select /*+ CARDINALITY(IDS, 10) */
                     ev.item,
                     ev.loc,
                     ev.loc_type,
                     ev.selling_unit_retail,
                     ev.selling_uom,
                     ev.multi_units,
                     ev.multi_unit_retail,
                     ev.multi_selling_uom,
                     ev.prom_event,
                     ev.price_chg_type,
                     ev.currency_code,
                     ev.old_selling_unit_retail,
                     ev.old_selling_uom,
                     ev.vendor_funded_ind,
                     ev.funding_type,
                     ev.funding_amount,
                     ev.funding_amount_currency,
                     ev.funding_percent,
                     ev.deal_id,
                     ev.deal_detail_id,
                     ev.active_date,
                     ev.is_on_clearance,
                     ev.pc_reason_code,
                     ev.change_type,
                     ev.change_amount,
                     ev.clear_ind,
                     ev.primary_cntry,
                     ev.primary_supp,
                     ev.il_rowid,
                     ev.current_unit_retail,
                     ev.price_event_id,
                     ev.dept,
                     ev.class,
                     ev.subclass,
                     ev.standard_uom,
                     ev.pack_ind,
                     ev.sellable_ind,
                     ev.orderable_ind,
                     ev.pack_type,
                     ev.orig_event_type,
                     ev.current_selling_retail,
                     ev.current_selling_uom,
                     ev.current_clear_retail,
                     ev.current_clear_uom,
                     ev.current_simple_promo_retail,
                     ev.current_simple_promo_uom,
                     ev.is_on_simple_promo
                from TABLE(CAST(I_THD_LUW_TBL AS OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_itemloc_thread th,
                     rpm_event_itemloc ev
               where th.thread_num      = value(ids)
                 and ev.item            = th.item
                 and ev.loc             = th.loc
                 and ev.orig_event_type = th.price_event_type
                 and ev.price_event_id  = th.price_event_id
               order by ev.item,
                        ev.loc,
                        ev.active_date);   -- Make sure item locs are processed from earliest to latest...

BEGIN

   -- Retrieve all staged executed item/locs to be consumed by RMS...
   open C_LUWS;

   -- Feed them into RMS!
   loop
      fetch C_LUWS BULK COLLECT INTO L_pc_tbl LIMIT 10000;

      if L_pc_tbl.COUNT != 0 then
         RMSSUB_PRICECHANGE.CONSUME(L_status_code,
                                    O_error_message,
                                    L_pc_tbl);

         if L_status_code != API_CODES.SUCCESS then
            close C_LUWS;
            return 0;
         end if;
      end if;

      EXIT WHEN C_LUWS%NOTFOUND;
   end loop;

   close C_LUWS;

   -- Remove staged item/locs consumed... (Note:
   -- This block is the result of SQL tuning.   Iteration
   -- was shown as faster that a join from the thread object.)

   for i IN I_thd_luw_tbl.FIRST..I_thd_luw_tbl.LAST loop

      -- Clear staged item loc table using thread number
      delete
        from rpm_event_itemloc t1
       where EXISTS (select 'x'
                       from rpm_itemloc_thread t2
                      where t2.thread_num       = I_thd_luw_tbl(i)
                        and t2.item             = t1.item
                        and t2.loc              = t1.loc
                        and t2.price_event_id   = t1.price_event_id
                        and t2.price_event_type = t1.orig_event_type);

      -- Finally clear staged item-loc THREAD assignment table using thread number
      delete
        from rpm_itemloc_thread
       where thread_num = I_thd_luw_tbl(i);

   end loop;

   return 1;

EXCEPTION
        WHEN OTHERS THEN
                O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
        return 0;

END PROCESS_ITEM_LOC_LUWS_IN_RMS;

----------------------------------------

--  Processes ALL staged item-location deals in RPM_EVENT_ITEMLOC_DEALS.
--  Use only for price event execution in BATCH mode!

FUNCTION PROCESS_ITEM_LOC_DEALS(O_error_message    OUT VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_DEALS';

BEGIN

   return PROCESS_ITEM_LOC_DEALS (O_error_message,
                                  NEW OBJ_PRICE_CHANGE_TBL());

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;

END PROCESS_ITEM_LOC_DEALS;
----------------------------------------
-- Processes staged item-location deals in RPM_EVENT_ITEMLOC_DEALS
-- for specific price events.
-- I_EVENTS_TBL can be null and will force the function
-- to process ALL staged information in RPM_EVENT_ITEMLOC_DEALS

FUNCTION PROCESS_ITEM_LOC_DEALS(O_error_message    OUT VARCHAR2,
                                I_events_tbl    IN     OBJ_PRICE_CHANGE_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_DEALS';
   L_status_code   VARCHAR2(1);

BEGIN

   RMSSUB_PRICECHANGE.PROCESS_STAGED_DEALS(L_status_code,
                                           O_error_message,
                                           I_events_tbl);

   if L_status_code != API_CODES.SUCCESS then
      return 0;
   end if;

   if I_events_tbl is NOT NULL and
      I_events_tbl.COUNT > 0 then

      delete from rpm_event_itemloc_deals target
       where EXISTS (select 'x'
                       from table(cast(I_events_tbl AS OBJ_PRICE_CHANGE_TBL)) source
                      where target.orig_event_id   = source.price_change_id
                        and target.orig_event_type = source.price_change_type);
   else
      delete from rpm_event_itemloc_deals;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;

END PROCESS_ITEM_LOC_DEALS;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--                         PRIVATE PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION EXECUTE_PRICE_CHANGES(O_error_message          OUT VARCHAR2,
                               I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                               I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program  VARCHAR2(100) := 'RPM_PRICE_EVENT_SQL.EXECUTE_PRICE_CHANGES';
   L_vdate    DATE          := GET_VDATE();

   L_pc_il_tbl         OBJ_PRICE_CHANGE_IL_TBL;

   cursor C_GET_PC_IL is
   select OBJ_PRICE_CHANGE_IL_REC(pc.PRICE_CHANGE_ID,
                                  pc.PRICE_CHANGE_TYPE,
                                  item.item,
                                  loc.location)
     from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
          rpm_price_event_thread_item item,
          rpm_price_event_thread_loc loc
    where pc.price_change_type  = 'PC'
     and item.thread_number    = -999
     and item.pe_thread_id     = LP_pc_pe_thread_id
     and loc.pe_thread_id      = item.pe_thread_id
      and item.price_event_id   = pc.price_change_id
      and item.price_event_type = pc.price_change_type
      and loc.price_event_id    = item.price_event_id
      and loc.price_event_type  = item.price_event_type;

BEGIN

   open C_GET_PC_IL;
   fetch C_GET_PC_IL bulk collect into L_pc_il_tbl;
   close C_GET_PC_IL;

   if L_pc_il_tbl is NOT NULL and
      L_pc_il_tbl.count > 0 then

      if EXECUTE_PRICE_CHANGES(O_error_message,
                               L_pc_il_tbl,
                               I_change_event_status) = FALSE Then
         return false;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END EXECUTE_PRICE_CHANGES;
--------------------------------------------------------------------------------

FUNCTION EXECUTE_PRICE_CHANGES(O_error_message          OUT VARCHAR2,
                               I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                               I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program VARCHAR2(45)         := 'RPM_PRICE_EVENT_SQL.EXECUTE_PRICE_CHANGES';
   L_pc_rec  OBJ_PRICE_CHANGE_REC := NULL;
   L_pc_tbl  OBJ_PRICE_CHANGE_TBL := OBJ_PRICE_CHANGE_TBL();

   cursor C_PC_TBL is
      select /*+ CARDINALITY (pc, 1000) */
             distinct pc.price_change_id,
             pc.price_change_type
        from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc
       where pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE;

BEGIN

   if POPULATE_GTT(O_error_message,
                   I_pc_il_tbl) = 0 then
      return FALSE;
   end if;

   merge into rpm_event_itemloc target
   using (select distinct
                 t.item item,
                 t.loc,
                 t.loc_type,
                 t.selling_unit_retail,
                 t.selling_uom,
                 t.multi_units,
                 t.multi_unit_retail,
                 t.multi_selling_uom,
                 t.current_selling_retail,
                 t.current_selling_uom,
                 t.current_clear_retail,
                 t.current_clear_uom,
                 t.current_simple_promo_retail,
                 t.current_simple_promo_uom,
                 t.prom_event,
                 t.price_chg_type,
                 t.currency_code,
                 t.old_selling_unit_retail,
                 t.old_selling_uom,
                 t.vendor_funded_ind,
                 t.funding_type,
                 t.funding_amount,
                 t.funding_amount_currency,
                 t.funding_percent,
                 t.deal_id,
                 t.deal_detail_id,
                 t.active_date,
                 t.is_on_clearance,
                 t.pc_reason_code,
                 t.change_type,
                 t.change_amount,
                 t.clear_ind,
                 t.primary_cntry,
                 t.primary_supp,
                 t.il_rowid,
                 t.current_unit_retail,
                 case
                    when t.promo_id is NOT NULL and
                       rpile2.promo_id is NOT NULL and
                       NOT (rpile2.promo_id             = t.promo_id and
                            rpile2.promo_dtl_id        != t.promo_dtl_id and
                            rpile2.exception_parent_id is NULL) and
                       NOT (rpile2.promo_id             = t.promo_id and
                            rpile2.promo_comp_id        = t.promo_comp_id and
                            rpile2.promo_dtl_id        != t.promo_dtl_id and
                            rpile2.exception_parent_id is NOT NULL) then
                       t.future_retail_id
                    else
                       NULL
                 end pc_affected_promo_fr_id,
                 t.price_event_id,
                 t.last_execution_date,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.standard_uom,
                 t.pack_ind,
                 t.sellable_ind,
                 t.orderable_ind,
                 t.pack_type,
                 t.orig_event_type,
                 t.is_on_simple_promo
            from (select /*+ CARDINALITY (pc, 1000)*/
                         rfr.item item,
                         rfr.location loc,
                         case rfr.zone_node_type
                            when RPM_CONSTANTS.ZONE_NODE_TYPE_STORE then
                               RPM_CONSTANTS.LOCATION_TYPE_STORE
                            else
                               RPM_CONSTANTS.LOCATION_TYPE_WH
                         end loc_type,
                         rfr.selling_retail selling_unit_retail,
                         rfr.selling_uom,
                         rfr.multi_units,
                         rfr.multi_unit_retail,
                         rfr.multi_selling_uom,
                         rfr.selling_retail current_selling_retail,
                         rfr.selling_uom current_selling_uom,
                         rfr.clear_retail current_clear_retail,
                         rfr.clear_uom current_clear_uom,
                         rfr.simple_promo_retail current_simple_promo_retail,
                         rfr.simple_promo_uom current_simple_promo_uom,
                         NULL prom_event,
                         RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_chg_type,
                         NVL(rfr.selling_retail_currency, rfr.multi_unit_retail_currency) currency_code,
                         il.selling_unit_retail old_selling_unit_retail,
                         il.selling_uom old_selling_uom,
                         rpc.vendor_funded_ind,
                         rpc.funding_type,
                         rpc.funding_amount,
                         rpc.funding_amount_currency,
                         rpc.funding_percent,
                         rpc.deal_id,
                         rpc.deal_detail_id,
                         rfr.action_date active_date,
                         DECODE(rfr.clearance_id,
                                NULL, 0,
                                DECODE(rfr.clear_start_ind,
                                       RPM_CONSTANTS.END_IND, 0,
                                       1)) is_on_clearance,
                         DECODE(rfr.clearance_id,
                                NULL, rpc.reason_code,
                                DECODE(rfr.clear_start_ind,
                                       RPM_CONSTANTS.END_IND, rpc.reason_code,
                                       (select reason_code
                                          from rpm_clearance
                                         where clearance_id = rfr.clearance_id))) pc_reason_code,
                         NULL as change_type,
                         NULL as change_amount,
                         il.clear_ind,
                         il.primary_cntry,
                         il.primary_supp,
                         il.rowid il_rowid,
                         il.unit_retail current_unit_retail,
                         pc.price_change_id price_event_id,
                         LP_vdate last_execution_date,
                         im.dept,
                         im.class,
                         im.subclass,
                         im.standard_uom,
                         im.pack_ind,
                         im.sellable_ind,
                         im.orderable_ind,
                         im.pack_type,
                         RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE orig_event_type,
                         rfr.on_simple_promo_ind is_on_simple_promo,
                         rfr.future_retail_id,
                         rpile.promo_id,
                         rpile.promo_comp_id,
                         rpile.promo_dtl_id
                    from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                         rpm_price_change rpc,
                         item_master im,
                         item_loc il,
                         rpm_future_retail_gtt rfr,
                         rpm_promo_item_loc_expl_gtt rpile
                   where pc.price_change_type          = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                     and rpc.price_change_id           = pc.price_change_id
                     and rpc.change_type              != RPM_CONSTANTS.RESET_POS
                     and im.item                       = pc.item
                     and il.item                       = pc.item
                     and il.loc                        = pc.location
                     and rfr.dept                      = im.dept
                     and rfr.item                      = pc.item
                     and rfr.location                  = pc.location
                     and rfr.price_change_id           = pc.price_change_id
                     and rpile.price_event_id (+)      = rfr.price_event_id
                     and rpile.item (+)                = rfr.item
                     and rpile.location (+)            = rfr.location
                     and rfr.action_date              >= rpile.detail_start_date (+)
                     and rfr.action_date              <= NVL(rpile.detail_end_date (+), rfr.action_date)
                     and rpile.detail_change_type (+) != RPM_CONSTANTS.RETAIL_EXCLUDE
                     and rpile.type (+)                = RPM_CONSTANTS.SIMPLE_CODE
                     and rpile.customer_type (+)       is NULL) t,
                 rpm_promo_item_loc_expl_gtt rpile2
           where rpile2.item (+)               = t.item
             and rpile2.location (+)           = t.loc
             and rpile2.detail_change_type (+) = RPM_CONSTANTS.RETAIL_EXCLUDE
          union all
          select /*+ CARDINALITY(pc, 1000)*/
                  pc.item,
                  il.loc,
                  il.loc_type,
                  rpc.change_amount                  selling_unit_retail,
                  rpc.change_selling_uom             selling_uom,
                  rpc.multi_units,
                  rpc.multi_unit_retail,
                  rpc.multi_selling_uom,
                  rpc.change_amount                  current_selling_retail,
                  rpc.change_selling_uom             current_selling_uom,
                  rpc.change_amount                  current_clear_retail,
                  NULL                               current_clear_uom,
                  rpc.change_amount                  current_simple_promo_retail,
                  NULL                               current_simple_promo_uom,
                  NULL                               prom_event,
                  RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_chg_type,
                  NVL(rizp.selling_retail_currency, rizp.multi_unit_retail_currency) currency_code,
                  il.selling_unit_retail             old_selling_unit_retail,
                  il.selling_uom                     old_selling_uom,
                  rpc.vendor_funded_ind,
                  rpc.funding_type,
                  rpc.funding_amount,
                  rpc.funding_amount_currency,
                  rpc.funding_percent,
                  rpc.deal_id,
                  rpc.deal_detail_id,
                  rpc.effective_date                 active_date,
                  0                                  is_on_clearance,
                  rpc.reason_code                    pc_reason_code,
                  NULL                               change_type,
                  NULL                               change_amount,
                  il.clear_ind,
                  il.primary_cntry,
                  il.primary_supp,
                  il.rowid                           il_rowid,
                  il.unit_retail                     current_unit_retail,
                  NULL                               pc_affected_promo_fr_id,
                  pc.price_change_id                 price_event_id,
                  LP_vdate                           last_execution_date,
                  im.dept,
                  im.class,
                  im.subclass,
                  im.standard_uom,
                  im.pack_ind,
                  im.sellable_ind,
                  im.orderable_ind,
                  im.pack_type,
                  RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE orig_event_type,
                  0                                  is_on_simple_promo
             from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                  rpm_price_change rpc,
                  item_master im,
                  item_loc il,
                  rpm_item_zone_price rizp
            where pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
              and rpc.price_change_id  = pc.price_change_id
              and rpc.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
              and rpc.change_type      = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
              and rpc.diff_id          is NULL
              and im.item              = pc.item
              and im.item_level        = im.tran_level - 1
              and pc.item              = il.item
              and pc.location          = il.loc
              and rizp.item            = pc.item
              and rizp.zone_id         = rpc.zone_id) source
   on (    target.price_chg_type  = source.price_chg_type
       and target.orig_event_type = source.orig_event_type
       and target.price_event_id  = source.price_event_id
       and target.item            = source.item
       and target.loc             = source.loc
       and target.loc_type        = source.loc_type
       and target.active_date     = source.active_date)
   when MATCHED then
      update
         set selling_unit_retail         = source.selling_unit_retail,
             selling_uom                 = source.selling_uom,
             multi_units                 = source.multi_units,
             multi_unit_retail           = source.multi_unit_retail,
             multi_selling_uom           = source.multi_selling_uom,
             prom_event                  = source.prom_event,
             currency_code               = source.currency_code,
             old_selling_unit_retail     = source.old_selling_unit_retail,
             old_selling_uom             = source.old_selling_uom,
             vendor_funded_ind           = source.vendor_funded_ind,
             funding_type                = source.funding_type,
             funding_amount              = source.funding_amount,
             funding_amount_currency     = source.funding_amount_currency,
             funding_percent             = source.funding_percent,
             deal_id                     = source.deal_id,
             deal_detail_id              = source.deal_detail_id,
             is_on_clearance             = source.is_on_clearance,
             pc_reason_code              = source.pc_reason_code,
             change_type                 = source.change_type,
             change_amount               = source.change_amount,
             clear_ind                   = source.clear_ind,
             primary_cntry               = source.primary_cntry,
             primary_supp                = source.primary_supp,
             il_rowid                    = source.il_rowid,
             current_unit_retail         = source.current_unit_retail,
             pc_affected_promo_fr_id     = source.pc_affected_promo_fr_id,
             last_execution_date         = source.last_execution_date,
             dept                        = source.dept,
             class                       = source.class,
             subclass                    = source.subclass,
             standard_uom                = source.standard_uom,
             pack_ind                    = source.pack_ind,
             sellable_ind                = source.sellable_ind,
             orderable_ind               = source.orderable_ind,
             pack_type                   = source.pack_type,
             current_selling_retail      = source.current_selling_retail,
             current_selling_uom         = source.current_selling_uom,
             current_clear_retail        = source.current_clear_retail,
             current_clear_uom           = source.current_clear_uom,
             current_simple_promo_retail = source.current_simple_promo_retail,
             current_simple_promo_uom    = source.current_simple_promo_uom,
             is_on_simple_promo          = source.is_on_simple_promo
   when NOT MATCHED then
      insert(item,
             loc,
             loc_type,
             selling_unit_retail,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             prom_event,
             price_chg_type,
             currency_code,
             old_selling_unit_retail,
             old_selling_uom,
             vendor_funded_ind,
             funding_type,
             funding_amount,
             funding_amount_currency,
             funding_percent,
             deal_id,
             deal_detail_id,
             active_date,
             is_on_clearance,
             pc_reason_code,
             change_type,
             change_amount,
             clear_ind,
             primary_cntry,
             primary_supp,
             il_rowid,
             current_unit_retail,
             pc_affected_promo_fr_id,
             price_event_id,
             last_execution_date,
             dept,
             class,
             subclass,
             standard_uom,
             pack_ind,
             sellable_ind,
             orderable_ind,
             pack_type,
             orig_event_type,
             current_selling_retail,
             current_selling_uom,
             current_clear_retail,
             current_clear_uom,
             current_simple_promo_retail,
             current_simple_promo_uom,
             is_on_simple_promo)
      values(source.item,
             source.loc,
             source.loc_type,
             source.selling_unit_retail,
             source.selling_uom,
             source.multi_units,
             source.multi_unit_retail,
             source.multi_selling_uom,
             source.prom_event,
             source.price_chg_type,
             source.currency_code,
             source.old_selling_unit_retail,
             source.old_selling_uom,
             source.vendor_funded_ind,
             source.funding_type,
             source.funding_amount,
             source.funding_amount_currency,
             source.funding_percent,
             source.deal_id,
             source.deal_detail_id,
             source.active_date,
             source.is_on_clearance,
             source.pc_reason_code,
             source.change_type,
             source.change_amount,
             source.clear_ind,
             source.primary_cntry,
             source.primary_supp,
             source.il_rowid,
             source.current_unit_retail,
             source.pc_affected_promo_fr_id,
             source.price_event_id,
             source.last_execution_date,
             source.dept,
             source.class,
             source.subclass,
             source.standard_uom,
             source.pack_ind,
             source.sellable_ind,
             source.orderable_ind,
             source.pack_type,
             source.orig_event_type,
             source.current_selling_retail,
             source.current_selling_uom,
             source.current_clear_retail,
             source.current_clear_uom,
             source.current_simple_promo_retail,
             source.current_simple_promo_uom,
             source.is_on_simple_promo);

   if I_change_event_status = TRUE then

      for rec IN C_PC_TBL loop
         L_pc_rec := OBJ_PRICE_CHANGE_REC(rec.price_change_id,
                                          rec.price_change_type);
         L_pc_tbl.EXTEND();
         L_pc_tbl(L_pc_tbl.COUNT) := L_pc_rec;
      end loop;

      if L_pc_tbl is NOT NULL and
         L_pc_tbl.COUNT > 0 then

         -- Updating RPM_ITEM_ZONE_PRICE, for zone level price changes
         -- when there is new zone has been added to the primary zone group.

         if RPM_PRICE_EVENT_SQL.UPDATE_ITEM_ZONE_PRICE(O_error_message,
                                                       L_pc_tbl) = FALSE then
            return FALSE;
         end if;

         merge into rpm_price_change target
         using (select /*+ CARDINALITY(pc, 1000)*/
                       pc.price_change_id
                  from table(cast(L_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                       rpm_price_change rpc
                 where pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                   and rpc.price_change_id  = pc.price_change_id
                   and rpc.state           != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE) source
         on (target.price_change_id = source.price_change_id)
         when MATCHED then
            update
               set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

      end if;

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_PRICE_CHANGES;
--------------------------------------------------------------------------------

FUNCTION EXECUTE_CLEARANCES(O_error_message          OUT VARCHAR2,
                            I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                            I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'RPM_PRICE_EVENT_SQL.EXECUTE_CLEARANCES';

   L_cl_il_tbl         OBJ_PRICE_CHANGE_IL_TBL;

   cursor C_GET_CL_IL is
   select OBJ_PRICE_CHANGE_IL_REC(pc.PRICE_CHANGE_ID,
                                  pc.PRICE_CHANGE_TYPE,
                                  item.item,
                                  loc.location)
     from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
          rpm_price_event_thread_item item,
          rpm_price_event_thread_loc loc
    where pc.price_change_type  IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET)
      and item.thread_number    = -999
      and item.pe_thread_id     = LP_cl_pe_thread_id
      and loc.pe_thread_id      = item.pe_thread_id
      and item.price_event_id   = pc.price_change_id
      and item.price_event_type = pc.price_change_type
      and loc.price_event_id    = item.price_event_id
      and loc.price_event_type  = item.price_event_type;

BEGIN

   open C_GET_CL_IL;
   fetch C_GET_CL_IL BULK COLLECT into L_cl_il_tbl;
   close C_GET_CL_IL;

   if L_cl_il_tbl is NOT NULL and
      L_cl_il_tbl.COUNT > 0 then

      if EXECUTE_CLEARANCES(O_error_message,
                            L_cl_il_tbl,
                            FALSE) = FALSE Then
         return false;
      end if;

   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_CLEARANCES;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_CLEARANCES(O_error_message          OUT VARCHAR2,
                            I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                            I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PRICE_EVENT_SQL.EXECUTE_CLEARANCES';

   L_pc_rec OBJ_PRICE_CHANGE_REC := NULL;
   L_pc_tbl OBJ_PRICE_CHANGE_TBL := OBJ_PRICE_CHANGE_TBL();

   cursor C_PC_TBL is
      select /*+ CARDINALITY (pc, 1000)*/
             distinct pc.price_change_id,
             pc.price_change_type
        from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc
       where pc.price_change_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                      RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET);

BEGIN

   if POPULATE_GTT(O_error_message,
                   I_pc_il_tbl) = 0 then
      return FALSE;
   end if;

   merge /*+ INDEX(target, rpm_event_itemloc_i1)*/ into rpm_event_itemloc target
   using (select distinct
                 t.item,
                 t.loc,
                 t.loc_type,
                 t.selling_unit_retail,
                 t.selling_uom,
                 t.multi_units,
                 t.multi_unit_retail,
                 t.multi_selling_uom,
                 t.current_selling_retail,
                 t.current_selling_uom,
                 t.current_clear_retail,
                 t.current_clear_uom,
                 t.current_simple_promo_retail,
                 t.current_simple_promo_uom,
                 NULL prom_event,
                 t.price_chg_type,
                 t.currency_code,
                 t.old_selling_unit_retail,
                 t.old_selling_uom,
                 t.vendor_funded_ind,
                 t.funding_type,
                 t.funding_amount,
                 t.funding_amount_currency,
                 t.funding_percent,
                 t.deal_id,
                 t.deal_detail_id,
                 t.active_date,
                 t.is_on_clearance,
                 t.pc_reason_code,
                 NULL change_type,
                 NULL change_amount,
                 t.clear_ind,
                 t.primary_cntry,
                 t.primary_supp,
                 t.il_rowid,
                 t.current_unit_retail,
                 case
                    when t.promo_id is NOT NULL and
                         rpile2.promo_id is NOT NULL and
                         NOT (rpile2.promo_id             = t.promo_id and
                              rpile2.promo_dtl_id        != t.promo_dtl_id and
                              rpile2.exception_parent_id is NULL) and
                         NOT (rpile2.promo_id             = t.promo_id and
                              rpile2.promo_comp_id        = t.promo_comp_id and
                              rpile2.promo_dtl_id        != t.promo_dtl_id and
                              rpile2.exception_parent_id is NOT NULL) then
                       ---
                       t.future_retail_id
                    else
                       NULL
                 end pc_affected_promo_fr_id,
                 t.price_event_id,
                 t.last_execution_date,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.standard_uom,
                 t.pack_ind,
                 t.sellable_ind,
                 t.orderable_ind,
                 t.pack_type,
                 t.orig_event_type,
                 t.is_on_simple_promo
            from (select /*+ CARDINALITY (pc, 1000)*/
                         rfr.item,
                         rfr.location loc,
                         case rfr.zone_node_type
                            when RPM_CONSTANTS.ZONE_NODE_TYPE_STORE then
                               RPM_CONSTANTS.LOCATION_TYPE_STORE
                            else
                               RPM_CONSTANTS.LOCATION_TYPE_WH
                         end loc_type,
                         rfr.clear_retail selling_unit_retail,
                         rfr.clear_uom selling_uom,
                         rfr.multi_units,
                         rfr.multi_unit_retail,
                         rfr.multi_selling_uom,
                         rfr.selling_retail current_selling_retail,
                         rfr.selling_uom current_selling_uom,
                         rfr.clear_retail current_clear_retail,
                         rfr.clear_uom current_clear_uom,
                         rfr.simple_promo_retail current_simple_promo_retail,
                         rfr.simple_promo_uom current_simple_promo_uom,
                         RPM_CONSTANTS.PE_TYPE_CLEARANCE price_chg_type,
                         NVL(rfr.clear_retail_currency, rfr.selling_retail_currency) currency_code,
                         il.selling_unit_retail old_selling_unit_retail,
                         il.selling_uom old_selling_uom,
                         rc.vendor_funded_ind,
                         rc.funding_type,
                         rc.funding_amount,
                         rc.funding_amount_currency,
                         rc.funding_percent,
                         rc.deal_id,
                         rc.deal_detail_id,
                         rfr.action_date active_date,
                         1 is_on_clearance,
                         rc.reason_code pc_reason_code,
                         il.clear_ind,
                         il.primary_cntry,
                         il.primary_supp,
                         il.rowid il_rowid,
                         il.unit_retail current_unit_retail,
                         pc.price_change_id price_event_id,
                         LP_vdate last_execution_date,
                         im.dept,
                         im.class,
                         im.subclass,
                         im.standard_uom,
                         im.pack_ind,
                         im.sellable_ind,
                         im.orderable_ind,
                         im.pack_type,
                         RPM_CONSTANTS.PE_TYPE_CLEARANCE orig_event_type,
                         rfr.on_simple_promo_ind is_on_simple_promo,
                         rfr.future_retail_id,
                         rpile.promo_id,
                         rpile.promo_comp_id,
                         rpile.promo_dtl_id
                    from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                         rpm_clearance rc,
                         item_master im,
                         item_loc il,
                         rpm_future_retail_gtt rfr,
                         rpm_promo_item_loc_expl_gtt rpile
                   where pc.price_change_type          = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                     and rc.clearance_id               = pc.price_change_id
                     and im.item                       = pc.item
                     and il.item                       = pc.item
                     and il.loc                        = pc.location
                     and rfr.price_event_id            = pc.price_change_id
                     and rfr.dept                      = im.dept
                     and rfr.item                      = pc.item
                     and rfr.location                  = pc.location
                     and rfr.clear_start_ind           IN (RPM_CONSTANTS.START_IND,
                                                           RPM_CONSTANTS.START_END_IND)
                     and rfr.clearance_id              = pc.price_change_id
                     and rpile.price_event_id (+)      = rfr.price_event_id
                     and rpile.item (+)                = rfr.item
                     and rpile.location (+)            = rfr.location
                     and rfr.action_date              >= rpile.detail_start_date (+)
                     and rfr.action_date              <= NVL(rpile.detail_end_date (+), rfr.action_date)
                     and rpile.detail_change_type (+) != RPM_CONSTANTS.RETAIL_EXCLUDE
                     and rpile.type (+)                = RPM_CONSTANTS.SIMPLE_CODE
                     and rpile.customer_type (+)       is NULL) t,
                 rpm_promo_item_loc_expl_gtt rpile2
           where rpile2.item (+)               = t.item
             and rpile2.location (+)           = t.loc
             and rpile2.detail_change_type (+) = RPM_CONSTANTS.RETAIL_EXCLUDE
          union all
          select distinct
                 t.item,
                 t.loc,
                 t.loc_type,
                 t.selling_unit_retail,
                 t.selling_uom,
                 t.multi_units,
                 t.multi_unit_retail,
                 t.multi_selling_uom,
                 t.current_selling_retail,
                 t.current_selling_uom,
                 t.current_clear_retail,
                 t.current_clear_uom,
                 t.current_simple_promo_retail,
                 t.current_simple_promo_uom,
                 NULL prom_event,
                 t.price_chg_type,
                 t.currency_code,
                 t.old_selling_unit_retail,
                 t.old_selling_uom,
                 NULL vendor_funded_ind,
                 NULL funding_type,
                 NULL funding_amount,
                 NULL funding_amount_currency,
                 NULL funding_percent,
                 NULL deal_id,
                 NULL deal_detail_id,
                 t.active_date,
                 t.is_on_clearance,
                 t.pc_reason_code,
                 NULL change_type,
                 NULL change_amount,
                 t.clear_ind,
                 t.primary_cntry,
                 t.primary_supp,
                 t.il_rowid,
                 t.current_unit_retail,
                 case
                    when t.promo_id is NOT NULL and
                         rpile2.promo_id is NOT NULL and
                         NOT (rpile2.promo_id             = t.promo_id and
                              rpile2.promo_dtl_id        != t.promo_dtl_id and
                              rpile2.exception_parent_id is NULL) and
                         NOT (rpile2.promo_id             = t.promo_id and
                              rpile2.promo_comp_id        = t.promo_comp_id and
                              rpile2.promo_dtl_id        != t.promo_dtl_id and
                              rpile2.exception_parent_id is NOT NULL) then
                       ---
                       t.future_retail_id
                    else
                       NULL
                 end pc_affected_promo_fr_id,
                 t.price_event_id,
                 t.last_execution_date,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.standard_uom,
                 t.pack_ind,
                 t.sellable_ind,
                 t.orderable_ind,
                 t.pack_type,
                 t.orig_event_type,
                 t.is_on_simple_promo
            from (select /*+ CARDINALITY (pc, 1000)*/
                         rfr.item,
                         rfr.location loc,
                         case rfr.zone_node_type
                            when 0 then
                               RPM_CONSTANTS.LOCATION_TYPE_STORE
                            else
                               RPM_CONSTANTS.LOCATION_TYPE_WH
                         end loc_type,
                         rfr.clear_retail selling_unit_retail,
                         rfr.clear_uom selling_uom,
                         rfr.multi_units,
                         rfr.multi_unit_retail,
                         rfr.multi_selling_uom,
                         rfr.selling_retail current_selling_retail,
                         rfr.selling_uom current_selling_uom,
                         rfr.clear_retail current_clear_retail,
                         rfr.clear_uom current_clear_uom,
                         rfr.simple_promo_retail current_simple_promo_retail,
                         rfr.simple_promo_uom current_simple_promo_uom,
                         RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET price_chg_type,
                         NVL(rfr.clear_retail_currency, rfr.selling_retail_currency) currency_code,
                         il.selling_unit_retail old_selling_unit_retail,
                         il.selling_uom old_selling_uom,
                         rfr.action_date active_date,
                         0 is_on_clearance,
                         rc.reason_code pc_reason_code,
                         'N' clear_ind,
                         il.primary_cntry,
                         il.primary_supp,
                         il.rowid il_rowid,
                         il.unit_retail current_unit_retail,
                         pc.price_change_id price_event_id,
                         LP_vdate last_execution_date,
                         im.dept,
                         im.class,
                         im.subclass,
                         im.standard_uom,
                         im.pack_ind,
                         im.sellable_ind,
                         im.orderable_ind,
                         im.pack_type,
                         RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET orig_event_type,
                         rfr.on_simple_promo_ind is_on_simple_promo,
                         rfr.future_retail_id,
                         rpile.promo_id,
                         rpile.promo_comp_id,
                         rpile.promo_dtl_id
                    from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                         rpm_clearance_reset rc,
                         item_master im,
                         item_loc il,
                         rpm_future_retail_gtt rfr,
                         rpm_promo_item_loc_expl_gtt rpile
                   where pc.price_change_type          = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                     and rc.clearance_id               = pc.price_change_id
                     and im.item                       = pc.item
                     and il.item                       = pc.item
                     and il.loc                        = pc.location
                     and rfr.price_event_id            = pc.price_change_id
                     and rfr.dept                      = im.dept
                     and rfr.item                      = pc.item
                     and rfr.location                  = pc.location
                     and rfr.clear_start_ind           = RPM_CONSTANTS.END_IND
                     and rpile.price_event_id (+)      = rfr.price_event_id
                     and rpile.item (+)                = rfr.item
                     and rpile.location (+)            = rfr.location
                     and rfr.action_date              >= rpile.detail_start_date (+)
                     and rfr.action_date              <= NVL(rpile.detail_end_date (+), rfr.action_date)
                     and rpile.detail_change_type (+) != RPM_CONSTANTS.RETAIL_EXCLUDE
                     and rpile.type (+)                = RPM_CONSTANTS.SIMPLE_CODE
                     and rpile.customer_type (+)       is NULL) t,
                 rpm_promo_item_loc_expl_gtt rpile2
           where rpile2.item (+)               = t.item
             and rpile2.location (+)           = t.loc
             and rpile2.detail_change_type (+) = RPM_CONSTANTS.RETAIL_EXCLUDE) source
   on (    target.price_chg_type  = source.price_chg_type
       and target.orig_event_type = source.orig_event_type
       and target.price_event_id  = source.price_event_id
       and target.item            = source.item
       and target.loc             = source.loc
       and target.loc_type        = source.loc_type
       and target.active_date     = source.active_date)
   when MATCHED then
      update
         set selling_unit_retail         = source.selling_unit_retail,
             selling_uom                 = source.selling_uom,
             multi_units                 = source.multi_units,
             multi_unit_retail           = source.multi_unit_retail,
             multi_selling_uom           = source.multi_selling_uom,
             prom_event                  = source.prom_event,
             currency_code               = source.currency_code,
             old_selling_unit_retail     = source.old_selling_unit_retail,
             old_selling_uom             = source.old_selling_uom,
             vendor_funded_ind           = source.vendor_funded_ind,
             funding_type                = source.funding_type,
             funding_amount              = source.funding_amount,
             funding_amount_currency     = source.funding_amount_currency,
             funding_percent             = source.funding_percent,
             deal_id                     = source.deal_id,
             deal_detail_id              = source.deal_detail_id,
             is_on_clearance             = source.is_on_clearance,
             pc_reason_code              = source.pc_reason_code,
             change_type                 = source.change_type,
             change_amount               = source.change_amount,
             clear_ind                   = source.clear_ind,
             primary_cntry               = source.primary_cntry,
             primary_supp                = source.primary_supp,
             il_rowid                    = source.il_rowid,
             current_unit_retail         = source.current_unit_retail,
             pc_affected_promo_fr_id     = source.pc_affected_promo_fr_id,
             last_execution_date         = source.last_execution_date,
             dept                        = source.dept,
             class                       = source.class,
             subclass                    = source.subclass,
             standard_uom                = source.standard_uom,
             pack_ind                    = source.pack_ind,
             sellable_ind                = source.sellable_ind,
             orderable_ind               = source.orderable_ind,
             pack_type                   = source.pack_type,
             current_selling_retail      = source.current_selling_retail,
             current_selling_uom         = source.current_selling_uom,
             current_clear_retail        = source.current_clear_retail,
             current_clear_uom           = source.current_clear_uom,
             current_simple_promo_retail = source.current_simple_promo_retail,
             current_simple_promo_uom    = source.current_simple_promo_uom,
             is_on_simple_promo          = source.is_on_simple_promo
   when NOT MATCHED then
      insert (item,
              loc,
              loc_type,
              selling_unit_retail,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_selling_uom,
              prom_event,
              price_chg_type,
              currency_code,
              old_selling_unit_retail,
              old_selling_uom,
              vendor_funded_ind,
              funding_type,
              funding_amount,
              funding_amount_currency,
              funding_percent,
              deal_id,
              deal_detail_id,
              active_date,
              is_on_clearance,
              pc_reason_code,
              change_type,
              change_amount,
              clear_ind,
              primary_cntry,
              primary_supp,
              il_rowid,
              current_unit_retail,
              pc_affected_promo_fr_id,
              price_event_id,
              last_execution_date,
              dept,
              class,
              subclass,
              standard_uom,
              pack_ind,
              sellable_ind,
              orderable_ind,
              pack_type,
              orig_event_type,
              current_selling_retail,
              current_selling_uom,
              current_clear_retail,
              current_clear_uom,
              current_simple_promo_retail,
              current_simple_promo_uom,
              is_on_simple_promo)
      values (source.item,
              source.loc,
              source.loc_type,
              source.selling_unit_retail,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_selling_uom,
              source.prom_event,
              source.price_chg_type,
              source.currency_code,
              source.old_selling_unit_retail,
              source.old_selling_uom,
              source.vendor_funded_ind,
              source.funding_type,
              source.funding_amount,
              source.funding_amount_currency,
              source.funding_percent,
              source.deal_id,
              source.deal_detail_id,
              source.active_date,
              source.is_on_clearance,
              source.pc_reason_code,
              source.change_type,
              source.change_amount,
              source.clear_ind,
              source.primary_cntry,
              source.primary_supp,
              source.il_rowid,
              source.current_unit_retail,
              source.pc_affected_promo_fr_id,
              source.price_event_id,
              source.last_execution_date,
              source.dept,
              source.class,
              source.subclass,
              source.standard_uom,
              source.pack_ind,
              source.sellable_ind,
              source.orderable_ind,
              source.pack_type,
              source.orig_event_type,
              source.current_selling_retail,
              source.current_selling_uom,
              source.current_clear_retail,
              source.current_clear_uom,
              source.current_simple_promo_retail,
              source.current_simple_promo_uom,
              source.is_on_simple_promo);

   if I_change_event_status then

      for rec IN C_PC_TBL loop
         L_pc_rec := OBJ_PRICE_CHANGE_REC(rec.price_change_id,
                                          rec.price_change_type);
         L_pc_tbl.EXTEND();
         L_pc_tbl(L_pc_tbl.COUNT) := L_pc_rec;
      end loop;

      if L_pc_tbl is NOT NULL and
         L_pc_tbl.COUNT > 0 then

         merge into rpm_clearance target
         using (select /*+ CARDINALITY (pc, 1000)*/
                       pc.price_change_id
                  from table(cast(L_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                       rpm_clearance rc
                 where pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                   and pc.price_change_id   = rc.clearance_id
                   and rc.state            != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE) source
         on (target.clearance_id = source.price_change_id)
         when MATCHED then
            update
               set target.state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

         merge into rpm_clearance_reset target
         using (select /*+ CARDINALITY (pc, 1000)*/
                       pc.price_change_id
                  from table(cast(L_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                       rpm_clearance_reset rcr
                 where pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                   and pc.price_change_id   = rcr.clearance_id
                   and rcr.state           != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE) source
         on (target.clearance_id = source.price_change_id)
         when MATCHED then
            update
               set target.state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_CLEARANCES;

--------------------------------------------------------------------------------
FUNCTION EXECUTE_PROMOS(O_error_message          OUT VARCHAR2,
                        I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                        I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.EXECUTE_PROMOS';

   L_sp_il_tbl     OBJ_PRICE_CHANGE_IL_TBL;

   cursor C_GET_SP_IL is
      select OBJ_PRICE_CHANGE_IL_REC(pc.price_change_id,
                                     pc.price_change_type,
                                     item.item,
                                     loc.location)
        from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
             rpm_price_event_thread_item item,
             rpm_price_event_thread_loc loc
       where pc.price_change_type  IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                       RPM_CONSTANTS.PE_TYPE_PROMO_END)
         and item.thread_number    = -999
         and item.pe_thread_id     = LP_sp_pe_thread_id
         and loc.pe_thread_id      = item.pe_thread_id
         and item.price_event_id   = pc.price_change_id
         and item.price_event_type = pc.price_change_type
         and loc.price_event_id    = item.price_event_id
         and loc.price_event_type  = item.price_event_type;

BEGIN

   open C_GET_SP_IL;
   fetch C_GET_SP_IL BULK COLLECT into L_sp_il_tbl;
   close C_GET_SP_IL;

   if L_sp_il_tbl is NOT NULL and
      L_sp_il_tbl.COUNT > 0 then

     if EXECUTE_PROMOS(O_error_message,
                       L_sp_il_tbl,
                       false) = FALSE Then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_PROMOS;

--------------------------------------------------------------------------------

FUNCTION EXECUTE_PROMOS(O_error_message          OUT VARCHAR2,
                        I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                        I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PRICE_EVENT_SQL.EXECUTE_PROMOS';

   L_pc_il_tbl OBJ_PRICE_CHANGE_IL_TBL := NULL;
   L_pc_tbl    OBJ_PRICE_CHANGE_TBL    := NULL;

   cursor C_PC_TBL is
      select OBJ_PRICE_CHANGE_REC(price_change_id,
                                  price_change_type)
        from (select /*+ CARDINALITY (pc, 1000)*/
                     distinct pc.price_change_id,
                     pc.price_change_type
                from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc
               where pc.price_change_type IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                              RPM_CONSTANTS.PE_TYPE_PROMO_END));

   -- Filter that input collection, retain promo dtl records only if they that are:
   --  1) 'PS' promos.
   --  1) 'PE' promos that are not in "pending cancel" state
   --  2) 'PE' promos that are pending cancel but don't have:
   --     a) siblins that are pending cancel/cancel, and the sibling has a lower id than the promo
   --     b) siblings that are non pending cancel/cancel
   cursor C_FILTERED_PE is
      select /*+ CARDINALITY (pcil, 1000)*/
             OBJ_PRICE_CHANGE_IL_REC(pcil.price_change_id,
                                     pcil.price_change_type,
                                     pcil.item,
                                     pcil.location)
        from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pcil,
             rpm_promo_dtl rpd
       where rpd.promo_dtl_id       = pcil.price_change_id
         and pcil.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_START
      union all
      select /*+ CARDINALITY (pcil, 1000)*/
             OBJ_PRICE_CHANGE_IL_REC(pcil.price_change_id,
                                     pcil.price_change_type,
                                     pcil.item,
                                     pcil.location)
        from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pcil,
             rpm_promo_dtl rpd
       where rpd.promo_dtl_id       = pcil.price_change_id
         and rpd.state             != RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
         and pcil.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END
      union all
      (select OBJ_PRICE_CHANGE_IL_REC(pcil2.price_change_id,
                                      pcil2.price_change_type,
                                      pcil2.item,
                                      pcil2.location)
         from (select /*+ CARDINALITY (pcil, 1000)*/
                      pcil.price_change_id,
                      pcil.price_change_type,
                      pcil.item,
                      pcil.location
                 from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pcil,
                      rpm_promo_dtl rpd
                where rpd.promo_dtl_id       = pcil.price_change_id
                  and pcil.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END
                  and rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
               minus
              (select temp1.orig_promo_dtl_id,
                      RPM_CONSTANTS.PE_TYPE_PROMO_END,
                      temp1.item,
                      temp1.loc
                 from rpm_sibling_promo_il_gtt temp1,
                      rpm_promo_dtl rpd,
                      (select temp2.orig_promo_dtl_id,
                              MIN(temp2.sibling_promo_dtl_id) min_promo_dtl_id,
                              temp2.item,
                              temp2.loc
                         from rpm_sibling_promo_il_gtt temp2
                        where temp2.sibling_state IN (RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                      RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                          and rownum              > 0
                     group by temp2.orig_promo_dtl_id,
                              temp2.item,
                              temp2.loc) prom_sib
                where temp1.orig_promo_dtl_id  = prom_sib.orig_promo_dtl_id
                  and temp1.item               = prom_sib.item
                  and temp1.loc                = prom_sib.loc
                  and temp1.orig_promo_dtl_id  > prom_sib.min_promo_dtl_id
                  and temp1.orig_promo_dtl_id  = rpd.promo_dtl_id
                  and rpd.state                = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                  and temp1.sibling_state      IN (RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                   RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                union all
                select temp1.orig_promo_dtl_id,
                       RPM_CONSTANTS.PE_TYPE_PROMO_END,
                       temp1.item,
                       temp1.loc
                  from rpm_sibling_promo_il_gtt temp1,
                       rpm_promo_dtl rpd
                 where temp1.orig_promo_dtl_id  = rpd.promo_dtl_id
                   and rpd.state                = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                   and temp1.sibling_state      IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                    RPM_CONSTANTS.PR_COMPLETE_STATE_CODE))) pcil2);
BEGIN

   --insert into the GTT table the price event promo detail's possible siblings exploded at the item/loc level
   insert into rpm_sibling_promo_il_gtt(orig_promo_dtl_id,
                                        sibling_promo_dtl_id,
                                        sibling_state,
                                        item,
                                        loc)
      select 
             promo_item.orig_promo_dtl_id,
             promo_item.sibling_promo_dtl_id,
             promo_item.state,
             ril.item,
             ril.loc
        from -- get the items for the sibling promos
             (--item level
              with pcil_tbl as
                 (select /*+ CARDINALITY (pcil, 1000)*/
                         distinct pcil.price_change_id
                    from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pcil
                   where pcil.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END)
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pdmn.promo_dtl_id
                 and pdmn.merch_type          = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item                  = pdmn.item
                 and im.item_level            = im.tran_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- item parent level
              select
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pdmn.promo_dtl_id
                 and pdmn.merch_type          = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item_parent           = pdmn.item
                 and im.item_level            = im.tran_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- parent diff level
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pdmn.promo_dtl_id
                 and pdmn.merch_type          = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item_parent           = pdmn.item
                 and im.item_level            = im.tran_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and (   im.diff_1 = pdmn.diff_id
                      or im.diff_2 = pdmn.diff_id
                      or im.diff_3 = pdmn.diff_id
                      or im.diff_4 = pdmn.diff_id)
              union all
              -- itemlist tran level
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_promo_dtl_skulist pds
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pdmn.promo_dtl_id
                 and pdmn.merch_type          = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and pds.price_event_id       = pdmn.promo_dtl_id
                 and pds.skulist              = pdmn.skulist
                 and pds.item_level           = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item                  = pds.item
                 and im.item_level            = im.tran_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- itemlist parent level
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_promo_dtl_skulist pds
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pdmn.promo_dtl_id
                 and pdmn.merch_type          = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and pds.price_event_id       = pdmn.promo_dtl_id
                 and pds.skulist              = pdmn.skulist
                 and pds.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent           = pds.item
                 and im.item_level            = im.tran_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- price_event_itemlist (peil) - item level
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_merch_list_detail rmld
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id         = pdmn.promo_dtl_id
                 and pdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rmld.merch_list_id        = pdmn.price_event_itemlist
                 and rmld.merch_level          = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item                 = im.item
                 and im.tran_level             = im.item_level
                 and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- price_event_itemlist (peil) - parent item level
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_merch_list_detail rmld
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id         = pdmn.promo_dtl_id
                 and pdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rmld.merch_list_id        = pdmn.price_event_itemlist
                 and rmld.merch_level          = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item                 = im.item_parent
                 and im.tran_level             = im.item_level
                 and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- merch hierarchy level
              select
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     im.item,
                     im.dept
                from pcil_tbl pcil,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im
               where rpd1.promo_dtl_id        = pcil.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pdmn.promo_dtl_id
                 and pdmn.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                  RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                  RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.item_level            = im.tran_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.dept                  = pdmn.dept
                 and im.class                 = NVL(pdmn.class, im.class)
                 and im.subclass              = NVL(pdmn.subclass, im.subclass)) promo_item,
             -- get the locations for the sibling promos
             (-- location level
              with pcil_tbl2 as
                 (select /*+ CARDINALITY (pcil, 1000)*/
                         distinct pcil.price_change_id
                    from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pcil
                   where pcil.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END)
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     pzl.location
                from pcil_tbl2 pcil2,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_zone_location pzl
               where rpd1.promo_dtl_id        = pcil2.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pzl.promo_dtl_id
                 and pzl.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
              union all
              -- zone level
              select 
                     rpd1.promo_dtl_id orig_promo_dtl_id,
                     rpd2.promo_dtl_id sibling_promo_dtl_id,
                     rpd2.state,
                     rzl.location location
                from pcil_tbl2 pcil2,
                     rpm_promo_dtl rpd1,
                     rpm_promo_dtl rpd2,
                     rpm_promo_zone_location pzl,
                     rpm_zone_location rzl
               where rpd1.promo_dtl_id        = pcil2.price_change_id
                 and rpd1.state               = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                 and rpd1.exception_parent_id is NOT NULL
                 and rpd1.exception_parent_id = rpd2.exception_parent_id
                 and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
                 and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
                 and rpd2.promo_dtl_id        = pzl.promo_dtl_id
                 and pzl.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rzl.loc_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rzl.zone_id              = pzl.zone_id) promo_loc,
             rpm_item_loc ril
       where promo_item.orig_promo_dtl_id    = promo_loc.orig_promo_dtl_id
         and promo_item.sibling_promo_dtl_id = promo_loc.sibling_promo_dtl_id
         and ril.item                        = promo_item.item
         and ril.loc                         = promo_loc.location
         and ril.dept                        = promo_item.dept;

   open C_FILTERED_PE;
   fetch C_FILTERED_PE BULK COLLECT into L_pc_il_tbl;
   close C_FILTERED_PE;

   if POPULATE_GTT(O_error_message,
                   L_pc_il_tbl) = 0 then
      return FALSE;
   end if;

   merge /*+ INDEX(target, rpm_event_itemloc_i1)*/ into rpm_event_itemloc target
   using (select item,
                 loc,
                 loc_type,
                 selling_unit_retail,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_selling_uom,
                 current_selling_retail,
                 current_selling_uom,
                 current_clear_retail,
                 current_clear_uom,
                 current_simple_promo_retail,
                 current_simple_promo_uom,
                 NULL prom_event,
                 price_chg_type,
                 currency_code,
                 old_selling_unit_retail,
                 old_selling_uom,
                 vendor_funded_ind,
                 funding_type,
                 funding_amount,
                 funding_amount_currency,
                 funding_percent,
                 deal_id,
                 deal_detail_id,
                 TRUNC(active_date) active_date,
                 is_on_clearance,
                 pc_reason_code,
                 change_type,
                 change_amount,
                 clear_ind,
                 primary_cntry,
                 primary_supp,
                 il_rowid,
                 current_unit_retail,
                 pc_affected_promo_fr_id,
                 price_event_id,
                 last_execution_date,
                 dept,
                 class,
                 subclass,
                 standard_uom,
                 pack_ind,
                 sellable_ind,
                 orderable_ind,
                 pack_type,
                 orig_event_type,
                 is_on_simple_promo
            from (select item,
                         loc,
                         loc_type,
                         selling_unit_retail,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_selling_uom,
                         RANK() OVER (PARTITION BY item,
                                                   loc,
                                                   trunc_value
                                          ORDER BY active_date desc) rank_value,
                         current_selling_retail,
                         current_selling_uom,
                         current_clear_retail,
                         current_clear_uom,
                         current_simple_promo_retail,
                         current_simple_promo_uom,
                         NULL prom_event,
                         price_chg_type,
                         currency_code,
                         old_selling_unit_retail,
                         old_selling_uom,
                         vendor_funded_ind,
                         funding_type,
                         funding_amount,
                         funding_amount_currency,
                         funding_percent,
                         deal_id,
                         deal_detail_id,
                         active_date,
                         is_on_clearance,
                         pc_reason_code,
                         change_type,
                         change_amount,
                         clear_ind,
                         primary_cntry,
                         primary_supp,
                         il_rowid,
                         current_unit_retail,
                         pc_affected_promo_fr_id,
                         price_event_id,
                         last_execution_date,
                         dept,
                         class,
                         subclass,
                         standard_uom,
                         pack_ind,
                         sellable_ind,
                         orderable_ind,
                         pack_type,
                         orig_event_type,
                         is_on_simple_promo
                    from (select /*+ CARDINALITY (pc, 1000)*/
                                 rfr.item,
                                 rfr.location loc,
                                 DECODE(rfr.zone_node_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                        RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                                 rfr.simple_promo_retail selling_unit_retail,
                                 rfr.simple_promo_uom selling_uom,
                                 rfr.multi_units,
                                 rfr.multi_unit_retail,
                                 rfr.multi_selling_uom,
                                 rfr.selling_retail current_selling_retail,
                                 rfr.selling_uom current_selling_uom,
                                 rfr.clear_retail current_clear_retail,
                                 rfr.clear_uom current_clear_uom,
                                 rfr.simple_promo_retail current_simple_promo_retail,
                                 rfr.simple_promo_uom current_simple_promo_uom,
                                 NULL prom_event,
                                 RPM_CONSTANTS.PE_TYPE_PROMO_START price_chg_type,
                                 rfr.simple_promo_retail_currency currency_code,
                                 NULL old_selling_unit_retail,
                                 NULL old_selling_uom,
                                 'N' vendor_funded_ind,
                                 NULL funding_type,
                                 NULL funding_amount,
                                 NULL funding_amount_currency,
                                 NULL funding_percent,
                                 NULL deal_id,
                                 NULL deal_detail_id,
                                 rfr.action_date active_date,
                                 TRUNC(rfr.action_date) trunc_value,
                                 DECODE(rfr.clearance_id,
                                        NULL, 0,
                                        DECODE(rfr.clear_start_ind,
                                               RPM_CONSTANTS.END_IND, 0,
                                               1)) is_on_clearance,
                                 NULL pc_reason_code,
                                 NULL change_type,
                                 NULL change_amount,
                                 il.clear_ind,
                                 il.primary_cntry,
                                 il.primary_supp,
                                 il.rowid il_rowid,
                                 il.unit_retail current_unit_retail,
                                 NULL pc_affected_promo_fr_id,
                                 pc.price_change_id price_event_id,
                                 LP_vdate last_execution_date,
                                 im.dept,
                                 im.class,
                                 im.subclass,
                                 im.standard_uom,
                                 im.pack_ind,
                                 im.sellable_ind,
                                 im.orderable_ind,
                                 im.pack_type,
                                 RPM_CONSTANTS.PE_TYPE_PROMO_START orig_event_type,
                                 rfr.on_simple_promo_ind is_on_simple_promo
                            from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                                 item_master im,
                                 item_loc il,
                                 rpm_promo_item_loc_expl_gtt rpile,
                                 rpm_future_retail_gtt rfr,
                                 rpm_promo_dtl rpd
                           where pc.price_change_type        = RPM_CONSTANTS.PE_TYPE_PROMO_START
                                 and im.item                 = pc.item
                                 and il.item                 = pc.item
                                 and il.loc                  = pc.location
                                 and rpile.price_event_id    = pc.price_change_id
                                 and rpile.dept              = im.dept
                                 and rpile.item              = pc.item
                                 and rpile.location          = pc.location
                                 and rpile.promo_dtl_id      = pc.price_change_id
                                 and rpile.type              = RPM_CONSTANTS.SIMPLE_CODE
                                 and rpile.detail_change_type != RPM_CONSTANTS.RETAIL_EXCLUDE
                                 and rpile.price_event_id    = rfr.price_event_id
                                 and rpile.item              = rfr.item
                                 and rpile.location          = rfr.location
                                 and rpile.detail_start_date = rfr.action_date
                                 and rfr.dept                = rpile.dept
                                 and rfr.price_event_id      = pc.price_change_id
                                 and rpd.promo_dtl_id        = pc.price_change_id
                                 and rpile.customer_type     is NULL
                                 and (   rpile.detail_apply_to_code          = RPM_CONSTANTS.REGULAR_AND_CLEARANCE
                                      or (    rpile.detail_apply_to_code     = RPM_CONSTANTS.REGULAR_ONLY
                                          and (   rfr.clearance_id           is NULL
                                               or (    rfr.clearance_id      is NOT NULL
                                                   and rfr.clear_start_ind   IN (RPM_CONSTANTS.END_IND,
                                                                                 RPM_CONSTANTS.START_END_IND)
                                                   and rpd.end_date          > rfr.action_date)))
                                          or (    rpile.detail_apply_to_code = RPM_CONSTANTS.CLEARANCE_ONLY
                                              and (    rfr.clearance_id      is NOT NULL
                                                   and rfr.clear_start_ind   NOT IN (RPM_CONSTANTS.END_IND,
                                                                                     RPM_CONSTANTS.START_END_IND))))))
           where rank_value = 1
          union all
          select distinct
                 t.item item,
                 t.loc,
                 t.loc_type,
                 t.selling_unit_retail,
                 t.selling_uom,
                 t.multi_units,
                 t.multi_unit_retail,
                 t.multi_selling_uom,
                 t.current_selling_retail,
                 t.current_selling_uom,
                 t.current_clear_retail,
                 t.current_clear_uom,
                 t.current_simple_promo_retail,
                 t.current_simple_promo_uom,
                 NULL prom_event,
                 t.price_chg_type,
                 t.currency_code,
                 NULL old_selling_unit_retail,
                 NULL old_selling_uom,
                 'N' vendor_funded_ind,
                 NULL funding_type,
                 NULL funding_amount,
                 NULL funding_amount_currency,
                 NULL funding_percent,
                 NULL deal_id,
                 NULL deal_detail_id,
                 t.active_date,
                 t.is_on_clearance,
                 NULL pc_reason_code,
                 NULL change_type,
                 NULL change_amount,
                 t.clear_ind,
                 t.primary_cntry,
                 t.primary_supp,
                 t.il_rowid,
                 t.current_unit_retail,
                 pc_affected_promo_fr_id,
                 t.price_event_id,
                 t.last_execution_date,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.standard_uom,
                 t.pack_ind,
                 t.sellable_ind,
                 t.orderable_ind,
                 t.pack_type,
                 t.orig_event_type,
                 t.is_on_simple_promo
            from (select s.item,
                         loc,
                         loc_type,
                         selling_unit_retail,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_selling_uom,
                         current_selling_retail,
                         current_selling_uom,
                         current_clear_retail,
                         current_clear_uom,
                         current_simple_promo_retail,
                         current_simple_promo_uom,
                         price_chg_type,
                         currency_code,
                         active_date,
                         RANK() OVER (PARTITION BY s.item,
                                                   s.loc,
                                                   trunc_value
                                          ORDER BY active_date desc) rank_value,
                         is_on_clearance,
                         clear_ind,
                         primary_cntry,
                         primary_supp,
                         il_rowid,
                         current_unit_retail,
                         case
                            when s.promo_id is NOT NULL and
                                 rpile2.promo_id is NOT NULL and
                                 NOT (rpile2.promo_id             = s.promo_id and
                                      rpile2.promo_dtl_id        != s.promo_dtl_id and
                                      rpile2.exception_parent_id is NULL) and
                                 NOT (rpile2.promo_id             = s.promo_id and
                                      rpile2.promo_comp_id        = s.promo_comp_id and
                                      rpile2.promo_dtl_id        != s.promo_dtl_id and
                                      rpile2.exception_parent_id is NOT NULL) then
                               ---
                               s.future_retail_id
                            else
                               NULL
                         end pc_affected_promo_fr_id,
                         s.price_event_id,
                         last_execution_date,
                         s.dept,
                         s.class,
                         s.subclass,
                         standard_uom,
                         pack_ind,
                         sellable_ind,
                         orderable_ind,
                         pack_type,
                         orig_event_type,
                         is_on_simple_promo,
                         future_retail_id,
                         s.promo_id,
                         s.promo_comp_id,
                         s.promo_dtl_id
                    from (select /*+ CARDINALITY (pc, 1000) */
                                 rfr.item item,
                                 rfr.location loc,
                                 case rfr.zone_node_type
                                    when RPM_CONSTANTS.ZONE_NODE_TYPE_STORE then
                                       RPM_CONSTANTS.LOCATION_TYPE_STORE
                                    else
                                       RPM_CONSTANTS.LOCATION_TYPE_WH
                                 end loc_type,
                                 rfr.simple_promo_retail selling_unit_retail,
                                 rfr.simple_promo_uom selling_uom,
                                 rfr.multi_units,
                                 rfr.multi_unit_retail,
                                 rfr.multi_selling_uom,
                                 rfr.selling_retail current_selling_retail,
                                 rfr.selling_uom current_selling_uom,
                                 rfr.clear_retail current_clear_retail,
                                 rfr.clear_uom current_clear_uom,
                                 rfr.simple_promo_retail current_simple_promo_retail,
                                 rfr.simple_promo_uom current_simple_promo_uom,
                                 RPM_CONSTANTS.PE_TYPE_PROMO_END price_chg_type,
                                 NVL(rfr.simple_promo_retail_currency, rfr.selling_retail_currency) currency_code,
                                 rfr.action_date active_date,
                                 TRUNC(rfr.action_date) trunc_value,
                                 DECODE(rfr.clearance_id,
                                        NULL, 0,
                                        DECODE(rfr.clear_start_ind,
                                               RPM_CONSTANTS.END_IND, 0,
                                               1)) is_on_clearance,
                                 il.clear_ind,
                                 il.primary_cntry,
                                 il.primary_supp,
                                 il.rowid il_rowid,
                                 il.unit_retail current_unit_retail,
                                 pc.price_change_id price_event_id,
                                 LP_vdate last_execution_date,
                                 im.dept,
                                 im.class,
                                 im.subclass,
                                 im.standard_uom,
                                 im.pack_ind,
                                 im.sellable_ind,
                                 im.orderable_ind,
                                 im.pack_type,
                                 RPM_CONSTANTS.PE_TYPE_PROMO_END orig_event_type,
                                 rfr.on_simple_promo_ind is_on_simple_promo,
                                 case rpile.promo_dtl_id
                                    when pc.price_change_id then
                                       NULL
                                    else
                                       rfr.future_retail_id
                                 end future_retail_id,
                                 case rpile.promo_dtl_id
                                    when pc.price_change_id then
                                       NULL
                                    else
                                       rpile.promo_id
                                 end promo_id,
                                 case rpile.promo_dtl_id
                                    when pc.price_change_id then
                                       NULL
                                    else
                                       rpile.promo_comp_id
                                 end promo_comp_id,
                                 case rpile.promo_dtl_id
                                    when pc.price_change_id then
                                       NULL
                                    else
                                       rpile.promo_dtl_id
                                 end promo_dtl_id
                            from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                                 rpm_promo_dtl rpd,
                                 item_master im,
                                 item_loc il,
                                 rpm_future_retail_gtt rfr,
                                 rpm_promo_item_loc_expl_gtt rpile
                           where pc.price_change_type          = RPM_CONSTANTS.PE_TYPE_PROMO_END
                             and rpd.promo_dtl_id              = pc.price_change_id
                             and im.item                       = pc.item
                             and il.item                       = pc.item
                             and il.loc                        = pc.location
                             and rfr.dept                      = im.dept
                             and rfr.price_event_id            = pc.price_change_id
                             and rfr.item                      = pc.item
                             and rfr.location                  = pc.location
                             and rfr.action_date               = rpd.end_date + RPM_CONSTANTS.ONE_SECOND
                             and rpile.price_event_id (+)      = rfr.price_event_id
                             and rpile.item (+)                = rfr.item
                             and rpile.location (+)            = rfr.location
                             and rfr.action_date              >= rpile.detail_start_date (+)
                             and rfr.action_date              <= NVL(rpile.detail_end_date(+) + RPM_CONSTANTS.ONE_MINUTE, rfr.action_date)
                             and rpile.detail_change_type (+) != RPM_CONSTANTS.RETAIL_EXCLUDE
                             and rpile.type (+)                = RPM_CONSTANTS.SIMPLE_CODE
                             and rpile.customer_type (+)      is NULL
                             and (   rpile.detail_apply_to_code          = RPM_CONSTANTS.REGULAR_AND_CLEARANCE
                                  or (    rpile.detail_apply_to_code     = RPM_CONSTANTS.REGULAR_ONLY
                                      and (   rfr.clearance_id           is NULL
                                           or (    rfr.clearance_id      is NOT NULL
                                               and rfr.clear_start_ind   IN (RPM_CONSTANTS.END_IND,
                                                                             RPM_CONSTANTS.START_END_IND)
                                               and rpd.end_date          > rfr.action_date)))
                                      or (    rpile.detail_apply_to_code = RPM_CONSTANTS.CLEARANCE_ONLY
                                          and (    rfr.clearance_id      is NOT NULL
                                               and rfr.clear_start_ind   NOT IN (RPM_CONSTANTS.END_IND,
                                                                                 RPM_CONSTANTS.START_END_IND))))) s,
                         rpm_promo_item_loc_expl_gtt rpile2
                   where rpile2.item (+)                = s.item
                     and rpile2.location (+)            = s.loc
                     and rpile2.detail_change_type (+) != RPM_CONSTANTS.RETAIL_EXCLUDE) t
           where t.rank_value = 1) source
   on (    target.price_chg_type  = source.price_chg_type
       and target.orig_event_type = source.orig_event_type
       and target.price_event_id  = source.price_event_id
       and target.item            = source.item
       and target.loc             = source.loc
       and target.loc_type        = source.loc_type
       and target.active_date     = source.active_date)
   when MATCHED then
      update
         set target.selling_unit_retail         = source.selling_unit_retail,
             target.selling_uom                 = source.selling_uom,
             target.multi_units                 = source.multi_units,
             target.multi_unit_retail           = source.multi_unit_retail,
             target.multi_selling_uom           = source.multi_selling_uom,
             target.prom_event                  = source.prom_event,
             target.currency_code               = source.currency_code,
             target.old_selling_unit_retail     = source.old_selling_unit_retail,
             target.old_selling_uom             = source.old_selling_uom,
             target.vendor_funded_ind           = source.vendor_funded_ind,
             target.funding_type                = source.funding_type,
             target.funding_amount              = source.funding_amount,
             target.funding_amount_currency     = source.funding_amount_currency,
             target.funding_percent             = source.funding_percent,
             target.deal_id                     = source.deal_id,
             target.deal_detail_id              = source.deal_detail_id,
             target.is_on_clearance             = source.is_on_clearance,
             target.pc_reason_code              = source.pc_reason_code,
             target.change_type                 = source.change_type,
             target.change_amount               = source.change_amount,
             target.clear_ind                   = source.clear_ind,
             target.primary_cntry               = source.primary_cntry,
             target.primary_supp                = source.primary_supp,
             target.il_rowid                    = source.il_rowid,
             target.current_unit_retail         = source.current_unit_retail,
             target.pc_affected_promo_fr_id     = source.pc_affected_promo_fr_id,
             target.last_execution_date         = source.last_execution_date,
             target.dept                        = source.dept,
             target.class                       = source.class,
             target.subclass                    = source.subclass,
             target.standard_uom                = source.standard_uom,
             target.pack_ind                    = source.pack_ind,
             target.sellable_ind                = source.sellable_ind,
             target.orderable_ind               = source.orderable_ind,
             target.pack_type                   = source.pack_type,
             target.current_selling_retail      = source.current_selling_retail,
             target.current_selling_uom         = source.current_selling_uom,
             target.current_clear_retail        = source.current_clear_retail,
             target.current_clear_uom           = source.current_clear_uom,
             target.current_simple_promo_retail = source.current_simple_promo_retail,
             target.current_simple_promo_uom    = source.current_simple_promo_uom,
             target.is_on_simple_promo          = source.is_on_simple_promo
   when NOT MATCHED then
      insert(item,
             loc,
             loc_type,
             selling_unit_retail,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             prom_event,
             price_chg_type,
             currency_code,
             old_selling_unit_retail,
             old_selling_uom,
             vendor_funded_ind,
             funding_type,
             funding_amount,
             funding_amount_currency,
             funding_percent,
             deal_id,
             deal_detail_id,
             active_date,
             is_on_clearance,
             pc_reason_code,
             change_type,
             change_amount,
             clear_ind,
             primary_cntry,
             primary_supp,
             il_rowid,
             current_unit_retail,
             pc_affected_promo_fr_id,
             price_event_id,
             last_execution_date,
             dept,
             class,
             subclass,
             standard_uom,
             pack_ind,
             sellable_ind,
             orderable_ind,
             pack_type,
             orig_event_type,
             current_selling_retail,
             current_selling_uom,
             current_clear_retail,
             current_clear_uom,
             current_simple_promo_retail,
             current_simple_promo_uom,
             is_on_simple_promo)
      values(source.item,
             source.loc,
             source.loc_type,
             source.selling_unit_retail,
             source.selling_uom,
             source.multi_units,
             source.multi_unit_retail,
             source.multi_selling_uom,
             source.prom_event,
             source.price_chg_type,
             source.currency_code,
             source.old_selling_unit_retail,
             source.old_selling_uom,
             source.vendor_funded_ind,
             source.funding_type,
             source.funding_amount,
             source.funding_amount_currency,
             source.funding_percent,
             source.deal_id,
             source.deal_detail_id,
             source.active_date,
             source.is_on_clearance,
             source.pc_reason_code,
             source.change_type,
             source.change_amount,
             source.clear_ind,
             source.primary_cntry,
             source.primary_supp,
             source.il_rowid,
             source.current_unit_retail,
             source.pc_affected_promo_fr_id,
             source.price_event_id,
             source.last_execution_date,
             source.dept,
             source.class,
             source.subclass,
             source.standard_uom,
             source.pack_ind,
             source.sellable_ind,
             source.orderable_ind,
             source.pack_type,
             source.orig_event_type,
             source.current_selling_retail,
             source.current_selling_uom,
             source.current_clear_retail,
             source.current_clear_uom,
             source.current_simple_promo_retail,
             source.current_simple_promo_uom,
             source.is_on_simple_promo);

   if I_change_event_status then

      open C_PC_TBL;
      fetch C_PC_TBL BULK COLLECT into L_pc_tbl;
      close C_PC_TBL;

      if L_pc_tbl is NOT NULL and
         L_pc_tbl.COUNT > 0 then

         merge into rpm_promo_dtl target
         using (select /*+ CARDINALITY (pc, 1000)*/
                       rpd.promo_dtl_id,
                       case
                          when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_START then
                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                          when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END and
                               rpd.state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE
                          when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END and
                               rpd.state != RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                             RPM_CONSTANTS.PR_COMPLETE_STATE_CODE
                          else
                             rpd.state
                       end as end_state
                  from table(cast(L_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                       rpm_promo_dtl rpd
                 where rpd.promo_dtl_id     = pc.price_change_id
                   and pc.price_change_type IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                RPM_CONSTANTS.PE_TYPE_PROMO_END)
                   and 1 = (case
                               when pc.price_change_type  = RPM_CONSTANTS.PE_TYPE_PROMO_START and
                                    rpd.state != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then
                                  1
                               when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PROMO_END and
                                    rpd.state NOT IN (RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                      RPM_CONSTANTS.PR_COMPLETE_STATE_CODE) then
                                  1
                               else
                                  0
                            end)) source
         on (target.promo_dtl_id = source.promo_dtl_id)
         when MATCHED then
            update
               set target.state = source.end_state;

      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_PROMOS;

--------------------------------------------------------------------------------

FUNCTION EXECUTE_AFFECTED_PROMOS(O_error_message          OUT VARCHAR2,
                                 I_pc_tbl              IN     OBJ_PRICE_CHANGE_TBL,
                                 I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.EXECUTE_AFFECTED_PROMOS';

   L_pc_il_tbl OBJ_PRICE_CHANGE_IL_TBL := NULL;

   cursor C_GET_PC_IL is
      select OBJ_PRICE_CHANGE_IL_REC(pc.PRICE_CHANGE_ID,
                                     pc.PRICE_CHANGE_TYPE,
                                     item.item,
                                     loc.location)
        from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
             rpm_price_event_thread_item item,
             rpm_price_event_thread_loc loc
       where pc.price_change_type  IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                       RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                       RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                       RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                       RPM_CONSTANTS.PE_TYPE_PROMO_END)
         and item.thread_number    = -999
         and item.pe_thread_id     IN (LP_pc_pe_thread_id,
                                       LP_cl_pe_thread_id,
                                       LP_sp_pe_thread_id)
         and loc.pe_thread_id      = item.pe_thread_id
         and item.price_event_id   = pc.price_change_id
         and item.price_event_type = pc.price_change_type
         and loc.price_event_id    = item.price_event_id
         and loc.price_event_type  = item.price_event_type;

BEGIN

   open C_GET_PC_IL;
   fetch C_GET_PC_IL BULK COLLECT into L_pc_il_tbl;
   close C_GET_PC_IL;

   if L_pc_il_tbl is NOT NULL and
      L_pc_il_tbl.COUNT > 0 then

      if EXECUTE_AFFECTED_PROMOS(O_error_message,
                                 L_pc_il_tbl,
                                 FALSE) = FALSE Then
         return false;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_AFFECTED_PROMOS;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_AFFECTED_PROMOS(O_error_message          OUT VARCHAR2,
                                 I_pc_il_tbl           IN     OBJ_PRICE_CHANGE_IL_TBL,
                                 I_change_event_status IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.EXECUTE_AFFECTED_PROMOS';

BEGIN

   merge /*+INDEX(target rpm_event_itemloc_i1)*/ into rpm_event_itemloc target
   using (select /*+ INDEX(rfr PK_RPM_FUTURE_RETAIL) INDEX(staged rpm_event_itemloc_i1)
                     ORDERED CARDINALITY(pc, 1000) INDEX(rpile, rpm_promo_item_loc_expl_i1) */
                 rfr.item,
                 rfr.location,
                 DECODE(rfr.zone_node_type,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                 rfr.simple_promo_retail selling_unit_retail,
                 rfr.simple_promo_uom selling_uom,
                 rfr.multi_units multi_units,
                 rfr.multi_unit_retail multi_unit_retail,
                 rfr.multi_selling_uom multi_selling_uom,
                 rfr.selling_retail      current_selling_retail,
                 rfr.selling_uom         current_selling_uom,
                 rfr.clear_retail        current_clear_retail,
                 rfr.clear_uom           current_clear_uom,
                 rfr.simple_promo_retail current_simple_promo_retail,
                 rfr.simple_promo_uom    current_simple_promo_uom,
                 RPM_CONSTANTS.PE_TYPE_PROMO_START price_chg_type,
                 rfr.simple_promo_retail_currency currency_code,
                 'N'  vendor_funded_ind,
                 rfr.action_date active_date,
                 DECODE(rfr.clearance_id,
                        NULL, 0,
                        DECODE(rfr.clear_start_ind,
                               RPM_CONSTANTS.END_IND, 0,
                               1)) is_on_clearance,
                 il.clear_ind clear_ind,
                 il.primary_cntry primary_cntry,
                 il.primary_supp primary_supp,
                 il.rowid il_rowid,
                 il.unit_retail current_unit_retail,
                 pc.price_change_id price_event_id,   -- Ensure that we remember the price change or clearance id that impacted these rows
                 LP_vdate last_execution_date,
                 im.dept dept,
                 im.class class,
                 im.subclass subclass,
                 im.standard_uom standard_uom,
                 im.pack_ind pack_ind,
                 im.sellable_ind sellable_ind,
                 im.orderable_ind orderable_ind,
                 im.pack_type pack_type,
                 pc.price_change_type orig_event_type, -- Ensure that we remember the type (price change or clearance) that impacted these rows
                 rfr.on_simple_promo_ind is_on_simple_promo
            from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                 rpm_event_itemloc staged,
                 rpm_future_retail_gtt rfr,
                 rpm_promo_item_loc_expl_gtt rpile,
                 item_master im,
                 item_loc il
           where pc.price_change_type           IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                    RPM_CONSTANTS.PE_TYPE_PROMO_END)
             and staged.price_chg_type          = pc.price_change_type
             and staged.orig_event_type         = pc.price_change_type
             and staged.price_event_id          = pc.price_change_id
             and staged.item                    = pc.item
             and staged.loc                     = pc.location
             and staged.pc_affected_promo_fr_id is NOT NULL
             and rfr.future_retail_id           = staged.pc_affected_promo_fr_id
             and il.item                        = rfr.item
             and il.loc                         = rfr.location
             and im.item                        = rfr.item
             and il.item                        = pc.item
             and il.loc                         = pc.location
             and rpile.type                     = RPM_CONSTANTS.SIMPLE_CODE
             and rpile.detail_change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
             and rpile.price_event_id           = pc.price_change_id
             and rpile.price_event_id           = rfr.price_event_id
             and rpile.item                     = rfr.item
             and rpile.location                 = rfr.location
             and rpile.dept                     = rfr.dept
             and rfr.action_date                > rpile.detail_start_date
             and rfr.action_date               <= NVL(rpile.detail_end_date, rfr.action_date)
             and (   rpile.detail_apply_to_code          = RPM_CONSTANTS.REGULAR_AND_CLEARANCE
                  or (    rpile.detail_apply_to_code     = RPM_CONSTANTS.REGULAR_ONLY
                      and (   rfr.clearance_id           is NULL
                           or (    rfr.clearance_id      is NOT NULL
                               and rfr.clear_start_ind   IN (RPM_CONSTANTS.END_IND,
                                                             RPM_CONSTANTS.START_END_IND)
                               and rpile.detail_end_date > rfr.action_date)))
                      or (    rpile.detail_apply_to_code = RPM_CONSTANTS.CLEARANCE_ONLY
                          and (    rfr.clearance_id      is NOT NULL
                               and rfr.clear_start_ind   NOT IN (RPM_CONSTANTS.END_IND,
                                                                 RPM_CONSTANTS.START_END_IND))))) source
   on (    target.price_chg_type  = source.price_chg_type
       and target.orig_event_type = source.orig_event_type
       and target.price_event_id  = source.price_event_id
       and target.item            = source.item
       and target.loc             = source.location
       and target.loc_type        = source.loc_type
       and target.active_date     = source.active_date)
   when MATCHED then
      update
         set target.selling_unit_retail         = source.selling_unit_retail,
             target.selling_uom                 = source.selling_uom,
             target.multi_units                 = source.multi_units,
             target.multi_unit_retail           = source.multi_unit_retail,
             target.multi_selling_uom           = source.multi_selling_uom,
             target.currency_code               = source.currency_code,
             target.vendor_funded_ind           = source.vendor_funded_ind,
             target.is_on_clearance             = source.is_on_clearance,
             target.clear_ind                   = source.clear_ind,
             target.primary_cntry               = source.primary_cntry,
             target.primary_supp                = source.primary_supp,
             target.il_rowid                    = source.il_rowid,
             target.current_unit_retail         = source.current_unit_retail,
             target.last_execution_date         = source.last_execution_date,
             target.dept                        = source.dept,
             target.class                       = source.class,
             target.subclass                    = source.subclass,
             target.standard_uom                = source.standard_uom,
             target.pack_ind                    = source.pack_ind,
             target.sellable_ind                = source.sellable_ind,
             target.orderable_ind               = source.orderable_ind,
             target.pack_type                   = source.pack_type,
             target.current_selling_retail      = source.current_selling_retail,
             target.current_selling_uom         = source.current_selling_uom,
             target.current_clear_retail        = source.current_clear_retail,
             target.current_clear_uom           = source.current_clear_uom,
             target.current_simple_promo_retail = source.current_simple_promo_retail,
             target.current_simple_promo_uom    = source.current_simple_promo_uom,
             target.is_on_simple_promo          = source.is_on_simple_promo
   when NOT MATCHED then
      insert (item,
              loc,
              loc_type,
              selling_unit_retail,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_selling_uom,
              price_chg_type,
              currency_code,
              vendor_funded_ind,
              active_date,
              is_on_clearance,
              clear_ind,
              primary_cntry,
              primary_supp,
              il_rowid,
              current_unit_retail,
              price_event_id,
              last_execution_date,
              dept,
              class,
              subclass,
              standard_uom,
              pack_ind,
              sellable_ind,
              orderable_ind,
              pack_type,
              orig_event_type,
              current_selling_retail,
              current_selling_uom,
              current_clear_retail,
              current_clear_uom,
              current_simple_promo_retail,
              current_simple_promo_uom,
              is_on_simple_promo)
   values(source.item,
          source.location,
          source.loc_type,
          source.selling_unit_retail,
          source.selling_uom,
          source.multi_units,
          source.multi_unit_retail,
          source.multi_selling_uom,
          source.price_chg_type,
          source.currency_code,
          source.vendor_funded_ind,
          source.active_date,
          source.is_on_clearance,
          source.clear_ind,
          source.primary_cntry,
          source.primary_supp,
          source.il_rowid,
          source.current_unit_retail,
          source.price_event_id,
          source.last_execution_date,
          source.dept,
          source.class,
          source.subclass,
          source.standard_uom,
          source.pack_ind,
          source.sellable_ind,
          source.orderable_ind,
          source.pack_type,
          source.orig_event_type,
          source.current_selling_retail,
          source.current_selling_uom,
          source.current_clear_retail,
          source.current_clear_uom,
          source.current_simple_promo_retail,
          source.current_simple_promo_uom,
          source.is_on_simple_promo);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXECUTE_AFFECTED_PROMOS;
--------------------------------------------------------------------------------

FUNCTION UPDATE_ITEM_ZONE_PRICE(O_error_message    OUT VARCHAR2,
                               I_pc_tbl         IN     OBJ_PRICE_CHANGE_TBL)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_PRICE_EVENT_SQL.UPDATE_ITEM_ZONE_PRICE';

   L_rpm_item_pricing_tbl OBJ_ITEM_ZONE_PRICE_TBL := OBJ_ITEM_ZONE_PRICE_TBL();
   L_item_pricing_rec     OBJ_ITEM_ZONE_PRICE_REC := NULL;

   L_converted_retail_value NUMBER(20,4) := NULL;

   cursor C_PC is
      select OBJ_ITEM_ZONE_PRICE_REC(item,
                                     zone,
                                     selling_retail,
                                     selling_retail_currency,
                                     selling_uom,
                                     std_retail,
                                     std_retail_currency,
                                     std_uom,
                                     need_uom_convert,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_unit_retail_currency,
                                     multi_selling_uom,
                                     lock_version)
        from (-- for tran level items
              select item,
                     zone,
                     selling_retail,
                     selling_retail_currency,
                     selling_uom,
                     std_retail,
                     std_retail_currency,
                     std_uom,
                     need_uom_convert,
                     multi_units,
                     multi_unit_retail,
                     multi_unit_retail_currency,
                     multi_selling_uom,
                     lock_version
               from (select /*+CARDINALITY(pc 1) */
                            rfr.item,
                            rfr.zone,
                            RANK() OVER (PARTITION BY rfr.item,
                                                      rfr.zone
                                             ORDER BY rfr.action_date desc) ranking,
                            rfr.selling_retail,
                            rfr.selling_retail_currency,
                            rfr.selling_uom,
                            rfr.selling_retail std_retail,
                            rfr.selling_retail_currency std_retail_currency,
                            im.standard_uom std_uom,
                            case
                               when rpc.change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                                  case
                                     when im.standard_uom != NVL(rpc.change_selling_uom, im.standard_uom) then
                                        'Y'
                                     else
                                        'N'
                                  end
                               else
                                  case
                                     when im.standard_uom != NVL(rfr.selling_uom, im.standard_uom) then
                                        'Y'
                                     else
                                        'N'
                               end
                            end need_uom_convert,
                            rfr.multi_units,
                            rfr.multi_unit_retail,
                            rfr.multi_unit_retail_currency,
                            rfr.multi_selling_uom,
                            rfr.lock_version
                       from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                            rpm_zone_future_retail rfr,
                            rpm_price_change rpc,
                            item_master im
                      where rfr.price_change_id  = rpc.price_change_id
                        and rpc.price_change_id  = pc.price_change_id
                        and im.item              = rfr.item
                        and im.tran_level        = im.item_level
                        and pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE)
              where ranking = 1
              union
              -- for parent level items with an existing RPM_ITEM_ZONE_PRICE row and location range (RFR exists)
              select item,
                     zone,
                     selling_retail,
                     selling_retail_currency,
                     selling_uom,
                     std_retail,
                     std_retail_currency,
                     std_uom,
                     need_uom_convert,
                     multi_units,
                     multi_unit_retail,
                     multi_unit_retail_currency,
                     multi_selling_uom,
                     lock_version
                from (select /*+CARDINALITY(pc 1) */
                             rizp.item,
                             rizp.zone_id zone,
                             RANK() OVER (PARTITION BY rizp.item,
                                                       rizp.zone_id
                                              ORDER BY rpc.effective_date desc,
                                                       rpc.approval_date) ranking,
                             rpc.change_amount selling_retail,
                             rizp.selling_retail_currency,
                             case
                                when rpc.change_selling_uom = im.standard_uom then
                                   rpc.change_selling_uom
                                else
                                   rizp.selling_uom
                             end selling_uom,
                             rpc.change_amount std_retail,
                             rizp.selling_retail_currency std_retail_currency,
                             im.standard_uom std_uom,
                             case
                                when im.standard_uom != NVL(rpc.change_selling_uom, im.standard_uom) then
                                   'Y'
                                else
                                   'N'
                             end need_uom_convert,
                             rpc.multi_units,
                             rpc.multi_unit_retail,
                             rpc.multi_unit_retail_currency,
                             rpc.multi_selling_uom,
                             rizp.lock_version
                        from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                             rpm_price_change rpc,
                             rpm_item_zone_price rizp,
                             item_master im,
                             rpm_future_retail_gtt rfr_gtt
                       where rpc.price_change_id             = pc.price_change_id
                         and rpc.change_type                 = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                         and rizp.item                       = rpc.item
                         and rizp.zone_id                    = rpc.zone_id
                         and im.item                         = rpc.item
                         and (im.tran_level - im.item_level) = 1
                         and rpc.diff_id                     is NULL
                         and pc.price_change_type            = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                         and rfr_gtt.item_parent             = im.item
                         and rfr_gtt.price_event_id          = rpc.price_change_id
                         and rfr_gtt.action_date             = rpc.effective_date)
              where ranking = 1
              union
              -- for parent level items with an existing RPM_ITEM_ZONE_PRICE row and are not ranged yet
              select item,
                     zone,
                     selling_retail,
                     selling_retail_currency,
                     selling_uom,
                     std_retail,
                     std_retail_currency,
                     std_uom,
                     need_uom_convert,
                     multi_units,
                     multi_unit_retail,
                     multi_unit_retail_currency,
                     multi_selling_uom,
                     lock_version
                from (select item,
                             zone,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             std_retail,
                             std_retail_currency,
                             std_uom,
                             need_uom_convert,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             lock_version,
                             rfr_ind,
                             effective_date,
                             RANK() OVER (PARTITION BY item,
                                                       zone
                                              ORDER BY effective_date desc,
                                                       approval_date desc) ranking
                        from (select item,
                                     zone,
                                     selling_retail,
                                     selling_retail_currency,
                                     selling_uom,
                                     std_retail,
                                     std_retail_currency,
                                     std_uom,
                                     need_uom_convert,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_unit_retail_currency,
                                     multi_selling_uom,
                                     lock_version,
                                     rfr_ind,
                                     effective_date,
                                     approval_date,
                                     SUM(rfr_ind) OVER (PARTITION BY item,
                                                                     zone,
                                                                     effective_date) sum_rfr_ind
                                from (select /*+ INDEX(rpc) INDEX(im PK_ITEM_MASTER) */
                                             rizp.item,
                                             rizp.zone_id zone,
                                             rpc.change_amount selling_retail,
                                             rizp.selling_retail_currency,
                                             case
                                                when rpc.change_selling_uom = im.standard_uom then
                                                   rpc.change_selling_uom
                                                else
                                                   rizp.selling_uom
                                             end selling_uom,
                                             rpc.change_amount std_retail,
                                             rizp.selling_retail_currency std_retail_currency,
                                             im.standard_uom std_uom,
                                             case
                                                when im.standard_uom != NVL(rpc.change_selling_uom, im.standard_uom) then
                                                   'Y'
                                                else
                                                   'N'
                                             end need_uom_convert,
                                             rpc.multi_units multi_units,
                                             rpc.multi_unit_retail multi_unit_retail,
                                             rpc.multi_unit_retail_currency multi_unit_retail_currency,
                                             rpc.multi_selling_uom multi_selling_uom,
                                             rizp.lock_version lock_version,
                                             case
                                                when rfr_gtt.future_retail_id is NULL then
                                                   0
                                                else
                                                   1
                                             end rfr_ind,
                                             rpc.effective_date,
                                             rpc.approval_date
                                        from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                                             rpm_price_change rpc,
                                             rpm_item_zone_price rizp,
                                             item_master im,
                                             rpm_future_retail_gtt rfr_gtt
                                       where rpc.price_change_id             = pc.price_change_id
                                         and rpc.change_type                 = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                         and rizp.item                       = rpc.item
                                         and rizp.zone_id                    = rpc.zone_id
                                         and im.item                         = rpc.item
                                         and (im.tran_level - im.item_level) = 1
                                         and rpc.diff_id                     is NULL
                                         and pc.price_change_type            = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                         and rfr_gtt.item_parent (+)         = rpc.item
                                         and rfr_gtt.zone_id (+)             = rpc.zone_id
                                         and rfr_gtt.price_event_id (+)      = rpc.price_change_id))
                       where sum_rfr_ind = 0)
              where ranking = 1
              union
              -- for parent level items at the zone without an RPM_ITEM_ZONE_PRICE row
              select item,
                     zone,
                     selling_retail,
                     selling_retail_currency,
                     selling_uom,
                     std_retail,
                     std_retail_currency,
                     std_uom,
                     need_uom_convert,
                     multi_units,
                     multi_unit_retail,
                     multi_unit_retail_currency,
                     multi_selling_uom,
                     lock_version
                from (select /*+ INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) CARDINALITY(pc 1) */
                             rpc.item,
                             rpc.zone_id as zone,
                             RANK() OVER (PARTITION BY rpc.item,
                                                       rpc.zone_id
                                              ORDER BY rpc.effective_date desc,
                                                       rpc.approval_date desc) ranking,
                             rpc.change_amount selling_retail,
                             rpc.change_currency selling_retail_currency,
                             rpc.change_selling_uom selling_uom,
                             rpc.change_amount std_retail,
                             rpc.change_currency std_retail_currency,
                             im.standard_uom std_uom,
                             'N' need_uom_convert,
                             rpc.multi_units,
                             rpc.multi_unit_retail,
                             rpc.multi_unit_retail_currency,
                             rpc.multi_selling_uom,
                             rpc.lock_version
                        from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                             rpm_price_change rpc,
                             item_master im
                       where rpc.price_change_id             = pc.price_change_id
                         and rpc.change_type                 = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                         and rpc.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and NOT EXISTS (select 1
                                           from rpm_item_zone_price rizp
                                          where rizp.item    = rpc.item
                                            and rizp.zone_id = rpc.zone_id)
                         and im.item                         = rpc.item
                         and (im.tran_level - im.item_level) = 1
                         and rpc.diff_id                     is NULL
                         and pc.price_change_type            = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE)
              where ranking = 1);

BEGIN

   open C_PC;
   fetch C_PC BULK COLLECT into L_rpm_item_pricing_tbl;
   close C_PC;

   if L_rpm_item_pricing_tbl.COUNT > 0 then

      for i IN 1..L_rpm_item_pricing_tbl.COUNT LOOP

         L_item_pricing_rec := L_rpm_item_pricing_tbl(i);

         if L_item_pricing_rec.need_uom_convert = 'Y' then

            if RPM_WRAPPER.UOM_CONVERT_VALUE(O_error_message,
                                             L_converted_retail_value,
                                             L_item_pricing_rec.item,
                                             L_item_pricing_rec.selling_retail,
                                             L_item_pricing_rec.std_uom,                --- to this UOM
                                             L_item_pricing_rec.selling_uom) = 0 then   --- from this UOM
               return FALSE;
            end if;

            L_item_pricing_rec.std_retail := L_converted_retail_value;

         end if;
      end loop;

      merge /*+ INDEX(rizp RPM_ITEM_ZONE_PRICE_I1) */ into rpm_item_zone_price rizp
      using (select item,
                    zone,
                    selling_retail,
                    selling_retail_currency,
                    selling_uom,
                    std_retail,
                    std_retail_currency,
                    std_uom,
                    multi_units,
                    multi_unit_retail,
                    multi_unit_retail_currency,
                    multi_selling_uom,
                    lock_version
               from table(cast(L_rpm_item_pricing_tbl as OBJ_ITEM_ZONE_PRICE_TBL))) pc
      on (    rizp.item    = pc.item
          and rizp.zone_id = pc.zone)
      when MATCHED then
         update
            set selling_retail             = pc.selling_retail,
                selling_retail_currency    = pc.selling_retail_currency,
                selling_uom                = pc.selling_uom,
                standard_retail            = pc.std_retail,
                standard_retail_currency   = pc.std_retail_currency,
                standard_uom               = pc.std_uom,
                multi_units                = pc.multi_units,
                multi_unit_retail          = pc.multi_unit_retail,
                multi_unit_retail_currency = pc.multi_unit_retail_currency
      when NOT MATCHED then
         insert (item_zone_price_id,
                 item,
                 zone_id,
                 standard_retail,
                 standard_retail_currency,
                 standard_uom,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 lock_version)
         values (RPM_ITEM_ZONE_PRICE_SEQ.NEXTVAL,
                 pc.item,
                 pc.zone,
                 pc.std_retail,
                 pc.std_retail_currency,
                 pc.std_uom,
                 pc.selling_retail,
                 pc.selling_retail_currency,
                 pc.selling_uom,
                 pc.multi_units,
                 pc.multi_unit_retail,
                 pc.multi_unit_retail_currency,
                 pc.multi_selling_uom,
                 pc.lock_version);

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;

END UPDATE_ITEM_ZONE_PRICE;

----------------------------------------------------------------

-- Populates RPM_ITEMLOC_THREAD based on current contents of
-- RPM_EVENT_ITEMLOC
FUNCTION INITIALIZE_ITEMLOC_THREADS(O_error_msg              OUT VARCHAR2,
                                    O_max_thread_num         OUT NUMBER,
                                    I_min_items_in_thread IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.INITIALIZE_ITEMLOC_THREADS';
   L_luw NUMBER := 0;

BEGIN

   delete from rpm_itemloc_thread;

   L_luw := I_min_items_in_thread;

   -- Guard against divide by zero...
   if L_luw is NULL or
      L_luw = 0 then
      ---
      L_luw := 1;
   end if;

   ---------------------------
   -- This insert generates thread number for each item/location in RPM_EVENT_ITEMLOC.
   -- Rules:
   -- o  The number of rows in rpm_itemloc_thread should equal that of rpm_event_itemloc.
   -- o  For a given thread number, the number of unique item/locations associated <= LUW.
   -- o  All thread numbers for multiple rows associated with 1 item/location should be the same.
   ---------------------------

   insert into rpm_itemloc_thread (item,
                                   loc,
                                   thread_num,
                                   price_event_type,
                                   price_event_id)
      (select t.item,
              t.loc,
              ceil(t.totalbyil / l_luw) as thread_num,
              t.orig_event_type,
              t.price_event_id
         from (select ilcount.item,
                      ilcount.loc,
                      ilcount.orig_event_type,
                      ilcount.price_event_id,
                      SUM(ilcount.total) OVER (ORDER BY ilcount.item,
                                                        ilcount.loc) totalbyil  -- reports sum of same item/loc instances
                 from (select /*+ PARALLEL(ev, 16) */
                              distinct item,
                              loc,
                              orig_event_type,
                              price_event_id,
                              1 as total
                         from rpm_event_itemloc ev) ilcount) t);

   select MAX(thread_num)
     into o_max_thread_num
     from rpm_itemloc_thread;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END;
-----------------------------------------------------------------------------------------

-- Explodes price event item locs and processes them in RMS.
FUNCTION UPDATE_RMS(O_error_message         OUT VARCHAR2,
                    I_pc_tbl             IN     OBJ_PRICE_CHANGE_TBL,
                    I_change_event_state IN     BOOLEAN DEFAULT FALSE)
RETURN NUMBER
AS
BEGIN

   RETURN EXECUTE_EVENT_FULL_PROCESS(O_error_message,
                                     I_pc_tbl,
                                     NULL,
                                     I_change_event_state);

END UPDATE_RMS;

-----------------------------------------------------------------------------------------
/**
 * Process the item/location for the price event in RMS.
 **/
FUNCTION UPDATE_RMS(O_error_message         OUT VARCHAR2,
                    I_pc_il_tbl          IN     OBJ_PRICE_CHANGE_IL_TBL,
                    I_change_event_state IN     BOOLEAN DEFAULT FALSE)
RETURN NUMBER
AS
BEGIN

   RETURN EXECUTE_EVENT_FULL_PROCESS(O_error_message,
                                     NULL,
                                     I_pc_il_tbl,
                                     I_change_event_state);

END UPDATE_RMS;

-----------------------------------------------------------------------------------------
/**
 * EXECUTES price events fully.   Option to change the price events' state
 * accordingly is provided.
 **/
FUNCTION EXECUTE_EVENT_FULL_PROCESS(O_error_message         OUT VARCHAR2,
                                    I_pc_tbl             IN     OBJ_PRICE_CHANGE_TBL,
                                    I_pc_il_tbl          IN     OBJ_PRICE_CHANGE_IL_TBL,
                                    I_change_event_state IN     BOOLEAN)
RETURN NUMBER
AS
   L_pc_tbl        OBJ_PRICE_CHANGE_TBL;
   L_pc_il_tbl     OBJ_PRICE_CHANGE_IL_TBL;
   L_pc_rec        OBJ_PRICE_CHANGE_REC;
   L_threads       OBJ_NUMERIC_ID_TABLE;
   L_thread_id     NUMBER;
--
   L_effective_date  DATE := GET_VDATE + 1;

   CURSOR C_PC_TBL IS
   select OBJ_PRICE_CHANGE_REC(price_change_id,
                               price_change_type)
     from (select distinct
                  price_change_id,
                  price_change_type
             from table(cast(L_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) il);
--
BEGIN
     -- Create a thread number value...
     SELECT RPM_ITEMLOC_THREAD_SEQ.NEXTVAL INTO L_thread_id FROM dual;

     if I_pc_tbl is not NULL and
        I_pc_tbl.count > 0 then

        L_pc_tbl := I_pc_tbl;

        if POPULATE_PE_THREAD_EXEC(O_error_message,
                                   L_pc_tbl,
                                   L_effective_date) = 0 then
           return 0;
        end if;

      -- Explode Price Changes, Clearances and Simple Promotions

        if EXPLODE_PRICE_CHANGES(O_error_message) = 0 then
           return 0;
        end if;

        if EXPLODE_CLEARANCES(O_error_message) = 0 then
           return 0;
        end if;

        if EXPLODE_SIMPLE_PROMO(O_error_message) = 0 then
           return 0;
        end if;

        -- Execute price events.   Expect RPM_EVENT_ITEMLOC to be populated by affected
        -- item locations.
        IF EXECUTE_PRICE_CHANGES(O_error_message, L_pc_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

        IF EXECUTE_CLEARANCES(O_error_message, L_pc_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

        IF EXECUTE_PROMOS(O_error_message, L_pc_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

        IF EXECUTE_AFFECTED_PROMOS(O_error_message, L_pc_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

     end if;

     if I_pc_il_tbl is not NULL and
        I_pc_il_tbl.count > 0 then

        L_pc_il_tbl := I_pc_il_tbl;

      -- Execute price events.   Expect RPM_EVENT_ITEMLOC to be populated by affected
        -- item locations.
        IF EXECUTE_PRICE_CHANGES(O_error_message, L_pc_il_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

        IF EXECUTE_CLEARANCES(O_error_message, L_pc_il_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

        IF EXECUTE_PROMOS(O_error_message, L_pc_il_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;

        IF EXECUTE_AFFECTED_PROMOS(O_error_message, L_pc_il_tbl, I_change_event_state) = FALSE THEN
           return 0;
        END IF;
        --
        open C_PC_TBL;
        fetch C_PC_TBL bulk collect into L_pc_tbl;
        close C_PC_TBL;
        --
     end if;

     -- Assign threads for each item-loc in RPM_EVENT_ITEMLOC into RPM_ITEMLOC_THREAD
     -- only for the particular input price events...
     INSERT INTO RPM_ITEMLOC_THREAD
        (item,
         loc,
         thread_num,
         price_event_type,
         price_event_id)
     SELECT /*+ CARDINALITY(PE, 10) */ DISTINCT
            il.ITEM,
            il.LOC,
            L_thread_id,
            il.orig_event_type,
            il.price_event_id
       FROM RPM_EVENT_ITEMLOC il,
            TABLE(CAST(L_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pe
      WHERE il.orig_event_type = pe.price_change_type
        AND il.price_event_id = pe.price_change_id;

     -- Process item locs for the thread (thread tied to specific events)
     L_threads := new OBJ_NUMERIC_ID_TABLE(L_thread_id);

     IF RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_LUWS_IN_RMS(O_error_message,
                                                         L_threads) = 0 THEN
        return 0;
     END IF;

     -- Process deals for specific events only!
     IF RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_DEALS(O_error_message, L_pc_tbl) = 0 THEN
        return 0;
     END IF;

     return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := O_error_message || Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_EVENT_SQL.EXECUTE_EVENT_FULL_PROCESS',
                                        TO_CHAR(SQLCODE));
      return 0;


END EXECUTE_EVENT_FULL_PROCESS;

--------------------------------------------------------------------------------
FUNCTION EXPLODE_PRICE_CHANGES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.EXPLODE_PRICE_CHANGES';

BEGIN

   --Instrumentation
	 BEGIN
	    RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to Explode Price Changes');

	  
	 EXCEPTION

      when OTHERS then
         goto EXP_PC_1;

   END;

   <<EXP_PC_1>>	
   
   insert into rpm_price_event_thread_item(pe_thread_id,
                                           price_event_id,
                                           price_event_type,
                                           item,
                                           thread_number)
      select LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item              = rpet.item
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      select 
             LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent       = rpet.item
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      select 
             LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent       = rpet.item
         and (   im.diff_1        = rpet.diff_id
              or im.diff_2        = rpet.diff_id
              or im.diff_3        = rpet.diff_id
              or im.diff_4        = rpet.diff_id)
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      select
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_link_code_attribute rlca,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rlca.link_code       = rpet.link_code
         and im.item              = rlca.item
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Item lists - tran level items
      select
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_price_change_skulist pcsl,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and pcsl.price_event_id  = rpet.price_event_id
         and pcsl.item_level      = RPM_CONSTANTS.IL_ITEM_LEVEL
         and pcsl.item            = im.item
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level
      union all
      -- Item lists - parent items
      select 
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_price_change_skulist pcsl,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and pcsl.price_event_id  = rpet.price_event_id
         and pcsl.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item            = im.item_parent
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level
      union all
      -- Populate Price Event ItemList - parent item level
      select
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_merch_list_detail rmld,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id   = rpet.price_event_itemlist
         and rmld.merch_level     = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item            = im.item_parent
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Populate Price Event ItemList - item level
      select 
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_merch_list_detail rmld,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id   = rpet.price_event_itemlist
         and rmld.merch_level     = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item            = im.item
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Populate Item Parent if the PC is at Parent Level and Primary Zone and Fixed Price
      select LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             item_master im,
             rpm_price_change pc
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item              = rpet.item
         and im.item_level        = im.tran_level - 1
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and pc.price_change_id   = rpet.price_event_id
         and pc.change_type       = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
         and EXISTS (select 1
                       from rpm_merch_retail_def_expl merch,
                            rpm_zone rz
                      where merch.dept               = im.dept
                        and merch.class              = im.class
                        and merch.subclass           = im.subclass
                        and merch.regular_zone_group = rz.zone_group_id
                        and rz.zone_id               = rpet.zone_node_id)
      union all
      -- Item lists - parent items -- Populate Item Parent if the PC is at Parent Level and Primary Zone and Fixed Price
      select distinct LP_pc_pe_thread_id,
              rpet.price_event_id,
              rpet.price_event_type,
              im.item,
              -999
        from rpm_price_event_thread_exec rpet,
             rpm_price_change_skulist pcsl,
             item_master im,
             rpm_price_change pc
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pcsl.price_event_id  = rpet.price_event_id
         and pcsl.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item            = im.item
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level - 1
         and pc.price_change_id   = rpet.price_event_id
         and pc.change_type       = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
         and EXISTS (select 1
                       from rpm_merch_retail_def_expl merch,
                            rpm_zone rz
                      where merch.dept               = im.dept
                        and merch.class              = im.class
                        and merch.subclass           = im.subclass
                        and merch.regular_zone_group = rz.zone_group_id
                        and rz.zone_id               = rpet.zone_node_id)
      union all
      -- Price Event Item lists - parent items -- Populate Item Parent if the PC is at Parent Level and Primary Zone and Fixed Price
      select distinct LP_pc_pe_thread_id,
              rpet.price_event_id,
              rpet.price_event_type,
              im.item,
              -999
        from rpm_price_event_thread_exec rpet,
             rpm_merch_list_detail rmld,
             item_master im,
             rpm_price_change pc
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id   = rpet.price_event_itemlist
         and rmld.merch_level     = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item            = im.item
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level - 1
         and pc.price_change_id   = rpet.price_event_id
         and pc.change_type       = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
         and EXISTS (select 1
                       from rpm_merch_retail_def_expl merch,
                            rpm_zone rz
                      where merch.dept               = im.dept
                        and merch.class              = im.class
                        and merch.subclass           = im.subclass
                        and merch.regular_zone_group = rz.zone_group_id
                        and rz.zone_id               = rpet.zone_node_id)
      union all
      select
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_merch_list_detail mld,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.skulist         = mld.merch_list_id
         and mld.item             = im.item
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level
      union all
      -- SGE Item lists - parent items
      select 
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_merch_list_detail mld,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.skulist         = mld.merch_list_id
         and mld.item             = im.item_parent
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level
      union all
      -- SGE Item lists - Parent Item Diff
      select 
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             im.item,
             -999
        from rpm_price_event_thread_exec rpet,
             rpm_merch_list_detail mld,
             item_master im
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.skulist         = mld.merch_list_id
         and mld.item             = im.item_parent
         and (   im.diff_1        = mld.diff_id
              or im.diff_2        = mld.diff_id
              or im.diff_3        = mld.diff_id
              or im.diff_4        = mld.diff_id)
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level        = im.tran_level;

   insert into rpm_price_event_thread_loc(pe_thread_id,
                                          price_event_id,
                                          price_event_type,
                                          location)
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
              LP_pc_pe_thread_id,
              rpet.price_event_id,
              rpet.price_event_type,
              rpet.zone_node_id
         from rpm_price_event_thread_exec rpet
        where rpet.pe_thread_id     = LP_pc_pe_thread_id
          and rpet.merch_node_type != RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
          and rpet.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
      union all
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             rzl.location
        from rpm_price_event_thread_exec rpet,
             rpm_zone_location rzl
       where rpet.pe_thread_id     = LP_pc_pe_thread_id
         and rpet.merch_node_type != RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rpet.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id           = rpet.zone_node_id
      union all
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) HASH(rpet, rlca) */
             distinct LP_pc_pe_thread_id,
             rpet.price_event_id,
             rpet.price_event_type,
             rpet.zone_node_id
        from rpm_price_event_thread_exec rpet,
             rpm_link_code_attribute rlca
       where rpet.pe_thread_id    = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rpet.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rlca.link_code       = rpet.link_code
         and rlca.location        = rpet.zone_node_id
      union all
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) HASH(rpet, rlca, rzl) */
            distinct LP_pc_pe_thread_id,
            rpet.price_event_id,
            rpet.price_event_type,
            rlca.location
       from rpm_price_event_thread_exec rpet,
            rpm_link_code_attribute rlca,
            rpm_zone_location rzl
      where rpet.pe_thread_id     = LP_pc_pe_thread_id
         and rpet.merch_node_type = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
        and rpet.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
        and rlca.link_code        = rpet.link_code
        and rzl.zone_id           = rpet.zone_node_id
        and rlca.location         = rzl.location;
        
    --Instrumentation
	  BEGIN
	    RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to Explode Price Changes');

	  
	 EXCEPTION

      when OTHERS then
         goto EXP_PC_2;

   END;

   <<EXP_PC_2>>	

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := O_error_msg ||SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
      return 0;

END EXPLODE_PRICE_CHANGES;

--------------------------------------------------------------------------------
FUNCTION EXPLODE_CLEARANCES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS
   L_program  VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.EXPLODE_CLEARANCES';

BEGIN

   --Instrumentation
	 BEGIN
	  	RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to Explode Clearances');

	  
	 EXCEPTION

      when OTHERS then
         goto EXP_CL_1;

   END;

   <<EXP_CL_1>>	

   insert into rpm_price_event_thread_item(pe_thread_id,
                                           price_event_id,
                                           price_event_type,
                                           item,
                                           thread_number)
   select LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
      and im.item              = rpet.item
      and im.item_level        = im.tran_level
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   select 
          LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
      and im.item_parent       = rpet.item
      and im.item_level        = im.tran_level
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   select 
          LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
      and im.item_parent       = rpet.item
      and (   im.diff_1        = rpet.diff_id
           or im.diff_2        = rpet.diff_id
           or im.diff_3        = rpet.diff_id
           or im.diff_4        = rpet.diff_id)
      and im.item_level        = im.tran_level
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   -- Item lists - tran level items
   select
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_clearance_skulist csl,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and csl.price_event_id   = rpet.price_event_id
      and csl.item_level       = RPM_CONSTANTS.IL_ITEM_LEVEL
      and csl.item             = im.item
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level
   union all
   -- Item lists - parent items
   select 
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_clearance_skulist csl,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and csl.price_event_id   = rpet.price_event_id
      and csl.item_level       = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
      and csl.item             = im.item_parent
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level
   union all
   -- Price Event Itemlists - parent item
   select 
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_merch_list_detail rmld,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
      and rmld.merch_list_id   = rpet.price_event_itemlist
      and rmld.merch_level     = RPM_CONSTANTS.PARENT_MERCH_TYPE
      and rmld.item            = im.item_parent
      and im.item_level        = im.tran_level
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   -- Price Event Itemlists - item
   select
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_merch_list_detail rmld,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
      and rmld.merch_list_id   = rpet.price_event_itemlist
      and rmld.merch_level     = RPM_CONSTANTS.ITEM_MERCH_TYPE
      and rmld.item            = im.item
      and im.item_level        = im.tran_level
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   --SGE Item lists - tran level items
   select
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_merch_list_detail mld,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and mld.merch_list_id    = rpet.skulist
      and mld.item             = im.item
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level
   union all
   --SGE Item lists - parent items
   select
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_merch_list_detail mld,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and mld.merch_list_id    = rpet.skulist
      and mld.item             = im.item_parent
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level
   union all
   --SGE Item lists - parent Diff items
   select 
          distinct LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_merch_list_detail mld,
          item_master im
    where rpet.pe_thread_id    = LP_cl_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and mld.merch_list_id    = rpet.skulist
      and mld.item             = im.item_parent
      and (   im.diff_1        = mld.diff_id
           or im.diff_2        = mld.diff_id
           or im.diff_3        = mld.diff_id
           or im.diff_4        = mld.diff_id)
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level;

   insert into rpm_price_event_thread_loc(pe_thread_id,
                                          price_event_id,
                                          price_event_type,
                                          location)
   select LP_cl_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          case 
             when rpet.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
                rzl.location 
             else rpet.zone_node_id 
          end
     from (select price_event_id,
                  price_event_type,
                  zone_node_id,
                  zone_node_type
             from rpm_price_event_thread_exec
            where pe_thread_id = LP_cl_pe_thread_id) rpet,
          (select zone_id, 
                  location 
             from rpm_zone_location) rzl
    where rpet.zone_node_id = rzl.zone_id(+);

   --Instrumentation
	 BEGIN
	  	RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to Explode Clearances');
       EXCEPTION

      when OTHERS then
         goto EXP_CL_2;

   END;
 	 <<EXP_CL_2>>	

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := O_error_msg ||SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
      return 0;

END EXPLODE_CLEARANCES;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_SIMPLE_PROMO(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS
   L_program  VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.EXPLODE_SIMPLE_PROMO';

BEGIN
   --Instrumentation
	 BEGIN
	  	RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to Explode Simple Promotions');
	 EXCEPTION

      when OTHERS then
         goto EXP_SP_1;

   END;

   <<EXP_SP_1>>
   
   insert into rpm_price_event_thread_item(pe_thread_id,
                                           price_event_id,
                                           price_event_type,
                                           item,
                                           thread_number)
   --   Item Level
   select LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          rpm_promo_dtl_list_grp rpdlg,
          rpm_promo_dtl_list rpdl,
          rpm_promo_dtl_disc_ladder rpddl,
          rpm_promo_dtl_merch_node rpdm,
          item_master im
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
      and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
      and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
      and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
      and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
      and im.item                    = rpdm.item
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   --   Item Parent Level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          rpm_promo_dtl_list_grp rpdlg,
          rpm_promo_dtl_list rpdl,
          rpm_promo_dtl_disc_ladder rpddl,
          rpm_promo_dtl_merch_node rpdm,
          item_master im
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
      and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
      and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
      and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
      and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
      and im.item_parent             = rpdm.item
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   --   Parent Diff Level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          rpm_promo_dtl_list_grp rpdlg,
          rpm_promo_dtl_list rpdl,
          rpm_promo_dtl_disc_ladder rpddl,
          rpm_promo_dtl_merch_node rpdm,
          item_master im
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
      and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
      and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
      and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
      and rpdm.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
      and im.item_parent             = rpdm.item
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and (   im.diff_1              = rpdm.diff_id
           or im.diff_2              = rpdm.diff_id
           or im.diff_3              = rpdm.diff_id
           or im.diff_4              = rpdm.diff_id)
   union all
   --   Merch Hierarchy Level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          rpm_promo_dtl_list_grp rpdlg,
          rpm_promo_dtl_list rpdl,
          rpm_promo_dtl_disc_ladder rpddl,
          rpm_promo_dtl_merch_node rpdm,
          item_master im
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
      and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
      and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
      and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
      and rpdm.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
      and im.dept                    = rpdm.dept
      and im.class                   = NVL(rpdm.class, im.class)
      and im.subclass                = NVL(rpdm.subclass, im.subclass)
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   -- Item list - tran item level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          item_master im,
          rpm_promo_dtl_skulist pdsl
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and pdsl.item_level            = RPM_CONSTANTS.IL_ITEM_LEVEL
      and im.item                    = pdsl.item
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES      
	  and exists (select 1
                    from rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm
                   where rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdm.skulist               = pdsl.skulist
            	     and rpdm.promo_dtl_id          = pdsl.price_event_id)
   union all
   -- Item Lists - parent item level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          item_master im,
          rpm_promo_dtl_skulist pdsl
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and pdsl.item_level            = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
      and im.item_parent             = pdsl.item
      and im.item_level              = im.tran_level  
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES        
	  and exists (select 1
                    from rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm
                   where rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdm.skulist               = pdsl.skulist
            	     and rpdm.promo_dtl_id          = pdsl.price_event_id)
   union all
   -- Price Event ItemList - parent item
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          rpm_promo_dtl_list_grp rpdlg,
          rpm_promo_dtl_list rpdl,
          rpm_promo_dtl_disc_ladder rpddl,
          rpm_promo_dtl_merch_node rpdm,
          item_master im,
          rpm_merch_list_detail rmld
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
      and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
      and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
      and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
      and rpdm.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
      and rpdm.price_event_itemlist  = rmld.merch_list_id
      and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
      and rmld.item                  = im.item_parent
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   -- Price Event ItemList - tran item level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          rpm_promo_dtl rpd,
          rpm_promo_dtl_list_grp rpdlg,
          rpm_promo_dtl_list rpdl,
          rpm_promo_dtl_disc_ladder rpddl,
          rpm_promo_dtl_merch_node rpdm,
          item_master im,
          rpm_merch_list_detail rmld
    where rpet.pe_thread_id          = LP_sp_pe_thread_id
      and rpd.promo_dtl_id           = rpet.price_event_id
      and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
      and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
      and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
      and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
      and rpdm.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
      and rpdm.price_event_itemlist  = rmld.merch_list_id
      and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
      and rmld.item                  = im.item
      and im.item_level              = im.tran_level
      and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
   union all
   -- SGE Item list - tran item level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          item_master im,
          rpm_merch_list_detail mld
    where rpet.pe_thread_id    = LP_sp_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and rpet.skulist         = mld.merch_list_id
      and im.item              = mld.item
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level
   union all
   -- SGE Item Lists - parent item level
   select 
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          item_master im,
          rpm_merch_list_detail mld
    where rpet.pe_thread_id    = LP_sp_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and rpet.skulist         = mld.merch_list_id
      and im.item_parent       = mld.item
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level
   union all
   -- SGE Item Lists - parent item level
   select
          LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          im.item,
          -999
     from rpm_price_event_thread_exec rpet,
          item_master im,
          rpm_merch_list_detail mld
    where rpet.pe_thread_id    = LP_sp_pe_thread_id
      and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
      and rpet.skulist         = mld.merch_list_id
      and im.item_parent       = mld.item
      and (   im.diff_1        = mld.diff_id
           or im.diff_2        = mld.diff_id
           or im.diff_3        = mld.diff_id
           or im.diff_4        = mld.diff_id)
      and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      and im.item_level        = im.tran_level;

   insert into rpm_price_event_thread_loc(pe_thread_id,
                                          price_event_id,
                                          price_event_type,
                                          location)
   select LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          rpet.zone_node_id
     from rpm_price_event_thread_exec rpet
    where rpet.pe_thread_id    = LP_sp_pe_thread_id
      and rpet.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
   union all
   select /*+ hash(rpet,rzl) hash(rpet,rzl) */ LP_sp_pe_thread_id,
          rpet.price_event_id,
          rpet.price_event_type,
          rzl.location
     from rpm_price_event_thread_exec rpet,
          rpm_zone_location rzl
    where rpet.pe_thread_id   = LP_sp_pe_thread_id
      and rpet.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
      and rzl.zone_id         = rpet.zone_node_id;

   --Instrumentation
	 BEGIN
	  	RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to Explode Simple Promotions');
	 EXCEPTION

      when OTHERS then
         goto EXP_SP_2;

   END;

   <<EXP_SP_2>>
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := O_error_msg ||SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
      return 0;

END EXPLODE_SIMPLE_PROMO;
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg    OUT VARCHAR2,
                      I_pc_il_tbl IN     OBJ_PRICE_CHANGE_IL_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_PRICE_EVENT_SQL.POPULATE_GTT';

BEGIN

   delete rpm_price_inquiry_gtt;
   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;

   insert into rpm_price_inquiry_gtt (price_event_id,
                                      item,
                                      item_parent,
                                      diff_id,
                                      dept,
                                      location,
                                      fr_zone_node_type,
                                      fr_zone_id)
      select s.price_change_id,
             s.item,
             s.item_parent,
             s.diff_1,
             s.dept,
             s.location,
             loc.zone_node_type,
             s.zone_id
        from (select t.price_change_id,
                     t.item,
                     t.item_parent,
                     t.diff_1,
                     t.dept,
                     t.location,
                     rzl.zone_id,
                     ROW_NUMBER() OVER (PARTITION BY t.price_change_id,
                                                     t.item,
                                                     t.location
                                            ORDER BY NVL(rzl.zone_id, -9999999999) desc) rank
                from (select /*+ CARDINALITY (pc, 10) ORDERED */
                             pc.price_change_id,
                             pc.price_change_type,
                             pc.item,
                             im.item_parent,
                             im.dept,
                             case
                                when im.item_parent is NULL and
                                     im.item_level = im.tran_level then
                                   NULL
                                else
                                   im.diff_1
                             end diff_1,
                             pc.location,
                             rz.zone_id
                        from table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) pc,
                             item_master im,
                             (
						      select distinct * 
						        from RPM_MERCH_RETAIL_DEF_EXPL
						     ) mer,
                             rpm_zone rz
                       where im.item          = pc.item
                         and mer.dept         = im.dept
                         and mer.class        = im.class
                         and mer.subclass     = im.subclass
                         and rz.zone_group_id = mer.regular_zone_group
                         and rownum           > 0) t,
                     rpm_zone_location rzl
               where rzl.zone_id (+)  = t.zone_id
                 and rzl.location (+) = t.location) s,
             (select store loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE zone_node_type
                from store
              union all
              select wh loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                from wh) loc
       where s.rank  = 1
         and loc.loc = s.location;

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      zone_node_type,
                                      location,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      item_parent,
                                      diff_id,
                                      zone_id,
                                      cur_hier_level,
                                      max_hier_level)
      select s.price_event_id,
             s.future_retail_id,
             s.dept,
             s.class,
             s.subclass,
             s.item,
             s.zone_node_type,
             s.location,
             s.action_date,
             s.selling_retail,
             s.selling_retail_currency,
             s.selling_uom,
             s.multi_units,
             s.multi_unit_retail,
             s.multi_unit_retail_currency,
             s.multi_selling_uom,
             s.clear_retail,
             s.clear_retail_currency,
             s.clear_uom,
             s.simple_promo_retail,
             s.simple_promo_retail_currency,
             s.simple_promo_uom,
             s.price_change_id,
             s.price_change_display_id,
             s.pc_exception_parent_id,
             s.pc_change_type,
             s.pc_change_amount,
             s.pc_change_currency,
             s.pc_change_percent,
             s.pc_change_selling_uom,
             s.pc_null_multi_ind,
             s.pc_multi_units,
             s.pc_multi_unit_retail,
             s.pc_multi_unit_retail_currency,
             s.pc_multi_selling_uom,
             s.pc_price_guide_id,
             s.clearance_id,
             s.clearance_display_id,
             s.clear_mkdn_index,
             s.clear_start_ind,
             s.clear_change_type,
             s.clear_change_amount,
             s.clear_change_currency,
             s.clear_change_percent,
             s.clear_change_selling_uom,
             s.clear_price_guide_id,
             s.loc_move_from_zone_id,
             s.loc_move_to_zone_id,
             s.location_move_id,
             s.lock_version,
             s.on_simple_promo_ind,
             s.on_complex_promo_ind,
             s.item_parent,
             s.diff_id,
             s.zone_id,
             s.cur_hier_level,
             s.max_hier_level
        from (with c_pc as ( select /*+ cardinality(PC , 100 )  */ * from  table(cast(I_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) PC )
              select z.price_event_id,
                     z.future_retail_id,
                     z.dept,
                     z.class,
                     z.subclass,
                     z.t_item item,
                     z.t_zone_node_type zone_node_type,
                     z.t_location location,
                     z.action_date,
                     z.selling_retail,
                     z.selling_retail_currency,
                     z.selling_uom,
                     z.multi_units,
                     z.multi_unit_retail,
                     z.multi_unit_retail_currency,
                     z.multi_selling_uom,
                     z.clear_retail,
                     z.clear_retail_currency,
                     z.clear_uom,
                     z.simple_promo_retail,
                     z.simple_promo_retail_currency,
                     z.simple_promo_uom,
                     z.price_change_id,
                     z.price_change_display_id,
                     z.pc_exception_parent_id,
                     z.pc_change_type,
                     z.pc_change_amount,
                     z.pc_change_currency,
                     z.pc_change_percent,
                     z.pc_change_selling_uom,
                     z.pc_null_multi_ind,
                     z.pc_multi_units,
                     z.pc_multi_unit_retail,
                     z.pc_multi_unit_retail_currency,
                     z.pc_multi_selling_uom,
                     z.pc_price_guide_id,
                     z.clearance_id,
                     z.clearance_display_id,
                     z.clear_mkdn_index,
                     z.clear_start_ind,
                     z.clear_change_type,
                     z.clear_change_amount,
                     z.clear_change_currency,
                     z.clear_change_percent,
                     z.clear_change_selling_uom,
                     z.clear_price_guide_id,
                     z.loc_move_from_zone_id,
                     z.loc_move_to_zone_id,
                     z.location_move_id,
                     z.lock_version,
                     z.on_simple_promo_ind,
                     z.on_complex_promo_ind,
                     z.t_item_parent item_parent,
                     z.t_diff_id diff_id,
                     z.t_zone_id zone_id,
                     RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                     z.max_hier_level,
                     ROW_NUMBER() OVER (PARTITION BY z.price_event_id,
                                                     z.t_item,
                                                     z.t_location,
                                                     z.action_date
                                            ORDER BY DECODE(z.cur_hier_level,
                                                            RPM_CONSTANTS.FR_HIER_ITEM_LOC, 1,
                                                            RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 2,
                                                            RPM_CONSTANTS.FR_HIER_DIFF_LOC, 3,
                                                            RPM_CONSTANTS.FR_HIER_PARENT_LOC, 4,
                                                            RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 5,
                                                            RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 8)) rank
                from (-- Curr. Hier Level - Tran_Item_Loc
                      select 
                             t.price_event_id,
                             t.item t_item,
                             t.fr_zone_node_type t_zone_node_type,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             fr.future_retail_id,
                             fr.item,
                             fr.dept,
                             fr.class,
                             fr.subclass,
                             fr.zone_node_type,
                             fr.location,
                             fr.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clear_exception_parent_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             fr.simple_promo_retail,
                             fr.simple_promo_retail_currency,
                             fr.simple_promo_uom,
                             fr.on_simple_promo_ind,
                             fr.on_complex_promo_ind,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             fr.pc_exception_parent_id,
                             fr.pc_change_type,
                             fr.pc_change_amount,
                             fr.pc_change_currency,
                             fr.pc_change_percent,
                             fr.pc_change_selling_uom,
                             fr.pc_null_multi_ind,
                             fr.pc_multi_units,
                             fr.pc_multi_unit_retail,
                             fr.pc_multi_unit_retail_currency,
                             fr.pc_multi_selling_uom,
                             fr.pc_price_guide_id,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_mkdn_index,
                             fr.clear_start_ind,
                             fr.clear_change_type,
                             fr.clear_change_amount,
                             fr.clear_change_currency,
                             fr.clear_change_percent,
                             fr.clear_change_selling_uom,
                             fr.clear_price_guide_id,
                             fr.loc_move_from_zone_id,
                             fr.loc_move_to_zone_id,
                             fr.location_move_id,
                             fr.lock_version,
                             fr.item_parent,
                             fr.diff_id,
                             fr.zone_id,
                             fr.max_hier_level,
                             fr.cur_hier_level
                        from c_pc pc,
                             rpm_price_inquiry_gtt t,
                             rpm_future_retail fr
                       where pc.item            = t.item
                         and pc.location        = t.location
                         and pc.price_change_id = t.price_event_id
                         and fr.dept            = t.dept
                         and fr.item            = t.item
                         and fr.location        = t.location
                         and fr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                         and (   (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and fr.price_change_id   = t.price_event_id)
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and fr.clearance_id      = t.price_event_id
                                  and fr.clear_start_ind   IN (RPM_CONSTANTS.START_IND,
                                                               RPM_CONSTANTS.START_END_IND))
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                  and fr.clear_start_ind   = RPM_CONSTANTS.END_IND)
                              or pc.price_change_type      IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                               RPM_CONSTANTS.PE_TYPE_PROMO_END))
                      union all
                      -- Curr. Hier Level - Tran_Item_Zone
                      select 
                             t.price_event_id,
                             t.item t_item,
                             t.fr_zone_node_type t_zone_node_type,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             fr.future_retail_id,
                             fr.item,
                             fr.dept,
                             fr.class,
                             fr.subclass,
                             fr.zone_node_type,
                             fr.location,
                             fr.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clear_exception_parent_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             fr.simple_promo_retail,
                             fr.simple_promo_retail_currency,
                             fr.simple_promo_uom,
                             fr.on_simple_promo_ind,
                             fr.on_complex_promo_ind,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             fr.pc_exception_parent_id,
                             fr.pc_change_type,
                             fr.pc_change_amount,
                             fr.pc_change_currency,
                             fr.pc_change_percent,
                             fr.pc_change_selling_uom,
                             fr.pc_null_multi_ind,
                             fr.pc_multi_units,
                             fr.pc_multi_unit_retail,
                             fr.pc_multi_unit_retail_currency,
                             fr.pc_multi_selling_uom,
                             fr.pc_price_guide_id,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_mkdn_index,
                             fr.clear_start_ind,
                             fr.clear_change_type,
                             fr.clear_change_amount,
                             fr.clear_change_currency,
                             fr.clear_change_percent,
                             fr.clear_change_selling_uom,
                             fr.clear_price_guide_id,
                             fr.loc_move_from_zone_id,
                             fr.loc_move_to_zone_id,
                             fr.location_move_id,
                             fr.lock_version,
                             fr.item_parent,
                             fr.diff_id,
                             fr.zone_id,
                             fr.max_hier_level,
                             fr.cur_hier_level
                        from c_pc pc,
                             rpm_price_inquiry_gtt t,
                             rpm_future_retail fr
                       where pc.item            = t.item
                         and pc.location        = t.location
                         and pc.price_change_id = t.price_event_id
                         and fr.dept            = t.dept
                         and fr.item            = t.item
                         and fr.location        = t.fr_zone_id
                         and fr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                         and (   (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and fr.price_change_id   = t.price_event_id)
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and fr.clearance_id      = t.price_event_id
                                  and fr.clear_start_ind   IN (RPM_CONSTANTS.START_IND,
                                                               RPM_CONSTANTS.START_END_IND))
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                  and fr.clear_start_ind   = RPM_CONSTANTS.END_IND)
                              or pc.price_change_type      IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                               RPM_CONSTANTS.PE_TYPE_PROMO_END))
                      union all
                      -- Curr. Hier Level - Parent_Diff_Loc
                      select 
                             t.price_event_id,
                             t.item t_item,
                             t.fr_zone_node_type t_zone_node_type,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             fr.future_retail_id,
                             fr.item,
                             fr.dept,
                             fr.class,
                             fr.subclass,
                             fr.zone_node_type,
                             fr.location,
                             fr.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clear_exception_parent_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             fr.simple_promo_retail,
                             fr.simple_promo_retail_currency,
                             fr.simple_promo_uom,
                             fr.on_simple_promo_ind,
                             fr.on_complex_promo_ind,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             fr.pc_exception_parent_id,
                             fr.pc_change_type,
                             fr.pc_change_amount,
                             fr.pc_change_currency,
                             fr.pc_change_percent,
                             fr.pc_change_selling_uom,
                             fr.pc_null_multi_ind,
                             fr.pc_multi_units,
                             fr.pc_multi_unit_retail,
                             fr.pc_multi_unit_retail_currency,
                             fr.pc_multi_selling_uom,
                             fr.pc_price_guide_id,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_mkdn_index,
                             fr.clear_start_ind,
                             fr.clear_change_type,
                             fr.clear_change_amount,
                             fr.clear_change_currency,
                             fr.clear_change_percent,
                             fr.clear_change_selling_uom,
                             fr.clear_price_guide_id,
                             fr.loc_move_from_zone_id,
                             fr.loc_move_to_zone_id,
                             fr.location_move_id,
                             fr.lock_version,
                             fr.item_parent,
                             fr.diff_id,
                             fr.zone_id,
                             fr.max_hier_level,
                             fr.cur_hier_level
                        from c_pc pc,
                             rpm_price_inquiry_gtt t,
                             rpm_future_retail fr
                       where pc.item            = t.item
                         and pc.location        = t.location
                         and pc.price_change_id = t.price_event_id
                         and fr.dept            = t.dept
                         and fr.item            = t.item_parent
                         and fr.diff_id         = t.diff_id
                         and fr.location        = t.location
                         and fr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                         and (   (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and fr.price_change_id   = t.price_event_id)
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and fr.clearance_id      = t.price_event_id
                                  and fr.clear_start_ind   IN (RPM_CONSTANTS.START_IND,
                                                               RPM_CONSTANTS.START_END_IND))
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                  and fr.clear_start_ind   = RPM_CONSTANTS.END_IND)
                              or pc.price_change_type      IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                               RPM_CONSTANTS.PE_TYPE_PROMO_END))
                      union all
                      -- Curr. Hier Level - Parent_Loc
                      select 
                             t.price_event_id,
                             t.item t_item,
                             t.fr_zone_node_type t_zone_node_type,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             fr.future_retail_id,
                             fr.item,
                             fr.dept,
                             fr.class,
                             fr.subclass,
                             fr.zone_node_type,
                             fr.location,
                             fr.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clear_exception_parent_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             fr.simple_promo_retail,
                             fr.simple_promo_retail_currency,
                             fr.simple_promo_uom,
                             fr.on_simple_promo_ind,
                             fr.on_complex_promo_ind,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             fr.pc_exception_parent_id,
                             fr.pc_change_type,
                             fr.pc_change_amount,
                             fr.pc_change_currency,
                             fr.pc_change_percent,
                             fr.pc_change_selling_uom,
                             fr.pc_null_multi_ind,
                             fr.pc_multi_units,
                             fr.pc_multi_unit_retail,
                             fr.pc_multi_unit_retail_currency,
                             fr.pc_multi_selling_uom,
                             fr.pc_price_guide_id,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_mkdn_index,
                             fr.clear_start_ind,
                             fr.clear_change_type,
                             fr.clear_change_amount,
                             fr.clear_change_currency,
                             fr.clear_change_percent,
                             fr.clear_change_selling_uom,
                             fr.clear_price_guide_id,
                             fr.loc_move_from_zone_id,
                             fr.loc_move_to_zone_id,
                             fr.location_move_id,
                             fr.lock_version,
                             fr.item_parent,
                             fr.diff_id,
                             fr.zone_id,
                             fr.max_hier_level,
                             fr.cur_hier_level
                        from c_pc pc,
                             rpm_price_inquiry_gtt t,
                             rpm_future_retail fr
                       where pc.item            = t.item
                         and pc.location        = t.location
                         and pc.price_change_id = t.price_event_id
                         and fr.dept            = t.dept
                         and fr.item            = t.item_parent
                         and fr.location        = t.location
                         and fr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                         and (   (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and fr.price_change_id   = t.price_event_id)
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and fr.clearance_id      = t.price_event_id
                                  and fr.clear_start_ind   IN (RPM_CONSTANTS.START_IND,
                                                               RPM_CONSTANTS.START_END_IND))
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                  and fr.clear_start_ind   = RPM_CONSTANTS.END_IND)
                              or pc.price_change_type      IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                               RPM_CONSTANTS.PE_TYPE_PROMO_END))
                      union all
                      -- Curr. Hier Level - Parent_Diff_Zone
                      select 
                             t.price_event_id,
                             t.item t_item,
                             t.fr_zone_node_type t_zone_node_type,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             fr.future_retail_id,
                             fr.item,
                             fr.dept,
                             fr.class,
                             fr.subclass,
                             fr.zone_node_type,
                             fr.location,
                             fr.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clear_exception_parent_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             fr.simple_promo_retail,
                             fr.simple_promo_retail_currency,
                             fr.simple_promo_uom,
                             fr.on_simple_promo_ind,
                             fr.on_complex_promo_ind,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             fr.pc_exception_parent_id,
                             fr.pc_change_type,
                             fr.pc_change_amount,
                             fr.pc_change_currency,
                             fr.pc_change_percent,
                             fr.pc_change_selling_uom,
                             fr.pc_null_multi_ind,
                             fr.pc_multi_units,
                             fr.pc_multi_unit_retail,
                             fr.pc_multi_unit_retail_currency,
                             fr.pc_multi_selling_uom,
                             fr.pc_price_guide_id,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_mkdn_index,
                             fr.clear_start_ind,
                             fr.clear_change_type,
                             fr.clear_change_amount,
                             fr.clear_change_currency,
                             fr.clear_change_percent,
                             fr.clear_change_selling_uom,
                             fr.clear_price_guide_id,
                             fr.loc_move_from_zone_id,
                             fr.loc_move_to_zone_id,
                             fr.location_move_id,
                             fr.lock_version,
                             fr.item_parent,
                             fr.diff_id,
                             fr.zone_id,
                             fr.max_hier_level,
                             fr.cur_hier_level
                        from c_pc pc,
                             rpm_price_inquiry_gtt t,
                             rpm_future_retail fr
                       where pc.item            = t.item
                         and pc.location        = t.location
                         and pc.price_change_id = t.price_event_id
                         and fr.dept            = t.dept
                         and fr.item            = t.item_parent
                         and fr.diff_id         = t.diff_id
                         and fr.location        = t.fr_zone_id
                         and fr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and (   (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and fr.price_change_id   = t.price_event_id)
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and fr.clearance_id      = t.price_event_id
                                  and fr.clear_start_ind   IN (RPM_CONSTANTS.START_IND,
                                                               RPM_CONSTANTS.START_END_IND))
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                  and fr.clear_start_ind   = RPM_CONSTANTS.END_IND)
                              or pc.price_change_type      IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                               RPM_CONSTANTS.PE_TYPE_PROMO_END))
                      union all
                      -- Curr. Hier Level - Parent_Zone
                      select 
                             t.price_event_id,
                             t.item t_item,
                             t.fr_zone_node_type t_zone_node_type,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             fr.future_retail_id,
                             fr.item,
                             fr.dept,
                             fr.class,
                             fr.subclass,
                             fr.zone_node_type,
                             fr.location,
                             fr.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clear_exception_parent_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             fr.simple_promo_retail,
                             fr.simple_promo_retail_currency,
                             fr.simple_promo_uom,
                             fr.on_simple_promo_ind,
                             fr.on_complex_promo_ind,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             fr.pc_exception_parent_id,
                             fr.pc_change_type,
                             fr.pc_change_amount,
                             fr.pc_change_currency,
                             fr.pc_change_percent,
                             fr.pc_change_selling_uom,
                             fr.pc_null_multi_ind,
                             fr.pc_multi_units,
                             fr.pc_multi_unit_retail,
                             fr.pc_multi_unit_retail_currency,
                             fr.pc_multi_selling_uom,
                             fr.pc_price_guide_id,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_mkdn_index,
                             fr.clear_start_ind,
                             fr.clear_change_type,
                             fr.clear_change_amount,
                             fr.clear_change_currency,
                             fr.clear_change_percent,
                             fr.clear_change_selling_uom,
                             fr.clear_price_guide_id,
                             fr.loc_move_from_zone_id,
                             fr.loc_move_to_zone_id,
                             fr.location_move_id,
                             fr.lock_version,
                             fr.item_parent,
                             fr.diff_id,
                             fr.zone_id,
                             fr.max_hier_level,
                             fr.cur_hier_level
                        from c_pc pc,
                             rpm_price_inquiry_gtt t,
                             rpm_future_retail fr
                       where pc.item            = t.item
                         and pc.location        = t.location
                         and pc.price_change_id = t.price_event_id
                         and fr.dept            = t.dept
                         and fr.item            = t.item_parent
                         and fr.location        = t.fr_zone_id
                         and fr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                         and (   (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and fr.price_change_id   = t.price_event_id)
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and fr.clearance_id      = t.price_event_id
                                  and fr.clear_start_ind   IN (RPM_CONSTANTS.START_IND,
                                                               RPM_CONSTANTS.START_END_IND))
                              or (    pc.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                  and fr.clear_start_ind   = RPM_CONSTANTS.END_IND)
                              or pc.price_change_type      IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                               RPM_CONSTANTS.PE_TYPE_PROMO_END))) z) s
       where s.rank = 1;

   insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                            promo_item_loc_expl_id,
                                            item,
                                            dept,
                                            class,
                                            subclass,
                                            location,
                                            promo_id,
                                            promo_display_id,
                                            promo_secondary_ind,
                                            promo_comp_id,
                                            comp_display_id,
                                            promo_dtl_id,
                                            type,
                                            customer_type,
                                            detail_secondary_ind,
                                            detail_start_date,
                                            detail_end_date,
                                            detail_apply_to_code,
                                            detail_change_type,
                                            detail_change_amount,
                                            detail_change_currency,
                                            detail_change_percent,
                                            detail_change_selling_uom,
                                            detail_price_guide_id,
                                            exception_parent_id,
                                            zone_node_type,
                                            item_parent,
                                            diff_id,
                                            zone_id,
                                            max_hier_level,
                                            cur_hier_level,
                                            timebased_dtl_ind)
      select s.price_event_id,
             s.promo_item_loc_expl_id,
             s.item,
             s.dept,
             s.class,
             s.subclass,
             s.location,
             s.promo_id,
             s.promo_display_id,
             s.promo_secondary_ind,
             s.promo_comp_id,
             s.comp_display_id,
             s.promo_dtl_id,
             s.type,
             s.customer_type,
             s.detail_secondary_ind,
             s.detail_start_date,
             s.detail_end_date,
             s.detail_apply_to_code,
             s.detail_change_type,
             s.detail_change_amount,
             s.detail_change_currency,
             s.detail_change_percent,
             s.detail_change_selling_uom,
             s.detail_price_guide_id,
             s.exception_parent_id,
             s.zone_node_type,
             s.item_parent,
             s.diff_id,
             s.zone_id,
             s.max_hier_level,
             s.cur_hier_level,
             s.timebased_dtl_ind
        from (select distinct
                     price_event_id,
                     promo_item_loc_expl_id,
                     t_item item,
                     dept,
                     class,
                     subclass,
                     t_location location,
                     promo_id,
                     promo_display_id,
                     promo_secondary_ind,
                     promo_comp_id,
                     comp_display_id,
                     promo_dtl_id,
                     type,
                     customer_type,
                     detail_secondary_ind,
                     detail_start_date,
                     detail_end_date,
                     detail_apply_to_code,
                     detail_change_type,
                     detail_change_amount,
                     detail_change_currency,
                     detail_change_percent,
                     detail_change_selling_uom,
                     detail_price_guide_id,
                     exception_parent_id,
                     t_zone_node_type zone_node_type,
                     t_item_parent item_parent,
                     t_diff_id diff_id,
                     t_zone_id zone_id,
                     max_hier_level,
                     RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                     timebased_dtl_ind,
                     ROW_NUMBER() OVER (PARTITION BY price_event_id,
                                                     t_item,
                                                     t_location,
                                                     detail_start_date
                                            ORDER BY DECODE(cur_hier_level,
                                                            RPM_CONSTANTS.FR_HIER_ITEM_LOC, 1,
                                                            RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 2,
                                                            RPM_CONSTANTS.FR_HIER_DIFF_LOC, 3,
                                                            RPM_CONSTANTS.FR_HIER_PARENT_LOC, 4,
                                                            RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 5,
                                                            RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 8)) rank
                from (-- Curr. Hier Level - Tran_Item_Loc
                      select t.price_event_id,
                             t.item t_item,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             t.fr_zone_node_type t_zone_node_type,
                             rpile.promo_item_loc_expl_id,
                             rpile.item,
                             rpile.dept,
                             rpile.class,
                             rpile.subclass,
                             rpile.location,
                             rpile.promo_id,
                             rpile.promo_display_id,
                             rpile.promo_secondary_ind,
                             rpile.promo_comp_id,
                             rpile.comp_display_id,
                             rpile.promo_dtl_id,
                             rpile.type,
                             rpile.customer_type,
                             rpile.detail_secondary_ind,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.detail_apply_to_code,
                             rpile.detail_change_type,
                             rpile.detail_change_amount,
                             rpile.detail_change_currency,
                             rpile.detail_change_percent,
                             rpile.detail_change_selling_uom,
                             rpile.detail_price_guide_id,
                             rpile.exception_parent_id,
                             rpile.zone_node_type,
                             rpile.zone_id,
                             rpile.item_parent,
                             rpile.diff_id,
                             rpile.max_hier_level,
                             rpile.cur_hier_level,
                             rpile.timebased_dtl_ind
                        from rpm_price_inquiry_gtt t,
                             rpm_promo_item_loc_expl rpile
                       where rpile.dept           = t.dept
                         and rpile.item           = t.item
                         and rpile.location       = t.location
                         and rpile.CUSTOMER_TYPE is NULL
                         and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                      union all
                      -- Curr. Hier Level - Tran_Item_Zone
                      select t.price_event_id,
                             t.item t_item,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             t.fr_zone_node_type t_zone_node_type,
                             rpile.promo_item_loc_expl_id,
                             rpile.item,
                             rpile.dept,
                             rpile.class,
                             rpile.subclass,
                             rpile.location,
                             rpile.promo_id,
                             rpile.promo_display_id,
                             rpile.promo_secondary_ind,
                             rpile.promo_comp_id,
                             rpile.comp_display_id,
                             rpile.promo_dtl_id,
                             rpile.type,
                             rpile.customer_type,
                             rpile.detail_secondary_ind,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.detail_apply_to_code,
                             rpile.detail_change_type,
                             rpile.detail_change_amount,
                             rpile.detail_change_currency,
                             rpile.detail_change_percent,
                             rpile.detail_change_selling_uom,
                             rpile.detail_price_guide_id,
                             rpile.exception_parent_id,
                             rpile.zone_node_type,
                             rpile.zone_id,
                             rpile.item_parent,
                             rpile.diff_id,
                             rpile.max_hier_level,
                             rpile.cur_hier_level,
                             rpile.timebased_dtl_ind
                        from rpm_price_inquiry_gtt t,
                             rpm_promo_item_loc_expl rpile
                       where rpile.dept           = t.dept
                         and rpile.item           = t.item
                         and rpile.location       = t.fr_zone_id
                         and rpile.CUSTOMER_TYPE is NULL
                         and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      union all
                      -- Curr. Hier Level - Parent_Diff_Loc
                      select t.price_event_id,
                             t.item t_item,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             t.fr_zone_node_type t_zone_node_type,
                             rpile.promo_item_loc_expl_id,
                             rpile.item,
                             rpile.dept,
                             rpile.class,
                             rpile.subclass,
                             rpile.location,
                             rpile.promo_id,
                             rpile.promo_display_id,
                             rpile.promo_secondary_ind,
                             rpile.promo_comp_id,
                             rpile.comp_display_id,
                             rpile.promo_dtl_id,
                             rpile.type,
                             rpile.customer_type,
                             rpile.detail_secondary_ind,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.detail_apply_to_code,
                             rpile.detail_change_type,
                             rpile.detail_change_amount,
                             rpile.detail_change_currency,
                             rpile.detail_change_percent,
                             rpile.detail_change_selling_uom,
                             rpile.detail_price_guide_id,
                             rpile.exception_parent_id,
                             rpile.zone_node_type,
                             rpile.zone_id,
                             rpile.item_parent,
                             rpile.diff_id,
                             rpile.max_hier_level,
                             rpile.cur_hier_level,
                             rpile.timebased_dtl_ind
                        from rpm_price_inquiry_gtt t,
                             rpm_promo_item_loc_expl rpile
                       where rpile.dept           = t.dept
                         and rpile.item           = t.item_parent
                         and rpile.diff_id        = t.diff_id
                         and rpile.location       = t.location
                         and rpile.CUSTOMER_TYPE is NULL
                         and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                      union all
                      -- Curr. Hier Level - Parent_Loc
                      select t.price_event_id,
                             t.item t_item,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             t.fr_zone_node_type t_zone_node_type,
                             rpile.promo_item_loc_expl_id,
                             rpile.item,
                             rpile.dept,
                             rpile.class,
                             rpile.subclass,
                             rpile.location,
                             rpile.promo_id,
                             rpile.promo_display_id,
                             rpile.promo_secondary_ind,
                             rpile.promo_comp_id,
                             rpile.comp_display_id,
                             rpile.promo_dtl_id,
                             rpile.type,
                             rpile.customer_type,
                             rpile.detail_secondary_ind,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.detail_apply_to_code,
                             rpile.detail_change_type,
                             rpile.detail_change_amount,
                             rpile.detail_change_currency,
                             rpile.detail_change_percent,
                             rpile.detail_change_selling_uom,
                             rpile.detail_price_guide_id,
                             rpile.exception_parent_id,
                             rpile.zone_node_type,
                             rpile.zone_id,
                             rpile.item_parent,
                             rpile.diff_id,
                             rpile.max_hier_level,
                             rpile.cur_hier_level,
                             rpile.timebased_dtl_ind
                        from rpm_price_inquiry_gtt t,
                             rpm_promo_item_loc_expl rpile
                       where rpile.dept           = t.dept
                         and rpile.item           = t.item_parent
                         and rpile.location       = t.location
                         and rpile.CUSTOMER_TYPE is NULL
                         and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      union all
                      -- Curr. Hier Level - Parent_Diff_Zone
                      select t.price_event_id,
                             t.item t_item,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             t.fr_zone_node_type t_zone_node_type,
                             rpile.promo_item_loc_expl_id,
                             rpile.item,
                             rpile.dept,
                             rpile.class,
                             rpile.subclass,
                             rpile.location,
                             rpile.promo_id,
                             rpile.promo_display_id,
                             rpile.promo_secondary_ind,
                             rpile.promo_comp_id,
                             rpile.comp_display_id,
                             rpile.promo_dtl_id,
                             rpile.type,
                             rpile.customer_type,
                             rpile.detail_secondary_ind,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.detail_apply_to_code,
                             rpile.detail_change_type,
                             rpile.detail_change_amount,
                             rpile.detail_change_currency,
                             rpile.detail_change_percent,
                             rpile.detail_change_selling_uom,
                             rpile.detail_price_guide_id,
                             rpile.exception_parent_id,
                             rpile.zone_node_type,
                             rpile.zone_id,
                             rpile.item_parent,
                             rpile.diff_id,
                             rpile.max_hier_level,
                             rpile.cur_hier_level,
                             rpile.timebased_dtl_ind
                        from rpm_price_inquiry_gtt t,
                             rpm_promo_item_loc_expl rpile
                       where rpile.dept           = t.dept
                         and rpile.item           = t.item_parent
                         and rpile.diff_id        = t.diff_id
                         and rpile.location       = t.fr_zone_id
                         and rpile.CUSTOMER_TYPE is NULL
                         and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                      union all
                      -- Curr. Hier Level - Parent_Zone
                      select t.price_event_id,
                             t.item t_item,
                             t.location t_location,
                             t.item_parent t_item_parent,
                             t.diff_id t_diff_id,
                             t.fr_zone_id t_zone_id,
                             t.fr_zone_node_type t_zone_node_type,
                             rpile.promo_item_loc_expl_id,
                             rpile.item,
                             rpile.dept,
                             rpile.class,
                             rpile.subclass,
                             rpile.location,
                             rpile.promo_id,
                             rpile.promo_display_id,
                             rpile.promo_secondary_ind,
                             rpile.promo_comp_id,
                             rpile.comp_display_id,
                             rpile.promo_dtl_id,
                             rpile.type,
                             rpile.customer_type,
                             rpile.detail_secondary_ind,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.detail_apply_to_code,
                             rpile.detail_change_type,
                             rpile.detail_change_amount,
                             rpile.detail_change_currency,
                             rpile.detail_change_percent,
                             rpile.detail_change_selling_uom,
                             rpile.detail_price_guide_id,
                             rpile.exception_parent_id,
                             rpile.zone_node_type,
                             rpile.zone_id,
                             rpile.item_parent,
                             rpile.diff_id,
                             rpile.max_hier_level,
                             rpile.cur_hier_level,
                             rpile.timebased_dtl_ind
                        from rpm_price_inquiry_gtt t,
                             rpm_promo_item_loc_expl rpile
                       where rpile.dept           = t.dept
                         and rpile.item           = t.item_parent
                         and rpile.location       = t.fr_zone_id
                         and rpile.CUSTOMER_TYPE is NULL
                         and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE) z) s
       where s.rank = 1;

    return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END POPULATE_GTT;
--------------------------------------------------------------------------------
FUNCTION POPULATE_PE_THREAD_EXEC(O_error_msg         OUT VARCHAR2,
                                 I_pc_tbl         IN     OBJ_PRICE_CHANGE_TBL,
                                 I_effective_date IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.POPULATE_PE_THREAD_EXEC';

   L_truncate_pe_thread_exec   VARCHAR2(100) := 'truncate table rpm_price_event_thread_exec';
   L_truncate_pe_thread_status VARCHAR2(100) := 'truncate table rpm_price_event_thread_status';
   L_truncate_pe_thread_item   VARCHAR2(100) := 'truncate table rpm_price_event_thread_item';
   L_truncate_pe_thread_loc    VARCHAR2(100) := 'truncate table rpm_price_event_thread_loc';

BEGIN

   --Instrumentation
   BEGIN
   
      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to Populate PriceEvent threads');

   EXCEPTION

      when OTHERS then
         goto PPTE_1;

   END;

   <<PPTE_1>>
   
   -- These inserts populate the rpm_price_event_thread_exec with all price events that need to be
   -- executed. While doing that, it also populates the merchandise node and zone node that will be used
   -- for the explosion so that the explosion function does not need to access the price event tables again.
   -- As for Price Change, Clearance and Simple Promotion threading will be done at item/location,
   -- the thread_number for this will be just be populated with 1. The actual threading will be done in
   -- RPM_PRICE_EVENT_THREAD_ITEM table

   EXECUTE IMMEDIATE L_truncate_pe_thread_exec;
   EXECUTE IMMEDIATE L_truncate_pe_thread_status;
   EXECUTE IMMEDIATE L_truncate_pe_thread_item;
   EXECUTE IMMEDIATE L_truncate_pe_thread_loc;

   if I_pc_tbl is NOT NULL and
      I_pc_tbl.COUNT > 0 then

      -- Get Price Changes
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              merch_node_type,
                                              item,
                                              diff_id,
                                              link_code,
                                              skulist,
                                              zone_node_type,
                                              zone_node_id,
                                              price_event_itemlist)
         select /*+ INDEX(rpc, rpm_price_change_I7) */ LP_pc_pe_thread_id,
                rpc.price_change_id,
                RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                case
                   when link_code is NOT NULL then
                      RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                   when diff_id is NOT NULL then
                      RPM_CONSTANTS.PARENT_ITEM_DIFF
                   when skulist is NOT NULL then
                      RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   when price_event_itemlist is NOT NULL then
                      RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   else
                      RPM_CONSTANTS.ITEM_LEVEL_ITEM
                end, -- merch_node_type
                item,
                diff_id,
                link_code,
                skulist,
                zone_node_type,
                DECODE(zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, zone_id,
                       location),
                price_event_itemlist
           from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                rpm_price_change rpc
          where pc.price_change_type     = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
            and rpc.price_change_id      = pc.price_change_id
            and (    LP_emergency_ind    = 1
                 or (    rpc.state       = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                     and effective_date <= I_effective_date + 1 - RPM_CONSTANTS.ONE_SECOND));

      -- Get Clearances and Clearance Resets
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              merch_node_type,
                                              item,
                                              diff_id,
                                              skulist,
                                              zone_node_type,
                                              zone_node_id,
                                              price_event_itemlist)
         select /*+ INDEX(rc, rpm_clearance_I9) */ LP_cl_pe_thread_id,
                clearance_id,
                RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                case
                   when diff_id is NOT NULL then
                      RPM_CONSTANTS.PARENT_ITEM_DIFF
                   when skulist is NOT NULL then
                      RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   when price_event_itemlist is NOT NULL then
                      RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   else
                      RPM_CONSTANTS.ITEM_LEVEL_ITEM
                end, -- merch_node_type
                item,
                diff_id,
                skulist,
                zone_node_type,
                DECODE(zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, zone_id,
                       location),
                price_event_itemlist
           from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                rpm_clearance
          where pc.price_change_type            = RPM_CONSTANTS.PE_TYPE_CLEARANCE
            and clearance_id                    = pc.price_change_id
            and (    LP_emergency_ind           = 1
                 or (    state                  = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                     and TRUNC(effective_date) <= I_effective_date + 1))
         union all
         select LP_cl_pe_thread_id,
                clearance_id,
                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET price_event_type,
                RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                item,
                NULL,
                NULL,
                zone_node_type,
                location,
                NULL
           from rpm_clearance_reset
          where state                  = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and TRUNC(effective_date) <= I_effective_date;

      -- Get Simple Promotion
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              merch_node_type,
                                              item,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              skulist,
                                              zone_node_type,
                                              zone_node_id,
                                              price_event_itemlist)
         select LP_sp_pe_thread_id,
                rpd.promo_dtl_id,
                case
                   when LP_emergency_ind = 1 or
                        rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      ---
                      RPM_CONSTANTS.PE_TYPE_PROMO_START
                   else
                      RPM_CONSTANTS.PE_TYPE_PROMO_END
                end price_event_type,
                rpdm.merch_type,
                rpdm.item,
                rpdm.diff_id,
                rpdm.dept,
                rpdm.class,
                rpdm.subclass,
                rpdm.skulist,
                rpzl.zone_node_type,
                DECODE(rpzl.zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpzl.zone_id,
                       rpzl.location),
                rpdm.price_event_itemlist
           from table(cast(I_pc_tbl as OBJ_PRICE_CHANGE_TBL)) pc,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                rpm_promo_dtl_merch_node rpdm,
                rpm_promo_zone_location rpzl
          where rpd.promo_dtl_id                    =  pc.price_change_id
            and pc.price_change_type                IN(RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                       RPM_CONSTANTS.PE_TYPE_PROMO_END)
            and (   LP_emergency_ind = 1
                 or (   (    rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                         and TRUNC(rpd.start_date) <= I_effective_date)
                     or (    rpd.state              = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                         and TRUNC(rpd.end_date)   <= I_effective_date - 1)
                     or (    rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                         and TRUNC(rpd.end_date)   <= I_effective_date - 1)))
            and rpc.promo_comp_id                   = rpd.promo_comp_id
            and rpc.type                            = RPM_CONSTANTS.SIMPLE_CODE
            and rpdm.promo_dtl_id                   = rpd.promo_dtl_id
            and rpzl.promo_dtl_id                   = rpd.promo_dtl_id;

   else

      -- Get Price Changes
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              merch_node_type,
                                              item,
                                              diff_id,
                                              link_code,
                                              skulist,
                                              zone_node_type,
                                              zone_node_id,
                                              price_event_itemlist)
         select /*+ INDEX(rpc, rpm_price_change_I7) */ LP_pc_pe_thread_id,
                price_change_id,
                RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                case
                   when link_code is NOT NULL then
                      RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                   when diff_id is NOT NULL then
                      RPM_CONSTANTS.PARENT_ITEM_DIFF
                   when skulist is NOT NULL then
                      RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   when price_event_itemlist is NOT NULL then
                      RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   else
                      RPM_CONSTANTS.ITEM_LEVEL_ITEM
                end,  -- merch_node_type
                item,
                diff_id,
                link_code,
                skulist,
                zone_node_type,
                DECODE(zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, zone_id,
                       location),
                price_event_itemlist
           from rpm_price_change
          where state           = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and effective_date <= I_effective_date + 1 - RPM_CONSTANTS.ONE_SECOND
         union all
         select LP_pc_pe_thread_id,
                price_change_id,
                RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                case
                   when link_code is NOT NULL then
                      RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                   when diff_id is NOT NULL then
                      RPM_CONSTANTS.PARENT_ITEM_DIFF
                   when skulist is NOT NULL then
                      RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   when price_event_itemlist is NOT NULL then
                      RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   else
                      RPM_CONSTANTS.ITEM_LEVEL_ITEM
                end,  -- merch_node_type
                item,
                diff_id,
                link_code,
                skulist,
                zone_node_type,
                DECODE(zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, zone_id,
                       location),
                price_event_itemlist
           from rpm_chunk_cc_stage_pee rccsp,
                rpm_price_change rpc
          where rccsp.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
            and rccsp.process_id       is NULL
            and rccsp.price_event_id   = rpc.price_change_id;

      -- Get Clearances and Clearance Resets
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              merch_node_type,
                                              item,
                                              diff_id,
                                              skulist,
                                              zone_node_type,
                                              zone_node_id,
                                              price_event_itemlist)
         select /*+ INDEX(rc, RPM_CLEARANCE_I9) */ LP_cl_pe_thread_id,
                clearance_id,
                RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                case
                   when diff_id is NOT NULL then
                      RPM_CONSTANTS.PARENT_ITEM_DIFF
                   when skulist is NOT NULL then
                      RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   when price_event_itemlist is NOT NULL then
                      RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   else
                      RPM_CONSTANTS.ITEM_LEVEL_ITEM
                end,  -- merch_node_type
                item,
                diff_id,
                skulist,
                zone_node_type,
                DECODE(zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, zone_id,
                       location),
                price_event_itemlist
           from rpm_clearance
          where state                  = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and TRUNC(effective_date) <= I_effective_date
         union all
         select LP_cl_pe_thread_id,
                clearance_id,
                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET price_event_type,
                RPM_CONSTANTS.ITEM_LEVEL_ITEM,  -- merch_node_type
                item,
                NULL,
                NULL,
                zone_node_type,
                location,
                NULL
           from rpm_clearance_reset
          where state           = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and effective_date  <= I_effective_date + 1 - RPM_CONSTANTS.ONE_SECOND
         union all
         select LP_cl_pe_thread_id,
                clearance_id,
                RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                case
                   when diff_id is NOT NULL then
                      RPM_CONSTANTS.PARENT_ITEM_DIFF
                   when skulist is NOT NULL then
                      RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   when price_event_itemlist is NOT NULL then
                      RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   else
                      RPM_CONSTANTS.ITEM_LEVEL_ITEM
                end,  -- merch_node_type
                item,
                diff_id,
                skulist,
                zone_node_type,
                DECODE(zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, zone_id,
                       location),
                price_event_itemlist
           from rpm_chunk_cc_stage_pee rccsp,
                rpm_clearance rc
          where rccsp.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
            and rccsp.process_id       is NULL
            and rccsp.price_event_id   = rc.clearance_id;

      -- Get Simple Promotion
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              merch_node_type,
                                              item,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              skulist,
                                              zone_node_type,
                                              zone_node_id,
                                              price_event_itemlist)
         select LP_sp_pe_thread_id,
                rpd.promo_dtl_id,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      RPM_CONSTANTS.PE_TYPE_PROMO_START
                   else
                      RPM_CONSTANTS.PE_TYPE_PROMO_END
                end price_event_type,
                rpdm.merch_type,
                rpdm.item,
                rpdm.diff_id,
                rpdm.dept,
                rpdm.class,
                rpdm.subclass,
                rpdm.skulist,
                rpzl.zone_node_type,
                DECODE(rpzl.zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpzl.zone_id,
                       rpzl.location),
                rpdm.price_event_itemlist
           from rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                rpm_promo_dtl_merch_node rpdm,
                rpm_promo_zone_location rpzl
          where (   (    rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                     and TRUNC(rpd.start_date) <= I_effective_date)
                 or (    rpd.state              = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1)
                 or (    rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1))
            and rpc.promo_comp_id               = rpd.promo_comp_id
            and rpc.type                        = RPM_CONSTANTS.SIMPLE_CODE
            and rpdm.promo_dtl_id               = rpd.promo_dtl_id
            and rpzl.promo_dtl_id               = rpd.promo_dtl_id
         union all
         select LP_sp_pe_thread_id,
                rpd.promo_dtl_id,
                RPM_CONSTANTS.PE_TYPE_PROMO_START,
                rpdm.merch_type,
                rpdm.item,
                rpdm.diff_id,
                rpdm.dept,
                rpdm.class,
                rpdm.subclass,
                rpdm.skulist,
                rpzl.zone_node_type,
                DECODE(rpzl.zone_node_type,
                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpzl.zone_id,
                       rpzl.location),
                rpdm.price_event_itemlist
           from rpm_chunk_cc_stage_pee rccsp,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                rpm_promo_dtl_merch_node rpdm,
                rpm_promo_zone_location rpzl
          where rccsp.price_event_type = RPM_CONSTANTS.PE_TYPE_PROMO_START
            and rccsp.process_id       is NULL
            and rccsp.price_event_id   = rpd.promo_dtl_id
            and rpc.promo_comp_id      = rpd.promo_comp_id
            and rpc.type               = RPM_CONSTANTS.SIMPLE_CODE
            and rpdm.promo_dtl_id      = rpd.promo_dtl_id
            and rpzl.promo_dtl_id      = rpd.promo_dtl_id;

      -- Get Threshold Promotion
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type)
         select LP_ot_pe_thread_id,
                rpd.promo_dtl_id,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_START
                   else
                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_END
                end price_event_type
           from rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where (   (    rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                     and TRUNC(rpd.start_date) <= I_effective_date)
                 or (    rpd.state              = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1)
                 or (    rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1))
            and rpc.promo_comp_id               = rpd.promo_comp_id
            and rpc.type                        = RPM_CONSTANTS.THRESHOLD_CODE;

      -- Get Finance Promotion
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type)
         select LP_ot_pe_thread_id,
                rpd.promo_dtl_id,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      RPM_CONSTANTS.PE_TYPE_FINANCE_START
                   else
                      RPM_CONSTANTS.PE_TYPE_FINANCE_END
                end price_event_type
           from rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where (   (    rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                     and TRUNC(rpd.start_date) <= I_effective_date)
                 or (    rpd.state              = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1)
                 or (    rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1))
            and rpc.promo_comp_id               = rpd.promo_comp_id
            and rpc.type                        = RPM_CONSTANTS.FINANCE_CODE;

      -- Get Complex Promotion
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type)
         select LP_ot_pe_thread_id,
                rpd.promo_dtl_id,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      RPM_CONSTANTS.PE_TYPE_BUYGET_START
                   else
                      RPM_CONSTANTS.PE_TYPE_BUYGET_END
                end price_event_type
           from rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where (   (    rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                     and TRUNC(rpd.start_date) <= I_effective_date)
                 or (    rpd.state              = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                     and TRUNC (rpd.end_date)  <= I_effective_date - 1)
                 or (    rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                     and TRUNC(rpd.end_date)   <= I_effective_date - 1))
            and rpc.promo_comp_id               = rpd.promo_comp_id
            and rpc.type                        = RPM_CONSTANTS.COMPLEX_CODE;

      -- Get Transaction Promotion
      insert into rpm_price_event_thread_exec(pe_thread_id,
                                              price_event_id,
                                              price_event_type)
         select LP_ot_pe_thread_id,
                rpd.promo_dtl_id,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_START
                   else
                      RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_END
                end price_event_type
           from rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where (   (    rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                     and TRUNC(rpd.start_date) <= I_effective_date)
                 or (    rpd.state              = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                     and TRUNC (rpd.end_date)  <= I_effective_date - 1)
                 or (    rpd.state              = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE
                     and TRUNC(rpd.end_date)   <= TRUNC(I_effective_date) - 1))
            and rpc.promo_comp_id               = rpd.promo_comp_id
            and rpc.type                        = RPM_CONSTANTS.TRANSACTION_CODE;

   end if;
   
   --Instrumentation
   BEGIN
   
      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to Populate PriceEvent threads');

   EXCEPTION

      when OTHERS then
         goto PPTE_2;

   END;

   <<PPTE_2>>

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := O_error_msg ||SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
      return 0;

END POPULATE_PE_THREAD_EXEC;
---------------------------------------------------------------------------------

PROCEDURE ANALYZE_STAGE_TABLES_INDICES(O_return_code      OUT NUMBER,
                                       O_error_message    OUT VARCHAR2,
                                       I_schema_owner  IN     VARCHAR2,
                                       I_stage_table   IN     VARCHAR2 DEFAULT 'ALL')
AS
  L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.ANALYZE_STAGE_TABLES_INDICES';
  L_index_rebuild VARCHAR2(1000) := NULL;
  L_table_analyze VARCHAR2(1000) := NULL;
  
BEGIN
    -- Analyze index first, then table.  Capture error if stats on table were locked
    FOR C_REC in (select INDEX_NAME,
                         PARTITION_NAME
                  from   USER_IND_PARTITIONS
                  where  INDEX_NAME in (select INDEX_NAME
                                        from   USER_INDEXES
                                        where TABLE_NAME IN ('RPM_EVENT_ITEMLOC','RPM_PRICE_EVENT_THREAD','RPM_ITEMLOC_THREAD')
                  and PARTITIONED = 'YES'
                  and ( I_stage_table = 'ALL'
                        OR TABLE_NAME = I_stage_table ))) LOOP
                      L_index_rebuild := 'alter index '|| C_REC.index_name || ' rebuild partition '|| C_REC.PARTITION_NAME;
        BEGIN
            EXECUTE IMMEDIATE L_index_rebuild;
        EXCEPTION
            WHEN OTHERS THEN
               O_error_message := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                                    TO_CHAR(SQLCODE));
        END;
    END LOOP;

    FOR C_REC in (select INDEX_NAME
                  from   USER_INDEXES
                  where  TABLE_NAME IN ('RPM_EVENT_ITEMLOC','RPM_PRICE_EVENT_THREAD','RPM_ITEMLOC_THREAD')
                  and PARTITIONED = 'NO'
                  and ( I_stage_table = 'ALL'
                        OR TABLE_NAME = I_stage_table )) LOOP
                      L_index_rebuild := 'alter index '|| C_REC.index_name|| ' rebuild ';
        BEGIN
            EXECUTE IMMEDIATE L_index_rebuild;
        EXCEPTION
            WHEN OTHERS THEN
               O_error_message := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                                    TO_CHAR(SQLCODE));
        END;
    END LOOP;

    FOR C_REC in (select DECODE(I_stage_table, 'RPM_PRICE_EVENT_THREAD',
                         'RPM_PRICE_EVENT_THREAD_EXEC',TABLE_NAME) TABLE_NAME
                  from   USER_TABLES
                  where  TABLE_NAME IN ('RPM_EVENT_ITEMLOC','RPM_PRICE_EVENT_THREAD','RPM_ITEMLOC_THREAD')
                  and PARTITIONED = 'NO'
                  and ( I_stage_table = 'ALL'
                        OR TABLE_NAME = I_stage_table )) LOOP
                      L_table_analyze := 'analyze table  '|| C_REC.table_name|| ' estimate statistics sample 10 percent';
        BEGIN
            EXECUTE IMMEDIATE L_table_analyze;
        EXCEPTION
            WHEN OTHERS THEN
              O_error_message := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                                    TO_CHAR(SQLCODE));
        END;
    END LOOP;

   O_return_code := 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_return_code := 0;

END ANALYZE_STAGE_TABLES_INDICES;

----------------------------------------------------------------------------
-- Populates rpm_price_event_thread_exec based on current contents of
-- Price Events

FUNCTION INITIALIZE_PRICEEVENT_THREADS(O_error_msg                     OUT VARCHAR2,
                                       O_max_thread_num                OUT NUMBER,
                                       I_min_itemloc_in_thread      IN     NUMBER,
                                       I_effective_date             IN     DATE,
                                       I_restart                    IN     NUMBER DEFAULT 0,
                                       I_emer_chunk_data_process_id IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.INITIALIZE_PRICEEVENT_THREADS';

   L_luw            NUMBER               := 0;
   L_max_thread_num NUMBER(10)           := 0;
   L_emergency_pe   OBJ_PRICE_CHANGE_TBL := NULL;
   LP_trace_name    VARCHAR2(10)	 			 := 'IPT';

   cursor C_MAX_THREAD is
      select NVL(MAX(thread_number), 0)
        from rpm_price_event_thread_item
       where thread_number > 0;

   cursor C_GET_EMER_CHUNK_DATA is
      select OBJ_PRICE_CHANGE_REC(price_event_id,
                                  price_event_type)
        from rpm_chunk_cc_stage_pee
       where process_id = I_emer_chunk_data_process_id;

BEGIN

   --Instrumentation
   BEGIN
   
	  		RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                            LP_error_msg,
                                            RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                            RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                            RPM_CONSTANTS.BATCH_NAME_PEE,
                                            L_program,
                                            RPM_CONSTANTS.LOG_STARTED,
                                           'Starting PriceEventExecutionBatch');

         RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_PEE||LP_trace_name);

   EXCEPTION

      when OTHERS then
         goto INIT_PE_THREADS_1;

   END;

   <<INIT_PE_THREADS_1>>

   if I_restart = 0 then

      L_luw := I_min_itemloc_in_thread;

      -- Guard against divide by zero
      if L_luw is NULL or
         L_luw = 0 then
         L_luw := 10000;
      end if;

      if I_emer_chunk_data_process_id is NOT NULL then

         LP_emergency_ind := 1;

         open C_GET_EMER_CHUNK_DATA;
         fetch C_GET_EMER_CHUNK_DATA BULK COLLECT into L_emergency_pe;
         close C_GET_EMER_CHUNK_DATA;

      end if;

      if POPULATE_PE_THREAD_EXEC(O_error_msg,
                                 L_emergency_pe,
                                 I_effective_date) = 0 then
         return 0;
      end if;

      -- Explode Price Changes, Clearances and Simple Promotions
      if EXPLODE_PRICE_CHANGES(O_error_msg) = 0 then
         return 0;
      end if;

      if EXPLODE_CLEARANCES(O_error_msg) = 0 then
         return 0;
      end if;

      if EXPLODE_SIMPLE_PROMO(O_error_msg) = 0 then
         return 0;
      end if;

      -- Threading the Item/Location for Price Change
      insert into rpm_price_event_thread_item(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              item,
                                              thread_number)
         select pe_thread_id,
                price_event_id,
                price_event_type,
                item,
                CEIL((row_num * loc_count)/L_luw)
           from (select it.pe_thread_id,
                        it.price_event_id,
                        it.price_event_type,
                        it.item,
                        DENSE_RANK() OVER (ORDER BY it.pe_thread_id,
                                                    it.price_event_id,
                                                    it.item) row_num,
                        case
                           when NVL(loc.loc_count, 1) > L_luw then
                              L_luw
                           else
                              NVL(loc.loc_count, 1)
                        end as loc_count
                   from rpm_price_event_thread_item it,
                        (select pe_thread_id,
                                price_event_id,
                                COUNT(price_event_id) loc_count
                           from rpm_price_event_thread_loc
                          where pe_thread_id = LP_pc_pe_thread_id
                          group by pe_thread_id,
                                   price_event_id) loc
                  where it.thread_number       = -999
                    and it.pe_thread_id        = LP_pc_pe_thread_id
                    and loc.pe_thread_id (+)   = it.pe_thread_id
                    and loc.price_event_id (+) = it.price_event_id);

      open C_MAX_THREAD;
      fetch C_MAX_THREAD into L_max_thread_num;
      close C_MAX_THREAD;

      if L_max_thread_num is NULL then
         L_max_thread_num := 0;
      end if;

      -- Threading the Item/Location for Clearance
      insert into rpm_price_event_thread_item(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              item,
                                              thread_number)
         select pe_thread_id,
                price_event_id,
                price_event_type,
                item,
                CEIL((row_num * loc_count)/L_luw) + L_max_thread_num
           from (select it.pe_thread_id,
                        it.price_event_id,
                        it.price_event_type,
                        it.item,
                        DENSE_RANK() OVER (ORDER BY it.pe_thread_id,
                                                    it.price_event_id,
                                                    it.item) row_num,
                        case
                           when loc.loc_count > L_luw then
                              L_luw
                           else
                              loc.loc_count
                        end loc_count
                   from rpm_price_event_thread_item it,
                        (select pe_thread_id,
                                price_event_id,
                                COUNT(price_event_id) loc_count
                           from rpm_price_event_thread_loc
                          where pe_thread_id = LP_cl_pe_thread_id
                          group by pe_thread_id,
                                   price_event_id) loc
                  where it.thread_number   = -999
                    and it.pe_thread_id    = LP_cl_pe_thread_id
                    and loc.pe_thread_id   = it.pe_thread_id
                    and loc.price_event_id = it.price_event_id);

      open C_MAX_THREAD;
      fetch C_MAX_THREAD into L_max_thread_num;
      close C_MAX_THREAD;

      if L_max_thread_num is NULL then
         L_max_thread_num := 0;
      end if;

      -- Threading the Item/Location for Simple Promotion
      insert into rpm_price_event_thread_item(pe_thread_id,
                                              price_event_id,
                                              price_event_type,
                                              item,
                                              thread_number)
         select pe_thread_id,
                price_event_id,
                price_event_type,
                item,
                CEIL((row_num * loc_count)/L_luw) + L_max_thread_num
           from (select it.pe_thread_id,
                        it.price_event_id,
                        it.price_event_type,
                        it.item,
                        DENSE_RANK() OVER (ORDER BY it.pe_thread_id,
                                                    it.price_event_id,
                                                    it.item) row_num,
                        case
                           when loc.loc_count > L_luw then
                              L_luw
                           else
                              loc.loc_count
                        end as loc_count
                   from rpm_price_event_thread_item it,
                        (select pe_thread_id,
                                price_event_id,
                                COUNT(price_event_id) loc_count
                           from rpm_price_event_thread_loc
                          where pe_thread_id = LP_sp_pe_thread_id
                          group by pe_thread_id,
                                   price_event_id) loc
                  where it.thread_number   = -999
                    and it.pe_thread_id    = LP_sp_pe_thread_id
                    and loc.pe_thread_id   = it.pe_thread_id
                    and loc.price_event_id = it.price_event_id);

      insert into rpm_price_event_thread_status(pe_thread_id,
                                                price_event_id,
                                                price_event_type,
                                                thread_number,
                                                status)
         select distinct pe_thread_id,
                price_event_id,
                price_event_type,
                thread_number,
                'N'
           from rpm_price_event_thread_item
          where pe_thread_id  IN (LP_pc_pe_thread_id,
                                  LP_cl_pe_thread_id,
                                  LP_sp_pe_thread_id)
            and thread_number > 0;

   end if;

   merge into rpm_promo_dtl target
   using (select case
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_START then
                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_BUYGET_START then
                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_START then
                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_FINANCE_START then
                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_END and
                         rpd.state != RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_COMPLETE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_END and
                         rpd.state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_CANCELLED_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_FINANCE_END and
                         rpd.state != RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_COMPLETE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_FINANCE_END and
                         rpd.state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_CANCELLED_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_BUYGET_END and
                         rpd.state != RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_COMPLETE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_BUYGET_END and
                         rpd.state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_CANCELLED_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_END and
                         rpd.state != RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_COMPLETE_STATE_CODE
                    when pc.price_change_type = RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_END and
                         rpd.state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_CANCELLED_STATE_CODE
                    else
                       rpd.state
                 end state,
                 rpd.promo_dtl_id
            from rpm_promo_dtl rpd,
                 (select distinct price_event_id price_change_id,
                         price_event_type price_change_type
                    from rpm_price_event_thread_exec stat,
                         rpm_promo_dtl rpd
                   where stat.pe_thread_id = LP_ot_pe_thread_id
                     and rpd.promo_dtl_id  = stat.price_event_id
                     and 1 = (case
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_START and
                                      rpd.state != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_START and
                                      rpd.state != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_BUYGET_START and
                                      rpd.state != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_START and
                                      rpd.state != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_END and
                                      rpd.state NOT IN (RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                        RPM_CONSTANTS.PR_CANCELLED_STATE_CODE) then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_END and
                                      rpd.state NOT IN (RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                        RPM_CONSTANTS.PR_CANCELLED_STATE_CODE) then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_BUYGET_END and
                                      rpd.state NOT IN (RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                        RPM_CONSTANTS.PR_CANCELLED_STATE_CODE) then
                                    1
                                 when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_TRAN_PROMO_END and
                                      rpd.state NOT IN (RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                        RPM_CONSTANTS.PR_CANCELLED_STATE_CODE) then
                                    1
                                 else
                                    0
                                 end)
                     and rownum            > 0) pc
           where rpd.promo_dtl_id = pc.price_change_id
             and LP_emergency_ind = 0) source
   on (target.promo_dtl_id = source.promo_dtl_id)
   when MATCHED then
      update
         set target.state = source.state;

   open C_MAX_THREAD;
   fetch C_MAX_THREAD into O_max_thread_num;
   close C_MAX_THREAD;

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to Initialize PriceEvent threads');

   EXCEPTION

      when OTHERS then
         goto INIT_PE_THREADS_2;

   END;

   <<INIT_PE_THREADS_2>>
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END INITIALIZE_PRICEEVENT_THREADS;
-----------------------------------------------------------------------------------------

FUNCTION PROCESS_PRICE_EVENT_LUWS (O_error_message    OUT VARCHAR2,
                                   I_thd_luw_tbl   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRICE_EVENT_SQL.PROCESS_PRICE_EVENT_LUWS';

   L_thread_number    NUMBER                  := NULL;
   L_price_event_type VARCHAR2(3)             := NULL;
   L_pc_tbl           OBJ_PRICE_CHANGE_TBL    := NULL;
   L_pc_il_tbl        OBJ_PRICE_CHANGE_IL_TBL := NULL;
   L_cl_il_tbl        OBJ_PRICE_CHANGE_IL_TBL := NULL;
   L_sp_il_tbl        OBJ_PRICE_CHANGE_IL_TBL := NULL;
   LP_trace_name      VARCHAR2(50)            :='PEL';

   cursor C_GET_PE_TYPE is
      select price_event_type
        from rpm_price_event_thread_status stat
       where thread_number = L_thread_number;

   -- Get All Price Changes Item/Location on this Thread
   cursor C_GET_PC_IL is
      select OBJ_PRICE_CHANGE_IL_REC(price_event_id,
                                     price_event_type,
                                     item,
                                     location)
        from (with price_changes as
                (select /*+MATERIALIZE */
                        stat.price_event_id,
                        stat.price_event_type,
                        rpc.item,
                        rpc.diff_id,
                        rpc.link_code,
                        rpc.skulist,
                        rpc.price_event_itemlist,
                        rpc.location,
                        rpc.zone_id,
                        rpc.zone_node_type,
                        rpc.sys_generated_exclusion
                   from rpm_price_event_thread_status stat,
                        rpm_price_change rpc
                  where stat.thread_number    = L_thread_number
                    and stat.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                    and stat.status          != RPM_CONSTANTS.CC_STATUS_COMPLETED
                    and stat.price_event_id   = rpc.exception_parent_id
                    and rpc.state             IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                                  RPM_CONSTANTS.PC_PENDING_STATE_CODE)
                    and rownum                > 0)
              select stat.price_event_id,
                     stat.price_event_type,
                     item.item,
                     loc.location
                from rpm_price_event_thread_status stat,
                     rpm_price_event_thread_item item,
                     rpm_price_event_thread_loc loc
               where stat.thread_number    = L_thread_number
                 and stat.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and stat.status          != RPM_CONSTANTS.CC_STATUS_COMPLETED
                 and item.pe_thread_id     = stat.pe_thread_id
                 and item.price_event_id   = stat.price_event_id
                 and item.price_event_type = stat.price_event_type
                 and item.thread_number    = stat.thread_number
                 and loc.pe_thread_id      = stat.pe_thread_id
                 and loc.price_event_id    = stat.price_event_id
                 and loc.price_event_type  = stat.price_event_type
              -- Remove all the exceptions/exclusions
              minus
              (-- Item/Loc level
               select
                      rpc.price_event_id,
                      rpc.price_event_type,
                      rpc.item,
                      rpc.location
                 from price_changes rpc,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.item           = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Item/Zone level
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      rpc.item,
                      rzl.location
                 from price_changes rpc,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id        = rzl.zone_id
                  and rpc.item           = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Parent/Loc level
               union all
               select
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.item           = im.item_parent
                  and rpc.diff_id        is NULL
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Parent-Diff/Loc level
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.diff_id        is NOT NULL
                  and rpc.item           = im.item_parent
                  and rpc.diff_id        IN (im.diff_1,
                                             im.diff_2,
                                             im.diff_3,
                                             im.diff_4)
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- skulist/loc - parent items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_price_change_skulist rpcs,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 0
                  and rpc.skulist                         = rpcs.skulist
                  and rpcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                  and rpcs.item                           = im.item_parent
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- skulist/loc - tran level items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_price_change_skulist rpcs,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 0
                  and rpc.skulist                         = rpcs.skulist
                  and rpcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                  and rpcs.item                           = im.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- Price Event Item List (PEIL) Item Parent /loc
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.price_event_itemlist is NOT NULL
                  and rpc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                = im.item_parent
                  and im.item_level            = im.tran_level
                  and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                   > 0
               -- Price Event Item List (PEIL) Item /loc
               union all
               select
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.price_event_itemlist is NOT NULL
                  and rpc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                = im.item
                  and im.item_level            = im.tran_level
                  and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                   > 0
               -- Processing System Generated Exclusions Price Changes
               -- merchlist/zone - parent items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id                         = rzl.zone_id
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/zone - parent-diff items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id                         = rzl.zone_id
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.diff_1                           = rmld.diff_id
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/zone - tran level items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id                         = rzl.zone_id
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                           = im.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/loc - parent items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/loc - parent-diff items
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.diff_1                           = rmld.diff_id
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/loc - tran level items
               union all
               select
                      rpc.price_event_id,
                      rpc.price_event_type,
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                           = im.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- link code/zone
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      rlca.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_link_code_attribute rlca,
                      rpm_zone_location rzl,
                      item_master im
                where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id        = rzl.zone_id
                  and rpc.link_code      is NOT NULL
                  and rpc.link_code      = rlca.link_code
                  and rlca.location      = rzl.location
                  and rlca.loc_type      = rzl.loc_type
                  and rownum             > 0
               -- link code/loc
               union all
               select 
                      rpc.price_event_id,
                      rpc.price_event_type,
                      rlca.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_link_code_attribute rlca,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.link_code      is NOT NULL
                  and rpc.link_code      = rlca.link_code
                  and rlca.location      = rpc.location
                  and rownum             > 0));

   -- Get All CLearances Item/Location on this Thread
   cursor C_GET_CL_IL is
      select OBJ_PRICE_CHANGE_IL_REC(price_event_id,
                                     price_event_type,
                                     item,
                                     location)
        from (with clearances as
                (select /*+ MATERIALIZE */
                        stat.price_event_id,
                        stat.price_event_type,
                        rc.item,
                        rc.diff_id,
                        rc.skulist,
                        rc.price_event_itemlist,
                        rc.location,
                        rc.zone_id,
                        rc.zone_node_type,
                        rc.sys_generated_exclusion
                   from rpm_price_event_thread_status stat,
                        rpm_clearance rc
                  where stat.thread_number    = L_thread_number
                    and stat.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                  RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET)
                    and stat.status          != RPM_CONSTANTS.CC_STATUS_COMPLETED
                    and stat.price_event_id   = rc.exception_parent_id
                    and rc.state              IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                                  RPM_CONSTANTS.PC_PENDING_STATE_CODE)
                    and rownum                > 0)
              select
                     stat.price_event_id,
                     stat.price_event_type,
                     item.item,
                     loc.location
                from rpm_price_event_thread_status stat,
                     rpm_price_event_thread_item item,
                     rpm_price_event_thread_loc loc
               where stat.thread_number    = L_thread_number
                 and stat.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                               RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET)
                 and stat.status          != RPM_CONSTANTS.CC_STATUS_COMPLETED
                 and item.pe_thread_id     = stat.pe_thread_id
                 and item.price_event_id   = stat.price_event_id
                 and item.price_event_type = stat.price_event_type
                 and item.thread_number    = stat.thread_number
                 and loc.pe_thread_id      = stat.pe_thread_id
                 and loc.price_event_id    = stat.price_event_id
                 and loc.price_event_type  = stat.price_event_type
              -- Remove all the exceptions/exclusions
              minus
              (-- Item/Loc level
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      rc.item,
                      rc.location
                 from clearances rc,
                      item_master im
                where rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.item           = im.item
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               -- Item/Zone level
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      rc.item,
                      rzl.location
                 from clearances rc,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id        = rzl.zone_id
                  and rc.item           = im.item
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               -- Parent/Loc level
               union all
               select
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im
                where rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.item           = im.item_parent
                  and rc.diff_id        is NULL
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               -- Parent-Diff/Loc level
               union all
               select
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im
                where rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.diff_id        is NOT NULL
                  and rc.item           = im.item_parent
                  and rc.diff_id        IN (im.diff_1,
                                            im.diff_2,
                                            im.diff_3,
                                            im.diff_4)
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               -- skulist/loc - parent items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_clearance_skulist rcs,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 0
                  and rc.skulist                         = rcs.skulist
                  and rcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                  and rcs.item                           = im.item_parent
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- skulist/loc - tran level items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_clearance_skulist rcs,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 0
                  and rc.skulist                         = rcs.skulist
                  and rcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                  and rcs.item                           = im.item
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- Price Event Itemlist (PEIL) - parent item /loc level
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.price_event_itemlist is NOT NULL
                  and rc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item               = im.item_parent
                  and im.item_level           = im.tran_level
                  and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                  > 0
               -- Price Event Itemlist (PEIL) - item /loc level
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.price_event_itemlist is NOT NULL
                  and rc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item               = im.item
                  and im.item_level           = im.tran_level
                  and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                  > 0
               -- merchlist/zone - parent items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rzl.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id                         = rzl.zone_id
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/zone - parent-diff items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rzl.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id                         = rzl.zone_id
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.diff_1                          = rmld.diff_id
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/zone - tran level items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rzl.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id                         = rzl.zone_id
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                          = im.item
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/loc - parent items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im,
                      rpm_merch_list_detail rmld
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/loc - parent-diff items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im,
                      rpm_merch_list_detail rmld
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.diff_1                          = rmld.diff_id
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/loc - tran level items
               union all
               select 
                      rc.price_event_id,
                      rc.price_event_type,
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im,
                      rpm_merch_list_detail rmld
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                          = im.item
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0));

   -- Get All Simple Promotion Item/Location on this Thread
   cursor C_GET_SP_IL is
      select OBJ_PRICE_CHANGE_IL_REC(price_event_id,
                                     price_event_type,
                                     item,
                                     location)
        from (with promos as
                (select /*+  MATERIALIZE */
                        stat.price_event_id,
                        stat.price_event_type,
                        rpdmn.dept,
                        rpdmn.class,
                        rpdmn.subclass,
                        rpdmn.item,
                        rpdmn.diff_id,
                        rpdmn.skulist,
                        rpdmn.price_event_itemlist,
                        rpdmn.merch_type,
                        rpzl.location,
                        rpzl.zone_id,
                        rpzl.zone_node_type,
                        rpd.sys_generated_exclusion
                   from rpm_price_event_thread_status stat,
                        rpm_promo_dtl rpd,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_promo_zone_location rpzl
                  where stat.thread_number    = L_thread_number
                    and stat.price_event_type IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                                  RPM_CONSTANTS.PE_TYPE_PROMO_END)
                    and stat.status          != RPM_CONSTANTS.CC_STATUS_COMPLETED
                    and stat.price_event_id   = rpd.exception_parent_id
                    and rpd.state             IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                  RPM_CONSTANTS.PR_COMPLETE_STATE_CODE)
                    and rpd.promo_dtl_id      = rpdmn.promo_dtl_id
                    and rpd.promo_dtl_id      = rpzl.promo_dtl_id
                    and rownum                > 0)
              select stat.price_event_id,
                     stat.price_event_type,
                     item.item,
                     loc.location
                from rpm_price_event_thread_status stat,
                     rpm_price_event_thread_item item,
                     rpm_price_event_thread_loc loc
               where stat.thread_number    = L_thread_number
                 and stat.price_event_type IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                               RPM_CONSTANTS.PE_TYPE_PROMO_END)
                 and stat.status          != RPM_CONSTANTS.CC_STATUS_COMPLETED
                 and item.pe_thread_id     = stat.pe_thread_id
                 and item.price_event_id   = stat.price_event_id
                 and item.price_event_type = stat.price_event_type
                 and item.thread_number    = stat.thread_number
                 and loc.pe_thread_id      = stat.pe_thread_id
                 and loc.price_event_id    = stat.price_event_id
                 and loc.price_event_type  = stat.price_event_type
              minus
              (-- Item/Loc level
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      item_master im
                where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                  and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpd.item           = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Item/Zone
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rzl.location
                 from promos rpd,
                      item_master im,
                      rpm_zone_location rzl
                where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                  and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpd.zone_id        = rzl.zone_id
                  and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpd.item           = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Parent/Loc
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      item_master im
                where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                  and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpd.item           = im.item_parent
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Parent-Diff/Loc
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      item_master im
                where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                  and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpd.item           = im.item_parent
                  and rpd.diff_id        IN (im.diff_1,
                                             im.diff_2,
                                             im.diff_3,
                                             im.diff_4)
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Merch Hier/Loc
               union all
               select
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      item_master im
                where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                  and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and im.dept            = rpd.dept
                  and im.class           = NVL(rpd.class, im.class)
                  and im.subclass        = NVL(rpd.subclass, im.subclass)
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Skulist/Loc - parent items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_promo_dtl_skulist rpds,
                      item_master im
                where NVL(rpd.sys_generated_exclusion, 0) = 0
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpds.skulist                        = rpd.skulist
                  and rpds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                  and im.item_parent                      = rpds.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- Skulist/Loc - tran level items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_promo_dtl_skulist rpds,
                      item_master im
                where NVL(rpd.sys_generated_exclusion, 0) = 0
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpds.skulist                        = rpd.skulist
                  and rpds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                  and im.item                             = rpds.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- Price Event Itemlist (PEIL) parent item /Loc
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                  and rmld.merch_list_id = rpd.price_event_itemlist
                  and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item          = im.item_parent
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Price Event Itemlist (PEIL) tran item /Loc
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                  and rmld.merch_list_id = rpd.price_event_itemlist
                  and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item          = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Merchlist/Zone - parent items
               union all
               select
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rzl.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where NVL(rpd.sys_generated_exclusion, 0) = 1
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rmld.merch_list_id                  = rpd.skulist
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and im.item_parent                      = rmld.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rzl.zone_id                         = rpd.zone_id
                  and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rownum                              > 0
               -- Merchlist/Zone - parent diff items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rzl.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where NVL(rpd.sys_generated_exclusion, 0) = 1
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rmld.merch_list_id                  = rpd.skulist
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and im.item_parent                      = rmld.item
                  and im.diff_1                           = rmld.diff_id
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rzl.zone_id                         = rpd.zone_id
                  and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rownum                              > 0
               -- Merchlist/Zone - tran level items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rzl.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where NVL(rpd.sys_generated_exclusion, 0) = 1
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rmld.merch_list_id                  = rpd.skulist
                  and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and im.item                             = rmld.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rzl.zone_id                         = rpd.zone_id
                  and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rownum                              > 0
               -- Merchlist/Loc - parent items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im
                where NVL(rpd.sys_generated_exclusion, 0) = 1
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rmld.merch_list_id                  = rpd.skulist
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and im.item_parent                      = rmld.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- Merchlist/Loc - parent diff items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im
                where NVL(rpd.sys_generated_exclusion, 0) = 1
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rmld.merch_list_id                  = rpd.skulist
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and im.item_parent                      = rmld.item
                  and im.diff_1                           = rmld.diff_id
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- Merchlist/Loc - tran level items
               union all
               select 
                      rpd.price_event_id,
                      rpd.price_event_type,
                      im.item,
                      rpd.location
                 from promos rpd,
                      rpm_merch_list_detail rmld,
                      item_master im
                where NVL(rpd.sys_generated_exclusion, 0) = 1
                  and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                  and rmld.merch_list_id                  = rpd.skulist
                  and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and im.item                             = rmld.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0));

   -- Lock to freeze the RPM_PRICE_EVENT_THREAD_STATUS
   cursor C_LOCK (I_pe_thread_id IN NUMBER) is
      select status
        from rpm_price_event_thread_status
       where pe_thread_id  = I_pe_thread_id
         and thread_number = L_thread_number
         for update of status wait 1800;

   cursor C_EVENTS_PC is
      select OBJ_PRICE_CHANGE_REC(price_event_id,
                                  price_event_type)
        from (select distinct price_event_id,
                     price_event_type,
                     SUM(DECODE(stat.status,
                                'C', 1,
                                0)) OVER (PARTITION BY price_event_id) count_complete_pe,
                     SUM(1) OVER (PARTITION BY price_event_id) count_all_pe
                from rpm_price_event_thread_status stat,
                     rpm_price_change rpc
               where stat.pe_thread_id   = LP_pc_pe_thread_id
                 and rpc.price_change_id = stat.price_event_id
                 and rpc.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE)
       where count_all_pe = count_complete_pe;

BEGIN

   -- Assume there is always one Thread Number in the input
   if I_thd_luw_tbl is NOT NULL and
      I_thd_luw_tbl.COUNT > 0 then
      L_thread_number := I_thd_luw_tbl(1);
   else
      O_error_message := 'INVALID_THREAD_NUMBER';
      return 0;
   end if;

   --Instrumentation
   BEGIN
   
	  RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                            LP_error_msg,
                                            RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                            RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                            RPM_CONSTANTS.BATCH_NAME_PEE,
                                            L_program||L_thread_number,
                                            RPM_CONSTANTS.LOG_STARTED,
                                           'Starting to Process Price Event LUWs for thread number - '||L_thread_number);

	  RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_PEE||LP_trace_name||L_thread_number);

   EXCEPTION

      when OTHERS then
         goto PPEL_1;

   END;

   <<PPEL_1>>
   
   open C_GET_PE_TYPE;
   fetch C_GET_PE_TYPE into L_price_event_type;
   close C_GET_PE_TYPE;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_GET_PC_IL;
      fetch C_GET_PC_IL BULK COLLECT into L_pc_il_tbl;
      close C_GET_PC_IL;

      if L_pc_il_tbl is NOT NULL and
         L_pc_il_tbl.COUNT > 0 then

         if EXECUTE_PRICE_CHANGES(O_error_message,
                                  L_pc_il_tbl,
                                  FALSE) = FALSE Then
            return 0;
         end if;

         if EXECUTE_AFFECTED_PROMOS(O_error_message,
                                    L_pc_il_tbl,
                                    FALSE) = FALSE Then
            return 0;
         end if;
      end if;

      open C_LOCK(LP_pc_pe_thread_id);
      close C_LOCK;

      update rpm_price_event_thread_status
         set status = 'C'
       where thread_number    = L_thread_number
         and price_event_type = L_price_event_type;

      open C_EVENTS_PC;
      fetch C_EVENTS_PC BULK COLLECT into L_pc_tbl;
      close C_EVENTS_PC;

      if L_pc_tbl is NOT NULL and
         L_pc_tbl.COUNT > 0 then
         ---
         if UPDATE_ITEM_ZONE_PRICE(O_error_message,
                                   L_pc_tbl) = FALSE then
            return 0;
         end if;
      end if;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      open C_GET_CL_IL;
      fetch C_GET_CL_IL BULK COLLECT into L_cl_il_tbl;
      close C_GET_CL_IL;

      if L_cl_il_tbl is NOT NULL and
         L_cl_il_tbl.COUNT > 0 then

         if EXECUTE_CLEARANCES(O_error_message,
                               L_cl_il_tbl,
                               FALSE) = FALSE Then
            return 0;
         end if;

         if EXECUTE_AFFECTED_PROMOS(O_error_message,
                                    L_cl_il_tbl,
                                    FALSE) = FALSE Then
            return 0;
         end if;
      end if;

      open C_LOCK(LP_cl_pe_thread_id);
      close C_LOCK;

      update rpm_price_event_thread_status
         set status = 'C'
       where thread_number    = L_thread_number
         and price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                  RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET);

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                RPM_CONSTANTS.PE_TYPE_PROMO_END) then

      open C_GET_SP_IL;
      fetch C_GET_SP_IL BULK COLLECT into L_sp_il_tbl;
      close C_GET_SP_IL;

      if L_sp_il_tbl is NOT NULL and
         L_sp_il_tbl.COUNT > 0 then

         if EXECUTE_PROMOS(O_error_message,
                           L_sp_il_tbl,
                           FALSE) = FALSE Then
            return 0;
         end if;

         if EXECUTE_AFFECTED_PROMOS(O_error_message,
                                    L_sp_il_tbl,
                                    FALSE) = FALSE Then
            return 0;
         end if;
      end if;

      open C_LOCK(LP_sp_pe_thread_id);
      close C_LOCK;

      update rpm_price_event_thread_status
         set status = 'C'
       where thread_number    = L_thread_number
         and price_event_type IN (RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                  RPM_CONSTANTS.PE_TYPE_PROMO_END);

   end if;

   --Instrumentation
   BEGIN
   
      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program||L_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to Process Price Event LUWs for thread number - '||L_thread_number);
   EXCEPTION

      when OTHERS then
         goto PPEL_2;

   END;
   <<PPEL_2>>
   
   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END PROCESS_PRICE_EVENT_LUWS;

--------------------------------------------------------------------------------------------------

FUNCTION UPDATE_PE_STATUS(O_error_message    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_PRICE_EVENT_SQL.UPDATE_PE_STATUS';

   L_pc_tbl_update  OBJ_PRICE_CHANGE_TBL := NULL;
   L_pe_tbl_pending OBJ_PRICE_CHANGE_TBL := NULL;

   cursor C_EVENTS_PE is
      select NEW OBJ_PRICE_CHANGE_REC(price_event_id,
                                      price_event_type)
        from (select distinct
                     price_event_id,
                     price_event_type
                from rpm_price_event_thread_status
               where status != 'C');

   cursor C_EVENTS_PC_UPDATE is
      select NEW OBJ_PRICE_CHANGE_REC(price_event_id,
                                      price_event_type)
        from (select price_event_id,
                     price_event_type
                from rpm_price_event_thread_exec stat,
                     rpm_price_change rpc
               where stat.pe_thread_id   = LP_pc_pe_thread_id
                 and rpc.price_change_id = stat.price_event_id
                 and rpc.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                 and stat.price_event_id NOT IN (select pc.price_change_id
                                                    from table(cast(L_pe_tbl_pending as OBJ_PRICE_CHANGE_TBL)) pc
                                                   where pc.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE));

BEGIN

   --Instrumentation
   BEGIN
   
      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to update PE status');


   EXCEPTION

      when OTHERS then
         goto UPDATE_PE_1;

   END;
   <<UPDATE_PE_1>>
   
   open C_EVENTS_PE;
   fetch C_EVENTS_PE BULK COLLECT into L_pe_tbl_pending;
   close C_EVENTS_PE;

   open C_EVENTS_PC_UPDATE;
   fetch C_EVENTS_PC_UPDATE BULK COLLECT into L_pc_tbl_update;
   close C_EVENTS_PC_UPDATE;

   if L_pc_tbl_update is NOT NULL and
      L_pc_tbl_update.COUNT > 0 then

      merge into rpm_price_change target
      using (select pc.price_change_id
               from table(cast(L_pc_tbl_update as OBJ_PRICE_CHANGE_TBL)) pe,
                    rpm_price_change pc
              where pe.price_change_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                and pc.state            != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                and pc.price_change_id   = pe.price_change_id) source
      on (target.price_change_id = source.price_change_id)
      when MATCHED then
         update
            set target.state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

   end if;

   merge into rpm_clearance
   using (select price_event_id
            from rpm_price_event_thread_exec stat,
                 rpm_clearance rc
           where stat.pe_thread_id     = LP_cl_pe_thread_id
             and rc.clearance_id       = stat.price_event_id
             and rc.state             != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
             and stat.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
             and stat.price_event_id   NOT IN (select /*+ HASH_AJ */ cl.price_change_id
                                                 from table(cast(L_pe_tbl_pending as OBJ_PRICE_CHANGE_TBL)) cl
                                                where cl.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                                  and cl.price_change_id   is NOT NULL)) source
   on (clearance_id = source.price_event_id)
   when MATCHED then
      update
         set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

   merge into rpm_clearance_reset
   using (select price_event_id
            from rpm_price_event_thread_exec stat,
                 rpm_clearance_reset rc
           where stat.pe_thread_id      = LP_cl_pe_thread_id
             and rc.clearance_id        = stat.price_event_id
             and rc.state              != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
             and stat.price_event_type  = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
             and stat.price_event_id    NOT IN (select /*+ HASH_AJ */ cl.price_change_id
                                                  from table(cast(L_pe_tbl_pending as OBJ_PRICE_CHANGE_TBL)) cl
                                                 where cl.price_change_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
                                                   and cl.price_change_id   is NOT NULL)) source
   on (clearance_id = source.price_event_id)
   when MATCHED then
      update
         set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

   merge into rpm_promo_dtl target
   using (select case
                    when price_event_type = RPM_CONSTANTS.PE_TYPE_PROMO_START then
                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                    when price_event_type = RPM_CONSTANTS.PE_TYPE_PROMO_END and
                         rpd.state NOT IN (RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                           RPM_CONSTANTS.PR_CANCELLED_STATE_CODE) then
                       RPM_CONSTANTS.PR_COMPLETE_STATE_CODE
                    when price_event_type = RPM_CONSTANTS.PE_TYPE_PROMO_END and
                         rpd.state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                       RPM_CONSTANTS.PR_CANCELLED_STATE_CODE
                    else
                         rpd.state
                 end state,
                 rpd.promo_dtl_id,
                 price_event_id price_change_id,
                 price_event_type
            from rpm_price_event_thread_exec stat,
                 rpm_promo_dtl rpd
           where stat.pe_thread_id   = LP_sp_pe_thread_id
             and rpd.promo_dtl_id    = stat.price_event_id
             and stat.price_event_id NOT IN (select prm.price_change_id
                                               from table(cast(L_pe_tbl_pending as OBJ_PRICE_CHANGE_TBL)) prm)
             and 1 = (case
                          when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_PROMO_START and
                                rpd.state != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then
                             1
                          when stat.price_event_type = RPM_CONSTANTS.PE_TYPE_PROMO_END and
                                rpd.state NOT IN (RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                  RPM_CONSTANTS.PR_CANCELLED_STATE_CODE) then
                             1
                          else
                             0
                      end)) source
   on (target.promo_dtl_id = source.promo_dtl_id)
   when MATCHED then
      update
         set target.state = source.state;

   --Instrumentation
   BEGIN
   
      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_PEE,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to update PE status');
      
   EXCEPTION

      when OTHERS then
         goto UPDATE_PE_2;

   END;
   <<UPDATE_PE_2>>
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END UPDATE_PE_STATUS;

--------------------------------------------------------------------------------------------------

END RPM_PRICE_EVENT_SQL;
/
