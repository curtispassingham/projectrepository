CREATE OR REPLACE PACKAGE RPM_PRICE_EVENT_ITEM_LIST_SQL AS

---------------------------------------------------------
FUNCTION VALIDATE_UPLOADED_DATA(O_error_msg          OUT VARCHAR2,
                                O_valid_result       OUT NUMBER,
                                I_merch_list_id   IN     NUMBER,
                                I_merch_list_desc IN     VARCHAR2,
                                I_user_id         IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------
FUNCTION VALIDATE_TXN_EXCLUSION_PEIL(O_error_msg                OUT VARCHAR2,
                                     O_some_data_removed_ind    OUT NUMBER,
                                     O_all_data_removed_ind     OUT NUMBER,
                                     I_promo_dtl_id          IN     NUMBER,
                                     I_buylists              IN     OBJ_MERCH_NODE_TBL,
                                     I_merch_list_id         IN     NUMBER,
                                     I_merch_list_desc       IN     VARCHAR2,
                                     I_user_id               IN     VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION PURGE_MERCH_LIST_DTL_WS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION PURGE_LIST(O_error_msg        OUT VARCHAR2,
                    I_merch_list_id IN     NUMBER)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION COPY_LIST(O_error_msg           OUT VARCHAR2,
                   O_new_list_id         OUT NUMBER,
                   O_valid_result        OUT NUMBER,
                   I_existing_list_id IN     NUMBER,
                   I_new_pe_id        IN     NUMBER,
                   I_pe_type          IN     VARCHAR2,
                   I_user_id          IN     VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION GET_PEIL_DCS(O_error_message     OUT VARCHAR2,
                      O_merch_data        OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                      I_merch_list_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
---------------------------------------------------------

END RPM_PRICE_EVENT_ITEM_LIST_SQL;
/
