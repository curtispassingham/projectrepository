CREATE OR REPLACE PACKAGE BODY RPM_CC_PROMO_CONSTRAINT AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_PROMO_CONSTRAINT.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_vdate DATE := GET_VDATE;

   L_pc_promo_overlap_ind RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROMO_OVERLAP_IND%TYPE := NULL;

   cursor C_CHECK is
      select distinct fr.price_event_id,
             fr.future_retail_id
        from rpm_promo_dtl rpd,
             (select rfr.price_event_id,
                     rfr.future_retail_id,
                     ilex.promo_dtl_id
                from rpm_future_retail_gtt rfr,
                     rpm_promo_item_loc_expl_gtt ilex,
                     (select gtt.price_event_id,
                             gtt.item,
                             gtt.location,
                             gtt.action_date,
                             con.pc_promo_days
                        from rpm_future_retail_gtt gtt,
                             rpm_pricing_constraint con,
                             rpm_price_change rpc
                       where gtt.price_event_id              NOT IN (select ccet.price_event_id
                                                                       from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
                         and gtt.price_change_id             is NOT NULL
                         and con.dept                        = gtt.dept
                         and con.class                       = gtt.class
                         and con.subclass                    = gtt.subclass
                         and gtt.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                         and con.location                    = gtt.location
                         and rpc.price_change_id             = gtt.price_change_id
                         and gtt.action_date                >= (L_vdate - con.pc_promo_days)
                         and NVL(rpc.ignore_constraints, 0) != 1
                      union all
                      select gtt.price_event_id,
                             gtt.item,
                             gtt.location,
                             gtt.action_date,
                             con.pc_promo_days
                        from rpm_future_retail_gtt gtt,
                             rpm_pricing_constraint con,
                             rpm_price_change rpc
                       where gtt.price_event_id              NOT IN (select ccet.price_event_id
                                                                       from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
                         and gtt.price_change_id             is NOT NULL
                         and con.dept                        = gtt.dept
                         and con.class                       = gtt.class
                         and con.subclass                    = gtt.subclass
                         and gtt.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpc.price_change_id             = gtt.price_change_id
                         and gtt.action_date                >= (L_vdate - con.pc_promo_days)
                         and NVL(rpc.ignore_constraints, 0) != 1
                         and EXISTS (select 1
                                       from rpm_zone_location rzl
                                      where rzl.zone_id  = gtt.location
                                        and con.location = rzl.location
                                        and rownum       = 1)) cons
             where ilex.price_event_id           = cons.price_event_id
               and ilex.item                     = cons.item
               and ilex.location                 = cons.location
               and TRUNC(ilex.detail_start_date) BETWEEN cons.action_date and (cons.action_date + cons.pc_promo_days)
               and (   ilex.detail_change_type   is NULL
                    or ilex.detail_change_type   != RPM_CONSTANTS.RETAIL_EXCLUDE)
               and rfr.price_event_id            = cons.price_event_id
               and rfr.item                      = cons.item
               and rfr.location                  = cons.location
               and rfr.action_date               = ilex.detail_start_date
               and NOT EXISTS (select 1
                                 from rpm_promo_item_loc_expl_gtt ilex2
                                where ilex2.price_event_id                         = rfr.price_event_id
                                  and ilex2.item                                   = rfr.item
                                  and ilex2.location                               = rfr.location
                                  and ilex2.detail_start_date                     <= rfr.action_date
                                  and NVL(ilex2.detail_end_date, rfr.action_date) >= rfr.action_date
                                  and (   (    ilex.customer_type                 is NULL
                                           and ilex2.customer_type                is NULL)
                                       or ilex.customer_type                      = ilex2.customer_type)
                                  and ilex2.detail_change_type                    is NOT NULL
                                  and ilex2.detail_change_type                    = RPM_CONSTANTS.RETAIL_EXCLUDE
                                  and (   (    ilex2.exception_parent_id          is NOT NULL
                                           and ilex.promo_id                      = ilex2.promo_id
                                           and ilex.promo_comp_id                 = ilex2.promo_comp_id)
                                       or (    ilex2.exception_parent_id          is NULL
                                           and ilex.promo_id                      = ilex2.promo_id)))) fr
       where rpd.promo_dtl_id               = fr.promo_dtl_id
         and NVL(rpd.ignore_constraints, 0) != 1
      union
      select rfr.price_event_id,
             rfr.future_retail_id
        from rpm_future_retail_gtt rfr,
             rpm_price_change rpc,
             (select /*+ ORDERED */
                     gtt.price_event_id,
                     gtt.item,
                     gtt.location,
                     gtt.action_date,
                     con.pc_promo_days,
                     ilex.detail_end_date
                from rpm_promo_item_loc_expl_gtt ilex,
                     rpm_future_retail_gtt gtt,
                     rpm_pricing_constraint con,
                     rpm_promo_dtl rpd
               where ilex.price_event_id             NOT IN (select ccet.price_event_id
                                                               from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
                 and (   ilex.detail_change_type is NULL
                      or ilex.detail_change_type != RPM_CONSTANTS.RETAIL_EXCLUDE)
                 and (   ilex.detail_end_date is NULL
                      or ilex.detail_end_date >= L_vdate)
                 and gtt.price_event_id              = ilex.price_event_id
                 and gtt.item                        = ilex.item
                 and gtt.location                    = ilex.location
                 and gtt.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and gtt.action_date                 = ilex.detail_start_date
                 and rpd.promo_dtl_id                = ilex.promo_dtl_id
                 and NVL(rpd.ignore_constraints, 0) != 1
                 and con.dept                        = gtt.dept
                 and con.class                       = gtt.class
                 and con.subclass                    = gtt.subclass
                 and con.location                    = gtt.location
                 and NOT EXISTS (select 1
                                   from rpm_promo_item_loc_expl_gtt ilex2
                                  where ilex2.price_event_id                        = gtt.price_event_id
                                    and ilex2.item                                  = gtt.item
                                    and ilex2.location                              = gtt.location
                                    and ilex2.detail_start_date                     <= gtt.action_date
                                    and NVL(ilex2.detail_end_date, gtt.action_date) >= gtt.action_date
                                    and (   (    ilex.customer_type                 is NULL
                                             and ilex2.customer_type                is NULL)
                                         or ilex.customer_type                      = ilex2.customer_type)
                                    and ilex2.detail_change_type                    is NOT NULL
                                    and ilex2.detail_change_type                    = RPM_CONSTANTS.RETAIL_EXCLUDE
                                    and (   (    ilex2.exception_parent_id          is NOT NULL
                                             and ilex.promo_id                      = ilex2.promo_id
                                             and ilex.promo_comp_id                 = ilex2.promo_comp_id)
                                         or (    ilex2.exception_parent_id          is NULL
                                             and ilex.promo_id                      = ilex2.promo_id)))
              union all
              select gtt.price_event_id,
                     gtt.item,
                     gtt.location,
                     gtt.action_date,
                     con.pc_promo_days,
                     ilex.detail_end_date
                from rpm_promo_item_loc_expl_gtt ilex,
                     rpm_future_retail_gtt gtt,
                     rpm_pricing_constraint con,
                     rpm_promo_dtl rpd
               where ilex.price_event_id             NOT IN (select ccet.price_event_id
                                                               from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
                 and (   ilex.detail_change_type is NULL
                      or ilex.detail_change_type != RPM_CONSTANTS.RETAIL_EXCLUDE)
                 and (   ilex.detail_end_date is NULL
                      or ilex.detail_end_date >= L_vdate)
                 and gtt.price_event_id              = ilex.price_event_id
                 and gtt.item                        = ilex.item
                 and gtt.location                    = ilex.location
                 and gtt.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and gtt.action_date                 = ilex.detail_start_date
                 and rpd.promo_dtl_id                = ilex.promo_dtl_id
                 and NVL(rpd.ignore_constraints, 0) != 1
                 and con.dept                        = gtt.dept
                 and con.class                       = gtt.class
                 and con.subclass                    = gtt.subclass
                 and EXISTS (select 1
                               from rpm_zone_location rzl
                              where rzl.zone_id  = gtt.location
                                and con.location = rzl.location
                                and rownum       = 1)
                 and NOT EXISTS (select 1
                                   from rpm_promo_item_loc_expl_gtt ilex2
                                  where ilex2.price_event_id                        = gtt.price_event_id
                                    and ilex2.item                                  = gtt.item
                                    and ilex2.location                              = gtt.location
                                    and ilex2.detail_start_date                     <= gtt.action_date
                                    and NVL(ilex2.detail_end_date, gtt.action_date) >= gtt.action_date
                                    and (   (    ilex.customer_type                 is NULL
                                             and ilex2.customer_type                is NULL)
                                         or ilex.customer_type                      = ilex2.customer_type)
                                    and ilex2.detail_change_type                    is NOT NULL
                                    and ilex2.detail_change_type                    = RPM_CONSTANTS.RETAIL_EXCLUDE
                                    and (   (    ilex2.exception_parent_id          is NOT NULL
                                             and ilex.promo_id                      = ilex2.promo_id
                                             and ilex.promo_comp_id                 = ilex2.promo_comp_id)
                                         or (    ilex2.exception_parent_id          is NULL
                                             and ilex.promo_id                      = ilex2.promo_id)))
         and rownum                         > 0) cons
       where rfr.price_event_id              = cons.price_event_id
         and rfr.item                        = cons.item
         and rfr.location                    = cons.location
         and rfr.action_date                 BETWEEN (TRUNC(cons.action_date) - cons.pc_promo_days) and TRUNC(cons.detail_end_date)
         and rfr.price_change_id             is NOT NULL
         and rpc.price_change_id             = rfr.price_change_id
         and NVL(rpc.ignore_constraints, 0) != 1;

   cursor C_CHECK_2 is
      select fr.price_event_id,
             fr.future_retail_id
        from rpm_promo_dtl rpd,
             (select cons.price_event_id,
                     cons.future_retail_id,
                     ilex.promo_dtl_id
                from rpm_promo_item_loc_expl_gtt ilex,
                     (select gtt.price_event_id,
                             gtt.future_retail_id,
                             gtt.item,
                             gtt.location,
                             gtt.action_date
                        from rpm_future_retail_gtt gtt,
                             rpm_price_change rpc
                       where gtt.price_event_id              NOT IN (select ccet.price_event_id
                                                                       from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
                         and gtt.price_change_id             is NOT NULL
                         and rpc.price_change_id             = gtt.price_change_id
                         and gtt.action_date                 >= L_vdate
                         and NVL(rpc.ignore_constraints, 0)  != 1) cons
               where ilex.price_event_id           = cons.price_event_id
                 and ilex.item                     = cons.item
                 and ilex.location                 = cons.location
                 and (   ilex.detail_change_type   is NULL
                      or ilex.detail_change_type   != RPM_CONSTANTS.RETAIL_EXCLUDE)
                 and TRUNC(ilex.detail_start_date) = cons.action_date
                 and NOT EXISTS (select 1
                                   from rpm_promo_item_loc_expl_gtt ilex2
                                  where ilex2.price_event_id                         = cons.price_event_id
                                    and ilex2.item                                   = cons.item
                                    and ilex2.location                               = cons.location
                                    and ilex2.detail_start_date                      <= cons.action_date
                                    and NVL(ilex2.detail_end_date, cons.action_date) >= cons.action_date
                                    and (   (    ilex.customer_type                  is NULL
                                             and ilex2.customer_type                 is NULL)
                                         or ilex.customer_type                       = ilex2.customer_type)
                                    and ilex2.detail_change_type                     is NOT NULL
                                    and ilex2.detail_change_type                     = RPM_CONSTANTS.RETAIL_EXCLUDE
                                    and (   (    ilex2.exception_parent_id           is NOT NULL
                                             and ilex.promo_id                       = ilex2.promo_id
                                             and ilex.promo_comp_id                  = ilex2.promo_comp_id)
                                         or (    ilex2.exception_parent_id           is NULL
                                             and ilex.promo_id                       = ilex2.promo_id)))) fr
       where rpd.promo_dtl_id               = fr.promo_dtl_id
         and NVL(rpd.ignore_constraints, 0) != 1
      union
      select rfr.price_event_id,
             rfr.future_retail_id
        from rpm_future_retail_gtt rfr,
             rpm_price_change rpc,
             (select gtt.price_event_id,
                     gtt.item,
                     gtt.location,
                     gtt.action_date
                from rpm_future_retail_gtt gtt,
                     rpm_promo_item_loc_expl_gtt ilex,
                     rpm_promo_dtl rpd
               where gtt.price_event_id            NOT IN (select ccet.price_event_id
                                                             from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
                 and ilex.price_event_id           = gtt.price_event_id
                 and ilex.item                     = gtt.item
                 and ilex.location                 = gtt.location
                 and (   ilex.detail_change_type   is NULL
                      or ilex.detail_change_type   != RPM_CONSTANTS.RETAIL_EXCLUDE)
                 and gtt.action_date               = ilex.detail_start_date
                 and rpd.promo_dtl_id              = ilex.promo_dtl_id
                 and gtt.action_date               >= L_vdate
                 and NVL(rpd.ignore_constraints,0) != 1
                 and NOT EXISTS (select 1
                                   from rpm_promo_item_loc_expl_gtt ilex2
                                  where ilex2.price_event_id                        = gtt.price_event_id
                                    and ilex2.item                                  = gtt.item
                                    and ilex2.location                              = gtt.location
                                    and ilex2.detail_start_date                     <= gtt.action_date
                                    and NVL(ilex2.detail_end_date, gtt.action_date) >= gtt.action_date
                                    and (   (    ilex.customer_type                 is NULL
                                             and ilex2.customer_type                is NULL)
                                         or ilex.customer_type                      = ilex2.customer_type)
                                    and ilex2.detail_change_type                    is NOT NULL
                                    and ilex2.detail_change_type                    = RPM_CONSTANTS.RETAIL_EXCLUDE
                                    and (   (    ilex2.exception_parent_id          is NOT NULL
                                             and ilex.promo_id                      = ilex2.promo_id
                                             and ilex.promo_comp_id                 = ilex2.promo_comp_id)
                                         or (    ilex2.exception_parent_id          is NULL
                                             and ilex.promo_id                      = ilex2.promo_id)))
                 and rownum                        > 0) cons
       where rfr.price_event_id             = cons.price_event_id
         and rfr.item                       = cons.item
         and rfr.location                   = cons.location
         and rfr.action_date                = TRUNC(cons.action_date)
         and rfr.price_change_id            is NOT NULL
         and rfr.action_date                >= L_vdate
         and rpc.price_change_id            = rfr.price_change_id
         and NVL(rpc.ignore_constraints, 0) != 1;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then

         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      for rec IN C_CHECK loop
         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'promotion_constraint_violation');
         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;
         end if;

      end loop;

      select price_change_promo_overlap_ind
        into L_pc_promo_overlap_ind
        from rpm_system_options;

      if L_pc_promo_overlap_ind = RPM_CONSTANTS.BOOLEAN_TRUE then
         return 1;
      end if;

      for rec IN C_CHECK_2 loop
         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'promotion_constraint_violation');
         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;
         end if;
      end loop;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END RPM_CC_PROMO_CONSTRAINT;
/