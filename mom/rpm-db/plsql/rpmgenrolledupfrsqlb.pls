CREATE OR REPLACE PACKAGE BODY RPM_GENERATE_ROLLUP_FR_SQL AS
--------------------------------------------------------------------------------

   LP_BULK_CC_PE_ID  CONSTANT NUMBER(15) := -1111199999;
   LP_PRICE_EVENT_ID CONSTANT NUMBER(15) := -1111199999;

--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT_HIGH_VOL(O_error_msg    OUT VARCHAR2,
                               I_dept      IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_PARENT_ZONE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_PARENT_LOC(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_ITEM_ZONE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_DIFF_ZONE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_DIFF_LOC(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_ROLLUP_FR_FOR_PZG_UPD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(60) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ROLLUP_FR_FOR_PZG_UPD';

BEGIN

   if GENERATE_DIFF_LOC(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_ITEM_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_DIFF_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_PARENT_LOC(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_PARENT_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if PUSH_BACK(O_error_msg) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_ROLLUP_FR_FOR_PZG_UPD;

--------------------------------------------------------------------------------

FUNCTION GENERATE_ROLLUP_FR_BY_ITEMS(O_error_msg    OUT VARCHAR2,
                                     I_items     IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ROLLUP_FR_BY_ITEMS';

BEGIN

   delete rpm_merch_node_zone_node_gtt;
   delete rpm_me_item_gtt;

   insert into rpm_me_item_gtt
      (dept,
       class,
       subclass,
       item_parent,
       item,
       diff_1)
   select distinct
          t.dept,
          t.class,
          t.subclass,
          t.item_parent,
          t.item,
          t.diff_1
     from (select /*+ CARDINALITY (it, 100) */
                  im.dept,
                  im.class,
                  im.subclass,
                  im.item_parent,
                  im.item,
                  DECODE(im.item_parent,
                         NULL, NULL,
                         im.diff_1) diff_1
             from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids,
                  item_master im
            where im.item         = VALUE(ids)
              and im.tran_level   = im.item_level
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           select /*+ CARDINALITY (it, 100) */
                  im.dept,
                  im.class,
                  im.subclass,
                  im.item_parent,
                  im.item,
                  im.diff_1
             from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids,
                  item_master im
            where im.item_parent  = VALUE(ids)
              and im.tran_level   = im.item_level
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t;

   insert into rpm_merch_node_zone_node_gtt
      (dept,
       class,
       subclass,
       location,
       zone_node_type,
       zone_id)
   select distinct
          t.dept,
          t.class,
          t.subclass,
          rzl.location,
          rzl.loc_type,
          rzl.zone_id
     from (select /*+ CARDINALITY (it, 100) */
                  distinct
                  im.dept,
                  im.class,
                  im.subclass
             from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids,
                  item_master im
            where im.item = VALUE(ids)) t,
          rpm_merch_retail_def_expl mer,
          rpm_zone rz,
          rpm_zone_location rzl
    where mer.dept         = t.dept
      and mer.class        = t.class
      and mer.subclass     = t.subclass
      and rz.zone_group_id = mer.regular_zone_group
      and rzl.zone_id      = rz.zone_id;

   if POPULATE_GTT (O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_DIFF_LOC(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_ITEM_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_DIFF_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_PARENT_LOC(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_PARENT_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if PUSH_BACK(O_error_msg) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_ROLLUP_FR_BY_ITEMS;
--------------------------------------------------------------------------------

FUNCTION GENERATE_ROLLUP_FR_FOR_NIL(O_error_msg        OUT VARCHAR2,
                                    I_thread_number IN     NUMBER,
                                    I_process_id    IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ROLLUP_FR_FOR_NIL';

   L_dept_item_locs OBJ_NUM_NUM_STR_TBL := NULL;
   L_start_time     TIMESTAMP           := SYSTIMESTAMP;

   cursor C_GET_DEPS is
      select distinct dept
        from rpm_nil_rollup_sibling_gtt;

   cursor C_GET_DEPT_ITEM_LOCS(I_dept IN NUMBER) is
      select OBJ_NUM_NUM_STR_REC(location,
                                 zone_node_type,
                                 item)
        from (select item,
                     location,
                     zone_node_type
                from rpm_nil_rollup_sibling_gtt
               where dept = I_dept
              union all
              select distinct item,
                     zone_id location,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                from rpm_nil_rollup_sibling_gtt
               where dept = I_dept
              union all
              select distinct item_parent item,
                     location,
                     zone_node_type
                from rpm_nil_rollup_sibling_gtt
               where dept = I_dept
              union all
              select distinct item_parent item,
                     zone_id location,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                from rpm_nil_rollup_sibling_gtt
               where dept = I_dept);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;
   delete rpm_nil_rollup_sibling_gtt;

   insert into rpm_nil_rollup_sibling_gtt (dept,
                                           item,
                                           item_parent,
                                           diff_id,
                                           location,
                                           zone_node_type,
                                           zone_id)
      select dept,
             item,
             item_parent,
             diff_id,
             location,
             zone_node_type,
             zone_id
        from (with rnrt as
                 (select dept,
                         item,
                         item_level,
                         location,
                         zone_node_type
                    from rpm_nil_rollup_thread
                   where thread_number = I_thread_number
                     and process_id    = I_process_id) -- end of rnrt
              -- parent/zone
              select distinct rnrt.dept,
                     im.item,
                     im.item_parent,
                     im.diff_1    diff_id,
                     ril.loc      location,
                     rzl.loc_type zone_node_type,
                     rzl.zone_id
                from rnrt,
                     rpm_zone_location rzl,
                     item_master im,
                     rpm_item_loc ril
               where rnrt.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and rnrt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rnrt.item           = im.item_parent
                 and rnrt.location       = rzl.zone_id
                 and ril.dept            = im.dept
                 and ril.item            = im.item
                 and ril.loc             = rzl.location
              union all
              -- item zone
              select distinct rnrt.dept,
                     ril.item,
                     NULL         item_parent,
                     NULL         diff_id,
                     ril.loc      location,
                     rzl.loc_type zone_node_type,
                     rzl.zone_id
                from rnrt,
                     rpm_zone_location rzl,
                     rpm_item_loc ril
               where rnrt.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and rnrt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rnrt.location       = rzl.zone_id
                 and ril.dept            = rnrt.dept
                 and ril.item            = rnrt.item
                 and ril.loc             = rzl.location
              union all
              -- parent loc
              select distinct rnrt.dept,
                     im.item,
                     im.item_parent,
                     im.diff_1     diff_id,
                     rnrt.location,
                     rnrt.zone_node_type,
                     NULL          zone_id
                from rnrt,
                     item_master im,
                     rpm_item_loc ril
               where rnrt.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and rnrt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and im.item_parent      = rnrt.item
                 and ril.dept            = im.dept
                 and ril.item            = im.item
                 and ril.loc             = rnrt.location
              union all
              -- item loc
              select dept,
                     item,
                     NULL     item_parent,
                     NULL     diff_id,
                     location,
                     zone_node_type,
                     NULL     zone_id
                from rnrt
               where item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and rnrt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE));

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_nil_rollup_sibling_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Populate RFR_GTT with timelines at item-loc level ONLY.  This includes pre-existing
   -- timelines that are related to the data being brought into the system as part of this
   -- run of NIL.  This is done in order to ensure that timelines reflect existing events
   -- accurately.  The IL level timelines created here will be used to generate higher level
   -- timelines and then the rollup logic will later remove whatever is not needed.

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      diff_id,
                                      item_parent,
                                      zone_node_type,
                                      location,
                                      zone_id,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      rfr_rowid,
                                      timeline_seq,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      max_hier_level,
                                      cur_hier_level)
      select -9999999999,
             DECODE(fr_rank,
                    3, future_retail_id,
                    RPM_FUTURE_RETAIL_SEQ.NEXTVAL) future_retail_id,
             dept,
             class,
             subclass,
             item,
             diff_id,
             item_parent,
             zone_node_type,
             location,
             zone_id,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             DECODE(fr_rank,
                    3, rfr_rowid,
                    NULL) rfr_rowid,
             timeline_seq,
             on_simple_promo_ind,
             on_complex_promo_ind,
             max_hier_level,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC
        from (select rfr.future_retail_id,
                     rfr.dept,
                     rfr.class,
                     rfr.subclass,
                     t.to_item item,
                     t.to_diff_id diff_id,
                     t.to_item_parent item_parent,
                     t.to_zone_node_type zone_node_type,
                     t.to_location location,
                     t.to_zone_id zone_id,
                     rfr.action_date,
                     rfr.selling_retail,
                     rfr.selling_retail_currency,
                     rfr.selling_uom,
                     rfr.multi_units,
                     rfr.multi_unit_retail,
                     rfr.multi_unit_retail_currency,
                     rfr.multi_selling_uom,
                     rfr.clear_retail,
                     rfr.clear_retail_currency,
                     rfr.clear_uom,
                     rfr.simple_promo_retail,
                     rfr.simple_promo_retail_currency,
                     rfr.simple_promo_uom,
                     rfr.price_change_id,
                     rfr.price_change_display_id,
                     rfr.pc_exception_parent_id,
                     rfr.pc_change_type,
                     rfr.pc_change_amount,
                     rfr.pc_change_currency,
                     rfr.pc_change_percent,
                     rfr.pc_change_selling_uom,
                     rfr.pc_null_multi_ind,
                     rfr.pc_multi_units,
                     rfr.pc_multi_unit_retail,
                     rfr.pc_multi_unit_retail_currency,
                     rfr.pc_multi_selling_uom,
                     rfr.pc_price_guide_id,
                     rfr.clearance_id,
                     rfr.clearance_display_id,
                     rfr.clear_mkdn_index,
                     rfr.clear_start_ind,
                     rfr.clear_change_type,
                     rfr.clear_change_amount,
                     rfr.clear_change_currency,
                     rfr.clear_change_percent,
                     rfr.clear_change_selling_uom,
                     rfr.clear_price_guide_id,
                     rfr.loc_move_from_zone_id,
                     rfr.loc_move_to_zone_id,
                     rfr.location_move_id,
                     rfr.lock_version,
                     rfr.rowid rfr_rowid,
                     NULL timeline_seq,
                     rfr.on_simple_promo_ind,
                     rfr.on_complex_promo_ind,
                     rfr.max_hier_level,
                     t.fr_rank,
                     MAX(t.fr_rank) OVER (PARTITION BY t.to_item,
                                                       t.to_location,
                                                       t.to_zone_node_type) max_fr_rank
                from (-- IL data from PZ timelines
                      select 0                                 fr_rank,
                             dept,
                             item_parent                       from_item,
                             NULL                              from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and zone_id     is NOT NULL
                         and rownum      > 0
                      union all
                      -- IL data from PDZ timelines
                      select distinct
                             1                                 fr_rank,
                             dept,
                             item_parent                       from_item,
                             diff_id                           from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and diff_id     is NOT NULL
                         and zone_id     is NOT NULL
                         and rownum      > 0
                      union all
                      -- IL data from PL timelines
                      select distinct
                             1              fr_rank,
                             dept,
                             item_parent    from_item,
                             NULL           from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and rownum      > 0
                      union all
                      -- IL data from IZ timelines
                      select distinct
                             2                                 fr_rank,
                             dept,
                             item                              from_item,
                             diff_id                           from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where zone_id is NOT NULL
                         and rownum  > 0
                      union all
                      -- IL data from PDL timelines
                      select distinct
                             2              fr_rank,
                             dept,
                             item_parent    from_item,
                             diff_id        from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and diff_id     is NOT NULL
                         and rownum      > 0
                      union all
                      -- IL data from IL timelines
                      select distinct
                             3              fr_rank,
                             dept,
                             item           from_item,
                             diff_id        from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where rownum > 0) t,
                     rpm_future_retail rfr
               where rfr.dept                 = t.dept
                 and rfr.item                 = t.from_item
                 and NVL(rfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                 and rfr.location             = t.from_location
                 and rfr.zone_node_type       = t.from_zone_node_type) fr_il
       where fr_rank = max_fr_rank;

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_future_retail_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                            promo_item_loc_expl_id,
                                            item,
                                            diff_id,
                                            item_parent,
                                            dept,
                                            class,
                                            subclass,
                                            location,
                                            zone_node_type,
                                            zone_id,
                                            promo_id,
                                            promo_display_id,
                                            promo_secondary_ind,
                                            promo_comp_id,
                                            comp_display_id,
                                            promo_dtl_id,
                                            type,
                                            customer_type,
                                            detail_secondary_ind,
                                            detail_start_date,
                                            detail_end_date,
                                            detail_apply_to_code,
                                            detail_change_type,
                                            detail_change_amount,
                                            detail_change_currency,
                                            detail_change_percent,
                                            detail_change_selling_uom,
                                            detail_price_guide_id,
                                            exception_parent_id,
                                            promo_comp_msg_type,
                                            rpile_rowid,
                                            max_hier_level,
                                            cur_hier_level,
                                            timebased_dtl_ind)
      select /*+ INDEX(rpile, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(rpile) */
             -9999999999,
             DECODE(rpile_rank,
                    3, promo_item_loc_expl_id,
                    RPM_PROM_IL_EXPL_SEQ.NEXTVAL) promo_item_loc_expl_id,
             item,
             diff_id,
             item_parent,
             dept,
             class,
             subclass,
             location,
             zone_node_type,
             zone_id,
             promo_id,
             promo_display_id,
             promo_secondary_ind,
             promo_comp_id,
             comp_display_id,
             promo_dtl_id,
             type,
             customer_type,
             detail_secondary_ind,
             detail_start_date,
             detail_end_date,
             detail_apply_to_code,
             detail_change_type,
             detail_change_amount,
             detail_change_currency,
             detail_change_percent,
             detail_change_selling_uom,
             detail_price_guide_id,
             exception_parent_id,
             promo_comp_msg_type,
             DECODE(rpile_rank,
                    3, rpile_rowid,
                    NULL) rpile_rowid,
             max_hier_level,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC,
             timebased_dtl_ind
        from (select rpile.promo_item_loc_expl_id,
                     t.to_item item,
                     t.to_diff_id diff_id,
                     t.to_item_parent item_parent,
                     t.to_zone_id zone_id,
                     rpile.dept,
                     rpile.class,
                     rpile.subclass,
                     t.to_location location,
                     t.to_zone_node_type zone_node_type,
                     rpile.promo_id,
                     rpile.promo_display_id,
                     rpile.promo_secondary_ind,
                     rpile.promo_comp_id,
                     rpile.comp_display_id,
                     rpile.promo_dtl_id,
                     rpile.type,
                     rpile.customer_type,
                     rpile.detail_secondary_ind,
                     rpile.detail_start_date,
                     rpile.detail_end_date,
                     rpile.detail_apply_to_code,
                     rpile.detail_change_type,
                     rpile.detail_change_amount,
                     rpile.detail_change_currency,
                     rpile.detail_change_percent,
                     rpile.detail_change_selling_uom,
                     rpile.detail_price_guide_id,
                     rpile.exception_parent_id,
                     NULL promo_comp_msg_type,
                     rpile.rowid rpile_rowid,
                     rpile.max_hier_level,
                     t.rpile_rank,
                     MAX(t.rpile_rank) OVER (PARTITION BY t.to_item,
                                                          t.to_location,
                                                          t.to_zone_node_type) max_rpile_rank,
                     rpile.timebased_dtl_ind
                from (-- IL data from PZ timelines
                      select 0                                 rpile_rank,
                             dept,
                             item_parent                       from_item,
                             NULL                              from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and zone_id     is NOT NULL
                      union all
                      -- IL data from PDZ timelines
                      select distinct
                             1                                 rpile_rank,
                             dept,
                             item_parent                       from_item,
                             diff_id                           from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and diff_id     is NOT NULL
                         and zone_id     is NOT NULL
                      union all
                      -- IL data from PL timelines
                      select distinct
                             1              rpile_rank,
                             dept,
                             item_parent    from_item,
                             NULL           from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                      union all
                      -- IL data from IZ timelines
                      select distinct
                             2                                 rpile_rank,
                             dept,
                             item                              from_item,
                             diff_id                           from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where zone_id is NOT NULL
                      union all
                      -- IL data from PDL timelines
                      select distinct
                             2              rpile_rank,
                             dept,
                             item_parent    from_item,
                             diff_id        from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and diff_id     is NOT NULL
                      union all
                      -- IL data from IL timelines
                      select distinct
                             3              rpile_rank,
                             dept,
                             item           from_item,
                             diff_id        from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt) t,
                     rpm_promo_item_loc_expl rpile
               where rpile.dept                 = t.dept
                 and rpile.item                 = t.from_item
                 and NVL(rpile.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                 and rpile.location             = t.from_location
                 and rpile.zone_node_type       = t.from_zone_node_type) rpile_il
       where rpile_rank = max_rpile_rank;

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_promo_item_loc_expl_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                              cust_segment_promo_id,
                                              item,
                                              zone_node_type,
                                              location,
                                              action_date,
                                              customer_type,
                                              dept,
                                              promo_retail,
                                              promo_retail_currency,
                                              promo_uom,
                                              complex_promo_ind,
                                              cspfr_rowid,
                                              item_parent,
                                              diff_id,
                                              zone_id,
                                              max_hier_level,
                                              cur_hier_level)
      select -9999999999,
             DECODE(cspfr_rank,
                    3, cust_segment_promo_id,
                    RPM_CUST_SEG_PROMO_SEQ.NEXTVAL) cust_segment_promo_id,
             item,
             zone_node_type,
             location,
             action_date,
             customer_type,
             dept,
             promo_retail,
             promo_retail_currency,
             promo_uom,
             complex_promo_ind,
             DECODE(cspfr_rank,
                    3, cspfr_rowid,
                    NULL) cspfr_rowid,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC
        from (select cspfr.cust_segment_promo_id,
                     t.to_item item,
                     t.to_zone_node_type zone_node_type,
                     t.to_location location,
                     cspfr.action_date,
                     cspfr.customer_type,
                     cspfr.dept,
                     cspfr.promo_retail,
                     cspfr.promo_retail_currency,
                     cspfr.promo_uom,
                     cspfr.complex_promo_ind,
                     cspfr.rowid cspfr_rowid,
                     t.to_item_parent item_parent,
                     t.to_diff_id diff_id,
                     t.to_zone_id zone_id,
                     cspfr.max_hier_level,
                     cspfr_rank,
                     MAX(cspfr_rank) OVER (PARTITION BY t.to_item,
                                                        t.to_location,
                                                        t.to_zone_node_type) max_cspfr_rank
                from (-- IL data from PZ timelines
                      select 0                                 cspfr_rank,
                             dept,
                             item_parent                       from_item,
                             NULL                              from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and zone_id     is NOT NULL
                      union all
                      -- IL data from PDZ timelines
                      select distinct
                             1                                 cspfr_rank,
                             dept,
                             item_parent                       from_item,
                             diff_id                           from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and diff_id     is NOT NULL
                         and zone_id     is NOT NULL
                      union all
                      -- IL data from PL timelines
                      select distinct
                             1              cspfr_rank,
                             dept,
                             item_parent    from_item,
                             NULL           from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                      union all
                      -- IL data from IZ timelines
                      select distinct
                             2                                 cspfr_rank,
                             dept,
                             item                              from_item,
                             diff_id                           from_diff_id,
                             zone_id                           from_location,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                             item                              to_item,
                             item_parent                       to_item_parent,
                             diff_id                           to_diff_id,
                             location                          to_location,
                             zone_node_type                    to_zone_node_type,
                             zone_id                           to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where zone_id is NOT NULL
                      union all
                      -- IL data from PDL timelines
                      select distinct
                             2              cspfr_rank,
                             dept,
                             item_parent    from_item,
                             diff_id        from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt
                       where item_parent is NOT NULL
                         and diff_id     is NOT NULL
                      union all
                      -- IL data from IL timelines
                      select distinct
                             3              cspfr_rank,
                             dept,
                             item           from_item,
                             diff_id        from_diff_id,
                             location       from_location,
                             zone_node_type from_zone_node_type,
                             item           to_item,
                             item_parent    to_item_parent,
                             diff_id        to_diff_id,
                             location       to_location,
                             zone_node_type to_zone_node_type,
                             zone_id        to_zone_id
                        from rpm_nil_rollup_sibling_gtt) t,
                    rpm_cust_segment_promo_fr cspfr
              where cspfr.dept                 = t.dept
                and cspfr.item                 = t.from_item
                and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and cspfr.location             = t.from_location
                and cspfr.zone_node_type       = t.from_zone_node_type) cspfr_il
       where cspfr_rank = max_cspfr_rank
         and EXISTS (select 1
                       from rpm_promo_item_loc_expl_gtt ilex
                      where ilex.dept                 = cspfr_il.dept
                        and ilex.item                 = cspfr_il.item
                        and NVL(ilex.diff_id, '-999') = NVL(cspfr_il.diff_id, '-999')
                        and ilex.location             = cspfr_il.location
                        and ilex.zone_node_type       = cspfr_il.zone_node_type
                        and ilex.customer_type        = cspfr_il.customer_type
                        and rownum                    = 1);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_cust_segment_promo_fr_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Clean up future retail data that was exploded to IL level...
   -- Incase the staged NIL data spans multiple depts, loop on the
   -- dept value for partition only access to the future retail tables.

   for rec IN C_GET_DEPS loop

      open C_GET_DEPT_ITEM_LOCS(rec.dept);
      fetch C_GET_DEPT_ITEM_LOCS BULK COLLECT into L_dept_item_locs;
      close C_GET_DEPT_ITEM_LOCS;

      forall i IN 1..L_dept_item_locs.COUNT
         delete
           from rpm_future_retail
          where dept           = rec.dept
            and item           = L_dept_item_locs(i).string
            and location       = L_dept_item_locs(i).number_1
            and zone_node_type = L_dept_item_locs(i).number_2;

      forall i IN 1..L_dept_item_locs.COUNT
         delete
           from rpm_promo_item_loc_expl
          where dept           = rec.dept
            and item           = L_dept_item_locs(i).string
            and location       = L_dept_item_locs(i).number_1
            and zone_node_type = L_dept_item_locs(i).number_2;

      forall i IN 1..L_dept_item_locs.COUNT
         delete
           from rpm_cust_segment_promo_fr
          where dept           = rec.dept
            and item           = L_dept_item_locs(i).string
            and location       = L_dept_item_locs(i).number_1
            and zone_node_type = L_dept_item_locs(i).number_2;

   end loop;

   if GENERATE_DIFF_LOC(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_ITEM_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_DIFF_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_PARENT_LOC(O_error_msg) = 0 then
      return 0;
   end if;

   if GENERATE_PARENT_ZONE(O_error_msg) = 0 then
      return 0;
   end if;

   -- All data created on RFR_GTT here will be "pushed back" to RFR as part
   -- of the NIL related rollup logic.  This saves having to repopulate the
   -- GTT data within the same transaction.

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_ROLLUP_FR_FOR_NIL;
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_ROLLUP_FR_SQL.POPULATE_GTT';

BEGIN

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;

   -- Populate RPM_FUTURE_RETAIL_GTT with the new column populated as well
   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      zone_node_type,
                                      location,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      rfr_rowid,
                                      timeline_seq,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      item_parent,
                                      diff_id,
                                      zone_id,
                                      cur_hier_level,
                                      max_hier_level)
      -- Get all records on rpm_future_retail
      select /*+ LEADING(im) USE_NL(im, fr) */
             -9999999999,
             future_retail_id,
             fr.dept,
             fr.class,
             fr.subclass,
             fr.item,
             fr.zone_node_type,
             fr.location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             fr.rowid,
             NULL,
             fr.on_simple_promo_ind,
             fr.on_complex_promo_ind,
             im.item_parent,
             im.diff_1,
             s.zone_id,
             'IL' cur_hier_level,
             case
                when im.item_parent is NOT NULL and
                     s.zone_id is NOT NULL then
                   RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                when im.item_parent is NOT NULL and
                     s.zone_id is NULL then
                   RPM_CONSTANTS.FR_HIER_PARENT_LOC
                when im.item_parent is NULL and
                     s.zone_id is NOT NULL then
                   RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                else
                   RPM_CONSTANTS.FR_HIER_ITEM_LOC
             end as max_hier_level
        from rpm_me_item_gtt im,
             rpm_future_retail fr,
             rpm_merch_node_zone_node_gtt s
       where fr.dept           = im.dept
         and fr.item           = im.item
         and fr.dept           = s.dept (+)
         and fr.class          = s.class (+)
         and fr.subclass       = s.subclass (+)
         and fr.location       = s.location (+)
         and fr.zone_node_type = s.zone_node_type (+);

   -- Populate RPM_PROMO_ITEM_LOC_EXPL_GTT with the new column populated as well
   insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                            promo_item_loc_expl_id,
                                            item,
                                            dept,
                                            class,
                                            subclass,
                                            location,
                                            promo_id,
                                            promo_display_id,
                                            promo_secondary_ind,
                                            promo_comp_id,
                                            comp_display_id,
                                            promo_dtl_id,
                                            type,
                                            customer_type,
                                            detail_secondary_ind,
                                            detail_start_date,
                                            detail_end_date,
                                            detail_apply_to_code,
                                            detail_change_type,
                                            detail_change_amount,
                                            detail_change_currency,
                                            detail_change_percent,
                                            detail_change_selling_uom,
                                            detail_price_guide_id,
                                            exception_parent_id,
                                            promo_comp_msg_type,
                                            rpile_rowid,
                                            zone_node_type,
                                            item_parent,
                                            diff_id,
                                            zone_id,
                                            cur_hier_level,
                                            max_hier_level,
                                            timebased_dtl_ind)
      select /*+ INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I3) USE_NL(fr, ilex) */
             -9999999999,
             ilex.promo_item_loc_expl_id,
             ilex.item,
             ilex.dept,
             ilex.class,
             ilex.subclass,
             ilex.location,
             ilex.promo_id,
             ilex.promo_display_id,
             ilex.promo_secondary_ind,
             ilex.promo_comp_id,
             ilex.comp_display_id,
             ilex.promo_dtl_id,
             ilex.type,
             ilex.customer_type,
             ilex.detail_secondary_ind,
             ilex.detail_start_date,
             ilex.detail_end_date,
             ilex.detail_apply_to_code,
             ilex.detail_change_type,
             ilex.detail_change_amount,
             ilex.detail_change_currency,
             ilex.detail_change_percent,
             ilex.detail_change_selling_uom,
             ilex.detail_price_guide_id,
             ilex.exception_parent_id,
             NULL,
             ilex.rowid,
             fr.zone_node_type,
             fr.item_parent,
             fr.diff_id,
             fr.zone_id,
             fr.cur_hier_level,
             fr.max_hier_level,
             ilex.timebased_dtl_ind
        from (select t.dept,
                     t.item,
                     t.location,
                     t.zone_node_type,
                     t.item_parent,
                     t.diff_id,
                     t.zone_id,
                     t.cur_hier_level,
                     t.max_hier_level
                from (select gtt.dept,
                             gtt.item,
                             gtt.location,
                             gtt.zone_node_type,
                             gtt.item_parent,
                             gtt.diff_id,
                             gtt.zone_id,
                             gtt.cur_hier_level,
                             gtt.max_hier_level,
                             RANK() OVER (PARTITION BY gtt.dept,
                                                       gtt.item,
                                                       gtt.location
                                              ORDER BY gtt.action_date desc) rank
                        from rpm_future_retail_gtt gtt) t
               where t.rank = 1) fr,
             rpm_promo_item_loc_expl ilex
       where ilex.dept     = fr.dept
         and ilex.item     = fr.item
         and ilex.location = fr.location;

   -- Populate RPM_CUST_SEGMENT_PROMO_FR_GTT with the new column populated as well
   insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                              cust_segment_promo_id,
                                              item,
                                              zone_node_type,
                                              location,
                                              action_date,
                                              customer_type,
                                              dept,
                                              promo_retail,
                                              promo_retail_currency,
                                              promo_uom,
                                              complex_promo_ind,
                                              cspfr_rowid,
                                              item_parent,
                                              diff_id,
                                              zone_id,
                                              cur_hier_level,
                                              max_hier_level)
      select -9999999999,
             cspfr.cust_segment_promo_id,
             cspfr.item,
             cspfr.zone_node_type,
             cspfr.location,
             cspfr.action_date,
             cspfr.customer_type,
             cspfr.dept,
             cspfr.promo_retail,
             cspfr.promo_retail_currency,
             cspfr.promo_uom,
             cspfr.complex_promo_ind,
             cspfr.rowid,
             fr.item_parent,
             fr.diff_id,
             fr.zone_id,
             fr.cur_hier_level,
             fr.max_hier_level
        from (select t.dept,
                     t.item,
                     t.location,
                     t.zone_node_type,
                     t.item_parent,
                     t.diff_id,
                     t.zone_id,
                     t.cur_hier_level,
                     t.max_hier_level
                from (select gtt.dept,
                             gtt.item,
                             gtt.location,
                             gtt.item_parent,
                             gtt.zone_node_type,
                             gtt.diff_id,
                             gtt.zone_id,
                             gtt.cur_hier_level,
                             gtt.max_hier_level,
                             RANK() OVER (PARTITION BY gtt.dept,
                                                       gtt.item,
                                                       gtt.location
                                              ORDER BY gtt.action_date desc) rank
                        from rpm_future_retail_gtt gtt) t
               where t.rank = 1) fr,
             rpm_cust_segment_promo_fr cspfr
       where cspfr.dept     = fr.dept
         and cspfr.item     = fr.item
         and cspfr.location = fr.location;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END POPULATE_GTT;
--------------------------------------------------------------------------------

FUNCTION POPULATE_GTT_HIGH_VOL(O_error_msg    OUT VARCHAR2,
                               I_dept      IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_ROLLUP_FR_SQL.POPULATE_GTT_HIGH_VOL';

BEGIN

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;

   -- Populate RPM_FUTURE_RETAIL_GTT with the new column populated as well
   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       lock_version,
       rfr_rowid,
       timeline_seq,
       on_simple_promo_ind,
       on_complex_promo_ind,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
      ----
      -- Get all records on rpm_future_retail...
      ----
      select /*+ leading(im) use_NL(im, fr) */
             -9999999999,
             future_retail_id,
             fr.dept,
             fr.class,
             fr.subclass,
             fr.item,
             fr.zone_node_type,
             fr.location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             fr.rowid,
             NULL,
             fr.on_simple_promo_ind,
             fr.on_complex_promo_ind,
             im.item_parent,
             im.diff_1,
             s.zone_id,
             'IL' cur_hier_level,
             case
                when im.item_parent is NOT NULL and
                     s.zone_id is NOT NULL then
                   RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                when im.item_parent is NOT NULL and
                     s.zone_id is NULL then
                   RPM_CONSTANTS.FR_HIER_PARENT_LOC
                when im.item_parent is NULL and
                     s.zone_id is NOT NULL then
                   RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                else
                   RPM_CONSTANTS.FR_HIER_ITEM_LOC
             end as max_hier_level
        from rpm_me_item_gtt im,
             rpm_future_retail fr,
             rpm_merch_node_zone_node_gtt s
       where fr.dept           = I_dept
         and fr.item           = im.item
         and fr.dept           = s.dept (+)
         and fr.class          = s.class (+)
         and fr.subclass       = s.subclass (+)
         and fr.location       = s.location (+)
         and fr.zone_node_type = s.zone_node_type (+);

   -- Populate RPM_PROMO_ITEM_LOC_EXPL_GTT with the new column populated as well
   insert into rpm_promo_item_loc_expl_gtt
      (price_event_id,
       promo_item_loc_expl_id,
       item,
       dept,
       class,
       subclass,
       location,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       promo_comp_id,
       comp_display_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       exception_parent_id,
       promo_comp_msg_type,
       rpile_rowid,
       zone_node_type,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level,
       timebased_dtl_ind)
   select /*+ INDEX(ilex, rpm_promo_item_loc_expl_i3) use_NL(fr, ilex) */
          -9999999999,
          ilex.promo_item_loc_expl_id,
          ilex.item,
          ilex.dept,
          ilex.class,
          ilex.subclass,
          ilex.location,
          ilex.promo_id,
          ilex.promo_display_id,
          ilex.promo_secondary_ind,
          ilex.promo_comp_id,
          ilex.comp_display_id,
          ilex.promo_dtl_id,
          ilex.type,
          ilex.customer_type,
          ilex.detail_secondary_ind,
          ilex.detail_start_date,
          ilex.detail_end_date,
          ilex.detail_apply_to_code,
          ilex.detail_change_type,
          ilex.detail_change_amount,
          ilex.detail_change_currency,
          ilex.detail_change_percent,
          ilex.detail_change_selling_uom,
          ilex.detail_price_guide_id,
          ilex.exception_parent_id,
          NULL,
          ilex.rowid,
          fr.zone_node_type,
          fr.item_parent,
          fr.diff_id,
          fr.zone_id,
          fr.cur_hier_level,
          fr.max_hier_level,
          ilex.timebased_dtl_ind
     from (select t.dept,
                  t.item,
                  t.location,
                  t.zone_node_type,
                  t.item_parent,
                  t.diff_id,
                  t.zone_id,
                  t.cur_hier_level,
                  t.max_hier_level
             from (select gtt.dept,
                          gtt.item,
                          gtt.location,
                          gtt.zone_node_type,
                          gtt.item_parent,
                          gtt.diff_id,
                          gtt.zone_id,
                          gtt.cur_hier_level,
                          gtt.max_hier_level,
                          RANK() OVER (PARTITION BY gtt.dept,
                                                    gtt.item,
                                                    gtt.location
                                           ORDER BY gtt.action_date desc) rank
                     from rpm_future_retail_gtt gtt) t
            where t.rank = 1) fr,
          rpm_promo_item_loc_expl ilex
    where ilex.dept     = I_dept
      and ilex.item     = fr.item
      and ilex.location = fr.location;

   -- Populate RPM_CUST_SEGMENT_PROMO_FR_GTT with the new column populated as well
   insert into rpm_cust_segment_promo_fr_gtt
      (price_event_id,
       cust_segment_promo_id,
       item,
       zone_node_type,
       location,
       action_date,
       customer_type,
       dept,
       promo_retail,
       promo_retail_currency,
       promo_uom,
       complex_promo_ind,
       cspfr_rowid,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select -9999999999,
          cspfr.cust_segment_promo_id,
          cspfr.item,
          cspfr.zone_node_type,
          cspfr.location,
          cspfr.action_date,
          cspfr.customer_type,
          cspfr.dept,
          cspfr.promo_retail,
          cspfr.promo_retail_currency,
          cspfr.promo_uom,
          cspfr.complex_promo_ind,
          cspfr.rowid,
          fr.item_parent,
          fr.diff_id,
          fr.zone_id,
          fr.cur_hier_level,
          fr.max_hier_level
     from (select t.dept,
                  t.item,
                  t.location,
                  t.zone_node_type,
                  t.item_parent,
                  t.diff_id,
                  t.zone_id,
                  t.cur_hier_level,
                  t.max_hier_level
             from (select gtt.dept,
                          gtt.item,
                          gtt.location,
                          gtt.item_parent,
                          gtt.zone_node_type,
                          gtt.diff_id,
                          gtt.zone_id,
                          gtt.cur_hier_level,
                          gtt.max_hier_level,
                          RANK() OVER(PARTITION BY gtt.dept,
                                                   gtt.item,
                                                   gtt.location
                                          ORDER BY gtt.action_date desc) rank
                     from rpm_future_retail_gtt gtt) t
            where t.rank = 1) fr,
          rpm_cust_segment_promo_fr cspfr
    where cspfr.dept     = I_dept
      and cspfr.item     = fr.item
      and cspfr.location = fr.location;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END POPULATE_GTT_HIGH_VOL;
--------------------------------------------------------------------------------

FUNCTION GENERATE_PARENT_ZONE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_PARENT_ZONE';

BEGIN

   delete gtt_num_num_str_str_date_date;
   delete rpm_parent_zone_src_gtt;

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select t4.item_parent,
             t4.zone_id,
             t4.item,
             t4.location
        from (select distinct t3.dept,
                     t3.item_parent,
                     t3.zone_id,
                     t3.item,
                     t3.location
                from (select t2.item_parent,
                             t2.zone_id,
                             t2.dept,
                             t2.class,
                             t2.subclass,
                             t2.count_date,
                             t2.item,
                             t2.location,
                             t2.count_pe,
                             ROW_NUMBER() OVER (PARTITION BY t2.item_parent,
                                                             t2.zone_id
                                                    ORDER BY t2.count_pe,
                                                             t2.cnt desc,
                                                             t2.count_lm,
                                                             t2.count_date,
                                                             t2.item,
                                                             t2.location) rank
                        from (select t1.item_parent,
                                     t1.zone_id,
                                     t1.dept,
                                     t1.class,
                                     t1.subclass,
                                     t1.count_date,
                                     t1.item,
                                     t1.location,
                                     (t1.count_pc + t1.count_cl + t1.count_pr) count_pe,
                                     COUNT(t1.count_date) OVER (PARTITION BY t1.item_parent,
                                                                             t1.zone_id,
                                                                             t1.count_date) cnt,
                                     t1.count_lm
                                from (select fr.item_parent,
                                             fr.zone_id,
                                             fr.dept,
                                             fr.class,
                                             fr.subclass,
                                             fr.item,
                                             fr.location,
                                             SUM(case
                                                    when fr.price_change_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_pc,
                                             SUM(case
                                                    when fr.clearance_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_cl,
                                             SUM(fr.on_simple_promo_ind + fr.on_complex_promo_ind) OVER (PARTITION BY fr.item_parent,
                                                                                                                      fr.zone_id,
                                                                                                                      fr.item,
                                                                                                                      fr.location) count_pr,
                                             COUNT(action_date) OVER (PARTITION BY fr.item_parent,
                                                                                   fr.zone_id,
                                                                                   fr.item,
                                                                                   fr.location) count_date,
                                             SUM(case
                                                    when fr.location_move_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_lm
                                        from rpm_future_retail_gtt fr
                                       where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                         and fr.max_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE) t1 ) t2 ) t3
               where t3.rank = 1) t4
       where NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept           = t4.dept
                            and rfr.item           = t4.item_parent
                            and rfr.location       = t4.zone_id
                            and rfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                            and rownum             = 1);

   -- Populate rpm_parent_zone_src_gtt to provide the information on from which 'IL' the 'PZ' is generated

   insert into rpm_parent_zone_src_gtt
      (item_parent,
       zone_id,
       item,
       location)
      select varchar2_1,
             number_1,
             varchar2_2,
             number_2
        from gtt_num_num_str_str_date_date;

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 s.varchar2_1 item,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.number_1 location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 RPM_CONSTANTS.FR_HIER_PARENT_ZONE max_hier_level,
                 RPM_CONSTANTS.FR_HIER_PARENT_ZONE cur_hier_level
            from rpm_future_retail_gtt gtt,
                 gtt_num_num_str_str_date_date s
           where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.item           = s.varchar2_2
             and gtt.location       = s.number_2) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ LEADING(s) INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 s.varchar2_1 item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.number_1 location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 RPM_CONSTANTS.FR_HIER_PARENT_ZONE max_hier_level,
                 RPM_CONSTANTS.FR_HIER_PARENT_ZONE cur_hier_level,
                 ilex.timebased_dtl_ind
            from rpm_promo_item_loc_expl_gtt ilex,
                 gtt_num_num_str_str_date_date s
           where ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and ilex.item           = s.varchar2_2
             and ilex.location       = s.number_2
             and (   EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm
                              where rpdm.promo_dtl_id = ilex.promo_dtl_id
                                and (   (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                                         and rpdm.item       = s.varchar2_1)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class
                                         and rpdm.subclass   = ilex.subclass)
                                     or rpdm.merch_type      = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM))
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_promo_dtl_skulist rpds
                              where rpdm1.promo_dtl_id  = ilex.promo_dtl_id
                                and rpdm1.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                and rpds.price_event_id = rpdm1.promo_dtl_id
                                and rpds.skulist        = rpdm1.skulist
                                and rpds.item           = s.varchar2_1)
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_merch_list_detail rmld
                              where rpdm1.promo_dtl_id = ilex.promo_dtl_id
                                and rpdm1.merch_type   = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                and rmld.merch_list_id = rpdm1.price_event_itemlist
                                and rmld.item          = s.varchar2_1))
             and EXISTS (select 1
                           from rpm_promo_zone_location
                          where promo_dtl_id   = ilex.promo_dtl_id
                            and zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and zone_id        = s.number_1)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 s.varchar2_1 item,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.number_1 location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 RPM_CONSTANTS.FR_HIER_PARENT_ZONE max_hier_level,
                 RPM_CONSTANTS.FR_HIER_PARENT_ZONE cur_hier_level
            from rpm_cust_segment_promo_fr_gtt cspfr,
                 gtt_num_num_str_str_date_date s
           where cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and cspfr.item           = s.varchar2_2
             and cspfr.location       = s.number_2) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.max_hier_level,
              source.cur_hier_level);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_PARENT_ZONE;
--------------------------------------------------------------------------------

FUNCTION GENERATE_PARENT_LOC(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_PARENT_LOC';

BEGIN

   delete gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date (varchar2_1,
                                              number_1,
                                              varchar2_2,
                                              number_2)
      select t4.item_parent,
             t4.location,
             t4.item,
             t4.location
        from (select distinct t3.dept,
                     t3.item_parent,
                     t3.location,
                     t3.item
                from (select t2.item_parent,
                             t2.location,
                             t2.dept,
                             t2.class,
                             t2.subclass,
                             t2.count_date,
                             t2.item,
                             ROW_NUMBER() OVER (PARTITION BY t2.item_parent,
                                                             t2.location
                                                    ORDER BY t2.count_pe,
                                                             t2.cnt desc,
                                                             t2.count_lm,
                                                             t2.count_date,
                                                             t2.item,
                                                             t2.future_retail_id) rank
                        from (select t1.item_parent,
                                     t1.location,
                                     t1.dept,
                                     t1.class,
                                     t1.subclass,
                                     t1.count_date,
                                     t1.item,
                                     t1.future_retail_id,
                                     (t1.count_pc + t1.count_cl + t1.count_pr) count_pe,
                                     COUNT(t1.count_date) OVER (PARTITION BY t1.item_parent,
                                                                             t1.location,
                                                                             t1.count_date) cnt,
                                     t1.count_lm
                                from (select fr.item_parent,
                                             fr.location,
                                             fr.dept,
                                             fr.class,
                                             fr.subclass,
                                             fr.item,
                                             fr.future_retail_id,
                                             SUM(case
                                                    when fr.price_change_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.location,
                                                                         fr.item) count_pc,
                                             SUM(case
                                                    when fr.clearance_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.location,
                                                                         fr.item) count_cl,
                                             SUM(fr.on_simple_promo_ind + fr.on_complex_promo_ind) OVER (PARTITION BY fr.item_parent,
                                                                                                                      fr.location,
                                                                                                                      fr.item) count_pr,
                                             COUNT(action_date) OVER (PARTITION BY fr.item_parent,
                                                                                   fr.location,
                                                                                   fr.item) count_date,
                                             SUM(case
                                                    when fr.location_move_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.location,
                                                                         fr.item) count_lm
                                        from rpm_future_retail_gtt fr
                                       where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                         and fr.max_hier_level IN (RPM_CONSTANTS.FR_HIER_PARENT_LOC,
                                                                   RPM_CONSTANTS.FR_HIER_PARENT_ZONE)) t1 ) t2 ) t3
               where t3.rank = 1) t4
       where NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept           = t4.dept
                            and rfr.item           = t4.item_parent
                            and rfr.location       = t4.location
                            and rfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                            and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                            and rownum             = 1);

   -- Delete the 'PL' if the 'PZ' is generated from the same Location of the 'PL'
   -- This to make sure that they are coming from the same 'IL'
   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1) IN (select item_parent,
                                location
                           from rpm_parent_zone_src_gtt);

   -- Source the 'PL' with the same 'IL' that sourced the 'PZ' if they are from the same location.
   insert into gtt_num_num_str_str_date_date (varchar2_1,
                                              number_1,
                                              varchar2_2,
                                              number_2)
      select distinct item_parent,
             location,
             item,
             location
        from rpm_parent_zone_src_gtt gtt
       where NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = gtt.item_parent
                            and t.number_1   = gtt.location);

   -- Populate rpm_parent_loc_src_gtt to provide the information on from which 'IL' the 'PL' is generated

   delete from rpm_parent_loc_src_gtt; -- holds parent/loc data

   insert into rpm_parent_loc_src_gtt (item_parent,
                                       location,
                                       item)
      select varchar2_1,
             number_1,
             varchar2_2
        from gtt_num_num_str_str_date_date;

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 s.varchar2_1 item,
                 gtt.zone_node_type,
                 s.number_1 location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 gtt.zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_PARENT_LOC cur_hier_level
            from rpm_future_retail_gtt gtt,
                 gtt_num_num_str_str_date_date s
           where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.item           = s.varchar2_2
             and gtt.location       = s.number_2) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ LEADING(s) INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 s.varchar2_1 item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.number_1 location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 ilex.zone_node_type,
                 ilex.zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_PARENT_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from rpm_promo_item_loc_expl_gtt ilex,
                 gtt_num_num_str_str_date_date s
           where ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and ilex.item           = s.varchar2_2
             and ilex.location       = s.number_2
             and (   EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm
                              where rpdm.promo_dtl_id = ilex.promo_dtl_id
                                and (   (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                                         and rpdm.item       = s.varchar2_1)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class
                                         and rpdm.subclass   = ilex.subclass)
                                     or rpdm.merch_type      = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM))
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_promo_dtl_skulist rpds
                              where rpdm1.promo_dtl_id  = ilex.promo_dtl_id
                                and rpdm1.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                and rpds.price_event_id = rpdm1.promo_dtl_id
                                and rpds.skulist        = rpdm1.skulist
                                and rpds.item           = s.varchar2_1)
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_merch_list_detail rmld
                              where rpdm1.promo_dtl_id = ilex.promo_dtl_id
                                and rpdm1.merch_type   = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                and rmld.merch_list_id = rpdm1.price_event_itemlist
                                and rmld.item          = s.varchar2_1))
             and EXISTS (select 1
                           from rpm_promo_zone_location rpzl
                          where promo_dtl_id = ilex.promo_dtl_id
                            and (    zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                 and zone_id        = ilex.zone_id)
                             or (    zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                 and location       = ilex.location))) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 s.varchar2_1 item,
                 cspfr.zone_node_type,
                 s.number_1 location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 cspfr.zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_PARENT_LOC cur_hier_level
            from rpm_cust_segment_promo_fr_gtt cspfr,
                 gtt_num_num_str_str_date_date s
           where cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and cspfr.item           = s.varchar2_2
             and cspfr.location       = s.number_2) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   -- Verify if there is an existing 'DZ' timeline for the newly created 'PL' but no asscociated 'DL' timeline.
   -- then Create 'DL' timeline from 'DZ'
   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 gtt.item,
                 s.zone_node_type,
                 s.location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 gtt.diff_id,
                 gtt.location zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
            from (select t.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         im.diff_1 diff_id
                    from (select distinct varchar2_1 item_parent,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_future_retail_gtt gtt
           where gtt.item           = s.item_parent
             and gtt.location       = s.zone_id
             and gtt.diff_id        = s.diff_id
             and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt fr
                              where fr.item           = gtt.item
                                and fr.location       = s.location
                                and fr.diff_id        = gtt.diff_id
                                and fr.zone_node_type = s.zone_node_type
                                and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
             and EXISTS (select 1
                           from item_master im,
                                rpm_item_loc ril
                          where im.item_parent = gtt.item
                            and im.diff_1      = gtt.diff_id
                            and ril.dept       = im.dept
                            and ril.item       = im.item
                            and ril.loc        = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ LEADING(s) INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 ilex.item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 s.zone_node_type,
                 ilex.diff_id,
                 ilex.location zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from (select t.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         im.diff_1 diff_id
                    from (select distinct varchar2_1 item_parent,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.item           = s.item_parent
             and ilex.location       = s.zone_id
             and ilex.diff_id        = s.diff_id
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
             and NOT EXISTS (select 1
                               from rpm_promo_item_loc_expl_gtt gtt
                              where gtt.item           = ilex.item
                                and gtt.location       = s.location
                                and gtt.diff_id        = ilex.diff_id
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
             and EXISTS (select 1
                           from item_master im,
                                rpm_item_loc ril
                          where im.item_parent = ilex.item
                            and im.diff_1      = ilex.diff_id
                            and ril.dept       = im.dept
                            and ril.item       = im.item
                            and ril.loc        = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 cspfr.item,
                 s.zone_node_type,
                 s.location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 cspfr.diff_id,
                 cspfr.location zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
            from (select t.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         im.diff_1 diff_id
                    from (select distinct varchar2_1 item_parent,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_cust_segment_promo_fr_gtt cspfr
           where cspfr.item           = s.item_parent
             and cspfr.location       = s.zone_id
             and cspfr.diff_id        = s.diff_id
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
             and NOT EXISTS (select 1
                               from rpm_cust_segment_promo_fr_gtt gtt
                              where gtt.item           = cspfr.item
                                and gtt.location       = s.location
                                and gtt.diff_id        = cspfr.diff_id
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
             and EXISTS (select 1
                           from item_master im,
                                rpm_item_loc ril
                          where im.item_parent = cspfr.item
                            and im.diff_1      = cspfr.diff_id
                            and ril.dept       = im.dept
                            and ril.item       = im.item
                            and ril.loc        = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   -- Verify if there is any existing 'IZ' timeline for the newly created 'PL' but no asscociated 'IL' timeline.
   -- then Create 'IL' timeline from 'IZ'
   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 gtt.item,
                 s.zone_node_type,
                 s.location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 s.item_parent item_parent,
                 gtt.diff_id,
                 gtt.location zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select t.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         t.item
                    from (select distinct varchar2_1 item_parent,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_future_retail_gtt gtt
           where gtt.item           = s.item
             and gtt.location       = s.zone_id
             and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt fr
                              where fr.item           = gtt.item
                                and fr.location       = s.location
                                and fr.zone_node_type = s.zone_node_type
                                and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = gtt.dept
                            and ril.item = gtt.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ LEADING(s) INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 ilex.item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 s.zone_node_type,
                 s.item_parent item_parent,
                 ilex.diff_id,
                 ilex.location zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from (select t.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         t.item
                    from (select distinct varchar2_1 item_parent,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.item           = s.item
             and ilex.location       = s.zone_id
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and NOT EXISTS (select 1
                               from rpm_promo_item_loc_expl_gtt gtt
                              where gtt.item           = ilex.item
                                and gtt.location       = s.location
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = ilex.dept
                            and ril.item = ilex.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 cspfr.item,
                 s.zone_node_type,
                 s.location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 s.item_parent item_parent,
                 cspfr.diff_id,
                 cspfr.location zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select t.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         t.item
                    from (select distinct varchar2_1 item_parent,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_cust_segment_promo_fr_gtt cspfr
           where cspfr.item           = s.item
             and cspfr.location       = s.zone_id
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and NOT EXISTS (select 1
                               from rpm_cust_segment_promo_fr_gtt gtt
                              where gtt.item           = cspfr.item
                                and gtt.location       = s.location
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = cspfr.dept
                            and ril.item = cspfr.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_PARENT_LOC;
--------------------------------------------------------------------------------

FUNCTION GENERATE_ITEM_ZONE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ITEM_ZONE';

BEGIN

   delete gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select t4.item,
             t4.zone_id,
             t4.item,
             t4.location
        from (select distinct t3.dept,
                     t3.item,
                     t3.zone_id,
                     t3.location
                from (select t2.item,
                             t2.zone_id,
                             t2.dept,
                             t2.class,
                             t2.subclass,
                             t2.count_date,
                             t2.location,
                             ROW_NUMBER() OVER (PARTITION BY t2.item,
                                                             t2.zone_id
                                                    ORDER BY t2.count_pe,
                                                             t2.cnt desc,
                                                             t2.count_lm,
                                                             t2.count_date,
                                                             t2.location) rank
                        from (select t1.item,
                                     t1.zone_id,
                                     t1.dept,
                                     t1.class,
                                     t1.subclass,
                                     t1.count_date,
                                     t1.location,
                                     (t1.count_pc + t1.count_cl + t1.count_pr) count_pe,
                                     COUNT(t1.count_date) OVER (PARTITION BY t1.item,
                                                                             t1.zone_id,
                                                                             t1.count_date) cnt,
                                     t1.count_lm
                                from (select fr.item,
                                             fr.zone_id,
                                             fr.dept,
                                             fr.class,
                                             fr.subclass,
                                             fr.location,
                                             SUM(case
                                                    when fr.price_change_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item,
                                                                         fr.zone_id,
                                                                         fr.location) count_pc,
                                             SUM(case
                                                    when fr.clearance_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item,
                                                                         fr.zone_id,
                                                                         fr.location) count_cl,
                                             SUM(fr.on_simple_promo_ind + fr.on_complex_promo_ind) OVER (PARTITION BY fr.item,
                                                                                                                      fr.zone_id,
                                                                                                                      fr.location) count_pr,
                                             COUNT(action_date) OVER (PARTITION BY fr.item,
                                                                                   fr.zone_id,
                                                                                   fr.location) count_date,
                                             SUM(case
                                                    when fr.location_move_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item,
                                                                         fr.zone_id,
                                                                         fr.location) count_lm
                                        from rpm_future_retail_gtt fr
                                       where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                         and fr.max_hier_level IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                                                   RPM_CONSTANTS.FR_HIER_PARENT_ZONE)) t1 ) t2 ) t3
               where t3.rank = 1) t4
       where NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept           = t4.dept
                            and rfr.item           = t4.item
                            and rfr.location       = t4.zone_id
                            and rfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                            and rownum             = 1);

   -- Delete the 'IZ' in which the item is the source of the 'PZ'.
   -- This is to make sure that the 'IZ' is sourced from the same 'IL' that sourced the 'PZ'

   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1) IN (select distinct item,
                                zone_id
                           from rpm_parent_zone_src_gtt);

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select distinct item,
             zone_id,
             item,
             location
        from rpm_parent_zone_src_gtt gtt
       where NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = gtt.item
                            and t.number_1   = gtt.zone_id);

   -- Delete the 'IZ' in which the item is the source of the 'DZ'.
   -- This is to make sure that the 'IZ' is sourced from the same 'IL' that sourced the 'DZ'

   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1) IN (select distinct item,
                                zone_id
                           from rpm_diff_zone_src_gtt);

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select distinct item,
             zone_id,
             item,
             location
        from rpm_diff_zone_src_gtt gtt
       where NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = gtt.item
                            and t.number_1   = gtt.zone_id);

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 s.varchar2_1 item,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.number_1 location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 gtt.item_parent,
                 gtt.diff_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE cur_hier_level
            from rpm_future_retail_gtt gtt,
                 gtt_num_num_str_str_date_date s
           where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.item           = s.varchar2_2
             and gtt.location       = s.number_2) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 s.varchar2_1 item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.number_1 location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 ilex.item_parent,
                 ilex.diff_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE cur_hier_level,
                 ilex.timebased_dtl_ind
            from rpm_promo_item_loc_expl_gtt ilex,
                 gtt_num_num_str_str_date_date s
           where ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and ilex.item           = s.varchar2_2
             and ilex.location       = s.number_2
             and (   EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm
                              where rpdm.promo_dtl_id = ilex.promo_dtl_id
                                and (   (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                                         and (   rpdm.item   = s.varchar2_1
                                              or rpdm.item   = ilex.item_parent))
                                     or (    rpdm.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class
                                         and rpdm.subclass   = ilex.subclass)
                                     or rpdm.merch_type      = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM))
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_promo_dtl_skulist rpds
                              where rpdm1.promo_dtl_id  = ilex.promo_dtl_id
                                and rpdm1.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                and rpds.price_event_id = rpdm1.promo_dtl_id
                                and rpds.skulist        = rpdm1.skulist
                                and (   rpds.item       = s.varchar2_1
                                     or rpds.item       = ilex.item_parent))
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_merch_list_detail rmld
                              where rpdm1.promo_dtl_id = ilex.promo_dtl_id
                                and rpdm1.merch_type   = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                and rmld.merch_list_id = rpdm1.price_event_itemlist
                                and (   rmld.item      = s.varchar2_1
                                     or rmld.item      = ilex.item_parent)))
             and EXISTS (select 1
                           from rpm_promo_zone_location
                          where promo_dtl_id   = ilex.promo_dtl_id
                            and zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and zone_id        = s.number_1)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              item_parent,
              diff_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.item_parent,
              source.diff_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 s.varchar2_1 item,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.number_1 location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 cspfr.item_parent,
                 cspfr.diff_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE cur_hier_level
            from rpm_cust_segment_promo_fr_gtt cspfr,
                 gtt_num_num_str_str_date_date s
           where cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and cspfr.item           = s.varchar2_2
             and cspfr.location       = s.number_2) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.max_hier_level,
              source.cur_hier_level);

   -- Verify if there is and existing 'DL' timeline for the newly created 'IZ' but no asscociated 'IL' timeline.
   -- then Create 'IL' timeline from 'DL'

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 s.item,
                 s.zone_node_type,
                 s.location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 s.item_parent,
                 gtt.diff_id,
                 s.zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select t.item,
                         im.item_parent,
                         im.diff_1 diff_id,
                         rzl.loc_type zone_node_type,
                         rzl.location,
                         t.location zone_id
                    from (select distinct varchar2_1 item,
                                 number_1 location
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_zone_location rzl
                   where im.item     = t.item
                     and rzl.zone_id = t.location
                     and rownum      > 0) s,
                 rpm_future_retail_gtt gtt
           where gtt.item           = s.item_parent
             and gtt.diff_id        = s.diff_id
             and gtt.location       = s.location
             and gtt.zone_id        = s.zone_id
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt fr
                              where fr.item           = s.item
                                and fr.location       = s.location
                                and fr.diff_id        = gtt.diff_id
                                and fr.zone_node_type = s.zone_node_type
                                and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = gtt.dept
                            and ril.item = s.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 s.item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 s.zone_node_type,
                 s.item_parent,
                 ilex.diff_id,
                 ilex.zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from (select t.item,
                         im.item_parent,
                         im.diff_1 diff_id,
                         rzl.loc_type zone_node_type,
                         rzl.location,
                         t.location zone_id
                    from (select distinct varchar2_1 item,
                                 number_1 location
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_zone_location rzl
                   where im.item     = t.item
                     and rzl.zone_id = t.location
                     and rownum      > 0) s,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.item           = s.item_parent
             and ilex.diff_id        = s.diff_id
             and ilex.location       = s.location
             and ilex.zone_id        = s.zone_id
             and ilex.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
             and NOT EXISTS (select 1
                               from rpm_promo_item_loc_expl_gtt gtt
                              where gtt.item           = s.item
                                and gtt.location       = s.location
                                and gtt.diff_id        = ilex.diff_id
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = ilex.dept
                            and ril.item = s.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 s.item,
                 s.zone_node_type,
                 s.location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 s.item_parent,
                 cspfr.diff_id,
                 s.zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select t.item,
                         im.item_parent,
                         im.diff_1 diff_id,
                         rzl.loc_type zone_node_type,
                         rzl.location,
                         t.location zone_id
                    from (select distinct varchar2_1 item,
                                 number_1 location
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_zone_location rzl
                   where im.item     = t.item
                     and rzl.zone_id = t.location
                     and rownum      > 0) s,
                 rpm_cust_segment_promo_fr_gtt cspfr
           where cspfr.item           = s.item_parent
             and cspfr.diff_id        = s.diff_id
             and cspfr.location       = s.location
             and cspfr.zone_id        = s.zone_id
             and cspfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
             and NOT EXISTS (select 1
                               from rpm_cust_segment_promo_fr_gtt gtt
                              where gtt.item           = s.item
                                and gtt.location       = s.location
                                and gtt.diff_id        = cspfr.diff_id
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = cspfr.dept
                            and ril.item = s.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   -- Verify if there is any existing 'PL' timeline for the newly created 'IZ' but no asscociated 'IL' timeline.
   -- then Create 'IL' timeline from 'PL'

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 s.item,
                 s.zone_node_type,
                 s.location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 s.item_parent,
                 gtt.diff_id,
                 s.zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select t.item,
                         im.item_parent,
                         im.diff_1 diff_id,
                         rzl.loc_type zone_node_type,
                         rzl.location,
                         t.location zone_id
                    from (select distinct varchar2_1 item,
                                 number_1 location
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_zone_location rzl
                   where im.item     = t.item
                     and rzl.zone_id = t.location
                     and rownum      > 0) s,
                 rpm_future_retail_gtt gtt
           where gtt.item           = s.item_parent
             and gtt.location       = s.location
             and gtt.zone_id        = s.zone_id
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt fr
                              where fr.item           = s.item
                                and fr.location       = s.location
                                and fr.zone_node_type = s.zone_node_type
                                and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = gtt.dept
                            and ril.item = s.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 s.item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 s.zone_node_type,
                 s.item_parent,
                 ilex.diff_id,
                 s.zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from (select t.item,
                         im.item_parent,
                         im.diff_1 diff_id,
                         rzl.loc_type zone_node_type,
                         rzl.location,
                         t.location zone_id
                    from (select distinct varchar2_1 item,
                                 number_1 location
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_zone_location rzl
                   where im.item     = t.item
                     and rzl.zone_id = t.location
                     and rownum      > 0) s,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.item           = s.item_parent
             and ilex.location       = s.location
             and ilex.zone_id        = s.zone_id
             and ilex.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
             and NOT EXISTS (select 1
                               from rpm_promo_item_loc_expl_gtt gtt
                              where gtt.item           = s.item
                                and gtt.location       = s.location
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = ilex.dept
                            and ril.item = s.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 s.item,
                 s.zone_node_type,
                 s.location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 s.item_parent,
                 cspfr.diff_id,
                 s.zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select t.item,
                         im.item_parent,
                         im.diff_1 diff_id,
                         rzl.loc_type zone_node_type,
                         rzl.location,
                         t.location zone_id
                    from (select distinct varchar2_1 item,
                                 number_1 location
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_zone_location rzl
                   where im.item     = t.item
                     and rzl.zone_id = t.location
                     and rownum      > 0) s,
                 rpm_cust_segment_promo_fr_gtt cspfr
           where cspfr.item           = s.item_parent
             and cspfr.location       = s.location
             and cspfr.zone_id        = s.zone_id
             and cspfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
             and NOT EXISTS (select 1
                               from rpm_cust_segment_promo_fr_gtt gtt
                              where gtt.item           = s.item
                                and gtt.location       = s.location
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = cspfr.dept
                            and ril.item = s.item
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_ITEM_ZONE;
--------------------------------------------------------------------------------

FUNCTION GENERATE_DIFF_ZONE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_DIFF_ZONE';

BEGIN

   delete gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select t4.diff_id,
             t4.zone_id,
             t4.item,
             t4.location
        from (select distinct t3.dept,
                     t3.item_parent,
                     t3.diff_id,
                     t3.zone_id,
                     t3.item,
                     t3.location
                from (select t2.item_parent,
                             t2.diff_id,
                             t2.zone_id,
                             t2.dept,
                             t2.class,
                             t2.subclass,
                             t2.count_date,
                             t2.item,
                             t2.location,
                             t2.count_pe,
                             ROW_NUMBER() OVER (PARTITION BY t2.item_parent,
                                                             t2.diff_id,
                                                             t2.zone_id
                                                    ORDER BY t2.count_pe,
                                                             t2.cnt desc,
                                                             t2.count_lm,
                                                             t2.count_date,
                                                             t2.item,
                                                             t2.location) rank
                        from (select t1.item_parent,
                                     t1.diff_id,
                                     t1.zone_id,
                                     t1.dept,
                                     t1.class,
                                     t1.subclass,
                                     t1.count_date,
                                     t1.item,
                                     t1.location,
                                     (t1.count_pc + t1.count_cl + t1.count_pr) count_pe,
                                     COUNT(t1.count_date) OVER (PARTITION BY t1.item_parent,
                                                                             t1.diff_id,
                                                                             t1.zone_id,
                                                                             t1.count_date) cnt,
                                     t1.count_lm
                                from (select fr.item_parent,
                                             fr.diff_id,
                                             fr.zone_id,
                                             fr.dept,
                                             fr.class,
                                             fr.subclass,
                                             fr.item,
                                             fr.location,
                                             SUM(case
                                                    when fr.price_change_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.diff_id,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_pc,
                                             SUM(case
                                                    when fr.clearance_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.diff_id,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_cl,
                                             SUM(fr.on_simple_promo_ind + fr.on_complex_promo_ind) OVER (PARTITION BY fr.item_parent,
                                                                                                                      fr.diff_id,
                                                                                                                      fr.zone_id,
                                                                                                                      fr.item,
                                                                                                                      fr.location) count_pr,
                                             COUNT(action_date) OVER (PARTITION BY fr.item_parent,
                                                                                   fr.diff_id,
                                                                                   fr.zone_id,
                                                                                   fr.item,
                                                                                   fr.location) count_date,
                                             SUM(case
                                                    when fr.location_move_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.diff_id,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_lm
                                        from rpm_future_retail_gtt fr
                                       where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                         and fr.diff_id        is NOT NULL
                                         and fr.max_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE) t1 ) t2 ) t3
               where t3.rank = 1) t4
       where NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept           = t4.dept
                            and rfr.item           = t4.item_parent
                            and rfr.diff_id        = t4.diff_id
                            and rfr.location       = t4.zone_id
                            and rfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                            and rownum             = 1);

   -- Delete the 'DZ' in which the item is the source of the 'PZ'.
   -- This is to make sure that the 'DZ' is sourced from the same 'IL' that sourced the 'PZ'

   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1,
           varchar2_2) IN (select distinct im.diff_1,
                                  src.zone_id,
                                  src.item
                             from rpm_parent_zone_src_gtt src,
                                  item_master im
                            where im.item        = src.item
                              and im.item_parent = src.item_parent
                              and im.diff_1      is NOT NULL);

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select distinct im.diff_1,
             src.zone_id,
             src.item,
             src.location
        from rpm_parent_zone_src_gtt src,
             item_master im
       where im.item        = src.item
         and im.item_parent = src.item_parent
         and im.diff_1      is NOT NULL
         and NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = im.diff_1
                            and t.number_1   = src.zone_id);

   -- Populate rpm_diff_zone_src_gtt to provide the information on from which 'IL' the 'DZ' is generated

   delete rpm_diff_zone_src_gtt;

   insert into rpm_diff_zone_src_gtt
      (diff_id,
       zone_id,
       item,
       location)
      select varchar2_1,
             number_1,
             varchar2_2,
             number_2
        from gtt_num_num_str_str_date_date;

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 gtt.item_parent item,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.number_1 location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 s.varchar2_1 diff_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_ZONE cur_hier_level
            from rpm_future_retail_gtt gtt,
                 gtt_num_num_str_str_date_date s
           where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.item           = s.varchar2_2
             and gtt.location       = s.number_2) source
   on (    target.item           = source.item
       and target.diff_id        = source.diff_id
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              diff_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.diff_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 ilex.item_parent item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.number_1 location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.varchar2_1 diff_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_ZONE cur_hier_level,
                 ilex.timebased_dtl_ind
            from rpm_promo_item_loc_expl_gtt ilex,
                 gtt_num_num_str_str_date_date s
           where ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and ilex.item           = s.varchar2_2
             and ilex.location       = s.number_2
             and (   EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm
                              where rpdm.promo_dtl_id = ilex.promo_dtl_id
                                and (   (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                                         and rpdm.item       = ilex.item_parent)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                                         and rpdm.item       = ilex.item_parent
                                         and rpdm.diff_id    = s.varchar2_1)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class
                                         and rpdm.subclass   = ilex.subclass)
                                     or rpdm.merch_type      = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM))
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_promo_dtl_skulist rpds
                              where rpdm1.promo_dtl_id  = ilex.promo_dtl_id
                                and rpdm1.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                and rpds.price_event_id = rpdm1.promo_dtl_id
                                and rpds.skulist        = rpdm1.skulist
                                and rpds.item           = ilex.item_parent)
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_merch_list_detail rmld
                              where rpdm1.promo_dtl_id = ilex.promo_dtl_id
                                and rpdm1.merch_type   = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                and rmld.merch_list_id = rpdm1.price_event_itemlist
                                and rmld.item          = ilex.item_parent))
             and EXISTS (select 1
                           from rpm_promo_zone_location
                          where promo_dtl_id   = ilex.promo_dtl_id
                            and zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and zone_id        = s.number_1)) source
   on (    target.item           = source.item
       and target.diff_id        = source.diff_id
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              diff_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.diff_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 cspfr.item_parent item,
                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type,
                 s.number_1 location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 s.varchar2_1 diff_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_ZONE cur_hier_level
            from rpm_cust_segment_promo_fr_gtt cspfr,
                 gtt_num_num_str_str_date_date s
           where cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and cspfr.item           = s.varchar2_2
             and cspfr.location       = s.number_2) source
   on (    target.item           = source.item
       and target.diff_id        = source.diff_id
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              diff_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.diff_id,
              source.max_hier_level,
              source.cur_hier_level);

   -- Verify if there is and existing 'PL' timeline for the newly created 'DZ' but no asscociated 'DL' timeline.
   -- then Create 'DL' timeline from 'PL'

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 gtt.item,
                 gtt.zone_node_type,
                 gtt.location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 s.diff_id,
                 s.zone_id zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
            from (select distinct im.item_parent,
                         s.varchar2_1 diff_id,
                         s.number_1 zone_id
                    from gtt_num_num_str_str_date_date s,
                         item_master im
                   where im.item = s.varchar2_2
                     and rownum  > 0) s,
                 rpm_future_retail_gtt gtt
           where gtt.item           = s.item_parent
             and gtt.zone_id        = s.zone_id
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt fr
                              where fr.item           = gtt.item
                                and fr.location       = gtt.location
                                and fr.diff_id        = s.diff_id
                                and fr.zone_node_type = gtt.zone_node_type
                                and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
             and EXISTS (select 1
                           from item_master im,
                                rpm_item_loc ril
                          where im.item_parent = gtt.item
                            and im.diff_1      = s.diff_id
                            and ril.dept       = im.dept
                            and ril.item       = im.item
                            and ril.loc        = gtt.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ LEADING(s) INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 ilex.item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 ilex.location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 ilex.zone_node_type,
                 s.diff_id,
                 s.zone_id zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from (select distinct im.item_parent,
                         s.varchar2_1 diff_id,
                         s.number_1 zone_id
                    from gtt_num_num_str_str_date_date s,
                         item_master im
                   where im.item = s.varchar2_2
                     and rownum  > 0) s,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.item           = s.item_parent
             and ilex.zone_id        = s.zone_id
             and ilex.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
             and NOT EXISTS (select 1
                               from rpm_promo_item_loc_expl_gtt gtt
                              where gtt.item           = ilex.item
                                and gtt.location       = ilex.location
                                and gtt.diff_id        = s.diff_id
                                and gtt.zone_node_type = ilex.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
             and EXISTS (select 1
                           from item_master im,
                                rpm_item_loc ril
                          where im.item_parent = ilex.item
                            and im.diff_1      = s.diff_id
                            and ril.dept       = im.dept
                            and ril.item       = im.item
                            and ril.loc        = ilex.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 cspfr.item,
                 cspfr.zone_node_type,
                 cspfr.location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 s.diff_id,
                 s.zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
            from (select distinct im.item_parent,
                         s.varchar2_1 diff_id,
                         s.number_1 zone_id
                    from gtt_num_num_str_str_date_date s,
                         item_master im
                   where im.item = s.varchar2_2
                     and rownum  > 0) s,
                 rpm_cust_segment_promo_fr_gtt cspfr
          where cspfr.item           = s.item_parent
            and cspfr.zone_id        = s.zone_id
            and cspfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
            and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
            and NOT EXISTS (select 1
                              from rpm_cust_segment_promo_fr_gtt gtt
                             where gtt.item           = cspfr.item
                               and gtt.location       = cspfr.location
                               and gtt.diff_id        = s.diff_id
                               and gtt.zone_node_type = cspfr.zone_node_type
                               and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
            and EXISTS (select 1
                          from item_master im,
                               rpm_item_loc ril
                         where im.item_parent = cspfr.item
                           and im.diff_1      = s.diff_id
                           and ril.dept       = im.dept
                           and ril.item       = im.item
                           and ril.loc        = cspfr.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_DIFF_ZONE;
--------------------------------------------------------------------------------

FUNCTION GENERATE_DIFF_LOC(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_DIFF_LOC';

BEGIN

   delete gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select t4.diff_id,
             t4.location,
             t4.item,
             t4.location
        from (select distinct t3.dept,
                     t3.item_parent,
                     t3.diff_id,
                     t3.location,
                     t3.item
                from (select t2.item_parent,
                             t2.diff_id,
                             t2.dept,
                             t2.class,
                             t2.subclass,
                             t2.count_date,
                             t2.item,
                             t2.location,
                             t2.count_pe,
                             ROW_NUMBER() OVER (PARTITION BY t2.item_parent,
                                                             t2.diff_id,
                                                             t2.location
                                                    ORDER BY t2.count_pe,
                                                             t2.cnt desc,
                                                             t2.count_lm,
                                                             t2.count_date,
                                                             t2.item) rank
                        from (select t1.item_parent,
                                     t1.diff_id,
                                     t1.zone_id,
                                     t1.dept,
                                     t1.class,
                                     t1.subclass,
                                     t1.count_date,
                                     t1.item,
                                     t1.location,
                                     (t1.count_pc + t1.count_cl + t1.count_pr) count_pe,
                                     COUNT(t1.count_date) OVER (PARTITION BY t1.item_parent,
                                                                             t1.diff_id,
                                                                             t1.location,
                                                                             t1.count_date) cnt,
                                     t1.count_lm
                                from (select fr.item_parent,
                                             fr.diff_id,
                                             fr.zone_id,
                                             fr.dept,
                                             fr.class,
                                             fr.subclass,
                                             fr.item,
                                             fr.location,
                                             SUM(case
                                                    when fr.price_change_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.diff_id,
                                                                         fr.location,
                                                                         fr.item) count_pc,
                                             SUM(case
                                                    when fr.clearance_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.diff_id,
                                                                         fr.location,
                                                                         fr.item) count_cl,
                                             SUM(fr.on_simple_promo_ind + fr.on_complex_promo_ind) OVER (PARTITION BY fr.item_parent,
                                                                                                                      fr.diff_id,
                                                                                                                      fr.zone_id,
                                                                                                                      fr.item,
                                                                                                                      fr.location) count_pr,
                                             COUNT(action_date) OVER (PARTITION BY fr.item_parent,
                                                                                   fr.diff_id,
                                                                                   fr.zone_id,
                                                                                   fr.item,
                                                                                   fr.location) count_date,
                                             SUM(case
                                                    when fr.location_move_id is NULL then
                                                       0
                                                    else
                                                       1
                                                 end) OVER (PARTITION BY fr.item_parent,
                                                                         fr.diff_id,
                                                                         fr.zone_id,
                                                                         fr.item,
                                                                         fr.location) count_lm
                                        from rpm_future_retail_gtt fr
                                       where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                         and fr.diff_id        is NOT NULL
                                         and fr.max_hier_level IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                                   RPM_CONSTANTS.FR_HIER_PARENT_LOC)) t1 ) t2 ) t3
               where t3.rank = 1) t4
       where NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept           = t4.dept
                            and rfr.item           = t4.item_parent
                            and rfr.diff_id        = t4.diff_id
                            and rfr.location       = t4.location
                            and rfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                            and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                            and rownum             = 1);

   -- Delete the 'DL' in which the item is the source of the 'PZ'.
   -- This is to make sure that the 'DL' is sourced from the same 'IL' that sourced the 'PZ'

   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1,
           varchar2_2) IN (select distinct im.diff_1,
                                  src.location,
                                  src.item
                             from rpm_parent_zone_src_gtt src,
                                  item_master im
                            where im.item        = src.item
                              and im.item_parent = src.item_parent
                              and im.diff_1      is NOT NULL);

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select distinct im.diff_1,
             src.location,
             src.item,
             src.location
        from rpm_parent_zone_src_gtt src,
             item_master im
       where im.item        = src.item
         and im.item_parent = src.item_parent
         and im.diff_1      is NOT NULL
         and NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = im.diff_1
                            and t.number_1   = src.location);

   -- Delete the 'DL' in which the item is the source of the 'DZ'.
   -- This is to make sure that the 'DL' is sourced from the same 'IL' that sourced the 'DZ'

   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1,
           varchar2_2) IN (select distinct diff_id,
                                  location,
                                  item
                             from rpm_diff_zone_src_gtt);

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select distinct src.diff_id,
             src.location,
             src.item,
             src.location
        from rpm_diff_zone_src_gtt src
       where NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = src.diff_id
                            and t.number_1   = src.location);

   -- Delete the 'DL' in which the item is the source of the 'PL'.
   -- This is to make sure that the 'DL' is sourced from the same 'IL' that sourced the 'PL'

   delete gtt_num_num_str_str_date_date
    where (varchar2_1,
           number_1,
           varchar2_2) IN (select distinct im.diff_1,
                                  src.location,
                                  src.item
                             from rpm_parent_loc_src_gtt src,
                                  item_master im
                            where im.item        = src.item
                              and im.item_parent = src.item_parent
                              and im.diff_1      is NOT NULL);

   insert into gtt_num_num_str_str_date_date
      (varchar2_1,
       number_1,
       varchar2_2,
       number_2)
      select distinct im.diff_1,
             src.location,
             src.item,
             src.location
        from rpm_parent_loc_src_gtt src,
             item_master im
       where im.item        = src.item
         and im.item_parent = src.item_parent
         and im.diff_1      is NOT NULL
         and NOT EXISTS (select 1
                           from gtt_num_num_str_str_date_date t
                          where t.varchar2_1 = im.diff_1
                            and t.number_1   = src.location);

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 gtt.item_parent item,
                 gtt.zone_node_type,
                 s.number_1 location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 s.varchar2_1 diff_id,
                 gtt.zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
            from rpm_future_retail_gtt gtt,
                 gtt_num_num_str_str_date_date s
           where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and gtt.item           = s.varchar2_2
             and gtt.location       = s.number_2) source
   on (    target.item           = source.item
       and target.diff_id        = source.diff_id
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 ilex.item_parent item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.number_1 location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 ilex.zone_node_type,
                 s.varchar2_1 diff_id,
                 ilex.zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from rpm_promo_item_loc_expl_gtt ilex,
                 gtt_num_num_str_str_date_date s
           where ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and ilex.item           = s.varchar2_2
             and ilex.location       = s.number_2
             and (   EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm
                              where rpdm.promo_dtl_id = ilex.promo_dtl_id
                                and (   (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                                         and rpdm.item       = ilex.item_parent)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                                         and rpdm.item       = ilex.item_parent
                                         and rpdm.diff_id    = s.varchar2_1)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class)
                                     or (    rpdm.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                                         and rpdm.dept       = ilex.dept
                                         and rpdm.class      = ilex.class
                                         and rpdm.subclass   = ilex.subclass)
                                     or rpdm.merch_type      = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM))
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_promo_dtl_skulist rpds
                              where rpdm1.promo_dtl_id  = ilex.promo_dtl_id
                                and rpdm1.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                and rpds.price_event_id = rpdm1.promo_dtl_id
                                and rpds.skulist        = rpdm1.skulist
                                and rpds.item           = ilex.item_parent)
                  or EXISTS (select 1
                               from rpm_promo_dtl_merch_node rpdm1,
                                    rpm_merch_list_detail rmld
                              where rpdm1.promo_dtl_id = ilex.promo_dtl_id
                                and rpdm1.merch_type   = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                and rmld.merch_list_id = rpdm1.price_event_itemlist
                                and rmld.item          = ilex.item_parent))
             and EXISTS (select 1
                           from rpm_promo_zone_location rpzl
                          where promo_dtl_id = ilex.promo_dtl_id
                            and (    zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                 and zone_id        = ilex.zone_id)
                             or (    zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                 and location       = ilex.location))) source
   on (    target.item           = source.item
       and target.diff_id        = source.diff_id
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 cspfr.item_parent item,
                 cspfr.zone_node_type,
                 s.number_1 location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 s.varchar2_1 diff_id,
                 cspfr.zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
            from rpm_cust_segment_promo_fr_gtt cspfr,
                 gtt_num_num_str_str_date_date s
           where cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             and cspfr.item           = s.varchar2_2
             and cspfr.location       = s.number_2) source
   on (    target.item           = source.item
       and target.diff_id        = source.diff_id
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   -- Verify if there is an existing 'IZ' timeline for the newly created 'DL' but no asscociated 'IL' timeline.
   -- then Create 'IL' timeline from 'IZ'

   merge into rpm_future_retail_gtt target
   using (select gtt.price_event_id,
                 gtt.dept,
                 gtt.class,
                 gtt.subclass,
                 gtt.item,
                 s.zone_node_type,
                 s.location,
                 gtt.action_date,
                 gtt.selling_retail,
                 gtt.selling_retail_currency,
                 gtt.selling_uom,
                 gtt.multi_units,
                 gtt.multi_unit_retail,
                 gtt.multi_unit_retail_currency,
                 gtt.multi_selling_uom,
                 gtt.clear_retail,
                 gtt.clear_retail_currency,
                 gtt.clear_uom,
                 gtt.simple_promo_retail,
                 gtt.simple_promo_retail_currency,
                 gtt.simple_promo_uom,
                 gtt.price_change_id,
                 gtt.price_change_display_id,
                 gtt.pc_exception_parent_id,
                 gtt.pc_change_type,
                 gtt.pc_change_amount,
                 gtt.pc_change_currency,
                 gtt.pc_change_percent,
                 gtt.pc_change_selling_uom,
                 gtt.pc_null_multi_ind,
                 gtt.pc_multi_units,
                 gtt.pc_multi_unit_retail,
                 gtt.pc_multi_unit_retail_currency,
                 gtt.pc_multi_selling_uom,
                 gtt.pc_price_guide_id,
                 gtt.clearance_id,
                 gtt.clearance_display_id,
                 gtt.clear_mkdn_index,
                 gtt.clear_start_ind,
                 gtt.clear_change_type,
                 gtt.clear_change_amount,
                 gtt.clear_change_currency,
                 gtt.clear_change_percent,
                 gtt.clear_change_selling_uom,
                 gtt.clear_price_guide_id,
                 gtt.loc_move_from_zone_id,
                 gtt.loc_move_to_zone_id,
                 gtt.location_move_id,
                 gtt.lock_version,
                 gtt.timeline_seq,
                 gtt.on_simple_promo_ind,
                 gtt.on_complex_promo_ind,
                 gtt.item_parent item_parent,
                 gtt.diff_id diff_id,
                 s.zone_id zone_id,
                 gtt.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select im.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         t.diff_id diff_id
                    from (select distinct varchar2_1 diff_id,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_future_retail_gtt gtt
           where gtt.item_parent    = s.item_parent
             and gtt.diff_id        = s.diff_id
             and gtt.location       = s.zone_id
             and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt fr
                              where fr.item           = gtt.item
                                and fr.location       = s.location
                                and fr.zone_node_type = s.zone_node_type
                                and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = gtt.item
                            and ril.dept = gtt.dept
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              timeline_seq,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.timeline_seq,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ LEADING(s) INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_GTT_I3) USE_NL(ilex) */
                 ilex.price_event_id,
                 ilex.item,
                 ilex.dept,
                 ilex.class,
                 ilex.subclass,
                 s.location,
                 ilex.promo_id,
                 ilex.promo_display_id,
                 ilex.promo_secondary_ind,
                 ilex.promo_comp_id,
                 ilex.comp_display_id,
                 ilex.promo_dtl_id,
                 ilex.type,
                 ilex.customer_type,
                 ilex.detail_secondary_ind,
                 ilex.detail_start_date,
                 ilex.detail_end_date,
                 ilex.detail_apply_to_code,
                 ilex.detail_change_type,
                 ilex.detail_change_amount,
                 ilex.detail_change_currency,
                 ilex.detail_change_percent,
                 ilex.detail_change_selling_uom,
                 ilex.detail_price_guide_id,
                 ilex.exception_parent_id,
                 s.zone_node_type,
                 ilex.item_parent,
                 ilex.diff_id,
                 s.zone_id zone_id,
                 ilex.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                 ilex.timebased_dtl_ind
            from (select im.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         t.diff_id diff_id
                    from (select distinct varchar2_1 diff_id,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.item_parent    = s.item_parent
             and ilex.diff_id        = s.diff_id
             and ilex.location       = s.zone_id
             and ilex.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and ilex.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and NOT EXISTS (select 1
                               from rpm_promo_item_loc_expl_gtt gtt
                              where gtt.item           = ilex.item
                                and gtt.location       = s.location
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = ilex.item
                            and ril.dept = ilex.dept
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              zone_node_type,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.zone_node_type,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              source.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select cspfr.price_event_id,
                 cspfr.item,
                 s.zone_node_type,
                 s.location,
                 cspfr.action_date,
                 cspfr.customer_type,
                 cspfr.dept,
                 cspfr.promo_retail,
                 cspfr.promo_retail_currency,
                 cspfr.promo_uom,
                 cspfr.complex_promo_ind,
                 cspfr.item_parent,
                 cspfr.diff_id,
                 s.zone_id zone_id,
                 cspfr.max_hier_level,
                 RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
            from (select im.item_parent,
                         t.location,
                         rzl.loc_type zone_node_type,
                         rz.zone_id,
                         t.diff_id diff_id
                    from (select distinct varchar2_1 diff_id,
                                 number_1 location,
                                 varchar2_2 item
                            from gtt_num_num_str_str_date_date) t,
                         item_master im,
                         rpm_merch_retail_def_expl mer,
                         rpm_zone_location rzl,
                         rpm_zone rz
                   where im.item          = t.item
                     and mer.dept         = im.dept
                     and mer.class        = im.class
                     and mer.subclass     = im.subclass
                     and rzl.location     = t.location
                     and rz.zone_id       = rzl.zone_id
                     and rz.zone_group_id = mer.regular_zone_group
                     and rownum           > 0) s,
                 rpm_cust_segment_promo_fr_gtt cspfr
           where cspfr.item_parent    = s.item_parent
             and cspfr.diff_id        = s.diff_id
             and cspfr.location       = s.zone_id
             and cspfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and NOT EXISTS (select 1
                               from rpm_cust_segment_promo_fr_gtt gtt
                              where gtt.item           = cspfr.item
                                and gtt.location       = s.location
                                and gtt.zone_node_type = s.zone_node_type
                                and gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
             and EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = cspfr.item
                            and ril.dept = cspfr.dept
                            and ril.loc  = s.location)) source
   on (    target.item           = source.item
       and target.location       = source.location
       and target.diff_id        = source.diff_id
       and target.zone_node_type = source.zone_node_type
       and target.cur_hier_level = source.cur_hier_level)
   when NOT MATCHED then
      insert (price_event_id,
              cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.price_event_id,
              RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GENERATE_DIFF_LOC;
--------------------------------------------------------------------------------

FUNCTION PUSH_BACK(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_GENERATE_ROLLUP_FR_SQL.PUSH_BACK';

BEGIN

   merge /*+ rowid(rfr) */ into rpm_future_retail rfr
   using rpm_future_retail_gtt rfrg
   on (rfr.rowid = rfrg.rfr_rowid)
   when NOT MATCHED then
      insert(future_retail_id,
             dept,
             class,
             subclass,
             item,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_exception_parent_id,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             on_simple_promo_ind,
             on_complex_promo_ind,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level)
      values(rfrg.future_retail_id,
             rfrg.dept,
             rfrg.class,
             rfrg.subclass,
             rfrg.item,
             rfrg.zone_node_type,
             rfrg.location,
             rfrg.action_date,
             rfrg.selling_retail,
             rfrg.selling_retail_currency,
             rfrg.selling_uom,
             rfrg.multi_units,
             rfrg.multi_unit_retail,
             rfrg.multi_unit_retail_currency,
             rfrg.multi_selling_uom,
             rfrg.clear_exception_parent_id,
             rfrg.clear_retail,
             rfrg.clear_retail_currency,
             rfrg.clear_uom,
             NVL(rfrg.simple_promo_retail, rfrg.clear_retail),
             NVL(rfrg.simple_promo_retail_currency, rfrg.clear_retail_currency),
             NVL(rfrg.simple_promo_uom, rfrg.clear_uom),
             rfrg.price_change_id,
             rfrg.price_change_display_id,
             rfrg.pc_exception_parent_id,
             rfrg.pc_change_type,
             rfrg.pc_change_amount,
             rfrg.pc_change_currency,
             rfrg.pc_change_percent,
             rfrg.pc_change_selling_uom,
             rfrg.pc_null_multi_ind,
             rfrg.pc_multi_units,
             rfrg.pc_multi_unit_retail,
             rfrg.pc_multi_unit_retail_currency,
             rfrg.pc_multi_selling_uom,
             rfrg.pc_price_guide_id,
             rfrg.clearance_id,
             rfrg.clearance_display_id,
             rfrg.clear_mkdn_index,
             rfrg.clear_start_ind,
             rfrg.clear_change_type,
             rfrg.clear_change_amount,
             rfrg.clear_change_currency,
             rfrg.clear_change_percent,
             rfrg.clear_change_selling_uom,
             rfrg.clear_price_guide_id,
             rfrg.loc_move_from_zone_id,
             rfrg.loc_move_to_zone_id,
             rfrg.location_move_id,
             NVL(rfrg.on_simple_promo_ind, 0),
             NVL(rfrg.on_complex_promo_ind, 0),
             rfrg.item_parent,
             rfrg.diff_id,
             rfrg.zone_id,
             rfrg.max_hier_level,
             rfrg.cur_hier_level);

   merge /*+ rowid(rpile) */ into rpm_promo_item_loc_expl rpile
   using rpm_promo_item_loc_expl_gtt rpileg
   on (rpile.rowid = rpileg.rpile_rowid)
   when NOT MATCHED then
      insert (promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              zone_node_type,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              timebased_dtl_ind)
      values (rpileg.promo_item_loc_expl_id,
              rpileg.item,
              rpileg.dept,
              rpileg.class,
              rpileg.subclass,
              rpileg.location,
              rpileg.zone_node_type,
              rpileg.promo_id,
              rpileg.promo_display_id,
              rpileg.promo_secondary_ind,
              rpileg.promo_comp_id,
              rpileg.comp_display_id,
              rpileg.promo_dtl_id,
              rpileg.type,
              rpileg.customer_type,
              rpileg.detail_secondary_ind,
              rpileg.detail_start_date,
              rpileg.detail_end_date,
              rpileg.detail_apply_to_code,
              rpileg.detail_change_type,
              rpileg.detail_change_amount,
              rpileg.detail_change_currency,
              rpileg.detail_change_percent,
              rpileg.detail_change_selling_uom,
              rpileg.detail_price_guide_id,
              rpileg.exception_parent_id,
              rpileg.item_parent,
              rpileg.diff_id,
              rpileg.zone_id,
              rpileg.max_hier_level,
              rpileg.cur_hier_level,
              rpileg.timebased_dtl_ind);

   merge into rpm_cust_segment_promo_fr target
   using rpm_cust_segment_promo_fr_gtt source
   on (target.rowid = source.cspfr_rowid)
   when NOT MATCHED then
      insert (cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.cust_segment_promo_id,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END PUSH_BACK;
--------------------------------------------------------------------------------

END RPM_GENERATE_ROLLUP_FR_SQL;
/
