CREATE OR REPLACE PACKAGE BODY RPM_CHUNK_CC_THREADING_SQL AS
-------------------------------------------------------------------------------
   LP_vdate                DATE   := GET_VDATE;
   LP_push_back_start_date DATE   := LP_vdate;
   LP_emergency_perm       NUMBER := NULL;

--------------------------------------------------------------------------------
FUNCTION RESET_PE_STATUS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_id       IN     NUMBER,
                         I_price_event_type     IN     VARCHAR2,
                         I_start_state          IN     VARCHAR2,
                         I_bulk_cc_pe_id        IN     NUMBER,
                         I_parent_thread_number IN     NUMBER,
                         I_thread_number        IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION UPDATE_PROMO_DETAIL_DATES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION PROCESS_PAYLOAD(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                         I_rib_trans_id         IN     NUMBER,
                         I_bulk_cc_pe_id        IN     NUMBER,
                         I_parent_thread_number IN     NUMBER,
                         I_thread_number        IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_FP_UOM(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_id   IN     NUMBER,
                         I_price_event_type IN     VARCHAR2,
                         I_bulk_cc_pe_id    IN     NUMBER,
                         I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION EXPL_TIMELINES_TO_IL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_id   IN     NUMBER,
                              I_bulk_cc_pe_id    IN     NUMBER,
                              I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION GENERATE_EXCLUSIONS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_type IN     VARCHAR2,
                             I_price_event_id   IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION SGE_OUTPUT(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION INIT_CHUNKS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                     I_bulk_cc_pe_id    IN     NUMBER,
                     I_pe_sequence_id   IN     NUMBER,
                     I_pe_thread_number IN     NUMBER,
                     I_transaction_id   IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_THREADING_SQL.INIT_CHUNKS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_price_event_id      NUMBER(15)   := NULL;
   L_price_event_type    VARCHAR2(3)  := NULL;
   L_start_state         VARCHAR2(60) := NULL;
   L_end_state           VARCHAR2(60) := NULL;

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_error        NUMBER                   := 0;

   L_pe_ids_area_diff    NUMBER := NULL;
   L_reset_on_clr_rmv_id NUMBER := NULL;

   cursor C_DATA is
      select rbcpt.price_event_id,
             rbcpt.price_event_type,
             rbcp.start_state,
             rbcp.end_state
        from rpm_bulk_cc_pe_thread rbcpt,
             rpm_bulk_cc_pe rbcp
       where rbcpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
         and rbcpt.thread_number               = I_pe_thread_number
         and rbcpt.parent_thread_number        = I_pe_sequence_id
         and rbcpt.man_txn_exclusion_parent_id is NULL
         and rbcp.bulk_cc_pe_id                = rbcpt.bulk_cc_pe_id;

   cursor C_AREA_DIFF is
      -- Item
      select rpc.price_change_id
        from rpm_price_change rpc,
             rpm_codes rcd,
             item_master im,
             rpm_area_diff_prim_expl adp
       where rpc.price_change_id      = L_price_event_id
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.link_code            is NULL
         and rcd.code_id              = rpc.reason_code
         and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                              RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE)
         and rpc.zone_id              = adp.zone_hier_id
         and rpc.item                 = im.item
         and im.dept                  = adp.dept
         and im.class                 = adp.class
         and im.subclass              = adp.subclass
      union all
      -- Item List
      select rpc.price_change_id
        from rpm_price_change rpc,
             rpm_price_change_skulist rpcs,
             rpm_codes rcd,
             item_master im,
             rpm_area_diff_prim_expl adp
       where rpc.price_change_id      = L_price_event_id
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.skulist              is NOT NULL
         and rpc.price_event_itemlist is NULL
         and rpc.link_code            is NULL
         and rcd.code_id              = rpc.reason_code
         and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                              RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE)
         and rpc.zone_id              = adp.zone_hier_id
         and rpc.skulist              = rpcs.skulist
         and rpcs.item                = im.item
         and im.dept                  = adp.dept
         and im.class                 = adp.class
         and im.subclass              = adp.subclass
      union all
      -- PEIL
      select rpc.price_change_id
        from rpm_price_change rpc,
             rpm_merch_list_detail rmld,
             rpm_codes rcd,
             item_master im,
             rpm_area_diff_prim_expl adp
       where rpc.price_change_id      = L_price_event_id
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.price_event_itemlist is NOT NULL
         and rpc.skulist              is NULL
         and rpc.link_code            is NULL
         and rcd.code_id              = rpc.reason_code
         and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                              RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE)
         and rpc.zone_id              = adp.zone_hier_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.item                = im.item
         and im.dept                  = adp.dept
         and im.class                 = adp.class
         and im.subclass              = adp.subclass
      union all
      -- Link Code
      select rpc.price_change_id
        from rpm_price_change rpc,
             rpm_codes rcd
       where rpc.price_change_id      = L_price_event_id
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.link_code            is NOT NULL
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rcd.code_id              = rpc.reason_code
         and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                              RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_pe_thread_number: '|| I_pe_thread_number);

   open C_DATA;
   fetch C_DATA into L_price_event_id,
                     L_price_event_type,
                     L_start_state,
                     L_end_state;
   close C_DATA;

   if VALIDATE_FP_UOM(O_cc_error_tbl,
                      L_price_event_id,
                      L_price_event_type,
                      I_bulk_cc_pe_id,
                      I_pe_thread_number) = 0 then
      L_error := 1;
   end if;

   if L_error = 0 then

      if RPM_BULK_CC_THREADING_SQL.LOCK_PE(O_cc_error_tbl,
                                           I_bulk_cc_pe_id,
                                           I_transaction_id,
                                           I_pe_sequence_id,
                                           I_pe_thread_number,
                                           OBJ_NUMERIC_ID_TABLE(L_price_event_id)) = 0 then

         L_error := 1;

      end if;

   end if;

   if L_error = 1 or
      (O_cc_error_tbl is NOT NULL and
       O_cc_error_tbl.COUNT > 0) then

      if RESET_PE_STATUS(L_cc_error_tbl,
                         L_price_event_id,
                         L_price_event_type,
                         L_start_state,
                         I_bulk_cc_pe_id,
                         I_pe_sequence_id,
                         I_pe_thread_number) = 0 then

         O_cc_error_tbl := L_cc_error_tbl;

         if L_error = 1 then
            return 0;
         else
            return 1;
         end if;

      end if;

      if RPM_FUTURE_RETAIL_SQL.POPULATE_CC_ERROR_TABLE(O_cc_error_tbl,
                                                       L_price_event_type) = 0 then
         return 0;
      end if;

      return 1;

   end if;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then
      open C_AREA_DIFF;
      fetch C_AREA_DIFF into L_pe_ids_area_diff;
      close C_AREA_DIFF;
   end if;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE and
      L_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE then
      ---
      L_reset_on_clr_rmv_id := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

   end if;

   update rpm_bulk_cc_pe_thread
      set chunk_price_event_payload_id = RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL,
          chunk_clearance_payload_id   = L_reset_on_clr_rmv_id,
          area_diff_ind                = case
                                            when L_pe_ids_area_diff is NOT NULL then
                                               1
                                            else
                                               0
                                         end
    where bulk_cc_pe_id               = I_bulk_cc_pe_id
      and parent_thread_number        = I_pe_sequence_id
      and thread_number               = I_pe_thread_number
      and man_txn_exclusion_parent_id is NULL;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_pe_thread_number: '|| I_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INIT_CHUNKS;

-----------------------------------------------------------------------------------
FUNCTION CHUNK_THREAD_ITEM_LOCS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                O_max_chunk           OUT NUMBER,
                                I_bulk_cc_pe_id    IN     NUMBER,
                                I_pe_sequence_id   IN     NUMBER,
                                I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_THREADING_SQL.CHUNK_THREAD_ITEM_LOCS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_thread_il_count NUMBER := NULL;
   L_price_event_id  NUMBER := NULL;
   L_max_chunk       NUMBER := NULL;

   L_chunk_no NUMBER := 1;
   L_total_tl NUMBER := 0;
   L_ranged   NUMBER := 0;
   L_index    NUMBER := 0;

   cursor C_RANGED is
      select 1
        from rpm_bulk_cc_pe_item i,
             rpm_bulk_cc_pe_location l
       where i.bulk_cc_pe_id  = I_bulk_cc_pe_id
         and i.price_event_id = L_price_event_id
         and l.bulk_cc_pe_id  = I_bulk_cc_pe_id
         and l.price_event_id = L_price_event_id
         and i.bulk_cc_pe_id  = l.bulk_cc_pe_id
         and i.price_event_id = l.price_event_id
         and i.itemloc_id     = l.itemloc_id
         and i.chunk_number   = L_index
         and EXISTS (select 1
                       from rpm_item_loc ril
                      where ril.dept = i.dept
                        and ril.item = i.item
                        and ril.loc  = l.location)
         and rownum           = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_pe_thread_number: '|| I_pe_thread_number);

   select thread_luw_count
     into L_thread_il_count
     from rpm_batch_control
    where program_name = 'com.retek.rpm.app.bulkcc.service.ChunkConflictCheckAppService';

   select price_event_id
     into L_price_event_id
     from rpm_bulk_cc_pe_thread
    where bulk_cc_pe_id               = I_bulk_cc_pe_id
      and parent_thread_number        = I_pe_sequence_id
      and thread_number               = I_pe_thread_number
      and man_txn_exclusion_parent_id is NULL;

   if NVL(L_thread_il_count, 0) = 0 then
      L_thread_il_count := RPM_CONSTANTS.THREAD_CHUNK_IL_COUNT;
   end if;

   merge into rpm_bulk_cc_pe_item target
   using (select price_event_id,
                 item_child,
                 chunk_number
            from (select price_event_id,
                         item_child,
                         chunk_number,
                         ROW_NUMBER() OVER (PARTITION BY price_event_id,
                                                         item_child
                                                ORDER BY item_child) rank
                    from (select price_event_id,
                                 item_child,
                                 parent_item,
                                 chunk_number
                            from rpm_bulk_cc_pe_thread rbcpt,
                                 rpm_bulk_cc_pe_item rbcpi,
                                 rpm_bulk_cc_pe_location rbcpl,
                                 item_master im
                           where rbcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                             and rbcpt.parent_thread_number = I_pe_sequence_id
                             and rbcpt.thread_number        = I_pe_thread_number
                             and rbcpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                             and rbcpt.bulk_cc_pe_id        = rbcpi.bulk_cc_pe_id
                             and rbcpl.bulk_cc_pe_id        = I_bulk_cc_pe_id
                             and rbcpt.bulk_cc_pe_id        = rbcpl.bulk_cc_pe_id
                             and rbcpt.price_event_id       = rbcpi.price_event_id
                             and rbcpt.price_event_id       = rbcpl.price_event_id
                             and rbcpi.bulk_cc_pe_id        = rbcpl.bulk_cc_pe_id
                             and rbcpi.price_event_id       = rbcpl.price_event_id
                             and rbcpi.itemloc_id           = rbcpl.itemloc_id
                             and rbcpi.item                 = im.item
                          model partition by (rbcpi.bulk_cc_pe_id)
                                dimension by (ROW_NUMBER() OVER (PARTITION BY rbcpi.bulk_cc_pe_id
                                                                     ORDER BY NVL2(rbcpi.item_parent, rbcpi.item_parent, rbcpi.item),
                                                                                   rbcpi.item) rn)
                                measures (COUNT(distinct rbcpi.item) OVER (PARTITION BY rbcpi.bulk_cc_pe_id,
                                                                                        NVL2(rbcpi.item_parent, rbcpi.item_parent, rbcpi.item)) item_count,
                                          rbcpi.price_event_id,
                                          DECODE(pe_merch_level,
                                                 6, NVL2(rbcpi.item_parent, im.item_parent, NVL(im.item_parent, rbcpi.item)),
                                                 NVL2(rbcpi.item_parent, rbcpi.item_parent, rbcpi.item))item_group,
                                          rbcpi.item item_child,
                                          rbcpi.item_parent parent_item,
                                          rbcpi.pe_merch_level,
                                          1 run_total,
                                          1 chunk_number)
                                 rules (run_total[rn] = case
                                                           when CV(rn) = 1 then
                                                              item_count[CV(rn)]
                                                           when item_group[CV(rn)] = item_group[CV(rn) - 1] then
                                                              run_total[CV(rn) - 1]
                                                           when item_count[CV(rn)] + run_total[CV(rn) - 1] > L_thread_il_count then
                                                              item_count[CV(rn)]
                                                           else
                                                              item_count[CV(rn)] + run_total[CV(rn) - 1]
                                                        end,
                                        chunk_number[rn] = case
                                                              when CV(rn) = 1 then
                                                                 1
                                                              when item_group[CV(rn)] = item_group[CV(rn) - 1] then
                                                                 chunk_number[CV(rn) - 1]
                                                              when item_count[CV(rn)] + run_total[CV(rn) - 1] > L_thread_il_count then
                                                                 chunk_number[CV(rn) - 1] + 1
                                                              else
                                                                 chunk_number[CV(rn) - 1]
                                                           end)))
             where rank = 1) source
   on (    target.bulk_cc_pe_id  = I_bulk_cc_pe_id
       and target.price_event_id = source.price_event_id
       and target.item           = source.item_child
       and target.thread_number  = I_pe_thread_number)
   when MATCHED then
      update
         set target.chunk_number = source.chunk_number;

   select MAX(chunk_number)
     into L_max_chunk
     from rpm_bulk_cc_pe_item
    where bulk_cc_pe_id  = I_bulk_cc_pe_id
      and price_event_id = L_price_event_id;

   -- This Logic here to check and make sure that the first chunk will always have ranged item/loc
   for i IN 1..L_max_chunk loop

      L_index := i;

      open C_RANGED;
      fetch C_RANGED into L_ranged;
      close C_RANGED;

      if NVL(L_ranged, 0) = 1 then

         if i = 1 then
            exit;

         else
            update rpm_bulk_cc_pe_item i
               set chunk_number = chunk_number * -1
             where i.bulk_cc_pe_id  = I_bulk_cc_pe_id
               and i.price_event_id = L_price_event_id
               and chunk_number     = 1;

            update rpm_bulk_cc_pe_item i
               set chunk_number = 1
             where i.bulk_cc_pe_id  = I_bulk_cc_pe_id
               and i.price_event_id = L_price_event_id
               and chunk_number     = i;

            update rpm_bulk_cc_pe_item i
               set chunk_number = i
             where i.bulk_cc_pe_id  = I_bulk_cc_pe_id
               and i.price_event_id = L_price_event_id
               and chunk_number     = -1;

            exit;
         end if;
      end if;
   end loop;

   O_max_chunk := L_max_chunk;

   for j IN 1..O_max_chunk loop

      insert into rpm_bulk_cc_pe_chunk (bulk_cc_pe_id,
                                        parent_thread_number,
                                        thread_number,
                                        chunk_number,
                                        status,
                                        push_back_status)
         values (I_bulk_cc_pe_id,
                 I_pe_sequence_id,
                 I_pe_thread_number,
                 j,
                 'I',
                 RPM_CONSTANTS.CC_STATUS_IN_PROGRESS);

   end loop;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_pe_thread_number: '|| I_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CHUNK_THREAD_ITEM_LOCS;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                   I_bulk_cc_pe_id    IN     NUMBER,
                   I_pe_sequence_id   IN     NUMBER,
                   I_thread_number    IN     NUMBER,
                   I_chunk_number     IN     NUMBER,
                   I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_THREADING_SQL.PUSH_BACK';

   L_end_date   DATE      := TO_DATE('3000', 'YYYY');
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number ||
                                      ' - I_price_event_type: '|| I_price_event_type);


   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      merge into rpm_clearance_reset rc
      using (select clearance_id,
                    clearance_display_id,
                    state,
                    reason_code,
                    item,
                    location,
                    zone_node_type,
                    effective_date,
                    out_of_stock_date,
                    create_date,
                    create_id,
                    approval_date,
                    approval_id,
                    rc_rowid
               from rpm_clearance_ws
              where reset_ind            = 1
                and bulk_cc_pe_id        = I_bulk_cc_pe_id
                and parent_thread_number = I_pe_sequence_id
                and thread_number        = I_thread_number
                and chunk_number         = I_chunk_number) rcg
      on (rc.clearance_id = rcg.clearance_id)
      when MATCHED then
         update
            set rc.state             = rcg.state,
                rc.approval_date     = rcg.approval_date,
                rc.approval_id       = rcg.approval_id,
                rc.effective_date    = rcg.effective_date,
                rc.out_of_stock_date = rcg.out_of_stock_date
      when NOT MATCHED then
         insert (clearance_id,
                 clearance_display_id,
                 state,
                 reason_code,
                 item,
                 location,
                 zone_node_type,
                 effective_date,
                 out_of_stock_date,
                 create_date,
                 create_id,
                 approval_date,
                 approval_id)
         values (rcg.clearance_id,
                 rcg.clearance_display_id,
                 rcg.state,
                 rcg.reason_code,
                 rcg.item,
                 rcg.location,
                 rcg.zone_node_type,
                 rcg.effective_date,
                 rcg.out_of_stock_date,
                 rcg.create_date,
                 rcg.create_id,
                 rcg.approval_date,
                 rcg.approval_id);

      merge into rpm_clearance rc
      using (select clearance_id,
                    clearance_display_id,
                    state,
                    reason_code,
                    exception_parent_id,
                    item,
                    diff_id,
                    zone_id,
                    location,
                    zone_node_type,
                    effective_date,
                    out_of_stock_date,
                    reset_date,
                    change_type,
                    change_amount,
                    change_currency,
                    change_percent,
                    change_selling_uom,
                    price_guide_id,
                    vendor_funded_ind,
                    funding_type,
                    funding_amount,
                    funding_amount_currency,
                    funding_percent,
                    supplier,
                    deal_id,
                    deal_detail_id,
                    partner_type,
                    partner_id,
                    create_date,
                    create_id,
                    approval_date,
                    approval_id,
                    rc_rowid
               from rpm_clearance_ws
              where reset_ind            = 0
                and bulk_cc_pe_id        = I_bulk_cc_pe_id
                and parent_thread_number = I_pe_sequence_id
                and thread_number        = I_thread_number
                and chunk_number         = I_chunk_number) rcg
      on (rc.clearance_id = rcg.clearance_id)
      when MATCHED then
         update
            set rc.state             = rcg.state,
                rc.approval_date     = rcg.approval_date,
                rc.approval_id       = rcg.approval_id,
                rc.effective_date    = rcg.effective_date,
                rc.out_of_stock_date = rcg.out_of_stock_date
      when NOT MATCHED then
         insert (clearance_id,
                 clearance_display_id,
                 state,
                 reason_code,
                 exception_parent_id,
                 item,
                 diff_id,
                 zone_id,
                 location,
                 zone_node_type,
                 effective_date,
                 out_of_stock_date,
                 reset_date,
                 change_type,
                 change_amount,
                 change_currency,
                 change_percent,
                 change_selling_uom,
                 price_guide_id,
                 vendor_funded_ind,
                 funding_type,
                 funding_amount,
                 funding_amount_currency,
                 funding_percent,
                 supplier,
                 deal_id,
                 deal_detail_id,
                 partner_type,
                 partner_id,
                 create_date,
                 create_id,
                 approval_date,
                 approval_id)
         values (rcg.clearance_id,
                 rcg.clearance_display_id,
                 rcg.state,
                 rcg.reason_code,
                 rcg.exception_parent_id,
                 rcg.item,
                 rcg.diff_id,
                 rcg.zone_id,
                 rcg.location,
                 rcg.zone_node_type,
                 rcg.effective_date,
                 rcg.out_of_stock_date,
                 rcg.reset_date,
                 rcg.change_type,
                 rcg.change_amount,
                 rcg.change_currency,
                 rcg.change_percent,
                 rcg.change_selling_uom,
                 rcg.price_guide_id,
                 rcg.vendor_funded_ind,
                 rcg.funding_type,
                 rcg.funding_amount,
                 rcg.funding_amount_currency,
                 rcg.funding_percent,
                 rcg.supplier,
                 rcg.deal_id,
                 rcg.deal_detail_id,
                 rcg.partner_type,
                 rcg.partner_id,
                 rcg.create_date,
                 rcg.create_id,
                 rcg.approval_date,
                 rcg.approval_id);
   end if;

   merge into rpm_promo_item_loc_expl rpile
   using (select promo_item_loc_expl_id,
                 item,
                 dept,
                 class,
                 subclass,
                 location,
                 zone_node_type,
                 promo_id,
                 promo_display_id,
                 promo_secondary_ind,
                 promo_comp_id,
                 comp_display_id,
                 promo_dtl_id,
                 type,
                 customer_type,
                 detail_secondary_ind,
                 detail_start_date,
                 detail_end_date,
                 detail_apply_to_code,
                 timebased_dtl_ind,
                 detail_change_type,
                 detail_change_amount,
                 detail_change_currency,
                 detail_change_percent,
                 detail_change_selling_uom,
                 detail_price_guide_id,
                 exception_parent_id,
                 deleted_ind,
                 rpile_rowid,
                 diff_id,
                 item_parent,
                 zone_id,
                 cur_hier_level,
                 max_hier_level
            from rpm_promo_item_loc_expl_ws
           where bulk_cc_pe_id            = I_bulk_cc_pe_id
             and parent_thread_number     = I_pe_sequence_id
             and thread_number            = I_thread_number
             and chunk_number             = I_chunk_number
             and (   TRUNC(detail_start_date) >= TRUNC(LP_push_back_start_date)
                  or rpile_rowid              is NULL
                  or I_price_event_type       IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE))) rpileg
      on (rpile.promo_item_loc_expl_id = rpileg.promo_item_loc_expl_id)
   when MATCHED then
      update
         set promo_id                  = rpileg.promo_id,
             promo_display_id          = rpileg.promo_display_id,
             promo_secondary_ind       = rpileg.promo_secondary_ind,
             promo_comp_id             = rpileg.promo_comp_id,
             comp_display_id           = rpileg.comp_display_id,
             promo_dtl_id              = rpileg.promo_dtl_id,
             type                      = rpileg.type,
             detail_secondary_ind      = rpileg.detail_secondary_ind,
             detail_start_date         = rpileg.detail_start_date,
             detail_end_date           = rpileg.detail_end_date,
             detail_apply_to_code      = rpileg.detail_apply_to_code,
             timebased_dtl_ind         = rpileg.timebased_dtl_ind,
             detail_change_type        = rpileg.detail_change_type,
             detail_change_amount      = rpileg.detail_change_amount,
             detail_change_currency    = rpileg.detail_change_currency,
             detail_change_percent     = rpileg.detail_change_percent,
             detail_change_selling_uom = rpileg.detail_change_selling_uom,
             detail_price_guide_id     = rpileg.detail_price_guide_id,
             exception_parent_id       = rpileg.exception_parent_id
   when NOT MATCHED then
      insert (promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              zone_node_type,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              timebased_dtl_ind,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              diff_id,
              item_parent,
              zone_id,
              cur_hier_level,
              max_hier_level)
      values (rpileg.promo_item_loc_expl_id,
              rpileg.item,
              rpileg.dept,
              rpileg.class,
              rpileg.subclass,
              rpileg.location,
              rpileg.zone_node_type,
              rpileg.promo_id,
              rpileg.promo_display_id,
              rpileg.promo_secondary_ind,
              rpileg.promo_comp_id,
              rpileg.comp_display_id,
              rpileg.promo_dtl_id,
              rpileg.type,
              rpileg.customer_type,
              rpileg.detail_secondary_ind,
              rpileg.detail_start_date,
              rpileg.detail_end_date,
              rpileg.detail_apply_to_code,
              rpileg.timebased_dtl_ind,
              rpileg.detail_change_type,
              rpileg.detail_change_amount,
              rpileg.detail_change_currency,
              rpileg.detail_change_percent,
              rpileg.detail_change_selling_uom,
              rpileg.detail_price_guide_id,
              rpileg.exception_parent_id,
              rpileg.diff_id,
              rpileg.item_parent,
              rpileg.zone_id,
              rpileg.cur_hier_level,
              rpileg.max_hier_level);

   merge into rpm_cust_segment_promo_fr target
   using (select cust_segment_promo_id,
                 item,
                 location,
                 zone_node_type,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 cspfr_rowid,
                 diff_id,
                 item_parent,
                 zone_id,
                 cur_hier_level,
                 max_hier_level
            from rpm_cust_segment_promo_fr_ws ws
           where (   TRUNC(action_date) >= TRUNC(LP_push_back_start_date)
                  or cspfr_rowid        is NULL)
             and bulk_cc_pe_id        = I_bulk_cc_pe_id
             and parent_thread_number = I_pe_sequence_id
             and thread_number        = I_thread_number
             and chunk_number         = I_chunk_number
             and promo_retail         is NOT NULL) source
   on (target.cust_segment_promo_id = source.cust_segment_promo_id)
   when MATCHED then
      update
         set target.customer_type         = source.customer_type,
             target.promo_retail          = source.promo_retail,
             target.promo_retail_currency = source.promo_retail_currency,
             target.promo_uom             = source.promo_uom,
             target.complex_promo_ind     = source.complex_promo_ind
   when NOT MATCHED then
      insert (cust_segment_promo_id,
              item,
              location,
              zone_node_type,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              diff_id,
              item_parent,
              zone_id,
              cur_hier_level,
              max_hier_level)
      values (source.cust_segment_promo_id,
              source.item,
              source.location,
              source.zone_node_type,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.diff_id,
              source.item_parent,
              source.zone_id,
              source.cur_hier_level,
              source.max_hier_level);

   merge into rpm_future_retail rfr
   using (select future_retail_id,
                 item,
                 dept,
                 class,
                 subclass,
                 zone_node_type,
                 location,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_exception_parent_id,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 on_simple_promo_ind,
                 on_complex_promo_ind,
                 price_change_id,
                 price_change_display_id,
                 pc_exception_parent_id,
                 pc_change_type,
                 pc_change_amount,
                 pc_change_currency,
                 pc_change_percent,
                 pc_change_selling_uom,
                 pc_null_multi_ind,
                 pc_multi_units,
                 pc_multi_unit_retail,
                 pc_multi_unit_retail_currency,
                 pc_multi_selling_uom,
                 pc_price_guide_id,
                 clearance_id,
                 clearance_display_id,
                 clear_mkdn_index,
                 clear_start_ind,
                 clear_change_type,
                 clear_change_amount,
                 clear_change_currency,
                 clear_change_percent,
                 clear_change_selling_uom,
                 clear_price_guide_id,
                 loc_move_from_zone_id,
                 loc_move_to_zone_id,
                 location_move_id,
                 rfr_rowid,
                 diff_id,
                 item_parent,
                 zone_id,
                 cur_hier_level,
                 max_hier_level
            from rpm_future_retail_ws
           where (   TRUNC(action_date) >= TRUNC(LP_push_back_start_date)
                  or rfr_rowid          is NULL)
             and bulk_cc_pe_id        = I_bulk_cc_pe_id
             and parent_thread_number = I_pe_sequence_id
             and thread_number        = I_thread_number
             and chunk_number         = I_chunk_number) rfrg
   on (rfr.future_retail_id = rfrg.future_retail_id)
   when MATCHED then
      update
         set rfr.selling_retail                = rfrg.selling_retail,
             rfr.selling_retail_currency       = rfrg.selling_retail_currency,
             rfr.selling_uom                   = rfrg.selling_uom,
             rfr.multi_units                   = rfrg.multi_units,
             rfr.multi_unit_retail             = rfrg.multi_unit_retail,
             rfr.multi_unit_retail_currency    = rfrg.multi_unit_retail_currency,
             rfr.multi_selling_uom             = rfrg.multi_selling_uom,
             rfr.clear_exception_parent_id     = rfrg.clear_exception_parent_id,
             rfr.clear_retail                  = rfrg.clear_retail,
             rfr.clear_retail_currency         = rfrg.clear_retail_currency,
             rfr.clear_uom                     = rfrg.clear_uom,
             rfr.simple_promo_retail           = NVL(rfrg.simple_promo_retail, rfrg.clear_retail),
             rfr.simple_promo_retail_currency  = NVL(rfrg.simple_promo_retail_currency, rfrg.clear_retail_currency),
             rfr.simple_promo_uom              = NVL(rfrg.simple_promo_uom, rfrg.clear_uom),
             rfr.on_simple_promo_ind           = NVL(rfrg.on_simple_promo_ind, rfr.on_simple_promo_ind),
             rfr.on_complex_promo_ind          = NVL(rfrg.on_complex_promo_ind, rfr.on_complex_promo_ind),
             rfr.price_change_id               = rfrg.price_change_id,
             rfr.price_change_display_id       = rfrg.price_change_display_id,
             rfr.pc_exception_parent_id        = rfrg.pc_exception_parent_id,
             rfr.pc_change_type                = rfrg.pc_change_type,
             rfr.pc_change_amount              = rfrg.pc_change_amount,
             rfr.pc_change_currency            = rfrg.pc_change_currency,
             rfr.pc_change_percent             = rfrg.pc_change_percent,
             rfr.pc_change_selling_uom         = rfrg.pc_change_selling_uom,
             rfr.pc_null_multi_ind             = rfrg.pc_null_multi_ind,
             rfr.pc_multi_units                = rfrg.pc_multi_units,
             rfr.pc_multi_unit_retail          = rfrg.pc_multi_unit_retail,
             rfr.pc_multi_unit_retail_currency = rfrg.pc_multi_unit_retail_currency,
             rfr.pc_multi_selling_uom          = rfrg.pc_multi_selling_uom,
             rfr.pc_price_guide_id             = rfrg.pc_price_guide_id,
             rfr.clearance_id                  = rfrg.clearance_id,
             rfr.clearance_display_id          = rfrg.clearance_display_id,
             rfr.clear_mkdn_index              = rfrg.clear_mkdn_index,
             rfr.clear_start_ind               = rfrg.clear_start_ind,
             rfr.clear_change_type             = rfrg.clear_change_type,
             rfr.clear_change_amount           = rfrg.clear_change_amount,
             rfr.clear_change_currency         = rfrg.clear_change_currency,
             rfr.clear_change_percent          = rfrg.clear_change_percent,
             rfr.clear_change_selling_uom      = rfrg.clear_change_selling_uom,
             rfr.clear_price_guide_id          = rfrg.clear_price_guide_id,
             rfr.loc_move_from_zone_id         = rfrg.loc_move_from_zone_id,
             rfr.loc_move_to_zone_id           = rfrg.loc_move_to_zone_id,
             rfr.location_move_id              = rfrg.location_move_id
   when NOT MATCHED then
      insert (future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_exception_parent_id,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              on_simple_promo_ind,
              on_complex_promo_ind,
              diff_id,
              item_parent,
              zone_id,
              cur_hier_level,
              max_hier_level)
      values (rfrg.future_retail_id,
              rfrg.dept,
              rfrg.class,
              rfrg.subclass,
              rfrg.item,
              rfrg.zone_node_type,
              rfrg.location,
              rfrg.action_date,
              rfrg.selling_retail,
              rfrg.selling_retail_currency,
              rfrg.selling_uom,
              rfrg.multi_units,
              rfrg.multi_unit_retail,
              rfrg.multi_unit_retail_currency,
              rfrg.multi_selling_uom,
              rfrg.clear_exception_parent_id,
              rfrg.clear_retail,
              rfrg.clear_retail_currency,
              rfrg.clear_uom,
              NVL(rfrg.simple_promo_retail, rfrg.clear_retail),
              NVL(rfrg.simple_promo_retail_currency, rfrg.clear_retail_currency),
              NVL(rfrg.simple_promo_uom, rfrg.clear_uom),
              rfrg.price_change_id,
              rfrg.price_change_display_id,
              rfrg.pc_exception_parent_id,
              rfrg.pc_change_type,
              rfrg.pc_change_amount,
              rfrg.pc_change_currency,
              rfrg.pc_change_percent,
              rfrg.pc_change_selling_uom,
              rfrg.pc_null_multi_ind,
              rfrg.pc_multi_units,
              rfrg.pc_multi_unit_retail,
              rfrg.pc_multi_unit_retail_currency,
              rfrg.pc_multi_selling_uom,
              rfrg.pc_price_guide_id,
              rfrg.clearance_id,
              rfrg.clearance_display_id,
              rfrg.clear_mkdn_index,
              rfrg.clear_start_ind,
              rfrg.clear_change_type,
              rfrg.clear_change_amount,
              rfrg.clear_change_currency,
              rfrg.clear_change_percent,
              rfrg.clear_change_selling_uom,
              rfrg.clear_price_guide_id,
              rfrg.loc_move_from_zone_id,
              rfrg.loc_move_to_zone_id,
              rfrg.location_move_id,
              NVL(rfrg.on_simple_promo_ind, 0),
              NVL(rfrg.on_complex_promo_ind, 0),
              rfrg.diff_id,
              rfrg.item_parent,
              rfrg.zone_id,
              rfrg.cur_hier_level,
              rfrg.max_hier_level);

   delete from rpm_future_retail rfr
    where rfr.rowid IN (select rfrg.rfr_rowid
                          from rpm_future_retail_ws rfrg
                         where rfrg.deleted_ind          = 1
                           and rfrg.bulk_cc_pe_id        = I_bulk_cc_pe_id
                           and rfrg.parent_thread_number = I_pe_sequence_id
                           and rfrg.thread_number        = I_thread_number
                           and rfrg.chunk_number         = I_chunk_number);

   delete from rpm_cust_segment_promo_fr cspf
    where cspf.rowid IN (select cspfg.cspfr_rowid
                           from rpm_cust_segment_promo_fr_ws cspfg,
                                rpm_promo_item_loc_expl_ws rpileg
                          where rpileg.deleted_ind          = 1
                            and rpileg.bulk_cc_pe_id        = I_bulk_cc_pe_id
                            and rpileg.parent_thread_number = I_pe_sequence_id
                            and rpileg.thread_number        = I_thread_number
                            and rpileg.chunk_number         = I_chunk_number
                            and cspfg.bulk_cc_pe_id         = I_bulk_cc_pe_id
                            and cspfg.parent_thread_number  = I_pe_sequence_id
                            and cspfg.thread_number         = I_thread_number
                            and cspfg.chunk_number          = I_chunk_number
                            and rpileg.dept                 = cspfg.dept
                            and rpileg.item                 = cspfg.item
                            and NVL(rpileg.diff_id, '-999') = NVL(cspfg.diff_id, '-999')
                            and rpileg.location             = cspfg.location
                            and rpileg.zone_node_type       = cspfg.zone_node_type
                            and rpileg.customer_type        is NOT NULL
                            and rpileg.customer_type        = cspfg.customer_type
                            and cspfg.action_date           BETWEEN rpileg.detail_start_date and
                                                                    NVL(rpileg.detail_end_date, L_end_date)
                            -- if the start date of the promo to be deleted is same as another promotions
                            -- end date plus 1, we don't want to delete that record.
                            and NOT EXISTS (select 'x'
                                              from rpm_promo_item_loc_expl_ws rpileg2
                                             where rpileg2.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                               and rpileg2.parent_thread_number = I_pe_sequence_id
                                               and rpileg2.thread_number        = I_thread_number
                                               and rpileg2.chunk_number         = I_chunk_number
                                               and rpileg2.dept                 = cspfg.dept
                                               and rpileg2.item                 = cspfg.item
                                               and NVL(rpileg2.diff_id, '-999') = NVL(cspfg.diff_id, '-999')
                                               and rpileg2.location             = cspfg.location
                                               and rpileg2.zone_node_type       = cspfg.zone_node_type
                                               and rpileg2.customer_type        is NOT NULL
                                               and rpileg2.customer_type        = cspfg.customer_type
                                               and NVL(rpileg2.deleted_ind, 0)  = 0
                                               and cspfg.action_date            BETWEEN rpileg2.detail_start_date and
                                                                                        NVL(rpileg2.detail_end_date, L_end_date) + RPM_CONSTANTS.ONE_MINUTE));

   delete from rpm_promo_item_loc_expl rpile
    where rpile.rowid IN (select rpileg.rpile_rowid
                            from rpm_promo_item_loc_expl_ws rpileg
                           where rpileg.deleted_ind          = 1
                             and rpileg.bulk_cc_pe_id        = I_bulk_cc_pe_id
                             and rpileg.parent_thread_number = I_pe_sequence_id
                             and rpileg.thread_number        = I_thread_number
                             and rpileg.chunk_number         = I_chunk_number);

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number ||
                               ' - I_price_event_type: '|| I_price_event_type,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK;
-------------------------------------------------------------------------------------------

FUNCTION UPDATE_CHUNK_STATUS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                             I_bulk_cc_pe_id        IN     NUMBER,
                             I_parent_thread_number IN     NUMBER,
                             I_pe_thread_number     IN     NUMBER,
                             I_pe_chunk_number      IN     NUMBER,
                             I_status               IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.UPDATE_CHUNK_STATUS';

   cursor C_LOCK is
      select status
        from rpm_bulk_cc_pe_chunk rpec
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and chunk_number         = I_pe_chunk_number
         and parent_thread_number = I_parent_thread_number
         and thread_number        = I_pe_thread_number
         for update of rpec.status wait 600;

BEGIN

   open C_LOCK;
   close C_LOCK;

   update rpm_bulk_cc_pe_chunk
      set status               = I_status
    where bulk_cc_pe_id        = I_bulk_cc_pe_id
      and chunk_number         = I_pe_chunk_number
      and parent_thread_number = I_parent_thread_number
      and thread_number        = I_pe_thread_number;

   if SQL%NOTFOUND then
      insert into rpm_bulk_cc_pe_chunk
         (bulk_cc_pe_id,
          parent_thread_number,
          thread_number,
          chunk_number,
          status,
          has_conflict)
      values
         (I_bulk_cc_pe_id,
          I_parent_thread_number,
          I_pe_thread_number,
          I_pe_chunk_number,
          I_status,
          NULL);
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_CHUNK_STATUS;
--------------------------------------------------------------------------------
FUNCTION RESET_PE_STATUS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_id       IN     NUMBER,
                         I_price_event_type     IN     VARCHAR2,
                         I_start_state          IN     VARCHAR2,
                         I_bulk_cc_pe_id        IN     NUMBER,
                         I_parent_thread_number IN     NUMBER,
                         I_thread_number        IN     NUMBER)
RETURN NUMBER IS

   L_program               VARCHAR2(50) := 'RPM_CHUNK_CC_THREADING_SQL.RESET_PE_STATUS';
   L_end_state             VARCHAR2(60) := NULL;
   L_old_promo_end_date    DATE         := NULL;
   L_old_timebased_dtl_ind NUMBER       := NULL;

BEGIN

   select end_state,
          old_promo_end_date,
          old_timebased_dtl_ind
     into L_end_state,
          L_old_promo_end_date,
          L_old_timebased_dtl_ind
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   --
   -- Set Back the state to the starting state
   --
   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      update /*+ INDEX(rpc PK_RPM_PRICE_CHANGE) */ rpm_price_change rpc
         set state = I_start_state
       where price_change_id = I_price_event_id;

   elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      update /*+ INDEX(rc PK_RPM_CLEARANCE) */ rpm_clearance rc
         set state = I_start_state
       where clearance_id = I_price_event_id;

   elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO)  then

      update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
         set state             = I_start_state,
             end_date          = DECODE(L_end_state,
                                        RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, L_old_promo_end_date,
                                        end_date),
             timebased_dtl_ind = NVL(L_old_timebased_dtl_ind, timebased_dtl_ind)
       where promo_dtl_id = I_price_event_id;

      update /*+ INDEX(rpd RPM_PROMO_DTL_I2) */ rpm_promo_dtl rpd
         set state             = I_start_state,
             end_date          = DECODE(L_end_state,
                                        RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, L_old_promo_end_date,
                                        end_date),
             timebased_dtl_ind = NVL(L_old_timebased_dtl_ind, timebased_dtl_ind)
       where exception_parent_id = I_price_event_id
         and man_txn_exclusion   = 1
         and I_price_event_type  IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO);

   elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
         set state              = I_start_state,
             end_date           = L_old_promo_end_date,
             timebased_dtl_ind  = NVL(L_old_timebased_dtl_ind, timebased_dtl_ind)
       where promo_dtl_id = I_price_event_id;

      update /*+ INDEX(rpd RPM_PROMO_DTL_I2) */ rpm_promo_dtl rpd
         set state              = I_start_state,
             end_date           = L_old_promo_end_date,
             timebased_dtl_ind  = NVL(L_old_timebased_dtl_ind, timebased_dtl_ind)
       where exception_parent_id = I_price_event_id
         and man_txn_exclusion   = 1
         and I_price_event_type  IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE);
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RESET_PE_STATUS;
-----------------------------------------------------------------------------------
FUNCTION ROLL_BACK(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                   I_price_event_type     IN     VARCHAR2,
                   I_bulk_cc_pe_id        IN     NUMBER,
                   I_price_event_id       IN     NUMBER,
                   I_thread_number        IN     NUMBER,
                   I_parent_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_THREADING_SQL.ROLL_BACK';

   L_price_event_payload_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_GET_PRICE_EVENT_PAYLOAD_IDS is
      select distinct rpep.price_event_payload_id
        from rpm_price_event_payload rpep,
             rpm_bulk_cc_pe_thread rbcpt
       where rpep.bulk_cc_pe_id        = I_bulk_cc_pe_id
         and rbcpt.bulk_cc_pe_id       = I_bulk_cc_pe_id
         and rbcpt.price_event_id      = I_price_event_id
         and rpep.parent_thread_number = rbcpt.parent_thread_number
         and rpep.thread_number        = rbcpt.thread_number;

BEGIN

   open C_GET_PRICE_EVENT_PAYLOAD_IDS;
   fetch C_GET_PRICE_EVENT_PAYLOAD_IDS BULK COLLECT into L_price_event_payload_ids;
   close C_GET_PRICE_EVENT_PAYLOAD_IDS;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      forall i IN 1..L_price_event_payload_ids.COUNT
         delete
           from rpm_price_chg_payload
          where price_event_payload_id = L_price_event_payload_ids(i);

      delete
        from rpm_pc_ticket_request
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and thread_number        = I_thread_number
         and parent_thread_number = I_parent_thread_number;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      forall i IN 1..L_price_event_payload_ids.COUNT
         delete
           from rpm_clearance_payload
          where price_event_payload_id = L_price_event_payload_ids(i);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      delete
        from rpm_promo_item_payload
       where promo_dtl_list_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                                  rpdlp.promo_dtl_list_payload_id
                                             from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_promo_dtl_list_payload rpdlp,
                                                  rpm_promo_dtl_list_grp_payload rpdgp,
                                                  rpm_promo_dtl_payload rpdp
                                            where rpdp.price_event_payload_id         = VALUE(ids)
                                              and rpdgp.promo_dtl_payload_id          = rpdp.promo_dtl_payload_id
                                              and rpdlp.promo_dtl_list_grp_payload_id = rpdgp.promo_dtl_list_grp_payload_id);

      delete
        from rpm_promo_disc_ldr_payload
       where promo_dtl_list_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                                  rpdlp.promo_dtl_list_payload_id
                                             from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_promo_dtl_list_payload rpdlp,
                                                  rpm_promo_dtl_list_grp_payload rpdgp,
                                                  rpm_promo_dtl_payload rpdp
                                            where rpdp.price_event_payload_id         = VALUE(ids)
                                              and rpdgp.promo_dtl_payload_id          = rpdp.promo_dtl_payload_id
                                              and rpdlp.promo_dtl_list_grp_payload_id = rpdgp.promo_dtl_list_grp_payload_id);

      delete
        from rpm_promo_dtl_mn_payload
       where promo_dtl_list_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                                  rpdlp.promo_dtl_list_payload_id
                                             from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_promo_dtl_list_payload rpdlp,
                                                  rpm_promo_dtl_list_grp_payload rpdgp,
                                                  rpm_promo_dtl_payload rpdp
                                            where rpdp.price_event_payload_id         = VALUE(ids)
                                              and rpdgp.promo_dtl_payload_id          = rpdp.promo_dtl_payload_id
                                              and rpdlp.promo_dtl_list_grp_payload_id = rpdgp.promo_dtl_list_grp_payload_id);

      delete
        from rpm_promo_dtl_list_payload
       where promo_dtl_list_grp_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                                      rpdgp.promo_dtl_list_grp_payload_id
                                                 from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                      rpm_promo_dtl_list_grp_payload rpdgp,
                                                      rpm_promo_dtl_payload rpdp
                                                where rpdp.price_event_payload_id = VALUE(ids)
                                                  and rpdgp.promo_dtl_payload_id  = rpdp.promo_dtl_payload_id);

      delete
        from rpm_promo_dtl_list_grp_payload
       where promo_dtl_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                             rpdp.promo_dtl_payload_id
                                        from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_promo_dtl_payload rpdp
                                       where rpdp.price_event_payload_id = VALUE(ids));

      delete
        from rpm_promo_location_payload
       where promo_dtl_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                             rpdp.promo_dtl_payload_id
                                        from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_promo_dtl_payload rpdp
                                       where rpdp.price_event_payload_id = VALUE(ids));

      delete
        from rpm_promo_item_loc_sr_payload
       where promo_dtl_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                             rpdp.promo_dtl_payload_id
                                        from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_promo_dtl_payload rpdp
                                       where rpdp.price_event_payload_id = VALUE(ids));

      delete
        from rpm_threshold_int_payload
       where promo_dtl_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                             rpdp.promo_dtl_payload_id
                                        from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_promo_dtl_payload rpdp
                                       where rpdp.price_event_payload_id = VALUE(ids));

      delete
        from rpm_fin_cred_dtl_payload
       where promo_dtl_payload_id IN (select /*+ CARDINALITY (ids, 10) */
                                             rpdp.promo_dtl_payload_id
                                        from table(cast(L_price_event_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_promo_dtl_payload rpdp
                                       where rpdp.price_event_payload_id = VALUE(ids));

      forall i IN 1..L_price_event_payload_ids.COUNT
         delete
           from rpm_promo_dtl_payload
          where price_event_payload_id = L_price_event_payload_ids(i);

   end if;

   forall i IN 1..L_price_event_payload_ids.COUNT
      delete
        from rpm_price_event_payload
       where price_event_payload_id = L_price_event_payload_ids(i);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END ROLL_BACK;
-----------------------------------------------------------------------------------
-- This function is called when Price Event is executed via Chunk.  When Price Event
-- is processed via Chunk, each chunk will have different unique payload id when the
-- Price Event generates a MOD Message.  This function will consolidate
-- the different payload id from different chunk into one new payload id.

FUNCTION PROCESS_PAYLOAD(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                         I_rib_trans_id         IN     NUMBER,
                         I_bulk_cc_pe_id        IN     NUMBER,
                         I_parent_thread_number IN     NUMBER,
                         I_thread_number        IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CHUNK_CC_THREADING_SQL.PROCESS_PAYLOAD';

   L_new_price_event_payload_id NUMBER(10) := NULL;

   L_rib_family     RPM_PRICE_EVENT_PAYLOAD.RIB_FAMILY%TYPE     := NULL;
   L_rib_type       RPM_PRICE_EVENT_PAYLOAD.RIB_TYPE%TYPE       := NULL;
   L_publish_status RPM_PRICE_EVENT_PAYLOAD.PUBLISH_STATUS%TYPE := NULL;

   L_price_event_payload_id     OBJ_NUMERIC_ID_TABLE                                      := NULL;
   L_promo_dtl_payload_ids      OBJ_NUMERIC_ID_TABLE                                      := NULL;
   L_promo_dtl_id               RPM_PROMO_DTL.PROMO_DTL_ID%TYPE                           := NULL;
   L_promo_dtl_list_grp_id      RPM_PROMO_DTL_LIST_GRP_PAYLOAD.PROMO_DTL_LIST_GRP_ID%TYPE := NULL;
   L_promo_dtl_list_id          RPM_PROMO_DTL_LIST_PAYLOAD.PROMO_DTL_LIST_ID%TYPE         := NULL;
   L_promo_dtl_list_payload_ids OBJ_NUMERIC_ID_TABLE                                      := NULL;

   TYPE TYP_ROWID IS TABLE OF ROWID;

   L_rowids TYP_ROWID := NULL;

   L_new_promo_dtl_payload_id  RPM_PROMO_DTL_PAYLOAD.PROMO_DTL_PAYLOAD_ID%TYPE           := NULL;
   L_new_promo_dtl_list_pay_id RPM_PROMO_DTL_LIST_PAYLOAD.PROMO_DTL_LIST_PAYLOAD_ID%TYPE := NULL;

   cursor C_PE_PAYLOAD is
      select distinct rib_family,
             rib_type,
             publish_status
        from rpm_price_event_payload
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_parent_thread_number
         and thread_number        = I_thread_number
         and rib_type             IN (RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD,
                                      RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD,
                                      RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD);

   cursor C_PRICE_CHANGE is
      select price_event_payload_id
        from rpm_price_event_payload
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_parent_thread_number
         and thread_number        = I_thread_number
         and rib_family           = L_rib_family
         and rib_type             = L_rib_type
         and publish_status       = L_publish_status;

   cursor C_CLEARANCE is
      select price_event_payload_id
        from rpm_price_event_payload
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_parent_thread_number
         and thread_number        = I_thread_number
         and rib_family           = L_rib_family
         and rib_type             = L_rib_type
         and publish_status       = L_publish_status;

   cursor C_PROMO_DTL is
      select distinct rpdp.promo_dtl_id
        from rpm_promo_dtl_payload rpdp,
             rpm_price_event_payload rpep
       where rpdp.price_event_payload_id = rpep.price_event_payload_id
         and rpep.bulk_cc_pe_id          = I_bulk_cc_pe_id
         and rpep.parent_thread_number   = I_parent_thread_number
         and rpep.thread_number          = I_thread_number
         and rpep.rib_family             = L_rib_family
         and rpep.rib_type               = L_rib_type
         and rpep.publish_status         = L_publish_status;

   cursor C_PROMO_DTL_PAYLOAD is
      select rpdp.promo_dtl_payload_id
        from rpm_promo_dtl_payload rpdp,
             rpm_price_event_payload rpep
       where rpdp.promo_dtl_id           = L_promo_dtl_id
         and rpdp.price_event_payload_id = rpep.price_event_payload_id
         and rpep.bulk_cc_pe_id          = I_bulk_cc_pe_id
         and rpep.parent_thread_number   = I_parent_thread_number
         and rpep.thread_number          = I_thread_number
         and rpep.rib_family             = L_rib_family
         and rpep.rib_type               = L_rib_type
         and rpep.publish_status         = L_publish_status
       order by rpdp.promo_dtl_payload_id;

   cursor C_NEW_PROMO_DTL_PAYLOAD_ID is
      select rpdp.promo_dtl_payload_id
        from rpm_promo_dtl_payload rpdp,
             rpm_price_event_payload rpep
       where rpdp.promo_dtl_id           = L_promo_dtl_id
         and rpdp.price_event_payload_id = rpep.price_event_payload_id
         and rpep.bulk_cc_pe_id          = I_bulk_cc_pe_id
         and rpep.parent_thread_number   = I_parent_thread_number
         and rpep.thread_number          = I_thread_number
         and rpep.rib_family             = L_rib_family
         and rpep.rib_type               = L_rib_type
         and rpep.publish_status         = L_publish_status
         and EXISTS (select pdlgp.promo_dtl_list_grp_payload_id
                       from rpm_promo_dtl_list_grp_payload pdlgp
                      where pdlgp.promo_dtl_payload_id = rpdp.promo_dtl_payload_id);

   cursor C_LIST_GRP is
      select /*+ CARDINALITY (ids 10) */
             distinct promo_dtl_list_grp_id
        from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list_grp_payload rpdlgp
       where rpdlgp.promo_dtl_payload_id = VALUE(ids);

   cursor C_LIST is
      select /*+ CARDINALITY (ids 10) */
             distinct rpdlp.promo_dtl_list_id
        from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list_grp_payload rpdlgp,
             rpm_promo_dtl_list_payload rpdlp
       where rpdlgp.promo_dtl_payload_id         = VALUE(ids)
         and rpdlgp.promo_dtl_list_grp_id        = L_promo_dtl_list_grp_id
         and rpdlp.promo_dtl_list_grp_payload_id = rpdlgp.promo_dtl_list_grp_payload_id;

   cursor C_LIST_PAYLOAD is
      select /*+ CARDINALITY (ids 10) */
             rpdlp.promo_dtl_list_payload_id
        from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list_payload rpdlp,
             rpm_promo_dtl_list_grp_payload rpdlgp
       where rpdlgp.promo_dtl_payload_id         = VALUE(ids)
         and rpdlgp.promo_dtl_list_grp_id        = L_promo_dtl_list_grp_id
         and rpdlp.promo_dtl_list_id             = L_promo_dtl_list_id
         and rpdlp.promo_dtl_list_grp_payload_id = rpdlgp.promo_dtl_list_grp_payload_id;

   cursor C_ILSR_ROWIDS is
      select rowid
        from rpm_promo_item_loc_sr_payload
       where promo_dtl_payload_id IN (select VALUE(ids)
                                        from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids
                                      minus
                                      select L_new_promo_dtl_payload_id
                                        from dual);

   cursor C_PC_TICKET_REQ_EXISTING is
      select t.price_change_id,
             t.loc,
             t.item,
             t.unit_retail,
             t.multi_units,
             t.multi_unit_retail,
             rptr.rowid t_rowid
        from (select price_change_id,
                     loc,
                     item,
                     unit_retail,
                     multi_units,
                     multi_unit_retail
                from rpm_pc_ticket_request
               where bulk_cc_pe_id||'*'||thread_number||'*'||parent_thread_number =
                        I_bulk_cc_pe_id||'*'||I_thread_number||'*'||I_parent_thread_number) t,
             rpm_pc_ticket_request rptr
       where t.price_change_id  = rptr.price_change_id
         and t.loc              = rptr.loc
         and t.item             = rptr.item
         and rptr.bulk_cc_pe_id||'*'||rptr.thread_number||'*'||rptr.parent_thread_number !=
                I_bulk_cc_pe_id||'*'||I_thread_number||'*'||I_parent_thread_number;

   TYPE PC_TICKET_REQUEST_TBL is table of C_PC_TICKET_REQ_EXISTING%ROWTYPE;

   L_pc_ticket_request_tbl PC_TICKET_REQUEST_TBL;

BEGIN
   -- For each Family/Type/Publish Status this function will combine the payload records from each chunk
   -- into one message structure.

   for rec IN C_PE_PAYLOAD loop

      L_new_price_event_payload_id := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;
      L_rib_family                 := rec.rib_family;
      L_rib_type                   := rec.rib_type;
      L_publish_status             := rec.publish_status;

      insert into rpm_price_event_payload
         (price_event_payload_id,
          transaction_id,
          rib_family,
          rib_type,
          publish_status,
          bulk_cc_pe_id,
          parent_thread_number,
          thread_number)
      values
         (L_new_price_event_payload_id,
          I_rib_trans_id,
          rec.rib_family,
          rec.rib_type,
          rec.publish_status,
          I_bulk_cc_pe_id,
          I_parent_thread_number,
          I_thread_number);

      open C_PRICE_CHANGE;
      fetch C_PRICE_CHANGE BULK COLLECT into L_price_event_payload_id;
      close C_PRICE_CHANGE;

      forall i IN 1..L_price_event_payload_id.COUNT
         update rpm_price_chg_payload
            set price_event_payload_id = L_new_price_event_payload_id
          where price_event_payload_id = L_price_event_payload_id(i);

      open C_PC_TICKET_REQ_EXISTING;
      fetch C_PC_TICKET_REQ_EXISTING BULK COLLECT into L_pc_ticket_request_tbl;
      close C_PC_TICKET_REQ_EXISTING;

      if L_pc_ticket_request_tbl is NOT NULL and
         L_pc_ticket_request_tbl.COUNT > 0 then

         --Update values from chunking into their original values if they exist
         forall i IN 1..L_pc_ticket_request_tbl.COUNT
            update rpm_pc_ticket_request
               set unit_retail       = L_pc_ticket_request_tbl(i).unit_retail,
                   multi_units       = L_pc_ticket_request_tbl(i).multi_units,
                   multi_unit_retail = L_pc_ticket_request_tbl(i).multi_unit_retail
             where rowid = L_pc_ticket_request_tbl(i).t_rowid;

         -- Remove only the chunking values that were merged above
         forall i IN 1..L_pc_ticket_request_tbl.COUNT
            delete
              from rpm_pc_ticket_request
             where bulk_cc_pe_id||'*'||thread_number||'*'||parent_thread_number =
                      I_bulk_cc_pe_id||'*'||I_thread_number||'*'||I_parent_thread_number
               and price_change_id      = L_pc_ticket_request_tbl(i).price_change_id
               and loc                  = L_pc_ticket_request_tbl(i).loc
               and item                 = L_pc_ticket_request_tbl(i).item;

      end if;

      L_price_event_payload_id := NULL;

      open C_CLEARANCE;
      fetch C_CLEARANCE BULK COLLECT into L_price_event_payload_id;
      close C_CLEARANCE;

      forall i IN 1..L_price_event_payload_id.COUNT
         update rpm_clearance_payload
            set price_event_payload_id = L_new_price_event_payload_id
          where price_event_payload_id = L_price_event_payload_id(i);

      for dtl IN C_PROMO_DTL loop

         L_promo_dtl_id := dtl.promo_dtl_id;

         open C_PROMO_DTL_PAYLOAD;
         fetch C_PROMO_DTL_PAYLOAD BULK COLLECT into L_promo_dtl_payload_ids;
         close C_PROMO_DTL_PAYLOAD;

         if L_promo_dtl_payload_ids is NOT NULL and
            L_promo_dtl_payload_ids.COUNT > 0 then

            open C_NEW_PROMO_DTL_PAYLOAD_ID;
            fetch C_NEW_PROMO_DTL_PAYLOAD_ID into L_new_promo_dtl_payload_id;
            close C_NEW_PROMO_DTL_PAYLOAD_ID;

            forall i IN 1..L_promo_dtl_payload_ids.COUNT
               update rpm_promo_location_payload
                  set promo_dtl_payload_id = L_new_promo_dtl_payload_id
                where promo_dtl_payload_id = L_promo_dtl_payload_ids(i);

            delete from rpm_promo_location_payload
             where promo_dtl_payload_id      = L_new_promo_dtl_payload_id
               and promo_location_payload_id IN (select promo_location_payload_id
                                                   from (select promo_location_payload_id,
                                                                RANK() OVER (PARTITION BY promo_dtl_payload_id,
                                                                                          location
                                                                                 ORDER BY promo_location_payload_id) rank
                                                           from rpm_promo_location_payload
                                                          where promo_dtl_payload_id = L_new_promo_dtl_payload_id)
                                                  where rank > 1);

            insert into rpm_promo_item_loc_sr_payload
               (promo_item_loc_sr_payload_id,
                promo_dtl_payload_id,
                item,
                location,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                effective_date,
                ref_promo_dtl_id)
            select RPM_PROMO_ITEM_LOC_SR_PAY_SEQ.NEXTVAL,
                   L_new_promo_dtl_payload_id,
                   item,
                   location,
                   selling_retail,
                   selling_retail_currency,
                   selling_uom,
                   effective_date,
                   ref_promo_dtl_id
              from rpm_promo_item_loc_sr_payload
             where promo_dtl_payload_id IN (select VALUE(ids)
                                              from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids
                                            minus
                                            select L_new_promo_dtl_payload_id
                                              from dual);

            open C_ILSR_ROWIDS;
            fetch C_ILSR_ROWIDS BULK COLLECT into L_rowids;
            close C_ILSR_ROWIDS;

            forall i IN 1..L_rowids.COUNT
               delete rpm_promo_item_loc_sr_payload
                where rowid = L_rowids(i);

            for rec1 IN C_LIST_GRP loop

               L_promo_dtl_list_grp_id := rec1.promo_dtl_list_grp_id;

               for rec2 IN C_LIST loop

                  L_promo_dtl_list_id := rec2.promo_dtl_list_id;

                  open C_LIST_PAYLOAD;
                  fetch C_LIST_PAYLOAD BULK COLLECT into L_promo_dtl_list_payload_ids;
                  close C_LIST_PAYLOAD;

                  if L_promo_dtl_list_payload_ids is NOT NULL and
                     L_promo_dtl_list_payload_ids.COUNT > 0 then

                     select promo_dtl_list_payload_id
                       into L_new_promo_dtl_list_pay_id
                       from rpm_promo_dtl_list_grp_payload grp,
                            rpm_promo_dtl_list_payload lst
                      where grp.promo_dtl_payload_id          = L_new_promo_dtl_payload_id
                        and grp.promo_dtl_list_grp_payload_id = lst.promo_dtl_list_grp_payload_id;

                     forall i IN 1..L_promo_dtl_list_payload_ids.COUNT
                        update rpm_promo_item_payload
                           set promo_dtl_list_payload_id = L_new_promo_dtl_list_pay_id
                         where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(i);

                     delete from rpm_promo_item_payload
                      where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(1)
                        and promo_item_payload_id     IN (select promo_item_payload_id
                                                            from (select promo_item_payload_id,
                                                                         RANK() OVER (PARTITION BY promo_dtl_list_payload_id,
                                                                                                   item
                                                                                          ORDER BY promo_item_payload_id) rank
                                                                    from rpm_promo_item_payload
                                                                   where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(1))
                                                           where rank > 1);

                     forall i IN 1..L_promo_dtl_list_payload_ids.COUNT
                        update rpm_promo_disc_ldr_payload
                           set promo_dtl_list_payload_id = L_new_promo_dtl_list_pay_id
                         where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(i);

                     delete from rpm_promo_disc_ldr_payload
                      where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(1)
                        and promo_disc_ldr_payload_id IN (select promo_disc_ldr_payload_id
                                                            from (select promo_disc_ldr_payload_id,
                                                                         RANK() OVER (PARTITION BY promo_dtl_list_payload_id,
                                                                                                   change_type,
                                                                                                   change_amount,
                                                                                                   change_currency,
                                                                                                   change_percent,
                                                                                                   change_selling_uom,
                                                                                                   qual_type,
                                                                                                   qual_value,
                                                                                                   duration
                                                                                          ORDER BY promo_disc_ldr_payload_id) rank
                                                                    from rpm_promo_disc_ldr_payload
                                                                   where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(1))
                                                           where rank > 1);

                     update rpm_promo_dtl_list_payload
                        set promo_dtl_list_grp_payload_id = (select promo_dtl_list_grp_payload_id
                                                               from rpm_promo_dtl_list_grp_payload
                                                              where promo_dtl_list_grp_id = L_promo_dtl_list_grp_id
                                                                and promo_dtl_payload_id  = L_new_promo_dtl_payload_id
                                                                and rownum                = 1)
                      where promo_dtl_list_payload_id = L_promo_dtl_list_payload_ids(1);

                     forall i IN 1..L_promo_dtl_list_payload_ids.COUNT
                        delete from rpm_promo_dtl_mn_payload
                         where promo_dtl_list_payload_id  = L_promo_dtl_list_payload_ids(i)
                           and promo_dtl_list_payload_id != L_new_promo_dtl_list_pay_id;

                     forall i IN 1..L_promo_dtl_list_payload_ids.COUNT
                        delete from rpm_promo_dtl_list_payload
                         where promo_dtl_list_payload_id  = L_promo_dtl_list_payload_ids(i)
                           and promo_dtl_list_payload_id != L_new_promo_dtl_list_pay_id;

                  end if;

               end loop;

               forall i IN 1..L_promo_dtl_payload_ids.COUNT
                  delete from rpm_promo_dtl_list_grp_payload
                   where promo_dtl_list_grp_id = rec1.promo_dtl_list_grp_id
                     and promo_dtl_payload_id  = L_promo_dtl_payload_ids(i)
                     and promo_dtl_payload_id != L_new_promo_dtl_payload_id;

            end loop;

            update rpm_promo_dtl_payload
               set price_event_payload_id = L_new_price_event_payload_id
             where promo_dtl_payload_id   = L_new_promo_dtl_payload_id
               and promo_dtl_id           = L_promo_dtl_id
               and price_event_payload_id IN (select price_event_payload_id
                                                from rpm_price_event_payload
                                               where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                                 and parent_thread_number = I_parent_thread_number
                                                 and thread_number        = I_thread_number
                                                 and rib_family           = L_rib_family
                                                 and rib_type             = L_rib_type
                                                 and publish_status       = L_publish_status);

            delete
              from rpm_threshold_int_payload
             where promo_dtl_payload_id IN (select promo_dtl_payload_id
                                              from rpm_promo_dtl_payload
                                             where promo_dtl_payload_id   IN (select VALUE(ids)
                                                                                from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids)
                                               and promo_dtl_id           = L_promo_dtl_id
                                               and price_event_payload_id != L_new_price_event_payload_id
                                               and price_event_payload_id IN (select price_event_payload_id
                                                                                from rpm_price_event_payload
                                                                               where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                                                                 and parent_thread_number = I_parent_thread_number
                                                                                 and thread_number        = I_thread_number
                                                                                 and rib_family           = L_rib_family
                                                                                 and rib_type             = L_rib_type
                                                                                 and publish_status       = L_publish_status));

            delete from rpm_promo_dtl_payload
             where promo_dtl_payload_id   IN (select VALUE(ids)
                                                from table(cast(L_promo_dtl_payload_ids as OBJ_NUMERIC_ID_TABLE)) ids)
               and promo_dtl_id            = L_promo_dtl_id
               and price_event_payload_id != L_new_price_event_payload_id
               and price_event_payload_id IN (select price_event_payload_id
                                                from rpm_price_event_payload
                                               where bulk_cc_pe_id        = I_bulk_cc_pe_id
                                                 and parent_thread_number = I_parent_thread_number
                                                 and thread_number        = I_thread_number
                                                 and rib_family           = L_rib_family
                                                 and rib_type             = L_rib_type
                                                 and publish_status       = L_publish_status);

         end if;

      end loop;

      delete from rpm_price_chg_payload
       where price_event_payload_id IN (select price_event_payload_id
                                          from rpm_price_event_payload
                                         where bulk_cc_pe_id           = I_bulk_cc_pe_id
                                           and parent_thread_number    = I_parent_thread_number
                                           and thread_number           = I_thread_number
                                           and rib_family              = rec.rib_family
                                           and rib_type                = rec.rib_type
                                           and publish_status          = rec.publish_status
                                           and price_event_payload_id != L_new_price_event_payload_id);

      delete from rpm_clearance_payload
       where price_event_payload_id IN (select price_event_payload_id
                                          from rpm_price_event_payload
                                         where bulk_cc_pe_id           = I_bulk_cc_pe_id
                                           and parent_thread_number    = I_parent_thread_number
                                           and thread_number           = I_thread_number
                                           and rib_family              = rec.rib_family
                                           and rib_type                = rec.rib_type
                                           and publish_status          = rec.publish_status
                                           and price_event_payload_id != L_new_price_event_payload_id);

      delete from rpm_price_event_payload
       where bulk_cc_pe_id           = I_bulk_cc_pe_id
         and parent_thread_number    = I_parent_thread_number
         and thread_number           = I_thread_number
         and rib_family              = rec.rib_family
         and rib_type                = rec.rib_type
         and publish_status          = rec.publish_status
         and price_event_payload_id != L_new_price_event_payload_id;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));

      return 0;

END PROCESS_PAYLOAD;
-----------------------------------------------------------------------------------

FUNCTION UPDATE_PROMO_DETAIL_DATES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_THREADING_SQL.UPDATE_PROMO_DETAIL_DATES';

BEGIN

   merge into rpm_promo_dtl target
   using (select /*+ CARDINALITY (ids 10) */
                 rpd.promo_dtl_id,
                 case
                    when TRUNC(rpd.start_date) < start_date_threshold then
                       start_date_threshold
                    else
                       rpd.start_date
                 end start_date,
                 case
                    when rpd.end_date is NULL then
                       rpd.end_date
                    when TRUNC(rpd.end_date) <= start_date_threshold then
                       start_date_threshold
                    else
                       rpd.end_date
                 end end_date
            from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl rpd,
                 (select case
                            when LP_emergency_perm = 1 then
                               LP_vdate
                            else
                               LP_vdate + price_change_processing_days
                         end start_date_threshold
                    from rpm_system_options) rs
           where rpd.promo_dtl_id             = VALUE(ids)
              or (    rpd.exception_parent_id = VALUE(ids)
                  and rpd.man_txn_exclusion   = 1)) source
      on (target.promo_dtl_id = source.promo_dtl_id)
   when MATCHED then
      update
         set target.start_date = source.start_date,
             target.end_date   = source.end_date;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_PROMO_DETAIL_DATES;
---------------------------------------------------------------------------------------

FUNCTION PROCESS_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                       I_bulk_cc_pe_id    IN     NUMBER,
                       I_pe_sequence_id   IN     NUMBER,
                       I_pe_thread_number IN     NUMBER,
                       I_pe_chunk_number  IN     NUMBER,
                       I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_THREADING_SQL.PROCESS_CHUNK';

   L_price_event_ids         OBJ_NUMERIC_ID_TABLE     := NULL;
   L_price_event_type        VARCHAR2(3)              := NULL;
   L_persist_ind             VARCHAR2(1)              := NULL;
   L_start_state             VARCHAR2(60)             := NULL;
   L_end_state               VARCHAR2(60)             := NULL;
   L_user_name               VARCHAR2(30)             := NULL;
   L_emergency_perm          NUMBER(1)                := NULL;
   L_secondary_bulk_cc_pe_id NUMBER(10)               := NULL;
   L_secondary_ind           NUMBER(1)                := NULL;
   L_any_chunk_has_err       NUMBER                   := 0;
   L_cc_error_tbl            CONFLICT_CHECK_ERROR_TBL := NULL;
   L_start_time              TIMESTAMP                := SYSTIMESTAMP;

   L_sys_option_tolerance RPM_SYSTEM_OPTIONS.SYS_GEN_EXCLUSION_TOLERANCE%TYPE := NULL;
   L_pe_id_tol_exceed     NUMBER(15)                                          := NULL;

   L_error_message VARCHAR2(255) := NULL;

   cursor C_ANY_CHUNK_HAS_ERR is
      select COUNT(1)
        from rpm_bulk_cc_pe_chunk
       where bulk_cc_pe_id            =  I_bulk_cc_pe_id
         and parent_thread_number     =  I_pe_sequence_id
         and thread_number            =  I_pe_thread_number
         and chunk_number            != I_pe_chunk_number
         and (   status               = 'E'
              or NVL(has_conflict, 0) = 1);

   cursor C_TX is
      select price_event_type,
             persist_ind,
             start_state,
             end_state,
             user_name,
             emergency_perm,
             secondary_bulk_cc_pe_id,
             secondary_ind
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_PE is
      select price_event_id
        from RPM_BULK_CC_PE_THREAD
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number;

    cursor C_TOLERANCE_EXCEED is
      select price_event_id
        from (select ((d.cc_count / h.price_event_total) * 100) percent,
                     h.price_event_id
                from rpm_cc_sys_gen_head_ws h,
                     (select SUM(tran_item_count) cc_count,
                             price_event_id
                        from (select distinct price_event_id,
                                     tran_item_count,
                                     future_retail_id,
                                     MIN(future_retail_id) OVER (PARTITION BY item,
                                                                              NVL(diff_id, '-999'),
                                                                              location,
                                                                              zone_node_type) min_fr_id
                                from rpm_cc_sys_gen_detail_ws
                               where bulk_cc_pe_id = I_bulk_cc_pe_id
                                 and sequence_id   = I_pe_sequence_id
                                 and thread_number = I_pe_thread_number)
                       where future_retail_id = min_fr_id
                       group by price_event_id) d
               where h.price_event_id = d.price_event_id) tol
       where tol.percent > L_sys_option_tolerance
      union all
      select distinct wsh.price_event_id
        from rpm_cc_sys_gen_head_ws wsh,
             rpm_cc_sys_gen_detail_ws wsd
       where wsh.tolerance_ind  = 0
         and wsd.price_event_id = wsh.price_event_id
         and wsd.bulk_cc_pe_id  = I_bulk_cc_pe_id
         and wsd.sequence_id    = I_pe_sequence_id
         and wsd.thread_number  = I_pe_thread_number;

   cursor C_COPY_SGE_TO_OUTPUT is
      select CONFLICT_CHECK_ERROR_REC(price_event_id,
                                      future_retail_id,
                                      error_type,
                                      error_string,
                                      cs_promo_fr_id)
        from rpm_cc_sys_gen_detail_ws
       where price_event_id = L_pe_id_tol_exceed;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_thread_number: '|| I_pe_thread_number ||
                                      ' - I_chunk_number: ' || I_pe_chunk_number);

   -- check to see if any previous chunk has error or conflict
   -- if so all the conflict check process should be skipped and the status of the current
   -- chunk should be set to 'S'kipped

   open  C_ANY_CHUNK_HAS_ERR;
   fetch C_ANY_CHUNK_HAS_ERR into L_any_chunk_has_err;
   close C_ANY_CHUNK_HAS_ERR;

   if L_any_chunk_has_err > 0 then
      if RPM_CHUNK_CC_THREADING_SQL.UPDATE_CHUNK_STATUS(O_cc_error_tbl,
                                                        I_bulk_cc_pe_id,
                                                        I_pe_sequence_id,
                                                        I_pe_thread_number,
                                                        I_pe_chunk_number,
                                                        'S') = 0 then
         return 0;
      end if;
   else

      select sys_gen_exclusion_tolerance
        into L_sys_option_tolerance
        from rpm_system_options;

      open C_TX;
      fetch C_TX into L_price_event_type,
                      L_persist_ind,
                      L_start_state,
                      L_end_state,
                      L_user_name,
                      L_emergency_perm,
                      L_secondary_bulk_cc_pe_id,
                      L_secondary_ind;
      close C_TX;

      open C_PE;
      fetch C_PE BULK COLLECT into L_price_event_ids;
      close C_PE;

      if RPM_CAPTURE_GTT_SQL.INIT_VARS(L_error_message,
                                       L_user_name,
                                       I_bulk_cc_pe_id,
                                       I_pe_sequence_id,
                                       I_pe_thread_number,
                                       I_pe_chunk_number) = 0 then

         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));

         return 0;

      end if;

      if RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_PRICE_EVENTS(O_cc_error_tbl,
                                                       I_bulk_cc_pe_id,
                                                       I_pe_sequence_id,
                                                       I_pe_thread_number,
                                                       I_pe_chunk_number,
                                                       L_price_event_ids,
                                                       L_price_event_type,
                                                       I_rib_trans_id,
                                                       L_persist_ind,
                                                       L_start_state,
                                                       L_end_state,
                                                       L_user_name,
                                                       L_emergency_perm,
                                                       L_secondary_bulk_cc_pe_id,
                                                       L_secondary_ind) = 0 then

         L_cc_error_tbl := O_cc_error_tbl;

         if RPM_CHUNK_CC_THREADING_SQL.UPDATE_CHUNK_STATUS(O_cc_error_tbl,
                                                           I_bulk_cc_pe_id,
                                                           I_pe_sequence_id,
                                                           I_pe_thread_number,
                                                           I_pe_chunk_number,
                                                           'E') = 0 then
            return 0;
         end if;

         O_cc_error_tbl := L_cc_error_tbl;

         update rpm_bulk_cc_pe_chunk
            set has_conflict = 0
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_pe_sequence_id
            and thread_number        = I_pe_thread_number
            and chunk_number         = I_pe_chunk_number;

         insert into rpm_chunk_con_check_tbl (bulk_cc_pe_id,
                                              parent_thread_number,
                                              thread_number,
                                              chunk_number,
                                              price_event_id,
                                              future_retail_id,
                                              error_type,
                                              error_string)
            select I_bulk_cc_pe_id,
                   I_pe_sequence_id,
                   I_pe_thread_number,
                   I_pe_chunk_number,
                   price_event_id,
                   future_retail_id,
                   error_type,
                   error_string
              from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err;

         return 0;

      elsif O_cc_error_tbl is NOT NULL and
            O_cc_error_tbl.COUNT > 0  and
            O_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then

         L_cc_error_tbl := O_cc_error_tbl;

         if RPM_CHUNK_CC_THREADING_SQL.UPDATE_CHUNK_STATUS(O_cc_error_tbl,
                                                           I_bulk_cc_pe_id,
                                                           I_pe_sequence_id,
                                                           I_pe_thread_number,
                                                           I_pe_chunk_number,
                                                           'C') = 0 then
            return 0;
         end if;

         update rpm_bulk_cc_pe_chunk
            set has_conflict = 1
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_pe_sequence_id
            and thread_number        = I_pe_thread_number
            and chunk_number         = I_pe_chunk_number;

         O_cc_error_tbl := L_cc_error_tbl;

         insert into rpm_chunk_con_check_tbl (bulk_cc_pe_id,
                                              parent_thread_number,
                                              thread_number,
                                              chunk_number,
                                              price_event_id,
                                              future_retail_id,
                                              error_type,
                                              error_string)
            select I_bulk_cc_pe_id,
                   I_pe_sequence_id,
                   I_pe_thread_number,
                   I_pe_chunk_number,
                   price_event_id,
                   future_retail_id,
                   error_type,
                   error_string
              from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err;

      else

         if RPM_CHUNK_CC_THREADING_SQL.UPDATE_CHUNK_STATUS(O_cc_error_tbl,
                                                           I_bulk_cc_pe_id,
                                                           I_pe_sequence_id,
                                                           I_pe_thread_number,
                                                           I_pe_chunk_number,
                                                           'C') = 0 then
            return 0;
         end if;

         update rpm_bulk_cc_pe_chunk
            set has_conflict = NULL
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_pe_sequence_id
            and thread_number        = I_pe_thread_number
            and chunk_number         = I_pe_chunk_number;

      end if;

      -- Copy the 'SGE_GTT' tables to the 'SGE_WS' tables
      insert into rpm_cc_sys_gen_head_ws(price_event_id,
                                         price_event_total,
                                         tolerance_ind)
         select gtt.price_event_id,
                gtt.price_event_total,
                gtt.tolerance_ind
           from rpm_cc_sys_gen_head_gtt gtt
          where NOT EXISTS (select 'x'
                              from rpm_cc_sys_gen_head_ws ws
                             where gtt.price_event_id = ws.price_event_id);

      insert into rpm_cc_sys_gen_detail_ws(price_event_id,
                                           item,
                                           diff_id,
                                           cur_hier_level,
                                           max_hier_level,
                                           dept,
                                           location,
                                           zone_node_type,
                                           action_date,
                                           future_retail_id,
                                           cs_promo_fr_id,
                                           error_type,
                                           error_string,
                                           bulk_cc_pe_id,
                                           thread_number,
                                           sequence_id,
                                           chunk_number,
                                           price_event_type,
                                           tran_item_count)
         select gtt.price_event_id,
                gtt.item,
                gtt.diff_id,
                gtt.cur_hier_level,
                gtt.max_hier_level,
                gtt.dept,
                gtt.location,
                gtt.zone_node_type,
                gtt.action_date,
                gtt.future_retail_id,
                gtt.cs_promo_fr_id,
                gtt.error_type,
                gtt.error_string,
                I_bulk_cc_pe_id,
                I_pe_thread_number,
                I_pe_sequence_id,
                I_pe_chunk_number,
                L_price_event_type,
                gtt.tran_item_count
           from rpm_cc_sys_gen_detail_gtt gtt;

      open C_TOLERANCE_EXCEED;
      fetch C_TOLERANCE_EXCEED into L_pe_id_tol_exceed;
      close C_TOLERANCE_EXCEED;

      -- The conflict errors exceeded tolerances, so no SGE will be created, and the CC errors will be outputted
      -- like a normal conflict error.
      if L_pe_id_tol_exceed is NOT NULL then

         -- update the SGE_WS to indicate that it exceeded the tolerance.
         update rpm_cc_sys_gen_head_ws
            set tolerance_ind = 0
          where price_event_id = L_pe_id_tol_exceed
            and tolerance_ind != 0;

         -- copy the contents of rpm_cc_sys_gen_detail_ws to the output collection.
         open C_COPY_SGE_TO_OUTPUT;
         fetch C_COPY_SGE_TO_OUTPUT BULK COLLECT into O_cc_error_tbl;
         close C_COPY_SGE_TO_OUTPUT;

         L_cc_error_tbl := O_cc_error_tbl;

         if RPM_CHUNK_CC_THREADING_SQL.UPDATE_CHUNK_STATUS(O_cc_error_tbl,
                                                           I_bulk_cc_pe_id,
                                                           I_pe_sequence_id,
                                                           I_pe_thread_number,
                                                           I_pe_chunk_number,
                                                           'C') = 0 then
            return 0;
         end if;

         update rpm_bulk_cc_pe_chunk
            set has_conflict = 1
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_pe_sequence_id
            and thread_number        = I_pe_thread_number
            and chunk_number         = I_pe_chunk_number;

         O_cc_error_tbl := L_cc_error_tbl;

         insert into rpm_chunk_con_check_tbl(bulk_cc_pe_id,
                                             parent_thread_number,
                                             thread_number,
                                             chunk_number,
                                             price_event_id,
                                             future_retail_id,
                                             error_type,
                                             error_string)
            select I_bulk_cc_pe_id,
                   I_pe_sequence_id,
                   I_pe_thread_number,
                   I_pe_chunk_number,
                   price_event_id,
                   future_retail_id,
                   error_type,
                   error_string
              from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err;

      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_thread_number: '|| I_pe_thread_number ||
                               ' - I_chunk_number: ' || I_pe_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CHUNK;
--------------------------------------------------------------------------------
FUNCTION PROCESS_THREAD(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                        O_purge_ws_tables     OUT NUMBER,
                        I_bulk_cc_pe_id    IN     NUMBER,
                        I_pe_sequence_id   IN     NUMBER,
                        I_pe_thread_number IN     NUMBER,
                        I_price_event_type IN     VARCHAR2,
                        I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CHUNK_CC_THREADING_SQL.PROCESS_THREAD';

   L_all_chunks_done         NUMBER       := 0;
   L_chunk_in_progress       NUMBER       := 0;
   L_thread_has_error_chunks NUMBER       := 0;
   L_end_state               VARCHAR2(60) := NULL;
   L_user_name               VARCHAR2(60) := NULL;

   L_price_event_id   NUMBER       := NULL;
   L_price_event_type VARCHAR2(3)  := NULL;
   L_start_state      VARCHAR2(60) := NULL;
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;

   L_cc_error_tbl         CONFLICT_CHECK_ERROR_TBL := NULL;
   L_deal_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;

   cursor C_GET_CHUNK_CONFLICTS is
      select NEW CONFLICT_CHECK_ERROR_REC(price_event_id,
                                          future_retail_id,
                                          error_type,
                                          error_string)
        from rpm_chunk_con_check_tbl
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and error_type           = RPM_CONSTANTS.CONFLICT_ERROR;

   cursor C_GET_CHUNK_ERRORS is
      select NEW CONFLICT_CHECK_ERROR_REC(price_event_id,
                                          future_retail_id,
                                          error_type,
                                          error_string)
        from rpm_chunk_con_check_tbl
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and error_type           = RPM_CONSTANTS.PLSQL_ERROR
         and rownum               = 1;

   cursor C_THREAD_HAS_ERROR_CHUNKS is
      select 1
        from rpm_bulk_cc_pe_chunk
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and (   status IN ('E',
                            'S')
              or (    status = 'C'
                  and has_conflict = 1));

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_thread_number: '|| I_pe_thread_number);

   O_purge_ws_tables := 0;

   while L_all_chunks_done = 0 loop

      select COUNT(1)
        into L_chunk_in_progress
        from rpm_bulk_cc_pe_chunk
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and status               = 'I';

      if L_chunk_in_progress > 0 then
         L_all_chunks_done := 0;
         dbms_lock.sleep(15);
      else
         L_all_chunks_done := 1;
      end if;
   end loop;

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_thread_number: '|| I_pe_thread_number ||
                                      ' - L_all_chunks_done: '|| L_all_chunks_done);

   open C_THREAD_HAS_ERROR_CHUNKS;
   fetch C_THREAD_HAS_ERROR_CHUNKS into L_thread_has_error_chunks;
   close C_THREAD_HAS_ERROR_CHUNKS;

   select price_event_type,
          start_state,
          end_state,
          user_name
     into L_price_event_type,
          L_start_state,
          L_end_state,
          L_user_name
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   select price_event_id
     into L_price_event_id
     from rpm_bulk_cc_pe_thread
    where bulk_cc_pe_id               = I_bulk_cc_pe_id
      and parent_thread_number        = I_pe_sequence_id
      and thread_number               = I_pe_thread_number
      and man_txn_exclusion_parent_id is NULL;

   if L_thread_has_error_chunks = 1 then

      if RESET_PE_STATUS(O_cc_error_tbl,
                         L_price_event_id,
                         L_price_event_type,
                         L_start_state,
                         I_bulk_cc_pe_id,
                         I_pe_sequence_id,
                         I_pe_thread_number) = 0 then
         return 0;
      end if;

      O_purge_ws_tables := 1;

      L_cc_error_tbl := O_cc_error_tbl;

      if ROLL_BACK(O_cc_error_tbl,
                   I_price_event_type,
                   I_bulk_cc_pe_id,
                   L_price_event_id,
                   I_pe_thread_number,
                   I_pe_sequence_id) = 0 then
         return 0;
      end if;

      O_cc_error_tbl := L_cc_error_tbl;

      open C_GET_CHUNK_ERRORS;
      fetch C_GET_CHUNK_ERRORS BULK COLLECT into O_cc_error_tbl;
      close C_GET_CHUNK_ERRORS;

      L_cc_error_tbl := O_cc_error_tbl;

      if L_cc_error_tbl is NULL or
         L_cc_error_tbl.COUNT = 0 then

         open C_GET_CHUNK_CONFLICTS;
         fetch C_GET_CHUNK_CONFLICTS BULK COLLECT into O_cc_error_tbl;
         close C_GET_CHUNK_CONFLICTS;

         L_cc_error_tbl := O_cc_error_tbl;

      end if;

      O_cc_error_tbl := L_cc_error_tbl;

      delete
        from rpm_pe_cc_lock
       where bulk_cc_pe_id      = I_bulk_cc_pe_id
         and rib_transaction_id = I_rib_trans_id
         and price_event_id     = L_price_event_id;

   end if;

   if L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) and
      L_thread_has_error_chunks = 0 and
      L_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
      ---
      L_deal_price_event_ids := OBJ_NUMERIC_ID_TABLE(L_price_event_id);

      if RPM_BULK_CC_ACTIONS_SQL.CREATE_OR_UPDATE_PROMO_DEAL(O_cc_error_tbl,
                                                             L_deal_price_event_ids,
                                                             L_user_name) = 0 then

         L_cc_error_tbl := O_cc_error_tbl;

         if RESET_PE_STATUS(O_cc_error_tbl,
                            L_price_event_id,
                            L_price_event_type,
                            L_start_state,
                            I_bulk_cc_pe_id,
                            I_pe_sequence_id,
                            I_pe_thread_number) = 0 then
            return 0;
         end if;

         O_purge_ws_tables := 1;

         if ROLL_BACK(O_cc_error_tbl,
                      I_price_event_type,
                      I_bulk_cc_pe_id,
                      L_price_event_id,
                      I_pe_thread_number,
                      I_pe_sequence_id) = 0 then
           return 0;
         end if;

         delete
           from rpm_pe_cc_lock
          where bulk_cc_pe_id      = I_bulk_cc_pe_id
            and rib_transaction_id = I_rib_trans_id
            and price_event_id     = L_price_event_id;

      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_thread_number: '|| I_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_THREAD;

--------------------------------------------------------------------------------

FUNCTION PURGE_CHUNK_WS_TABLES(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                               I_bulk_cc_pe_id        IN     NUMBER    DEFAULT NULL,
                               I_parent_thread_number IN     NUMBER    DEFAULT NULL,
                               I_thread_number        IN     NUMBER    DEFAULT NULL,
                               I_pending_cc_pe_ids    IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_THREADING_SQL.PURGE_CHUNK_WS_TABLES';

   L_max_chunk_number NUMBER    := 0;
   L_start_time       TIMESTAMP := SYSTIMESTAMP;

   L_truncate_fr_ws   VARCHAR2(40) := 'truncate table rpm_future_retail_ws';
   L_truncate_pile_ws VARCHAR2(45) := 'truncate table rpm_promo_item_loc_expl_ws';
   L_truncate_cspf_ws VARCHAR2(45) := 'truncate table rpm_cust_segment_promo_fr_ws';
   L_truncate_cl_ws   VARCHAR2(35) := 'truncate table rpm_clearance_ws';
   L_truncate_sgeh_ws VARCHAR2(40) := 'truncate table rpm_cc_sys_gen_head_ws';
   L_truncate_sged_ws VARCHAR2(40) := 'truncate table rpm_cc_sys_gen_detail_ws';

BEGIN

   if I_bulk_cc_pe_id is NULL and
      I_parent_thread_number is NULL and
      I_thread_number is NULL then

      if I_pending_cc_pe_ids is NULL or
         I_pending_cc_pe_ids.COUNT = 0 then

         EXECUTE IMMEDIATE L_truncate_fr_ws;
         EXECUTE IMMEDIATE L_truncate_pile_ws;
         EXECUTE IMMEDIATE L_truncate_cspf_ws;
         EXECUTE IMMEDIATE L_truncate_cl_ws;
         EXECUTE IMMEDIATE L_truncate_sgeh_ws;
         EXECUTE IMMEDIATE L_truncate_sged_ws;

      else

         delete rpm_future_retail_ws
          where bulk_cc_pe_id NOT IN (select VALUE(ids)
                                        from table(cast(I_pending_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

         delete rpm_promo_item_loc_expl_ws
          where bulk_cc_pe_id NOT IN (select VALUE(ids)
                                        from table(cast(I_pending_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

         delete rpm_cust_segment_promo_fr_ws
          where bulk_cc_pe_id NOT IN (select VALUE(ids)
                                        from table(cast(I_pending_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

         delete rpm_clearance_ws
          where bulk_cc_pe_id NOT IN (select VALUE(ids)
                                        from table(cast(I_pending_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

         delete rpm_cc_sys_gen_head_ws
          where price_event_id IN (select price_event_id
                                     from rpm_cc_sys_gen_detail_ws
                                     where bulk_cc_pe_id NOT IN (select VALUE(ids)
                                                                   from table(cast(I_pending_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids));

         delete rpm_cc_sys_gen_detail_ws
          where bulk_cc_pe_id NOT IN (select VALUE(ids)
                                        from table(cast(I_pending_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

      end if;

   else

      select MAX(chunk_number)
        into L_max_chunk_number
        from rpm_bulk_cc_pe_chunk
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_parent_thread_number
         and thread_number        = I_thread_number;

      -- All the WS table are partitioned by chunk number, so from a performance perspective,
      -- it's going to be faster to have a loop like this and include the chunk number in the
      -- where condition.

      for i IN 1..NVL(L_max_chunk_number, 0) loop

         delete
           from rpm_future_retail_ws
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_parent_thread_number
            and thread_number        = I_thread_number
            and chunk_number         = i;

         delete
           from rpm_promo_item_loc_expl_ws
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_parent_thread_number
            and thread_number        = I_thread_number
            and chunk_number         = i;

         delete
           from rpm_cust_segment_promo_fr_ws
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_parent_thread_number
            and thread_number        = I_thread_number
            and chunk_number         = i;

         delete
           from rpm_clearance_ws
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and parent_thread_number = I_parent_thread_number
            and thread_number        = I_thread_number
            and chunk_number         = i;

         delete
           from rpm_cc_sys_gen_head_ws
          where price_event_id IN (select price_event_id
                                     from rpm_cc_sys_gen_detail_ws
                                    where bulk_cc_pe_id = I_bulk_cc_pe_id
                                      and thread_number = I_thread_number
                                      and chunk_number  = i);

         delete
           from rpm_cc_sys_gen_detail_ws
          where bulk_cc_pe_id = I_bulk_cc_pe_id
            and thread_number = I_thread_number
            and chunk_number  = i;

      end loop;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: '|| I_parent_thread_number ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PURGE_CHUNK_WS_TABLES;
--------------------------------------------------------------------------------
FUNCTION PROCESS_PUSH_BACK_THREAD(O_cc_error_tbl             OUT CONFLICT_CHECK_ERROR_TBL,
                                  O_purge_ws_tables          OUT NUMBER,
                                  O_post_push_back_failed    OUT NUMBER,
                                  I_bulk_cc_pe_id         IN     NUMBER,
                                  I_pe_sequence_id        IN     NUMBER,
                                  I_pe_thread_number      IN     NUMBER,
                                  I_price_event_type      IN     VARCHAR2,
                                  I_rib_trans_id          IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_CHUNK_CC_THREADING_SQL.PROCESS_PUSH_BACK_THREAD';

   L_all_pb_chunks_done  NUMBER := 0;
   L_chunk_in_progress   NUMBER := 0;
   L_pb_has_error_chunks NUMBER := 0;

   L_persist_ind    VARCHAR2(1)              := NULL;
   L_start_state    VARCHAR2(60)             := NULL;
   L_end_state      VARCHAR2(60)             := NULL;
   L_cc_error_tbl   CONFLICT_CHECK_ERROR_TBL := NULL;
   L_price_event_id NUMBER                   := NULL;
   L_start_time     TIMESTAMP                := SYSTIMESTAMP;

   cursor C_GET_CHUNK_ERRORS is
      select NEW CONFLICT_CHECK_ERROR_REC(L_price_event_id,
                                          future_retail_id,
                                          RPM_CONSTANTS.CONFLICT_ERROR,
                                         'push_back_processing_error_status_pending')
        from rpm_chunk_con_check_tbl
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and error_type           = RPM_CONSTANTS.PLSQL_ERROR
         and rownum               = 1;

   cursor C_THREAD_HAS_ERROR_CHUNKS is
      select 1
        from rpm_bulk_cc_pe_chunk
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and status               = RPM_CONSTANTS.CC_STATUS_COMPLETED
         and push_back_status     = RPM_CONSTANTS.CC_STATUS_IN_ERROR;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: ' || I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_pe_thread_number: '|| I_pe_thread_number);

   O_purge_ws_tables := 0;
   O_post_push_back_failed := 0;

   while L_all_pb_chunks_done = 0 loop
      select COUNT(1)
        into L_chunk_in_progress
        from rpm_bulk_cc_pe_chunk
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and status               = RPM_CONSTANTS.CC_STATUS_COMPLETED
         and push_back_status     = RPM_CONSTANTS.CC_STATUS_IN_PROGRESS;

      if L_chunk_in_progress > 0 then
         dbms_lock.sleep(15);
      else
         L_all_pb_chunks_done := 1;
      end if;
   end loop;

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: ' || I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_pe_thread_number: '|| I_pe_thread_number ||
                                      ' - L_all_pb_chunks_done: '|| L_all_pb_chunks_done);

   open C_THREAD_HAS_ERROR_CHUNKS;
   fetch C_THREAD_HAS_ERROR_CHUNKS into L_pb_has_error_chunks;
   close C_THREAD_HAS_ERROR_CHUNKS;

   select price_event_id
     into L_price_event_id
     from rpm_bulk_cc_pe_thread
    where bulk_cc_pe_id               = I_bulk_cc_pe_id
      and parent_thread_number        = I_pe_sequence_id
      and thread_number               = I_pe_thread_number
      and man_txn_exclusion_parent_id is NULL;

   select persist_ind,
          start_state,
          end_state
     into L_persist_ind,
          L_start_state,
          L_end_state
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   if L_pb_has_error_chunks = 0 and
      L_persist_ind = 'Y' and
      ((I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE and
        L_end_state != RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) or
       (I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE and
        L_end_state != RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) or
       I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET or
       (I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) and
        L_end_state != RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE) or
       (I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE))) then

      if PROCESS_PE_POST_PUSH_BACK(O_cc_error_tbl,
                                   I_bulk_cc_pe_id,
                                   I_pe_sequence_id,
                                   I_pe_thread_number,
                                   I_rib_trans_id) = 0 then

         O_post_push_back_failed := 1;

         return 0;

      end if;

      O_purge_ws_tables := 1;

      --Handle the System Generated Exclusions
      if ((I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE and
           L_end_state        = RPM_CONSTANTS.PC_APPROVED_STATE_CODE) or
          (I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE and
           L_end_state        = RPM_CONSTANTS.PC_APPROVED_STATE_CODE) or
          (I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                  RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) and
           L_end_state        = RPM_CONSTANTS.PR_APPROVED_STATE_CODE)) then

         --Call this function to Generate Exlusions in case the 'SGE_WS' tables have anything to process.
         if GENERATE_EXCLUSIONS(O_cc_error_tbl,
                                I_price_event_type,
                                L_price_event_id) = 0 then

            O_post_push_back_failed := 1;

            return 0;

         end if;

         insert into rpm_cc_sys_gen_head_gtt(price_event_id,
                                             price_event_total,
                                             tolerance_ind)
            select distinct price_event_id,
                   1,
                   1
              from rpm_cc_sys_gen_detail_ws
             where price_event_id = L_price_event_id
               and bulk_cc_pe_id  = I_bulk_cc_pe_id
               and sequence_id    = I_pe_sequence_id
               and thread_number  = I_pe_thread_number;

         insert into rpm_cc_sys_gen_detail_gtt(price_event_id,
                                               dept,
                                               item,
                                               diff_id,
                                               cur_hier_level,
                                               max_hier_level,
                                               tran_item_count,
                                               location,
                                               zone_node_type,
                                               future_retail_id,
                                               cs_promo_fr_id,
                                               error_type,
                                               error_string,
                                               action_date)
            select price_event_id,
                   dept,
                   item,
                   diff_id,
                   cur_hier_level,
                   max_hier_level,
                   tran_item_count,
                   location,
                   zone_node_type,
                   future_retail_id,
                   cs_promo_fr_id,
                   error_type,
                   error_string,
                   action_date
              from rpm_cc_sys_gen_detail_ws
             where price_event_id = L_price_event_id
               and bulk_cc_pe_id  = I_bulk_cc_pe_id
               and sequence_id    = I_pe_sequence_id
               and thread_number  = I_pe_thread_number;

         if RPM_FUTURE_RETAIL_SQL.GENERATE_TIMELINE(O_cc_error_tbl,
                                                    I_bulk_cc_pe_id,
                                                    I_pe_sequence_id,
                                                    I_pe_thread_number) = 0 then
            O_post_push_back_failed := 1;
            return 0;
         end if;

         if RPM_FUTURE_RETAIL_SQL.PUSH_BACK(O_cc_error_tbl,
                                            I_price_event_type) = 0 then
            O_post_push_back_failed := 1;
            return 0;
         end if;

         --Call this function that will copy the SGE records back to the O_cc_error_table for binocular purposes.
         if SGE_OUTPUT(O_cc_error_tbl) = 0 then
            O_post_push_back_failed := 1;
            return 0;
         end if;

      elsif ((I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE and
              L_end_state        = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) or
             (I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE and
              L_end_state        = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE) or
             (I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) and
              L_end_state        = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE)) then

         --Call function to clean up the SGE since the parent price event is being unapproved.
         if RPM_FUTURE_RETAIL_SQL.CLEAN_SGE_ON_REMOVE(O_cc_error_tbl,
                                                      OBJ_NUMERIC_ID_TABLE(L_price_event_id),
                                                      I_price_event_type) = 0 then
            O_post_push_back_failed := 1;
            return 0;
         end if;
      end if;

   elsif L_pb_has_error_chunks = 1 then

      O_post_push_back_failed := 1;

      --Insert a con check error if there were any errored-out chunks
      insert into rpm_con_check_err(con_check_err_id,
                                    ref_class,
                                    ref_id,
                                    ref_display_id,
                                    error_date,
                                    message_key)
         select RPM_CON_CHECK_ERR_SEQ.NEXTVAL,
                RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                price_event_id,
                price_event_display_id,
                LP_vdate,
                'push_back_processing_error_status_pending'
           from rpm_bulk_cc_pe_thread
          where bulk_cc_pe_id               = I_bulk_cc_pe_id
            and man_txn_exclusion_parent_id is NULL;

      open C_GET_CHUNK_ERRORS;
      fetch C_GET_CHUNK_ERRORS BULK COLLECT into O_cc_error_tbl;
      close C_GET_CHUNK_ERRORS;

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         update /*+ INDEX(rpc PK_RPM_PRICE_CHANGE) */ rpm_price_change rpc
            set state = RPM_CONSTANTS.PC_PENDING_STATE_CODE
          where price_change_id = L_price_event_id;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

         update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
            set state = RPM_CONSTANTS.PR_PENDING_STATE_CODE
          where promo_dtl_id = L_price_event_id;

         update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
            set state = RPM_CONSTANTS.PR_PENDING_STATE_CODE
          where exception_parent_id = L_price_event_id;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                   RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

         update /*+ INDEX(rc PK_RPM_CLEARANCE) */ rpm_clearance rc
            set state = RPM_CONSTANTS.PC_PENDING_STATE_CODE
          where clearance_id = L_price_event_id;

      end if;

   elsif L_persist_ind != 'Y' and
         ((I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE and
           (L_end_state = RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE or
            (L_end_state = L_start_state and
             L_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE))) or
          (I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE and
           (L_end_state = RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE or
            (L_end_state = L_start_state and
             L_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE))) or
          (I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                  RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                  RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                  RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) and
           (L_end_state = RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE or
            (L_end_state = L_start_state and
             L_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE)))) then

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         update /*+ INDEX(rpc PK_RPM_PRICE_CHANGE) */ rpm_price_change rpc
            set state = L_end_state
          where price_change_id = L_price_event_id;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

         update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
            set state = L_end_state
          where promo_dtl_id = L_price_event_id;

         update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
            set state = L_end_state
          where exception_parent_id = L_price_event_id;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                   RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

         update /*+ INDEX(rc PK_RPM_CLEARANCE) */ rpm_clearance rc
            set state = L_end_state
          where clearance_id = L_price_event_id;

      end if;

      O_purge_ws_tables := 1;

      L_cc_error_tbl := O_cc_error_tbl;

      if ROLL_BACK(O_cc_error_tbl,
                   I_price_event_type,
                   I_bulk_cc_pe_id,
                   L_price_event_id,
                   I_pe_thread_number,
                   I_pe_sequence_id) = 0 then
         O_post_push_back_failed := 1;
         return 0;
      end if;

      delete
        from rpm_pe_cc_lock
       where bulk_cc_pe_id      = I_bulk_cc_pe_id
         and rib_transaction_id = I_rib_trans_id
         and price_event_id     = L_price_event_id;

      O_cc_error_tbl := L_cc_error_tbl;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: ' || I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_pe_thread_number: '|| I_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PUSH_BACK_THREAD;
--------------------------------------------------------------------------------

FUNCTION PROCESS_PUSH_BACK_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_bulk_cc_pe_id    IN     NUMBER,
                                 I_pe_sequence_id   IN     NUMBER,
                                 I_pe_thread_number IN     NUMBER,
                                 I_chunk_number     IN     NUMBER,
                                 I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_THREADING_SQL.PROCESS_PUSH_BACK_CHUNK';

   L_price_event_type VARCHAR2(3)  := NULL;
   L_persist_ind      VARCHAR2(1)  := NULL;
   L_end_state        VARCHAR2(60) := NULL;
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;

   cursor C_TX is
      select price_event_type,
             persist_ind,
             end_state
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

BEGIN

   open C_TX;
   fetch C_TX into L_price_event_type,
                   L_persist_ind,
                   L_end_state;
   close C_TX;

   -- Push Back only for Approving or Un-Approving

   if L_persist_ind = 'Y' and
      ((L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE and
        L_end_state != RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) or
       (L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE and
        L_end_state != RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) or
       L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET or
       (L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                               RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) and
        L_end_state != RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE)) then
      --
      if PUSH_BACK(O_cc_error_tbl,
                   I_bulk_cc_pe_id,
                   I_pe_sequence_id,
                   I_pe_thread_number,
                   I_chunk_number,
                   L_price_event_type) = 0 then

         return 0;

      end if;
   end if;

   update rpm_bulk_cc_pe_chunk
      set push_back_status = RPM_CONSTANTS.CC_STATUS_COMPLETED,
          transaction_id   = I_rib_trans_id
    where bulk_cc_pe_id        = I_bulk_cc_pe_id
      and parent_thread_number = I_pe_sequence_id
      and thread_number        = I_pe_thread_number
      and chunk_number         = I_chunk_number;

   return 1;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_thread_number: '|| I_pe_thread_number ||
                               ' - I_chunk_number: '|| I_chunk_number,
                   L_start_time);

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PUSH_BACK_CHUNK;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_PE_POST_PUSH_BACK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_bulk_cc_pe_id    IN     NUMBER,
                                   I_pe_sequence_id   IN     NUMBER,
                                   I_pe_thread_number IN     NUMBER,
                                   I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_CHUNK_CC_THREADING_SQL.PROCESS_PE_POST_PUSH_BACK';

   L_price_event_id          NUMBER       := NULL;
   L_price_event_type        VARCHAR2(3)  := NULL;
   L_persist_ind             VARCHAR2(1)  := NULL;
   L_start_state             VARCHAR2(60) := NULL;
   L_end_state               VARCHAR2(60) := NULL;
   L_user_name               VARCHAR2(30) := NULL;
   L_emergency_perm          NUMBER(1)    := NULL;
   L_secondary_bulk_cc_pe_id NUMBER(10)   := NULL;
   L_secondary_ind           NUMBER(1)    := NULL;
   L_secondary_count         NUMBER       := NULL;
   L_asynch_ind              NUMBER(1)    := NULL;
   L_need_il_explode_ind     NUMBER(1)    := NULL;
   L_new_promo_end_date      DATE         := NULL;
   L_pe_start_date           DATE         := NULL;
   L_need_secondary          NUMBER(1)    := NULL;
   L_area_diff_ind           NUMBER(1)    := NULL;
   L_man_txn_excl_exists     NUMBER(1)    := NULL;
   L_start_time              TIMESTAMP    := SYSTIMESTAMP;

   L_thread_processor_class RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE := NULL;
   L_auto_clean_ind         RPM_BULK_CC_PE.AUTO_CLEAN_IND%TYPE         := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                                      ' - I_thread_number: '|| I_pe_thread_number);

   select price_event_type,
          persist_ind,
          start_state,
          end_state,
          user_name,
          emergency_perm,
          secondary_bulk_cc_pe_id,
          secondary_ind,
          asynch_ind,
          need_il_explode_ind,
          thread_processor_class,
          auto_clean_ind,
          new_promo_end_date into L_price_event_type,
                                  L_persist_ind,
                                  L_start_state,
                                  L_end_state,
                                  L_user_name,
                                  L_emergency_perm,
                                  L_secondary_bulk_cc_pe_id,
                                  L_secondary_ind,
                                  L_asynch_ind,
                                  L_need_il_explode_ind,
                                  L_thread_processor_class,
                                  L_auto_clean_ind,
                                  L_new_promo_end_date
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   select price_event_id,
          area_diff_ind,
          price_event_start_date,
          man_txn_excl_exists into L_price_event_id,
                                   L_area_diff_ind,
                                   L_pe_start_date,
                                   L_man_txn_excl_exists
     from rpm_bulk_cc_pe_thread
    where bulk_cc_pe_id               = I_bulk_cc_pe_id
      and parent_thread_number        = I_pe_sequence_id
      and thread_number               = I_pe_thread_number
      and man_txn_exclusion_parent_id is NULL;

   if PROCESS_PAYLOAD(O_cc_error_tbl,
                      I_rib_trans_id,
                      I_bulk_cc_pe_id,
                      I_pe_sequence_id,
                      I_pe_thread_number) = 0 then
      return 0;
   end if;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if L_end_state = RPM_CONSTANTS.PC_APPROVED_STATE_CODE then

         if RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_CHANGE_INSERT(O_cc_error_tbl,
                                                           OBJ_NUMERIC_ID_TABLE(L_price_event_id)) = 0 then
            return 0;
         end if;

         -- Do Area Differential
         if L_secondary_ind = 0 and
            L_area_diff_ind = 1 then

            if RPM_AREA_DIFF_PRICE_CHANGE.CHUNK_APPLY_AREA_DIFFS_TO_PC(O_cc_error_tbl,
                                                                       L_price_event_id,
                                                                       I_bulk_cc_pe_id,
                                                                       L_user_name,
                                                                       I_pe_sequence_id,
                                                                       I_pe_thread_number) = FALSE then
               return 0;
            end if;

            select COUNT(1)
              into L_secondary_count
              from numeric_id_gtt;

            if L_secondary_count > 0 then

               insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                                  price_event_id,
                                                  price_event_type)
                  select L_secondary_bulk_cc_pe_id,
                         numeric_id,
                         RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                    from numeric_id_gtt;

               update rpm_bulk_cc_pe
                  set secondary_ind = 1
                where bulk_cc_pe_id = I_bulk_cc_pe_id;

               merge into rpm_bulk_cc_pe target
               using (select L_secondary_bulk_cc_pe_id bulk_cc_pe_id
                        from dual) source
               on (target.bulk_cc_pe_id = source.bulk_cc_pe_id)
               when MATCHED then
                  update
                     set secondary_bulk_cc_pe_id = NULL
               when NOT MATCHED then
                  insert (bulk_cc_pe_id,
                          price_event_type,
                          persist_ind,
                          start_state,
                          end_state,
                          user_name,
                          emergency_perm,
                          secondary_ind,
                          asynch_ind,
                          auto_clean_ind,
                          thread_processor_class,
                          need_il_explode_ind)
                  values (source.bulk_cc_pe_id,
                          L_price_event_type,
                          L_persist_ind,
                          L_start_state,
                          L_end_state,
                          L_user_name,
                          L_emergency_perm,
                          1,
                          L_asynch_ind,
                          L_auto_clean_ind,
                          L_thread_processor_class,
                          L_need_il_explode_ind);
            end if;--L_secondary_count
         end if;--L_secondary_ind

      elsif L_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE then

         if RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_CHANGE_REMOVE(O_cc_error_tbl,
                                                           OBJ_NUMERIC_ID_TABLE(L_price_event_id)) = 0 then
            return 0;

         end if;

         -- Do Area Differential
         if L_secondary_ind = 0 then

            if RPM_AREA_DIFF_PRICE_CHANGE.UNAPPROVE_AREA_DIFF_PC(O_cc_error_tbl,
                                                                 L_need_secondary,
                                                                 OBJ_NUMERIC_ID_TABLE(L_price_event_id),
                                                                 L_secondary_bulk_cc_pe_id) = FALSE then
               return 0;

            end if;

            if L_need_secondary = 1 then

               update rpm_bulk_cc_pe
                  set secondary_ind = 1
                where bulk_cc_pe_id = I_bulk_cc_pe_id;

               merge into rpm_bulk_cc_pe target
               using (select L_secondary_bulk_cc_pe_id bulk_cc_pe_id
                        from dual) source
               on (target.bulk_cc_pe_id = source.bulk_cc_pe_id)
               when MATCHED then
                  update
                     set secondary_bulk_cc_pe_id = NULL
               when NOT MATCHED then
                  insert (bulk_cc_pe_id,
                          price_event_type,
                          persist_ind,
                          start_state,
                          end_state,
                          user_name,
                          emergency_perm,
                          secondary_ind,
                          asynch_ind,
                          auto_clean_ind,
                          thread_processor_class,
                          need_il_explode_ind)
                  values (source.bulk_cc_pe_id,
                          L_price_event_type,
                          L_persist_ind,
                          L_start_state,
                          L_end_state,
                          L_user_name,
                          L_emergency_perm,
                          1,
                          L_asynch_ind,
                          L_auto_clean_ind,
                          L_thread_processor_class,
                          L_need_il_explode_ind);
             end if;--L_need_secondary

         else
            -- When unapproving a secondary area diff pc, detach the hierarchy
            -- Then the price changes are deleted
            if RPM_AREA_DIFF_PRICE_CHANGE.DELETE_AREA_DIFF_PC(O_cc_error_tbl,
                                                              OBJ_NUMERIC_ID_TABLE(L_price_event_id)) = FALSE then
               return 0;
            end if;

         end if;--L_secondary_ind

      end if;--L_end_state

      update /*+ INDEX(rpc PK_RPM_PRICE_CHANGE) */ rpm_price_change rpc
         set state         = case
                                when L_end_state = RPM_CONSTANTS.PC_APPROVED_STATE_CODE and
                                     L_pe_start_date <= LP_vdate then
                                   RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                else
                                   L_end_state
                             end,
             approval_date = DECODE(L_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, SYSDATE,
                                    NULL),
             approval_id   = DECODE(L_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, L_user_name,
                                    NULL)
       where price_change_id = L_price_event_id;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      if L_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE then

         if UPDATE_PROMO_DETAIL_DATES(O_cc_error_tbl,
                                      OBJ_NUMERIC_ID_TABLE(L_price_event_id)) = 0 then
            return 0;

         end if;
      end if;

      update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
         set state         = case
                                when L_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE and
                                     L_pe_start_date <= LP_vdate then
                                   ---
                                   RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                else
                                   TO_NUMBER(L_end_state)
                             end,
             approval_date = DECODE(L_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, approval_date,
                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, approval_date,
                                    NULL),
             approval_id   = DECODE(L_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_id, L_user_name),
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, approval_id,
                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, approval_id,
                                    NULL),
             end_date      = DECODE(L_end_state,
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, TO_DATE(CONCAT(TO_CHAR(LP_vdate, 'YYYYMMDD'), '23:59:59'), 'YYYYMMDDHH24:MI:SS'),
                                    end_date)
       where promo_dtl_id = L_price_event_id;

       if L_man_txn_excl_exists = 1 then

          update /*+ INDEX(rpd RPM_PROMO_DTL_I2) */ rpm_promo_dtl rpd
             set state         = case
                                    when L_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE and
                                         L_pe_start_date <= LP_vdate then
                                       ---
                                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                    else
                                       TO_NUMBER(L_end_state)
                                 end,
                 approval_date = DECODE(L_end_state,
                                        RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                        RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, approval_date,
                                        RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, approval_date,
                                        NULL),
                 approval_id   = DECODE(L_end_state,
                                        RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_id, L_user_name),
                                        RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, approval_id,
                                        RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, approval_id,
                                        NULL),
                 end_date      = DECODE(L_end_state,
                                        RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, TO_DATE(CONCAT(TO_CHAR(LP_vdate, 'YYYYMMDD'), '23:59:59'), 'YYYYMMDDHH24:MI:SS'),
                                        end_date)
           where exception_parent_id = L_price_event_id
             and man_txn_exclusion   = 1;

       end if;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
         set state    = L_end_state,
             end_date = L_new_promo_end_date
       where promo_dtl_id = L_price_event_id;

      if L_man_txn_excl_exists = 1 then

         update /*+ INDEX(rpd RPM_PROMO_DTL_I2) */ rpm_promo_dtl rpd
            set state    = L_end_state,
                end_date = L_new_promo_end_date
          where exception_parent_id   = L_price_event_id;

      end if;

   elsif L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      update /*+ INDEX(rc PK_RPM_CLEARANCE) */ rpm_clearance rc
         set state         = case
                                when L_end_state = RPM_CONSTANTS.PC_APPROVED_STATE_CODE and
                                     L_pe_start_date <= LP_vdate then
                                   RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                else
                                   L_end_state
                             end,
             approval_date = DECODE(L_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, SYSDATE,
                                    NULL),
             approval_id   = DECODE(L_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, L_user_name,
                                    NULL)
       where clearance_id = L_price_event_id;

   elsif L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET then

      update /*+ INDEX(rc PK_RPM_CLEARANCE) */ rpm_clearance rc
         set state = L_end_state
       where clearance_id = L_price_event_id;

   end if;

   if L_pe_start_date <= LP_vdate and
      L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE) then

      insert into rpm_chunk_cc_stage_pee (chunk_cc_stage_pee_id,
                                          price_event_type,
                                          price_event_id)
         values (RPM_CHUNK_CC_STAGE_PEE_SEQ.NEXTVAL,
                 DECODE(L_price_event_type,
                        RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION, RPM_CONSTANTS.PE_TYPE_PROMO_START,
                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO, RPM_CONSTANTS.PE_TYPE_PROMO_START,
                        L_price_event_type),
                 L_price_event_id);
   end if;

   delete
     from rpm_pe_cc_lock
    where bulk_cc_pe_id                = I_bulk_cc_pe_id
      and NVL(rib_transaction_id, -99) = NVL(I_rib_trans_id, -99)
      and price_event_id               = L_price_event_id;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: '|| I_pe_sequence_id ||
                               ' - I_thread_number: '|| I_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PE_POST_PUSH_BACK;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_FP_UOM(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_id   IN     NUMBER,
                         I_price_event_type IN     VARCHAR2,
                         I_bulk_cc_pe_id    IN     NUMBER,
                         I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CHUNK_CC_THREADING_SQL.VALIDATE_FP_UOM';

   L_error_rec      CONFLICT_CHECK_ERROR_REC               := NULL;
   L_uoms           OBJ_VARCHAR_ID_TABLE                   := OBJ_VARCHAR_ID_TABLE();
   L_is_unique_uom  RPM_SYSTEM_OPTIONS.IS_UNIQUE_UOM%TYPE  := 0;
   L_uom_value      RPM_SYSTEM_OPTIONS.UOM_VALUE%TYPE      := NULL;

   L_promo_dtl_disc_ladder_id  RPM_PROMO_DTL_DISC_LADDER.PROMO_DTL_DISC_LADDER_ID%TYPE  := NULL;

   L_merch_nd_zone_nd_date_tbl OBJ_MERCH_ND_ZONE_ND_DATE_TBL := OBJ_MERCH_ND_ZONE_ND_DATE_TBL();
   L_merch_nd_zone_nd_date_rec OBJ_MERCH_ND_ZONE_ND_DATE_REC := NULL;

   cursor C_FP_PC is
      select OBJ_MERCH_ND_ZONE_ND_DATE_REC(rpc.zone_node_type,
                                           rpc.location,
                                           rpc.zone_id,
                                           NULL, --merch_type
                                           NULL, --dept
                                           NULL, --class
                                           NULL, --subclass
                                           rpc.item,
                                           rpc.diff_id,
                                           rpc.skulist,
                                           rpc.effective_date,
                                           rpc.price_event_itemlist) rec_uom
        from rpm_price_change rpc
       where rpc.price_change_id = I_price_event_id
         and change_selling_uom  is NULL
         and rpc.change_type     = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE;

   cursor C_FP_PROMO is
      select rpddl.promo_dtl_disc_ladder_id,
             OBJ_MERCH_ND_ZONE_ND_DATE_REC(rpzl.zone_node_type,
                                           rpzl.location,
                                           rpzl.zone_id,
                                           rpdm.merch_type,
                                           rpdm.dept,
                                           rpdm.class,
                                           rpdm.subclass,
                                           rpdm.item,
                                           rpdm.diff_id,
                                           rpdm.skulist,
                                           rpd.start_date,
                                           rpdm.price_event_itemlist) rec_uom
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdm,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl
       where rpd.promo_dtl_id           = I_price_event_id
         and rpzl.promo_dtl_id          = rpd.promo_dtl_id
         and rpdm.promo_dtl_id          = rpd.promo_dtl_id
         and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
         and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
         and rpddl.change_selling_uom   is NULL
         and rpddl.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE;

   cursor C_CHECK_UOM is
      select distinct t.selling_uom
        from (-- Merch Hier / Location level
              select distinct FIRST_VALUE (fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                              fr.location
                                                                     ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im
               where gtt.item        is NULL
                 and gtt.diff_id     is NULL
                 and gtt.zone_id     is NULL
                 and fr.dept         = gtt.dept
                 and fr.class        = NVL(gtt.class, im.class)
                 and fr.subclass     = NVL(gtt.subclass, im.subclass)
                 and fr.location     = gtt.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.tran_level   = im.item_level
                 and im.item         = fr.item
              union all
              -- Item or Parent Item / Location level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im
               where gtt.item        is NOT NULL
                 and gtt.zone_id     is NULL
                 and gtt.diff_id     is NULL
                 and im.item         = fr.item
                 and fr.location     = gtt.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and (   im.item        = gtt.item
                      or im.item_parent = gtt.item)
              union all
              --      Parent Diff/Location level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im
               where gtt.item        is NOT NULL
                 and gtt.zone_id     is NULL
                 and gtt.diff_id     is NOT NULL
                 and im.item_parent  = gtt.item
                 and im.item         = fr.item
                 and fr.location     = gtt.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and (   im.diff_1   = gtt.diff_id
                      or im.diff_2   = gtt.diff_id
                      or im.diff_3   = gtt.diff_id
                      or im.diff_4   = gtt.diff_id)
              union all
              --   Link Code/Location Level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_link_code_attribute rlca,
                     item_master im
               where gtt.item        is NOT NULL
                 and gtt.zone_id     is NULL
                 and gtt.item        = to_char(rlca.link_code)
                 and im.item         = rlca.item
                 and im.item         = fr.item
                 and fr.location     = rlca.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              --  Merch Hier/Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item        is NULL
                 and gtt.diff_id     is NULL
                 and gtt.zone_id     is NOT NULL
                 and fr.dept         = gtt.dept
                 and fr.class        = NVL(gtt.class, im.class)
                 and fr.subclass     = NVL(gtt.subclass, im.subclass)
                 and fr.location     = rzl.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.tran_level   = im.item_level
                 and im.item         = fr.item
                 and rzl.zone_id     = gtt.zone_id
                 and gtt.location    is NULL
              union all
              -- Item or Parent Item/Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item        is NOT NULL
                 and gtt.diff_id     is NULL
                 and gtt.zone_id     is NOT NULL
                 and fr.dept         = im.dept
                 and fr.class        = im.class
                 and fr.subclass     = im.subclass
                 and fr.item         = im.item
                 and fr.location     = rzl.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and (   im.item        = gtt.item
                      or im.item_parent = gtt.item)
                 and rzl.zone_id     = gtt.zone_id
                 and gtt.location    is NULL
              union all
              -- Parent Diff/ Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item        is NOT NULL
                 and gtt.diff_id     is NOT NULL
                 and gtt.zone_id     is NOT NULL
                 and fr.dept         = im.dept
                 and fr.class        = im.class
                 and fr.subclass     = im.subclass
                 and fr.item         = im.item
                 and fr.location     = rzl.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.item_parent  = gtt.item
                 and rzl.zone_id     = gtt.zone_id
                 and gtt.location    is NULL
                 and (   im.diff_1   = gtt.diff_id
                      or im.diff_2   = gtt.diff_id
                      or im.diff_3   = gtt.diff_id
                      or im.diff_4   = gtt.diff_id)
              union all
              -- Link Code / Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_link_code_attribute rlca,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item         is NOT NULL
                 and gtt.zone_id      is NOT NULL
                 and gtt.location     is NULL
                 and gtt.item         = to_char(rlca.link_code)
                 and im.item          = rlca.item
                 and im.item          = fr.item
                 and rzl.zone_id      = gtt.zone_id
                 and fr.location      = rzl.location
                 and fr.action_date  <= gtt.effective_date
                 and im.dept          = fr.dept
                 and im.class         = fr.class
                 and im.subclass      = fr.subclass
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS) t;

   cursor C_PC_SKULIST is
      select distinct t.selling_uom
        from (-- item level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item
                 and pcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item_parent
                 and pcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- item level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item
                 and pcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item_parent
                 and pcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS) t;

   cursor C_PR_SKULIST is
      select distinct t.selling_uom
        from (-- item level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item
                 and pds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item_parent
                 and pds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- item level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item
                 and pds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item_parent
                 and pds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS) t;

   cursor C_PEIL is
      select distinct t.selling_uom
        from (-- item level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.price_event_itemlist is NOT NULL
                 and gtt.zone_id              is NULL
                 and gtt.location             is NOT NULL
                 and rmld.merch_list_id       = gtt.price_event_itemlist
                 and rmld.item                = im.item
                 and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item                  = fr.item
                 and fr.action_date          <= gtt.effective_date
                 and im.dept                  = fr.dept
                 and im.class                 = fr.class
                 and im.subclass              = fr.subclass
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- parent level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.price_event_itemlist is NOT NULL
                 and gtt.zone_id              is NULL
                 and gtt.location             is NOT NULL
                 and rmld.merch_list_id       = gtt.price_event_itemlist
                 and rmld.item                = im.item_parent
                 and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item                  = fr.item
                 and fr.action_date          <= gtt.effective_date
                 and im.dept                  = fr.dept
                 and im.class                 = fr.class
                 and im.subclass              = fr.subclass
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- item level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.price_event_itemlist is NOT NULL
                 and gtt.zone_id              is NOT NULL
                 and gtt.location             is NULL
                 and rmld.merch_list_id       = gtt.price_event_itemlist
                 and rmld.item                = im.item
                 and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item                  = fr.item
                 and rzl.zone_id              = gtt.zone_id
                 and fr.location              = rzl.location
                 and fr.action_date          <= gtt.effective_date
                 and im.dept                  = fr.dept
                 and im.class                 = fr.class
                 and im.subclass              = fr.subclass
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- parent level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(L_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.price_event_itemlist is NOT NULL
                 and gtt.zone_id              is NOT NULL
                 and gtt.location             is NULL
                 and rmld.merch_list_id       = gtt.price_event_itemlist
                 and rmld.item                = im.item_parent
                 and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item                  = fr.item
                 and rzl.zone_id              = gtt.zone_id
                 and fr.location              = rzl.location
                 and fr.action_date          <= gtt.effective_date
                 and im.dept                  = fr.dept
                 and im.class                 = fr.class
                 and im.subclass              = fr.subclass
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t;

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_FP_PC;
      fetch C_FP_PC into L_merch_nd_zone_nd_date_rec;
      close C_FP_PC;

   elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO) then

      open C_FP_PROMO;
      fetch C_FP_PROMO into L_promo_dtl_disc_ladder_id,
                            L_merch_nd_zone_nd_date_rec;
      close C_FP_PROMO;

   end if;

   if L_merch_nd_zone_nd_date_rec is NOT NULL then
      L_merch_nd_zone_nd_date_tbl := OBJ_MERCH_ND_ZONE_ND_DATE_TBL(L_merch_nd_zone_nd_date_rec);
   end if;

   -- if not dealing with a FP price event that has a NULL value for the UOM, nothing left to do....
   if L_merch_nd_zone_nd_date_tbl is NULL or
      L_merch_nd_zone_nd_date_tbl.COUNT = 0 then

      return 1;

   end if;

   select is_unique_uom,
          uom_value into L_is_unique_uom,
                         L_uom_value
     from rpm_system_options;

   if L_is_unique_uom = 1 then

      L_uoms := OBJ_VARCHAR_ID_TABLE(L_uom_value);

   else

      if EXPL_TIMELINES_TO_IL(O_cc_error_tbl,
                              I_price_event_id,
                              I_bulk_cc_pe_id,
                              I_pe_thread_number) = 0 then
         return 0;
      end if;

      if L_merch_nd_zone_nd_date_tbl(1).skulist is NULL and
         L_merch_nd_zone_nd_date_tbl(1).price_event_itemlist is NULL then

         open C_CHECK_UOM;
         fetch C_CHECK_UOM BULK COLLECT into L_uoms;
         close C_CHECK_UOM;

      elsif L_merch_nd_zone_nd_date_tbl(1).skulist is NOT NULL then

         if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

            open C_PC_SKULIST;
            fetch C_PC_SKULIST BULK COLLECT into L_uoms;
            close C_PC_SKULIST;

         elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO) then

            open C_PR_SKULIST;
            fetch C_PR_SKULIST BULK COLLECT into L_uoms;
            close C_PR_SKULIST;

         end if;

      elsif L_merch_nd_zone_nd_date_tbl(1).price_event_itemlist is NOT NULL then

         open C_PEIL;
         fetch C_PEIL BULK COLLECT into L_uoms;
         close C_PEIL;

      end if;

   end if;

   if L_uoms is NOT NULL and
      L_uoms.COUNT = 1 then
      ---
      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         update rpm_price_change
            set change_selling_uom = L_uoms(1)
          where price_change_id = I_price_event_id
            and (   change_selling_uom != L_uoms(1)
                 or change_selling_uom is NULL);

      elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION) then

         update rpm_promo_dtl_disc_ladder
            set change_selling_uom = L_uoms(1)
          where promo_dtl_disc_ladder_id = L_promo_dtl_disc_ladder_id
            and (   change_selling_uom != L_uoms(1)
                 or change_selling_uom is NULL);

      end if;

   else -- more than one UOM is found...

      L_error_rec := CONFLICT_CHECK_ERROR_REC(I_price_event_id,
                                              NULL,
                                              RPM_CONSTANTS.CONFLICT_ERROR,
                                              'selling_uom_not_unique');
      if O_cc_error_tbl is NULL then
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      else
         O_cc_error_tbl.EXTEND;
         O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_error_rec;
      end if;

      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(sQLCODE))));

      return 0;

END VALIDATE_FP_UOM;

--------------------------------------------------------------------------------
FUNCTION EXPL_TIMELINES_TO_IL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_id   IN     NUMBER,
                              I_bulk_cc_pe_id    IN     NUMBER,
                              I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_THREADING_SQL.EXPL_TIMELINES_TO_IL';

   L_dept NUMBER(4) := NULL;

   cursor C_DEPTS is
      select distinct dept
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id    = I_bulk_cc_pe_id
         and thread_number    = I_pe_thread_number
         and price_event_id   = I_price_event_id
         and merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE;

BEGIN

   delete rpm_fr_item_loc_expl_gtt;

   -- Explode all timelines on FR to the IL level making sure that only those ranged are retrieved
   -- Loop through all the depts in the price event (this will usually only be one) for a cleaner join to RIL

   for rec IN C_DEPTS loop
      L_dept := rec.dept;

      insert into rpm_fr_item_loc_expl_gtt (price_event_id,
                                            future_retail_id,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            zone_node_type,
                                            location,
                                            action_date,
                                            selling_retail,
                                            selling_retail_currency,
                                            selling_uom,
                                            multi_units,
                                            multi_unit_retail,
                                            multi_unit_retail_currency,
                                            multi_selling_uom,
                                            clear_retail,
                                            clear_retail_currency,
                                            clear_uom,
                                            simple_promo_retail,
                                            simple_promo_retail_currency,
                                            simple_promo_uom,
                                            promo_comp_msg_type,
                                            price_change_id,
                                            price_change_display_id,
                                            pc_exception_parent_id,
                                            pc_change_type,
                                            pc_change_amount,
                                            pc_change_currency,
                                            pc_change_percent,
                                            pc_change_selling_uom,
                                            pc_null_multi_ind,
                                            pc_multi_units,
                                            pc_multi_unit_retail,
                                            pc_multi_unit_retail_currency,
                                            pc_multi_selling_uom,
                                            pc_price_guide_id,
                                            clearance_id,
                                            clearance_display_id,
                                            clear_mkdn_index,
                                            clear_start_ind,
                                            clear_change_type,
                                            clear_change_amount,
                                            clear_change_currency,
                                            clear_change_percent,
                                            clear_change_selling_uom,
                                            clear_price_guide_id,
                                            loc_move_from_zone_id,
                                            loc_move_to_zone_id,
                                            location_move_id,
                                            promo_dtl_id,
                                            detail_start_date,
                                            detail_end_date,
                                            type,
                                            promo_id,
                                            exception_parent_id)
         select price_event_id,
                future_retail_id,
                dept,
                class,
                subclass,
                to_item,
                to_zone_node_type,
                to_location,
                action_date,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                clear_retail,
                clear_retail_currency,
                clear_uom,
                simple_promo_retail,
                simple_promo_retail_currency,
                simple_promo_uom,
                promo_comp_msg_type,
                price_change_id,
                price_change_display_id,
                pc_exception_parent_id,
                pc_change_type,
                pc_change_amount,
                pc_change_currency,
                pc_change_percent,
                pc_change_selling_uom,
                pc_null_multi_ind,
                pc_multi_units,
                pc_multi_unit_retail,
                pc_multi_unit_retail_currency,
                pc_multi_selling_uom,
                pc_price_guide_id,
                clearance_id,
                clearance_display_id,
                clear_mkdn_index,
                clear_start_ind,
                clear_change_type,
                clear_change_amount,
                clear_change_currency,
                clear_change_percent,
                clear_change_selling_uom,
                clear_price_guide_id,
                loc_move_from_zone_id,
                loc_move_to_zone_id,
                location_move_id,
                promo_dtl_id,
                detail_start_date,
                detail_end_date,
                type,
                promo_id,
                exception_parent_id
           from (select /*+ ORDERED */
                        t.price_event_id,
                        rfr.future_retail_id,
                        rfr.dept,
                        rfr.class,
                        rfr.subclass,
                        t.to_item,
                        t.to_zone_node_type,
                        t.to_location,
                        rfr.action_date,
                        rfr.selling_retail,
                        rfr.selling_retail_currency,
                        rfr.selling_uom,
                        rfr.multi_units,
                        rfr.multi_unit_retail,
                        rfr.multi_unit_retail_currency,
                        rfr.multi_selling_uom,
                        rfr.clear_retail,
                        rfr.clear_retail_currency,
                        rfr.clear_uom,
                        rfr.simple_promo_retail,
                        rfr.simple_promo_retail_currency,
                        rfr.simple_promo_uom,
                        rfr.price_change_id,
                        rfr.price_change_display_id,
                        rfr.pc_exception_parent_id,
                        rfr.pc_change_type,
                        rfr.pc_change_amount,
                        rfr.pc_change_currency,
                        rfr.pc_change_percent,
                        rfr.pc_change_selling_uom,
                        rfr.pc_null_multi_ind,
                        rfr.pc_multi_units,
                        rfr.pc_multi_unit_retail,
                        rfr.pc_multi_unit_retail_currency,
                        rfr.pc_multi_selling_uom,
                        rfr.pc_price_guide_id,
                        rfr.clearance_id,
                        rfr.clearance_display_id,
                        rfr.clear_mkdn_index,
                        rfr.clear_start_ind,
                        rfr.clear_change_type,
                        rfr.clear_change_amount,
                        rfr.clear_change_currency,
                        rfr.clear_change_percent,
                        rfr.clear_change_selling_uom,
                        rfr.clear_price_guide_id,
                        rfr.loc_move_from_zone_id,
                        rfr.loc_move_to_zone_id,
                        rfr.location_move_id,
                        rpile.promo_dtl_id,
                        rpile.detail_start_date,
                        rpile.detail_end_date,
                        rpile.type,
                        rpile.promo_id,
                        rpile.exception_parent_id,
                        rpile.promo_comp_msg_type,
                        t.rank,
                        MAX(t.rank) OVER (PARTITION BY t.price_event_id,
                                                       t.to_item,
                                                       t.to_location) max_rank
                   from (-- Look at P/Z level first and rank that data 0
                         select distinct
                                0 rank,
                                t.dept,
                                t.price_event_id,
                                t.from_item,
                                NULL from_diff_id,
                                rz.zone_id from_location,
                                t.from_zone_node_type,
                                t.to_item,
                                t.to_location,
                                t.to_zone_node_type
                           from (select /*+ ORDERED index(rpl, RPM_BULK_CC_PE_LOCATION_I1) */
                                        rpi.dept,
                                        rpi.class,
                                        rpi.subclass,
                                        rpi.price_event_id,
                                        im.item_parent                    from_item,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                        rpi.item                          to_item,
                                        rpl.location                      to_location,
                                        rpl.zone_node_type                to_zone_node_type,
                                        rpl.location                      location,
                                        rpl.zone_node_type                zone_node_type
                                   from rpm_bulk_cc_pe_item rpi,
                                        rpm_bulk_cc_pe_location rpl,
                                        item_master im
                                  where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                    and rpi.thread_number        = I_pe_thread_number
                                    and rpi.price_event_id       = I_price_event_id
                                    and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                    and rpi.dept                 = L_dept
                                    and im.item                  = rpi.item
                                    and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                    and rpl.price_event_id       = rpi.price_event_id
                                    and rpl.itemloc_id           = rpi.itemloc_id
                                    and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                    and rownum                   > 0) t,
                                rpm_merch_retail_def_expl rmrde,
                                rpm_zone rz,
                                rpm_zone_location rzl
                          where rmrde.dept               = t.dept
                            and rmrde.class              = t.class
                            and rmrde.subclass           = t.subclass
                            and rmrde.regular_zone_group = rz.zone_group_id
                            and rz.zone_id               = rzl.zone_id
                            and rzl.location             = t.location
                            and rzl.loc_type             = t.zone_node_type
                            and rownum                  >= 1
                         union all
                         -- Look at PD/Z level and rank that data 1
                         select distinct
                                1 rank,
                                t.dept,
                                t.price_event_id,
                                t.from_item,
                                t.from_diff_id,
                                rz.zone_id from_location,
                                t.from_zone_node_type,
                                t.to_item,
                                t.to_location,
                                t.to_zone_node_type
                           from (select /*+ ORDERED index(rpl, RPM_BULK_CC_PE_LOCATION_I1) */
                                        rpi.dept,
                                        rpi.class,
                                        rpi.subclass,
                                        rpi.price_event_id,
                                        im.item_parent                    from_item,
                                        rpi.diff_id                       from_diff_id,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                        rpi.item                          to_item,
                                        rpl.location                      to_location,
                                        rpl.zone_node_type                to_zone_node_type,
                                        rpl.location                      location,
                                        rpl.zone_node_type                zone_node_type
                                   from rpm_bulk_cc_pe_item rpi,
                                        rpm_bulk_cc_pe_location rpl,
                                        item_master im
                                  where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                    and rpi.thread_number        = I_pe_thread_number
                                    and rpi.price_event_id       = I_price_event_id
                                    and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                    and rpi.dept                 = L_dept
                                    and im.item                  = rpi.item
                                    and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                    and rpl.price_event_id       = rpi.price_event_id
                                    and rpl.itemloc_id           = rpi.itemloc_id
                                    and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                    and rownum                   > 0) t,
                                rpm_merch_retail_def_expl rmrde,
                                rpm_zone rz,
                                rpm_zone_location rzl
                          where rmrde.dept               = t.dept
                            and rmrde.class              = t.class
                            and rmrde.subclass           = t.subclass
                            and rmrde.regular_zone_group = rz.zone_group_id
                            and rz.zone_id               = rzl.zone_id
                            and rzl.location             = t.location
                            and rzl.loc_type             = t.zone_node_type
                            and rownum                  >= 1
                         union all
                         -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                         select /*+ ORDERED index(rpl, RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct
                                1 rank,
                                rpi.dept,
                                rpi.price_event_id,
                                im.item_parent                 from_item,
                                NULL                           from_diff_id,
                                rpl.location                   from_location,
                                rpl.zone_node_type             from_zone_node_type,
                                rpi.item                       to_item,
                                rpl.location                   to_location,
                                rpl.zone_node_type             to_zone_node_type
                           from rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl,
                                item_master im
                          where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                            and rpi.thread_number        = I_pe_thread_number
                            and rpi.price_event_id       = I_price_event_id
                            and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                            and rpi.dept                 = L_dept
                            and im.item                  = rpi.item
                            and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                            and rpl.price_event_id       = rpi.price_event_id
                            and rpl.itemloc_id           = rpi.itemloc_id
                            and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                            and rownum                  >= 1
                         union all
                         -- Look at I/Z level and rank that data 2
                         select distinct
                                2 rank,
                                t.dept,
                                t.price_event_id,
                                t.from_item,
                                t.from_diff_id,
                                rz.zone_id from_location,
                                t.from_zone_node_type,
                                t.to_item,
                                t.to_location,
                                t.to_zone_node_type
                           from (select /*+ ORDERED index(rpl, RPM_BULK_CC_PE_LOCATION_I1) */
                                        rpi.dept,
                                        rpi.class,
                                        rpi.subclass,
                                        rpi.price_event_id,
                                        rpi.item                          from_item,
                                        rpi.diff_id                       from_diff_id,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                        rpi.item                          to_item,
                                        rpl.location                      to_location,
                                        rpl.zone_node_type                to_zone_node_type,
                                        rpl.location                      location,
                                        rpl.zone_node_type                zone_node_type
                                   from rpm_bulk_cc_pe_item rpi,
                                        rpm_bulk_cc_pe_location rpl
                                  where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                    and rpi.thread_number        = I_pe_thread_number
                                    and rpi.price_event_id       = I_price_event_id
                                    and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                    and rpi.dept                 = L_dept
                                    and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                    and rpl.price_event_id       = rpi.price_event_id
                                    and rpl.itemloc_id           = rpi.itemloc_id
                                    and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                    and rownum                   > 0) t,
                                rpm_merch_retail_def_expl rmrde,
                                rpm_zone rz,
                                rpm_zone_location rzl
                          where rmrde.dept               = t.dept
                            and rmrde.class              = t.class
                            and rmrde.subclass           = t.subclass
                            and rmrde.regular_zone_group = rz.zone_group_id
                            and rz.zone_id               = rzl.zone_id
                            and rzl.location             = t.location
                            and rzl.loc_type             = t.zone_node_type
                            and rownum                  >= 1
                         union all
                         -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                         select /*+ ORDERED index(rpl, RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct
                                2 rank,
                                rpi.dept,
                                rpi.price_event_id,
                                im.item_parent                 from_item,
                                rpi.diff_id                    from_diff_id,
                                rpl.location                   from_location,
                                rpl.zone_node_type             from_zone_node_type,
                                rpi.item                       to_item,
                                rpl.location                   to_location,
                                rpl.zone_node_type             to_zone_node_type
                           from rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl,
                                item_master im
                          where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                            and rpi.thread_number        = I_pe_thread_number
                            and rpi.price_event_id       = I_price_event_id
                            and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                            and rpi.dept                 = L_dept
                            and im.item                  = rpi.item
                            and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                            and rpl.price_event_id       = rpi.price_event_id
                            and rpl.itemloc_id           = rpi.itemloc_id
                            and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                            and rownum                  >= 1
                         union all
                         -- Look at the I/L level and rank that data 3
                         select /*+ ORDERED index(rpl, RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct
                                3 rank,
                                rpi.dept,
                                rpi.price_event_id,
                                rpi.item                       from_item,
                                NULL                           from_diff_id,
                                rpl.location                   from_location,
                                rpl.zone_node_type             from_zone_node_type,
                                rpi.item                       to_item,
                                rpl.location                   to_location,
                                rpl.zone_node_type             to_zone_node_type
                           from rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                            and rpi.thread_number        = I_pe_thread_number
                            and rpi.price_event_id       = I_price_event_id
                            and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                            and rpi.dept                 = L_dept
                            and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                            and rpl.price_event_id       = rpi.price_event_id
                            and rpl.itemloc_id           = rpi.itemloc_id
                            and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                            and rownum                  >= 1) t,
                        rpm_future_retail rfr,
                        rpm_promo_item_loc_expl_gtt rpile,
                        rpm_item_loc ril
                  where ril.dept               = L_dept
                    and rfr.dept               = L_dept
                    and rfr.dept               = t.dept
                    and rfr.item               = t.from_item
                    and NVL(rfr.diff_id, -999) = NVL(t.from_diff_id, -999)
                    and rfr.location           = t.from_location
                    and rfr.zone_node_type     = t.from_zone_node_type
                    and rfr.item               = rpile.item(+)
                    and NVL(rfr.diff_id, -999) = NVL(rpile.diff_id (+), -999)
                    and rfr.location           = rpile.location(+)
                    and rfr.dept               = rpile.dept(+)
                    and rfr.zone_node_type     = rpile.zone_node_type (+)
                    and rfr.action_date        BETWEEN rpile.detail_start_date(+) and NVL(rpile.detail_end_date(+), TO_DATE('3000', 'YYYY'))
                    and ril.dept               = t.dept
                    and ril.item               = t.to_item
                    and ril.loc                = t.to_location)
          where rank = max_rank;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END EXPL_TIMELINES_TO_IL;
--------------------------------------------------------------------------------
FUNCTION GENERATE_EXCLUSIONS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_type IN     VARCHAR2,
                             I_price_event_id   IN     NUMBER)

RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_THREADING_SQL.GENERATE_EXCLUSIONS';

   L_pe_id_sge_exists NUMBER(15) := NULL;

   cursor C_SGE_TO_PROCESS is
      select price_event_id
        from rpm_cc_sys_gen_head_ws
       where price_event_id = I_price_event_id;

BEGIN

   open C_SGE_TO_PROCESS;
   fetch C_SGE_TO_PROCESS into L_pe_id_sge_exists;
   close C_SGE_TO_PROCESS;

   if L_pe_id_sge_exists is NOT NULL then

      insert into rpm_merch_list_head (merch_list_id,
                                       price_event_id,
                                       price_event_type,
                                       merch_list_desc,
                                       merch_list_type)
         select RPM_MERCH_LIST_ID_SEQ.NEXTVAL,
                L_pe_id_sge_exists,
                I_price_event_type,
                NVL(def_merch_list_desc, 'SYSTEM GENERATED LIST')||' '||RPM_MERCH_LIST_ID_SEQ.NEXTVAL,
                RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
           from rpm_system_options_def;

      insert into rpm_merch_list_detail (merch_list_id,
                                         merch_level,
                                         item,
                                         item_desc,
                                         diff_id,
                                         diff_desc)
         select merch_list_id,
                merch_level,
                item,
                item_desc,
                diff_id,
                diff_desc
           from (-- tran level items
                 select distinct mlh.merch_list_id,
                        RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level,
                        im.item,
                        im.item_desc,
                        NULL diff_id,
                        NULL diff_desc
                   from rpm_cc_sys_gen_detail_ws wsd,
                        rpm_cc_sys_gen_head_ws wsh,
                        item_master im,
                        rpm_merch_list_head mlh
                  where wsd.price_event_id   = mlh.price_event_id
                    and wsh.price_event_id   = wsd.price_event_id
                    and wsh.tolerance_ind    = 1
                    and mlh.price_event_type = I_price_event_type
                    and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                    and wsd.item             = im.item
                    and wsd.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC)
                 union
                 -- parent level items
                 select distinct mlh.merch_list_id,
                        RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level,
                        im.item,
                        im.item_desc,
                        NULL diff_id,
                        NULL diff_desc
                   from rpm_cc_sys_gen_detail_ws wsd,
                        rpm_cc_sys_gen_head_ws wsh,
                        item_master im,
                        rpm_merch_list_head mlh
                  where wsd.price_event_id   = mlh.price_event_id
                    and wsh.price_event_id   = wsd.price_event_id
                    and wsh.tolerance_ind    = 1
                    and mlh.price_event_type = I_price_event_type
                    and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                    and wsd.item             = im.item
                    and wsd.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                 RPM_CONSTANTS.FR_HIER_PARENT_LOC)
                 union
                 -- parent diff level items
                 select distinct mlh.merch_list_id,
                        RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level,
                        im.item,
                        im.item_desc,
                        im.diff_1 diff_id,
                        d.diff_desc diff_desc
                   from rpm_cc_sys_gen_detail_ws wsd,
                        rpm_cc_sys_gen_head_ws wsh,
                        item_master im,
                        diff_ids d,
                        rpm_merch_list_head mlh
                  where wsd.price_event_id   = mlh.price_event_id
                    and wsh.price_event_id   = wsd.price_event_id
                    and wsh.tolerance_ind    = 1
                    and mlh.price_event_type = I_price_event_type
                    and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                    and wsd.item             = im.item
                    and wsd.diff_id          = im.diff_1
                    and im.diff_1            = d.diff_id
                    and wsd.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_DIFF_ZONE,
                                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC));

      -- Create the exclusions based on the price event and the merch_list created
      if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

         insert into rpm_promo_dtl (promo_dtl_id,
                                    promo_comp_id,
                                    promo_dtl_display_id,
                                    ignore_constraints,
                                    apply_to_code,
                                    start_date,
                                    end_date,
                                    approval_date,
                                    create_date,
                                    create_id,
                                    approval_id,
                                    state,
                                    attribute_1,
                                    attribute_2,
                                    attribute_3,
                                    exception_parent_id,
                                    from_location_move,
                                    price_guide_id,
                                    threshold_id,
                                    sys_generated_exclusion,
                                    timebased_dtl_ind)
            select RPM_PROMO_DTL_SEQ.NEXTVAL, -- promo_dtl_id
                   rpd.promo_comp_id,
                   RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL, -- promo_dtl_display_id
                   rpd.ignore_constraints,
                   rpd.apply_to_code,
                   rpd.start_date,
                   rpd.end_date,
                   rpd.approval_date,
                   rpd.create_date,
                   rpd.create_id,
                   rpd.approval_id,
                   rpd.state,
                   rpd.attribute_1,
                   rpd.attribute_2,
                   rpd.attribute_3,
                   L_pe_id_sge_exists, -- exception_parent_id
                   rpd.from_location_move,
                   rpd.price_guide_id,
                   rpd.threshold_id,
                   1, -- sys_generated_exclusion
                   rpd.timebased_dtl_ind
              from rpm_promo_dtl rpd
             where rpd.promo_dtl_id = L_pe_id_sge_exists;

         insert into rpm_promo_zone_location (promo_zone_id,
                                              promo_dtl_id,
                                              zone_node_type,
                                              zone_id,
                                              location)
            select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                   rpd2.promo_dtl_id,
                   pzl.zone_node_type,
                   pzl.zone_id,
                   pzl.location
              from rpm_promo_zone_location pzl,
                   rpm_promo_dtl rpd1,
                   rpm_promo_dtl rpd2
             where L_pe_id_sge_exists           = rpd1.promo_dtl_id
               and rpd1.promo_dtl_id            = rpd2.exception_parent_id
               and rpd2.sys_generated_exclusion = 1
               and rpd1.promo_dtl_id            = pzl.promo_dtl_id;

         insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                             promo_dtl_id)
            select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                   rpd2.promo_dtl_id
              from rpm_promo_dtl rpd2
             where L_pe_id_sge_exists           = rpd2.exception_parent_id
               and rpd2.sys_generated_exclusion = 1;

         insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                         promo_dtl_list_grp_id)
            select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                   pdlg.promo_dtl_list_grp_id
              from rpm_promo_dtl rpd2,
                   rpm_promo_dtl_list_grp pdlg
             where L_pe_id_sge_exists           = rpd2.exception_parent_id
               and rpd2.sys_generated_exclusion = 1
               and rpd2.promo_dtl_id            = pdlg.promo_dtl_id;

         insert into rpm_promo_dtl_merch_node (promo_dtl_merch_node_id,
                                               promo_dtl_list_id,
                                               promo_dtl_id,
                                               merch_type,
                                               skulist)
            select RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                   pdl.promo_dtl_list_id,
                   rpd2.promo_dtl_id,
                   RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM,
                   mlh.merch_list_id
              from rpm_promo_dtl rpd2,
                   rpm_promo_dtl_list_grp pdlg,
                   rpm_promo_dtl_list pdl,
                   rpm_merch_list_head mlh
             where L_pe_id_sge_exists           = rpd2.exception_parent_id
               and rpd2.sys_generated_exclusion = 1
               and rpd2.promo_dtl_id            = pdlg.promo_dtl_id
               and pdlg.promo_dtl_list_grp_id   = pdl.promo_dtl_list_grp_id
               and L_pe_id_sge_exists           = mlh.price_event_id
               and mlh.merch_list_type          = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
               and mlh.price_event_type         = I_price_event_type;

         insert into rpm_promo_dtl_disc_ladder (promo_dtl_disc_ladder_id,
                                                promo_dtl_list_id,
                                                change_type,
                                                qual_type,
                                                qual_value)
            select RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                   pdl.promo_dtl_list_id,
                   RPM_CONSTANTS.RETAIL_EXCLUDE,
                   orig.qual_type,
                   orig.qual_value
              from rpm_promo_dtl rpd2,
                   rpm_promo_dtl_list_grp pdlg,
                   rpm_promo_dtl_list pdl,
                   (select pddl.qual_type,
                           pddl.qual_value
                      from rpm_promo_dtl rpd,
                           rpm_promo_dtl_list_grp pdlg2,
                           rpm_promo_dtl_list pdl2,
                           rpm_promo_dtl_disc_ladder pddl
                     where L_pe_id_sge_exists          = rpd.promo_dtl_id
                       and rpd.promo_dtl_id            = pdlg2.promo_dtl_id
                       and pdlg2.promo_dtl_list_grp_id = pdl2.promo_dtl_list_grp_id
                       and pdl2.promo_dtl_list_id      = pddl.promo_dtl_list_id) orig
             where L_pe_id_sge_exists           = rpd2.exception_parent_id
               and rpd2.sys_generated_exclusion = 1
               and rpd2.promo_dtl_id            = pdlg.promo_dtl_id
               and pdlg.promo_dtl_list_grp_id   = pdl.promo_dtl_list_grp_id;

         -- Update the rpm_merch_list_head.price_event_id now that it's available...
         merge into rpm_merch_list_head target
         using (select rpd.promo_dtl_id,
                       pdmn.skulist
                  from rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node pdmn
                 where L_pe_id_sge_exists = rpd.exception_parent_id
                   and rpd.promo_dtl_id   = pdmn.promo_dtl_id) source
         on (source.skulist = target.merch_list_id)
         when MATCHED then
            update
               set target.price_event_id = source.promo_dtl_id;

      elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

         insert into rpm_clearance (clearance_id,
                                    clearance_display_id,
                                    state,
                                    reason_code,
                                    exception_parent_id,
                                    zone_id,
                                    location,
                                    zone_node_type,
                                    effective_date,
                                    out_of_stock_date,
                                    reset_date,
                                    change_type,
                                    price_guide_id,
                                    vendor_funded_ind,
                                    funding_type,
                                    funding_amount,
                                    funding_amount_currency,
                                    funding_percent,
                                    supplier,
                                    deal_id,
                                    deal_detail_id,
                                    partner_type,
                                    partner_id,
                                    create_date,
                                    create_id,
                                    approval_date,
                                    approval_id,
                                    lock_version,
                                    skulist,
                                    sys_generated_exclusion)
         select RPM_CLEARANCE_SEQ.NEXTVAL,
                RPM_CLEARANCE_DISPLAY_SEQ.NEXTVAL,
                state,
                reason_code,
                L_pe_id_sge_exists, -- exception_parent_id
                zone_id,
                location,
                zone_node_type,
                effective_date,
                out_of_stock_date,
                reset_date,
                RPM_CONSTANTS.RETAIL_EXCLUDE, -- change_type
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                funding_amount_currency,
                funding_percent,
                supplier,
                deal_id,
                deal_detail_id,
                partner_type,
                partner_id,
                create_date,
                create_id,
                approval_date,
                approval_id,
                lock_version,
                mlh.merch_list_id, -- skulist,
                1 -- sys_generated_exclusion
           from rpm_clearance cl,
                rpm_merch_list_head mlh
          where cl.clearance_id      = L_pe_id_sge_exists
            and L_pe_id_sge_exists   = mlh.price_event_id
            and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
            and mlh.price_event_type = I_price_event_type;

         -- Update the rpm_merch_list_head.price_event_id now that it's available...
         merge into rpm_merch_list_head target
         using (select rc.clearance_id,
                       rc.skulist
                  from rpm_clearance rc
                 where L_pe_id_sge_exists = rc.exception_parent_id) source
         on (target.merch_list_id = source.skulist)
         when MATCHED then
            update
               set target.price_event_id = source.clearance_id;

      elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         insert into rpm_price_change (price_change_id,
                                       price_change_display_id,
                                       state,
                                       reason_code,
                                       exception_parent_id,
                                       zone_id,
                                       location,
                                       zone_node_type,
                                       link_code,
                                       effective_date,
                                       change_type,
                                       null_multi_ind,
                                       multi_units,
                                       multi_unit_retail,
                                       multi_unit_retail_currency,
                                       multi_selling_uom,
                                       price_guide_id,
                                       vendor_funded_ind,
                                       funding_type,
                                       funding_amount,
                                       funding_amount_currency,
                                       funding_percent,
                                       deal_id,
                                       deal_detail_id,
                                       create_date,
                                       create_id,
                                       approval_date,
                                       approval_id,
                                       area_diff_parent_id,
                                       ignore_constraints,
                                       lock_version,
                                       skulist,
                                       sys_generated_exclusion)
         select RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                state,
                reason_code,
                L_pe_id_sge_exists, -- exception_parent_id
                zone_id,
                location,
                zone_node_type,
                link_code,
                effective_date,
                RPM_CONSTANTS.RETAIL_EXCLUDE, -- change_type
                null_multi_ind,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                funding_amount_currency,
                funding_percent,
                deal_id,
                deal_detail_id,
                create_date,
                create_id,
                approval_date,
                approval_id,
                area_diff_parent_id,
                ignore_constraints,
                lock_version,
                mlh.merch_list_id, -- skulist,
                1 -- sys_generated_exclusion
           from rpm_price_change pc,
                rpm_merch_list_head mlh
          where pc.price_change_id   = L_pe_id_sge_exists
            and I_price_event_id     = mlh.price_event_id
            and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
            and mlh.price_event_type = I_price_event_type;

         -- Update the rpm_merch_list_head.price_event_id now that it's available...
         merge into rpm_merch_list_head target
         using (select rpc.price_change_id,
                       rpc.skulist
                  from rpm_price_change rpc
                 where L_pe_id_sge_exists = rpc.exception_parent_id) source
         on (target.merch_list_id = source.skulist)
         when MATCHED then
            update
               set target.price_event_id = source.price_change_id;

      end if;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_EXCLUSIONS;
--------------------------------------------------------------------------------
FUNCTION SGE_OUTPUT(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)

RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_THREADING_SQL.SGE_OUTPUT';

   -- Conflict errors that have had SGE's created for it.
   cursor C_SGE_CC_ERROR is
       select CONFLICT_CHECK_ERROR_REC(ra.exclusion_price_event_id,
                                       ra.future_retail_id,
                                       ra.error_type,
                                       ra.error_string,
                                       ra.cs_promo_fr_id)
         from (select cc.exclusion_price_event_id,
                      fr.future_retail_id,
                      cc.error_type,
                      cc.error_string,
                      cc.cs_promo_fr_id,
                      ROW_NUMBER() OVER (PARTITION BY wsd.price_event_id,
                                                      fr.item,
                                                      fr.diff_id,
                                                      fr.location,
                                                      fr.zone_node_type
                                             ORDER BY fr.action_date desc) rank
                from (select distinct rpd.promo_dtl_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from rpm_cc_sys_gen_detail_ws err,
                             rpm_cc_sys_gen_head_ws wsh,
                             rpm_promo_dtl rpd
                       where wsh.price_event_id          = err.price_event_id
                         and wsh.tolerance_ind           = 1
                         and err.price_event_id          = rpd.exception_parent_id
                         and err.price_event_type        IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION)
                         and err.cs_promo_fr_id          is NULL
                         and rpd.sys_generated_exclusion = 1
                      union all
                      select distinct rpc.price_change_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from rpm_cc_sys_gen_detail_ws err,
                             rpm_cc_sys_gen_head_ws wsh,
                             rpm_price_change rpc
                       where wsh.price_event_id          = err.price_event_id
                         and wsh.tolerance_ind           = 1
                         and rpc.exception_parent_id     = err.price_event_id
                         and err.price_event_type        = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                         and rpc.sys_generated_exclusion = 1
                     union all
                      select distinct rc.clearance_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from rpm_cc_sys_gen_detail_ws err,
                             rpm_cc_sys_gen_head_ws wsh,
                             rpm_clearance rc
                       where wsh.price_event_id         = err.price_event_id
                         and wsh.tolerance_ind          = 1
                         and rc.exception_parent_id     = err.price_event_id
                         and err.price_event_type       = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                         and rc.sys_generated_exclusion = 1) cc,
                     rpm_cc_sys_gen_detail_ws wsd,
                     rpm_future_retail fr
               where cc.cs_promo_fr_id        is NULL
                 and cc.parent_price_event_id = wsd.price_event_id
                 and wsd.dept                 = fr.dept
                 and wsd.item                 = fr.item
                 and NVL(wsd.diff_id, '-999') = NVL(fr.diff_id, '-999')
                 and wsd.cur_hier_level       = fr.cur_hier_level
                 and wsd.location             = fr.location
                 and wsd.zone_node_type       = fr.zone_node_type
                 and wsd.action_date         >= fr.action_date) ra
        where ra.rank = 1
       union all
       -- Cust Seg conflict errors that have had SGE's created for it for.
       select CONFLICT_CHECK_ERROR_REC(ra.exclusion_price_event_id,
                                       ra.future_retail_id,
                                       ra.error_type,
                                       ra.error_string,
                                       ra.cs_promo_fr_id)
         from (select cc.exclusion_price_event_id,
                      cc.future_retail_id,
                      cc.error_type,
                      cc.error_string,
                      fr.cust_segment_promo_id cs_promo_fr_id,
                      ROW_NUMBER() OVER (PARTITION BY wsd.price_event_id,
                                                      fr.item,
                                                      fr.diff_id,
                                                      fr.location,
                                                      fr.zone_node_type
                                             ORDER BY fr.action_date desc) rank
                 from (select distinct rpd.promo_dtl_id exclusion_price_event_id,
                              err.price_event_id parent_price_event_id,
                              err.future_retail_id,
                              err.error_type,
                              err.error_string,
                              err.cs_promo_fr_id
                         from rpm_cc_sys_gen_detail_ws err,
                              rpm_cc_sys_gen_head_ws wsh,
                              rpm_promo_dtl rpd
                        where wsh.price_event_id          = err.price_event_id
                          and wsh.tolerance_ind           = 1
                          and err.price_event_id          = rpd.exception_parent_id
                          and err.price_event_type        IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                          and rpd.sys_generated_exclusion = 1) cc,
                      rpm_cc_sys_gen_detail_ws wsd,
                      rpm_cust_segment_promo_fr fr
                where cc.cs_promo_fr_id        is NOT NULL
                  and cc.parent_price_event_id = wsd.price_event_id
                  and wsd.dept                 = fr.dept
                  and wsd.item                 = fr.item
                  and NVL(wsd.diff_id, '-999') = NVL(fr.diff_id, '-999')
                  and wsd.cur_hier_level       = fr.cur_hier_level
                  and wsd.location             = fr.location
                  and wsd.zone_node_type       = fr.zone_node_type
                  and wsd.action_date         >= fr.action_date) ra
        where ra.rank = 1;

BEGIN

   -- Assign the SGE errors back to the main CC error collection.  To ensure that the SGE's
   -- will have binoculars in the UI.  But before that, the SGE CC errors will have to
   -- be sanitized to look for real future retail record.  As well as convert the parent price event
   -- to the SGE price event id.

   open C_SGE_CC_ERROR;
   fetch C_SGE_CC_ERROR BULK COLLECT into O_cc_error_tbl;
   close C_SGE_CC_ERROR;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END SGE_OUTPUT;
----------------------------------------------------------------------------------------------

-- This function would be called when the PUSH_BACK process fails due to an unexpected PLSQL error.
FUNCTION RESET_PE_PUSH_BACK_FAILURE (IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_bulk_cc_pe_id    IN     NUMBER,
                                     I_pe_sequence_id   IN     NUMBER,
                                     I_pe_thread_number IN     NUMBER,
                                     I_rib_trans_id     IN     NUMBER,
                                     I_chunk_number     IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_THREADING_SQL.RESET_PE_PUSH_BACK_FAILURE';

   L_price_event_type VARCHAR2(3) := NULL;
   L_price_event_id   NUMBER      := NULL;
   L_pe_display_id    NUMBER      := NULL;

   cursor C_PE is
      select rbcpt.price_event_id,
             rbcpt.price_event_display_id,
             rbcp.price_event_type
        from rpm_bulk_cc_pe_thread rbcpt,
             rpm_bulk_cc_pe rbcp
       where rbcpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
         and rbcpt.parent_thread_number        = I_pe_sequence_id
         and rbcpt.thread_number               = I_pe_thread_number
         and rbcpt.man_txn_exclusion_parent_id is NULL
         and rbcp.bulk_cc_pe_id                = rbcpt.bulk_cc_pe_id
         and rbcp.price_event_type             = rbcpt.price_event_type;

BEGIN

   open C_PE;
   fetch C_PE into L_price_event_id,
                   L_pe_display_id,
                   L_price_event_type;
   close C_PE;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      update /*+ INDEX(rpc PK_RPM_PRICE_CHANGE) */ rpm_price_change rpc
         set state = RPM_CONSTANTS.PC_PENDING_STATE_CODE
       where price_change_id = L_price_event_id;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      update /*+ INDEX(rpd PK_RPM_PROMO_DTL) */ rpm_promo_dtl rpd
         set state = RPM_CONSTANTS.PR_PENDING_STATE_CODE
       where promo_dtl_id = L_price_event_id;

      if L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         update /*+ INDEX(rpd RPM_PROMO_DTL_I2) */ rpm_promo_dtl rpd
            set state = RPM_CONSTANTS.PR_PENDING_STATE_CODE
          where exception_parent_id = L_price_event_id
            and man_txn_exclusion   = 1;

      end if;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      update /*+ INDEX(rc PK_RPM_CLEARANCE) */ rpm_clearance rc
         set state = RPM_CONSTANTS.PC_PENDING_STATE_CODE
       where clearance_id = L_price_event_id;

   end if;

   if I_chunk_number is NULL then

     -- populate the CC table for the binocular in GUI when error_type is 1

      insert into rpm_con_check_err(con_check_err_id,
                                    ref_class,
                                    ref_id,
                                    ref_display_id,
                                    error_date,
                                    message_key)
         values (RPM_CON_CHECK_ERR_SEQ.NEXTVAL,
                 RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                 L_price_event_id,
                 L_pe_display_id,
                 LP_vdate,
                'push_back_processing_error_status_pending');

   else

      update rpm_bulk_cc_pe_chunk
         set push_back_status = RPM_CONSTANTS.CC_STATUS_IN_ERROR,
             transaction_id   = I_rib_trans_id
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and chunk_number         = I_chunk_number;

   end if;

   if IO_cc_error_tbl is NOT NULL then

      forall i IN 1 .. IO_cc_error_tbl.COUNT
         insert into rpm_chunk_con_check_tbl (bulk_cc_pe_id,
                                              parent_thread_number,
                                              thread_number,
                                              chunk_number,
                                              price_event_id,
                                              future_retail_id,
                                              error_type,
                                              error_string)
            values (I_bulk_cc_pe_id,
                    I_pe_sequence_id,
                    I_pe_thread_number,
                    I_chunk_number,
                    IO_cc_error_tbl(i).price_event_id,
                    IO_cc_error_tbl(i).future_retail_id,
                    IO_cc_error_tbl(i).error_type,
                    IO_cc_error_tbl(i).error_string);
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END RESET_PE_PUSH_BACK_FAILURE;
------------------------------------------------------------------------------------------
END RPM_CHUNK_CC_THREADING_SQL;
/
