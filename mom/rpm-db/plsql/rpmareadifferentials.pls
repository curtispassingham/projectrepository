CREATE OR REPLACE PACKAGE RPM_AREA_DIFFERENTIAL_SQL AS

	STRATEGY_TYPE_AREA_DIFF		CONSTANT VARCHAR2(1) := '0';
	STRATEGY_TYPE_CLEARANCE		CONSTANT VARCHAR2(1) := '1';
	STRATEGY_TYPE_COMPETITIVE	CONSTANT VARCHAR2(1) := '2';
	STRATEGY_TYPE_MARGIN		CONSTANT VARCHAR2(1) := '3';
	STRATEGY_TYPE_MAINTAIN_MARGIN	CONSTANT VARCHAR2(1) := '4';

--------------------------------------------------------

PROCEDURE SEARCH(O_return_code		OUT NUMBER,
                 O_error_msg		OUT VARCHAR2,
                 O_ad_tbl		OUT OBJ_AREA_DIFF_TBL,
                 O_ad_count		OUT NUMBER,
                 I_ad_strategy_ids	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_depts		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_classes		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_subclasses	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_zone_groups	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_zones		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_price_guides	IN  OBJ_NUMERIC_ID_TABLE);

--------------------------------------------------------

END RPM_AREA_DIFFERENTIAL_SQL;
/

