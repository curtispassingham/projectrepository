CREATE OR REPLACE PACKAGE RPM_PROMO_SQL AS
--------------------------------------------------------

FUNCTION GET_BASIS_RETAIL_VALUES(O_error_msg                IN OUT VARCHAR2,
                                 I_obj_rpm_promo_tbl        IN     OBJ_RPM_PROMO_TBL,
                                 O_obj_rpm_promo_values_tbl IN OUT OBJ_RPM_PROMO_VALUES_TBL)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION VALIDATE_FP_ITEM_EXCLUSION(O_error_msg        IN OUT VARCHAR2,
                                    O_merch_nodes         OUT OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                    I_merch_nodes      IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                    I_merch_nodes_excl IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION VALIDATE_IL_FOR_CANCEL(O_error_message                 OUT VARCHAR2,
                                O_display_participate_msg       OUT NUMBER,
                                O_valid_merch_nd_zone_nd        OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                                I_promo_dtl_id               IN     NUMBER,
                                I_merch_nd_zone_nd_to_check  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                I_curr_merch_nd_zone_nd_data IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION TXN_EXCL_TRAN_FROM_PARENT_DIFF(O_error_message    OUT VARCHAR2,
                                        O_tran_items       OUT OBJ_VARCHAR_ID_TABLE,
                                        I_parent_diffs  IN     OBJ_ITEM_DIFF_LOC_DATE_TBL)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION PROM_DTL_STATUS_UPDATE(O_error_message      OUT VARCHAR2,
                                I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                I_state           IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
END RPM_PROMO_SQL;
/
