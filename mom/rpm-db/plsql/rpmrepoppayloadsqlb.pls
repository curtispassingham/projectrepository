CREATE OR REPLACE PACKAGE BODY RPM_REPOP_PAYLOAD_DATA AS

--------------------------------------------------------------------------------
-- Package variables declaration
--------------------------------------------------------------------------------
LP_location    NUMBER := NULL;
LP_action_date DATE   := NULL;
LP_vdate       DATE   := GET_VDATE;

LP_cc_error_tbl   CONFLICT_CHECK_ERROR_TBL := NULL;
LP_depts          OBJ_NUMERIC_ID_TABLE     := NULL;
LP_price_event_id NUMBER(15)               := -99999;
LP_luw_count      NUMBER                   := 100000;


--------------------------------------------------------------------------------
-- Function Prototypes declaration
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Populate RPM_FUTURE_RETAIL_GTT based on the location and action_date
-- provided in the input paramters
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Update the respective MSG_TYPE columns of RPM_FUTURE_RETAIL_GTT.
-- For Clearance, update CLEAR_MSG_TYPE with the 'CRE' message.
-- For Price Change, update PC_MSG_TYPE with the 'CRE' message.
-- For Promotions, update PROMO_COMP_MSG_TYPE on rpm_fr_item_loc_expl_gtt with 'CRE' message
--------------------------------------------------------------------------------
FUNCTION UPDATE_MSG_TYPE(O_error_msg OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Call RPM_CC_PUBLISH to populate the PAYLOAD tables
--------------------------------------------------------------------------------
FUNCTION STAGE_MESSAGES(O_error_msg     OUT VARCHAR2,
                        I_thread    IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

FUNCTION POPULATE_GTT (O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_REPOP_PAYLOAD_DATA.POPULATE_GTT';

   -- Cursor Variables
   cursor C_LOCK_RFR is
      select /*+ ORDERED */
             rfr.rowid
        from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_future_retail rfr,
             rpm_zone_location rzl
       where rfr.dept                    = value(ids)
         and (   (    rfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rfr.location       = LP_location)
              or (    rfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rzl.location       = LP_location
                  and rzl.zone_id        = rfr.location))
         and rfr.action_date >= LP_action_date
         for update of rfr.future_retail_id nowait;

   cursor C_LOCK_PR is
      select /*+ ORDERED */
             rfr.rowid
        from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_item_loc_expl rfr,
             rpm_zone_location rzl
       where rfr.dept                    = value(ids)
         and (   (    rfr.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rfr.location       = LP_location)
              or (    rfr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rzl.location       = LP_location
                  and rzl.zone_id        = rfr.location))
         and rfr.detail_start_date       >= LP_action_date
         for update of rfr.promo_item_loc_expl_id nowait;

BEGIN

   -- Empty the GTT tables
   delete from rpm_future_retail_gtt;
   delete from rpm_promo_item_loc_expl_gtt;

   -- Empty the payload tables

   delete from rpm_promo_item_payload;
   delete from rpm_promo_disc_ldr_payload;
   delete from rpm_promo_dtl_mn_payload;
   delete from rpm_promo_dtl_list_payload;
   delete from rpm_promo_dtl_list_grp_payload;
   delete from rpm_price_chg_payload;
   delete from rpm_clearance_payload;
   delete from rpm_promo_item_loc_sr_payload;
   delete from rpm_promo_location_payload;
   delete from rpm_promo_dtl_prc_rng_payload;
   delete from rpm_fin_cred_dtl_payload;
   delete from rpm_threshold_int_payload;
   delete from rpm_promo_dtl_payload;
   delete from rpm_price_event_payload;

   -- Lock the records in RPM_FUTURE_RETAIL to ensure that no events are added/modified when the batch is run
   open C_LOCK_RFR;
   close C_LOCK_RFR;

   -- Copy the records from RPM_FUTURE_RETAIL into RPM_FUTURE_RETAIL_GTT

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       item,
       dept,
       class,
       subclass,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       on_simple_promo_ind,
       on_complex_promo_ind,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       item_parent,
       zone_id,
       cur_hier_level)
   select LP_price_event_id,
          in2.future_retail_id,
          in2.to_item,
          in2.dept,
          in2.class,
          in2.subclass,
          in2.to_loc_type,
          in2.to_location,
          in2.action_date,
          in2.selling_retail,
          in2.selling_retail_currency,
          in2.selling_uom,
          in2.multi_units,
          in2.multi_unit_retail,
          in2.multi_unit_retail_currency,
          in2.multi_selling_uom,
          in2.clear_retail,
          in2.clear_retail_currency,
          in2.clear_uom,
          in2.simple_promo_retail,
          in2.simple_promo_retail_currency,
          in2.simple_promo_uom,
          in2.on_simple_promo_ind,
          in2.on_complex_promo_ind,
          in2.price_change_id,
          in2.price_change_display_id,
          in2.pc_exception_parent_id,
          in2.pc_change_type,
          in2.pc_change_amount,
          in2.pc_change_currency,
          in2.pc_change_percent,
          in2.pc_change_selling_uom,
          in2.pc_null_multi_ind,
          in2.pc_multi_units,
          in2.pc_multi_unit_retail,
          in2.pc_multi_unit_retail_currency,
          in2.pc_multi_selling_uom,
          in2.pc_price_guide_id,
          in2.clearance_id,
          in2.clearance_display_id,
          in2.clear_mkdn_index,
          in2.clear_start_ind,
          in2.clear_change_type,
          in2.clear_change_amount,
          in2.clear_change_currency,
          in2.clear_change_percent,
          in2.clear_change_selling_uom,
          in2.clear_price_guide_id,
          in2.loc_move_from_zone_id,
          in2.loc_move_to_zone_id,
          in2.location_move_id,
          in2.to_item_parent,
          in2.to_zone_id,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC
     from (select in1.future_retail_id,
                  in1.to_item,
                  in1.dept,
                  in1.class,
                  in1.subclass,
                  in1.to_loc_type,
                  in1.to_location,
                  in1.action_date,
                  in1.selling_retail,
                  in1.selling_retail_currency,
                  in1.selling_uom,
                  in1.multi_units,
                  in1.multi_unit_retail,
                  in1.multi_unit_retail_currency,
                  in1.multi_selling_uom,
                  in1.clear_retail,
                  in1.clear_retail_currency,
                  in1.clear_uom,
                  in1.simple_promo_retail,
                  in1.simple_promo_retail_currency,
                  in1.simple_promo_uom,
                  in1.on_simple_promo_ind,
                  in1.on_complex_promo_ind,
                  in1.price_change_id,
                  in1.price_change_display_id,
                  in1.pc_exception_parent_id,
                  in1.pc_change_type,
                  in1.pc_change_amount,
                  in1.pc_change_currency,
                  in1.pc_change_percent,
                  in1.pc_change_selling_uom,
                  in1.pc_null_multi_ind,
                  in1.pc_multi_units,
                  in1.pc_multi_unit_retail,
                  in1.pc_multi_unit_retail_currency,
                  in1.pc_multi_selling_uom,
                  in1.pc_price_guide_id,
                  in1.clearance_id,
                  in1.clearance_display_id,
                  in1.clear_mkdn_index,
                  in1.clear_start_ind,
                  in1.clear_change_type,
                  in1.clear_change_amount,
                  in1.clear_change_currency,
                  in1.clear_change_percent,
                  in1.clear_change_selling_uom,
                  in1.clear_price_guide_id,
                  in1.loc_move_from_zone_id,
                  in1.loc_move_to_zone_id,
                  in1.location_move_id,
                  in1.to_item_parent,
                  in1.to_zone_id,
                  in1.hier_rank,
                  MAX(in1.hier_rank) OVER (PARTITION BY in1.to_item,
                                                        in1.to_location) max_rank
             from (-- Get all IL level data, rank it 3
                   select /*+ INDEX(fr RPM_FUTURE_RETAIL_I4) ORDERED */
                          rfr.future_retail_id,
                          rfr.dept,
                          rfr.class,
                          rfr.subclass,
                          rfr.action_date,
                          rfr.selling_retail,
                          rfr.selling_retail_currency,
                          rfr.selling_uom,
                          rfr.multi_units,
                          rfr.multi_unit_retail,
                          rfr.multi_unit_retail_currency,
                          rfr.multi_selling_uom,
                          rfr.clear_retail,
                          rfr.clear_retail_currency,
                          rfr.clear_uom,
                          rfr.simple_promo_retail,
                          rfr.simple_promo_retail_currency,
                          rfr.simple_promo_uom,
                          rfr.on_simple_promo_ind,
                          rfr.on_complex_promo_ind,
                          rfr.price_change_id,
                          rfr.price_change_display_id,
                          rfr.pc_exception_parent_id,
                          rfr.pc_change_type,
                          rfr.pc_change_amount,
                          rfr.pc_change_currency,
                          rfr.pc_change_percent,
                          rfr.pc_change_selling_uom,
                          rfr.pc_null_multi_ind,
                          rfr.pc_multi_units,
                          rfr.pc_multi_unit_retail,
                          rfr.pc_multi_unit_retail_currency,
                          rfr.pc_multi_selling_uom,
                          rfr.pc_price_guide_id,
                          rfr.clearance_id,
                          rfr.clearance_display_id,
                          rfr.clear_mkdn_index,
                          rfr.clear_start_ind,
                          rfr.clear_change_type,
                          rfr.clear_change_amount,
                          rfr.clear_change_currency,
                          rfr.clear_change_percent,
                          rfr.clear_change_selling_uom,
                          rfr.clear_price_guide_id,
                          rfr.loc_move_from_zone_id,
                          rfr.loc_move_to_zone_id,
                          rfr.location_move_id,
                          rfr.item           to_item,
                          rfr.item_parent    to_item_parent,
                          rfr.location       to_location,
                          rfr.zone_node_type to_loc_type,
                          rfr.zone_id        to_zone_id,
                          3 hier_rank
                     from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_future_retail rfr
                    where rfr.dept            = value(ids)
                      and rfr.location        = LP_location
                      and rfr.action_date    >= LP_action_date
                      and rfr.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                      and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                   -- Get all IZ level data and rank it 2
                   union all
                   select /*+ INDEX(fr RPM_FUTURE_RETAIL_I4) ORDERED */
                          rfr.future_retail_id,
                          rfr.dept,
                          rfr.class,
                          rfr.subclass,
                          rfr.action_date,
                          rfr.selling_retail,
                          rfr.selling_retail_currency,
                          rfr.selling_uom,
                          rfr.multi_units,
                          rfr.multi_unit_retail,
                          rfr.multi_unit_retail_currency,
                          rfr.multi_selling_uom,
                          rfr.clear_retail,
                          rfr.clear_retail_currency,
                          rfr.clear_uom,
                          rfr.simple_promo_retail,
                          rfr.simple_promo_retail_currency,
                          rfr.simple_promo_uom,
                          rfr.on_simple_promo_ind,
                          rfr.on_complex_promo_ind,
                          rfr.price_change_id,
                          rfr.price_change_display_id,
                          rfr.pc_exception_parent_id,
                          rfr.pc_change_type,
                          rfr.pc_change_amount,
                          rfr.pc_change_currency,
                          rfr.pc_change_percent,
                          rfr.pc_change_selling_uom,
                          rfr.pc_null_multi_ind,
                          rfr.pc_multi_units,
                          rfr.pc_multi_unit_retail,
                          rfr.pc_multi_unit_retail_currency,
                          rfr.pc_multi_selling_uom,
                          rfr.pc_price_guide_id,
                          rfr.clearance_id,
                          rfr.clearance_display_id,
                          rfr.clear_mkdn_index,
                          rfr.clear_start_ind,
                          rfr.clear_change_type,
                          rfr.clear_change_amount,
                          rfr.clear_change_currency,
                          rfr.clear_change_percent,
                          rfr.clear_change_selling_uom,
                          rfr.clear_price_guide_id,
                          rfr.loc_move_from_zone_id,
                          rfr.loc_move_to_zone_id,
                          rfr.location_move_id,
                          rfr.item        to_item,
                          rfr.item_parent to_item_parent,
                          rzl.location    to_location,
                          rzl.loc_type    to_loc_type,
                          rzl.zone_id     to_zone_id,
                          2 hier_rank
                     from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_zone_location rzl,
                          rpm_future_retail rfr,
                          rpm_item_loc ril
                    where rfr.dept            = value(ids)
                      and rzl.location        = LP_location
                      and rfr.location        = rzl.zone_id
                      and rfr.action_date    >= LP_action_date
                      and rfr.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      and ril.item            = rfr.item
                      and ril.loc             = rzl.location
                   -- Get all PDL level data and rank it 2
                   union all
                   select /*+ INDEX(fr RPM_FUTURE_RETAIL_I4) ORDERD */
                          rfr.future_retail_id,
                          rfr.dept,
                          rfr.class,
                          rfr.subclass,
                          rfr.action_date,
                          rfr.selling_retail,
                          rfr.selling_retail_currency,
                          rfr.selling_uom,
                          rfr.multi_units,
                          rfr.multi_unit_retail,
                          rfr.multi_unit_retail_currency,
                          rfr.multi_selling_uom,
                          rfr.clear_retail,
                          rfr.clear_retail_currency,
                          rfr.clear_uom,
                          rfr.simple_promo_retail,
                          rfr.simple_promo_retail_currency,
                          rfr.simple_promo_uom,
                          rfr.on_simple_promo_ind,
                          rfr.on_complex_promo_ind,
                          rfr.price_change_id,
                          rfr.price_change_display_id,
                          rfr.pc_exception_parent_id,
                          rfr.pc_change_type,
                          rfr.pc_change_amount,
                          rfr.pc_change_currency,
                          rfr.pc_change_percent,
                          rfr.pc_change_selling_uom,
                          rfr.pc_null_multi_ind,
                          rfr.pc_multi_units,
                          rfr.pc_multi_unit_retail,
                          rfr.pc_multi_unit_retail_currency,
                          rfr.pc_multi_selling_uom,
                          rfr.pc_price_guide_id,
                          rfr.clearance_id,
                          rfr.clearance_display_id,
                          rfr.clear_mkdn_index,
                          rfr.clear_start_ind,
                          rfr.clear_change_type,
                          rfr.clear_change_amount,
                          rfr.clear_change_currency,
                          rfr.clear_change_percent,
                          rfr.clear_change_selling_uom,
                          rfr.clear_price_guide_id,
                          rfr.loc_move_from_zone_id,
                          rfr.loc_move_to_zone_id,
                          rfr.location_move_id,
                          im.item            to_item,
                          im.item_parent     to_item_parent,
                          rfr.location       to_location,
                          rfr.zone_node_type to_loc_type,
                          rfr.zone_id        to_zone_id,
                          2 hier_rank
                     from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
                          item_master im,
                          rpm_future_retail rfr,
                          rpm_item_loc ril
                    where rfr.dept            = value(ids)
                      and rfr.item            = im.item_parent
                      and rfr.diff_id         is NOT NULL
                      and rfr.diff_id         = im.diff_1
                      and im.item_level       = im.tran_level
                      and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and rfr.location        = LP_location
                      and rfr.action_date    >= LP_action_date
                      and rfr.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                      and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                      and ril.item            = im.item
                      and ril.loc             = rfr.location
                   -- Get all PDZ data and rank it 1
                   union all
                   select /*+ INDEX(fr RPM_FUTURE_RETAIL_I4) ORDERED */
                          rfr.future_retail_id,
                          rfr.dept,
                          rfr.class,
                          rfr.subclass,
                          rfr.action_date,
                          rfr.selling_retail,
                          rfr.selling_retail_currency,
                          rfr.selling_uom,
                          rfr.multi_units,
                          rfr.multi_unit_retail,
                          rfr.multi_unit_retail_currency,
                          rfr.multi_selling_uom,
                          rfr.clear_retail,
                          rfr.clear_retail_currency,
                          rfr.clear_uom,
                          rfr.simple_promo_retail,
                          rfr.simple_promo_retail_currency,
                          rfr.simple_promo_uom,
                          rfr.on_simple_promo_ind,
                          rfr.on_complex_promo_ind,
                          rfr.price_change_id,
                          rfr.price_change_display_id,
                          rfr.pc_exception_parent_id,
                          rfr.pc_change_type,
                          rfr.pc_change_amount,
                          rfr.pc_change_currency,
                          rfr.pc_change_percent,
                          rfr.pc_change_selling_uom,
                          rfr.pc_null_multi_ind,
                          rfr.pc_multi_units,
                          rfr.pc_multi_unit_retail,
                          rfr.pc_multi_unit_retail_currency,
                          rfr.pc_multi_selling_uom,
                          rfr.pc_price_guide_id,
                          rfr.clearance_id,
                          rfr.clearance_display_id,
                          rfr.clear_mkdn_index,
                          rfr.clear_start_ind,
                          rfr.clear_change_type,
                          rfr.clear_change_amount,
                          rfr.clear_change_currency,
                          rfr.clear_change_percent,
                          rfr.clear_change_selling_uom,
                          rfr.clear_price_guide_id,
                          rfr.loc_move_from_zone_id,
                          rfr.loc_move_to_zone_id,
                          rfr.location_move_id,
                          im.item        to_item,
                          im.item_parent to_item_parent,
                          rzl.location   to_location,
                          rzl.loc_type   to_loc_type,
                          rzl.zone_id    to_zone_id,
                          1 hier_rank
                     from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_zone_location rzl,
                          item_master im,
                          rpm_future_retail rfr,
                          rpm_item_loc ril
                    where rfr.dept            = value(ids)
                      and rfr.item            = im.item_parent
                      and rfr.diff_id         is NOT NULL
                      and rfr.diff_id         = im.diff_1
                      and im.item_level       = im.tran_level
                      and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and rzl.location        = LP_location
                      and rfr.location        = rzl.zone_id
                      and rfr.action_date    >= LP_action_date
                      and rfr.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                      and ril.item            = im.item
                      and ril.loc             = rzl.location
                   -- Get all PL data and rank it 1
                   union all
                   select /*+ INDEX(fr RPM_FUTURE_RETAIL_I4) ORDERED */
                          rfr.future_retail_id,
                          rfr.dept,
                          rfr.class,
                          rfr.subclass,
                          rfr.action_date,
                          rfr.selling_retail,
                          rfr.selling_retail_currency,
                          rfr.selling_uom,
                          rfr.multi_units,
                          rfr.multi_unit_retail,
                          rfr.multi_unit_retail_currency,
                          rfr.multi_selling_uom,
                          rfr.clear_retail,
                          rfr.clear_retail_currency,
                          rfr.clear_uom,
                          rfr.simple_promo_retail,
                          rfr.simple_promo_retail_currency,
                          rfr.simple_promo_uom,
                          rfr.on_simple_promo_ind,
                          rfr.on_complex_promo_ind,
                          rfr.price_change_id,
                          rfr.price_change_display_id,
                          rfr.pc_exception_parent_id,
                          rfr.pc_change_type,
                          rfr.pc_change_amount,
                          rfr.pc_change_currency,
                          rfr.pc_change_percent,
                          rfr.pc_change_selling_uom,
                          rfr.pc_null_multi_ind,
                          rfr.pc_multi_units,
                          rfr.pc_multi_unit_retail,
                          rfr.pc_multi_unit_retail_currency,
                          rfr.pc_multi_selling_uom,
                          rfr.pc_price_guide_id,
                          rfr.clearance_id,
                          rfr.clearance_display_id,
                          rfr.clear_mkdn_index,
                          rfr.clear_start_ind,
                          rfr.clear_change_type,
                          rfr.clear_change_amount,
                          rfr.clear_change_currency,
                          rfr.clear_change_percent,
                          rfr.clear_change_selling_uom,
                          rfr.clear_price_guide_id,
                          rfr.loc_move_from_zone_id,
                          rfr.loc_move_to_zone_id,
                          rfr.location_move_id,
                          im.item            to_item,
                          im.item_parent     to_item_parent,
                          rfr.location       to_location,
                          rfr.zone_node_type to_loc_type,
                          rfr.zone_id        to_zone_id,
                          1 hier_rank
                     from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
                          item_master im,
                          rpm_future_retail rfr,
                          rpm_item_loc ril
                    where rfr.dept            = value(ids)
                      and rfr.item            = im.item_parent
                      and rfr.diff_id         is NULL
                      and im.item_level       = im.tran_level
                      and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and rfr.location        = LP_location
                      and rfr.action_date    >= LP_action_date
                      and rfr.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                      and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      and ril.item            = im.item
                      and ril.loc             = rfr.location
                   -- Get all PZ data and rank it 0
                   union all
                   select /*+ INDEX(fr RPM_FUTURE_RETAIL_I4) ORDERED */
                          rfr.future_retail_id,
                          rfr.dept,
                          rfr.class,
                          rfr.subclass,
                          rfr.action_date,
                          rfr.selling_retail,
                          rfr.selling_retail_currency,
                          rfr.selling_uom,
                          rfr.multi_units,
                          rfr.multi_unit_retail,
                          rfr.multi_unit_retail_currency,
                          rfr.multi_selling_uom,
                          rfr.clear_retail,
                          rfr.clear_retail_currency,
                          rfr.clear_uom,
                          rfr.simple_promo_retail,
                          rfr.simple_promo_retail_currency,
                          rfr.simple_promo_uom,
                          rfr.on_simple_promo_ind,
                          rfr.on_complex_promo_ind,
                          rfr.price_change_id,
                          rfr.price_change_display_id,
                          rfr.pc_exception_parent_id,
                          rfr.pc_change_type,
                          rfr.pc_change_amount,
                          rfr.pc_change_currency,
                          rfr.pc_change_percent,
                          rfr.pc_change_selling_uom,
                          rfr.pc_null_multi_ind,
                          rfr.pc_multi_units,
                          rfr.pc_multi_unit_retail,
                          rfr.pc_multi_unit_retail_currency,
                          rfr.pc_multi_selling_uom,
                          rfr.pc_price_guide_id,
                          rfr.clearance_id,
                          rfr.clearance_display_id,
                          rfr.clear_mkdn_index,
                          rfr.clear_start_ind,
                          rfr.clear_change_type,
                          rfr.clear_change_amount,
                          rfr.clear_change_currency,
                          rfr.clear_change_percent,
                          rfr.clear_change_selling_uom,
                          rfr.clear_price_guide_id,
                          rfr.loc_move_from_zone_id,
                          rfr.loc_move_to_zone_id,
                          rfr.location_move_id,
                          im.item        to_item,
                          im.item_parent to_item_parent,
                          rzl.location   to_location,
                          rzl.loc_type   to_loc_type,
                          rzl.zone_id    to_zone_id,
                          0 hier_rank
                     from table(cast(LP_depts as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_zone_location rzl,
                          item_master im,
                          rpm_future_retail rfr,
                          rpm_item_loc ril
                    where rfr.dept            = value(ids)
                      and rfr.item            = im.item_parent
                      and rfr.diff_id         is NULL
                      and im.item_level       = im.tran_level
                      and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                      and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      and rzl.location        = LP_location
                      and rfr.location        = rzl.zone_id
                      and rfr.action_date    >= LP_action_date
                      and rfr.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                      and ril.item            = im.item
                      and ril.loc             = rzl.location) in1) in2
    where in2.hier_rank = in2.max_rank;

   open C_LOCK_PR;
   close C_LOCK_PR;

   insert into rpm_promo_item_loc_expl_gtt
     (price_event_id,
      promo_item_loc_expl_id,
      item,
      dept,
      class,
      subclass,
      location,
      zone_node_type,
      promo_id,
      promo_display_id,
      promo_secondary_ind,
      promo_comp_id,
      comp_display_id,
      promo_dtl_id,
      type,
      customer_type,
      detail_secondary_ind,
      detail_start_date,
      detail_end_date,
      detail_apply_to_code,
      detail_change_type,
      detail_change_amount,
      detail_change_currency,
      detail_change_percent,
      detail_change_selling_uom,
      detail_price_guide_id,
      exception_parent_id)
   select in2.price_event_id,
          in2.promo_item_loc_expl_id,
          in2.tran_item,
          in2.dept,
          in2.class,
          in2.subclass,
          in2.to_location,
          in2.to_zone_node_type,
          in2.promo_id,
          in2.promo_display_id,
          in2.promo_secondary_ind,
          in2.promo_comp_id,
          in2.comp_display_id,
          in2.promo_dtl_id,
          in2.type,
          in2.customer_type,
          in2.detail_secondary_ind,
          in2.detail_start_date,
          in2.detail_end_date,
          in2.detail_apply_to_code,
          in2.detail_change_type,
          in2.detail_change_amount,
          in2.detail_change_currency,
          in2.detail_change_percent,
          in2.detail_change_selling_uom,
          in2.detail_price_guide_id,
          in2.exception_parent_id
     from (select in1.price_event_id,
                  in1.promo_item_loc_expl_id,
                  in1.tran_item,
                  in1.dept,
                  in1.class,
                  in1.subclass,
                  in1.to_location,
                  in1.to_zone_node_type,
                  in1.promo_id,
                  in1.promo_display_id,
                  in1.promo_secondary_ind,
                  in1.promo_comp_id,
                  in1.comp_display_id,
                  in1.promo_dtl_id,
                  in1.type,
                  in1.customer_type,
                  in1.detail_secondary_ind,
                  in1.detail_start_date,
                  in1.detail_end_date,
                  in1.detail_apply_to_code,
                  in1.detail_change_type,
                  in1.detail_change_amount,
                  in1.detail_change_currency,
                  in1.detail_change_percent,
                  in1.detail_change_selling_uom,
                  in1.detail_price_guide_id,
                  in1.exception_parent_id,
                  in1.hier_rank,
                  MAX(in1.hier_rank) OVER (PARTITION BY in1.tran_item,
                                                        in1.location) max_rank
             from (-- Get all Item Loc data and rank it 3
                   select distinct
                          fr.price_event_id,
                          pe.promo_item_loc_expl_id,
                          pe.dept,
                          pe.class,
                          pe.subclass,
                          pe.promo_id,
                          pe.promo_display_id,
                          pe.promo_secondary_ind,
                          pe.promo_comp_id,
                          pe.comp_display_id,
                          pe.promo_dtl_id,
                          pe.type,
                          pe.customer_type,
                          pe.detail_secondary_ind,
                          pe.detail_start_date,
                          pe.detail_end_date,
                          pe.detail_apply_to_code,
                          pe.detail_change_type,
                          pe.detail_change_amount,
                          pe.detail_change_currency,
                          pe.detail_change_percent,
                          pe.detail_change_selling_uom,
                          pe.detail_price_guide_id,
                          pe.exception_parent_id,
                          pe.location,
                          pe.item           tran_item,
                          fr.location       to_location,
                          fr.zone_node_type to_zone_node_type,
                          3 hier_rank
                     from rpm_promo_item_loc_expl pe,
                          rpm_future_retail_gtt fr
                    where pe.dept               = fr.dept
                      and pe.item               = fr.item
                      and pe.location           = fr.location
                      and pe.cur_hier_level     = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                      and pe.detail_start_date >= LP_action_date
                   union all
                   -- Get all Item Zone data and rank it 2
                   select distinct
                          fr.price_event_id,
                          pe.promo_item_loc_expl_id,
                          pe.dept,
                          pe.class,
                          pe.subclass,
                          pe.promo_id,
                          pe.promo_display_id,
                          pe.promo_secondary_ind,
                          pe.promo_comp_id,
                          pe.comp_display_id,
                          pe.promo_dtl_id,
                          pe.type,
                          pe.customer_type,
                          pe.detail_secondary_ind,
                          pe.detail_start_date,
                          pe.detail_end_date,
                          pe.detail_apply_to_code,
                          pe.detail_change_type,
                          pe.detail_change_amount,
                          pe.detail_change_currency,
                          pe.detail_change_percent,
                          pe.detail_change_selling_uom,
                          pe.detail_price_guide_id,
                          pe.exception_parent_id,
                          pe.location,
                          pe.item           tran_item,
                          fr.location       to_location,
                          fr.zone_node_type to_zone_node_type,
                          2 hier_rank
                     from rpm_promo_item_loc_expl pe,
                          rpm_future_retail_gtt fr
                    where pe.dept               = fr.dept
                      and pe.item               = fr.item
                      and pe.location           = fr.zone_id
                      and pe.cur_hier_level     = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      and pe.detail_start_date >= LP_action_date
                   union all
                   -- Get all Parent Diff Loc data and rank it 2
                   select distinct
                          fr.price_event_id,
                          pe.promo_item_loc_expl_id,
                          pe.dept,
                          pe.class,
                          pe.subclass,
                          pe.promo_id,
                          pe.promo_display_id,
                          pe.promo_secondary_ind,
                          pe.promo_comp_id,
                          pe.comp_display_id,
                          pe.promo_dtl_id,
                          pe.type,
                          pe.customer_type,
                          pe.detail_secondary_ind,
                          pe.detail_start_date,
                          pe.detail_end_date,
                          pe.detail_apply_to_code,
                          pe.detail_change_type,
                          pe.detail_change_amount,
                          pe.detail_change_currency,
                          pe.detail_change_percent,
                          pe.detail_change_selling_uom,
                          pe.detail_price_guide_id,
                          pe.exception_parent_id,
                          pe.location,
                          fr.item           tran_item,
                          fr.location       to_location,
                          fr.zone_node_type to_zone_node_type,
                          2 hier_rank
                     from rpm_promo_item_loc_expl pe,
                          rpm_future_retail_gtt fr
                    where pe.dept               = fr.dept
                      and pe.item               = fr.item_parent
                      and pe.diff_id            is NOT NULL
                      and pe.diff_id            is NOT NULL
                      and pe.diff_id            = fr.diff_id
                      and pe.location           = fr.location
                      and pe.cur_hier_level     = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                      and pe.detail_start_date >= LP_action_date
                   union all
                   -- Get all Parent Diff Zone data and rank it 1
                   select distinct
                          fr.price_event_id,
                          pe.promo_item_loc_expl_id,
                          pe.dept,
                          pe.class,
                          pe.subclass,
                          pe.promo_id,
                          pe.promo_display_id,
                          pe.promo_secondary_ind,
                          pe.promo_comp_id,
                          pe.comp_display_id,
                          pe.promo_dtl_id,
                          pe.type,
                          pe.customer_type,
                          pe.detail_secondary_ind,
                          pe.detail_start_date,
                          pe.detail_end_date,
                          pe.detail_apply_to_code,
                          pe.detail_change_type,
                          pe.detail_change_amount,
                          pe.detail_change_currency,
                          pe.detail_change_percent,
                          pe.detail_change_selling_uom,
                          pe.detail_price_guide_id,
                          pe.exception_parent_id,
                          pe.location,
                          fr.item           tran_item,
                          fr.location       to_location,
                          fr.zone_node_type to_zone_node_type,
                          1 hier_rank
                     from rpm_promo_item_loc_expl pe,
                          rpm_future_retail_gtt fr
                    where pe.dept               = fr.dept
                      and pe.item               = fr.item_parent
                      and pe.diff_id            is NOT NULL
                      and pe.diff_id            is NOT NULL
                      and pe.diff_id            = fr.diff_id
                      and pe.location           = fr.zone_id
                      and pe.cur_hier_level     = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                      and pe.detail_start_date >= LP_action_date
                   union all
                   -- Get all Parent Loc data and rank it 1
                   select distinct
                          fr.price_event_id,
                          pe.promo_item_loc_expl_id,
                          pe.dept,
                          pe.class,
                          pe.subclass,
                          pe.promo_id,
                          pe.promo_display_id,
                          pe.promo_secondary_ind,
                          pe.promo_comp_id,
                          pe.comp_display_id,
                          pe.promo_dtl_id,
                          pe.type,
                          pe.customer_type,
                          pe.detail_secondary_ind,
                          pe.detail_start_date,
                          pe.detail_end_date,
                          pe.detail_apply_to_code,
                          pe.detail_change_type,
                          pe.detail_change_amount,
                          pe.detail_change_currency,
                          pe.detail_change_percent,
                          pe.detail_change_selling_uom,
                          pe.detail_price_guide_id,
                          pe.exception_parent_id,
                          pe.location,
                          fr.item           tran_item,
                          fr.location       to_location,
                          fr.zone_node_type to_zone_node_type,
                          1 hier_rank
                     from rpm_promo_item_loc_expl pe,
                          rpm_future_retail_gtt fr
                    where pe.dept               = fr.dept
                      and pe.item               = fr.item_parent
                      and pe.diff_id            is NULL
                      and pe.location           = fr.location
                      and pe.cur_hier_level     = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      and pe.detail_start_date >= LP_action_date
                   union all
                   -- Get all Parent Zone data and rank it 0
                   select distinct
                          fr.price_event_id,
                          pe.promo_item_loc_expl_id,
                          pe.dept,
                          pe.class,
                          pe.subclass,
                          pe.promo_id,
                          pe.promo_display_id,
                          pe.promo_secondary_ind,
                          pe.promo_comp_id,
                          pe.comp_display_id,
                          pe.promo_dtl_id,
                          pe.type,
                          pe.customer_type,
                          pe.detail_secondary_ind,
                          pe.detail_start_date,
                          pe.detail_end_date,
                          pe.detail_apply_to_code,
                          pe.detail_change_type,
                          pe.detail_change_amount,
                          pe.detail_change_currency,
                          pe.detail_change_percent,
                          pe.detail_change_selling_uom,
                          pe.detail_price_guide_id,
                          pe.exception_parent_id,
                          pe.location,
                          fr.item           tran_item,
                          fr.location       to_location,
                          fr.zone_node_type to_zone_node_type,
                          0 hier_rank
                     from rpm_promo_item_loc_expl pe,
                          rpm_future_retail_gtt fr
                    where pe.dept               = fr.dept
                      and pe.item               = fr.item_parent
                      and pe.diff_id            is NULL
                      and pe.location           = fr.zone_id
                      and pe.cur_hier_level     = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                      and pe.detail_start_date >= LP_action_date) in1) in2
    where in2.hier_rank = in2.max_rank;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END POPULATE_GTT;
--------------------------------------------------------------------------------

FUNCTION UPDATE_MSG_TYPE(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_REPOP_PAYLOAD_DATA.UPDATE_MSG_TYPE';

BEGIN

   -- Update Price Changes, Clearances and Promotions
   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 location,
                 action_date
            from rpm_future_retail_gtt
           where price_event_id = LP_price_event_id) source
   on (    target.price_event_id = source.price_event_id
       and target.item           = source.item
       and target.location       = source.location
       and target.action_date    = source.action_date)
   when MATCHED then
      update
         set target.pc_msg_type    = case
                                        when target.price_change_id is NOT NULL then
                                           RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE
                                     end,
             target.clear_msg_type = case
                                        when target.clearance_id is NOT NULL and
                                             target.clear_start_ind IN (RPM_CONSTANTS.START_IND,
                                                                        RPM_CONSTANTS.START_END_IND) then
                                        ---
                                        RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE
                                     end;

   update rpm_promo_item_loc_expl_gtt
      set promo_comp_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
    where dept              IN (select L_depts.column_value
                                  from table(cast(LP_depts AS OBJ_NUMERIC_ID_TABLE)) L_depts)
      and location           = LP_location
      and detail_start_date >= LP_action_date;

   update rpm_promo_fr_item_loc_gtt
      set promo_comp_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
    where dept              IN (select L_depts.column_value
                                  from table(cast(LP_depts AS OBJ_NUMERIC_ID_TABLE)) L_depts)
      and location           = LP_location
      and detail_start_date >= LP_action_date;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END UPDATE_MSG_TYPE;
--------------------------------------------------------------------------------

FUNCTION STAGE_MESSAGES(O_error_msg    OUT VARCHAR2,
                        I_thread    IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_REPOP_PAYLOAD_DATA.STAGE_MESSAGES';

   L_rib_trans_id               NUMBER               := I_thread;
   L_price_event_ids_to_process OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   cursor C_PRICE_CHANGE_IDS is
      select distinct price_change_id
        from rpm_fr_item_loc_expl_gtt
       where pc_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE;

   cursor C_CLEAR_IDS is
      select distinct clearance_id
        from rpm_fr_item_loc_expl_gtt
       where clear_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE;

   cursor C_SIMPLE_PROMO_DTL_IDS is
      select distinct promo_dtl_id
        from rpm_promo_item_loc_expl_gtt
       where promo_comp_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
         and type                = RPM_CONSTANTS.SIMPLE_CODE;

   cursor C_COMPLEX_PROMO_DTL_IDS is
      select distinct promo_dtl_id
        from rpm_promo_item_loc_expl_gtt
       where promo_comp_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
         and type                IN (RPM_CONSTANTS.COMPLEX_CODE,
                                     RPM_CONSTANTS.THRESHOLD_CODE);

BEGIN

   delete from rpm_fr_item_loc_expl_gtt;

   insert into rpm_fr_item_loc_expl_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       pc_msg_type,
       clear_msg_type,
       pc_selling_retail_ind,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_multi_unit_ind,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       promo_dtl_id,
       detail_start_date,
       detail_end_date,
       type,
       promo_id,
       exception_parent_id)
      select rfr.price_event_id,
             future_retail_id,
             rfr.dept,
             rfr.class,
             rfr.subclass,
             rfr.item,
             rfr.zone_node_type,
             rfr.location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             pc_msg_type,
             clear_msg_type,
             pc_selling_retail_ind,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_multi_unit_ind,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             rpile.promo_dtl_id,
             rpile.detail_start_date,
             rpile.detail_end_date,
             rpile.type,
             rpile.promo_id,
             rpile.exception_parent_id
        from rpm_future_retail_gtt rfr,
             rpm_promo_item_loc_expl_gtt rpile
       where rfr.price_event_id     = rpile.price_event_id (+)
         and rfr.item               = rpile.item(+)
         and rfr.location           = rpile.location(+)
         and rfr.dept               = rpile.dept(+)
         and rfr.action_date        BETWEEN rpile.detail_start_date(+) and NVL(rpile.detail_end_date(+), TO_DATE('3000', 'YYYY'));

   open C_PRICE_CHANGE_IDS;
   fetch C_PRICE_CHANGE_IDS BULK COLLECT into L_price_event_ids_to_process;
   close C_PRICE_CHANGE_IDS;

   if RPM_CC_PUBLISH.STAGE_PC_MESSAGES(LP_cc_error_tbl,
                                       L_rib_trans_id,
                                       NULL, -- I_bulk_cc_pe_id
                                       L_price_event_ids_to_process,
                                       0, -- I_chunk_number
                                       1) = 0 then -- I_repoppay_ind
      if LP_cc_error_tbl is NOT NULL or
         LP_cc_error_tbl.COUNT > 0 then

         O_error_msg := 'Error in loading Price change messages: '||LP_cc_error_tbl(1).error_string;

      end if;

      return FALSE;

   end if;

   open C_CLEAR_IDS;
   fetch C_CLEAR_IDS BULK COLLECT into L_price_event_ids_to_process;
   close C_CLEAR_IDS;

   if RPM_CC_PUBLISH.STAGE_CLR_MESSAGES(LP_cc_error_tbl,
                                        L_rib_trans_id,
                                        NULL, -- I_bulk_cc_pe_id
                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                        L_price_event_ids_to_process,
                                        0) = 0 then
      if LP_cc_error_tbl is NOT NULL or
         LP_cc_error_tbl.COUNT > 0 then

         O_error_msg := 'Error in loading Clearance Messages: '||LP_cc_error_tbl(1).error_string;

      end if;

      return FALSE;

   end if;

   open  C_SIMPLE_PROMO_DTL_IDS;
   fetch C_SIMPLE_PROMO_DTL_IDS BULK COLLECT into L_price_event_ids_to_process;
   close C_SIMPLE_PROMO_DTL_IDS;

   if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(LP_cc_error_tbl,
                                         L_rib_trans_id,
                                         NULL, -- I_bulk_cc_pe_id
                                         L_price_event_ids_to_process,
                                         NULL, -- I_process_price_event_type
                                         RPM_CONSTANTS.SIMPLE_CODE,
                                         RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE) = 0 then
      if LP_cc_error_tbl is NOT NULL or
         LP_cc_error_tbl.COUNT > 0 then

         O_error_msg := 'Error in loading Simple Promotion Messages: '||LP_cc_error_tbl(1).error_string;

      end if;

      return FALSE;

   end if;

   open  C_COMPLEX_PROMO_DTL_IDS;
   fetch C_COMPLEX_PROMO_DTL_IDS BULK COLLECT into L_price_event_ids_to_process;
   close C_COMPLEX_PROMO_DTL_IDS;

   if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(LP_cc_error_tbl,
                                         L_rib_trans_id,
                                         NULL, -- I_bulk_cc_pe_id
                                         L_price_event_ids_to_process,
                                         NULL, -- I_process_price_event_type
                                         RPM_CONSTANTS.COMPLEX_CODE,
                                         RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE) = 0 then
      if LP_cc_error_tbl is NOT NULL or
         LP_cc_error_tbl.COUNT > 0 then

         O_error_msg := 'Error in loading complex promotion Messages: '||LP_cc_error_tbl(1).error_string;

      end if;

      return FALSE;

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END STAGE_MESSAGES;

--------------------------------------------------------------------------------

FUNCTION REPOPULATE_PAYLOAD_TABLES(O_error_msg       OUT VARCHAR2,
                                   I_location    IN      NUMBER,
                                   I_action_date IN      DATE,
                                   I_thread      IN      NUMBER,
                                   I_luw_cnt     IN      NUMBER)
RETURN NUMBER IS

   L_program  VARCHAR2(60) := 'RPM_REPOP_PAYLOAD_DATA.REPOPULATE_PAYLOAD_TABLES';

   cursor C_DEPTS_THREADS is
      select dept
        from (select rownum cnt,
                     dept
                from deps
               order by dept)
       where cnt >= ((I_thread - 1) * I_luw_cnt) + 1
         and cnt <  (I_thread * I_luw_cnt) + 1 ;

BEGIN

   LP_location := I_location;

   if I_action_date is NOT NULL then
      LP_action_date := I_action_date;
   else
      LP_action_date := LP_vdate + 1;
   end if;

   -- Get the subset of departments that need to be processed
   open C_DEPTS_THREADS;
   fetch C_DEPTS_THREADS BULK COLLECT into LP_depts;
   close C_DEPTS_THREADS;

   if POPULATE_GTT(O_error_msg) = FALSE then
      return 0;
   end if;

   if UPDATE_MSG_TYPE(O_error_msg) = FALSE then
      return 0;
   end if;

   if STAGE_MESSAGES(O_error_msg,
                     I_thread) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END REPOPULATE_PAYLOAD_TABLES;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_GET_THREADS(O_error_msg           OUT VARCHAR2,
                              O_threads             OUT NUMBER,
                              O_luw_cnt             OUT NUMBER,
                              I_location            IN  NUMBER,
                              I_action_date         IN  DATE)
RETURN NUMBER IS

   L_program  VARCHAR2(50)  := 'RPM_REPOP_PAYLOAD_DATA.VALIDATE_GET_THREADS';

   L_total_deps NUMBER       := 0;
   L_loc_check  NUMBER       := 0;
   L_luw_cnt    NUMBER       := 0;

   L_loc_type VARCHAR2(3);

   L_recognize_wh_as_locations RPM_SYSTEM_OPTIONS.RECOGNIZE_WH_AS_LOCATIONS%TYPE;

   cursor C_DEPTS is
      select COUNT(*)
        from deps;

   cursor C_CHECK_LOC is
      select 1,
             'S'
        from store
       where store = I_location
      union all
      select 1,
             'W'
        from wh
       where wh = I_location;

   cursor C_BATCH is
      select thread_luw_count
        from rpm_batch_control
       where program_name = 'com.retek.rpm.batch.refreshPos.RefreshPosDataBatch';

BEGIN

   -- Check if the location exists in RPM
   open  C_CHECK_LOC;
   fetch C_CHECK_LOC into L_loc_check,
                          L_loc_type;
   close C_CHECK_LOC;

   if L_loc_check = 0 then
      O_error_msg   := L_program||' - Location does not exist';
      return 0;
   elsif L_loc_check = 1 and
         L_loc_type = 'W' then
         ---
         if RPM_SYSTEM_OPTIONS_SQL.GET_RECOGNIZE_WH_AS_LOCATIONS(L_recognize_wh_as_locations,
                                                                 O_error_msg) = FALSE then
            O_error_msg   := L_program||' - get_recognize_wh_as_locations';
            return 0;
         end if;

         if L_recognize_wh_as_locations = 0 then
            O_error_msg   := L_program||' - Warehouse not recognized.';
            return 0;
         end if;
   end if;

   if I_action_date is NOT NULL then
      if I_action_date <= LP_vdate then
         O_error_msg   := L_program||' - Date should be greater than todays date';
         return 0;
      end if;
   end if;

   -- Total departments in the system will also be equal to the total threads for this batch.
   open C_DEPTS;
   fetch C_DEPTS into L_total_deps;
   close C_DEPTS;

   O_luw_cnt := L_total_deps; -- Total departments in the system = LUW

   -- RPM_BATCH_CONTROL holds the value for the LUW count.
   open  C_BATCH;
   fetch C_BATCH into L_luw_cnt;
   close C_BATCH;

   if NVL(L_luw_cnt, 0) = 0 then
      L_luw_cnt := LP_luw_count;
   end if;

   -- Approximate number of departments per threads
   O_threads := (case CEIL(L_total_deps / L_luw_cnt)
                    when 0 then
                       1
                    else CEIL(L_total_deps / L_luw_cnt)
                 end) ;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := Sql_Lib.CREATE_MSG( 'PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_GET_THREADS;

END RPM_REPOP_PAYLOAD_DATA;
/
