CREATE OR REPLACE PACKAGE RPM_EXE_CC_QUERY_RULES AS
--------------------------------------------------------
FUNCTION EXECUTE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
END;
/

