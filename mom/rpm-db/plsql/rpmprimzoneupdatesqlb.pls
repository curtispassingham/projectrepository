CREATE OR REPLACE PACKAGE BODY RPM_PRIMARY_ZONE_UPDATE_SQL AS

LP_price_event_id  NUMBER  := -987654321;

FUNCTION CLEAN_STAGED_DATA(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

-------------------------------------------------

FUNCTION EXPLODE_TIMELINES_TO_IL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg             OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION EXECUTE(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(40) := 'RPM_PRIMARY_ZONE_UPDATE_SQL.EXECUTE';

   L_pzm_rowids       OBJ_VARCHAR_ID_TABLE  := NULL;
   L_locked           VARCHAR2(200)         := NULL;
   L_items_to_process OBJ_VARCHAR_ID_TABLE  := NULL;

   cursor C_LOCK_DATA is
      select rowid
        from rpm_prim_zone_modifications
      for update nowait;

   cursor C_CC_LOCK_CHECK is
      -- Merch Hier level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm
       where (   (    rpcl.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                  and rpcl.dept       = pzm.dept
                  and rpcl.class      = NVL(pzm.class, rpcl.class)
                  and rpcl.subclass   = NVL(pzm.subclass, rpcl.subclass))
              or (    rpcl.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                  and rpcl.dept       = pzm.dept
                  and rpcl.class      = NVL(pzm.class, rpcl.class))
              or (    rpcl.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                  and rpcl.dept       = pzm.dept))
      union all
      -- Item and Parent Item level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm,
             item_master im
       where rpcl.merch_type IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                 RPM_CONSTANTS.PARENT_ITEM_DIFF)
         and rpcl.item       = im.item
         and im.dept         = pzm.dept
         and im.class        = NVL(pzm.class, im.class)
         and im.subclass     = NVL(pzm.subclass,im.subclass)
      union all
      -- Link Code level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm,
             rpm_link_code_attribute rlca,
             item_master im
       where rpcl.merch_type   = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rpcl.link_code_id = rlca.link_code
         and rlca.item         = im.item
         and im.dept           = pzm.dept
         and im.class          = NVL(pzm.class, im.class)
         and im.subclass       = NVL(pzm.subclass, im.subclass)
      union all
      -- Price Change skulist level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm,
             rpm_price_change_skulist rpcs,
             item_master im
       where rpcl.merch_type       = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpcl.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and rpcl.price_event_id   = rpcs.price_event_id
         and rpcl.skulist          = rpcs.skulist
         and rpcs.item             = im.item
         and im.dept               = pzm.dept
         and im.class              = NVL(pzm.class, im.class)
         and im.subclass           = NVL(pzm.subclass, im.subclass)
      union all
      -- Clearance skulist level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm,
             rpm_clearance_skulist rcs,
             item_master im
       where rpcl.merch_type       = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpcl.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                       RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET)
         and rpcl.price_event_id   = rcs.price_event_id
         and rpcl.skulist          = rcs.skulist
         and rcs.item              = im.item
         and im.dept               = pzm.dept
         and im.class              = NVL(pzm.class, im.class)
         and im.subclass           = NVL(pzm.subclass, im.subclass)
      union all
      -- Promotion skulist level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm,
             rpm_promo_dtl_skulist rpds,
             item_master im
       where rpcl.merch_type       = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpcl.price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                       RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                       RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
         and rpcl.price_event_id   = rpds.price_event_id
         and rpcl.skulist          = rpds.skulist
         and rpds.item             = im.item
         and im.dept               = pzm.dept
         and im.class              = NVL(pzm.class, im.class)
         and im.subclass           = NVL(pzm.subclass, im.subclass)
      union all
      -- Price Event ItemList (PEIL) level locks...
      select rpcl.price_event_id
        from rpm_pe_cc_lock rpcl,
             rpm_prim_zone_modifications pzm,
             rpm_merch_list_detail rmld,
             item_master im
       where rpcl.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpcl.price_event_itemlist = rmld.merch_list_id
         and (  (    rmld.item         = im.item_parent
                 and rmld.merch_level  = RPM_CONSTANTS.PARENT_MERCH_TYPE)
              or(    rmld.item         = im.item
                 and rmld.merch_level  = RPM_CONSTANTS.ITEM_MERCH_TYPE))
         and im.dept                   = pzm.dept
         and im.class                  = NVL(pzm.class, im.class)
         and im.subclass               = NVL(pzm.subclass, im.subclass);

BEGIN

   open C_LOCK_DATA;
   fetch C_LOCK_DATA BULK COLLECT into L_pzm_rowids;
   close C_LOCK_DATA;

   if CLEAN_STAGED_DATA(O_error_msg) = 0 then
      return 0;
   end if;

   open C_CC_LOCK_CHECK;
   fetch C_CC_LOCK_CHECK into L_locked;
   close C_CC_LOCK_CHECK;

   -- If there is overlap between locked data and PZG updates, stop all processing.
   if L_locked is NOT NULL then
      return 1;
   end if;

   if EXPLODE_TIMELINES_TO_IL(O_error_msg) = 0 then
      return 0;
   end if;

   delete from rpm_future_retail
    where future_retail_id IN (select future_retail_id
                                 from rpm_future_retail fr,
                                      rpm_me_item_gtt rmig
                                where fr.dept            = rmig.dept
                                  and fr.class           = rmig.class
                                  and fr.subclass        = rmig.subclass
                                  and fr.cur_hier_level != RPM_CONSTANTS.FR_HIER_ITEM_LOC);

   delete from rpm_promo_item_loc_expl
    where promo_item_loc_expl_id IN (select promo_item_loc_expl_id
                                       from rpm_promo_item_loc_expl fr,
                                            rpm_me_item_gtt rmig
                                      where fr.dept            = rmig.dept
                                        and fr.class           = rmig.class
                                        and fr.subclass        = rmig.subclass
                                        and fr.cur_hier_level != RPM_CONSTANTS.FR_HIER_ITEM_LOC);

   delete from rpm_cust_segment_promo_fr
    where cust_segment_promo_id IN (select cust_segment_promo_id
                                      from rpm_cust_segment_promo_fr fr,
                                           rpm_me_item_gtt rmig,
                                           item_master im
                                     where im.dept            = rmig.dept
                                       and im.class           = rmig.class
                                       and im.subclass        = rmig.subclass
                                       and im.item            = fr.item
                                       and im.dept            = fr.dept
                                       and fr.cur_hier_level != RPM_CONSTANTS.FR_HIER_ITEM_LOC);

   if RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ROLLUP_FR_FOR_PZG_UPD(O_error_msg) = 0 then
      return 0;
   end if;

   if RPM_ROLLUP.ROLL_FUTURE_RETAIL_FOR_PZ_UPD(O_error_msg) = 0 then
      return 0;
   end if;

   forall i IN 1..L_pzm_rowids.count
      delete
        from rpm_prim_zone_modifications
       where rowid = L_pzm_rowids(i);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END EXECUTE;

-------------------------------------------------

FUNCTION CLEAN_STAGED_DATA(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(50) := 'RPM_PRIMARY_ZONE_UPDATE_SQL.CLEAN_STAGED_DATA';

   cursor C_DUP_DATA is
      select distinct
             pzm.dept,
             pzm.class,
             pzm.subclass,
             MAX(pzm.prim_zone_mod_id) OVER (PARTITION BY pzm.dept,
                                                          pzm.class,
                                                          pzm.subclass) max_id
        from (select dept,
                     class,
                     subclass,
                     count(*)
                from rpm_prim_zone_modifications
              having COUNT(*) > 1
               group by dept,
                        class,
                        subclass) t,
             rpm_prim_zone_modifications pzm
       where pzm.dept                = t.dept
         and NVL(pzm.class, -999)    = NVL(NVL(t.class, pzm.class), -999)
         and NVL(pzm.subclass, -999) = NVL(NVL(t.subclass, pzm.subclass), -999);

BEGIN

   for rec in C_DUP_DATA loop

      delete
        from rpm_prim_zone_modifications pzm
       where pzm.dept                = rec.dept
         and NVL(pzm.class, -999)    = NVL(rec.class, -999)
         and NVL(pzm.subclass, -999) = NVL(rec.subclass, -999)
         and pzm.prim_zone_mod_id   != rec.max_id;

   end loop;

   -- Delete staged data for new records where there is already a higher level record with the same PZG
   delete from rpm_prim_zone_modifications
    where prim_zone_mod_id IN (select rpzm.prim_zone_mod_id
                                 from rpm_prim_zone_modifications rpzm
                                where EXISTS (select 'x'
                                                from rpm_merch_retail_def rmrd
                                               where rpzm.merch_type           is NOT NULL
                                                 and rmrd.merch_type           = 3
                                                 and rpzm.merch_type           IN (1, 2)
                                                 and rmrd.dept                 = rpzm.dept
                                                 and rmrd.regular_zone_group   = rpzm.new_pzg
                                                 and rmrd.merch_retail_def_id != rpzm.merch_retail_def_id
                                                 and rpzm.old_pzg              is NULL
                                              union all
                                              select 'x'
                                                from rpm_merch_retail_def rmrd
                                               where rpzm.merch_type           is NOT NULL
                                                 and rmrd.merch_type           = 2
                                                 and rpzm.merch_type           = 1
                                                 and rmrd.dept                 = rpzm.dept
                                                 and rmrd.class                = rpzm.class
                                                 and rmrd.regular_zone_group   = rpzm.new_pzg
                                                 and rmrd.merch_retail_def_id != rpzm.merch_retail_def_id
                                                 and rpzm.old_pzg              is NULL));

   -- Delete staged data for new records where there is no higher level record
   -- at all - means it is a completely new merch hier to the system

   delete from rpm_prim_zone_modifications
     where prim_zone_mod_id IN (select rpzm.prim_zone_mod_id
                                  from rpm_prim_zone_modifications rpzm
                                 where EXISTS (select 'x'
                                                 from rpm_merch_retail_def rmrd
                                                where rpzm.old_pzg              is NULL
                                                  and rmrd.dept                 = rpzm.dept
                                                  and NVL(rmrd.class, -999)     = NVL(NVL(rpzm.class, rmrd.class), -999)
                                                  and NVL(rmrd.subclass, -999)  = NVL(NVL(rpzm.subclass, rmrd.subclass), -999)
                                                  and rmrd.merch_retail_def_id != rpzm.merch_retail_def_id));

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CLEAN_STAGED_DATA;

-------------------------------------------------

FUNCTION EXPLODE_TIMELINES_TO_IL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_PRIMARY_ZONE_UPDATE_SQL.EXPLODE_TIMELINES_TO_IL';

BEGIN

   delete rpm_merch_node_zone_node_gtt;
   delete rpm_me_item_gtt;

   insert into rpm_me_item_gtt
      (dept,
       class,
       subclass)
   select distinct s.dept,
          s.class,
          s.subclass
     from rpm_prim_zone_modifications pzm,
          subclass s
    where s.dept         = pzm.dept
      and s.class        = NVL(pzm.class, s.class)
      and s.subclass     = NVL(pzm.subclass, s.subclass);

   insert into rpm_merch_node_zone_node_gtt
      (dept,
       class,
       subclass,
       location,
       zone_node_type,
       zone_id)
   select distinct
          t.dept,
          t.class,
          t.subclass,
          rzl.location,
          rzl.loc_type,
          rzl.zone_id
     from (select dept,
                  class,
                  subclass
             from rpm_me_item_gtt) t,
          rpm_prim_zone_modifications pzm,
          rpm_merch_retail_def_expl rmrd,
          rpm_zone rz,
          rpm_zone_location rzl
    where t.dept           = pzm.dept
      and t.class          = NVL(pzm.class, t.class)
      and t.subclass       = NVL(pzm.subclass, t.subclass)
      and rmrd.dept        = t.dept
      and rmrd.class       = t.class
      and rmrd.subclass    = t.subclass
      and rz.zone_group_id = rmrd.regular_zone_group
      and rzl.zone_id      = rz.zone_id;

   if POPULATE_GTT(O_error_msg) = 0 then
      return 0;
   end if;

   merge into rpm_future_retail_gtt target
   using (select t2.price_event_id,
                 t2.dept,
                 t2.class,
                 t2.subclass,
                 t2.to_item,
                 t2.to_diff_id,
                 t2.to_item_parent,
                 t2.to_zone_node_type,
                 t2.to_location,
                 t2.action_date,
                 t2.selling_retail,
                 t2.selling_retail_currency,
                 t2.selling_uom,
                 t2.multi_units,
                 t2.multi_unit_retail,
                 t2.multi_unit_retail_currency,
                 t2.multi_selling_uom,
                 t2.clear_retail,
                 t2.clear_retail_currency,
                 t2.clear_uom,
                 t2.simple_promo_retail,
                 t2.simple_promo_retail_currency,
                 t2.simple_promo_uom,
                 t2.price_change_id,
                 t2.price_change_display_id,
                 t2.pc_exception_parent_id,
                 t2.pc_change_type,
                 t2.pc_change_amount,
                 t2.pc_change_currency,
                 t2.pc_change_percent,
                 t2.pc_change_selling_uom,
                 t2.pc_null_multi_ind,
                 t2.pc_multi_units,
                 t2.pc_multi_unit_retail,
                 t2.pc_multi_unit_retail_currency,
                 t2.pc_multi_selling_uom,
                 t2.pc_price_guide_id,
                 t2.clearance_id,
                 t2.clearance_display_id,
                 t2.clear_mkdn_index,
                 t2.clear_start_ind,
                 t2.clear_change_type,
                 t2.clear_change_amount,
                 t2.clear_change_currency,
                 t2.clear_change_percent,
                 t2.clear_change_selling_uom,
                 t2.clear_price_guide_id,
                 t2.loc_move_from_zone_id,
                 t2.loc_move_to_zone_id,
                 t2.location_move_id,
                 t2.lock_version,
                 t2.cur_hier_level,
                 t2.rfr_rowid,
                 t2.timeline_seq,
                 t2.on_simple_promo_ind,
                 t2.on_complex_promo_ind,
                 s.zone_id to_zone_id
            from (select t1.price_event_id,
                         t1.dept,
                         t1.class,
                         t1.subclass,
                         t1.to_item,
                         t1.to_diff_id,
                         t1.to_item_parent,
                         t1.to_zone_node_type,
                         t1.to_location,
                         t1.action_date,
                         t1.selling_retail,
                         t1.selling_retail_currency,
                         t1.selling_uom,
                         t1.multi_units,
                         t1.multi_unit_retail,
                         t1.multi_unit_retail_currency,
                         t1.multi_selling_uom,
                         t1.clear_retail,
                         t1.clear_retail_currency,
                         t1.clear_uom,
                         t1.simple_promo_retail,
                         t1.simple_promo_retail_currency,
                         t1.simple_promo_uom,
                         t1.price_change_id,
                         t1.price_change_display_id,
                         t1.pc_exception_parent_id,
                         t1.pc_change_type,
                         t1.pc_change_amount,
                         t1.pc_change_currency,
                         t1.pc_change_percent,
                         t1.pc_change_selling_uom,
                         t1.pc_null_multi_ind,
                         t1.pc_multi_units,
                         t1.pc_multi_unit_retail,
                         t1.pc_multi_unit_retail_currency,
                         t1.pc_multi_selling_uom,
                         t1.pc_price_guide_id,
                         t1.clearance_id,
                         t1.clearance_display_id,
                         t1.clear_mkdn_index,
                         t1.clear_start_ind,
                         t1.clear_change_type,
                         t1.clear_change_amount,
                         t1.clear_change_currency,
                         t1.clear_change_percent,
                         t1.clear_change_selling_uom,
                         t1.clear_price_guide_id,
                         t1.loc_move_from_zone_id,
                         t1.loc_move_to_zone_id,
                         t1.location_move_id,
                         t1.lock_version,
                         t1.cur_hier_level,
                         t1.rfr_rowid,
                         t1.timeline_seq,
                         t1.on_simple_promo_ind,
                         t1.on_complex_promo_ind,
                         RANK() OVER (PARTITION BY t1.to_item,
                                                   t1.to_location,
                                                   t1.to_zone_node_type
                                          ORDER BY t1.fr_rank desc) rank
                    from (-- explode PZ
                          select fr.price_event_id,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.action_date,
                                 fr.selling_retail,
                                 fr.selling_retail_currency,
                                 fr.selling_uom,
                                 fr.multi_units,
                                 fr.multi_unit_retail,
                                 fr.multi_unit_retail_currency,
                                 fr.multi_selling_uom,
                                 fr.clear_retail,
                                 fr.clear_retail_currency,
                                 fr.clear_uom,
                                 fr.simple_promo_retail,
                                 fr.simple_promo_retail_currency,
                                 fr.simple_promo_uom,
                                 fr.price_change_id,
                                 fr.price_change_display_id,
                                 fr.pc_exception_parent_id,
                                 fr.pc_change_type,
                                 fr.pc_change_amount,
                                 fr.pc_change_currency,
                                 fr.pc_change_percent,
                                 fr.pc_change_selling_uom,
                                 fr.pc_null_multi_ind,
                                 fr.pc_multi_units,
                                 fr.pc_multi_unit_retail,
                                 fr.pc_multi_unit_retail_currency,
                                 fr.pc_multi_selling_uom,
                                 fr.pc_price_guide_id,
                                 fr.clearance_id,
                                 fr.clearance_display_id,
                                 fr.clear_mkdn_index,
                                 fr.clear_start_ind,
                                 fr.clear_change_type,
                                 fr.clear_change_amount,
                                 fr.clear_change_currency,
                                 fr.clear_change_percent,
                                 fr.clear_change_selling_uom,
                                 fr.clear_price_guide_id,
                                 fr.loc_move_from_zone_id,
                                 fr.loc_move_to_zone_id,
                                 fr.location_move_id,
                                 fr.lock_version,
                                 fr.cur_hier_level,
                                 fr.rfr_rowid,
                                 fr.timeline_seq,
                                 fr.on_simple_promo_ind,
                                 fr.on_complex_promo_ind,
                                 0 fr_rank,
                                 im.item        to_item,
                                 im.diff_1      to_diff_id,
                                 im.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_future_retail_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                             and im.item_parent    = fr.item
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and rzl.zone_id       = fr.location
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = rzl.location
                          union all
                          -- explode PL
                          select fr.price_event_id,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.action_date,
                                 fr.selling_retail,
                                 fr.selling_retail_currency,
                                 fr.selling_uom,
                                 fr.multi_units,
                                 fr.multi_unit_retail,
                                 fr.multi_unit_retail_currency,
                                 fr.multi_selling_uom,
                                 fr.clear_retail,
                                 fr.clear_retail_currency,
                                 fr.clear_uom,
                                 fr.simple_promo_retail,
                                 fr.simple_promo_retail_currency,
                                 fr.simple_promo_uom,
                                 fr.price_change_id,
                                 fr.price_change_display_id,
                                 fr.pc_exception_parent_id,
                                 fr.pc_change_type,
                                 fr.pc_change_amount,
                                 fr.pc_change_currency,
                                 fr.pc_change_percent,
                                 fr.pc_change_selling_uom,
                                 fr.pc_null_multi_ind,
                                 fr.pc_multi_units,
                                 fr.pc_multi_unit_retail,
                                 fr.pc_multi_unit_retail_currency,
                                 fr.pc_multi_selling_uom,
                                 fr.pc_price_guide_id,
                                 fr.clearance_id,
                                 fr.clearance_display_id,
                                 fr.clear_mkdn_index,
                                 fr.clear_start_ind,
                                 fr.clear_change_type,
                                 fr.clear_change_amount,
                                 fr.clear_change_currency,
                                 fr.clear_change_percent,
                                 fr.clear_change_selling_uom,
                                 fr.clear_price_guide_id,
                                 fr.loc_move_from_zone_id,
                                 fr.loc_move_to_zone_id,
                                 fr.location_move_id,
                                 fr.lock_version,
                                 fr.cur_hier_level,
                                 fr.rfr_rowid,
                                 fr.timeline_seq,
                                 fr.on_simple_promo_ind,
                                 fr.on_complex_promo_ind,
                                 1 fr_rank,
                                 im.item            to_item,
                                 im.diff_1          to_diff_id,
                                 im.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_future_retail_gtt fr,
                                 item_master im,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                             and im.item_parent    = fr.item
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = fr.location
                          union all
                          -- explode DZ
                          select fr.price_event_id,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.action_date,
                                 fr.selling_retail,
                                 fr.selling_retail_currency,
                                 fr.selling_uom,
                                 fr.multi_units,
                                 fr.multi_unit_retail,
                                 fr.multi_unit_retail_currency,
                                 fr.multi_selling_uom,
                                 fr.clear_retail,
                                 fr.clear_retail_currency,
                                 fr.clear_uom,
                                 fr.simple_promo_retail,
                                 fr.simple_promo_retail_currency,
                                 fr.simple_promo_uom,
                                 fr.price_change_id,
                                 fr.price_change_display_id,
                                 fr.pc_exception_parent_id,
                                 fr.pc_change_type,
                                 fr.pc_change_amount,
                                 fr.pc_change_currency,
                                 fr.pc_change_percent,
                                 fr.pc_change_selling_uom,
                                 fr.pc_null_multi_ind,
                                 fr.pc_multi_units,
                                 fr.pc_multi_unit_retail,
                                 fr.pc_multi_unit_retail_currency,
                                 fr.pc_multi_selling_uom,
                                 fr.pc_price_guide_id,
                                 fr.clearance_id,
                                 fr.clearance_display_id,
                                 fr.clear_mkdn_index,
                                 fr.clear_start_ind,
                                 fr.clear_change_type,
                                 fr.clear_change_amount,
                                 fr.clear_change_currency,
                                 fr.clear_change_percent,
                                 fr.clear_change_selling_uom,
                                 fr.clear_price_guide_id,
                                 fr.loc_move_from_zone_id,
                                 fr.loc_move_to_zone_id,
                                 fr.location_move_id,
                                 fr.lock_version,
                                 fr.cur_hier_level,
                                 fr.rfr_rowid,
                                 fr.timeline_seq,
                                 fr.on_simple_promo_ind,
                                 fr.on_complex_promo_ind,
                                 1 fr_rank,
                                 im.item        to_item,
                                 im.diff_1      to_diff_id,
                                 im.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_future_retail_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                             and im.item_parent    = fr.item
                             and im.diff_1         = fr.diff_id
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and rzl.zone_id       = fr.location
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = rzl.location
                          union all
                          -- explode DL
                          select fr.price_event_id,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.action_date,
                                 fr.selling_retail,
                                 fr.selling_retail_currency,
                                 fr.selling_uom,
                                 fr.multi_units,
                                 fr.multi_unit_retail,
                                 fr.multi_unit_retail_currency,
                                 fr.multi_selling_uom,
                                 fr.clear_retail,
                                 fr.clear_retail_currency,
                                 fr.clear_uom,
                                 fr.simple_promo_retail,
                                 fr.simple_promo_retail_currency,
                                 fr.simple_promo_uom,
                                 fr.price_change_id,
                                 fr.price_change_display_id,
                                 fr.pc_exception_parent_id,
                                 fr.pc_change_type,
                                 fr.pc_change_amount,
                                 fr.pc_change_currency,
                                 fr.pc_change_percent,
                                 fr.pc_change_selling_uom,
                                 fr.pc_null_multi_ind,
                                 fr.pc_multi_units,
                                 fr.pc_multi_unit_retail,
                                 fr.pc_multi_unit_retail_currency,
                                 fr.pc_multi_selling_uom,
                                 fr.pc_price_guide_id,
                                 fr.clearance_id,
                                 fr.clearance_display_id,
                                 fr.clear_mkdn_index,
                                 fr.clear_start_ind,
                                 fr.clear_change_type,
                                 fr.clear_change_amount,
                                 fr.clear_change_currency,
                                 fr.clear_change_percent,
                                 fr.clear_change_selling_uom,
                                 fr.clear_price_guide_id,
                                 fr.loc_move_from_zone_id,
                                 fr.loc_move_to_zone_id,
                                 fr.location_move_id,
                                 fr.lock_version,
                                 fr.cur_hier_level,
                                 fr.rfr_rowid,
                                 fr.timeline_seq,
                                 fr.on_simple_promo_ind,
                                 fr.on_complex_promo_ind,
                                 2 fr_rank,
                                 im.item            to_item,
                                 im.diff_1          to_diff_id,
                                 im.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_future_retail_gtt fr,
                                 item_master im,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                             and im.item_parent    = fr.item
                             and im.diff_1         = fr.diff_id
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = fr.location
                          union all
                          -- explode IZ
                          select fr.price_event_id,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.action_date,
                                 fr.selling_retail,
                                 fr.selling_retail_currency,
                                 fr.selling_uom,
                                 fr.multi_units,
                                 fr.multi_unit_retail,
                                 fr.multi_unit_retail_currency,
                                 fr.multi_selling_uom,
                                 fr.clear_retail,
                                 fr.clear_retail_currency,
                                 fr.clear_uom,
                                 fr.simple_promo_retail,
                                 fr.simple_promo_retail_currency,
                                 fr.simple_promo_uom,
                                 fr.price_change_id,
                                 fr.price_change_display_id,
                                 fr.pc_exception_parent_id,
                                 fr.pc_change_type,
                                 fr.pc_change_amount,
                                 fr.pc_change_currency,
                                 fr.pc_change_percent,
                                 fr.pc_change_selling_uom,
                                 fr.pc_null_multi_ind,
                                 fr.pc_multi_units,
                                 fr.pc_multi_unit_retail,
                                 fr.pc_multi_unit_retail_currency,
                                 fr.pc_multi_selling_uom,
                                 fr.pc_price_guide_id,
                                 fr.clearance_id,
                                 fr.clearance_display_id,
                                 fr.clear_mkdn_index,
                                 fr.clear_start_ind,
                                 fr.clear_change_type,
                                 fr.clear_change_amount,
                                 fr.clear_change_currency,
                                 fr.clear_change_percent,
                                 fr.clear_change_selling_uom,
                                 fr.clear_price_guide_id,
                                 fr.loc_move_from_zone_id,
                                 fr.loc_move_to_zone_id,
                                 fr.location_move_id,
                                 fr.lock_version,
                                 fr.cur_hier_level,
                                 fr.rfr_rowid,
                                 fr.timeline_seq,
                                 fr.on_simple_promo_ind,
                                 fr.on_complex_promo_ind,
                                 2 fr_rank,
                                 fr.item        to_item,
                                 fr.diff_id     to_diff_id,
                                 fr.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_future_retail_gtt fr,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                             and rzl.zone_id       = fr.location
                             and ril.dept          = fr.dept
                             and ril.item          = fr.item
                             and ril.loc           = rzl.location
                          -- explode IL
                          union all
                          select fr.price_event_id,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.action_date,
                                 fr.selling_retail,
                                 fr.selling_retail_currency,
                                 fr.selling_uom,
                                 fr.multi_units,
                                 fr.multi_unit_retail,
                                 fr.multi_unit_retail_currency,
                                 fr.multi_selling_uom,
                                 fr.clear_retail,
                                 fr.clear_retail_currency,
                                 fr.clear_uom,
                                 fr.simple_promo_retail,
                                 fr.simple_promo_retail_currency,
                                 fr.simple_promo_uom,
                                 fr.price_change_id,
                                 fr.price_change_display_id,
                                 fr.pc_exception_parent_id,
                                 fr.pc_change_type,
                                 fr.pc_change_amount,
                                 fr.pc_change_currency,
                                 fr.pc_change_percent,
                                 fr.pc_change_selling_uom,
                                 fr.pc_null_multi_ind,
                                 fr.pc_multi_units,
                                 fr.pc_multi_unit_retail,
                                 fr.pc_multi_unit_retail_currency,
                                 fr.pc_multi_selling_uom,
                                 fr.pc_price_guide_id,
                                 fr.clearance_id,
                                 fr.clearance_display_id,
                                 fr.clear_mkdn_index,
                                 fr.clear_start_ind,
                                 fr.clear_change_type,
                                 fr.clear_change_amount,
                                 fr.clear_change_currency,
                                 fr.clear_change_percent,
                                 fr.clear_change_selling_uom,
                                 fr.clear_price_guide_id,
                                 fr.loc_move_from_zone_id,
                                 fr.loc_move_to_zone_id,
                                 fr.location_move_id,
                                 fr.lock_version,
                                 fr.cur_hier_level,
                                 fr.rfr_rowid,
                                 fr.timeline_seq,
                                 fr.on_simple_promo_ind,
                                 fr.on_complex_promo_ind,
                                 3 fr_rank,
                                 fr.item            to_item,
                                 fr.diff_id         to_diff_id,
                                 fr.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_future_retail_gtt fr
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC) t1) t2,
                 rpm_merch_node_zone_node_gtt s
           where t2.rank              = 1
             and t2.dept              = s.dept (+)
             and t2.class             = s.class (+)
             and t2.subclass          = s.subclass (+)
             and t2.to_location       = s.location (+)
             and t2.to_zone_node_type = s.zone_node_type (+)) source
   on (    target.item                 = source.to_item
       and NVL(target.diff_id, -99999) = NVL(source.to_diff_id, -99999)
       and target.location             = source.to_location
       and target.zone_node_type       = source.to_zone_node_type
       and target.action_date          = source.action_date)
   when MATCHED then
      update 
         set target.zone_id        = source.to_zone_id,
             target.max_hier_level = case
                                        when source.to_item_parent is NOT NULL and
                                             source.to_zone_id is NOT NULL then
                                           RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                        when source.to_item_parent is NOT NULL and
                                             source.to_zone_id is NULL then
                                           RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                        when source.to_item_parent is NULL and
                                             source.to_zone_id is NOT NULL then
                                           RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                        else
                                           RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                     end
   when NOT MATCHED then
      insert
         (price_event_id,
          future_retail_id,
          dept,
          class,
          subclass,
          item,
          diff_id,
          item_parent,
          zone_node_type,
          location,
          zone_id,
          action_date,
          selling_retail,
          selling_retail_currency,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_unit_retail_currency,
          multi_selling_uom,
          clear_retail,
          clear_retail_currency,
          clear_uom,
          simple_promo_retail,
          simple_promo_retail_currency,
          simple_promo_uom,
          price_change_id,
          price_change_display_id,
          pc_exception_parent_id,
          pc_change_type,
          pc_change_amount,
          pc_change_currency,
          pc_change_percent,
          pc_change_selling_uom,
          pc_null_multi_ind,
          pc_multi_units,
          pc_multi_unit_retail,
          pc_multi_unit_retail_currency,
          pc_multi_selling_uom,
          pc_price_guide_id,
          clearance_id,
          clearance_display_id,
          clear_mkdn_index,
          clear_start_ind,
          clear_change_type,
          clear_change_amount,
          clear_change_currency,
          clear_change_percent,
          clear_change_selling_uom,
          clear_price_guide_id,
          loc_move_from_zone_id,
          loc_move_to_zone_id,
          location_move_id,
          lock_version,
          rfr_rowid,
          timeline_seq,
          on_simple_promo_ind,
          on_complex_promo_ind,
          max_hier_level,
          cur_hier_level)
      values
         (source.price_event_id,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          source.dept,
          source.class,
          source.subclass,
          source.to_item,
          source.to_diff_id,
          source.to_item_parent,
          source.to_zone_node_type,
          source.to_location,
          source.to_zone_id,
          source.action_date,
          source.selling_retail,
          source.selling_retail_currency,
          source.selling_uom,
          source.multi_units,
          source.multi_unit_retail,
          source.multi_unit_retail_currency,
          source.multi_selling_uom,
          source.clear_retail,
          source.clear_retail_currency,
          source.clear_uom,
          source.simple_promo_retail,
          source.simple_promo_retail_currency,
          source.simple_promo_uom,
          source.price_change_id,
          source.price_change_display_id,
          source.pc_exception_parent_id,
          source.pc_change_type,
          source.pc_change_amount,
          source.pc_change_currency,
          source.pc_change_percent,
          source.pc_change_selling_uom,
          source.pc_null_multi_ind,
          source.pc_multi_units,
          source.pc_multi_unit_retail,
          source.pc_multi_unit_retail_currency,
          source.pc_multi_selling_uom,
          source.pc_price_guide_id,
          source.clearance_id,
          source.clearance_display_id,
          source.clear_mkdn_index,
          source.clear_start_ind,
          source.clear_change_type,
          source.clear_change_amount,
          source.clear_change_currency,
          source.clear_change_percent,
          source.clear_change_selling_uom,
          source.clear_price_guide_id,
          source.loc_move_from_zone_id,
          source.loc_move_to_zone_id,
          source.location_move_id,
          source.lock_version,
          case
             when source.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC then
                source.rfr_rowid
             else
                NULL
          end,
          source.timeline_seq,
          source.on_simple_promo_ind,
          source.on_complex_promo_ind,
          case
             when source.to_item_parent is NOT NULL and
                  source.to_zone_id is NOT NULL then
                RPM_CONSTANTS.FR_HIER_PARENT_ZONE
             when source.to_item_parent is NOT NULL and
                  source.to_zone_id is NULL then
                RPM_CONSTANTS.FR_HIER_PARENT_LOC
             when source.to_item_parent is NULL and
                  source.to_zone_id is NOT NULL then
                RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             else
                RPM_CONSTANTS.FR_HIER_ITEM_LOC
          end,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC);

   delete from rpm_future_retail_gtt
    where cur_hier_level != RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   merge into rpm_promo_item_loc_expl_gtt target
   using (select t2.price_event_id,
                 t2.promo_item_loc_expl_id,
                 t2.item,
                 t2.dept,
                 t2.class,
                 t2.subclass,
                 t2.location,
                 t2.promo_id,
                 t2.promo_display_id,
                 t2.promo_secondary_ind,
                 t2.promo_comp_id,
                 t2.comp_display_id,
                 t2.promo_dtl_id,
                 t2.type,
                 t2.customer_type,
                 t2.detail_secondary_ind,
                 t2.detail_start_date,
                 t2.detail_end_date,
                 t2.detail_apply_to_code,
                 t2.detail_change_type,
                 t2.detail_change_amount,
                 t2.detail_change_currency,
                 t2.detail_change_percent,
                 t2.detail_change_selling_uom,
                 t2.detail_price_guide_id,
                 t2.exception_parent_id,
                 t2.promo_comp_msg_type,
                 t2.rpile_rowid,
                 t2.zone_node_type,
                 t2.item_parent,
                 t2.diff_id,
                 t2.zone_id,
                 t2.max_hier_level,
                 t2.cur_hier_level,
                 t2.fr_rank,
                 t2.to_item,
                 t2.to_diff_id,
                 t2.to_item_parent,
                 t2.to_location,
                 t2.to_zone_node_type,
                 t2.rank,
                 s.zone_id to_zone_id
            from (select t1.price_event_id,
                         t1.promo_item_loc_expl_id,
                         t1.item,
                         t1.dept,
                         t1.class,
                         t1.subclass,
                         t1.location,
                         t1.promo_id,
                         t1.promo_display_id,
                         t1.promo_secondary_ind,
                         t1.promo_comp_id,
                         t1.comp_display_id,
                         t1.promo_dtl_id,
                         t1.type,
                         t1.customer_type,
                         t1.detail_secondary_ind,
                         t1.detail_start_date,
                         t1.detail_end_date,
                         t1.detail_apply_to_code,
                         t1.detail_change_type,
                         t1.detail_change_amount,
                         t1.detail_change_currency,
                         t1.detail_change_percent,
                         t1.detail_change_selling_uom,
                         t1.detail_price_guide_id,
                         t1.exception_parent_id,
                         t1.promo_comp_msg_type,
                         t1.rpile_rowid,
                         t1.zone_node_type,
                         t1.item_parent,
                         t1.diff_id,
                         t1.zone_id,
                         t1.max_hier_level,
                         t1.cur_hier_level,
                         t1.fr_rank,
                         t1.to_item,
                         t1.to_diff_id,
                         t1.to_item_parent,
                         t1.to_location,
                         t1.to_zone_node_type,
                         RANK() OVER (PARTITION by t1.to_item,
                                                   t1.to_location,
                                                   t1.to_zone_node_type
                                          ORDER BY t1.fr_rank desc) rank
                    from (-- explode PZ
                          select fr.price_event_id,
                                 fr.promo_item_loc_expl_id,
                                 fr.item,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.location,
                                 fr.promo_id,
                                 fr.promo_display_id,
                                 fr.promo_secondary_ind,
                                 fr.promo_comp_id,
                                 fr.comp_display_id,
                                 fr.promo_dtl_id,
                                 fr.type,
                                 fr.customer_type,
                                 fr.detail_secondary_ind,
                                 fr.detail_start_date,
                                 fr.detail_end_date,
                                 fr.detail_apply_to_code,
                                 fr.detail_change_type,
                                 fr.detail_change_amount,
                                 fr.detail_change_currency,
                                 fr.detail_change_percent,
                                 fr.detail_change_selling_uom,
                                 fr.detail_price_guide_id,
                                 fr.exception_parent_id,
                                 fr.promo_comp_msg_type,
                                 fr.rpile_rowid,
                                 fr.zone_node_type,
                                 fr.item_parent,
                                 fr.diff_id,
                                 fr.zone_id,
                                 fr.max_hier_level,
                                 fr.cur_hier_level,
                                 0 fr_rank,
                                 im.item        to_item,
                                 im.diff_1      to_diff_id,
                                 im.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_promo_item_loc_expl_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                             and im.item_parent    = fr.item
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and rzl.zone_id       = fr.location
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = rzl.location
                           union all
                          -- explode PL
                          select fr.price_event_id,
                                 fr.promo_item_loc_expl_id,
                                 fr.item,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.location,
                                 fr.promo_id,
                                 fr.promo_display_id,
                                 fr.promo_secondary_ind,
                                 fr.promo_comp_id,
                                 fr.comp_display_id,
                                 fr.promo_dtl_id,
                                 fr.type,
                                 fr.customer_type,
                                 fr.detail_secondary_ind,
                                 fr.detail_start_date,
                                 fr.detail_end_date,
                                 fr.detail_apply_to_code,
                                 fr.detail_change_type,
                                 fr.detail_change_amount,
                                 fr.detail_change_currency,
                                 fr.detail_change_percent,
                                 fr.detail_change_selling_uom,
                                 fr.detail_price_guide_id,
                                 fr.exception_parent_id,
                                 fr.promo_comp_msg_type,
                                 fr.rpile_rowid,
                                 fr.zone_node_type,
                                 fr.item_parent,
                                 fr.diff_id,
                                 fr.zone_id,
                                 fr.max_hier_level,
                                 fr.cur_hier_level,
                                 1 fr_rank,
                                 im.item            to_item,
                                 im.diff_1          to_diff_id,
                                 im.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_promo_item_loc_expl_gtt fr,
                                 item_master im,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                             and im.item_parent    = fr.item
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = fr.location
                           union all
                          -- explode DZ
                          select fr.price_event_id,
                                 fr.promo_item_loc_expl_id,
                                 fr.item,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.location,
                                 fr.promo_id,
                                 fr.promo_display_id,
                                 fr.promo_secondary_ind,
                                 fr.promo_comp_id,
                                 fr.comp_display_id,
                                 fr.promo_dtl_id,
                                 fr.type,
                                 fr.customer_type,
                                 fr.detail_secondary_ind,
                                 fr.detail_start_date,
                                 fr.detail_end_date,
                                 fr.detail_apply_to_code,
                                 fr.detail_change_type,
                                 fr.detail_change_amount,
                                 fr.detail_change_currency,
                                 fr.detail_change_percent,
                                 fr.detail_change_selling_uom,
                                 fr.detail_price_guide_id,
                                 fr.exception_parent_id,
                                 fr.promo_comp_msg_type,
                                 fr.rpile_rowid,
                                 fr.zone_node_type,
                                 fr.item_parent,
                                 fr.diff_id,
                                 fr.zone_id,
                                 fr.max_hier_level,
                                 fr.cur_hier_level,
                                 1 fr_rank,
                                 im.item        to_item,
                                 im.diff_1      to_diff_id,
                                 im.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_promo_item_loc_expl_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                             and im.item_parent    = fr.item
                             and im.diff_1         = fr.diff_id
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and rzl.zone_id       = fr.location
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = rzl.location
                           union all
                          -- explode DL
                          select fr.price_event_id,
                                 fr.promo_item_loc_expl_id,
                                 fr.item,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.location,
                                 fr.promo_id,
                                 fr.promo_display_id,
                                 fr.promo_secondary_ind,
                                 fr.promo_comp_id,
                                 fr.comp_display_id,
                                 fr.promo_dtl_id,
                                 fr.type,
                                 fr.customer_type,
                                 fr.detail_secondary_ind,
                                 fr.detail_start_date,
                                 fr.detail_end_date,
                                 fr.detail_apply_to_code,
                                 fr.detail_change_type,
                                 fr.detail_change_amount,
                                 fr.detail_change_currency,
                                 fr.detail_change_percent,
                                 fr.detail_change_selling_uom,
                                 fr.detail_price_guide_id,
                                 fr.exception_parent_id,
                                 fr.promo_comp_msg_type,
                                 fr.rpile_rowid,
                                 fr.zone_node_type,
                                 fr.item_parent,
                                 fr.diff_id,
                                 fr.zone_id,
                                 fr.max_hier_level,
                                 fr.cur_hier_level,
                                 2 fr_rank,
                                 im.item            to_item,
                                 im.diff_1          to_diff_id,
                                 im.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_promo_item_loc_expl_gtt fr,
                                 item_master im,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                             and im.item_parent    = fr.item
                             and im.diff_1         = fr.diff_id
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = fr.location
                           union all
                          -- explode IZ
                          select fr.price_event_id,
                                 fr.promo_item_loc_expl_id,
                                 fr.item,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.location,
                                 fr.promo_id,
                                 fr.promo_display_id,
                                 fr.promo_secondary_ind,
                                 fr.promo_comp_id,
                                 fr.comp_display_id,
                                 fr.promo_dtl_id,
                                 fr.type,
                                 fr.customer_type,
                                 fr.detail_secondary_ind,
                                 fr.detail_start_date,
                                 fr.detail_end_date,
                                 fr.detail_apply_to_code,
                                 fr.detail_change_type,
                                 fr.detail_change_amount,
                                 fr.detail_change_currency,
                                 fr.detail_change_percent,
                                 fr.detail_change_selling_uom,
                                 fr.detail_price_guide_id,
                                 fr.exception_parent_id,
                                 fr.promo_comp_msg_type,
                                 fr.rpile_rowid,
                                 fr.zone_node_type,
                                 fr.item_parent,
                                 fr.diff_id,
                                 fr.zone_id,
                                 fr.max_hier_level,
                                 fr.cur_hier_level,
                                 2 fr_rank,
                                 fr.item        to_item,
                                 fr.diff_id     to_diff_id,
                                 fr.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_promo_item_loc_expl_gtt fr,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                             and rzl.zone_id       = fr.location
                             and ril.dept          = fr.dept
                             and ril.item          = fr.item
                             and ril.loc           = rzl.location
                          -- explode IL
                           union all
                          select fr.price_event_id,
                                 fr.promo_item_loc_expl_id,
                                 fr.item,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 fr.location,
                                 fr.promo_id,
                                 fr.promo_display_id,
                                 fr.promo_secondary_ind,
                                 fr.promo_comp_id,
                                 fr.comp_display_id,
                                 fr.promo_dtl_id,
                                 fr.type,
                                 fr.customer_type,
                                 fr.detail_secondary_ind,
                                 fr.detail_start_date,
                                 fr.detail_end_date,
                                 fr.detail_apply_to_code,
                                 fr.detail_change_type,
                                 fr.detail_change_amount,
                                 fr.detail_change_currency,
                                 fr.detail_change_percent,
                                 fr.detail_change_selling_uom,
                                 fr.detail_price_guide_id,
                                 fr.exception_parent_id,
                                 fr.promo_comp_msg_type,
                                 fr.rpile_rowid,
                                 fr.zone_node_type,
                                 fr.item_parent,
                                 fr.diff_id,
                                 fr.zone_id,
                                 fr.max_hier_level,
                                 fr.cur_hier_level,
                                 3 fr_rank,
                                 fr.item            to_item,
                                 fr.diff_id         to_diff_id,
                                 fr.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_promo_item_loc_expl_gtt fr
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC) t1) t2,
                 rpm_merch_node_zone_node_gtt s
           where t2.rank              = 1
             and t2.dept              = s.dept (+)
             and t2.class             = s.class (+)
             and t2.subclass          = s.subclass (+)
             and t2.to_location       = s.location (+)
             and t2.to_zone_node_type = s.zone_node_type (+)) source
   on (    target.item                 = source.to_item
       and NVL(target.diff_id, -99999) = NVL(source.to_diff_id, -99999)
       and target.location             = source.to_location
       and target.zone_node_type       = source.to_zone_node_type
       and target.promo_dtl_id         = source.promo_dtl_id)
   when MATCHED then
      update
         set target.zone_id        = source.to_zone_id,
             target.max_hier_level = case
                                        when source.to_item_parent is NOT NULL and
                                             source.to_zone_id is NOT NULL then
                                           RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                        when source.to_item_parent is NOT NULL and
                                             source.to_zone_id is NULL then
                                           RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                        when source.to_item_parent is NULL and
                                             source.to_zone_id is NOT NULL then
                                           RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                        else
                                           RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                     end
   when NOT MATCHED then
      insert
         (price_event_id,
          promo_item_loc_expl_id,
          item,
          dept,
          class,
          subclass,
          location,
          promo_id,
          promo_display_id,
          promo_secondary_ind,
          promo_comp_id,
          comp_display_id,
          promo_dtl_id,
          type,
          customer_type,
          detail_secondary_ind,
          detail_start_date,
          detail_end_date,
          detail_apply_to_code,
          detail_change_type,
          detail_change_amount,
          detail_change_currency,
          detail_change_percent,
          detail_change_selling_uom,
          detail_price_guide_id,
          exception_parent_id,
          promo_comp_msg_type,
          rpile_rowid,
          zone_node_type,
          item_parent,
          diff_id,
          zone_id,
          max_hier_level,
          cur_hier_level)
      values
         (source.price_event_id,
          RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
          source.to_item,
          source.dept,
          source.class,
          source.subclass,
          source.to_location,
          source.promo_id,
          source.promo_display_id,
          source.promo_secondary_ind,
          source.promo_comp_id,
          source.comp_display_id,
          source.promo_dtl_id,
          source.type,
          source.customer_type,
          source.detail_secondary_ind,
          source.detail_start_date,
          source.detail_end_date,
          source.detail_apply_to_code,
          source.detail_change_type,
          source.detail_change_amount,
          source.detail_change_currency,
          source.detail_change_percent,
          source.detail_change_selling_uom,
          source.detail_price_guide_id,
          source.exception_parent_id,
          source.promo_comp_msg_type,
          case
             when source.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC then
                source.rpile_rowid
             else
                NULL
          end,
          source.to_zone_node_type,
          source.to_item_parent,
          source.to_diff_id,
          source.to_zone_id,
          case
             when source.to_item_parent is NOT NULL and
                  source.to_zone_id is NOT NULL then
                RPM_CONSTANTS.FR_HIER_PARENT_ZONE
             when source.to_item_parent is NOT NULL and
                  source.to_zone_id is NULL then
                RPM_CONSTANTS.FR_HIER_PARENT_LOC
             when source.to_item_parent is NULL and
                  source.to_zone_id is NOT NULL then
                RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             else
                RPM_CONSTANTS.FR_HIER_ITEM_LOC
          end,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC);

   delete from rpm_promo_item_loc_expl_gtt
    where cur_hier_level != RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select t2.*,
                 s.zone_id to_zone_id
            from (select t1.*,
                         RANK() OVER (PARTITION BY t1.to_item,
                                                   t1.to_location,
                                                   t1.to_zone_node_type
                                          ORDER BY t1.fr_rank desc) rank
                    from (-- explode PZ
                          select fr.*,
                                 0 fr_rank,
                                 im.class,
                                 im.subclass,
                                 im.item        to_item,
                                 im.diff_1      to_diff_id,
                                 im.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_cust_segment_promo_fr_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                             and im.item_parent    = fr.item
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and rzl.zone_id       = fr.location
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = rzl.location
                           union all
                          -- explode PL
                          select fr.*,
                                 1 fr_rank,
                                 im.class,
                                 im.subclass,
                                 im.item            to_item,
                                 im.diff_1          to_diff_id,
                                 im.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_cust_segment_promo_fr_gtt fr,
                                 item_master im,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                             and im.item_parent    = fr.item
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = fr.location
                           union all
                          -- explode DZ
                          select fr.*,
                                 1 fr_rank,
                                 im.class,
                                 im.subclass,
                                 im.item        to_item,
                                 im.diff_1      to_diff_id,
                                 im.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_cust_segment_promo_fr_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                             and im.item_parent    = fr.item
                             and im.diff_1         = fr.diff_id
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and rzl.zone_id       = fr.location
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = rzl.location
                           union all
                          -- explode DL
                          select fr.*,
                                 2 fr_rank,
                                 im.class,
                                 im.subclass,
                                 im.item            to_item,
                                 im.diff_1          to_diff_id,
                                 im.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_cust_segment_promo_fr_gtt fr,
                                 item_master im,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                             and im.item_parent    = fr.item
                             and im.diff_1         = fr.diff_id
                             and im.item_level     = im.tran_level
                             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.dept          = im.dept
                             and ril.item          = im.item
                             and ril.loc           = fr.location
                           union all
                          -- explode IZ
                          select fr.*,
                                 2 fr_rank,
                                 im.class,
                                 im.subclass,
                                 fr.item        to_item,
                                 fr.diff_id     to_diff_id,
                                 fr.item_parent to_item_parent,
                                 rzl.location   to_location,
                                 rzl.loc_type   to_zone_node_type
                            from rpm_cust_segment_promo_fr_gtt fr,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                             and im.item           = fr.item
                             and rzl.zone_id       = fr.location
                             and ril.dept          = fr.dept
                             and ril.item          = fr.item
                             and ril.loc           = rzl.location
                          -- explode IL
                           union all
                          select fr.*,
                                 3 fr_rank,
                                 im.class,
                                 im.subclass,
                                 fr.item            to_item,
                                 fr.diff_id         to_diff_id,
                                 fr.item_parent     to_item_parent,
                                 fr.location        to_location,
                                 fr.zone_node_type  to_zone_node_type
                            from rpm_cust_segment_promo_fr_gtt fr,
                                 item_master im
                           where fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                             and im.item           = fr.item) t1) t2,
                 rpm_merch_node_zone_node_gtt s
           where t2.rank              = 1
             and t2.dept              = s.dept (+)
             and t2.class             = s.class (+)
             and t2.subclass          = s.subclass (+)
             and t2.to_location       = s.location (+)
             and t2.to_zone_node_type = s.zone_node_type (+)) source
   on (    target.item                 = source.to_item
       and NVL(target.diff_id, -99999) = NVL(source.to_diff_id, -99999)
       and target.location             = source.to_location
       and target.zone_node_type       = source.to_zone_node_type
       and target.customer_type        = source.customer_type
       and target.action_date          = source.action_date)
   when MATCHED then
      update set
         target.zone_id        = source.to_zone_id,
         target.max_hier_level = case
                                    when source.to_item_parent is NOT NULL and
                                         source.to_zone_id is NOT NULL then
                                       RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                    when source.to_item_parent is NOT NULL and
                                         source.to_zone_id is NULL then
                                       RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                    when source.to_item_parent is NULL and
                                         source.to_zone_id is NOT NULL then
                                       RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                    else
                                       RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                 end
   when NOT MATCHED then
      insert
         (price_event_id,
          cust_segment_promo_id,
          item,
          zone_node_type,
          location,
          action_date,
          customer_type,
          dept,
          promo_retail,
          promo_retail_currency,
          promo_uom,
          complex_promo_ind,
          cspfr_rowid,
          item_parent,
          diff_id,
          zone_id,
          max_hier_level,
          cur_hier_level)
      values
         (source.price_event_id,
          RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
          source.to_item,
          source.to_zone_node_type,
          source.to_location,
          source.action_date,
          source.customer_type,
          source.dept,
          source.promo_retail,
          source.promo_retail_currency,
          source.promo_uom,
          source.complex_promo_ind,
          case
             when source.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC then
                source.cspfr_rowid
             else
                NULL
          end,
          source.to_item_parent,
          source.to_diff_id,
          source.to_zone_id,
          case
             when source.to_item_parent is NOT NULL and
                  source.to_zone_id is NOT NULL then
                RPM_CONSTANTS.FR_HIER_PARENT_ZONE
             when source.to_item_parent is NOT NULL and
                  source.to_zone_id is NULL then
                RPM_CONSTANTS.FR_HIER_PARENT_LOC
             when source.to_item_parent is NULL and
                  source.to_zone_id is NOT NULL then
                RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             else
                RPM_CONSTANTS.FR_HIER_ITEM_LOC
             end,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC);

   delete from rpm_cust_segment_promo_fr_gtt
    where cur_hier_level != RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END EXPLODE_TIMELINES_TO_IL;

-------------------------------------------------

FUNCTION VALIDATE_PZG_DEL(O_error_msg            OUT VARCHAR2,
                          O_invalid_ids          OUT OBJ_NUMERIC_ID_TABLE,
                          I_ids_to_be_deleted IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program  VARCHAR2(50)  := 'RPM_PRIMARY_ZONE_UPDATE_SQL.VALIDATE_PZG_DEL';

   L_check_val  NUMBER  := NULL;

   cursor C_CHECK is
      -- subclass being deleted
      select value(ids)
        from table(cast(I_ids_to_be_deleted as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_merch_retail_def mh
       where mh.merch_retail_def_id = value(ids)
         and mh.subclass            is NOT NULL
         and mh.class               is NOT NULL
         and mh.dept                is NOT NULL
         and NOT EXISTS (select 'x'
                           from rpm_merch_retail_def rmrd
                          where rmrd.dept     = mh.dept
                            and rmrd.class    = mh.class
                            and rmrd.subclass is NULL
                         union all
                         select 'x'
                           from rpm_merch_retail_def rmrd
                          where rmrd.dept     = mh.dept
                            and rmrd.class    is NULL
                            and rmrd.subclass is NULL)
      -- class being deleted
      union all
      select value(ids)
        from table(cast(I_ids_to_be_deleted as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_merch_retail_def mh
       where mh.merch_retail_def_id = value(ids)
         and mh.subclass            is NULL
         and mh.class               is NOT NULL
         and mh.dept                is NOT NULL
         and NOT EXISTS (select 'x'
                           from rpm_merch_retail_def rmrd
                          where rmrd.dept     = mh.dept
                            and rmrd.class    is NULL
                            and rmrd.subclass is NULL)
      -- dept being deleted
      union all
      select value(ids)
        from table(cast(I_ids_to_be_deleted as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_merch_retail_def mh
       where mh.merch_retail_def_id = value(ids)
         and mh.subclass            is NULL
         and mh.class               is NULL
         and mh.dept                is NOT NULL;

BEGIN

   open C_CHECK;
   fetch C_CHECK BULK COLLECT into O_invalid_ids;
   close C_CHECK;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_PZG_DEL;

--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRIMARY_ZONE_UPDATE_SQL.POPULATE_GTT';

BEGIN

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;

   -- Populate RPM_FUTURE_RETAIL_GTT
   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       lock_version,
       rfr_rowid,
       timeline_seq,
       on_simple_promo_ind,
       on_complex_promo_ind,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
      ----
      -- Get all records on rpm_future_retail...
      ----
      select LP_price_event_id,
             future_retail_id,
             fr.dept,
             fr.class,
             fr.subclass,
             fr.item,
             fr.zone_node_type,
             fr.location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             fr.rowid,
             NULL,
             fr.on_simple_promo_ind,
             fr.on_complex_promo_ind,
             fr.item_parent,
             fr.diff_id,
             fr.zone_id,
             fr.cur_hier_level,
             fr.max_hier_level
        from rpm_future_retail fr,
             rpm_me_item_gtt rmig
       where fr.dept     = rmig.dept
         and fr.class    = rmig.class
         and fr.subclass = rmig.subclass;

   -- Populate RPM_PROMO_ITEM_LOC_EXPL_GTT
   insert into rpm_promo_item_loc_expl_gtt
      (price_event_id,
       promo_item_loc_expl_id,
       item,
       dept,
       class,
       subclass,
       location,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       promo_comp_id,
       comp_display_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       exception_parent_id,
       promo_comp_msg_type,
       rpile_rowid,
       zone_node_type,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select LP_price_event_id,
          ilex.promo_item_loc_expl_id,
          ilex.item,
          ilex.dept,
          ilex.class,
          ilex.subclass,
          ilex.location,
          ilex.promo_id,
          ilex.promo_display_id,
          ilex.promo_secondary_ind,
          ilex.promo_comp_id,
          ilex.comp_display_id,
          ilex.promo_dtl_id,
          ilex.type,
          ilex.customer_type,
          ilex.detail_secondary_ind,
          ilex.detail_start_date,
          ilex.detail_end_date,
          ilex.detail_apply_to_code,
          ilex.detail_change_type,
          ilex.detail_change_amount,
          ilex.detail_change_currency,
          ilex.detail_change_percent,
          ilex.detail_change_selling_uom,
          ilex.detail_price_guide_id,
          ilex.exception_parent_id,
          NULL,
          ilex.rowid,
          ilex.zone_node_type,
          ilex.item_parent,
          ilex.diff_id,
          ilex.zone_id,
          ilex.cur_hier_level,
          ilex.max_hier_level
     from rpm_promo_item_loc_expl ilex,
          rpm_me_item_gtt rmig
    where ilex.dept     = rmig.dept
      and ilex.class    = rmig.class
      and ilex.subclass = rmig.subclass;

   -- Populate RPM_CUST_SEGMENT_PROMO_FR_GTT
   insert into rpm_cust_segment_promo_fr_gtt
      (price_event_id,
       cust_segment_promo_id,
       item,
       zone_node_type,
       location,
       action_date,
       customer_type,
       dept,
       promo_retail,
       promo_retail_currency,
       promo_uom,
       complex_promo_ind,
       cspfr_rowid,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select LP_price_event_id,
          cspfr.cust_segment_promo_id,
          cspfr.item,
          cspfr.zone_node_type,
          cspfr.location,
          cspfr.action_date,
          cspfr.customer_type,
          cspfr.dept,
          cspfr.promo_retail,
          cspfr.promo_retail_currency,
          cspfr.promo_uom,
          cspfr.complex_promo_ind,
          cspfr.rowid,
          cspfr.item_parent,
          cspfr.diff_id,
          cspfr.zone_id,
          cspfr.cur_hier_level,
          cspfr.max_hier_level
     from (select im.dept,
                  im.item
             from rpm_me_item_gtt rmig,
                  item_master im
            where im.dept         = rmig.dept
              and im.class        = rmig.class
              and im.subclass     = rmig.subclass
              and im.item_level   = im.tran_level
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           select im.dept,
                  im.item
             from rpm_me_item_gtt rmig,
                  item_master im
            where im.dept         = rmig.dept
              and im.class        = rmig.class
              and im.subclass     = rmig.subclass
              and im.item_level   = im.tran_level - 1
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t,
          rpm_cust_segment_promo_fr cspfr
    where cspfr.dept = t.dept
      and cspfr.item = t.item;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END POPULATE_GTT;
--------------------------------------------------------------------------------

END RPM_PRIMARY_ZONE_UPDATE_SQL;
/
