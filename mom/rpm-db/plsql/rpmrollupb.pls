CREATE OR REPLACE PACKAGE BODY RPM_ROLLUP AS

LP_rfr_rollup_batch CONSTANT VARCHAR2(45) := 'com.retek.rpm.batch.FutureRetailRollupBatch';

NUMERIC_TRUE  CONSTANT NUMBER(1) := 1;
NUMERIC_FALSE CONSTANT NUMBER(1) := 0;
--------------------------------------------------------

FUNCTION PERFORM_ROLLUP(O_error_msg          IN OUT VARCHAR2,
                        I_rollup_type        IN     VARCHAR2,
                        I_max_hier_level_tbl IN     OBJ_VARCHAR_ID_TABLE,
                        I_cur_hier_level_tbl IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION POPULATE_GTT(O_error_msg        OUT VARCHAR2,
                      I_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION PUSH_BACK(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION POP_GTT_AND_ROLL_UP(O_error_msg      OUT VARCHAR2,
                             I_rollup_type IN     RPM_FUTURE_RETAIL.DEPT%TYPE DEFAULT RPM_ROLLUP.ALL_TO_ALL)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION VALIDATE_INTERSECTION(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION PUSH_BACK_FOR_NIL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION VALIDATE_RETAIL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IZ_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_PL_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IL_TO_IZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IL_TO_PL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IL_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IL_TO_DL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IZ_TO_DZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_DL_TO_DZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_DL_TO_PL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_DZ_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_DL_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IL_TO_DZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION ROLL_IZ_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_IZ_TO_PZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_ZONE;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IZ_TO_PZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IZ_TO_PZ;
--------------------------------------------------------
FUNCTION ROLL_PL_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_PL_TO_PZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.PL_TO_PZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_PL_TO_PZ;
--------------------------------------------------------
FUNCTION ROLL_IL_TO_IZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_IL_TO_IZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_ZONE;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_DIFF_ZONE;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(3) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.extend;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IL_TO_IZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IL_TO_IZ;
--------------------------------------------------------
FUNCTION ROLL_IL_TO_PL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_IL_TO_PL';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_LOC;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IL_TO_PL,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IL_TO_PL;
--------------------------------------------------------
FUNCTION ROLL_IL_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_IL_TO_PZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IL_TO_PZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IL_TO_PZ;
--------------------------------------------------------
FUNCTION ROLL_IL_TO_DL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_IL_TO_DL';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_LOC;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_DIFF_ZONE;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(3) := RPM_CONSTANTS.FR_HIER_PARENT_LOC;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(4) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IL_TO_DL,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IL_TO_DL;
--------------------------------------------------------
FUNCTION ROLL_IZ_TO_DZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_IZ_TO_DZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_ZONE;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_ZONE;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IZ_TO_DZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IZ_TO_DZ;
--------------------------------------------------------
FUNCTION ROLL_DL_TO_DZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_DL_TO_DZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_ZONE;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.DL_TO_DZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_DL_TO_DZ;
--------------------------------------------------------
FUNCTION ROLL_DL_TO_PL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_DL_TO_PL';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_LOC;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.DL_TO_PL,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_DL_TO_PL;
--------------------------------------------------------
FUNCTION ROLL_DZ_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25)  := 'RPM_ROLLUP.ROLL_DZ_TO_PZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_ZONE;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.DZ_TO_PZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_DZ_TO_PZ;
--------------------------------------------------------
FUNCTION ROLL_DL_TO_PZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25) := 'RPM_ROLLUP.ROLL_DL_TO_PZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.DL_TO_PZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_DL_TO_PZ;
--------------------------------------------------------
FUNCTION ROLL_IL_TO_DZ(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(25)  := 'RPM_ROLLUP.ROLL_IL_TO_DZ';

   L_max_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_cur_hier_level_tbl OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Do not move/remove the first record of this collection
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_DIFF_ZONE;
   L_max_hier_level_tbl.EXTEND;
   L_max_hier_level_tbl(2) := RPM_CONSTANTS.FR_HIER_PARENT_ZONE;

   L_cur_hier_level_tbl.EXTEND;
   L_cur_hier_level_tbl(1) := RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   if PERFORM_ROLLUP(O_error_msg,
                     RPM_ROLLUP.IL_TO_DZ,
                     L_max_hier_level_tbl,
                     L_cur_hier_level_tbl) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;
END ROLL_IL_TO_DZ;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION PERFORM_ROLLUP(O_error_msg          IN OUT VARCHAR2,
                        I_rollup_type        IN     VARCHAR2,
                        I_max_hier_level_tbl IN     OBJ_VARCHAR_ID_TABLE,
                        I_cur_hier_level_tbl IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_ROLLUP.PERFORM_ROLLUP';

BEGIN

   delete rpm_rollup_gtt;

   insert into rpm_rollup_gtt (item,
                               diff_id,
                               dept,
                               class,
                               subclass,
                               zone_node_type,
                               location,
                               target_item,
                               target_diff_id,
                               target_zone_node_type,
                               target_zone_node,
                               rollup_type,
                               parent_tl_rec_count,
                               child_tl_rec_count)
      select inner.item,
             inner.diff_id,
             inner.dept,
             inner.class,
             inner.subclass,
             inner.zone_node_type,
             inner.location,
             inner.target_item,
             inner.target_diff_id,
             inner.target_zone_node_type,
             inner.target_zone_node,
             I_rollup_type,
             p_tl_count,
             i_tl_count
        from (--get item/locations that can be deleted
              --assumes extra dates will never exist on the parent timeline
              select i.item,
                     i.diff_id,
                     i.dept,
                     i.class,
                     i.subclass,
                     i.zone_node_type,
                     i.location,
                     i.zone_id,
                     i.target_item,
                     i.target_diff_id,
                     i.target_zone_node,
                     i.target_zone_node_type,
                     p.tl_count p_tl_count,
                     i.tl_count i_tl_count,
                     ROW_NUMBER() OVER (PARTITION BY i.item,
                                                     NVL(i.diff_id, -999),
                                                     i.location,
                                                     i.zone_node_type
                                            ORDER BY i.action_date) row_cnt,
                     -- Compare the fields of each record that matches up between the child timeline (i) and the parent
                     -- timeline (p) for any differences between the two.  For those that match up exactly, data for the
                     -- child timeline will be inserted into the rpm_rollup_gtt table.
                     SUM(DECODE(i.selling_retail,
                                NVL(p.selling_retail, -999), 0,
                                1) +
                         DECODE(i.selling_retail_currency,
                                NVL(p.selling_retail_currency, -999), 0,
                                1) +
                         DECODE(i.selling_uom,
                                NVL(p.selling_uom,-999), 0,
                                1) +
                         DECODE(NVL(i.multi_units, -999),
                                NVL(p.multi_units, -999), 0,
                                1) +
                         DECODE(NVL(i.multi_unit_retail, -999),
                                NVL(p.multi_unit_retail, -999), 0,
                                1) +
                         DECODE(NVL(i.multi_unit_retail_currency, -999),
                                NVL(p.multi_unit_retail_currency, -999), 0,
                                1) +
                         DECODE(NVL(i.multi_selling_uom,-999),
                                NVL(p.multi_selling_uom, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_retail, -999),
                                NVL(p.clear_retail, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_retail_currency, -999),
                                NVL(p.clear_retail_currency, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_uom, -999),
                                NVL(p.clear_uom, -999), 0,
                                1) +
                         DECODE(NVL(i.simple_promo_retail, -999),
                                NVL(p.simple_promo_retail, -999), 0,
                                1) +
                         DECODE(NVL(i.simple_promo_retail_currency, -999),
                                NVL(p.simple_promo_retail_currency, -999), 0,
                                1) +
                         DECODE(NVL(i.simple_promo_uom, -999),
                                NVL(p.simple_promo_uom, -999), 0,
                                1) +
                         DECODE(i.on_simple_promo_ind,
                                NVL(p.on_simple_promo_ind, -999), 0,
                                1) +
                         DECODE(i.on_complex_promo_ind,
                                NVL(p.on_complex_promo_ind, -999), 0,
                                1) +
                         DECODE(NVL(i.price_change_id, -999),
                                NVL(p.price_change_id, -999), 0,
                                1) +
                         DECODE(NVL(i.price_change_display_id, -999),
                                NVL(p.price_change_display_id, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_exception_parent_id, -999),
                                NVL(p.pc_exception_parent_id, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_change_type, -999),
                                NVL(p.pc_change_type, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_change_amount, -999),
                                NVL(p.pc_change_amount, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_change_currency, -999),
                                NVL(p.pc_change_currency, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_change_percent, -999),
                                NVL(p.pc_change_percent, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_change_selling_uom, -999),
                                NVL(p.pc_change_selling_uom, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_null_multi_ind, -999),
                                NVL(p.pc_null_multi_ind, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_multi_units, -999),
                                NVL(p.pc_multi_units, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_multi_unit_retail, -999),
                                NVL(p.pc_multi_unit_retail, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_multi_unit_retail_currency, -999),
                                NVL(p.pc_multi_unit_retail_currency, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_multi_selling_uom, -999),
                                NVL(p.pc_multi_selling_uom, -999), 0,
                                1) +
                         DECODE(NVL(i.pc_price_guide_id, -999),
                                NVL(p.pc_price_guide_id, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_exception_parent_id, -999),
                                NVL(p.clear_exception_parent_id, -999), 0,
                                1) +
                         DECODE(NVL(i.clearance_id, -999),
                                NVL(p.clearance_id, -999), 0,
                                1) +
                         DECODE(NVL(i.clearance_display_id, -999),
                                NVL(p.clearance_display_id, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_mkdn_index, -999),
                                NVL(p.clear_mkdn_index, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_start_ind, -999),
                                NVL(p.clear_start_ind, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_change_type, -999),
                                NVL(p.clear_change_type, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_change_amount, -999),
                                NVL(p.clear_change_amount, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_change_currency, -999),
                                NVL(p.clear_change_currency, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_change_percent, -999),
                                NVL(p.clear_change_percent, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_change_selling_uom, -999),
                                NVL(p.clear_change_selling_uom, -999), 0,
                                1) +
                         DECODE(NVL(i.clear_price_guide_id, -999),
                                NVL(p.clear_price_guide_id, -999), 0,
                                1) +
                         DECODE(NVL(i.loc_move_from_zone_id, -999),
                                NVL(p.loc_move_from_zone_id, -999), 0,
                                1) +
                         DECODE(NVL(i.loc_move_to_zone_id, -999),
                                NVL(p.loc_move_to_zone_id, -999), 0,
                                1) +
                         DECODE(NVL(i.location_move_id, -999),
                                NVL(p.location_move_id, -999), 0,
                                1)) OVER (PARTITION BY i.item,
                                                       i.diff_id,
                                                       i.location,
                                                       i.zone_node_type) variation_count
                from (select dept,
                             item,
                             diff_id,
                             location,
                             zone_node_type,
                             action_date,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             clear_retail,
                             clear_retail_currency,
                             clear_uom,
                             simple_promo_retail,
                             simple_promo_retail_currency,
                             simple_promo_uom,
                             on_simple_promo_ind,
                             on_complex_promo_ind,
                             price_change_id,
                             price_change_display_id,
                             pc_exception_parent_id,
                             pc_change_type,
                             pc_change_amount,
                             pc_change_currency,
                             pc_change_percent,
                             pc_change_selling_uom,
                             pc_null_multi_ind,
                             pc_multi_units,
                             pc_multi_unit_retail,
                             pc_multi_unit_retail_currency,
                             pc_multi_selling_uom,
                             pc_price_guide_id,
                             clear_exception_parent_id,
                             clearance_id,
                             clearance_display_id,
                             clear_mkdn_index,
                             clear_start_ind,
                             clear_change_type,
                             clear_change_amount,
                             clear_change_currency,
                             clear_change_percent,
                             clear_change_selling_uom,
                             clear_price_guide_id,
                             loc_move_from_zone_id,
                             loc_move_to_zone_id,
                             location_move_id,
                             cur_hier_level,
                             max_hier_level,
                             COUNT(1) OVER (PARTITION BY item,
                                                         NVL(diff_id, -999),
                                                         location,
                                                         zone_node_type) tl_count
                        from rpm_future_retail_gtt) p,
                     (select /*+ INDEX(r, RPM_FUTURE_RETAIL_GTT_TMP) */
                             r.dept,
                             r.class,
                             r.subclass,
                             r.item,
                             r.diff_id,
                             r.location,
                             r.zone_node_type,
                             r.action_date,
                             r.selling_retail,
                             r.selling_retail_currency,
                             r.selling_uom,
                             r.multi_units,
                             r.multi_unit_retail,
                             r.multi_unit_retail_currency,
                             r.multi_selling_uom,
                             r.clear_retail,
                             r.clear_retail_currency,
                             r.clear_uom,
                             r.simple_promo_retail,
                             r.simple_promo_retail_currency,
                             r.simple_promo_uom,
                             r.on_simple_promo_ind,
                             r.on_complex_promo_ind,
                             r.price_change_id,
                             r.price_change_display_id,
                             r.pc_exception_parent_id,
                             r.pc_change_type,
                             r.pc_change_amount,
                             r.pc_change_currency,
                             r.pc_change_percent,
                             r.pc_change_selling_uom,
                             r.pc_null_multi_ind,
                             r.pc_multi_units,
                             r.pc_multi_unit_retail,
                             r.pc_multi_unit_retail_currency,
                             r.pc_multi_selling_uom,
                             r.pc_price_guide_id,
                             r.clear_exception_parent_id,
                             r.clearance_id,
                             r.clearance_display_id,
                             r.clear_mkdn_index,
                             r.clear_start_ind,
                             r.clear_change_type,
                             r.clear_change_amount,
                             r.clear_change_currency,
                             r.clear_change_percent,
                             r.clear_change_selling_uom,
                             r.clear_price_guide_id,
                             r.loc_move_from_zone_id,
                             r.loc_move_to_zone_id,
                             r.location_move_id,
                             r.zone_id,
                             r.item_parent,
                             r.cur_hier_level,
                             r.max_hier_level,
                             COUNT(1) OVER (PARTITION BY r.item,
                                                         NVL(r.diff_id, -999),
                                                         r.location,
                                                         r.zone_node_type) tl_count,
                             DECODE(I_rollup_type,
                                    RPM_ROLLUP.IL_TO_IZ, r.item,
                                    RPM_ROLLUP.IL_TO_PL, r.item_parent,
                                    RPM_ROLLUP.IZ_TO_PZ, r.item_parent,
                                    RPM_ROLLUP.PL_TO_PZ, r.item,
                                    RPM_ROLLUP.IL_TO_PZ, r.item_parent,
                                    RPM_ROLLUP.IL_TO_DL, r.item_parent,
                                    RPM_ROLLUP.IZ_TO_DZ, r.item_parent,
                                    RPM_ROLLUP.DL_TO_DZ, r.item,
                                    RPM_ROLLUP.DL_TO_PL, r.item,
                                    RPM_ROLLUP.DZ_TO_PZ, r.item,
                                    RPM_ROLLUP.DL_TO_PZ, r.item,
                                    RPM_ROLLUP.IL_TO_DZ, r.item_parent) target_item,
                             DECODE(I_rollup_type,
                                    RPM_ROLLUP.IL_TO_IZ, NVL(r.diff_id,'-999'),
                                    RPM_ROLLUP.IL_TO_PL, '-999',
                                    RPM_ROLLUP.IZ_TO_PZ, '-999',
                                    RPM_ROLLUP.PL_TO_PZ, '-999',
                                    RPM_ROLLUP.IL_TO_PZ, '-999',
                                    RPM_ROLLUP.IL_TO_DL, r.diff_id,
                                    RPM_ROLLUP.IZ_TO_DZ, r.diff_id,
                                    RPM_ROLLUP.DL_TO_DZ, r.diff_id,
                                    RPM_ROLLUP.DL_TO_PL, '-999',
                                    RPM_ROLLUP.DZ_TO_PZ, '-999',
                                    RPM_ROLLUP.DL_TO_PZ, '-999',
                                    RPM_ROLLUP.IL_TO_DZ, r.diff_id) target_diff_id,
                             DECODE(I_rollup_type,
                                    RPM_ROLLUP.IL_TO_IZ, 1,
                                    RPM_ROLLUP.IL_TO_PL, r.zone_node_type,
                                    RPM_ROLLUP.IZ_TO_PZ, r.zone_node_type,
                                    RPM_ROLLUP.PL_TO_PZ, 1,
                                    RPM_ROLLUP.IL_TO_PZ, 1,
                                    RPM_ROLLUP.IL_TO_DL, r.zone_node_type,
                                    RPM_ROLLUP.IZ_TO_DZ, r.zone_node_type,
                                    RPM_ROLLUP.DL_TO_DZ, 1,
                                    RPM_ROLLUP.DL_TO_PL, r.zone_node_type,
                                    RPM_ROLLUP.DZ_TO_PZ, r.zone_node_type,
                                    RPM_ROLLUP.DL_TO_PZ, 1,
                                    RPM_ROLLUP.IL_TO_DZ, 1) target_zone_node_type,
                             DECODE(I_rollup_type,
                                    RPM_ROLLUP.IL_TO_IZ, r.zone_id,
                                    RPM_ROLLUP.IL_TO_PL, r.location,
                                    RPM_ROLLUP.IZ_TO_PZ, r.location,
                                    RPM_ROLLUP.PL_TO_PZ, r.zone_id,
                                    RPM_ROLLUP.IL_TO_PZ, r.zone_id,
                                    RPM_ROLLUP.IL_TO_DL, r.location,
                                    RPM_ROLLUP.IZ_TO_DZ, r.location,
                                    RPM_ROLLUP.DL_TO_DZ, r.zone_id,
                                    RPM_ROLLUP.DL_TO_PL, r.location,
                                    RPM_ROLLUP.DZ_TO_PZ, r.location,
                                    RPM_ROLLUP.DL_TO_PZ, r.zone_id,
                                    RPM_ROLLUP.IL_TO_DZ, r.zone_id) target_zone_node
                        from rpm_future_retail_gtt r
                       where r.cur_hier_level       IN (select value(cr)
                                                          from table(cast(I_cur_hier_level_tbl as OBJ_VARCHAR_ID_TABLE)) cr)
                         and r.max_hier_level       IN (select value(mx)
                                                          from table(cast(I_max_hier_level_tbl as OBJ_VARCHAR_ID_TABLE)) mx)
                         and NVL(r.timeline_seq, 0) NOT IN (-999,  -- already marked for deletion
                                                            -1111) -- marked as needed due to mismatch in timeline rec count
                         and rownum                 > 0) i
               where i.dept                        = p.dept (+)
                 and i.target_item                 = p.item (+)
                 and NVL(i.target_diff_id, '-999') = NVL(p.diff_id (+), '-999')
                 and i.target_zone_node            = p.location (+)
                 and i.target_zone_node_type       = p.zone_node_type (+)
                 and i.action_date                 = p.action_date (+)
                 and i.max_hier_level              = p.max_hier_level (+)
                 and p.cur_hier_level (+)          = I_max_hier_level_tbl(1)) inner
       where inner.variation_count = 0
         and inner.row_cnt         = 1;

   -- if there is a "child" timeline marked for deletion that has a timeline rec count different
   -- from the parent timeline rec count, mark that timeline on RFR_GTT with a -1111 timeline_seq
   -- and remove the record from rpm_rollup_gtt so that it will be retained.  This is needed for
   -- a scenario where an executed clearance was not inherited by NIL, for example.

   merge into rpm_future_retail_gtt target
   using (select item,
                 diff_id,
                 location,
                 zone_node_type
            from rpm_rollup_gtt
           where parent_tl_rec_count != child_tl_rec_count) source
   on (    target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type)
   when MATCHED then
      update
         set timeline_seq = -1111;

   delete
     from rpm_rollup_gtt
    where parent_tl_rec_count != child_tl_rec_count;

   -- remove any records that correspond to timelines that do not match up on RPILE

   delete from rpm_rollup_gtt
    where (item,
           zone_node_type,
           location) IN (select inner.item,
                                inner.zone_node_type,
                                inner.location
                           from (select g.item,
                                        g.zone_node_type,
                                        g.location,
                                        ROW_NUMBER() OVER (PARTITION BY g.item,
                                                                        NVL(g.diff_id, '-999'),
                                                                        g.zone_node_type,
                                                                        g.location
                                                               ORDER BY g.promo_dtl_id) row_cnt,
                                        SUM(DECODE(g.promo_dtl_id,
                                                   NVL(p.promo_dtl_id, -999), 0,
                                                   1)) OVER (PARTITION BY g.item,
                                                                          NVL(g.diff_id, '-999'),
                                                                          g.zone_node_type,
                                                                          g.location) variation_count
                                   from (select gtt.dept,
                                                gtt.target_item,
                                                gtt.target_zone_node,
                                                gtt.target_zone_node_type,
                                                gtt.item,
                                                gtt.diff_id,
                                                gtt.zone_node_type,
                                                gtt.location,
                                                NVL(gtt.target_diff_id, '-999') inner_query_diff_id,
                                                i.promo_dtl_id
                                           from rpm_rollup_gtt gtt,
                                                rpm_promo_item_loc_expl_gtt i
                                          where gtt.rollup_type        = I_rollup_type
                                            and i.item                 = gtt.item
                                            and NVL(i.diff_id, '-999') = NVL(gtt.diff_id, '-999')
                                            and i.location             = gtt.location
                                            and i.zone_node_type       = gtt.zone_node_type
                                            and NVL(i.deleted_ind, 0) != 1) g,
                                        rpm_promo_item_loc_expl_gtt p
                                  where g.dept                  = p.dept (+)
                                    and g.target_item           = p.item (+)
                                    and g.inner_query_diff_id   = NVL(p.diff_id (+), '-999')
                                    and g.target_zone_node      = p.location (+)
                                    and g.target_zone_node_type = p.zone_node_type (+)
                                    and g.promo_dtl_id          = p.promo_dtl_id (+)) inner
                          where inner.variation_count != 0
                            and inner.row_cnt          = 1);

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select /*+ ORDERED */
                 distinct
                 cspf.price_event_id,
                 cspf.item,
                 cspf.diff_id,
                 cspf.dept,
                 cspf.zone_node_type,
                 cspf.location,
                 cspf.action_date,
                 cspf.customer_type
            from rpm_rollup_gtt gtt,
                 rpm_cust_segment_promo_fr_gtt cspf
           where cspf.dept                          = gtt.dept
             and cspf.item                          = gtt.item
             and cspf.location                      = gtt.location
             and cspf.zone_node_type                = gtt.zone_node_type
             and NVL(cspf.diff_id, '-9999')         = NVL(gtt.diff_id, '-9999')
             and NVL(cspf.original_cs_promo_id, 0) != -999) source
   on (    target.price_event_id       = source.price_event_id
       and target.dept                 = source.dept
       and target.location             = source.location
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.customer_type        = source.customer_type
       and target.action_date          = source.action_date)
   when MATCHED then
      update
         set target.original_cs_promo_id = -999;

   merge into rpm_promo_item_loc_expl_gtt target
   using (select /*+ ORDERED */
                 distinct
                 ilex.price_event_id,
                 ilex.item,
                 ilex.diff_id,
                 ilex.dept,
                 ilex.zone_node_type,
                 ilex.location,
                 ilex.promo_dtl_id
            from rpm_rollup_gtt gtt,
                 rpm_promo_item_loc_expl_gtt ilex
           where ilex.dept                   = gtt.dept
             and ilex.item                   = gtt.item
             and ilex.location               = gtt.location
             and ilex.zone_node_type         = gtt.zone_node_type
             and NVL(ilex.diff_id, '-9999')  = NVL(gtt.diff_id, '-9999')
             and NVL(ilex.deleted_ind, 0)   != 1) source
   on (    target.price_event_id       = source.price_event_id
       and target.dept                 = source.dept
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.promo_dtl_id         = source.promo_dtl_id)
   when MATCHED then
      update
         set target.deleted_ind = 1;

   merge /*+ FULL(target) */ into rpm_future_retail_gtt target
   using (select /*+ ORDERED */
                 distinct
                 fr.price_event_id,
                 fr.item,
                 fr.diff_id,
                 fr.dept,
                 fr.zone_node_type,
                 fr.location,
                 fr.action_date
            from rpm_rollup_gtt gtt,
                 rpm_future_retail_gtt fr
           where fr.dept                   = gtt.dept
             and fr.item                   = gtt.item
             and fr.location               = gtt.location
             and fr.zone_node_type         = gtt.zone_node_type
             and NVL(fr.diff_id, '-9999')  = NVL(gtt.diff_id, '-9999')
             and NVL(fr.timeline_seq, 0)  != -999) source
      on (    target.price_event_id       = source.price_event_id
          and target.dept                 = source.dept
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.action_date          = source.action_date)
      when MATCHED then
         update
            set target.timeline_seq = -999;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END PERFORM_ROLLUP;
--------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg        OUT VARCHAR2,
                      I_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(25) := 'RPM_ROLLUP.POPULATE_GTT';
   L_dept    NUMBER(4)    := NULL;

   cursor C_DEPT is
      select dept
        from rpm_rfr_rollup_thread
       where thread_number = I_thread_number
         and rownum = 1;

BEGIN

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;
   delete rpm_rfr_rollup_item_expl_gtt;

   insert into rpm_rfr_rollup_item_expl_gtt
      (dept,
       item,
       item_level)
   select dept,
          item_id,
          item_type
     from rpm_rfr_rollup_thread
    where thread_number = I_thread_number
   union all
   select rrt.dept,
          im.item,
          rrt.item_type
     from rpm_rfr_rollup_thread rrt,
          item_master im
    where rrt.thread_number = I_thread_number
      and rrt.item_type     = RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_PARENT
      and rrt.item_id       = im.item_parent
      and im.item_level     = im.tran_level
      and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   open C_DEPT;
   fetch C_DEPT into L_dept;
   close C_DEPT;

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       lock_version,
       rfr_rowid,
       on_simple_promo_ind,
       on_complex_promo_ind,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
      select -9999999999,
             fr.future_retail_id,
             fr.dept,
             fr.class,
             fr.subclass,
             fr.item,
             fr.zone_node_type,
             fr.location,
             fr.action_date,
             fr.selling_retail,
             fr.selling_retail_currency,
             fr.selling_uom,
             fr.multi_units,
             fr.multi_unit_retail,
             fr.multi_unit_retail_currency,
             fr.multi_selling_uom,
             fr.clear_retail,
             fr.clear_retail_currency,
             fr.clear_uom,
             fr.simple_promo_retail,
             fr.simple_promo_retail_currency,
             fr.simple_promo_uom,
             fr.price_change_id,
             fr.price_change_display_id,
             fr.pc_exception_parent_id,
             fr.pc_change_type,
             fr.pc_change_amount,
             fr.pc_change_currency,
             fr.pc_change_percent,
             fr.pc_change_selling_uom,
             fr.pc_null_multi_ind,
             fr.pc_multi_units,
             fr.pc_multi_unit_retail,
             fr.pc_multi_unit_retail_currency,
             fr.pc_multi_selling_uom,
             fr.pc_price_guide_id,
             fr.clearance_id,
             fr.clearance_display_id,
             fr.clear_mkdn_index,
             fr.clear_start_ind,
             fr.clear_change_type,
             fr.clear_change_amount,
             fr.clear_change_currency,
             fr.clear_change_percent,
             fr.clear_change_selling_uom,
             fr.clear_price_guide_id,
             fr.loc_move_from_zone_id,
             fr.loc_move_to_zone_id,
             fr.location_move_id,
             fr.lock_version,
             fr.rowid  rfr_rowid,
             fr.on_simple_promo_ind,
             fr.on_complex_promo_ind,
             fr.item_parent,
             fr.diff_id,
             fr.zone_id,
             fr.cur_hier_level,
             fr.max_hier_level
        from rpm_future_retail fr,
             rpm_rfr_rollup_item_expl_gtt gtt
       where fr.dept = L_dept
         and fr.item = gtt.item;

   insert into rpm_promo_item_loc_expl_gtt
      (price_event_id,
       promo_item_loc_expl_id,
       item,
       dept,
       class,
       subclass,
       location,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       promo_comp_id,
       comp_display_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       exception_parent_id,
       rpile_rowid,
       zone_node_type,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select -9999999999,
          ilex.promo_item_loc_expl_id,
          ilex.item,
          ilex.dept,
          ilex.class,
          ilex.subclass,
          ilex.location,
          ilex.promo_id,
          ilex.promo_display_id,
          ilex.promo_secondary_ind,
          ilex.promo_comp_id,
          ilex.comp_display_id,
          ilex.promo_dtl_id,
          ilex.type,
          ilex.customer_type,
          ilex.detail_secondary_ind,
          ilex.detail_start_date,
          ilex.detail_end_date,
          ilex.detail_apply_to_code,
          ilex.detail_change_type,
          ilex.detail_change_amount,
          ilex.detail_change_currency,
          ilex.detail_change_percent,
          ilex.detail_change_selling_uom,
          ilex.detail_price_guide_id,
          ilex.exception_parent_id,
          ilex.rowid rpile_rowid,
          ilex.zone_node_type,
          ilex.item_parent,
          ilex.diff_id,
          ilex.zone_id,
          ilex.cur_hier_level,
          ilex.max_hier_level
     from rpm_promo_item_loc_expl ilex,
          rpm_rfr_rollup_item_expl_gtt gtt
    where ilex.dept = L_dept
      and ilex.item = gtt.item;

   insert into rpm_cust_segment_promo_fr_gtt
      (price_event_id,
       cust_segment_promo_id,
       item,
       zone_node_type,
       location,
       action_date,
       customer_type,
       dept,
       promo_retail,
       promo_retail_currency,
       promo_uom,
       complex_promo_ind,
       cspfr_rowid,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select -9999999999,
          cspfr.cust_segment_promo_id,
          cspfr.item,
          cspfr.zone_node_type,
          cspfr.location,
          cspfr.action_date,
          cspfr.customer_type,
          cspfr.dept,
          cspfr.promo_retail,
          cspfr.promo_retail_currency,
          cspfr.promo_uom,
          cspfr.complex_promo_ind,
          cspfr.rowid  cspfr_rowid,
          cspfr.item_parent,
          cspfr.diff_id,
          cspfr.zone_id,
          cspfr.cur_hier_level,
          cspfr.max_hier_level
     from (select t.item,
                  t.location
             from (select gtt.item,
                          gtt.location,
                          RANK() OVER(PARTITION BY gtt.item,
                                                   gtt.location
                                          ORDER BY gtt.action_date desc) rank
                     from rpm_future_retail_gtt gtt) t
            where t.rank = 1) fr,
          rpm_cust_segment_promo_fr cspfr
    where cspfr.dept     = L_dept
      and cspfr.item     = fr.item
      and cspfr.location = fr.location;

   return NUMERIC_TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;

END POPULATE_GTT;
--------------------------------------------------------
FUNCTION PUSH_BACK(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ROLLUP.PUSH_BACK';

BEGIN

   delete from rpm_future_retail rfr
    where rfr.rowid IN (select rfrg.rfr_rowid
                          from rpm_future_retail_gtt rfrg
                         where rfrg.timeline_seq = -999);

   delete from rpm_cust_segment_promo_fr cspf
    where cspf.rowid IN (select cspfg.cspfr_rowid
                           from rpm_cust_segment_promo_fr_gtt cspfg
                          where cspfg.original_cs_promo_id = -999);

   delete from rpm_promo_item_loc_expl rpile
    where rpile.rowid IN (select rpileg.rpile_rowid
                            from rpm_promo_item_loc_expl_gtt rpileg
                           where rpileg.deleted_ind = 1);

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;

END PUSH_BACK;
--------------------------------------------------------

FUNCTION ROLL_FUTURE_RETAIL(O_error_msg     IN OUT VARCHAR2,
                            I_thread_number IN     NUMBER,
                            I_rollup_type   IN     RPM_FUTURE_RETAIL.DEPT%TYPE DEFAULT RPM_ROLLUP.ALL_TO_ALL)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_ROLLUP.ROLL_FUTURE_RETAIL';

   L_rollup_type NUMBER(2) := NULL;

BEGIN

   L_rollup_type := I_rollup_type;

   if L_rollup_type is NULL then
      L_rollup_type := RPM_ROLLUP.ALL_TO_ALL;
   end if;

   if POPULATE_GTT(O_error_msg,
                   I_thread_number) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   -- Do not change the order of the calls to the rollup functions below.  The rollup for each
   -- level needs to be in the current order to prevent removing lower level timelines incorrectly.
   -- The order needed here is as follows:
   --          PL_TO_PZ
   --          DZ_TO_PZ
   --          IZ_TO_DZ then IZ_TO_PZ
   --          DL_TO_DZ then DL_TO_PL then DL_TO_PZ
   --          IL_TO_IZ then IL_TO_DL then IL_TO_DZ then IL_TO_PL then IL_TO_PZ

   if L_rollup_type = RPM_ROLLUP.PL_TO_PZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_PL_TO_PZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.DZ_TO_PZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_DZ_TO_PZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IZ_TO_DZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IZ_TO_DZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IZ_TO_PZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IZ_TO_PZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.DL_TO_DZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_DL_TO_DZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.DL_TO_PL or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_DL_TO_PL(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.DL_TO_PZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_DL_TO_PZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IL_TO_IZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IL_TO_IZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IL_TO_DL or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IL_TO_DL(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IL_TO_DZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IL_TO_DZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IL_TO_PL or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IL_TO_PL(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if L_rollup_type = RPM_ROLLUP.IL_TO_PZ or
      L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then
      ---
      if ROLL_IL_TO_PZ(O_error_msg) = NUMERIC_FALSE then
         return NUMERIC_FALSE;
      end if;
   end if;

   if VALIDATE_INTERSECTION(O_error_msg) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   if PUSH_BACK(O_error_msg) = NUMERIC_FALSE then
      return NUMERIC_FALSE;
   end if;

   update rpm_rfr_rollup_thread
      set status = 'C'
    where thread_number = I_thread_number;

   return NUMERIC_TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return NUMERIC_FALSE;

END ROLL_FUTURE_RETAIL;
--------------------------------------------------------

FUNCTION THREAD_ROLLUP_FR(O_error_msg    OUT VARCHAR2,
                          I_luw       IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_ROLLUP.THREAD_ROLLUP_FR';

   L_bookmark_value     RPM_BATCH_BOOKMARK.BOOKMARK_VALUE%TYPE := NULL;
   L_dept_bookmark      SUBCLASS.DEPT%TYPE                     := NULL;
   L_class_bookmark     SUBCLASS.CLASS%TYPE                    := NULL;
   L_subclass_bookmark  SUBCLASS.SUBCLASS%TYPE                 := NULL;
   L_first_colon        NUMBER                                 := NULL;
   L_second_colon       NUMBER                                 := NULL;
   L_temp_max_thread_no NUMBER(15)                             := 0;
   L_loc_count          NUMBER(10)                             := NULL;
   L_alter_session      VARCHAR2(35)                           := 'alter session enable parallel dml';

   cursor C_LOC_COUNT is
      select COUNT(1)
        from (select distinct rz.zone_group_id,
                     rz.zone_id
                from rpm_merch_retail_def_expl mer,
                     rpm_zone rz
               where rz.zone_group_id = mer.regular_zone_group) t,
             rpm_zone_location rzl
       where rzl.zone_id = t.zone_id
       group by t.zone_group_id
       order by COUNT(1) desc;

   -- cursor for getting the dept/class/subclass combination greater than the current bookmark
   -- change the degree of parallelism depending on the number of CPU's
   cursor C_GET_THREADS is
      select /*+ PARALLEL (r, 4) */
                r.dept,
                r.class,
                r.subclass,
                r.item item_id,
                r.item_type,
                DENSE_RANK() OVER (ORDER BY CEIL(r.item_count * L_loc_count/(I_luw))) thread_number,
                'N' status
           from (select s.dept,
                        s.class,
                        s.subclass,
                        s.item,
                        s.item_type,
                        s.dept_rank,
                        SUM(s.cnt) OVER (ORDER BY dept_rank,
                                                  rank) item_count
                   from (select t.dept,
                                t.class,
                                t.subclass,
                                t.item,
                                t.item_type,
                                t.cnt,
                                DENSE_RANK() OVER (PARTITION BY t.dept
                                                       ORDER BY t.item_type,
                                                                t.cnt,
                                                                t.item) rank,
                                DENSE_RANK() OVER (ORDER BY dept,
                                                            class,
                                                            subclass) dept_rank
                           from (-- Get all Item Parents for Dept/Class/Subclass
                                 select im1.dept,
                                        im1.class,
                                        im1.subclass,
                                        im1.item,
                                        RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_PARENT item_type,
                                        im.cnt
                                   from (select im1.item_parent,
                                                COUNT(1) cnt
                                           from item_master im1
                                          where im1.item_level   = im1.tran_level
                                            and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                            and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                            and im1.item_parent  is NOT NULL
                                            and (   im1.dept             > L_dept_bookmark
                                                 or (    im1.dept        = L_dept_bookmark
                                                     and im1.class       > L_class_bookmark)
                                                 or (    im1.dept        = L_dept_bookmark
                                                     and im1.class       = L_class_bookmark
                                                     and im1.subclass    > L_subclass_bookmark))
                                          group by im1.item_parent) im,
                                         item_master im1
                                  where im1.item = im.item_parent
                                 union all
                                 -- Get all Tran Items with NO Parent for Dept/Class/Subclass
                                 select dept,
                                        class,
                                        subclass,
                                        item,
                                        RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_ITEM item_type,
                                        1 cnt
                                   from item_master
                                  where item_level    = tran_level
                                    and item_parent   is NULL
                                    and status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                    and sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                    and (   dept          >  L_dept_bookmark
                                         or (    dept     = L_dept_bookmark
                                             and class    >  L_class_bookmark)
                                         or (    dept     = L_dept_bookmark
                                             and class    = L_class_bookmark
                                             and subclass > L_subclass_bookmark))) t) s) r;

   TYPE RFR_ROLLUP_THREAD_TBL is table of C_GET_THREADS%ROWTYPE;
   L_rfr_rollup_thread_tbl RFR_ROLLUP_THREAD_TBL := NULL;

BEGIN

   select bookmark_value
     into L_bookmark_value
     from rpm_batch_bookmark
    where batch_name = LP_rfr_rollup_batch;

   -- Get the maximum number of location in the zone group that is a primary zone group
   open C_LOC_COUNT;
   fetch C_LOC_COUNT into L_loc_count;
   close C_LOC_COUNT;

   EXECUTE IMMEDIATE L_alter_session;

   if L_bookmark_value is NOT NULL then
      -- get the dept, class and subclass values based on the bookmark value
      -- the bookmark value is separated by colon, for example: '1234:12:34'
      -- get the 1st position and 2nd position of the colon then get the actual values
      -- of the merch hierarchy values based on the positions of the string separator
      L_first_colon       := INSTR(L_bookmark_value, ':');
      L_second_colon      := INSTR(L_bookmark_value, ':', 1, 2);
      L_dept_bookmark     := TO_NUMBER(SUBSTR(L_bookmark_value, 1, L_first_colon - 1));
      L_class_bookmark    := TO_NUMBER(SUBSTR(L_bookmark_value, L_first_colon + 1, L_second_colon - L_first_colon - 1));
      L_subclass_bookmark := TO_NUMBER(SUBSTR(L_bookmark_value, L_second_colon + 1));

      open C_GET_THREADS;
      fetch C_GET_THREADS BULK COLLECT into L_rfr_rollup_thread_tbl;
      close C_GET_THREADS;

      if L_rfr_rollup_thread_tbl.COUNT > 0 then

         forall i IN 1..L_rfr_rollup_thread_tbl.COUNT
            insert into rpm_rfr_rollup_thread (dept,
                                               class,
                                               subclass,
                                               item_id,
                                               item_type,
                                               thread_number,
                                               status)
              values(L_rfr_rollup_thread_tbl(i).dept,
                     L_rfr_rollup_thread_tbl(i).class,
                     L_rfr_rollup_thread_tbl(i).subclass,
                     L_rfr_rollup_thread_tbl(i).item_id ,
                     L_rfr_rollup_thread_tbl(i).item_type,
                     L_rfr_rollup_thread_tbl(i).thread_number,
                     L_rfr_rollup_thread_tbl(i).status);

         select MAX(thread_number)
           into L_temp_max_thread_no
           from rpm_rfr_rollup_thread;

      end if;

      -- to start on the bookmark and those dept/class/subclass combination less than
      -- and equal the current bookmark
      insert into rpm_rfr_rollup_thread (dept,
                                         class,
                                         subclass,
                                         item_id,
                                         item_type,
                                         thread_number,
                                         status)
         -- change the degree of parallelism depending on the number of CPU's
         select /*+ PARALLEL (r, 4) */
                r.dept,
                r.class,
                r.subclass,
                r.item item_id,
                r.item_type,
                DENSE_RANK() OVER (ORDER BY CEIL(r.item_count * L_loc_count/(I_luw))) + L_temp_max_thread_no,
                'N'
           from (select s.dept,
                        s.class,
                        s.subclass,
                        s.item,
                        s.item_type,
                        s.dept_rank,
                        SUM(s.cnt) OVER (ORDER BY dept_rank,
                                                  rank) item_count
                   from (select t.dept,
                                t.class,
                                t.subclass,
                                t.item,
                                t.item_type,
                                t.cnt,
                                DENSE_RANK() OVER (PARTITION BY t.dept
                                                       ORDER BY t.item_type,
                                                                t.cnt,
                                                                t.item) rank,
                                DENSE_RANK() OVER (ORDER BY dept,
                                                            class,
                                                            subclass) dept_rank
                           from (-- Get all Item Parents for Dept/Class/Subclass
                                 select im1.dept,
                                        im1.class,
                                        im1.subclass,
                                        im1.item,
                                        RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_PARENT item_type,
                                        im.cnt
                                   from (select im1.item_parent,
                                                COUNT(1) cnt
                                           from item_master im1
                                          where im1.item_level   = im1.tran_level
                                            and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                            and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                            and im1.item_parent  is NOT NULL
                                            and (   im1.dept            < L_dept_bookmark
                                                 or (    im1.dept        = L_dept_bookmark
                                                     and im1.class      < L_class_bookmark)
                                                 or (    im1.dept        = L_dept_bookmark
                                                     and im1.class       = L_class_bookmark
                                                     and im1.subclass   <= L_subclass_bookmark))
                                          group by im1.item_parent) im,
                                         item_master im1
                                  where im1.item = im.item_parent
                                 union all
                                 -- Get all Tran Items with NO Parent for Dept/Class/Subclass
                                 select dept,
                                        class,
                                        subclass,
                                        item,
                                        RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_ITEM item_type,
                                        1 cnt
                                   from item_master
                                  where item_level     = tran_level
                                    and item_parent   is NULL
                                    and status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                    and sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                    and (   dept          <  L_dept_bookmark
                                         or (    dept      = L_dept_bookmark
                                             and class    <  L_class_bookmark)
                                         or (    dept      = L_dept_bookmark
                                             and class     = L_class_bookmark
                                             and subclass <= L_subclass_bookmark))) t) s) r;

   else

      -- initial run starting from the least dept/class/subclass combination
      insert into rpm_rfr_rollup_thread (dept,
                                         class,
                                         subclass,
                                         item_id,
                                         item_type,
                                         thread_number,
                                         status)
         -- change the degree of parallelism depending on the number of CPU's
         select /*+ PARALLEL (r, 4) */
                r.dept,
                r.class,
                r.subclass,
                r.item item_id,
                r.item_type,
                DENSE_RANK() OVER (ORDER BY CEIL(r.item_count * L_loc_count/(I_luw))) thread_number,
                'N'
           from (select s.dept,
                        s.class,
                        s.subclass,
                        s.item,
                        s.item_type,
                        s.dept_rank,
                        SUM(s.cnt) OVER (ORDER BY dept_rank,
                                                  rank) item_count
                   from (select t.dept,
                                t.class,
                                t.subclass,
                                t.item,
                                t.item_type,
                                t.cnt,
                                DENSE_RANK() OVER (PARTITION BY t.dept
                                                       ORDER BY t.item_type,
                                                                t.cnt,
                                                                t.item) rank,
                                DENSE_RANK() OVER (ORDER BY dept,
                                                            class,
                                                            subclass) dept_rank
                           from (-- Get all Item Parents for Dept/Class/Subclass
                                 select im1.dept,
                                        im1.class,
                                        im1.subclass,
                                        im1.item,
                                        RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_PARENT item_type,
                                        im.cnt
                                   from (select im1.item_parent,
                                                COUNT(1) cnt
                                           from item_master im1
                                          where im1.item_level   = im1.tran_level
                                            and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                            and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                            and im1.item_parent  is NOT NULL
                                          group by im1.item_parent) im,
                                         item_master im1
                                  where im1.item = im.item_parent
                                 union all
                                 -- Get all Tran Items with NO Parent for Dept/Class/Subclass
                                 select dept,
                                        class,
                                        subclass,
                                        item,
                                        RPM_CONSTANTS.RFR_ROLLUP_ITEM_LEVEL_ITEM item_type,
                                        1 cnt
                                   from item_master
                                  where item_level   = tran_level
                                    and item_parent  is NULL
                                    and status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                    and sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t) s) r;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END THREAD_ROLLUP_FR;

--------------------------------------------------------

FUNCTION ROLL_FUTURE_RETAIL_BY_ITEMS(O_error_msg      IN OUT VARCHAR2,
                                     I_items          IN     OBJ_VARCHAR_ID_TABLE,
                                     I_rollup_type    IN     RPM_FUTURE_RETAIL.DEPT%TYPE DEFAULT RPM_ROLLUP.ALL_TO_ALL)
RETURN NUMBER IS

   L_program  VARCHAR2(50)  := 'RPM_ROLLUP.ROLL_FUTURE_RETAIL_BY_ITEMS';

BEGIN

   delete rpm_me_item_gtt;

   -- Get All Item Parent and All Transaction Level that does not have any parent
   insert into rpm_me_item_gtt
      (dept,
       class,
       subclass,
       item_parent,
       item,
       item_level)
   select distinct
          t.dept,
          t.class,
          t.subclass,
          t.item_parent,
          t.item,
          t.item_level
     from (select im.dept,
                  im.class,
                  im.subclass,
                  im.item,
                  NULL item_parent,
                  RPM_CONSTANTS.ITEM_MERCH_TYPE item_level
             from item_master im,
                  table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids
            where im.item         = value(ids)
              and im.tran_level   = im.item_level
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and im.item_parent  is NULL
            union all
           select im.dept,
                  im.class,
                  im.subclass,
                  im.item,
                  NULL item_parent,
                  RPM_CONSTANTS.PARENT_MERCH_TYPE item_level
             from item_master im,
                  table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids
            where im.item         = value(ids)
              and im.item_level   = im.tran_level - 1
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
           select im.dept,
                  im.class,
                  im.subclass,
                  im.item_parent item,
                  NULL item_parent,
                  RPM_CONSTANTS.PARENT_MERCH_TYPE item_level
             from item_master im,
                  table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids
            where im.item         = value(ids)
              and im.tran_level   = im.item_level
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and im.item_parent  is NOT NULL
              and im.item_level   = im.tran_level - 1) t;

   -- Get All Children from found Item Parent
   insert into rpm_me_item_gtt
      (dept,
       class,
       subclass,
       item_parent,
       item,
       item_level)
   select im.dept,
          im.class,
          im.subclass,
          im.item_parent,
          im.item,
          RPM_CONSTANTS.ITEM_MERCH_TYPE item_level
     from rpm_me_item_gtt gtt,
          item_master im
    where gtt.item_level  = RPM_CONSTANTS.PARENT_MERCH_TYPE
      and im.item_parent  = gtt.item
      and im.tran_level   = im.item_level
      and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   if POP_GTT_AND_ROLL_UP(O_error_msg,
                          I_rollup_type) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END ROLL_FUTURE_RETAIL_BY_ITEMS;

--------------------------------------------------------

FUNCTION ROLL_FUTURE_RETAIL_FOR_NIL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_ROLLUP.ROLL_FUTURE_RETAIL_FOR_NIL';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- All rollup logic is based on GTT data which for NIL processing
   -- is populated by the RPM_GENERATE_ROLLUP_FR_SQL package.
   -- It's expected that at this point in NIL processing, the GTT tables
   -- will only have IL level data on them and that the perm RFR table
   -- will not have any timelines on it that relate to any data NIL
   -- was processing.

   if ROLL_IL_TO_IZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_IL_TO_PL(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_IZ_TO_PZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_PL_TO_PZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_IL_TO_PZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_IL_TO_DL(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_IZ_TO_DZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_DL_TO_DZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_DL_TO_PL(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_DZ_TO_PZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_DL_TO_PZ(O_error_msg) = 0 then
      return 0;
   end if;

   if ROLL_IL_TO_DZ(O_error_msg) = 0 then
      return 0;
   end if;

   if VALIDATE_RETAIL(O_error_msg) = 0 then
      return NUMERIC_FALSE;
   end if;

   if VALIDATE_INTERSECTION(O_error_msg) = 0 then
      return 0;
   end if;

   if PUSH_BACK_FOR_NIL(O_error_msg) = 0 then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END ROLL_FUTURE_RETAIL_FOR_NIL;

--------------------------------------------------------

FUNCTION ROLL_FUTURE_RETAIL_FOR_PZ_UPD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(50)  := 'RPM_ROLLUP.ROLL_FUTURE_RETAIL_FOR_PZ_UPD';

BEGIN

   delete rpm_me_item_gtt;

   insert into rpm_me_item_gtt
      (dept,
       class,
       subclass,
       item_parent,
       item,
       item_level)
   select distinct
          t.dept,
          t.class,
          t.subclass,
          t.item_parent,
          t.item,
          t.item_level
     from (select im.dept,
                  im.class,
                  im.subclass,
                  im.item,
                  NULL item_parent,
                  RPM_CONSTANTS.ITEM_MERCH_TYPE item_level
             from rpm_prim_zone_modifications pzm,
                  item_master im
            where im.dept         = pzm.dept
              and im.class        = NVL(pzm.class, im.class)
              and im.subclass     = NVL(pzm.subclass, im.subclass)
              and im.item_level   = im.tran_level
              and im.item_parent  is NULL
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           select im.dept,
                  im.class,
                  im.subclass,
                  im.item,
                  im.item_parent,
                  RPM_CONSTANTS.ITEM_MERCH_TYPE item_level
             from rpm_prim_zone_modifications pzm,
                  item_master im
            where im.dept         = pzm.dept
              and im.class        = NVL(pzm.class, im.class)
              and im.subclass     = NVL(pzm.subclass, im.subclass)
              and im.item_level   = im.tran_level
              and im.item_parent  is NOT NULL
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           select distinct
                  im.dept,
                  im.class,
                  im.subclass,
                  im.item,
                  NULL item_parent,
                  RPM_CONSTANTS.PARENT_MERCH_TYPE item_level
             from rpm_prim_zone_modifications pzm,
                  item_master im
            where im.dept         = pzm.dept
              and im.class        = NVL(pzm.class, im.class)
              and im.subclass     = NVL(pzm.subclass, im.subclass)
              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.item_level   = im.tran_level - 1) t;

   if POP_GTT_AND_ROLL_UP(O_error_msg) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END ROLL_FUTURE_RETAIL_FOR_PZ_UPD;

--------------------------------------------------------

FUNCTION VALIDATE_INTERSECTION(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(50)  := 'RPM_ROLLUP.VALIDATE_INTERSECTION';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Check any intersection that will generate the DL level timeline
   -- If there is any intersection, do not delete the DL level timeline
   -- Intersection DZ and PL
   delete rpm_price_inquiry_gtt;

   insert into rpm_price_inquiry_gtt
      (price_event_id,
       dept,
       class,
       subclass,
       item,
       diff_id,
       location,
       loc_type,
       item_parent)
   select price_event_id,
          dept,
          class,
          subclass,
          item,
          diff_id,
          location,
          zone_node_type,
          cur_hier_level
     from (select price_event_id,
                  dept,
                  class,
                  subclass,
                  item,
                  diff_id,
                  location,
                  zone_node_type,
                  cur_hier_level,
                  rank() over(partition by price_event_id,
                                           dept,
                                           class,
                                           subclass,
                                           item,
                                           NVL(diff_id, -99999),
                                           location,
                                           zone_node_type
                                  order by action_date) rnk
             from rpm_future_retail_gtt
            where nvl(timeline_seq, 0) != -999) t
    where t.rnk = 1;

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt
      (price_event_id,
       item,
       diff_id,
       location,
       item_parent)
   select distinct
          price_event_id,
          item,
          diff_id,
          location,
          RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level
     from (-- DZ and PL
           select price_event_id,
                  item,
                  diff_id,
                  location,
                  COUNT(1) OVER (PARTITION BY price_event_id,
                                              item,
                                              diff_id,
                                              location) cnt
             from (-- Look at the Parent-Diff/Zone level
                   select distinct
                          t.price_event_id,
                          t.item,
                          t.diff_id,
                          rzl.location
                     from rpm_price_inquiry_gtt t,
                          item_master im,
                          rpm_zone_location rzl,
                          rpm_item_loc ril
                    where t.item_parent = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                      and im.item_parent = t.item
                      and im.diff_1 = t.diff_id
                      and rzl.zone_id = t.location
                      and ril.dept = im.dept
                      and ril.item = im.item
                      and ril.loc  = rzl.location
                    union all
                   -- Look at the Parent/Loc level
                   select distinct
                          t.price_event_id,
                          t.item,
                          im.diff_1 diff_id,
                          t.location
                     from rpm_price_inquiry_gtt t,
                          item_master im,
                          rpm_item_loc ril
                    where t.item_parent = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      and im.item_parent = t.item
                      and ril.dept = im.dept
                      and ril.item = im.item
                      and ril.loc  = t.location))
    where cnt > 1;

   update rpm_cust_segment_promo_fr_gtt target
      set original_cs_promo_id = NULL
    where (target.item,
           target.diff_id,
           target.location) IN (select item,
                                       diff_id,
                                       location
                                  from rpm_item_loc_gtt
                                 where item_parent = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
      and target.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC;

   update rpm_promo_item_loc_expl_gtt target
      set deleted_ind = NULL
    where (target.item,
           target.diff_id,
           target.location) IN (select item,
                                       diff_id,
                                       location
                                  from rpm_item_loc_gtt
                                 where item_parent = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
      and target.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC;

   update rpm_future_retail_gtt target
      set timeline_seq = NULL
    where (target.item,
           target.diff_id,
           target.location) IN (select item,
                                       diff_id,
                                       location
                                  from rpm_item_loc_gtt
                                 where item_parent = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
      and target.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC;

   -- Check any intersection that will generate the IL level timeline
   -- If there is any intersection, do not delete the IL level timeline
   -- Intersection between IZ and PL
   -- Intersection between IZ and DL

   delete rpm_price_inquiry_gtt;

   insert into rpm_price_inquiry_gtt
      (price_event_id,
       dept,
       class,
       subclass,
       item,
       diff_id,
       location,
       loc_type,
       item_parent)
   select price_event_id,
          dept,
          class,
          subclass,
          item,
          diff_id,
          location,
          zone_node_type,
          cur_hier_level
     from (select price_event_id,
                  dept,
                  class,
                  subclass,
                  item,
                  diff_id,
                  location,
                  zone_node_type,
                  cur_hier_level,
                  rank() over(partition by price_event_id,
                                           dept,
                                           class,
                                           subclass,
                                           item,
                                           NVL(diff_id, -99999),
                                           location,
                                           zone_node_type
                                  order by action_date) rnk
             from rpm_future_retail_gtt
            where nvl(timeline_seq, 0) != -999) t
    where t.rnk = 1;

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt
      (price_event_id,
       item,
       location,
       item_parent)
   select distinct
          price_event_id,
          item,
          location,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
     from (-- Find the intersection of PL and IZ
           select price_event_id,
                  item,
                  location,
                  COUNT(1) OVER (PARTITION BY price_event_id,
                                              item,
                                              location) cnt
             from (-- Look at the Parent/Loc level
                   select t.price_event_id,
                          im.item,
                          t.location
                     from rpm_price_inquiry_gtt t,
                          item_master im,
                          rpm_item_loc ril
                    where t.item_parent = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      and im.item_parent = t.item
                      and ril.dept = im.dept
                      and ril.item = im.item
                      and ril.loc  = t.location
                    union all
                   -- Look at the Item/Zone level
                   select t.price_event_id,
                          t.item,
                          rzl.location
                     from rpm_price_inquiry_gtt t,
                          rpm_zone_location rzl,
                          rpm_item_loc ril
                    where t.item_parent = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      and rzl.zone_id = t.location
                      and ril.dept = t.dept
                      and ril.item = t.item
                      and ril.loc  = rzl.location)
            union all
           -- Find the intersection of DL and IZ
           select price_event_id,
                  item,
                  location,
                  COUNT(1) OVER (PARTITION BY price_event_id,
                                              item,
                                              location) cnt
             from (-- Look at the Parent Diff/Loc level
                   select t.price_event_id,
                          im.item,
                          t.location
                     from rpm_price_inquiry_gtt t,
                          item_master im,
                          rpm_item_loc ril
                    where t.item_parent = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                      and im.item_parent = t.item
                      and im.diff_1 = t.diff_id
                      and ril.dept = im.dept
                      and ril.item = im.item
                      and ril.loc  = t.location
                    union all
                   -- Look at the Item/Zone level
                   select t.price_event_id,
                          t.item,
                          rzl.location
                     from rpm_price_inquiry_gtt t,
                          rpm_zone_location rzl,
                          rpm_item_loc ril
                    where t.item_parent = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      and rzl.zone_id = t.location
                      and ril.dept = t.dept
                      and ril.item = t.item
                      and ril.loc  = rzl.location))
    where cnt > 1;

   update rpm_cust_segment_promo_fr_gtt target
      set original_cs_promo_id = NULL
    where (target.item, target.location) in
          (select item, location
             from rpm_item_loc_gtt
            where item_parent = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
      and target.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   update rpm_promo_item_loc_expl_gtt target
      set deleted_ind = NULL
    where (target.item, target.location) in
          (select item, location
             from rpm_item_loc_gtt
            where item_parent = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
      and target.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   update rpm_future_retail_gtt target
      set timeline_seq = NULL
    where (target.item, target.location) in
          (select item, location
             from rpm_item_loc_gtt
            where item_parent = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
      and target.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_INTERSECTION;

--------------------------------------------------------

FUNCTION POP_GTT_AND_ROLL_UP(O_error_msg      OUT VARCHAR2,
                             I_rollup_type IN     RPM_FUTURE_RETAIL.DEPT%TYPE DEFAULT RPM_ROLLUP.ALL_TO_ALL)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_ROLLUP.POP_GTT_AND_ROLL_UP';

   L_dept        NUMBER(4) := NULL;
   L_rollup_type NUMBER(2) := I_rollup_type;

   cursor C_DEPT is
      select distinct dept
        from rpm_me_item_gtt;

BEGIN

   if L_rollup_type is NULL then
      L_rollup_type := RPM_ROLLUP.ALL_TO_ALL;
   end if;

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;

   for rec IN C_DEPT loop
      L_dept := rec.dept;

      insert into rpm_future_retail_gtt (price_event_id,
                                         future_retail_id,
                                         dept,
                                         class,
                                         subclass,
                                         item,
                                         zone_node_type,
                                         location,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         rfr_rowid,
                                         on_simple_promo_ind,
                                         on_complex_promo_ind,
                                         item_parent,
                                         diff_id,
                                         zone_id,
                                         cur_hier_level,
                                         max_hier_level)
         -- Get all records on rpm_future_retail
         select -9999999999,
                fr.future_retail_id,
                fr.dept,
                fr.class,
                fr.subclass,
                fr.item,
                fr.zone_node_type,
                fr.location,
                fr.action_date,
                fr.selling_retail,
                fr.selling_retail_currency,
                fr.selling_uom,
                fr.multi_units,
                fr.multi_unit_retail,
                fr.multi_unit_retail_currency,
                fr.multi_selling_uom,
                fr.clear_retail,
                fr.clear_retail_currency,
                fr.clear_uom,
                fr.simple_promo_retail,
                fr.simple_promo_retail_currency,
                fr.simple_promo_uom,
                fr.price_change_id,
                fr.price_change_display_id,
                fr.pc_exception_parent_id,
                fr.pc_change_type,
                fr.pc_change_amount,
                fr.pc_change_currency,
                fr.pc_change_percent,
                fr.pc_change_selling_uom,
                fr.pc_null_multi_ind,
                fr.pc_multi_units,
                fr.pc_multi_unit_retail,
                fr.pc_multi_unit_retail_currency,
                fr.pc_multi_selling_uom,
                fr.pc_price_guide_id,
                fr.clearance_id,
                fr.clearance_display_id,
                fr.clear_mkdn_index,
                fr.clear_start_ind,
                fr.clear_change_type,
                fr.clear_change_amount,
                fr.clear_change_currency,
                fr.clear_change_percent,
                fr.clear_change_selling_uom,
                fr.clear_price_guide_id,
                fr.loc_move_from_zone_id,
                fr.loc_move_to_zone_id,
                fr.location_move_id,
                fr.lock_version,
                fr.rowid,
                fr.on_simple_promo_ind,
                fr.on_complex_promo_ind,
                fr.item_parent,
                fr.diff_id,
                fr.zone_id,
                fr.cur_hier_level,
                fr.max_hier_level
           from rpm_future_retail fr,
                rpm_me_item_gtt gtt
          where gtt.dept = L_dept
            and fr.dept  = gtt.dept
            and fr.item  = gtt.item;

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               zone_node_type,
                                               item_parent,
                                               diff_id,
                                               zone_id,
                                               cur_hier_level,
                                               max_hier_level)
         select /*+ LEADING(gtt) */
                -9999999999,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                ilex.zone_node_type,
                ilex.item_parent,
                ilex.diff_id,
                ilex.zone_id,
                ilex.cur_hier_level,
                ilex.max_hier_level
           from rpm_promo_item_loc_expl ilex,
                rpm_me_item_gtt gtt
          where gtt.dept  = L_dept
            and ilex.dept = gtt.dept
            and ilex.item = gtt.item;

      insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                                 cust_segment_promo_id,
                                                 item,
                                                 zone_node_type,
                                                 location,
                                                 action_date,
                                                 customer_type,
                                                 dept,
                                                 promo_retail,
                                                 promo_retail_currency,
                                                 promo_uom,
                                                 complex_promo_ind,
                                                 cspfr_rowid,
                                                 item_parent,
                                                 diff_id,
                                                 zone_id,
                                                 cur_hier_level,
                                                 max_hier_level)
         select -9999999999,
                cspfr.cust_segment_promo_id,
                cspfr.item,
                cspfr.zone_node_type,
                cspfr.location,
                cspfr.action_date,
                cspfr.customer_type,
                cspfr.dept,
                cspfr.promo_retail,
                cspfr.promo_retail_currency,
                cspfr.promo_uom,
                cspfr.complex_promo_ind,
                cspfr.rowid,
                cspfr.item_parent,
                cspfr.diff_id,
                cspfr.zone_id,
                cspfr.cur_hier_level,
                cspfr.max_hier_level
           from (select t.item,
                        t.location,
                        t.diff_id,
                        t.zone_node_type
                   from (select gtt.item,
                                gtt.location,
                                gtt.diff_id,
                                gtt.zone_node_type,
                                RANK() OVER (PARTITION BY gtt.item,
                                                          gtt.location,
                                                          NVL(gtt.diff_id, '-999999'),
                                                          gtt.zone_node_type
                                                 ORDER BY gtt.action_date desc) rank
                           from rpm_future_retail_gtt gtt
                          where dept = L_dept) t
                  where t.rank = 1) fr,
                rpm_cust_segment_promo_fr cspfr
          where cspfr.dept                    = L_dept
            and cspfr.item                    = fr.item
            and cspfr.location                = fr.location
            and NVL(cspfr.diff_id, '-999999') = NVL(fr.diff_id, '-999999')
            and cspfr.zone_node_type          = fr.zone_node_type;

      -- Do not change the order of the calls to the rollup functions below.  The rollup for each
      -- level needs to be in the current order to prevent removing lower level timelines incorrectly.
      -- The order needed here is as follows:
      --          PL_TO_PZ
      --          DZ_TO_PZ
      --          IZ_TO_DZ then IZ_TO_PZ
      --          DL_TO_DZ then DL_TO_PL then DL_TO_PZ
      --          IL_TO_IZ then IL_TO_DL then IL_TO_DZ then IL_TO_PL then IL_TO_PZ

      if L_rollup_type = RPM_ROLLUP.PL_TO_PZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_PL_TO_PZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.DZ_TO_PZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_DZ_TO_PZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

     if L_rollup_type = RPM_ROLLUP.IZ_TO_DZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IZ_TO_DZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.IZ_TO_PZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IZ_TO_PZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.DL_TO_DZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_DL_TO_DZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.DL_TO_PL or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_DL_TO_PL(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.DL_TO_PZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_DL_TO_PZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.IL_TO_IZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IL_TO_IZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.IL_TO_DL or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IL_TO_DL(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.IL_TO_DZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IL_TO_DZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.IL_TO_PL or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IL_TO_PL(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

      if L_rollup_type = RPM_ROLLUP.IL_TO_PZ or
         L_rollup_type = RPM_ROLLUP.ALL_TO_ALL then

         if ROLL_IL_TO_PZ(O_error_msg) = NUMERIC_FALSE then
            return NUMERIC_FALSE;
         end if;
      end if;

   end loop;

   if VALIDATE_INTERSECTION(O_error_msg) = 0 then
      return 0;
   end if;

   if PUSH_BACK(O_error_msg) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END POP_GTT_AND_ROLL_UP;

--------------------------------------------------------
FUNCTION CLEAN_UP(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(20) := 'RPM_ROLLUP.CLEAN_UP';

   L_max_thread_number          NUMBER(15)             := NULL;
   L_dept                       SUBCLASS.DEPT%TYPE     := NULL;
   L_class                      SUBCLASS.CLASS%TYPE    := NULL;
   L_subclass                   SUBCLASS.SUBCLASS%TYPE := NULL;
   L_truncate_rfr_rollup_thread VARCHAR2(40)           := 'truncate table rpm_rfr_rollup_thread';

BEGIN

   select MAX(thread_number)
     into L_max_thread_number
     from rpm_rfr_rollup_thread
    where status = 'C';

   -- get the dept, class and subclass with
   -- the maximum thread number and complete status for bookmark
   if L_max_thread_number is NOT NULL then
      select dept,
             class,
             subclass
        into L_dept,
             L_class,
             L_subclass
        from (select dept,
                     class,
                     subclass,
                     ROW_NUMBER() OVER (ORDER BY dept desc,
                                                 class desc,
                                                 subclass desc) rn
                from rpm_rfr_rollup_thread
               where thread_number = L_max_thread_number)
       where rn = 1;
   end if;

   if L_dept is NOT NULL then
      update rpm_batch_bookmark
         set bookmark_value = L_dept || ':' || L_class || ':' || L_subclass
       where batch_name = LP_rfr_rollup_batch;
   end if;

   EXECUTE IMMEDIATE L_truncate_rfr_rollup_thread;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END CLEAN_UP;
--------------------------------------------------------
FUNCTION PUSH_BACK_FOR_NIL(O_error_msg   OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_ROLLUP.PUSH_BACK_FOR_NIL';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into rpm_future_retail
      (future_retail_id,
       item,
       dept,
       class,
       subclass,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_exception_parent_id,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       on_simple_promo_ind,
       on_complex_promo_ind,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       item_parent,
       diff_id,
       zone_id,
       max_hier_level,
       cur_hier_level)
      select future_retail_id,
             item,
             dept,
             class,
             subclass,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_exception_parent_id,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             on_simple_promo_ind,
             on_complex_promo_ind,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level
        from rpm_future_retail_gtt
       where NVL(timeline_seq, 1) != -999;

   insert into rpm_promo_item_loc_expl
      (promo_item_loc_expl_id,
       item,
       dept,
       class,
       subclass,
       location,
       zone_node_type,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       promo_comp_id,
       comp_display_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       exception_parent_id,
       item_parent,
       diff_id,
       zone_id,
       max_hier_level,
       cur_hier_level,
       timebased_dtl_ind)
      select promo_item_loc_expl_id,
             item,
             dept,
             class,
             subclass,
             location,
             zone_node_type,
             promo_id,
             promo_display_id,
             promo_secondary_ind,
             promo_comp_id,
             comp_display_id,
             promo_dtl_id,
             type,
             customer_type,
             detail_secondary_ind,
             detail_start_date,
             detail_end_date,
             detail_apply_to_code,
             detail_change_type,
             detail_change_amount,
             detail_change_currency,
             detail_change_percent,
             detail_change_selling_uom,
             detail_price_guide_id,
             exception_parent_id,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level,
             timebased_dtl_ind
        from rpm_promo_item_loc_expl_gtt
       where NVL(deleted_ind, 0) != 1;

   insert into rpm_cust_segment_promo_fr
      (cust_segment_promo_id,
       item,
       zone_node_type,
       location,
       action_date,
       customer_type,
       dept,
       promo_retail,
       promo_retail_currency,
       promo_uom,
       complex_promo_ind,
       item_parent,
       diff_id,
       zone_id,
       max_hier_level,
       cur_hier_level)
      select cust_segment_promo_id,
             item,
             zone_node_type,
             location,
             action_date,
             customer_type,
             dept,
             promo_retail,
             promo_retail_currency,
             promo_uom,
             complex_promo_ind,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level
        from rpm_cust_segment_promo_fr_gtt
       where NVL(original_cs_promo_id, 0) != -999;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END PUSH_BACK_FOR_NIL;
--------------------------------------------------------

FUNCTION VALIDATE_RETAIL(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_ROLLUP.VALIDATE_RETAIL';

   L_item_locs  OBJ_NUM_NUM_STR_TBL := NULL;
   L_start_time TIMESTAMP           := SYSTIMESTAMP;

   cursor C_RPM_ITEM_LOC_GTT is
      select OBJ_NUM_NUM_STR_REC(NULL,
                                 location,
                                 item)
        from (select distinct il.item,
                     il.location
                from rpm_future_retail_gtt gtt,
                     rpm_fr_item_loc_expl_gtt il
               where gtt.dept            = il.dept
                 and gtt.item            = il.item
                 and gtt.location        = il.location
                 and gtt.zone_node_type  = il.zone_node_type
                 and gtt.action_date     = il.action_date
                 and gtt.selling_retail != il.selling_retail);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete from rpm_price_inquiry_gtt;
   delete from rpm_fr_item_loc_expl_gtt;

   -- Get all the 'IL' timelines marked for deletion
   insert into rpm_price_inquiry_gtt
      (price_event_id,
       dept,
       class,
       subclass,
       item,
       diff_id,
       item_parent,
       location,
       fr_zone_id,
       fr_zone_node_type)
    select price_event_id,
          dept,
          class,
          subclass,
          item,
          diff_id,
          item_parent,
          location,
          zone_id,
          zone_node_type
     from (select price_event_id,
                  dept,
                  class,
                  subclass,
                  item,
                  diff_id,
                  item_parent,
                  location,
                  zone_id,
                  zone_node_type,
                  RANK() OVER (PARTITION BY price_event_id,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            NVL(diff_id, '-99999'),
                                            location,
                                            zone_node_type
                                   ORDER BY action_date) rnk
             from rpm_future_retail_gtt
            where cur_hier_level       = RPM_CONSTANTS.FR_HIER_ITEM_LOC
              and NVL(timeline_seq, 0) = -999)t
    where t.rnk = 1;

   -- For all the 'IL' timelines marked for deletion, generate the 'IL' timeline from the higher level timelines that are not marked for deletion
   insert into rpm_fr_item_loc_expl_gtt(price_event_id,
                                        future_retail_id,
                                        item,
                                        dept,
                                        class,
                                        subclass,
                                        zone_node_type,
                                        location,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id)
      select in2.price_event_id,
             in2.future_retail_id,
             in2.il_item,
             in2.dept,
             in2.class,
             in2.subclass,
             in2.il_loc_type,
             in2.il_loc,
             in2.action_date,
             in2.selling_retail,
             in2.selling_retail_currency,
             in2.selling_uom,
             in2.multi_units,
             in2.multi_unit_retail,
             in2.multi_unit_retail_currency,
             in2.multi_selling_uom,
             in2.clear_retail,
             in2.clear_retail_currency,
             in2.clear_uom,
             in2.simple_promo_retail,
             in2.simple_promo_retail_currency,
             in2.simple_promo_uom,
             in2.price_change_id,
             in2.price_change_display_id,
             in2.pc_exception_parent_id,
             in2.pc_change_type,
             in2.pc_change_amount,
             in2.pc_change_currency,
             in2.pc_change_percent,
             in2.pc_change_selling_uom,
             in2.pc_null_multi_ind,
             in2.pc_multi_units,
             in2.pc_multi_unit_retail,
             in2.pc_multi_unit_retail_currency,
             in2.pc_multi_selling_uom,
             in2.pc_price_guide_id,
             in2.clearance_id,
             in2.clearance_display_id,
             in2.clear_mkdn_index,
             in2.clear_start_ind,
             in2.clear_change_type,
             in2.clear_change_amount,
             in2.clear_change_currency,
             in2.clear_change_percent,
             in2.clear_change_selling_uom,
             in2.clear_price_guide_id,
             in2.loc_move_from_zone_id,
             in2.loc_move_to_zone_id,
             in2.location_move_id
        from (select in1.future_retail_id,
                     in1.item,
                     in1.dept,
                     in1.class,
                     in1.subclass,
                     in1.zone_node_type,
                     in1.location,
                     in1.action_date,
                     in1.selling_retail,
                     in1.selling_retail_currency,
                     in1.selling_uom,
                     in1.multi_units,
                     in1.multi_unit_retail,
                     in1.multi_unit_retail_currency,
                     in1.multi_selling_uom,
                     in1.clear_exception_parent_id,
                     in1.clear_retail,
                     in1.clear_retail_currency,
                     in1.clear_uom,
                     in1.simple_promo_retail,
                     in1.simple_promo_retail_currency,
                     in1.simple_promo_uom,
                     in1.on_simple_promo_ind,
                     in1.on_complex_promo_ind,
                     in1.price_change_id,
                     in1.price_change_display_id,
                     in1.pc_exception_parent_id,
                     in1.pc_change_type,
                     in1.pc_change_amount,
                     in1.pc_change_currency,
                     in1.pc_change_percent,
                     in1.pc_change_selling_uom,
                     in1.pc_null_multi_ind,
                     in1.pc_multi_units,
                     in1.pc_multi_unit_retail,
                     in1.pc_multi_unit_retail_currency,
                     in1.pc_multi_selling_uom,
                     in1.pc_price_guide_id,
                     in1.clearance_id,
                     in1.clearance_display_id,
                     in1.clear_mkdn_index,
                     in1.clear_start_ind,
                     in1.clear_change_type,
                     in1.clear_change_amount,
                     in1.clear_change_currency,
                     in1.clear_change_percent,
                     in1.clear_change_selling_uom,
                     in1.clear_price_guide_id,
                     in1.loc_move_from_zone_id,
                     in1.loc_move_to_zone_id,
                     in1.location_move_id,
                     in1.lock_version,
                     in1.item_parent,
                     in1.diff_id,
                     in1.max_hier_level,
                     in1.cur_hier_level,
                     in1.fr_zone_id,
                     in1.price_event_id,
                     in1.il_item,
                     in1.il_item_parent,
                     in1.il_diff_id,
                     in1.il_loc,
                     in1.il_loc_type,
                     in1.hier_rank,
                     MAX(in1.hier_rank) OVER (PARTITION BY in1.il_item,
                                                           in1.il_loc) max_rank
                from (select f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.zone_id fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.fr_zone_node_type il_loc_type,
                             3 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail_gtt f
                       where f.dept                  = il.dept
                         and f.item                  = il.item
                         and NVL(f.diff_id, '-9999') = NVL(il.diff_id, '-9999')
                         and f.location              = il.location
                         and f.zone_node_type        = il.fr_zone_node_type
                         and f.cur_hier_level        = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                         and NVL(f.timeline_seq, 0) != -999
                      union all
                      select f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.location fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.fr_zone_node_type il_loc_type,
                             2 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail_gtt f
                       where f.dept                  = il.dept
                         and f.item                  = il.item
                         and NVL(f.diff_id, '-9999') = NVL(il.diff_id, '-9999')
                         and f.location              = il.fr_zone_id
                         and f.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and f.cur_hier_level        = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                         and NVL(f.timeline_seq, 0) != -999
                      union all
                      select f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.zone_id fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.fr_zone_node_type il_loc_type,
                             2 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail_gtt f
                       where f.dept                  = il.dept
                         and f.item                  = il.item_parent
                         and NVL(f.diff_id, '-9999') = NVL(il.diff_id, '-9999')
                         and f.location              = il.location
                         and f.zone_node_type        = il.fr_zone_node_type
                         and f.cur_hier_level        = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                         and NVL(f.timeline_seq, 0) != -999
                      union all
                      select f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.location fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.fr_zone_node_type il_loc_type,
                             1 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail_gtt f
                       where f.dept                  = il.dept
                         and f.item                  = il.item_parent
                         and NVL(f.diff_id, '-9999') = NVL(il.diff_id, '-9999')
                         and f.location              = il.fr_zone_id
                         and f.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and f.cur_hier_level        = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and NVL(f.timeline_seq, 0) != -999
                      union all
                      select f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.zone_id fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.fr_zone_node_type il_loc_type,
                             1 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail_gtt f
                       where f.dept                  = il.dept
                         and f.item                  = il.item_parent
                         and f.diff_id               is NULL
                         and f.location              = il.location
                         and f.zone_node_type        = il.fr_zone_node_type
                         and f.cur_hier_level        = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                         and NVL(f.timeline_seq, 0) != -999
                      union all
                      select f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.location fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.fr_zone_node_type il_loc_type,
                             0 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail_gtt f
                       where f.dept                  = il.dept
                         and f.item                  = il.item_parent
                         and f.diff_id               is NULL
                         and f.location              = il.fr_zone_id
                         and f.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and f.cur_hier_level        = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                         and NVL(f.timeline_seq, 0) != -999) in1) in2
       where in2.hier_rank = in2.max_rank;

   -- Get all the item/locations for which the higher level timleines does not generate correct selling retail
   open C_RPM_ITEM_LOC_GTT;
   fetch C_RPM_ITEM_LOC_GTT BULK COLLECT into L_item_locs;
   close C_RPM_ITEM_LOC_GTT;

   forall i IN 1..L_item_locs.COUNT
      update rpm_cust_segment_promo_fr_gtt
         set original_cs_promo_id = NULL
       where item           = L_item_locs(i).string
         and location       = L_item_locs(i).number_2
         and cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   forall i IN 1..L_item_locs.COUNT
      update rpm_promo_item_loc_expl_gtt
         set deleted_ind = NULL
       where item           = L_item_locs(i).string
         and location       = L_item_locs(i).number_2
         and cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   forall i IN 1..L_item_locs.COUNT
      update rpm_future_retail_gtt
         set timeline_seq = NULL
       where item           = L_item_locs(i).string
         and location       = L_item_locs(i).number_2
         and cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_RETAIL;
--------------------------------------------------------
FUNCTION GET_MAX_THREAD_NUMBER(O_error_msg            OUT VARCHAR2,
                               O_max_thread_number    OUT NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_ROLLUP.GET_MAX_THREAD_NUMBER';

BEGIN

   select MAX(thread_number)
     into O_max_thread_number
     from rpm_rfr_rollup_thread;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return 0;

END GET_MAX_THREAD_NUMBER;
--------------------------------------------------------
END RPM_ROLLUP;
/
