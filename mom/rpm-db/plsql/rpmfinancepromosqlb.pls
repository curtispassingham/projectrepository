CREATE OR REPLACE PACKAGE BODY RPM_FINANCE_PROMO_SQL AS
----------------------------------------------------------------------------------
FUNCTION PROCESS_PRICE_EVENTS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FINANCE_PROMO_SQL.PROCESS_PRICE_EVENTS';

   cursor C_CHECK_OVERLAP is
      select new CONFLICT_CHECK_ERROR_REC(rpd.promo_dtl_id,
                                          rpd1.promo_dtl_id,
                                          RPM_CONSTANTS.CONFLICT_ERROR,
                                          'overlap_finance_detail')
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl rpd1,
             rpm_promo_credit_dtl rpcd,
             rpm_promo_credit_dtl rpcd1
       where rpcd.promo_dtl_id     =  value(ids)
         and rpd.promo_dtl_id      =  rpcd.promo_dtl_id
         and rpcd1.promo_dtl_id    != value(ids)
         and rpcd1.promo_dtl_id    =  rpd1.promo_dtl_id
         and rpd1.state            IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpcd.financial_dtl_id = rpcd1.financial_dtl_id
         and (  (   rpd.start_date between rpd1.start_date and rpd1.end_date
                 or rpd.end_date between rpd1.start_date and rpd1.end_date)
             or (   rpd1.start_date between rpd.start_date and rpd.end_date
                 or rpd1.end_date between rpd.start_date and rpd.end_date));

BEGIN

   open C_CHECK_OVERLAP;
   fetch C_CHECK_OVERLAP BULK COLLECT INTO O_cc_error_tbl;
   close C_CHECK_OVERLAP;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PRICE_EVENTS;
----------------------------------------------------------------------------------------------

FUNCTION PROCESS_UPD_FIN_PROMO_EVENTS(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                                      I_new_promo_end_date IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_FINANCE_PROMO_SQL.PROCESS_UPD_FIN_PROMO_EVENTS';

   cursor C_CHECK_OVERLAP is
      select new CONFLICT_CHECK_ERROR_REC(rpd.promo_dtl_id,
                                          rpd1.promo_dtl_id,
                                          RPM_CONSTANTS.CONFLICT_ERROR,
                                          'overlap_finance_detail')
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl rpd1,
             rpm_promo_credit_dtl rpcd,
             rpm_promo_credit_dtl rpcd1
       where rpcd.promo_dtl_id     =  value(ids)
         and rpd.promo_dtl_id      =  rpcd.promo_dtl_id
         and rpcd1.promo_dtl_id   != value(ids)
         and rpcd1.promo_dtl_id    =  rpd1.promo_dtl_id
         and rpd1.state            IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                       RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpcd.financial_dtl_id = rpcd1.financial_dtl_id
         and (  (   rpd.start_date between rpd1.start_date and rpd1.end_date
                 or I_new_promo_end_date between rpd1.start_date and rpd1.end_date)
             or (   rpd1.start_date between rpd.start_date and rpd.end_date
                 or I_new_promo_end_date between rpd.start_date and rpd.end_date));

BEGIN

   open C_CHECK_OVERLAP;
   fetch C_CHECK_OVERLAP BULK COLLECT INTO O_cc_error_tbl;
   close C_CHECK_OVERLAP;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_UPD_FIN_PROMO_EVENTS;
------------------------------------------------------------------------------------------

FUNCTION UPDATE_FINANCE_PROMOTION(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                   I_rib_trans_id    IN     NUMBER,
                                   I_persist_ind     IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FINANCE_PROMO_SQL.UPDATE_FINANCE_PROMOTION';

   L_conflict_error_tbl  CONFLICT_CHECK_ERROR_TBL := NULL;
   L_nc_price_event_ids  OBJ_NUMERIC_ID_TABLE     := NULL;
   L_vdate               DATE                     := GET_VDATE;
   L_new_promo_end_date  DATE                     := NULL;

   cursor C_NO_CONFLICT_PC_IDS is
      select price_change_id
        from (select value(ids) price_change_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
               minus
              select distinct price_event_id price_change_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_GET_NEW_END_DATE is
      select end_date
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where promo_dtl_id = value(ids);

BEGIN

   open C_GET_NEW_END_DATE;
   fetch C_GET_NEW_END_DATE into L_new_promo_end_date;
   close C_GET_NEW_END_DATE;

   if RPM_FINANCE_PROMO_SQL.PROCESS_UPD_FIN_PROMO_EVENTS(O_cc_error_tbl,
                                                         I_price_event_ids,
                                                         L_new_promo_end_date) = 0 then
         return 0;
   end if;

   open C_NO_CONFLICT_PC_IDS;
   fetch C_NO_CONFLICT_PC_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_PC_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      if I_persist_ind = 'Y' then
         if RPM_CC_PUBLISH.STAGE_FINANCE_PROM_MESSAGES(O_cc_error_tbl,
                                                       I_rib_trans_id,
                                                       L_nc_price_event_ids,
                                                       RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD) = 0 then
            return 0;
         end if;
      elsif I_persist_ind = 'Y' then
         if RPM_CC_PUBLISH.STAGE_FIN_REMOVE_PROM_MESSAGES(O_cc_error_tbl,
                                                          I_rib_trans_id,
                                                          L_nc_price_event_ids,
                                                          RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD) = 0 then
            return 0;
         end if;
      end if;

      update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl target
         set end_date  = L_new_promo_end_date
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 1000) */ promo_dtl_id
                                from rpm_promo_dtl
                               where exception_parent_id in (select value(ids)
                                                               from (table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids)));
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        conflict_check_error_rec(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_FINANCE_PROMOTION;
--------------------------------------------------------------------------------
END RPM_FINANCE_PROMO_SQL;
/