CREATE OR REPLACE PACKAGE BODY RPM_APP_COMM_UTILITY_SQL AS
--------------------------------------------------------------------------------

PROCEDURE LOG_THIS(O_return_code     OUT NUMBER,
                   O_error_msg       OUT VARCHAR2,
                   I_in_bu        IN     VARCHAR2,
                   I_in_prod      IN     VARCHAR2,
                   I_in_batch     IN     VARCHAR2,
                   I_in_module    IN     VARCHAR2,
                   I_in_log_type  IN     VARCHAR2,
                   I_in_log_text  IN     VARCHAR2 DEFAULT NULL,
                   I_in_nbr_trans IN     NUMBER   DEFAULT NULL)
AS PRAGMA AUTONOMOUS_TRANSACTION;

   L_program 	  VARCHAR2(35) := 'RPM_APP_COMM_UTILITY_SQL.LOG_THIS';
   L_batch_name VARCHAR2(10) := NULL;

   L_start_time TIMESTAMP := NULL;
   L_end_time   TIMESTAMP := NULL;

BEGIN

   if I_in_log_type = RPM_CONSTANTS.LOG_STARTED then
      L_start_time := SYSTIMESTAMP;
   end if;

   if I_in_log_type = RPM_CONSTANTS.LOG_COMPLETED then
      L_end_time := SYSTIMESTAMP;
   end if;

   --getting the proper batch name
   case  
		when UPPER(I_in_batch) LIKE '%NEW%' then 
		   L_batch_name := 'NIL-'||REGEXP_SUBSTR(I_in_batch, '[^-]+', 1, 2);
		when UPPER(I_in_batch) LIKE '%LOC%' then 
		   L_batch_name := 'LM-'||REGEXP_SUBSTR(I_in_batch, '[^-]+', 1, 2);
		else 
		   L_batch_name := I_in_batch;
   end case;
   
   -- logging the batch step into batch_log table
   insert into rpm_batch_log (sid,
                              session_id,
                              product_bu,
                              product,
                              batch_name,
                              module,
                              start_time,
                              end_time,
                              transaction_count,
                              log_text,
                              status,
                              log_time)
      values (SYS_CONTEXT('USERENV', 'SID'),
              SYS_CONTEXT('USERENV', 'SESSIONID'),
              I_in_bu,
              I_in_prod,
              L_batch_name,
              I_in_module,
              L_start_time,
              L_end_time,
              I_in_nbr_trans,
              I_in_log_text,
              I_in_log_type,
              SYSTIMESTAMP);

   commit;

   O_return_code := 1;

EXCEPTION

   when OTHERS then

      BEGIN

         O_error_msg := SUBSTR(SQLERRM, 1,2000);

         insert into rpm_batch_log (sid,
                                    session_id,
                                    product_bu,
                                    product,
                                    batch_name,
                                    module,
                                    log_text,
                                    status,
                                    log_time)
            values (SYS_CONTEXT('USERENV', 'SID'),
                    SYS_CONTEXT('USERENV', 'SESSIONID'),
                    RPM_CONSTANTS.LOG_ERROR,
                    RPM_CONSTANTS.LOG_ERROR,
                    RPM_CONSTANTS.LOG_ERROR,
                    RPM_CONSTANTS.LOG_ERROR,
                    'Failure in batch log process: '||O_error_msg,
                    RPM_CONSTANTS.LOG_ERROR,
                    SYSTIMESTAMP);

         commit;

         o_return_code := 1; -- do not interrupt application

   EXCEPTION

      when OTHERS then

         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));

   END;

END LOG_THIS;

--------------------------------------------------------------------------------
PROCEDURE ENABLE_TRACE (I_in_batch       IN     VARCHAR2,
                        I_in_trace_level IN     NUMBER DEFAULT 8) AS

   L_enable_trace RPM_BATCH_TRACE.ENABLE_TRACE%TYPE := NULL;
   L_sid          VARCHAR2(10)                      := NULL;
   L_batch_name   VARCHAR2(10)                      := NULL;

   cursor C_ENABLE_TRACE is
      select NVL(UPPER(enable_trace), 'N')
        from rpm_batch_trace
       where UPPER(I_in_batch) LIKE '%'||UPPER(batch_name) ||'%';

BEGIN

   --fetching the sid
   L_sid := SYS_CONTEXT('USERENV','SESSIONID');
   
   case  
		when UPPER(I_in_batch) LIKE '%NEW%' then 
		   L_batch_name := 'NIL-'||REGEXP_SUBSTR(I_in_batch, '[^-]+', 1, 2);
		when UPPER(I_in_batch) LIKE '%LOC%' then 
		   L_batch_name := 'LM-'||REGEXP_SUBSTR(I_in_batch, '[^-]+', 1, 2);
		else 
		   L_batch_name := I_in_batch;
   end case;   

   open C_ENABLE_TRACE;
   fetch C_ENABLE_TRACE into L_enable_trace;
   close C_ENABLE_TRACE;

   if L_enable_trace = 'Y' then

      EXECUTE immediate 'ALTER SESSION SET TRACEFILE_IDENTIFIER='''||L_batch_name||'''';
      EXECUTE immediate 'ALTER SESSION SET EVENTS ''10046 TRACE NAME CONTEXT forever, LEVEL '||I_in_trace_level||'''';
      EXECUTE immediate 'ALTER SESSION SET timed_statistics = TRUE';

   end if;

EXCEPTION

   when OTHERS then
      DBMS_OUTPUT.PUT_LINE(SQLERRM); -- DO NOT INTERRUPT APPLICATION

END ENABLE_TRACE;

-----------------------------------------------------------------------------

END RPM_APP_COMM_UTILITY_SQL;
/