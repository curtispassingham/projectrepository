CREATE OR REPLACE PACKAGE RPM_PRICE_CHANGE_SQL AS

-----------------------------------------------------------------------------
PROCEDURE SEARCH(O_return_code         OUT NUMBER,
                 O_error_msg           OUT VARCHAR2,
                 O_pc_rec              OUT OBJ_PC_TBL,
                 O_pc_count            OUT NUMBER,
                 O_max_recs            OUT NUMBER,
                 I_ui_workflow_id   IN     VARCHAR2,
                 I_pc_criterias     IN     OBJ_PRICE_CHANGE_SEARCH_TBL,
                 I_page_sort_filter IN     OBJ_PAGE_SORT_FILTER_TBL,
                 I_filter_result    IN     NUMBER       DEFAULT 0);

-----------------------------------------------------------------------------
PROCEDURE SEARCH_BY_IDS(O_return_code           OUT NUMBER,
                        O_error_msg             OUT VARCHAR2,
                        O_pc_rec                OUT OBJ_PC_TBL,
                        O_pc_count              OUT NUMBER,
                        I_search_workflow_id IN     VARCHAR2,
                        I_ui_workflow_id     IN     VARCHAR2,
                        I_clearance_ind      IN     NUMBER,
                        I_pc_criterias       IN     OBJ_PRICE_CHANGE_SEARCH_TBL,
                        I_page_sort_filter   IN     OBJ_PAGE_SORT_FILTER_TBL,
                        I_refresh_ind        IN     NUMBER);

-----------------------------------------------------------------------------
PROCEDURE REFRESH_STATUS(O_return_code      OUT NUMBER,
                         O_error_msg        OUT VARCHAR2,
                         O_pc_rec_tbl       OUT OBJ_PC_TBL,
                         I_clearance_ind IN     NUMBER,
                         I_pc_criterias  IN     OBJ_PRICE_CHANGE_SEARCH_TBL);

-----------------------------------------------------------------------------
PROCEDURE GET_FILTERED_OBJECT(O_return_code         OUT NUMBER,
                              O_error_msg           OUT VARCHAR2,
                              O_pc_rec_tbl          OUT OBJ_PC_TBL,
                              O_pc_count            OUT NUMBER,
                              I_ui_workflow_id   IN     VARCHAR2,
                              I_page_sort_filter IN     OBJ_PAGE_SORT_FILTER_TBL);
-----------------------------------------------------------------------------
PROCEDURE RESET_WORKSPACE(O_return_code       OUT NUMBER,
                          O_error_msg         OUT VARCHAR2,
                          I_ui_workflow_id IN     VARCHAR2);
-----------------------------------------------------------------------------
PROCEDURE DELETE_PRICE_CHANGE(O_return_code         OUT NUMBER,
                              O_error_msg           OUT VARCHAR2,
                              I_ui_workflow_id   IN     VARCHAR2,
                              I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE);
-----------------------------------------------------------------------------
PROCEDURE GET_NEW_OR_MODIFIED(O_return_code         OUT NUMBER,
                              O_error_msg           OUT VARCHAR2,
                              O_pc_rec_tbl          OUT OBJ_PC_TBL,
                              I_ui_workflow_id   IN     VARCHAR2,
                              I_new_modified_ind IN     NUMBER);
-----------------------------------------------------------------------------
PROCEDURE CREATE_PC(O_return_code       OUT NUMBER,
                    O_error_msg         OUT VARCHAR2,
                    O_pc_rec_tbl        OUT OBJ_PC_TBL,
                    O_pc_count          OUT NUMBER,
                    I_ui_workflow_id IN     VARCHAR2,
                    I_pc_rec_tbl     IN     OBJ_PC_TBL,
                    I_ca_rec_tbl     IN     OBJ_CUST_ATTR_TBL);
-----------------------------------------------------------------------------
PROCEDURE UPDATE_PC(O_return_code       OUT NUMBER,
                    O_error_msg         OUT VARCHAR2,
                    O_pc_rec_tbl        OUT OBJ_PC_TBL,
                    I_ui_workflow_id IN     VARCHAR2,
                    I_pc_rec_tbl     IN     OBJ_PC_TBL,
                    I_ca_rec_tbl     IN     OBJ_CUST_ATTR_TBL);
-----------------------------------------------------------------------------
PROCEDURE GET_EXPANDED_VIEW(O_return_code       OUT NUMBER,
                            O_error_msg         OUT VARCHAR2,
                            O_pc_rec_tbl        OUT OBJ_PC_TBL,
                            I_pc_change_id   IN     OBJ_NUMERIC_ID_TABLE,
                            I_ui_workflow_id IN     VARCHAR2);
-------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_WITH_GUIDE(I_change_type             IN     NUMBER,
                                I_change_amt              IN     NUMBER,
                                I_retail                  IN     NUMBER,
                                I_change_pct              IN     NUMBER,
                                I_price_guide_id          IN     NUMBER,
                                I_selling_retail_currency IN     VARCHAR2,
                                I_dept_id                 IN     NUMBER,
                                I_clear_ind               IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION CURRENCY_CONVERT_VALUE (I_output_currency IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_input_value     IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_input_currency  IN     CURRENCIES.CURRENCY_CODE%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION UOM_CONVERT_VALUE (I_item        IN     ITEM_MASTER.ITEM%TYPE,
                            I_input_value IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                            I_from_uom    IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                            I_to_uom      IN     ITEM_MASTER.STANDARD_UOM%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION PROPOSE_DATE (O_error_msg      IN OUT VARCHAR2,
                       O_suggested_date IN OUT DATE,
                       I_item           IN     VARCHAR2,
                       I_diff_id        IN     VARCHAR2,
                       I_link_code      IN     NUMBER,
                       I_zone_node_id   IN     NUMBER,
                       I_zone_node_type IN     NUMBER,
                       I_effective_date IN     DATE)
RETURN NUMBER;
-------------------------------------------------------------------------------
PROCEDURE PURGE_PAST_PC (O_return_code    OUT NUMBER,
                         O_error_msg      OUT VARCHAR2,
                         O_number         OUT NUMBER);

-------------------------------------------------------------------------------
END RPM_PRICE_CHANGE_SQL;
/

