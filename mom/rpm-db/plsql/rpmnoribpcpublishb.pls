CREATE OR REPLACE PACKAGE BODY RPM_NO_RIB_PRICE_PUBLISH_SQL AS

--------------------------------------------------------
FIELD_DELIMITER     CONSTANT   VARCHAR2 (1) := '|';
--------------------------------------------------------
FUNCTION SETUP_PC_MESSAGES_THREAD(O_error_msg     OUT VARCHAR2,
                                  I_thread_luw IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION SETUP_CL_MESSAGES_THREAD(O_error_msg     OUT VARCHAR2,
                                  I_thread_luw IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION SETUP_PR_MESSAGES_THREAD(O_error_msg     OUT VARCHAR2,
                                  I_thread_luw IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION CREATE_PC_MESSAGES(O_error_msg     OUT VARCHAR2,
                            I_thread_num IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION CREATE_CL_MESSAGES(O_error_msg     OUT VARCHAR2,
                            I_thread_num IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION CREATE_PR_MESSAGES(O_error_msg     OUT VARCHAR2,
                            I_thread_num IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION CLEANUP_PC_PAYLOAD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION CLEANUP_CL_PAYLOAD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION CLEANUP_PR_PAYLOAD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION SETUP_MESSAGES_THREAD(O_error_msg       OUT VARCHAR2,
                               O_thread_num      OUT NUMBER,
                               I_event_family IN     VARCHAR2,
                               I_thread_luw   IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.SETUP_MESSAGES_THREAD';

   cursor C_SET_THREAD_NUM is
      select MAX(thread_number)
        from rpm_price_publish_stage
       where event_family = I_event_family;

BEGIN

   delete
     from rpm_price_publish_stage
    where event_family = I_event_family;

   delete
     from rpm_price_publish_data
    where event_family = I_event_family;

   delete
     from rpm_price_publish_location
    where event_family = I_event_family;

   if I_event_family = RPM_CONSTANTS.RIB_MSG_FAM_REG_PC then

      if SETUP_PC_MESSAGES_THREAD(O_error_msg,
                                  I_thread_luw) = 0 then
         return 0;
      end if;

   elsif I_event_family = RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC then

      if SETUP_CL_MESSAGES_THREAD(O_error_msg,
                                  I_thread_luw) = 0 then
         return 0;
      end if;

   elsif I_event_family = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC then

      if SETUP_PR_MESSAGES_THREAD(O_error_msg,
                                  I_thread_luw) = 0 then
         return 0;
      end if;

   end if;

   open C_SET_THREAD_NUM;
   fetch C_SET_THREAD_NUM into O_thread_num;
   close C_SET_THREAD_NUM;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END SETUP_MESSAGES_THREAD;
--------------------------------------------------------

FUNCTION CREATE_MESSAGES(O_error_msg       OUT VARCHAR2,
                         I_event_family IN     VARCHAR2,
                         I_thread_num   IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CREATE_MESSAGES';

BEGIN

   if I_event_family = RPM_CONSTANTS.RIB_MSG_FAM_REG_PC then

      if CREATE_PC_MESSAGES(O_error_msg,
                            I_thread_num) = 0 then
         return 0;
      end if;

   elsif I_event_family = RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC then

      if CREATE_CL_MESSAGES(O_error_msg,
                            I_thread_num) = 0 then
         return 0;
      end if;

   elsif I_event_family = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC then

      if CREATE_PR_MESSAGES(O_error_msg,
                            I_thread_num) = 0 then
         return 0;
      end if;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END CREATE_MESSAGES;
--------------------------------------------------------

FUNCTION SETUP_PC_MESSAGES_THREAD(O_error_msg     OUT VARCHAR2,
                                  I_thread_luw IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.SETUP_PC_MESSAGES_THREAD';

BEGIN

   insert into rpm_price_publish_stage (price_event_payload_id,
                                        event_family,
                                        thread_number)
      select price_event_payload_id,
             RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
             CEIL(seq/I_thread_luw)
        from (select price_event_payload_id,
                     SUM(one) OVER (ORDER BY price_event_payload_id) seq
                from (select price_event_payload_id,
                             1 one
                        from rpm_price_event_payload rpp
                       where rib_family     = RPM_CONSTANTS.RIB_MSG_FAM_REG_PC
                         and publish_status = RPM_CONSTANTS.PE_NOT_PUBLISHED
                         and NOT EXISTS (select 'x'
                                           from rpm_price_chg_payload pcp,
                                                rpm_pe_cc_lock l
                                          where pcp.price_event_payload_id = rpp.price_event_payload_id
                                            and l.price_event_id           = pcp.price_change_id
                                            and l.price_event_type         = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE)));

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END SETUP_PC_MESSAGES_THREAD;
--------------------------------------------------------

FUNCTION SETUP_CL_MESSAGES_THREAD(O_error_msg     OUT VARCHAR2,
                                  I_thread_luw IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.SETUP_CL_MESSAGES_THREAD';

BEGIN

   insert into rpm_price_publish_stage (price_event_payload_id,
                                        event_family,
                                        thread_number)
      select price_event_payload_id,
             RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
             CEIL(seq/I_thread_luw)
        from (select price_event_payload_id,
                     SUM(one) OVER (ORDER BY price_event_payload_id) seq
                from (select price_event_payload_id,
                             1 one
                        from rpm_price_event_payload rpp
                       where rib_family     = RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC
                         and publish_status = RPM_CONSTANTS.PE_NOT_PUBLISHED
                         and NOT EXISTS (select 'x'
                                           from rpm_clearance_payload rcp,
                                                rpm_pe_cc_lock l
                                          where rpp.price_event_payload_id = rcp.price_event_payload_id
                                            and l.price_event_id           = rcp.clearance_id
                                            and l.price_event_type         IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                                               RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET))));

   return 1;

EXCEPTION

   when OTHERS then

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END SETUP_CL_MESSAGES_THREAD;
--------------------------------------------------------

FUNCTION SETUP_PR_MESSAGES_THREAD(O_error_msg     OUT VARCHAR2,
                                  I_thread_luw IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.SETUP_PR_MESSAGES_THREAD';

BEGIN

   insert into rpm_price_publish_stage (price_event_payload_id,
                                        event_family,
                                        thread_number)
      select distinct t2.price_event_payload_id,
             RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
             CEIL(t2.seq/I_thread_luw)
        from (select t1.price_event_payload_id,
                     DENSE_RANK() OVER (ORDER BY t1.min_price_event_payload_id,
                                                 t1.detail_id,
                                                 t1.rib_type) seq
                from (select rpp.price_event_payload_id,
                             rpd.promo_dtl_id detail_id,
                             rpp.rib_type,
                             MIN(rpp.price_event_payload_id) OVER (PARTITION BY rpp.rib_type,
                                                                                rpd.promo_dtl_id) min_price_event_payload_id
                        from rpm_price_event_payload rpp,
                             rpm_promo_dtl_payload rpd
                       where rpp.rib_family             = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC
                         and rpd.price_event_payload_id = rpp.price_event_payload_id
                         and rpp.publish_status         = RPM_CONSTANTS.PE_NOT_PUBLISHED
                         and NOT EXISTS (select 'x'
                                           from rpm_pe_cc_lock pe_lock
                                          where pe_lock.price_event_id   = rpd.promo_dtl_id
                                            and pe_lock.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE))
                      union all
                      select rpep.price_event_payload_id,
                             rpdcp.promo_dtl_id detail_id,
                             rpep.rib_type,
                             MIN(rpep.price_event_payload_id) OVER (PARTITION BY rpep.rib_type,
                                                                                 rpdcp.promo_dtl_id) min_price_event_payload_id
                        from rpm_price_event_payload rpep,
                             rpm_promo_dtl_cil_payload rpdcp
                       where rpep.rib_family              = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC
                         and rpdcp.price_event_payload_id = rpep.price_event_payload_id
                         and rpep.publish_status          = RPM_CONSTANTS.PE_NOT_PUBLISHED
                         and NOT EXISTS (select 'x'
                                           from rpm_pe_cc_lock pe_lock
                                          where pe_lock.price_event_id   = rpdcp.promo_dtl_id
                                            and pe_lock.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE))) t1) t2;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END SETUP_PR_MESSAGES_THREAD;
--------------------------------------------------------

FUNCTION CREATE_PC_MESSAGES(O_error_msg     OUT VARCHAR2,
                            I_thread_num IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CREATE_PC_MESSAGES';

BEGIN

   insert all
      when 1 = 1 then
      into rpm_price_publish_data (event_family,
                                   thread_number,
                                   row_id,
                                   record_descriptor,
                                   message,
                                   price_event_payload_id,
                                   location,
                                   location_type)
                           values (RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                   I_thread_num,
                                   seq,
                                   record_descriptor,
                                   message,price_event_payload_id,
                                   location,
                                   location_type)
         when rank = 1 then
            into rpm_price_publish_location (event_family,
                                             location,
                                             location_type)
                                     values (RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                             location,
                                             location_type)
      select rownum seq,
             case
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE then
                   'FDETL'
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD then
                   'FDETL'
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_DEL then
                   'FDELE'
             end record_descriptor,
             case
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE then
                   'CRE'
                   || FIELD_DELIMITER || price_change_id
                   || FIELD_DELIMITER || item
                   || FIELD_DELIMITER || TO_CHAR(effective_date, 'YYYYMMDDHH24MISS')
                   || FIELD_DELIMITER || selling_unit_change_ind
                   || FIELD_DELIMITER || selling_retail
                   || FIELD_DELIMITER || selling_retail_uom
                   || FIELD_DELIMITER || selling_retail_currency
                   || FIELD_DELIMITER || multi_unit_change_ind
                   || FIELD_DELIMITER || multi_units
                   || FIELD_DELIMITER || multi_units_retail
                   || FIELD_DELIMITER || multi_units_uom
                   || FIELD_DELIMITER || multi_units_currency
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD then
                   'MOD'
                   || FIELD_DELIMITER || price_change_id
                   || FIELD_DELIMITER || item
                   || FIELD_DELIMITER || TO_CHAR(effective_date, 'YYYYMMDDHH24MISS')
                   || FIELD_DELIMITER || selling_unit_change_ind
                   || FIELD_DELIMITER || selling_retail
                   || FIELD_DELIMITER || selling_retail_uom
                   || FIELD_DELIMITER || selling_retail_currency
                   || FIELD_DELIMITER || multi_unit_change_ind
                   || FIELD_DELIMITER || multi_units
                   || FIELD_DELIMITER || multi_units_retail
                   || FIELD_DELIMITER || multi_units_uom
                   || FIELD_DELIMITER || multi_units_currency
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_DEL then
                   price_change_id
                   || FIELD_DELIMITER || item
                end message,
                price_event_payload_id,
                location,
                location_type,
                ROW_NUMBER() OVER (PARTITION BY location,
                                                location_type
                                       ORDER BY NULL) rank
           from (select rps.price_event_payload_id,
                        rpc.item,
                        rpc.location,
                        rpc.location_type,
                        rpp.rib_type,
                        rpc.price_change_id,
                        rpc.effective_date,
                        rpc.selling_unit_change_ind,
                        rpc.selling_retail,
                        rpc.selling_retail_uom,
                        rpc.selling_retail_currency,
                        rpc.multi_unit_change_ind,
                        rpc.multi_units,
                        rpc.multi_units_retail,
                        rpc.multi_units_uom,
                        rpc.multi_units_currency
                   from rpm_price_publish_stage rps,
                        rpm_price_event_payload rpp,
                        rpm_price_chg_payload rpc
                  where rps.event_family           = RPM_CONSTANTS.RIB_MSG_FAM_REG_PC
                    and rps.thread_number          = I_thread_num
                    and rpp.price_event_payload_id = rps.price_event_payload_id
                    and rpc.price_event_payload_id = rpp.price_event_payload_id
                  order by rps.price_event_payload_id);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END CREATE_PC_MESSAGES;
--------------------------------------------------------

FUNCTION CREATE_CL_MESSAGES(O_error_msg     OUT VARCHAR2,
                            I_thread_num IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CREATE_CL_MESSAGES';

BEGIN

   insert all
      when 1 = 1 then
         into rpm_price_publish_data (event_family,
                                      thread_number,
                                      row_id,
                                      record_descriptor,
                                      message,
                                      price_event_payload_id,
                                      location,
                                      location_type)
                              values (RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                      I_thread_num,
                                      seq,
                                      record_descriptor,
                                      message,price_event_payload_id,
                                      location,
                                      location_type)
      when rank = 1 then
         into rpm_price_publish_location (event_family,
                                          location,
                                          location_type)
                                  values (RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                          location,
                                          location_type)
      select seq,
             case
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE then
                   'FDETL'
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD then
                   'FDETL'
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_DEL then
                   'FDELE'
             end record_descriptor,
             case
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE then
                   'CRE'
                   || FIELD_DELIMITER || clearance_id
                   || FIELD_DELIMITER || item
                   || FIELD_DELIMITER || TO_CHAR(effective_date, 'YYYYMMDDHH24MISS')
                   || FIELD_DELIMITER || selling_retail
                   || FIELD_DELIMITER || selling_retail_uom
                   || FIELD_DELIMITER || selling_retail_currency
                   || FIELD_DELIMITER || reset_clearance_id
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD then
                   'MOD'
                   || FIELD_DELIMITER || clearance_id
                   || FIELD_DELIMITER || item
                   || FIELD_DELIMITER || TO_CHAR(effective_date, 'YYYYMMDDHH24MISS')
                   || FIELD_DELIMITER || selling_retail
                   || FIELD_DELIMITER || selling_retail_uom
                   || FIELD_DELIMITER || selling_retail_currency
                   || FIELD_DELIMITER || reset_clearance_id
                when rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_DEL then
                   clearance_id
                   || FIELD_DELIMITER || item
             end message,
             price_event_payload_id,
             location,
             location_type,
             ROW_NUMBER() OVER (PARTITION BY location,
                                             location_type
                                    ORDER BY NULL) rank
        from (select rownum + 1 seq,
                     rps.price_event_payload_id,
                     rpc.item,
                     rpc.location,
                     rpc.location_type,
                     rpp.rib_type,
                     rpc.clearance_id,
                     rpc.effective_date,
                     rpc.selling_retail,
                     rpc.selling_retail_uom,
                     rpc.selling_retail_currency,
                     rpc.reset_clearance_id
                from rpm_price_publish_stage rps,
                     rpm_price_event_payload rpp,
                     rpm_clearance_payload rpc
               where rps.event_family           = RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC
                 and rps.thread_number          = I_thread_num
                 and rpp.price_event_payload_id = rps.price_event_payload_id
                 and rpc.price_event_payload_id = rpp.price_event_payload_id
               order by rps.price_event_payload_id);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CREATE_CL_MESSAGES;
--------------------------------------------------------

FUNCTION CREATE_PR_MESSAGES(O_error_msg     OUT VARCHAR2,
                            I_thread_num IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CREATE_PR_MESSAGES';

   L_tmbpe_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TMBPE';
   L_tpdtl_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TPDTL';
   L_tpgrp_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TPGRP';
   L_tglist_descriptor RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TGLIST';
   L_tlitm_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TLITM';
   L_tpci_descriptor   RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TPCI';
   L_tpcdt_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TPCDT';
   L_tpdsc_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TPDSC';
   L_tpisr_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TPISR';
   L_ttail_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'TTAIL';
   L_fpdel_descriptor  RPM_PRICE_PUBLISH_DATA.RECORD_DESCRIPTOR%TYPE := 'FPDEL';

   L_indicator VARCHAR2(255) := NULL;
   L_rib_type  VARCHAR2(255) := NULL;

   L_counter       NUMBER      := 0;
   L_promo_id      NUMBER      := NULL;
   L_promo_dtl_id  NUMBER      := NULL;
   L_group_id      NUMBER      := NULL;
   L_list_id       NUMBER      := NULL;
   L_pe_payload_id NUMBER      := NULL;
   L_location      NUMBER      := 0;
   L_loc_type      VARCHAR2(1) := 'X';

   L_cancel_il_promo_dtl_ind NUMBER := NULL;

   cursor C_PROMOS is
      select price_event_payload_id,
             indctr,
             rib_type,
             promo_id,
             cancel_il_promo_dtl_ind,
             location,
             location_type
        from (select stg.price_event_payload_id,
                     pep.price_event_payload_id
                        || pep.rib_family
                        || pep.rib_type
                        || rpd.promo_dtl_id indctr,
                     pep.rib_type,
                     rpd.promo_id,
                     0 cancel_il_promo_dtl_ind,
                     rplp.location,
                     rplp.location_type,
                     ROW_NUMBER() OVER (PARTITION BY pep.price_event_payload_id,
                                                     rpd.promo_id,
                                                     rplp.location,
                                                     rplp.location_type
                                            ORDER BY rpd.promo_comp_id) promo_rank
                from rpm_price_publish_stage stg,
                     rpm_price_event_payload pep,
                     rpm_promo_dtl_payload rpd,
                     rpm_promo_location_payload rplp
               where stg.thread_number          = I_thread_num
                 and stg.event_family           = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC
                 and pep.price_event_payload_id = stg.price_event_payload_id
                 and rpd.price_event_payload_id = stg.price_event_payload_id
                 and rpd.promo_dtl_payload_id   = rplp.promo_dtl_payload_id
              union all
              select stg.price_event_payload_id,
                     pep.price_event_payload_id
                        || pep.rib_family
                        || pep.rib_type
                        || rpd.promo_dtl_id indctr,
                     pep.rib_type,
                     rpd.promo_id,
                     1 cancel_il_promo_dtl_ind,
                     rpdclp.location,
                     rpdclp.location_type,
                     ROW_NUMBER() OVER (PARTITION BY pep.price_event_payload_id,
                                                     rpd.promo_id,
                                                     rpdclp.location,
                                                     rpdclp.location_type
                                            ORDER BY rpd.promo_comp_id) promo_rank
                from rpm_price_publish_stage stg,
                     rpm_price_event_payload pep,
                     rpm_promo_dtl_cil_payload rpd,
                     rpm_promo_dtl_cil_loc_payload rpdclp
               where stg.thread_number            = I_thread_num
                 and stg.event_family             = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC
                 and pep.price_event_payload_id   = stg.price_event_payload_id
                 and rpd.price_event_payload_id   = stg.price_event_payload_id
                 and rpd.promo_dtl_cil_payload_id = rpdclp.promo_dtl_cil_payload_id) t
       where promo_rank = 1
       order by location,
                location_type,
                price_event_payload_id;

   cursor C_PROMO_DTL (I_price_event_payload_id IN NUMBER,
                       I_promo_id               IN NUMBER,
                       I_rib_type               IN VARCHAR2,
                       I_location_id            IN NUMBER) is
      select rpd.promo_id,
             rpd.promo_name,
             rpd.promo_desc,
             rpd.promo_comp_id,
             rpd.promo_comp_desc,
             rpd.promo_comp_type,
             rpd.promo_dtl_id,
             rpd.apply_order,
             rpd.threshold_id,
             rpd.start_date,
             rpd.end_date,
             rpd.apply_to_code,
             rpd.discount_limit,
             rpd.customer_type,
             rpd.thresh_qualification_type,
             rpd.exception_parent_id,
             -- The following rankings ensure that the flat file is built with transaction exclusion
             -- details immediately following their parent detail.
             DENSE_RANK() OVER (PARTITION BY rpd.price_event_payload_id,
                                             rpd.promo_id
                                    ORDER BY NVL(rpd.exception_parent_id, rpd.promo_dtl_id)) parent_rnk,
             DENSE_RANK() OVER (PARTITION BY rpd.price_event_payload_id,
                                             rpd.promo_id,
                                             NVL(rpd.exception_parent_id, rpd.promo_dtl_id)
                                    ORDER BY NVL(rpd.exception_parent_id, -1),
                                             rpd.promo_dtl_id) dtl_rnk
        from rpm_promo_dtl_payload rpd,
             rpm_price_event_payload pep,
             rpm_promo_location_payload rplp
       where pep.price_event_payload_id = I_price_event_payload_id
         and rpd.promo_id               = I_promo_id
         and rpd.price_event_payload_id = pep.price_event_payload_id
         and pep.rib_type               = I_rib_type
         and rplp.promo_dtl_payload_id  = rpd.promo_dtl_payload_id
         and rplp.location              = I_location_id
       order by parent_rnk,
                dtl_rnk;

   cursor C_GROUP (I_price_event_payload_id IN NUMBER,
                   I_promo_dtl_id           IN NUMBER,
                   I_rib_type               IN VARCHAR2) is
      select dlg.promo_dtl_list_grp_id
        from rpm_promo_dtl_list_grp_payload dlg,
             rpm_promo_dtl_payload rpd,
             rpm_price_event_payload pep
       where rpd.promo_dtl_payload_id   = dlg.promo_dtl_payload_id
         and rpd.promo_dtl_id           = I_promo_dtl_id
         and pep.price_event_payload_id = I_price_event_payload_id
         and rpd.price_event_payload_id = pep.price_event_payload_id
         and pep.rib_type               = I_rib_type;

   cursor C_GRP_LIST (I_price_event_payload_id IN NUMBER,
                      I_group_id               IN NUMBER,
                      I_promo_dtl_id           IN NUMBER,
                      I_rib_type               IN VARCHAR2) is
      select dl.promo_dtl_list_id,
             dl.reward_application,
             dl.description,
             DECODE(dl.reward_application,
                    0, pdprp.buy_list_min,
                    pdprp.get_list_min) price_range_min,
             DECODE(dl.reward_application,
                    0, pdprp.buy_list_max,
                    pdprp.get_list_max) price_range_max
        from rpm_promo_dtl_list_payload dl,
             rpm_promo_dtl_list_grp_payload dlg,
             rpm_promo_dtl_payload rpd,
             rpm_price_event_payload pep,
             rpm_promo_dtl_prc_rng_payload pdprp
       where rpd.promo_dtl_payload_id          = dlg.promo_dtl_payload_id
         and dlg.promo_dtl_list_grp_payload_id = dl.promo_dtl_list_grp_payload_id
         and dlg.promo_dtl_list_grp_id         = I_group_id
         and rpd.promo_dtl_id                  = I_promo_dtl_id
         and pep.price_event_payload_id        = I_price_event_payload_id
         and rpd.price_event_payload_id        = pep.price_event_payload_id
         and pep.rib_type                      = I_rib_type
         and rpd.promo_dtl_payload_id          = pdprp.promo_dtl_payload_id (+);

   cursor C_ITEM (I_price_event_payload_id IN NUMBER,
                  I_group_id               IN NUMBER,
                  I_promo_dtl_id           IN NUMBER,
                  I_list_id                IN NUMBER,
                  I_rib_type               IN VARCHAR2) is
      select itm.item
        from rpm_promo_item_payload itm,
             rpm_promo_dtl_list_payload dl,
             rpm_promo_dtl_list_grp_payload dlg,
             rpm_promo_dtl_payload rpd,
             rpm_price_event_payload pep
       where rpd.promo_dtl_payload_id          = dlg.promo_dtl_payload_id
         and dlg.promo_dtl_list_grp_payload_id = dl.promo_dtl_list_grp_payload_id
         and dl.promo_dtl_list_payload_id      = itm.promo_dtl_list_payload_id
         and dlg.promo_dtl_list_grp_id         = I_group_id
         and rpd.promo_dtl_id                  = I_promo_dtl_id
         and dl.promo_dtl_list_id              = I_list_id
         and pep.price_event_payload_id        = I_price_event_payload_id
         and rpd.price_event_payload_id        = pep.price_event_payload_id
         and pep.rib_type                      = I_rib_type;

   cursor C_DISC_LDR (I_price_event_payload_id IN NUMBER,
                      I_group_id               IN NUMBER,
                      I_promo_dtl_id           IN NUMBER,
                      I_list_id                IN NUMBER,
                      I_rib_type               IN VARCHAR2) is
      select rpdl.change_type,
             rpdl.change_amount,
             rpdl.change_currency,
             rpdl.change_percent,
             rpdl.change_selling_uom,
             rpdl.qual_type,
             rpdl.qual_value,
             rpdl.duration
        from rpm_promo_disc_ldr_payload rpdl,
             rpm_promo_dtl_list_payload dl,
             rpm_promo_dtl_list_grp_payload dlg,
             rpm_promo_dtl_payload rpd,
             rpm_price_event_payload pep
       where rpd.promo_dtl_payload_id          = dlg.promo_dtl_payload_id
         and dlg.promo_dtl_list_grp_payload_id = dl.promo_dtl_list_grp_payload_id
         and rpdl.promo_dtl_list_payload_id    = dl.promo_dtl_list_payload_id
         and dlg.promo_dtl_list_grp_id         = I_group_id
         and rpd.promo_dtl_id                  = I_promo_dtl_id
         and dl.promo_dtl_list_id              = I_list_id
         and pep.price_event_payload_id        = I_price_event_payload_id
         and rpd.price_event_payload_id        = pep.price_event_payload_id
         and pep.rib_type                      = I_rib_type;

   cursor C_ITEM_LOC_SR (I_price_event_payload_id IN NUMBER,
                         I_promo_dtl_id           IN NUMBER,
                         I_rib_type               IN VARCHAR2,
                         I_location               IN NUMBER) is
      select ilsp.item,
             ilsp.selling_retail,
             ilsp.selling_retail_currency,
             ilsp.selling_uom,
             ilsp.effective_date,
             ilsp.ref_promo_dtl_id
        from rpm_promo_item_loc_sr_payload ilsp,
             rpm_promo_dtl_payload rpd,
             rpm_price_event_payload pep
       where rpd.promo_dtl_payload_id   = ilsp.promo_dtl_payload_id
         and rpd.promo_dtl_id           = I_promo_dtl_id
         and pep.price_event_payload_id = I_price_event_payload_id
         and rpd.price_event_payload_id = pep.price_event_payload_id
         and pep.rib_type               = I_rib_type
         and ilsp.location              = I_location
       order by ilsp.effective_date;

   cursor C_DTL_DEL (I_price_event_payload_id IN NUMBER) is
      select pdp.promo_id,
             pdp.promo_comp_id,
             pdp.promo_dtl_id,
             pdlgp.promo_dtl_list_grp_id,
             pdlp.promo_dtl_list_id,
             pip.item,
             -- The following rankings ensure that the flat file is built with transaction exclusion
             -- details immediately following their parent detail.
             DENSE_RANK() OVER (PARTITION BY pdp.price_event_payload_id,
                                             pdp.promo_id
                                    ORDER BY NVL(pdp.exception_parent_id, pdp.promo_dtl_id)) parent_rnk,
             DENSE_RANK() OVER (PARTITION BY pdp.price_event_payload_id,
                                             pdp.promo_id,
                                             NVL(pdp.exception_parent_id, pdp.promo_dtl_id)
                                    ORDER BY NVL(pdp.exception_parent_id, -1),
                                             pdp.promo_dtl_id) dtl_rnk
        from rpm_price_event_payload pep,
             rpm_promo_dtl_payload pdp,
             rpm_promo_dtl_list_grp_payload pdlgp,
             rpm_promo_dtl_list_payload pdlp,
             rpm_promo_item_payload pip
       where pep.price_event_payload_id         = I_price_event_payload_id
         and pdp.price_event_payload_id         = pep.price_event_payload_id
         and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
         and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
         and pip.promo_dtl_list_payload_id      = pdlp.promo_dtl_list_payload_id
       order by parent_rnk,
                dtl_rnk;

   cursor C_CREDIT_DTL (I_price_event_payload_id IN NUMBER,
                        I_promo_dtl_id           IN NUMBER) is
      select rfcdp.financial_dtl_id,
             rfcdp.card_type,
             rfcdp.bin_from_value,
             rfcdp.bin_to_value,
             rfcdp.commission_rate,
             rfcdp.comments,
             pdp.apply_to_code
        from rpm_promo_dtl_payload pdp,
             rpm_fin_cred_dtl_payload rfcdp
       where pdp.price_event_payload_id = I_price_event_payload_id
         and pdp.promo_dtl_id           = I_promo_dtl_id
         and pdp.promo_dtl_payload_id   = rfcdp.promo_dtl_payload_id;

   -- Cancel Item Loc
   cursor C_CIL (I_price_event_payload_id IN NUMBER) is
      select rpdcp.promo_id,
             rpdcp.promo_comp_id,
             rpdcp.exception_parent_id promo_dtl_id,
             rpdcp.cancellation_date,
             rpdcip.item
        from rpm_price_event_payload rpep,
             rpm_promo_dtl_cil_payload rpdcp,
             rpm_promo_dtl_cil_item_payload rpdcip
       where rpep.price_event_payload_id     = I_price_event_payload_id
         and rpep.price_event_payload_id     = rpdcp.price_event_payload_id
         and rpdcp.promo_dtl_cil_payload_id  = rpdcip.promo_dtl_cil_payload_id;

BEGIN

   -- Process All the Promotions
   for promo_rec IN C_PROMOS loop

      if L_indicator is NULL or
         promo_rec.indctr != L_indicator or
         promo_rec.location != L_location or
         promo_rec.location_type != L_loc_type then

         insert into rpm_price_publish_location (event_family,
                                                 location,
                                                 location_type)
                                         values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                 promo_rec.location,
                                                 promo_rec.location_type);

         -- if we've already written at least one record and the promo_id changes or the
         -- rib_type changes and the current rib_type is not a DEL, write a TTAIL rec

         if L_rib_type is NOT NULL and
            (promo_rec.promo_id != L_promo_id or
             L_rib_type != promo_rec.rib_type or
             L_location != promo_rec.location or
             L_loc_type != promo_rec.location_type) and
            L_rib_type != RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL and
            L_cancel_il_promo_dtl_ind = 0 then

            L_counter := L_counter + 1;

            insert into rpm_price_publish_data (event_family,
                                                thread_number,
                                                row_id,
                                                record_descriptor,
                                                price_event_payload_id,
                                                location,
                                                location_type)
                                        values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                I_thread_num,
                                                L_counter,
                                                L_ttail_descriptor,
                                                L_pe_payload_id,
                                                L_location,
                                                L_loc_type);
         end if;

         L_pe_payload_id := promo_rec.price_event_payload_id;

         -- if we don't have a DEL on our hands and it's either the first time through the loop
         -- and it is not a cancel item loc promotion
         -- or the promo_id value has changed or the rib_type has changed then we should
         -- proceed with writting non DELETE records to the rpm_price_publish_data table.

         if promo_rec.rib_type != RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL  and
            promo_rec.cancel_il_promo_dtl_ind = 0 then

            -- TMBPE
            if L_promo_id is NULL or
               promo_rec.promo_id != L_promo_id or
               promo_rec.rib_type != L_rib_type or
               promo_rec.location != L_location or
               promo_rec.location_type != L_loc_type then

               L_counter := L_counter + 1;

               insert into rpm_price_publish_data (event_family,
                                                   thread_number,
                                                   row_id,
                                                   record_descriptor,
                                                   message,
                                                   price_event_payload_id,
                                                   location,
                                                   location_type)
                                           values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                   I_thread_num,
                                                   L_counter,
                                                   L_tmbpe_descriptor,
                                                   DECODE(promo_rec.rib_type,
                                                          RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE, 'CRE',
                                                          'MOD'),
                                                   L_pe_payload_id,
                                                   promo_rec.location,
                                                   promo_rec.location_type);

               L_promo_id := promo_rec.promo_id;

            end if;

            L_rib_type := promo_rec.rib_type;
            L_location := promo_rec.location;
            L_loc_type := promo_rec.location_type;

            -- TPDTL
            for detail_rec IN C_PROMO_DTL (L_pe_payload_id,
                                           L_promo_id,
                                           L_rib_type,
                                           L_location) loop

               L_counter := L_counter + 1;

               insert into rpm_price_publish_data (event_family,
                                                   thread_number,
                                                   row_id,
                                                   record_descriptor,
                                                   message,
                                                   price_event_payload_id,
                                                   location,
                                                   location_type)
                                           values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                   I_thread_num,
                                                   L_counter,
                                                   L_tpdtl_descriptor,
                                                   detail_rec.promo_id
                                                      || FIELD_DELIMITER || detail_rec.promo_comp_id
                                                      || FIELD_DELIMITER || detail_rec.promo_name
                                                      || FIELD_DELIMITER || detail_rec.promo_desc
                                                      || FIELD_DELIMITER || detail_rec.promo_comp_desc
                                                      || FIELD_DELIMITER || detail_rec.promo_comp_type
                                                      || FIELD_DELIMITER || detail_rec.promo_dtl_id
                                                      || FIELD_DELIMITER || TO_CHAR(detail_rec.start_date, 'YYYYMMDDHH24MISS')
                                                      || FIELD_DELIMITER || TO_CHAR(detail_rec.end_date, 'YYYYMMDDHH24MISS')
                                                      || FIELD_DELIMITER || detail_rec.apply_to_code
                                                      || FIELD_DELIMITER || detail_rec.discount_limit
                                                      || FIELD_DELIMITER || detail_rec.apply_order
                                                      || FIELD_DELIMITER || detail_rec.threshold_id
                                                      || FIELD_DELIMITER || detail_rec.customer_type
                                                      || FIELD_DELIMITER || detail_rec.thresh_qualification_type
                                                      || FIELD_DELIMITER || detail_rec.exception_parent_id,
                                                   L_pe_payload_id,
                                                   L_location,
                                                   L_loc_type);

               L_promo_dtl_id := detail_rec.promo_dtl_id;

               -- TPGRP
               for list_grp_rec IN C_GROUP (L_pe_payload_id,
                                            L_promo_dtl_id,
                                            L_rib_type) loop

                  L_counter := L_counter + 1;

                  insert into rpm_price_publish_data (event_family,
                                                      thread_number,
                                                      row_id,
                                                      record_descriptor,
                                                      message,
                                                      price_event_payload_id,
                                                      location,
                                                      location_type)
                                              values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                      I_thread_num,
                                                      L_counter,
                                                      L_tpgrp_descriptor,
                                                      list_grp_rec.promo_dtl_list_grp_id,
                                                      L_pe_payload_id,
                                                      L_location,
                                                      L_loc_type);

                  L_group_id := list_grp_rec.promo_dtl_list_grp_id;

                  -- TGLIST
                  for list_rec IN C_GRP_LIST (L_pe_payload_id,
                                              L_group_id,
                                              L_promo_dtl_id,
                                              L_rib_type) loop

                     L_counter := L_counter + 1;

                     insert into rpm_price_publish_data (event_family,
                                                         thread_number,
                                                         row_id,
                                                         record_descriptor,
                                                         message,
                                                         price_event_payload_id,
                                                         location,
                                                         location_type)
                                                 values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                         I_thread_num,
                                                         L_counter,
                                                         L_tglist_descriptor,
                                                         list_rec.promo_dtl_list_id
                                                            || FIELD_DELIMITER || list_rec.reward_application
                                                            || FIELD_DELIMITER || list_rec.description
                                                            || FIELD_DELIMITER || list_rec.price_range_min
                                                            || FIELD_DELIMITER || list_rec.price_range_max,
                                                         L_pe_payload_id,
                                                         L_location,
                                                         L_loc_type);

                     L_list_id := list_rec.promo_dtl_list_id;

                     -- TLITM
                     for item_rec IN C_ITEM (L_pe_payload_id,
                                             L_group_id,
                                             L_promo_dtl_id,
                                             L_list_id,
                                             L_rib_type) loop

                        L_counter := L_counter + 1;

                        insert into rpm_price_publish_data (event_family,
                                                            thread_number,
                                                            row_id,
                                                            record_descriptor,
                                                            message,
                                                            price_event_payload_id,
                                                            location,
                                                            location_type)
                                                    values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                            I_thread_num,
                                                            L_counter,
                                                            L_tlitm_descriptor,
                                                            item_rec.item,
                                                            L_pe_payload_id,
                                                            L_location,
                                                            L_loc_type);
                     end loop; -- TLITM

                     -- TPDSC
                     for disc_ldr_rec IN C_DISC_LDR (L_pe_payload_id,
                                                     L_group_id,
                                                     L_promo_dtl_id,
                                                     L_list_id,
                                                     L_rib_type) loop

                        L_counter := L_counter + 1;

                        insert into rpm_price_publish_data (event_family,
                                                            thread_number,
                                                            row_id,
                                                            record_descriptor,
                                                            message,
                                                            price_event_payload_id,
                                                            location,
                                                            location_type)
                                                    values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                            I_thread_num,
                                                            L_counter,
                                                            L_tpdsc_descriptor,
                                                            disc_ldr_rec.change_type
                                                               || FIELD_DELIMITER || disc_ldr_rec.change_amount
                                                               || FIELD_DELIMITER || disc_ldr_rec.change_currency
                                                               || FIELD_DELIMITER || disc_ldr_rec.change_percent
                                                               || FIELD_DELIMITER || disc_ldr_rec.change_selling_uom
                                                               || FIELD_DELIMITER || disc_ldr_rec.qual_type
                                                               || FIELD_DELIMITER || disc_ldr_rec.qual_value
                                                               || FIELD_DELIMITER || disc_ldr_rec.duration,
                                                            L_pe_payload_id,
                                                            L_location,
                                                            L_loc_type);
                     end loop; --TPDSC

                     if detail_rec.promo_comp_type = RPM_CONSTANTS.FINANCE_CODE then

                        for credit_dtl_rec IN C_CREDIT_DTL (L_pe_payload_id,
                                                            L_promo_dtl_id) loop

                           L_counter := L_counter + 1;

                           insert into rpm_price_publish_data (event_family,
                                                               thread_number,
                                                               row_id,
                                                               record_descriptor,
                                                               message,
                                                               price_event_payload_id,
                                                               location,
                                                               location_type)
                                                       values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                               I_thread_num,
                                                               L_counter,
                                                               L_tpcdt_descriptor,
                                                               credit_dtl_rec.financial_dtl_id
                                                                  || FIELD_DELIMITER || credit_dtl_rec.card_type
                                                                  || FIELD_DELIMITER || credit_dtl_rec.bin_from_value
                                                                  || FIELD_DELIMITER || credit_dtl_rec.bin_to_value
                                                                  || FIELD_DELIMITER || credit_dtl_rec.commission_rate
                                                                  || FIELD_DELIMITER || credit_dtl_rec.comments
                                                                  || FIELD_DELIMITER || credit_dtl_rec.apply_to_code,
                                                               L_pe_payload_id,
                                                               L_location,
                                                               L_loc_type);

                        end loop;  -- TPCDT

                     end if; -- if promo_comp_type = RPM_CONSTANTS.FINANCE_CODE

                  end loop; -- TGLIST

               end loop; -- TPGRP

               -- TPISR
               for item_loc_sr_rec IN C_ITEM_LOC_SR (L_pe_payload_id,
                                                     L_promo_dtl_id,
                                                     L_rib_type,
                                                     L_location) loop

                  L_counter := L_counter + 1;

                  insert into rpm_price_publish_data (event_family,
                                                      thread_number,
                                                      row_id,
                                                      record_descriptor,
                                                      message,
                                                      price_event_payload_id,
                                                      location,
                                                      location_type)
                                              values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                      I_thread_num,
                                                      L_counter,
                                                      L_tpisr_descriptor,
                                                      item_loc_sr_rec.item
                                                         || FIELD_DELIMITER || item_loc_sr_rec.selling_retail
                                                         || FIELD_DELIMITER || item_loc_sr_rec.selling_uom
                                                         || FIELD_DELIMITER || TO_CHAR(item_loc_sr_rec.effective_date, 'YYYYMMDDHH24MISS')
                                                         || FIELD_DELIMITER || item_loc_sr_rec.selling_retail_currency
                                                         || FIELD_DELIMITER || item_loc_sr_rec.ref_promo_dtl_id,
                                                      L_pe_payload_id,
                                                      L_location,
                                                      L_loc_type);

               end loop; -- TPISR

            end loop; -- TPDTL

            L_indicator := promo_rec.indctr;

         -- processing delete record
         elsif promo_rec.rib_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL then

            L_location := promo_rec.location;
            L_loc_type := promo_rec.location_type;

            L_indicator := promo_rec.indctr;
            L_promo_id  := promo_rec.promo_id;
            L_rib_type  := promo_rec.rib_type;

            for del_rec IN C_DTL_DEL(L_pe_payload_id) loop

               L_counter := L_counter + 1;

               insert into rpm_price_publish_data (event_family,
                                                   thread_number,
                                                   row_id,
                                                   record_descriptor,
                                                   message,
                                                   price_event_payload_id,
                                                   location,
                                                   location_type)
                                           values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                   I_thread_num,
                                                   L_counter,
                                                   L_fpdel_descriptor,
                                                   del_rec.promo_id
                                                      || FIELD_DELIMITER || del_rec.promo_comp_id
                                                      || FIELD_DELIMITER || del_rec.promo_dtl_id
                                                      || FIELD_DELIMITER || del_rec.promo_dtl_list_grp_id
                                                      || FIELD_DELIMITER || del_rec.promo_dtl_list_id
                                                      || FIELD_DELIMITER || del_rec.item,
                                                   L_pe_payload_id,
                                                   L_location,
                                                   L_loc_type);

            end loop;

         -- processing cancel item loc record
         elsif promo_rec.cancel_il_promo_dtl_ind = 1 then

            -- TPCI
            for cil_rec IN C_CIL (L_pe_payload_id) loop

               L_counter := L_counter + 1;

               L_location := promo_rec.location;
               L_loc_type := promo_rec.location_type;

               insert into rpm_price_publish_data (event_family,
                                                   thread_number,
                                                   row_id,
                                                   record_descriptor,
                                                   message,
                                                   price_event_payload_id,
                                                   location,
                                                   location_type)
                                           values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                                   I_thread_num,
                                                   L_counter,
                                                   L_tpci_descriptor,
                                                   cil_rec.promo_id
                                                      || FIELD_DELIMITER || cil_rec.promo_comp_id
                                                      || FIELD_DELIMITER || cil_rec.promo_dtl_id
                                                      || FIELD_DELIMITER || cil_rec.item
                                                      || FIELD_DELIMITER || TO_CHAR(cil_rec.cancellation_date, 'YYYYMMDDHH24MISS'),
                                                   L_pe_payload_id,
                                                   L_location,
                                                   L_loc_type);
            end loop; -- TPCI

            L_indicator := promo_rec.indctr;
            L_promo_id := promo_rec.promo_id;
            L_rib_type := promo_rec.rib_type;

         end if;

      end if; -- L_indicator is NULL or L_indicator != promo_rec.indctr

      L_location := promo_rec.location;
      L_loc_type := promo_rec.location_type;
      L_cancel_il_promo_dtl_ind := promo_rec.cancel_il_promo_dtl_ind;

   end loop; -- C_PROMOS

   if L_rib_type is NOT NULL and
      L_rib_type != RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL and
      L_cancel_il_promo_dtl_ind = 0 then

      L_counter := L_counter + 1;
      insert into rpm_price_publish_data (event_family,
                                          thread_number,
                                          row_id,
                                          record_descriptor,
                                          price_event_payload_id,
                                          location,
                                          location_type)
                                  values (RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                          I_thread_num,
                                          L_counter,
                                          L_ttail_descriptor,
                                          L_pe_payload_id,
                                          L_location,
                                          L_loc_type);
   end if;


   return 1;

EXCEPTION

   when OTHERS then

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CREATE_PR_MESSAGES;
--------------------------------------------------------
FUNCTION CLEANUP_PC_PAYLOAD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CLEANUP_PC_PAYLOAD';

   L_price_event_payload_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C1 is
      select pep.price_event_payload_id
        from rpm_price_event_payload pep,
             rpm_price_chg_payload pcp
       where pep.rib_family             = RPM_CONSTANTS.RIB_MSG_FAM_REG_PC
         and pep.publish_status         = RPM_CONSTANTS.PE_PUBLISHED
         and pep.price_event_payload_id = pcp.price_event_payload_id;

BEGIN

   open C1;
   fetch C1 BULK COLLECT into L_price_event_payload_ids;
   close C1;

   forall i IN 1..L_price_event_payload_ids.COUNT
      delete
        from rpm_price_chg_payload
       where price_event_payload_id = L_price_event_payload_ids(i);

   forall i IN 1..L_price_event_payload_ids.COUNT
      delete
        from rpm_price_event_payload
       where price_event_payload_id = L_price_event_payload_ids(i);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CLEANUP_PC_PAYLOAD;
--------------------------------------------------------

FUNCTION CLEANUP_CL_PAYLOAD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CLEANUP_CL_PAYLOAD';

   L_price_event_payload_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C1 is
      select pep.price_event_payload_id
        from rpm_price_event_payload pep,
             rpm_clearance_payload rcp
       where pep.rib_family             = RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC
         and pep.publish_status         = RPM_CONSTANTS.PE_PUBLISHED
         and pep.price_event_payload_id = rcp.price_event_payload_id;

BEGIN

   open C1;
   fetch C1 BULK COLLECT into L_price_event_payload_ids;
   close C1;

   forall i IN 1..L_price_event_payload_ids.COUNT
      delete
        from rpm_clearance_payload
       where price_event_payload_id = L_price_event_payload_ids(i);

   forall i IN 1..L_price_event_payload_ids.COUNT
      delete
        from rpm_price_event_payload
       where price_event_payload_id = L_price_event_payload_ids(i);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CLEANUP_CL_PAYLOAD;
--------------------------------------------------------

FUNCTION CLEANUP_PR_PAYLOAD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CLEANUP_PR_PAYLOAD';

   L_price_event_payload_id      RPM_PRICE_EVENT_PAYLOAD.PRICE_EVENT_PAYLOAD_ID%TYPE               := NULL;
   L_promo_dtl_payload_id        RPM_PROMO_DTL_PAYLOAD.PROMO_DTL_PAYLOAD_ID%TYPE                   := NULL;
   L_promo_dtl_cil_payload_id    RPM_PROMO_DTL_CIL_PAYLOAD.PROMO_DTL_CIL_PAYLOAD_ID%TYPE           := NULL;
   L_prm_dtl_list_grp_payload_id RPM_PROMO_DTL_LIST_GRP_PAYLOAD.PROMO_DTL_LIST_GRP_PAYLOAD_ID%TYPE := NULL;
   L_promo_dtl_list_payload_id   RPM_PROMO_DTL_LIST_PAYLOAD.PROMO_DTL_LIST_PAYLOAD_ID%TYPE         := NULL;

   cursor C_PRICE_EVENT_PAYLOAD is
      select distinct price_event_payload_id
        from rpm_price_event_payload pep
       where rib_family     = RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC
         and publish_status = RPM_CONSTANTS.PE_PUBLISHED;

   cursor C_PROMO_DTL_PAYLOAD is
      select distinct rpdp.promo_dtl_payload_id
        from rpm_promo_dtl_payload rpdp
       where rpdp.price_event_payload_id = L_price_event_payload_id;

   cursor C_PROMO_DTL_LIST_GRP is
      select distinct promo_dtl_list_grp_payload_id
        from rpm_promo_dtl_list_grp_payload
       where promo_dtl_payload_id = L_promo_dtl_payload_id;

   cursor C_PROMO_DTL_LIST is
      select distinct promo_dtl_list_payload_id
        from rpm_promo_dtl_list_payload
       where promo_dtl_list_grp_payload_id = L_prm_dtl_list_grp_payload_id;

   cursor C_PROMO_DTL_CIL_PAYLOAD is
      select distinct promo_dtl_cil_payload_id
        from rpm_promo_dtl_cil_payload
       where price_event_payload_id = L_price_event_payload_id;

BEGIN

   for rec1 IN C_PRICE_EVENT_PAYLOAD loop
      L_price_event_payload_id := rec1.price_event_payload_id;

      for rec2 IN C_PROMO_DTL_PAYLOAD loop
         L_promo_dtl_payload_id := rec2.promo_dtl_payload_id;

         for rec3 IN C_PROMO_DTL_LIST_GRP loop
            L_prm_dtl_list_grp_payload_id := rec3.promo_dtl_list_grp_payload_id;

            for rec4 IN C_PROMO_DTL_LIST loop
               L_promo_dtl_list_payload_id := rec4.promo_dtl_list_payload_id;

               delete
                 from rpm_promo_item_payload
                where promo_dtl_list_payload_id = L_promo_dtl_list_payload_id;

               delete
                 from rpm_promo_disc_ldr_payload
                where promo_dtl_list_payload_id = L_promo_dtl_list_payload_id;

               delete
                 from rpm_promo_dtl_mn_payload
                where promo_dtl_list_payload_id = L_promo_dtl_list_payload_id;

               delete
                 from rpm_promo_dtl_list_payload
                where promo_dtl_list_payload_id = L_promo_dtl_list_payload_id;

            end loop;

            delete
              from rpm_promo_dtl_list_grp_payload
             where promo_dtl_list_grp_payload_id = L_prm_dtl_list_grp_payload_id;

         end loop;

         delete
           from rpm_promo_item_loc_sr_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

         delete
           from rpm_promo_location_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

         delete
           from rpm_promo_dtl_prc_rng_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

         delete
           from rpm_fin_cred_dtl_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

         delete
           from rpm_threshold_int_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

         delete
           from rpm_promo_dtl_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

         delete
           from rpm_promo_fin_dtl_payload
          where promo_dtl_payload_id = L_promo_dtl_payload_id;

      end loop;

      -- Delete Cancel Item Loc payload
      for rec5 IN C_PROMO_DTL_CIL_PAYLOAD loop
         L_promo_dtl_cil_payload_id := rec5.promo_dtl_cil_payload_id;

         delete
           from rpm_promo_dtl_cil_loc_payload
          where promo_dtl_cil_payload_id = L_promo_dtl_cil_payload_id;

         delete
           from rpm_promo_dtl_cil_item_payload
          where promo_dtl_cil_payload_id = L_promo_dtl_cil_payload_id;

         delete
           from rpm_promo_dtl_cil_payload
          where promo_dtl_cil_payload_id = L_promo_dtl_cil_payload_id;

      end loop;

      delete
        from rpm_price_event_payload
       where price_event_payload_id = L_price_event_payload_id;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CLEANUP_PR_PAYLOAD;
--------------------------------------------------------

FUNCTION CLEANUP_ALL_PAYLOADS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.CLEANUP_ALL_PAYLOADS';

BEGIN

   if CLEANUP_PC_PAYLOAD(O_error_msg) = 0 then
      return 0;

   elsif CLEANUP_CL_PAYLOAD(O_error_msg) = 0 then
      return 0;

   elsif CLEANUP_PR_PAYLOAD(O_error_msg) = 0 then
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CLEANUP_ALL_PAYLOADS;
--------------------------------------------------------
FUNCTION UPDATE_PUBLISH_STATUS(O_error_msg       OUT VARCHAR2,
                               I_event_family IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_NO_RIB_PRICE_PUBLISH_SQL.UPDATE_PUBLISH_STATUS';

BEGIN

   update rpm_price_event_payload
      set publish_status = RPM_CONSTANTS.PE_PUBLISHED
    where price_event_payload_id IN (select distinct price_event_payload_id
                                       from rpm_price_publish_data
                                      where event_family = I_event_family);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END UPDATE_PUBLISH_STATUS;
--------------------------------------------------------
END RPM_NO_RIB_PRICE_PUBLISH_SQL;
/
