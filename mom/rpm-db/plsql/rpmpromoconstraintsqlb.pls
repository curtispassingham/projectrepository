CREATE OR REPLACE PACKAGE BODY RPM_PROMO_CONSTRAINT_SQL AS

PROCEDURE SEARCH(O_return_code              OUT NUMBER,
                 O_error_msg                OUT VARCHAR2,
                 O_lc_tbl                   OUT OBJ_PROMO_CONSTRAINT_TBL,
                 O_lc_count                 OUT NUMBER,
                 I_lc_criterias             IN  OBJ_PROMO_CONST_SEARCH_TBL)
IS

  L_criterias         OBJ_PROMO_CONST_SEARCH_TBL;
  L_criteria          OBJ_PROMO_CONST_SEARCH_REC;
  L_select            VARCHAR2(4000) := NULL;
  L_from              VARCHAR2(4000) := NULL; 
  L_where             VARCHAR2(4000) := NULL;
  
  TYPE promoConstCursorType IS REF CURSOR;         -- define weak REF CURSOR type
  il_lc               promoConstCursorType;        -- declare cursor variable
  L_from2             VARCHAR2(4000) := NULL; 
  L_from3	      VARCHAR2(4000) := NULL; 
  L_from4             VARCHAR2(4000) := NULL; 
  L_from5             VARCHAR2(4000) := NULL; 

BEGIN

  DELETE FROM rpm_pc_search_num_tbl;
  DELETE FROM rpm_pc_search_char_tbl;
  DELETE FROM rpm_pc_search_merch_tbl;   

  L_criterias   :=  I_lc_criterias ;
  L_criteria    :=  L_criterias(1);
  
  IF l_criteria.dept_class_subclasses IS NOT NULL AND 
      l_criteria.dept_class_subclasses.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_merch_tbl
      SELECT 'DEPT_CLASS_SUBCLASSES',
             dcs.*
      FROM TABLE(CAST(l_criteria.dept_class_subclasses AS obj_dept_class_subclass_tbl)) dcs;      
   
  END IF;
  
  IF l_criteria.zones IS NOT NULL AND 
      l_criteria.zones.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_num_tbl
      SELECT 'ZONES',
              z.*
      FROM TABLE(CAST(l_criteria.zones AS obj_numeric_id_table)) z;
   
  END IF;
  
  IF l_criteria.locations IS NOT NULL AND 
      l_criteria.locations.COUNT > 0 THEN
         
      INSERT INTO rpm_pc_search_num_tbl
      SELECT 'LOCATIONS',
              l.*
      FROM TABLE(CAST(l_criteria.locations AS obj_numeric_id_table)) l;
   
  END IF;   

  L_select :=  'SELECT /*+ ORDERED */ NEW obj_promo_constraint_rec(';
  
  L_select := L_select ||
                      ' rpc.pricing_constraint_id' ||                   --  PRICING_CONSTRAINT_ID     
                      ',rpc.dept' ||                                    --  DEPT     
                      ',de.dept_name' ||                                --  DEPT_NAME
                      ',rpc.class' ||                                   --  CLASS                          
                      ',cl.class_name' ||                               --  CLASS_NAME
                      ',rpc.subclass' ||                                --  SUBCLASS
                      ',su.sub_name' ||                                 --  SUBCLASS_NAME
                      ',loc.loctype' ||                                 --  ZONE_TYPE
                      ',rpc.location' ||                                --  LOCATION
                      ',loc.name' ||                                    --  LOCATION_DESCRIPTION
                      ',rpc.pc_promo_days'||       	                --  PC_PROM_DAYS
                      ',rpc.state)';       	                        --  state
  
  L_from :=           ' FROM ';
  
  
  L_from2 :=          'rpm_pricing_constraint rpc, ';
  
  L_from3 :=          'deps de, '||
                      'class cl, '||
                      'subclass su,'||
                      '(SELECT store loc, '||
                      '0 loctype, '||
                      'store_name name '||
                      'FROM store '||
                      'UNION '||
                      'SELECT wh loc, '||
                      '2 loctype, '||
                      'wh_name name '||
                      'FROM wh) loc ';
                      
  
  L_from4 := ') loccri, ';

  L_from5 := ' AND rownum > 0) loccri, ';

                      
  L_where := ' WHERE rpc.location = loc.loc ' ||
             'AND rpc.dept = de.dept ' ||
             'AND rpc.dept = cl.dept ' ||
             'AND rpc.class = cl.class '||
             'AND rpc.dept = su.dept ' ||
             'AND rpc.class = su.class ' ||
             'AND rpc.subclass = su.subclass ' ;
                    
  -- merch hierarchy
  
  IF L_criteria.dept_class_subclasses IS NOT NULL THEN
  
      L_from := L_from||
                '(SELECT cri.dept, NVL (cri.CLASS, sub.CLASS) class, '||
                'NVL (cri.subclass, sub.subclass) subclass '||
                'FROM rpm_pc_search_merch_tbl cri, subclass sub '||
                'WHERE sub.dept = cri.dept '||
                'AND sub.CLASS = NVL (cri.CLASS, sub.CLASS) '||
                'AND sub.subclass = NVL (cri.subclass, sub.subclass) '||
                'AND cri.criteria_type = ''DEPT_CLASS_SUBCLASSES'') cri, ';
  
      L_where := L_where ||
                 'AND rpc.dept = cri.dept '||
                 'AND rpc.CLASS = cri.class '||
                 'AND rpc.subclass = cri.subclass ';                 
             
      L_from := L_from||L_from2;
 
  END IF;
  
  -- location
  IF L_criteria.locations IS NOT NULL THEN
       L_from := L_from|| 
                 '(SELECT numeric_id location FROM rpm_pc_search_num_tbl WHERE criteria_type = ''LOCATIONS'' ';                 

       L_where := L_where ||
                  'AND rpc.LOCATION = loccri.location ';                     
              
    -- zone
   
  ELSIF L_criteria.zones IS NOT NULL THEN
       L_from := L_from|| 
                 '(SELECT location FROM rpm_zone_location rzl '||
                 'WHERE rzl.zone_id IN (SELECT numeric_id FROM rpm_pc_search_num_tbl WHERE criteria_type = ''ZONES'') ';
       
       L_where := L_where ||
                 'AND rpc.LOCATION = loccri.location ';
   
   -- zone group
  ELSIF L_criteria.zone_group IS NOT NULL THEN
       L_from := L_from|| 
                 '(SELECT LOCATION FROM rpm_zone_location rzl, rpm_zone rz '||
                 'WHERE rz.zone_group_id = '||L_criteria.zone_group||
                 'AND rzl.zone_id = rz.zone_id ';
       
       L_where := L_where ||
                  'AND rpc.LOCATION = loccri.location ';
  END IF;   
  
  IF L_criteria.dept_class_subclasses IS NOT NULL 
     AND (L_criteria.locations IS NOT NULL
         OR L_criteria.zones IS NOT NULL
         OR L_criteria.zone_group IS NOT NULL) THEN
  
     L_from := L_from||L_from5;
     
  END IF; 
  
  IF L_criteria.dept_class_subclasses IS NULL THEN         
  
      L_from := L_from||L_from4||L_from2;
      
  END IF;              
  
  L_from := L_from ||L_from3;

  OPEN il_lc FOR L_select||' '||L_from||' '||L_where;
  FETCH il_lc BULK COLLECT INTO O_lc_tbl;
  CLOSE il_lc;
   
  O_lc_count := O_lc_tbl.COUNT;

  O_return_code := 1;

EXCEPTION

   WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PROMO_CONSTRAINT_SQL.SEARCH',
                                        to_char(SQLCODE));
      O_return_code := 0;
    

END SEARCH;

END RPM_PROMO_CONSTRAINT_SQL;
/