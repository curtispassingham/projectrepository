CREATE OR REPLACE PACKAGE BODY RPM_EXE_CC_QUERY_RULES AS

FUNCTION EXECUTE_VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                          I_function_name    IN     RPM_CONFLICT_QUERY_CONTROL.CONFLICT_QUERY_FUNCTION_NAME%TYPE,
                          I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------

FUNCTION EXECUTE_VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                          I_function_name    IN     RPM_CONFLICT_QUERY_CONTROL.CONFLICT_QUERY_FUNCTION_NAME%TYPE,
                          I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_EXE_CC_QUERY_RULES.EXECUTE_VALIDATE';

   L_error_rec   CONFLICT_CHECK_ERROR_REC := NULL;
   L_call_string VARCHAR2(255)            := NULL;
   L_return      NUMBER                   := 1;

BEGIN

   L_call_string := 'BEGIN :return := '||I_function_name||'(:error_tbl, :price_event_type); END;';

   EXECUTE IMMEDIATE L_call_string
      USING OUT L_return,
            IN OUT IO_error_table,
            IN I_price_event_type;

   return L_return;

EXCEPTION

   when OTHERS then
      L_error_rec.price_event_id   := NULL;
      L_error_rec.future_retail_id := NULL;
      L_error_rec.error_type       := RPM_CONSTANTS.PLSQL_ERROR;
      L_error_rec.error_string     := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                         SQLERRM,
                                                         L_program,
                                                         TO_CHAR(SQLCODE));
      IO_error_table               := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END EXECUTE_VALIDATE;

--------------------------------------------------------

FUNCTION EXECUTE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_EXE_CC_QUERY_RULES.EXECUTE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   cursor C_RULES is
      select conflict_query_function_name
        from rpm_conflict_query_control
       where active = 'Y'
       order by execution_order;

BEGIN

   for rec IN C_RULES loop
      if EXECUTE_VALIDATE(IO_error_table,
                          rec.conflict_query_function_name,
                          I_price_event_type) = 0 then
         return 0;
      end if;
   end loop;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END EXECUTE;
--------------------------------------------------------
END;
/

