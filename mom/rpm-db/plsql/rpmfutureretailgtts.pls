CREATE OR REPLACE PACKAGE RPM_FUTURE_RETAIL_GTT_SQL AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION MERGE_LOCATION_MOVE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                             I_lm_rec       IN     RPM_LOCATION_MOVE%ROWTYPE)
RETURN NUMBER;

FUNCTION LOCATION_MOVE_SCRUB(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                             I_lm_rec       IN     RPM_LOCATION_MOVE%ROWTYPE)
RETURN NUMBER;

FUNCTION MERGE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION MERGE_PARENT_PRICE_CHANGE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_parent_ids   IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER;

FUNCTION MERGE_CLEARANCE(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                         I_clearance_ids   IN     OBJ_NUMERIC_ID_TABLE,
                         I_clear_start_ind IN     RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE)
RETURN NUMBER;

FUNCTION MERGE_CLEARANCE(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                         I_parent_ids      IN     OBJ_NUM_NUM_DATE_TBL,
                         I_clear_start_ind IN     RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE)
RETURN NUMBER;

FUNCTION UPD_CLEARANCE_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_clearance_ids    IN     OBJ_NUMERIC_ID_TABLE,
                             I_bulk_cc_pe_id    IN     NUMBER,
                             I_pe_sequence_id   IN     NUMBER,
                             I_pe_thread_number IN     NUMBER,
                             I_chunk_number     IN     NUMBER)
RETURN NUMBER;

FUNCTION UPD_NEW_CLEARANCE_RESET(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_clearance_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_rib_trans_id    IN     NUMBER,
                                 I_bulk_cc_pe_id   IN     NUMBER,
                                 I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                 I_chunk_number    IN     NUMBER)
RETURN NUMBER;

FUNCTION REMOVE_CLEARANCE_RESETS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                                 O_deleted_clearance_id    OUT OBJ_CLEARANCE_ITEM_LOC_TBL)
RETURN NUMBER;

FUNCTION MERGE_PROMOTION(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                         I_promo_recs         IN     OBJ_RPM_CC_PROMO_TBL,
                         I_child_promo_dtl_id IN     RPM_PROMO_DTL.PROMO_DTL_ID%TYPE DEFAULT NULL,
                         I_price_event_type   IN     VARCHAR2,
                         I_nil_ind            IN     NUMBER DEFAULT 0,
                         I_bulk_cc_pe_id      IN     NUMBER DEFAULT NULL)
RETURN NUMBER;

FUNCTION MERGE_NEW_PROMO_END_DATE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_promo_recs         IN     OBJ_RPM_CC_PROMO_TBL,
                                  I_price_event_type   IN     VARCHAR2,
                                  I_new_promo_end_date IN     DATE   DEFAULT NULL)
RETURN NUMBER;

FUNCTION GET_AFFECTED_PARENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             O_parents             OUT OBJ_NUM_NUM_DATE_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION GET_CHILDREN(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      O_child_event_ids     OUT OBJ_NUM_NUM_DATE_TBL,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION REMOVE_LOC_MOVE_TIMELINES(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_zone_ids     IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER;

FUNCTION REMOVE_TIMELINES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_type IN     VARCHAR2,
                          I_event_ids        IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER;

FUNCTION REMOVE_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids  IN     OBJ_NUM_NUM_DATE_TBL,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION REMOVE_FR_IL_TIMELINES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUM_NUM_DATE_TBL,
                                I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION GET_PROMO_SIBLINGS(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                            O_sibling_promo_ids    OUT OBJ_NUM_NUM_DATE_TBL,
                            I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

END RPM_FUTURE_RETAIL_GTT_SQL;
/