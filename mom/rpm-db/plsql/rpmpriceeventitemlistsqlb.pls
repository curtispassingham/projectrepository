CREATE OR REPLACE PACKAGE BODY RPM_PRICE_EVENT_ITEM_LIST_SQL AS

   LP_peil_no_valid_items   NUMBER := 0;
   LP_peil_some_valid_items NUMBER := 1;
   LP_peil_all_valid_items  NUMBER := 2;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_UPLOADED_DATA(O_error_msg          OUT VARCHAR2,
                                O_valid_result       OUT NUMBER,
                                I_merch_list_id   IN     NUMBER,
                                I_merch_list_desc IN     VARCHAR2,
                                I_user_id         IN     VARCHAR2)
RETURN NUMBER IS

   L_program         VARCHAR2(55) := 'RPM_PRICE_EVENT_ITEM_LIST_SQL.VALIDATE_UPLOADED_DATA';
   L_item_orig_count NUMBER       := 0;

BEGIN

   select COUNT(1)
     into L_item_orig_count
     from rpm_merch_list_dtl_ws
    where merch_list_id = I_merch_list_id;

   insert into rpm_merch_list_detail(merch_list_id,
                                     merch_level,
                                     item,
                                     item_desc)
      select distinct I_merch_list_id,
             DECODE(im.item_level,
                    im.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             im.item,
             im.item_desc
        from rsm_hierarchy_permission rhp,
             rsm_role_hierarchy_perm rrhp,
             rsm_hierarchy_type rht,
             rsm_user_role rur,
             subclass sc,
             rpm_merch_list_dtl_ws rmld,
             item_master im
       where rhp.reference_class    IN (RPM_CONSTANTS.RSM_REF_CLASS_DEPT,
                                        RPM_CONSTANTS.RSM_REF_CLASS_CLASS,
                                        RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS)
         and rhp.id                 = rrhp.parent_id
         and rrhp.hierarchy_type_id = rht.id
         and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
         and rrhp.role_id           = rur.role_id
         and rur.user_id            = I_user_id
         and rhp.key_value          IN (TO_CHAR(sc.dept),
                                        TO_CHAR(sc.dept||';'||sc.class),
                                        TO_CHAR(sc.dept||';'||sc.class||';'||sc.subclass))
         and rmld.merch_list_id     = I_merch_list_id
         and rmld.item              = im.item
         and im.tran_level         >= im.item_level
         and im.status              = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind        = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.dept                = sc.dept
         and im.class               = sc.class
         and im.subclass            = sc.subclass
         and NOT EXISTS (select 'x'
                           from rpm_merch_list_dtl_ws rmld2
                          where rmld2.item          = im.item_parent
                            and rmld2.merch_list_id = rmld.merch_list_id);

   --Determine whether any invalid items were removed
   if SQL%ROWCOUNT = 0 then
      O_valid_result := LP_peil_no_valid_items;

   elsif SQL%ROWCOUNT = L_item_orig_count then
      O_valid_result := LP_peil_all_valid_items;

   else
      O_valid_result := LP_peil_some_valid_items;

   end if;

   --Only insert into PEIL tables if valid items are present
   if O_valid_result != 0 then

      insert into rpm_merch_list_head (merch_list_id,
                                       merch_list_desc,
                                       merch_list_type)
         values (I_merch_list_id,
                 I_merch_list_desc,
                 RPM_CONSTANTS.MERCH_LIST_HEAD_PEIL);

      --Populate rpm_peil_dept_class_subclass with new records
      insert into rpm_peil_dept_class_subclass (merch_list_id,
                                                dept,
                                                class,
                                                subclass)
         select distinct rmld.merch_list_id,
                im.dept,
                im.class,
                im.subclass
           from rpm_merch_list_detail rmld,
                item_master im
          where rmld.merch_list_id = I_merch_list_id
            and rmld.item          = im.item;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END VALIDATE_UPLOADED_DATA;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_TXN_EXCLUSION_PEIL(O_error_msg                OUT VARCHAR2,
                                     O_some_data_removed_ind    OUT NUMBER,
                                     O_all_data_removed_ind     OUT NUMBER,
                                     I_promo_dtl_id          IN     NUMBER,
                                     I_buylists              IN     OBJ_MERCH_NODE_TBL,
                                     I_merch_list_id         IN     NUMBER,
                                     I_merch_list_desc       IN     VARCHAR2,
                                     I_user_id               IN     VARCHAR2)
RETURN NUMBER IS

   L_program         VARCHAR2(60) := 'RPM_PRICE_EVENT_ITEM_LIST_SQL.VALIDATE_TXN_EXCLUSION_PEIL';
   L_item_orig_count NUMBER       := 0;

BEGIN

   O_some_data_removed_ind := 0;
   O_all_data_removed_ind := 0;

   select COUNT(1)
     into L_item_orig_count
     from rpm_merch_list_dtl_ws
    where merch_list_id = I_merch_list_id;

   insert into rpm_merch_list_detail(merch_list_id,
                                     merch_level,
                                     item,
                                     item_desc)
      with peil_items as
         (select distinct im.item,
                 im.item_parent,
                 im.dept,
                 im.class,
                 im.subclass,
                 im.item_level,
                 im.tran_level,
                 im.item_desc,
                 im.diff_1,
                 im.diff_2,
                 im.diff_3,
                 im.diff_4
            from rsm_hierarchy_permission rhp,
                 rsm_role_hierarchy_perm rrhp,
                 rsm_hierarchy_type rht,
                 rsm_user_role rur,
                 subclass sc,
                 rpm_merch_list_dtl_ws rmld,
                 item_master im
           where rhp.reference_class    IN (RPM_CONSTANTS.RSM_REF_CLASS_DEPT,
                                            RPM_CONSTANTS.RSM_REF_CLASS_CLASS,
                                            RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS)
             and rhp.id                 = rrhp.parent_id
             and rrhp.hierarchy_type_id = rht.id
             and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
             and rrhp.role_id           = rur.role_id
             and rur.user_id            = I_user_id
             and rhp.key_value          IN (TO_CHAR(sc.dept),
                                            TO_CHAR(sc.dept||';'||sc.class),
                                            TO_CHAR(sc.dept||';'||sc.class||';'||sc.subclass))
             and rmld.merch_list_id     = I_merch_list_id
             and rmld.item              = im.item
             and im.tran_level         >= im.item_level
             and im.status              = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind        = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.dept                = sc.dept
             and im.class               = sc.class
             and im.subclass            = sc.subclass
             and NOT EXISTS (select 'x'
                               from rpm_merch_list_dtl_ws rmld2
                              where rmld2.item          = im.item_parent
                                and rmld2.merch_list_id = rmld.merch_list_id)) -- validation for items in PEIL
      -- buylist at parent item, list contains child
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi
       where bl.merch_level_type = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and bl.item             = pi.item_parent
      union all
      -- buylist at parent item, list contains parent
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi
       where bl.merch_level_type = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and bl.item             = pi.item
      union all
      -- buylist at parent diff, list contains child
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi
       where bl.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and bl.item             = pi.item_parent
         and bl.diff_id          IN (pi.diff_1,
                                     pi.diff_2,
                                     pi.diff_3,
                                     pi.diff_4)
      union all
      -- buylist at D/C/S, list contains tran item or parent
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi
       where bl.merch_level_type IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                     RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                     RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
         and pi.dept     = bl.dept
         and pi.class    = NVL(bl.class, pi.class)
         and pi.subclass = NVL(bl.subclass, pi.subclass)
      union all
      -- buylist at storewide, list contains tran item or parent - no filtering done here, take the list as it is
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi
       where bl.merch_level_type = RPM_CONSTANTS.STOREWIDE_MERCH_TYPE
      -- buylist at skulist, exclusion list contains matching tran item or parent
      union all
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi,
             skulist_detail sd
       where bl.merch_level_type = RPM_CONSTANTS.ITEM_LIST_TYPE
         and bl.skulist          = sd.skulist
         and sd.item             = pi.item
      -- buylist at skulist, exclusion list contains tran item under a parent
      union all
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi,
             skulist_detail sd
       where bl.merch_level_type = RPM_CONSTANTS.ITEM_LIST_TYPE
         and bl.skulist          = sd.skulist
         and sd.item             = pi.item_parent
         and pi.item_level       = pi.tran_level
      -- buylist at PEIL, exclusion list contains tran item or parent
      union all
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi,
             rpm_merch_list_detail rmld
       where bl.merch_level_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and bl.price_event_itemlist = rmld.merch_list_id
         and rmld.item               = pi.item
      -- buylist at PEIL, exclusion list contains tran item under a parent
      union all
      select I_merch_list_id,
             DECODE(pi.item_level,
                    pi.tran_level, RPM_CONSTANTS.ITEM_MERCH_TYPE,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE) merch_level,
             pi.item,
             pi.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             peil_items pi,
             rpm_merch_list_detail rmld
       where bl.merch_level_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and bl.price_event_itemlist = rmld.merch_list_id
         and rmld.item               = pi.item_parent
         and pi.item_level           = pi.tran_level;

   if SQL%ROWCOUNT = 0 then
      O_all_data_removed_ind := 1;

   elsif SQL%ROWCOUNT < L_item_orig_count then
      O_some_data_removed_ind := 1;

   end if;

   --Only insert into PEIL tables if valid items are present
   if O_all_data_removed_ind = 0 then

      insert into rpm_merch_list_head (merch_list_id,
                                       price_event_id,
                                       price_event_type,
                                       merch_list_desc,
                                       merch_list_type)
         values (I_merch_list_id,
                 I_promo_dtl_id,
                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                 I_merch_list_desc,
                 RPM_CONSTANTS.MERCH_LIST_HEAD_PEIL);

      --Populate rpm_peil_dept_class_subclass with new records
      insert into rpm_peil_dept_class_subclass (merch_list_id,
                                                dept,
                                                class,
                                                subclass)
         select distinct rmld.merch_list_id,
                im.dept,
                im.class,
                im.subclass
           from rpm_merch_list_detail rmld,
                item_master im
          where rmld.merch_list_id = I_merch_list_id
            and rmld.item          = im.item;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END VALIDATE_TXN_EXCLUSION_PEIL;
--------------------------------------------------------------------------------

FUNCTION PURGE_MERCH_LIST_DTL_WS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_PRICE_EVENT_ITEM_LIST_SQL.PURGE_MERCH_LIST_DTL_WS';

BEGIN

   EXECUTE IMMEDIATE 'truncate table rpm_merch_list_dtl_ws';

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END PURGE_MERCH_LIST_DTL_WS;
--------------------------------------------------------------------------------
FUNCTION PURGE_LIST(O_error_msg        OUT VARCHAR2,
                    I_merch_list_id IN     NUMBER)
RETURN NUMBER IS

   L_program           VARCHAR2(45)           := 'RPM_PRICE_EVENT_ITEM_LIST_SQL.PURGE_LIST';

BEGIN

   delete
     from rpm_merch_list_head rmlh
    where merch_list_id = I_merch_list_id;

   delete
     from rpm_merch_list_detail
    where merch_list_id = I_merch_list_id;

   delete
     from rpm_peil_dept_class_subclass
    where merch_list_id = I_merch_list_id;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END PURGE_LIST;

--------------------------------------------------------------------------------

FUNCTION COPY_LIST(O_error_msg           OUT VARCHAR2,
                   O_new_list_id         OUT NUMBER,
                   O_valid_result        OUT NUMBER,
                   I_existing_list_id IN     NUMBER,
                   I_new_pe_id        IN     NUMBER,
                   I_pe_type          IN     VARCHAR2,
                   I_user_id          IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_PRICE_EVENT_ITEM_LIST_SQL.COPY_LIST';

   L_existing_list_count NUMBER := 0;

BEGIN

   O_new_list_id := RPM_MERCH_LIST_ID_SEQ.NEXTVAL;

   select count(*)
     into L_existing_list_count
     from rpm_merch_list_detail
    where merch_list_id = I_existing_list_id;

   insert into rpm_merch_list_detail
      (merch_list_id,
       merch_level,
       item,
       item_desc,
       diff_id,
       diff_desc)
      select O_new_list_id,
             rmld.merch_level,
             rmld.item,
             rmld.item_desc,
             rmld.diff_id,
             rmld.diff_desc
        from rpm_merch_list_detail rmld,
             item_master im,
             (select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_DEPT
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = I_user_id
                 and sc.dept                = TO_NUMBER(rhp.key_value)
              union
              select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_CLASS
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = I_user_id
                 and rhp.key_value          = TO_CHAR(sc.dept||';'||sc.class)
              union
              select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = I_user_id
                 and rhp.key_value          = TO_CHAR(sc.dept||';'||sc.class||';'||sc.subclass)) sec
       where rmld.merch_list_id = I_existing_list_id
         and rmld.item          = im.item
         and im.dept            = sec.dept
         and im.class           = sec.class
         and im.subclass        = sec.subclass;

   if SQL%FOUND then

      if SQL%ROWCOUNT != L_existing_list_count then
         O_valid_result := LP_peil_some_valid_items;
      else
         O_valid_result := LP_peil_all_valid_items;
      end if;

      insert into rpm_merch_list_head
         (merch_list_id,
          price_event_id,
          price_event_type,
          merch_list_desc,
          merch_list_type)
         select O_new_list_id,
                I_new_pe_id,
                I_pe_type,
                merch_list_desc,
                RPM_CONSTANTS.MERCH_LIST_HEAD_PEIL
           from rpm_merch_list_head
          where merch_list_id = I_existing_list_id;

      -- Populate rpm_peil_dept_class_subclass with new records
      insert into rpm_peil_dept_class_subclass
         (merch_list_id,
          dept,
          class,
          subclass)
      select distinct
             rmld.merch_list_id,
             im.dept,
             im.class,
             im.subclass
        from rpm_merch_list_detail rmld,
             item_master im
       where rmld.merch_list_id = O_new_list_id
         and rmld.item          = im.item;

   else

      -- If nothing is on the list based on permissions, set the results indicator and clear out the list id

      O_valid_result := LP_peil_no_valid_items;
      O_new_list_id := NULL;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END COPY_LIST;

--------------------------------------------------------------------------------
FUNCTION GET_PEIL_DCS(O_error_message     OUT VARCHAR2,
                      O_merch_data        OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                      I_merch_list_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_PRICE_EVENT_ITEM_LIST_SQL.GET_PEIL_DCS';

   cursor C_GET_PEIL_DCS is
      select /*+ CARDINALITY(ids 10) */
             OBJ_MERCH_ZONE_ND_DESCS_REC(NULL, --zone_node_type
                                         NULL, --location,
                                         NULL, --loc_desc,
                                         NULL, --zone_id,
                                         NULL, --zone_display_id,
                                         NULL, --zone_desc,
                                         NULL, --merch_type,
                                         rpdcs.dept,
                                         d.dept_name,
                                         rpdcs.class,
                                         c.class_name,
                                         rpdcs.subclass,
                                         sc.sub_name,
                                         NULL, --item,
                                         NULL, --item_desc,
                                         NULL, --diff_id,
                                         NULL, --diff_desc,
                                         NULL, --skulist,
                                         NULL, --skulist_desc,
                                         NULL, --price_event_itemlist,
                                         NULL) --price_event_itemlist_desc
        from table(cast(I_merch_list_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_peil_dept_class_subclass rpdcs,
             deps d,
             class c,
             subclass sc
       where rpdcs.merch_list_id = VALUE(ids)
         and rpdcs.dept          = d.dept
         and rpdcs.dept          = c.dept
         and rpdcs.class         = c.class
         and rpdcs.dept          = sc.dept
         and rpdcs.class         = sc.class
         and rpdcs.subclass      = sc.subclass
         and d.dept              = c.dept
         and d.dept              = sc.dept
         and c.dept              = sc.dept
         and c.class             = sc.class;

BEGIN

   open C_GET_PEIL_DCS;
   fetch C_GET_PEIL_DCS BULK COLLECT into O_merch_data;
   close C_GET_PEIL_DCS;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return 0;

END GET_PEIL_DCS;

--------------------------------------------------------------------------------

END RPM_PRICE_EVENT_ITEM_LIST_SQL;
/
