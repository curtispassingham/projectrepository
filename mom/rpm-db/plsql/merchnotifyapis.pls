CREATE OR REPLACE PACKAGE MERCH_NOTIFY_API_SQL AS
--------------------------------------------------------

FUNCTION NEW_DEPARTMENT(O_error_msg   OUT VARCHAR2,
                        I_dept_id     IN  NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION DELETE_DEPARTMENT(O_error_msg   OUT VARCHAR2,
                           I_dept_id     IN  NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION ITEM_RECLASS(O_error_msg    OUT VARCHAR2,
                      I_item_id      IN  VARCHAR2,
                      I_reclass_date IN DATE,
                      I_new_dept     IN NUMBER,
                      I_new_class    IN NUMBER,
                      I_new_subclass IN NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION NEW_LOCATION(O_error_msg    OUT VARCHAR2,
                      I_new_location IN NUMBER,
                      I_new_loc_type IN VARCHAR2,
                      I_pricing_location IN NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

END MERCH_NOTIFY_API_SQL;
/

