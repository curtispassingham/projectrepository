CREATE OR REPLACE PACKAGE BODY RPM_ROLL_FORWARD_SQL AS
--------------------------------------------------------

LP_vdate DATE    := GET_VDATE;
LP_error BOOLEAN := FALSE;

LP_simple_promo_overlap       NUMBER                           := NULL;
LP_cc_error_tbl               CONFLICT_CHECK_ERROR_TBL         := NULL;
LP_last_currency_code         CURRENCIES.CURRENCY_CODE %TYPE   := NULL;
LP_currency_decimal_precision CURRENCIES.CURRENCY_RTL_DEC%TYPE := NULL;

LP_round_retail_calc_type NUMBER(1) := 0;

FUNCTION RPM_RF_MULTI_UNIT_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_CLEARANCE_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_RANK_PROMOS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_type IN     VARCHAR2)

RETURN NUMBER;

FUNCTION RPM_RF_SELLING_UOM_CARRIER(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_CLEAR_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_PROMO_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_CS_PROMO_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_SELLING_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_CLEARANCE_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_PROMOTION_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RPM_RF_CS_PROMOTION_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION APPLY_PRICE_GUIDE(I_selling_retail IN     NUMBER,
                           I_price_guide_id IN     NUMBER,
                           I_dept_id        IN     NUMBER)
RETURN NUMBER;

------------------------------------------------------------------------------------------------

FUNCTION EXECUTE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                 I_price_event_type IN     VARCHAR2,
                 I_nil_ind          IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_ROLL_FORWARD_SQL.EXECUTE';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_price_event_type: '|| I_price_event_type);

   select simple_promo_overlap_rule
     into LP_simple_promo_overlap
     from rpm_system_options;

   if I_nil_ind = 1 then

      merge /*+ INDEX(target, RPM_FUTURE_RETAIL_GTT_I1)*/ into rpm_future_retail_gtt target
      using (select price_event_id,
                    item,
                    diff_id,
                    location,
                    zone_node_type,
                    action_date,
                    ROW_NUMBER() OVER (PARTITION BY item,
                                                    NVL(diff_id, '-999'),
                                                    location,
                                                    zone_node_type
                                           ORDER BY action_date asc) - 1 rank_value
               from rpm_future_retail_gtt) source
      on (    target.price_event_id       = source.price_event_id
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.action_date          = source.action_date)
      when MATCHED then
         update
            set timeline_seq    = source.rank_value,
                step_identifier = RPM_CONSTANTS.CAPT_GTT_RF_TL_SEQ;

   else

      merge /*+ INDEX(target, RPM_FUTURE_RETAIL_GTT_I1)*/
      into rpm_future_retail_gtt target
      using (select price_event_id,
                    item,
                    diff_id,
                    location,
                    zone_node_type,
                    action_date,
                    ROW_NUMBER() OVER (PARTITION BY item,
                                                    NVL(diff_id, '-999'),
                                                    location,
                                                    zone_node_type
                                           ORDER BY action_date asc) rank_value
               from rpm_future_retail_gtt
              where action_date >= LP_vdate
             -- getting the seed record
             union all
             select seed_data.price_event_id,
                    seed_data.item,
                    seed_data.diff_id,
                    seed_data.location,
                    seed_data.zone_node_type,
                    seed_data.action_date,
                    seed_data.rank_value
               from (select /*+ INDEX(RPM_FUTURE_RETAIL_GTT_I1)*/
                            price_event_id,
                            item,
                            diff_id,
                            location,
                            zone_node_type,
                            action_date,
                            ROW_NUMBER() OVER (PARTITION BY item,
                                                            NVL(diff_id, '-999'),
                                                            location,
                                                            zone_node_type
                                                   ORDER BY action_date desc) - 1 rank_value
                       from rpm_future_retail_gtt
                      where action_date < LP_vdate) seed_data
              where seed_data.rank_value = 0) source
      on (    target.price_event_id       = source.price_event_id
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.action_date          = source.action_date)
      when MATCHED then
         update
            set timeline_seq    = source.rank_value,
                step_identifier = RPM_CONSTANTS.CAPT_GTT_RF_TL_SEQ;

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_TL_SEQ,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   if RPM_RF_MULTI_UNIT_RETAIL(O_cc_error_tbl,
                               I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_RF_CLEARANCE_CARRIER(O_cc_error_tbl,
                               I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_RF_RANK_PROMOS (O_cc_error_tbl,
                          I_price_event_type) = 0 then
      return 0;
   end if;


   if RPM_RF_SELLING_UOM_CARRIER(O_cc_error_tbl,
                                 I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_RF_CLEAR_UOM_CARRIER(O_cc_error_tbl,
                               I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_RF_PROMO_UOM_CARRIER(O_cc_error_tbl,
                               I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_RF_CS_PROMO_UOM_CARRIER(O_cc_error_tbl,
                                  I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_RF_SELLING_RETAIL(O_cc_error_tbl,
                            I_price_event_type) = 0 then
      return 0;
   end if;

   if LP_error then
      O_cc_error_tbl := LP_cc_error_tbl;
      return 0;
   end if;

   if RPM_RF_CLEARANCE_RETAIL(O_cc_error_tbl,
                              I_price_event_type) = 0 then
      return 0;
   end if;

   if LP_error then
      O_cc_error_tbl := LP_cc_error_tbl;
      return 0;
   end if;


   if RPM_RF_PROMOTION_RETAIL(O_cc_error_tbl,
                              I_price_event_type) = 0 then
      return 0;
   end if;

   if LP_error then
      O_cc_error_tbl := LP_cc_error_tbl;
      return 0;
   end if;

   if RPM_RF_CS_PROMOTION_RETAIL(O_cc_error_tbl,
                                 I_price_event_type) = 0 then
      return 0;
   end if;

   if LP_error then
      O_cc_error_tbl := LP_cc_error_tbl;
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program || ' - I_price_event_type: '|| I_price_event_type,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                   SQLERRM,
                                                                   L_program,
                                                                   TO_CHAR(SQLCODE))));
      return 0;

END EXECUTE;

---------------------------------------------------------------------------------------------------
FUNCTION RPM_RF_MULTI_UNIT_RETAIL(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_MULTI_UNIT_RETAIL';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 new_multi_units,
                 new_multi_unit_retail,
                 new_multi_unit_retail_currency,
                 new_multi_selling_uom
            from rpm_future_retail_gtt gtt
           where gtt.timeline_seq       is NOT NULL
             and (   gtt.timeline_seq   <= 1
                  or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                            RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE))
           model
              partition by (price_event_id,
                            item,
                            diff_id,
                            location,
                            zone_node_type)
              dimension by (timeline_seq)
              measures (gtt.multi_units                 new_multi_units,
                        gtt.multi_unit_retail           new_multi_unit_retail,
                        gtt.multi_unit_retail_currency  new_multi_unit_retail_currency,
                        gtt.multi_selling_uom           new_multi_selling_uom,
                        gtt.price_change_id,
                        gtt.pc_multi_units,
                        gtt.pc_multi_unit_retail,
                        gtt.pc_multi_unit_retail_currency,
                        gtt.pc_multi_selling_uom)
              rules sequential order (
                     new_multi_units[timeline_seq] order by timeline_seq =
                        case
                           when CV(timeline_seq) = 0 then
                              new_multi_units[CV(timeline_seq)]
                           when price_change_id[CV(timeline_seq)] is NOT NULL then
                              ---
                              pc_multi_units[CV(timeline_seq)]
                           else
                              new_multi_units[CV(timeline_seq) - 1]
                        end,
                     new_multi_unit_retail[timeline_seq] order by timeline_seq =
                        case
                           when CV(timeline_seq) = 0 then
                              new_multi_unit_retail[CV(timeline_seq)]
                           when price_change_id[CV(timeline_seq)] is NOT NULL then
                              ---
                              pc_multi_unit_retail[CV(timeline_seq)]
                           else
                              new_multi_unit_retail[CV(timeline_seq) - 1]
                        end,
                     new_multi_unit_retail_currency[timeline_seq] order by timeline_seq =
                        case
                           when CV(timeline_seq) = 0 then
                              new_multi_unit_retail_currency[CV(timeline_seq)]
                           when price_change_id[CV(timeline_seq)] is NOT NULL then
                              ---
                              pc_multi_unit_retail_currency[CV(timeline_seq)]
                           else
                              new_multi_unit_retail_currency[CV(timeline_seq) - 1]
                        end,
                     new_multi_selling_uom[timeline_seq] order by timeline_seq =
                        case
                           when CV(timeline_seq) = 0 then
                              new_multi_selling_uom[CV(timeline_seq)]
                           when price_change_id[CV(timeline_seq)] is NOT NULL then
                              ---
                              pc_multi_selling_uom[CV(timeline_seq)]
                           else
                              new_multi_selling_uom[CV(timeline_seq) - 1]
                        end)
           order by timeline_seq) source
      on (    target.price_event_id       = source.price_event_id
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.timeline_seq         = source.timeline_seq
          and target.timeline_seq        != 0)
    when MATCHED then
       update
          set target.multi_units                = source.new_multi_units,
              target.multi_unit_retail          = source.new_multi_unit_retail,
              target.multi_unit_retail_currency = source.new_multi_unit_retail_currency,
              target.multi_selling_uom          = source.new_multi_selling_uom,
              target.step_identifier            = RPM_CONSTANTS.CAPT_GTT_RF_MU_RETAIL;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_MU_RETAIL,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_MULTI_UNIT_RETAIL;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_CLEARANCE_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_CLEARANCE_CARRIER';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 old_clearance_id,
                 old_clear_start_ind,
                 out_string
            from (select price_event_id,
                         item,
                         diff_id,
                         location,
                         zone_node_type,
                         timeline_seq,
                         clearance_id old_clearance_id,
                         clear_start_ind old_clear_start_ind,
                         clearance_id||'~'||clear_start_ind||'~'||0  out_string
                    from rpm_future_retail_gtt gtt
                   where gtt.timeline_seq       is NOT NULL
                     and (   gtt.timeline_seq   <= 1
                          or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                    RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                                    RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                                                    RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)))
          model
          partition by (price_event_id,
                        item,
                        diff_id,
                        location,
                        zone_node_type)
          dimension by (timeline_seq)
          measures (old_clearance_id,
                    old_clear_start_ind,
                    out_string)
          rules sequential order (
             out_string[timeline_seq] ORDER BY timeline_seq =
                case
                   when CV(timeline_seq) = 0 then
                      out_string[CV(timeline_seq)]
                   when old_clearance_id[CV(timeline_seq)] is NULL and
                      (-- if previous clearance_id is NULL
                       SUBSTR(out_string[CV(timeline_seq) - 1],
                              1,
                              INSTR(out_string[CV(timeline_seq)-1], '~', 1) - 1) IS NULL or
                       -- if previous clear_start_ind = END_IND
                       SUBSTR(out_string[CV(timeline_seq) - 1],
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1) + 1,
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1, 2) - 1 -
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1)) = RPM_CONSTANTS.END_IND) then
                      ---
                      old_clearance_id[CV(timeline_seq)]||'~'||old_clear_start_ind[CV(timeline_seq)]||'~'||1
                      ---
                   when old_clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.IN_PROCESS_IND and
                      (-- if previous clearance_id is NULL
                       SUBSTR(out_string[CV(timeline_seq) - 1],
                              1,
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1) - 1) IS NULL or
                       -- if previous clear_start_ind = END_IND
                       SUBSTR(out_string[CV(timeline_seq) - 1],
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1) + 1,
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1, 2) - 1 -
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1)) = RPM_CONSTANTS.END_IND) then
                      ---
                      NULL||'~'||NULL||'~'||2
                      ---
                   when old_clearance_id[CV(timeline_seq)] IS NULL and
                      -- if previous clear_start_ind in (START_IND, IN_PROCESS_IND)
                      SUBSTR(out_string[CV(timeline_seq) - 1],
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1) + 1,
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1, 2) - 1 -
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1)) IN (RPM_CONSTANTS.START_IND,
                                                                                  RPM_CONSTANTS.IN_PROCESS_IND) then
                      ---
                      SUBSTR(out_string[CV(timeline_seq) - 1],
                             1,
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1) - 1)||'~'||RPM_CONSTANTS.IN_PROCESS_IND||'~'||3
                      ---
                   when old_clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.IN_PROCESS_IND and
                      -- if previous clearance_id is NOT NULL
                      SUBSTR(out_string[CV(timeline_seq) - 1],
                             1,
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1) - 1) is NOT NULL and
                      -- if current old_clearance_id != previous clearance_id
                      old_clearance_id[CV(timeline_seq)] != SUBSTR(out_string[CV(timeline_seq) - 1],
                                                                   1,
                                                                   INSTR(out_string[CV(timeline_seq) - 1], '~', 1) - 1) then
                      ---
                      SUBSTR(out_string[CV(timeline_seq) - 1],
                             1,
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1) - 1)||'~'||RPM_CONSTANTS.IN_PROCESS_IND||'~'||4
                      ---
                   when old_clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.START_IND and
                      (-- if previous clearance_id is NULL
                       SUBSTR(out_string[CV(timeline_seq) - 1],
                              1,
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1) - 1) is NULL or
                       -- if previous clear_start_ind = END_IND
                       SUBSTR(out_string[CV(timeline_seq) - 1],
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1) + 1,
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1, 2) - 1 -
                              INSTR(out_string[CV(timeline_seq) - 1], '~', 1)) = RPM_CONSTANTS.END_IND) then
                      ---
                      old_clearance_id[CV(timeline_seq)]||'~'||old_clear_start_ind[CV(timeline_seq)]||'~'||5
                      ---
                   when old_clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.START_IND and
                      -- if previous clear_start_ind in (START_IND, IN_PROCESS_IND)
                      SUBSTR(out_string[CV(timeline_seq) - 1],
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1) + 1,
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1, 2) - 1 -
                             INSTR(out_string[CV(timeline_seq) - 1], '~', 1)) IN (RPM_CONSTANTS.START_IND,
                                                                                  RPM_CONSTANTS.IN_PROCESS_IND) then
                      ---
                      old_clearance_id[CV(timeline_seq)]||'~'||old_clear_start_ind[CV(timeline_seq)]||'~'||6
                      ---
                   when old_clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.END_IND then
                      ---
                      old_clearance_id[CV(timeline_seq)]||'~'||old_clear_start_ind[CV(timeline_seq)]||'~'||7
                      ---
                   else
                      old_clearance_id[CV(timeline_seq)]||'~'||old_clear_start_ind[CV(timeline_seq)]||'~'||8
                end)
          order by timeline_seq) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.timeline_seq         = source.timeline_seq
       and target.timeline_seq        != 0)
   when MATCHED then
      update
         set target.clearance_id    = SUBSTR(out_string,
                                             1,
                                             INSTR(out_string, '~', 1) - 1),
             target.clear_start_ind = SUBSTR(out_string,
                                             INSTR(out_string, '~', 1) + 1,
                                             INSTR(out_string, '~', 1, 2) - 1 - INSTR(out_string, '~', 1)),
             target.lock_version    = SUBSTR(out_string,
                                             INSTR(out_string, '~', 1, 2) + 1),
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RF_CLR_CARRIER_1;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_CLR_CARRIER_1,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 new_clearance_display_id,
                 old_clearance_display_id,
                 new_clear_mkdn_index,
                 old_clear_mkdn_index,
                 new_clear_change_type,
                 old_clear_change_type,
                 new_clear_change_amount,
                 old_clear_change_amount,
                 new_clear_change_currency,
                 old_clear_change_currency,
                 new_clear_change_percent,
                 old_clear_change_percent,
                 new_clear_change_selling_uom,
                 old_clear_change_selling_uom,
                 new_clear_price_guide_id,
                 old_clear_price_guide_id,
                 condition_code
            from (select price_event_id,
                         item,
                         diff_id,
                         location,
                         zone_node_type,
                         timeline_seq,
                         clearance_display_id      new_clearance_display_id,
                         clearance_display_id      old_clearance_display_id,
                         clear_mkdn_index          new_clear_mkdn_index,
                         clear_mkdn_index          old_clear_mkdn_index,
                         clear_change_type         new_clear_change_type,
                         clear_change_type         old_clear_change_type,
                         clear_change_amount       new_clear_change_amount,
                         clear_change_amount       old_clear_change_amount,
                         clear_change_currency     new_clear_change_currency,
                         clear_change_currency     old_clear_change_currency,
                         clear_change_percent      new_clear_change_percent,
                         clear_change_percent      old_clear_change_percent,
                         clear_change_selling_uom  new_clear_change_selling_uom,
                         clear_change_selling_uom  old_clear_change_selling_uom,
                         clear_price_guide_id      new_clear_price_guide_id,
                         clear_price_guide_id      old_clear_price_guide_id,
                         lock_version              condition_code
                    from rpm_future_retail_gtt gtt
                   where gtt.timeline_seq       is NOT NULL
                     and (   gtt.timeline_seq   <= 1
                          or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                    RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                                    RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                                                    RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)))
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        zone_node_type)
          dimension by (timeline_seq)
          measures (new_clearance_display_id,
                    old_clearance_display_id,
                    new_clear_mkdn_index,
                    old_clear_mkdn_index,
                    new_clear_change_type,
                    old_clear_change_type,
                    new_clear_change_amount,
                    old_clear_change_amount,
                    new_clear_change_currency,
                    old_clear_change_currency,
                    new_clear_change_percent,
                    old_clear_change_percent,
                    new_clear_change_selling_uom,
                    old_clear_change_selling_uom,
                    new_clear_price_guide_id,
                    old_clear_price_guide_id,
                    condition_code)
           rules sequential order (
              new_clearance_display_id[timeline_seq] ORDER BY timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clearance_display_id[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clearance_display_id[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clearance_display_id[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clearance_display_id[CV(timeline_seq) - 1]
                    else
                       old_clearance_display_id[CV(timeline_seq)]
                 end,
              new_clear_mkdn_index[timeline_seq] order by timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_mkdn_index[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_mkdn_index[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_mkdn_index[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_mkdn_index[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 5 then
                       ---
                       1
                    when condition_code[CV(timeline_seq)] = 6 then
                       ---
                       new_clear_mkdn_index[CV(timeline_seq) - 1] + 1
                    when condition_code[CV(timeline_seq)] = 7 then
                       0
                    else
                       old_clear_mkdn_index[CV(timeline_seq)]
                 end,
              new_clear_change_type[timeline_seq] order by timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_change_type[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_change_type[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_change_type[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_change_type[CV(timeline_seq) - 1]
                    else
                       old_clear_change_type[CV(timeline_seq)]
                 end,
              new_clear_change_amount[timeline_seq] ORDER BY timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_change_amount[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_change_amount[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_change_amount[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_change_amount[CV(timeline_seq) - 1]
                    else
                       old_clear_change_amount[CV(timeline_seq)]
                 end,
              new_clear_change_currency[timeline_seq] order by timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_change_currency[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_change_currency[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_change_currency[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_change_currency[CV(timeline_seq) - 1]
                    else
                       old_clear_change_currency[CV(timeline_seq)]
                 end,
              new_clear_change_percent[timeline_seq] order by timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_change_percent[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_change_percent[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_change_percent[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_change_percent[CV(timeline_seq) - 1]
                    else
                       old_clear_change_percent[CV(timeline_seq)]
                 end,
              new_clear_change_selling_uom[timeline_seq] order by timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_change_selling_uom[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_change_selling_uom[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_change_selling_uom[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_change_selling_uom[CV(timeline_seq) - 1]
                    else
                       old_clear_change_selling_uom[CV(timeline_seq)]
                 end,
              new_clear_price_guide_id[timeline_seq] order by timeline_seq =
                 case
                    when CV(timeline_seq) = 0 then
                       old_clear_price_guide_id[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 1 then
                       ---
                       old_clear_price_guide_id[CV(timeline_seq)]
                    when condition_code[CV(timeline_seq)] = 2 then
                       ---
                       NULL
                    when condition_code[CV(timeline_seq)] = 3 then
                       ---
                       new_clear_price_guide_id[CV(timeline_seq) - 1]
                    when condition_code[CV(timeline_seq)] = 4 then
                       ---
                       new_clear_price_guide_id[CV(timeline_seq) - 1]
                    else
                       old_clear_price_guide_id[CV(timeline_seq)]
                 end)
          order by timeline_seq) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.timeline_seq         = source.timeline_seq
       and target.timeline_seq        != 0)
    when MATCHED then
       update
          set target.clearance_display_id     = source.new_clearance_display_id,
              target.clear_mkdn_index         = source.new_clear_mkdn_index,
              target.clear_change_type        = source.new_clear_change_type,
              target.clear_change_amount      = source.new_clear_change_amount,
              target.clear_change_currency    = source.new_clear_change_currency,
              target.clear_change_percent     = source.new_clear_change_percent,
              target.clear_change_selling_uom = source.new_clear_change_selling_uom,
              target.clear_price_guide_id     = source.new_clear_price_guide_id,
              target.step_identifier          = RPM_CONSTANTS.CAPT_GTT_RF_CLR_CARRIER_2;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_CLR_CARRIER_2,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_CLEARANCE_CARRIER;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_RANK_PROMOS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_type IN     VARCHAR2)

RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_RANK_PROMOS';

   L_promo_apply_order RPM_SYSTEM_OPTIONS.PROMO_APPLY_ORDER%TYPE := NULL;

   L_error_msg VARCHAR2(255) := NULL;
   L_rank_type NUMBER        := NULL;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE) then

      insert into rpm_cust_segment_promo_fr_gtt
         (price_event_id,
          cust_segment_promo_id,
          item,
          diff_id,
          location,
          zone_node_type,
          action_date,
          customer_type,
          dept,
          complex_promo_ind,
          zone_id,
          item_parent,
          cur_hier_level,
          max_hier_level,
          step_identifier)
      select price_event_id,
             RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
             item,
             diff_id,
             location,
             zone_node_type,
             action_date,
             customer_type,
             dept,
             0,
             zone_id,
             item_parent,
             cur_hier_level,
             max_hier_level,
             RPM_CONSTANTS.CAPT_GTT_RF_RANK_PROMOS
        from (select distinct frgtt.price_event_id,
                     frgtt.item,
                     frgtt.diff_id,
                     frgtt.location,
                     frgtt.zone_node_type,
                     frgtt.action_date,
                     cspfg.customer_type,
                     frgtt.dept,
                     frgtt.zone_id,
                     frgtt.item_parent,
                     frgtt.cur_hier_level,
                     frgtt.max_hier_level
                from rpm_future_retail_gtt frgtt,
                     rpm_cust_segment_promo_fr_gtt cspfg
               where cspfg.item                 = frgtt.item
                 and NVL(cspfg.diff_id, '-999') = NVL(frgtt.diff_id, '-999')
                 and cspfg.location             = frgtt.location
                 and cspfg.zone_node_type       = frgtt.zone_node_type
                 and cspfg.dept                 = frgtt.dept
                 and frgtt.timeline_seq        != 0
                 and NOT EXISTS (select /*+ INDEX(CSPFG1 RPM_CUST_SEGMENT_PROMO_GTT_I1) */
                                        'x'
                                   from rpm_cust_segment_promo_fr_gtt cspfg1
                                  where cspfg1.price_event_id       = frgtt.price_event_id
                                    and cspfg1.item                 = frgtt.item
                                    and NVL(cspfg1.diff_id, '-999') = NVL(frgtt.diff_id, '-999')
                                    and cspfg1.location             = frgtt.location
                                    and cspfg1.zone_node_type       = frgtt.zone_node_type
                                    and cspfg1.action_date          = frgtt.action_date
                                    and cspfg1.dept                 = frgtt.dept
                                    and cspfg1.customer_type        = cspfg.customer_type));

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_msg,
                                                      RPM_CONSTANTS.CAPT_GTT_RF_RANK_PROMOS,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      1, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_msg));
         return 0;

      end if;

   end if;

   --- populate the rpm_promo_fr_item_loc_gtt table first
   --  with data from both the rpm_future_retail_gtt and
   --  rpm_promo_item_loc_expl tables.

   delete from rpm_promo_fr_item_loc_gtt;

   insert into rpm_promo_fr_item_loc_gtt
      (item,
       diff_id,
       dept,
       class,
       subclass,
       location,
       zone_node_type,
       action_date,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       comp_display_id,
       promo_comp_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       promo_comp_msg_type,
       promo_calculated_rank_value,
       comp_calculated_rank_value,
       exception_parent_id,
       price_event_id,
       deleted_ind)
      select item,
             diff_id,
             dept,
             class,
             subclass,
             location,
             zone_node_type,
             action_date,
             promo_id,
             promo_display_id,
             promo_secondary_ind,
             comp_display_id,
             promo_comp_id,
             promo_dtl_id,
             type,
             customer_type,
             detail_secondary_ind,
             detail_start_date,
             detail_end_date,
             detail_apply_to_code,
             detail_change_type,
             detail_change_amount,
             detail_change_currency,
             detail_change_percent,
             detail_change_selling_uom,
             detail_price_guide_id,
             promo_comp_msg_type,
             promo_calculated_rank_value,
             comp_calculated_rank_value,
             exception_parent_id,
             price_event_id,
             deleted_ind
       from (select gtt.item,
                    gtt.diff_id,
                    gtt.dept,
                    gtt.class,
                    gtt.subclass,
                    gtt.location,
                    gtt.zone_node_type,
                    gtt.action_date,
                    rpile.promo_id,
                    rpile.promo_display_id,
                    rpile.promo_secondary_ind,
                    rpile.comp_display_id,
                    rpile.promo_comp_id,
                    rpile.promo_dtl_id,
                    rpile.type,
                    NULL customer_type,
                    rpile.detail_secondary_ind,
                    rpile.detail_start_date,
                    rpile.detail_end_date,
                    rpile.detail_apply_to_code,
                    rpile.detail_change_type,
                    rpile.detail_change_amount,
                    rpile.detail_change_currency,
                    rpile.detail_change_percent,
                    rpile.detail_change_selling_uom,
                    rpile.detail_price_guide_id,
                    rpile.promo_comp_msg_type,
                    1 promo_calculated_rank_value,
                    1 comp_calculated_rank_value,
                    rpile.exception_parent_id,
                    gtt.price_event_id,
                    rpile.deleted_ind
               from rpm_future_retail_gtt gtt,
                    rpm_promo_item_loc_expl_gtt rpile
              where gtt.item                 = rpile.item (+)
                and NVL(gtt.diff_id, '-999') = NVL(rpile.diff_id (+), '-999')
                and gtt.location             = rpile.location (+)
                and gtt.zone_node_type       = rpile.zone_node_type (+)
                and gtt.price_event_id       = rpile.price_event_id (+)
                and gtt.timeline_seq         > 0
                and rpile.customer_type (+) is NULL
                and gtt.action_date         >= rpile.detail_start_date (+)
                and gtt.action_date         <= NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
             union all
             select cspfg.item,
                    cspfg.diff_id,
                    cspfg.dept,
                    rpile.class,
                    rpile.subclass,
                    cspfg.location,
                    cspfg.zone_node_type,
                    cspfg.action_date,
                    rpile.promo_id,
                    rpile.promo_display_id,
                    rpile.promo_secondary_ind,
                    rpile.comp_display_id,
                    rpile.promo_comp_id,
                    rpile.promo_dtl_id,
                    rpile.type,
                    rpile.customer_type,
                    rpile.detail_secondary_ind,
                    rpile.detail_start_date,
                    rpile.detail_end_date,
                    rpile.detail_apply_to_code,
                    rpile.detail_change_type,
                    rpile.detail_change_amount,
                    rpile.detail_change_currency,
                    rpile.detail_change_percent,
                    rpile.detail_change_selling_uom,
                    rpile.detail_price_guide_id,
                    rpile.promo_comp_msg_type,
                    1 promo_calculated_rank_value,
                    1 comp_calculated_rank_value,
                    rpile.exception_parent_id,
                    cspfg.price_event_id,
                    rpile.deleted_ind
               from rpm_cust_segment_promo_fr_gtt cspfg,
                    rpm_promo_item_loc_expl_gtt rpile
              where cspfg.item                 = rpile.item
                and NVL(cspfg.diff_id, '-999') = NVL(rpile.diff_id (+), '-999')
                and cspfg.location             = rpile.location
                and cspfg.zone_node_type       = rpile.zone_node_type
                and cspfg.price_event_id       = rpile.price_event_id
                and rpile.customer_type       is NOT NULL
                and cspfg.customer_type        = rpile.customer_type
                and cspfg.dept                 = rpile.dept
                and cspfg.action_date         >= rpile.detail_start_date
                and cspfg.action_date         <= NVL(rpile.detail_end_date, TO_DATE('3000', 'YYYY')) + RPM_CONSTANTS.ONE_MINUTE
             union all
             select gtt.item,
                    gtt.diff_id,
                    gtt.dept,
                    gtt.class,
                    gtt.subclass,
                    gtt.location,
                    gtt.zone_node_type,
                    gtt.action_date,
                    NULL promo_id,
                    NULL promo_display_id,
                    NULL promo_secondary_ind,
                    NULL comp_display_id,
                    NULL promo_comp_id,
                    NULL promo_dtl_id,
                    NULL type,
                    NULL customer_type,
                    NULL detail_secondary_ind,
                    NULL detail_start_date,
                    NULL detail_end_date,
                    NULL detail_apply_to_code,
                    NULL detail_change_type,
                    NULL detail_change_amount,
                    NULL detail_change_currency,
                    NULL detail_change_percent,
                    NULL detail_change_selling_uom,
                    NULL detail_price_guide_id,
                    NULL promo_comp_msg_type,
                    NULL promo_calculated_rank_value,
                    NULL comp_calculated_rank_value,
                    NULL exception_parent_id,
                    gtt.price_event_id,
                    NULL deleted_ind
               from rpm_future_retail_gtt gtt
              where gtt.timeline_seq = 0);

   if LP_simple_promo_overlap = RPM_CONSTANTS.PR_OVERLAP_COMPOUNDING then
      --- Gather some needed info

      if RPM_SYSTEM_OPTIONS_SQL.GET_PROMO_APPLY_ORDER(L_promo_apply_order,
                                                      L_error_msg) != TRUE then
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_msg));
      end if;

      if L_promo_apply_order = RPM_CONSTANTS.PROMO_ORDER_BY_AMOUNT then
         L_rank_type := RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE;
      elsif L_promo_apply_order = RPM_CONSTANTS.PROMO_ORDER_BY_PERCENT then
         L_rank_type := RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE;
      end if;

      --- calculate the two initial promo/comp rankings and put
      --  the values back into the table

      merge /*+ USE_NL(target) */ into rpm_promo_fr_item_loc_gtt target
      using (select t.price_event_id,
                    t.item,
                    t.diff_id,
                    t.location,
                    t.zone_node_type,
                    t.action_date,
                    t.promo_id,
                    t.promo_comp_id,
                    t.promo_dtl_id,
                    fixed_price,
                    SUM(fixed_price) OVER (PARTITION BY price_event_id,
                                                        item,
                                                        diff_id,
                                                        location,
                                                        zone_node_type,
                                                        action_date,
                                                        promo_id
                                               ORDER BY fixed_price desc) summed_fixed_price,
                    secondary_ind,
                    SUM(secondary_ind) OVER (PARTITION BY price_event_id,
                                                          item,
                                                          diff_id,
                                                          location,
                                                          zone_node_type,
                                                          action_date,
                                                          promo_id
                                                 ORDER BY secondary_ind desc) summed_secondary_ind,
                    apply_order,
                    SUM(apply_order) OVER (PARTITION BY price_event_id,
                                                        item,
                                                        diff_id,
                                                        location,
                                                        zone_node_type,
                                                        action_date,
                                                        promo_id
                                               ORDER BY apply_order desc) summed_apply_order
               from (select /*+ INDEX(gtt RPM_PROMO_FR_ITEM_LOC_GTT_I1) */
                            gtt.price_event_id,
                            gtt.item,
                            gtt.diff_id,
                            gtt.location,
                            gtt.zone_node_type,
                            gtt.action_date,
                            gtt.promo_id,
                            gtt.promo_comp_id,
                            gtt.promo_dtl_id,
                            case
                               when gtt.detail_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                                  10000
                               else
                                  0
                            end fixed_price,
                            case
                               when NVL(gtt.detail_secondary_ind,0) = RPM_CONSTANTS.BOOLEAN_FALSE then
                                  1000
                               else
                                  0
                            end secondary_ind,
                            case
                               when gtt.detail_change_type = L_rank_type then
                                  100
                               else
                                  0
                            end apply_order
                       from rpm_promo_fr_item_loc_gtt gtt) t) source
      on (    target.price_event_id       = source.price_event_id
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.action_date          = source.action_date
          and target.promo_id             = source.promo_id
          and target.promo_comp_id        = source.promo_comp_id
          and target.promo_dtl_id         = source.promo_dtl_id)
      when MATCHED then
         update
            set promo_calculated_rank_value = 1000000 - source.summed_fixed_price - source.summed_secondary_ind - source.summed_apply_order,
                comp_calculated_rank_value  = 1000000 - source.fixed_price - source.secondary_ind - source.apply_order;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_RANK_PROMOS;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_SELLING_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_event_type IN     VARCHAR2)

RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_SELLING_UOM_CARRIER';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 new_selling_uom,
                 price_change_id,
                 pc_change_type,
                 pc_change_selling_uom
            from rpm_future_retail_gtt gtt
           where gtt.timeline_seq       is NOT NULL
             and (   gtt.timeline_seq   <= 1
                  or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                            RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE))
           model
           partition by (price_event_id,
                         item,
                         NVL(diff_id, '-999') diff_id,
                         location,
                         zone_node_type)
           dimension by (timeline_seq)
           measures (gtt.selling_uom new_selling_uom,
                     gtt.selling_uom old_selling_uom,
                     gtt.price_change_id,
                     gtt.pc_change_type,
                     gtt.pc_change_selling_uom)
           rules sequential order (
                  new_selling_uom[timeline_seq] order by timeline_seq =
                     case
                        when CV(timeline_seq) = 0 then
                           old_selling_uom[CV(timeline_seq)]
                        when price_change_id[CV(timeline_seq)] is NOT NULL and
                             pc_change_type[CV(timeline_seq)] = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                             pc_change_selling_uom[CV(timeline_seq)] IS NOT NULL then
                           ---
                           pc_change_selling_uom[CV(timeline_seq)]
                        else
                           NVL(new_selling_uom[CV(timeline_seq) - 1], old_selling_uom[CV(timeline_seq)])
                     end
                 )
            order by timeline_seq) source
   on (    target.price_event_id     = source.price_event_id
       and target.item               = source.item
       and NVL(target.diff_id, -999) = NVL(source.diff_id, -999)
       and target.location           = source.location
       and target.zone_node_type     = source.zone_node_type
       and target.timeline_seq       = source.timeline_seq
       and target.timeline_seq      != 0)
   when MATCHED then
      update
         set target.selling_uom     = source.new_selling_uom,
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RF_SELLING_UOM;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_SELLING_UOM,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_SELLING_UOM_CARRIER;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_CLEAR_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_CLEAR_UOM_CARRIER';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_future_retail_gtt target
   using (select timeline_seq,
                 price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 new_clear_uom,
                 clearance_id,
                 clear_change_type,
                 clear_change_selling_uom,
                 selling_uom
            from rpm_future_retail_gtt gtt
           where gtt.timeline_seq       is NOT NULL
             and (   gtt.timeline_seq   <= 1
                  or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                            RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                            RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                            RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                            RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE))
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        zone_node_type)
          dimension by (timeline_seq)
          measures (gtt.clear_uom new_clear_uom,
                    gtt.clearance_id,
                    gtt.clear_change_type,
                    gtt.clear_change_selling_uom,
                    gtt.selling_uom)
          rules sequential order (
             new_clear_uom[timeline_seq] order by timeline_seq =
                case
                   when clearance_id[CV(timeline_seq)] is NOT NULL and
                        clear_change_type[CV(timeline_seq)] = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                        clear_change_selling_uom[CV(timeline_seq)] is NOT NULL then
                      ---
                      clear_change_selling_uom[CV(timeline_seq)]
                   else
                      selling_uom[CV(timeline_seq)]
                end)
          order by timeline_seq) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.timeline_seq         = source.timeline_seq
       and target.timeline_seq        != 0)
   when MATCHED then
      update
         set target.clear_uom       = source.new_clear_uom,
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RF_CLEAR_UOM;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_CLEAR_UOM,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_CLEAR_UOM_CARRIER;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_PROMO_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_PROMO_UOM_CARRIER';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   -- Assume that the VALIDATE_PROMOTION_FOR_MERGE function in RPM_FUTURE_RETAIL_SQL already prevent
   -- Multiple Fixed Price Promotion

   merge /*+ INDEX (target RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 clear_uom,
                 deleted_ind,
                 detail_change_selling_uom,
                 type,
                 rank
            from (select /*+ LEADING(rpile) INDEX (gtt RPM_FUTURE_RETAIL_GTT_I1) */ gtt.price_event_id,
                         gtt.item,
                         gtt.diff_id,
                         gtt.location,
                         gtt.zone_node_type,
                         gtt.timeline_seq,
                         gtt.clear_uom,
                         rpile.deleted_ind,
                         case
                            when rpile.detail_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                                 rpile.customer_type is NULL and
                                 rpile.detail_change_selling_uom is NOT NULL then
                               ---
                               rpile.detail_change_selling_uom
                            else
                               NULL
                         end detail_change_selling_uom,
                         case
                            when rpile.detail_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                                 rpile.customer_type is NULL and
                                 rpile.detail_change_selling_uom is NOT NULL then
                               ---
                               rpile.type
                            else
                               NULL
                         end type,
                         ROW_NUMBER() OVER (PARTITION BY gtt.price_event_id,
                                                         gtt.item,
                                                         NVL(gtt.diff_id, '-999'),
                                                         gtt.location,
                                                         gtt.zone_node_type,
                                                         timeline_seq
                                                ORDER BY clear_uom,
                                                         detail_change_selling_uom) rank
                    from rpm_future_retail_gtt gtt,
                         rpm_promo_fr_item_loc_gtt rpile
                   where (   gtt.timeline_seq         = 1
                          or (    gtt.timeline_seq    > 1
                              and I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                         RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                                         RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE)))
                     and rpile.price_event_id       = gtt.price_event_id
                     and rpile.item                 = gtt.item
                     and NVL(rpile.diff_id, '-999') = NVL(gtt.diff_id, '-999')
                     and rpile.location             = gtt.location
                     and rpile.zone_node_type       = gtt.zone_node_type
                     and rpile.action_date          = gtt.action_date) t
          where t.rank = 1) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.timeline_seq         = source.timeline_seq
       and target.timeline_seq         != 0)
   when MATCHED then
      update
         set target.simple_promo_uom = case
                                          when NVL(source.deleted_ind, 0) != 1 and
                                               source.detail_change_selling_uom is NOT NULL and
                                               source.type = RPM_CONSTANTS.SIMPLE_CODE then
                                             ---
                                             source.detail_change_selling_uom
                                          else
                                             source.clear_uom
                                       end,
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RFR_PROMO_UOM;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RFR_PROMO_UOM,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_PROMO_UOM_CARRIER;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_CS_PROMO_UOM_CARRIER(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_CS_PROMO_UOM_CARRIER';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge /*+ INDEX (target RPM_CUST_SEGMENT_PROMO_GTT_I1) */
    into rpm_cust_segment_promo_fr_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 clear_uom,
                 deleted_ind,
                 detail_change_selling_uom,
                 new_promo_uom,
                 action_date,
                 customer_type,
                 seq,
                 key_rank_count
            from (select price_event_id,
                         item,
                         diff_id,
                         location,
                         zone_node_type,
                         clear_uom,
                         deleted_ind,
                         case
                            when detail_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                                 detail_change_selling_uom is NOT NULL then
                               ---
                               detail_change_selling_uom
                           else
                               NULL
                         end detail_change_selling_uom,
                         promo_uom,
                         action_date,
                         customer_type,
                         RANK() OVER (PARTITION BY price_event_id,
                                                   item,
                                                   NVL(diff_id, '-999'),
                                                   location,
                                                   zone_node_type,
                                                   action_date,
                                                   customer_type
                                          ORDER BY deleted_ind,
                                                   detail_start_date,
                                                   promo_dtl_id) seq,
                         COUNT(1) OVER (PARTITION BY price_event_id,
                                                     item,
                                                     NVL(diff_id, '-999'),
                                                     location,
                                                     zone_node_type,
                                                     action_date,
                                                     customer_type) key_rank_count
                    from (select t.price_event_id,
                                 t.item,
                                 t.diff_id,
                                 t.location,
                                 t.zone_node_type,
                                 t.clear_uom,
                                 rpile.deleted_ind,
                                 rpile.detail_change_type,
                                 rpile.detail_change_selling_uom,
                                 t.promo_uom,
                                 t.cspfg_action_date action_date,
                                 t.customer_type,
                                 rpile.detail_start_date,
                                 rpile.promo_dtl_id
                            from (select price_event_id,
                                         item,
                                         diff_id,
                                         location,
                                         zone_node_type,
                                         clear_uom,
                                         action_date,
                                         promo_uom,
                                         cspfg_action_date,
                                         customer_type,
                                         rank
                                    from (select /*+ INDEX (CSPFG RPM_CUST_SEGMENT_PROMO_GTT_I1) */
                                                 gtt.price_event_id,
                                                 gtt.item,
                                                 gtt.diff_id,
                                                 gtt.location,
                                                 gtt.zone_node_type,
                                                 gtt.clear_uom,
                                                 gtt.action_date,
                                                 cspfg.promo_uom,
                                                 cspfg.action_date cspfg_action_date,
                                                 cspfg.customer_type,
                                                 RANK() OVER (PARTITION BY gtt.price_event_id,
                                                                           gtt.item,
                                                                           NVL(gtt.diff_id, '-999'),
                                                                           gtt.location,
                                                                           gtt.zone_node_type,
                                                                           cspfg.action_date
                                                                  ORDER BY gtt.action_date desc) rank
                                            from rpm_future_retail_gtt gtt,
                                                 rpm_cust_segment_promo_fr_gtt cspfg
                                           where cspfg.price_event_id       = gtt.price_event_id
                                             and cspfg.item                 = gtt.item
                                             and NVL(cspfg.diff_id, '-999') = NVL(gtt.diff_id, '-999')
                                             and cspfg.location             = gtt.location
                                             and cspfg.zone_node_type       = gtt.zone_node_type
                                             and gtt.action_date           <= cspfg.action_date)
                                   where rank = 1) t,
                                 rpm_promo_fr_item_loc_gtt rpile
                           where t.price_event_id       = rpile.price_event_id (+)
                             and t.item                 = rpile.item (+)
                             and NVL(t.diff_id, '-999') = NVL(rpile.diff_id (+), '-999')
                             and t.location             = rpile.location (+)
                             and t.zone_node_type       = rpile.zone_node_type(+)
                             and t.cspfg_action_date    = rpile.action_date (+)
                             and t.customer_type (+)    is NOT NULL
                             and t.customer_type        = rpile.customer_type (+)))
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        zone_node_type,
                        customer_type,
                        action_date)
          dimension by (seq)
          measures (promo_uom new_promo_uom,
                    detail_change_selling_uom,
                    clear_uom,
                    deleted_ind,
                    key_rank_count)
          rules sequential order (
             new_promo_uom[seq] order by seq =
                case
                   when detail_change_selling_uom[CV(seq)] is NOT NULL and
                        NVL(deleted_ind[CV(seq)], 0) != 1 then
                      detail_change_selling_uom[CV(seq)]
                   else
                      clear_uom[CV(seq)]
                end)
          order by seq) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.customer_type        = source.customer_type
       and target.action_date          = source.action_date
       and source.seq                  = source.key_rank_count)
   when MATCHED then
      update
         set target.promo_uom       = source.new_promo_uom,
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_UOM;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_UOM,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_CS_PROMO_UOM_CARRIER;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_SELLING_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_SELLING_RETAIL';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LP_round_retail_calc_type := 1;

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 new_selling_retail,
                 new_selling_retail_currency,
                 new_selling_uom,
                 new_pc_msg_type,
                 new_pc_selling_retail_ind,
                 new_pc_multi_unit_ind
            from rpm_future_retail_gtt gtt
           where gtt.timeline_seq       is NOT NULL
             and (   gtt.timeline_seq   <= 1
                  or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                            RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE))
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        zone_node_type)
          dimension by (timeline_seq)
          measures (gtt.selling_retail          new_selling_retail,
                    gtt.selling_retail          old_selling_retail,
                    gtt.selling_retail_currency new_selling_retail_currency,
                    gtt.selling_uom             new_selling_uom,
                    gtt.pc_msg_type             new_pc_msg_type,
                    gtt.pc_selling_retail_ind   new_pc_selling_retail_ind,
                    gtt.pc_multi_unit_ind       new_pc_multi_unit_ind,
                    gtt.action_date,
                    gtt.price_change_id,
                    gtt.pc_change_type,
                    gtt.pc_change_amount,
                    gtt.pc_change_currency,
                    gtt.pc_change_percent,
                    gtt.pc_change_selling_uom,
                    gtt.pc_price_guide_id,
                    gtt.future_retail_id,
                    gtt.price_event_id          pc_price_event_id,
                    gtt.dept)
          rules (new_selling_retail[timeline_seq] order by timeline_seq =
                    case
                       when CV(timeline_seq) = 0 then
                          new_selling_retail[CV(timeline_seq)]
                       when price_change_id [CV(timeline_seq)] is NULL then
                          NVL(new_selling_retail[CV(timeline_seq) - 1], new_selling_retail[CV(timeline_seq)])
                       else
                          case
                             when new_selling_retail[CV(timeline_seq) - 1] is NULL and
                                  pc_change_type[CV(timeline_seq)] != RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                                ---
                                NULL
                             else
                                case
                                   when action_date[CV(timeline_seq)] = LP_vdate then
                                      case
                                         when I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                                     RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE) and
                                              price_change_id[CV(timeline_seq)] = pc_price_event_id[CV(timeline_seq)] then
                                            ---
                                            CALC_RETAIL_WITH_GUIDE(new_selling_retail_currency[CV(timeline_seq)],
                                                                   pc_change_type[CV(timeline_seq)],
                                                                   pc_change_amount[CV(timeline_seq)],
                                                                   new_selling_retail[CV(timeline_seq)],
                                                                   pc_change_percent[CV(timeline_seq)],
                                                                   pc_price_guide_id[CV(timeline_seq)],
                                                                   pc_price_event_id[CV(timeline_seq)],
                                                                   future_retail_id[CV(timeline_seq)],
                                                                   dept[CV(timeline_seq)])
                                         else
                                            new_selling_retail[CV(timeline_seq)]
                                      end
                                   else
                                       CALC_RETAIL_WITH_GUIDE(new_selling_retail_currency[CV(timeline_seq)],
                                                              pc_change_type[CV(timeline_seq)],
                                                              pc_change_amount[CV(timeline_seq)],
                                                              new_selling_retail[CV(timeline_seq) - 1],
                                                              pc_change_percent[CV(timeline_seq)],
                                                              pc_price_guide_id[CV(timeline_seq)],
                                                              pc_price_event_id[CV(timeline_seq)],
                                                              future_retail_id[CV(timeline_seq)],
                                                              dept[CV(timeline_seq)])
                                end
                          end
                    end,
                 new_selling_retail_currency[timeline_seq] order by timeline_seq =
                    case
                       when CV(timeline_seq) = 0 then
                          new_selling_retail_currency[CV(timeline_seq)]
                       when price_change_id [CV(timeline_seq)] is NULL then
                          NVL(new_selling_retail_currency[CV(timeline_seq) - 1], new_selling_retail_currency[CV(timeline_seq)])
                       else
                          case
                             when new_selling_retail[CV(timeline_seq) - 1] is NULL and
                                  pc_change_type[CV(timeline_seq)] != RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                                ---
                                NULL
                             else
                                case
                                   when action_date[CV(timeline_seq)] = LP_vdate then
                                      new_selling_retail_currency[CV(timeline_seq) - 1]
                                   else
                                      new_selling_retail_currency[CV(timeline_seq) - 1]
                                end
                          end
                    end,
                 new_selling_uom[timeline_seq] order by timeline_seq =
                    case
                       when CV(timeline_seq) = 0 then
                          new_selling_uom[CV(timeline_seq)]
                       else
                          case
                             when pc_change_type[CV(timeline_seq)] = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                                  pc_change_selling_uom[CV(timeline_seq)] is NOT NULL then
                                ---
                                pc_change_selling_uom[CV(timeline_seq)]
                             else
                                NVL(new_selling_uom[CV(timeline_seq) - 1], new_selling_uom[CV(timeline_seq)])
                          end
                    end,
                 new_pc_msg_type[timeline_seq] order by timeline_seq =
                    case
                       when CV(timeline_seq) != 1 and
                            new_selling_retail[CV(timeline_seq) - 1] is NOT NULL and
                            price_change_id[CV(timeline_seq)] is NOT NULL and
                            (price_change_id[CV(timeline_seq)] != pc_price_event_id[CV(timeline_seq)] or
                             (price_change_id[CV(timeline_seq)] = pc_price_event_id[CV(timeline_seq)] and
                              I_price_event_type != RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE)) and
                            old_selling_retail[CV(timeline_seq)] != new_selling_retail[CV(timeline_seq)] and
                            action_date[CV(timeline_seq)] != LP_vdate then
                          ---
                          RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD
                       else
                          new_pc_msg_type[CV(timeline_seq)]
                    end,
                 new_pc_selling_retail_ind[timeline_seq] order by timeline_seq =
                    case
                       when new_selling_retail[CV(timeline_seq) - 1] is NOT NULL and
                            price_change_id[CV(timeline_seq)] is NOT NULL and
                            old_selling_retail[CV(timeline_seq)] != new_selling_retail[CV(timeline_seq)] then
                          ---
                          1
                       else
                          NULL
                    end,
                 new_pc_multi_unit_ind[timeline_seq] order by timeline_seq =
                    case
                       when CV(timeline_seq) != 1 and
                            new_selling_retail[CV(timeline_seq) - 1] is NOT NULL and
                            price_change_id[CV(timeline_seq)] is NOT NULL and
                            old_selling_retail[CV(timeline_seq)] != new_selling_retail[CV(timeline_seq)] and
                            action_date[CV(timeline_seq)] != LP_vdate then
                          ---
                          0
                       else
                          new_pc_multi_unit_ind[CV(timeline_seq)]
                    end)
            order by timeline_seq) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.timeline_seq         = source.timeline_seq
       and target.timeline_seq        != 0)
   when MATCHED then
      update
         set target.selling_retail          = source.new_selling_retail,
             target.selling_retail_currency = source.new_selling_retail_currency,
             target.selling_uom             = source.new_selling_uom,
             target.pc_msg_type             = source.new_pc_msg_type,
             target.pc_selling_retail_ind   = source.new_pc_selling_retail_ind,
             target.pc_multi_unit_ind       = source.new_pc_multi_unit_ind,
             target.step_identifier         = RPM_CONSTANTS.CAPT_GTT_RF_SELL_RETAIL;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_SELL_RETAIL,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LP_round_retail_calc_type := 0;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_SELLING_RETAIL;

--------------------------------------------------------------------------------

FUNCTION RPM_RF_CLEARANCE_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_CLEARANCE_RETAIL';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 timeline_seq,
                 new_clear_retail,
                 new_clear_retail_currency,
                 new_clear_uom,
                 new_clear_msg_type
            from rpm_future_retail_gtt gtt
           where gtt.timeline_seq       is NOT NULL
             and (   gtt.timeline_seq   <= 1
                  or I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                            RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                            RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                            RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                            RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE))
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        zone_node_type)
          dimension by (timeline_seq)
          measures (gtt.clear_retail           new_clear_retail,
                    gtt.clear_retail           old_clear_retail,
                    gtt.clear_retail_currency  new_clear_retail_currency,
                    gtt.clear_uom              new_clear_uom,
                    gtt.clear_msg_type         new_clear_msg_type,
                    gtt.clear_msg_type         old_clear_msg_type,
                    gtt.clearance_id,
                    gtt.clear_start_ind,
                    gtt.selling_retail,
                    gtt.clear_change_type,
                    gtt.clear_change_amount,
                    gtt.clear_change_percent,
                    gtt.clear_price_guide_id,
                    gtt.selling_retail_currency,
                    gtt.selling_uom,
                    gtt.action_date,
                    gtt.future_retail_id,
                    gtt.price_event_id cl_price_event_id,
                    gtt.dept)
          rules automatic order (
                new_clear_retail[timeline_seq] order by timeline_seq =
                   case
                      when CV(timeline_seq) = 0 then
                         new_clear_retail[CV(timeline_seq)]
                      when clearance_id[CV(timeline_seq)] is NULL or
                           clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.END_IND then
                         ---
                         selling_retail[CV(timeline_seq)]
                      when clearance_id[CV(timeline_seq)] is NOT NULL then
                         case
                            when clear_start_ind[CV(timeline_seq)] IN (RPM_CONSTANTS.START_IND,
                                                                       RPM_CONSTANTS.START_END_IND) then
                               case
                                  when I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE and
                                       action_date[CV(timeline_seq)] = LP_vdate then
                                     new_clear_retail[CV(timeline_seq)]
                                   ---
                                  else
                                     CALC_RETAIL_WITH_GUIDE(new_clear_retail_currency[CV(timeline_seq)],
                                                            clear_change_type[CV(timeline_seq)],
                                                            clear_change_amount[CV(timeline_seq)],
                                                            selling_retail[CV(timeline_seq)],
                                                            clear_change_percent[CV(timeline_seq)],
                                                            clear_price_guide_id[CV(timeline_seq)],
                                                            cl_price_event_id[CV(timeline_seq)],
                                                            future_retail_id[CV(timeline_seq)],
                                                            dept[CV(timeline_seq)])

                               end
                            else
                               new_clear_retail[CV(timeline_seq) - 1]
                         end
                   end,
                new_clear_retail_currency[timeline_seq] order by timeline_seq =
                   case
                      when CV(timeline_seq) = 0 then
                         new_clear_retail_currency[CV(timeline_seq)]
                      when clearance_id[CV(timeline_seq)] is NULL or
                           clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.END_IND then
                         ---
                         selling_retail_currency[CV(timeline_seq)]
                      when clearance_id[CV(timeline_seq)] is NOT NULL then
                         case
                            when clear_start_ind[CV(timeline_seq)] IN (RPM_CONSTANTS.START_IND,
                                                                       RPM_CONSTANTS.START_END_IND) then
                               ---
                               selling_retail_currency[CV(timeline_seq)]
                            else
                               new_clear_retail_currency[CV(timeline_seq) - 1]
                         end
                   end,
                new_clear_uom[timeline_seq] order by timeline_seq =
                   case
                      when CV(timeline_seq) = 0 then
                         new_clear_uom[CV(timeline_seq)]
                      when clearance_id[CV(timeline_seq)] is NULL or
                           clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.END_IND then
                         ---
                         selling_uom[CV(timeline_seq)]
                      when clearance_id[CV(timeline_seq)] is NOT NULL then
                         case
                            when clear_start_ind[CV(timeline_seq)] IN (RPM_CONSTANTS.START_IND,
                                                                       RPM_CONSTANTS.START_END_IND) then
                               ---
                               selling_uom[CV(timeline_seq)]
                            else
                               new_clear_uom[CV(timeline_seq) - 1]
                         end
                   end,
                new_clear_msg_type[timeline_seq] order by timeline_seq =
                   case
                      when clearance_id[CV(timeline_seq)] is NOT NULL and
                           clear_start_ind[CV(timeline_seq)] IN (RPM_CONSTANTS.START_IND,
                                                                 RPM_CONSTANTS.START_END_IND) and
                           (CV(timeline_seq) != 1 or
                            (CV(timeline_seq) = 1 and
                             I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET))) and
                           (clearance_id[CV(timeline_seq)] != cl_price_event_id[CV(timeline_seq)] or
                            (clearance_id[CV(timeline_seq)] = cl_price_event_id[CV(timeline_seq)] and
                             I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET))) and
                           new_clear_retail[CV(timeline_seq)] != old_clear_retail[CV(timeline_seq)] then
                         ---
                         RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD
                      when clearance_id[CV(timeline_seq)] is NOT NULL and
                           clear_start_ind[CV(timeline_seq)] = RPM_CONSTANTS.END_IND and
                           (CV(timeline_seq) != 1 or
                            (CV(timeline_seq) = 1 and
                             I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET))) and
                           (clearance_id[CV(timeline_seq)] != cl_price_event_id[CV(timeline_seq)] or
                            (clearance_id[CV(timeline_seq)] = cl_price_event_id[CV(timeline_seq)] and
                             I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET)))and
                           new_clear_retail[CV(timeline_seq)] != old_clear_retail[CV(timeline_seq)] and
                           new_clear_msg_type[CV(timeline_seq)] is NULL then
                         ---
                         RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD
                      else
                         old_clear_msg_type[CV(timeline_seq)]
                   end)
            order by timeline_seq) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.timeline_seq         = source.timeline_seq
       and target.timeline_seq        != 0)
   when MATCHED then
      update
         set target.clear_retail          = source.new_clear_retail,
             target.clear_retail_currency = source.new_clear_retail_currency,
             target.clear_uom             = source.new_clear_uom,
             target.clear_msg_type        = source.new_clear_msg_type,
             target.step_identifier       = RPM_CONSTANTS.CAPT_GTT_RF_CLEAR_RETAIL;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_CLEAR_RETAIL,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_CLEARANCE_RETAIL;

--------------------------------------------------------------------------------
FUNCTION APPLY_PRICE_GUIDE(I_selling_retail IN     NUMBER,
                           I_price_guide_id IN     NUMBER,
                           I_dept_id        IN     NUMBER)
RETURN NUMBER IS

   L_new_retail NUMBER := NULL;

   cursor C_GUIDE is
      select new_price_val
        from rpm_price_guide_interval rpgi,
             rpm_price_guide rpg
       where rpgi.price_guide_id        = I_price_guide_id
         and rpgi.price_guide_id        = rpg.price_guide_id
         and rpg.corp_ind               = 1
         and ROUND(I_selling_retail, 2) BETWEEN rpgi.from_price_val and rpgi.to_price_val
      union all
      select new_price_val
        from rpm_price_guide_interval rpgi,
             rpm_price_guide rpg,
             rpm_price_guide_dept rpgd
       where rpgi.price_guide_id        = I_price_guide_id
         and rpgi.price_guide_id        = rpg.price_guide_id
         and rpg.price_guide_id         = rpgd.price_guide_id
         and rpgd.dept                  = I_dept_id
         and rpg.corp_ind               = 0
         and ROUND(I_selling_retail, 2) BETWEEN rpgi.from_price_val and rpgi.to_price_val;

BEGIN

   if I_price_guide_id is NULL then
      L_new_retail := I_selling_retail;
   else

      open C_GUIDE;
      fetch C_GUIDE into L_new_retail;

      if C_GUIDE%NOTFOUND then
         L_new_retail := I_selling_retail;
      end if;

      close C_GUIDE;

   end if;

   return L_new_retail;

END APPLY_PRICE_GUIDE;

-------------------------------------------------------------------------------------

FUNCTION CALC_RETAIL_WITH_GUIDE(I_currency_code         IN     VARCHAR2,
                                I_change_type           IN     NUMBER,
                                I_change_amt            IN     NUMBER,
                                I_seed_retail           IN     NUMBER,
                                I_change_pct            IN     NUMBER,
                                I_price_guide_id        IN     NUMBER,
                                I_price_event_id        IN     NUMBER,
                                I_future_retail_id      IN     NUMBER,
                                I_dept_id               IN     NUMBER,
                                I_promo_retail_calc_ind IN     NUMBER DEFAULT 0,
                                I_cur_promo_retail      IN     NUMBER DEFAULT NULL,
                                I_timeline_seq          IN     NUMBER DEFAULT 0,
                                I_seq                   IN     NUMBER DEFAULT 0,
                                I_cs_promo_fr_id        IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.CALC_RETAIL_WITH_GUIDE';

   L_retail      NUMBER        := 0;
   L_error_msg   VARCHAR2(100) := NULL;
   L_seed_retail NUMBER        := NULL;

BEGIN

   if I_currency_code != LP_last_currency_code or
      LP_last_currency_code is NULL then

         select currency_rtl_dec
           into LP_currency_decimal_precision
           from currencies
          where currency_code = I_currency_code
            and rownum        = 1;

      LP_last_currency_code := I_currency_code;

   end if;

   if I_promo_retail_calc_ind = 1 and
      LP_simple_promo_overlap = RPM_CONSTANTS.PR_OVERLAP_COMPOUNDING then
      ---
      L_seed_retail := I_cur_promo_retail;
   else
      L_seed_retail := I_seed_retail;
   end if;

   if I_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then

      L_retail := I_change_amt;

   elsif L_seed_retail is NULL then

      LP_error        := TRUE;
      L_error_msg     := 'original value cannot be null';
      LP_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(I_price_event_id,
                                                  I_future_retail_id,
                                                  RPM_CONSTANTS.CONFLICT_ERROR,
                                                  L_error_msg,
                                                  I_cs_promo_fr_id));
      return NULL;

   elsif I_change_type is NULL or
      I_change_type = RPM_CONSTANTS.RETAIL_NONE or
      I_change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
      ---
      L_retail := L_seed_retail;

   elsif I_change_type IN (RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE,
                           RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE) then
      L_retail := (L_seed_retail + I_change_amt);

   elsif I_change_type = RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE then

      -- Use rounding functionality if calc-ing regular retail
      if LP_round_retail_calc_type = 1 then
         L_retail := ROUND(L_seed_retail * (1 + I_change_pct/100), LP_currency_decimal_precision);
      -- Use truncate functionality for clear and promo retail to ensure full discount
      else
         L_retail := TRUNC(L_seed_retail * (1 + I_change_pct/100), LP_currency_decimal_precision);
      end if;

   else

      LP_error := TRUE;
      L_error_msg := 'do not know how to handle retail change value: ' ||I_change_type;
      LP_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(I_price_event_id,
                                                  I_future_retail_id,
                                                  RPM_CONSTANTS.CONFLICT_ERROR,
                                                  L_error_msg,
                                                  I_cs_promo_fr_id));
      return NULL;

   end if;

   if NVL(I_price_guide_id, 0) != 0 then

      return APPLY_PRICE_GUIDE(L_retail,
                               I_price_guide_id,
                               I_dept_id);

   end if;

   if I_promo_retail_calc_ind = 1 and
      LP_simple_promo_overlap = RPM_CONSTANTS.PR_OVERLAP_BEST_DEAL and
      L_retail > I_cur_promo_retail then
      ---
      if I_seq = 1 and
         I_timeline_seq = 1 then
         return L_retail;
      else
         return I_cur_promo_retail;
      end if;
      ---
   else
      return L_retail;
   end if;

END CALC_RETAIL_WITH_GUIDE;

--------------------------------------------------------------------------------

FUNCTION CALC_REF_PROMO_DTL_ID (I_old_simple_promo_retail   IN     NUMBER,
                                I_new_simple_promo_retail   IN     NUMBER,
                                I_promo_dtl_id              IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ROLL_FORWARD_SQL.CALC_REF_PROMO_DTL_ID';

   L_error_msg             VARCHAR2(100) := NULL;
   L_ref_promo_dtl_id      NUMBER        := NULL;


BEGIN

   if LP_simple_promo_overlap            = RPM_CONSTANTS.PR_OVERLAP_BEST_DEAL and
      I_new_simple_promo_retail         != I_old_simple_promo_retail and
      I_new_simple_promo_retail          < I_old_simple_promo_retail then
      ---
         L_ref_promo_dtl_id := I_promo_dtl_id;
    else
         L_ref_promo_dtl_id := NULL;
    end if;
                                                
    RETURN L_ref_promo_dtl_id;
          ---

END CALC_REF_PROMO_DTL_ID;

----------------------------------------

FUNCTION RPM_RF_PROMOTION_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_PROMOTION_RETAIL';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   if I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                 RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                 RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                 RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE) then
      return 1;
   end if;

   merge /*+ INDEX(target, RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt target
   using (select timeline_seq,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 action_date,
                 new_simple_promo_retail,
                 new_on_simple_promo_ind,
                 new_on_complex_promo_ind,
                 price_event_id,
                 new_promo_comp_msg_type,
                 ref_promo_dtl_id,
                 seq,
                 key_rank_count
            from (select timeline_seq,
                         item,
                         diff_id,
                         location,
                         zone_node_type,
                         action_date,
                         clearance_id,
                         clear_start_ind,
                         clear_retail,
                         selling_retail_currency,
                         simple_promo_retail,
                         on_simple_promo_ind,
                         on_complex_promo_ind,
                         future_retail_id,
                         promo_start_date,
                         price_event_id,
                         promo_id,
                         promo_comp_id,
                         promo_calculated_rank_value,
                         comp_calculated_rank_value,
                         detail_start_date,
                         type,
                         customer_type,
                         detail_apply_to_code,
                         detail_change_type,
                         detail_change_amount,
                         detail_change_percent,
                         detail_change_currency,
                         detail_price_guide_id,
                         promo_comp_msg_type,
                         promo_dtl_id,
                         deleted_ind,
                         dept,
                         ref_promo_dtl_id,
                         RANK() OVER (PARTITION BY all_data.price_event_id,
                                                   all_data.item,
                                                   NVL(all_data.diff_id, '-999'),
                                                   all_data.location,
                                                   all_data.zone_node_type,
                                                   all_data.action_date
                                          ORDER BY all_data.deleted_ind desc,
                                                   all_data.promo_calculated_rank_value,
                                                   all_data.comp_calculated_rank_value,
                                                   all_data.detail_start_date,
                                                   all_data.promo_id,
                                                   all_data.promo_dtl_id asc) seq,
                         COUNT(1) OVER (PARTITION BY all_data.price_event_id,
                                                     all_data.item,
                                                     NVL(all_data.diff_id, '-999'),
                                                     all_data.location,
                                                     all_data.zone_node_type,
                                                     all_data.action_date) key_rank_count
                    from (select /*+ LEADING(rpile) INDEX(gtt, RPM_FUTURE_RETAIL_GTT_I1) */
                                 distinct gtt.timeline_seq,
                                 gtt.item,
                                 gtt.diff_id,
                                 gtt.location,
                                 gtt.zone_node_type,
                                 gtt.action_date,
                                 gtt.clearance_id,
                                 gtt.clear_start_ind,
                                 gtt.clear_retail,
                                 gtt.selling_retail_currency,
                                 gtt.simple_promo_retail,
                                 gtt.on_simple_promo_ind,
                                 gtt.on_complex_promo_ind,
                                 gtt.future_retail_id,
                                 rpile.detail_start_date promo_start_date,
                                 gtt.price_event_id,
                                 rpile.promo_id,
                                 rpile.promo_comp_id,
                                 rpile.promo_calculated_rank_value,
                                 rpile.comp_calculated_rank_value,
                                 rpile.detail_start_date,
                                 rpile.type,
                                 rpile.customer_type,
                                 rpile.detail_apply_to_code,
                                 rpile.detail_change_type,
                                 rpile.detail_change_amount,
                                 rpile.detail_change_percent,
                                 rpile.detail_change_currency,
                                 rpile.detail_price_guide_id,
                                 gtt.promo_comp_msg_type,
                                 rpile.promo_dtl_id,
                                 NVL(rpile.deleted_ind, 0) deleted_ind,
                                 gtt.dept,
                                 gtt.ref_promo_dtl_id
                            from rpm_promo_fr_item_loc_gtt rpile,
                                 rpm_future_retail_gtt gtt
                           where rpile.customer_type (+)  is NULL
                             and gtt.price_event_id       = rpile.price_event_id (+)
                             and gtt.item                 = rpile.item (+)
                             and NVL(gtt.diff_id, '-999') = NVL(rpile.diff_id (+), '-999')
                             and gtt.location             = rpile.location (+)
                             and gtt.zone_node_type       = rpile.zone_node_type (+)
                             and gtt.action_date          = rpile.action_date (+)
                             and NOT EXISTS (select 'x'
                                               from rpm_promo_fr_item_loc_gtt rpile2
                                              where rpile.item                 = rpile2.item
                                                and NVL(rpile.diff_id, '-999') = NVL(rpile2.diff_id, '-999')
                                                and rpile.location             = rpile2.location
                                                and rpile.zone_node_type       = rpile2.zone_node_type
                                                and rpile.action_date          = rpile2.action_date
                                                and rpile.price_event_id       = rpile2.price_event_id
                                                and rpile2.customer_type       is NULL
                                                and rpile2.detail_change_type  = RPM_CONSTANTS.RETAIL_EXCLUDE
                                                and rpile2.promo_dtl_id        != rpile.promo_dtl_id
                                                and NVL(rpile2.deleted_ind, 0) != 1
                                                and (   (    rpile.promo_id             = rpile2.promo_id
                                                         and rpile2.exception_parent_id is NULL)
                                                     or (    rpile.promo_id             = rpile2.promo_id
                                                         and rpile.promo_comp_id        = rpile2.promo_comp_id
                                                         and rpile2.exception_parent_id = rpile.promo_dtl_id
                                                         and rpile2.exception_parent_id is NOT NULL)))) all_data
                   ORDER BY price_event_id,
                            item,
                            diff_id,
                            location,
                            zone_node_type,
                            action_date,
                            seq) ranked_promos
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        action_date,
                        zone_node_type)
          dimension by (seq)
          measures (ranked_promos.simple_promo_retail   new_simple_promo_retail,
                    ranked_promos.simple_promo_retail   old_simple_promo_retail,
                    ranked_promos.on_simple_promo_ind   new_on_simple_promo_ind,
                    ranked_promos.on_complex_promo_ind  new_on_complex_promo_ind,
                    promo_comp_msg_type                 new_promo_comp_msg_type,
                    future_retail_id,
                    promo_comp_id,
                    clear_retail,
                    selling_retail_currency,
                    key_rank_count,
                    type,
                    customer_type,
                    detail_change_type,
                    detail_change_amount,
                    detail_change_percent,
                    detail_price_guide_id,
                    detail_change_currency,
                    timeline_seq,
                    detail_apply_to_code,
                    promo_dtl_id,
                    clearance_id,
                    clear_start_ind,
                    deleted_ind,
                    price_event_id prom_price_event_id,
                    dept,
                    ranked_promos.ref_promo_dtl_id ref_promo_dtl_id)
          rules sequential order (
                 new_simple_promo_retail[seq] order by seq =
                    case
                       when CV(seq) = 1 and
                            timeline_seq[CV(seq)] = 0 then
                          ---
                          clear_retail[CV(seq)]
                       when CV(seq) = 1 and
                            timeline_seq[CV(seq)] != 0 then
                          ---
                          case
                             when type[CV(seq)] = RPM_CONSTANTS.SIMPLE_CODE and
                                  customer_type[CV(seq)] is NULL and
                                  (detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_AND_CLEARANCE or
                                   (clearance_id[CV(seq)] is NOT NULL and
                                    clear_start_ind[CV(seq)] != RPM_CONSTANTS.END_IND and
                                    detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.CLEARANCE_ONLY) or
                                  (((clearance_id[CV(seq)] is NOT NULL and
                                     clear_start_ind[CV(seq)] = RPM_CONSTANTS.END_IND) or
                                    clearance_id[CV(seq)] is NULL) and
                                   detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_ONLY)) and
                                  NVL(deleted_ind[CV(seq)], 0) != 1 then
                                ---
                                CALC_RETAIL_WITH_GUIDE(selling_retail_currency[CV(seq)],
                                                       detail_change_type[CV(seq)],
                                                       detail_change_amount[CV(seq)],
                                                       clear_retail[CV(seq)],
                                                       detail_change_percent[CV(seq)],
                                                       detail_price_guide_id[CV(seq)],
                                                       prom_price_event_id[CV(seq)],
                                                       future_retail_id[CV(seq)],
                                                       dept[CV(seq)],
                                                       1,  -- promo_retail_calc_ind
                                                       clear_retail[CV(seq)],
                                                       timeline_seq[CV(seq)],
                                                       CV(seq))
                             else
                                clear_retail[CV(seq)]
                          end
                       when CV(seq) != 1 then
                          case
                             when type[CV(seq)] = RPM_CONSTANTS.SIMPLE_CODE and
                                customer_type[CV(seq)] is NULL and
                                (detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_AND_CLEARANCE or
                                 (clearance_id[CV(seq)] is NOT NULL and
                                  clear_start_ind[CV(seq)] != RPM_CONSTANTS.END_IND and
                                  detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.CLEARANCE_ONLY) or
                                 (((clearance_id[CV(seq)] is NOT NULL and
                                    clear_start_ind[CV(seq)] = RPM_CONSTANTS.END_IND) or
                                    clearance_id[CV(seq)] is NULL) and
                                  detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_ONLY)) and
                                NVL(deleted_ind[CV(seq)], 0) != 1 then
                                ---
                                CALC_RETAIL_WITH_GUIDE(selling_retail_currency[CV(seq)],
                                                       detail_change_type[CV(seq)],
                                                       detail_change_amount[CV(seq)],
                                                       clear_retail[CV(seq)],
                                                       detail_change_percent[CV(seq)],
                                                       detail_price_guide_id[CV(seq)],
                                                       prom_price_event_id[CV(seq)],
                                                       future_retail_id[CV(seq)],
                                                       dept[CV(seq)],
                                                       1, -- promo_retail_calc_ind
                                                       new_simple_promo_retail[CV(seq) - 1],
                                                       timeline_seq[CV(seq)],
                                                       CV(seq))
                             else
                                new_simple_promo_retail[CV(seq) - 1]
                          end
                       else
                          clear_retail[CV(seq)]
                    end,
                 new_on_simple_promo_ind[seq] order by seq =
                    case
                       when CV(seq) = 1 and
                            timeline_seq[CV(seq)] = 0 then
                          0
                       when NVL(deleted_ind[CV(seq)], 0) = 1 then
                          0
                       when type[CV(seq)] = RPM_CONSTANTS.SIMPLE_CODE and
                            customer_type[CV(seq)] is NULL and
                            (detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_AND_CLEARANCE or
                             (clearance_id[CV(seq)] is NOT NULL and
                              clear_start_ind[CV(seq)] != RPM_CONSTANTS.END_IND and
                              detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.CLEARANCE_ONLY) or
                             (((clearance_id[CV(seq)] is NOT NULL and
                                clear_start_ind[CV(seq)] = RPM_CONSTANTS.END_IND) or
                                clearance_id[CV(seq)] is NULL) and
                              detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_ONLY)) and
                            detail_change_type[CV(seq)] != RPM_CONSTANTS.RETAIL_EXCLUDE then
                          1
                       when new_on_simple_promo_ind[CV(seq) - 1] = 1 then
                          1
                       else
                          0
                    end,
                 new_on_complex_promo_ind[seq] order by seq =
                    case
                       when CV(seq) = 1 and
                            timeline_seq[CV(seq)] = 0 then
                          0
                       when NVL(deleted_ind[CV(seq)], 0) = 1 then
                          0
                       when type[CV(seq)] IN (RPM_CONSTANTS.COMPLEX_CODE,
                                              RPM_CONSTANTS.THRESHOLD_CODE,
                                              RPM_CONSTANTS.TRANSACTION_CODE) and
                            customer_type[CV(seq)] is NULL and
                            (detail_change_type[CV(seq)]  is NULL or
                             detail_change_type[CV(seq)] != RPM_CONSTANTS.RETAIL_EXCLUDE) and
                            (detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_AND_CLEARANCE or
                             (clearance_id[CV(seq)] is NOT NULL and
                              clear_start_ind[CV(seq)] != RPM_CONSTANTS.END_IND and
                              detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.CLEARANCE_ONLY) or
                             (((clearance_id[CV(seq)] is NOT NULL and
                                clear_start_ind[CV(seq)] = RPM_CONSTANTS.END_IND) or
                                clearance_id[CV(seq)] is NULL) and
                              detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_ONLY)) then
                          1
                       when new_on_complex_promo_ind[CV(seq) - 1] = 1 then
                          1
                       else
                          0
                    end,
                 new_promo_comp_msg_type[seq] order by seq =
                    case
                       when old_simple_promo_retail[CV(seq)]  != new_simple_promo_retail[CV(seq)] and
                            customer_type[CV(seq)] is NULL and
                            NVL(deleted_ind[CV(seq)], 0) != 1 then
                          ---
                          RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD
                       when new_promo_comp_msg_type[CV(seq) - 1] is NOT NULL and
                            NVL(deleted_ind[CV(seq)], 0) != 1 then
                          new_promo_comp_msg_type[CV(seq) - 1]
                       else
                          new_promo_comp_msg_type[CV(seq)]
                    end,
                  ref_promo_dtl_id[seq] ORDER BY seq =
                    case
                       when type[CV(seq)] = RPM_CONSTANTS.SIMPLE_CODE THEN
                          --
                          CALC_REF_PROMO_DTL_ID (old_simple_promo_retail[CV(seq)],
                                                 new_simple_promo_retail[CV(seq)],
                                                 promo_dtl_id[CV(seq)])
                       else
                          NULL
                     end)) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.action_date          = source.action_date
       and target.timeline_seq         = source.timeline_seq
       and source.key_rank_count       = source.seq
       and source.timeline_seq        != 0)
   when MATCHED then
      update
         set target.simple_promo_retail  = source.new_simple_promo_retail,
             target.on_simple_promo_ind  = source.new_on_simple_promo_ind,
             target.on_complex_promo_ind = source.new_on_complex_promo_ind,
             target.promo_comp_msg_type  = source.new_promo_comp_msg_type,
             target.step_identifier      = RPM_CONSTANTS.CAPT_GTT_RF_PROMO_RETAIL,
             target.ref_promo_dtl_id     = source.ref_promo_dtl_id;

   -- Push MOD message types back to the item_loc table from future_retail
   -- RIB messaging...

   merge into rpm_promo_item_loc_expl_gtt target
   using (select distinct fr.price_event_id,
                 fr.diff_id,
                 fr.item,
                 fr.location,
                 fr.zone_node_type,
                 ilex.promo_dtl_id,
                 fr.promo_comp_msg_type
            from rpm_future_retail_gtt fr,
                 rpm_promo_item_loc_expl_gtt ilex
           where fr.promo_comp_msg_type        = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD
             and ilex.price_event_id           = fr.price_event_id
             and ilex.item                     = fr.item
             and NVL(ilex.diff_id, '-999')     = NVL(fr.diff_id, '-999')
             and ilex.location                 = fr.location
             and ilex.zone_node_type           = fr.zone_node_type
             and fr.action_date               >= ilex.detail_start_date
             and (   ilex.detail_end_date     is NULL
                  or fr.action_date            < ilex.detail_end_date + RPM_CONSTANTS.ONE_MINUTE)
             and ilex.promo_dtl_id            != fr.price_event_id
             and ilex.type                     = RPM_CONSTANTS.SIMPLE_CODE
             and (   ilex.promo_comp_msg_type is NULL
                  or ilex.promo_comp_msg_type != RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE)
             and ilex.detail_change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.promo_dtl_id         = source.promo_dtl_id)
   when MATCHED then
      update
         set target.promo_comp_msg_type = source.promo_comp_msg_type,
             target.step_identifier     = RPM_CONSTANTS.CAPT_GTT_RF_PROMO_RETAIL;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_PROMO_RETAIL,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_PROMOTION_RETAIL;
----------------------------------------

FUNCTION RPM_RF_CS_PROMOTION_RETAIL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ROLL_FORWARD_SQL.RPM_RF_CS_PROMOTION_RETAIL';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_cust_segment_promo_fr_gtt target
   using (select item,
                 diff_id,
                 location,
                 zone_node_type,
                 action_date,
                 new_cs_promo_retail,
                 new_complex_promo_ind,
                 new_promo_retail_currency,
                 price_event_id,
                 new_promo_comp_msg_type,
                 customer_type,
                 seq,
                 key_rank_count
            from (select cust_segment_promo_id,
                         item,
                         diff_id,
                         location,
                         zone_node_type,
                         action_date,
                         fr_action_date,
                         clearance_id,
                         clear_start_ind,
                         clear_retail,
                         clear_retail_currency,
                         fr_promo_retail,
                         fr_promo_retail_currency,
                         future_retail_id,
                         promo_retail,
                         complex_promo_ind,
                         promo_retail_currency,
                         price_event_id,
                         type,
                         detail_apply_to_code,
                         detail_change_type,
                         detail_change_amount,
                         detail_change_percent,
                         detail_price_guide_id,
                         promo_comp_msg_type,
                         promo_dtl_id,
                         deleted_ind,
                         detail_end_date,
                         customer_type,
                         dept,
                         RANK() OVER (PARTITION BY price_event_id,
                                                   item,
                                                   NVL(diff_id, '-999'),
                                                   location,
                                                   zone_node_type,
                                                   action_date,
                                                   customer_type
                                          ORDER BY deleted_ind desc,
                                                   action_date,
                                                   detail_end_date,
                                                   promo_dtl_id) seq,
                         COUNT(1) OVER (PARTITION BY item,
                                                     NVL(diff_id, '-999'),
                                                     location,
                                                     zone_node_type,
                                                     action_date,
                                                     customer_type) key_rank_count
                    from (select cspfg.cust_segment_promo_id,
                                 cspfg.item,
                                 cspfg.diff_id,
                                 cspfg.location,
                                 cspfg.zone_node_type,
                                 cspfg.action_date action_date,
                                 cspfg.action_date fr_action_date,
                                 gtt.clearance_id,
                                 gtt.clear_start_ind,
                                 gtt.clear_retail,
                                 gtt.clear_retail_currency,
                                 gtt.simple_promo_retail fr_promo_retail,
                                 gtt.simple_promo_retail_currency fr_promo_retail_currency,
                                 gtt.future_retail_id,
                                 cspfg.promo_retail,
                                 cspfg.complex_promo_ind,
                                 cspfg.promo_retail_currency,
                                 cspfg.price_event_id,
                                 rpile.type,
                                 rpile.detail_apply_to_code,
                                 rpile.detail_change_type,
                                 rpile.detail_change_amount,
                                 rpile.detail_change_percent,
                                 rpile.detail_price_guide_id,
                                 cspfg.promo_comp_msg_type,
                                 rpile.promo_dtl_id,
                                 NVL(rpile.deleted_ind, 0) deleted_ind,
                                 rpile.detail_end_date,
                                 cspfg.customer_type,
                                 gtt.dept,
                                 RANK() OVER (PARTITION BY gtt.price_event_id,
                                                           gtt.item,
                                                           NVL(gtt.diff_id, '-999'),
                                                           gtt.dept,
                                                           gtt.location,
                                                           gtt.zone_node_type,
                                                           cspfg.action_date
                                                  ORDER BY gtt.action_date desc) rank
                            from rpm_promo_fr_item_loc_gtt rpile,
                                 rpm_future_retail_gtt gtt,
                                 rpm_cust_segment_promo_fr_gtt cspfg
                           where cspfg.price_event_id       = rpile.price_event_id (+)
                             and cspfg.item                 = rpile.item (+)
                             and NVL(cspfg.diff_id, '-999') = NVL(rpile.diff_id (+), '-999')
                             and cspfg.location             = rpile.location (+)
                             and cspfg.zone_node_type       = rpile.zone_node_type (+)
                             and cspfg.action_date          = rpile.action_date (+)
                             and rpile.customer_type (+)    is NOT NULL
                             and cspfg.customer_type        = rpile.customer_type (+)
                             and cspfg.price_event_id       = gtt.price_event_id
                             and cspfg.item                 = gtt.item
                             and NVL(cspfg.diff_id, '-999') = NVL(gtt.diff_id , '-999')
                             and cspfg.location             = gtt.location
                             and cspfg.zone_node_type       = gtt.zone_node_type
                             and gtt.action_date           <= cspfg.action_date
                             and NOT EXISTS (select 'x'
                                               from rpm_promo_fr_item_loc_gtt rpile2
                                              where rpile.item                           = rpile2.item
                                                and NVL(rpile.diff_id, '-999')           = NVL(rpile2.diff_id, '-999')
                                                and rpile.location                       = rpile2.location
                                                and rpile.zone_node_type                 = rpile2.zone_node_type
                                                and rpile.action_date                    = rpile2.action_date
                                                and rpile.price_event_id                 = rpile2.price_event_id
                                                and rpile2.customer_type                is NOT NULL
                                                and rpile.customer_type                  = rpile2.customer_type
                                                and rpile2.detail_change_type            = RPM_CONSTANTS.RETAIL_EXCLUDE
                                                and rpile2.promo_dtl_id                 != rpile.promo_dtl_id
                                                and NVL(rpile2.deleted_ind, 0)          != 1
                                                and (   (    rpile.promo_id              = rpile2.promo_id
                                                         and rpile2.exception_parent_id is NULL)
                                                     or (    rpile.promo_id              = rpile2.promo_id
                                                         and rpile.promo_comp_id         = rpile2.promo_comp_id
                                                         and rpile2.exception_parent_id  = rpile.promo_dtl_id
                                                         and rpile2.exception_parent_id  is NOT NULL))))
                   where rank = 1
                   ORDER BY price_event_id,
                            item,
                            NVL(diff_id, '-999'),
                            location,
                            zone_node_type,
                            action_date,
                            seq) ranked_promos
          model
          partition by (price_event_id,
                        item,
                        NVL(diff_id, '-999') diff_id,
                        location,
                        zone_node_type,
                        customer_type,
                        action_date)
          dimension by (seq)
          measures (promo_retail          new_cs_promo_retail,
                    promo_retail          old_cs_promo_retail,
                    complex_promo_ind     new_complex_promo_ind,
                    promo_retail_currency new_promo_retail_currency,
                    promo_comp_msg_type   new_promo_comp_msg_type,
                    clear_retail,
                    fr_promo_retail,
                    fr_promo_retail_currency,
                    fr_action_date,
                    type,
                    detail_change_type,
                    detail_change_amount,
                    detail_change_percent,
                    detail_price_guide_id,
                    detail_apply_to_code,
                    promo_dtl_id,
                    clearance_id,
                    clear_start_ind,
                    clear_retail_currency,
                    deleted_ind,
                    key_rank_count,
                    detail_end_date,
                    future_retail_id,
                    price_event_id cspfg_price_event_id,
                    cust_segment_promo_id,
                    dept)
          rules sequential order (
                 new_cs_promo_retail[seq] order by seq =
                    case
                       when promo_dtl_id[CV(seq)] is NULL then
                          clear_retail[CV(seq)]
                       when fr_action_date[CV(seq)] = detail_end_date[CV(seq)] + RPM_CONSTANTS.ONE_MINUTE or
                            type[CV(seq)] != RPM_CONSTANTS.SIMPLE_CODE then
                          fr_promo_retail[CV(seq)]
                       when (detail_apply_to_code[CV(seq)] =  RPM_CONSTANTS.REGULAR_AND_CLEARANCE or
                            (clearance_id[CV(seq)] is NOT NULL and
                             clear_start_ind[CV(seq)] !=  RPM_CONSTANTS.END_IND and
                             detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.CLEARANCE_ONLY) or
                            (((clearance_id[CV(seq)] is NOT NULL and
                            clear_start_ind[CV(seq)] = RPM_CONSTANTS.END_IND)or
                               clearance_id[CV(seq)] is NULL) and
                             detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_ONLY)) and
                            NVL(deleted_ind[CV(seq)], 0) != 1 then
                          ---
                          CALC_RETAIL_WITH_GUIDE(clear_retail_currency[CV(seq)],
                                                 detail_change_type[CV(seq)],
                                                 detail_change_amount[CV(seq)],
                                                 clear_retail[CV(seq)],
                                                 detail_change_percent[CV(seq)],
                                                 detail_price_guide_id[CV(seq)],
                                                 cspfg_price_event_id[CV(seq)],
                                                 NULL, -- future_retail_id,
                                                 dept[CV(seq)],
                                                 0,    -- promo_retail_calc_ind
                                                 NULL, -- cur_promo_retail
                                                 0,    -- timeline_seq
                                                 0,    -- seq
                                                 cust_segment_promo_id[CV(seq)])
                       else
                          clear_retail[CV(seq)]
                    end,
                 new_promo_retail_currency[seq] order by seq =
                    case
                       when fr_action_date[CV(seq)] = detail_end_date[CV(seq)] + RPM_CONSTANTS.ONE_MINUTE then
                          fr_promo_retail_currency[CV(seq)]
                       when new_promo_retail_currency[CV(seq)] is NULL then
                          clear_retail_currency[CV(seq)]
                       else
                          clear_retail_currency[CV(seq)]
                    end,
                 new_complex_promo_ind[seq] order by seq =
                    case
                       when fr_action_date[CV(seq)] = detail_end_date[CV(seq)] + RPM_CONSTANTS.ONE_MINUTE then
                          0
                       when NVL(deleted_ind[CV(seq)], 0) = 1  then
                          0
                       when type[CV(seq)] IN (RPM_CONSTANTS.COMPLEX_CODE,
                                              RPM_CONSTANTS.THRESHOLD_CODE,
                                              RPM_CONSTANTS.TRANSACTION_CODE)and
                            (detail_change_type[CV(seq)] is NULL or
                             detail_change_type[CV(seq)] !=  RPM_CONSTANTS.RETAIL_EXCLUDE) and
                            (detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_AND_CLEARANCE or
                             (clearance_id[CV(seq)] is NOT NULL and
                              clear_start_ind[CV(seq)] != RPM_CONSTANTS.END_IND and
                              detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.CLEARANCE_ONLY) or
                             (((clearance_id[CV(seq)] is NOT NULL and
                                clear_start_ind[CV(seq)] = RPM_CONSTANTS.END_IND) or
                                clearance_id[CV(seq)] is NULL) and
                              detail_apply_to_code[CV(seq)] = RPM_CONSTANTS.REGULAR_ONLY)) then
                          1
                       when new_complex_promo_ind[CV(seq) - 1] = 1 then
                          1
                       else
                          0
                    end,
                 new_promo_comp_msg_type[seq] order by seq =
                    case
                       when fr_action_date[CV(seq)] = detail_end_date[CV(seq)] + RPM_CONSTANTS.ONE_MINUTE then
                          NULL
                       when (old_cs_promo_retail[CV(seq)] != new_cs_promo_retail[CV(seq)] or
                             old_cs_promo_retail[CV(seq)] != new_cs_promo_retail[CV(seq)]) and
                            (I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) or
                            (I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) and
                             promo_dtl_id[CV(seq)] != cspfg_price_event_id[CV(seq)])) and
                            NVL(deleted_ind[CV(seq)], 0) != 1 then
                          ---
                          RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD
                       when new_promo_comp_msg_type[CV(seq) - 1] is NOT NULL and
                            NVL(deleted_ind[CV(seq)],0) != 1 then
                          new_promo_comp_msg_type[CV(seq) - 1]
                       else
                          new_promo_comp_msg_type[CV(seq)]
                    end)) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.customer_type        = source.customer_type
       and target.action_date          = source.action_date
       and source.key_rank_count       = source.seq)
   when MATCHED then
      update
         set target.promo_retail          = source.new_cs_promo_retail,
             target.complex_promo_ind     = source.new_complex_promo_ind,
             target.promo_retail_currency = source.new_promo_retail_currency,
             target.promo_comp_msg_type   = source.new_promo_comp_msg_type,
             target.step_identifier       = RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_RETAIL;

   -- Push MOD message types back to the item_loc table from
   -- cust_seg_promo_fr for RIB messaging...

   merge into rpm_promo_item_loc_expl_gtt target
   using (select distinct
                 cspfg.price_event_id,
                 cspfg.item,
                 cspfg.diff_id,
                 cspfg.location,
                 cspfg.zone_node_type,
                 ilex.promo_dtl_id,
                 cspfg.promo_comp_msg_type
            from rpm_cust_segment_promo_fr_gtt cspfg,
                 rpm_promo_item_loc_expl_gtt ilex
           where cspfg.promo_comp_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD
             and ilex.price_event_id       = cspfg.price_event_id
             and ilex.item                 = cspfg.item
             and NVL(ilex.diff_id, '-999') = NVL(cspfg.diff_id, '-999')
             and ilex.location             = cspfg.location
             and ilex.zone_node_type       = cspfg.zone_node_type
             and cspfg.action_date        >= ilex.detail_start_date
             and (   ilex.detail_end_date is NULL
                  or cspfg.action_date     < ilex.detail_end_date)
             and ilex.promo_dtl_id        != cspfg.price_event_id
             and ilex.type                 = RPM_CONSTANTS.SIMPLE_CODE
             and ilex.detail_change_type  != RPM_CONSTANTS.RETAIL_EXCLUDE) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.promo_dtl_id         = source.promo_dtl_id)
   when MATCHED then
      update
         set target.promo_comp_msg_type = source.promo_comp_msg_type,
             target.step_identifier     = RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_RETAIL;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RF_CS_PROMO_RETAIL,
                                                   0, -- RFR
                                                   1, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RPM_RF_CS_PROMOTION_RETAIL;
----------------------------------------
END RPM_ROLL_FORWARD_SQL;
/
