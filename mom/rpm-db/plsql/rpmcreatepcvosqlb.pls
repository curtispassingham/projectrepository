CREATE OR REPLACE PACKAGE BODY RPM_CREATE_PC_VO_SQL AS
-------------------------------------------------------------------------------
LP_ui_workflow_id VARCHAR2(100) := '-9999999999';
-------------------------------------------------------------------------------
FUNCTION VALIDATE_LINK_CODE(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                            I_create_pc_tbl IN     OBJ_CREATE_PC_TBL,
                            I_user_id       IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                       I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_PARENT_DIFF(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                              I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_LIST(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                            I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER;

-------------------------------------------------------------------------------
FUNCTION VALIDATE_PRICE_EVENT_ITEM_LIST(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                                        I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER;

-------------------------------------------------------------------------------
FUNCTION VALIDATE_LINK_CODE(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                            I_create_pc_tbl IN     OBJ_CREATE_PC_TBL,
                            I_user_id       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CREATE_PC_VO_SQL.VALIDATE_LINK_CODE';

   L_dept_class_subclass   OBJ_DEPT_CLASS_SUBCLASS_TBL := NULL;
   L_secured_link_code_tbl OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_ranged_link_code_tbl  OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_error_tbl             OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_skip_link_code        NUMBER(6)                   := NULL;
   L_skip_link_codes       OBJ_NUMERIC_ID_TABLE        := NEW OBJ_NUMERIC_ID_TABLE();
   L_valid                 NUMBER(1)                   := NULL;
   L_error_message         VARCHAR2(255)               := NULL;
   L_workspace_ids         OBJ_NUMERIC_ID_TABLE        := NEW OBJ_NUMERIC_ID_TABLE();

   cursor C_SEC is
      select OBJ_DEPT_CLASS_SUBCLASS_REC(dept,
                                         class,
                                         subclass)
        from (select sc.dept,
                     NULL class,
                     NULL subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_DEPT
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = I_user_id
                 and sc.dept                = TO_NUMBER(rhp.key_value)
              union
              select sc.dept,
                     sc.class,
                     NULL subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_CLASS
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = I_user_id
                 and rhp.key_value          = TO_CHAR(sc.dept||';'||sc.class)
              union
              select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = I_user_id
                 and rhp.key_value          = TO_CHAR(sc.dept||';'||sc.class||';'||sc.subclass));

   cursor C_SECURED_LINK_CODE is
      select distinct
             link_code_id,
             link_code
        from (select /*+ ORDERED */
                     gtt.link_code_id,
                     gtt.link_code
                from rpm_pc_rec_gtt gtt
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                 and NOT EXISTS (select /*+ CARDINALITY(dcs 10) */
                                        1
                                   from table(cast(L_dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) dcs,
                                        rpm_link_code_attribute rlca,
                                        item_master im
                                  where rlca.link_code = gtt.link_code_id
                                    and im.item        = rlca.item
                                    and im.dept        = dcs.dept
                                    and im.class       = NVL(dcs.class, im.class)
                                    and im.subclass    = NVL(dcs.class, im.subclass)));

   cursor C_RANGED_LOC_LINK_CODE is
      select distinct
             gtt.rpm_price_workspace_id,
             gtt.link_code_id,
             gtt.link_code,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and NOT EXISTS (select 1
                           from rpm_link_code_attribute rlca,
                                rpm_item_loc ril
                          where rlca.link_code = gtt.link_code_id
                            and ril.item       = rlca.item
                            and ril.loc        = gtt.location);

   cursor C_RANGED_ZONE_LINK_CODE is
      select distinct
             gtt.rpm_price_workspace_id,
             gtt.link_code_id,
             gtt.link_code,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and gtt.zone_node_type = 1
         and NOT EXISTS (select 1
                           from rpm_link_code_attribute rlca,
                                rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rlca.link_code = gtt.link_code_id
                            and rzl.zone_id    = gtt.zone_id
                            and ril.item       = rlca.item
                            and ril.loc        = rzl.location);

   cursor C_ITEMS is
      select distinct link_code_id,
                      link_code,
                      item
        from (select /*+ ORDERED */
                     gtt.link_code_id,
                     gtt.link_code,
                     rlca.item
                from rpm_pc_rec_gtt gtt,
                     rpm_link_code_attribute rlca
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                 and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rlca.link_code     = gtt.link_code_id
                 and rlca.location      = gtt.location
              union all
              select /*+ ORDERED */
                     gtt.link_code_id,
                     gtt.link_code,
                     rlca.item
                from rpm_pc_rec_gtt gtt,
                     rpm_link_code_attribute rlca,
                     rpm_zone_location rzl
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                 and rzl.zone_id        = gtt.zone_id
                 and rlca.link_code     = gtt.link_code_id
                 and rlca.location      = rzl.location)
      order by link_code_id,
               link_code,
               item;

BEGIN

   open C_SEC;
   fetch C_SEC BULK COLLECT into L_dept_class_subclass;
   close C_SEC;

   for rec IN C_SECURED_LINK_CODE loop
      L_secured_link_code_tbl.EXTEND;
      L_secured_link_code_tbl(L_secured_link_code_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.link_code,
                                                                                            NULL,
                                                                                            NULL,
                                                                                            'link_code_merch_security_error',
                                                                                            'linkCode');
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := L_secured_link_code_tbl(L_secured_link_code_tbl.COUNT);
   end loop;

   if L_secured_link_code_tbl is NOT NULL and
      L_secured_link_code_tbl.COUNT > 0 then

      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and gtt.link_code      IN (select err.string_1
                                      from table(cast(L_secured_link_code_tbl as OBJ_STR_STR_STR_STR_STR_TBL)) err
                                     where err.string_4 = 'link_code_merch_security_error');
   end if;

   for rec IN C_RANGED_LOC_LINK_CODE loop
      L_ranged_link_code_tbl.EXTEND;
      L_ranged_link_code_tbl(L_ranged_link_code_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.link_code,
                                                                                          rec.zone_node_id,
                                                                                          NULL,
                                                                                          'linkcode_location_is_invalid',
                                                                                          'itemLocations');
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := L_ranged_link_code_tbl(L_ranged_link_code_tbl.COUNT);
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

   end loop;

   for rec IN C_RANGED_ZONE_LINK_CODE loop
      L_ranged_link_code_tbl.EXTEND;
      L_ranged_link_code_tbl(L_ranged_link_code_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.link_code,
                                                                                          rec.zone_node_display_id,
                                                                                          rec.zone_node_id,
                                                                                          'linkcode_zone_is_invalid',
                                                                                          'itemLocations');
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := L_ranged_link_code_tbl(L_ranged_link_code_tbl.COUNT);
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

   end loop;

   if L_workspace_ids is NOT NULL and
      L_workspace_ids.COUNT > 0 then
      --
      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id         = LP_ui_workflow_id
         and gtt.item_level             = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and gtt.rpm_price_workspace_id IN (select VALUE(err)
                                              from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
   end if;

   if I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then

      if I_create_pc_tbl(1).change_selling_uom is NOT NULL or
         I_create_pc_tbl(1).multi_selling_uom is NOT NULL then

         for rec IN C_ITEMS loop

            if L_skip_link_code is NULL or
               rec.link_code_id != L_skip_link_code then

               L_valid := 1;

               if I_create_pc_tbl(1).change_selling_uom is NOT NULL then

                  if RPM_WRAPPER.VALID_UOM_FOR_ITEMS(L_error_message,
                                                     L_valid,
                                                     I_create_pc_tbl(1).change_selling_uom,
                                                     rec.item,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL) = 0 then
                     O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                    OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                NULL,
                                                                NULL,
                                                                RPM_CONSTANTS.PLSQL_ERROR,
                                                                SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                   L_error_message,
                                                                                   L_program,
                                                                                   NULL)));
                     return 0;
                  end if;

                  if L_valid = 0 then

                     L_skip_link_code := rec.link_code_id;
                     L_skip_link_codes.EXTEND;
                     L_skip_link_codes(L_skip_link_codes.COUNT) := L_skip_link_code;
                     L_error_tbl.EXTEND;
                     L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).change_selling_uom,
                                                                                   rec.item,
                                                                                   NULL,
                                                                                   'invalid_uom_conversion',
                                                                                   'UOM');
                  end if;
               end if;

               if L_valid = 1 and
                  I_create_pc_tbl(1).multi_selling_uom is NOT NULL then

                  if RPM_WRAPPER.VALID_UOM_FOR_ITEMS(L_error_message,
                                                     L_valid,
                                                     I_create_pc_tbl(1).multi_selling_uom,
                                                     rec.item,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL) = 0 then
                     O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                    OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                NULL,
                                                                NULL,
                                                                RPM_CONSTANTS.PLSQL_ERROR,
                                                                SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                   L_error_message,
                                                                                   L_program,
                                                                                   NULL)));
                     return 0;
                  end if;

                  if L_valid = 0 then

                     L_skip_link_code := rec.link_code_id;
                     L_skip_link_codes.EXTEND;
                     L_skip_link_codes(L_skip_link_codes.COUNT) := L_skip_link_code;
                     L_error_tbl.EXTEND;
                     L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).multi_selling_uom,
                                                                                   rec.item,
                                                                                   NULL,
                                                                                   'invalid_uom_conversion',
                                                                                   'UOM');
                  end if;
               end if;
            end if;
         end loop;
      end if;
   end if;

   if L_skip_link_codes is NOT NULL and
      L_skip_link_codes.COUNT > 0 then

      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and gtt.link_code_id   IN (select VALUE(err)
                                      from table(cast(L_skip_link_codes as OBJ_NUMERIC_ID_TABLE)) err);
   end if;

   O_error_tbl := L_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                     OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                 NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_LINK_CODE;
-------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEM(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                       I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CREATE_PC_VO_SQL.VALIDATE_ITEM';

   L_error_tbl         OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_zone_ranging      NUMBER(1)                   := NULL;
   L_workspace_ids     OBJ_NUMERIC_ID_TABLE        := NEW OBJ_NUMERIC_ID_TABLE();
   L_valid             NUMBER(1)                   := NULL;
   L_skip_workspace_id NUMBER(15)                  := NULL;
   L_error_message     VARCHAR2(255)               := NULL;

   cursor C_ZONE_RANGING is
      select so.zone_ranging
        from rpm_system_options so;

   cursor C_RANGED_LOC_ITEM is
      select distinct
             gtt.rpm_price_workspace_id,
             gtt.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item            = gtt.item
         and im.item_level      = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where im.dept  = ril.dept
                            and ril.item = gtt.item
                            and ril.loc  = gtt.location)
      union all
      select distinct
             gtt.rpm_price_workspace_id,
             gtt.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent         = gtt.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where im.dept  = ril.dept
                            and ril.item = im.item
                            and ril.loc  = gtt.location);

   cursor C_RANGED_ZONE_ITEM is
      select distinct
             gtt.rpm_price_workspace_id,
             gtt.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item                = gtt.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and im.dept     = ril.dept
                            and ril.item    = gtt.item
                            and ril.loc     = rzl.location)
      union all
      select distinct
             gtt.rpm_price_workspace_id,
             gtt.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent         = gtt.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and im.dept     = ril.dept
                            and ril.item    = im.item
                            and ril.loc     = rzl.location);

   cursor C_LOC_ITEM_IN_LC is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             item_master im,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item            = gtt.item
         and im.item_level      = im.tran_level
         and rlca.item          = im.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code
      union all
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             item_master im,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and rlca.item          = im.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code;

   cursor C_ZONE_ITEM_IN_LC is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             item_master im,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item            = gtt.item
         and im.item_level      = im.tran_level
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = im.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code
      union all
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             item_master im,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = im.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code;

   cursor C_NO_UOM_LOC_ITEM is
      select distinct
             gtt.rpm_price_workspace_id
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where im.dept  = ril.dept
                            and ril.item = im.item
                            and ril.loc  = gtt.location);

   cursor C_NO_UOM_ZONE_ITEM is
      select distinct
             gtt.rpm_price_workspace_id
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item            = gtt.item
         and im.item_level      = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and ril.dept    = im.dept
                            and ril.item    = gtt.item
                            and ril.loc     = rzl.location)
      union all
      select distinct
             gtt.rpm_price_workspace_id
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and ril.dept    = im.dept
                            and ril.item    = im.item
                            and ril.loc     = rzl.location);

   cursor C_ITEMS is
      select distinct rpm_price_workspace_id,
             item
        from (select /*+ ORDERED */
                     gtt.rpm_price_workspace_id,
                     im.item
                from rpm_pc_rec_gtt gtt,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item            = gtt.item
                 and im.item_level      = im.tran_level
              union all
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id,
                     im.item
                from rpm_pc_rec_gtt gtt,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item_parent     = gtt.item
                 and im.item_level      = im.tran_level)
       order by rpm_price_workspace_id,
                item;

BEGIN

   open C_ZONE_RANGING;
   fetch C_ZONE_RANGING into L_zone_ranging;
   close C_ZONE_RANGING;

   for rec IN C_RANGED_LOC_ITEM loop

      L_error_tbl.EXTEND();
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_id,
                                                                    NULL,
                                                                    'item_location_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND();
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

   end loop;

   for rec IN C_RANGED_ZONE_ITEM loop

      L_error_tbl.EXTEND();
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_display_id,
                                                                    rec.zone_node_id,
                                                                    'item_zone_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND();
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

   end loop;

   if L_workspace_ids is NOT NULL and
      L_workspace_ids.COUNT > 0 then
      ---
      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id         = LP_ui_workflow_id
         and gtt.item_level             = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                   VALUE(err)
                                              from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 then

      for rec IN C_LOC_ITEM_IN_LC loop

         L_error_tbl.EXTEND();
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND();
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      for rec IN C_ZONE_ITEM_IN_LC loop

         L_error_tbl.EXTEND();
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND();
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         ---
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                      distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);

      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      I_create_pc_tbl(1).change_selling_uom is NULL then
      ---
      for rec IN C_NO_UOM_LOC_ITEM loop

         L_error_tbl.EXTEND();
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND();
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      for rec IN C_NO_UOM_ZONE_ITEM loop

         L_error_tbl.EXTEND();
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND();
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         ---
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                      distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);

      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      (I_create_pc_tbl(1).change_selling_uom is NOT NULL or
       I_create_pc_tbl(1).multi_selling_uom is NOT NULL) then
      ---
      for rec IN C_ITEMS loop

         if L_skip_workspace_id is NULL or
            rec.rpm_price_workspace_id != L_skip_workspace_id then
            ---
            L_valid := 1;

            if I_create_pc_tbl(1).change_selling_uom is NOT NULL then

               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS (L_error_message,
                                                   L_valid,
                                                   I_create_pc_tbl(1).change_selling_uom,
                                                   rec.item,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                    OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                NULL,
                                                                NULL,
                                                                RPM_CONSTANTS.PLSQL_ERROR,
                                                                SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                   L_error_message,
                                                                                   L_program,
                                                                                   NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND();
                  L_workspace_ids(L_workspace_ids.COUNT()) := L_skip_workspace_id;
                  L_error_tbl.EXTEND();
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).change_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;
            end if;

            if L_valid = 1 and
               I_create_pc_tbl(1).multi_selling_uom is NOT NULL then
               ---
               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS (L_error_message,
                                                   L_valid,
                                                   I_create_pc_tbl(1).multi_selling_uom,
                                                   rec.item,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                    OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                NULL,
                                                                NULL,
                                                                RPM_CONSTANTS.PLSQL_ERROR,
                                                                SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                   L_error_message,
                                                                                   L_program,
                                                                                   NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND();
                  L_workspace_ids(L_workspace_ids.COUNT()) := L_skip_workspace_id;
                  L_error_tbl.EXTEND();
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).multi_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;
            end if;
         end if;
     end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         ---
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                      distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);

      end if;

   end if;

   O_error_tbl := L_error_tbl;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                     OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                 NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_ITEM;
-------------------------------------------------------------------------------

FUNCTION VALIDATE_PARENT_DIFF(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                              I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CREATE_PC_VO_SQL.VALIDATE_PARENT_DIFF';

   L_error_tbl         OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_zone_ranging      NUMBER(1)                   := NULL;
   L_workspace_ids     OBJ_NUMERIC_ID_TABLE        := NEW OBJ_NUMERIC_ID_TABLE();
   L_valid             NUMBER(1)                   := NULL;
   L_skip_workspace_id NUMBER(15)                  := NULL;
   L_error_message     VARCHAR2(255)               := NULL;

   cursor C_RANGED_LOC_ITEM is
      select distinct
             gtt.rpm_price_workspace_id,
             im.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent         = gtt.item
         and im.item_level          = im.tran_level
         and (   im.diff_1          = gtt.diff_id
              or im.diff_2          = gtt.diff_id
              or im.diff_3          = gtt.diff_id
              or im.diff_3          = gtt.diff_id)
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = im.item
                            and ril.loc  = gtt.location);

   cursor C_RANGED_ZONE_ITEM is
      select distinct
             gtt.rpm_price_workspace_id,
             im.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent         = gtt.item
         and im.item_level          = im.tran_level
         and (   im.diff_1          = gtt.diff_id
              or im.diff_2          = gtt.diff_id
              or im.diff_3          = gtt.diff_id
              or im.diff_3          = gtt.diff_id)
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and ril.item    = im.item
                            and ril.loc     = rzl.location);

   cursor C_LOC_ITEM_IN_LC is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             item_master im,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and (   im.diff_1      = gtt.diff_id
              or im.diff_2      = gtt.diff_id
              or im.diff_3      = gtt.diff_id
              or im.diff_3      = gtt.diff_id)
         and rlca.item          = im.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code;

   cursor C_ZONE_ITEM_IN_LC is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             item_master im,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and (   im.diff_1      = gtt.diff_id
              or im.diff_2      = gtt.diff_id
              or im.diff_3      = gtt.diff_id
              or im.diff_3      = gtt.diff_id)
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = im.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code;

   cursor C_NO_UOM_LOC_ITEM is
      select distinct
             gtt.rpm_price_workspace_id
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and (   im.diff_1      = gtt.diff_id
              or im.diff_2      = gtt.diff_id
              or im.diff_3      = gtt.diff_id
              or im.diff_3      = gtt.diff_id)
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = im.item
                            and ril.loc  = gtt.location);

   cursor C_NO_UOM_ZONE_ITEM is
      select distinct
             gtt.rpm_price_workspace_id
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and (   im.diff_1      = gtt.diff_id
              or im.diff_2      = gtt.diff_id
              or im.diff_3      = gtt.diff_id
              or im.diff_3      = gtt.diff_id)
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and ril.item    = im.item
                            and ril.loc     = rzl.location);

   cursor C_ITEMS is
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
                      im.item
        from rpm_pc_rec_gtt gtt,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent     = gtt.item
         and im.item_level      = im.tran_level
         and (   im.diff_1      = gtt.diff_id
              or im.diff_2      = gtt.diff_id
              or im.diff_3      = gtt.diff_id
              or im.diff_3      = gtt.diff_id)
       order by gtt.rpm_price_workspace_id,
                im.item;

BEGIN

   select zone_ranging
     into L_zone_ranging
     from rpm_system_options;

   for rec IN C_RANGED_LOC_ITEM loop
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_id,
                                                                    NULL,
                                                                    'item_location_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
   end loop;

   for rec IN C_RANGED_ZONE_ITEM loop
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_display_id,
                                                                    rec.zone_node_id,
                                                                    'item_zone_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
   end loop;

   if L_workspace_ids is NOT NULL and
      L_workspace_ids.COUNT > 0 then
      --
      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id         = LP_ui_workflow_id
         and gtt.item_level             = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and gtt.rpm_price_workspace_id IN (select VALUE(err)
                                              from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 then

      for rec IN C_LOC_ITEM_IN_LC loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      for rec IN C_ZONE_ITEM_IN_LC loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.PARENT_ITEM_DIFF
            and gtt.rpm_price_workspace_id IN (select distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      I_create_pc_tbl(1).change_selling_uom is NULL then

      for rec IN C_NO_UOM_LOC_ITEM loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.extend;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      for rec IN C_NO_UOM_ZONE_ITEM loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.PARENT_ITEM_DIFF
            and gtt.rpm_price_workspace_id IN (select distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      (I_create_pc_tbl(1).change_selling_uom is NOT NULL or
       I_create_pc_tbl(1).multi_selling_uom is NOT NULL) then

      for rec IN C_ITEMS loop

         if L_skip_workspace_id is NULL or
            rec.rpm_price_workspace_id != L_skip_workspace_id then
            ---
            L_valid := 1;
            if I_create_pc_tbl(1).change_selling_uom is NOT NULL then
               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS(L_error_message,
                                                  L_valid,
                                                  I_create_pc_tbl(1).change_selling_uom,
                                                  rec.item,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                 OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                             NULL,
                                                             NULL,
                                                             RPM_CONSTANTS.PLSQL_ERROR,
                                                             SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                L_error_message,
                                                                                L_program,
                                                                                NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND;
                  L_workspace_ids(L_workspace_ids.COUNT()) := L_skip_workspace_id;
                  L_error_tbl.EXTEND;
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).change_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;
            end if;

            if L_valid = 1 and
               I_create_pc_tbl(1).multi_selling_uom is NOT NULL then

               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS(L_error_message,
                                                  L_valid,
                                                  I_create_pc_tbl(1).multi_selling_uom,
                                                  rec.item,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                 OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                             NULL,
                                                             NULL,
                                                             RPM_CONSTANTS.PLSQL_ERROR,
                                                             SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                L_error_message,
                                                                                L_program,
                                                                                NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND;
                  L_workspace_ids(L_workspace_ids.COUNT) := L_skip_workspace_id;
                  L_error_tbl.EXTEND;
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).multi_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;
            end if;
         end if;
     end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.PARENT_ITEM_DIFF
            and gtt.rpm_price_workspace_id IN (select distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;
   end if;
   --
   O_error_tbl := L_error_tbl;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                     OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                 NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_PARENT_DIFF;
-------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEM_LIST(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                            I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CREATE_PC_VO_SQL.VALIDATE_ITEM_LIST';

   L_error_tbl         OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_zone_ranging      NUMBER(1)                   := NULL;
   L_workspace_ids     OBJ_NUMERIC_ID_TABLE        := NEW OBJ_NUMERIC_ID_TABLE();
   L_valid             NUMBER(1)                   := NULL;
   L_skip_workspace_id NUMBER(15)                  := NULL;
   L_error_message     VARCHAR2(255)               := NULL;

   cursor C_RANGED_LOC_ITEM is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             sd.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and sd.skulist         = gtt.skulist
         and im.item            = sd.item
         and im.item_level      = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = im.item
                            and ril.loc  = gtt.location)
      union all
      select distinct
             gtt.rpm_price_workspace_id,
             sd.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and sd.skulist             = gtt.skulist
         and im.item_parent         = sd.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.item = im.item
                            and ril.loc = gtt.location);

   cursor C_RANGED_ZONE_ITEM is
      select distinct
             gtt.rpm_price_workspace_id,
             sd.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and sd.skulist             = gtt.skulist
         and im.item                = sd.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and ril.item    = im.item
                            and ril.loc     = rzl.location)
      union all
      select distinct
             gtt.rpm_price_workspace_id,
             sd.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and sd.skulist             = gtt.skulist
         and im.item_parent         = sd.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rzl.zone_id = gtt.zone_id
                            and ril.item    = im.item
                            and ril.loc     = rzl.location);

   cursor C_LOC_ITEM_IN_LC is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and sd.skulist         = gtt.skulist
         and im.item            = sd.item
         and im.item_level      = im.tran_level
         and rlca.item          = im.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code
      union all
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and sd.skulist         = gtt.skulist
         and im.item_parent     = sd.item
         and im.item_level      = im.tran_level
         and rlca.item          = im.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code;

   cursor C_ZONE_ITEM_IN_LC is
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and sd.skulist         = gtt.skulist
         and im.item            = sd.item
         and im.item_level      = im.tran_level
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = im.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code
      union all
      select /*+ ORDERED */ distinct
             gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             skulist_detail sd,
             item_master im,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and sd.skulist         = gtt.skulist
         and im.item_parent     = sd.item
         and im.item_level      = im.tran_level
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = im.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code;

   cursor C_NO_UOM_LOC_ITEM is
      select distinct
             rpm_price_workspace_id
        from (select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     skulist_detail sd,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and sd.skulist         = gtt.skulist
                 and im.item            = sd.item
                 and im.item_level      = im.tran_level
                 and NOT EXISTS (select 1
                                   from rpm_item_loc ril
                                  where ril.item = im.item
                                    and ril.loc  = gtt.location)
              union all
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     skulist_detail sd,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and sd.skulist         = gtt.skulist
                 and im.item_parent     = sd.item
                 and im.item_level      = im.tran_level
                 and NOT EXISTS (select 1
                                   from rpm_item_loc ril
                                  where ril.item = im.item
                                    and ril.loc  = gtt.location));

   cursor C_NO_UOM_ZONE_ITEM is
      select distinct
             rpm_price_workspace_id
        from (select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     skulist_detail sd,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and sd.skulist         = gtt.skulist
                 and im.item            = sd.item
                 and im.item_level      = im.tran_level
                 and NOT EXISTS (select 1
                                   from rpm_zone_location rzl,
                                        rpm_item_loc ril
                                  where rzl.zone_id = gtt.zone_id
                                    and ril.item    = im.item
                                    and ril.loc     = rzl.location)
              union all
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     skulist_detail sd,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and sd.skulist         = gtt.skulist
                 and im.item_parent     = sd.item
                 and im.item_level      = im.tran_level
                 and NOT EXISTS (select 1
                                   from rpm_zone_location rzl,
                                        rpm_item_loc ril
                                  where rzl.zone_id = gtt.zone_id
                                    and ril.item    = im.item
                                    and ril.loc     = rzl.location));

   cursor C_ITEMS is
      select distinct rpm_price_workspace_id,
             item
        from (select /*+ ORDERED */
                     gtt.rpm_price_workspace_id,
                     im.item
                from rpm_pc_rec_gtt gtt,
                     skulist_detail sd,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and sd.skulist         = gtt.skulist
                 and im.item            = sd.item
                 and im.item_level      = im.tran_level
              union all
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id,
                     im.item
                from rpm_pc_rec_gtt gtt,
                     skulist_detail sd,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and sd.skulist         = gtt.skulist
                 and im.item_parent     = sd.item
                 and im.item_level      = im.tran_level)
       order by rpm_price_workspace_id,
                item;

BEGIN

   select zone_ranging
     into L_zone_ranging
     from rpm_system_options;

   for rec IN C_RANGED_LOC_ITEM loop
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_id,
                                                                    NULL,
                                                                    'item_location_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
   end loop;

   for rec IN C_RANGED_ZONE_ITEM loop
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_display_id,
                                                                    rec.zone_node_id,
                                                                    'item_zone_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
   end loop;

   if L_workspace_ids is NOT NULL and
      L_workspace_ids.COUNT > 0 then
      --
      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id         = LP_ui_workflow_id
         and gtt.item_level             = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and gtt.rpm_price_workspace_id IN (select VALUE(err)
                                              from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 then

      for rec IN C_LOC_ITEM_IN_LC loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
      end loop;

      for rec IN C_ZONE_ITEM_IN_LC loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and gtt.rpm_price_workspace_id IN (select distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      I_create_pc_tbl(1).change_selling_uom is NULL then

      for rec IN C_NO_UOM_LOC_ITEM loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
      end loop;

      for rec IN C_NO_UOM_ZONE_ITEM loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and gtt.rpm_price_workspace_id IN (select distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      (I_create_pc_tbl(1).change_selling_uom is NOT NULL or
       I_create_pc_tbl(1).multi_selling_uom is NOT NULL) then

      for rec IN C_ITEMS loop

         if L_skip_workspace_id is NULL or
            rec.rpm_price_workspace_id != L_skip_workspace_id then

            L_valid := 1;

            if I_create_pc_tbl(1).change_selling_uom is NOT NULL then

               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS(L_error_message,
                                                  L_valid,
                                                  I_create_pc_tbl(1).change_selling_uom,
                                                  rec.item,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                 OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                             NULL,
                                                             NULL,
                                                             RPM_CONSTANTS.PLSQL_ERROR,
                                                             SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                L_error_message,
                                                                                L_program,
                                                                                NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND;
                  L_workspace_ids(L_workspace_ids.COUNT()) := L_skip_workspace_id;
                  L_error_tbl.EXTEND;
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).change_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;
            end if;

            if L_valid = 1 and
               I_create_pc_tbl(1).multi_selling_uom is NOT NULL then

               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS(L_error_message,
                                                  L_valid,
                                                  I_create_pc_tbl(1).multi_selling_uom,
                                                  rec.item,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                 OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                             NULL,
                                                             NULL,
                                                             RPM_CONSTANTS.PLSQL_ERROR,
                                                             SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                L_error_message,
                                                                                L_program,
                                                                                NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND;
                  L_workspace_ids(L_workspace_ids.COUNT()) := L_skip_workspace_id;
                  L_error_tbl.EXTEND;
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).multi_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;
            end if;
         end if;
     end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and gtt.rpm_price_workspace_id IN (select distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;
   end if;

   O_error_tbl := L_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                     OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                 NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_ITEM_LIST;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_PRICE_EVENT_ITEM_LIST(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                                        I_create_pc_tbl IN     OBJ_CREATE_PC_TBL)
RETURN NUMBER IS

   L_program                   VARCHAR2(55)                := 'RPM_CREATE_PC_VO_SQL.VALIDATE_PRICE_EVENT_ITEM_LIST';
   L_error_tbl                 OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();
   L_zone_ranging              NUMBER(1)                   := NULL;
   L_workspace_ids             OBJ_NUMERIC_ID_TABLE        := NEW OBJ_NUMERIC_ID_TABLE();
   L_valid                     NUMBER(1)                   := NULL;
   L_skip_workspace_id         NUMBER(15)                  := NULL;
   L_error_message             VARCHAR2(255)               := NULL;

   cursor C_RANGED_LOC_ITEM is
      -- Finds tran level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rmld.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             item_master im
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rmld.merch_list_id = gtt.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item            = rmld.item
         and im.item_level      = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = im.dept
                            and ril.item = im.item
                            and ril.loc  = gtt.location)
      union all
      -- Finds parent level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rmld.item,
             gtt.location zone_node_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rmld.merch_list_id     = gtt.price_event_itemlist
         and rmld.merch_level       = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent         = rmld.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_item_loc ril
                          where ril.dept = im.dept
                            and ril.item = im.item
                            and ril.loc  = gtt.location);

   cursor C_RANGED_ZONE_ITEM is
      -- Finds tran level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rmld.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id     = gtt.price_event_itemlist
         and rmld.merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                = rmld.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where ril.dept    = im.dept
                            and rzl.zone_id = gtt.zone_id
                            and ril.item    = im.item
                            and ril.loc     = rzl.location)
      union all
      -- Finds parent level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rmld.item,
             gtt.zone_id zone_node_id,
             gtt.zone_display_id zone_node_display_id,
             gtt.zone_node_type
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             item_master im
       where NVL(L_zone_ranging, 0) = 1
         and gtt.ui_workflow_id     = LP_ui_workflow_id
         and gtt.item_level         = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id     = gtt.price_event_itemlist
         and rmld.merch_level       = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent         = rmld.item
         and im.item_level          = im.tran_level
         and NOT EXISTS (select 1
                           from rpm_zone_location rzl,
                                rpm_item_loc ril
                          where ril.dept    = im.dept
                            and rzl.zone_id = gtt.zone_id
                            and ril.item    = im.item
                            and ril.loc     = rzl.location);

   cursor C_LOC_ITEM_IN_LC is
      -- Finds tran level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rmld.merch_list_id = gtt.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rlca.item          = rmld.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code
      union all
      -- Finds parent level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             item_master im,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rmld.merch_list_id = gtt.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent     = rmld.item
         and im.item_level      = im.tran_level
         and rlca.item          = im.item
         and rlca.location      = gtt.location
         and rco.code_id        = rlca.link_code;

   cursor C_ZONE_ITEM_IN_LC is
      -- Finds tran level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id = gtt.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = rmld.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code
      union all
      -- Finds parent level items
      select /*+ ORDERED */
             distinct gtt.rpm_price_workspace_id,
             rlca.item,
             rlca.location,
             rco.code link_code
        from rpm_pc_rec_gtt gtt,
             rpm_merch_list_detail rmld,
             item_master im,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca,
             rpm_codes rco
       where gtt.ui_workflow_id = LP_ui_workflow_id
         and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rmld.merch_list_id = gtt.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent     = rmld.item
         and im.item_level      = im.tran_level
         and rzl.zone_id        = gtt.zone_id
         and rlca.item          = im.item
         and rlca.location      = rzl.location
         and rco.code_id        = rlca.link_code;

   cursor C_NO_UOM_LOC_ITEM is
      select distinct rpm_price_workspace_id
        from (-- Finds tran level items
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rmld.merch_list_id = gtt.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item            = rmld.item
                 and im.item_level      = im.tran_level
                 and rownum             > 0
                 and NOT EXISTS (select 1
                                   from rpm_item_loc ril
                                  where ril.dept = im.dept
                                    and ril.item = im.item
                                    and ril.loc  = gtt.location)
              union all
              -- Finds parent level items
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rmld.merch_list_id = gtt.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent     = rmld.item
                 and im.item_level      = im.tran_level
                 and rownum             > 0
                 and NOT EXISTS (select 1
                                   from rpm_item_loc ril
                                  where ril.dept = im.dept
                                    and ril.item = im.item
                                    and ril.loc  = gtt.location));

   cursor C_NO_UOM_ZONE_ITEM is
      select distinct rpm_price_workspace_id
        from (-- Finds tran level items
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rmld.merch_list_id = gtt.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item            = rmld.item
                 and im.item_level      = im.tran_level
                 and rownum             > 0
                 and NOT EXISTS (select 1
                                   from rpm_zone_location rzl,
                                        rpm_item_loc ril
                                  where ril.dept    = im.dept
                                    and rzl.zone_id = gtt.zone_id
                                    and ril.item    = im.item
                                    and ril.loc     = rzl.location)
              union all
              -- Finds parent level items
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id
                from rpm_pc_rec_gtt gtt,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rmld.merch_list_id = gtt.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent     = rmld.item
                 and im.item_level      = im.tran_level
                 and rownum             > 0
                 and NOT EXISTS (select 1
                                   from rpm_zone_location rzl,
                                        rpm_item_loc ril
                                  where ril.dept    = im.dept
                                    and rzl.zone_id = gtt.zone_id
                                    and ril.item    = im.item
                                    and ril.loc     = rzl.location));

   cursor C_ITEMS is
      select distinct rpm_price_workspace_id,
             item
        from (-- Finds tran level items
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id,
                     rmld.item
                from rpm_pc_rec_gtt gtt,
                     rpm_merch_list_detail rmld
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rmld.merch_list_id = gtt.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rownum             > 0
              union all
              -- Finds parent level items
              select /*+ ORDERED */
                     gtt.rpm_price_workspace_id,
                     im.item
                from rpm_pc_rec_gtt gtt,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.ui_workflow_id = LP_ui_workflow_id
                 and gtt.item_level     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rmld.merch_list_id = gtt.price_event_itemlist
                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent     = rmld.item
                 and im.item_level      = im.tran_level
                 and rownum             > 0)
       order by rpm_price_workspace_id,
                item;

BEGIN

   select zone_ranging into L_zone_ranging
     from rpm_system_options;

   for rec IN C_RANGED_LOC_ITEM loop
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_id,
                                                                    NULL,
                                                                    'item_location_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
   end loop;

   for rec in C_RANGED_ZONE_ITEM loop
      L_error_tbl.EXTEND;
      L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                    rec.zone_node_display_id,
                                                                    rec.zone_node_id,
                                                                    'item_zone_is_invalid',
                                                                    'itemLocations');
      L_workspace_ids.EXTEND;
      L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
   end loop;

   if L_workspace_ids is NOT NULL and
      L_workspace_ids.COUNT > 0 then
      --
      delete from rpm_pc_rec_gtt gtt
       where gtt.ui_workflow_id         = LP_ui_workflow_id
         and gtt.item_level             = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                   VALUE(err)
                                              from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 then
      for rec IN C_LOC_ITEM_IN_LC loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
      end loop;

      for rec IN C_ZONE_ITEM_IN_LC loop
         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(rec.item,
                                                                       rec.location,
                                                                       rec.link_code,
                                                                       'item_location_attached_to_link_code',
                                                                       'itemLocations');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;
      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                      distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      I_create_pc_tbl(1).change_selling_uom is NULL then

      for rec IN C_NO_UOM_LOC_ITEM loop

         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      for rec IN C_NO_UOM_ZONE_ITEM loop

         L_error_tbl.EXTEND;
         L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       'selling_uom_required',
                                                                       'newSellingUOM');
         L_workspace_ids.EXTEND;
         L_workspace_ids(L_workspace_ids.COUNT) := rec.rpm_price_workspace_id;

      end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and gtt.rpm_price_workspace_id IN (select /*+ CARDINALITY(err 10) */
                                                      distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   L_workspace_ids := NEW OBJ_NUMERIC_ID_TABLE();

   if NVL(I_create_pc_tbl(1).clearance_ind, 0) = 0 and
      I_create_pc_tbl(1).change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
      (I_create_pc_tbl(1).change_selling_uom is NOT NULL or
       I_create_pc_tbl(1).multi_selling_uom is NOT NULL) then

      for rec IN C_ITEMS loop

         if L_skip_workspace_id is NULL or
            rec.rpm_price_workspace_id != L_skip_workspace_id then

            L_valid := 1;

            if I_create_pc_tbl(1).change_selling_uom is NOT NULL then

               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS (L_error_message,
                                                   L_valid,
                                                   I_create_pc_tbl(1).change_selling_uom,
                                                   rec.item,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL) = 0 then

                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                    OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                NULL,
                                                                NULL,
                                                                RPM_CONSTANTS.PLSQL_ERROR,
                                                                SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                   L_error_message,
                                                                                   L_program,
                                                                                   NULL)));
                  return 0;

               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND;
                  L_workspace_ids(L_workspace_ids.COUNT) := L_skip_workspace_id;
                  L_error_tbl.EXTEND();
                  L_error_tbl(L_error_tbl.COUNT) := OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).change_selling_uom,
                                                                                rec.item,
                                                                                NULL,
                                                                                'invalid_uom_conversion',
                                                                                'UOM');
               end if;

            end if;

            if L_valid = 1 and
               I_create_pc_tbl(1).multi_selling_uom is NOT NULL then
               if RPM_WRAPPER.VALID_UOM_FOR_ITEMS (L_error_message,
                                                   L_valid,
                                                   I_create_pc_tbl(1).multi_selling_uom,
                                                   rec.item,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL) = 0 then
                  O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                                    OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                                NULL,
                                                                NULL,
                                                                RPM_CONSTANTS.PLSQL_ERROR,
                                                                SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                                   L_error_message,
                                                                                   L_program,
                                                                                   NULL)));
                  return 0;
               end if;

               if L_valid = 0 then

                  L_skip_workspace_id := rec.rpm_price_workspace_id;
                  L_workspace_ids.EXTEND;
                  L_workspace_ids(L_workspace_ids.COUNT) := L_skip_workspace_id;
                  L_error_tbl.EXTEND;
                  L_error_tbl(L_error_tbl.COUNT) :=
                                        OBJ_STR_STR_STR_STR_STR_REC(I_create_pc_tbl(1).multi_selling_uom,
                                                                    rec.item,
                                                                    NULL,
                                                                    'invalid_uom_conversion',
                                                                    'UOM');
               end if;

            end if;

         end if;

     end loop;

      if L_workspace_ids is NOT NULL and
         L_workspace_ids.COUNT > 0 then
         --
         delete from rpm_pc_rec_gtt gtt
          where gtt.ui_workflow_id         = LP_ui_workflow_id
            and gtt.item_level             = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and gtt.rpm_price_workspace_id in (select /*+ CARDINALITY(err 10) */
                                                      distinct VALUE(err)
                                                 from table(cast(L_workspace_ids as OBJ_NUMERIC_ID_TABLE)) err);
      end if;

   end if;

   O_error_tbl := L_error_tbl;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                     OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                 NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_PRICE_EVENT_ITEM_LIST;

-------------------------------------------------------------------------------
FUNCTION VALIDATE_AND_GENERATE_PC(O_error_tbl        OUT OBJ_STR_STR_STR_STR_STR_TBL,
                                  O_pc_rec_tbl       OUT OBJ_PC_TBL,
                                  I_create_pc_tbl IN     OBJ_CREATE_PC_TBL,
                                  I_user_id       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CREATE_PC_VO_SQL.VALIDATE_AND_GENERATE_PC';

   L_merch_nodes OBJ_MERCH_NODE_TBL  := NULL;
   L_zone_nodes  OBJ_NUM_NUM_STR_TBL := NULL;

   L_error_tbl OBJ_STR_STR_STR_STR_STR_TBL := NEW OBJ_STR_STR_STR_STR_STR_TBL();

   cursor C_PC_REC is
      select OBJ_PC_REC(NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        I_create_pc_tbl(1).clearance_ind,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        gtt.item,
                        gtt.item_description,
                        gtt.item_level,
                        NULL,
                        gtt.diff_id,
                        gtt.diff_id_description,
                        gtt.dept,
                        gtt.class,
                        gtt.subclass,
                        gtt.skulist,
                        gtt.skulist_desc,
                        gtt.zone_id,
                        gtt.zone_display_id,
                        gtt.location,
                        gtt.location_description,
                        gtt.zone_node_type,
                        gtt.link_code_id,
                        gtt.link_code,
                        gtt.link_code_desc,
                        NULL,
                        gtt.currency_code,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        I_create_pc_tbl(1).change_type,
                        I_create_pc_tbl(1).change_amount,
                        I_create_pc_tbl(1).change_currency,
                        I_create_pc_tbl(1).change_percent,
                        I_create_pc_tbl(1).change_selling_uom,
                        I_create_pc_tbl(1).null_multi_ind,
                        I_create_pc_tbl(1).multi_units,
                        I_create_pc_tbl(1).multi_unit_retail,
                        I_create_pc_tbl(1).multi_unit_retail_currency,
                        I_create_pc_tbl(1).multi_selling_uom,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        gtt.price_event_itemlist,
                        gtt.price_event_itemlist_desc,
                        NULL,
                        NULL,
                        NULL,
                        NULL, -- cust_attr_id
                        I_create_pc_tbl(1).cust_attributes)
        from (select pcrec.item,
                     pcrec.item_description,
                     case
                        when pcrec.link_code_id is NOT NULL then
                           NULL
                        when pcrec.skulist is NOT NULL then
                           NULL
                        when pcrec.price_event_itemlist is NOT NULL then
                           NULL
                        when pcrec.item_level = RPM_CONSTANTS.ITEM_LEVEL_ITEM and
                             im.item_level = im.tran_level - 1 then
                           2
                        when pcrec.item_level = RPM_CONSTANTS.PARENT_ITEM_DIFF then
                           2
                        when pcrec.item_level = RPM_CONSTANTS.ITEM_LEVEL_ITEM and
                             im.item_level = im.tran_level then
                           1
                        else
                           NULL
                     end item_level,
                     pcrec.diff_id,
                     pcrec.diff_id_description,
                     pcrec.dept,
                     pcrec.class,
                     pcrec.subclass,
                     pcrec.skulist,
                     case
                        when pcrec.skulist is NOT NULL then
                           SUBSTR(pcrec.item_description, 1, 120)
                        else
                           NULL
                     end skulist_desc,
                     pcrec.zone_id,
                     pcrec.zone_display_id,
                     pcrec.location,
                     pcrec.location_description,
                     pcrec.zone_node_type,
                     pcrec.link_code_id,
                     pcrec.link_code,
                     case
                        when pcrec.link_code_id is NOT NULL then
                           SUBSTR(pcrec.item_description, 1, 40)
                        else
                           NULL
                     end link_code_desc,
                     pcrec.currency_code,
                     pcrec.price_event_itemlist,
                     case
                        when pcrec.price_event_itemlist is NOT NULL then
                           SUBSTR(pcrec.item_description, 1, 120)
                        else
                           NULL
                     end price_event_itemlist_desc,
                     pcrec.cust_attr_id
                from rpm_pc_rec_gtt pcrec,
                     item_master im
               where pcrec.ui_workflow_id = LP_ui_workflow_id
                 and im.item (+)          = pcrec.item) gtt;

BEGIN

   -- Assume the Input Object will always be populated and populated with ONE record ONLY

   L_merch_nodes := I_create_pc_tbl(1).merch_nodes;
   L_zone_nodes  := I_create_pc_tbl(1).zone_nodes;
   O_error_tbl   := NEW OBJ_STR_STR_STR_STR_STR_TBL();

   -- Generate All Merch Node/Zone Node combinations
   delete
     from rpm_pc_rec_gtt
    where ui_workflow_id = LP_ui_workflow_id;

   insert into rpm_pc_rec_gtt (rpm_price_workspace_id,
                               ui_workflow_id,
                               item_level,
                               dept,
                               class,
                               subclass,
                               item,
                               diff_id,
                               diff_id_description,
                               link_code_id,
                               link_code,
                               skulist,
                               item_description,
                               zone_id,
                               zone_display_id,
                               location,
                               location_description,
                               currency_code,
                               zone_node_type,
                               price_event_itemlist)
      select RPM_PRICE_WORKSPACE_SEQ.NEXTVAL,
             LP_ui_workflow_id,
             item_level,
             dept,
             class,
             subclass,
             item,
             diff_id,
             diff_id_description,
             link_code_id,
             link_code,
             skulist,
             merch_node_desc,
             zone_id,
             zone_display_id,
             location location,
             location_description,
             currency_code,
             zone_node_type,
             price_event_itemlist
        from (select /*+ ORDERED */
                     case
                        when itloc.link_code is NOT NULL then
                           RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.DEPT_MERCH_TYPE then
                           RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.CLASS_MERCH_TYPE then
                           RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.SUBCLASS_MERCH_TYPE then
                           RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.PARENT_MERCH_TYPE then
                           RPM_CONSTANTS.ITEM_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE then
                           RPM_CONSTANTS.PARENT_ITEM_DIFF
                        when itloc.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE then
                           RPM_CONSTANTS.ITEM_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.ITEM_LIST_TYPE then
                           RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        when itloc.merch_level_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE then
                           RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                        else
                           NULL
                     end item_level,
                     itloc.dept,
                     itloc.class,
                     itloc.subclass,
                     itloc.item,
                     itloc.diff_id,
                     itloc.diff_id_description,
                     itloc.link_code link_code_id,
                     itloc.skulist,
                     itloc.merch_node_desc,
                     rco.code link_code,
                     NULL zone_id,
                     NULL zone_display_id,
                     locat.location location,
                     locat.location_description,
                     locat.currency_code,
                     itloc.zone_node_type,
                     itloc.price_event_itemlist
                from (select /*+ CARDINALITY(it 10) CARDINALITY(loc 10)*/
                             it.merch_level_type,
                             it.dept,
                             it.class,
                             it.subclass,
                             it.item,
                             it.diff_id,
                             it.diff_id_description,
                             it.link_code,
                             it.skulist,
                             it.merch_node_desc,
                             loc.number_1 location,
                             loc.number_2 zone_node_type,
                             it.price_event_itemlist
                        from table(cast(L_merch_nodes as OBJ_MERCH_NODE_TBL)) it,
                             table(cast(L_zone_nodes as OBJ_NUM_NUM_STR_TBL)) loc
                       where loc.number_2 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                         and rownum       > 0) itloc,
                     (select store location,
                             store_name location_description,
                             currency_code
                        from store
                      union all
                      select wh location,
                             wh_name location_description,
                             currency_code
                        from wh) locat,
                     rpm_codes rco
               where locat.location  = itloc.location
                 and rco.code_id (+) = NVL(itloc.link_code, -999999)
              union all
              select /*+ ORDERED */
                     case
                        when itz.link_code is NOT NULL then
                           RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.DEPT_MERCH_TYPE then
                           RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.CLASS_MERCH_TYPE then
                           RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.SUBCLASS_MERCH_TYPE then
                           RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.PARENT_MERCH_TYPE then
                           RPM_CONSTANTS.ITEM_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE then
                           RPM_CONSTANTS.PARENT_ITEM_DIFF
                        when itz.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE then
                           RPM_CONSTANTS.ITEM_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.ITEM_LIST_TYPE then
                           RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        when itz.merch_level_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE then
                           RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                        else
                           NULL
                     end item_level,
                     itz.dept,
                     itz.class,
                     itz.subclass,
                     itz.item,
                     itz.diff_id,
                     itz.diff_id_description,
                     itz.link_code link_code_id,
                     itz.skulist,
                     itz.merch_node_desc,
                     rco.code link_code,
                     rz.zone_id,
                     rz.zone_display_id,
                     NULL,
                     rz.name location_description,
                     rz.currency_code,
                     itz.zone_node_type,
                     itz.price_event_itemlist
                from (select /*+ CARDINALITY(it 10) CARDINALITY(loc 10)*/
                             it.merch_level_type,
                             it.dept,
                             it.class,
                             it.subclass,
                             it.item,
                             it.diff_id,
                             it.diff_id_description,
                             it.link_code,
                             it.skulist,
                             it.merch_node_desc,
                             loc.number_1 zone_id,
                             loc.number_2 zone_node_type,
                             it.price_event_itemlist
                        from table(cast(L_merch_nodes as OBJ_MERCH_NODE_TBL)) it,
                             table(cast(L_zone_nodes as OBJ_NUM_NUM_STR_TBL)) loc
                       where loc.number_2 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rownum       > 0) itz,
                     rpm_zone rz,
                     rpm_codes rco
               where rz.zone_id      = itz.zone_id
                 and rco.code_id (+) = NVL(itz.link_code, -999999));

   -- Link Code can not be combined by any other level
   if L_merch_nodes(1).link_code is NOT NULL then

      if VALIDATE_LINK_CODE(L_error_tbl,
                            I_create_pc_tbl,
                            I_user_id) = 0 then
         return 0;
      end if;

      if L_error_tbl is NOT NULL and
         L_error_tbl.COUNT > 0 then

         for i IN 1..L_error_tbl.COUNT loop

            O_error_tbl.EXTEND;

            O_error_tbl(O_error_tbl.COUNT) := L_error_tbl(i);

         end loop;

      end if;

   else

      if VALIDATE_ITEM(L_error_tbl,
                       I_create_pc_tbl) = 0 then

         return 0;

      end if;

      if L_error_tbl is NOT NULL and
         L_error_tbl.COUNT > 0 then

         for i IN 1..L_error_tbl.COUNT loop

            O_error_tbl.EXTEND;

            O_error_tbl(O_error_tbl.COUNT) := L_error_tbl(i);

         end loop;

      end if;

      if VALIDATE_PARENT_DIFF(L_error_tbl,
                              I_create_pc_tbl) = 0 then

         return 0;

      end if;

      if L_error_tbl is NOT NULL and
         L_error_tbl.COUNT > 0 then

         for i IN 1..L_error_tbl.COUNT loop

            O_error_tbl.EXTEND;

            O_error_tbl(O_error_tbl.COUNT) := L_error_tbl(i);

         end loop;

      end if;

      if VALIDATE_ITEM_LIST(L_error_tbl,
                            I_create_pc_tbl) = 0 then
         return 0;

      end if;

      if L_error_tbl is NOT NULL and
         L_error_tbl.COUNT > 0 then

         for i IN 1..L_error_tbl.COUNT loop

            O_error_tbl.EXTEND;

            O_error_tbl(O_error_tbl.COUNT) := L_error_tbl(i);

         end loop;

      end if;

      if VALIDATE_PRICE_EVENT_ITEM_LIST(L_error_tbl,
                                        I_create_pc_tbl) = 0 then
         return 0;

      end if;

      if L_error_tbl is NOT NULL and
         L_error_tbl.COUNT > 0 then

         for i IN 1..L_error_tbl.COUNT loop

            O_error_tbl.EXTEND;

            O_error_tbl(O_error_tbl.COUNT) := L_error_tbl(i);

         end loop;

      end if;

   end if;

   open C_PC_REC;
   fetch C_PC_REC BULK COLLECT into O_pc_rec_tbl;
   close C_PC_REC;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_tbl := OBJ_STR_STR_STR_STR_STR_TBL(
                     OBJ_STR_STR_STR_STR_STR_REC(NULL,
                                                 NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_AND_GENERATE_PC;
-------------------------------------------------------------------------------
END RPM_CREATE_PC_VO_SQL;
/
