CREATE OR REPLACE TRIGGER RPM_TABLE_MRD_AIUDR
 AFTER INSERT OR UPDATE OR DELETE ON RPM_MERCH_RETAIL_DEF
 FOR EACH ROW

BEGIN

   if INSERTING and
      :NEW.merch_type != 3 then

      insert into rpm_prim_zone_modifications
         (prim_zone_mod_id,
          merch_retail_def_id,
          merch_type,
          dept,
          class,
          subclass,
          new_pzg,
          old_pzg)
      values (RPM_PZG_MODIFICATION_SEQ.NEXTVAL,
              :NEW.merch_retail_def_id,
              :NEW.merch_type,
              :NEW.dept,
              :NEW.class,
              :NEW.subclass,
              :NEW.regular_zone_group,
              NULL);

   elsif UPDATING then

      insert into rpm_prim_zone_modifications
         (prim_zone_mod_id,
          merch_retail_def_id,
          merch_type,
          dept,
          class,
          subclass,
          new_pzg,
          old_pzg)
      values (RPM_PZG_MODIFICATION_SEQ.NEXTVAL,
              :NEW.merch_retail_def_id,
              :NEW.merch_type,
              :NEW.dept,
              :NEW.class,
              :NEW.subclass,
              :NEW.regular_zone_group,
              :OLD.regular_zone_group);

   elsif DELETING then

      insert into rpm_prim_zone_modifications
         (prim_zone_mod_id,
          merch_retail_def_id,
          dept,
          class,
          subclass,
          new_pzg,
          old_pzg)
      values (RPM_PZG_MODIFICATION_SEQ.NEXTVAL,
              :OLD.merch_retail_def_id,
              :OLD.dept,
              :OLD.class,
              :OLD.subclass,
              NULL,
              :OLD.regular_zone_group);
   end if;

END;
/