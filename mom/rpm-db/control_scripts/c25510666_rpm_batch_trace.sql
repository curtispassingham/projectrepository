-- Deleting the earlier entries
delete from rpm_batch_trace
/

-- Seed data for Injector batch
INSERT INTO rpm_batch_trace
  (batch_name, enable_trace
  )
SELECT 'INJ',
  'N'
FROM dual
WHERE NOT EXISTS
  (SELECT 1 FROM rpm_batch_trace WHERE batch_name='INJ'
  )
/

-- Seed data for LocMoveBatch batch
INSERT INTO rpm_batch_trace
  (batch_name, enable_trace
  )
SELECT 'LM',
  'N'
FROM dual
WHERE NOT EXISTS
  (SELECT 1 FROM rpm_batch_trace WHERE batch_name='LM'
  )
/

-- Seed data for LocMoveSch batch
INSERT INTO rpm_batch_trace
  (batch_name, enable_trace
  )
SELECT 'LMS',
  'N'
FROM dual
WHERE NOT EXISTS
  (SELECT 1 FROM rpm_batch_trace WHERE batch_name='LMS'
  )
/

-- Seed data for NILBatch batch
INSERT INTO rpm_batch_trace
  (batch_name, enable_trace
  )
SELECT 'NIL',
  'N'
FROM dual
WHERE NOT EXISTS
  (SELECT 1 FROM rpm_batch_trace WHERE batch_name='NIL'
  )
/

-- Seed data for PEEBatch batch
INSERT INTO rpm_batch_trace
  (batch_name, enable_trace
  )
SELECT 'PEE',
  'N'
FROM dual
WHERE NOT EXISTS
  (SELECT 1 FROM rpm_batch_trace WHERE batch_name='PEE'
  )
/
