#! /bin/ksh
#-------------------------------------------------------------------------
#  File: rms_oi_forecast_history.ksh
#
#  Desc: UNIX shell script to save 4 weeks of item weekly forecasted sales
#        data to the item_forecast_hist table.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='rms_oi_forecast_history.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
  "
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: RETAIN_FORECAST_HISTORY 
# Purpose      : This will call FORECASTS_SQL.RETAIL_FORECAST_HIST which is
#                responsible for preserving 4 weeks of forecasted sales data.
#-------------------------------------------------------------------------
function RETAIN_FORECAST_HISTORY 
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_error_message   VARCHAR2(100);
      BEGIN
         if NOT FORECASTS_SQL.RETAIN_FORECAST_HIST(:GV_script_error) then
            raise FUNCTION_ERROR;
         end if;
         commit;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "FORECASTS_SQL.RETAIN_FORECAST_HIST Failed" "RETAIN_FORECAST_HISTORY" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "FORECASTS_SQL.RETAIN_FORECAST_HIST - Successfully Completed" "RETAIN_FORECAST_HISTORY" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}


#-----------------------------------------------
# Main program starts
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#Validate input parameters
CONNECT=$1

if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "rms_oi_forecast_history.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#--- Invoke Function
RETAIN_FORECAST_HISTORY

#--  Check for any Oracle errors from the SQLPLUS process
if [[ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]]; then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program rms_oi_forecast_history.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

exit 0

