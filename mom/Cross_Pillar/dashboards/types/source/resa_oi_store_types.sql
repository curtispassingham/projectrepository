DROP TYPE RESA_OI_STORE_STATUS_TBL FORCE;
/
DROP TYPE RESA_OI_STORE_STATUS_REC FORCE;
/

CREATE OR REPLACE TYPE RESA_OI_STORE_STATUS_REC AS OBJECT
(
  OPEN_STORE             NUMBER(20),
  NOT_LOADED_STORE       NUMBER(20),
  PARTIAL_LOADED_STORE   NUMBER(20),
  BUSINESS_DATE          DATE
)
/

CREATE OR REPLACE TYPE RESA_OI_STORE_STATUS_TBL AS TABLE OF RESA_OI_STORE_STATUS_REC
/
--------------------------------------------------------------------------------
DROP TYPE RESA_OI_LATE_POLLING_TBL FORCE;
/
DROP TYPE RESA_OI_LATE_POLLING_REC FORCE;
/

CREATE OR REPLACE TYPE RESA_OI_LATE_POLLING_REC AS OBJECT
(
  STORE               NUMBER(10),
  STORE_NAME          VARCHAR2(240),
  LATE_POLL_DAYS      NUMBER(3),
  LATE_POLL_IND       VARCHAR2(1)
)
/

CREATE OR REPLACE TYPE RESA_OI_LATE_POLLING_TBL AS TABLE OF RESA_OI_LATE_POLLING_REC
/
-----------------------------------------------------------------------------