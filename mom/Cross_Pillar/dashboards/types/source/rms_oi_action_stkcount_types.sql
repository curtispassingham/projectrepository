DROP TYPE RMS_OI_CYCLE_COUNT_STATUS_REC    FORCE;
DROP TYPE RMS_OI_CYCLE_COUNT_FAIL_TBL      FORCE;
DROP TYPE RMS_OI_CYCLE_COUNT_FAIL_REC      FORCE;
DROP TYPE RMS_OI_CYCLE_COUNT_LOC_TBL       FORCE;
DROP TYPE RMS_OI_CYCLE_COUNT_LOC_REC       FORCE;
DROP TYPE RMS_OI_CYLCNT_DCS_STATUS_REC     FORCE;
DROP TYPE RMS_OI_CYLCNT_LOC_DCS_REC        FORCE;
DROP TYPE RMS_OI_CYLCNT_LOC_DCS_TBL        FORCE;
DROP TYPE RMS_OI_CYLCNT_LOC_DCS_FAIL_REC   FORCE;
DROP TYPE RMS_OI_CYLCNT_LOC_DCS_FAIL_TBL   FORCE;


-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE RMS_OI_CYCLE_COUNT_FAIL_REC AS OBJECT
(
   CYCLE_COUNT       NUMBER(10),
   LOCATION          NUMBER(10),
   ERROR_MESSAGE     VARCHAR2(255)
)
/
CREATE OR REPLACE TYPE RMS_OI_CYCLE_COUNT_FAIL_TBL AS TABLE OF RMS_OI_CYCLE_COUNT_FAIL_REC
/
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE RMS_OI_CYCLE_COUNT_LOC_REC AS OBJECT
(
   CYCLE_COUNT       NUMBER(10),
   LOCATION          NUMBER(10)
)
/
CREATE OR REPLACE TYPE RMS_OI_CYCLE_COUNT_LOC_TBL AS TABLE OF RMS_OI_CYCLE_COUNT_LOC_REC
/
-----------------------------------------------------------------------------
CREATE OR REPLACE TYPE RMS_OI_CYCLE_COUNT_STATUS_REC AS OBJECT
(
   SUCCESS_CYCLE_COUNT_COUNT  NUMBER(12),
   SUCCESS_CYCLE_COUNT_TBL    RMS_OI_CYCLE_COUNT_LOC_TBL,
   FAIL_CYCLE_COUNT_COUNT     NUMBER(12),
   FAIL_CYCLE_COUNT_TBL       RMS_OI_CYCLE_COUNT_FAIL_TBL
)
/
-----------------------------------------------------------------------------
create or replace TYPE RMS_OI_CYLCNT_LOC_DCS_REC AS OBJECT
(
   CYCLE_COUNT       NUMBER(10),
   LOCATION          NUMBER(10),
   DEPT              NUMBER(4),
   CLASS             NUMBER(4),
   SUBCLASS          NUMBER(4)
)
/
-----------------------------------------------------------------------------
create or replace TYPE RMS_OI_CYLCNT_LOC_DCS_TBL AS TABLE OF RMS_OI_CYLCNT_LOC_DCS_REC
/
-----------------------------------------------------------------------------
create or replace TYPE RMS_OI_CYLCNT_LOC_DCS_FAIL_REC AS OBJECT
(
   CYCLE_COUNT       NUMBER(10),
   LOCATION          NUMBER(10),
   DEPT              NUMBER(4),
   CLASS             NUMBER(4),
   SUBCLASS          NUMBER(4),
   ERROR_MESSAGE     VARCHAR2(255)
)
/
-----------------------------------------------------------------------------
create or replace TYPE RMS_OI_CYLCNT_LOC_DCS_FAIL_TBL AS TABLE OF RMS_OI_CYLCNT_LOC_DCS_FAIL_REC
/
-----------------------------------------------------------------------------
create or replace TYPE RMS_OI_CYLCNT_DCS_STATUS_REC AS OBJECT
(
   SUCCESS_CYCLE_COUNT_COUNT  NUMBER(12),
   SUCCESS_CYCLE_COUNT_TBL    RMS_OI_CYLCNT_LOC_DCS_TBL,
   FAIL_CYCLE_COUNT_COUNT     NUMBER(12),
   FAIL_CYCLE_COUNT_TBL       RMS_OI_CYLCNT_LOC_DCS_FAIL_TBL
)
/
