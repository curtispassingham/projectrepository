
prompt dropping RMS_OI_MARGIN_IMPACT_REC AND RMS_OI_MARGIN_IMPACT_TBL
------------------------------------------------------------------------------------------
BEGIN
  EXECUTE immediate 'DROP type RMS_OI_MARGIN_IMPACT_TBL force';
  EXECUTE immediate 'DROP type RMS_OI_MARGIN_IMPACT_REC force';
EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/
------------------------------------------------------------------------------------------
prompt creating RMS_OI_MARGIN_IMPACT_REC

CREATE OR REPLACE TYPE RMS_OI_MARGIN_IMPACT_REC AS OBJECT (ITEM              VARCHAR2(25),
                                                           LOC               NUMBER(10),
                                                           ACTIVE_DATE       DATE,
                                                           UNIT_COST         NUMBER(20,4),
                                                           UNIT_RETAIL       NUMBER(20,4),
                                                           MARGIN_PERCENT    NUMBER(20,4),
                                                           CURRENCY_CODE     VARCHAR2(3));
/

------------------------------------------------------------------------------------------
prompt creating RMS_OI_MARGIN_IMPACT_TBL
CREATE OR REPLACE TYPE RMS_OI_MARGIN_IMPACT_TBL AS TABLE OF RMS_OI_MARGIN_IMPACT_REC;
/
      
------------------------------------------------------------------------------------------