CREATE OR REPLACE PACKAGE BODY RESA_OI_TENDER_SQL AS
--------------------------------------------------------------------------------
FUNCTION TENDER_SUMMARY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result        IN OUT RESA_OI_TENDER_TBL,
                        I_store         IN     SA_STORE_DAY.STORE%TYPE)
RETURN NUMBER IS
   L_program           VARCHAR2(61) := 'RESA_OI_TENDER_SQL.TENDER_SUMMARY';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;
   
   L_vdate             PERIOD.VDATE%TYPE := GET_VDATE();

   L_date_min          PERIOD.VDATE%TYPE := L_vdate-7;
   L_date_max          PERIOD.VDATE%TYPE := L_vdate-1;
   
   L_sys_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_tender_amt_prim   SA_TRAN_TENDER.TENDER_AMT%TYPE;
   L_tender_total_prim SA_TRAN_TENDER.TENDER_AMT%TYPE;
   
   cursor c_get_tender_summary is
      with v_tender_data as (select /*+ INDEX(ssd SA_STORE_DAY_I1)*/ssd.business_date,
                                     ssd.store_day_seq_no,
                                     ssd.store,
                                     stt.tender_type_group,
                                     sum(stt.tender_amt) tender_amt
                                from sa_store_day ssd,
                                     sa_tran_tender stt,
                                     sa_tran_head sth
                               where sth.tran_seq_no = stt.tran_seq_no
                                 and stt.tender_type_group in ('CASH','CCARD','CHECK')
                                 and business_date between L_date_min and L_date_max
                                 and ssd.store = I_store
                                 and ssd.store_day_seq_no = sth.store_day_seq_no (+)
                                 and exists (select 'x'
                                               from sa_user_loc_traits slt,
                                                    loc_traits_matrix ltm
                                              where slt.loc_trait = ltm.loc_trait
                                                and slt.user_id = get_user()
                                                and ssd.store = ltm.store
                                                and rownum = 1)
                                group by ssd.business_date,
                                         ssd.store_day_seq_no,
                                         ssd.store,
                                         stt.tender_type_group),
             v_date_range as (select L_date_min + level -1 business_date
                                from dual
                             connect by level  <= 7),
                       v1 as (select  vdr.business_date,
                                      vtd.tender_type_group,
                                      vtd.tender_amt,
                                      sum(vtd.tender_amt) over (partition by vtd.business_date) tender_amt_total,
                                      (select code_desc from v_code_detail vcd where vcd.code_type = 'TENT' and vcd.code = vtd.tender_type_group) code_desc,
                                      (select currency_code from v_store vs where vs.store = vtd.store) currency_code
                                from  v_tender_data vtd,
                                      v_date_range vdr
                                where vtd.business_date (+) = vdr.business_date)
      select RESA_OI_TENDER_REC(
                business_date,
                tender_type_group,
                code_desc,
                tender_amt,
                tender_amt_total,
                currency_code,
                NULL, -- tender amt primary
                NULL, -- tender total primary
                NULL  -- currency code primar
             )
      from v1
      order by business_date, 
               tender_type_group desc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   open c_get_tender_summary;

   fetch c_get_tender_summary bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_tender_summary - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_tender_summary;   

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_sys_options_row) = FALSE then
      return OI_UTILITY.FAILURE;
    end if;

   for i in 1..O_result.count loop

      L_tender_amt_prim := NULL;
      L_tender_total_prim := NULL;
 
      if (O_result(i).currency_store != L_sys_options_row.currency_code) then
         
         O_result(i).currency_prim := L_sys_options_row.currency_code;
         
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_result(i).tender_amt,
                                 O_result(i).currency_store,
                                 L_sys_options_row.currency_code,
                                 L_tender_amt_prim,
                                 'R',
                                 NULL,
                                 NULL) = FALSE then
            return OI_UTILITY.FAILURE;
         end if;

         O_result(i).tender_amt_prim := L_tender_amt_prim;
         
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_result(i).tender_total,
                                 O_result(i).currency_store,
                                 L_sys_options_row.currency_code,
                                 L_tender_total_prim,
                                 'R',
                                 NULL,
                                 NULL) = FALSE then
            return OI_UTILITY.FAILURE;
         end if;

         O_result(i).tender_total_prim := L_tender_total_prim;

      else
         O_result(i).tender_amt_prim := O_result(i).tender_amt;
         O_result(i).tender_total_prim := O_result(i).tender_total;
         O_result(i).currency_prim := O_result(i).currency_store;
      end if;
   end loop;

   OI_UTILITY.LOG_TIME(L_program,L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END TENDER_SUMMARY;
--------------------------------------------------------------------------------
END RESA_OI_TENDER_SQL;
/