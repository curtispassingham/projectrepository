CREATE OR REPLACE PACKAGE BODY RESA_OI_STORE_SQL AS
--------------------------------------------------------------------------------
FUNCTION STORE_DAY_STATUS_HIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_result        IN OUT RESA_OI_STORE_STATUS_TBL,
                               I_store         IN     SA_STORE_DAY.STORE%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RESA_OI_STORE.STORE_DAY_STATUS_HIST';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_min_date    PERIOD.VDATE%TYPE := GET_VDATE()-7;
   L_max_date    PERIOD.VDATE%TYPE := GET_VDATE()-1;

   cursor c_get_status_hist is
   select RESA_OI_STORE_STATUS_REC(
             NVL(data.open_store,0), -- OPEN_STORE,
             NVL(data.load_store,0),-- NOT_LOADED_STORE,
             NVL(data.parital_store,0),-- PARTIAL_LOADED_STORE,
             date_range.business_date -- BUSINESS_DATE
          )
     from (select L_min_date + level -1 business_date
             from dual
             connect by level  <= L_max_date-L_min_date+1) date_range,
          (select count(case when not ssd.audit_status = 'A' then ssd.store end) open_store,
                  count(case when ssd.data_status = 'R' then ssd.store end) load_store,
                  count(case when ssd.data_status = 'P' then ssd.store end) parital_store,
                  ssd.business_date
             from v_store v,
                  sa_store_day ssd
            where v.store       = ssd.store
              and ssd.store     = NVL(I_store, ssd.store)
              and ssd.business_date between L_min_date and L_max_date
              and ssd.data_status in ('P','R')
              and ssd.audit_status != 'A'
              and exists (select 'x'
                            from sa_user_loc_traits slt,
                                 loc_traits_matrix ltm
                           where slt.loc_trait = ltm.loc_trait
                             and slt.user_id = get_user()
                             and ssd.store = ltm.store
                             and rownum = 1)
             group by ssd.business_date) data
     where data.business_date (+) = date_range.business_date
     order by date_range.business_date desc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   open c_get_status_hist;

   fetch c_get_status_hist bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_status_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_status_hist;

   OI_UTILITY.LOG_TIME(L_program,L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END STORE_DAY_STATUS_HIST;
--------------------------------------------------------------------------------
FUNCTION LATE_POLLING_STORES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result        IN OUT RESA_OI_LATE_POLLING_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RESA_OI_STORE.LATE_POLLING_STORES';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_start_prev_month   DATE  := NULL;
   L_start_curr_month   DATE  := NULL;
   L_end_curr_month     DATE  := NULL;

   L_month_start        DATE  := NULL;
   L_month_end          DATE  := NULL;

   cursor c_get_month_range is
   select max(first_day) start_prev_454_month,
          p.start_454_month,
          p.end_454_month
     from calendar c,
          period p
    where c.first_day < p.START_454_MONTH
    group by p.start_454_month,
             p.end_454_month;

   cursor c_get_late_polling is
   select RESA_OI_LATE_POLLING_REC(data.store,       -- STORE
                                   data.store_name,  -- STORE_NAME
                                   data.late_days,   -- LATE_POLL_DAYS
                                   data.late_poll_ind)    -- LATE_POLL_IND (valid values 'P'revious month or  C'urrent month
     from (select v.store,
                  v.store_name,
                  (count(ssd.business_date)) late_days,
                  'P' late_poll_ind,
                  row_number() over (order by (count(ssd.business_date)) desc) rank_days
             from v_store v,
                  sa_store_day ssd
             where ssd.store = v.store
               and ssd.business_date between L_start_prev_month and (L_start_curr_month - 1)
               and ssd.data_status in ('P','R')
               and exists (select 'x'
                             from sa_user_loc_traits slt,
                                  loc_traits_matrix ltm
                            where slt.loc_trait = ltm.loc_trait
                              and slt.user_id = get_user()
                              and ssd.store = ltm.store
                              and rownum = 1)
             group by v.store,
                      v.store_name
            union all
           select v.store,
                  v.store_name,
                  (count(ssd.business_date)) late_days,
                  'C' late_poll_ind,
                  row_number() over (order by (count(ssd.business_date)) desc) rank_days
             from v_store v,
                  sa_store_day ssd
             where ssd.store = v.store
               and ssd.business_date between L_start_curr_month and L_end_curr_month
               and ssd.data_status in ('P','R')
               and exists (select 'x'
                             from sa_user_loc_traits slt,
                                  loc_traits_matrix ltm
                            where slt.loc_trait = ltm.loc_trait
                              and slt.user_id = get_user()
                              and ssd.store = ltm.store
                              and rownum = 1)
             group by v.store,
                      v.store_name) data
    where data.rank_days <= 10;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   open c_get_month_range;

   fetch c_get_month_range into L_start_prev_month,
                                L_start_curr_month,
                                L_end_curr_month;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_month_range - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_month_range;

   open c_get_late_polling;

   fetch c_get_late_polling bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_late_polling - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_late_polling;

   OI_UTILITY.LOG_TIME(L_program,L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END LATE_POLLING_STORES;
--------------------------------------------------------------------------------
END RESA_OI_STORE_SQL;
/