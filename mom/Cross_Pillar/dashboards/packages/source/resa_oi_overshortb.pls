CREATE OR REPLACE PACKAGE BODY RESA_OI_OVERSHORT_SQL AS
--------------------------------------------------------------------------------
FUNCTION OVERSHORT_SUMMARY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result        IN OUT RESA_OI_OVERSHORT_TBL)
RETURN NUMBER IS
   L_program          VARCHAR2(61) := 'RESA_OI_OVERSHORT_SQL.OVERSHORT_SUMMARY';
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;
   
   L_min_date         PERIOD.VDATE%TYPE := GET_VDATE()-7;
   L_max_date         PERIOD.VDATE%TYPE := GET_VDATE()-1;
   
   L_st_curr_count    NUMBER(10) := 0;
   L_sys_options_row  SYSTEM_OPTIONS%ROWTYPE;
   
   L_short_prim       NUMBER(20,4) := NULL;
   L_over_prim        NUMBER(20,4) := NULL;
   
   cursor c_get_store_curr_count is
   select count(distinct v.currency_code)
     from v_store v,
          sa_user_loc_traits slt,
          loc_traits_matrix ltm
    where slt.loc_trait = ltm.loc_trait
      and slt.user_id = get_user()
      and v.store = ltm.store;

   cursor c_get_overshort is
   select RESA_OI_OVERSHORT_REC(
             NVL(data.over_amt,0),
             NVL(data.short_amt,0),
             data.currency_code,
             date_range.business_date,
             data.store,
             data.store_name
          )
     from (select L_min_date + level -1 business_date
             from dual
             connect by level  <= 7) date_range,
          (select sum(case when stv.value < 0 then stv.value end) over_amt,
                  sum(case when stv.value >= 0 then stv.value end)  short_amt,
                  vs.currency_code,
                  ssd.business_date,
                  vs.store,
                  vs.store_name
             from v_sa_total_value stv,
                  v_store vs,
                  sa_store_day ssd,
                  sa_total st
            where st.total_id = 'OVRSHT_S'
              and ssd.business_date between L_min_date and L_max_date
              and st.total_seq_no = stv.total_seq_no
              and st.store_day_seq_no = stv.store_day_seq_no
              and stv.store_day_seq_no = ssd.store_day_seq_no
              and vs.store = ssd.store
              and exists (select 'x'
                            from sa_user_loc_traits slt,
                                 loc_traits_matrix ltm
                           where slt.loc_trait = ltm.loc_trait
                             and slt.user_id = get_user()
                             and ssd.store = ltm.store
                             and rownum = 1)
            group by vs.currency_code, 
                     ssd.business_date,   
                     vs.store, 
                     vs.store_name) data
    where date_range.business_date = data.business_date (+)
    order by date_range.business_date desc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   
   open c_get_overshort;

   fetch c_get_overshort bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_overshort - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_overshort;   
   
   open c_get_store_curr_count;
   fetch c_get_store_curr_count into L_st_curr_count;
   close c_get_store_curr_count;

   /* convert the over/short amounts to primary currency
      if the assigned stores have multiple currency codes */

   if (L_st_curr_count > 1) then

      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_sys_options_row) = FALSE then
         return OI_UTILITY.FAILURE;
      end if;

      for i in 1..O_result.count loop
         
         L_short_prim  := NULL;
         L_over_prim   := NULL;

         if (O_result(i).currency_code is NOT NULL and 
             O_result(i).currency_code != L_sys_options_row.currency_code) then

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    O_result(i).short_amt,
                                    O_result(i).currency_code,
                                    L_sys_options_row.currency_code,
                                    L_short_prim,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               return OI_UTILITY.FAILURE;
            end if;

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    O_result(i).over_amt,
                                    O_result(i).currency_code,
                                    L_sys_options_row.currency_code,
                                    L_over_prim,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               return OI_UTILITY.FAILURE;
            end if;
   
            O_result(i).short_amt := L_short_prim;   
            O_result(i).over_amt := L_over_prim;
            O_result(i).currency_code := L_sys_options_row.currency_code;
         
         end if;
      end loop;

   end if;

   OI_UTILITY.LOG_TIME(L_program,L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END OVERSHORT_SUMMARY;
--------------------------------------------------------------------------------
FUNCTION RC_OVERSHORT_DETAIL(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result           IN OUT RESA_OI_RC_OVERSHORT_TBL,
                             I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
RETURN NUMBER IS
   L_program        VARCHAR2(61) := 'RESA_OI_STORE.RC_OVERSHORT_DETAIL';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;
   
   L_overshort_prim NUMBER(20,4) := NULL;
   
   cursor c_get_rc_overshort is
      select RESA_OI_RC_OVERSHORT_REC(
                sum(data.value),  -- OVERSHORT_AMT
                v.currency_code,  -- CURRENCY_STORE
                NULL,             -- OVERSHORT_AMT_PRIM
                so.currency_code, -- CURRENCY_PRIM
                data.reg_cash    -- REGISTER/CASHIER
             )
        from v_store v,
             system_options so,
            (select stv.value,
                    case when sso.balance_level_ind = 'R' then
                       sbg.register
                    else 
                       case when sso.balance_level_ind = 'C' then
                          sbg.cashier
                       end
                    end as reg_cash,
                    ssd.store
               from v_sa_total_value stv,
                    v_store vs,
                    sa_store_day ssd,
                    sa_total st,
                    sa_balance_group sbg,
                    sa_system_options sso
              where st.total_seq_no = stv.total_seq_no
                and st.store_day_seq_no = ssd.store_day_seq_no
                and stv.store_day_seq_no = ssd.store_day_seq_no
                and sbg.store_day_seq_no = ssd.store_day_seq_no
                and stv.bal_group_seq_no = sbg.bal_group_seq_no
                and st.total_id = 'OVRSHT_B'
                and vs.store = ssd.store
                and ssd.store_day_seq_no = I_store_day_seq_no) data
       where data.store = v.store
         and data.reg_cash is NOT NULL
       group by data.reg_cash, 
                data.store, 
                v.currency_code, 
                so.currency_code;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' Start');

   if (I_store_day_seq_no is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            null,
                                            null,
                                            null);
      return OI_UTILITY.FAILURE;
   end if;

   open c_get_rc_overshort;

   fetch c_get_rc_overshort bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_rc_overshort - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_rc_overshort;   
   
   for i in 1..O_result.count loop

      L_overshort_prim := NULL;
      
      if (O_result(i).currency_store != O_result(i).currency_prim) then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_result(i).overshort_amt,
                                 O_result(i).currency_store,
                                 O_result(i).currency_prim,
                                 L_overshort_prim,
                                 'R',
                                 NULL,
                                 NULL) = FALSE then
            return OI_UTILITY.FAILURE;
         end if;
  
         O_result(i).overshort_amt_prim := L_overshort_prim;
      else
         O_result(i).overshort_amt_prim := O_result(i).overshort_amt;
      end if;
   end loop;
   
   OI_UTILITY.LOG_TIME(L_program,L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END RC_OVERSHORT_DETAIL;
--------------------------------------------------------------------------------
END RESA_OI_OVERSHORT_SQL;
/