CREATE OR REPLACE PACKAGE BODY RMS_OI_ITEM_DETAIL AS
------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAIL_INFO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_result                 OUT RMS_OI_ITEM_DETAIL_REC,
                              I_item                IN     ITEM_MASTER.ITEM%TYPE,
                              I_supplier            IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                              I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                              I_location            IN     ITEM_LOC.LOC%TYPE,
                              I_currency_code       IN     CURRENCIES.CURRENCY_CODE%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(100) := 'RMS_OI_ITEM_DETAIL.GET_ITEM_DETAIL_INFO';
   L_item        ITEM_MASTER.ITEM%TYPE;
   -- cursor to get item basic details like its dept,class, subclass etc
   cursor GET_ITEM_BASIC_INFO is
      select im.item_desc ,
             im.dept , 
             im.class, 
             im.subclass, 
             sc.sub_name,
             im.sellable_ind
        from v_item_master im, 
             v_subclass_tl sc 
       where im.dept = sc.dept 
         and im.class = sc.class 
         and im.subclass = sc.subclass
         and im.item = I_item;
    
   -- cursor to get item image name along with its path.
   cursor GET_ITEM_IMAGE_INFO (C_item ITEM_MASTER.ITEM%TYPE) is
      select TRIM(im.image_addr)||TRIM(im.image_name)
        from item_image im
       where im.item = C_item 
         and im.primary_ind = 'Y'; 
    
   --cursor to get item supplier related details including VPN, supplier pack size and unit cost.
   --Convert unit_cost (on ISC and ISCL) from supplier currency to output currency.
   cursor GET_ITEM_SUPP_INFO ( C_to_currency_code CURRENCIES.CURRENCY_CODE%TYPE,
                               C_location ITEM_LOC.LOC%TYPE) is 
      select isc.item, 
             isc.supplier,
             vs.sup_name,
             isc.origin_country_id,
             CURRENCY_SQL.CONVERT_VALUE('C',C_to_currency_code,vs.currency_code,isc.unit_cost) unit_cost, 
             isc.cost_uom,
             isc.supp_pack_size,
             isp.vpn              
       from  item_supp_country isc, 
             v_sups vs, 
             item_supplier isp              
       where isc.item = L_item  
         and isc.supplier = vs.supplier 
         and isp.supplier = isc.supplier
         and isp.item = isc.item          
         and (I_supplier is NOT NULL and isc.supplier = I_supplier  
          or  I_supplier is NULL and isp.primary_supp_ind ='Y')
         and (I_origin_country_id is NOT NULL and isc.origin_country_id = I_origin_country_id
          or  I_origin_country_id is NULL and isc.primary_country_ind ='Y')  
         and C_location is NULL
   union all
      select iscl.item, 
             iscl.supplier,
             vs.sup_name,
             iscl.origin_country_id,
             CURRENCY_SQL.CONVERT_VALUE('C',C_to_currency_code,vs.currency_code,iscl.unit_cost) unit_cost, 
             isc.cost_uom,    
             isc.supp_pack_size,
             isp.vpn             
        from item_supp_country isc,
             item_supp_country_loc iscl,
             v_sups vs,
             item_supplier isp
       where isc.item  = L_item  
         and iscl.item = isc.item 
         and isc.supplier = iscl.supplier 
         and isc.supplier  = vs.supplier 
         and isp.supplier   = isc.supplier
         and isp.item       = isc.item          
         and isc.origin_country_id = iscl.origin_country_id          
         and iscl.loc = C_location 
         and (I_supplier is NOT NULL and isc.supplier = I_supplier
          or  I_supplier is NULL and isp.primary_supp_ind = 'Y')     
         and (I_origin_country_id is NOT NULL and isc.origin_country_id = I_origin_country_id
          or  I_origin_country_id is NULL and isc.primary_country_ind = 'Y')  
         and C_location is NOT NULL;
        
   --cursor to get item supplier related details including VPN, supplier, supplier name etc..
   -- in case of no entry in the item_supp_country table for item and supplier combination.
   cursor GET_ITEM_SUPP_BASIC_INFO is 
      select isp.supplier,
             vs.sup_name,
             isp.vpn              
       from  v_sups vs, 
             item_supplier isp              
       where isp.item = L_item  
         and isp.supplier = vs.supplier                   
         and (I_supplier is NOT NULL and isp.supplier = I_supplier  
          or  I_supplier is NULL and isp.primary_supp_ind ='Y');
   -- cursor to get item retail info like item unit retail and selling UOM from item_loc if the item is ranged
   -- if not ranged to any location then retail info taken from price_hist table.
   cursor GET_ITEM_RETAIL_INFO (C_location ITEM_LOC.LOC%TYPE) is
      select il.selling_unit_retail,
             il.selling_uom     
        from item_loc il
       where il.item = L_item
         and il.loc = NVL(C_location ,il.loc) 
         and rownum =1
      union all   
      select ph.selling_unit_retail,
             ph.selling_uom
        from price_hist ph
       where ph.item = L_item
         and ph.loc = 0
         and ph.loc_type is NULL
         and C_location is NULL
         and not exists (select 'X' from item_loc where item = L_item);      
         
   -- cursor to get primary virtual WH for the given physical_wh
   cursor GET_PRIMARY_VWH is
      select primary_vwh
        from wh
       where wh = I_location
         and wh = physical_wh;
         
   -- cursor to get the location's currency code and if location is null then system's currency   
   cursor GET_LOC_CURRENCY(C_location ITEM_LOC.LOC%TYPE) is
      select currency_code
        from store
       where store = C_location                              
   union all
      select currency_code
        from wh
       where wh = C_location 
   union all      
      select currency_code
        from partner
       where partner_id = C_location
         and partner_type = 'E'
   union all
        select currency_code 
        from system_options
         where  C_location is NULL;
   
   cursor get_parent_item is
   select item_parent 
    from  item_master
   where  item = I_item;
   
   cursor is_ref_item is
   select 'Y' 
     from item_master
    where item= I_item
      and item_level > tran_level;

   cursor C_DEFAULT_ITEM_IMAGE is
      select TRIM(image_path) || TRIM(default_item_image)
        from system_options;


   L_to_currency_code  CURRENCIES.CURRENCY_CODE%TYPE;
   L_item_basic_info   GET_ITEM_BASIC_INFO%ROWTYPE;
   L_item_supp_info    GET_ITEM_SUPP_INFO%ROWTYPE;
   L_item_supp_basic_info GET_ITEM_SUPP_BASIC_INFO%ROWTYPE;
   L_item_retail_info  GET_ITEM_RETAIL_INFO%ROWTYPE;
   L_location          ITEM_LOC.LOC%TYPE;
   L_loc_currency_code CURRENCIES.CURRENCY_CODE%TYPE;
   L_qty               NUMBER;
   L_standard_uom      UOM_CLASS.UOM%TYPE;
   L_ref_item             VARCHAR(1) :='N';
   L_retail_exists     VARCHAR(1);
   L_unit_cost_prim            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_standard_unit_retail_prim ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_prim         ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_prim  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_prim          ITEM_LOC.SELLING_UOM%TYPE;
   L_prim_currency             CURRENCIES.CURRENCY_CODE%TYPE;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             NULL,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   -- I_currency_code is not passed then default system options currency is used. 
   if I_currency_code is NULL then
      select currency_code into L_to_currency_code
        from system_options;
        if L_to_currency_code is NULL then
           O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_CODE',                                        
                                                  NULL,
                                                  NULL);
           RAISE OI_UTILITY.PROGRAM_ERROR;  
        end if;
   else
      L_to_currency_code := I_currency_code;  
   end if;
   O_result := RMS_OI_ITEM_DETAIL_REC();
   O_result.currency_code := L_to_currency_code;
   --
   open GET_ITEM_BASIC_INFO;
   fetch GET_ITEM_BASIC_INFO into L_item_basic_info;
   close GET_ITEM_BASIC_INFO;   
   if L_item_basic_info.item_desc is NOT NULL then
      O_result.item := I_item;
      O_result.item_description := L_item_basic_info.item_desc;
      O_result.dept := L_item_basic_info.dept;
      O_result.class := L_item_basic_info.class;
      O_result.subclass := L_item_basic_info.subclass;
      O_result.sub_name := L_item_basic_info.sub_name;
   else 
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_01',                                        
                                             NULL,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;                                     
   end if;
   --
   --if the input is a physical wh, use the primary virtual wh of the physical wh to fetch unit_cost and unit_retail 
   L_location := NULL;
   if I_location is NOT NULL then
      open GET_PRIMARY_VWH;
      fetch GET_PRIMARY_VWH into L_location;
      close GET_PRIMARY_VWH;
      if L_location is NULL then
         L_location := I_location;
      end if; 
   end if;
   
   -- Check if the item passed is ref item ; if Yes then take all deficient info from its parent.
   open is_ref_item;
   fetch is_ref_item into L_ref_item;
   close is_ref_item;   
   L_item := NULL;
   if L_ref_item ='Y' then
      open get_parent_item;
      fetch get_parent_item into L_item;
      close get_parent_item;
   end if;
   if L_item is NULL then
     L_item := I_item;   
   end if;   
   --
   open GET_ITEM_SUPP_INFO(L_to_currency_code,L_location);
   fetch GET_ITEM_SUPP_INFO into L_item_supp_info;
   close GET_ITEM_SUPP_INFO; 
   
   select standard_uom into L_standard_uom
     from item_master
    where item = I_item; 
   L_qty := 0;       
 
   if L_item_supp_info.supplier is NOT NULL then
      O_result.supplier := L_item_supp_info.supplier;
      O_result.supplier_name := L_item_supp_info.sup_name;
      O_result.vpn := L_item_supp_info.vpn;
      O_result.origin_country_id := L_item_supp_info.origin_country_id;
      O_result.pack_size := L_item_supp_info.supp_pack_size;
      
      if  L_standard_uom != L_item_supp_info.cost_uom then
         if UOM_SQL.CONVERT(O_error_message,
                            L_qty,
                            L_item_supp_info.cost_uom,
                            1,
                            L_standard_uom,
                            I_item,
                            L_item_supp_info.supplier,
                            L_item_supp_info.origin_country_id) = FALSE then
             RAISE OI_UTILITY.PROGRAM_ERROR; 
         end if;
         --- Convert the unit cost from standard UOM to Cost UOM
         if L_qty <> 0 then
            L_item_supp_info.unit_cost := L_item_supp_info.unit_cost / L_qty;
         end if;
      end if;
      
      O_result.unit_cost := L_item_supp_info.unit_cost;
      O_result.cost_uom := L_item_supp_info.cost_uom;
   else 
      open GET_ITEM_SUPP_BASIC_INFO;
      fetch GET_ITEM_SUPP_BASIC_INFO into L_item_supp_basic_info;
      close GET_ITEM_SUPP_BASIC_INFO; 
      
      if L_item_supp_basic_info.supplier is NOT NULL then
         O_result.supplier := L_item_supp_basic_info.supplier;
         O_result.supplier_name := L_item_supp_basic_info.sup_name;
         O_result.vpn := L_item_supp_basic_info.vpn;
      else
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_SUP_REL_NOT_EXIST',                                        
                                               L_item,
                                               L_program,
                                               NULL);      
         RAISE OI_UTILITY.PROGRAM_ERROR; 
      end if;   
   end if;
   --
   if L_location is NULL then
      if PM_API_SQL.GET_PRIMARY_ZONE_GROUP_LOC(O_error_message,
                                               L_location,
                                               L_item) = FALSE then
         RAISE OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;  
   --L_location can be null that means item is not ranged to any of primary zone's location
   --in that case retail info is taken from any of ranged location 
   --If Item is not ranged to any of location then retail info is taken from price hist table.
   open GET_ITEM_RETAIL_INFO(L_location);
   fetch GET_ITEM_RETAIL_INFO into L_item_retail_info;
   close GET_ITEM_RETAIL_INFO;
   if L_item_retail_info.selling_unit_retail is NOT NULL then               
      open GET_LOC_CURRENCY(L_location);
      fetch GET_LOC_CURRENCY into L_loc_currency_code;
      close GET_LOC_CURRENCY;         
      if L_loc_currency_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_CURR_INFO_FOUND',                                        
                                                L_location,
                                                NULL,
                                                NULL);      
         RAISE OI_UTILITY.PROGRAM_ERROR; 
      end if;
      O_result.selling_unit_retail := CURRENCY_SQL.CONVERT_VALUE('R',L_to_currency_code,L_loc_currency_code,L_item_retail_info.selling_unit_retail);
      O_result.selling_uom := L_item_retail_info.selling_uom;
   elsif L_item_retail_info.selling_unit_retail is NULL then
      if L_item_basic_info.sellable_ind = 'Y' then
         if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message, 
                                                 L_unit_cost_prim,
                                                 L_standard_unit_retail_prim,
                                                 L_standard_uom_prim,
                                                 L_selling_unit_retail_prim,
                                                 L_selling_uom_prim,
                                                 L_item) = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG(O_error_message,
                                                  L_item,
                                                  L_program,
                                                  NULL);      
            RAISE OI_UTILITY.PROGRAM_ERROR; 
         end if;      

         if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                                 L_prim_currency) then
            O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_CODE',                                        
                                                  NULL,
                                                  NULL);
            RAISE OI_UTILITY.PROGRAM_ERROR;  
         end if;
         O_result.selling_unit_retail := CURRENCY_SQL.CONVERT_VALUE('R',L_to_currency_code,L_prim_currency,L_selling_unit_retail_prim);
         O_result.selling_uom := L_selling_uom_prim;		 
      end if;
   end if;      
   
   --
   open GET_ITEM_IMAGE_INFO(I_item);
   fetch GET_ITEM_IMAGE_INFO into O_result.image_addr_name;
   close GET_ITEM_IMAGE_INFO;
   if O_result.image_addr_name is NULL and
      L_ref_item ='Y' then
      open GET_ITEM_IMAGE_INFO(L_item);
      fetch GET_ITEM_IMAGE_INFO into O_result.image_addr_name;
      close GET_ITEM_IMAGE_INFO;
   end if;
   
   --if no image is available for item, show default image set in system_options table
   if O_result.image_addr_name is NULL then
      open C_DEFAULT_ITEM_IMAGE;
      fetch C_DEFAULT_ITEM_IMAGE into O_result.image_addr_name;
      close C_DEFAULT_ITEM_IMAGE;
   end if; 
   --
   return OI_UTILITY.SUCCESS;   
EXCEPTION
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;   
END GET_ITEM_DETAIL_INFO;
------------------------------------------------------------------------------------------------
END RMS_OI_ITEM_DETAIL;
/
            
            