CREATE OR REPLACE PACKAGE RMS_OI_BUYER AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : TODAY_SALES
--Purpose       : This function fetch today sales and top 10 item both by marging
--                and sales
--------------------------------------------------------------------------------
FUNCTION TODAY_SALES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_total_result               OUT RMS_OI_BUYER_SALES_REC,
                     O_top_10_by_sales_result     OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                     O_top_10_by_margin_result    OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                     I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                     I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                     I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                     I_subclasses              IN     OBJ_NUMERIC_ID_TABLE,
                     I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                     I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                     I_brands                  IN     OBJ_VARCHAR_ID_TABLE,
                     I_origin_countries        IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : WEEK_TO_DATE_SALES
--Purpose       : This function fetch week to date sales and top 10 item both by
--                marging and sales
--------------------------------------------------------------------------------
FUNCTION WEEK_TO_DATE_SALES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_total_result               OUT RMS_OI_BUYER_SALES_REC,
                            O_top_10_by_sales_result     OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                            O_top_10_by_margin_result    OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                            I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                            I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                            I_subclasses              IN     OBJ_NUMERIC_ID_TABLE,
                            I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                            I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                            I_brands                  IN     OBJ_VARCHAR_ID_TABLE,
                            I_origin_countries        IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : GET_ITEM_DETAIL
--Purpose       : This function fetches the item detail to be displayed
--------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result                OUT RMS_OI_BUYER_ITEM_PANE_REC,
                         I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_item               IN     ITEM_MASTER.ITEM%TYPE,
                         I_agg_diff_1         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_agg_diff_2         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_agg_diff_3         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_agg_diff_4         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_supplier_sites     IN     OBJ_NUMERIC_ID_TABLE,
                         I_stores             IN     OBJ_NUMERIC_ID_TABLE,
                         I_origin_countries   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : EARLY_LATE_SHIPMENTS
--Purpose       : This function insert PO/Shipment with issues into 
--                rms_oi_buyer_early_late_ship table
--------------------------------------------------------------------------------
FUNCTION EARLY_LATE_SHIPMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_eow_dates             OUT RMS_OI_END_OF_WEEK_DATES_TBL,
                              I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                              I_depts              IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes            IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses         IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites     IN     OBJ_NUMERIC_ID_TABLE,
                              I_stores             IN     OBJ_NUMERIC_ID_TABLE,
                              I_brands             IN     OBJ_VARCHAR_ID_TABLE,
                              I_order_contexts     IN     OBJ_VARCHAR_ID_TABLE,
                              I_origin_countries   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : OTB
--Purpose       : This function fetches Open to Buy values for the next 8 weeks
--------------------------------------------------------------------------------
FUNCTION OTB(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
             O_result            OUT RMS_OI_BUYER_OTB_TBL,
             I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
             I_depts          IN     OBJ_NUMERIC_ID_TABLE,
             I_classes        IN     OBJ_NUMERIC_ID_TABLE,
             I_subclasses     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : ORDER_TO_APPROVE
--Purpose       : This function insert PO needing approval by user in 
--                RMS_OI_BUYER_ORDERS_TO_APPROVE table
--------------------------------------------------------------------------------
FUNCTION ORDER_TO_APPROVE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_depts              IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes            IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses         IN     OBJ_NUMERIC_ID_TABLE,
                          I_supplier_sites     IN     OBJ_NUMERIC_ID_TABLE,
                          I_stores             IN     OBJ_NUMERIC_ID_TABLE,
                          I_brands             IN     OBJ_VARCHAR_ID_TABLE,
                          I_order_contexts     IN     OBJ_VARCHAR_ID_TABLE,
                          I_origin_countries   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : DELETE_SESSION_RECORDS
--Purpose       : This function clean the buyer dashboard tables for the user session.
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : DELETE_SESSION_ORDS_TO_APRV
--Purpose       : This function clean the orders to approve tables for the user session.
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ORDS_TO_APRV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : DELETE_SESSION_EARLY_LT_SHP
--Purpose       : This function clean the early late shipment tables for the user session.
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_EARLY_LT_SHP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : DELETE_EARLY_LATE_SHIPMENTS
--Purpose       : This function deletes POs  with ids that are passed as input
--                from rms_oi_buyer_early_late_ship table
--------------------------------------------------------------------------------
FUNCTION DELETE_EARLY_LATE_SHIPMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                     I_order_ids          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : REFRESH_EARLY_LATE_SHIPMENTS
--Purpose       : This function deletes POs  with ids that are passed as input
--                from rms_oi_buyer_early_late_ship table if criteria conditions
--                not are met.
--------------------------------------------------------------------------------
FUNCTION REFRESH_EARLY_LATE_SHIPMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                      I_order_ids          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : REFRESH_ORDER_TO_APPROVE
--Purpose       : This function deletes POs  with ids that are passed as input
--                from rms_oi_buyer_orders_to_approve table if criteria conditions
--                not are met.
--------------------------------------------------------------------------------
FUNCTION REFRESH_ORDER_TO_APPROVE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_order_ids          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_BUYER;
/