create or replace PACKAGE RMS_OI_DATA_STEWARD AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
   YES_IND                     CONSTANT VARCHAR2(1)    := 'Y';
   NO_IND                      CONSTANT VARCHAR2(1)    := 'N';
---------------------------------------------------------------------------------------------------
FUNCTION SETUP_INCOMPLETE_ITEMS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id             IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                                I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                I_origin_countries       IN     OBJ_VARCHAR_ID_TABLE,
                                I_date                   IN     OBJ_VARCHAR_ID_TABLE,
                                I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                                I_item_type              IN     OBJ_VARCHAR_ID_TABLE,
                                I_item_level             IN     OBJ_NUMERIC_ID_TABLE,
                                I_transaction_level      IN     OBJ_NUMERIC_ID_TABLE,
                                I_uda                    IN     OBJ_NUMERIC_ID_TABLE,
                                I_uda_value              IN     OBJ_NUMERIC_ID_TABLE,
                                I_brands                 IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_INCOMP_ITEMS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
---------------------------------------------------------------------------------------------------
FUNCTION SETUP_MARK_COMPLETE_ITEMS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_session_id             IN OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                   I_items                  IN OBJ_VARCHAR_ID_TABLE,
                                   I_parent_items           IN OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------------
END RMS_OI_DATA_STEWARD;
/
