CREATE OR REPLACE PACKAGE BODY OI_UTILITY AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION SETUP_MERCH_HIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_depts          IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes        IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses     IN     OBJ_NUMERIC_ID_TABLE,
                          I_input_required IN     BOOLEAN DEFAULT TRUE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_MERCH_HIER';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_6_NUM_6_STR_6_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   delete from gtt_6_num_6_str_6_date;
   
   if I_subclasses is not null and I_subclasses.count > 0 then

      if I_depts   is not null and I_depts.count   > 0 and 
         I_classes is not null and I_classes.count > 0 then
         insert into gtt_6_num_6_str_6_date (number_1,
                                             number_2,
                                             number_3,
                                             varchar2_3)
         select sbc.dept,
                sbc.class,
                sbc.subclass,
                sbc.sub_name
           from table(cast(I_depts as OBJ_NUMERIC_ID_TABLE)) input_depts,
                table(cast(I_classes as OBJ_NUMERIC_ID_TABLE)) input_classes,
                table(cast(I_subclasses as OBJ_NUMERIC_ID_TABLE)) input_subclasses,
                v_subclass sbc
          where sbc.dept = value(input_depts)
            and sbc.class = value(input_classes)
            and sbc.subclass = value(input_subclasses);
            
      elsif I_classes is not null and I_classes.count > 0 then
         insert into gtt_6_num_6_str_6_date (number_1,
                                             number_2,
                                             number_3,
                                             varchar2_3)
         select sbc.dept,
                sbc.class,
                sbc.subclass,
                sbc.sub_name
           from table(cast(I_classes as OBJ_NUMERIC_ID_TABLE)) input_classes,
                table(cast(I_subclasses as OBJ_NUMERIC_ID_TABLE)) input_subclasses,
                v_subclass sbc
          where sbc.class = value(input_classes)
            and sbc.subclass = value(input_subclasses);
            
      elsif I_depts is not null and I_depts.count > 0 then
         insert into gtt_6_num_6_str_6_date (number_1,
                                             number_2,
                                             number_3,
                                             varchar2_3)
         select sbc.dept,
                sbc.class,
                sbc.subclass,
                sbc.sub_name
           from table(cast(I_depts as OBJ_NUMERIC_ID_TABLE)) input_depts,
                table(cast(I_subclasses as OBJ_NUMERIC_ID_TABLE)) input_subclasses,
                v_subclass sbc
          where sbc.dept = value(input_depts)
            and sbc.subclass = value(input_subclasses);
      else
         insert into gtt_6_num_6_str_6_date (number_1,
                                             number_2,
                                             number_3,
                                             varchar2_3)
         select sbc.dept,
                sbc.class,
                sbc.subclass,
                sbc.sub_name
           from table(cast(I_subclasses as OBJ_NUMERIC_ID_TABLE)) input_subclasses,
                v_subclass sbc
          where sbc.subclass = value(input_subclasses);
      end if;
      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_6_num_6_str_6_date subclass - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   elsif I_classes is not null and I_classes.count > 0 then
      if I_depts is not null and I_depts.count > 0 then
         insert into gtt_6_num_6_str_6_date (number_1,
                                             number_2,
                                             number_3,
                                             varchar2_3)
         select sbc.dept,
                sbc.class,
                sbc.subclass,
                sbc.sub_name
           from table(cast(I_depts as OBJ_NUMERIC_ID_TABLE)) input_depts,
                table(cast(I_classes as OBJ_NUMERIC_ID_TABLE)) input_classes,
                v_subclass sbc
          where sbc.dept  = value(input_depts)
            and sbc.class = value(input_classes);
      else
         insert into gtt_6_num_6_str_6_date (number_1,
                                             number_2,
                                             number_3,
                                             varchar2_3)
         select sbc.dept,
                sbc.class,
                sbc.subclass,
                sbc.sub_name
           from table(cast(I_classes as OBJ_NUMERIC_ID_TABLE)) input_classes,
                v_subclass sbc
          where sbc.class = value(input_classes);
      end if;
      
      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_6_num_6_str_6_date class - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   elsif I_depts is not null and I_depts.count > 0 then

      insert into gtt_6_num_6_str_6_date (number_1,
                                          number_2,
                                          number_3,
                                          varchar2_3)
      select sbc.dept,
             sbc.class,
             sbc.subclass,
             sbc.sub_name
        from table(cast(I_depts as OBJ_NUMERIC_ID_TABLE)) input_depts,
             v_subclass sbc
       where sbc.dept = value(input_depts);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_6_num_6_str_6_date department - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   elsif I_input_required then  -- no hierarchy filter condition is defined
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_depts',
                                            L_program,
                                            NULL);
      return OI_UTILITY.FAILURE;
   else
      insert into gtt_6_num_6_str_6_date (number_1,
                                          number_2,
                                          number_3,
                                          varchar2_3)
      select sbc.dept,
             sbc.class,
             sbc.subclass,
             sbc.sub_name
        from v_subclass sbc;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_6_num_6_str_6_date all merch hier - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;


   merge into gtt_6_num_6_str_6_date target
   using (select distinct
                 cm.number_1,
                 cm.number_2,
                 d.dept_name,
                 c.class_name
            from gtt_6_num_6_str_6_date cm,
                 v_deps d,
                 v_class c
           where cm.number_1 = d.dept
             and cm.number_1 = c.dept
             and cm.number_2 = c.class) use_this
   on (    target.number_1 = use_this.number_1
       and target.number_2 = use_this.number_2)
   when matched then update
    set target.varchar2_1    = use_this.dept_name,
        target.varchar2_2    = use_this.class_name;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge names gtt_6_num_6_str_6_date - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if LOCK_STATS(O_error_message,
                 I_session_id,
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_MERCH_HIER;
--------------------------------------------------------------------------------
FUNCTION SETUP_ITEMS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                     I_depts             IN     OBJ_NUMERIC_ID_TABLE,
                     I_classes           IN     OBJ_NUMERIC_ID_TABLE,
                     I_subclasses        IN     OBJ_NUMERIC_ID_TABLE,
                     I_supplier_sites    IN     OBJ_NUMERIC_ID_TABLE,
                     I_origin_countries  IN     OBJ_VARCHAR_ID_TABLE,
                     I_brands            IN     OBJ_VARCHAR_ID_TABLE,
                     I_forecast_ind      IN     VARCHAR2,
                     I_input_required    IN     BOOLEAN DEFAULT TRUE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_ITEMS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_10_NUM_10_STR_10_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --insert merch hierarchy (subclasses) into gtt_6_num_6_str_6_date
   --  number_1   --dept
   --  number_2   --class
   --  number_3   --subclass
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  I_input_required) = OI_UTILITY.FAILURE then
     return OI_UTILITY.FAILURE;
   end if;

   --insert items into gtt_10_num_10_str_10_date
   delete from gtt_10_num_10_str_10_date;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   --  number_1    -- dept
   --  number_2    -- class
   --  number_3    -- subclass

   if I_brands is not null and I_brands.count> 0 then
      insert into gtt_10_num_10_str_10_date (varchar2_1,
                                             varchar2_2,
                                             varchar2_3,
                                             varchar2_4,
                                             varchar2_10,
                                             number_1,
                                             number_2,
                                             number_3)
         select vim.item,
                vim.item_parent,
                vim.item_desc,
                im.alc_item_type,
                im.standard_uom,
                vim.dept,
                vim.class,
                vim.subclass
           from gtt_6_num_6_str_6_date sbc,
                table(cast(I_brands as OBJ_VARCHAR_ID_TABLE)) input_brands,
                v_item_master vim,
                item_master im
          where vim.dept         = sbc.number_1
            and vim.class        = sbc.number_2
            and vim.subclass     = sbc.number_3
            and vim.status       = 'A'
            and vim.item_level   = vim.tran_level
            and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
            and vim.brand_name   = value(input_brands)
            and vim.item         = im.item;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_10_num_10_str_10_date item filtered by brand - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   else
      insert into gtt_10_num_10_str_10_date (varchar2_1,
                                             varchar2_2,
                                             varchar2_3,
                                             varchar2_4,
                                             varchar2_10,
                                             number_1,
                                             number_2,
                                             number_3)
         select vim.item,
                vim.item_parent,
                vim.item_desc,
                im.alc_item_type,
                im.standard_uom,
                vim.dept,
                vim.class,
                vim.subclass
           from gtt_6_num_6_str_6_date sbc,
                v_item_master vim,
                item_master im
          where vim.dept         = sbc.number_1
            and vim.class        = sbc.number_2
            and vim.subclass     = sbc.number_3
            and vim.status       = 'A'
            and vim.item_level   = vim.tran_level
            and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
            and vim.item         = im.item;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_10_num_10_str_10_date item no brand filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_origin_countries is not null and I_origin_countries.count > 0 and
      I_supplier_sites   is not null and I_supplier_sites.count   > 0 then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_ctrys,
                                table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups,
                                item_supp_country s
                          where value(input_ctrys) = s.origin_country_id
                            and value(input_sups)  = s.supplier
                            and s.item = gtt.varchar2_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date supplier/country - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;
   --
   if I_origin_countries is not null and I_origin_countries.count > 0 and
      (I_supplier_sites  is null     or  I_supplier_sites.count   = 0) then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_ctrys,
                                item_supp_country s
                          where value(input_ctrys) = s.origin_country_id
                            and s.item = gtt.varchar2_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date country - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_supplier_sites is not null and I_supplier_sites.count   > 0 and
      (I_origin_countries is  null or  I_origin_countries.count = 0) then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups,
                                item_supplier s
                          where value(input_sups) = s.supplier
                            and s.item = gtt.varchar2_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date supplier - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   merge into gtt_10_num_10_str_10_date target
   using (select im.item,
                 decode(imp.diff_1_aggregate_ind, 'Y', im.diff_1, null) agg_diff_1,
                 decode(imp.diff_2_aggregate_ind, 'Y', im.diff_2, null) agg_diff_2,
                 decode(imp.diff_3_aggregate_ind, 'Y', im.diff_3, null) agg_diff_3,
                 decode(imp.diff_4_aggregate_ind, 'Y', im.diff_4, null) agg_diff_4,
                 imp.item_desc item_parent_desc
            from gtt_10_num_10_str_10_date gtt,
                 v_item_master imp,
                 item_master im
           where gtt.varchar2_1   = im.item
             and im.alc_item_type = 'FASHIONSKU'
             and im.item_parent   = imp.item
   ) use_this
   on (target.varchar2_1 = use_this.item)
   when matched then update
    set target.varchar2_5 = agg_diff_1,
        target.varchar2_6 = agg_diff_2,
        target.varchar2_7 = agg_diff_3,
        target.varchar2_8 = agg_diff_4,
        target.varchar2_9 = item_parent_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge gtt_10_num_10_str_10_date agg diffs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if LOCK_STATS(O_error_message,
                 I_session_id,
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_ITEMS;
--------------------------------------------------------------------------------
FUNCTION SETUP_REFRESH_ITEMS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_supplier_sites    IN     OBJ_NUMERIC_ID_TABLE,
                             I_origin_countries  IN     OBJ_VARCHAR_ID_TABLE,
                             I_brands            IN     OBJ_VARCHAR_ID_TABLE,
                             I_forecast_ind      IN     VARCHAR2,
                             I_items             IN     OBJ_VARCHAR_ID_TABLE,
                             I_item_type         IN     item_master.alc_item_type%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_REFRESH_ITEMS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_10_NUM_10_STR_10_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --insert items into gtt_10_num_10_str_10_date
   delete from gtt_10_num_10_str_10_date;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   --  number_1    -- dept
   --  number_2    -- class
   --  number_3    -- subclass

   if I_brands is not null and I_brands.count> 0 then
      if I_item_type not in('STYLE','FA') then
         insert into gtt_10_num_10_str_10_date (varchar2_1,
                                                varchar2_2,
                                                varchar2_3,
                                                varchar2_4,
                                                varchar2_10,
                                                number_1,
                                                number_2,
                                                number_3)
         select vim.item,
                vim.item_parent,
                vim.item_desc,
                im2.alc_item_type,
                im2.standard_uom,
                vim.dept,
                vim.class,
                vim.subclass
          from  (select distinct item_parent item_parent
                            from item_master,
                                 table(cast(I_items  as OBJ_VARCHAR_ID_TABLE)) i_item_ids
                           where item        = value(i_item_ids)
                             and item_parent is not null) im1,
                table(cast(I_brands as OBJ_VARCHAR_ID_TABLE)) input_brands,
                item_master im2,
                v_item_master vim
          where im2.item_parent  = im1.item_parent
            and vim.item         = im2.item
            and vim.status       = 'A'
            and vim.item_level   = vim.tran_level
            and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
            and vim.brand_name   = value(input_brands)
          union all
          select vim.item,
                 vim.item_parent,
                 vim.item_desc,
                 im2.alc_item_type,
                 im2.standard_uom,
                 vim.dept,
                 vim.class,
                 vim.subclass
            from (select item
                    from item_master,
                         table(cast(I_items  as OBJ_VARCHAR_ID_TABLE)) i_item_ids
                   where item        = value(i_item_ids)
                     and item_parent is null) im1,
                 table(cast(I_brands as OBJ_VARCHAR_ID_TABLE)) input_brands,
                 item_master im2,
                 v_item_master vim
           where im2.item         = im1.item
             and vim.item         = im2.item
             and vim.status       = 'A'
             and vim.item_level   = vim.tran_level
             and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
             and vim.brand_name   = value(input_brands);
      else
         insert into gtt_10_num_10_str_10_date (varchar2_1,
                                                varchar2_2,
                                                varchar2_3,
                                                varchar2_4,
                                                varchar2_10,
                                                number_1,
                                                number_2,
                                                number_3)
         select vim.item,
                vim.item_parent,
                vim.item_desc,
                im.alc_item_type,
                im.standard_uom,
                vim.dept,
                vim.class,
                vim.subclass
           from table(cast(I_items  as OBJ_VARCHAR_ID_TABLE)) item_ids,
                table(cast(I_brands as OBJ_VARCHAR_ID_TABLE)) input_brands,
                v_item_master vim,
                item_master im
          where vim.item_parent  = value(item_ids)
            and vim.status       = 'A'
            and vim.item_level   = vim.tran_level
            and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
            and vim.brand_name   = value(input_brands)
            and vim.item         = im.item;
      end if;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_10_num_10_str_10_date item filtered by brand - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   else
      if I_item_type not in('STYLE','FA') then
         insert into gtt_10_num_10_str_10_date (varchar2_1,
                                                varchar2_2,
                                                varchar2_3,
                                                varchar2_4,
                                                varchar2_10,
                                                number_1,
                                                number_2,
                                                number_3)
         select vim.item,
                vim.item_parent,
                vim.item_desc,
                im2.alc_item_type,
                im2.standard_uom,
                vim.dept,
                vim.class,
                vim.subclass
          from  (select distinct item_parent item_parent
                            from item_master,
                                 table(cast(I_items  as OBJ_VARCHAR_ID_TABLE)) i_item_ids
                           where item        = value(i_item_ids)
                             and item_parent is not null) im1,
                item_master im2,
                v_item_master vim
          where im2.item_parent  = im1.item_parent
            and vim.item         = im2.item
            and vim.status       = 'A'
            and vim.item_level   = vim.tran_level
            and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
          union all
          select vim.item,
                 vim.item_parent,
                 vim.item_desc,
                 im2.alc_item_type,
                 im2.standard_uom,
                 vim.dept,
                 vim.class,
                 vim.subclass
            from (select item
                    from item_master,
                         table(cast(I_items  as OBJ_VARCHAR_ID_TABLE)) i_item_ids
                   where item        = value(i_item_ids)
                     and item_parent is null) im1,
                 item_master im2,
                 v_item_master vim
           where im2.item         = im1.item
             and vim.item         = im2.item
             and vim.status       = 'A'
             and vim.item_level   = vim.tran_level
             and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind);
      else
         insert into gtt_10_num_10_str_10_date (varchar2_1,
                                                varchar2_2,
                                                varchar2_3,
                                                varchar2_4,
                                                varchar2_10,
                                                number_1,
                                                number_2,
                                                number_3)
         select vim.item,
                vim.item_parent,
                vim.item_desc,
                im.alc_item_type,
                im.standard_uom,
                vim.dept,
                vim.class,
                vim.subclass
           from table(cast(I_items  as OBJ_VARCHAR_ID_TABLE)) item_ids,
                v_item_master vim,
                item_master im
          where vim.item_parent  = value(item_ids)
            and vim.status       = 'A'
            and vim.item_level   = vim.tran_level
            and vim.forecast_ind = NVL(I_forecast_ind, vim.forecast_ind)
            and vim.item         = im.item;
      end if;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_10_num_10_str_10_date item no brand filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_origin_countries is not null and I_origin_countries.count > 0 and
      I_supplier_sites   is not null and I_supplier_sites.count   > 0 then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_ctrys,
                                table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups,
                                item_supp_country s
                          where value(input_ctrys) = s.origin_country_id
                            and value(input_sups)  = s.supplier
                            and s.item = gtt.varchar2_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date supplier/country - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;
   --
   if I_origin_countries is not null and I_origin_countries.count > 0 and
      (I_supplier_sites  is null     or  I_supplier_sites.count   = 0) then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_ctrys,
                                item_supp_country s
                          where value(input_ctrys) = s.origin_country_id
                            and s.item = gtt.varchar2_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date country - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_supplier_sites is not null and I_supplier_sites.count   > 0 and
      (I_origin_countries is  null or  I_origin_countries.count = 0) then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups,
                                item_supplier s
                          where value(input_sups) = s.supplier
                            and s.item = gtt.varchar2_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date supplier - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   merge into gtt_10_num_10_str_10_date target
   using (select im.item,
                 decode(imp.diff_1_aggregate_ind, 'Y', im.diff_1, null) agg_diff_1,
                 decode(imp.diff_2_aggregate_ind, 'Y', im.diff_2, null) agg_diff_2,
                 decode(imp.diff_3_aggregate_ind, 'Y', im.diff_3, null) agg_diff_3,
                 decode(imp.diff_4_aggregate_ind, 'Y', im.diff_4, null) agg_diff_4,
                 imp.item_desc item_parent_desc
            from gtt_10_num_10_str_10_date gtt,
                 v_item_master imp,
                 item_master im
           where gtt.varchar2_1   = im.item
             and im.alc_item_type = 'FASHIONSKU'
             and im.item_parent   = imp.item
   ) use_this
   on (target.varchar2_1 = use_this.item)
   when matched then update
    set target.varchar2_5 = agg_diff_1,
        target.varchar2_6 = agg_diff_2,
        target.varchar2_7 = agg_diff_3,
        target.varchar2_8 = agg_diff_4,
        target.varchar2_9 = item_parent_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge gtt_10_num_10_str_10_date agg diffs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if LOCK_STATS(O_error_message,  
                 I_session_id,     
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_REFRESH_ITEMS;
--------------------------------------------------------------------------------
FUNCTION SETUP_ORDERS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_depts             IN     OBJ_NUMERIC_ID_TABLE,
                      I_classes           IN     OBJ_NUMERIC_ID_TABLE,
                      I_subclasses        IN     OBJ_NUMERIC_ID_TABLE,
                      I_supplier_sites    IN     OBJ_NUMERIC_ID_TABLE,
                      I_brands            IN     OBJ_VARCHAR_ID_TABLE,
                      I_order_contexts    IN     OBJ_VARCHAR_ID_TABLE,
                      I_stores            IN     OBJ_NUMERIC_ID_TABLE,
                      I_origin_countries  IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_ORDERS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_10_NUM_10_STR_10_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --insert merch hierarchy (subclasses) into gtt_6_num_6_str_6_date
   --  number_1   --dept
   --  number_2   --class
   --  number_3   --subclass
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses) = OI_UTILITY.FAILURE then
     return OI_UTILITY.FAILURE;
   end if;

   --Note: both SETUP_ITEMS and SETUP_ORDERS insert to the same gtt table.
   --These two functions should NOT be called for the same report.

   --insert orders into gtt_10_num_10_str_10_date
   delete from gtt_10_num_10_str_10_date;

   --gtt_10_num_10_str_10_date
   --  number_1    -- order_no
   --  varchar2_1  -- supplier_site_name
   --  varchar2_2  -- currency_code
   --  varchar2_3  -- create_by
   --  varchar2_4  -- comment_desc
   --  date_1      -- not_before_date
   --  date_2      -- not_after_date
   --  date_3      -- otb_eow_date

   --Do NOT allow approving franchise orders and customer orders (when OMS_IND = 'Y') in the buyer dashboard.
   --Franchise orders are approved in the Franchise screen.
   --Customer orders are approved outside of RMS in an OMS_IND = 'Y' configuration.
   if I_brands is not null and I_brands.count> 0 then
      insert into gtt_10_num_10_str_10_date (number_1,
                                             varchar2_1,
                                             varchar2_2,
                                             varchar2_3,
                                             varchar2_4,
                                             date_1,
                                             date_2,
                                             date_3)
         select oh.order_no,
                s.sup_name,
                oh.currency_code,
                oh.create_id,
                oh.comment_desc,
                oh.not_before_date,
                oh.not_after_date,
                oh.otb_eow_date
           from ordhead oh,
                v_sups s,
                product_config_options p,
                rms_oi_system_options sp
          where ((    sp.b_po_pending_approval_level = 'W'
                  and oh.status in ('S', 'W'))
                 or
                 (    sp.b_po_pending_approval_level = 'S'
                  and oh.status = 'S'))
            and oh.supplier = s.supplier
            and oh.wf_order_no is NULL  --exclude franchise orders
            and (p.oms_ind = 'N' or (p.oms_ind = 'Y' and oh.order_type != 'CO'))  --exclude customer orders when OMS_IND = 'Y'
            and exists (select 'x'
                          from ordsku os,
                               gtt_6_num_6_str_6_date sbc,
                               v_item_master vim,
                               table(cast(I_brands as OBJ_VARCHAR_ID_TABLE)) input_brands
                         where oh.order_no = os.order_no
                           and os.item = vim.item
                           and vim.dept = sbc.number_1
                           and vim.class = sbc.number_2
                           and vim.subclass = sbc.number_3
                           and vim.brand_name = value(input_brands)
                           and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_10_num_10_str_10_date item filtered by brand - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   else
      insert into gtt_10_num_10_str_10_date (number_1,
                                             varchar2_1,
                                             varchar2_2,
                                             varchar2_3,
                                             varchar2_4,
                                             date_1,
                                             date_2,
                                             date_3)
         select oh.order_no,
                s.sup_name,
                oh.currency_code,
                oh.create_id,
                oh.comment_desc,
                oh.not_before_date,
                oh.not_after_date,
                oh.otb_eow_date
           from ordhead oh,
                v_sups s,
                product_config_options p,
                rms_oi_system_options sp
          where ((    sp.b_po_pending_approval_level = 'W'
                  and oh.status in ('S', 'W'))
                 or
                 (    sp.b_po_pending_approval_level = 'S'
                  and oh.status = 'S'))
            and oh.supplier = s.supplier
            and oh.wf_order_no is NULL  --exclude franchise orders
            and (p.oms_ind = 'N' or (p.oms_ind = 'Y' and oh.order_type != 'CO'))  --exclude customer orders when OMS_IND = 'Y'
            and exists (select 'x'
                          from ordsku os,
                               gtt_6_num_6_str_6_date sbc,
                               v_item_master vim
                         where oh.order_no = os.order_no
                           and os.item = vim.item
                           and vim.dept = sbc.number_1
                           and vim.class = sbc.number_2
                           and vim.subclass = sbc.number_3
                           and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_10_num_10_str_10_date item no brand filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_supplier_sites is not null and I_supplier_sites.count > 0 then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups,
                                ordhead oh
                          where value(input_sups) = oh.supplier
                            and oh.order_no = gtt.number_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date supplier - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_order_contexts is not null and I_order_contexts.count > 0 then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_order_contexts as OBJ_VARCHAR_ID_TABLE)) input_types,
                                ordhead oh
                          where value(input_types) = oh.po_type
                            and oh.order_no = gtt.number_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date order type - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_stores is not null and I_stores.count > 0 then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_stores as OBJ_NUMERIC_ID_TABLE)) input_stores,
                                ordloc ol
                          where value(input_stores) = ol.location
                            and ol.order_no = gtt.number_1
                            and rownum = 1)
         and not exists (select 'x'
                           from table(cast(I_stores as OBJ_NUMERIC_ID_TABLE)) input_stores,
                                ordloc ol,
                                alloc_header ah,
                                alloc_detail ad
                          where ah.alloc_no = ad.alloc_no
                            and ah.order_no = ol.order_no
                            and ah.wh = ol.location
                            and value(input_stores) = ad.to_loc
                            and ol.order_no = gtt.number_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date stores - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_origin_countries is not null and I_origin_countries.count > 0 then
      delete from gtt_10_num_10_str_10_date gtt
       where not exists (select 'X'
                           from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_types,
                                ordsku os
                          where value(input_types) = os.origin_country_id
                            and os.order_no = gtt.number_1
                            and rownum = 1);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete gtt_10_num_10_str_10_date order type - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if LOCK_STATS(O_error_message,  
                 I_session_id,     
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_ORDERS;
--------------------------------------------------------------------------------
FUNCTION SETUP_LOCATIONS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_chains          IN     OBJ_NUMERIC_ID_TABLE,
                         I_areas           IN     OBJ_NUMERIC_ID_TABLE,
                         I_locations       IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_LOCATIONS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_NUM_NUM_STR_STR_DATE_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');


   delete from gtt_num_num_str_str_date_date;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type

   if I_locations is not null and I_locations.count > 0 then

      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      select distinct v.store,
             v.store_name,
             'S'
        from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_locs,
             v_store v
       where value(input_locs) = v.store
     union all
      select distinct v.wh,
             v.wh_name,
             'W'
        from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_locs,
             v_wh v
       where value(input_locs) = v.wh
     union all
      select distinct v.finisher_id,
             v.finisher_desc,
             'I'
        from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_locs,
             v_internal_finisher v
       where value(input_locs) = v.finisher_id
     union all
      select distinct v.finisher_id,
             v.finisher_desc,
             'E'
        from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_locs,
             v_external_finisher v
       where value(input_locs) = v.finisher_id;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date stores - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   elsif I_areas is not null and I_areas.count > 0 then

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select vs.store,
                vs.store_name,
                'S'
           from table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas,
                v_store vs
          where value(input_areas) = vs.area;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date areas - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   elsif I_chains is not null and I_chains.count > 0 then

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select vs.store,
                vs.store_name,
                'S'
           from table(cast(I_chains as OBJ_NUMERIC_ID_TABLE)) input_chains,
                v_store vs
          where value(input_chains) = vs.chain;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date chains - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if LOCK_STATS(O_error_message,  
                 I_session_id,     
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_LOCATIONS;
--------------------------------------------------------------------------------
FUNCTION SETUP_STORES(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_location_group_type  IN OUT VARCHAR2,
                      I_session_id           IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_areas                IN     OBJ_NUMERIC_ID_TABLE,
                      I_stores               IN     OBJ_NUMERIC_ID_TABLE,
                      I_store_grade_group_id IN     STORE_GRADE_GROUP.STORE_GRADE_GROUP_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_STORES';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_NUM_NUM_STR_STR_DATE_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   delete from gtt_num_num_str_str_date_date;

   --gtt_num_num_str_str_date_date
   --  number_1 -- store
   --  varchar2_1 -- area or store_grade
   --  varchar2_2 -- area_name or store_grade

   if I_stores is not null and I_stores.count > 0 then

      O_location_group_type := 'S';

      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      select distinct v.store,
             v.store,
             v.store_name
        from table(cast(I_stores as OBJ_NUMERIC_ID_TABLE)) input_stores,
             v_store v
       where value(input_stores) = v.store;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date stores - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      if I_areas is not null and I_areas.count > 0 then

         delete from gtt_num_num_str_str_date_date 
          where number_1 not in(select v.store 
                                  from v_store v,
                                       table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas
                                 where v.area = value(input_areas));

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' delete gtt_num_num_str_str_date_date area - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      
      elsif I_store_grade_group_id is not null then

         delete from gtt_num_num_str_str_date_date
          where number_1 not in(select g.store
                                  from store_grade_store g
                                 where g.store_grade_group_id = I_store_grade_group_id);

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' delete gtt_num_num_str_str_date_date store_grade_group - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      end if;

   else
      if I_areas is not null and I_areas.count > 0 then

         O_location_group_type := 'A';

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select vs.store,
                vr.region,
                vr.region_name
           from table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas,
                v_store vs,
                v_area va,
                v_region vr
          where value(input_areas) = vs.area
            and vs.area            = va.area
            and va.area            = vr.area;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date areas - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      elsif I_store_grade_group_id is not null then

         O_location_group_type := 'G';

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select sgs.store,
                sgs.store_grade,
                sgs.store_grade
           from store_grade_store sgs,
                v_store vs
          where I_store_grade_group_id = sgs.store_grade_group_id
            and sgs.store              = vs.store;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date store_grade_group - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      end if;
   end if;

   if LOCK_STATS(O_error_message,  
                 I_session_id,     
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_STORES;
--------------------------------------------------------------------------------
PROCEDURE LOG_TIME(I_log_message    IN VARCHAR2,
                   I_start_time     IN TIMESTAMP)
IS
   L_end_time              TIMESTAMP;
   L_hours                 varchar2(50);
   L_mins                  varchar2(50);
   L_seconds               varchar2(50);
   L_total_milliseconds    varchar2(50);

BEGIN
   L_end_time := SYSTIMESTAMP;
   select extract( hour from diff ) hours,
          extract( minute from diff ) minutes,
          extract( second from diff ) seconds,
          --
          extract( day from diff )*24*60*60*1000 +
             extract( hour from diff )*60*60*1000 +
             extract( minute from diff )*60*1000 +
             round(extract( second from diff )*1000) total_milliseconds
     into L_hours,
          L_mins,
          L_seconds,
          L_total_milliseconds
     from (select L_end_time-I_start_time diff from dual);
   LOGGER.LOG_INFORMATION(I_log_message||'~TIMING~'||L_hours||'~'||L_mins||'~'||L_seconds||'~'||L_total_milliseconds);
END;
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_SESSION_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_session_id       OUT OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.GET_NEXT_SESSION_ID';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN
   SELECT OI_SESSION_ID_SEQ.NEXTVAL
     INTO O_session_id
     FROM dual;

   insert into OI_SESSION_ID_LOG (SESSION_ID,
                                    USER_ID,
                                  REQUESTED_DATE)
                          values (O_session_id,
                                  get_user,
                                  sysdate);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_NEXT_SESSION_ID;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ID_LOG(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.DELETE_SESSION_ID_LOG';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from OI_SESSION_ID_LOG
         where SESSION_ID = I_session_id;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_ID_LOG;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_program        IN      VARCHAR2)
IS

   L_error_message VARCHAR2(1000) := IO_error_message;
   L_error_type    VARCHAR2(5)   := NULL;

BEGIN

  /* Create initial error message to be parsed in SQL_LIB.
   * This error message may alread be created */
   if L_error_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            I_program,
                                            to_char(SQLCODE));
   end if;
   --
   /* Pass out parsed error message and error type */
   SQL_LIB.API_MSG(L_error_type,
                   L_error_message);
   ---
   IO_error_message := L_error_message;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'MBL_COMMON_SVC.HANDLE_ERRORS',
                                             to_char(SQLCODE));

      SQL_LIB.API_MSG(L_error_type,
                      L_error_message);
      IO_error_message := L_error_message;
END HANDLE_ERRORS;
--------------------------------------------------------------------------------
FUNCTION PURGE_RMS_OI_TABLES(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.PURGE_RMS_OI_TABLES';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_owner         VARCHAR2(30);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   select SYS.DBMS_ASSERT.schema_name(upper(table_owner)) table_owner
     into L_owner
    from system_options;
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_BUYER_EARLY_LATE_SHIP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_BUYER_ORDERS_TO_APPROVE';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_INV_ANA_OPEN_ORDER';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_INV_ANA_VARIANCE';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_INV_CTL_NEG_INV';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_INV_ORD_ERRORS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_INV_ORD_ITEM_ERRORS';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_MISSING_STOCK_COUNT';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_OVERDUE_SHIP_ALLOC';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_OVERDUE_SHIP_RTV';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_OVERDUE_SHIP_TSF';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_STK_ORD_PEND_CLOSE';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_STOCK_COUNT_VARIANCE';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_TSF_PEND_APPROVE';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_UNEXPECTED_INV';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.RMS_OI_DATA_STWRD_INCOMP_ITEMS';

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END PURGE_RMS_OI_TABLES;
--------------------------------------------------------------------------------
FUNCTION SETUP_ORG_UNIT_LOCATIONS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_set_of_books   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_org_units      IN     OBJ_NUMERIC_ID_TABLE,
                                  I_locations      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.SETUP_ORG_UNIT_LOCATIONS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_dummy_sob_tbl   OBJ_NUMERIC_ID_TABLE := NULL;
   L_dummy_org_unit_tbl   OBJ_NUMERIC_ID_TABLE := NULL;
   L_dummy_locations_tbl   OBJ_NUMERIC_ID_TABLE := NULL;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_NUM_NUM_STR_STR_DATE_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');

   if (I_set_of_books is null) or
      (I_set_of_books is not null and I_set_of_books.count = 0) then
      L_dummy_sob_tbl := OBJ_NUMERIC_ID_TABLE();
      L_dummy_sob_tbl.extend;
      L_dummy_sob_tbl(1) := -1;
   end if;
   
   if (I_org_units is null) or
      (I_org_units is not null and I_org_units.count = 0) then
      L_dummy_org_unit_tbl := OBJ_NUMERIC_ID_TABLE();
      L_dummy_org_unit_tbl.extend;
      L_dummy_org_unit_tbl(1) := -1;
   end if;
   
   if (I_locations is null) or
      (I_locations is not null and I_locations.count = 0) then
      L_dummy_locations_tbl := OBJ_NUMERIC_ID_TABLE();
      L_dummy_locations_tbl.extend;
      L_dummy_locations_tbl(1) := -1;
   end if;
   
   delete from gtt_num_num_str_str_date_date;
   
   if (I_org_units is not null and I_org_units.count > 0 ) or 
      (I_set_of_books is not null and I_set_of_books.count > 0) or
      (I_locations is not null and I_locations.count > 0) then
            insert into gtt_num_num_str_str_date_date(number_1,
                                                      varchar2_1,
                                                      varchar2_2)
            select distinct v.store,
                   v.store_name,
                   'S'
              from table(cast(nvl(L_dummy_sob_tbl, I_set_of_books) as OBJ_NUMERIC_ID_TABLE)) input_set_of_books,
                   table(cast(nvl(L_dummy_org_unit_tbl, I_org_units) as OBJ_NUMERIC_ID_TABLE)) input_org_units,
                   table(cast(nvl(L_dummy_locations_tbl, I_locations) as OBJ_NUMERIC_ID_TABLE)) input_locs,
                   v_store v, org_unit ou 
             where decode(value(input_org_units), -1  ,  ou.org_unit_id, value(input_org_units)) = ou.org_unit_id
               and decode(value(input_set_of_books), -1 , ou.set_of_books_id, value(input_set_of_books)) = ou.set_of_books_id
               and decode(value(input_locs), -1, v.store, value(input_locs)) = v.store 
               and v.org_unit_id = ou.org_unit_id
             union all
            select distinct v.wh,
                   v.wh_name,
                   'W'
              from table(cast(nvl(L_dummy_sob_tbl, I_set_of_books) as OBJ_NUMERIC_ID_TABLE)) input_set_of_books,
                   table(cast(nvl(L_dummy_org_unit_tbl, I_org_units) as OBJ_NUMERIC_ID_TABLE)) input_org_units,
                   table(cast(nvl(L_dummy_locations_tbl, I_locations) as OBJ_NUMERIC_ID_TABLE)) input_locs,
                   v_wh v, org_unit ou 
             where decode(value(input_org_units), -1  ,  ou.org_unit_id, value(input_org_units)) = ou.org_unit_id
               and decode(value(input_set_of_books), -1 , ou.set_of_books_id, value(input_set_of_books)) = ou.set_of_books_id
                and v.org_unit_id = ou.org_unit_id
                and decode(value(input_locs), -1, v.wh, value(input_locs)) = v.wh
              union all
            select distinct v.finisher_id,
                   v.finisher_desc,
                   'E'
              from table(cast(nvl(L_dummy_sob_tbl, I_set_of_books) as OBJ_NUMERIC_ID_TABLE)) input_set_of_books,
                   table(cast(nvl(L_dummy_org_unit_tbl, I_org_units) as OBJ_NUMERIC_ID_TABLE)) input_org_units,
                   table(cast(nvl(L_dummy_locations_tbl, I_locations) as OBJ_NUMERIC_ID_TABLE)) input_locs,
                   v_external_finisher v, org_unit ou 
             where decode(value(input_org_units), -1  ,  ou.org_unit_id, value(input_org_units)) = ou.org_unit_id
               and decode(value(input_set_of_books), -1 , ou.set_of_books_id, value(input_set_of_books)) = ou.set_of_books_id
               and v.org_unit_id = ou.org_unit_id
               and decode(value(input_locs), -1, v.finisher_id, value(input_locs)) = v.finisher_id
             union all
            select distinct v.finisher_id,
                   v.finisher_desc,
                   'I'
              from table(cast(nvl(L_dummy_sob_tbl, I_set_of_books) as OBJ_NUMERIC_ID_TABLE)) input_set_of_books,
                   table(cast(nvl(L_dummy_org_unit_tbl, I_org_units) as OBJ_NUMERIC_ID_TABLE)) input_org_units,
                   table(cast(nvl(L_dummy_locations_tbl, I_locations) as OBJ_NUMERIC_ID_TABLE)) input_locs,
                   v_internal_finisher v, org_unit ou 
             where decode(value(input_org_units), -1  ,  ou.org_unit_id, value(input_org_units)) = ou.org_unit_id
               and decode(value(input_set_of_books), -1 , ou.set_of_books_id, value(input_set_of_books)) = ou.set_of_books_id
               and v.org_unit_id = ou.org_unit_id
               and decode(value(input_locs), -1, v.finisher_id, value(input_locs)) = v.finisher_id;

            LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                    ' insert gtt_num_num_str_str_date_date locations - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else  -- no hierarchy filter condition is defined
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            NULL,
                                            L_program,
                                            NULL);
      return OI_UTILITY.FAILURE;
   end if;

   if LOCK_STATS(O_error_message,  
                 I_session_id,     
                 L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_ORG_UNIT_LOCATIONS;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION LOCK_STATS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                    I_tabname         IN     USER_TAB_STATISTICS.TABLE_NAME%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'OI_UTILITY.LOCK_STATS';
   L_owner       dba_tab_statistics.owner%TYPE;
   L_stats_pref  VARCHAR2(61);
   L_locked      VARCHAR2(61);


BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start table:'||I_tabname|| ' Session id:'||I_session_id);

   select owner,
          gtt_stats_preference,
          locked
     into L_owner,
          L_stats_pref,
          L_locked
     from (select dts.owner, 
                  dbms_stats.get_prefs('GLOBAL_TEMP_TABLE_STATS',dts.owner,dts.table_name) as gtt_stats_preference,
                  nvl(dts.stattype_locked, 'NONE') as locked,
                  row_number() over (order by scope) as scope_order
             from dba_tab_statistics dts,
                  system_config_options s
            where dts.table_name = I_tabname
              and dts.owner = s.table_owner)
    where scope_order = 1;

   if L_stats_pref = 'SESSION' and L_locked = 'NONE' then
 
      -- Gather session statistics for the global temporary tables
      DBMS_STATS.GATHER_TABLE_STATS(ownname => L_owner,
                                    tabname => I_tabname);

      LOGGER.LOG_INFORMATION(L_program||' locked stats on: '||I_tabname);

   end if;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END LOCK_STATS;
--------------------------------------------------------------------------------
END OI_UTILITY;
/
