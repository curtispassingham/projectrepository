CREATE OR REPLACE PACKAGE BODY RMS_OI_FIN_ANALYST AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION WAC_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_itemloc_count          IN OUT NUMBER,
                      O_neg_wac_count          IN OUT NUMBER,
                      O_report_level           IN OUT VARCHAR2,
                      I_session_id             IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_set_of_books_ids       IN     OBJ_NUMERIC_ID_TABLE,
                      I_org_unit_ids           IN     OBJ_NUMERIC_ID_TABLE,
                      I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                      I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                      I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                      I_subclasses             IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program           VARCHAR2(60) := 'RMS_OI_FIN_ANALYST.WAC_VARIANCE';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;
   L_maximum_pct_found NUMBER       := 0;
   L_tolerance_pct RMS_OI_SYSTEM_OPTIONS.FA_WAC_VAR_TOLERANCE_PCT%TYPE;
   L_itemloc_cnt   RMS_OI_SYSTEM_OPTIONS.FA_WAC_VAR_ITEMLOC_CNT%TYPE;
   L_maximum_pct   RMS_OI_SYSTEM_OPTIONS.FA_WAC_VAR_MAXIMUM_PCT%TYPE;

   cursor c_oi_system_option is
     select  fa_wac_var_tolerance_pct
            ,fa_wac_var_itemloc_cnt
            ,fa_wac_var_maximum_pct
       from rms_oi_system_options;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_oi_system_option;
   fetch c_oi_system_option into L_tolerance_pct,
                                 L_itemloc_cnt,
                                 L_maximum_pct;
   close c_oi_system_option;
   --
   --insert merch hierarchy into gtt_6_num_6_str_6_date
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  FALSE) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   --insert merch hierarchy into gtt_num_num_str_str_date_date
   if OI_UTILITY.SETUP_ORG_UNIT_LOCATIONS(O_error_message,
                                          I_session_id,
                                          I_set_of_books_ids,
                                          I_org_unit_ids,
                                          I_locations) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   delete from rms_oi_wac_variance where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_wac_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   ---
   insert into rms_oi_wac_variance(session_id,
                                   item,
                                   item_desc,
                                   loc,
                                   loc_name,
                                   loc_type,
                                   unit_elc,
                                   avg_cost,
                                   variance_pct,
                                   inventory,
                                   currency_code,
                                   item_image,
                                   supplier,
                                   supplier_name,
                                   dept,
                                   class,
                                   subclass,
                                   subclass_name,
                                   unit_retail,
                                   wh,
                                   unit_elc_primary,
                                   average_cost_primary,
                                   inventory_primary,
                                   num_locs_wac_issue_item,
                                   num_items_wac_issue_loc)
                                   with currency_rates as (select from_currency,
                                               to_currency,
                                               effective_date,
                                               exchange_rate,
                                               exchange_type,
                                               currency_code
                                          from (select mv.from_currency,
                                                       mv.to_currency,
                                                       mv.effective_date,
                                                       mv.exchange_rate,
                                                       mv.exchange_type,
                                                       so.currency_code,
                                                       rank() over (partition by mv.from_currency,
                                                                                 mv.to_currency
                                                                        order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                                 mv.effective_date desc) date_rank
                                                  from mv_currency_conversion_rates mv,
                                                       system_config_options so
                                                 where effective_date <= get_vdate
                                                   and ((so.consolidation_ind = 'Y' and mv.exchange_type in('P', 'C', 'O'))
                                                        or
                                                        (so.consolidation_ind = 'N' and mv.exchange_type in('P', 'O'))))
                                         where date_rank = 1) 
                            select I_session_id,
                                   sub_table.item,
                                   vim.item_desc,
                                   sub_table.loc,
                                   sub_table.loc_name,
                                   sub_table.loc_type,
                                   sub_table.unit_elc * decode(sub_table.loc_type,'E',1,mvc2.exchange_rate),
                                   sub_table.av_cost,
                                   CASE WHEN (sub_table.av_cost - (sub_table.unit_elc * DECODE(sub_table.loc_type,'E',1,mvc2.exchange_rate))) = 0
                                             AND sub_table.av_cost = 0
                                        THEN 0
                                        WHEN sub_table.av_cost = 0
                                             AND ((sub_table.av_cost) - (sub_table.unit_elc * DECODE(sub_table.loc_type,'E',1,mvc2.exchange_rate))) <> 0
                                        THEN 9999999999.9999
                                   ELSE (((sub_table.av_cost) - (sub_table.unit_elc * DECODE(sub_table.loc_type,'E',1,mvc2.exchange_rate))) /(sub_table.av_cost)) * 100
                                   END variance_pct, --variance_pct --(av_cost - unit ELC)/av_cost * 100 in same currency
                                   sub_table.inventory_value,
                                   NVL(loc.currency_code, sub_table.currency_code),
                                   CASE WHEN ige.image_name is not NULL THEN
                                      ige.image_addr||'/'||ige.image_name
                                   ELSE
                                      NULL
                                   END  item_image,
                                   sub_table.supplier,
                                   vsp.sup_name,
                                   sub_table.dept,
                                   sub_table.class,
                                   sub_table.subclass,
                                   sub_table.subclase_name,
                                   il.unit_retail * mvc1.exchange_rate,                      --Unit retail in Primary Currency
                                   NVL(il.source_wh, loc.default_wh),
                                   sub_table.unit_elc * decode(sub_table.loc_type,'E',mvc1.exchange_rate,mvc.exchange_rate),                  --Unit ELC in primary currency
                                   sub_table.av_cost * mvc1.exchange_rate,                   --Average Cost in primary currency
                                   sub_table.inventory_value * mvc1.exchange_rate,           --Inventory in primary currency
                                   count(sub_table.loc)  over (partition by sub_table.item),--Number of loc with WAC variance issue for this item
                                   count(sub_table.item) over (partition by sub_table.loc)  --Number of items with WAC variance issue for this loc
                              from (select ils.item,
                                           ils.loc,
                                           fc_input.loc_name,
                                           ils.loc_type,
                                           NVL(fc_input.pricing_cost, ils.unit_cost) unit_elc,
                                           ils.av_cost,
                                           case when (ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran) < 0 and ils.av_cost < 0 then
                                                     ((ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran) * ils.av_cost) * -1 
                                                else (ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran) * ils.av_cost
                                           end inventory_value, --when both av_cost and qty are -ve,it should not show positive inventory value.
                                           fc_input.currency_code,
                                           NVL(fc_input.supplier, ils.primary_supp) supplier,
                                           fc_input.dept,
                                           fc_input.class,
                                           fc_input.subclass,
                                           fc_input.subclase_name
                                      from item_loc_soh ils,
                                           (select fc.item,
                                                   fc.supplier,
                                                   fc.location,
                                                   loc.varchar2_1 loc_name,
                                                   fc.currency_code,
                                                   fc.pricing_cost,
                                                   fc.dept,
                                                   fc.class,
                                                   fc.subclass,
                                                   dcs.varchar2_3 subclase_name,
                                                   rank() over(partition by fc.item,
                                                                            fc.supplier,
                                                                            fc.origin_country_id,
                                                                            fc.location
                                                   order by fc.active_date desc) latest_rec
                                             from  future_cost fc,
                                                   gtt_6_num_6_str_6_date dcs,
                                                   gtt_num_num_str_str_date_date loc
                                             where fc.primary_supp_country_ind = 'Y'
                                               and fc.active_date <= get_vdate
                                               and fc.dept        = dcs.number_1
                                               and fc.class       = dcs.number_2
                                               and fc.subclass    = dcs.number_3
                                               and fc.location     = loc.number_1
                                               and loc.varchar2_2 != 'E'
                                            union all
                                            select im.item,
                                                   null supplier,
                                                   loc.number_1,
                                                   loc.varchar2_1 loc_name,
                                                   prt.currency_code,
                                                   null pricing_cost,
                                                   dcs.number_1,
                                                   dcs.number_2,
                                                   dcs.number_3,
                                                   dcs.varchar2_3 subclase_name,
                                                   1 latest_rec
                                             from  item_master im,
                                                   gtt_6_num_6_str_6_date dcs,
                                                   gtt_num_num_str_str_date_date loc,
                                                   partner prt
                                             where prt.partner_type = 'E'
                                               and loc.varchar2_2   = 'E'
                                               and prt.partner_id   = loc.number_1
                                               and im.dept          = dcs.number_1
                                               and im.class         = dcs.number_2
                                               and im.subclass      = dcs.number_3) fc_input
                                     where ils.item            = fc_input.item
                                       and ils.loc             = fc_input.location
                                       and fc_input.latest_rec = 1) sub_table,
                                       v_item_master_tl vim,
                                       item_image       ige,
                                       v_sups_tl        vsp,
                                       item_loc         il,
                                       (select store loc,
                                               default_wh,
                                               currency_code,
                                               'S' loc_type
                                          from store
                                        union all
                                        select wh loc,
                                               default_wh,
                                               currency_code,
                                               'W' loc_type
                                         from wh) loc,
                                        currency_rates mvc,
                                        currency_rates mvc1,
                                        currency_rates mvc2
                             where (sub_table.av_cost      < 0 or
                                    ABS(CASE WHEN (sub_table.av_cost - (sub_table.unit_elc * DECODE(sub_table.loc_type,'E',1,mvc2.exchange_rate))) = 0
                                             AND sub_table.av_cost = 0
                                        THEN 0
                                        WHEN sub_table.av_cost = 0
                                             AND ((sub_table.av_cost) - (sub_table.unit_elc * DECODE(sub_table.loc_type,'E',1,mvc2.exchange_rate))) <> 0
                                        THEN 9999999999.9999
                                        ELSE (((sub_table.av_cost) - (sub_table.unit_elc * DECODE(sub_table.loc_type,'E',1,mvc2.exchange_rate))) /(sub_table.av_cost)) * 100
                                        END) >= L_tolerance_pct)
                               --
                               and sub_table.item = vim.item
                               --
                               and sub_table.item     = ige.item(+)
                               and ige.primary_ind(+) = 'Y'
                               --
                               and sub_table.supplier = vsp.supplier
                               --
                               and sub_table.item = il.item
                               and sub_table.loc  = il.loc
                               --
                               and sub_table.loc       = loc.loc(+)
                               and sub_table.loc_type  = loc.loc_type(+)
                               --
                               and mvc.to_currency         = mvc.currency_code
                               and mvc1.to_currency        = mvc1.currency_code
                               and mvc2.to_currency        = NVL(loc.currency_code, sub_table.currency_code)
                               --
                               and sub_table.currency_code = mvc.from_currency
                               and mvc1.from_currency       = NVL(loc.currency_code, sub_table.currency_code)
                               and sub_table.currency_code = mvc2.from_currency
                         order by sub_table.inventory_value;
   --
   merge into rms_oi_wac_variance target
   using (select wv.session_id,
                 wv.item,
                 wv.loc,
                 wv.wh,
                 vwh.wh_name,
                 ils.av_cost
            from rms_oi_wac_variance wv,
                 v_wh_tl vwh,
                 item_loc_soh ils
           where wv.session_id = I_session_id
             and wv.wh         = vwh.wh
             and wv.wh         = ils.loc
             and wv.item       = ils.item) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc
       and target.wh         = use_this.wh)
   when matched then update
    set target.wh_name         = use_this.wh_name,
        target.wh_average_cost = use_this.av_cost;
   ---
   merge into rms_oi_wac_variance target
   using ( with currency_rates as (select from_currency,
                                               to_currency,
                                               effective_date,
                                               exchange_rate,
                                               exchange_type,
                                               currency_code
                                          from (select mv.from_currency,
                                                       mv.to_currency,
                                                       mv.effective_date,
                                                       mv.exchange_rate,
                                                       mv.exchange_type,
                                                       so.currency_code,
                                                       rank() over (partition by mv.from_currency,
                                                                                 mv.to_currency
                                                                        order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                                 mv.effective_date desc) date_rank
                                                  from mv_currency_conversion_rates mv,
                                                       system_config_options so
                                                 where effective_date <= get_vdate
                                                   and ((so.consolidation_ind = 'Y' and mv.exchange_type in('P', 'C', 'O'))
                                                        or
                                                        (so.consolidation_ind = 'N' and mv.exchange_type in('P', 'O'))))
                                         where date_rank = 1)
      select wv.item,
             wv.loc,
             sum(ils.av_cost * mvc1.exchange_rate)/count(st_locs.store) chain_averge
        from rms_oi_wac_variance wv,
             v_store st_chain,
             v_store st_locs,
             item_loc_soh ils,
             currency_rates mvc1
       where wv.session_id      = I_session_id
         and st_chain.store     = wv.loc
         and st_chain.chain     = st_locs.chain
         and ils.item           = wv.item
         and ils.loc            = st_locs.store
         and mvc1.to_currency   = mvc1.currency_code
         and mvc1.from_currency = st_locs.currency_code
       group by wv.item,
                wv.loc) use_this
   on (    target.session_id = I_session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.chain_averge = use_this.chain_averge;
   ---
   
   select count(variance_pct)
          into O_itemloc_count
     from rms_oi_wac_variance
    where session_id = I_session_id;
   --
   select count(avg_cost)
          into O_neg_wac_count
     from rms_oi_wac_variance
    where avg_cost <0
      and session_id = I_session_id;
   --
   select count(variance_pct)
          into L_maximum_pct_found
     from rms_oi_wac_variance
    where variance_pct >= L_maximum_pct
      and session_id = I_session_id
      and rownum = 1;
   --
   if O_neg_wac_count      > 0 or
      (O_itemloc_count     >= L_itemloc_cnt and 
       L_maximum_pct_found > 0) then
      O_report_level := RED;
   elsif O_itemloc_count     >= L_itemloc_cnt or
         L_maximum_pct_found > 0 then
      O_report_level := YELLOW;
   else
      O_report_level := GREEN;
   end if;
   ---
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' End');
   RETURN OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if c_oi_system_option%ISOPEN then close c_oi_system_option; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if c_oi_system_option%ISOPEN then close c_oi_system_option; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END WAC_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION GET_CHAIN_AVERAGE_COST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_chain_average_cost    OUT NUMBER,
                                I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                I_item               IN     ITEM_LOC_SOH.ITEM%TYPE,
                                I_location           IN     ITEM_LOC_SOH.LOC%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(60) := 'RMS_OI_FIN_ANALYST.GET_CHAIN_AVERAGE_COST';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_exist       BOOLEAN;
   
   cursor c_get_av_cost is
      select sum(ils.av_cost)/count(st_locs.store)
        from v_store st_chain,
             v_store st_locs,
             item_loc_soh ils
       where st_chain.store = I_location
         and st_chain.chain = st_locs.chain
         and ils.item       = I_item
         and ils.loc        = st_locs.store;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---   
   open c_get_av_cost;
   fetch c_get_av_cost into O_chain_average_cost;
   close c_get_av_cost;
   ---
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' End');
   RETURN OI_UTILITY.SUCCESS;
   
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if c_get_av_cost%ISOPEN then close c_get_av_cost; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if c_get_av_cost%ISOPEN then close c_get_av_cost; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END GET_CHAIN_AVERAGE_COST;
--------------------------------------------------------------------------------
FUNCTION CUMULATIVE_MARKON_PCT_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_cum_markon_count       IN OUT NUMBER,
                                        O_neg_cum_markon_count   IN OUT NUMBER,
                                        O_report_level           IN OUT VARCHAR2,
                                        I_session_id             IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                        I_set_of_books_ids       IN     OBJ_NUMERIC_ID_TABLE,
                                        I_org_unit_ids           IN     OBJ_NUMERIC_ID_TABLE,
                                        I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                                        I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                        I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                        I_subclasses             IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(60) := 'RMS_OI_FIN_ANALYST.CUMULATIVE_MARKON_PCT_VARIANCE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                         DATE := GET_VDATE;
   L_cum_markon_min_var_pct        rms_oi_system_options.fa_cum_markon_min_var_pct%TYPE;
   L_cum_markon_var_critical_cnt   rms_oi_system_options.fa_cum_markon_var_critical_cnt%TYPE;
   L_max_cum_markon_pct            financial_unit_options.max_cum_markon_pct%TYPE;
   L_min_cum_markon_pct            financial_unit_options.min_cum_markon_pct%TYPE;
   L_primary_currency              system_options.currency_code%TYPE;
   L_consolidation_ind             system_options.consolidation_ind%TYPE;
   L_next_eom_date                 system_variables.next_eom_date%TYPE;
   L_last_eom_date                 system_variables.last_eom_date%TYPE;
   L_max_cnt                       NUMBER := 0;
   L_min_cnt                       NUMBER := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.fa_cum_markon_min_var_pct,
          oi.fa_cum_markon_var_critical_cnt,
          fo.max_cum_markon_pct,
          fo.min_cum_markon_pct,
          so.currency_code,
          so.consolidation_ind,
          sv.next_eom_date,
          sv.last_eom_date
     into L_cum_markon_min_var_pct,
          L_cum_markon_var_critical_cnt,
          L_max_cum_markon_pct,
          L_min_cum_markon_pct,
          L_primary_currency,
          L_consolidation_ind,
          L_next_eom_date,
          L_last_eom_date
     from rms_oi_system_options oi,
          financial_unit_options fo,
          system_options so,
          system_variables sv;

   -- gtt_6_num_6_str_6_date
   --   number_1   dept
   --   number_2   class
   --   number_3   subclass
   --   varchar2_1 dept name
   --   varchar2_2 class name
   --   varchar2_3 subclass name
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  FALSE) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   -- gtt_num_num_str_str_date_date
   --   number_1   location
   --   varchar2_1 loc name
   --   varchar2_2 loc type
   if OI_UTILITY.SETUP_ORG_UNIT_LOCATIONS(O_error_message,
                                          I_session_id,
                                          I_set_of_books_ids,
                                          I_org_unit_ids,
                                          I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete from gtt_10_num_10_str_10_date;

   -- gtt_10_num_10_str_10_date
   --   number_1   location
   --   varchar2_1 loc name
   --   varchar2_2 loc type
   --   date_1     eom_date
   insert into gtt_10_num_10_str_10_date(number_1,
                                         varchar2_1,
                                         varchar2_2,
                                         varchar2_3,
                                         date_1)
   with dt as (select i.eom_date
                 from (select distinct md.eom_date,
                                       dense_rank() over(order by md.eom_date desc) rnk
                         from month_data md,
                              (select distinct number_1, number_2, number_3 from gtt_6_num_6_str_6_date) mh,
                              (select distinct number_1 from gtt_num_num_str_str_date_date) loc
                        where md.dept     = mh.number_1
                          and md.class    = mh.number_2
                          and md.subclass = mh.number_3
                          and md.location = loc.number_1
                          and md.eom_date < L_next_eom_date) i
                where i.rnk < 4)
   select oi.number_1,
          oi.varchar2_1,
          oi.varchar2_2,
          s.currency_code,
          dt.eom_date
     from gtt_num_num_str_str_date_date oi,
          store s,
          dt
    where oi.number_1   = s.store
      and oi.varchar2_2 = 'S'
   union all
   select oi.number_1,
          oi.varchar2_1,
          oi.varchar2_2,
          w.currency_code,
          dt.eom_date
     from gtt_num_num_str_str_date_date oi,
          wh w,
          dt
    where oi.number_1   = w.wh
      and oi.varchar2_2 in('W','I')
   union all
   select oi.number_1,
          oi.varchar2_1,
          oi.varchar2_2,
          p.currency_code,
          dt.eom_date
     from gtt_num_num_str_str_date_date oi,
          partner p,
          dt
    where oi.number_1    = p.partner_id
      and p.partner_type = 'E'
      and oi.varchar2_2  = 'E';

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert dates gtt_10_num_10_str_10_date - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_cum_markon_pct_variance where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_cum_markon_pct_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_cum_markon_pct_variance (session_id,
                                               dept,
                                               dept_name,
                                               class,
                                               class_name,
                                               subclass,
                                               subclass_name,
                                               loc,
                                               loc_type,
                                               loc_name,
                                               calculated_markon_pct,
                                               posted_markon_pct,
                                               budgeted_markon_pct,
                                               variance_markon_pct,
                                               htd_gafs_retail,
                                               htd_gafs_cost,
                                               cls_stk_retail,
                                               cls_stk_cost,
                                               htd_gafs_retail_prim_cur,
                                               htd_gafs_cost_prim_cur,
                                               cls_stk_retail_prim_cur,
                                               cls_stk_cost_prim_cur,
                                               eom_date,
                                               currency_code,
                                               subclass_eom_date_loc_cnt,
                                               loc_eom_date_subclass_cnt)
   with curr_conv as (select from_currency,
                             to_currency,
                             effective_date,
                             exchange_rate,
                             exchange_type
                        from (select from_currency,
                                     to_currency,
                                     effective_date,
                                     exchange_rate,
                                     exchange_type,
                                     rank() over (partition by from_currency,
                                                               to_currency
                                                      order by decode(exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                               effective_date desc) date_rank
                                from mv_currency_conversion_rates
                               where effective_date <= L_vdate
                                 and ((L_consolidation_ind = 'Y' and exchange_type in('P', 'C', 'O'))
                                      or
                                      (L_consolidation_ind = 'N' and exchange_type in('P', 'O')))
                             )
                       where date_rank = 1)
   select I_session_id,
          md.dept,
          mh.dept_name,
          md.class,
          mh.class_name,
          md.subclass,
          mh.subclass_name,
          loc.loc,
          loc.loc_type,
          loc.loc_name,
          ((md.htd_gafs_retail-md.htd_gafs_cost)/decode(nvl(md.htd_gafs_retail,1), 0, 1, nvl(md.htd_gafs_retail,1)))*100 calculated_markon_pct,
          md.cum_markon_pct posted_markon_pct,
          d.bud_int budgeted_markon_pct,
          (((((md.htd_gafs_retail-md.htd_gafs_cost)/decode(nvl(md.htd_gafs_retail,1), 0, 1, nvl(md.htd_gafs_retail,1)))*100)-d.bud_int)/decode(nvl(d.bud_int,1), 0, 1, nvl(d.bud_int,1)))*100 variance_markon_pct,
          md.htd_gafs_retail,
          md.htd_gafs_cost,
          md.cls_stk_retail,
          md.cls_stk_cost,
          md.htd_gafs_retail * curr_conv.exchange_rate,
          md.htd_gafs_cost * curr_conv.exchange_rate,
          md.cls_stk_retail * curr_conv.exchange_rate,
          md.cls_stk_cost * curr_conv.exchange_rate,
          md.eom_date,
          loc.currency_code,
          count(*) over (partition by md.dept, md.class, md.subclass, md.eom_date) eom_date_loc_cnt,
          count(*) over (partition by md.location, md.loc_type, md.eom_date) eom_date_sbc_cnt
     from (select number_1   dept,
                  number_2   class,
                  number_3   subclass,
                  varchar2_1 dept_name,
                  varchar2_2 class_name,
                  varchar2_3 subclass_name
             from gtt_6_num_6_str_6_date) mh,
          (select number_1   loc,
                  varchar2_1 loc_name,
                  varchar2_2 loc_type,
                  varchar2_3 currency_code,
                  date_1     eom_date
             from gtt_10_num_10_str_10_date) loc,
          month_data md,
          deps d,
          rms_oi_dept_options rdo,
          curr_conv
    where md.dept                 = mh.dept
      and md.class                = mh.class
      and md.subclass             = mh.subclass
      and md.location             = loc.loc
      and md.loc_type             = loc.loc_type
      and md.eom_date             = loc.eom_date
      and md.dept                 = d.dept
      and md.dept                 = rdo.dept(+)
      --
      and md.htd_gafs_retail     >= 0
      and md.htd_gafs_cost       >= 0
      and md.currency_ind         = 'L'
      --
      and curr_conv.from_currency = loc.currency_code
      and curr_conv.to_currency   = L_primary_currency
      --
      and ( (((md.htd_gafs_retail-md.htd_gafs_cost)/decode(nvl(md.htd_gafs_retail,1), 0, 1, nvl(md.htd_gafs_retail,1)))*100) < 0
            or
            ABS(((((((md.htd_gafs_retail - md.htd_gafs_cost)/decode(nvl(md.htd_gafs_retail,1), 0, 1, nvl(md.htd_gafs_retail,1)))*100) - d.bud_int) / decode(nvl(d.bud_int,1), 0 ,1, nvl(d.bud_int,1))) * 100) - d.bud_int) >
               nvl(rdo.fa_cum_markon_min_var_pct, L_cum_markon_min_var_pct)
          );

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_cum_markon_pct_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select sum(decode(sign(cm.calculated_markon_pct), -1, 1, 1)),
          sum(decode(sign(cm.calculated_markon_pct), -1, 1, 0)),
          sum(decode(sign(L_max_cum_markon_pct-cm.calculated_markon_pct), -1, 1, 0)),
          sum(decode(sign(L_min_cum_markon_pct-cm.calculated_markon_pct), -1, 0, 1))
     into O_cum_markon_count,
          O_neg_cum_markon_count,
          L_max_cnt,
          L_min_cnt
     from rms_oi_cum_markon_pct_variance cm
    where cm.session_id = I_session_id
      and cm.eom_date   = L_last_eom_date;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' fetch rms_oi_cum_markon_pct_variance outputs - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if O_neg_cum_markon_count > 0 then
      O_report_level := RMS_OI_FIN_ANALYST.RED;
   elsif L_max_cnt > 0 and O_cum_markon_count >= L_cum_markon_var_critical_cnt then
      O_report_level := RMS_OI_FIN_ANALYST.RED;
   elsif L_min_cnt > 0 and O_cum_markon_count >= L_cum_markon_var_critical_cnt then
      O_report_level := RMS_OI_FIN_ANALYST.RED;
   elsif L_max_cnt > 0 then
      O_report_level := RMS_OI_FIN_ANALYST.YELLOW;
   elsif L_min_cnt > 0 then
      O_report_level := RMS_OI_FIN_ANALYST.YELLOW;
   else
      O_report_level := RMS_OI_FIN_ANALYST.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END CUMULATIVE_MARKON_PCT_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_VALUE_VARIANCE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_stk_cnt_value_var_count IN OUT NUMBER,
                                    O_report_level            IN OUT VARCHAR2,
                                    I_session_id              IN OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                    I_set_of_books_ids        IN OBJ_NUMERIC_ID_TABLE,
                                    I_org_unit_ids            IN OBJ_NUMERIC_ID_TABLE,
                                    I_locations               IN OBJ_NUMERIC_ID_TABLE,
                                    I_depts                   IN OBJ_NUMERIC_ID_TABLE,
                                    I_classes                 IN OBJ_NUMERIC_ID_TABLE,
                                    I_subclasses              IN OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program    VARCHAR2(60) := 'RMS_OI_FIN_ANALYST.STOCK_COUNT_VALUE_VARIANCE';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;
   L_vdate      DATE         := get_vdate;
   L_fa_stk_count_var_pct       RMS_OI_SYSTEM_OPTIONS.FA_STK_CNT_VALUE_TOLERENCE_PCT%TYPE;
   L_fa_stk_count_var_loc_cnt   RMS_OI_SYSTEM_OPTIONS.FA_STK_CNT_VALUE_VAR_CRIT_CNT%TYPE;
   L_seven_day_count NUMBER(10) := 0;
BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   SELECT oi.fa_stk_cnt_value_tolerence_pct,
          oi.fa_stk_cnt_value_var_crit_cnt
     INTO L_fa_stk_count_var_pct,
          L_fa_stk_count_var_loc_cnt
     FROM rms_oi_system_options oi;

   --insert merch hierarchy into gtt_6_num_6_str_6_date
   -- gtt_6_num_6_str_6_date
   --   number_1   dept
   --   number_2   class
   --   number_3   subclass
   --   varchar2_1 dept name
   --   varchar2_2 class name
   --   varchar2_3 subclass name     
   IF OI_UTILITY.SETUP_MERCH_HIER(O_error_message, 
                                  I_session_id, 
                                  I_depts, 
                                  I_classes, 
                                  I_subclasses,
                                  FALSE) = 0 THEN
      RETURN OI_UTILITY.FAILURE;
   END IF;
    -- gtt_num_num_str_str_date_date
    --   number_1   location
    --   varchar2_1 loc name
    --   varchar2_2 loc type
    IF OI_UTILITY.SETUP_ORG_UNIT_LOCATIONS(O_error_message, 
                                           I_session_id, 
                                           I_set_of_books_ids, 
                                           I_org_unit_ids, 
                                           I_locations) = 0 THEN
       RETURN OI_UTILITY.FAILURE;
    END IF;
  
   DELETE FROM rms_oi_stk_cnt_value_variance WHERE session_id = I_session_id;
    
   insert into rms_oi_stk_cnt_value_variance (session_id,
                                              cycle_count,
                                              cycle_count_desc,
                                              stocktake_type,
                                              stocktake_date,
                                              dept,
                                              dept_name,
                                              class,
                                              class_name,
                                              subclass,
                                              subclass_name,
                                              loc,
                                              loc_type,
                                              loc_name,
                                              total_unit_variance_pct,
                                              over_unit_variance_pct,
                                              short_unit_variance_pct,
                                              total_variance_pct,
                                              snapshot_value,
                                              actual_value)
   /* Cursor to fetch Stock Counts */
   with cycles as (select sh.cycle_count,
                          sh.cycle_count_desc,
                          sh.stocktake_type,
                          sh.stocktake_date,
                          spl.dept,
                          spl.class,
                          spl.subclass,
                          d.dept_name,
                          d.class_name,
                          d.subclass_name,
                          spl.location,
                          spl.loc_type,
                          loc_gtt.varchar2_1 loc_name,
                          SUM(ssl.snapshot_on_hand_qty) tot_snapshot_on_hand_qty,
                          SUM(CASE WHEN ssl.physical_count_qty > ssl.snapshot_on_hand_qty
                                      THEN (ssl.physical_count_qty - ssl.snapshot_on_hand_qty)
                                   ELSE 0 END) over_sum,
                          SUM(CASE WHEN ssl.physical_count_qty < ssl.snapshot_on_hand_qty
                                       THEN (ssl.physical_count_qty - ssl.snapshot_on_hand_qty)
                                    ELSE 0 END) short_sum,
                          SUM(CASE d.profit_calc_type 
                               WHEN 1 THEN ssl.physical_count_qty * ssl. snapshot_unit_cost   
                               WHEN 2 THEN ssl.physical_count_qty * ssl.snapshot_unit_retail                                 
                          END) AS actual_value,
                          SUM(CASE d.profit_calc_type 
                               WHEN 1 THEN ssl.snapshot_on_hand_qty * ssl. snapshot_unit_cost
                               WHEN 2 THEN ssl.snapshot_on_hand_qty * ssl.snapshot_unit_retail                                 
                          END) AS snapshot_value                          
                     from stake_head sh,
                          stake_sku_loc ssl,
                          stake_prod_loc spl,
                          --
                          (select number_1 dept,
                                  number_2 class,
                                  number_3 subclass,
                                  varchar2_1 dept_name,
                                  varchar2_2 class_name,
                                  varchar2_3 subclass_name,
                                  deps.profit_calc_type
                             from gtt_6_num_6_str_6_date gtt_depts,
                                  deps
                            where gtt_depts.number_1 = deps.dept) d,
                          --
                          gtt_num_num_str_str_date_date loc_gtt
                    where sh.cycle_count   = spl.cycle_count
                      and ssl.cycle_count  = spl.cycle_count
                      and d.dept           = spl.dept
                      and d.class          = spl.class
                      and d.subclass       = spl.subclass
                      and ssl.dept         = spl.dept
                      and ssl.class        = spl.class
                      and ssl.subclass     = spl.subclass
                      and loc_gtt.number_1 = spl.location
                      and ssl.location     = spl.location
                      and ssl.processed    = 'P'
                      and spl.processed    = 'N'
                    group by sh.cycle_count,
                             sh.cycle_count_desc,
                             sh.stocktake_type,
                             sh.stocktake_date,
                             spl.dept,
                             spl.class,
                             spl.subclass,
                             d.dept_name,
                             d.class_name,
                             d.subclass_name,
                             spl.location,
                             spl.loc_type,
                             loc_gtt.varchar2_1)
   select I_session_id,
          i.cycle_count,
          i.cycle_count_desc,
          i.stocktake_type,
          i.stocktake_date,
          i.dept,
          i.dept_name,
          i.class,
          i.class_name,
          i.subclass,
          i.subclass_name,
          i.location,
          i.loc_type,
          i.loc_name,
          (case when i.over_sum =0 and i.tot_snapshot_on_hand_qty = 0 then
                   0
                   when i.tot_snapshot_on_hand_qty = 0 then
                   100
                   else (i.over_sum  * 100) / i.tot_snapshot_on_hand_qty  
              end
              +	   
              abs(case when i.short_sum =0 and i.tot_snapshot_on_hand_qty = 0 then
                   0
                   when i.tot_snapshot_on_hand_qty = 0 then
                   100
                   else (i.short_sum  * 100) / i.tot_snapshot_on_hand_qty  
              end)),
          case when i.over_sum =0 and i.tot_snapshot_on_hand_qty = 0 then
               0
               when i.tot_snapshot_on_hand_qty = 0 then
               100
               else (i.over_sum  * 100) / i.tot_snapshot_on_hand_qty  
          end,
          case when i.short_sum =0 and i.tot_snapshot_on_hand_qty = 0 then
               0
               when i.tot_snapshot_on_hand_qty = 0 then
               100
               else (i.short_sum  * 100) / i.tot_snapshot_on_hand_qty  
          end,
          case when (i.actual_value - i.snapshot_value) = 0  and i.snapshot_value = 0 then 
                   0
                   when i.snapshot_value = 0 and (i.actual_value - i.snapshot_value) <> 0 then 
                   9999999999.9999
                   else (i.actual_value - i.snapshot_value) * 100 / i.snapshot_value 
          end,
          i.snapshot_value,
          i.actual_value
     from cycles i
    where L_fa_stk_count_var_pct < abs(case when i.over_sum =0 and i.tot_snapshot_on_hand_qty = 0 then
                   0
                   when i.tot_snapshot_on_hand_qty = 0 then
                   100
                   else (i.over_sum  * 100) / i.tot_snapshot_on_hand_qty  
              end
              +	   
              case when i.short_sum =0 and i.tot_snapshot_on_hand_qty = 0 then
                   0
                   when i.tot_snapshot_on_hand_qty = 0 then
                   100
                   else (i.short_sum  * 100) / i.tot_snapshot_on_hand_qty  
              end);
 
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id|| ' insert rms_oi_stk_cnt_value_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
  
   select count(*)
     into O_stk_cnt_value_var_count
     from rms_oi_stk_cnt_value_variance
    where session_id = I_session_id;
    
   select count(*)
     into L_seven_day_count
     from rms_oi_stk_cnt_value_variance oi
    where oi.session_id              = I_session_id
      and L_vdate - oi.stocktake_date >= 7;
     
   if L_seven_day_count             > 0 AND O_stk_cnt_value_var_count > L_fa_stk_count_var_loc_cnt THEN
      O_report_level                := RMS_OI_FIN_ANALYST.RED;
   elsif L_seven_day_count          > 0 OR O_stk_cnt_value_var_count > L_fa_stk_count_var_loc_cnt THEN
      O_report_level                := RMS_OI_FIN_ANALYST.YELLOW;
   else
      O_report_level := RMS_OI_FIN_ANALYST.GREEN;
   end if;
   
   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   RETURN OI_UTILITY.SUCCESS;

EXCEPTION
   WHEN OI_UTILITY.PROGRAM_ERROR THEN
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   WHEN OTHERS THEN
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END STOCK_COUNT_VALUE_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION SHRINKAGE_VARIANCE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_shrinkage_var_count     IN OUT NUMBER,
                            O_neg_shrinkage_var_count IN OUT NUMBER,
                            O_report_level            IN OUT VARCHAR2,
                            I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_set_of_books_ids        IN     OBJ_NUMERIC_ID_TABLE,
                            I_org_unit_ids            IN     OBJ_NUMERIC_ID_TABLE,
                            I_locations               IN     OBJ_NUMERIC_ID_TABLE,
                            I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                            I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                            I_subclasses              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(60) := 'RMS_OI_FIN_ANALYST.SHRINKAGE_VARIANCE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_shrinkage_var_tolerance_pct rms_oi_system_options.fa_shrinkage_var_tolerance_pct%TYPE;
   L_shrinkage_var_max_pct       rms_oi_system_options.fa_shrinkage_var_max_pct%TYPE;
   L_shrinkage_var_critical_cnt  rms_oi_system_options.fa_shrinkage_var_critical_cnt%TYPE;
   l_max_variance_pct            rms_oi_shrinkage_variance.variance_pct%TYPE := 0;
   L_last_eom_date               system_variables.last_eom_date%TYPE;

   cursor c_oi_system_option is
   select oi.fa_shrinkage_var_tolerance_pct,
          oi.fa_shrinkage_var_max_pct,
          oi.fa_shrinkage_var_critical_cnt,
          sv.last_eom_date
     from rms_oi_system_options oi,
          system_variables sv;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_oi_system_option;
   fetch c_oi_system_option into L_shrinkage_var_tolerance_pct,
                                 L_shrinkage_var_max_pct,
                                 L_shrinkage_var_critical_cnt,
                                 L_last_eom_date;
   close c_oi_system_option;
   --
   --insert merch hierarchy into gtt_6_num_6_str_6_date
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  FALSE) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   --- 
   --insert merch hierarchy into gtt_num_num_str_str_date_date
   if OI_UTILITY.SETUP_ORG_UNIT_LOCATIONS(O_error_message,
                                          I_session_id,
                                          I_set_of_books_ids,
                                          I_org_unit_ids,
                                          I_locations) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_shrinkage_variance where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_shrinkage_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_shrinkage_variance (session_id,
                                          region,
                                          region_name,
                                          district,
                                          district_name,
                                          loc,
                                          loc_type,
                                          loc_name,
                                          dept,
                                          dept_name,
                                          class,
                                          class_name,
                                          subclass,
                                          subclass_name,
                                          budgeted_shrinkage,
                                          budgeted_shrinkage_rate,
                                          actual_shrinkage,
                                          actual_shrinkage_rate,
                                          total_sales,
                                          variance_pct,
                                          unit_and_value_count,
                                          currency_code)
      /* Cursor to fetch Departments */
      with depts as (select number_1 dept,
                            number_2 class,
                            number_3 subclass,
                            varchar2_1 dept_name,
                            varchar2_2 class_name,
                            varchar2_3 subclass_name
                       from gtt_6_num_6_str_6_date gtt_depts),
      /* Cursor to fetch Locations */
      locations as  (/* cursor to fetch location details for a store*/
                     select number_1 location, 
                            varchar2_1 location_name,
                            varchar2_2 loc_type,
                            vs.region,
                            vr.region_name,
                            vd.district,
                            vd.district_name,
                            vs.currency_code
                       from gtt_num_num_str_str_date_date gtt,
                            v_store vs,
                            v_region vr,
                            v_district vd
                      where gtt.varchar2_2 = 'S'
                        and vs.store = gtt.number_1
                        and vr.region = vs.region
                        and vd.district = vs.district)
      select session_id,
             region,
             region_name,
             district,
             district_name,
             loc,
             loc_type,
             loc_name,           
             dept,
             dept_name,
             class,
             class_name,
             subclass,
             subclass_name,
             budgeted_shrinkage,
             budgeted_shrinkage_rate,
             actual_shrinkage,
             case when actual_shrinkage = 0  and total_sales = 0 then 0                          
                  when total_sales = 0 and actual_shrinkage <> 0 then 9999999999999999.9999                          
                  else ((actual_shrinkage / total_sales) * 100 ) 
             end as actual_shrinkage_rate,
             total_sales,
             case when budgeted_shrinkage = 0  and actual_shrinkage = 0 then 0
                  when budgeted_shrinkage = 0 and actual_shrinkage <> 0 then 9999999999999999.9999
                  else (((actual_shrinkage - budgeted_shrinkage)/ budgeted_shrinkage) * 100 ) 
             end as variance_pct,
             unit_and_value_count,
             currency_code from (
                                 select I_session_id session_id,
                                        l.region region,
                                        l.region_name region_name,
                                        l.district district,
                                        l.district_name district_name,
                                        md.location loc,
                                        l.loc_type loc_type,
                                        l.location_name loc_name,           
                                        md.dept dept,
                                        d.dept_name dept_name,
                                        md.class class,
                                        d.class_name class_name,
                                        md.subclass subclass,
                                        d.subclass_name subclass_name,
                                        case deps.profit_calc_type 
                                            when 1 then (nvl(md.net_sales_cost,0) * nvl(hdb.shrinkage_pct,0)/100)
                                            when 2 then (nvl(md.net_sales_retail,0) * nvl(hdb.shrinkage_pct,0)/100)
                                          end as budgeted_shrinkage,
                                        nvl(hdb.shrinkage_pct,0) as budgeted_shrinkage_rate,
                                        case deps.profit_calc_type 
                                            when 1 then nvl(md.shrinkage_cost,0) 
                                            when 2 then nvl(md.shrinkage_retail,0)
                                          end as actual_shrinkage,
                                        case deps.profit_calc_type 
                                            when 1 then nvl(md.net_sales_cost,0)
                                            when 2 then nvl(md.net_sales_retail,0)
                                        end as total_sales,
                                        case deps.profit_calc_type 
                                            when 1 then nvl(md.stocktake_actstk_cost,0)
                                            when 2 then nvl(md.stocktake_actstk_retail ,0)
                                        end as unit_and_value_count,
                                        l.currency_code
                                   from month_data md, 
                                        half_data_budget hdb, 
                                        depts d, 
                                        locations l,
                                        deps deps
                                  where md.dept = d.dept
                                    and md.class = d.class
                                    and md.subclass = d.subclass
                                    and md.location = l.location
                                    and md.loc_type = l.loc_type
                                    and md.eom_date = L_last_eom_date
                                    and md.currency_ind = 'L'
                                    and hdb.half_no = md.half_no
                                    and hdb.dept  = md.dept
                                    and hdb.location = md.location   
                                    and hdb.loc_type = md.loc_type
                                    and deps.dept = md.dept)
      where ABS( case when budgeted_shrinkage = 0  and actual_shrinkage = 0 then 0
                      when budgeted_shrinkage = 0 and actual_shrinkage <> 0 then 9999999999999999.9999
                      else (((actual_shrinkage - budgeted_shrinkage)/ budgeted_shrinkage) * 100) 
                      end) >= L_shrinkage_var_tolerance_pct;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_shrinkage_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_shrinkage_var_count
     from rms_oi_shrinkage_variance
    where session_id = I_session_id;

   if O_shrinkage_var_count is null then
      O_shrinkage_var_count := 0;
   end if;

   select count(*)
     into O_neg_shrinkage_var_count
     from rms_oi_shrinkage_variance
    where session_id = I_session_id
      and variance_pct < 0;

   if O_neg_shrinkage_var_count is null then
      O_neg_shrinkage_var_count := 0;
   end if;

   select MAX(ABS(variance_pct))
     into l_max_variance_pct
     from rms_oi_shrinkage_variance
    where session_id = I_session_id;

   if l_max_variance_pct > L_shrinkage_var_max_pct and O_shrinkage_var_count > L_shrinkage_var_critical_cnt then
      O_report_level := RMS_OI_FIN_ANALYST.RED;
   elsif l_max_variance_pct >= L_shrinkage_var_max_pct or O_shrinkage_var_count >= L_shrinkage_var_critical_cnt then
      O_report_level := RMS_OI_FIN_ANALYST.YELLOW;
   elsif (l_max_variance_pct = 0 or l_max_variance_pct <  L_shrinkage_var_max_pct) 
     and  O_shrinkage_var_count < L_shrinkage_var_critical_cnt then
      O_report_level := RMS_OI_FIN_ANALYST.GREEN;
   else 
      O_report_level := RMS_OI_FIN_ANALYST.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   RETURN OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if c_oi_system_option%ISOPEN then close c_oi_system_option; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if c_oi_system_option%ISOPEN then close c_oi_system_option; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END SHRINKAGE_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION LATE_POSTED_TRANSACTIONS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_late_posted_tran_count  IN OUT NUMBER,
                                  O_report_level            IN OUT VARCHAR2,
                                  I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_set_of_books_ids        IN     OBJ_NUMERIC_ID_TABLE,
                                  I_org_unit_ids            IN     OBJ_NUMERIC_ID_TABLE,
                                  I_locations               IN     OBJ_NUMERIC_ID_TABLE,
                                  I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                                  I_subclasses              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(60) := 'RMS_OI_FIN_ANALYST.LATE_POSTED_TRANSACTIONS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   
   L_threshold_trn_cnt RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_THRESHOLD_TRN_CNT%TYPE;
   L_threshold_loc_cnt RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_THRESHOLD_LOC_CNT%TYPE;
   L_org_hier_level    RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_ORG_HIER_LEVEL%TYPE;
   L_loc_count         NUMBER;
   
   cursor c_oi_system_option is
     select fa_late_post_threshold_trn_cnt,
            fa_late_post_threshold_loc_cnt,
            fa_late_post_org_hier_level
       from rms_oi_system_options;
   
   cursor c_get_tran_count is
     select sum(tran_count)
       from rms_oi_late_post_transactions
      where session_id = I_session_id
        and loc is not NULL;
       
BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   --
   open c_oi_system_option;
   fetch c_oi_system_option into L_threshold_trn_cnt,
                                 L_threshold_loc_cnt,
                                 L_org_hier_level;
   close c_oi_system_option;
   --
   --insert merch hierarchy into gtt_6_num_6_str_6_date
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  FALSE) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   --- 
   --insert merch hierarchy into gtt_num_num_str_str_date_date
   if OI_UTILITY.SETUP_ORG_UNIT_LOCATIONS(O_error_message,
                                          I_session_id,
                                          I_set_of_books_ids,
                                          I_org_unit_ids,
                                          I_locations) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   delete from rms_oi_late_post_transactions where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_late_post_transactions - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   ---
   insert into rms_oi_late_post_transactions(session_id,
                                             loc,
                                             loc_name,
                                             loc_type,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             subclass_name,
                                             tran_type,
                                             tran_count,
                                             total_cost,
                                             total_retail)
                                      select I_session_id,
                                             tdh.location,
                                             loc.varchar2_1 loc_name,
                                             tdh.loc_type,
                                             tdh.dept,
                                             dcs.varchar2_1 dept_name,
                                             tdh.class,
                                             dcs.varchar2_2 class_name,
                                             tdh.subclass,
                                             dcs.varchar2_3 subclass_name,
                                             tdh.tran_code,
                                             count(*),
                                             sum(NVL(tdh.total_cost,0)),
                                             sum(NVL(tdh.total_retail,0))
                                        from tran_data_history tdh,
                                             gtt_6_num_6_str_6_date dcs,
                                             gtt_num_num_str_str_date_date loc,
                                             system_variables sysv
                                        where tdh.tran_code  not in (2, 3, 4, 5, 25, 41, 44)
                                          and tdh.tran_date  <= sysv.last_eom_date
                                          and tdh.post_date   > sysv.last_eom_date
                                          and tdh.dept        = dcs.number_1
                                          and tdh.class       = dcs.number_2
                                          and tdh.subclass    = dcs.number_3
                                          and tdh.location    = loc.number_1
                                        group by tdh.location, loc.varchar2_1, tdh.loc_type,
                                                 tdh.dept,     dcs.varchar2_1,
                                                 tdh.class,    dcs.varchar2_2,
                                                 tdh.subclass, dcs.varchar2_3,
                                                 tdh.tran_code;
   --
   merge into rms_oi_late_post_transactions target
   using (select org_hier_value Region,
                 region_name,
                 null           District,
                 null           District_name,
                 wh             loc,
                 currency_code
            from wh,
                 v_region_tl
           where org_hier_type = 30
             and org_hier_value = region
          union all
          select d.region,
                 r.region_name,
                 org_hier_value District,
                 vd.district_name,
                 wh             loc,
                 wh.currency_code
            from wh, 
                 district d,
                 v_region_tl r,
                 v_district_tl vd
           where org_hier_type = 40
             and d.district = org_hier_value
             and d.district = vd.district
             and d.region = r.region
          union all
          select wh.physical_wh    Region,
                 v_wh_tl.wh_name   Region_name,
                 null          District,
                 null           District_name,
                 wh.wh            loc,
                 currency_code
            from wh,
                 v_wh_tl
           where org_hier_type not in (30,40)
             and wh.physical_wh = v_wh_tl.wh
          union all
          select s.region,
                 r.region_name,
                 s.district,
                 d.district_name,
                 s.store            loc,
                 currency_code
            from v_store s,
                 v_region_tl r,
                 v_district_tl d
           where s.district = d.district
             and s.region = r.region) use_this
   on (    target.session_id = I_session_id
       and target.loc        = use_this.loc)
   when matched then update
    set target.region        = use_this.region,
        target.region_name   = use_this.region_name,
        target.district      = use_this.district,
        target.district_name = use_this.district_name,
        target.currency_code = use_this.currency_code;
   --
   merge into rms_oi_late_post_transactions target
   using (select code,
                 decode
            from v_tran_data_codes_tl) use_this
   on (    target.session_id = I_session_id
       and target.tran_type  = use_this.code)
   when matched then update
    set target.tran_type_desc = use_this.decode;
   --
   --roll up to physical_wh/region
   insert into rms_oi_late_post_transactions(session_id,
                                             region,
                                             region_name,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             subclass_name,
                                             tran_type,
                                             tran_type_desc,
                                             tran_count,
                                             total_cost,
                                             total_retail,
                                             currency_code)
                                      select session_id,
                                             region,
                                             region_name,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             subclass_name,
                                             tran_type,
                                             tran_type_desc,
                                             sum(tran_count),
                                             sum(total_cost * mvc.exchange_rate),
                                             sum(total_retail * mvc.exchange_rate),
                                             org_hier.currency_code
                                        from rms_oi_late_post_transactions lpt,
                                             (select wh org_hier_id,
                                                     currency_code
                                                from wh 
                                               where wh = physical_wh
                                              union all 
                                              select region org_hier_id,
                                                     currency_code
                                                from region) org_hier,
                                             (select from_currency,
                                                     to_currency,
                                                     effective_date,
                                                     exchange_rate,
                                                     exchange_type
                                                from (select mv.from_currency,
                                                             mv.to_currency,
                                                             mv.effective_date,
                                                             mv.exchange_rate,
                                                             mv.exchange_type,
                                                             rank() over (partition by mv.from_currency,
                                                                                       mv.to_currency
                                                                              order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                                       mv.effective_date desc) date_rank
                                                        from mv_currency_conversion_rates mv,
                                                             system_config_options so
                                                       where effective_date <= get_vdate
                                                         and ((so.consolidation_ind = 'Y' and mv.exchange_type in('P', 'C', 'O'))
                                                              or
                                                              (so.consolidation_ind = 'N' and mv.exchange_type in('P', 'O'))))
                                               where date_rank = 1) mvc
                                       where lpt.session_id    = I_session_id
                                         and lpt.region        = org_hier.org_hier_id
                                         and mvc.from_currency = lpt.currency_code
                                         and mvc.to_currency   = org_hier.currency_code
                                       group by session_id,
                                                region,
                                                region_name,
                                                dept,
                                                dept_name,
                                                class,
                                                class_name,
                                                subclass,
                                                subclass_name,
                                                tran_type,
                                                tran_type_desc,
                                                org_hier.currency_code;
   --
   --roll up to district
   insert into rms_oi_late_post_transactions(session_id,
                                             region,
                                             region_name,
                                             district,
                                             district_name,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             subclass_name,
                                             tran_type,
                                             tran_type_desc,
                                             tran_count,
                                             total_cost,
                                             total_retail,
                                             currency_code)
                                      select session_id,
                                             lpt.region,
                                             region_name,
                                             lpt.district,
                                             lpt.district_name,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             subclass_name,
                                             tran_type,
                                             tran_type_desc,
                                             sum(tran_count),
                                             sum(total_cost   * mvc.exchange_rate),
                                             sum(total_retail * mvc.exchange_rate),
                                             d.currency_code
                                        from rms_oi_late_post_transactions lpt,
                                             district d,
                                             (select from_currency,
                                                     to_currency,
                                                     effective_date,
                                                     exchange_rate,
                                                     exchange_type
                                                from (select mv.from_currency,
                                                             mv.to_currency,
                                                             mv.effective_date,
                                                             mv.exchange_rate,
                                                             mv.exchange_type,
                                                             rank() over (partition by mv.from_currency,
                                                                                       mv.to_currency
                                                                              order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                                       mv.effective_date desc) date_rank
                                                        from mv_currency_conversion_rates mv,
                                                             system_config_options so
                                                       where effective_date <= get_vdate
                                                         and ((so.consolidation_ind = 'Y' and mv.exchange_type in('P', 'C', 'O'))
                                                              or
                                                              (so.consolidation_ind = 'N' and mv.exchange_type in('P', 'O'))))
                                               where date_rank = 1) mvc
                                        where lpt.session_id = I_session_id
                                          and loc            is not NULL
                                          and lpt.district   = d.district
                                         and mvc.from_currency = lpt.currency_code
                                         and mvc.to_currency   = d.currency_code
                                        group by session_id,
                                                 lpt.region,
                                                 region_name,
                                                 lpt.district,
                                                 lpt.district_name,
                                                 dept,
                                                 dept_name,
                                                 class,
                                                 class_name,
                                                 subclass,
                                                 subclass_name,
                                                 tran_type,
                                                 tran_type_desc,
                                                 d.currency_code;
   ---
   open c_get_tran_count;
   fetch c_get_tran_count into O_late_posted_tran_count;
   close c_get_tran_count;
   
   if O_late_posted_tran_count is NULL then
      O_late_posted_tran_count := 0;
   end if;
   --
   select count(distinct loc)
     into L_loc_count
     from rms_oi_late_post_transactions
    where session_id = I_session_id
      and loc is not null;
   --
   if (O_late_posted_tran_count > 0 and O_late_posted_tran_count >= L_threshold_trn_cnt) and
      (L_loc_count > 0 and L_loc_count >= L_threshold_loc_cnt) then
      O_report_level := RED;
   elsif (O_late_posted_tran_count > 0 and O_late_posted_tran_count >= L_threshold_trn_cnt) or
         (L_loc_count > 0 and L_loc_count >= L_threshold_loc_cnt) then
      O_report_level := YELLOW;
   else
      O_report_level := GREEN;
   end if;
   ---
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' End');
   RETURN OI_UTILITY.SUCCESS;
   
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if c_oi_system_option%ISOPEN then close c_oi_system_option; end if;
      if c_get_tran_count%ISOPEN then close c_get_tran_count; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if c_oi_system_option%ISOPEN then close c_oi_system_option; end if;
      if c_get_tran_count%ISOPEN then close c_get_tran_count; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END LATE_POSTED_TRANSACTIONS;
--------------------------------------------------------------------------------
FUNCTION REFRESH_STK_CNT_VALUE_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_stock_cnt_count        IN OUT NUMBER,
                                        O_report_level           IN OUT VARCHAR2,
                                        I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                        I_cylcnt_dcs_locs        IN     RMS_OI_CYLCNT_LOC_DCS_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_FIN_ANALYST.REFRESH_STK_CNT_VALUE_VARIANCE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                      DATE := get_vdate;
   L_fa_stk_count_var_pct       RMS_OI_SYSTEM_OPTIONS.FA_STK_CNT_VALUE_TOLERENCE_PCT%TYPE;
   L_fa_stk_count_var_loc_cnt   RMS_OI_SYSTEM_OPTIONS.FA_STK_CNT_VALUE_VAR_CRIT_CNT%TYPE;
   L_seven_day_count NUMBER(10) := 0;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   SELECT oi.fa_stk_cnt_value_tolerence_pct,
          oi.fa_stk_cnt_value_var_crit_cnt
     INTO L_fa_stk_count_var_pct,
          L_fa_stk_count_var_loc_cnt
     FROM rms_oi_system_options oi;

   delete from rms_oi_stk_cnt_value_variance
      where session_id  = I_session_id
        and (cycle_count, loc, dept, class, subclass) in (select cycle_cnt_locs.cycle_count, cycle_cnt_locs.location, cycle_cnt_locs.dept, cycle_cnt_locs.class, cycle_cnt_locs.subclass
                                     from table(cast(I_cylcnt_dcs_locs as RMS_OI_CYLCNT_LOC_DCS_TBL)) cycle_cnt_locs);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stk_cnt_value_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_stock_cnt_count
     from rms_oi_stk_cnt_value_variance
    where session_id = I_session_id;
    
   select count(*)
     into L_seven_day_count
     from rms_oi_stk_cnt_value_variance oi
    where oi.session_id              = I_session_id
      and L_vdate - oi.stocktake_date >= 7;
     
   if L_seven_day_count             > 0 AND O_stock_cnt_count > L_fa_stk_count_var_loc_cnt THEN
      O_report_level                := RMS_OI_FIN_ANALYST.RED;
   elsif L_seven_day_count          > 0 OR O_stock_cnt_count > L_fa_stk_count_var_loc_cnt THEN
      O_report_level                := RMS_OI_FIN_ANALYST.YELLOW;
   else
      O_report_level := RMS_OI_FIN_ANALYST.GREEN;
   end if;
   

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_STK_CNT_VALUE_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STK_CNT_VAL_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_FIN_ANALYST.DELETE_SESSION_STK_CNT_VAL_VAR';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_stk_cnt_value_variance where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stk_cnt_value_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_STK_CNT_VAL_VAR;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_WAC_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_FIN_ANALYST.DELETE_SESSION_WAC_VAR';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_wac_variance where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_wac_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_WAC_VAR;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_CUM_MARKON_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_FIN_ANALYST.DELETE_SESSION_CUM_MARKON_VAR';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_cum_markon_pct_variance where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_cum_markon_pct_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_CUM_MARKON_VAR;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_SHRINKAGE_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_FIN_ANALYST.DELETE_SESSION_SHRINKAGE_VAR';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_shrinkage_variance where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_shrinkage_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_SHRINKAGE_VAR;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_LATE_POST_TRANS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_FIN_ANALYST.DELETE_SESSION_LATE_POST_TRANS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_late_post_transactions where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_late_post_transactions - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_LATE_POST_TRANS;
--------------------------------------------------------------------------------
END RMS_OI_FIN_ANALYST;
/
