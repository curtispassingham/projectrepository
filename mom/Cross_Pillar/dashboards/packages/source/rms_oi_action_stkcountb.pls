CREATE OR REPLACE PACKAGE BODY RMS_OI_ACTION_STKCOUNT AS
--------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_CONT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_session_id     IN     oi_session_id_log.session_id%TYPE,
                         I_cycle_count    IN     stake_head.cycle_count%type)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_LOCATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result            OUT RMS_OI_CYCLE_COUNT_STATUS_REC,
                         I_session_id     IN     oi_session_id_log.session_id%TYPE,
                         I_cycle_cnt_locs IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.DELETE_LOCATION';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

   L_cycle_count         stake_head.cycle_count%type;
   L_location            stake_location.location%type;
   L_loc_type            stake_location.loc_type%type;
   L_error_message       rtk_errors.rtk_text%type;
   L_exists              VARCHAR2(1) := 'N';
   L_processed           VARCHAR2(1) := 'N';


   cursor c_exists is
     select 'Y'
       from stake_location
      where cycle_count = L_cycle_count
        and location    = L_location;

   cursor c_processed is
     select 'Y'
       from stake_sku_loc
      where cycle_count = L_cycle_count
        and processed   = 'P'
        and location    = L_location
        and rownum      = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --nothing to do
   if I_cycle_cnt_locs is null or I_cycle_cnt_locs.count < 1 then
      return 1;
   end if;


   O_result :=  RMS_OI_CYCLE_COUNT_STATUS_REC(0, RMS_OI_CYCLE_COUNT_LOC_TBL(), 0, RMS_OI_CYCLE_COUNT_FAIL_TBL());

   for i in 1..I_cycle_cnt_locs.count loop

      SAVEPOINT cc_savepoint;

      L_cycle_count := I_cycle_cnt_locs(i).cycle_count;
      L_location    := I_cycle_cnt_locs(i).location;

      L_exists := 'N';
      open c_exists;
      fetch c_exists into L_exists;
      close c_exists;

      L_processed := 'N';
      open c_processed;
      fetch c_processed into L_processed;
      close c_processed;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' cyc cnt:'||L_cycle_count||' loc:'||L_location
                             ||' L_exists: '||L_exists||' L_processed: '||L_processed);

      if LOCATION_ATTRIB_SQL.GET_TYPE(L_error_message,
                                      L_loc_type,
                                      L_location) = FALSE then

            OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
            O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
            O_result.fail_cycle_count_tbl.extend;
            O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_error_message);

      elsif L_exists = 'N' then

         L_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_LOC', L_location, L_cycle_count, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);

         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_error_message);

      elsif L_processed = 'Y' then

         L_error_message := SQL_LIB.CREATE_MSG('NO_DEL_STKCOUNT_LOC', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);

         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_error_message);

      else

         if STKCOUNT_SQL.PRE_DELETES_STKCNT_LOC(L_cycle_count,
                                                L_loc_type,
                                                L_location,
                                                L_error_message) = FALSE then
            L_error_message := sql_lib.create_msg(L_error_message);
            OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
            O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
            O_result.fail_cycle_count_tbl.extend;
            O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
               RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                           L_location,
                                           L_error_message);

            ROLLBACK TO SAVEPOINT cc_savepoint;

         else

            O_result.success_cycle_count_count := O_result.success_cycle_count_count + 1;
            O_result.success_cycle_count_tbl.extend;
            O_result.success_cycle_count_tbl(O_result.success_cycle_count_tbl.count) :=
               RMS_OI_CYCLE_COUNT_LOC_REC(L_cycle_count,
                                          L_location);

         end if;

      end if;

   end loop;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END DELETE_LOCATION;
--------------------------------------------------------------------------------
FUNCTION ACCEPT_RESULTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result            OUT RMS_OI_CYCLE_COUNT_STATUS_REC,
                        I_session_id     IN     oi_session_id_log.session_id%TYPE,
                        I_cycle_cnt_locs IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.ACCEPT_RESULTS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_user                VARCHAR2(30) := get_user;

   L_cycle_count         stake_head.cycle_count%type;
   L_location            stake_location.location%type;
   L_loc_type            stake_location.loc_type%type;
   L_error_message       rtk_errors.rtk_text%type;
   L_exists              VARCHAR2(1) := 'N';

   cursor c_exists is
     select 'Y'
       from stake_location
      where cycle_count = L_cycle_count
        and location    = L_location;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --nothing to do
   if I_cycle_cnt_locs is null or I_cycle_cnt_locs.count < 1 then
      return 1;
   end if;

   O_result :=  RMS_OI_CYCLE_COUNT_STATUS_REC(0, RMS_OI_CYCLE_COUNT_LOC_TBL(), 0, RMS_OI_CYCLE_COUNT_FAIL_TBL());

   for i in 1..I_cycle_cnt_locs.count loop

      SAVEPOINT acc_savepoint;

      L_cycle_count := I_cycle_cnt_locs(i).cycle_count;
      L_location    := I_cycle_cnt_locs(i).location;

      L_exists := 'N';
      open c_exists;
      fetch c_exists into L_exists;
      close c_exists;

      if LOCATION_ATTRIB_SQL.GET_TYPE(L_error_message,
                                      L_loc_type,
                                      L_location) = FALSE then

         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
         RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                     L_location,
                                     L_error_message);

      elsif LOCK_STAKE_CONT(L_error_message,
                            I_session_id,
                            L_cycle_count) = 0 then

         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_error_message);

      elsif L_exists = 'N' then

         L_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_LOC', L_location, L_cycle_count, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);

         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYCLE_COUNT_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_error_message);

      else

         delete from stake_cont
          where cycle_count = L_cycle_count
            and location    = L_location
            and run_type    = 'T'
            and user_id     = L_user;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                   ' delete T stake_cont rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         insert into stake_cont(cycle_count,
                                item,
                                loc_type,
                                location,
                                run_type,
                                user_id)
         select ssl.cycle_count,
                ssl.item,
                ssl.loc_type,
                ssl.location,
                'A',
                L_user
           from stake_sku_loc ssl
          where ssl.cycle_count    = L_cycle_count
            and ssl.loc_type       = L_loc_type
            and ssl.location       = L_location
            and ssl.processed      = 'N'
            --
            and not exists (select 'x'
                              from stake_cont sc
                             where sc.cycle_count    = ssl.cycle_count
                               and sc.loc_type       = ssl.loc_type
                               and sc.location       = ssl.location
                               and sc.user_id        = L_user
                               and sc.run_type       = 'A'
                               and sc.item           = ssl.item);

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                   ' insert stake_cont rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         O_result.success_cycle_count_count := O_result.success_cycle_count_count + 1;
         O_result.success_cycle_count_tbl.extend;
         O_result.success_cycle_count_tbl(O_result.success_cycle_count_tbl.count) :=
            RMS_OI_CYCLE_COUNT_LOC_REC(L_cycle_count,
                                       L_location);
      end if;

   end loop;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END ACCEPT_RESULTS;
--------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_CONT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_session_id     IN     oi_session_id_log.session_id%TYPE,
                         I_cycle_count    IN     stake_head.cycle_count%type)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.LOCK_STAKE_CONT';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor c_lock is
     select 'x'
       from stake_cont
      where cycle_count = I_cycle_count;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_lock;
   close c_lock;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when RECORD_LOCKED then
      if c_lock%isopen then
         close c_lock;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            null,
                                            null,
                                            null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END LOCK_STAKE_CONT;
--------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_PROD_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id     IN     oi_session_id_log.session_id%TYPE,
                             I_cycle_count    IN     stake_head.cycle_count%type)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.LOCK_STAKE_PROD_LOC';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor c_lock is
     select 'x'
       from stake_prod_loc
      where cycle_count = I_cycle_count;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_lock;
   close c_lock;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when RECORD_LOCKED then
      if c_lock%isopen then
         close c_lock;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            null,
                                            null,
                                            null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END LOCK_STAKE_PROD_LOC;
--------------------------------------------------------------------------------
FUNCTION ACCEPT_VALUE_VARIRANCE_RESULTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_result             OUT RMS_OI_CYLCNT_DCS_STATUS_REC,
                                        I_session_id      IN     oi_session_id_log.session_id%TYPE,
                                        I_cylcnt_dcs_locs IN     RMS_OI_CYLCNT_LOC_DCS_TBL)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.ACCEPT_VALUE_VARIRANCE_RESULTS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_user                VARCHAR2(30) := get_user;

   L_cycle_count         stake_head.cycle_count%type;
   L_location            stake_location.location%type;
   L_loc_type            stake_location.loc_type%type;
   L_dept                stake_prod_loc.dept%type;
   L_class               stake_prod_loc.class%type;
   L_subclass            stake_prod_loc.subclass%type;
   L_error_message       rtk_errors.rtk_text%type;
   L_exists              VARCHAR2(1) := 'N';

    cursor c_exists is
     select 'Y'
       from stake_location
      where cycle_count = L_cycle_count
        and location    = L_location;


BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --nothing to do
   if I_cylcnt_dcs_locs is null or I_cylcnt_dcs_locs.count < 1 then
      return 1;
   end if;

   O_result :=  RMS_OI_CYLCNT_DCS_STATUS_REC(0, RMS_OI_CYLCNT_LOC_DCS_TBL(), 0, RMS_OI_CYLCNT_LOC_DCS_FAIL_TBL());

   for i in 1..I_cylcnt_dcs_locs.count loop

      SAVEPOINT acc_savepoint;

      L_cycle_count := I_cylcnt_dcs_locs(i).cycle_count;
      L_location    := I_cylcnt_dcs_locs(i).location;
      L_dept        := I_cylcnt_dcs_locs(i).dept;
      L_class       := I_cylcnt_dcs_locs(i).class;
      L_subclass    := I_cylcnt_dcs_locs(i).subclass;

      L_exists := 'N';
      open c_exists;
      fetch c_exists into L_exists;
      close c_exists;

      if LOCATION_ATTRIB_SQL.GET_TYPE(L_error_message,
                                      L_loc_type,
                                      L_location) = FALSE then

         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
         RMS_OI_CYLCNT_LOC_DCS_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_dept,
                                        L_class,
                                        L_subclass,
                                        L_error_message);

      elsif LOCK_STAKE_PROD_LOC(L_error_message,
                                I_session_id,
                                L_cycle_count) = 0 then

         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYLCNT_LOC_DCS_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_dept,
                                        L_class,
                                        L_subclass,
                                        L_error_message);

      elsif L_exists = 'N' then

         L_error_message := SQL_LIB.CREATE_MSG('STKCNT_INV_LOC', L_location, L_cycle_count, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);

         O_result.fail_cycle_count_count := O_result.fail_cycle_count_count + 1;
         O_result.fail_cycle_count_tbl.extend;
         O_result.fail_cycle_count_tbl(O_result.fail_cycle_count_tbl.count) :=
            RMS_OI_CYLCNT_LOC_DCS_FAIL_REC(L_cycle_count,
                                        L_location,
                                        L_dept,
                                        L_class,
                                        L_subclass,
                                        L_error_message);

      else

         update stake_prod_loc
            set processed   = 'P'
          where cycle_count = L_cycle_count
            and loc_type    = L_loc_type
            and location    = L_location
            and dept        = L_dept
            and class       = L_class
            and subclass    = L_subclass;


         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                   ' update stake_prod_loc rows - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         O_result.success_cycle_count_count := O_result.success_cycle_count_count + 1;
         O_result.success_cycle_count_tbl.extend;
         O_result.success_cycle_count_tbl(O_result.success_cycle_count_tbl.count) :=
            RMS_OI_CYLCNT_LOC_DCS_REC(L_cycle_count,
                                      L_location,
                                      L_dept,
                                      L_class,
                                      L_subclass);
      end if;

   end loop;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END ACCEPT_VALUE_VARIRANCE_RESULTS;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_STKCOUNT;
/
