CREATE OR REPLACE PACKAGE RESA_OI_STORE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name : STORE_DAY_STATUS_HIST
--Purpose       : This function retrieves the count of open stores for the past
--                7 days.
--                To be used by the following reports:
--                   OPEN STORE DAYS (DASHBOARD)
--                   STORE STATUS HISTORY (CONTEXTUAL STORE DAY SEARCH)
--                   STORE STATUS HISTORY (CONTEXTUAL STORE DAY SUMMARY)
--------------------------------------------------------------------------------
FUNCTION STORE_DAY_STATUS_HIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_result        IN OUT RESA_OI_STORE_STATUS_TBL,
                               I_store         IN     SA_STORE_DAY.STORE%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : LATE_POLLING_STORES
--Purpose       : This function retrieves the count of late polling stores for
--                the past 7 days.
--                To be used by the following reports:
--                   LATE POLLING STORES (CURRENT MONTH)
--                   LATE POLLING STORES (LAST MONTH)
--------------------------------------------------------------------------------
FUNCTION LATE_POLLING_STORES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result        IN OUT RESA_OI_LATE_POLLING_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RESA_OI_STORE_SQL;
/