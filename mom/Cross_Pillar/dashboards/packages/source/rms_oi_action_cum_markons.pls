CREATE OR REPLACE PACKAGE RMS_OI_ACTION_CUM_MARKON AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION UPDATE_DEPT_BUD_INT_PCT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_dept          IN     ORDHEAD.DEPT%TYPE,
                                 I_bud_int_pct   IN     DEPS.BUD_INT%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END RMS_OI_ACTION_CUM_MARKON;
/
