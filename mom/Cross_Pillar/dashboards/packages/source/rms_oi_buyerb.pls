create or replace PACKAGE BODY RMS_OI_BUYER AS
--------------------------------------------------------------------------------
--PRIVATE FUNCTIONS
--------------------------------------------------------------------------------
FUNCTION GET_OTB_EOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_eow_dates        OUT RMS_OI_END_OF_WEEK_DATES_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POP_TODAYS_SALES_TMP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POP_WEEK_TO_DATE_SALES_TMP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SALES_AND_MARGIN(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_total_result               OUT RMS_OI_BUYER_SALES_REC,
                          O_top_10_by_sales_result     OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                          O_top_10_by_margin_result    OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                          I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses              IN     OBJ_NUMERIC_ID_TABLE,
                          I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                          I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_brands                  IN     OBJ_VARCHAR_ID_TABLE,
                          I_origin_countries        IN     OBJ_VARCHAR_ID_TABLE,
                          I_date_range_ind          IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION GET_OTB_EOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_eow_dates        OUT RMS_OI_END_OF_WEEK_DATES_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_BUYER.GET_OTB_EOW';
   L_dd          NUMBER(2);
   L_mm          NUMBER(2);
   L_yyyy        NUMBER(4);
   L_ret_code    VARCHAR2(5);
   L_vdate       DATE := get_vdate;
   L_eow         DATE;

BEGIN

   L_dd   := TO_NUMBER(TO_CHAR(L_vdate, 'DD'),'09');
   L_mm   := TO_NUMBER(TO_CHAR(L_vdate, 'MM'), '09');
   L_yyyy := TO_NUMBER(TO_CHAR(L_vdate, 'YYYY'), '0999');
   ---
   CAL_TO_454_LDOW(L_dd,
                   L_mm,
                   L_yyyy,
                   L_dd,
                   L_mm,
                   L_yyyy,
                   L_ret_code,
                   O_error_message);
   --
   if L_ret_code = 'FALSE' then
       return OI_UTILITY.FAILURE;
   end if;
   ---
   L_eow := TO_DATE((TO_CHAR(L_dd)||'-'||
                         TO_CHAR(L_mm)||'-'||
                         TO_CHAR(L_yyyy)),'DD-MM-YYYY');
   ---
   O_eow_dates := RMS_OI_END_OF_WEEK_DATES_TBL(L_eow,
                                               L_eow + 7,
                                               L_eow + 14,
                                               L_eow + 21,
                                               L_eow + 28,
                                               L_eow + 35,
                                               L_eow + 42,
                                               L_eow + 49);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_OTB_EOW;
--------------------------------------------------------------------------------
FUNCTION POP_TODAYS_SALES_TMP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_BUYER.POP_TODAYS_SALES_TMP';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   O_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_tran_code           TRAN_DATA.TRAN_CODE%TYPE;
   L_vdate               PERIOD.VDATE%TYPE := GET_VDATE();

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   delete from rms_oi_buyer_sales_gtt;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                            O_system_options_row) = FALSE then
      return OI_UTILITY.FAILURE;
   end if;

   --use Net Sales VAT Exclusive (tran code 2) when stkldgr_vat_incl_retl_ind = 'Y'; otherwise use Net Sales (tran code 1)
   if O_system_options_row.stkldgr_vat_incl_retl_ind = 'Y' then
      L_tran_code := 2;
   else
      L_tran_code := 1;
   end if;

   insert into rms_oi_buyer_sales_gtt(session_id,
                                      item, --holds either item or item_parent
                                      item_desc,  --holds either item_desc or item_parent_desc
                                      agg_diff_1,
                                      agg_diff_2,
                                      agg_diff_3,
                                      agg_diff_4,
                                      agg_diff_1_desc,
                                      agg_diff_2_desc,
                                      agg_diff_3_desc,
                                      agg_diff_4_desc,
                                      total_retail_prim,
                                      total_cost_prim,
                                      total_margin_prim,
                                      prim_currency_code,
                                      sales_units,
                                      standard_uom)
      with items as (select varchar2_1 item,
                            varchar2_2 item_parent,
                            varchar2_3 item_desc,
                            varchar2_4 alc_item_type,
                            varchar2_5 agg_diff_1,
                            varchar2_6 agg_diff_2,
                            varchar2_7 agg_diff_3,
                            varchar2_8 agg_diff_4,
                            varchar2_9 item_parent_desc,
                            varchar2_10 standard_uom,
                            number_1 dept,
                            number_2 class,
                            number_3 subclass
                       from gtt_10_num_10_str_10_date),
          stores as (select s.store,
                            s.currency_code
                       from gtt_num_num_str_str_date_date g,
                            store s
                      where s.store = g.number_1
                        and rownum > 0),
          curr_conv as (select distinct so.currency_code,
                                        mv.from_currency,
                                        first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency order by mv.exchange_type asc, mv.effective_date desc) as exchange_rate
                                   from system_config_options so,
                                        mv_currency_conversion_rates mv
                                  where mv.to_currency = so.currency_code
                                    and (so.consolidation_ind = 'Y' and mv.exchange_type in ('C', 'O')
                                      or so.consolidation_ind = 'N' and mv.exchange_type = 'O')
                                    and mv.effective_date <= L_vdate
                                    and rownum > 0),
          --tran_data total_cost and total_retail converted to primary currency
          sales_prim as (select /*+ leading(it) */ it.item,
                                it.item_parent,
                                it.item_desc,
                                it.item_parent_desc,
                                it.agg_diff_1,
                                it.agg_diff_2,
                                it.agg_diff_3,
                                it.agg_diff_4,
                                it.standard_uom,
                                st.store,
                                --total_cost converted to primary currency
                                case when st.currency_code = mv.currency_code or NVL(td.total_cost, 0) = 0 then
                                   NVL(td.total_cost, 0)
                                else
                                   td.total_cost * mv.exchange_rate
                                end total_cost_prim, 
                                --total_retail converted to primary currency
                                case when st.currency_code = mv.currency_code or NVL(td.total_retail, 0) = 0 then
                                    NVL(td.total_retail, 0)
                                else
                                    td.total_retail * mv.exchange_rate
                                end total_retail_prim,
                                mv.currency_code prim_currency_code,
                                td.units 
                           from items it,
                                tran_data td,
                                stores st,
                                curr_conv mv
                          where td.item = it.item
                            and td.dept = it.dept
                            and td.class = it.class
                            and td.subclass = it.subclass
                            and td.location = st.store
                            and td.tran_code = L_tran_code
                            and td.tran_date <= L_vdate
                            and mv.from_currency = st.currency_code)
        --total_cost, total_retail (sales), margin (total_retail - total_cost) summed up by location
        --For tran-level items without parent and tran-level items with parent but no diff aggregation,
        --keep the sales and margin at item level; for tran-level items with parent and at least one diff
        --is aggregated, sum up sales and margin by parent/aggregated diff.
        select distinct I_session_id session_id,
               sp.item,
               sp.item_desc,
               null agg_diff_1,
               null agg_diff_2,
               null agg_diff_3,
               null agg_diff_4,
               null agg_diff_1_desc,
               null agg_diff_2_desc,
               null agg_diff_3_desc,
               null agg_diff_4_desc,
               sum(NVL(sp.total_retail_prim, 0)) over (partition by sp.item) total_retail_prim,
               sum(NVL(sp.total_cost_prim, 0)) over (partition by sp.item) total_cost_prim,
               sum(NVL(sp.total_retail_prim, 0) - NVL(sp.total_cost_prim, 0)) over (partition by sp.item) total_margin_prim,
               sp.prim_currency_code,
               sum(NVL(sp.units, 0)) over (partition by sp.item) sales_units,
               sp.standard_uom
          from sales_prim sp
         where sp.agg_diff_1 is NULL
           and sp.agg_diff_2 is NULL
           and sp.agg_diff_3 is NULL
           and sp.agg_diff_4 is NULL
         union all
        select distinct I_session_id session_id,
               sp.item_parent,
               sp.item_parent_desc,
               sp.agg_diff_1,
               sp.agg_diff_2,
               sp.agg_diff_3,
               sp.agg_diff_4,
               (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_1 = d.diff_id), --agg_diff_1_desc,
               (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_2 = d.diff_id), --agg_diff_2_desc,
               (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_3 = d.diff_id), --agg_diff_3_desc,
               (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_4 = d.diff_id), --agg_diff_4_desc,
               sum(NVL(sp.total_retail_prim, 0)) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) total_retail_prim,
               sum(NVL(sp.total_cost_prim, 0)) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) total_cost_prim,
               sum(NVL(sp.total_retail_prim, 0) - NVL(sp.total_cost_prim, 0)) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) total_margin_prim,
               sp.prim_currency_code,
               sum(NVL(sp.units, 0)) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) sales_units,
               sp.standard_uom
          from sales_prim sp
         where (sp.agg_diff_1 is NOT NULL
            or  sp.agg_diff_2 is NOT NULL
            or  sp.agg_diff_3 is NOT NULL
            or  sp.agg_diff_4 is NOT NULL);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_buyer_sales_gtt for today - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END POP_TODAYS_SALES_TMP;
--------------------------------------------------------------------------------
FUNCTION POP_WEEK_TO_DATE_SALES_TMP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_BUYER.POP_WEEK_TO_DATE_SALES_TMP';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_vdate       PERIOD.VDATE%TYPE := GET_VDATE();
   L_eow_date    ITEM_LOC_HIST.EOW_DATE%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   delete from rms_oi_buyer_sales_gtt;

   if DATES_SQL.GET_EOW_DATE(O_error_message,
                             L_eow_date,
                             L_vdate) = FALSE then
      return OI_UTILITY.FAILURE;
   end if;

   insert into rms_oi_buyer_sales_gtt(session_id,
                                      item, --holds either item or item_parent
                                      item_desc,  --holds either item_desc or item_parent_desc
                                      agg_diff_1,
                                      agg_diff_2,
                                      agg_diff_3,
                                      agg_diff_4,
                                      agg_diff_1_desc,
                                      agg_diff_2_desc,
                                      agg_diff_3_desc,
                                      agg_diff_4_desc,
                                      total_retail_prim,
                                      total_cost_prim,
                                      total_margin_prim,
                                      prim_currency_code,
                                      sales_units,
                                      standard_uom)
      with items as (select varchar2_1 item,
                            varchar2_2 item_parent,
                            varchar2_3 item_desc,
                            varchar2_4 alc_item_type,
                            varchar2_5 agg_diff_1,
                            varchar2_6 agg_diff_2,
                            varchar2_7 agg_diff_3,
                            varchar2_8 agg_diff_4,
                            varchar2_9 item_parent_desc,
                            varchar2_10 standard_uom
                       from gtt_10_num_10_str_10_date),
          stores as (select s.store,
                            s.currency_code
                       from gtt_num_num_str_str_date_date g,
                            store s
                      where s.store = g.number_1
                        and rownum > 0),
          curr_conv as (select distinct so.currency_code,
                                        mv.from_currency,
                                        first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency order by mv.exchange_type asc, mv.effective_date desc) as exchange_rate
                                   from system_config_options so,
                                        mv_currency_conversion_rates mv
                                  where mv.to_currency = so.currency_code
                                    and (so.consolidation_ind = 'Y' and mv.exchange_type in ('C', 'O')
                                      or so.consolidation_ind = 'N' and mv.exchange_type = 'O')
                                    and mv.effective_date <= L_vdate
                                    and rownum > 0),
          --item_loc_hist value and gp converted to primary currency filtered by item/stores/eow_date
          sales_prim as (select /*+ leading(it) */ distinct it.item,
                                it.item_parent,
                                it.item_desc,
                                it.item_parent_desc,
                                it.standard_uom,
                                it.agg_diff_1,
                                it.agg_diff_2,
                                it.agg_diff_3,
                                it.agg_diff_4,
                                st.store,
                                --retail value of the sales converted to primary currency
                                case when st.currency_code = mv.currency_code or NVL(ilh.value, 0) = 0 then
                                    NVL(ilh.value, 0)
                                else
                                    ilh.value * mv.exchange_rate 
                                end retail_value_prim,
                                --gross_profit of the sales converted to primary currency
                                case when st.currency_code = mv.currency_code or NVL(ilh.gp, 0) = 0 then
                                    NVL(ilh.gp, 0)
                                else
                                    ilh.gp * mv.exchange_rate 
                                end gross_profit_prim,
                                mv.currency_code prim_currency_code,
                                NVL(ilh.sales_issues,0) sales_issues
                           from item_loc_hist ilh,
                                items it,
                                stores st,
                                curr_conv mv
                          where ilh.item = it.item
                            and ilh.loc = st.store
                            and ilh.eow_date = L_eow_date
                            and mv.from_currency = st.currency_code)
        --cost of sales (retail - margin), sales (retail_value), margin (gross_profit) summed up by location and sale_type
        --For tran-level items without parent and tran-level items with parent but no diff aggregation,
        --keep the sales and margin at item level; for tran-level items with parent and at least one diff
        --is aggregated, sum up sales and margin by parent/aggregated diff.
        select v.session_id,
               v.item,
               v.item_desc,
               v.agg_diff_1,
               v.agg_diff_2,
               v.agg_diff_3,
               v.agg_diff_4,
               v.agg_diff_1_desc,
               v.agg_diff_2_desc,
               v.agg_diff_3_desc,
               v.agg_diff_4_desc,
               v.total_retail_prim,
               v.total_retail_prim - v.total_margin_prim,  --total cost of sales
               v.total_margin_prim,
               v.prim_currency_code,
               v.sales_units,
               v.standard_uom
          from (select distinct I_session_id session_id,
                       sp.item,
                       sp.item_desc,
                       null agg_diff_1,
                       null agg_diff_2,
                       null agg_diff_3,
                       null agg_diff_4,
                       null agg_diff_1_desc,
                       null agg_diff_2_desc,
                       null agg_diff_3_desc,
                       null agg_diff_4_desc,
                       sum(NVL(sp.retail_value_prim, 0)) over (partition by sp.item) total_retail_prim,
                       sum(NVL(sp.gross_profit_prim, 0)) over (partition by sp.item) total_margin_prim,
                       sp.prim_currency_code,
                       sum(sp.sales_issues) over (partition by sp.item)  sales_units,
                       sp.standard_uom
                  from sales_prim sp
                 where sp.agg_diff_1 is NULL
                   and sp.agg_diff_2 is NULL
                   and sp.agg_diff_3 is NULL
                   and sp.agg_diff_4 is NULL
                 union all
                select distinct I_session_id session_id,
                       sp.item_parent,
                       sp.item_parent_desc,
                       sp.agg_diff_1,
                       sp.agg_diff_2,
                       sp.agg_diff_3,
                       sp.agg_diff_4,
                       (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_1 = d.diff_id), --agg_diff_1_desc,
                       (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_2 = d.diff_id), --agg_diff_2_desc,
                       (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_3 = d.diff_id), --agg_diff_3_desc,
                       (select d.diff_desc from v_diff_ids_tl d where sp.agg_diff_4 = d.diff_id), --agg_diff_4_desc,
                       sum(NVL(sp.retail_value_prim, 0)) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) total_retail_prim,
                       sum(NVL(sp.gross_profit_prim, 0)) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) total_margin_prim,
                       sp.prim_currency_code,
                       sum(sp.sales_issues) over (partition by sp.item_parent, sp.agg_diff_1, sp.agg_diff_2, sp.agg_diff_3, sp.agg_diff_4) sales_units,
                       sp.standard_uom
                  from sales_prim sp
                 where (sp.agg_diff_1 is NOT NULL
                    or  sp.agg_diff_2 is NOT NULL
                    or  sp.agg_diff_3 is NOT NULL
                    or  sp.agg_diff_4 is NOT NULL)) v;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_buyer_sales_gtt for week to date - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END POP_WEEK_TO_DATE_SALES_TMP;
--------------------------------------------------------------------------------
FUNCTION SALES_AND_MARGIN(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_total_result               OUT RMS_OI_BUYER_SALES_REC,
                          O_top_10_by_sales_result     OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                          O_top_10_by_margin_result    OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                          I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses              IN     OBJ_NUMERIC_ID_TABLE,
                          I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                          I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_brands                  IN     OBJ_VARCHAR_ID_TABLE,
                          I_origin_countries        IN     OBJ_VARCHAR_ID_TABLE,
                          I_date_range_ind          IN     VARCHAR2)
RETURN NUMBER IS

   L_program              VARCHAR2(61) := 'RMS_OI_BUYER.SALES_AND_MARGIN';
   L_start_time           TIMESTAMP    := SYSTIMESTAMP;

   cursor C_TOP_10_BY_SALES is
      select RMS_OI_BUYER_TOP_TEN_ITEMS_REC(v.item,                --ITEM              VARCHAR2(25),
                                            v.item_desc,           --ITEM_DESC         VARCHAR2(250),
                                            v.agg_diff_1,          --AGG_DIFF_1        VARCHAR2(10),
                                            v.agg_diff_2,          --AGG_DIFF_2        VARCHAR2(10),
                                            v.agg_diff_3,          --AGG_DIFF_3        VARCHAR2(10),
                                            v.agg_diff_4,          --AGG_DIFF_4        VARCHAR2(10),
                                            v.agg_diff_1_desc,     --AGG_DIFF_1_DESC   VARCHAR2(120),
                                            v.agg_diff_2_desc,     --AGG_DIFF_2_DESC   VARCHAR2(120),
                                            v.agg_diff_3_desc,     --AGG_DIFF_3_DESC   VARCHAR2(120),
                                            v.agg_diff_4_desc,     --AGG_DIFF_4_DESC   VARCHAR2(120), 
                                            v.sales_units,         --SALES_UNITS       NUMBER(12,4),
                                            v.standard_uom,        --UOM               VARCHAR2(4),
                                            v.cost,                --COST_OF_SALES     NUMBER(20,4),
                                            v.sales,               --SALES             NUMBER(10),
                                            v.margin,              --MARGIN            NUMBER(10),
                                            decode(d.markup_calc_type, 'R', decode(NVL(v.sales,0), 0, 0, v.margin/v.sales*100), 
                                                                            decode(NVL(v.cost,0), 0, 0, v.margin/v.cost*100)), --MARGIN_PERCENT    NUMBER(12,4),
                                            v.prim_currency_code)  --CURRENCY_CODE     VARCHAR2(3)
        from item_master im,
             deps d,
             (select * 
                from (select item, 
                             item_desc,
                             agg_diff_1,
                             agg_diff_2,
                             agg_diff_3,
                             agg_diff_4,
                             agg_diff_1_desc,
                             agg_diff_2_desc,
                             agg_diff_3_desc,
                             agg_diff_4_desc,
                             total_cost_prim cost,
                             total_retail_prim sales,
                             total_margin_prim margin,
                             prim_currency_code,
                             sales_units,
                             standard_uom,
                             row_number() over (order by total_retail_prim desc) sales_rank
                        from rms_oi_buyer_sales_gtt) s
               where s.sales_rank <= 10) v
       where v.item = im.item
         and im.dept = d.dept
       order by v.sales desc;     

   cursor C_TOP_10_BY_MARGIN is
      select RMS_OI_BUYER_TOP_TEN_ITEMS_REC(v.item,                --ITEM              VARCHAR2(25),
                                            v.item_desc,           --ITEM_DESC         VARCHAR2(250),
                                            v.agg_diff_1,          --AGG_DIFF_1        VARCHAR2(10),
                                            v.agg_diff_2,          --AGG_DIFF_2        VARCHAR2(10),
                                            v.agg_diff_3,          --AGG_DIFF_3        VARCHAR2(10),
                                            v.agg_diff_4,          --AGG_DIFF_4        VARCHAR2(10),
                                            v.agg_diff_1_desc,     --AGG_DIFF_1_DESC   VARCHAR2(120),
                                            v.agg_diff_2_desc,     --AGG_DIFF_2_DESC   VARCHAR2(120),
                                            v.agg_diff_3_desc,     --AGG_DIFF_3_DESC   VARCHAR2(120),
                                            v.agg_diff_4_desc,     --AGG_DIFF_4_DESC   VARCHAR2(120), 
                                            v.sales_units,         --SALES_UNITS       NUMBER(12,4),
                                            v.standard_uom,        --UOM               VARCHAR2(4),
                                            v.cost,                --COST_OF_SALES     NUMBER(20,4),
                                            v.sales,               --SALES             NUMBER(10),
                                            v.margin,              --MARGIN            NUMBER(10),
                                            decode(d.markup_calc_type, 'R', decode(NVL(v.sales,0), 0, 0, v.margin/v.sales*100), 
                                                                            decode(NVL(v.cost,0), 0, 0, v.margin/v.cost*100)), --MARGIN_PERCENT    NUMBER(12,4),
                                            v.prim_currency_code)  --CURRENCY_CODE     VARCHAR2(3)
        from item_master im,
             deps d,
             (select * 
                from (select item, 
                             item_desc,
                             agg_diff_1,
                             agg_diff_2,
                             agg_diff_3,
                             agg_diff_4,
                             agg_diff_1_desc,
                             agg_diff_2_desc,
                             agg_diff_3_desc,
                             agg_diff_4_desc,
                             total_cost_prim cost,
                             total_retail_prim sales,
                             total_margin_prim margin,
                             prim_currency_code,
                             sales_units,
                             standard_uom,
                             row_number() over (order by total_margin_prim desc) margin_rank
                        from rms_oi_buyer_sales_gtt) s
               where s.margin_rank <= 10) v
       where v.item = im.item
         and im.dept = d.dept
       order by v.margin desc;     

   cursor C_TOTAL is
      select RMS_OI_BUYER_SALES_REC(t.total_sales,                             --TOTAL_SALES            NUMBER(20,4),
                                    decode(NVL(t.total_sales, 0), 0, 0, s.top_10_sales/t.total_sales*100),  --TOP_TEN_SALES_SHARE    NUMBER(12,4),    
                                    t.total_margin,                            --TOTAL_MARGIN           NUMBER(20,4),
                                    decode(NVL(t.total_margin, 0), 0, 0, m.top_10_margin/t.total_margin*100), --TOP_TEN_MARGIN_SHARE   NUMBER(12,4),
                                    so.currency_code)                          --CURRENCY_CODE          VARCHAR2(3)
        from --total
             (select sum(total_retail_prim) total_sales,
                     sum(total_margin_prim) total_margin
                from rms_oi_buyer_sales_gtt) t,
             --top_10_sales          
             (select sum(total_retail_prim) top_10_sales
                from (select total_retail_prim,
                             row_number() over (order by total_retail_prim desc) sales_rank
                        from rms_oi_buyer_sales_gtt) s
                       where s.sales_rank <= 10) s,
             --top_10_margin                
             (select sum(total_margin_prim) top_10_margin
                from (select total_margin_prim,
                             row_number() over (order by total_margin_prim desc) margin_rank
                        from rms_oi_buyer_sales_gtt) s
                       where s.margin_rank <= 10) m,
             system_config_options so;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --insert a list of applicable stores to gtt_num_num_str_str_date_date
   --gtt_num_num_str_str_date_date
   --  number_1 -- store
   --  varchar2_1 -- area or store_grade (not used)
   --  varchar2_2 -- area_name or store_grade (not used)
   if I_stores is NOT NULL and I_stores.COUNT > 0 then
      if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                    I_session_id,
                                    NULL, --I_chains,
                                    NULL, --I_areas,
                                    I_stores) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;
   else
      -- no store is selected, insert all stores to gtt
      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      select v.store,
             v.store,
             v.store_name
        from v_store v;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date all stores - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   --insert a list of applicable tran-level items to gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   --  varchar2_9 -- item_parent_desc
   --  varchar2_10 --standard_uom
   --  number_1    --dept
   --  number_2    --class
   --  number_3    --subclass
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             I_origin_countries,
                             I_brands,
                             NULL) = OI_UTILITY.FAILURE then   --I_forecast_ind
      return OI_UTILITY.FAILURE;
   end if;

   --Populate RMS_OI_BUYER_SALES_GTT with sales and margin data converted
   --to primary currency, and filtered and rolled up at the right level
   if (I_date_range_ind = 'T') then
      if POP_TODAYS_SALES_TMP(O_error_message,
                              I_session_id) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
   end if;
   else --week to date
      if POP_WEEK_TO_DATE_SALES_TMP(O_error_message,
                                    I_session_id) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;
   end if;

   -- Populate object for TOP 10 by Sales
   open C_TOP_10_BY_SALES;
   fetch C_TOP_10_BY_SALES bulk collect into O_top_10_by_sales_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor C_TOP_10_BY_SALES - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close C_TOP_10_BY_SALES;

   -- Populate object for TOP 10 by Margin
   open C_TOP_10_BY_MARGIN;
   fetch C_TOP_10_BY_MARGIN bulk collect into O_top_10_by_margin_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor C_TOP_10_BY_MARGIN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close C_TOP_10_BY_MARGIN;

   -- Populate object for total
   open C_TOTAL;
   fetch C_TOTAL into O_total_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor C_TOTAL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close C_TOTAL;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SALES_AND_MARGIN;
--------------------------------------------------------------------------------
FUNCTION TODAY_SALES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_total_result               OUT RMS_OI_BUYER_SALES_REC,
                     O_top_10_by_sales_result     OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                     O_top_10_by_margin_result    OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                     I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                     I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                     I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                     I_subclasses              IN     OBJ_NUMERIC_ID_TABLE,
                     I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                     I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                     I_brands                  IN     OBJ_VARCHAR_ID_TABLE,
                     I_origin_countries        IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'RMS_OI_BUYER.TODAY_SALES';
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;
   L_date_range_ind   VARCHAR2(1) := 'T';  --for today

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if SALES_AND_MARGIN(O_error_message,
                       O_total_result,
                       O_top_10_by_sales_result,
                       O_top_10_by_margin_result,
                       I_session_id,
                       I_depts,
                       I_classes,
                       I_subclasses,
                       I_supplier_sites,
                       I_stores,
                       I_brands,
                       I_origin_countries,
                       L_date_range_ind) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END TODAY_SALES;
--------------------------------------------------------------------------------
FUNCTION WEEK_TO_DATE_SALES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_total_result               OUT RMS_OI_BUYER_SALES_REC,
                            O_top_10_by_sales_result     OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                            O_top_10_by_margin_result    OUT RMS_OI_BUYER_TOP_TEN_ITEMS_TBL,
                            I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                            I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                            I_subclasses              IN     OBJ_NUMERIC_ID_TABLE,
                            I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                            I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                            I_brands                  IN     OBJ_VARCHAR_ID_TABLE,
                            I_origin_countries        IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.WEEK_TO_DATE_SALES';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_date_range_ind      VARCHAR2(1) := 'W';  --for week to date

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if SALES_AND_MARGIN(O_error_message,
                       O_total_result,
                       O_top_10_by_sales_result,
                       O_top_10_by_margin_result,
                       I_session_id,
                       I_depts,
                       I_classes,
                       I_subclasses,
                       I_supplier_sites,
                       I_stores,
                       I_brands,
                       I_origin_countries,
                       L_date_range_ind) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END WEEK_TO_DATE_SALES;
--------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result                OUT RMS_OI_BUYER_ITEM_PANE_REC,
                         I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_item               IN     ITEM_MASTER.ITEM%TYPE,
                         I_agg_diff_1         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_agg_diff_2         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_agg_diff_3         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_agg_diff_4         IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_supplier_sites     IN     OBJ_NUMERIC_ID_TABLE,
                         I_stores             IN     OBJ_NUMERIC_ID_TABLE,
                         I_origin_countries   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.GET_ITEM_DETAIL';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   
   L_supplier_site       SUPS.SUPPLIER%TYPE := null;
   L_origin_country      COUNTRY.COUNTRY_ID%TYPE := null;
   L_store               STORE.STORE%TYPE := null;
   L_image_addr          ITEM_IMAGE.IMAGE_ADDR%TYPE := null;
   L_rpm_ind             VARCHAR2(1);
   L_currency_code       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   --   
   cursor C_RPM_IND  is 
     select RPM_IND 
        from 
       system_options;
   
   cursor C_CURRENCY_CODE is  
     select currency_code from system_options;      
  
   cursor C_ITEM_IMAGE is
      select image_addr || image_name
        from item_image
       where item = I_item
         and (primary_ind = 'Y' or display_priority = 1);

   cursor C_ITEM_IMAGE_PARENT_DIFF is
      select ii.image_addr || ii.image_name
        from item_image ii,
             item_master im,
             item_master im_parent
       where im.item_parent = I_item
         and im.item_parent = im_parent.item
         and (   im_parent.diff_1_aggregate_ind = 'N'
              or 
                 (    im_parent.diff_1_aggregate_ind = 'Y'
                  and NVL(im.diff_1, '-1') = NVL(I_agg_diff_1, NVL(im.diff_1, '-1'))))
         and (   im_parent.diff_2_aggregate_ind = 'N'
              or 
                 (    im_parent.diff_2_aggregate_ind = 'Y'
                  and NVL(im.diff_2, '-1') = NVL(I_agg_diff_2, NVL(im.diff_2, '-1'))))
         and (   im_parent.diff_3_aggregate_ind = 'N'
              or 
                 (    im_parent.diff_3_aggregate_ind = 'Y'
                  and NVL(im.diff_3, '-1') = NVL(I_agg_diff_3, NVL(im.diff_3, '-1'))))
         and (   im_parent.diff_4_aggregate_ind = 'N'
              or 
                 (    im_parent.diff_4_aggregate_ind = 'Y'
                  and NVL(im.diff_4, '-1') = NVL(I_agg_diff_4, NVL(im.diff_4, '-1'))))
         and ii.item = im.item
         and (ii.primary_ind = 'Y' or ii.display_priority = 1)
         and rownum = 1;

 cursor C_GET_ITEM is
   with vpn as (select s.supplier,
                       t.sup_name,
                       s.vpn 
                  from item_supplier s,
                       v_sups_tl t
                 where s.item = I_item
                   and s.supplier = t.supplier
                   and (L_supplier_site is NULL and s.primary_supp_ind = 'Y'
                    or L_supplier_site is NOT NULL and s.supplier = L_supplier_site)),
        items as (select item,
                         dept,
                         class,
                         subclass,
                                     curr_selling_unit_retail 
                    from item_master
                   where item = I_item
                     and I_agg_diff_1 is NULL
                     and I_agg_diff_2 is NULL
                     and I_agg_diff_3 is NULL
                     and I_agg_diff_4 is NULL
                   union all
                  select item,
                         dept,
                         class,
                         subclass,
                         curr_selling_unit_retail
                    from item_master 
                   where (I_agg_diff_1 is NOT NULL
                      or  I_agg_diff_2 is NOT NULL
                      or  I_agg_diff_3 is NOT NULL
                      or  I_agg_diff_4 is NOT NULL)
                     and item_parent = I_item
                     and NVL(diff_1, '-1') = NVL(I_agg_diff_1, NVL(diff_1, '-1'))
                     and NVL(diff_2, '-1') = NVL(I_agg_diff_2, NVL(diff_2, '-1'))
                     and NVL(diff_3, '-1') = NVL(I_agg_diff_3, NVL(diff_3, '-1'))
                     and NVL(diff_4, '-1') = NVL(I_agg_diff_4, NVL(diff_4, '-1'))),
        --For items aggregrated to parent/diff, display the max unit_cost and min unit_retail
        --among items in the parent/aggregate group to show the smallest margin.
        cost as (select MAX(iscl.unit_cost) unit_cost,
                        min(s.currency_code) currency_code
                   from item_supp_country_loc iscl,
                        item_supp_country isc,
                        item_supplier isup,
                        sups s,
                        items
                  where isup.supplier = s.supplier
                    and isup.item = items.item
                    and isc.item = isup.item
                    and isc.supplier = isup.supplier
                    and iscl.item = isc.item
                    and iscl.supplier = isc.supplier
                    and iscl.origin_country_id = isc.origin_country_id
                    and (L_supplier_site is NULL and isup.primary_supp_ind = 'Y'
                     or L_supplier_site is NOT NULL and isup.supplier = L_supplier_site)
                    and (L_origin_country is NULL and isc.primary_country_ind = 'Y'
                     or L_origin_country is NOT NULL and isc.origin_country_id = L_origin_country)
                    and (L_store is NULL and iscl.primary_loc_ind = 'Y'
                     or L_store is NOT NULL and iscl.loc = L_store)),
        retail as (select MIN(il.unit_retail) unit_retail,
                          min(s.currency_code) currency_code
                     from item_loc il,
                          store s,
                          items
                    where L_store is NOT NULL   --single store, get unit_retail from item_loc
                      and s.store = L_store
                      and il.item = items.item
                      and il.loc = s.store
                    union all
                   select MIN(p.standard_retail) unit_retail,
                          min(p.standard_retail_currency) currency_code 
                     from rpm_item_zone_price p, 
                          rpm_zone z, 
                          rpm_zone_group g,
                          rpm_merch_retail_def_expl d,
                          items
                    where L_rpm_ind ='Y'
                      and L_store is NULL  --no store or multiple stores, get unit_retail from rpm_item_zone_price
                      and items.dept = d.dept
                      and items.class = d.class
                      and items.subclass = d.subclass
                      and g.zone_group_id = d.regular_zone_group
                      and g.zone_group_id = z.zone_group_id
                      and z.base_ind = '1'
                      and p.zone_id = z.zone_id
                      and p.item = items.item
                    union all
                     select items.curr_selling_unit_retail    unit_retail,
                            L_currency_code             currency_code
                     from   items				 
                    where   L_rpm_ind = 'N'
                      and   L_store is null)
      select RMS_OI_BUYER_ITEM_PANE_REC(I_item,          --ITEM,
                                        d.supplier,      --SUPPLIER,
                                        d.sup_name,      --SUPPLIER_NAME,
                                        d.vpn,           --VPN
                                        d.unit_cost,     --UNIT_COST
                                        d.unit_retail,   --UNIT_RETAIL
                                        d.currency_code, --CURRENCY_CODE,
                                        L_image_addr)    --ITEM_IMAGE
        from (select distinct v.supplier,
                     v.sup_name,
                     v.vpn,
                     case when c.currency_code = so.currency_code or NVL(c.unit_cost,0) = 0 then
                        c.unit_cost
                     else
                        c.unit_cost * first_value(mvc.exchange_rate) over (partition by mvc.from_currency, mvc.to_currency order by mvc.exchange_type asc, mvc.effective_date desc)
                     end unit_cost,
                     case when r.currency_code = so.currency_code or NVL(r.unit_retail,0) = 0 then
                        r.unit_retail
                     else
                        r.unit_retail * first_value(mvr.exchange_rate) over (partition by mvr.from_currency, mvr.to_currency order by mvr.exchange_type asc, mvr.effective_date desc)
                     end unit_retail,
                     so.currency_code
               from vpn v,
                    cost c,
                    retail r,
                    system_config_options so,
                    period p,
                    mv_currency_conversion_rates mvc,
                    mv_currency_conversion_rates mvr
              where mvc.from_currency = c.currency_code
                and mvc.to_currency = so.currency_code
                and (so.consolidation_ind = 'Y' and mvc.exchange_type in ('C', 'O')
                 or so.consolidation_ind = 'N' and mvc.exchange_type = 'O')
                and mvc.effective_date <= p.vdate
                and mvr.from_currency = r.currency_code
                and mvr.to_currency = so.currency_code
                and (so.consolidation_ind = 'Y' and mvr.exchange_type in ('C', 'O')
                 or so.consolidation_ind = 'N' and mvr.exchange_type = 'O')
                and mvr.effective_date <= p.vdate) d;

   cursor C_DEFAULT_ITEM_IMAGE is
      select TRIM(image_path) || TRIM(default_item_image)
        from system_options;


BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --if one and only one supplier is entered in the filter criteria, 
   --use that supplier to fetch VPN and supplier name
   if (I_supplier_sites is NOT NULL and I_supplier_sites.COUNT = 1) then
      L_supplier_site := I_supplier_sites(1);
   end if;

   --if one and only one country is entered in the filter criteria, 
   --use that country to fetch cost
   if (I_origin_countries is NOT NULL and I_origin_countries.COUNT = 1) then
      L_origin_country := I_origin_countries(1);
   end if;

   --if one and only one store is entered in the filter criteria, 
   --use that store to fetch cost and retail
   if (I_stores is NOT NULL and I_stores.COUNT = 1) then
      L_store := I_stores(1);
   end if;

   if I_agg_diff_1 is NULL and I_agg_diff_2 is NULL and
      I_agg_diff_3 is NULL and I_agg_diff_4 is NULL then

      --I_item is a tran-level item if all diff aggregates are NULL.
      --Use the tran-level item to get item_image.
      open C_ITEM_IMAGE;
      fetch C_ITEM_IMAGE into L_image_addr;
      close C_ITEM_IMAGE;
   else  
      --I_item is a parent item if at least one diff aggregate is NOT NULL.
      --Fetch image of any child item matching the parent/aggregated diff combination.
      open C_ITEM_IMAGE_PARENT_DIFF;
      fetch C_ITEM_IMAGE_PARENT_DIFF into L_image_addr;
      close C_ITEM_IMAGE_PARENT_DIFF;   

      --if no image is available for any applicable child item, show
      --image of the parent item. In this case, I_item is a parent item.
      if L_image_addr is NULL then
         open C_ITEM_IMAGE;
         fetch C_ITEM_IMAGE into L_image_addr;
         close C_ITEM_IMAGE;
      end if;
   end if;
   
   --if no image is available for item, show default image set in system_options table
      if L_image_addr is NULL then
         open C_DEFAULT_ITEM_IMAGE;
         fetch C_DEFAULT_ITEM_IMAGE into L_image_addr;
         close C_DEFAULT_ITEM_IMAGE;
      end if;   

   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;
   
   open C_CURRENCY_CODE;
   fetch C_CURRENCY_CODE into L_currency_code;
   close C_CURRENCY_CODE;

   open c_get_item;
   fetch c_get_item into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close c_get_item;
   
   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END GET_ITEM_DETAIL;
--------------------------------------------------------------------------------
FUNCTION EARLY_LATE_SHIPMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_eow_dates             OUT RMS_OI_END_OF_WEEK_DATES_TBL,
                              I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                              I_depts              IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes            IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses         IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites     IN     OBJ_NUMERIC_ID_TABLE,
                              I_stores             IN     OBJ_NUMERIC_ID_TABLE,
                              I_brands             IN     OBJ_VARCHAR_ID_TABLE,
                              I_order_contexts     IN     OBJ_VARCHAR_ID_TABLE,
                              I_origin_countries   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.EARLY_LATE_SHIPMENTS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_first_eow_date      DATE;
   L_start_date          DATE;
   L_end_date            DATE;
   L_vdate               DATE         := get_vdate;
   L_move_7_days         NUMBER       := 7; --to move a week 
   L_move_6_days         NUMBER       := 6; --to move to start of week
   L_supp_list           VARCHAR2(1)  := 'N';
   L_store_list          VARCHAR2(1)  := 'N';
   L_order_context_list  VARCHAR2(1)  := 'N';
   L_countries_list      VARCHAR2(1)  := 'N';
      
   cursor c_start_end_8_weeks is
     select min(value(input_eow))-L_move_7_days,
            max(value(input_eow))
       from table(cast(O_eow_dates as RMS_OI_END_OF_WEEK_DATES_TBL)) input_eow;
   
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   
   if I_supplier_sites is not null and I_supplier_sites.count > 0 then
      L_supp_list := 'Y';
   end if;
   --
   if I_order_contexts is not null and I_order_contexts.count > 0 then
      L_order_context_list := 'Y';
   end if;
   --
   if I_origin_countries is not null and I_origin_countries.count > 0 then
      L_countries_list := 'Y';
   end if;
   --
   
   if GET_OTB_EOW(O_error_message,
                  O_eow_dates) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   -- 
   open c_start_end_8_weeks;
   fetch c_start_end_8_weeks into L_first_eow_date,
                                  L_end_date;
   close c_start_end_8_weeks;
   -- go to the first day of the first week
   L_start_date := L_first_eow_date -6;
   ---
   delete RMS_OI_BUYER_EARLY_LATE_SHIP
    where session_id = I_session_id;
   --
   if I_stores is not null and I_stores.count > 0 then
      L_store_list := 'Y';
      --      
      --insert a list of applicable stores to gtt_num_num_str_str_date_date
      --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 NULL, --I_chains,
                                 NULL, --I_areas,
                                 I_stores) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;
   end if;
   --
   --insert a list of applicable tran-level items to gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent      (not used)
   --  varchar2_3 -- item_desc        (not used)
   --  varchar2_4 -- alc_item_type    (not used)
   --  varchar2_5 -- agg_diff_1       (not used)
   --  varchar2_6 -- agg_diff_2       (not used)
   --  varchar2_7 -- agg_diff_3       (not used)
   --  varchar2_8 -- agg_diff_4       (not used)
   --  varchar2_9 -- item_parent_desc (not used)
   --  varchar2_10 --standard_uom     (not used)
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             I_origin_countries,
                             I_brands,
                             NULL) = OI_UTILITY.FAILURE then   --I_forecast_ind
      return OI_UTILITY.FAILURE;
   end if;
   ---
   insert into rms_oi_buyer_early_late_ship(session_id,
                                            order_no,
                                            sup_name,
                                            not_before_date,
                                            not_after_date,
                                            est_arr_date,
                                            otb_eow_date,
                                            shipment_issue)
                                     select I_session_id,
                                            order_no,
                                            vs.sup_name,
                                            not_before_date,
                                            not_after_date,
                                            est_arr_date,
                                            otb_eow_date,
                                            shipment_issue 
                                       from (select order_no,
                                                    supplier,
                                                    not_before_date,
                                                    not_after_date,
                                                    est_arr_date,
                                                    otb_eow_date,
                                                    shipment_issue,
                                                                      row_number() over (partition by order_no  
                                                                           order by est_arr_date) earlist_shipment
                                                                   from (select order_no,
                                                            supplier,
                                                            not_before_date,
                                                            not_after_date,
                                                            est_arr_date,
                                                            otb_eow_date,
                                                            case when est_arr_date < not_before_date then 'ESHIP'
                                                                 when est_arr_date > not_after_date then 'LSHIP'
                                                                 when est_arr_date is null then
                                                                      case when not_after_date < (L_vdate + max_lead_time) then 'ASNNR'
                                                                           when not_after_date - (otb_eow_date-L_move_7_days) <= sysop.b_num_days_nad_eow then 'OTBIN'
                                                                           when not_after_date between otb_eow_date-sysop.b_num_days_nad_eow and otb_eow_date then 'OTBOUT'
                                                                      end --end case when est_arr_date is null
                                                                 when est_arr_date between otb_eow_date-L_move_7_days-L_move_6_days and otb_eow_date-L_move_7_days then 'OTBIN' --in the week prior to the week of OTB EOW date
                                                                 when est_arr_date between otb_eow_date-L_move_6_days-sysop.b_num_days_bow_ead and otb_eow_date-L_move_6_days then 'OTBIN'
                                                                 when est_arr_date > otb_eow_date then 'OTBOUT'
                                                                 when est_arr_date between otb_eow_date-sysop.b_num_days_ead_otb and otb_eow_date then 'OTBOUT'
                                                            else null end shipment_issue
                                                       from (select oh.order_no,
                                                                    oh.supplier,
                                                                    oh.not_before_date,
                                                                    oh.not_after_date,
                                                                    s.est_arr_date,
                                                                    oh.otb_eow_date,
                                                                    oh.status,
                                                                    s.shipment,
                                                                    max (NVL(ipc.lead_time,0) + NVL(ipcl.pickup_lead_time,0)) 
                                                                         over (partition by oh.order_no) as max_lead_time
                                                               from ordhead oh,
                                                                     shipment s,
                                                                     ordloc ol,
                                                                     gtt_10_num_10_str_10_date item_gtt,
                                                                     item_supp_country ipc,
                                                                     item_supp_country_loc ipcl
                                                              where oh.order_type != 'CO'
                                                                and (   oh.status   = 'A' 
                                                                     or (    oh.status           in ('W','S')
                                                                         and oh.orig_approval_id is not NULL))
                                                                --
                                                                and oh.order_no = s.order_no(+)
                                                                and NVL(s.status_code,'I') in ('I','V','U')
                                                                --
                                                                and ol.order_no =  oh.order_no
                                                                and ol.item     = item_gtt.varchar2_1
                                                                --
                                                                and ipc.item     = ol.item
                                                                and ipc.supplier= oh.supplier
                                                                and (   (    L_countries_list        = 'N'
                                                                         and ipc.primary_country_ind = 'Y')
                                                                     or exists (select 'X'
                                                                                   from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_oc
                                                                                  where value(input_oc) = ipc.origin_country_id
                                                                                    and rownum = 1))
                                                                --
                                                                and ipcl.item             = ipc.item
                                                                and ipc.supplier          = ipcl.supplier
                                                                and ipc.origin_country_id = ipcl.origin_country_id
                                                                and ipcl.loc              = ol.location
                                                                --
                                                                and oh.not_after_date  between L_start_date and L_end_date
                                                                --
                                                                and (   L_supp_list = 'N'
                                                                     or exists (select 'X'
                                                                                   from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups
                                                                                  where value(input_sups) = oh.supplier
                                                                                    and rownum = 1))
                                                                --
                                                                and (   L_store_list = 'N'
                                                                     or exists (select 'X'
                                                                                   from gtt_num_num_str_str_date_date store_gtt
                                                                                  where store_gtt.number_1   = ol.location
                                                                                    and ol.loc_type = 'S'
                                                                                    and rownum = 1)
                                                                     or exists (select 'X'
                                                                                  from gtt_num_num_str_str_date_date store_gtt,
                                                                                       alloc_header ah,
                                                                                       alloc_detail ad
                                                                                  where oh.order_no         = ah.order_no
                                                                                    and ah.alloc_no         = ad.alloc_no
                                                                                    and store_gtt.number_1  = ad.to_loc
                                                                                    and ad.to_loc_type      = 'S' 
                                                                                    and rownum = 1))
                                                                --
                                                                and (   L_order_context_list = 'N'
                                                                     or exists (select 'X'
                                                                                   from table(cast(I_order_contexts as OBJ_VARCHAR_ID_TABLE)) input_type
                                                                                  where value(input_type) = oh.po_type
                                                                                    and rownum = 1))
                                                            ),
                                                            rms_oi_system_options sysop)
                                                                    where shipment_issue is not null) detail,
                                            v_sups_tl vs          
                                      where earlist_shipment = 1
                                                          and detail.supplier = vs.supplier
                                      order by est_arr_date;
                                      
   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END EARLY_LATE_SHIPMENTS;
--------------------------------------------------------------------------------
FUNCTION OTB(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
             O_result            OUT RMS_OI_BUYER_OTB_TBL,
             I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
             I_depts          IN     OBJ_NUMERIC_ID_TABLE,
             I_classes        IN     OBJ_NUMERIC_ID_TABLE,
             I_subclasses     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.OTB';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_eow_dates           RMS_OI_END_OF_WEEK_DATES_TBL;
   L_system_options_rec  SYSTEM_OPTIONS%ROWTYPE;
   
   
   cursor c_otb is
      select RMS_OI_BUYER_OTB_REC(value(input_eow),
                                  NVL(budget_amt, 0),
                                  NVL(receipts_amt, 0),
                                  NVL(approved_amt - receipts_amt, 0), --outstanding amt
                                  NVL(budget_amt - (receipts_amt + (approved_amt - receipts_amt)), 0), -- open budget
                                  L_system_options_rec.currency_code)
       from (select eow_date,
                    SUM(otb.a_budget_amt + otb.b_budget_amt + otb.n_budget_amt) budget_amt,
                    SUM(otb.a_receipts_amt + otb.b_receipts_amt + otb.n_receipts_amt) receipts_amt,
                    SUM(otb.a_approved_amt + otb.b_approved_amt + otb.n_approved_amt) approved_amt
             from otb,
                  gtt_6_num_6_str_6_date sbc
            where (otb.dept        = sbc.number_1 or otb.dept is null)
              and (otb.class       = sbc.number_2 or otb.class is null)
              and (otb.subclass    = sbc.number_3 or otb.subclass is null)
            group by eow_date) otb_data,
            table(cast(L_eow_dates as RMS_OI_END_OF_WEEK_DATES_TBL)) input_eow
             where value(input_eow) = otb_data.eow_date(+)
              order by value(input_eow);
   
   
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   
   --insert merch hierarchy into gtt_6_num_6_str_6_date
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if GET_OTB_EOW(O_error_message,
                   L_eow_dates) = OI_UTILITY.FAILURE then
       RAISE OI_UTILITY.PROGRAM_ERROR;
    end if;
   --
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   open c_otb;
   fetch c_otb bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_otb - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close c_otb;
   

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if c_otb%ISOPEN then close c_otb; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if c_otb%ISOPEN then close c_otb; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END OTB;
--------------------------------------------------------------------------------
FUNCTION ORDER_TO_APPROVE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_depts              IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes            IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses         IN     OBJ_NUMERIC_ID_TABLE,
                          I_supplier_sites     IN     OBJ_NUMERIC_ID_TABLE,
                          I_stores             IN     OBJ_NUMERIC_ID_TABLE,
                          I_brands             IN     OBJ_VARCHAR_ID_TABLE,
                          I_order_contexts     IN     OBJ_VARCHAR_ID_TABLE,
                          I_origin_countries   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.ORDER_TO_APPROVE';
   L_start_time          TIMESTAMP := SYSTIMESTAMP;

   L_approval_amt        RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   --
   delete RMS_OI_BUYER_ORDERS_TO_APPROVE
    where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete RMS_OI_BUYER_ORDERS_TO_APPROVE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --
   --insert a list of applicable orders to gtt_10_num_10_str_10_date
   --gtt_10_num_10_str_10_date
   --  number_1    -- order_no
   --  varchar2_1  -- supplier_site_name
   --  varchar2_2  -- currency_code
   --  varchar2_3  -- create_by
   --  varchar2_4  -- comment_desc
   --  date_1      -- not_before_date
   --  date_2      -- not_after_date
   --  date_3      -- otb_eow_date
   if OI_UTILITY.SETUP_ORDERS(O_error_message,
                              I_session_id,
                              I_depts,
                              I_classes,
                              I_subclasses,
                              I_supplier_sites,
                              I_brands,
                              I_order_contexts,
                              I_stores,
                              I_origin_countries) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if ORDER_APPROVE_SQL.GET_APPROVAL_AMT(O_error_message,
                                         L_approval_amt) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if (L_approval_amt = 0) then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_AUTHOR',NULL,NULL,NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   insert into RMS_OI_BUYER_ORDERS_TO_APPROVE(session_id,
                                              order_no,
                                              sup_name,
                                              not_before_date,
                                              not_after_date,
                                              otb_eow_date, 
                                              total_cost,
                                              total_retail,
                                              currency_code,
                                              create_id,
                                              comment_desc,
                                              status,
                                              status_desc,
                                              supplier,
                                              dept,
                                              dept_name,
                                              po_type,
                                              po_type_desc,
                                              master_po_no,
                                              written_date,
                                              markup_percent,
                                              total_units,
                                              multiple_uom_ind,
                                              po_last_update_datetime,
                                              po_last_update_id)
      with orders as (select number_1 order_no,
                             varchar2_1 supplier_site_name,
                             varchar2_2 currency_code,
                             varchar2_3 created_by,
                             varchar2_4 comment_desc,
                             date_1 not_before_date,
                             date_2 not_after_date,
                             date_3 otb_eow_date,
                             oh.exchange_rate,
                             oh.status,
                             type_orst.code_desc status_desc,
                             oh.supplier,
                             oh.dept,
                             d.dept_name,
                             d.markup_calc_type,
                             oh.po_type,
                             pt.po_type_desc,
                             oh.master_po_no,
                             oh.written_date,
                             oh.last_update_datetime,
                             oh.last_update_id
                        from gtt_10_num_10_str_10_date,
                             ordhead oh,
                             v_deps d,
                             v_code_detail_tl type_orst,
                             v_po_type_tl pt
                       where oh.order_no           = number_1
                         and NVL(oh.dept,-999)     = d.dept(+)
                         and type_orst.code_type   = 'ORST'
                         and type_orst.code        = oh.status
                         and NVL(oh.po_type,'-99') = pt.po_type(+)),
           --eligible ordlocs with unit_retail converted from local to primary currency
           ordlocs as (select distinct o.order_no,
                              ol.item,
                              ol.location,
                              ol.qty_ordered,
                              -- unit_cost on ORDLOC is in order currency, no conversion needed
                              ol.unit_cost unit_cost_ord,
                              -- unit_retail on ORDLOC is in local currency, convert to primary currency
                              case 
                                 when loc_currency.currency_code != so.currency_code then
                                    NVL(ol.unit_retail, 0) * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc, mv.effective_date desc) 
                                 else 
                                    NVL(ol.unit_retail,0)
                              end unit_retail_prim
                         from orders o,
                              ordloc ol,
                              item_master im,
                              (select store loc,    
                                      'S' loc_type,
                                      currency_code from store
                                union all
                               select wh loc, 
                                      'W' loc_type,
                                      currency_code from wh) loc_currency,
                              mv_currency_conversion_rates mv,
                              system_config_options so,
                              period p
                        where o.order_no = ol.order_no
                          and im.item    = ol.item
                          and ol.location = loc_currency.loc
                          and ol.loc_type = loc_currency.loc_type
                          and mv.from_currency = loc_currency.currency_code
                          and mv.to_currency = so.currency_code
                          and (so.consolidation_ind = 'Y' and mv.exchange_type in ('P', 'C', 'O')
                           or so.consolidation_ind = 'N' and mv.exchange_type in ('P', 'O'))
                          and mv.effective_date <= p.vdate),
           --eligible orders with total_cost (in order currency) and total_retail (in primary currency) rolled up at the order level
           order_view as (select o.order_no,
                                 o.supplier,
                                 o.supplier_site_name,
                                 o.not_before_date,
                                 o.not_after_date,
                                 o.otb_eow_date,
                                 sum(ol.unit_cost_ord * ol.qty_ordered) over (partition by o.order_no) total_cost_ord,
                                 sum(ol.unit_retail_prim * ol.qty_ordered) over (partition by o.order_no) total_retail_prim,
                                 sum(ol.qty_ordered) over (partition by o.order_no) total_units,
                                 o.currency_code,
                                 o.created_by,
                                 o.comment_desc,
                                 o.exchange_rate,
                                 o.status, 
                                 o.status_desc,
                                 o.dept,
                                 o.dept_name,
                                 o.markup_calc_type,
                                 o.po_type,
                                 o.po_type_desc,
                                 o.master_po_no,
                                 o.written_date,
                                 o.last_update_datetime,
                                 o.last_update_id,
                                 count(distinct im.standard_uom) over (partition by o.order_no ) uom_count,
                                 ROW_NUMBER () over (partition by o.order_no order by ol.item, ol.location) rownumb
                            from orders o,
                                 ordlocs ol,
                                 item_master im
                           where o.order_no = ol.order_no
                             and im.item    = ol.item)
      select I_session_id, 
             v.order_no,
             v.supplier_site_name,
             v.not_before_date,
             v.not_after_date,
             v.otb_eow_date,
             v.total_cost,
             v.total_retail,
             v.currency_code,
             v.created_by,
             v.comment_desc,
             v.status,
             v.status_desc,
             v.supplier,
             v.dept,
             v.dept_name,
             v.po_type,
             v.po_type_desc,
             v.master_po_no,
             v.written_date,
             CASE WHEN v.total_retail = 0 OR v.total_cost = 0 THEN
                     0
                  WHEN v.markup_calc_type = 'C' THEN
                     ROUND((v.total_retail - v.total_cost) / v.total_cost * 100, 4)
                  ELSE 
                     ROUND((v.total_retail - v.total_cost) / v.total_retail * 100, 4)
             END markup_percent,
             v.total_units,
             CASE WHEN uom_count = 1 THEN
                    'N'
                  ELSE
                    'Y'
             END multiple_uom_ind,
             v.last_update_datetime,
             v.last_update_id
        from procurement_unit_options po,
             rms_oi_system_options sp,
             (--order currency and primary currency are the same, no need to convert
              select o.order_no, 
                     o.supplier_site_name,
                     o.not_before_date,
                     o.not_after_date,
                     o.otb_eow_date,
                     o.total_cost_ord total_cost,
                     o.total_retail_prim total_retail,
                     o.total_units,
                     o.currency_code,
                     o.created_by,
                     o.comment_desc,
                     o.status,
                     o.status_desc,
                     o.supplier,
                     o.dept,
                     o.dept_name,
                     o.markup_calc_type,
                     o.po_type,
                     o.po_type_desc,
                     o.master_po_no,
                     o.written_date,
                     o.uom_count,
                     o.last_update_datetime,
                     o.last_update_id,
                     o.total_cost_ord total_cost_prim,
                     o.total_retail_prim total_retail_prim
                from order_view o,
                     system_config_options so
               where o.currency_code = so.currency_code
                 and o.rownumb = 1
               union all
              --order currency and primary currency are different, but exchange_rate is defined on order, convert based on order exchange rate
              select o.order_no, 
                     o.supplier_site_name,
                     o.not_before_date,
                     o.not_after_date,
                     o.otb_eow_date,
                     o.total_cost_ord total_cost,
                     -- convert total_retail from primary currency to order currency for display
                     o.total_retail_prim*o.exchange_rate total_retail,
                     o.total_units,
                     o.currency_code,
                     o.created_by,
                     o.comment_desc,
                     o.status,
                     o.status_desc,
                     o.supplier,
                     o.dept,
                     o.dept_name,
                     o.markup_calc_type,
                     o.po_type,
                     o.po_type_desc,
                     o.master_po_no,
                     o.written_date,
                     o.uom_count,
                     o.last_update_datetime,
                     o.last_update_id,
                     -- convert total_cost from order currency to primary currency for comparison with order approval amount (which is in primary currency)
                     o.total_cost_ord/o.exchange_rate total_cost_prim,
                     o.total_retail_prim total_retail_prim
                from order_view o,
                     system_config_options so
               where o.currency_code != so.currency_code
                 and o.exchange_rate is NOT NULL
                 and o.rownumb = 1
               union all
              --order currency and primary currency are different and exchange_rate is NOT defined on order, convert based on MV
              select distinct o.order_no, 
                     o.supplier_site_name,
                     o.not_before_date,
                     o.not_after_date,
                     o.otb_eow_date,
                     o.total_cost_ord total_cost,
                     -- convert total_retail from primary currency to order currency for display
                     o.total_retail_prim * first_value(mv2.exchange_rate) over (partition by mv2.from_currency, mv2.to_currency order by decode(mv2.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc, mv2.effective_date desc) total_retail,
                     o.total_units,
                     o.currency_code,
                     o.created_by,
                     o.comment_desc,
                     o.status,
                     o.status_desc,
                     o.supplier,
                     o.dept,
                     o.dept_name,
                     o.markup_calc_type,
                     o.po_type,
                     o.po_type_desc,
                     o.master_po_no,
                     o.written_date,
                     o.uom_count,
                     o.last_update_datetime,
                     o.last_update_id,
                     -- convert total_cost from order currency to primary currency for comparison with order approval amount (which is in primary currency)
                     NVL(o.total_cost_ord, 0) * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc, mv.effective_date desc) total_cost_prim,
                     o.total_retail_prim total_retail_prim
                from order_view o,
                     mv_currency_conversion_rates mv,  --for conversion from order to primary currency
                     mv_currency_conversion_rates mv2, --for conversion from primary to order currency
                     system_config_options so,
                     period p
               where o.currency_code != so.currency_code
                 and o.exchange_rate is NULL
                 and o.rownumb = 1
                 and mv.from_currency = o.currency_code
                 and mv.to_currency = so.currency_code
                 and (so.consolidation_ind = 'Y' and mv.exchange_type in ('P', 'C', 'O')
                  or so.consolidation_ind = 'N' and mv.exchange_type in ('P', 'O'))
                 and mv.effective_date <= p.vdate
                 and mv2.from_currency = so.currency_code
                 and mv2.to_currency = o.currency_code
                 and (so.consolidation_ind = 'Y' and mv2.exchange_type in ('P', 'C', 'O')
                  or so.consolidation_ind = 'N' and mv2.exchange_type in ('P', 'O'))
                 and mv2.effective_date <= p.vdate) v
      where po.ord_appr_amt_code = 'C' 
        and (  (sp.b_po_pending_approval_level = 'W'
                and v.status in ('S', 'W'))
            or (sp.b_po_pending_approval_level = 'S'
                and v.status                   = 'S'))
        and v.total_cost_prim <= L_approval_amt
         or po.ord_appr_amt_code = 'R'
        and v.total_retail_prim <= L_approval_amt;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END ORDER_TO_APPROVE;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.DELETE_SESSION_RECORDS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN
   
   delete from RMS_OI_BUYER_ORDERS_TO_APPROVE
    where session_id = I_session_id;
   --
   delete from RMS_OI_BUYER_EARLY_LATE_SHIP
    where session_id = I_session_id;
   --
   if OI_UTILITY.DELETE_SESSION_ID_LOG(O_error_message,
                                       I_session_id) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_RECORDS;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ORDS_TO_APRV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.DELETE_SESSION_ORDS_TO_APRV';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN
   
   delete from rms_oi_buyer_orders_to_approve
    where session_id = I_session_id;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_ORDS_TO_APRV;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_EARLY_LT_SHP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.DELETE_SESSION_EARLY_LT_SHP';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN

   delete from rms_oi_buyer_early_late_ship
    where session_id = I_session_id;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_EARLY_LT_SHP;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ORDER_TO_APPROVE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_order_ids          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.REFRESH_ORDER_TO_APPROVE';
   L_start_time          TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   --
   merge into rms_oi_buyer_orders_to_approve target
   using (select I_session_id session_id,
                 oh.order_no order_no,
                 oh.status status,
                 type_orst.code_desc status_desc,
                 oh.comment_desc comment_desc
            from ordhead oh,
                 v_code_detail_tl type_orst,
                 table(cast(I_order_ids  as OBJ_NUMERIC_ID_TABLE)) order_ids
           where oh.order_no           = value(order_ids)
             and type_orst.code_type   = 'ORST'
             and type_orst.code        = oh.status) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no)
   when matched then update
    set target.status         = use_this.status,
        target.status_desc    = use_this.status_desc,
        target.comment_desc   = use_this.comment_desc;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_buyer_orders_to_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete rms_oi_buyer_orders_to_approve
    where session_id = I_session_id
      and order_no in ( select value(order_ids) order_no
                          from table(cast(I_order_ids  as OBJ_NUMERIC_ID_TABLE)) order_ids
                         minus
                        select oh.order_no
                          from ordhead oh,
                               table(cast(I_order_ids  as OBJ_NUMERIC_ID_TABLE)) order_ids,
                               product_config_options p,
                               rms_oi_system_options sp
                         where oh.order_no   = value(order_ids)
                     and oh.wf_order_no                  is NULL
                           and (  (     sp.b_po_pending_approval_level = 'W'
                                    and oh.status                      in ('S', 'W'))
                                or (    sp.b_po_pending_approval_level = 'S'
                                    and oh.status                      = 'S'))
                           and (     p.oms_ind                         = 'N'
                                or (    p.oms_ind                      = 'Y'
                                    and oh.order_type                 != 'CO')));
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_buyer_orders_to_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END REFRESH_ORDER_TO_APPROVE;
--------------------------------------------------------------------------------
FUNCTION DELETE_EARLY_LATE_SHIPMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                     I_order_ids          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.DELETE_EARLY_LATE_SHIPMENTS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_buyer_early_late_ship
         where session_id =  I_session_id
           and order_no   in ( select value(order_ids)
                             from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids);
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_buyer_early_late_ship - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_EARLY_LATE_SHIPMENTS;
--------------------------------------------------------------------------------
FUNCTION REFRESH_EARLY_LATE_SHIPMENTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                      I_order_ids          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_BUYER.REFRESH_EARLY_LATE_SHIPMENTS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_start_date          DATE;
   L_end_date            DATE;
   L_vdate               DATE         := get_vdate;
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select d.eow_date - 6,
          d.eow_date + 49
     into L_start_date,
          L_end_date
     from day_level_calendar d
    where d.day = L_vdate;
   ---
   merge into rms_oi_buyer_early_late_ship target
   using (select I_session_id session_id,
                 oh.order_no,
                 oh.not_before_date,
                 oh.not_after_date,
                 oh.otb_eow_date
            from ordhead oh,
                 table(cast(I_order_ids  as OBJ_NUMERIC_ID_TABLE)) order_ids
           where oh.order_no = value(order_ids)
              and oh.order_type != 'CO'
              and (   oh.status   = 'A'
                   or (     oh.status           in ('W','S')
                        and oh.orig_approval_id is not NULL))
              and oh.not_after_date  between L_start_date and L_end_date
              and exists(select 1
                           from shipment s
                          where oh.order_no            = s.order_no(+)
                            and NVL(s.status_code,'I') in ('I','V','U'))) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no)
   when matched then update
    set target.not_before_date = use_this.not_before_date,
        target.not_after_date  = use_this.not_after_date,
        target.otb_eow_date    = use_this.otb_eow_date;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_buyer_early_late_ship - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_buyer_early_late_ship
         where session_id = I_session_id
           and order_no in ( select value(order_ids) ord_id
                               from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids
                              minus
                             select oh.order_no
                               from ordhead oh,
                                    table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids
                              where oh.order_no = value(order_ids)
                                and oh.order_type != 'CO'
                                and (   oh.status   = 'A'
                                     or (     oh.status           in ('W','S')
                                          and oh.orig_approval_id is not NULL))
                                and oh.not_after_date  between L_start_date and L_end_date
                                and exists(select 1
                                             from shipment s
                                            where oh.order_no            = s.order_no(+)
                                              and NVL(s.status_code,'I') in ('I','V','U')));
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_buyer_early_late_ship - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END REFRESH_EARLY_LATE_SHIPMENTS;
--------------------------------------------------------------------------------
END RMS_OI_BUYER;
/