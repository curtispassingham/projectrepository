CREATE OR REPLACE PACKAGE BODY RMS_OI_ACTION_CUM_MARKON AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
--------------------------------------------------------------------------------
FUNCTION LOCK_DEPS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_session_id      IN     oi_session_id_log.session_id%TYPE,
                   I_dept            IN     ordhead.dept%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
------------------------------------------------------------------------------------
FUNCTION UPDATE_DEPT_BUD_INT_PCT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_dept          IN     ORDHEAD.DEPT%TYPE,
                                 I_bud_int_pct   IN     DEPS.BUD_INT%TYPE)
RETURN NUMBER IS

   L_program           VARCHAR2(61)        := 'RMS_OI_ACTION_CUM_MARKON.UPDATE_DEPT_BUD_INT_PCT';
   L_start_time        TIMESTAMP           := SYSTIMESTAMP;
   L_bud_mkup          DEPS.BUD_MKUP%TYPE;
   
         
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if LOCK_DEPS(O_error_message,
                I_session_id,
                I_dept) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if I_bud_int_pct >= 100 or I_bud_int_pct <= 0 then 

      O_error_message := SQL_LIB.CREATE_MSG('COST_MARKUP_OUT', null, null, null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;

   else

      if I_bud_int_pct * 100 / (100 - I_bud_int_pct) < -99.99 or 
         I_bud_int_pct * 100 / (100 - I_bud_int_pct) > 999.999 then

         O_error_message := SQL_LIB.CREATE_MSG('COST_MARKUP_OUT', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         RETURN OI_UTILITY.FAILURE;

      else

         L_bud_mkup := round(I_bud_int_pct * 100 / (100 - I_bud_int_pct), 4);

      end if;

   end if;

   update deps
      set bud_int  = I_bud_int_pct,
          bud_mkup = L_bud_mkup
    where dept = I_dept;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END UPDATE_DEPT_BUD_INT_PCT;
--------------------------------------------------------------------------------
FUNCTION LOCK_DEPS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_session_id      IN     oi_session_id_log.session_id%TYPE,
                   I_dept            IN     ORDHEAD.DEPT%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'RMS_OI_ACTION_CUM_MARKON.LOCK_DEPS';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   L_dept         deps.dept%type;

   cursor c_lock_d is
     select d.dept
       from deps d
      where d.dept = I_dept
    for update nowait;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_lock_d;
   fetch c_lock_d into L_dept;
   close c_lock_d;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when RECORD_LOCKED then
      if c_lock_d%isopen then
         close c_lock_d;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            null,
                                            null,
                                            null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END LOCK_DEPS;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END RMS_OI_ACTION_CUM_MARKON;
/
