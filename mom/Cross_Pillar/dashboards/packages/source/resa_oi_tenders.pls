CREATE OR REPLACE PACKAGE RESA_OI_TENDER_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name : TENDER_SUMMARY
--Purpose       : This function retrieves the tender amount for the past
--                7 days for the given store.
--                To be used by the following report/s:
--                TENDER SUMMARY (CONTEXTUAL TENDER SUMMARY)
--------------------------------------------------------------------------------
FUNCTION TENDER_SUMMARY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result        IN OUT RESA_OI_TENDER_TBL,
                        I_store         IN     SA_STORE_DAY.STORE%TYPE)
RETURN NUMBER;

END RESA_OI_TENDER_SQL;
/