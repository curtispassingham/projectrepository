CREATE OR REPLACE PACKAGE BODY RMS_OI_ACTION_ITEMLOC AS
--------------------------------------------------------------------------------
FUNCTION VALIDATE_STATUS_CHANGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item             IN     ITEM_LOC.ITEM%TYPE,
                                I_loc              IN     ITEM_LOC.LOC%TYPE,
                                I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                                I_simple_pack_ind  IN     ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                                I_old_status       IN     ITEM_LOC.STATUS%TYPE,
                                I_old_ranged_ind   IN     ITEM_LOC.RANGED_IND%TYPE,
                                I_new_status       IN     ITEM_LOC.STATUS%TYPE,
                                I_new_ranged_ind   IN     ITEM_LOC.RANGED_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item             IN     ITEM_LOC.ITEM%TYPE,
                       I_loc              IN     ITEM_LOC.LOC%TYPE,
                       I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_simple_pack_ind  IN     ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                       I_old_status       IN     ITEM_LOC.STATUS%TYPE,
                       I_new_status       IN     ITEM_LOC.STATUS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result               OUT RMS_OI_ITEMLOC_STATUS_REC,
                       I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                       I_itemlocs          IN     OBJ_ITEMLOC_TBL,
                       I_new_status        IN     ITEM_LOC.STATUS%TYPE,
                       I_new_ranged_ind    IN     ITEM_LOC.RANGED_IND%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'RMS_OI_ACTION_ITEMLOC.UPDATE_STATUS';
   L_start_time      TIMESTAMP    := SYSTIMESTAMP;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_username        USERS.USER_NAME%TYPE  := GET_USER;

   TYPE rowid_tbl IS TABLE OF ROWID;
   L_il_rowids       rowid_tbl;
  
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_ITEM_LOCS is
      select il.item,
             il.loc,
             il.loc_type,
             il.status old_status,
             il.ranged_ind old_ranged_ind, 
             im.pack_ind,
             im.simple_pack_ind
        from item_master im,
             item_loc il,
             table(cast(I_itemlocs as OBJ_ITEMLOC_TBL)) input
       where im.item = il.item
         and il.item = input.item
         and il.loc = input.loc;

  TYPE itemloc_tbl is table of C_ITEM_LOCS%ROWTYPE index by BINARY_INTEGER;
  L_itemloc_tbl itemloc_tbl;

  cursor C_LOCK_ITEMLOC is
     select il.rowid
       from item_loc il,
            item_master im,
            table(cast(O_result.success_itemloc_tbl as OBJ_ITEMLOC_TBL)) input
      where (im.item = input.item
         or  im.item_parent = input.item
         or  im.item_grandparent = input.item)
        and im.tran_level >= im.item_level
        and il.item = im.item
        and il.loc = input.loc
        and (il.status != I_new_status
         or  il.ranged_ind != I_new_ranged_ind)
        for update of il.status nowait;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --nothing to do, return success
   O_result := RMS_OI_ITEMLOC_STATUS_REC(0,
                                         OBJ_ITEMLOC_TBL(),
                                         0,
                                         RMS_OI_ITEMLOC_FAIL_TBL());

   if I_itemlocs is null or I_itemlocs.count < 1 then
      return OI_UTILITY.SUCCESS;
   end if;

   --check required input
   if I_session_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_session_id',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_new_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_status',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_new_ranged_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_ranged_ind',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   --retrieve additional item/loc attributes for processing
   open C_ITEM_LOCS;
   fetch C_ITEM_LOCS bulk collect into L_itemloc_tbl;
   close C_ITEM_LOCS;

   if L_itemloc_tbl is NULL or L_itemloc_tbl.COUNT != I_itemlocs.COUNT then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_REL',
                                             NULL,
                                             NULL,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   for i in 1 .. L_itemloc_tbl.count loop
 
      SAVEPOINT itemloc_savepoint;

      if RMS_OI_ACTION_ITEMLOC.VALIDATE_STATUS_CHANGE(L_error_message,
                                                      L_itemloc_tbl(i).item,
                                                      L_itemloc_tbl(i).loc,
                                                      L_itemloc_tbl(i).loc_type,
                                                      L_itemloc_tbl(i).pack_ind,
                                                      L_itemloc_tbl(i).simple_pack_ind,
                                                      L_itemloc_tbl(i).old_status,
                                                      L_itemloc_tbl(i).old_ranged_ind,
                                                      I_new_status,
                                                      I_new_ranged_ind) = FALSE then

         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_itemloc_count := O_result.fail_itemloc_count + 1;
         O_result.fail_itemloc_tbl.extend;
         O_result.fail_itemloc_tbl(O_result.fail_itemloc_tbl.count) := RMS_OI_ITEMLOC_FAIL_REC(L_itemloc_tbl(i).item,
                                                                                               L_itemloc_tbl(i).loc,
                                                                                               L_error_message);
         ROLLBACK TO SAVEPOINT itemloc_savepoint;
         continue;
      else
         O_result.success_itemloc_count := O_result.success_itemloc_count + 1;
         O_result.success_itemloc_tbl.extend;
         O_result.success_itemloc_tbl(O_result.success_itemloc_tbl.count) := OBJ_ITEMLOC_REC(L_itemloc_tbl(i).item,
                                                                                             L_itemloc_tbl(i).loc);        
      end if;
   end loop;

   --bulk update status/ranged_ind for item/loc that are successfully processed so far
   --include child and grandchild items that are tran-level or above
   open C_LOCK_ITEMLOC;
   fetch C_LOCK_ITEMLOC bulk collect into L_il_rowids;
   close C_LOCK_ITEMLOC;

   forall i in L_il_rowids.first .. L_il_rowids.last 
      update item_loc
         set status = I_new_status,
             ranged_ind = I_new_ranged_ind,
             status_update_date = decode(status, I_new_status, status_update_date, sysdate),
             last_update_datetime = sysdate,
             last_update_id = L_username
       where rowid = L_il_rowids(i); 

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                               NULL,
                                                               NULL);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END UPDATE_STATUS;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_STATUS_CHANGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item             IN     ITEM_LOC.ITEM%TYPE,
                                I_loc              IN     ITEM_LOC.LOC%TYPE,
                                I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                                I_simple_pack_ind  IN     ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                                I_old_status       IN     ITEM_LOC.STATUS%TYPE,
                                I_old_ranged_ind   IN     ITEM_LOC.RANGED_IND%TYPE,
                                I_new_status       IN     ITEM_LOC.STATUS%TYPE,
                                I_new_ranged_ind   IN     ITEM_LOC.RANGED_IND%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'RMS_OI_ACTION_ITEMLOC.VALIDATE_STATUS_CHANGE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Item: '||I_item||' Loc: '||I_loc);

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_ind',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_simple_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_simple_pack_ind',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_old_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_old_ranged_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_ranged_ind',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- no status change, pass validation
   if I_new_status = I_old_status then
      return TRUE;
   end if;

   -- validate that the item/loc status can be changed
   if ITEM_LOC_SQL.STATUS_CHANGE_VALID(O_error_message,
                                       I_item, 
                                       I_loc,
                                       I_loc_type,
                                       I_old_status,
                                       I_new_status) = FALSE then
      return FALSE;
   end if;

   -- validate that the pack/loc status can be updated
   if I_pack_ind = 'Y' then
      if VALIDATE_PACK(O_error_message,
                       I_item,
                       I_loc,
                       I_loc_type,
                       I_simple_pack_ind,
                       I_old_status,
                       I_new_status) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_STATUS_CHANGE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item             IN     ITEM_LOC.ITEM%TYPE,
                       I_loc              IN     ITEM_LOC.LOC%TYPE,
                       I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_simple_pack_ind  IN     ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                       I_old_status       IN     ITEM_LOC.STATUS%TYPE,
                       I_new_status       IN     ITEM_LOC.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'RMS_OI_ACTION_ITEMLOC.VALIDATE_PACK';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_exists        VARCHAR2(1) := NULL;
   L_comp_item     ITEM_MASTER.ITEM%TYPE;     
   L_rowid         ROWID;

   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_pp_cost_event_tbl       OBJ_PP_COST_EVENT_TBL := OBJ_PP_COST_EVENT_TBL(OBJ_PP_COST_EVENT_REC(NULL,NULL,NULL));
   L_username                USERS.USER_NAME%TYPE  := GET_USER;

   cursor C_COMP_DEL_DISC is
      select 'x'
        from packitem pi,
             item_loc il
       where pi.pack_no = I_item
         and il.item = pi.item
         and il.loc = I_loc
         and il.status in ('D', 'C');

   cursor C_COMP_INACTIVE is
      select il.status
        from packitem pi,
             item_loc il
       where pi.pack_no = I_item
         and il.item = pi.item
         and il.loc = I_loc
         and il.status = 'I';

   cursor C_PRIMARY_COST_PACK is
      select il.item,
             il.rowid
        from packitem pi,
             item_loc il
       where pi.pack_no  = I_item
         and il.item  = pi.item
         and il.loc = I_loc
         and il.primary_cost_pack = pi.pack_no
         for update of il.item nowait; 

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Item: '||I_item||' Loc: '||I_loc);

   -- Change the pack/loc to Active status:
   if I_new_status = 'A' then

      open C_COMP_DEL_DISC;
      fetch C_COMP_DEL_DISC into L_exists;
      close C_COMP_DEL_DISC;

      --at least one of the pack component item/loc is in D (deleted) or C (discontinued) status,
      --cannot change the pack/loc to A (active) status.
      if L_exists = 'x' then
         O_error_message := SQL_LIB.CREATE_MSG('PACKITEM_DELETE_DISC',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;

      open C_COMP_INACTIVE;
      fetch C_COMP_INACTIVE into L_exists;
      close C_COMP_INACTIVE;

      --at least one of the pack component item/loc is in I (inactive) status, 
      --update component item and its child/grandchild items to 'A'ctive status
      if L_exists = 'x' then
         if ITEM_LOC_SQL.UPDATE_PACK_COMP_STATUS(O_error_message,
                                                 I_item,       --I_packitem
                                                 I_loc_type,   --I_group_type
                                                 I_loc,        --I_group_value
                                                 NULL) = FALSE then  --I_currency_code
            return FALSE;
         end if;
      end if;
   end if;

   -- Change the simple pack/loc to Deleted status:
   if I_simple_pack_ind = 'Y' and I_new_status = 'D' then

      open C_PRIMARY_COST_PACK;
      fetch C_PRIMARY_COST_PACK into L_comp_item, L_rowid;
      close C_PRIMARY_COST_PACK;

      -- simple pack is the primary_cost_pack of its component item at the location,
      -- 1) Add event to remove the primary cost pack from the future cost table,
      -- 2) Update component item/loc's primary cost pack to NULL
      if L_comp_item is NOT NULL then

         L_pp_cost_event_tbl(1).item := L_comp_item;
         L_pp_cost_event_tbl(1).location := I_loc;         
         -- Setting pack item to null will remove the primary cost pack from future cost table.
         L_pp_cost_event_tbl(1).pack_no := NULL; 

         if FUTURE_COST_EVENT_SQL.ADD_PRIMARY_PACK_COST_CHG(O_error_message,
                                                            L_cost_event_process_id,
                                                            L_pp_cost_event_tbl,
                                                            L_username) = FALSE then
            return FALSE;
         end if;  

         update item_loc
            set primary_cost_pack = NULL,
                last_update_datetime = SYSDATE,
                last_update_id = L_username
          where rowid = L_rowid;           
       end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                             'ITEM_LOC',
                                                              NULL,
                                                              NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PACK;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_ITEMLOC;
/
