CREATE OR REPLACE PACKAGE BODY RMS_OI_INV_CONTROL AS
--------------------------------------------------------------------------------
FUNCTION EXPAND_LOC_TO_WH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_chains          IN     OBJ_NUMERIC_ID_TABLE,
                          I_areas           IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_NEG_INV_LOCS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_session_id           IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_areas                IN     OBJ_NUMERIC_ID_TABLE,
                            I_stores               IN     OBJ_NUMERIC_ID_TABLE,
                            I_store_grade_group_id IN     STORE_GRADE_GROUP.STORE_GRADE_GROUP_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION NEGATIVE_INVENTORY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_item_loc_count         IN OUT NUMBER,
                            O_report_level           IN OUT VARCHAR2,
                            I_session_id             IN     oi_session_id_log.session_id%TYPE,
                            I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                            I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                            I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                            I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                            I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                            I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                            I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.NEGATIVE_INVENTORY';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_ic_neg_inv_warn            rms_oi_system_options.ic_neg_inv_warn%TYPE;
   L_ic_neg_inv_critial         rms_oi_system_options.ic_neg_inv_critial%TYPE;
   L_ic_neg_inv_tolerance_qty   rms_oi_system_options.ic_neg_inv_tolerance_qty%TYPE;

   L_min_soh                    item_loc_soh.stock_on_hand%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_neg_inv_warn,
          oi.ic_neg_inv_critial,
          oi.ic_neg_inv_tolerance_qty
     into L_ic_neg_inv_warn,
          L_ic_neg_inv_critial,
          L_ic_neg_inv_tolerance_qty
     from rms_oi_system_options oi;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,
                       I_session_id,
                       I_chains,
                       I_areas) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_inv_ctl_neg_inv where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   -------------------------------------
   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   --  number_1    -- dept
   --  number_2    -- class
   --  number_3    -- subclass
   insert into rms_oi_inv_ctl_neg_inv (session_id,
                                       item,
                                       item_desc,
                                       standard_uom,
                                       loc,
                                       loc_name,
                                       loc_type,
                                       stock_on_hand,
                                       in_progress_po_count,
                                       in_progress_tsf_count,
                                       in_progress_alloc_count)
   with item_locs as (select gttloc.number_1   loc,
                             gttloc.varchar2_1 loc_name,
                             gttloc.varchar2_2 loc_type,
                             --
                             gttitem.varchar2_1  item,
                             gttitem.varchar2_3  item_desc,
                             gttitem.varchar2_10 standard_uom,
                             gttitem.number_1    dept,
                             oid.ic_neg_inv_tolerance_qty
                        from gtt_num_num_str_str_date_date gttloc,
                             gtt_10_num_10_str_10_date gttitem,
                             rms_oi_dept_options oid
                       where gttitem.number_1 = oid.dept(+)
                         and rownum > 0
   )
   select /*+ ORDERED */ I_session_id,
          ils.item,
          item_locs.item_desc,
          item_locs.standard_uom,
          item_locs.loc,
          item_locs.loc_name,
          item_locs.loc_type,
          ils.stock_on_hand + ils.pack_comp_soh,
          0 in_progress_po_count,
          0 in_progress_tsf_count,
          0 in_progress_alloc_count
     from item_locs,
          item_loc_soh ils
    where item_locs.item = ils.item
      and item_locs.loc  = ils.loc
      and (ils.stock_on_hand + ils.pack_comp_soh) < 0 - abs(nvl(item_locs.ic_neg_inv_tolerance_qty,L_ic_neg_inv_tolerance_qty));

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ctl_neg_inv target
   using (select i.session_id, 
                 i.item,
                 i.loc,
                 sum(i.ord_cnt) in_progress_po_count,
                 sum(i.tsf_cnt) in_progress_tsf_count,
                 sum(i.alloc_cnt) in_progress_alloc_count
            from (select /*+ index(ss, pk_shipsku) */  
                         inv.session_id,
                         inv.item,
                         inv.loc,
                         sh.shipment,
                         decode(sh.order_no, null, 0, ss.qty_expected) ord_cnt,
                         decode(ss.distro_type, 'T', ss.qty_expected, 0) tsf_cnt,
                         decode(ss.distro_type, 'A', ss.qty_expected, 0) alloc_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     and inv.loc = sh.to_loc
                     and inv.loc_type = 'S'
                   union all
                   select /*+ index(ss, pk_shipsku) */  
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected ord_cnt,
                          0 tsf_cnt,
                          0 alloc_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         shipsku_loc ssl
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ss.shipment    = ssl.shipment
                     and ss.seq_no      = ssl.seq_no
                     and ss.item        = ssl.item
                     and inv.loc        = ssl.to_loc
                     and inv.loc_type   = 'W'
                 ) i
           group by i.session_id, 
                    i.item,
                    i.loc
   ) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.in_progress_po_count    = use_this.in_progress_po_count,
        target.in_progress_tsf_count   = use_this.in_progress_tsf_count,
        target.in_progress_alloc_count = use_this.in_progress_alloc_count;
        
   merge into rms_oi_inv_ctl_neg_inv target
   using (select i.session_id,
                 i.item,
                 i.loc,
                 sum(i.tsf_cnt) in_progress_tsf_count
            from (select /*+ index(ss, pk_shipsku) */
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected tsf_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         tsfitem_inv_flow tif
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ss.distro_no   = tif.tsf_no
                     and ss.seq_no      = tif.tsf_seq_no
                     and ss.item        = tif.item
                     and inv.loc        = tif.to_loc
                     and inv.loc_type   = 'W'
                   union all
                  select /*+ index(ss, pk_shipsku) */  
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected tsf_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         tsfhead th,
                         tsfdetail td 
                   where inv.session_id = I_session_id
                     and td.tsf_no      = th.tsf_no
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ss.distro_no   = td.tsf_no                 
                     and ss.item        = td.item
                     and inv.loc        = th.to_loc
                     and inv.loc_type   = 'W'
                 ) i
           group by i.session_id,
                    i.item,
                    i.loc
   ) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.in_progress_tsf_count   = use_this.in_progress_tsf_count;
    
   merge into rms_oi_inv_ctl_neg_inv target
   using (select i.session_id,
                 i.item,
                 i.loc,
                 sum(i.alloc_cnt) in_progress_alloc_count
            from (select /*+ index(ss, pk_shipsku) */
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected alloc_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         alloc_header ah,
                         alloc_detail ad
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ah.alloc_no    = ad.alloc_no
                     and ss.distro_no   = ad.alloc_no
                     and ss.item        = ah.item
                     and inv.loc        = ad.to_loc
                     and inv.loc_type   = 'W'
                 ) i
           group by i.session_id,
                    i.item,
                    i.loc
   ) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.in_progress_alloc_count = use_this.in_progress_alloc_count;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge counts rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*), 
          min(stock_on_hand) 
     into O_item_loc_count,
          L_min_soh
     from rms_oi_inv_ctl_neg_inv i 
    where i.session_id = I_session_id;

   if L_min_soh <= L_ic_neg_inv_critial then
      O_report_level := RMS_OI_INV_CONTROL.NEG_INV_CRITIAL;
   elsif L_min_soh <= L_ic_neg_inv_warn then
      O_report_level := RMS_OI_INV_CONTROL.NEG_INV_WARN;
   else
      O_report_level := RMS_OI_INV_CONTROL.NEG_INV;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END NEGATIVE_INVENTORY;
--------------------------------------------------------------------------------
FUNCTION NEGATIVE_INVENTORY_DETAIL(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                   I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                   I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                                   I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                   I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                                   I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                                   I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.NEGATIVE_INVENTORY_DETAIL';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_ic_neg_inv_tolerance_qty   rms_oi_system_options.ic_neg_inv_tolerance_qty%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_neg_inv_tolerance_qty
     into L_ic_neg_inv_tolerance_qty
     from rms_oi_system_options oi;

   --gtt_num_num_str_str_date_date
   --  number_1     -- loc
   --  varchar2_1   -- loc_name
   --  varchar2_2   -- loc_type
   if SETUP_NEG_INV_LOCS(O_error_message,
                         I_session_id,
                         I_areas,
                         I_stores,
                         I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             I_brands,
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_inv_ctl_neg_inv where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --gtt_num_num_str_str_date_date
   --  number_1   -- loc
   --  varchar2_1 -- loc_name
   --  varchar2_2 -- loc_type
   -------------------------------------
   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   --  number_1    -- dept
   --  number_2    -- class
   --  number_3    -- subclass
   insert into rms_oi_inv_ctl_neg_inv (session_id,
                                       item,
                                       item_desc,
                                       standard_uom,
                                       loc,
                                       loc_name,
                                       loc_type,
                                       stock_on_hand,
                                       in_progress_po_count,
                                       in_progress_tsf_count,
                                       in_progress_alloc_count)
   with item_locs as (select gttloc.number_1   loc,
                             gttloc.varchar2_1 loc_name,
                             gttloc.varchar2_2 loc_type,
                             --
                             gttitem.varchar2_1  item,
                             gttitem.varchar2_3  item_desc,
                             gttitem.varchar2_10 standard_uom,
                             gttitem.number_1    dept,
                             oid.ic_neg_inv_tolerance_qty
                        from gtt_num_num_str_str_date_date gttloc,
                             gtt_10_num_10_str_10_date gttitem,
                             rms_oi_dept_options oid
                       where gttitem.number_1 = oid.dept(+)
                         and rownum > 0
   )
   select /*+ ORDERED */ I_session_id,
          ils.item,
          item_locs.item_desc,
          item_locs.standard_uom,
          item_locs.loc,
          item_locs.loc_name,
          item_locs.loc_type,
          ils.stock_on_hand + ils.pack_comp_soh,
          0 in_progress_po_count,
          0 in_progress_tsf_count,
          0 in_progress_alloc_count
     from item_locs,
          item_loc_soh ils
    where item_locs.item = ils.item
      and item_locs.loc  = ils.loc
      and (ils.stock_on_hand + ils.pack_comp_soh) < nvl(item_locs.ic_neg_inv_tolerance_qty,L_ic_neg_inv_tolerance_qty);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ctl_neg_inv target
   using (select i.session_id, 
                 i.item,
                 i.loc,
                 sum(i.ord_cnt) in_progress_po_count,
                 sum(i.tsf_cnt) in_progress_tsf_count,
                 sum(i.alloc_cnt) in_progress_alloc_count
            from (select /*+ index(ss, pk_shipsku) */  
                         inv.session_id,
                         inv.item,
                         inv.loc,
                         sh.shipment,
                         decode(sh.order_no, null, 0, ss.qty_expected) ord_cnt,
                         decode(ss.distro_type, 'T', ss.qty_expected, 0) tsf_cnt,
                         decode(ss.distro_type, 'A', ss.qty_expected, 0) alloc_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     and inv.loc = sh.to_loc
                     and inv.loc_type = 'S'
                   union all
                   select /*+ index(ss, pk_shipsku) */  
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected ord_cnt,
                          0 tsf_cnt,
                          0 alloc_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         shipsku_loc ssl
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ss.shipment    = ssl.shipment
                     and ss.seq_no      = ssl.seq_no
                     and ss.item        = ssl.item
                     and inv.loc        = ssl.to_loc
                     and inv.loc_type   = 'W'
                 ) i
           group by i.session_id, 
                    i.item,
                    i.loc
   ) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.in_progress_po_count    = use_this.in_progress_po_count,
        target.in_progress_tsf_count   = use_this.in_progress_tsf_count,
        target.in_progress_alloc_count = use_this.in_progress_alloc_count;
        
   merge into rms_oi_inv_ctl_neg_inv target
   using (select i.session_id,
                 i.item,
                 i.loc,
                 sum(i.tsf_cnt) in_progress_tsf_count
            from (select /*+ index(ss, pk_shipsku) */
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected tsf_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         tsfitem_inv_flow tif
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ss.distro_no   = tif.tsf_no
                     and ss.seq_no      = tif.tsf_seq_no
                     and ss.item        = tif.item
                     and inv.loc        = tif.to_loc
                     and inv.loc_type   = 'W'
                   union all
                  select /*+ index(ss, pk_shipsku) */  
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected tsf_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         tsfhead th,
                         tsfdetail td 
                   where inv.session_id = I_session_id
                     and td.tsf_no      = th.tsf_no
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ss.distro_no   = td.tsf_no                 
                     and ss.item        = td.item
                     and inv.loc        = th.to_loc
                     and inv.loc_type   = 'W'
                 ) i
           group by i.session_id,
                    i.item,
                    i.loc
   ) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.in_progress_tsf_count   = use_this.in_progress_tsf_count;
    
   merge into rms_oi_inv_ctl_neg_inv target
   using (select i.session_id,
                 i.item,
                 i.loc,
                 sum(i.alloc_cnt) in_progress_alloc_count
            from (select /*+ index(ss, pk_shipsku) */
                          inv.session_id,
                          inv.item,
                          inv.loc,
                          sh.shipment,
                          ss.qty_expected alloc_cnt
                    from rms_oi_inv_ctl_neg_inv inv,
                         shipment sh,
                         shipsku ss,
                         alloc_header ah,
                         alloc_detail ad
                   where inv.session_id = I_session_id
                     and sh.shipment    = ss.shipment
                     and inv.item       = ss.item
                     and sh.status_code in('I','E','V','U')
                     --
                     and ah.alloc_no    = ad.alloc_no
                     and ss.distro_no   = ad.alloc_no
                     and ss.item        = ah.item
                     and inv.loc        = ad.to_loc
                     and inv.loc_type   = 'W'
                 ) i
           group by i.session_id,
                    i.item,
                    i.loc
   ) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.in_progress_alloc_count = use_this.in_progress_alloc_count;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge counts rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END NEGATIVE_INVENTORY_DETAIL;
--------------------------------------------------------------------------------
FUNCTION SETUP_NEG_INV_LOCS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_session_id           IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_areas                IN     OBJ_NUMERIC_ID_TABLE,
                            I_stores               IN     OBJ_NUMERIC_ID_TABLE,
                            I_store_grade_group_id IN     STORE_GRADE_GROUP.STORE_GRADE_GROUP_ID%TYPE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.SETUP_NEG_INV_LOCS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_NUM_NUM_STR_STR_DATE_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   delete from gtt_num_num_str_str_date_date;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location name
   --  varchar2_2 -- location type

   if I_stores is not null and I_stores.count > 0 then
      if I_areas is not null and I_areas.count > 0 then
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select v.store,
                v.store_name,
                'S'
           from table(cast(I_stores as OBJ_NUMERIC_ID_TABLE)) input_stores,
                table(cast(I_areas as OBJ_NUMERIC_ID_TABLE))  input_areas,
                v_store v
          where value(input_stores) = v.store
            and value(input_areas)  = v.area;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date stores and areas - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      elsif I_store_grade_group_id is not null then
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select v.store,
                v.store_name,
                'S'
           from table(cast(I_stores as OBJ_NUMERIC_ID_TABLE)) input_stores,
                store_grade_store g,
                v_store v
          where value(input_stores)    = v.store
            and g.store                = v.store
            and g.store_grade_group_id = I_store_grade_group_id;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date stores and store_grade_group - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      else
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select distinct v.store,
                v.store_name,
                'S'
           from table(cast(I_stores as OBJ_NUMERIC_ID_TABLE)) input_stores,
                v_store v
          where value(input_stores) = v.store;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date stores - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      end if;
   else
      if I_areas is not null and I_areas.count > 0 then
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select v.store,
                v.store_name,
                'S'
           from table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas,
                v_store v
          where value(input_areas) = v.area;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date areas - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      elsif I_store_grade_group_id is not null then
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_1,
                                                   varchar2_2)
         select v.store,
                v.store_name,
                'S'
           from store_grade_store g,
                v_store v
          where I_store_grade_group_id = g.store_grade_group_id
            and g.store                = v.store;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert gtt_num_num_str_str_date_date store_grade_group - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      end if;
   end if;

   if OI_UTILITY.LOCK_STATS(O_error_message,
                            I_session_id,
                            L_tabname) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   if I_areas is not null then
      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      with wh_expl as (select w.org_hier_type,
                              w.wh,
                              a.area
                         from area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and w.org_hier_value = a.area
                          and w.org_hier_type  = 20
                       union all
                       select w.org_hier_type,
                              w.wh,
                              a.area
                         from area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and w.org_hier_value = r.region
                          and w.org_hier_type  = 30
                       union all
                       select w.org_hier_type,
                              w.wh,
                              a.area
                         from area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and w.org_hier_value = d.district
                          and w.org_hier_type  = 40)
      select distinct v.wh,
             v.wh_name,
             'W'
        from wh_expl,
             v_wh v,
             table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas
       where wh_expl.area          = value(input_areas) 
         and wh_expl.org_hier_type in(20,30,40)
         and wh_expl.wh            = v.wh
      union all
      select distinct v.wh,
             v.wh_name,
             'W'
        from wh w,
             v_wh v
       where w.org_hier_type = 1
         and w.physical_wh   = w.wh
         and w.wh            = v.physical_wh
         and v.wh           != v.physical_wh;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date warehouses for area id - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      with wh_expl as (select w.org_hier_type,
                              w.wh,
                              a.area
                         from area a,
                              region r,
                              district d,
                              wh w,
                              v_internal_finisher v
                        where d.region         = r.region
                          and r.area           = a.area
                          and w.org_hier_value = a.area
                          and w.org_hier_type  = 20
                          and w.wh             = v.finisher_id
                       union all
                       select w.org_hier_type,
                              w.wh,
                              a.area
                         from area a,
                              region r,
                              district d,
                              wh w,
                              v_internal_finisher v
                        where d.region         = r.region
                          and r.area           = a.area
                          and w.org_hier_value = r.region
                          and w.org_hier_type  = 30
                          and w.wh             = v.finisher_id
                       union all
                       select w.org_hier_type,
                              w.wh,
                              a.area
                         from area a,
                              region r,
                              district d,
                              wh w,
                              v_internal_finisher v
                        where d.region         = r.region
                          and r.area           = a.area
                          and w.org_hier_value = d.district
                          and w.org_hier_type  = 40
                          and w.wh             = v.finisher_id)
      select distinct v.finisher_id,
             v.finisher_desc,
             'W'
        from wh_expl,
             v_internal_finisher v,
             table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas
       where wh_expl.wh            = v.finisher_id
         and wh_expl.area          = value(input_areas)
         and wh_expl.org_hier_type in(20,30,40);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date internal finishers for area id - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   if I_store_grade_group_id is not null then
      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      select v.wh,
             v.wh_name,
             'W'
        from v_wh v
       where v.wh != v.physical_wh;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date virtual warehouses for store grade group id - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      select distinct v.finisher_id,
             v.finisher_desc,
             'W'
        from v_internal_finisher v;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date internal finishers for store grade group id - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_NEG_INV_LOCS;
--------------------------------------------------------------------------------
FUNCTION OVERDUE_SHIPMENT_TSF(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_transfer_count         IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.OVERDUE_SHIPMENT_TSF';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                       DATE := get_vdate;
   L_ic_overdue_ship_days        rms_oi_system_options.ic_overdue_ship_days%TYPE;
   L_ic_overdue_ship_count       rms_oi_system_options.ic_overdue_ship_count%TYPE;
   L_intercompany_transfer_basis system_options.intercompany_transfer_basis%TYPE;
   L_oms_ind                     system_options.oms_ind%TYPE;
   L_std_av_ind                  system_options.std_av_ind%TYPE;
   L_seven_day_count             NUMBER(10) := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');


   select oi.ic_overdue_ship_days,
          oi.ic_overdue_ship_count,
          so.intercompany_transfer_basis,
          so.oms_ind,
          so.std_av_ind
     into L_ic_overdue_ship_days,
          L_ic_overdue_ship_count,
          L_intercompany_transfer_basis, 
          L_oms_ind,
          L_std_av_ind
     from rms_oi_system_options oi,
          system_options so;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_overdue_ship_tsf where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_tsf - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_overdue_ship_tsf (session_id,
                                        tsf_no,
                                        tsf_type,
                                        from_loc,
                                        from_loc_name,
                                        from_loc_type,
                                        to_loc,
                                        to_loc_name,
                                        to_loc_type,
                                        approval_date,
                                        not_after_date,
                                        delivery_date,
                                        finisher,
                                        finisher_name,
                                        intercompany_ind,
                                        tsf_qty,
                                        multi_uom_ind)
   select /*+ ordered */ 
          distinct I_session_id,
                   th.tsf_no,
                   th.tsf_type,
                   th.leg_1_from_loc,
                   vf.loc_name,
                   th.leg_1_from_loc_type,
                   --
                   nvl(vt2.loc,th.leg_1_to_loc),
                   nvl(vt2.loc_name,vt.loc_name),
                   nvl(vt2.loc_type,th.leg_1_to_loc_type),
                   --
                   th.approval_date,
                   nvl(th.not_after_date, th.delivery_date),
                   th.delivery_date,
                   decode(th.leg_2_from_loc,null,null,th.leg_1_to_loc) finisher,
                   decode(th.leg_2_from_loc,null,null,vt.loc_name) finisher_name,
                   --
                   case when L_intercompany_transfer_basis = 'T'
                      then case when th.leg_1_to_loc_entity is null or
                                     nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) is null or
                                     th.leg_1_to_loc_entity  = nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) then
                                        'A'
                                   else
                                        'E'
                                   end
                      else case when th.leg_1_to_loc_sob_id is null or
                                     nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) is null or
                                     th.leg_1_to_loc_sob_id  = nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) then
                                        'A'
                                   else
                                        'E'
                                   end
                   end intercompany_ind,
                   --
                   null tsf_qty,
                   null multi_uom_ind
     from gtt_num_num_str_str_date_date loc_gtt,
          v_transfer_head th,
          --
          (select loc, loc_name, loc_type, currency_code
             from v_loc_comm_attrib_sec
           union all
           select to_number(partner_id), partner_desc, partner_type, currency_code 
             from v_partner 
            where partner_type = 'E'
           union all
           select finisher_id, finisher_desc, 'I', currency_code
             from v_internal_finisher) vt,
          --
          v_loc_comm_attrib_sec vf,
          v_loc_comm_attrib_sec vt2
    where th.overall_status      = 'A'
      and (th.leg_1_from_loc     = loc_gtt.number_1
           or
           th.leg_1_to_loc       = loc_gtt.number_1)
      and L_vdate                > nvl(th.not_after_date, th.delivery_date) + L_ic_overdue_ship_days
      and th.leg_1_to_loc        = vt.loc
      and th.leg_1_to_loc_type   = vt.loc_type
      and th.leg_1_from_loc      = vf.loc
      and th.leg_1_from_loc_type = vf.loc_type
      and th.leg_2_to_loc        = vt2.loc(+)
      and th.leg_2_to_loc_type   = vt2.loc_type(+)
      and exists (select 'X'
                    from tsfdetail td,
                         gtt_10_num_10_str_10_date item_gtt
                   where td.tsf_no = th.tsf_no
                     and td.item   = item_gtt.varchar2_1
                     and rownum    = 1)
      and th.tsf_type NOT IN ('FO','FR')
      and (L_oms_ind = 'N' or (L_oms_ind = 'Y' and th.tsf_type <> 'CO'));

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_overdue_ship_tsf - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_overdue_ship_tsf target
   using (select /*+ ORDERED index(td, pk_tsfdetail) */ oi.session_id,
                 oi.tsf_no,
                 sum(td.tsf_qty) tsf_qty,
                 count(distinct im.standard_uom) uom_cnt
            from rms_oi_overdue_ship_tsf oi,
                 tsfdetail td,
                 item_master im
           where oi.session_id = I_session_id
             and oi.tsf_no     = td.tsf_no
             and td.item       = im.item
    group by oi.session_id,
             oi.tsf_no) use_this
   on (    target.session_id = use_this.session_id
       and target.tsf_no     = use_this.tsf_no)
   when matched then update
    set target.tsf_qty       = use_this.tsf_qty,
        target.multi_uom_ind = decode(use_this.uom_cnt, 1, 'N', 'Y');

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge tsfdetail info rms_oi_overdue_ship_tsf - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*) 
     into O_transfer_count
     from rms_oi_overdue_ship_tsf 
    where session_id = I_session_id;

   select count(*) 
     into L_seven_day_count
     from rms_oi_overdue_ship_tsf oi 
    where oi.session_id = I_session_id
      and L_vdate - oi.not_after_date >= L_ic_overdue_ship_days + 7;

   if L_seven_day_count > 0 and O_transfer_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_transfer_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END OVERDUE_SHIPMENT_TSF;
--------------------------------------------------------------------------------
FUNCTION OVERDUE_SHIPMENT_ALLOC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_alloc_count            IN OUT NUMBER,
                                O_report_level           IN OUT VARCHAR2,
                                I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                                I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                                I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                                I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.OVERDUE_SHIPMENT_ALLOC';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                    DATE := get_vdate;
   L_ic_overdue_ship_days     rms_oi_system_options.ic_overdue_ship_days%TYPE;
   L_ic_overdue_ship_count    rms_oi_system_options.ic_overdue_ship_count%TYPE;
   L_std_av_ind               system_options.std_av_ind%TYPE;
   L_seven_day_count          NUMBER(10) := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_overdue_ship_days,
          oi.ic_overdue_ship_count,
          roi.std_av_ind
     into L_ic_overdue_ship_days,
          L_ic_overdue_ship_count,
          L_std_av_ind
     from rms_oi_system_options oi,
          system_options roi;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_overdue_ship_alloc where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_alloc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_overdue_ship_alloc (session_id,
                                          alloc_no,
                                          from_loc,
                                          from_loc_name,
                                          from_loc_type,
                                          item,
                                          item_desc,
                                          release_date,
                                          alloc_qty)
   select I_session_id,
          alloc_no,
          wh,
          wh_name,
          'W',
          item,
          item_desc,
          release_date,
          null
     from (select ah.alloc_no,
                  ah.wh,
                  v_wh.wh_name,
                  ah.item,
                  item_gtt.varchar2_3 item_desc,
                  ah.release_date,
                  row_number() over (partition by ah.alloc_no
                                         order by ah.alloc_no) row_rank
             from gtt_10_num_10_str_10_date item_gtt,
                  gtt_num_num_str_str_date_date loc_gtt,
                  v_wh,
                  alloc_header ah,
                  alloc_detail ad
            where ah.item     = item_gtt.varchar2_1
              and ah.alloc_no = ad.alloc_no
              and ah.status   in('A', 'R')
              and L_vdate     > ah.release_date + L_ic_overdue_ship_days
              and (   ah.wh     = loc_gtt.number_1
                   or ad.to_loc = loc_gtt.number_1)
              and v_wh.wh     = ah.wh)
    where row_rank = 1;
 
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_overdue_ship_alloc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_overdue_ship_alloc target
   using (select oi.session_id,
                 oi.alloc_no,
                 sum(ad.qty_allocated) alloc_qty
            from rms_oi_overdue_ship_alloc oi,
                 alloc_detail ad
           where oi.session_id  = I_session_id
             and oi.alloc_no    = ad.alloc_no
           group by oi.session_id,
                    oi.alloc_no) use_this
   on (    target.session_id   = use_this.session_id
       and target.alloc_no     = use_this.alloc_no)
   when matched then update
    set target.alloc_qty    = use_this.alloc_qty;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge alloc_qty rms_oi_overdue_ship_alloc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_alloc_count
     from rms_oi_overdue_ship_alloc
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_overdue_ship_alloc oi
    where oi.session_id = I_session_id
      and L_vdate - oi.release_date >= L_ic_overdue_ship_days + 7;

   if L_seven_day_count > 0 and O_alloc_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_alloc_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END OVERDUE_SHIPMENT_ALLOC;
--------------------------------------------------------------------------------
FUNCTION OVERDUE_SHIPMENT_RTV(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rtv_count              IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.OVERDUE_SHIPMENT_RTV';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                    DATE := get_vdate;
   L_ic_overdue_ship_days     rms_oi_system_options.ic_overdue_ship_days%TYPE;
   L_ic_overdue_ship_count    rms_oi_system_options.ic_overdue_ship_count%TYPE;
   L_seven_day_count          NUMBER(10) := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_overdue_ship_days,
          oi.ic_overdue_ship_count
     into L_ic_overdue_ship_days,
          L_ic_overdue_ship_count
     from rms_oi_system_options oi;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_overdue_ship_rtv where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_rtv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_overdue_ship_rtv (session_id,
                                        rtv_order_no,
                                        from_loc,
                                        from_loc_name,
                                        from_loc_type,
                                        supplier_site,
                                        supplier_site_name,
                                        created_date,
                                        not_after_date,
                                        rtv_qty,
                                        multi_uom_ind)
   select I_session_id,
          rh.rtv_order_no,
          loc_gtt.number_1,   -- location
          loc_gtt.varchar2_1, -- location_name
          loc_gtt.varchar2_2, -- location_type
          rh.supplier,
          vs.sup_name,
          nvl(rh.not_after_date, rh.created_date),
          rh.not_after_date,
          null rtv_qty,
          null multi_uom_ind
     from gtt_num_num_str_str_date_date loc_gtt,
          rtv_head rh,
          v_sups vs
    where loc_gtt.number_1 = decode(rh.store,-1,rh.wh,rh.store)
      and rh.status_ind    in(10, 12)
      and L_vdate          > nvl(rh.not_after_date, rh.created_date) + L_ic_overdue_ship_days
      and rh.supplier      = vs.supplier
      and exists (select 'X'
                    from rtv_detail rd,
                         gtt_10_num_10_str_10_date item_gtt
                   where rd.rtv_order_no = rh.rtv_order_no
                     and rd.item         = item_gtt.varchar2_1
                     and rownum          = 1);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_overdue_ship_rtv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_supplier_sites is not null and I_supplier_sites.count > 0 then

      delete from rms_oi_overdue_ship_rtv r
       where r.session_id = I_session_id
         and supplier_site not in(select value(input_sups)
                                    from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete sups rms_oi_overdue_ship_rtv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   merge into rms_oi_overdue_ship_rtv target
   using (
          select rd.rtv_order_no,
                 sum(rd.qty_requested) qty_requested,
                 count(distinct im.standard_uom) uom_cnt
            from rms_oi_overdue_ship_rtv r,
                 rtv_detail rd,
                 item_master im
           where r.session_id   = I_session_id
             and r.rtv_order_no = rd.rtv_order_no
             and rd.item        = im.item
           group by rd.rtv_order_no) use_this
   on (    target.session_id   = I_session_id
       and target.rtv_order_no = use_this.rtv_order_no)
   when matched then update
    set target.rtv_qty       = use_this.qty_requested,
        target.multi_uom_ind = decode(use_this.uom_cnt, 1, 'N', 'Y');

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rtv_detail rms_oi_overdue_ship_rtv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_rtv_count
     from rms_oi_overdue_ship_rtv
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_overdue_ship_rtv oi
    where oi.session_id = I_session_id
      and L_vdate - NVL(oi.not_after_date, oi.created_date) >= L_ic_overdue_ship_days + 7;

   if L_seven_day_count > 0 and O_rtv_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_rtv_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END OVERDUE_SHIPMENT_RTV;
--------------------------------------------------------------------------------
FUNCTION UNEXPECTED_INVENTORY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_loc_count         IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.UNEXPECTED_INVENTORY';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_ic_unexp_inv_inactive_ind    rms_oi_system_options.ic_unexp_inv_inactive_ind%TYPE;
   L_ic_unexp_inv_discontinue_ind rms_oi_system_options.ic_unexp_inv_discontinue_ind%TYPE;
   L_ic_unexp_inv_delete_ind      rms_oi_system_options.ic_unexp_inv_delete_ind%TYPE;
   L_ic_unexp_inv_critial_count   rms_oi_system_options.ic_unexp_inv_critial_count%TYPE;
   L_ic_unexp_inv_warn_count      rms_oi_system_options.ic_unexp_inv_warn_count%TYPE;
   L_ic_unexp_inv_tolerance_qty   rms_oi_system_options.ic_unexp_inv_tolerance_qty%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_unexp_inv_inactive_ind,
          oi.ic_unexp_inv_discontinue_ind,
          oi.ic_unexp_inv_delete_ind,
          oi.ic_unexp_inv_critial_count,
          oi.ic_unexp_inv_warn_count,
          ic_unexp_inv_tolerance_qty
     into L_ic_unexp_inv_inactive_ind,
          L_ic_unexp_inv_discontinue_ind,
          L_ic_unexp_inv_delete_ind,
          L_ic_unexp_inv_critial_count,
          L_ic_unexp_inv_warn_count,
          L_ic_unexp_inv_tolerance_qty
     from rms_oi_system_options oi;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_unexpected_inv_gtt;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_unexpected_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_unexpected_inv where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_unexpected_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into gtt_15_num_15_str_15_date(varchar2_1,
                                         number_1,
                                         number_2)
         with item_locs as (select gttloc.number_1 location,
                                   gttloc.varchar2_1 location_name,
                                   gttloc.varchar2_2 location_type,
                                   --
                                   gttitem.varchar2_1  item,
                                   gttitem.varchar2_2  item_parent,
                                   gttitem.varchar2_3  item_desc,
                                   gttitem.varchar2_4  alc_item_type,
                                   gttitem.varchar2_5  agg_diff_1,
                                   gttitem.varchar2_6  agg_diff_2,
                                   gttitem.varchar2_7  agg_diff_3,
                                   gttitem.varchar2_8  agg_diff_4,
                                   gttitem.varchar2_9  item_parent_desc,
                                   gttitem.varchar2_10 standard_uom
                              from gtt_num_num_str_str_date_date gttloc,
                                   gtt_10_num_10_str_10_date gttitem
                             where gttloc.varchar2_2 = 'W'
                               and rownum > 0)
         select item_locs.item,
                item_locs.location,
                NVL(sum((d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_outbound
           from alloc_header h,
                item_locs,
                alloc_detail d
          where h.status        in ('A', 'R')
            and h.item          = item_locs.item
            and d.alloc_no      = h.alloc_no
            and h.wh            = item_locs.location
            and d.qty_allocated > NVL(d.qty_transferred,0)
            and (exists (select 'x'
                           from ordhead o
                          where o.order_no                  = h.order_no
                            and o.status                    in ('A', 'C')
                            and o.include_on_order_ind      = 'N'
                            and NVL(NULL, trunc(o.not_before_date)) <= trunc(o.not_before_date)
                            and rownum = 1) or
                 exists (select 'x'
                           from alloc_header h1
                          where h1.alloc_no                             = h.order_no
                            and h1.status                               in ('A', 'R')
                            and NVL(NULL, h1.release_date) <= h1.release_date
                            and rownum = 1) or
                 exists (select 'x'
                           from tsfhead t
                          where t.tsf_no          = h.order_no
                            and t.status          in ('A','S','P','L','C')
                            and (t.not_after_date is null or
                                 (NVL(NULL, nvl(t.delivery_date,sysdate)) <= nvl(t.delivery_date,sysdate)))
                            and rownum = 1))
          group by item_locs.item,
                   item_locs.location;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert alloc_outbound gtt_15_num_15_str_15_date - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_unexpected_inv (session_id,
                                      item,
                                      item_desc,
                                      item_parent,
                                      item_parent_desc,
                                      loc,
                                      loc_name,
                                      loc_type,
                                      status,
                                      ranged_ind,
                                      stock_on_hand,
                                      reserved_qty,
                                      non_sellable_qty,
                                      unexpected_qty,
                                      dept,
                                      dept_name,
                                      class,
                                      class_name,
                                      subclass,
                                      subclass_name)
   with item_locs as (select gttloc.number_1 location,
                             gttloc.varchar2_1 location_name,
                             gttloc.varchar2_2 location_type,
                             --
                             gttitem.varchar2_1  item,
                             gttitem.varchar2_2  item_parent,
                             gttitem.varchar2_3  item_desc,
                             gttitem.varchar2_4  alc_item_type,
                             gttitem.varchar2_5  agg_diff_1,
                             gttitem.varchar2_6  agg_diff_2,
                             gttitem.varchar2_7  agg_diff_3,
                             gttitem.varchar2_8  agg_diff_4,
                             gttitem.varchar2_9  item_parent_desc,
                             gttitem.varchar2_10 standard_uom,
                             gttitem.number_1    dept,
                             gttmh.varchar2_1    dept_name,
                             gttmh.number_2      class,
                             gttmh.varchar2_2    class_name,
                             gttmh.number_3      subclass,
                             gttmh.varchar2_3    subclass_name,
                             nvl(odi.ic_unexp_inv_tolerance_qty, L_ic_unexp_inv_tolerance_qty) tolerance_qty,
                             nvl(odi.ic_unexp_inv_inactive_ind, L_ic_unexp_inv_inactive_ind) ic_unexp_inv_inactive_ind,
                             nvl(odi.ic_unexp_inv_discontinue_ind, L_ic_unexp_inv_discontinue_ind) ic_unexp_inv_discontinue_ind,
                             nvl(odi.ic_unexp_inv_delete_ind, L_ic_unexp_inv_delete_ind) ic_unexp_inv_delete_ind
                        from gtt_num_num_str_str_date_date gttloc,
                             gtt_10_num_10_str_10_date gttitem,
                             gtt_6_num_6_str_6_date gttmh,
                             rms_oi_dept_options odi
                       where gttitem.number_1 = gttmh.number_1
                         and gttitem.number_2 = gttmh.number_2
                         and gttitem.number_3 = gttmh.number_3
                         and gttitem.number_1 = odi.dept(+))
   select I_session_id,
          item_locs.item,
          item_locs.item_desc,
          item_locs.item_parent,
          item_locs.item_parent_desc, 
          item_locs.location,
          item_locs.location_name,
          item_locs.location_type,
          --
          il.status,
          il.ranged_ind,
          --
          ils.stock_on_hand + ils.pack_comp_soh,
          ils.tsf_reserved_qty + ils.pack_comp_resv + ils.rtv_qty + ils.customer_resv + ils.pack_comp_cust_resv,
          ils.non_sellable_qty + nvl(ils.pack_comp_non_sellable,0) + nvl(ao.number_2,0),
          --
          (ils.stock_on_hand + ils.pack_comp_soh) -
          (ils.tsf_reserved_qty + ils.pack_comp_resv + ils.rtv_qty + ils.customer_resv + ils.pack_comp_cust_resv + nvl(ao.number_2,0)),
          --
          item_locs.dept,
          item_locs.dept_name,
          item_locs.class,
          item_locs.class_name,
          item_locs.subclass,
          item_locs.subclass_name
     from item_locs,
          item_loc il,
          item_loc_soh ils,
          gtt_15_num_15_str_15_date ao
    where item_locs.item     = il.item
      and item_locs.location = il.loc
      and il.item            = ils.item
      and il.loc             = ils.loc
      and ((il.ranged_ind    = 'N')
           or
           (il.ranged_ind     = 'Y' and
            il.status         in('I','C','D'))
          )
      and il.item            = ao.varchar2_1(+)
      and il.loc             = ao.number_1(+)
      and (ils.stock_on_hand + ils.pack_comp_soh) -
           (ils.tsf_reserved_qty + ils.pack_comp_resv +
            ils.rtv_qty + ils.customer_resv +
            ils.pack_comp_cust_resv + nvl(ao.number_2,0)) >=  item_locs.tolerance_qty;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_unexpected_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_unexpected_inv ui
    where ui.session_id = I_session_id
      and ui.ranged_ind = 'Y'
      and not exists (select 'X' 
                        from (select ui2.ranged_ind, 
                                     ui2.status,
                                     nvl(odi.ic_unexp_inv_inactive_ind, L_ic_unexp_inv_inactive_ind) inactive_ind,
                                     nvl(odi.ic_unexp_inv_discontinue_ind, L_ic_unexp_inv_discontinue_ind) disc_ind,
                                     nvl(odi.ic_unexp_inv_delete_ind, L_ic_unexp_inv_delete_ind) del_ind
                                from rms_oi_unexpected_inv ui2,
                                     rms_oi_dept_options odi
                               where ui2.dept       = odi.dept(+)) i
                       where i.status in(select 'I' from dual where i.inactive_ind = 'Y'
                                         union all
                                         select 'C' from dual where i.disc_ind = 'Y'
                                         union all
                                         select 'D' from dual where i.del_ind = 'Y'));

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_unexpected_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_item_loc_count
     from rms_oi_unexpected_inv
    where session_id = I_session_id;

   if O_item_loc_count > L_ic_unexp_inv_critial_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif O_item_loc_count > L_ic_unexp_inv_warn_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END UNEXPECTED_INVENTORY;
--------------------------------------------------------------------------------
FUNCTION TSF_PENDING_APPROVE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_transfer_count         IN OUT NUMBER,
                             O_report_level           IN OUT VARCHAR2,
                             I_session_id             IN     oi_session_id_log.session_id%TYPE,
                             I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                             I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                             I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                             I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                             I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                             I_view_intracompany_priv IN     VARCHAR2,   --'Y' or 'N'
                             I_view_intercompany_priv IN     VARCHAR2)   --'Y' or 'N'
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.TSF_PENDING_APPROVE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_tsf_apprv_pend_crit_days    rms_oi_system_options.ic_tsf_apprv_pend_critial_days%TYPE;
   L_ic_tsf_apprv_pend_crit_cnt     rms_oi_system_options.ic_tsf_apprv_pend_critial_cnt%TYPE;
   L_intercompany_transfer_basis    system_options.intercompany_transfer_basis%TYPE;
   L_oms_ind                        system_options.oms_ind%TYPE;
   L_std_av_ind                     system_options.std_av_ind%TYPE;
   L_seven_day_count                NUMBER(10);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_tsf_apprv_pend_critial_days,
          oi.ic_tsf_apprv_pend_critial_cnt,
          so.intercompany_transfer_basis,
          so.oms_ind,
          so.std_av_ind
     into L_ic_tsf_apprv_pend_crit_days,
          L_ic_tsf_apprv_pend_crit_cnt,
          L_intercompany_transfer_basis,
          L_oms_ind,
          L_std_av_ind
     from rms_oi_system_options oi,
          system_options so;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_tsf_pend_approve where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_tsf_pend_approve (session_id,
                                        tsf_no,
                                        tsf_type,
                                        from_loc,
                                        from_loc_name,
                                        from_loc_type,
                                        to_loc,
                                        to_loc_name,
                                        to_loc_type,
                                        delivery_date,
                                        create_date,
                                        tsf_cost,
                                        tsf_retail,
                                        currency_code,
                                        finisher,
                                        finisher_name,
                                        intercompany_ind)
   select /*+ ordered */
          distinct I_session_id,
                   th.tsf_no,
                   th.tsf_type,
                   th.leg_1_from_loc,
                   vf.loc_name,
                   th.leg_1_from_loc_type,
                   --
                   nvl(vt2.loc,th.leg_1_to_loc),
                   nvl(vt2.loc_name,vt.loc_name),
                   nvl(vt2.loc_type,th.leg_1_to_loc_type),
                   --
                   th.delivery_date,
                   th.create_date,
                   null tsf_cost,
                   null tsf_retail,
                   vf.currency_code,
                   decode(th.leg_2_from_loc,null,null,th.leg_1_to_loc) finisher,
                   decode(th.leg_2_from_loc,null,null,vt.loc_name) finisher_name,
                   --
                   case when L_intercompany_transfer_basis = 'T'
                      then case when th.leg_1_to_loc_entity is null or
                                     nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) is null or
                                     th.leg_1_to_loc_entity  = nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) then
                                        'A'
                                   else
                                        'E'
                                   end
                      else case when th.leg_1_to_loc_sob_id is null or
                                     nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) is null or
                                     th.leg_1_to_loc_sob_id  = nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) then
                                        'A'
                                   else
                                        'E'
                                   end
                   end intercompany_ind
                   --
     from gtt_num_num_str_str_date_date loc_gtt,
          v_transfer_head th,
          --
          (select loc, loc_name, loc_type, currency_code
             from v_loc_comm_attrib_sec
           union all
           select to_number(partner_id), partner_desc, partner_type, currency_code
             from v_partner
            where partner_type = 'E'
           union all
           select finisher_id, finisher_desc, 'I', currency_code 
             from v_internal_finisher
           ) vt,
          --
          v_loc_comm_attrib_sec vf,
          v_loc_comm_attrib_sec vt2
    where th.overall_status      = 'B'
      and (th.leg_1_from_loc     = loc_gtt.number_1
           or
           th.leg_1_to_loc       = loc_gtt.number_1)
      and L_vdate                > th.create_date + L_ic_tsf_apprv_pend_crit_days
      and th.leg_1_to_loc        = vt.loc
      and th.leg_1_to_loc_type   = vt.loc_type
      and th.leg_1_from_loc      = vf.loc
      and th.leg_1_from_loc_type = vf.loc_type
      and th.leg_2_to_loc        = vt2.loc(+)
      and th.leg_2_to_loc_type   = vt2.loc_type(+)
      and th.wf_order_no         is null --no franchise order tsfs
      and th.rma_no              is null --no franchise return tsfs
      and (L_oms_ind = 'N' or (L_oms_ind = 'Y' and th.tsf_type <> 'CO')) --no cust order tsfs
      and exists (select 'X'
                    from tsfdetail td,
                         gtt_10_num_10_str_10_date item_gtt
                   where td.tsf_no = th.tsf_no
                     and td.item   = item_gtt.varchar2_1
                     and rownum    = 1);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_view_intracompany_priv = 'N' then

      delete from rms_oi_tsf_pend_approve pa
       where pa.session_id       = I_session_id 
         and pa.intercompany_ind = 'A';

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete I_view_intracompany_priv rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;
 
   if I_view_intercompany_priv = 'N' then

      delete from rms_oi_tsf_pend_approve pa
       where pa.session_id       = I_session_id
         and pa.intercompany_ind = 'E';

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete I_view_intercompany_priv rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   merge into rms_oi_tsf_pend_approve target
   using (select /*+ ORDERED index(il, pk_item_loc) index(ilsoh, pk_item_loc_soh) */ tmp.session_id,
                 tmp.tsf_no,
                 sum(tmp.tsf_qty * ilsoh.unit_cost * tmp.pack_qty) tsf_cost,
                 sum(tmp.tsf_qty * ilsoh.av_cost   * tmp.pack_qty) tsf_av_cost,
                 sum(tmp.tsf_qty * il.unit_retail  * tmp.pack_qty) tsf_retail,
                 sum(tmp.tsf_qty * tmp.tsf_cost) td_cost,
                 sum(tmp.tsf_qty * tmp.tsf_price) td_retail
            from (select oi.session_id,
                         oi.tsf_no,
                         oi.from_loc,
                         td.tsf_qty,
                         td.tsf_cost,
                         td.tsf_price,
                         nvl(vpq.item, td.item) item,
                         nvl(vpq.qty, 1) pack_qty
                    from rms_oi_tsf_pend_approve oi,
                         tsfdetail td,
                         v_packsku_qty vpq
                   where oi.session_id   = I_session_id
                     and (oi.tsf_cost   is null
                          or
                          oi.tsf_retail is null)
                     and oi.tsf_no      = td.tsf_no
                     and td.item        = vpq.pack_no(+)
                 ) tmp,
                 item_loc il,
                 item_loc_soh ilsoh
           where tmp.item     = il.item
             and tmp.from_loc = il.loc
             and il.item      = ilsoh.item
             and il.loc       = ilsoh.loc
           group by tmp.session_id,
                    tmp.tsf_no) use_this
   on (    target.session_id = use_this.session_id
       and target.tsf_no     = use_this.tsf_no)
   when matched then update
    set target.tsf_cost   = nvl(use_this.td_cost, decode(L_std_av_ind, 'S', use_this.tsf_cost, use_this.tsf_av_cost)),
        target.tsf_retail = nvl(use_this.td_retail, use_this.tsf_retail);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge item_loc_soh info rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_transfer_count
     from rms_oi_tsf_pend_approve
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_tsf_pend_approve oi
    where oi.session_id = I_session_id
      and L_vdate - oi.create_date >= L_ic_tsf_apprv_pend_crit_days + 7;

   if L_seven_day_count > 0 and O_transfer_count > L_ic_tsf_apprv_pend_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_transfer_count > L_ic_tsf_apprv_pend_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END TSF_PENDING_APPROVE;
--------------------------------------------------------------------------------
FUNCTION STKORD_PENDING_CLOSE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stkord_count           IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                              I_view_intercompany_priv IN     VARCHAR2)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.STKORD_PENDING_CLOSE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_stkord_cls_pend_crit_days   rms_oi_system_options.ic_stkord_cls_pend_crit_days%TYPE;
   L_ic_stkord_cls_pend_crit_cnt    rms_oi_system_options.ic_stkord_cls_pend_crit_cnt%TYPE;
   L_seven_day_count                NUMBER(10) := 0;
   L_critial_count                  NUMBER(10);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_stkord_cls_pend_crit_days,
          oi.ic_stkord_cls_pend_crit_cnt
     into L_ic_stkord_cls_pend_crit_days,
          L_ic_stkord_cls_pend_crit_cnt
     from rms_oi_system_options oi;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1  -- item
   --  varchar2_2  -- item_parent
   --  varchar2_3  -- item_desc
   --  varchar2_4  -- alc_item_type
   --  varchar2_5  -- agg_diff_1
   --  varchar2_6  -- agg_diff_2
   --  varchar2_7  -- agg_diff_3
   --  varchar2_8  -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,           --I_origin_countries
                             null,           --I_brands
                             null,           --I_forecast_ind
                             FALSE) = 0 then --I_input_required
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_stk_ord_pend_close where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stk_ord_pend_close - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_stk_ord_pend_close (session_id,
                                          stock_order_id,
                                          stock_order_type,
                                          from_loc,
                                          from_loc_name,
                                          from_loc_type,
                                          to_loc,
                                          to_loc_name,
                                          to_loc_type,
                                          receipt_date,
                                          multi_uom_ind,
                                          received_pct,
                                          reconciled_qty,
                                          stock_order_qty,
                                          shipped_qty,
                                          received_qty,
                                          cancelled_qty,
                                          finisher,
                                          finisher_name)
   select I_session_id,
          i.tsf_no,
          'T' stock_order_type,
          i.from_loc,
          i.from_loc_name,
          i.from_loc_type,
          i.to_loc,
          i.to_loc_name,
          i.to_loc_type,
          i.receipt_date,
          case when i.uom_cnt > 1 
             then 'Y'
             else 'N' end multi_uom_ind,
          nvl(i.received_qty,0) / i.stock_order_qty,
          i.shipped_qty - nvl(i.received_qty,0),
          i.stock_order_qty,
          i.shipped_qty,
          nvl(i.received_qty,0),
          i.cancelled_qty,
          i.finisher,
          i.finisher_name
     from (select /*+ ordered */ th.tsf_no,
                  th.leg_1_from_loc from_loc,
                  vf.loc_name from_loc_name,
                  th.leg_1_from_loc_type from_loc_type,
                  --
                  nvl(vt2.loc,th.leg_1_to_loc) to_loc,
                  nvl(vt2.loc_name,vt.loc_name) to_loc_name,
                  nvl(vt2.loc_type,th.leg_1_to_loc_type) to_loc_type,
                  --
                  decode(th.leg_2_from_loc,null,null,th.leg_1_to_loc) finisher,
                  decode(th.leg_2_from_loc,null,null,vt.loc_name) finisher_name,
                  --
                  max(ss.receive_date) receipt_date,
                  max(th.delivery_date) delivery_date,
                  count(distinct im.standard_uom) uom_cnt,
                  sum(td.tsf_qty) stock_order_qty,
                  sum(ss.qty_expected) shipped_qty,
                  sum(ss.qty_received) received_qty,
                  sum(td.cancelled_qty) cancelled_qty
             from gtt_num_num_str_str_date_date loc_gtt,
                  v_transfer_head th,
                  --
                  (select loc, loc_name, loc_type, currency_code
                     from v_loc_comm_attrib_sec
                   union all
                   select to_number(partner_id), partner_desc, partner_type, currency_code
                     from v_partner
                    where partner_type = 'E'
                   union all
                   select finisher_id, finisher_desc, 'I', currency_code
                     from v_internal_finisher) vt,
                  --
                  v_loc_comm_attrib_sec vf,
                  v_loc_comm_attrib_sec vt2,  
                  (select s2.distro_no,
                          sum(s2.qty_expected) qty_expected, 
                          sum(s2.qty_received) qty_received,
                          max(s1.receive_date) receive_date, 
                          s2.item
                    from shipment s1,
                         shipsku s2
                   where s1.shipment = s2.shipment 
                     and s2.adjust_type is null
                     and s2.distro_type='T'
                   group by s2.distro_no,
                            s2.item) ss,
                  tsfdetail td,
                  item_master im,
                  tsfitem_inv_flow tif
            where th.overall_status              != 'C'
              and nvl(tsf_type,'X') != nvl(decode(I_view_intercompany_priv, 'N','IC',null),'X')
              and th.tsf_no = tif.tsf_no(+)
              and td.tsf_no = tif.tsf_no(+)
              and ((th.leg_1_from_loc              = loc_gtt.number_1 or tif.from_loc = loc_gtt.number_1)
                   or
                   (th.leg_1_to_loc                = loc_gtt.number_1 or tif.to_loc = loc_gtt.number_1)
                   or
                   (th.leg_2_from_loc              = loc_gtt.number_1 or tif.from_loc = loc_gtt.number_1)
                   or
                   (th.leg_2_to_loc                = loc_gtt.number_1 or tif.to_loc = loc_gtt.number_1))
              and th.leg_1_to_loc                 = vt.loc
              and th.leg_1_to_loc_type            = vt.loc_type
              and th.leg_1_from_loc               = vf.loc
              and th.leg_1_from_loc_type          = vf.loc_type
              and th.leg_2_to_loc                 = vt2.loc(+)
              and th.leg_2_to_loc_type            = vt2.loc_type(+)
              and (   th.tsf_no = ss.distro_no
                   or nvl(th.child_tsf_no,-999) = ss.distro_no)
              and th.tsf_no                       = td.tsf_no
              and ss.item                         = td.item
              and td.item                         = im.item
              and exists (select 'X'
                            from tsfdetail td2,
                                 gtt_10_num_10_str_10_date item_gtt
                           where (   td2.tsf_no   = th.tsf_no
                                  or td2.tsf_no   = nvl(th.child_tsf_no,-999))
                             and td2.item     = item_gtt.varchar2_1
                             and td2.tsf_qty != nvl(td2.received_qty,0)
                             and rownum       = 1)
            group by th.tsf_no,
                     th.leg_1_from_loc,
                     vf.loc_name,
                     th.leg_1_from_loc_type,
                     nvl(vt2.loc,th.leg_1_to_loc),
                     nvl(vt2.loc_name,vt.loc_name),
                     nvl(vt2.loc_type,th.leg_1_to_loc_type),
                     decode(th.leg_2_from_loc,null,null,th.leg_1_to_loc),
                     decode(th.leg_2_from_loc,null,null,vt.loc_name)) i
    where nvl(i.receipt_date,i.delivery_date) < L_vdate - L_ic_stkord_cls_pend_crit_days
      and i.shipped_qty != nvl(i.received_qty,0);
        
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert tsf rms_oi_stk_ord_pend_close - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_stk_ord_pend_close (session_id,
                                          stock_order_id,
                                          stock_order_type,
                                          from_loc,
                                          from_loc_name,
                                          from_loc_type,
                                          to_loc,
                                          to_loc_name,
                                          to_loc_type,
                                          receipt_date,
                                          multi_uom_ind,
                                          received_pct,
                                          reconciled_qty,
                                          stock_order_qty,
                                          shipped_qty,
                                          received_qty,
                                          cancelled_qty,
                                          finisher,
                                          finisher_name)
   select I_session_id,
          i.alloc_no,
          i.stock_order_type,
          i.from_loc,
          i.from_loc_name,
          i.from_loc_type,
          i.to_loc,
          i.to_loc_name,
          i.to_loc_type,
          i.receipt_date,
          case when i.uom_cnt > 1 
             then 'Y'
             else 'N' end multi_uom_ind,
          nvl(i.received_qty,0) / i.stock_order_qty,
          i.shipped_qty - nvl(i.received_qty,0),
          i.stock_order_qty,
          i.shipped_qty,
          i.received_qty,
          i.cancelled_qty,
          i.finisher,
          i.finisher_name
     from (select /*+ leading(loc_gtt) */ I_session_id,
                  ah.alloc_no,
                  'A' stock_order_type,
                  ah.wh from_loc,
                  vf.loc_name from_loc_name,
                  'W' from_loc_type,
                  ad.to_loc,
                  vt.loc_name to_loc_name,
                  ad.to_loc_type,
                  max(sh.receive_date) receipt_date,
                  max(ah.release_date) release_date,
                  count(distinct item_gtt.varchar2_10) uom_cnt,
                  sum(ad.qty_allocated) stock_order_qty,
                  sum(ss.qty_expected) shipped_qty,
                  sum(ss.qty_received) received_qty,
                  sum(ad.qty_cancelled) cancelled_qty,
                  null finisher,
                  null finisher_name
             from gtt_num_num_str_str_date_date loc_gtt,
                  alloc_header ah,
                  v_loc_comm_attrib_sec vf,
                  v_loc_comm_attrib_sec vt,
                  shipsku ss,
                  shipment sh,
                  gtt_10_num_10_str_10_date item_gtt,
                  alloc_detail ad
            where ah.status      in ('R','A')
              and (ah.wh         = loc_gtt.number_1
                   or
                   ad.to_loc     = loc_gtt.number_1)
              and ah.wh          = vf.loc
              and ad.to_loc      = vt.loc
              and ah.alloc_no    = ss.distro_no
              and ss.distro_type = 'A'
              and ss.shipment    = sh.shipment
              and ss.adjust_type  is null
              --and sh.receive_date is not null /* Since Receive date will not null if all the shipments are received */
              and ss.item        = item_gtt.varchar2_1
              and ah.alloc_no    = ad.alloc_no
              and ss.item        = ah.item
            group by ah.alloc_no,
                     ah.wh,
                     vf.loc_name,
                     ad.to_loc,
                     vt.loc_name,
                     ad.to_loc_type) i
    where nvl(i.receipt_date,i.release_date) < L_vdate - L_ic_stkord_cls_pend_crit_days
      and (i.shipped_qty - nvl(i.received_qty,0)) != 0;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert alloc rms_oi_stk_ord_pend_close - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_stkord_count
     from rms_oi_stk_ord_pend_close
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_stk_ord_pend_close oi
    where oi.session_id = I_session_id
      and (   (    oi.receipt_date is not null 
               and L_vdate - oi.receipt_date >= 7 + L_ic_stkord_cls_pend_crit_days)
           or (    oi.receipt_date is null
               and exists (select 'x'
                              from tsfhead th,
                                   shipsku ss,
                                   shipment s
                             where th.tsf_no      = oi.stock_order_id
                               and th.tsf_no      = ss.distro_no
                               and ss.adjust_type is null
                               and ss.distro_type ='T'
                               and ss.shipment    = s.shipment
                               and s.receive_date is null
                               and L_vdate - s.ship_date >= 7 + L_ic_stkord_cls_pend_crit_days)));

   if L_seven_day_count > 0 and O_stkord_count > L_ic_stkord_cls_pend_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_stkord_count > L_ic_stkord_cls_pend_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END STKORD_PENDING_CLOSE;
--------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_MISSING(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_stock_cnt_count        IN OUT NUMBER,
                             O_report_level           IN OUT VARCHAR2,
                             I_session_id             IN     oi_session_id_log.session_id%TYPE,
                             I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                             I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                             I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                             I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.STOCK_COUNT_MISSING';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_miss_stk_count_crit_days    rms_oi_system_options.ic_miss_stk_count_crit_days%TYPE;
   L_ic_miss_stk_count_crit_cnt     rms_oi_system_options.ic_miss_stk_count_crit_cnt%TYPE;
   L_seven_day_count                NUMBER(10) := 0;
   L_critial_count                  NUMBER(10);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_miss_stk_count_crit_days,
          oi.ic_miss_stk_count_crit_cnt
     into L_ic_miss_stk_count_crit_days,
          L_ic_miss_stk_count_crit_cnt
     from rms_oi_system_options oi;

   --gtt_6_num_6_str_6_date
   --  number_1 -- dept
   --  number_2 -- class
   --  number_3 -- subclass
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  FALSE) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_missing_stock_count where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_missing_stock_count - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_missing_stock_count (session_id,
                                           cycle_count,
                                           cycle_count_desc,
                                           loc,
                                           loc_type,
                                           loc_name,
                                           stocktake_date)
   select I_session_id,
          i.cycle_count,
          i.cycle_count_desc,  
          i.location,
          i.loc_type,
          i.location_name,
          i.stocktake_date
     from (select I_session_id,
                  sh.cycle_count,
                  sh.cycle_count_desc,
                  ssl.location,
                  ssl.loc_type,
                  loc_gtt.varchar2_1 location_name,
                  sh.stocktake_date,
                  row_number() over (partition by sh.cycle_count, ssl.location, sh.stocktake_date
                                         order by sh.cycle_count) row_rank
             from stake_head sh,
                  stake_sku_loc ssl,
                  gtt_num_num_str_str_date_date loc_gtt
            where sh.cycle_count    = ssl.cycle_count
              and loc_gtt.number_1  = ssl.location
              and L_vdate          >= sh.stocktake_date + L_ic_miss_stk_count_crit_days
              and ssl.processed     = 'N'
              and exists (select 'X'
                            from stake_sku_loc ssl2,
                                 gtt_6_num_6_str_6_date mh_gtt
                           where sh.cycle_count    = ssl2.cycle_count
                             and mh_gtt.number_1   = ssl2.dept
                             and mh_gtt.number_2   = ssl2.class
                             and mh_gtt.number_3   = ssl2.subclass
                             and rownum            = 1)) i
    where i.row_rank = 1;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_missing_stock_count - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_stock_cnt_count
     from rms_oi_missing_stock_count
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_missing_stock_count oi
    where oi.session_id = I_session_id
      and L_vdate - oi.stocktake_date >= L_ic_miss_stk_count_crit_days + 7;

   if L_seven_day_count > 0 and O_stock_cnt_count > L_ic_miss_stk_count_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_stock_cnt_count > L_ic_miss_stk_count_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END STOCK_COUNT_MISSING;
--------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stock_cnt_count        IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.STOCK_COUNT_VARIANCE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_stk_count_var_pct           rms_oi_system_options.ic_stk_count_var_pct%TYPE;
   L_ic_stk_count_var_loc_cnt       rms_oi_system_options.ic_stk_count_var_loc_cnt%TYPE;
   L_seven_day_count                NUMBER(10) := 0;
   L_critial_count                  NUMBER(10);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_stk_count_var_pct,
          oi.ic_stk_count_var_loc_cnt
     into L_ic_stk_count_var_pct,
          L_ic_stk_count_var_loc_cnt
     from rms_oi_system_options oi;

   --gtt_6_num_6_str_6_date
   --  number_1 -- dept
   --  number_2 -- class
   --  number_3 -- subclass
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses,
                                  FALSE) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_num_num_str_str_date_date
   --  number_1   -- location
   --  varchar2_1 -- location_name
   --  varchar2_2 -- location_type
   if OI_UTILITY.SETUP_LOCATIONS(O_error_message,
                                 I_session_id,
                                 I_chains,
                                 I_areas,
                                 I_locations) = 0 then
      return OI_UTILITY.FAILURE;
   end if;
   if EXPAND_LOC_TO_WH(O_error_message,  
                       I_session_id,     
                       I_chains,         
                       I_areas) = 0 then 
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_stock_count_variance where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stock_count_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_stock_count_variance (session_id,
                                            cycle_count,
                                            cycle_count_desc,
                                            stocktake_type,
                                            stocktake_date,
                                            loc,
                                            loc_type,
                                            loc_name,
                                            total_variance_pct,
                                            over_variance_pct,
                                            short_variance_pct,
                                            snapshot_on_hand_qty,
                                            snapshot_in_transit_qty,
                                            physical_count_qty)
   select I_session_id,
          i.cycle_count,
          i.cycle_count_desc,
          i.stocktake_type,
          i.stocktake_date,
          i.location,
          i.loc_type,
          i.loc_name,
          --
          abs(case when i.over_sum = 0 and i.snapshot_on_hand_qty = 0 then
                      0
                   when i.snapshot_on_hand_qty = 0 then
                      100
                   else
                      (i.over_sum * 100) / i.snapshot_on_hand_qty
              end
              + 
              case when i.short_sum = 0 and i.snapshot_on_hand_qty = 0 then
                      0
                   when i.snapshot_on_hand_qty = 0 then
                      100
                   else
                      (i.short_sum * 100) / i.snapshot_on_hand_qty
              end) total_variance_pct,
          --
          case when i.over_sum = 0 and i.snapshot_on_hand_qty = 0 then
                  0
               when i.snapshot_on_hand_qty = 0 then
                  100
               else
                  (i.over_sum * 100) / i.snapshot_on_hand_qty
          end over_variance_pct,
          --
          case when i.short_sum = 0 and i.snapshot_on_hand_qty = 0 then
                  0
               when i.snapshot_on_hand_qty = 0 then
                  100
               else
                  (i.short_sum * 100) / i.snapshot_on_hand_qty
          end short_variance_pct,
          i.snapshot_on_hand_qty,
          i.snapshot_in_transit_qty,
          i.physical_count_qty
     from (select sh.cycle_count,
                  sh.cycle_count_desc,
                  sh.stocktake_type,
                  sh.stocktake_date,
                  ssl.location,
                  ssl.loc_type,
                  loc_gtt.varchar2_1 loc_name,
                  sum(nvl(ssl.snapshot_on_hand_qty,0)) snapshot_on_hand_qty,
                  sum(nvl(ssl.snapshot_in_transit_qty,0)) snapshot_in_transit_qty,
                  sum(ssl.physical_count_qty) physical_count_qty,
                  --
                  sum(case when nvl(ssl.physical_count_qty,0) > nvl(ssl.snapshot_on_hand_qty,0)
                     then nvl(ssl.physical_count_qty,0) - nvl(ssl.snapshot_on_hand_qty,0)
                     else 0 end) over_sum,
                  --
                  sum(case when nvl(ssl.physical_count_qty,0) < nvl(ssl.snapshot_on_hand_qty,0)
                     then nvl(ssl.physical_count_qty,0) - nvl(ssl.snapshot_on_hand_qty,0)
                     else 0 end) short_sum
                  --
             from stake_head sh,
                  stake_sku_loc ssl,
                  gtt_num_num_str_str_date_date loc_gtt
            where sh.cycle_count    = ssl.cycle_count
              and loc_gtt.number_1  = ssl.location
              and ssl.processed     = 'N'
              --
              and (   sh.stocktake_type = 'U'
                   or (    sh.stocktake_type = 'B'
                       and exists (select 'X'
                                     from stake_prod_loc spl
                                    where ssl.cycle_count   = spl.cycle_count
                                      and ssl.dept          = spl.dept
                                      and ssl.class         = spl.class
                                      and ssl.location      = spl.location
                                      and ssl.loc_type      = spl.loc_type
                                      and spl.processed     = 'N'
                                      and rownum            = 1)))
              --
              and exists (select 'X'
                            from stake_sku_loc ssl2,
                                 gtt_6_num_6_str_6_date mh_gtt
                           where sh.cycle_count    = ssl2.cycle_count
                             and mh_gtt.number_1   = ssl2.dept
                             and mh_gtt.number_2   = ssl2.class
                             and mh_gtt.number_3   = ssl2.subclass
                             --exclude old stock counts without physical count 
                             and (   (    ssl2.physical_count_qty is not null
                                      and sh.stocktake_date < L_vdate)
                                  or sh.stocktake_date >= L_vdate)
                             and rownum            = 1)
              and not exists (select 'X'
                                from stake_cont sc
                               where sc.cycle_count = ssl.cycle_count
                                 and sc.location    = ssl.location
                                 and sc.loc_type    = ssl.loc_type
                                 and sc.item        = ssl.item
                                 and sc.run_type    = 'A')
            group by sh.cycle_count,
                     sh.cycle_count_desc,
                     sh.stocktake_type,
                     sh.stocktake_date,
                     ssl.location,
                     ssl.loc_type,
                     loc_gtt.varchar2_1) i
    where L_ic_stk_count_var_pct <
          abs(case when i.over_sum = 0 and i.snapshot_on_hand_qty = 0 then
                      0
                   when i.snapshot_on_hand_qty = 0 then
                      100
                   else
                      (i.over_sum * 100) / i.snapshot_on_hand_qty
              end
              +
              case when i.short_sum = 0 and i.snapshot_on_hand_qty = 0 then
                      0
                   when i.snapshot_on_hand_qty = 0 then
                      100
                   else
                      (i.short_sum * 100) / i.snapshot_on_hand_qty
              end)
      and i.physical_count_qty is not NULL;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_stock_count_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_stock_cnt_count
     from rms_oi_stock_count_variance
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_stock_count_variance oi
    where oi.session_id = I_session_id
      and L_vdate - oi.stocktake_date >= 7;

   if L_seven_day_count > 0 and O_stock_cnt_count > L_ic_stk_count_var_loc_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_stock_cnt_count > L_ic_stk_count_var_loc_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END STOCK_COUNT_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DELETE_SESSION_RECORDS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN

   delete from rms_oi_inv_ctl_neg_inv      where session_id = I_session_id;
   delete from rms_oi_overdue_ship_tsf     where session_id = I_session_id;
   delete from rms_oi_overdue_ship_alloc   where session_id = I_session_id;
   delete from rms_oi_overdue_ship_rtv     where session_id = I_session_id;
   delete from rms_oi_unexpected_inv       where session_id = I_session_id;
   delete from rms_oi_tsf_pend_approve     where session_id = I_session_id;
   delete from rms_oi_stk_ord_pend_close   where session_id = I_session_id;
   delete from rms_oi_missing_stock_count  where session_id = I_session_id;
   delete from rms_oi_stock_count_variance where session_id = I_session_id;

   if OI_UTILITY.DELETE_SESSION_ID_LOG(O_error_message,
                                       I_session_id) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_RECORDS;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION EXPAND_LOC_TO_WH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_chains          IN     OBJ_NUMERIC_ID_TABLE,
                          I_areas           IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.EXPAND_LOC_TO_WH';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_tabname     USER_TAB_STATISTICS.TABLE_NAME%TYPE := 'GTT_NUM_NUM_STR_STR_DATE_DATE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if I_areas is not null and I_areas.count > 0 then

      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      with wh_expl as (select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = c.chain 
                          and w.org_hier_type  = 10
                          and w.wh             = w.physical_wh
                       union all
                       select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = a.area
                          and w.org_hier_type  = 20
                          and w.wh             = w.physical_wh
                       union all
                       select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = r.region
                          and w.org_hier_type  = 30
                          and w.wh             = w.physical_wh
                       union all
                       select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = d.district
                          and w.org_hier_type  = 40
                          and w.wh             = w.physical_wh)
      select distinct v.wh,
             v.wh_name,
             'W'
        from wh_expl,
             v_wh v,
             table(cast(I_areas as OBJ_NUMERIC_ID_TABLE)) input_areas
       where wh_expl.wh            = v.physical_wh
         and wh_expl.area          = value(input_areas) 
         and wh_expl.org_hier_type in(20,30,40)
      union all
      select distinct v.wh,
             v.wh_name,
             'W'
        from wh w,
             v_wh v
       where w.org_hier_type = 1
         and w.physical_wh   = w.wh
         and w.wh            = v.physical_wh
         and v.wh           != v.physical_wh;
       
      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' AREA insert gtt_num_num_str_str_date_date - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   elsif I_chains is not null and I_chains.count > 0 then

      insert into gtt_num_num_str_str_date_date(number_1,
                                                varchar2_1,
                                                varchar2_2)
      with wh_expl as (select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = c.chain
                          and w.org_hier_type  = 10
                          and w.wh             = w.physical_wh
                       union all
                       select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = a.area
                          and w.org_hier_type  = 20
                          and w.wh             = w.physical_wh
                       union all
                       select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = r.region
                          and w.org_hier_type  = 30
                          and w.wh             = w.physical_wh
                       union all
                       select w.org_hier_type,
                              w.wh,
                              1 company,
                              c.chain,
                              a.area,
                              r.region,
                              d.district
                         from chain c,
                              area a,
                              region r,
                              district d,
                              wh w
                        where d.region         = r.region
                          and r.area           = a.area
                          and a.chain          = c.chain
                          --
                          and w.org_hier_value = d.district
                          and w.org_hier_type  = 40
                          and w.wh             = w.physical_wh)
      select distinct v.wh,
             v.wh_name,
             'W'
        from wh_expl,
             v_wh v,
             table(cast(I_chains as OBJ_NUMERIC_ID_TABLE)) input_chains
       where wh_expl.wh            = v.physical_wh
         and wh_expl.chain         = value(input_chains)
         and wh_expl.org_hier_type in(10,20,30,40)
      union all
      select distinct v.wh,
             v.wh_name,
             'W'
        from wh w,
             v_wh v
       where w.org_hier_type = 1
         and w.physical_wh   = w.wh
         and w.wh            = v.physical_wh
         and v.wh           != v.physical_wh;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' CHAIN insert gtt_num_num_str_str_date_date - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   if OI_UTILITY.LOCK_STATS(O_error_message,
                            I_session_id,
                            L_tabname) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END EXPAND_LOC_TO_WH;
--------------------------------------------------------------------------------
FUNCTION REFRESH_TSF_PENDING_APPROVE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_transfer_count         IN OUT NUMBER,
                                     O_report_level           IN OUT VARCHAR2,
                                     I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                     I_tsf_nos                IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER  IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.REFRESH_TSF_PENDING_APPROVE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_tsf_apprv_pend_crit_days    rms_oi_system_options.ic_tsf_apprv_pend_critial_days%TYPE;
   L_ic_tsf_apprv_pend_crit_cnt     rms_oi_system_options.ic_tsf_apprv_pend_critial_cnt%TYPE;
   L_seven_day_count                NUMBER(10);
BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_tsf_apprv_pend_critial_days,
          oi.ic_tsf_apprv_pend_critial_cnt
     into L_ic_tsf_apprv_pend_crit_days,
          L_ic_tsf_apprv_pend_crit_cnt
     from rms_oi_system_options oi;

   delete from rms_oi_tsf_pend_approve
      where session_id  = I_session_id
        and tsf_no in (select value(tsf_nos)
                              from table(cast(I_tsf_nos as OBJ_NUMERIC_ID_TABLE)) tsf_nos);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_transfer_count
     from rms_oi_tsf_pend_approve
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_tsf_pend_approve oi
    where oi.session_id = I_session_id
      and L_vdate - oi.create_date >= L_ic_tsf_apprv_pend_crit_days + 7;

   if L_seven_day_count > 0 and O_transfer_count > L_ic_tsf_apprv_pend_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_transfer_count > L_ic_tsf_apprv_pend_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_TSF_PENDING_APPROVE;
--------------------------------------------------------------------------------
FUNCTION REFRESH_OVERDUE_SHIPMENT_TSF(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_transfer_count         IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_tsf_nos                IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.REFRESH_OVERDUE_SHIPMENT_TSF';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                       DATE := get_vdate;
   L_ic_overdue_ship_days        rms_oi_system_options.ic_overdue_ship_days%TYPE;
   L_ic_overdue_ship_count       rms_oi_system_options.ic_overdue_ship_count%TYPE;
   L_seven_day_count             NUMBER(10) := 0;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_overdue_ship_days,
          oi.ic_overdue_ship_count
     into L_ic_overdue_ship_days,
          L_ic_overdue_ship_count
     from rms_oi_system_options oi;


   delete from rms_oi_overdue_ship_tsf
      where session_id  = I_session_id
        and tsf_no in (select value(tsf_nos)
                              from table(cast(I_tsf_nos as OBJ_NUMERIC_ID_TABLE)) tsf_nos);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_transfer_count
     from rms_oi_overdue_ship_tsf
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_overdue_ship_tsf oi
    where oi.session_id = I_session_id
      and L_vdate - oi.not_after_date >= L_ic_overdue_ship_days + 7;

   if L_seven_day_count > 0 and O_transfer_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_transfer_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_OVERDUE_SHIPMENT_TSF;
--------------------------------------------------------------------------------
FUNCTION REFRESH_OVERDUE_SHIPMENT_RTV(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_rtv_count              IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_rtv_order_nos          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.REFRESH_OVERDUE_SHIPMENT_RTV';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                    DATE := get_vdate;
   L_ic_overdue_ship_days     rms_oi_system_options.ic_overdue_ship_days%TYPE;
   L_ic_overdue_ship_count    rms_oi_system_options.ic_overdue_ship_count%TYPE;
   L_seven_day_count          NUMBER(10) := 0;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_overdue_ship_days,
          oi.ic_overdue_ship_count
     into L_ic_overdue_ship_days,
          L_ic_overdue_ship_count
     from rms_oi_system_options oi;

   delete from rms_oi_overdue_ship_rtv
      where session_id  = I_session_id
        and rtv_order_no in (select value(rtv_order_nos)
                               from table(cast(I_rtv_order_nos as OBJ_NUMERIC_ID_TABLE)) rtv_order_nos);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_rtv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_rtv_count
     from rms_oi_overdue_ship_rtv
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_overdue_ship_rtv oi
    where oi.session_id = I_session_id
      and L_vdate - oi.not_after_date >= L_ic_overdue_ship_days + 7;

   if L_seven_day_count > 0 and O_rtv_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_rtv_count > L_ic_overdue_ship_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_OVERDUE_SHIPMENT_RTV;
--------------------------------------------------------------------------------
FUNCTION REFRESH_STOCK_COUNT_MISSING(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_stock_cnt_count        IN OUT NUMBER,
                                     O_report_level           IN OUT VARCHAR2,
                                     I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                     I_cycle_cnt_locs         IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.REFRESH_STOCK_COUNT_MISSING';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_miss_stk_count_crit_days    rms_oi_system_options.ic_miss_stk_count_crit_days%TYPE;
   L_ic_miss_stk_count_crit_cnt     rms_oi_system_options.ic_miss_stk_count_crit_cnt%TYPE;
   L_seven_day_count                NUMBER(10) := 0;
   L_critial_count                  NUMBER(10);

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_miss_stk_count_crit_days,
          oi.ic_miss_stk_count_crit_cnt
     into L_ic_miss_stk_count_crit_days,
          L_ic_miss_stk_count_crit_cnt
     from rms_oi_system_options oi;

   delete from rms_oi_missing_stock_count
      where session_id  = I_session_id
        and (cycle_count, loc) in (select cycle_cnt_locs.cycle_count, cycle_cnt_locs.location
                                     from table(cast(I_cycle_cnt_locs as RMS_OI_CYCLE_COUNT_LOC_TBL)) cycle_cnt_locs);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_missing_stock_count - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_stock_cnt_count
     from rms_oi_missing_stock_count
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_missing_stock_count oi
    where oi.session_id = I_session_id
      and L_vdate - oi.stocktake_date >= L_ic_miss_stk_count_crit_days + 7;

   if L_seven_day_count > 0 and O_stock_cnt_count > L_ic_miss_stk_count_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_stock_cnt_count > L_ic_miss_stk_count_crit_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_STOCK_COUNT_MISSING;
--------------------------------------------------------------------------------
FUNCTION REFRESH_STOCK_COUNT_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_stock_cnt_count        IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_cycle_cnt_locs         IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.REFRESH_STOCK_COUNT_VARIANCE';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                          DATE := get_vdate;
   L_ic_stk_count_var_pct           rms_oi_system_options.ic_stk_count_var_pct%TYPE;
   L_ic_stk_count_var_loc_cnt       rms_oi_system_options.ic_stk_count_var_loc_cnt%TYPE;
   L_seven_day_count                NUMBER(10) := 0;
   L_critial_count                  NUMBER(10);

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_stk_count_var_pct,
          oi.ic_stk_count_var_loc_cnt
     into L_ic_stk_count_var_pct,
          L_ic_stk_count_var_loc_cnt
     from rms_oi_system_options oi;

   delete from rms_oi_stock_count_variance
      where session_id  = I_session_id
        and (cycle_count, loc) in (select cycle_cnt_locs.cycle_count, cycle_cnt_locs.location
                                     from table(cast(I_cycle_cnt_locs as RMS_OI_CYCLE_COUNT_LOC_TBL)) cycle_cnt_locs);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stock_count_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_stock_cnt_count
     from rms_oi_stock_count_variance
    where session_id = I_session_id;

   select count(*)
     into L_seven_day_count
     from rms_oi_stock_count_variance oi
    where oi.session_id = I_session_id
      and L_vdate - oi.stocktake_date >= 7;

   if L_seven_day_count > 0 and O_stock_cnt_count > L_ic_stk_count_var_loc_cnt then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif L_seven_day_count > 0 or O_stock_cnt_count > L_ic_stk_count_var_loc_cnt then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_STOCK_COUNT_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION REFRESH_UNEXPECTED_INVENTORY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_item_loc_count         IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_itemlocs               IN     OBJ_ITEMLOC_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_CONTROL.REFRESH_UNEXPECTED_INVENTORY';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_ic_unexp_inv_inactive_ind    rms_oi_system_options.ic_unexp_inv_inactive_ind%TYPE;
   L_ic_unexp_inv_discontinue_ind rms_oi_system_options.ic_unexp_inv_discontinue_ind%TYPE;
   L_ic_unexp_inv_delete_ind      rms_oi_system_options.ic_unexp_inv_delete_ind%TYPE;
   L_ic_unexp_inv_critial_count   rms_oi_system_options.ic_unexp_inv_critial_count%TYPE;
   L_ic_unexp_inv_warn_count      rms_oi_system_options.ic_unexp_inv_warn_count%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select oi.ic_unexp_inv_inactive_ind,
          oi.ic_unexp_inv_discontinue_ind,
          oi.ic_unexp_inv_delete_ind,
          oi.ic_unexp_inv_critial_count,
          oi.ic_unexp_inv_warn_count
     into L_ic_unexp_inv_inactive_ind,
          L_ic_unexp_inv_discontinue_ind,
          L_ic_unexp_inv_delete_ind,
          L_ic_unexp_inv_critial_count,
          L_ic_unexp_inv_warn_count
     from rms_oi_system_options oi;

   --varchar2_1 -- item_loc status
   delete from gtt_6_num_6_str_6_date;
   if L_ic_unexp_inv_inactive_ind = 'Y' then
      insert into gtt_6_num_6_str_6_date(varchar2_1) values ('I');
   end if;
   if L_ic_unexp_inv_discontinue_ind = 'Y' then
      insert into gtt_6_num_6_str_6_date(varchar2_1) values ('C');
   end if;
   if L_ic_unexp_inv_delete_ind = 'Y' then
      insert into gtt_6_num_6_str_6_date(varchar2_1) values ('D');
   end if;

   merge into rms_oi_unexpected_inv target
   using (select I_session_id session_id,
                 il.item,
                 il.loc,
                 il.ranged_ind,
                 il.status
            from table(cast(I_itemlocs as OBJ_ITEMLOC_TBL)) i_item_locs,
                 item_loc il
           where il.item = i_item_locs.item
             and il.loc  = i_item_locs.loc) use_this
   on (    target.session_id = use_this.session_id
       and target.item       = use_this.item
       and target.loc        = use_this.loc)
   when matched then update
    set target.ranged_ind    = use_this.ranged_ind,
        target.status        = use_this.status;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_unexpected_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_unexpected_inv
      where session_id  = I_session_id
        and (item, loc) in (select i_item_locs.item,
                                   i_item_locs.loc
                              from table(cast(I_itemlocs as OBJ_ITEMLOC_TBL)) i_item_locs
                             minus
                            select i_item_locs.item,
                                   i_item_locs.loc
                              from table(cast(I_itemlocs as OBJ_ITEMLOC_TBL)) i_item_locs,
                                   item_loc il
                             where il.item = i_item_locs.item
                               and il.loc  = i_item_locs.loc
                               and ((il.ranged_ind    = 'N')
                                    or
                                    (il.ranged_ind     = 'Y' and
                                     il.status         in(select /*+ cardinality( gtt, 5 ) */ gtt.varchar2_1 
                                                            from gtt_6_num_6_str_6_date gtt))
                                    )
                              );

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_unexpected_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select count(*)
     into O_item_loc_count
     from rms_oi_unexpected_inv
    where session_id = I_session_id;

   if O_item_loc_count > L_ic_unexp_inv_critial_count then
      O_report_level := RMS_OI_INV_CONTROL.RED;
   elsif O_item_loc_count > L_ic_unexp_inv_warn_count then
      O_report_level := RMS_OI_INV_CONTROL.YELLOW;
   else
      O_report_level := RMS_OI_INV_CONTROL.GREEN;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_UNEXPECTED_INVENTORY;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_NEG_INV(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_NEG_INV';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_inv_ctl_neg_inv where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ctl_neg_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_NEG_INV;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OVERDUE_TSF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_OVERDUE_TSF';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_overdue_ship_tsf where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_tsf - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_OVERDUE_TSF;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OVERDUE_ALLOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_OVERDUE_ALLOC';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_overdue_ship_alloc where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_alloc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_OVERDUE_ALLOC;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OVERDUE_RTV(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_OVERDUE_RTV';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_overdue_ship_rtv where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_overdue_ship_rtv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_OVERDUE_RTV;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_UNEXPECTED_INV(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_UNEXPECTED_INV';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_unexpected_inv where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_unexpected_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_UNEXPECTED_INV;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_TSF_PENDING_APP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_TSF_PENDING_APP';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_tsf_pend_approve where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_tsf_pend_approve - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_TSF_PENDING_APP;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STKORD_PENDING(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_STKORD_PENDING';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_stk_ord_pend_close where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stk_ord_pend_close - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_STKORD_PENDING;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STKCNT_MISSING(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_STKCNT_MISSING';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_missing_stock_count where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_missing_stock_count - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_STKCNT_MISSING;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STKCNT_VARIANCE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_INV_CONTROL.DELETE_SESSION_STKCNT_VARIANCE';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_stock_count_variance where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_stock_count_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_STKCNT_VARIANCE;
--------------------------------------------------------------------------------
END RMS_OI_INV_CONTROL;
/
