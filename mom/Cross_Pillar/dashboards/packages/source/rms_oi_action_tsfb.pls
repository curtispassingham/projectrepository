CREATE OR REPLACE PACKAGE BODY RMS_OI_ACTION_TSF AS
--------------------------------------------------------------------------------
FUNCTION CALL_XTSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_tsf_no           OUT TSFHEAD.TSF_NO%TYPE,
                   I_tsf_object    IN     RMS_OI_TSF_TSFHEAD_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION APPROVE_TRANSFERS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result                     OUT RMS_OI_TSF_STATUS_REC,
                           I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                           I_tsf_nos                 IN     OBJ_NUMERIC_ID_TABLE,
                           I_appr_intracompany_priv  IN     VARCHAR2,
                           I_appr_intercompany_priv  IN     VARCHAR2)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_ACTION_TSF.APPROVE_TRANSFERS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_tsf_no          TSFHEAD.TSF_NO%TYPE;
   L_tsf_entity_type VARCHAR2(1); 
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_system_options  SYSTEM_OPTIONS%ROWTYPE;

   cursor C_TSF is
      select case when L_system_options.intercompany_transfer_basis = 'T' then
                case when th.leg_1_to_loc_entity is null or
                          nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) is null or
                          th.leg_1_to_loc_entity  = nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) then
                        'A'
                     else
                        'E'
                     end
             else 
                case when th.leg_1_to_loc_sob_id is null or
                          nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) is null or
                          th.leg_1_to_loc_sob_id  = nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) then
                        'A'
                     else
                        'E'
                     end
             end tsf_entity_type
        from v_transfer_head th
       where th.tsf_no = L_tsf_no;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if I_session_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_session_id',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   --nothing to do
   if I_tsf_nos is null or I_tsf_nos.count < 1 then
      return OI_UTILITY.SUCCESS;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   O_result := RMS_OI_TSF_STATUS_REC(0,
                                     OBJ_NUMERIC_ID_TABLE(),
                                     0,
                                     RMS_OI_TSF_FAIL_TBL());

   for i in 1 .. I_tsf_nos.count loop
      L_tsf_no := I_tsf_nos(i);
 
      SAVEPOINT tsf_savepoint;

      open C_TSF;
      fetch C_TSF into L_tsf_entity_type;
      close C_TSF;

      --check approval privilege
      if (L_tsf_entity_type = 'A' and I_appr_intracompany_priv = 'N' or
          L_tsf_entity_type = 'E' and I_appr_intercompany_priv = 'N') then
         L_error_message := SQL_LIB.CREATE_MSG('USER_PRIVS',
                                                NULL,
                                                NULL,
                                                NULL);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_tsf_count := O_result.fail_tsf_count + 1;
         O_result.fail_tsf_tbl.extend;
         O_result.fail_tsf_tbl(O_result.fail_tsf_tbl.count) := RMS_OI_TSF_FAIL_REC(L_tsf_no,
                                                                                   L_error_message);
         ROLLBACK TO SAVEPOINT tsf_savepoint;
         continue;
      end if;

      if TSF_STATUS_SQL.APPROVE_TRANSFER(L_error_message,
                                         L_tsf_no) = TSF_CONSTANTS.FAILURE then

         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_tsf_count := O_result.fail_tsf_count + 1;
         O_result.fail_tsf_tbl.extend;
         O_result.fail_tsf_tbl(O_result.fail_tsf_tbl.count) := RMS_OI_TSF_FAIL_REC(L_tsf_no,
                                                                                   L_error_message);
         ROLLBACK TO SAVEPOINT tsf_savepoint;
         continue;
      else
         O_result.success_tsf_count := O_result.success_tsf_count + 1;
         O_result.success_tsf_tbl.extend;
         O_result.success_tsf_tbl(O_result.success_tsf_tbl.count) := L_tsf_no;
      end if;
   end loop;

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END APPROVE_TRANSFERS;
--------------------------------------------------------------------------------
FUNCTION DELETE_TRANSFERS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_result                      OUT RMS_OI_TSF_STATUS_REC,
                          I_session_id               IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_tsf_nos                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_maint_intracompany_priv  IN     VARCHAR2,
                          I_maint_intercompany_priv  IN     VARCHAR2)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_ACTION_TSF.DELETE_TRANSFERS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_tsf_no          TSFHEAD.TSF_NO%TYPE;
   L_tsf_entity_type VARCHAR2(1); 
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_system_options  SYSTEM_OPTIONS%ROWTYPE;

   cursor C_TSF is
      select case when L_system_options.intercompany_transfer_basis = 'T' then
                case when th.leg_1_to_loc_entity is null or
                          nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) is null or
                          th.leg_1_to_loc_entity  = nvl(th.leg_2_from_loc_entity,th.leg_1_from_loc_entity) then
                        'A'
                     else
                        'E'
                     end
             else 
                case when th.leg_1_to_loc_sob_id is null or
                          nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) is null or
                          th.leg_1_to_loc_sob_id  = nvl(th.leg_2_from_loc_sob_id,th.leg_1_from_loc_sob_id) then
                        'A'
                     else
                        'E'
                     end
             end tsf_entity_type
        from v_transfer_head th
       where th.tsf_no = L_tsf_no;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if I_session_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_session_id',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   --nothing to do
   if I_tsf_nos is null or I_tsf_nos.count < 1 then
      return OI_UTILITY.SUCCESS;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   O_result := RMS_OI_TSF_STATUS_REC(0,
                                     OBJ_NUMERIC_ID_TABLE(),
                                     0,
                                     RMS_OI_TSF_FAIL_TBL());

   for i in 1 .. I_tsf_nos.count loop
      L_tsf_no := I_tsf_nos(i);
 
      SAVEPOINT tsf_savepoint;

      open C_TSF;
      fetch C_TSF into L_tsf_entity_type;
      close C_TSF;

      --check approval privilege
      if (L_tsf_entity_type = 'A' and I_maint_intracompany_priv = 'N' or
          L_tsf_entity_type = 'E' and I_maint_intercompany_priv = 'N') then
         L_error_message := SQL_LIB.CREATE_MSG('USER_PRIVS',
                                                NULL,
                                                NULL,
                                                NULL);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_tsf_count := O_result.fail_tsf_count + 1;
         O_result.fail_tsf_tbl.extend;
         O_result.fail_tsf_tbl(O_result.fail_tsf_tbl.count) := RMS_OI_TSF_FAIL_REC(L_tsf_no,
                                                                                   L_error_message);
         ROLLBACK TO SAVEPOINT tsf_savepoint;
         continue;
      end if;

      --Once created, the transfer cannot be deleted from the database.
      --It will be updated to 'D'elete status.
      if TSF_STATUS_SQL.UPDATE_TO_DELETE_STATUS(L_error_message,
                                                L_tsf_no) = TSF_CONSTANTS.FAILURE then

         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_tsf_count := O_result.fail_tsf_count + 1;
         O_result.fail_tsf_tbl.extend;
         O_result.fail_tsf_tbl(O_result.fail_tsf_tbl.count) := RMS_OI_TSF_FAIL_REC(L_tsf_no,
                                                                                   L_error_message);
         ROLLBACK TO SAVEPOINT tsf_savepoint;
         continue;
      else
         O_result.success_tsf_count := O_result.success_tsf_count + 1;
         O_result.success_tsf_tbl.extend;
         O_result.success_tsf_tbl(O_result.success_tsf_tbl.count) := L_tsf_no;
      end if;
   end loop;

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END DELETE_TRANSFERS;
--------------------------------------------------------------------------------
FUNCTION CREATE_TRANSFER(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_no             OUT TSFHEAD.TSF_NO%TYPE,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_tsf_object      IN     RMS_OI_TSF_TSFHEAD_REC)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_ACTION_TSF.CREATE_TRANSFER';
   L_start_time  TIMESTAMP := SYSTIMESTAMP;
   L_exists      VARCHAR2(1) := NULL;

   cursor C_FINISHER is
      select 'x'
        from wh
       where wh in (I_tsf_object.from_loc, I_tsf_object.to_loc)
         and finisher_ind = 'Y';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if I_session_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_session_id',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.tsf_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object.tsf_type',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object.from_loc_type',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object.from_loc',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object.to_loc_type',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object.to_loc',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.TSFDTL_TBL is NULL or I_tsf_object.TSFDTL_TBL.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_object.TSFDTL_TBL',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   --Only include transfer types that can be created in RMS transfer screen.
   --CO transfers are not supported from the dashboard.
   if I_tsf_object.tsf_type NOT in (TSF_CONSTANTS.TSF_TYPE_AD, 
                                    TSF_CONSTANTS.TSF_TYPE_CF, 
                                    TSF_CONSTANTS.TSF_TYPE_MR, 
                                    TSF_CONSTANTS.TSF_TYPE_RAC, 
                                    TSF_CONSTANTS.TSF_TYPE_RV,
                                    TSF_CONSTANTS.TSF_TYPE_IC, 
                                    TSF_CONSTANTS.TSF_TYPE_BT) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_TSF_TYPE',
                                             I_tsf_object.tsf_type,
                                             'AD, CF, MR, RAC, RV, IC, or BT',
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   --Only single leg transfer can be created through the dashboard
   --Both from-loc and to-loc can only be a store or a warehouse, NOT finishers
   if I_tsf_object.from_loc_type NOT in (TSF_CONSTANTS.LOC_TYPE_S, TSF_CONSTANTS.LOC_TYPE_W) or
      I_tsf_object.to_loc_type NOT in (TSF_CONSTANTS.LOC_TYPE_S, TSF_CONSTANTS.LOC_TYPE_W) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_LOC_TYPE_S_W',
                                             NULL,
                                             NULL,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_tsf_object.from_loc_type = 'W' or I_tsf_object.to_loc_type = 'W' then
      open C_FINISHER;
      fetch C_FINISHER into L_exists;
      close C_FINISHER;

      --either from-loc or to-loc is an internal finisher
      if L_exists = 'x' then
         O_error_message := SQL_LIB.CREATE_MSG('WH_FINISHER_INV_LOC',
                                                NULL,
                                                NULL,
                                                NULL);
         RAISE OI_UTILITY.PROGRAM_ERROR;
      end if; 
   end if;

   if CALL_XTSF(O_error_message,
                O_tsf_no,
                I_tsf_object) = OI_UTILITY.FAILURE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
END CREATE_TRANSFER;
--------------------------------------------------------------------------------
FUNCTION CALL_XTSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_tsf_no           OUT TSFHEAD.TSF_NO%TYPE,
                   I_tsf_object    IN     RMS_OI_TSF_TSFHEAD_REC)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'RMS_OI_ACTION_TSF.CALL_XTSF';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_return_code    VARCHAR2(10) := 'FALSE';
   L_new_tsf_no     TSFHEAD.TSF_NO%TYPE;
   L_user           VARCHAR2(30) := GET_USER;

   L_xtsfdesc_rec   "RIB_XTsfDesc_REC";
   L_message_type   VARCHAR2(50);
   L_status_code    VARCHAR2(2);

   cursor C_CONV_TO_RIB is
      select "RIB_XTsfDesc_REC"( 0  --rib_oid
                               , L_new_tsf_no
                               , I_tsf_object.from_loc_type
                               , I_tsf_object.from_loc
                               , I_tsf_object.to_loc_type
                               , I_tsf_object.to_loc
                               , I_tsf_object.delivery_date
                               , I_tsf_object.dept
                               , NULL    --routing_code
                               , 'N'     --freight_code, default to 'N'ormal
                               , I_tsf_object.tsf_type
                               , CAST(MULTISET(select "RIB_XTsfDtl_REC"( 0 --rib_oid
                                                                       , input_dtl.item
                                                                       , input_dtl.tsf_qty
                                                                       , NULL   --supp_pack_size
                                                                       , NULL   --inv_status
                                                                       , NULL)  --unit_cost
                                                 from table(cast(I_tsf_object.TSFDTL_TBL as RMS_OI_TSF_TSFDTL_TBL)) input_dtl
                                                where I_tsf_object.inventory_type = 'A'
                                                union all
                                                select "RIB_XTsfDtl_REC"( 0 --rib_oid
                                                                       , qty.item
                                                                       , qty.qty
                                                                       , NULL   --supp_pack_size
                                                                       , qty.inv_status 
                                                                       , NULL)  --unit_cost
                                                 from inv_status_qty qty, 
                                                      table(cast(I_tsf_object.TSFDTL_TBL as RMS_OI_TSF_TSFDTL_TBL)) input_dtl
                                                where I_tsf_object.inventory_type = 'U'
                                                  and qty.location = I_tsf_object.from_loc
                                                  and qty.loc_type = I_tsf_object.from_loc_type
                                                  and qty.item = input_dtl.item ) as "RIB_XTsfDtl_TBL")
                               , TSF_CONSTANTS.TSF_STATUS_I  --input status
                               , L_user    --user_id
                               , NULL      --comment_desc
                               , NULL      --context_type
                               , NULL)     --context_value      
        from dual;

BEGIN

   NEXT_TRANSFER_NUMBER(L_new_tsf_no,
                        L_return_code,
                        O_error_message);
   if L_return_code = 'FALSE' then
      return OI_UTILITY.FAILURE;
   end if;

   --Build a XTsfDesc RIB message and call the consume function to create a transfer in Input status.
   --For Available inventory, transfer qty comes from the dashboard input I_tsf_object.
   --For Unavailable inventory, transfer qty is by inv_status and comes from inv_status_qty. 
   open C_CONV_TO_RIB;
   fetch C_CONV_TO_RIB into L_xtsfdesc_rec;
   close C_CONV_TO_RIB;

   L_message_type := RMSSUB_XTSF.LP_cre_type;

   RMSSUB_XTSF.CONSUME(L_status_code,
                       O_error_message,
                       L_xtsfdesc_rec,
                       L_message_type);

   if L_status_code != 'S' then
      return OI_UTILITY.FAILURE;
   end if;

   O_tsf_no := L_new_tsf_no;

   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END CALL_XTSF;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_TSF;
/
