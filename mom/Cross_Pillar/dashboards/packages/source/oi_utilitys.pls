CREATE OR REPLACE PACKAGE OI_UTILITY AS
--------------------------------------------------------------------------------
SUCCESS      CONSTANT NUMBER(1) := 1;
FAILURE      CONSTANT NUMBER(1) := 0;
----
PROGRAM_ERROR EXCEPTION;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
PROCEDURE LOG_TIME(I_log_message    IN VARCHAR2,
                   I_start_time     IN TIMESTAMP);
--------------------------------------------------------------------------------
FUNCTION LOCK_STATS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                    I_tabname         IN     USER_TAB_STATISTICS.TABLE_NAME%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_MERCH_HIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_depts          IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes        IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses     IN     OBJ_NUMERIC_ID_TABLE,
                          I_input_required IN     BOOLEAN DEFAULT TRUE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_ITEMS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                     I_depts             IN     OBJ_NUMERIC_ID_TABLE,
                     I_classes           IN     OBJ_NUMERIC_ID_TABLE,
                     I_subclasses        IN     OBJ_NUMERIC_ID_TABLE,
                     I_supplier_sites    IN     OBJ_NUMERIC_ID_TABLE,
                     I_origin_countries  IN     OBJ_VARCHAR_ID_TABLE,
                     I_brands            IN     OBJ_VARCHAR_ID_TABLE,
                     I_forecast_ind      IN     VARCHAR2,
                     I_input_required    IN     BOOLEAN DEFAULT TRUE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_REFRESH_ITEMS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_supplier_sites    IN     OBJ_NUMERIC_ID_TABLE,
                             I_origin_countries  IN     OBJ_VARCHAR_ID_TABLE,
                             I_brands            IN     OBJ_VARCHAR_ID_TABLE,
                             I_forecast_ind      IN     VARCHAR2,
                             I_items             IN     OBJ_VARCHAR_ID_TABLE,
                             I_item_type         IN     item_master.alc_item_type%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_ORDERS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_depts             IN     OBJ_NUMERIC_ID_TABLE,
                      I_classes           IN     OBJ_NUMERIC_ID_TABLE,
                      I_subclasses        IN     OBJ_NUMERIC_ID_TABLE,
                      I_supplier_sites    IN     OBJ_NUMERIC_ID_TABLE,
                      I_brands            IN     OBJ_VARCHAR_ID_TABLE,
                      I_order_contexts    IN     OBJ_VARCHAR_ID_TABLE,
                      I_stores            IN     OBJ_NUMERIC_ID_TABLE,
                      I_origin_countries  IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_LOCATIONS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_chains          IN     OBJ_NUMERIC_ID_TABLE,
                         I_areas           IN     OBJ_NUMERIC_ID_TABLE,
                         I_locations       IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_STORES(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_location_group_type  IN OUT VARCHAR2,
                      I_session_id           IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_areas                IN     OBJ_NUMERIC_ID_TABLE,
                      I_stores               IN     OBJ_NUMERIC_ID_TABLE,
                      I_store_grade_group_id IN     STORE_GRADE_GROUP.STORE_GRADE_GROUP_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_SESSION_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_session_id       OUT OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ID_LOG(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_program              IN      VARCHAR2);
--------------------------------------------------------------------------------
FUNCTION PURGE_RMS_OI_TABLES(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SETUP_ORG_UNIT_LOCATIONS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id     IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_set_of_books   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_org_units      IN     OBJ_NUMERIC_ID_TABLE,
                                  I_locations      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END OI_UTILITY;
/
