CREATE OR REPLACE PACKAGE RMS_OI_ACTION_ITEMLOC AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name : UPDATE_STATUS
--Purpose       : This function updates a collection of item locations to
--                a new status and a new ranged indicator.
--------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result               OUT RMS_OI_ITEMLOC_STATUS_REC,
                       I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                       I_itemlocs          IN     OBJ_ITEMLOC_TBL,
                       I_new_status        IN     ITEM_LOC.STATUS%TYPE,
                       I_new_ranged_ind    IN     ITEM_LOC.RANGED_IND%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_ITEMLOC;
/
