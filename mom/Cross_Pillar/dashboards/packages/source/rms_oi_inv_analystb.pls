CREATE OR REPLACE PACKAGE BODY RMS_OI_INV_ANALYST AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION SETUP_WHS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_session_id           IN     oi_session_id_log.session_id%TYPE,
                   I_item                 IN     item_master.item%TYPE,
                   I_item_type            IN     item_master.alc_item_type%TYPE,
                   I_agg_diff_1           IN     item_master.diff_1%TYPE,
                   I_agg_diff_2           IN     item_master.diff_2%TYPE,
                   I_agg_diff_3           IN     item_master.diff_3%TYPE,
                   I_agg_diff_4           IN     item_master.diff_4%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POP_ILSOH_CONTEXT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_session_id              IN     oi_session_id_log.session_id%TYPE,
                           I_item                    IN     item_master.item%TYPE,
                           I_item_type               IN     item_master.alc_item_type%TYPE,
                           I_agg_diff_1              IN     item_master.diff_1%TYPE,
                           I_agg_diff_2              IN     item_master.diff_2%TYPE,
                           I_agg_diff_3              IN     item_master.diff_3%TYPE,
                           I_agg_diff_4              IN     item_master.diff_4%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POP_ILSOH_FILTER(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id              IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_FORECAST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_session_id              IN     oi_session_id_log.session_id%TYPE,
                        I_start_date              IN     DATE,
                        I_end_date                IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_FORECAST_HIST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id              IN     oi_session_id_log.session_id%TYPE,
                             I_start_date              IN     DATE,
                             I_end_date                IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_ON_ORDER(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_session_id              IN     oi_session_id_log.session_id%TYPE,
                        I_forward_date            IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_CO_INBOUND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id              IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_WH_CROSSLINK(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_session_id              IN     oi_session_id_log.session_id%TYPE,
                            I_forward_date            IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_ALLOC_INBOUND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id              IN     oi_session_id_log.session_id%TYPE,
                             I_forward_date            IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_ALLOC_OUTBOUND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_session_id              IN     oi_session_id_log.session_id%TYPE,
                              I_forward_date            IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_SALES_HIST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id              IN     oi_session_id_log.session_id%TYPE,
                          I_history_date            IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION MERGE_UNIT_RETAIL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_session_id              IN     oi_session_id_log.session_id%TYPE,
                           I_store_count             IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ROLLUP_INV_VAR_FORECAST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id              IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_ORDER_ERROR_REC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_order_error_rec     IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                  I_session_id          IN     oi_session_id_log.session_id%TYPE,
                                  I_order_date_key_tbl  IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_ORDER_ITEM_ERROR_REC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_order_error_rec     IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                       I_session_id          IN     oi_session_id_log.session_id%TYPE,
                                       I_order_date_key_tbl  IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_ORD_ITM_ERROR_REC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_order_error_rec     IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                    I_session_id          IN     oi_session_id_log.session_id%TYPE,
                                    I_order_date_key_tbl  IN     OBJ_NUM_STR_STR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION STORE_INVENTORY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result                  IN OUT RMS_OI_STORE_INV_TBL,
                         I_session_id              IN     oi_session_id_log.session_id%TYPE,
                         I_item                    IN     item_master.item%TYPE,
                         I_item_type               IN     item_master.alc_item_type%TYPE,
                         I_agg_diff_1              IN     item_master.diff_1%TYPE,
                         I_agg_diff_2              IN     item_master.diff_2%TYPE,
                         I_agg_diff_3              IN     item_master.diff_3%TYPE,
                         I_agg_diff_4              IN     item_master.diff_4%TYPE,
                         I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                         I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                         I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.STORE_INVENTORY';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

   L_location_group_type VARCHAR2(1);

   L_vdate               DATE         := get_vdate;
   L_forward_date        DATE         := null;

   cursor c_store_inv is
     select RMS_OI_STORE_INV_REC(i.location_group_type,
                                 i.location_group_id,
                                 i.location_group_name,
                                 i.stock_on_hand,
                                 i.inbound_qty,
                                 i.reserved_qty,
                                 i.cust_back_order_qty,
                                 i.non_sellable_qty,
                                 i.forecast_qty,
                                 i.available_qty)
       from (select L_location_group_type location_group_type,
                    oi.grouping_loc_id location_group_id,
                    oi.grouping_loc_name location_group_name,
                    sum(oi.stock_on_hand) stock_on_hand,
                    sum(in_transit_qty + pack_comp_intran + on_order_qty + pl_tsf_inbound + alloc_inbound +
                        tsf_expected_qty + pack_comp_exp) inbound_qty,
                    sum(tsf_reserved_qty + pack_comp_resv + rtv_qty + customer_resv + pack_comp_cust_resv + co_inbound) reserved_qty,
                    sum(customer_backorder + pack_comp_cust_back) cust_back_order_qty,
                    sum(non_sellable_qty + pack_comp_non_sellable) non_sellable_qty,
                    sum(forecast_sales) forecast_qty,
                    --
                    sum(oi.stock_on_hand) -
                    (sum(tsf_reserved_qty + pack_comp_resv + rtv_qty + customer_resv + pack_comp_cust_resv + co_inbound)) -
                    (sum(customer_backorder + pack_comp_cust_back)) -
                    (sum(non_sellable_qty + pack_comp_non_sellable)) available_qty
               from rms_oi_inv_ana_inv_gtt oi
              where session_id = I_session_id
              group by oi.grouping_loc_id,
                       oi.grouping_loc_name) i
      order by i.inbound_qty + i.available_qty desc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   delete from rms_oi_inv_ana_inv_gtt where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select d.eow_date + 21
     into L_forward_date
     from day_level_calendar d 
    where d.day = L_vdate;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if POP_ILSOH_CONTEXT(O_error_message,
                        I_session_id,
                        I_item,
                        I_item_type,
                        I_agg_diff_1,
                        I_agg_diff_2,
                        I_agg_diff_3,
                        I_agg_diff_4) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_FORECAST(O_error_message,
                     I_session_id,
                     L_vdate,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ON_ORDER(O_error_message,
                     I_session_id,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_CO_INBOUND(O_error_message,
                       I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_WH_CROSSLINK(O_error_message,
                         I_session_id,
                         L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ALLOC_INBOUND(O_error_message,
                          I_session_id,
                          L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   open c_store_inv;
   fetch c_store_inv bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_store_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close c_store_inv;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END STORE_INVENTORY;
--------------------------------------------------------------------------------
FUNCTION WH_INVENTORY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_result                  IN OUT RMS_OI_WH_INV_TBL,
                      I_session_id              IN     oi_session_id_log.session_id%TYPE,
                      I_item                    IN     item_master.item%TYPE,
                      I_item_type               IN     item_master.alc_item_type%TYPE,
                      I_agg_diff_1              IN     item_master.diff_1%TYPE,
                      I_agg_diff_2              IN     item_master.diff_2%TYPE,
                      I_agg_diff_3              IN     item_master.diff_3%TYPE,
                      I_agg_diff_4              IN     item_master.diff_4%TYPE,
                      I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                      I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                      I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.WH_INVENTORY';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_location_group_type VARCHAR2(1);
   L_vdate               DATE         := get_vdate;
   L_forward_date        DATE         := null;

   cursor c_wh_inv is
     select RMS_OI_WH_INV_REC(i2.wh,
                              i2.wh_name,
                              i2.stock_on_hand,
                              i2.inbound_qty,
                              i2.reserved_qty,
                              i2.cust_back_order_qty,
                              i2.non_sellable_qty,
                              i2.available_qty)
       from (select i.wh,
                    i.wh_name,
                    i.stock_on_hand,
                    i.inbound_qty,
                    i.reserved_qty,
                    i.cust_back_order_qty,
                    i.non_sellable_qty,
                    i.available_qty,
                    rank() over (order by i.available_qty desc, i.wh_name) avail_rank
               from (select oi.loc wh,
                            oi.grouping_loc_name wh_name,
                            sum(oi.stock_on_hand + oi.pack_comp_soh) stock_on_hand,
                            sum(in_transit_qty + pack_comp_intran + on_order_qty + pl_tsf_inbound + alloc_inbound +
                                tsf_expected_qty + pack_comp_exp) inbound_qty,
                            sum(tsf_reserved_qty + pack_comp_resv + rtv_qty + customer_resv + pack_comp_cust_resv + 
                                co_inbound + alloc_outbound) reserved_qty,
                            sum(customer_backorder + pack_comp_cust_back) cust_back_order_qty,
                            sum(non_sellable_qty + pack_comp_non_sellable) non_sellable_qty,
                            --
                            sum(oi.stock_on_hand + oi.pack_comp_soh) -
                            (sum(tsf_reserved_qty + pack_comp_resv + rtv_qty + customer_resv + pack_comp_cust_resv + 
                                 co_inbound + alloc_outbound)) -
                            (sum(customer_backorder + pack_comp_cust_back)) -
                            (sum(non_sellable_qty + pack_comp_non_sellable)) available_qty
                    from rms_oi_inv_ana_inv_gtt oi
                   where session_id = I_session_id
                   group by oi.loc,
                            oi.grouping_loc_name) i) i2
      where i2.avail_rank <= 5
      order by i2.inbound_qty + i2.available_qty desc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   delete from rms_oi_inv_ana_inv_gtt where session_id = I_session_id;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select d.eow_date + 21
     into L_forward_date
     from day_level_calendar d 
    where d.day = L_vdate;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if SETUP_WHS(O_error_message,
                I_session_id,
                I_item,
                I_item_type,
                I_agg_diff_1,
                I_agg_diff_2,
                I_agg_diff_3,
                I_agg_diff_4) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if POP_ILSOH_CONTEXT(O_error_message,
                        I_session_id,
                        I_item,
                        I_item_type,
                        I_agg_diff_1,
                        I_agg_diff_2,
                        I_agg_diff_3,
                        I_agg_diff_4) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ON_ORDER(O_error_message,
                     I_session_id,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ALLOC_OUTBOUND(O_error_message,
                           I_session_id,
                           L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ALLOC_INBOUND(O_error_message,
                          I_session_id,
                          L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   open c_wh_inv;
   fetch c_wh_inv bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_wh_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close c_wh_inv;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END WH_INVENTORY;
--------------------------------------------------------------------------------
FUNCTION OPEN_ORDERS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_session_id              IN     oi_session_id_log.session_id%TYPE,
                     I_item                    IN     item_master.item%TYPE,
                     I_item_type               IN     item_master.alc_item_type%TYPE,
                     I_agg_diff_1              IN     item_master.diff_1%TYPE,
                     I_agg_diff_2              IN     item_master.diff_2%TYPE,
                     I_agg_diff_3              IN     item_master.diff_3%TYPE,
                     I_agg_diff_4              IN     item_master.diff_4%TYPE,
                     I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                     I_order_types             IN     OBJ_VARCHAR_ID_TABLE,
                     I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                     I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                     I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.OPEN_ORDERS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate               DATE := get_vdate;
   L_forward_date        DATE;
   L_location_group_type VARCHAR2(1);
   L_check_order_type    VARCHAR2(1) := 'N';

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select d.eow_date + 21
     into L_forward_date
     from day_level_calendar d
    where d.day = L_vdate;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete from numeric_id_gtt;
   if I_supplier_sites is not null and I_supplier_sites.count > 0 then
      insert into numeric_id_gtt(numeric_id)
      select value(input_sups) from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups;
   else
      insert into numeric_id_gtt(numeric_id)
      select supplier from sups;
   end if;

   if I_order_types is not null and I_order_types.count > 0 then
      L_check_order_type := 'Y';
   end if;

   delete from rms_oi_inv_ana_open_order where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ana_open_order areas - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_inv_ana_open_order (session_id,
                                          order_no,
                                          supplier_site,
                                          sup_name,
                                          not_before_date,
                                          not_after_date,
                                          otb_eow_date,
                                          qty_ordered,
                                          qty_received,
                                          order_type,
                                          wf_order_no)
   select I_session_id,
          oh.order_no,
          oh.supplier,
          vs.sup_name,
          oh.not_before_date,
          oh.not_after_date,
          oh.otb_eow_date,
          sum(ol.qty_ordered) qty_ordered,
          sum(ol.qty_received) qty_received,
          oh.order_type,
          oh.wf_order_no
     from gtt_num_num_str_str_date_date loc_gtt,
          ordloc ol,
          v_sups vs,
          ordhead oh,
          numeric_id_gtt sup_gtt
    where I_item_type not     in('STYLE','FA')
      and ol.item             = I_item
      and ol.location         = loc_gtt.number_1
      and ol.order_no         = oh.order_no
      and oh.supplier         = sup_gtt.numeric_id
      and oh.supplier         = vs.supplier
      and oh.not_after_date  >= L_vdate
      and oh.not_after_date  <= L_forward_date
      and oh.status             in('A','S','W')
      and oh.orig_approval_date is not null
      and oh.order_type         != 'CO'
      and (L_check_order_type = 'N'  or
           oh.order_type      in(select value(input_order_types) 
                                  from table(cast(I_order_types as OBJ_VARCHAR_ID_TABLE)) input_order_types))
    group by oh.order_no,
             oh.supplier,
             vs.sup_name,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             oh.order_type,
             oh.wf_order_no
   union all
   select I_session_id,
          oh.order_no,
          oh.supplier,
          vs.sup_name,
          oh.not_before_date,
          oh.not_after_date,
          oh.otb_eow_date,
          sum(ol.qty_ordered) qty_ordered,
          sum(ol.qty_received) qty_received,
          oh.order_type,
          oh.wf_order_no
     from gtt_num_num_str_str_date_date loc_gtt,
          ordloc ol,
          item_master im,
          v_sups vs,
          ordhead oh,
          numeric_id_gtt sup_gtt
    where I_item_type         = 'STYLE'
      and im.item_parent      = I_item
      and ol.item             = im.item
      and ol.location         = loc_gtt.number_1
      and ol.order_no         = oh.order_no
      and oh.supplier         = sup_gtt.numeric_id
      and oh.supplier         = vs.supplier
      and oh.not_after_date  >= L_vdate
      and oh.not_after_date  <= L_forward_date
      and oh.status             in('A','S','W')
      and oh.orig_approval_date is not null
      and oh.order_type         != 'CO'
      and (L_check_order_type = 'N'  or
           oh.order_type      in(select value(input_order_types)
                                   from table(cast(I_order_types as OBJ_VARCHAR_ID_TABLE)) input_order_types))
    group by oh.order_no,
             oh.supplier,
             vs.sup_name,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             oh.order_type,
             oh.wf_order_no
   union all
   select I_session_id,
          oh.order_no,
          oh.supplier,
          vs.sup_name,
          oh.not_before_date,
          oh.not_after_date,
          oh.otb_eow_date,
          sum(ol.qty_ordered) qty_ordered,
          sum(ol.qty_received) qty_received,
          oh.order_type,
          oh.wf_order_no
     from gtt_num_num_str_str_date_date loc_gtt,
          item_master im,
          ordloc ol,
          ordhead oh,
          numeric_id_gtt sup_gtt,
          v_sups vs
    where I_item_type           = 'FA'
      and im.item_parent        = I_item
      and nvl(im.diff_1,'NU|L') = nvl(I_agg_diff_1, nvl(im.diff_1,'NU|L'))
      and nvl(im.diff_2,'NU|L') = nvl(I_agg_diff_2, nvl(im.diff_2,'NU|L'))
      and nvl(im.diff_3,'NU|L') = nvl(I_agg_diff_3, nvl(im.diff_3,'NU|L'))
      and nvl(im.diff_4,'NU|L') = nvl(I_agg_diff_4, nvl(im.diff_4,'NU|L'))
      and ol.item               = im.item
      and ol.location           = loc_gtt.number_1
      and ol.order_no           = oh.order_no
      and oh.supplier           = sup_gtt.numeric_id
      and oh.supplier           = vs.supplier
      and oh.not_after_date    >= L_vdate
      and oh.not_after_date    <= L_forward_date
      and oh.status             in('A','S','W')
      and oh.orig_approval_date is not null
      and oh.order_type         != 'CO'
      and (L_check_order_type   = 'N'  or
           oh.order_type        in(select value(input_order_types)
                                     from table(cast(I_order_types as OBJ_VARCHAR_ID_TABLE)) input_order_types))
    group by oh.order_no,
             oh.supplier,
             vs.sup_name,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             oh.order_type,
             oh.wf_order_no;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_open_order areas - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END OPEN_ORDERS;
--------------------------------------------------------------------------------
FUNCTION LEAD_TIMES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result                  IN OUT RMS_OI_LEAD_TIME_TBL,
                    I_session_id              IN     oi_session_id_log.session_id%TYPE,
                    I_item                    IN     item_master.item%TYPE,
                    I_item_type               IN     item_master.alc_item_type%TYPE,
                    I_agg_diff_1              IN     item_master.diff_1%TYPE,
                    I_agg_diff_2              IN     item_master.diff_2%TYPE,
                    I_agg_diff_3              IN     item_master.diff_3%TYPE,
                    I_agg_diff_4              IN     item_master.diff_4%TYPE,
                    I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                    I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                    I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.LEAD_TIMES';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate               DATE := get_vdate;
   L_history_date        DATE;
   L_location_group_type VARCHAR2(1);

   cursor c_lead_time is
      select RMS_OI_LEAD_TIME_REC(avg(plan_sup_lead_time),
                                  avg(plan_wh_lead_time),
                                  avg(plan_review_lead_time),
                                  avg(actual_sup_lead_time),
                                  avg(actual_wh_lead_time),
                                  avg(actual_review_lead_time))
        from rms_oi_lead_time_gtt;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select d.eow_date - 27
     into L_history_date
     from day_level_calendar d
    where d.day = L_vdate;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_lead_time_gtt;
   insert into rms_oi_lead_time_gtt (item,
                                     loc,
                                     plan_sup_lead_time,
                                     plan_wh_lead_time,
                                     plan_review_lead_time,
                                     actual_sup_lead_time,
                                     actual_wh_lead_time,
                                     actual_review_lead_time)
   select I_item,
          gtt.number_1 loc,
          nvl(isc.lead_time, 0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time, 0)) plan_sup_lead_time,
          null plan_wh_lead_time,
          null plan_review_lead_time,
          null actual_sup_lead_time,
          null actual_wh_lead_time,
          null actual_review_lead_time
     from gtt_num_num_str_str_date_date gtt,
          item_supp_country isc,
          item_supp_country_loc iscl,
          item_master im
    where I_item_type             not in('STYLE','FA')
      and isc.item                = I_item
      and isc.primary_supp_ind    = 'Y'
      and isc.primary_country_ind = 'Y'
      and isc.item                = iscl.item
      and isc.supplier            = iscl.supplier
      and isc.origin_country_id   = iscl.origin_country_id
      and iscl.loc                = gtt.number_1
      and isc.item                = im.item
      and im.status               = 'A'
   union all
   select im.item,
          gtt.number_1 loc,
          nvl(isc.lead_time, 0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time, 0)) plan_sup_lead_time,
          null plan_wh_lead_time,
          null plan_review_lead_time,
          null actual_sup_lead_time,
          null actual_wh_lead_time,
          null actual_review_lead_time
     from gtt_num_num_str_str_date_date gtt,
          item_master im,
          item_supp_country isc,
          item_supp_country_loc iscl
    where I_item_type             = 'STYLE'
      and im.item_parent          = I_item
      and im.status               = 'A'
      and isc.item                = im.item
      and isc.primary_supp_ind    = 'Y'
      and isc.primary_country_ind = 'Y'
      and isc.item                = iscl.item
      and isc.supplier            = iscl.supplier
      and isc.origin_country_id   = iscl.origin_country_id
      and iscl.loc                = gtt.number_1
   union all
   select im.item,
          gtt.number_1 loc,
          nvl(isc.lead_time, 0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time, 0)) plan_sup_lead_time,
          null plan_wh_lead_time,
          null plan_review_lead_time,
          null actual_sup_lead_time,
          null actual_wh_lead_time,
          null actual_review_lead_time
     from gtt_num_num_str_str_date_date gtt,
          item_master im,
          item_supp_country isc,
          item_supp_country_loc iscl
    where I_item_type           = 'FA'
      and im.item_parent        = I_item
      and im.status             = 'A'
      and nvl(im.diff_1,'NU|L') = nvl(I_agg_diff_1, nvl(im.diff_1,'NU|L'))
      and nvl(im.diff_2,'NU|L') = nvl(I_agg_diff_2, nvl(im.diff_2,'NU|L'))
      and nvl(im.diff_3,'NU|L') = nvl(I_agg_diff_3, nvl(im.diff_3,'NU|L'))
      and nvl(im.diff_4,'NU|L') = nvl(I_agg_diff_4, nvl(im.diff_4,'NU|L'))
      and isc.item                = im.item
      and isc.primary_supp_ind    = 'Y'
      and isc.primary_country_ind = 'Y'
      and isc.item                = iscl.item
      and isc.supplier            = iscl.supplier
      and isc.origin_country_id   = iscl.origin_country_id
      and iscl.loc                = gtt.number_1;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_lead_time_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_lead_time_gtt target
   using (select oi.item,
                 oi.loc,
                 ril.wh_lead_time plan_wh_lead_time,
                 decode(ril.review_cycle,	
                 1,1,
                 ril.review_cycle * 7) plan_review_lead_time
            from rms_oi_lead_time_gtt oi,
                 repl_item_loc ril
           where oi.item = ril.item 
             and oi.loc  = ril.location
   ) use_this
   on (    target.item = use_this.item
       and target.loc  = use_this.loc)
   when matched then update
    set target.plan_wh_lead_time     = use_this.plan_wh_lead_time,
        target.plan_review_lead_time = use_this.plan_review_lead_time;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge plan_wh_lead_time,plan_review_lead_time rms_oi_lead_time_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_lead_time_gtt target
   using (select oi.item,
                 oi.loc,
                 count(*) day_cnt
            from rms_oi_lead_time_gtt oi,
                 repl_day rd
           where oi.item                  = rd.item
             and oi.loc                   = rd.location
             and oi.plan_review_lead_time = 0
           group by oi.item,
                    oi.loc
   ) use_this
   on (    target.item = use_this.item
       and target.loc  = use_this.loc)
   when matched then update
    set target.plan_review_lead_time = 7 / use_this.day_cnt;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge plan_review_lead_time(day) rms_oi_lead_time_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --actual sup lead time
   merge into rms_oi_lead_time_gtt target
   using (select i.item,
                 i.loc,
                 avg(i.actual_sup_lead_time) actual_sup_lead_time
            from (select oi.item,
                         oi.loc,
                         avg(sh.receive_date - oh.orig_approval_date) actual_sup_lead_time
                    from rms_oi_lead_time_gtt oi,
                         shipsku ss,
                         shipment sh,
                         ordhead oh
                   where oi.item          = ss.item
                     and sh.shipment      = ss.shipment
                     and sh.to_loc        = oi.loc
                     and sh.receive_date  is not null
                     and sh.receive_date >= L_history_date
                     and oh.order_no      = sh.order_no
                   group by oi.item,
                            oi.loc
                  union all
                  select oi.item,
                         oi.loc,
                         avg(sh.receive_date - oh.orig_approval_date) actual_sup_lead_time
                    from rms_oi_lead_time_gtt oi,
                         alloc_header ah,
                         alloc_detail ad,
                         ordhead oh,
                         shipment sh,
                         shipsku ss
                   where oi.item          = ah.item
                     and oi.loc           = ad.to_loc
                     and ah.alloc_no      = ad.alloc_no
                     and ah.order_no      = oh.order_no
                     and oh.order_no      = sh.order_no
                     and ah.wh            in (select w.wh from wh w where w.physical_wh = sh.to_loc)
                     and sh.shipment      = ss.shipment
                     and ss.item          = oi.item
                     and sh.receive_date >= L_history_date
                   group by oi.item,
                            oi.loc
                 ) i
           group by i.item, 
                    i.loc
   ) use_this
   on (    target.item = use_this.item
       and target.loc  = use_this.loc)
   when matched then update
    set target.actual_sup_lead_time = use_this.actual_sup_lead_time;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge actual_sup_lead_time rms_oi_lead_time_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --actual wh lead time
   merge into rms_oi_lead_time_gtt target
   using (select oi.item,
                 oi.loc,
                 avg(sh.receive_date - sh.ship_date) actual_wh_lead_time
            from rms_oi_lead_time_gtt oi,
                 shipsku ss,
                 shipment sh
           where oi.item          = ss.item
             and oi.loc           = sh.to_loc
             and sh.shipment      = ss.shipment
             and sh.from_loc_type = 'W'
             and sh.receive_date  is not null
             and sh.receive_date >= L_history_date
             and exists (select 'x'
                           from repl_item_loc ril
                          where ril.item     = oi.item
                            and ril.location = oi.loc)
             group by oi.item,
                      oi.loc
   ) use_this
   on (    target.item = use_this.item
       and target.loc  = use_this.loc)
   when matched then update
    set target.actual_wh_lead_time = use_this.actual_wh_lead_time;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge actual_wh_lead_time rms_oi_lead_time_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --actual review cycle
   merge into rms_oi_lead_time_gtt target
   using (select inner.item, 
                 inner.loc,
                 sum(inner.order_cnt) order_cnt,
                 sum(inner.tsf_cnt) tsf_cnt,
                 sum(inner.alloc_cnt) alloc_cnt
            from (select distinct oi.item,
                         oi.loc,
                         count(distinct oh.order_no) order_cnt,
                         0 tsf_cnt,
                         0 alloc_cnt
                    from (select gtt.item,
                                 gtt.loc
                            from rms_oi_lead_time_gtt gtt,
                                 repl_item_loc ril
                           where gtt.item = ril.item
                             and gtt.loc  = ril.location) oi,
                         ordloc ol,
                         ordhead oh
                   where oi.item                   = ol.item
                     and oi.loc                    = ol.location
                     and ol.order_no               = oh.order_no
                     and (oh.status = 'A' 
                          or 
                          (oh.status = 'C' and nvl(ol.qty_received,0) > 0)
                         )
                     and oh.orig_approval_date     between L_history_date and get_vdate
                   group by oi.item,
                            oi.loc
                  union all
                  select distinct oi.item,
                         oi.loc,
                         0 order_cnt,
                         count(distinct th.tsf_no) tsf_cnt,
                         0 alloc_cnt
                    from (select gtt.item,
                                 gtt.loc
                            from rms_oi_lead_time_gtt gtt,
                                 repl_item_loc ril
                           where gtt.item = ril.item
                             and gtt.loc  = ril.location) oi,
                         tsfhead th,
                         tsfdetail td
                   where oi.item              = td.item
                     and oi.loc               = th.to_loc
                     and td.tsf_no            = th.tsf_no
                     and th.status            in ('A','S','C','P','L')
                     and th.approval_date     between L_history_date and get_vdate
                   group by oi.item,
                            oi.loc
                  union all
                  select distinct oi.item,
                         oi.loc,
                         0 order_cnt,
                         0 tsf_cnt,
                         count(distinct ah.alloc_no) alloc_cnt
                    from (select gtt.item,
                                 gtt.loc
                            from rms_oi_lead_time_gtt gtt,
                                 repl_item_loc ril
                           where gtt.item = ril.item
                             and gtt.loc  = ril.location) oi,
                         alloc_header ah,
                         alloc_detail ad
                   where oi.item            = ah.item
                     and oi.loc             = ad.to_loc
                     and ad.alloc_no        = ah.alloc_no
                     and ah.status          in ('A','R','C')
                     and ah.release_date    between L_history_date and get_vdate
                     --
                   group by oi.item,
                            oi.loc) inner
           group by inner.item, 
                    inner.loc
   ) use_this
   on (    target.item                           = use_this.item
       and target.loc                            = use_this.loc
       and use_this.order_cnt + use_this.tsf_cnt + use_this.alloc_cnt > 0)
   when matched then update
    set target.actual_review_lead_time = 28 / (use_this.order_cnt + use_this.tsf_cnt + use_this.alloc_cnt);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge actual_review_lead_time rms_oi_lead_time_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open c_lead_time;
   fetch c_lead_time bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_store_inv - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   close c_lead_time;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END LEAD_TIMES;
--------------------------------------------------------------------------------
FUNCTION INV_VAR_FORECAST(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id             IN     oi_session_id_log.session_id%TYPE,
                          I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                          I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                          I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                          I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                          I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.INV_VAR_FORECAST';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate               DATE         := get_vdate;
   L_user                VARCHAR2(30) := get_user;
   L_forward_date        DATE;
   L_history_date        DATE;
   L_location_group_type VARCHAR2(1);
   L_store_count         NUMBER(8);
   L_primary_currency    system_options.currency_code%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select currency_code
     into L_primary_currency
     from system_options;

   select d.eow_date + 21,
          d.eow_date - 21
     into L_forward_date,
          L_history_date
     from day_level_calendar d
    where d.day = L_vdate;

   delete from rms_oi_inv_ana_variance_defer v 
    where v.defer_to_date <= L_vdate;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete  rms_oi_inv_ana_variance_defer expired deferred items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   select count(*) 
     into L_store_count
     from gtt_num_num_str_str_date_date;

   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,         --I_origin_countries
                             I_brands,
                             'Y') = 0 then --I_forecast_ind
      return OI_UTILITY.FAILURE;
   end if;

   delete from gtt_10_num_10_str_10_date
    where rowid in ( select gtt.rowid
                      from gtt_10_num_10_str_10_date gtt,
                           rms_oi_inv_ana_variance_defer d
                     where d.alc_item_type            = 'STYLE'
                       and gtt.varchar2_2             = d.item
                       and d.defer_user               = L_user
                       and d.defer_to_date            > L_vdate
                    union all
                    select gtt.rowid
                      from gtt_10_num_10_str_10_date gtt,
                           rms_oi_inv_ana_variance_defer d
                     where d.alc_item_type            = 'FA'
                       and gtt.varchar2_2             = d.item
                       and nvl(gtt.varchar2_5,'nu|l') = nvl(d.agg_diff_1,'nu|l')
                       and nvl(gtt.varchar2_6,'nu|l') = nvl(d.agg_diff_2,'nu|l')
                       and nvl(gtt.varchar2_7,'nu|l') = nvl(d.agg_diff_3,'nu|l')
                       and nvl(gtt.varchar2_8,'nu|l') = nvl(d.agg_diff_4,'nu|l')
                       and d.defer_user               = L_user
                       and d.defer_to_date            > L_vdate
                    union all
                    select gtt.rowid
                      from gtt_10_num_10_str_10_date gtt,
                           rms_oi_inv_ana_variance_defer d
                     where d.alc_item_type            not in('STYLE','FA')
                       and gtt.varchar2_1             = d.item
                       and d.defer_user               = L_user
                       and d.defer_to_date            > L_vdate);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete  gtt_10_num_10_str_10_date deferred items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
   if POP_ILSOH_FILTER(O_error_message,
                       I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_FORECAST_HIST(O_error_message,
                          I_session_id,
                          L_history_date,
                          L_vdate) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_FORECAST(O_error_message,
                     I_session_id,
                     L_vdate,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ON_ORDER(O_error_message,
                     I_session_id,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_CO_INBOUND(O_error_message,
                       I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_WH_CROSSLINK(O_error_message,
                         I_session_id,
                         L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ALLOC_INBOUND(O_error_message,
                          I_session_id,
                          L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_SALES_HIST(O_error_message,
                       I_session_id,
                       L_history_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_UNIT_RETAIL(O_error_message,
                        I_session_id,
                        L_store_count) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if ROLLUP_INV_VAR_FORECAST(O_error_message,
                              I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_inv_ana_variance
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ana_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_inv_ana_variance (session_id,
                                        item,
                                        item_desc,
                                        alc_item_type,
                                        agg_diff_1,
                                        agg_diff_2,
                                        agg_diff_3,
                                        agg_diff_4,
                                        sales_units,
                                        sales_var_to_forecast_pct,
                                        inv_units,
                                        inv_var_value,
                                        inv_var_to_forecast_pct,
                                        currency_code)
   select I_session_id,
          oi.item,
          oi.item_desc,
          oi.alc_item_type,
          oi.agg_diff_1,
          oi.agg_diff_2,
          oi.agg_diff_3,
          oi.agg_diff_4,
          sum(oi.sales_hist_qty) sales_units,
          --
          ((sum(oi.sales_hist_qty) - sum(oi.forecast_sales_hist)) / sum(oi.forecast_sales_hist)) * 100 sales_var_to_forecast_pct,
          --
          sum(oi.stock_on_hand + oi.pack_comp_soh) +
           sum(oi.in_transit_qty + oi.pack_comp_intran + oi.on_order_qty + oi.pl_tsf_inbound + oi.alloc_inbound + 
               oi.tsf_expected_qty + oi.pack_comp_exp) -
           sum(oi.tsf_reserved_qty + oi.pack_comp_resv + oi.rtv_qty + oi.customer_resv + oi.pack_comp_cust_resv + oi.co_inbound) -
           sum(oi.customer_backorder + oi.pack_comp_cust_back) -
           sum(oi.non_sellable_qty + oi.pack_comp_non_sellable) -
           sum(oi.forecast_sales) inv_units,
          --
          (sum(oi.stock_on_hand + oi.pack_comp_soh) +
            sum(oi.in_transit_qty + oi.pack_comp_intran + oi.on_order_qty + oi.pl_tsf_inbound + oi.alloc_inbound + 
                oi.tsf_expected_qty + oi.pack_comp_exp) -
            sum(oi.tsf_reserved_qty + oi.pack_comp_resv + oi.rtv_qty + oi.customer_resv + oi.pack_comp_cust_resv + oi.co_inbound) -
            sum(oi.customer_backorder + oi.pack_comp_cust_back) -
            sum(oi.non_sellable_qty + oi.pack_comp_non_sellable) -
            sum(oi.forecast_sales)) * avg(oi.unit_retail) inv_var_value,
          --
          ((sum(oi.stock_on_hand + oi.pack_comp_soh) +
             sum(oi.in_transit_qty + oi.pack_comp_intran + oi.on_order_qty + oi.pl_tsf_inbound + oi.alloc_inbound + 
                 oi.tsf_expected_qty + oi.pack_comp_exp) -
             sum(oi.tsf_reserved_qty + oi.pack_comp_resv + oi.rtv_qty + oi.customer_resv + oi.pack_comp_cust_resv + oi.co_inbound) -
             sum(oi.customer_backorder + oi.pack_comp_cust_back) -
             sum(oi.non_sellable_qty + oi.pack_comp_non_sellable) -
             sum(oi.forecast_sales)) / sum(forecast_sales)) * 100 inv_var_to_forecast_pct,
          L_primary_currency
     from rms_oi_inv_ana_inv_gtt oi
    group by oi.item,
             oi.item_desc,
             oi.alc_item_type,
             oi.agg_diff_1,
             oi.agg_diff_2,
             oi.agg_diff_3,
             oi.agg_diff_4;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END INV_VAR_FORECAST;
--------------------------------------------------------------------------------
FUNCTION DEFER_INV_VAR_FORECAST_ITEM(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id              IN     oi_session_id_log.session_id%TYPE,
                                     I_item                    IN     item_master.item%TYPE,
                                     I_item_type               IN     item_master.alc_item_type%TYPE,
                                     I_agg_diff_1              IN     item_master.diff_1%TYPE,
                                     I_agg_diff_2              IN     item_master.diff_2%TYPE,
                                     I_agg_diff_3              IN     item_master.diff_3%TYPE,
                                     I_agg_diff_4              IN     item_master.diff_4%TYPE,
                                     I_defer_to_date           IN     DATE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DEFER_INV_VAR_FORECAST_ITEM';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_user                 VARCHAR2(30) := get_user;
   L_vdate                DATE         := get_vdate;
   L_location_group_type  VARCHAR2(1);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   insert into rms_oi_inv_ana_variance_defer (session_id,
                                              defer_to_date,
                                              defer_user,
                                              item,
                                              alc_item_type,
                                              agg_diff_1,
                                              agg_diff_2,
                                              agg_diff_3,
                                              agg_diff_4)
   values (I_session_id,
           I_defer_to_date,
           L_user,
           I_item,
           I_item_type,
           I_agg_diff_1,
           I_agg_diff_2,
           I_agg_diff_3,
           I_agg_diff_4);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_variance_defer - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_inv_ana_variance
         where session_id                  = I_session_id
           and item                        = I_item
           and alc_item_type               = I_item_type
           and nvl(agg_diff_1, 'nul|l')    = nvl(I_agg_diff_1,'nul|l')
           and nvl(agg_diff_2, 'nul|l')    = nvl(I_agg_diff_2,'nul|l')
           and nvl(agg_diff_3, 'nul|l')    = nvl(I_agg_diff_3,'nul|l')
           and nvl(agg_diff_4, 'nul|l')    = nvl(I_agg_diff_4,'nul|l');

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ana_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_item_type  = 'STYLE' then
      delete from rms_oi_inv_ana_variance
         where session_id   = I_session_id
           and item         in (select item
                                  from item_master
                                 where item_parent =  I_item);
      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete rms_oi_inv_ana_variance for item type STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   elsif I_item_type  = 'FA' then
      delete from rms_oi_inv_ana_variance
            where session_id = I_session_id
              and item       in( select item
                                   from item_master
                                  where item_parent =  I_item
                                    and (   (    I_agg_diff_1    is not null
                                             and I_agg_diff_1    =  diff_1)
                                         or (    I_agg_diff_2    is not null
                                             and I_agg_diff_2    =  diff_2)
                                         or (    I_agg_diff_3    is not null
                                             and I_agg_diff_3    =  diff_3)
                                         or (    I_agg_diff_4    is not null
                                             and I_agg_diff_4    =  diff_4)));
      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete rms_oi_inv_ana_variance for item type FA - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DEFER_INV_VAR_FORECAST_ITEM;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_orders_past_nad             IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_orders_to_close             IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_not_approved_orders         IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_once_approved_orders        IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_missing_order_data          IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                      I_depts                       IN     OBJ_NUMERIC_ID_TABLE,
                      I_classes                     IN     OBJ_NUMERIC_ID_TABLE,
                      I_subclasses                  IN     OBJ_NUMERIC_ID_TABLE,
                      I_supplier_sites              IN     OBJ_NUMERIC_ID_TABLE,
                      I_areas                       IN     OBJ_NUMERIC_ID_TABLE,
                      I_stores                      IN     OBJ_NUMERIC_ID_TABLE,
                      I_store_grade_group_id        IN     store_grade_group.store_grade_group_id%TYPE,
                      I_show_ord_err_past_nad       IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_ord_to_close   IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_never_apprv    IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_once_apprv     IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_miss_ord_data  IN     VARCHAR2,   --'Y' or 'N'
                      I_brands                      IN     OBJ_VARCHAR_ID_TABLE default NULL,
                      I_order_types                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.ORDER_ERRORS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                DATE := get_vdate;
   L_eow_date             DATE := null;
   L_location_group_type  VARCHAR2(1);
   L_check_order_type     VARCHAR2(1) := 'N';

   L_ord_err_factory_ind            rms_oi_system_options.ia_ord_err_factory_ind%TYPE;

   L_order_date_key_tbl             OBJ_NUM_NUM_STR_TBL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   O_orders_past_nad      := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());
   O_orders_to_close      := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());
   O_not_approved_orders  := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());
   O_once_approved_orders := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());
   O_missing_order_data   := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());

   select so.ia_ord_err_factory_ind
     into L_ord_err_factory_ind
     from rms_oi_system_options so;

   if I_show_ord_err_past_nad       = 'N' and
      I_show_ord_err_ord_to_close   = 'N' and
      I_show_ord_err_never_apprv    = 'N' and
      I_show_ord_err_once_apprv     = 'N' and
      I_show_ord_err_miss_ord_data  = 'N' then
      return OI_UTILITY.SUCCESS;
   end if;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,          --I_origin_countries
                             I_brands,
                             null) = 0 then --I_forecast_ind
      return OI_UTILITY.FAILURE;
   end if;

   if I_order_types is not null and I_order_types.count > 0 then
      L_check_order_type := 'Y';
   end if;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id|| ' end setup ');

   delete from numeric_id_gtt;
   insert into numeric_id_gtt(numeric_id)
   select distinct oh.order_no
     from gtt_num_num_str_str_date_date gtt_store,
          gtt_10_num_10_str_10_date gtt_item,
          ordloc ol,
          ordhead oh
    where gtt_store.number_1  = ol.location
      and gtt_item.varchar2_1 = ol.item
      and ol.order_no         = oh.order_no
      and oh.status           in('A','S','W')
      and oh.order_type      != 'CO'
      and (L_check_order_type = 'N'  or
           oh.order_type      in(select value(input_order_types)
                                   from table(cast(I_order_types as OBJ_VARCHAR_ID_TABLE)) input_order_types))
   union
   select distinct oh.order_no
     from gtt_num_num_str_str_date_date gtt_store,
          gtt_10_num_10_str_10_date gtt_item,
          ordhead oh,
          alloc_header ah,
          alloc_detail ad
    where oh.status           in('A','S','W')
      and oh.order_no         = ah.order_no
      and gtt_item.varchar2_1 = ah.item
      and ah.alloc_no         = ad.alloc_no
      and gtt_store.number_1  = ad.to_loc
      and oh.order_type      != 'CO'
      and (L_check_order_type = 'N'  or
           oh.order_type      in(select value(input_order_types)
                                   from table(cast(I_order_types as OBJ_VARCHAR_ID_TABLE)) input_order_types));

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert orders numeric_id_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_supplier_sites is not null and I_supplier_sites.count > 0 then
      delete from numeric_id_gtt gtt
       where numeric_id not in(select oh.order_no
                                 from ordhead oh,
                                      table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups
                                 where oh.supplier = value(input_sups));

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete orders numeric_id_gtt sup filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   --setup past 7 day date ranges
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(varchar2_1, date_1)
   select '1',  L_vdate-1 from dual union all
   select '2',  L_vdate-2 from dual union all
   select '3',  L_vdate-3 from dual union all
   select '4',  L_vdate-4 from dual union all
   select '5',  L_vdate-5 from dual union all
   select '6',  L_vdate-6 from dual union all
   select '7+', L_vdate-7 from dual;

-- O_orders_past_nad

   if I_show_ord_err_past_nad = 'Y' then

      select OBJ_NUM_NUM_STR_REC(inner.order_no, 
                                 null, 
                                 inner.date_key)
        bulk collect into L_order_date_key_tbl
        from (select i.date_key,
                     i.order_no
               from  (select oh.order_no,
                                 date_gtt.varchar2_1 date_key,
                                 sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) outstanding_qty
                            from numeric_id_gtt ord_gtt,
                                 ordhead oh,
                                 ordloc ol,
                                 gtt_6_num_6_str_6_date date_gtt
                           where ord_gtt.numeric_id    = oh.order_no
                             and oh.status             in('A','S','W')
                             and oh.orig_approval_date is not null
                              and oh.order_no         = ol.order_no 
                             and ((date_gtt.varchar2_1 != '7+' and oh.not_after_date = date_gtt.date_1)
                                   or
                                  (date_gtt.varchar2_1 = '7+' and oh.not_after_date <= date_gtt.date_1)
                                 )
                      group by date_gtt.varchar2_1,
                               oh.order_no)i
              where i.outstanding_qty > 0)inner;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_orders_past_nad temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_orders_past_nad,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;

   end if;
   
   --setup future 7 day date ranges
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(varchar2_1, date_1)
   select '1', L_vdate+1 from dual union all
   select '2', L_vdate+2 from dual union all
   select '3', L_vdate+3 from dual union all
   select '4', L_vdate+4 from dual union all
   select '5', L_vdate+5 from dual union all
   select '6', L_vdate+6 from dual union all
   select '7', L_vdate+7 from dual;

--O_orders_to_close

   if I_show_ord_err_ord_to_close = 'Y' then

      select OBJ_NUM_NUM_STR_REC(inner.order_no,
                                 null,
                                 inner.date_key)
        bulk collect into L_order_date_key_tbl
        from (select i.date_key,
                     i.order_no
                from (select date_gtt.varchar2_1 date_key,
                             oh.order_no,
                             sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) outstanding_qty
                        from numeric_id_gtt ord_gtt,
                             ordhead oh,
                             gtt_6_num_6_str_6_date date_gtt,
                             ordloc ol
                       where ord_gtt.numeric_id  = oh.order_no
                         and oh.status           in('A','S','W')
                         and oh.orig_approval_date is not null
                         and oh.not_after_date   = date_gtt.date_1
                         and oh.order_no         = ol.order_no
                         and not exists ( select 1 
                                            from shipment sh,shipsku sk
                                           where sh.shipment = sk.shipment
                                             and sh.order_no = ol.order_no
                                             and sk.item = ol.item
                                             and nvl(sk.qty_expected,0) - nvl(sk.qty_received,0) > 0)
                       group by date_gtt.varchar2_1,
                                oh.order_no) i
               where i.outstanding_qty > 0
             ) inner;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_orders_to_close temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_orders_to_close,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;

   end if;

--O_not_approved_orders

   if I_show_ord_err_never_apprv = 'Y' then
      select OBJ_NUM_NUM_STR_REC(inner.order_no,
                                 null,
                                 inner.date_key)
        bulk collect into L_order_date_key_tbl
        from (select date_gtt.varchar2_1 date_key,
                     i.order_no
                from gtt_6_num_6_str_6_date date_gtt,
                     (select oh.order_no,
                             min(oh.not_before_date - (nvl(isc.lead_time,0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time,0)))) latest_approve_date
                        from numeric_id_gtt ord_gtt,
                             ordhead oh,
                             ordsku os,
                             ordloc ol,
                             item_supp_country isc,
                             item_supp_country_loc iscl
                       where ord_gtt.numeric_id    = oh.order_no
                         and oh.order_no           = os.order_no
                         and oh.supplier           = isc.supplier
                         and os.item               = isc.item
                         and os.origin_country_id  = isc.origin_country_id
                         and oh.status             in('W','S')
                         and oh.orig_approval_date is null
                         and os.order_no           = ol.order_no
                         and os.item               = ol.item
                         and isc.item              = iscl.item
                         and isc.supplier          = iscl.supplier
                         and isc.origin_country_id = iscl.origin_country_id
                         and iscl.loc              = ol.location
                       group by oh.order_no) i
               where date_gtt.date_1 = i.latest_approve_date
             ) inner;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_not_approved_orders temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_not_approved_orders,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;

   end if;

--O_once_approved_orders

   if I_show_ord_err_once_apprv = 'Y' then

      select OBJ_NUM_NUM_STR_REC(inner.order_no,
                                 null,
                                 inner.date_key)
        bulk collect into L_order_date_key_tbl
        from (select date_gtt.varchar2_1 date_key,
                     i.order_no
                from gtt_6_num_6_str_6_date date_gtt,
                     (select oh.order_no,
                             min(oh.not_before_date - (nvl(isc.lead_time,0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time,0)))) latest_approve_date
                        from numeric_id_gtt ord_gtt,
                             ordhead oh,
                             ordsku os,
                             ordloc ol,
                             item_supp_country isc,
                             item_supp_country_loc iscl
                       where ord_gtt.numeric_id    = oh.order_no
                         and oh.order_no           = os.order_no
                         and oh.supplier           = isc.supplier
                         and os.item               = isc.item
                         and os.origin_country_id  = isc.origin_country_id 
                         and oh.status             in('W','S')
                         and oh.orig_approval_date is not null
                         and os.order_no           = ol.order_no
                         and os.item               = ol.item
                         and isc.item              = iscl.item
                         and isc.supplier          = iscl.supplier
                         and isc.origin_country_id = iscl.origin_country_id
                         and iscl.loc              = ol.location
                       group by oh.order_no) i
               where date_gtt.date_1 = i.latest_approve_date
             ) inner;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_once_approved_orders temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_once_approved_orders,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;
   
   end if;

   --setup future 4 week date ranges
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(varchar2_1, date_1, date_2)
   select '1', L_vdate,       d.eow_date    from day_level_calendar d where d.day = L_vdate union all
   select '2', d.eow_date+1,  d.eow_date+7  from day_level_calendar d where d.day = L_vdate union all
   select '3', d.eow_date+8,  d.eow_date+14 from day_level_calendar d where d.day = L_vdate union all
   select '4', d.eow_date+15, d.eow_date+21 from day_level_calendar d where d.day = L_vdate;

   select d.eow_date
     into L_eow_date
     from day_level_calendar d where d.day = L_vdate;

--O_missing_order_data

   if I_show_ord_err_miss_ord_data = 'Y' then

      select OBJ_NUM_NUM_STR_REC(inner.order_no,
                                 null,
                                 inner.date_key)
        bulk collect into L_order_date_key_tbl
        from (select date_gtt.varchar2_1 date_key,
                     oh.order_no,
                     sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) remain_qty
                from numeric_id_gtt ord_gtt,
                     ordhead oh,
                     gtt_6_num_6_str_6_date date_gtt,
                     ordloc ol
               where ord_gtt.numeric_id    = oh.order_no
                 and oh.status             in('W','S','A')
                 and oh.orig_approval_date is not null
                 and (    oh.import_order_ind    = 'Y'
                      and (oh.discharge_port is null or
                           oh.lading_port    is null))
                 and oh.not_before_date between date_gtt.date_1 and date_gtt.date_2
                 and oh.order_no           = ol.order_no
               group by date_gtt.varchar2_1,
                        oh.order_no
              union
              select date_gtt.varchar2_1 date_key,
                     oh.order_no,
                     sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) remain_qty
                from numeric_id_gtt ord_gtt,
                     ordhead oh,
                     gtt_6_num_6_str_6_date date_gtt,
                     ordloc ol
               where ord_gtt.numeric_id    = oh.order_no
                 and oh.status             in('W','S','A')
                 and oh.orig_approval_date is not null
                 and L_ord_err_factory_ind = 'Y'
                 and oh.factory            is null
                 and oh.not_before_date between date_gtt.date_1 and date_gtt.date_2
                 and oh.order_no           = ol.order_no
               group by date_gtt.varchar2_1,
                        oh.order_no
             ) inner
       where inner.remain_qty > 0;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_missing_order_data temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_missing_order_data,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;

   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END ORDER_ERRORS;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS_ITEM(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_missing_item_data      IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                           I_session_id             IN     oi_session_id_log.session_id%TYPE,
                           I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                           I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                           I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                           I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                           I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                           I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                           I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                           I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL,
                           I_order_types            IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.ORDER_ERRORS_ITEM';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_vdate                DATE := get_vdate;
   L_eow_date             DATE := null;
   L_location_group_type  VARCHAR2(1);
   L_check_order_type     VARCHAR2(1) := 'N';

   L_ord_err_ref_item_ind           rms_oi_system_options.ia_ord_err_ref_item_ind%TYPE;
   L_import_ind                     system_options.import_ind%TYPE;
   L_elc_ind                        system_options.elc_ind%TYPE;
   L_order_hts_ind                  system_options.order_hts_ind%TYPE;

   L_order_date_key_tbl             OBJ_NUM_NUM_STR_TBL;

BEGIN


   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   O_missing_item_data    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());

   select so.ia_ord_err_ref_item_ind,
          syso.import_ind,
          syso.elc_ind,
          syso.order_hts_ind
     into L_ord_err_ref_item_ind,
          L_import_ind,
          L_elc_ind,
          L_order_hts_ind
     from rms_oi_system_options so,
          system_options syso;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,          --I_origin_countries
                             I_brands,
                             null) = 0 then --I_forecast_ind
      return OI_UTILITY.FAILURE;
   end if;

   select d.eow_date
     into L_eow_date
     from day_level_calendar d where d.day = L_vdate;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id|| ' fetch eow_date');

   if I_order_types is not null and I_order_types.count > 0 then
      L_check_order_type := 'Y';
   end if;

   delete from numeric_id_gtt;

   insert into numeric_id_gtt(numeric_id)
   select distinct oh.order_no
     from gtt_num_num_str_str_date_date gtt_store,
          gtt_10_num_10_str_10_date gtt_item,
          ordloc ol,
          ordhead oh
    where gtt_store.number_1    = ol.location
      and gtt_item.varchar2_1   = ol.item
      and ol.order_no           = oh.order_no
      and oh.status             in ('W','S','A')
      and oh.orig_approval_date is not null
      and oh.not_before_date    between L_vdate and L_eow_date+21
      and oh.order_type        != 'CO' 
      and (L_check_order_type   = 'N'  or
           oh.order_type        in(select value(input_order_types)
                                     from table(cast(I_order_types as OBJ_VARCHAR_ID_TABLE)) input_order_types));

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert orders numeric_id_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_supplier_sites is not null and I_supplier_sites.count > 0 then
      delete from numeric_id_gtt gtt
       where numeric_id not in(select oh.order_no
                                 from ordhead oh,
                                      table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups
                                 where oh.supplier = value(input_sups));

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' delete orders numeric_id_gtt sup filter - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;


   --setup future 4 week date ranges
   delete from gtt_6_num_6_str_6_date;
   insert into gtt_6_num_6_str_6_date(varchar2_1, date_1, date_2)
   select '1', L_vdate,       d.eow_date    from day_level_calendar d where d.day = L_vdate union all
   select '2', d.eow_date+1,  d.eow_date+7  from day_level_calendar d where d.day = L_vdate union all
   select '3', d.eow_date+8,  d.eow_date+14 from day_level_calendar d where d.day = L_vdate union all
   select '4', d.eow_date+15, d.eow_date+21 from day_level_calendar d where d.day = L_vdate;

--O_missing_item_data

   with ords as (select i.order_no,
                        i.import_order_ind,
                        i.import_country_id,
                        i.date_key
                   from (select oh.order_no,
                                oh.import_order_ind,
                                oh.import_country_id,
                                date_gtt.varchar2_1 date_key,
                                sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) remain_qty
                           from numeric_id_gtt ord_gtt,
                                ordhead oh,
                                ordloc ol,
                                gtt_6_num_6_str_6_date date_gtt
                          where ord_gtt.numeric_id    = oh.order_no
                            and oh.not_before_date    between date_gtt.date_1 and date_gtt.date_2
                            and oh.status             in ('W','S','A')
                            and oh.orig_approval_date is not null
                            and oh.order_no           = ol.order_no
                          group by oh.order_no,
                                   oh.import_order_ind,
                                   oh.import_country_id,
                                   date_gtt.varchar2_1) i
                  where i.remain_qty > 0)
   --
   select OBJ_NUM_NUM_STR_REC(inner.order_no, 
                              null, 
                              inner.date_key)
     bulk collect into L_order_date_key_tbl
     from (select distinct ords.order_no,
                  ords.date_key,
                  os.item
             from ords,
                  ordsku os,
                  ordloc ol,
                  gtt_10_num_10_str_10_date gtt_item,
                  gtt_num_num_str_str_date_date gtt_store
            where ords.order_no          = os.order_no
              and L_ord_err_ref_item_ind = 'Y'
              and os.ref_item            is null
              and os.item                = gtt_item.varchar2_1
              and os.order_no            = ol.order_no
              and os.item                = ol.item
              and ol.location            = gtt_store.number_1
           union all
           select i.order_no,
                  i.date_key,
                  i.item
             from (select distinct ords.order_no,
                          ords.date_key,
                          ol.item,
                          case when d.markup_calc_type = 'R'
                             then (il.unit_retail - ol.unit_cost) / nullif(il.unit_retail,0)
                             else (il.unit_retail - ol.unit_cost) / nullif(ol.unit_cost,0) end margin
                     from ords,
                          ordloc ol,
                          item_loc il,
                          item_master im,
                          deps d,
                          gtt_10_num_10_str_10_date gtt_item,
                          gtt_num_num_str_str_date_date gtt_store
                    where ords.order_no  = ol.order_no
                      and ol.item        = il.item
                      and ol.location    = il.loc
                      and il.item        = im.item
                      and im.dept        = d.dept
                      and ol.item        = gtt_item.varchar2_1
                      and ol.location    = gtt_store.number_1) i
            where i.margin <= 0
           union all
           select distinct ords.order_no,
                  ords.date_key,
                  os.item
             from ords,
                  ordsku os,
                  ordloc ol,
                  gtt_10_num_10_str_10_date gtt_item,
                  gtt_num_num_str_str_date_date gtt_store
            where L_import_ind            = 'Y'
              and L_elc_ind               = 'Y'
              and ords.import_order_ind   = 'Y'
              and ords.order_no           = os.order_no
              and ords.import_country_id != os.origin_country_id
              and os.item                 = gtt_item.varchar2_1
              and os.order_no             = ol.order_no
              and os.item                 = ol.item
              and ol.location             = gtt_store.number_1
              and ((L_order_hts_ind = 'Y'
                    and not exists (select 'X'
                                      from ordsku_hts h,
                                           item_master im
                                      where os.order_no     = h.order_no
                                        and os.item         = h.item
                                        and h.pack_item     is null
                                        and h.status        = 'A'
                                        and os.item         = im.item
                                        and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.pack_type != 'B'))
                                      union all
                                     select 'X'
                                       from ordsku_hts h,
                                            item_master im,
                                            v_packsku_qty v
                                      where os.order_no     = h.order_no
                                        and os.item         = h.pack_item
                                        and h.status        = 'A'
                                        and os.item         = im.item
                                        and im.pack_ind     = 'Y'
                                        and im.pack_type    = 'B'
                                        and h.pack_item     = v.pack_no
                                        and h.item          = v.item))
                 or(L_order_hts_ind = 'N'
                    and exists (select 'X'
                                  from ordsku_hts h,
                                       item_master im
                                 where os.order_no     = h.order_no
                                   and os.item         = h.item
                                   and h.pack_item     is null
                                   and h.status        <> 'A'
                                   and os.item         = im.item
                                   and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.pack_type != 'B'))
                                 union all
                                select 'X'
                                  from ordsku_hts h,
                                       item_master im,
                                       v_packsku_qty v
                                 where os.order_no     = h.order_no
                                   and os.item         = h.pack_item
                                   and h.status        <> 'A'
                                   and os.item         = im.item
                                   and im.pack_ind     = 'Y'
                                   and im.pack_type    = 'B'
                                   and h.pack_item     = v.pack_no
                                   and h.item          = v.item)))
          ) inner;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' temp population - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POPULATE_ORDER_ITEM_ERROR_REC(O_error_message,
                                    O_missing_item_data,
                                    I_session_id,
                                    L_order_date_key_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END ORDER_ERRORS_ITEM;
--------------------------------------------------------------------------------
FUNCTION POPULATE_ORDER_ERROR_REC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_order_error_rec     IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                  I_session_id          IN     oi_session_id_log.session_id%TYPE,
                                  I_order_date_key_tbl  IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.POPULATE_ORDER_ERROR_REC';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id);

   select distinct t.number_1
     bulk collect into O_order_error_rec.orders
     from table(cast(I_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) t;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' bulk collect into O_order_error_rec.orders - COUNT: ' || O_order_error_rec.orders.count);

   select RMS_OI_DATE_VALUE_REC(inner2.date_key,
                                inner2.ord_count)
     bulk collect into O_order_error_rec.date_key_values
     from (select inner.date_key,
                  count(inner.order_no) ord_count
             from (select distinct t.number_1 order_no,
                                   t.string date_key
                    from table(cast(I_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) t
                  ) inner
            group by inner.date_key
          ) inner2;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' bulk collect into O_order_error_rec.date_key_values - COUNT: ' || O_order_error_rec.date_key_values.count);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END POPULATE_ORDER_ERROR_REC;
--------------------------------------------------------------------------------
FUNCTION POPULATE_ORDER_ITEM_ERROR_REC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_order_error_rec     IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                       I_session_id          IN     oi_session_id_log.session_id%TYPE,
                                       I_order_date_key_tbl  IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.POPULATE_ORDER_ITEM_ERROR_REC';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id);

   select distinct t.number_1
     bulk collect into O_order_error_rec.orders
     from table(cast(I_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) t;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' bulk collect into O_order_error_rec.orders - COUNT: ' || O_order_error_rec.orders.count);

   select RMS_OI_DATE_VALUE_REC(inner2.date_key,
                                inner2.ord_count)
     bulk collect into O_order_error_rec.date_key_values
     from (select inner.date_key,
                  count(*) ord_count
             from (select t.number_1 order_no,
                          t.string date_key
                    from table(cast(I_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) t
                  ) inner
            group by inner.date_key
          ) inner2;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' bulk collect into O_order_error_rec.date_key_values - COUNT: ' || O_order_error_rec.date_key_values.count);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END POPULATE_ORDER_ITEM_ERROR_REC;
--------------------------------------------------------------------------------
FUNCTION POPULATE_ORD_ITM_ERROR_REC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_order_error_rec     IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                    I_session_id          IN     oi_session_id_log.session_id%TYPE,
                                    I_order_date_key_tbl  IN     OBJ_NUM_STR_STR_TBL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.POPULATE_ORD_ITM_ERROR_REC';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id);

   select distinct t.number_1
     bulk collect into O_order_error_rec.orders
     from table(cast(I_order_date_key_tbl as OBJ_NUM_STR_STR_TBL)) t;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' bulk collect into O_order_error_rec.orders - COUNT: ' || O_order_error_rec.orders.count);

   select RMS_OI_DATE_VALUE_REC(inner2.date_key,
                                inner2.ord_count)
     bulk collect into O_order_error_rec.date_key_values
     from (select innr.date_key,
                  count(innr.order_no) ord_count
             from (select distinct t.number_1 order_no,
                                   t.string_2 date_key
                    from table(cast(I_order_date_key_tbl as OBJ_NUM_STR_STR_TBL)) t
                  ) innr
            group by innr.date_key
          ) inner2;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' bulk collect into O_order_error_rec.date_key_values - COUNT: ' || O_order_error_rec.date_key_values.count);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END POPULATE_ORD_ITM_ERROR_REC;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id         IN     oi_session_id_log.session_id%TYPE,
                             I_orders             IN     OBJ_NUMERIC_ID_TABLE,
                             I_missing_order_data IN     VARCHAR2)   --'Y' or 'N'
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.ORDER_ERRORS_DETAIL';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_ord_err_factory_ind      rms_oi_system_options.ia_ord_err_factory_ind%TYPE;
   L_lading_port_error_msg    rtk_errors.rtk_text%type;
   L_discharge_port_error_msg rtk_errors.rtk_text%type;
   L_factory_error_msg        rtk_errors.rtk_text%type;
   L_consolidation_ind        system_options.consolidation_ind%TYPE;
   L_vdate                    DATE := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id);

   select so.ia_ord_err_factory_ind
     into L_ord_err_factory_ind
     from rms_oi_system_options so;

   select consolidation_ind
     into L_consolidation_ind
     from system_options;

   delete from rms_oi_inv_ord_errors
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_missing_order_data = 'Y' then

      select cd.code_desc
        into L_lading_port_error_msg
        from v_code_detail_tl cd
       where cd.code_type = 'OEOE'
         and cd.code      = 'LAD';
   
      select cd.code_desc
        into L_discharge_port_error_msg
        from v_code_detail_tl cd
       where cd.code_type = 'OEOE'
         and cd.code      = 'DISC';
   
      select cd.code_desc
        into L_factory_error_msg
        from v_code_detail_tl cd
       where cd.code_type = 'OEOE'
         and cd.code      = 'FACT';

      insert into rms_oi_inv_ord_errors(session_id,
                                        order_no,
                                        supplier_site,
                                        status,
                                        not_before_date,
                                        not_after_date,
                                        otb_eow_date,
                                        total_cost,
                                        received_percent,
                                        import_order_ind,
                                        master_po_no,
                                        currency_code,
                                        error_message,
                                        dept,
                                        po_type,
                                        comment_desc,
                                        po_last_update_datetime,
                                        po_last_update_id,
                                        order_type,
                                        wf_order_no)
      select I_session_id,
             oh.order_no,
             oh.supplier,
             oh.status,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             null,
             null,
             oh.import_order_ind,
             oh.master_po_no,
             oh.currency_code,
             L_lading_port_error_msg,
             oh.dept,
             oh.po_type,
             oh.comment_desc,
             oh.last_update_datetime,
             oh.last_update_id,
             oh.order_type,
             oh.wf_order_no
        from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
             ordhead oh
       where oh.order_no         = value(input_orders)
         and oh.import_order_ind = 'Y'
         and oh.lading_port      is null
      union all
      select I_session_id,
             oh.order_no,
             oh.supplier,
             oh.status,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             null,
             null,
             oh.import_order_ind,
             oh.master_po_no,
             oh.currency_code,
             L_discharge_port_error_msg,
             oh.dept,
             oh.po_type,
             oh.comment_desc,
             oh.last_update_datetime,
             oh.last_update_id,
             oh.order_type,
             oh.wf_order_no
        from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
             ordhead oh
       where oh.order_no         = value(input_orders)
         and oh.import_order_ind = 'Y'
         and oh.discharge_port   is null
      union all
      select I_session_id,
             oh.order_no,
             oh.supplier,
             oh.status,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             null,
             null,
             oh.import_order_ind,
             oh.master_po_no,
             oh.currency_code,
             L_factory_error_msg,
             oh.dept,
             oh.po_type,
             oh.comment_desc,
             oh.last_update_datetime,
             oh.last_update_id,
             oh.order_type,
             oh.wf_order_no
        from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
             ordhead oh
       where oh.order_no           = value(input_orders)
         and L_ord_err_factory_ind = 'Y'
         and oh.factory            is null;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert missing rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else 

      insert into rms_oi_inv_ord_errors(session_id,
                                        order_no,
                                        supplier_site,
                                        status,
                                        not_before_date,
                                        not_after_date,
                                        otb_eow_date,
                                        total_cost,
                                        received_percent,
                                        import_order_ind,
                                        master_po_no,
                                        currency_code,
                                        dept,
                                        po_type,
                                        comment_desc,
                                        po_last_update_datetime,
                                        po_last_update_id,
                                        order_type,
                                        wf_order_no)
      select I_session_id,
             oh.order_no,
             oh.supplier,
             oh.status,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             null,
             null,
             oh.import_order_ind,
             oh.master_po_no,
             oh.currency_code,
             oh.dept,
             oh.po_type,
             oh.comment_desc,
             oh.last_update_datetime,
             oh.last_update_id,
             oh.order_type,
             oh.wf_order_no
        from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
             ordhead oh
       where oh.order_no = value(input_orders);

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   --Add Dept name
   merge into rms_oi_inv_ord_errors target
   using (select distinct oerr.order_no,
                 oerr.session_id,
                 oerr.dept,
                 d.dept_name
            from rms_oi_inv_ord_errors oerr,
                 v_deps_tl  d
           where oerr.session_id = I_session_id
             and oerr.dept       = d.dept) use_this
   on (    target.order_no   = use_this.order_no
       and target.session_id = use_this.session_id
       and target.dept       = use_this.dept)
   when matched then update
    set target.dept_name = use_this.dept_name;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge dept name rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
    
   --Add PO Type desc
   merge into rms_oi_inv_ord_errors target
   using (select distinct oerr.order_no,
                 oerr.session_id,
                 oerr.po_type,
                 pt.po_type_desc
            from rms_oi_inv_ord_errors oerr,
                 v_po_type_tl pt
           where oerr.session_id = I_session_id
             and oerr.po_type    = pt.po_type) use_this
   on (    target.order_no   = use_this.order_no
       and target.session_id = use_this.session_id
       and target.po_type    = use_this.po_type)
   when matched then update
    set target.po_type_desc = use_this.po_type_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge po type desc name rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ord_errors target
   using(with curr_conv as (select from_currency,
                                   to_currency,
                                   effective_date,
                                   exchange_rate,
                                   exchange_type
                              from (select from_currency,
                                           to_currency,
                                           effective_date,
                                           exchange_rate,
                                           exchange_type,
                                           rank() over (partition by from_currency,
                                                                     to_currency
                                                            order by decode(exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                     effective_date desc) date_rank
                                      from mv_currency_conversion_rates
                                     where effective_date <= L_vdate
                                       and ((L_consolidation_ind = 'Y' and exchange_type in('P', 'C', 'O'))
                                            or
                                            (L_consolidation_ind = 'N' and exchange_type in('P', 'O')))
                                   )
                             where date_rank = 1)
          select distinct oerr.order_no,
                 oerr.session_id,
                 sum(ol.qty_ordered * ol.unit_cost) over (partition by ol.order_no, oerr.error_message) total_cost,
                 --
                 case when sum(ol.qty_ordered) over (partition by ol.order_no, oerr.error_message) = 0 then
                    0
                 else
                    sum(nvl(ol.qty_received,0)) over (partition by ol.order_no, oerr.error_message) / 
                       sum(ol.qty_ordered) over (partition by ol.order_no, oerr.error_message) 
                 end received_percent,
                 --
                 sum((ol.unit_retail*curr_conv.exchange_rate) * ol.qty_ordered) over (partition by ol.order_no, oerr.error_message) total_retail,
                 sum(ol.qty_ordered) over (partition by ol.order_no, oerr.error_message) total_units,
                 sum(nvl(ol.qty_received,0)) over (partition by ol.order_no, oerr.error_message) received_units,
                 sum(ol.qty_ordered - nvl(ol.qty_received,0)) over (partition by ol.order_no, oerr.error_message) outstanding_units,
                 sum((ol.qty_ordered - nvl(ol.qty_received,0)) * ol.unit_cost) over (partition by ol.order_no, oerr.error_message) outstanding_cost,
                 sum((ol.qty_ordered - nvl(ol.qty_received,0)) * (ol.unit_retail*curr_conv.exchange_rate)) over (partition by ol.order_no, oerr.error_message) outstanding_retail,
                 count(distinct im.standard_uom) over (partition by ol.order_no, oerr.error_message) uom_count,
                 ROW_NUMBER () over (partition by ol.order_no order by ol.item, ol.location, oerr.error_message) rownumb
            from rms_oi_inv_ord_errors oerr,
                 ordhead oh,
                 v_loc_comm_attrib_nosec vl,
                 ordloc ol,
                 item_master im,
                 curr_conv
           where oerr.session_id  = I_session_id
             and oerr.order_no    = oh.order_no
             and oerr.order_no    = ol.order_no
             and ol.item          = im.item
             and ol.location      = vl.loc
             and vl.currency_code = curr_conv.from_currency
             and oh.currency_code = curr_conv.to_currency) use_this
   on (    target.order_no   = use_this.order_no
       and target.session_id = use_this.session_id
       and use_this.rownumb  = 1)
   when matched then update
    set target.total_cost         = use_this.total_cost,
        target.received_percent   = use_this.received_percent,
        target.total_retail       = use_this.total_retail,
        target.total_units        = use_this.total_units,
        target.received_units     = use_this.received_units,
        target.outstanding_units  = use_this.outstanding_units,
        target.outstanding_cost   = use_this.outstanding_cost,
        target.outstanding_retail = use_this.outstanding_retail,
        target.multiple_uom_ind   = DECODE(use_this.uom_count,1, 'N', 'Y');

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge cost/rcv pct rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END ORDER_ERRORS_DETAIL;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS_ITEM_DETAIL(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                  I_orders                 IN     OBJ_NUMERIC_ID_TABLE,
                                  --
                                  I_depts                  IN     OBJ_NUMERIC_ID_TABLE default null,
                                  I_classes                IN     OBJ_NUMERIC_ID_TABLE default null,
                                  I_subclasses             IN     OBJ_NUMERIC_ID_TABLE default null,
                                  I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE default null,
                                  I_areas                  IN     OBJ_NUMERIC_ID_TABLE default null,
                                  I_stores                 IN     OBJ_NUMERIC_ID_TABLE default null,
                                  I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE default null,
                                  I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.ORDER_ERRORS_ITEM_DETAIL';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_ord_err_ref_item_ind           rms_oi_system_options.ia_ord_err_ref_item_ind%TYPE;
   L_ord_err_factory_ind            rms_oi_system_options.ia_ord_err_factory_ind%TYPE;
   L_import_ind                     system_options.import_ind%TYPE;
   L_elc_ind                        system_options.elc_ind%TYPE;
   L_order_hts_ind                  system_options.order_hts_ind%TYPE;

   L_ref_item_error_msg             rtk_errors.rtk_text%type;
   L_hts_error_msg                  rtk_errors.rtk_text%type;
   L_margin_error_msg               rtk_errors.rtk_text%type;
   L_location_group_type            VARCHAR2(1);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select so.ia_ord_err_ref_item_ind,
          so.ia_ord_err_factory_ind,
          rso.import_ind,
          rso.elc_ind,
          rso.order_hts_ind
     into L_ord_err_ref_item_ind,
          L_ord_err_factory_ind,
          L_import_ind,
          L_elc_ind,
          L_order_hts_ind
     from rms_oi_system_options so,
          system_options rso;

   select cd.code_desc 
     into L_ref_item_error_msg
     from v_code_detail_tl cd
    where cd.code_type = 'OEIE'
      and cd.code      = 'REFIT';

   select cd.code_desc 
     into L_hts_error_msg
     from v_code_detail_tl cd
    where cd.code_type = 'OEIE'
      and cd.code      = 'HTS';

   select cd.code_desc 
     into L_margin_error_msg
     from v_code_detail_tl cd
    where cd.code_type = 'OEIE'
      and cd.code      = 'MARG';

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   if OI_UTILITY.SETUP_ITEMS(O_error_message,
                             I_session_id,
                             I_depts,
                             I_classes,
                             I_subclasses,
                             I_supplier_sites,
                             null,          --I_origin_countries
                             I_brands,
                             null) = 0 then --I_forecast_ind
      return OI_UTILITY.FAILURE;
   end if;



   delete from rms_oi_inv_ord_item_errors 
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_item_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if L_ord_err_ref_item_ind = 'Y' then

   --gtt_num_num_str_str_date_date --  number_1     --loc
   --gtt_10_num_10_str_10_date --  varchar2_1 -- item

      insert into rms_oi_inv_ord_item_errors(session_id,
                                             order_no,
                                             item,
                                             item_desc,
                                             item_parent,
                                             item_parent_desc,
                                             vpn,
                                             ref_item,
                                             ref_item_desc,
                                             ref_item_action_ind,
                                             import_order_ind,
                                             margin,
                                             error_message,
                                             order_type,
                                             wf_order_no)
      select distinct I_session_id,
             oh.order_no,
             vim.item,
             vim.item_desc,
             vimp.item,
             vimp.item_desc,
             isup.vpn,
             vimr.item ref_item,
             vimr.item_desc ref_item_desc,
             decode(vimr.item, null, 'N', 'Y') ref_item_action_ind,
             oh.import_order_ind,
             null margin,
             L_ref_item_error_msg,
             oh.order_type,
             oh.wf_order_no
        from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
             ordhead oh,
             ordsku os,
             item_supplier isup,
             v_item_master vim,
             v_item_master vimp,
             item_master vimr,
             ordloc ol,
             gtt_num_num_str_str_date_date gtt_loc,
             gtt_10_num_10_str_10_date gtt_item
       where value(input_orders) = oh.order_no
         and oh.order_no         = os.order_no
         and os.ref_item         is null
         and oh.supplier         = isup.supplier
         and os.item             = isup.item
         and os.item             = vim.item
         and vim.item_parent     = vimp.item(+)
         and os.item             = vimr.item_parent(+)
         and 'Y'                 = vimr.primary_ref_item_ind(+)
         --
         and os.order_no         = ol.order_no
         and os.item             = ol.item
         and os.item             = gtt_item.varchar2_1
         and ol.location         = gtt_loc.number_1;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert ref_item rms_oi_inv_ord_item_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   insert into rms_oi_inv_ord_item_errors(session_id,
                                          order_no,
                                          item,
                                          item_desc,
                                          item_parent,
                                          item_parent_desc,
                                          vpn,
                                          ref_item,
                                          ref_item_desc,
                                          ref_item_action_ind,
                                          import_order_ind,
                                          margin,
                                          error_message,
                                          order_type,
                                          wf_order_no)
   select distinct I_session_id,
          oh.order_no,
          vim.item,
          vim.item_desc,
          vimp.item,
          vimp.item_desc,
          isup.vpn,
          vimr.item,
          vimr.item_desc,
          null ref_item_action_ind,
          oh.import_order_ind,
          null margin,
          L_hts_error_msg,
          oh.order_type,
          oh.wf_order_no
     from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
          ordhead oh,
          ordsku os,
          item_supplier isup,
          v_item_master vim,
          v_item_master vimp,
          v_item_master vimr,
          ordloc ol,
          gtt_num_num_str_str_date_date gtt_loc,
          gtt_10_num_10_str_10_date gtt_item
    where value(input_orders)   = oh.order_no
      and L_import_ind          = 'Y'
      and L_elc_ind             = 'Y'
      and oh.import_order_ind   = 'Y'
      and oh.order_no           = os.order_no
      and oh.import_country_id != os.origin_country_id
      and oh.order_no           = os.order_no
      and oh.supplier           = isup.supplier
      and os.item               = isup.item
      and os.item               = vim.item
      and vim.item_parent       = vimp.item(+)
      and os.ref_item           = vimr.item(+)
      --
      and os.order_no           = ol.order_no
      and os.item               = ol.item
      and os.item               = gtt_item.varchar2_1
      and ol.location           = gtt_loc.number_1
      --
      and ((L_order_hts_ind  = 'Y'
            and not exists (select 'X'
                              from ordsku_hts h
                              where os.order_no      = h.order_no
                                and os.item          = h.item
                                and h.pack_item      is null
                                and h.status         = 'A'
                                and (vim.pack_ind    = 'N' or (vim.pack_ind = 'Y' and vim.pack_type != 'B'))
                              union all
                             select 'X'
                               from ordsku_hts h,
                                    v_packsku_qty v
                              where os.order_no      = h.order_no
                                and os.item          = h.pack_item
                                and h.status         = 'A'
                                and vim.pack_ind     = 'Y'
                                and vim.pack_type    = 'B'
                                and h.pack_item      = v.pack_no
                                and h.item           = v.item))
         or(L_order_hts_ind  = 'N'
            and exists (select 'X'
                          from ordsku_hts h
                          where os.order_no      = h.order_no
                            and os.item          = h.item
                            and h.pack_item      is null
                            and h.status         <> 'A'
                            and (vim.pack_ind    = 'N' or (vim.pack_ind = 'Y' and vim.pack_type != 'B'))
                          union all
                         select 'X'
                           from ordsku_hts h,
                                v_packsku_qty v
                          where os.order_no      = h.order_no
                            and os.item          = h.pack_item
                            and h.status         <> 'A'
                            and vim.pack_ind     = 'Y'
                            and vim.pack_type    = 'B'
                            and h.pack_item      = v.pack_no
                            and h.item           = v.item)));


   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert hts rms_oi_inv_ord_item_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_inv_ord_item_errors(session_id,
                                          order_no,
                                          item,
                                          item_desc,
                                          item_parent,
                                          item_parent_desc,
                                          vpn,
                                          ref_item,
                                          ref_item_desc,
                                          ref_item_action_ind,
                                          import_order_ind,
                                          margin,
                                          error_message,
                                          order_type,
                                          wf_order_no)
   select I_session_id session_id,
          i.order_no,
          i.item,
          i.item_desc,
          i.item_parent,
          i.item_parent_desc,
          i.vpn,
          i.ref_item,
          i.ref_item_desc,
          null ref_item_action_ind,
          i.import_order_ind,
          min(i.margin),
          L_margin_error_msg,
          i.order_type,
          i.wf_order_no
     from (select oh.order_no,
                  vim.item,
                  vim.item_desc,
                  vimp.item item_parent,
                  vimp.item_desc item_parent_desc,
                  isup.vpn,
                  vimr.item ref_item,
                  vimr.item_desc ref_item_desc,
                  oh.import_order_ind,
                  case when d.markup_calc_type = 'R'
                     then (il.unit_retail - ol.unit_cost) / nullif(il.unit_retail,0) 
                     else (il.unit_retail - ol.unit_cost) / nullif(ol.unit_cost,0) end margin,
                  oh.order_type,
                  oh.wf_order_no
             from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
                  ordhead oh,
                  ordsku os,
                  ordloc ol,
                  item_loc il,
                  item_supplier isup,
                  v_item_master vim,
                  v_item_master vimp,
                  v_item_master vimr,
                  deps d,
                  gtt_num_num_str_str_date_date gtt_loc,
                  gtt_10_num_10_str_10_date gtt_item
            where value(input_orders) = oh.order_no
              and oh.order_no         = os.order_no
              and oh.supplier         = isup.supplier
              and os.item             = isup.item
              and os.item             = vim.item
              and vim.item_parent     = vimp.item(+)
              and os.ref_item         = vimr.item(+)
              and os.order_no         = ol.order_no
              and os.item             = ol.item
              and ol.item             = il.item
              and ol.location         = il.loc
              and vim.dept            = d.dept
              and ol.item             = gtt_item.varchar2_1
              and ol.location         = gtt_loc.number_1) i
    where i.margin <= 0
    group by i.order_no,
             i.item,
             i.item_desc,
             i.item_parent,
             i.item_parent_desc,
             i.vpn,
             i.ref_item,
             i.ref_item_desc,
             i.import_order_ind,
             i.order_type,
             i.wf_order_no;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert margin rms_oi_inv_ord_item_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ord_item_errors target
   using (select i.session_id,
                 i.order_no,
                 i.item,
                 min(margin) margin
            from (select oi.session_id,
                         oi.order_no,
                         oi.item,
                         case when d.markup_calc_type = 'R'
                               then (il.unit_retail - ol.unit_cost) / nullif(il.unit_retail,0)
                               else (il.unit_retail - ol.unit_cost) / nullif(ol.unit_cost,0) end margin
                    from rms_oi_inv_ord_item_errors oi,
                         item_master im,
                         deps d,
                         ordloc ol,
                         item_loc il
                   where oi.session_id  = I_session_id
                     and oi.margin      is null
                     and oi.item        = im.item
                     and im.dept        = d.dept
                     and oi.order_no    = ol.order_no
                     and oi.item        = ol.item
                     and ol.item        = il.item
                     and ol.location    = il.loc) i
           group by i.session_id,
                    i.order_no,
                    i.item) use_this
   on (    target.session_id = use_this.session_id
       and target.order_no   = use_this.order_no
       and target.item       = use_this.item)
   when matched then update
    set target.margin = use_this.margin;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge margin rms_oi_inv_ord_item_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END ORDER_ERRORS_ITEM_DETAIL;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DELETE_SESSION_RECORDS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN
   
   delete from rms_oi_inv_ana_variance    where session_id = I_session_id;
   delete from rms_oi_inv_ana_open_order  where session_id = I_session_id;
   delete from rms_oi_inv_ord_errors      where session_id = I_session_id;
   delete from rms_oi_inv_ord_item_errors where session_id = I_session_id;

   if OI_UTILITY.DELETE_SESSION_ID_LOG(O_error_message,
                                       I_session_id) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_RECORDS;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_VARIANCE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DELETE_SESSION_VARIANCE';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN
   
   delete from rms_oi_inv_ana_variance    where session_id = I_session_id;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_VARIANCE;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OPEN_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DELETE_SESSION_OPEN_ORDER';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN

   delete from rms_oi_inv_ana_open_order  where session_id = I_session_id;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_OPEN_ORDER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ORD_ERRS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DELETE_SESSION_ORD_ERRS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN
   
   delete from rms_oi_inv_ord_errors      where session_id = I_session_id;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_ORD_ERRS;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ORD_ITEM_ERRS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_INV_ANALYST.DELETE_SESSION_ORD_ITEM_ERRS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
BEGIN

   delete from rms_oi_inv_ord_item_errors where session_id = I_session_id;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_ORD_ITEM_ERRS;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION SETUP_WHS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_session_id           IN     oi_session_id_log.session_id%TYPE,
                   I_item                 IN     item_master.item%TYPE,
                   I_item_type            IN     item_master.alc_item_type%TYPE,
                   I_agg_diff_1           IN     item_master.diff_1%TYPE,
                   I_agg_diff_2           IN     item_master.diff_2%TYPE,
                   I_agg_diff_3           IN     item_master.diff_3%TYPE,
                   I_agg_diff_4           IN     item_master.diff_4%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.SETUP_WHS';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_count       NUMBER(10)   := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');


   -- From OI_UTILITY.SETUP_STORES gtt_num_num_str_str_date_date has
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   -- put the stores in numeric_id_gtt so we can get their sourcing whs into gtt_num_num_str_str_date_date

   delete from numeric_id_gtt;
   insert into numeric_id_gtt select number_1 from gtt_num_num_str_str_date_date;

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1,
                                             varchar2_2)
   select distinct i2.source_wh,
                   i2.source_wh,
                   i2.wh_name
     from (select i.item,
                  i.store,
                  i.source_wh,
                  i.wh_name,
                  i.rnk,
                  min(i.rnk) over (partition by i.item, i.store)  min_rnk
             from (select distinct il.item,
                                   gtt.numeric_id store,
                                   il.source_wh,
                                   v.wh_name,
                                   1 rnk
                     from numeric_id_gtt gtt,
                          item_loc il,
                          v_wh v,
                          item_loc ilw
                    where I_item_type      not in('STYLE','FA')
                      and il.item          = I_item
                      and il.loc           = gtt.numeric_id
                      and il.source_method = 'W'
                      and il.source_wh     is not null
                      and il.source_wh     = v.wh
                      and il.status        = 'A'
                      and il.item          = ilw.item
                      and ilw.loc          = v.wh
                      and ilw.status       = 'A'
                   union all
                   select distinct il.item, 
                                   gtt.numeric_id store,
                                   il.source_wh,
                                   v.wh_name,
                                   1 rnk
                     from numeric_id_gtt gtt,
                          item_loc il,
                          v_wh v,
                          item_loc ilw
                    where I_item_type      = 'STYLE'
                      and il.item_parent   = I_item
                      and il.loc           = gtt.numeric_id
                      and il.source_method = 'W'
                      and il.source_wh     is not null
                      and il.source_wh     = v.wh
                      and il.status        = 'A'
                      and il.item          = ilw.item
                      and ilw.loc          = v.wh
                      and ilw.status       = 'A'
                   union all
                   select distinct il.item,
                                   gtt.numeric_id store,
                                   il.source_wh,
                                   v.wh_name,
                                   1 rnk
                     from numeric_id_gtt gtt,
                          item_master im,
                          item_loc il,
                          v_wh v,
                          item_loc ilw
                    where I_item_type           = 'FA'
                      and im.item_parent        = I_item
                      and nvl(im.diff_1,'NU|L') = nvl(I_agg_diff_1, nvl(im.diff_1,'NU|L'))
                      and nvl(im.diff_2,'NU|L') = nvl(I_agg_diff_2, nvl(im.diff_2,'NU|L'))
                      and nvl(im.diff_3,'NU|L') = nvl(I_agg_diff_3, nvl(im.diff_3,'NU|L'))
                      and nvl(im.diff_4,'NU|L') = nvl(I_agg_diff_4, nvl(im.diff_4,'NU|L'))
                      and il.item               = im.item
                      and il.loc                = gtt.numeric_id
                      and il.source_method      = 'W'
                      and il.source_wh          is not null
                      and il.source_wh          = v.wh
                      and il.status             = 'A'
                      and il.item               = ilw.item
                      and ilw.loc               = v.wh
                      and ilw.status            = 'A'
                   union all
                      select distinct il.item,
                                      gtt.numeric_id store,
                                      v.wh,
                                      v.wh_name,
                                      2 rnk
                        from numeric_id_gtt gtt,
                             item_loc il,
                             v_wh v,
                             store s
                       where I_item_type      not in('STYLE','FA')
                         and il.item          = I_item
                         and s.store          = gtt.numeric_id
                         and s.default_wh     = v.wh
                         and v.wh             = il.loc
                         and il.status        = 'A'
                      union all
                      select distinct il.item,
                                      gtt.numeric_id store,
                                      v.wh,
                                      v.wh_name,
                                      2 rnk
                        from numeric_id_gtt gtt,
                             item_loc il,
                             v_wh v,
                             store s
                       where I_item_type      = 'STYLE'
                         and il.item_parent   = I_item
                         and s.store          = gtt.numeric_id
                         and s.default_wh     = v.wh
                         and v.wh             = il.loc
                         and il.status        = 'A'
                      union all
                      select distinct il.item,
                                      gtt.numeric_id store,
                                      v.wh,
                                      v.wh_name,
                                      2 rnk
                        from numeric_id_gtt gtt,
                             item_master im,
                             item_loc il,
                             v_wh v,
                             store s
                       where I_item_type           = 'FA'
                         and im.item_parent        = I_item
                         and nvl(im.diff_1,'NU|L') = nvl(I_agg_diff_1, nvl(im.diff_1,'NU|L'))
                         and nvl(im.diff_2,'NU|L') = nvl(I_agg_diff_2, nvl(im.diff_2,'NU|L'))
                         and nvl(im.diff_3,'NU|L') = nvl(I_agg_diff_3, nvl(im.diff_3,'NU|L'))
                         and nvl(im.diff_4,'NU|L') = nvl(I_agg_diff_4, nvl(im.diff_4,'NU|L'))
                         and il.item               = im.item
                         and s.store               = gtt.numeric_id
                         and s.default_wh          = v.wh
                         and v.wh                  = il.loc
                         and il.status             = 'A'
                  ) i
          ) i2
    where i2.rnk = i2.min_rnk;
                
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert gtt_num_num_str_str_date_date default wh - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END SETUP_WHS;
--------------------------------------------------------------------------------
FUNCTION POP_ILSOH_CONTEXT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_session_id              IN     oi_session_id_log.session_id%TYPE,
                           I_item                    IN     item_master.item%TYPE,
                           I_item_type               IN     item_master.alc_item_type%TYPE,
                           I_agg_diff_1              IN     item_master.diff_1%TYPE,
                           I_agg_diff_2              IN     item_master.diff_2%TYPE,
                           I_agg_diff_3              IN     item_master.diff_3%TYPE,
                           I_agg_diff_4              IN     item_master.diff_4%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.POP_ILSOH_CONTEXT';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   insert into rms_oi_inv_ana_inv_gtt (session_id,
                                       item,
                                       loc,
                                       grouping_loc_id,
                                       grouping_loc_name,
                                       stock_on_hand,
                                       in_transit_qty,
                                       pack_comp_intran,
                                       pack_comp_soh,
                                       tsf_reserved_qty,
                                       pack_comp_resv,
                                       tsf_expected_qty,
                                       pack_comp_exp,
                                       rtv_qty,
                                       non_sellable_qty,
                                       pack_comp_non_sellable,
                                       customer_resv,
                                       pack_comp_cust_resv,
                                       customer_backorder,
                                       pack_comp_cust_back,
                                       forecast_sales,
                                       on_order_qty,
                                       co_inbound,
                                       pl_tsf_inbound,
                                       alloc_inbound,
                                       alloc_outbound,
                                       avail_qty)
   select I_session_id,
          ils.item,
          ils.loc,
          gtt.varchar2_1,
          gtt.varchar2_2,
          ils.stock_on_hand,
          ils.in_transit_qty,
          ils.pack_comp_intran,
          ils.pack_comp_soh,
          ils.tsf_reserved_qty,
          ils.pack_comp_resv,
          ils.tsf_expected_qty,
          ils.pack_comp_exp,
          ils.rtv_qty,
          ils.non_sellable_qty,
          ils.pack_comp_non_sellable,
          ils.customer_resv,
          ils.pack_comp_cust_resv,
          ils.customer_backorder,
          ils.pack_comp_cust_back,
          0 forecast_sales,
          0 on_order_qty,
          0 co_inbound,
          0 pl_tsf_inbound,
          0 alloc_inbound,
          0 alloc_outbound,
          0 avail_qty
     from gtt_num_num_str_str_date_date gtt,
          item_loc_soh ils,
          item_loc il
    where I_item_type not in('STYLE','FA')
      and ils.item        = I_item
      and ils.loc         = gtt.number_1
      and ils.item        = il.item
      and ils.loc         = il.loc
      and il.status       = 'A'
   union all
   select I_session_id,
          ils.item,
          ils.loc,
          gtt.varchar2_1,
          gtt.varchar2_2,
          ils.stock_on_hand,
          ils.in_transit_qty,
          ils.pack_comp_intran,
          ils.pack_comp_soh,
          ils.tsf_reserved_qty,
          ils.pack_comp_resv,
          ils.tsf_expected_qty,
          ils.pack_comp_exp,
          ils.rtv_qty,
          ils.non_sellable_qty,
          ils.pack_comp_non_sellable,
          ils.customer_resv,
          ils.pack_comp_cust_resv,
          ils.customer_backorder,
          ils.pack_comp_cust_back,
          0 forecast_sales,
          0 on_order_qty,
          0 co_inbound,
          0 pl_tsf_inbound,
          0 alloc_inbound,
          0 alloc_outbound,
          0 avail_qty
     from gtt_num_num_str_str_date_date gtt,
          item_loc_soh ils,
          item_loc il
    where I_item_type     = 'STYLE'
      and ils.item_parent = I_item
      and ils.loc         = gtt.number_1
      and ils.item        = il.item
      and ils.loc         = il.loc
      and il.status       = 'A'
   union all
   select I_session_id,
          ils.item,
          ils.loc,
          gtt.varchar2_1,
          gtt.varchar2_2,
          ils.stock_on_hand,
          ils.in_transit_qty,
          ils.pack_comp_intran,
          ils.pack_comp_soh,
          ils.tsf_reserved_qty,
          ils.pack_comp_resv,
          ils.tsf_expected_qty,
          ils.pack_comp_exp,
          ils.rtv_qty,
          ils.non_sellable_qty,
          ils.pack_comp_non_sellable,
          ils.customer_resv,
          ils.pack_comp_cust_resv,
          ils.customer_backorder,
          ils.pack_comp_cust_back,
          0 forecast_sales,
          0 on_order_qty,
          0 co_inbound,
          0 pl_tsf_inbound,
          0 alloc_inbound,
          0 alloc_outbound,
          0 avail_qty
     from gtt_num_num_str_str_date_date gtt,
          item_master im,
          item_loc_soh ils,
          item_loc il
    where I_item_type           = 'FA'
      and im.item_parent        = I_item
      and nvl(im.diff_1,'NU|L') = nvl(I_agg_diff_1, nvl(im.diff_1,'NU|L'))
      and nvl(im.diff_2,'NU|L') = nvl(I_agg_diff_2, nvl(im.diff_2,'NU|L'))
      and nvl(im.diff_3,'NU|L') = nvl(I_agg_diff_3, nvl(im.diff_3,'NU|L'))
      and nvl(im.diff_4,'NU|L') = nvl(I_agg_diff_4, nvl(im.diff_4,'NU|L'))
      and ils.item              = im.item
      and ils.loc               = gtt.number_1
      and ils.item              = il.item
      and ils.loc               = il.loc
      and il.status             = 'A';

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END POP_ILSOH_CONTEXT;
--------------------------------------------------------------------------------
FUNCTION POP_ILSOH_FILTER(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id              IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.POP_ILSOH_FILTER';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   delete from rms_oi_inv_ana_inv_gtt;

   --gtt_num_num_str_str_date_date
   --  number_1     --loc
   --  varchar2_1   --grouping_loc_id
   --  varchar2_2   --grouping_loc_name
   -------------------------------------
   --gtt_10_num_10_str_10_date
   --  varchar2_1 -- item
   --  varchar2_2 -- item_parent
   --  varchar2_3 -- item_desc
   --  varchar2_4 -- alc_item_type
   --  varchar2_5 -- agg_diff_1
   --  varchar2_6 -- agg_diff_2
   --  varchar2_7 -- agg_diff_3
   --  varchar2_8 -- agg_diff_4
   --  varchar2_9  -- item_parent_desc
   --  varchar2_10 -- standard_uom

   insert into rms_oi_inv_ana_inv_gtt (session_id,
                                       item,
                                       item_parent,
                                       item_desc,
                                       alc_item_type,
                                       agg_diff_1,
                                       agg_diff_2,
                                       agg_diff_3,
                                       agg_diff_4,
                                       loc,
                                       grouping_loc_id,
                                       grouping_loc_name,
                                       stock_on_hand,
                                       in_transit_qty,
                                       pack_comp_intran,
                                       pack_comp_soh,
                                       tsf_reserved_qty,
                                       pack_comp_resv,
                                       tsf_expected_qty,
                                       pack_comp_exp,
                                       rtv_qty,
                                       non_sellable_qty,
                                       pack_comp_non_sellable,
                                       customer_resv,
                                       pack_comp_cust_resv,
                                       customer_backorder,
                                       pack_comp_cust_back,
                                       forecast_sales,
                                       forecast_sales_hist,
                                       on_order_qty,
                                       co_inbound,
                                       pl_tsf_inbound,
                                       alloc_inbound,
                                       alloc_outbound,
                                       sales_hist_qty,
                                       unit_retail,
                                       avail_qty)
   with item_locs as (select gttloc.number_1 loc,
                             gttloc.varchar2_1 grouping_loc_id,
                             gttloc.varchar2_2 grouping_loc_name,
                             --
                             gttitem.varchar2_1 item,
                             gttitem.varchar2_2 item_parent,
                             gttitem.varchar2_3 item_desc,
                             gttitem.varchar2_4 alc_item_type,
                             gttitem.varchar2_5 agg_diff_1,
                             gttitem.varchar2_6 agg_diff_2,
                             gttitem.varchar2_7 agg_diff_3,
                             gttitem.varchar2_8 agg_diff_4
                        from gtt_num_num_str_str_date_date gttloc,
                             gtt_10_num_10_str_10_date gttitem
                       where rownum > 0
   )
   select I_session_id,
          ils.item,
          ils.item_parent,
          item_locs.item_desc,
          item_locs.alc_item_type,
          item_locs.agg_diff_1,
          item_locs.agg_diff_2,
          item_locs.agg_diff_3,
          item_locs.agg_diff_4,
          ils.loc,
          item_locs.grouping_loc_id,
          item_locs.grouping_loc_name,
          ils.stock_on_hand,
          ils.in_transit_qty,
          ils.pack_comp_intran,
          ils.pack_comp_soh,
          ils.tsf_reserved_qty,
          ils.pack_comp_resv,
          ils.tsf_expected_qty,
          ils.pack_comp_exp,
          ils.rtv_qty,
          ils.non_sellable_qty,
          ils.pack_comp_non_sellable,
          ils.customer_resv,
          ils.pack_comp_cust_resv,
          ils.customer_backorder,
          ils.pack_comp_cust_back,
          null forecast_sales,
          null forecast_sales_hist,
          0 on_order_qty,
          0 co_inbound,
          0 pl_tsf_inbound,
          0 alloc_inbound,
          0 alloc_outbound,
          0 sales_hist_qty,
          0 unit_retail,
          0 avail_qty
     from item_locs,
          item_loc_soh ils
    where item_locs.item = ils.item
      and item_locs.loc  = ils.loc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END POP_ILSOH_FILTER;
--------------------------------------------------------------------------------
FUNCTION MERGE_FORECAST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_session_id              IN     oi_session_id_log.session_id%TYPE,
                        I_start_date              IN     DATE,
                        I_end_date                IN     DATE)
RETURN NUMBER IS
   L_program           VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_FORECAST';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;
   L_first_week_eow    DATE;
   L_first_week_ratio  NUMBER(20,10);
   L_hist_ratio        NUMBER(20,10);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select d.eow_date,
          ((d.eow_date - d.day) + 1) / 7,
          1 - (((d.eow_date - d.day) + 1) / 7)
     into L_first_week_eow,
          L_first_week_ratio,
          L_hist_ratio
     from day_level_calendar d
    where d.day = I_start_date;

   LOGGER.LOG_INFORMATION(L_program||' L_first_week_ratio: '||L_first_week_ratio);

   merge into rms_oi_inv_ana_inv_gtt target
   using (
          with week_f as (select f.item,
                                 f.loc,
                                 f.eow_date,
                                 sum(decode(f.eow_date, L_first_week_eow,
                                            f.forecast_sales * L_first_week_ratio,
                                            f.forecast_sales)
                                 ) forecast_sales,
                                 sum(decode(f.eow_date, L_first_week_eow,
                                            f.forecast_sales * L_hist_ratio,
                                            0)
                                 ) forecast_sales_hist
                            from rms_oi_inv_ana_inv_gtt oi,
                                 item_forecast f
                           where oi.item    = f.item
                             and oi.loc     = f.loc
                             and f.eow_date between I_start_date and I_end_date
                           group by f.item,
                                    f.loc,
                                    f.eow_date)
          select week_f.item,
                 week_f.loc,
                 sum(week_f.forecast_sales) forecast_sales,
                 sum(week_f.forecast_sales_hist) forecast_sales_hist
            from week_f
           group by week_f.item,
                    week_f.loc
   ) use_this
   on (    target.item = use_this.item
       and target.loc  = use_this.loc)
   when matched then update
    set target.forecast_sales      = use_this.forecast_sales,
        target.forecast_sales_hist = target.forecast_sales_hist + use_this.forecast_sales_hist;
   ---
   --round values when standard UOM is EA
   merge into rms_oi_inv_ana_inv_gtt target2
   using (select t.item,
                 t.loc
            from rms_oi_inv_ana_inv_gtt t,
                 item_master im
           where t.item = im.item
             and im.standard_uom = 'EA') use_this
   on (    target2.item = use_this.item
       and target2.loc  = use_this.loc)
   when matched then update
    set target2.forecast_sales      = round(target2.forecast_sales),
        target2.forecast_sales_hist = round(target2.forecast_sales_hist);
   

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge forcasts rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_FORECAST;
--------------------------------------------------------------------------------
FUNCTION MERGE_FORECAST_HIST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id              IN     oi_session_id_log.session_id%TYPE,
                             I_start_date              IN     DATE,
                             I_end_date                IN     DATE)
RETURN NUMBER IS
   L_program           VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_FORECAST_HIST';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;
   L_first_week_eow    DATE;
   L_first_week_ratio  NUMBER(20,10);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   merge into rms_oi_inv_ana_inv_gtt target
   using (select f.item,
                 f.loc,
                 sum(f.forecast_sales) forecast_sales_hist
            from rms_oi_inv_ana_inv_gtt oi,
                 item_forecast_hist f
           where oi.item    = f.item
             and oi.loc     = f.loc
             and f.eow_date between I_start_date and I_end_date
           group by f.item,
                    f.loc) use_this
   on (    target.item = use_this.item
       and target.loc  = use_this.loc)
   when matched then update
    set target.forecast_sales_hist = target.forecast_sales_hist + use_this.forecast_sales_hist;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge forcasts hist rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_FORECAST_HIST;
--------------------------------------------------------------------------------
FUNCTION MERGE_ON_ORDER(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_session_id              IN     oi_session_id_log.session_id%TYPE,
                        I_forward_date            IN     DATE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_ON_ORDER';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
   L_vdate       DATE         := get_vdate;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   merge into rms_oi_inv_ana_inv_gtt target
   using(select /*+ ordered index(ol ordloc_i1) */
                oi.item,
                oi.loc,
                sum(ol.qty_ordered - nvl(ol.qty_received,0)) on_order_qty
           from rms_oi_inv_ana_inv_gtt oi,
                ordloc ol,
                ordhead oh
          where oh.status               = 'A'
            and oh.order_type          != 'CO'
            and oh.include_on_order_ind = 'Y'
            and oh.order_no             = ol.order_no
            and oh.not_before_date     <= I_forward_date
            and ol.location             = oi.loc
            and ol.item                 = oi.item
            and ol.qty_ordered          > nvl(ol.qty_received,0)
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.on_order_qty = use_this.on_order_qty;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge on_order rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ana_inv_gtt target
   using(select /*+ ordered index(ol ordloc_i1) */
                oi.item,
                oi.loc,
                sum(p.pack_item_qty * (ol.qty_ordered - nvl(ol.qty_received,0))) on_order_qty
           from rms_oi_inv_ana_inv_gtt oi,
                ordloc ol,
                ordhead oh,
                packitem_breakout p
          where oh.status               = 'A'
            and oh.order_type          != 'CO'
            and oh.include_on_order_ind = 'Y'
            and oh.order_no             = ol.order_no
            and oh.not_before_date     <= I_forward_date
            and ol.location             = oi.loc
            and ol.qty_ordered          > nvl(ol.qty_received,0)
            --
            and oi.item                 = p.item
            and p.pack_no               = ol.item
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.on_order_qty = target.on_order_qty + use_this.on_order_qty;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge on_order pack comp rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);


   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_ON_ORDER;
--------------------------------------------------------------------------------
FUNCTION MERGE_CO_INBOUND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id              IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_CO_INBOUND';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   merge into rms_oi_inv_ana_inv_gtt target
   using(select oi.item,
                oi.loc,
                nvl(sum((d.tsf_qty - nvl(d.ship_qty,0))),0) co_inbound
           from rms_oi_inv_ana_inv_gtt oi,
                tsfhead h,
                tsfdetail d
          where oi.item          = d.item
            and oi.loc           = h.to_loc
            and h.tsf_no         = d.tsf_no
            and h.status         in ('A','S','P','L','C')
            and h.tsf_type       = 'CO'
            and d.tsf_qty        > nvl(d.ship_qty,0)
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.co_inbound = use_this.co_inbound;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge co_inbound rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ana_inv_gtt target
   using(select oi.item,
                oi.loc,
                nvl(sum(p.pack_item_qty * (d.tsf_qty - nvl(d.ship_qty,0))),0) co_inbound
           from rms_oi_inv_ana_inv_gtt oi,
                tsfhead h,
                tsfdetail d,
                packitem_breakout p
          where oi.loc           = h.to_loc
            and h.tsf_no         = d.tsf_no
            and h.status         in ('A','S','P','L','C')
            and h.tsf_type       = 'CO'
            and d.tsf_qty        > nvl(d.ship_qty,0)
            --
            and oi.item          = p.item
            and p.pack_no        = d.item
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.co_inbound = target.co_inbound + use_this.co_inbound;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge co_inbound pack comp rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_CO_INBOUND;
--------------------------------------------------------------------------------
FUNCTION MERGE_WH_CROSSLINK(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_session_id              IN     oi_session_id_log.session_id%TYPE,
                            I_forward_date            IN     DATE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_WH_CROSSLINK';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

   L_wh_crosslink_ind    system_options.wh_cross_link_ind%TYPE := null;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if SYSTEM_OPTIONS_SQL.GET_WH_CROSS_LINK_IND(O_error_message,
                                               L_wh_crosslink_ind) = FALSE then
      return OI_UTILITY.FAILURE;
   end if;

   if L_wh_crosslink_ind = 'Y' then

      merge into rms_oi_inv_ana_inv_gtt target
      using(select oi.item,
                   oi.loc,
                   nvl(sum((d.tsf_qty - nvl(d.ship_qty,0))),0) pl_tsf_inbound
              from rms_oi_inv_ana_inv_gtt oi,
                   tsfhead h,
                   tsfdetail d
             where oi.item                 = d.item
               and oi.loc                  = h.to_loc
               and h.tsf_no                = d.tsf_no
               and h.status                in ('A','E','S','B')
               and h.tsf_type              = 'PL'
               and d.tsf_qty               > nvl(d.ship_qty,0)
               and trunc(h.delivery_date) <= I_forward_date
             group by oi.item,
                      oi.loc) use_this
         on (    target.item = use_this.item
             and target.loc  = use_this.loc)
         when matched then update
          set target.pl_tsf_inbound = use_this.pl_tsf_inbound;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' merge pl_tsf_inbound rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

      merge into rms_oi_inv_ana_inv_gtt target
      using(select oi.item,
                   oi.loc,
                   nvl(sum(p.pack_item_qty * (d.tsf_qty - nvl(d.ship_qty,0))),0) pl_tsf_inbound
              from rms_oi_inv_ana_inv_gtt oi,
                   tsfhead h,
                   tsfdetail d,
                   packitem_breakout p
             where oi.loc                  = h.to_loc
               and h.tsf_no                = d.tsf_no
               and h.status                in ('A','E','S','B')
               and h.tsf_type              = 'PL'
               and d.tsf_qty               > nvl(d.ship_qty,0)
               and trunc(h.delivery_date) <= I_forward_date
               --
               and oi.item                 = p.item
               and p.pack_no               = d.item
             group by oi.item,
                      oi.loc) use_this
         on (    target.item = use_this.item
             and target.loc  = use_this.loc)
         when matched then update
          set target.pl_tsf_inbound = target.pl_tsf_inbound + use_this.pl_tsf_inbound;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' merge pl_tsf_inbound pack comp rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_WH_CROSSLINK;
--------------------------------------------------------------------------------
FUNCTION MERGE_ALLOC_INBOUND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id              IN     oi_session_id_log.session_id%TYPE,
                             I_forward_date            IN     DATE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_ALLOC_INBOUND';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   merge into rms_oi_inv_ana_inv_gtt target
   using(select oi.item,
                oi.loc,
                NVL(sum((d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_inbound
           from alloc_header h,
                rms_oi_inv_ana_inv_gtt oi,
                alloc_detail d
          where h.status        in ('A', 'R')
            and h.item          = oi.item
            and d.alloc_no      = h.alloc_no
            and d.to_loc        = oi.loc
            and d.qty_allocated > NVL(d.qty_transferred,0)
            and (exists (select 'x'
                           from ordhead o
                          where o.order_no                                    = h.order_no
                            and o.status                                      in ('A', 'C')
                            and NVL(I_forward_date, trunc(o.not_before_date)) >= trunc(o.not_before_date)
                            and rownum = 1) or
                 exists (select 'x'
                           from alloc_header h1
                          where h1.alloc_no                             = h.order_no
                            and h1.status                               in ('A', 'R')
                            and NVL(I_forward_date, h1.release_date) >= h1.release_date
                            and rownum = 1) or
                 exists (select 'x'
                           from tsfhead t
                          where t.tsf_no          = h.order_no
                            and t.status          in ('A','S','P','L','C')
                            and (t.not_after_date is null or
                                 (NVL(I_forward_date, nvl(t.delivery_date,sysdate)) <= nvl(t.delivery_date,sysdate)))
                            and rownum = 1))
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.alloc_inbound = use_this.alloc_inbound;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge alloc_inbound rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ana_inv_gtt target
   using(select oi.item,
                oi.loc,
                NVL(sum(p.pack_item_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_inbound
           from alloc_header h,
                rms_oi_inv_ana_inv_gtt oi,
                alloc_detail d,
                packitem_breakout p
          where h.status        in ('A', 'R')
            and d.alloc_no      = h.alloc_no
            and d.to_loc        = oi.loc
            and d.qty_allocated > NVL(d.qty_transferred,0)
            --
            and oi.item         = p.item
            and p.pack_no       = h.item
            and (exists (select 'x'
                           from ordhead o
                          where o.order_no                                    = h.order_no
                            and o.status                                      in ('A', 'C')
                            and NVL(I_forward_date, trunc(o.not_before_date)) >= trunc(o.not_before_date)
                            and rownum = 1) or
                 exists (select 'x'
                           from alloc_header h1
                          where h1.alloc_no                             = h.order_no
                            and h1.status                               in ('A', 'R')
                            and NVL(I_forward_date, h1.release_date) >= h1.release_date
                            and rownum = 1) or
                 exists (select 'x'
                           from tsfhead t
                          where t.tsf_no          = h.order_no
                            and t.status          in ('A','S','P','L','C')
                            and (t.not_after_date is null or
                                 (NVL(I_forward_date, nvl(t.delivery_date,sysdate)) <= nvl(t.delivery_date,sysdate)))
                            and rownum = 1))
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.alloc_inbound = target.alloc_inbound + use_this.alloc_inbound;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge alloc_inbound pack comp rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_ALLOC_INBOUND;
--------------------------------------------------------------------------------
FUNCTION MERGE_ALLOC_OUTBOUND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_session_id              IN     oi_session_id_log.session_id%TYPE,
                              I_forward_date            IN     DATE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_ALLOC_OUTBOUND';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   merge into rms_oi_inv_ana_inv_gtt target
   using(select oi.item,
                oi.loc,
                NVL(sum((d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_outbound
           from alloc_header h,
                rms_oi_inv_ana_inv_gtt oi,
                alloc_detail d
          where h.status        in ('A', 'R')
            and h.item          = oi.item
            and d.alloc_no      = h.alloc_no
            and h.wh            = oi.loc
            and d.qty_allocated > NVL(d.qty_transferred,0)
            and (exists (select 'x'
                           from ordhead o
                          where o.order_no                  = h.order_no
                            and o.status                    in ('A', 'C')
                            and NVL(I_forward_date, trunc(o.not_before_date)) >= trunc(o.not_before_date)
                            and rownum = 1) or
                 exists (select 'x'
                           from alloc_header h1
                          where h1.alloc_no                             = h.order_no
                            and h1.status                               in ('A', 'R')
                            and NVL(I_forward_date, h1.release_date) <= h1.release_date
                            and rownum = 1) or
                 exists (select 'x'
                           from tsfhead t
                          where t.tsf_no          = h.order_no
                            and t.status          in ('A','S','P','L','C')
                            and (t.not_after_date is null or
                                 (NVL(I_forward_date, nvl(t.delivery_date,sysdate)) <= nvl(t.delivery_date,sysdate)))
                            and rownum = 1))
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.alloc_outbound = use_this.alloc_outbound;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge alloc_outbound rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ana_inv_gtt target
   using(select oi.item,
                oi.loc,
                NVL(sum(p.pack_item_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_outbound
           from alloc_header h,
                rms_oi_inv_ana_inv_gtt oi,
                alloc_detail d,
                packitem_breakout p
          where h.status        in ('A', 'R')
            and d.alloc_no      = h.alloc_no
            and h.wh            = oi.loc
            and d.qty_allocated > NVL(d.qty_transferred,0)
            --
            and oi.item         = p.item
            and p.pack_no       = h.item
            and (exists (select 'x'
                           from ordhead o
                          where o.order_no                  = h.order_no
                            and o.status                    in ('A', 'C')
                            and NVL(I_forward_date, trunc(o.not_before_date)) >= trunc(o.not_before_date)
                            and rownum = 1) or
                 exists (select 'x'
                           from alloc_header h1
                          where h1.alloc_no                             = h.order_no
                            and h1.status                               in ('A', 'R')
                            and NVL(I_forward_date, h1.release_date) <= h1.release_date
                            and rownum = 1) or
                 exists (select 'x'
                           from tsfhead t
                          where t.tsf_no          = h.order_no
                            and t.status          in ('A','S','P','L','C')
                            and (t.not_after_date is null or
                                 (NVL(I_forward_date, nvl(t.delivery_date,sysdate)) <= nvl(t.delivery_date,sysdate)))
                            and rownum = 1))
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.alloc_outbound = target.alloc_outbound + use_this.alloc_outbound;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge alloc_outbound pack comp rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_ALLOC_OUTBOUND;
--------------------------------------------------------------------------------
FUNCTION MERGE_SALES_HIST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id              IN     oi_session_id_log.session_id%TYPE,
                          I_history_date            IN     DATE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_SALES_HIST';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   merge into rms_oi_inv_ana_inv_gtt target
   using(select /*+ ordered index(ilh PK_ITEM_LOC_HIST) */
                oi.item,
                oi.loc,
                sum(ilh.sales_issues) sales_hist_qty
           from rms_oi_inv_ana_inv_gtt oi,
                item_loc_hist ilh
          where oi.item = ilh.item
            and oi.loc  = ilh.loc
            and ilh.eow_date >= I_history_date
          group by oi.item,
                   oi.loc) use_this
      on (    target.item = use_this.item
          and target.loc  = use_this.loc)
      when matched then update
       set target.sales_hist_qty = use_this.sales_hist_qty;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge sales_hist_qty rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_SALES_HIST;
--------------------------------------------------------------------------------
FUNCTION MERGE_UNIT_RETAIL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_session_id              IN     oi_session_id_log.session_id%TYPE,
                           I_store_count             IN     NUMBER)
RETURN NUMBER IS
   L_program           VARCHAR2(61) := 'RMS_OI_INV_ANALYST.MERGE_UNIT_RETAIL';
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;
   L_vdate             DATE         := get_vdate;
   L_primary_currency  system_options.currency_code%TYPE;
   L_consolidation_ind system_options.consolidation_ind%TYPE;
   L_rpm_ind           varchar2(1);
   cursor C_RPM_IND  is 
      select RPM_IND 
	    from 
	   system_options;	      

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select currency_code,
          consolidation_ind
     into L_primary_currency,
          L_consolidation_ind
     from system_options;
	 
   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;	 	 

  merge into rms_oi_inv_ana_inv_gtt target
   using(with curr_conv as (select from_currency,
                                   to_currency,
                                   effective_date,
                                   exchange_rate,
                                   exchange_type
                              from (select from_currency,
                                           to_currency,
                                           effective_date,
                                           exchange_rate,
                                           exchange_type,
                                           rank() over (partition by from_currency,
                                                                     to_currency                                                     
                                                            order by decode(exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                     effective_date desc) date_rank
                                      from mv_currency_conversion_rates
                                     where effective_date <= L_vdate
                                       and ((L_consolidation_ind = 'Y' and exchange_type in('P', 'C', 'O'))
                                            or
                                            (L_consolidation_ind = 'N' and exchange_type in('P', 'O')))
                                   )
                             where date_rank = 1)
         select oi.item,
                avg(il.unit_retail * curr_conv.exchange_rate) unit_retail
           from rms_oi_inv_ana_inv_gtt oi,
                store st,
                item_loc il,
                curr_conv
          where I_store_count           = 1
            and oi.loc                  = st.store
            and oi.item                 = il.item
            and oi.loc                  = il.loc
            and curr_conv.from_currency = st.currency_code
            and curr_conv.to_currency   = L_primary_currency
          group by oi.item
         union all
         select oi.item,
                avg(p.standard_retail * curr_conv.exchange_rate) unit_retail
           from rms_oi_inv_ana_inv_gtt oi,
                item_master im,
                rpm_item_zone_price p,
                rpm_zone z,
                rpm_zone_group g,
                rpm_merch_retail_def_expl d,
                curr_conv
          where I_store_count          != 1
		        and L_rpm_ind               ='Y'                     -- Change to Fetch Standard retail from RPM zone tables when RPM_IND is 'Y'
            and oi.item                 = im.item
            and im.dept                 = d.dept
            and im.class                = d.class
            and im.subclass             = d.subclass
            and g.zone_group_id         = d.regular_zone_group
            and g.zone_group_id         = z.zone_group_id
            and z.base_ind              = 1
            and p.zone_id               = z.zone_id
            and p.item                  = oi.item
            --
            and curr_conv.from_currency = p.standard_retail_currency
            and curr_conv.to_currency   = L_primary_currency
           group by oi.item
          union all                                            
          select  oi.item oi,
                  im.curr_selling_unit_retail unit_retail
             from rms_oi_inv_ana_inv_gtt oi,
                  item_master im
           where I_store_count != 1	
             and L_rpm_ind      =  'N'
             and im.item        =  oi.item                         ---- Change to Fetch Standard retail from RPM zone tables when RPM_IND is 'N'             		                   				  
          ) use_this
      on (target.item = use_this.item)
      when matched then update
       set target.unit_retail = use_this.unit_retail;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge unit_retail rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rms_oi_inv_ana_inv_gtt target
   using(with curr_conv as (select from_currency,
                                   to_currency,
                                   effective_date,
                                   exchange_rate,
                                   exchange_type
                              from (select from_currency,
                                           to_currency,
                                           effective_date,
                                           exchange_rate,
                                           exchange_type,
                                           rank() over (partition by from_currency,
                                                                     to_currency
                                                            order by decode(exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                     effective_date desc) date_rank
                                      from mv_currency_conversion_rates
                                     where effective_date <= L_vdate
                                       and ((L_consolidation_ind = 'Y' and exchange_type in('P', 'C', 'O'))
                                            or
                                            (L_consolidation_ind = 'N' and exchange_type in('P', 'O')))
                                   )
                             where date_rank = 1)
         select oi.item,
                avg(il.unit_retail * curr_conv.exchange_rate) unit_retail
           from rms_oi_inv_ana_inv_gtt oi,
                store st,
                item_loc il,
                curr_conv
          where oi.loc  = st.store
            and oi.item = il.item
            and oi.loc  = il.loc
            and curr_conv.from_currency = st.currency_code
            and curr_conv.to_currency   = L_primary_currency
          group by oi.item) use_this
      on (target.item = use_this.item)
      when matched then update
       set target.unit_retail = use_this.unit_retail;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge unit_retail rms_oi_inv_ana_inv_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);



   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END MERGE_UNIT_RETAIL;
--------------------------------------------------------------------------------
FUNCTION ROLLUP_INV_VAR_FORECAST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id              IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS
   L_program     VARCHAR2(61) := 'RMS_OI_INV_ANALYST.ROLLUP_INV_VAR_FORECAST';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   insert into rms_oi_inv_ana_inv_gtt (session_id,
                                       item,
                                       item_parent,
                                       item_desc,
                                       alc_item_type,
                                       agg_diff_1,
                                       agg_diff_2,
                                       agg_diff_3,
                                       agg_diff_4,
                                       loc,
                                       grouping_loc_id,
                                       grouping_loc_name,
                                       stock_on_hand,
                                       in_transit_qty,
                                       pack_comp_intran,
                                       pack_comp_soh,
                                       tsf_reserved_qty,
                                       pack_comp_resv,
                                       tsf_expected_qty,
                                       pack_comp_exp,
                                       rtv_qty,
                                       non_sellable_qty,
                                       pack_comp_non_sellable,
                                       customer_resv,
                                       pack_comp_cust_resv,
                                       customer_backorder,
                                       pack_comp_cust_back,
                                       forecast_sales,
                                       on_order_qty,
                                       co_inbound,
                                       pl_tsf_inbound,
                                       alloc_inbound,
                                       alloc_outbound,
                                       sales_hist_qty,
                                       avail_qty)
   select I_session_id,
          oi.item_parent item,
          null item_parent,
          imp.item_desc,
          'FA' alc_item_type,
          oi.agg_diff_1,
          oi.agg_diff_2,
          oi.agg_diff_3,
          oi.agg_diff_4,
          oi.loc,
          oi.grouping_loc_id,
          oi.grouping_loc_name,
          sum(oi.stock_on_hand) stock_on_hand,
          sum(oi.in_transit_qty) in_transit_qty,
          sum(oi.pack_comp_intran) pack_comp_intran,
          sum(oi.pack_comp_soh) pack_comp_soh,
          sum(oi.tsf_reserved_qty) tsf_reserved_qty,
          sum(oi.pack_comp_resv) pack_comp_resv,
          sum(oi.tsf_expected_qty) tsf_expected_qty,
          sum(oi.pack_comp_exp) pack_comp_exp,
          sum(oi.rtv_qty) rtv_qty,
          sum(oi.non_sellable_qty) non_sellable_qty,
          sum(oi.pack_comp_non_sellable) pack_comp_non_sellable,
          sum(oi.customer_resv) customer_resv,
          sum(oi.pack_comp_cust_resv) pack_comp_cust_resv,
          sum(oi.customer_backorder) customer_backorder,
          sum(oi.pack_comp_cust_back) pack_comp_cust_back,
          sum(oi.forecast_sales) forecast_sales,
          sum(oi.on_order_qty) on_order_qty,
          sum(oi.co_inbound) co_inbound,
          sum(oi.pl_tsf_inbound) pl_tsf_inbound,
          sum(oi.alloc_inbound) alloc_inbound,
          sum(oi.alloc_outbound) alloc_outbound,
          sum(oi.sales_hist_qty) sales_hist_qty,
          sum(oi.avail_qty) avail_qty
     from rms_oi_inv_ana_inv_gtt oi,
          item_master imp
    where oi.alc_item_type      = 'FASHIONSKU'
      and imp.item              = oi.item_parent
    group by oi.item_parent,
             imp.item_desc,
             oi.agg_diff_1,
             oi.agg_diff_2,
             oi.agg_diff_3,
             oi.agg_diff_4,
             oi.loc,
             oi.grouping_loc_id,
             oi.grouping_loc_name;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_inv_gtt FA - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_inv_ana_inv_gtt (session_id,
                                       item,
                                       item_parent,
                                       item_desc,
                                       alc_item_type,
                                       agg_diff_1,
                                       agg_diff_2,
                                       agg_diff_3,
                                       agg_diff_4,
                                       loc,
                                       grouping_loc_id,
                                       grouping_loc_name,
                                       stock_on_hand,
                                       in_transit_qty,
                                       pack_comp_intran,
                                       pack_comp_soh,
                                       tsf_reserved_qty,
                                       pack_comp_resv,
                                       tsf_expected_qty,
                                       pack_comp_exp,
                                       rtv_qty,
                                       non_sellable_qty,
                                       pack_comp_non_sellable,
                                       customer_resv,
                                       pack_comp_cust_resv,
                                       customer_backorder,
                                       pack_comp_cust_back,
                                       forecast_sales,
                                       on_order_qty,
                                       co_inbound,
                                       pl_tsf_inbound,
                                       alloc_inbound,
                                       alloc_outbound,
                                       sales_hist_qty,
                                       avail_qty)
   select I_session_id,
          oi.item_parent item,
          null item_parent,
          imp.item_desc,
          'STYLE' alc_item_type,
          null agg_diff_1,
          null agg_diff_2,
          null agg_diff_3,
          null agg_diff_4,
          oi.loc,
          oi.grouping_loc_id,
          oi.grouping_loc_name,
          sum(oi.stock_on_hand) stock_on_hand,
          sum(oi.in_transit_qty) in_transit_qty,
          sum(oi.pack_comp_intran) pack_comp_intran,
          sum(oi.pack_comp_soh) pack_comp_soh,
          sum(oi.tsf_reserved_qty) tsf_reserved_qty,
          sum(oi.pack_comp_resv) pack_comp_resv,
          sum(oi.tsf_expected_qty) tsf_expected_qty,
          sum(oi.pack_comp_exp) pack_comp_exp,
          sum(oi.rtv_qty) rtv_qty,
          sum(oi.non_sellable_qty) non_sellable_qty,
          sum(oi.pack_comp_non_sellable) pack_comp_non_sellable,
          sum(oi.customer_resv) customer_resv,
          sum(oi.pack_comp_cust_resv) pack_comp_cust_resv,
          sum(oi.customer_backorder) customer_backorder,
          sum(oi.pack_comp_cust_back) pack_comp_cust_back,
          sum(oi.forecast_sales) forecast_sales,
          sum(oi.on_order_qty) on_order_qty,
          sum(oi.co_inbound) co_inbound,
          sum(oi.pl_tsf_inbound) pl_tsf_inbound,
          sum(oi.alloc_inbound) alloc_inbound,
          sum(oi.alloc_outbound) alloc_outbound,
          sum(oi.sales_hist_qty) sales_hist_qty,
          sum(oi.avail_qty) avail_qty
     from rms_oi_inv_ana_inv_gtt oi,
          item_master imp
    where imp.item              = oi.item_parent
    group by oi.item_parent,
             imp.item_desc,
             oi.loc,
             oi.grouping_loc_id,
             oi.grouping_loc_name;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_inv_gtt STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END ROLLUP_INV_VAR_FORECAST;
--------------------------------------------------------------------------------
FUNCTION REFRESH_INV_VAR_FORECAST(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                  I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                  I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                                  I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                                  I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                                  I_items                  IN     OBJ_VARCHAR_ID_TABLE,
                                  I_item_type              IN     item_master.alc_item_type%TYPE)
RETURN NUMBER IS
   L_program             VARCHAR2(61)         := 'RMS_OI_INV_ANALYST.REFRESH_INV_VAR_FORECAST';
   L_start_time          TIMESTAMP            := SYSTIMESTAMP;

   L_vdate               DATE         := get_vdate;
   L_user                VARCHAR2(30) := get_user;
   L_forward_date        DATE;
   L_history_date        DATE;
   L_location_group_type VARCHAR2(1);
   L_store_count         NUMBER(8);
   L_primary_currency    system_options.currency_code%TYPE;

BEGIN

LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   select currency_code
     into L_primary_currency
     from system_options;

   select d.eow_date + 21,
          d.eow_date - 21
     into L_forward_date,
          L_history_date
     from day_level_calendar d
    where d.day = L_vdate;

   delete from rms_oi_inv_ana_variance_defer v
    where v.defer_to_date <= L_vdate;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete  rms_oi_inv_ana_variance_defer expired deferred items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if OI_UTILITY.SETUP_STORES(O_error_message,
                              L_location_group_type,
                              I_session_id,
                              I_areas,
                              I_stores,
                              I_store_grade_group_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if OI_UTILITY.SETUP_REFRESH_ITEMS(O_error_message,
                                     I_session_id,
                                     I_supplier_sites,
                                     null,         --I_origin_countries
                                     null,         --I_brands
                                     'Y',          --I_forecast_ind
                                     I_items,
                                     I_item_type) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete from gtt_10_num_10_str_10_date
    where rowid in (select gtt.rowid
                      from gtt_10_num_10_str_10_date gtt,
                           rms_oi_inv_ana_variance_defer d
                     where d.alc_item_type            = 'STYLE'
                       and gtt.varchar2_2             = d.item
                       and d.defer_user               = L_user
                       and d.defer_to_date            > L_vdate
                    union all
                    select gtt.rowid
                      from gtt_10_num_10_str_10_date gtt,
                           rms_oi_inv_ana_variance_defer d
                     where d.alc_item_type            = 'FA'
                       and gtt.varchar2_2             = d.item
                       and nvl(gtt.varchar2_5,'nu|l') = nvl(d.agg_diff_1,'nu|l')
                       and nvl(gtt.varchar2_6,'nu|l') = nvl(d.agg_diff_2,'nu|l')
                       and nvl(gtt.varchar2_7,'nu|l') = nvl(d.agg_diff_3,'nu|l')
                       and nvl(gtt.varchar2_8,'nu|l') = nvl(d.agg_diff_4,'nu|l')
                       and d.defer_user               = L_user
                       and d.defer_to_date            > L_vdate
                    union all
                    select gtt.rowid
                      from gtt_10_num_10_str_10_date gtt,
                           rms_oi_inv_ana_variance_defer d
                     where d.alc_item_type            not in('STYLE','FA')
                       and gtt.varchar2_1             = d.item
                       and d.defer_user               = L_user
                       and d.defer_to_date            > L_vdate);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete  gtt_10_num_10_str_10_date deferred items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POP_ILSOH_FILTER(O_error_message,
                       I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_FORECAST_HIST(O_error_message,
                          I_session_id,
                          L_history_date,
                          L_vdate) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_FORECAST(O_error_message,
                     I_session_id,
                     L_vdate,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ON_ORDER(O_error_message,
                     I_session_id,
                     L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_CO_INBOUND(O_error_message,
                       I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_WH_CROSSLINK(O_error_message,
                         I_session_id,
                         L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_ALLOC_INBOUND(O_error_message,
                          I_session_id,
                          L_forward_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_SALES_HIST(O_error_message,
                       I_session_id,
                       L_history_date) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if MERGE_UNIT_RETAIL(O_error_message,
                        I_session_id,
                        L_store_count) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   if ROLLUP_INV_VAR_FORECAST(O_error_message,
                              I_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete from rms_oi_inv_ana_variance inv
    where session_id = I_session_id
      and exists (select 1
                    from rms_oi_inv_ana_inv_gtt inv_gtt
                   where inv_gtt.session_id              =  inv.session_id
                     and inv_gtt.item                    =  inv.item
                     and inv_gtt.alc_item_type           = inv.alc_item_type
                     and nvl(inv_gtt.agg_diff_1, 'nu|l') = nvl(inv.agg_diff_1, 'nu|l')
                     and nvl(inv_gtt.agg_diff_2, 'nu|l') = nvl(inv.agg_diff_2, 'nu|l')
                     and nvl(inv_gtt.agg_diff_3, 'nu|l') = nvl(inv.agg_diff_3, 'nu|l')
                     and nvl(inv_gtt.agg_diff_4, 'nu|l') = nvl(inv.agg_diff_4, 'nu|l'));

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ana_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rms_oi_inv_ana_variance (session_id,
                                        item,
                                        item_desc,
                                        alc_item_type,
                                        agg_diff_1,
                                        agg_diff_2,
                                        agg_diff_3,
                                        agg_diff_4,
                                        sales_units,
                                        sales_var_to_forecast_pct,
                                        inv_units,
                                        inv_var_value,
                                        inv_var_to_forecast_pct,
                                        currency_code)
      select I_session_id,
          oi.item,
          oi.item_desc,
          oi.alc_item_type,
          oi.agg_diff_1,
          oi.agg_diff_2,
          oi.agg_diff_3,
          oi.agg_diff_4,
          sum(oi.sales_hist_qty) sales_units,
          --
          ((sum(oi.sales_hist_qty) - sum(oi.forecast_sales_hist)) / sum(oi.forecast_sales_hist)) * 100 sales_var_to_forecast_pct,
          --
          sum(oi.stock_on_hand + oi.pack_comp_soh) +
          sum(oi.in_transit_qty + oi.pack_comp_intran + oi.on_order_qty + oi.pl_tsf_inbound + oi.alloc_inbound) -
          sum(oi.tsf_reserved_qty + oi.pack_comp_resv + oi.rtv_qty + oi.customer_resv + oi.pack_comp_cust_resv + oi.co_inbound) -
          sum(oi.customer_backorder + oi.pack_comp_cust_back) -
          sum(oi.non_sellable_qty + oi.pack_comp_non_sellable) -
          sum(oi.forecast_sales) inv_units,
          --
          (sum(oi.stock_on_hand + oi.pack_comp_soh) +
           sum(oi.in_transit_qty + oi.pack_comp_intran + oi.on_order_qty + oi.pl_tsf_inbound + oi.alloc_inbound) -
           sum(oi.tsf_reserved_qty + oi.pack_comp_resv + oi.rtv_qty + oi.customer_resv + oi.pack_comp_cust_resv + oi.co_inbound) -
           sum(oi.customer_backorder + oi.pack_comp_cust_back) -
           sum(oi.non_sellable_qty + oi.pack_comp_non_sellable) -
           sum(oi.forecast_sales)) * avg(oi.unit_retail) inv_var_value,
          --
          ((sum(oi.stock_on_hand + oi.pack_comp_soh) +
            sum(oi.in_transit_qty + oi.pack_comp_intran + oi.on_order_qty + oi.pl_tsf_inbound + oi.alloc_inbound) -
            sum(oi.tsf_reserved_qty + oi.pack_comp_resv + oi.rtv_qty + oi.customer_resv + oi.pack_comp_cust_resv + oi.co_inbound) -
            sum(oi.customer_backorder + oi.pack_comp_cust_back) -
            sum(oi.non_sellable_qty + oi.pack_comp_non_sellable) -
            sum(oi.forecast_sales)) / sum(forecast_sales)) * 100 inv_var_to_forecast_pct,
          L_primary_currency
     from rms_oi_inv_ana_inv_gtt oi
    group by oi.item,
             oi.item_desc,
             oi.alc_item_type,
             oi.agg_diff_1,
             oi.agg_diff_2,
             oi.agg_diff_3,
             oi.agg_diff_4;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' insert rms_oi_inv_ana_variance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_INV_VAR_FORECAST;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ORD_PAST_NAD_ERR(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_orders_past_nad             IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                  I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                                  I_order_ids                   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_order_ids_to_delete         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program               VARCHAR2(61)         := 'RMS_OI_INV_ANALYST.REFRESH_ORD_PAST_NAD_ERR';
   L_start_time            TIMESTAMP            := SYSTIMESTAMP;
   L_vdate                 DATE                 := get_vdate;
   L_order_date_key_tbl    OBJ_NUM_NUM_STR_TBL;

BEGIN
   O_orders_past_nad    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());

   delete from gtt_6_num_6_str_6_date;

   insert into gtt_6_num_6_str_6_date(varchar2_1, date_1)
      select '1',  L_vdate-1 from dual union all
      select '2',  L_vdate-2 from dual union all
      select '3',  L_vdate-3 from dual union all
      select '4',  L_vdate-4 from dual union all
      select '5',  L_vdate-5 from dual union all
      select '6',  L_vdate-6 from dual union all
      select '7+', L_vdate-7 from dual;

   select OBJ_NUM_NUM_STR_REC(innr.order_no,
                              null,
                              innr.date_key)
      bulk collect into L_order_date_key_tbl
      from (select oh.order_no,
                   date_gtt.varchar2_1 date_key
              from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                   ordhead oh,
                   gtt_6_num_6_str_6_date date_gtt
              where value(order_ids)      = oh.order_no
                and oh.status             in('A','S','W')
                and oh.orig_approval_date is not null
                and ((date_gtt.varchar2_1 != '7+' and oh.not_after_date = date_gtt.date_1)
                      or
                     (date_gtt.varchar2_1 = '7+' and oh.not_after_date <= date_gtt.date_1))
             ) innr;

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_orders_past_nad temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_orders_past_nad,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;

   merge into rms_oi_inv_ord_errors target
   using (select I_session_id session_id,
                 oh.order_no,
                 oh.status,
                 oh.not_before_date,
                 oh.not_after_date,
                 oh.otb_eow_date,
                 oh.comment_desc
            from ordhead oh,
                 table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) order_ids
           where oh.order_no     = value(order_ids)) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no)
   when matched then update
    set target.status           = use_this.status,
        target.not_before_date  = use_this.not_before_date,
        target.not_after_date   = use_this.not_after_date,
        target.otb_eow_date     = use_this.otb_eow_date,
        target.comment_desc     = use_this.comment_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_inv_ord_errors
    where session_id = I_session_id
      and order_no in (select value(delete_order_ids) ord_id
                         from table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) delete_order_ids
                       minus
                       select order_date_keys.number_1 ord_id
                         from table(cast(L_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) order_date_keys);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

  OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_ORD_PAST_NAD_ERR;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ORD_TO_CLOSE_ERR(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_orders_to_close             IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                  I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                                  I_order_ids                   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_order_ids_to_delete         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_program                VARCHAR2(61) := 'RMS_OI_INV_ANALYST.REFRESH_ORD_TO_CLOSE_ERR';
   L_start_time             TIMESTAMP                     := SYSTIMESTAMP;
   L_vdate                  DATE               := get_vdate;
   L_order_date_key_tbl     OBJ_NUM_NUM_STR_TBL;
BEGIN

   O_orders_to_close    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());
   --setup future 7 day date ranges
   delete from gtt_6_num_6_str_6_date;

   insert into gtt_6_num_6_str_6_date(varchar2_1, date_1)
      select '1', L_vdate+1 from dual union all
      select '2', L_vdate+2 from dual union all
      select '3', L_vdate+3 from dual union all
      select '4', L_vdate+4 from dual union all
      select '5', L_vdate+5 from dual union all
      select '6', L_vdate+6 from dual union all
      select '7', L_vdate+7 from dual;

   select OBJ_NUM_NUM_STR_REC(innr.order_no,
                              null,
                              innr.date_key)
     bulk collect into L_order_date_key_tbl
     from (select i.date_key,
                  i.order_no
             from (select date_gtt.varchar2_1 date_key,
                          oh.order_no,
                          sum(ol.qty_ordered) - sum(ol.qty_received) outstanding_qty
                     from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                          ordhead oh,
                          gtt_6_num_6_str_6_date date_gtt,
                          ordloc ol
                    where value(order_ids)      = oh.order_no
                      and oh.status           in('A','S','W')
                      and oh.orig_approval_date is not null
                      and oh.not_after_date   = date_gtt.date_1
                      and oh.order_no         = ol.order_no
                    group by date_gtt.varchar2_1,
                             oh.order_no) i
            where i.outstanding_qty > 0
          ) innr;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' O_orders_to_close temp - COUNT '||L_order_date_key_tbl.count);

      if POPULATE_ORDER_ERROR_REC(O_error_message,
                                  O_orders_to_close,
                                  I_session_id,
                                  L_order_date_key_tbl) = OI_UTILITY.FAILURE then
         return OI_UTILITY.FAILURE;
      end if;

   merge into rms_oi_inv_ord_errors target
   using (select I_session_id session_id,
                 oh.order_no,
                 oh.status,
                 oh.not_before_date,
                 oh.not_after_date,
                 oh.otb_eow_date,
                 oh.comment_desc
            from ordhead oh,
                 table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) order_ids
           where oh.order_no     = value(order_ids)) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no)
   when matched then update
    set target.status           = use_this.status,
        target.not_before_date  = use_this.not_before_date,
        target.not_after_date   = use_this.not_after_date,
        target.otb_eow_date     = use_this.otb_eow_date,
        target.comment_desc     = use_this.comment_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_inv_ord_errors
    where session_id = I_session_id
      and order_no   in ( select value(delete_order_ids) ord_id
                            from table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) delete_order_ids
                          minus
                          select order_date_keys.number_1 ord_id
                            from table(cast(L_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) order_date_keys);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_ORD_TO_CLOSE_ERR;
--------------------------------------------------------------------------------
FUNCTION REFRESH_NEVER_APPRV_ORD_ERR(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_not_approved_orders         IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                     I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                                     I_order_ids                   IN     OBJ_NUMERIC_ID_TABLE,
                                     I_order_ids_to_delete         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61)         := 'RMS_OI_INV_ANALYST.REFRESH_NEVER_APPRV_ORD_ERR';
   L_start_time          TIMESTAMP            := SYSTIMESTAMP;
   L_vdate               DATE                 := get_vdate;
   L_order_date_key_tbl  OBJ_NUM_NUM_STR_TBL;

BEGIN
   O_not_approved_orders    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());

   delete from gtt_6_num_6_str_6_date;
      insert into gtt_6_num_6_str_6_date(varchar2_1, date_1)
      select '1', L_vdate+1 from dual union all
      select '2', L_vdate+2 from dual union all
      select '3', L_vdate+3 from dual union all
      select '4', L_vdate+4 from dual union all
      select '5', L_vdate+5 from dual union all
      select '6', L_vdate+6 from dual union all
      select '7', L_vdate+7 from dual;

   select OBJ_NUM_NUM_STR_REC(innr.order_no,
                              null,
                              innr.date_key)
      bulk collect into L_order_date_key_tbl
      from (select date_gtt.varchar2_1 date_key,
                 i.order_no
            from gtt_6_num_6_str_6_date date_gtt,
                 (select oh.order_no,
                         min(oh.not_before_date - (nvl(isc.lead_time,0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time,0)))) latest_approve_date
                    from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                         ordhead oh,
                         ordsku os,
                         ordloc ol,
                         item_supp_country isc,
                         item_supp_country_loc iscl
                   where value(order_ids)      = oh.order_no
                     and oh.order_no           = os.order_no
                     and oh.supplier           = isc.supplier
                     and os.item               = isc.item
                     and os.origin_country_id  = isc.origin_country_id
                     and oh.status             in('W','S')
                     and oh.orig_approval_date is null
                     and os.order_no           = ol.order_no
                     and os.item               = ol.item
                     and isc.item              = iscl.item
                     and isc.supplier          = iscl.supplier
                     and isc.origin_country_id = iscl.origin_country_id
                     and iscl.loc              = ol.location
                   group by oh.order_no) i
           where date_gtt.date_1 = i.latest_approve_date
         ) innr;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' O_not_approved_orders temp - COUNT '||L_order_date_key_tbl.count);

   if POPULATE_ORDER_ERROR_REC(O_error_message,
                               O_not_approved_orders,
                               I_session_id,
                               L_order_date_key_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   merge into rms_oi_inv_ord_errors target
   using (select I_session_id session_id,
                 oh.order_no,
                 oh.status,
                 oh.not_before_date,
                 oh.not_after_date,
                 oh.otb_eow_date,
                 oh.comment_desc
            from ordhead oh,
                 table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) order_ids
           where oh.order_no     = value(order_ids)) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no)
   when matched then update
    set target.status           = use_this.status,
        target.not_before_date  = use_this.not_before_date,
        target.not_after_date   = use_this.not_after_date,
        target.otb_eow_date     = use_this.otb_eow_date,
        target.comment_desc     = use_this.comment_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_inv_ord_errors
    where session_id = I_session_id
      and order_no in ( select value(delete_order_ids) ord_id
                          from table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) delete_order_ids
                        minus
                        select order_date_keys.number_1 ord_id
                          from table(cast(L_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) order_date_keys);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_NEVER_APPRV_ORD_ERR;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ONCE_APPRV_ORD_ERR(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_once_approved_orders        IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                    I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                                    I_order_ids                   IN     OBJ_NUMERIC_ID_TABLE,
                                    I_order_ids_to_delete         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program              VARCHAR2(61)         := 'RMS_OI_INV_ANALYST.REFRESH_ONCE_APPRV_ORD_ERR';
   L_start_time           TIMESTAMP            := SYSTIMESTAMP;
   L_vdate                DATE                 := get_vdate;
   L_order_date_key_tbl   OBJ_NUM_NUM_STR_TBL;

BEGIN
   O_once_approved_orders    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());

   delete from gtt_6_num_6_str_6_date;
      insert into gtt_6_num_6_str_6_date(varchar2_1, date_1)
      select '1', L_vdate+1 from dual union all
      select '2', L_vdate+2 from dual union all
      select '3', L_vdate+3 from dual union all
      select '4', L_vdate+4 from dual union all
      select '5', L_vdate+5 from dual union all
      select '6', L_vdate+6 from dual union all
      select '7', L_vdate+7 from dual;

   select OBJ_NUM_NUM_STR_REC(innr.order_no,
                              null,
                              innr.date_key)
      bulk collect into L_order_date_key_tbl
      from (select date_gtt.varchar2_1 date_key,
                   i.order_no
              from gtt_6_num_6_str_6_date date_gtt,
                   (select oh.order_no,
                           min(oh.not_before_date - (nvl(isc.lead_time,0) + nvl(iscl.pickup_lead_time, nvl(isc.pickup_lead_time,0)))) latest_approve_date
                      from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                           ordhead oh,
                           ordsku os,
                           ordloc ol,
                           item_supp_country isc,
                           item_supp_country_loc iscl
                     where value(order_ids)      = oh.order_no
                       and oh.order_no           = os.order_no
                       and oh.supplier           = isc.supplier
                       and os.item               = isc.item
                       and os.origin_country_id  = isc.origin_country_id
                       and oh.status             in('W','S')
                       and oh.orig_approval_date is not null
                       and os.order_no           = ol.order_no
                       and os.item               = ol.item
                       and isc.item              = iscl.item
                       and isc.supplier          = iscl.supplier
                       and isc.origin_country_id = iscl.origin_country_id
                       and iscl.loc              = ol.location
                     group by oh.order_no) i
             where date_gtt.date_1 = i.latest_approve_date
             ) innr;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' O_once_approved_orders temp - COUNT '||L_order_date_key_tbl.count);

   if POPULATE_ORDER_ERROR_REC(O_error_message,
                               O_once_approved_orders,
                               I_session_id,
                               L_order_date_key_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   merge into rms_oi_inv_ord_errors target
   using (select I_session_id session_id,
                 oh.order_no,
                 oh.status,
                 oh.not_before_date,
                 oh.not_after_date,
                 oh.otb_eow_date,
                 oh.comment_desc
            from ordhead oh,
                 table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) order_ids
           where oh.order_no     = value(order_ids)) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no)
   when matched then update
    set target.status           = use_this.status,
        target.not_before_date  = use_this.not_before_date,
        target.not_after_date   = use_this.not_after_date,
        target.otb_eow_date     = use_this.otb_eow_date,
        target.comment_desc     = use_this.comment_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_inv_ord_errors
    where session_id = I_session_id
      and order_no in ( select value(delete_order_ids) ord_id
                          from table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) delete_order_ids
                        minus
                        select order_date_keys.number_1 ord_id
                          from table(cast(L_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) order_date_keys);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

  OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
  return OI_UTILITY.SUCCESS;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_ONCE_APPRV_ORD_ERR;
--------------------------------------------------------------------------------
FUNCTION REFRESH_MISS_ORD_DATA_ERR(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_missing_order_data          IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                   I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                                   I_order_ids                   IN     OBJ_NUMERIC_ID_TABLE,
                                   I_order_ids_to_delete         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program              VARCHAR2(61)   := 'RMS_OI_INV_ANALYST.REFRESH_MISS_ORD_DATA_ERR';
   L_start_time           TIMESTAMP      := SYSTIMESTAMP;
   L_vdate                DATE           := get_vdate;

   L_ord_err_factory_ind  rms_oi_system_options.ia_ord_err_factory_ind%TYPE;
   L_order_date_key_tbl   OBJ_NUM_NUM_STR_TBL;

BEGIN
   O_missing_order_data    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());

   --setup future 4 week date ranges
   delete from gtt_6_num_6_str_6_date;
      insert into gtt_6_num_6_str_6_date(varchar2_1, date_1, date_2)
      select '1', L_vdate,       d.eow_date    from day_level_calendar d where d.day = L_vdate union all
      select '2', d.eow_date+1,  d.eow_date+7  from day_level_calendar d where d.day = L_vdate union all
      select '3', d.eow_date+8,  d.eow_date+14 from day_level_calendar d where d.day = L_vdate union all
      select '4', d.eow_date+15, d.eow_date+21 from day_level_calendar d where d.day = L_vdate;

      select OBJ_NUM_NUM_STR_REC(innr.order_no,
                                 null,
                                 innr.date_key)
        bulk collect into L_order_date_key_tbl
        from (select date_gtt.varchar2_1 date_key,
                     oh.order_no,
                     sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) remain_qty
                from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                     ordhead oh,
                     gtt_6_num_6_str_6_date date_gtt,
                     ordloc ol
               where value(order_ids)      = oh.order_no
                 and oh.status             in('W','S','A')
                 and oh.orig_approval_date is not null
                 and (    oh.import_order_ind    = 'Y'
                      and (oh.discharge_port is null or
                           oh.lading_port    is null))
                 and oh.not_before_date between date_gtt.date_1 and date_gtt.date_2
                 and oh.order_no           = ol.order_no
              union
              select date_gtt.varchar2_1 date_key,
                     oh.order_no,
                     sum(ol.qty_ordered) - sum(nvl(ol.qty_received,0)) remain_qty
                from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                     ordhead oh,
                     gtt_6_num_6_str_6_date date_gtt,
                     ordloc ol
               where value(order_ids)      = oh.order_no
                 and oh.status             in('W','S','A')
                 and oh.orig_approval_date is not null
                 and L_ord_err_factory_ind = 'Y'
                 and oh.factory            is null
                 and oh.not_before_date between date_gtt.date_1 and date_gtt.date_2
                 and oh.order_no           = ol.order_no
             ) innr
       where innr.remain_qty > 0;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' O_missing_order_data temp - COUNT '||L_order_date_key_tbl.count);

   if POPULATE_ORDER_ERROR_REC(O_error_message,
                               O_missing_order_data,
                               I_session_id,
                               L_order_date_key_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   merge into rms_oi_inv_ord_errors target
   using (select oi.session_id,
                 oh.order_no,
                 oi.error_message,
                 oh.status,
                 oh.not_before_date,
                 oh.not_after_date,
                 oh.otb_eow_date,
                 oh.comment_desc
            from ordhead oh,
                 table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) order_ids,
                 rms_oi_inv_ord_errors oi
           where oh.order_no     = value(order_ids)
             and oi.session_id   = I_session_id
             and oi.order_no     = oh.order_no) use_this
   on (    target.session_id     = use_this.session_id
       and target.order_no       = use_this.order_no
       and target.error_message  = use_this.error_message)
   when matched then update
    set target.status           = use_this.status,
        target.not_before_date  = use_this.not_before_date,
        target.not_after_date   = use_this.not_after_date,
        target.otb_eow_date     = use_this.otb_eow_date,
        target.comment_desc     = use_this.comment_desc;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' merge rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from rms_oi_inv_ord_errors
    where session_id = I_session_id
      and order_no in ( select value(delete_order_ids) ord_id
                          from table(cast(I_order_ids_to_delete as OBJ_NUMERIC_ID_TABLE)) delete_order_ids
                        minus
                        select order_date_keys.number_1 ord_id
                          from table(cast(L_order_date_key_tbl as OBJ_NUM_NUM_STR_TBL)) order_date_keys);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_MISS_ORD_DATA_ERR;
--------------------------------------------------------------------------------
FUNCTION REFRESH_MISS_ITM_DATA_ERR(O_error_message                  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_missing_item_data              IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                   I_session_id                     IN     oi_session_id_log.session_id%TYPE,
                                   I_order_ids                      IN     OBJ_NUMERIC_ID_TABLE,
                                   I_ord_itm_ids_to_delete          IN     RMS_OI_ORDER_ITEM_TBL)
RETURN NUMBER IS
   L_program                        VARCHAR2(61) := 'RMS_OI_INV_ANALYST.REFRESH_MISS_ITM_DATA_ERR';
   L_start_time                     TIMESTAMP    := SYSTIMESTAMP;
   L_vdate                          DATE := get_vdate;

   L_ord_err_ref_item_ind           rms_oi_system_options.ia_ord_err_ref_item_ind%TYPE;
   L_import_ind                     system_options.import_ind%TYPE;
   L_elc_ind                        system_options.elc_ind%TYPE;
   L_order_hts_ind                  system_options.order_hts_ind%TYPE;
   L_order_date_key_tbl             OBJ_NUM_STR_STR_TBL;
BEGIN
   select so.ia_ord_err_ref_item_ind,
          syso.import_ind,
          syso.elc_ind,
          syso.order_hts_ind
     into L_ord_err_ref_item_ind,
          L_import_ind,
          L_elc_ind,
          L_order_hts_ind
     from rms_oi_system_options so,
          system_options syso;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');


   delete from rms_oi_inv_ord_item_errors
         where session_id = I_session_id
          and (order_no, item) in (select delete_ord_item_ids.order_no ord_id,
                                          delete_ord_item_ids.item itm
                                     from table(cast(I_ord_itm_ids_to_delete as RMS_OI_ORDER_ITEM_TBL)) delete_ord_item_ids
                                   minus
                                   select order_date_keys.number_1 ord_id,
                                          order_date_keys.string_1 itm
                                     from table(cast(L_order_date_key_tbl as OBJ_NUM_STR_STR_TBL)) order_date_keys);

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_inv_ord_item_errors - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   O_missing_item_data    := RMS_OI_ORDER_ERROR_TILE_REC(RMS_OI_DATE_VALUE_TBL(),OBJ_NUMERIC_ID_TABLE());
   --setup future 4 week date ranges
   delete from gtt_6_num_6_str_6_date;
      insert into gtt_6_num_6_str_6_date(varchar2_1, date_1, date_2)
      select '1', L_vdate,       d.eow_date    from day_level_calendar d where d.day = L_vdate union all
      select '2', d.eow_date+1,  d.eow_date+7  from day_level_calendar d where d.day = L_vdate union all
      select '3', d.eow_date+8,  d.eow_date+14 from day_level_calendar d where d.day = L_vdate union all
      select '4', d.eow_date+15, d.eow_date+21 from day_level_calendar d where d.day = L_vdate;

   with ords as (select oh.order_no,
                        oh.import_order_ind,
                        oh.import_country_id,
                        date_gtt.varchar2_1 date_key
                   from table(cast(I_order_ids as OBJ_NUMERIC_ID_TABLE)) order_ids,
                        ordhead oh,
                        gtt_6_num_6_str_6_date date_gtt
                  where value(order_ids)      = oh.order_no
                    and oh.not_before_date    between date_gtt.date_1 and date_gtt.date_2
                    and oh.status             in ('W','S','A')
                    and oh.orig_approval_date is not null)
   --
   select OBJ_NUM_STR_STR_REC(innr.order_no,
                              innr.item,
                              innr.date_key)
     bulk collect into L_order_date_key_tbl
     from (select ords.order_no,
                  os.item,
                  ords.date_key
             from ords,
                  ordsku os
            where ords.order_no          = os.order_no
              and L_ord_err_ref_item_ind = 'Y'
              and os.ref_item            is null
              and exists (select 'x' 
                            from rms_oi_inv_ord_item_errors oie
                           where oie.session_id = I_session_id 
                             and oie.order_no   = ords.order_no
                             and oie.item       = os.item)
           union
           select i.order_no,
                  i.item,
                  i.date_key
             from (select ords.order_no,
                          im.item,
                          ords.date_key,
                          case when d.markup_calc_type = 'R'
                               then (il.unit_retail - ol.unit_cost) / nullif(il.unit_retail,0)
                               else (il.unit_retail - ol.unit_cost) / nullif(ol.unit_cost,0) end margin
                     from ords,
                          ordloc ol,
                          item_loc il,
                          item_master im,
                          deps d
                    where ords.order_no  = ol.order_no
                      and ol.item        = il.item
                      and ol.location    = il.loc
                      and il.item        = im.item
                      and im.dept        = d.dept
                      and exists (select 'x'
                                    from rms_oi_inv_ord_item_errors oie
                                   where oie.session_id = I_session_id
                                     and oie.order_no   = ords.order_no
                                     and oie.item       = ol.item)) i
            where i.margin <= 0
           union
           select ords.order_no,
                  os.item,
                  ords.date_key
             from ords,
                  ordsku os
            where L_import_ind            = 'Y'
              and L_elc_ind               = 'Y'
              and ords.import_order_ind   = 'Y'
              and ords.order_no           = os.order_no
              and ords.import_country_id != os.origin_country_id
              and exists (select 'x'
                            from rms_oi_inv_ord_item_errors oie
                           where oie.session_id = I_session_id
                             and oie.order_no   = ords.order_no
                             and oie.item       = os.item)
              and ((L_order_hts_ind  = 'Y'
                    and not exists (select 'X'
                                      from ordsku_hts h,
                                           item_master im
                                     where os.order_no     = h.order_no
                                       and os.item         = h.item
                                       and h.pack_item     is null
                                       and h.status        = 'A'
                                       and os.item         = im.item
                                       and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.pack_type != 'B'))
                                     union all
                                    select 'X'
                                      from ordsku_hts h,
                                           item_master im,
                                           v_packsku_qty v
                                     where os.order_no     = h.order_no
                                       and os.item         = h.pack_item
                                       and h.status        = 'A'
                                       and os.item         = im.item
                                       and im.pack_ind     = 'Y'
                                       and im.pack_type    = 'B'
                                       and h.pack_item     = v.pack_no
                                       and h.item          = v.item
                                    ))
                 or(L_order_hts_ind  = 'N'
                    and exists (select 'X'
                                      from ordsku_hts h,
                                           item_master im
                                     where os.order_no     = h.order_no
                                       and os.item         = h.item
                                       and h.pack_item     is null
                                       and h.status        <> 'A'
                                       and os.item         = im.item
                                       and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.pack_type != 'B'))
                                     union all
                                    select 'X'
                                      from ordsku_hts h,
                                           item_master im,
                                           v_packsku_qty v
                                     where os.order_no     = h.order_no
                                       and os.item         = h.pack_item
                                       and h.status        <>'A'
                                       and os.item         = im.item
                                       and im.pack_ind     = 'Y'
                                       and im.pack_type    = 'B'
                                       and h.pack_item     = v.pack_no
                                       and h.item          = v.item)))
          ) innr;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' temp population - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POPULATE_ORD_ITM_ERROR_REC(O_error_message,
                                 O_missing_item_data,
                                 I_session_id,
                                 L_order_date_key_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END REFRESH_MISS_ITM_DATA_ERR;
--------------------------------------------------------------------------------
END RMS_OI_INV_ANALYST;
/
