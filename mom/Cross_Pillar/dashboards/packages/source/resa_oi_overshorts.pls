CREATE OR REPLACE PACKAGE RESA_OI_OVERSHORT_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name : OVERSHORT_SUMMARY
--Purpose       : This function retrieves the over/short summary for the past
--                7 days for assigned stores.
--                To be used by the following reports:
--                   OVER/SHORT SUMMARY REPORT (DASHBOARD)
--                   OVER/SHORT DETAILS REPORT (CONTEXTUAL DASHBOARD)
--                   OVER/SHORT HISTORY REPORT (CONTEXTUAL STORE DAY SEARCH)
--------------------------------------------------------------------------------
FUNCTION OVERSHORT_SUMMARY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result        IN OUT RESA_OI_OVERSHORT_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : RC_OVERSHORT_DETAIL
--Purpose       : This function retrieves the over/short details for
--                the give store day.
--                To be used by the following reports:
--                   CASHIER OVER/SHORT REPORT (CONTEXTUAL STORE DAY SUMMARY)
--                   REGISTER OVER/SHORT REPORT (CONTEXTUAL STORE DAY SUMMARY)
--------------------------------------------------------------------------------
FUNCTION RC_OVERSHORT_DETAIL(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result           IN OUT RESA_OI_RC_OVERSHORT_TBL,
                             I_store_day_seq_no IN     SA_STORE_DAY.STORE_DAY_SEQ_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RESA_OI_OVERSHORT_SQL;
/