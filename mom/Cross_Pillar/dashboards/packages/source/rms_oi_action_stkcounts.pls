CREATE OR REPLACE PACKAGE RMS_OI_ACTION_STKCOUNT AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
FUNCTION DELETE_LOCATION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result            OUT RMS_OI_CYCLE_COUNT_STATUS_REC,
                         I_session_id     IN     oi_session_id_log.session_id%TYPE,
                         I_cycle_cnt_locs IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ACCEPT_RESULTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result            OUT RMS_OI_CYCLE_COUNT_STATUS_REC,
                        I_session_id     IN     oi_session_id_log.session_id%TYPE,
                        I_cycle_cnt_locs IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ACCEPT_VALUE_VARIRANCE_RESULTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_result             OUT RMS_OI_CYLCNT_DCS_STATUS_REC,
                                        I_session_id      IN     oi_session_id_log.session_id%TYPE,
                                        I_cylcnt_dcs_locs IN     RMS_OI_CYLCNT_LOC_DCS_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_STKCOUNT;
/
