CREATE OR REPLACE PACKAGE RMS_OI_MARGIN_IMPACT AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
-- Function Name: GET_MARGIN_IMPACT_INFO
-- Purpose      : This function is invoked from the Margin Impact Contextual BI Report 
--                to get the Unit Cost, Unit Retail and Margin infomration for an Item 
--                or Item/Location in a Cost Change depending on if the BI report is 
--                invoked from the Cost Change by Item or Cost Change by Item/Loc screen.
--                Item can either be a tran-level item, or a parent/grandparent item.
--                When the function is invoked with a parent/grandparent item, Unit Cost 
--                and Unit Retail averaged across all tran-level items are returned. 
--                This function will be used to populate a transient VO in ADF and the data 
--                will be plotted as a Line graph in the Margin Impact Contextual BI report.
--                Both unit cost and unit retail line graphs will use the cost change active
--                dates to plot the graphs.
------------------------------------------------------------------------------------------------
FUNCTION GET_MARGIN_IMPACT_INFO(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_result          OUT     RMS_OI_MARGIN_IMPACT_TBL,
                                I_item            IN      ITEM_MASTER.ITEM%TYPE,
                                I_supplier        IN      COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                                I_origin_country  IN      COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE,
                                I_location        IN      COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                                I_cost_change     IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_active_date     IN      COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                                I_currency_code   IN      CURRENCIES.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: GET_CONVERTED_CURR_VALUE
-- Purpose      : This function is used get the currency converted value of a Unit cost/retail. 
------------------------------------------------------------------------------------------------
FUNCTION GET_CONVERTED_CURR_VALUE(I_from_currency   IN   CURRENCIES.CURRENCY_CODE%TYPE,
                                  I_to_currency     IN   CURRENCIES.CURRENCY_CODE%TYPE,
                                  I_input_value     IN   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                  I_effective_date  IN   PERIOD.VDATE%TYPE DEFAULT NULL,
                                  I_functional_area IN   VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------
END RMS_OI_MARGIN_IMPACT;
/
