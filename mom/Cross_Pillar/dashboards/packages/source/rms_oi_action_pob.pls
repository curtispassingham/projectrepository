CREATE OR REPLACE PACKAGE BODY RMS_OI_ACTION_PO AS
--------------------------------------------------------------------------------
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
--------------------------------------------------------------------------------
------------------------------------------------------------------------------------
FUNCTION PERSIST_UPDATE_PO_DATES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_result          IN OUT RMS_OI_PO_STATUS_REC,
                                 I_not_before_date IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                                 I_not_after_date  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                                 I_otb_eow_date    IN     ORDHEAD.OTB_EOW_DATE%TYPE,
                                 I_orders          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_UPDATE_DATES_ERROR_TEXTS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_date_nbd_not_before_today    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nbd_before_nad               OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nbd_before_otb               OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nad_before_otb               OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nad_after_pd                 OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nad_after_today              OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_no_chng_approv_deal          OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_not_after_contract           OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_not_before_contract          OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_otb_not_bfr_today            OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_inv_order_no                 OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_co_po_no_edit                OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_f_po_no_edit                 OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_program                   IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                           I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION LOCK_ORDERS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                     I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_WITH_NO_ERRORS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                                  O_orders           OUT OBJ_NUMERIC_ID_TABLE,
                                  I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_APPROVAL_AMT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                                     I_orders        IN     OBJ_NUMERIC_ID_TABLE,
                                     I_approval_amt  IN     RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_APPROVAL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid_lc_ind          OUT VARCHAR2,
                                 I_ordhead_row        IN     ORDHEAD%ROWTYPE,
                                 I_system_options_rec IN     SYSTEM_OPTIONS%ROWTYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PERSIST_ORDER_APPROVAL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_ordhead_row        IN     ORDHEAD%ROWTYPE,
                                I_system_options_rec IN     SYSTEM_OPTIONS%ROWTYPE,
                                I_valid_lc_ind       IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_ORDER_ITEMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                             I_po_rec        IN     RMS_OI_CREATE_PO_REC,
                             I_loc_type      IN     ORDHEAD.LOC_TYPE%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION LOCK_ORDSKU(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_session_id      IN     oi_session_id_log.session_id%TYPE,
                     I_order_no        IN     ordsku.order_no%type,
                     I_item            IN     ordsku.item%type)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION APPROVE_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           OUT RMS_OI_PO_STATUS_REC,
                    I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                    I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.APPROVE_PO';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_po_fail_tbl         RMS_OI_PO_FAIL_TBL   := RMS_OI_PO_FAIL_TBL();
   L_po_fail_rec         RMS_OI_PO_FAIL_REC;
   L_po_success_tbl      OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_orders              OBJ_NUMERIC_ID_TABLE;
   L_approval_amt        RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE;
   L_system_options_rec  SYSTEM_OPTIONS%ROWTYPE;
   L_valid_lc_ind        VARCHAR2(1);
        
   cursor C_PO_TO_APPROVE is
     select oh.*
       from ordhead oh,
            table(cast(L_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where oh.order_no    = value(input_orders);
   
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   O_result := new RMS_OI_PO_STATUS_REC(0,null,0,null);
   --
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   if ORDER_APPROVE_SQL.GET_APPROVAL_AMT(O_error_message,
                                         L_approval_amt) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if (L_approval_amt = 0) then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_AUTHOR',NULL,NULL,NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if VALIDATE_ORDER_NO(O_error_message,
                        L_po_fail_tbl,
                        I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if VALIDATE_ORDER_APPROVAL_AMT(O_error_message,
                                  L_po_fail_tbl,
                                  I_orders,
                                  L_approval_amt) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if GET_ORDER_WITH_NO_ERRORS(O_error_message,
                               L_po_fail_tbl,
                               L_orders,
                               I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if LOCK_ORDERS(O_error_message,
                  L_po_fail_tbl,
                  L_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if L_po_fail_tbl is not NULL and L_po_fail_tbl.count > 0 then
      O_result.fail_orders_count := L_po_fail_tbl.count;
      if L_po_fail_tbl.count = I_orders.count then
         O_result.fail_orders_tbl   := L_po_fail_tbl;
         return OI_UTILITY.SUCCESS;
      end if;       
   end if;
   ---
   if GET_ORDER_WITH_NO_ERRORS(O_error_message,
                               L_po_fail_tbl,
                               L_orders,
                               I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   for to_approve in C_PO_TO_APPROVE loop
      SAVEPOINT rms_oi_approve_po_savepoint;
      O_error_message := NULL;
      --
      if to_approve.status = 'A' then
         L_po_success_tbl.extend;
         L_po_success_tbl(L_po_success_tbl.count) := to_approve.order_no;
         O_result.success_orders_count := O_result.success_orders_count +1;
      elsif to_approve.status = 'C' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_APPR_CLOSED_PO',NULL,NULL,NULL);
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_approve.order_no,
                                                 O_error_message);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      else
         if VALIDATE_ORDER_APPROVAL(O_error_message,
                                    L_valid_lc_ind,
                                    to_approve,
                                    L_system_options_rec) = OI_UTILITY.FAILURE then
            L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_approve.order_no,
                                                    O_error_message);
            L_po_fail_tbl.extend;
            L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
            O_result.fail_orders_count := O_result.fail_orders_count +1;
            --
            ROLLBACK TO rms_oi_approve_po_savepoint;
         else
            if PERSIST_ORDER_APPROVAL(O_error_message,
                                      to_approve,
                                      L_system_options_rec,
                                      L_valid_lc_ind) = OI_UTILITY.FAILURE then
               L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_approve.order_no,
                                                       O_error_message);
               L_po_fail_tbl.extend;
               L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
               O_result.fail_orders_count := O_result.fail_orders_count +1;
               --
               ROLLBACK TO rms_oi_approve_po_savepoint;
            else
               L_po_success_tbl.extend;
               L_po_success_tbl(L_po_success_tbl.count) := to_approve.order_no;
               O_result.success_orders_count := O_result.success_orders_count +1;
            end if;--PERSIST_ORDER_APPROVAL
         end if;--VALIDATE_ORDER_APPROVAL
      end if;--to_approve.status = 'A'
      ---
   end loop;
   ---
   O_result.success_orders_tbl := L_po_success_tbl;
   O_result.fail_orders_tbl    := L_po_fail_tbl;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
END APPROVE_PO;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_APPROVAL_AMT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                                     I_orders        IN     OBJ_NUMERIC_ID_TABLE,
                                     I_approval_amt  IN     RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.VALIDATE_ORDER_APPROVAL_AMT';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_not_allow_appr_ord  RTK_ERRORS.RTK_TEXT%TYPE;
   L_po_fail_tbl         RMS_OI_PO_FAIL_TBL   := RMS_OI_PO_FAIL_TBL();
   
   cursor C_GET_APP_AMT_ERRORS is
     select RMS_OI_PO_FAIL_REC(order_no,
                               error_msg)
       from (--eligible orders total_cost and total_retail rolled up at order level
             with
             order_view as (select h.order_no,
                                   sum(ol.unit_cost * ol.qty_ordered) over (partition by h.order_no) total_cost,
                                   sum(ol.unit_retail * ol.qty_ordered) over (partition by h.order_no) total_retail,
                                   h.currency_code,
                                   h.exchange_rate,
                                   rank() over (partition by h.order_no
                                                    order by ol.item,ol.location) order_rank
                              from ordhead h,
                                   ordloc ol,
                                   table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
                             where h.order_no = value(input_orders)
                               and h.order_no = ol.order_no)
             select v.order_no,
                    CASE WHEN po.ord_appr_amt_code = 'C' AND
                              v.total_cost_prim > I_approval_amt THEN
                              L_not_allow_appr_ord
                         WHEN po.ord_appr_amt_code = 'R' AND
                              v.total_retail_prim > I_approval_amt THEN
                              L_not_allow_appr_ord
                         ELSE NULL
                    END error_msg
               from procurement_unit_options po,
                    (--order currency and primary currency are the same, no need to convert
                     select o.order_no,
                            NVL(o.total_cost, 0) total_cost_prim,
                            NVL(o.total_retail, 0) total_retail_prim
                       from order_view o,
                            system_config_options so
                      where o.currency_code = so.currency_code
                        and o.order_rank =1
                      union all
                     --order currency and primary currency are different, but exchange_rate is defined on order, convert based on order exchange rate
                     select o.order_no,
                            -- convert total_cost from order currency to primary currency for comparison with order approval amount
                            NVL(o.total_cost, 0)/o.exchange_rate total_cost_prim,
                            -- convert total_retail from order currency to primary currency for comparison with order approval amount
                            NVL(o.total_retail, 0)/o.exchange_rate total_retail_prim
                       from order_view o,
                            system_config_options so
                      where o.currency_code != so.currency_code
                        and o.exchange_rate is NOT NULL
                        and o.order_rank =1
                      union all
                     --order currency and primary currency are different and exchange_rate is NOT defined on order, convert based on MV
                     select distinct o.order_no,
                            -- convert total_cost from order currency to primary currency for comparison with order approval amount
                            NVL(o.total_cost, 0) * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc, mv.effective_date desc) total_cost_prim,
                            -- convert total_retail from order currency to primary currency for comparison with order approval amount
                            NVL(o.total_retail, 0) * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency order by decode(mv.exchange_type, 'P', 1, 'C', 2, 'O', 3) asc, mv.effective_date desc) total_retail_prim
                       from order_view o,
                            mv_currency_conversion_rates mv,
                            system_config_options so,
                            period p
                      where o.currency_code != so.currency_code
                        and o.exchange_rate is NULL
                        and o.order_rank =1
                        and mv.from_currency = o.currency_code
                        and mv.to_currency = so.currency_code
                        and (   (    so.consolidation_ind = 'Y' 
                                 and mv.exchange_type in ('P', 'C', 'O'))
                             or (    so.consolidation_ind = 'N' 
                                 and mv.exchange_type in ('P', 'O')))
                        and mv.effective_date <= p.vdate) v)
      where error_msg is not null;
   
   
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   L_not_allow_appr_ord := sql_lib.create_msg('NOT_ALLOW_APPR_ORD', I_approval_amt);
   OI_UTILITY.HANDLE_ERRORS(L_not_allow_appr_ord, L_program);
   ---
   open C_GET_APP_AMT_ERRORS;
   fetch C_GET_APP_AMT_ERRORS bulk collect into L_po_fail_tbl;
   close C_GET_APP_AMT_ERRORS;
   --
   if L_po_fail_tbl is not null and L_po_fail_tbl.count > 0 then
      O_po_fail_tbl := O_po_fail_tbl MULTISET UNION ALL L_po_fail_tbl;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_GET_APP_AMT_ERRORS%ISOPEN then close C_GET_APP_AMT_ERRORS; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_GET_APP_AMT_ERRORS%ISOPEN then close C_GET_APP_AMT_ERRORS; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END VALIDATE_ORDER_APPROVAL_AMT;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_APPROVAL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid_lc_ind          OUT VARCHAR2,
                                 I_ordhead_row        IN     ORDHEAD%ROWTYPE,
                                 I_system_options_rec IN     SYSTEM_OPTIONS%ROWTYPE)
RETURN NUMBER IS

   L_program                VARCHAR2(61) := 'RMS_OI_ACTION_PO.VALIDATE_ORDER_APPROVAL';
   
   L_contract_header        CONTRACT_HEADER%ROWTYPE;
   L_supp_status            SUPS.SUP_STATUS%TYPE;
   L_order_no               ORDHEAD.ORDER_NO%TYPE := I_ordhead_row.order_no;
   L_date                   ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_new_status_to_validate ORDHEAD.STATUS%TYPE;
   L_error_collection       OBJ_ERROR_TBL;
   L_ord_apprerr_exist      BOOLEAN;
   L_same_ou                BOOLEAN;
   L_seq_no                 NUMBER(10)   := 1;
   L_function_key           VARCHAR2(30) := 'ORDER_APPROVE_UI';
   L_order_exists           VARCHAR2(1)  := 'N';
   L_cust_ind               VARCHAR2(1)  := 'N';
   L_cust_ord_ind           VARCHAR2(1)  := 'N';
   L_approved_ord           VARCHAR2(1);
   L_invalid_date           VARCHAR2(1);
   L_store_closed_ind       VARCHAR2(1);
   L_store_opened_ind       VARCHAR2(1);
   L_clearance_ind          VARCHAR2(1);
   L_wksht_rec_ind          VARCHAR2(1);
   L_hts_ind                VARCHAR2(1);
   L_valid_lc_ind           VARCHAR2(1);
   L_scale_ind              VARCHAR2(1);
   L_recalc_ind             VARCHAR2(1);
   L_round_ind              VARCHAR2(1);
   L_alloc_excess_ind       VARCHAR2(1);
   L_cust_details_ind       VARCHAR2(1);
   L_items_ind              VARCHAR2(1);
   L_excess_ind             VARCHAR2(5);
   L_invalid_items          VARCHAR2(54);
   L_subclass_list          VARCHAR2(154) := NULL;
   L_exists_ind             VARCHAR2(10); 
   L_procedure_key          VARCHAR2(30)  := 'CHECK_UTIL_CODE_EXISTS';
  
BEGIN
   ---
   if I_system_options_rec.oms_ind = 'Y' and 
      I_ordhead_row.order_type = 'CO' then
      O_error_message := sql_lib.create_msg('CO_PO_NO_EDIT');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if I_ordhead_row.wf_order_no is NOT NULL then
      O_error_message := sql_lib.create_msg('F_PO_NO_EDIT');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if ORDER_VALIDATE_SQL.ORDER_QTY_EXISTS(O_error_message,
                                          L_order_exists,
                                          I_ordhead_row.order_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if L_order_exists = 'Y' then
      O_error_message := sql_lib.create_msg('NO_QTY_NO_SUBM_APPROV');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;--L_order_exists = 'Y'
   ---
   if SUPP_ATTRIB_SQL.GET_STATUS(I_ordhead_row.supplier,
                                 L_supp_status,
                                 O_error_message) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if L_supp_status = 'I' then
      O_error_message := sql_lib.create_msg('NO_APPROV_INACT_SUPP');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if CORESVC_PO.CUSTOM_APPR_VAL(O_error_message,
                                 L_ord_apprerr_exist,
                                 L_order_no,
                                 L_function_key,
                                 L_seq_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if; 
   --
   if L_ord_apprerr_exist = TRUE then
      raise OI_UTILITY.PROGRAM_ERROR; 
   end if;
   ---
   if ORDER_STATUS_SQL.CHECK_LOC_STATUS(O_error_message,
                                        L_approved_ord,
                                        I_ordhead_row.order_no)= FALSE then
      if L_approved_ord is not NULL then
         O_error_message := sql_lib.create_msg('NOT_ACTIVE_ORD_ITEM');
      end if;
      raise OI_UTILITY.PROGRAM_ERROR;
   end if; 
   ---
   if I_ordhead_row.pickup_date is not NULL and
      I_ordhead_row.pickup_date <= I_ordhead_row.not_before_date then
      L_date := I_ordhead_row.pickup_date;
   else
      L_date := I_ordhead_row.not_before_date;
   end if;
   --
   if I_ordhead_row.status = 'W' then
      if I_ordhead_row.otb_eow_date is NULL then
         O_error_message := sql_lib.create_msg('NULL_OTB_EOW_DATE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_ordhead_row.purchase_type in ('FOB','BACK')
         and I_ordhead_row.pickup_date is NULL then
         O_error_message := sql_lib.create_msg('NULL_PICKUP_DATE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_ordhead_row.not_before_date is NULL then
         O_error_message := sql_lib.create_msg('NULL_NOT_BEF_DATE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_ordhead_row.not_after_date is NULL then
         O_error_message := sql_lib.create_msg('NULL_NOT_AFT_DATE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_ordhead_row.earliest_ship_date is NULL then
         O_error_message := sql_lib.create_msg('NULL_EARLY_SHIP_DATE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_ordhead_row.latest_ship_date is NULL then
         O_error_message := sql_lib.create_msg('NULL_LATE_SHIP_DATE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_system_options_rec.default_tax_type = 'GTAX' then
         if L10N_SQL.CALL_EXEC_FUNC_DOC(O_error_message, 
                                        L_exists_ind, 
                                        L_procedure_key,
                                        I_ordhead_row.import_country_id,
                                        'PO',
                                        I_ordhead_row.order_no) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;  
         if L_exists_ind!= 'Y' then
            O_error_message := sql_lib.create_msg('NO_UTILIZATION_EXIST');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;--default_tax_type = 'GTAX'
      ---
      if I_ordhead_row.order_type = 'CO' then
         if ORDER_VALIDATE_SQL.CHECK_CUST_RECS(O_error_message,
                                               L_cust_ind,
                                               I_ordhead_row.order_no) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         if L_cust_ind = 'N' then
            O_error_message := sql_lib.create_msg('MUST_ENTER_CUST_DETAIL');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         L_cust_ord_ind := 'Y';
      end if;
      ---
      if ORDER_VALIDATE_SQL.CHECK_DETAIL_RECS(O_error_message,
                                              L_cust_details_ind,
                                              L_items_ind,
                                              I_ordhead_row.order_no,
                                              L_cust_ord_ind) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      if L_items_ind = 'N' then
         O_error_message := sql_lib.create_msg('NO_SUBMIT_IF_NO_ITEMS');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_ordhead_row.contract_no is not NULL then
         if CONTRACT_ORDER_SQL.VALIDATE_UPDATES(O_error_message,
                                                I_ordhead_row.contract_no,
                                                I_ordhead_row.order_no,
                                                I_ordhead_row.status,
                                                'A') = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         ---
         if CONTRACT_SQL.GET_CONTRACT_HEADER_ROW(O_error_message,
                                                 L_contract_header,
                                                 I_ordhead_row.contract_no) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if; 
         --
         if I_ordhead_row.not_before_date < L_contract_header.start_date or
            I_ordhead_row.not_before_date > L_contract_header.end_date then
            O_error_message := sql_lib.create_msg('NOT_BEFORE_CONTRACT');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         if I_ordhead_row.not_after_date < L_contract_header.start_date or
            I_ordhead_row.not_after_date > L_contract_header.end_date then
            O_error_message := sql_lib.create_msg('NOT_AFTER_CONTRACT');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         if I_ordhead_row.pickup_date < L_contract_header.start_date or
            I_ordhead_row.pickup_date > L_contract_header.end_date then
            O_error_message := sql_lib.create_msg('PICKUP_CONTRACT');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;--contract_no is not NULL
      ---
      if I_ordhead_row.purchase_type = 'BACK' and
         I_ordhead_row.backhaul_type is NULL  then
         O_error_message := sql_lib.create_msg('BACKHAUL_TYPE_REQ');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if; 
      ---
      if I_ordhead_row.import_type is NOT NULL and 
         I_ordhead_row.import_id is NULL then
         O_error_message := sql_lib.create_msg('IMPORTER_REQ');
         raise OI_UTILITY.PROGRAM_ERROR;
      elsif I_ordhead_row.import_id is NULL and 
            I_ordhead_row.location is NULL and
            I_system_options_rec.org_unit_ind = 'Y' then
         if ORDER_ATTRIB_SQL.CHECK_SUPP_LOC_OU(O_error_message,
                                               L_same_ou,
                                               I_ordhead_row.order_no,
                                               I_ordhead_row.supplier) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         if L_same_ou = FALSE then
            O_error_message := sql_lib.create_msg('SUPP_LOC_DIFF_OU');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;               
      end if;
      ---
      --this will make ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE all validation to submit and approve
      L_new_status_to_validate := 'S';
   else 
      --this will make ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE skip validating moving the PO
      -- from worksheet to submit
      L_new_status_to_validate := 'A';
   end if;
   --
   if ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE(O_error_message,
                                            L_invalid_date,
                                            L_store_closed_ind,
                                            L_store_opened_ind,
                                            L_clearance_ind,
                                            L_wksht_rec_ind,
                                            L_hts_ind,
                                            L_valid_lc_ind,
                                            L_scale_ind,
                                            L_recalc_ind,
                                            L_round_ind,
                                            L_invalid_items,
                                            L_error_collection,
                                            NULL,
                                            I_ordhead_row.order_no,
                                            L_new_status_to_validate,
                                            L_date,
                                            I_ordhead_row.not_after_date,
                                            I_ordhead_row.supplier,
                                            I_ordhead_row.dept) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if O_error_message is not NULL then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if L_invalid_date = 'Y' and I_ordhead_row.orig_approval_date is NULL then
      O_error_message := sql_lib.create_msg('CANNOT_APPR_ORDER_DATE');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   O_valid_lc_ind := L_valid_lc_ind;
   --
   if I_ordhead_row.payment_method                = 'LC' and 
      I_system_options_rec.import_ind = 'Y'  and
      L_valid_lc_ind                  = 'N' then
      O_error_message := sql_lib.create_msg('INV_PO_LC');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if; 
   ---
   if ORDER_VALIDATE_SQL.CHECK_ALLOC_QTY(O_error_message,
                                         L_alloc_excess_ind,
                                         I_ordhead_row.order_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if I_system_options_rec.otb_system_ind = 'Y' and
      I_ordhead_row.order_type                       != 'CO' then
      if OTB_SQL.ORD_CHECK(I_ordhead_row.order_no,
                           L_subclass_list,
                           L_excess_ind,
                           O_error_message) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END VALIDATE_ORDER_APPROVAL;
--------------------------------------------------------------------------------
FUNCTION PERSIST_ORDER_APPROVAL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_ordhead_row        IN     ORDHEAD%ROWTYPE,
                                I_system_options_rec IN     SYSTEM_OPTIONS%ROWTYPE,
                                I_valid_lc_ind       IN     VARCHAR2)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'RMS_OI_ACTION_PO.PERSIST_ORDER_APPROVAL';
   
   L_ranged_loc       VARCHAR2(1);
   L_print_online_ind VARCHAR2(1) := 'N';
   L_lc_ref_id        ORDLC.LC_REF_ID%TYPE;
   L_deal_status      DEAL_HEAD.STATUS%TYPE  := NULL;
   L_deal_type        DEAL_HEAD.TYPE%TYPE    := NULL;
   L_deal_id          DEAL_HEAD.DEAL_ID%TYPE := NULL;
   L_attached         BOOLEAN;

   cursor C_GET_DEAL_INFO is
      select status, 
             type, 
             deal_id
        from deal_head
       where deal_head.order_no = I_ordhead_row.order_no;

BEGIN
   -- check if the items in the order are intentionally ranged to the location/s
   if ORDER_SETUP_SQL.CHECK_RANGED_LOC(O_error_message,
                                       L_ranged_loc,
                                       I_ordhead_row.order_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if L_ranged_loc = 'N' then
      if ORDER_SETUP_SQL.UPDATE_RANGED_IND(O_error_message,
                                           I_ordhead_row.order_no,
                                           NULL,
                                           NULL) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   if I_ordhead_row.status = 'W' then
      if ORDER_STATUS_SQL.DELETE_WKSHT(O_error_message,
                                       I_ordhead_row.order_no) = FALSE then
          raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;--I_status = 'W'
   ---
   if I_ordhead_row.order_type = 'CO' then
      if I_ordhead_row.status = 'W' then
         if ORDER_SETUP_SQL.POP_ORDCUST_DETAIL(O_error_message,
                                               I_ordhead_row.order_no) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;--I_status = 'W'
      ---
      if ORDER_SETUP_SQL.DELETE_ORD_INV_MGMT(O_error_message,
                                             I_ordhead_row.order_no) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   if ORDER_INSTOCK_DATE_SQL.MASS_UPD_INSTOCK_DT(O_error_message,
                                                 I_ordhead_row.order_no,
                                                 I_ordhead_row.supplier,
                                                 NULL) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if ORDER_STATUS_SQL.UPDATE_ALLOC_STATUS(O_error_message,
                                           I_ordhead_row.order_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if I_ordhead_row.payment_method = 'LC' and I_system_options_rec.import_ind = 'Y' then
      if I_valid_lc_ind = 'Y' then
         if ORDER_SETUP_SQL.GET_LC_REF_ID(O_error_message,
                                          L_lc_ref_id,
                                          I_ordhead_row.order_no) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;--GET_LC_REF_ID
         ---
         if L_lc_ref_id is not NULL then
            if LC_SQL.ATTACHED_LC(O_error_message,
                                  L_attached,
                                  I_ordhead_row.order_no) = FALSE then
               raise OI_UTILITY.PROGRAM_ERROR;
            end if;--ATTACHED_LC
            --
            if L_attached = FALSE then
               if LC_SQL.ADD_PO(O_error_message,
                                L_lc_ref_id,
                                I_ordhead_row.order_no) = FALSE then
                  raise OI_UTILITY.PROGRAM_ERROR;
               end if;--ADD_PO
            else
               if LC_SQL.ORDER_UPDATE_LC(O_error_message,
                                         I_ordhead_row.order_no) = FALSE then
                  raise OI_UTILITY.PROGRAM_ERROR;
               end if;--ORDER_UPDATE_LC
            end if;--L_attached
         end if;--L_lc_ref_id is not NULL
      end if;--I_valid_lc_ind = 'Y'
      --
      if (I_valid_lc_ind                          = 'O' and
          I_system_options_rec.rtm_simplified_ind = 'N') or
         (I_valid_lc_ind                          = 'Y' and
          L_lc_ref_id                            is NULL)   then
         --Add Purchase Order to the list available of orders available to
         --be added to a Letter of Credit (lc_ordapply table).
         if LC_SQL.WRITE_LCORDAPP(O_error_message,
                                  FALSE,
                                  I_ordhead_row.order_no) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
   end if; 
   ---
   open C_GET_DEAL_INFO;
   fetch C_GET_DEAL_INFO into L_deal_status, L_deal_type, L_deal_id;
   close C_GET_DEAL_INFO;
   ---
   if L_deal_type = 'O' and
      L_deal_status in ('W','R') then
      if DEAL_SQL.DELETE_DEALS(O_error_message,
                               L_deal_id) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   if OTB_SQL.ORD_APPROVE (I_ordhead_row.order_no,
                           O_error_message) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if TICKET_SQL.APPROVE(O_error_message,
                         I_ordhead_row.order_no,
                         L_print_online_ind) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if ORDER_SETUP_SQL.DELETE_TEMP_TABLES (O_error_message,
                                          I_ordhead_row.order_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if CONTRACT_ORDER_SQL.UPDATE_ORDSTAT (I_ordhead_row.contract_no,
                                         I_ordhead_row.order_no,
                                         I_ordhead_row.status,
                                         'A',
                                         O_error_message) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if I_ordhead_row.contract_no is not NULL then
      if CONTRACT_SQL.POP_COST_HIST(O_error_message,
                                    I_ordhead_row.contract_no,
                                    I_ordhead_row.order_no) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   if ORDER_STATUS_SQL.INSERT_ORD_TAX_BREAKUP(O_error_message,
                                              I_ordhead_row.order_no) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if ORDER_STATUS_SQL.INSERT_ORDER_REVISION(O_error_message,
                                             I_ordhead_row.order_no,
                                             'A') = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   --Call this function to create F orders for xdock PO allocations. 
   --Only applicable to PO's that are created in replenishment, but are being approved manually.
   if I_ordhead_row.orig_ind = 0 then
      if WF_ALLOC_SQL.REPL_SYNC_F_ORDER(O_error_message,
                                        NULL,
                                        I_ordhead_row.order_no) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   update ordhead
      set status             = 'A',
          reject_code        = NULL,
          orig_approval_date = NVL(orig_approval_date, get_vdate),
          orig_approval_id   = NVL(orig_approval_id, get_user)
    where order_no = I_ordhead_row.order_no;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_GET_DEAL_INFO%ISOPEN then close C_GET_DEAL_INFO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_GET_DEAL_INFO%ISOPEN then close C_GET_DEAL_INFO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END PERSIST_ORDER_APPROVAL;
--------------------------------------------------------------------------------
FUNCTION REJECT_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT RMS_OI_PO_STATUS_REC,
                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                   I_comments      IN     ORDHEAD.COMMENT_DESC%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program                VARCHAR2(61)             := 'RMS_OI_ACTION_PO.REJECT_PO';
   L_start_time             TIMESTAMP                := SYSTIMESTAMP;
   L_no_order_with_contract RTK_ERRORS.RTK_TEXT%TYPE := sql_lib.create_msg('NO_ORDER_WITH_CONTRACT');
   L_no_qty_no_wksht        RTK_ERRORS.RTK_TEXT%TYPE := sql_lib.create_msg('NO_QTY_NO_WKSHT');
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_po_fail_tbl_tmp        RMS_OI_PO_FAIL_TBL       := RMS_OI_PO_FAIL_TBL();
   L_po_fail_tbl            RMS_OI_PO_FAIL_TBL       := RMS_OI_PO_FAIL_TBL();
   L_po_fail_rec            RMS_OI_PO_FAIL_REC;
   L_po_success_tbl         OBJ_NUMERIC_ID_TABLE     := OBJ_NUMERIC_ID_TABLE();
   L_orders                 OBJ_NUMERIC_ID_TABLE;
   L_valid                  BOOLEAN                  := TRUE;
   L_cancel_id              ORDLOC.CANCEL_ID%TYPE    := get_user;
   L_alloc_close_ind        VARCHAR2(1)              := 'Y';
        
   cursor C_PO_TO_REJECT is
     select order_no,
            status,
            contract_no,
            payment_method,
            order_type,
            wf_order_no
       from ordhead oh,
            table(cast(L_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where oh.order_no    = value(input_orders);
        
   cursor C_FULLY_RECEIVED_ERROR is
     select RMS_OI_PO_FAIL_REC(order_no,
                               L_no_qty_no_wksht)
       from ordhead oh,
            table(cast(L_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where oh.order_no = value(input_orders)
        and oh.status   = 'A'
        and not exists (select 'x'
                          from ordloc l
                         where l.order_no = oh.order_no
                           and (   (nvl(l.qty_received,0) < l.qty_ordered)
                                or l.qty_ordered          = 0)
                           and rownum     = 1);
   
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   O_result := new RMS_OI_PO_STATUS_REC(0,null,0,null);
   --
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   if VALIDATE_ORDER_NO(O_error_message,
                        L_po_fail_tbl,
                        I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if LOCK_ORDERS(O_error_message,
                  L_po_fail_tbl,
                  I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   if GET_ORDER_WITH_NO_ERRORS(O_error_message,
                               L_po_fail_tbl,
                               L_orders,
                               I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   OI_UTILITY.HANDLE_ERRORS(L_no_qty_no_wksht, L_program);
   --
   open C_FULLY_RECEIVED_ERROR;
   fetch C_FULLY_RECEIVED_ERROR bulk collect into L_po_fail_tbl_tmp;
   close C_FULLY_RECEIVED_ERROR;
   if L_po_fail_tbl_tmp is not null and L_po_fail_tbl_tmp.count > 0 then
      L_po_fail_tbl := L_po_fail_tbl MULTISET UNION ALL L_po_fail_tbl_tmp;
   end if;
   --
   if L_po_fail_tbl is not NULL and L_po_fail_tbl.count > 0 then
      O_result.fail_orders_count := L_po_fail_tbl.count;
      if L_po_fail_tbl.count = I_orders.count then
         O_result.fail_orders_tbl   := L_po_fail_tbl;
         return OI_UTILITY.SUCCESS;
      end if;       
   end if;
   ---
   if GET_ORDER_WITH_NO_ERRORS(O_error_message,
                               L_po_fail_tbl,
                               L_orders,
                               I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   OI_UTILITY.HANDLE_ERRORS(L_no_order_with_contract, L_program);
   ---
   for to_reject in C_PO_TO_REJECT loop
      SAVEPOINT rms_oi_reject_po_savepoint;
      --
      if to_reject.status = 'W' then
         L_po_success_tbl.extend;
         L_po_success_tbl(L_po_success_tbl.count) := to_reject.order_no;
         O_result.success_orders_count := O_result.success_orders_count +1;
      elsif L_system_options_rec.contract_ind = 'Y' and
            to_reject.contract_no is not NULL then
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                 L_no_order_with_contract);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      elsif L_system_options_rec.oms_ind = 'Y' and 
            to_reject.order_type = 'CO' then
         O_error_message := sql_lib.create_msg('CO_PO_NO_EDIT');
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                 O_error_message);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      elsif to_reject.wf_order_no is NOT NULL then
         O_error_message := sql_lib.create_msg('F_PO_NO_EDIT');
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                 O_error_message);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      else
         L_valid := TRUE;
         if to_reject.payment_method = 'LC' and
            L_system_options_rec.rtm_simplified_ind = 'N' then
            if LC_SQL.DELETE_LCORDAPP(O_error_message,
                                      to_reject.order_no) = FALSE then
               OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
               L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                       O_error_message);
               L_po_fail_tbl.extend;
               L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
               O_result.fail_orders_count := O_result.fail_orders_count +1;
               --
               ROLLBACK TO rms_oi_reject_po_savepoint;
               L_valid := FALSE;
            end if;
         end if;--to_reject.payment_method = 'LC
         ---
         if L_valid then
            if to_reject.status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE(to_reject.order_no,
                                        O_error_message) = FALSE then
                  OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                  L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                          O_error_message);
                  L_po_fail_tbl.extend;
                  L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                  O_result.fail_orders_count := O_result.fail_orders_count +1;
                  --
                  ROLLBACK TO rms_oi_reject_po_savepoint;
                  L_valid := FALSE;
               else
                  if CONTRACT_ORDER_SQL.UPDATE_ORDSTAT (to_reject.contract_no,
                                                        to_reject.order_no,
                                                        to_reject.status,
                                                        'C',
                                                        O_error_message) = FALSE then
                     OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                     L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                             O_error_message);
                     L_po_fail_tbl.extend;
                     L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                     O_result.fail_orders_count := O_result.fail_orders_count +1;
                     --
                     ROLLBACK TO rms_oi_reject_po_savepoint;
                     L_valid := FALSE;
                  else
                     if CANCEL_ALLOC_SQL.CANCEL_SINGLE_ALLOC(O_error_message,
                                                             to_reject.order_no,
                                                             L_cancel_id,
                                                             L_alloc_close_ind) = FALSE then
                        OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                        L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                                O_error_message);
                        L_po_fail_tbl.extend;
                        L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                        O_result.fail_orders_count := O_result.fail_orders_count +1;
                        --
                        ROLLBACK TO rms_oi_reject_po_savepoint;
                        L_valid := FALSE;
                     else
                        if CANCEL_ALLOC_SQL.CANCEL_ASN_ALLOC(O_error_message,
                                                             to_reject.order_no,
                                                             L_alloc_close_ind) = FALSE then
                           OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                           L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                                   O_error_message);
                           L_po_fail_tbl.extend;
                           L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                           O_result.fail_orders_count := O_result.fail_orders_count +1;
                           --
                           ROLLBACK TO rms_oi_reject_po_savepoint;
                           L_valid := FALSE;
                        end if;--CANCEL_ASN_ALLOC
                     end if;--CANCEL_SINGLE_ALLOC
                  end if;--UPDATE_ORDSTAT
               end if;--ORD_UNAPPROVE   
            elsif to_reject.status = 'S' then
               if ORDER_STATUS_SQL.REJECT_PO_NOTIFICATION(O_error_message, 
                                                          to_reject.order_no)= OI_UTILITY.FAILURE then
                  L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_reject.order_no,
                                                          O_error_message);
                  L_po_fail_tbl.extend;
                  L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                  O_result.fail_orders_count := O_result.fail_orders_count +1;
                  --
                  ROLLBACK TO rms_oi_reject_po_savepoint;
                  L_valid := FALSE;
               end if;
            end if; --to_reject.status = 'A'
            --
            if L_valid then
               update ordhead
                  set status       = 'W',
                      comment_desc = comment_desc || I_comments
                where order_no = to_reject.order_no;
               --
               L_po_success_tbl.extend;
               L_po_success_tbl(L_po_success_tbl.count) := to_reject.order_no;
               O_result.success_orders_count := O_result.success_orders_count +1;
            end if;--L_valid 2
         end if; --L_valid
      end if;--status
   end loop;
   ---
   O_result.success_orders_tbl := L_po_success_tbl;
   O_result.fail_orders_tbl    := L_po_fail_tbl;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_PO_TO_REJECT%ISOPEN then close C_PO_TO_REJECT; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_PO_TO_REJECT%ISOPEN then close C_PO_TO_REJECT; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
END REJECT_PO;
--------------------------------------------------------------------------------
FUNCTION CANCEL_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT RMS_OI_PO_STATUS_REC,
                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                   I_reason_code   IN     CODE_DETAIL.CODE%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61)          := 'RMS_OI_ACTION_PO.CANCEL_PO';
   L_cannot_cancel_ord   RTK_ERRORS.RTK_TEXT%TYPE;
   L_open_appts          RTK_ERRORS.RTK_TEXT%TYPE;
   L_system_options_rec  SYSTEM_OPTIONS%ROWTYPE;
   L_exists              BOOLEAN;
   L_found               VARCHAR2(1)           := NULL;
   L_cancel_id           ORDLOC.CANCEL_ID%TYPE := get_user;
   L_vdate               DATE                  := get_vdate;
   L_start_time          TIMESTAMP             := SYSTIMESTAMP;
   L_orders              OBJ_NUMERIC_ID_TABLE;
   L_po_fail_tbl         RMS_OI_PO_FAIL_TBL    := RMS_OI_PO_FAIL_TBL();
   L_po_fail_rec         RMS_OI_PO_FAIL_REC;
   L_po_success_tbl      OBJ_NUMERIC_ID_TABLE  := OBJ_NUMERIC_ID_TABLE();
   
   cursor C_VALIDATE_CODE is        
      select 'X' 
        from code_detail 
       where code_type = 'ORCA'
         and code      = I_reason_code;
        
   cursor C_PO_TO_CANCEL is
     select order_no,
            status,
            orig_approval_id,
            contract_no,
            order_type,
            wf_order_no
       from ordhead oh,
            table(cast(L_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where oh.order_no    = value(input_orders);
      
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   O_result := new RMS_OI_PO_STATUS_REC(0,null,0,null);
   --
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   --
   open C_VALIDATE_CODE;
   fetch C_VALIDATE_CODE into L_found;
   close C_VALIDATE_CODE;
   --
   if L_found is NULL then
      O_error_message := sql_lib.create_msg('INV_REASON_CODE');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if VALIDATE_ORDER_NO(O_error_message,
                        L_po_fail_tbl,
                        I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if LOCK_ORDERS(O_error_message,
                  L_po_fail_tbl,
                  I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if L_po_fail_tbl is not NULL and L_po_fail_tbl.count > 0 then
      O_result.fail_orders_count := L_po_fail_tbl.count;
      if L_po_fail_tbl.count = I_orders.count then
         O_result.fail_orders_tbl   := L_po_fail_tbl;
         return OI_UTILITY.SUCCESS;
      end if;       
   end if;
   ---
   if GET_ORDER_WITH_NO_ERRORS(O_error_message,
                               L_po_fail_tbl,
                               L_orders,
                               I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   L_cannot_cancel_ord := sql_lib.create_msg('CANNOT_CANCEL_ORD'); 
   OI_UTILITY.HANDLE_ERRORS(L_cannot_cancel_ord, L_program);
   L_open_appts := sql_lib.create_msg('OPEN_APPTS'); 
   OI_UTILITY.HANDLE_ERRORS(L_open_appts, L_program);
   ---
   for to_cancel in C_PO_TO_CANCEL loop
      SAVEPOINT rms_oi_cancel_po_savepoint;
      --
      if to_cancel.status = 'C' then
         L_po_success_tbl.extend;
         L_po_success_tbl(L_po_success_tbl.count) := to_cancel.order_no;
         O_result.success_orders_count := O_result.success_orders_count +1;
      elsif to_cancel.orig_approval_id is NULL    or
         (L_system_options_rec.contract_ind = 'Y' and
          to_cancel.contract_no is not NULL) then
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                 L_cannot_cancel_ord);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      elsif L_system_options_rec.oms_ind = 'Y' and 
            to_cancel.order_type = 'CO' then
         O_error_message := sql_lib.create_msg('CO_PO_NO_EDIT');
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                 O_error_message);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      elsif to_cancel.wf_order_no is NOT NULL then
         O_error_message := sql_lib.create_msg('F_PO_NO_EDIT');
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                 O_error_message);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      else
          if APPOINTMENTS_SQL.OPEN_APPOINTMENTS(O_error_message,
                                                L_exists,
                                                to_cancel.order_no,
                                                'P') = FALSE then
             OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
             L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                     O_error_message);
             L_po_fail_tbl.extend;
             L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
             O_result.fail_orders_count := O_result.fail_orders_count +1;
             --
             ROLLBACK TO rms_oi_cancel_po_savepoint;
          else
             if L_exists then
                L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                        L_open_appts);
                L_po_fail_tbl.extend;
                L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                O_result.fail_orders_count := O_result.fail_orders_count +1;
                --
                ROLLBACK TO rms_oi_cancel_po_savepoint;
             else
                if ORDER_STATUS_SQL.CANCEL_SHIPMENT(O_error_message,
                                                    to_cancel.order_no)= FALSE then
                   OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                   L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                           O_error_message);
                   L_po_fail_tbl.extend;
                   L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                   O_result.fail_orders_count := O_result.fail_orders_count +1;
                   --
                   ROLLBACK TO rms_oi_cancel_po_savepoint;
                else
                   if ORDER_STATUS_SQL.DELETE_DEAL_CALC_QUEUE(O_error_message,
                                                              to_cancel.order_no) = FALSE then
                      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                      L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                              O_error_message);
                      L_po_fail_tbl.extend;
                      L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                      O_result.fail_orders_count := O_result.fail_orders_count +1;
                      --
                      ROLLBACK TO rms_oi_cancel_po_savepoint;
                   else
                      if ORDER_STATUS_SQL.CANCEL_ALL(O_error_message,
                                                     to_cancel.order_no,
                                                     I_reason_code,
                                                     L_cancel_id,
                                                     'Y') = FALSE then
                         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_cancel.order_no,
                                                                 O_error_message);
                         L_po_fail_tbl.extend;
                         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
                         O_result.fail_orders_count := O_result.fail_orders_count +1;
                         --
                         ROLLBACK TO rms_oi_cancel_po_savepoint;
                      else 
                         update ordhead
                            set close_date = L_vdate,
                                status     = 'C'
                          where order_no   = to_cancel.order_no;
                         --
                         L_po_success_tbl.extend;
                         L_po_success_tbl(L_po_success_tbl.count) := to_cancel.order_no;
                         O_result.success_orders_count := O_result.success_orders_count +1;
                      end if;--CANCEL_ALL
                   end if;--DELETE_DEAL_CALC_QUEUE
                end if; --CANCEL_SHIPMENT
             end if;--if L_exists OPEN_APPOINTMENTS
          end if;--APPOINTMENTS_SQL.OPEN_APPOINTMENTS
      end if;--if to_cancel.status not...
   end loop;
   ---
   O_result.success_orders_tbl := L_po_success_tbl;
   O_result.fail_orders_tbl    := L_po_fail_tbl;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_VALIDATE_CODE%ISOPEN then close C_VALIDATE_CODE; end if;
      if C_PO_TO_CANCEL%ISOPEN then close C_PO_TO_CANCEL; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_VALIDATE_CODE%ISOPEN then close C_VALIDATE_CODE; end if;
      if C_PO_TO_CANCEL%ISOPEN then close C_PO_TO_CANCEL; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
END CANCEL_PO;
--------------------------------------------------------------------------------
FUNCTION CANCEL_PO_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result           OUT RMS_OI_PO_ITEM_LOC_STATUS_REC,
                        I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                        I_reason_code   IN     CODE_DETAIL.CODE%TYPE,
                        I_order         IN     ORDHEAD.ORDER_NO%TYPE,
                        I_item          IN     ITEM_MASTER.ITEM%TYPE,
                        I_agg_diff_1    IN     ITEM_MASTER.DIFF_1%TYPE,
                        I_agg_diff_2    IN     ITEM_MASTER.DIFF_2%TYPE,
                        I_agg_diff_3    IN     ITEM_MASTER.DIFF_3%TYPE,
                        I_agg_diff_4    IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.CANCEL_PO_ITEM';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_found               VARCHAR2(1)  := NULL;
   L_po_status           ORDHEAD.STATUS%TYPE;
   L_rec_locked          RTK_ERRORS.RTK_TEXT%TYPE := sql_lib.create_msg('REC_LOCKED');
   L_ship_appt_qty       ORDLOC.QTY_ORDERED%TYPE;
   L_exptd_recv_qty      ORDLOC.QTY_ORDERED%TYPE;
   L_qty_cancelled       ORDLOC.QTY_ORDERED%TYPE;
   L_new_qty_ord         ORDLOC.QTY_ORDERED%TYPE  := 0;
   L_valid_qty_change    BOOLEAN;
   L_adequate_sup_avail  BOOLEAN := TRUE;
   L_unit_tolerance      REPL_ITEM_LOC.UNIT_TOLERANCE%TYPE;
   L_pct_tolerance       REPL_ITEM_LOC.PCT_TOLERANCE%TYPE;
   L_pct_change          ORDLOC.UNIT_COST%TYPE;
   L_min_repl_qty        ORDLOC.QTY_ORDERED%TYPE;
   L_max_repl_qty        ORDLOC.QTY_ORDERED%TYPE;
   L_user                VARCHAR2(60)             := get_user;
   L_vdate               DATE                     := get_vdate;
   L_contract_type       CONTRACT_HEADER.CONTRACT_TYPE%TYPE;
   L_covered_inbound     INTEGER;
   L_total_ord_qty_table WRP_ROUNDING_ORDQTY_TBL;
   
   cursor C_VALIDATE_CODE is        
      select 'X' 
        from code_detail 
       where code_type = 'ORCA'
         and code      = I_reason_code;
         
   cursor C_GET_ORDLOC is        
      select h.order_no,
             h.supplier,
             h.contract_no,
             l.item,
             l.location,
             l.loc_type,
             l.qty_received,
             l.qty_ordered,
             l.qty_cancelled,
             l.original_repl_qty,
             im.deposit_item_type,
             im.container_item
        from ordhead h,
             ordloc l,
             item_master im
       where h.order_no    = I_order
         and h.status      = 'A'
         and l.order_no    = h.order_no
         and l.item        = im.item
         and im.tran_level = im.item_level
         and (   im.item   = I_item
              or (    im.item_parent = I_item
                  and NVL(im.diff_1, '-999') = NVL(I_agg_diff_1,NVL(im.diff_1, '-999'))
                  and NVL(im.diff_1, '-999') = NVL(I_agg_diff_1,NVL(im.diff_1, '-999'))
                  and NVL(im.diff_2, '-999') = NVL(I_agg_diff_2,NVL(im.diff_2, '-999'))
                  and NVL(im.diff_3, '-999') = NVL(I_agg_diff_3,NVL(im.diff_3, '-999'))
                  and NVL(im.diff_4, '-999') = NVL(I_agg_diff_4,NVL(im.diff_4, '-999'))));
         
   cursor C_GET_ORDLOC_QTY is        
      select WRP_ROUNDING_ORDQTY_REC(l.item,
                                     l.location,
                                     l.qty_ordered + l.qty_cancelled)
        from ordloc l
       where l.order_no = I_order;
         
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);

   O_result := RMS_OI_PO_ITEM_LOC_STATUS_REC(0,RMS_OI_PO_ITEM_LOC_SUCCESS_TBL(),0,RMS_OI_PO_ITEM_LOC_FAIL_TBL());

   ---
   open C_VALIDATE_CODE;
   fetch C_VALIDATE_CODE into L_found;
   close C_VALIDATE_CODE;
   --
   if L_found is NULL then
      O_error_message := sql_lib.create_msg('INV_REASON_CODE');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if ORDER_SETUP_SQL.LOCK_DETAIL(O_error_message,
                                  I_order,
                                  NULL,
                                  NULL,
                                  'ordhead') = FALSE then
      if O_error_message like '%TABLE_LOCKED%' then
         O_error_message := L_rec_locked;
      end if;
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   for to_cancel_item_loc in C_GET_ORDLOC loop

      SAVEPOINT rms_oi_cancel_po_savepoint;

      if to_cancel_item_loc.qty_received >= to_cancel_item_loc.qty_ordered then
         L_error_message := sql_lib.create_msg('REC_NO_CANCEL_ORDER', to_cancel_item_loc.item);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
         O_result.fail_order_item_locs_tbl.extend;
         O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) := 
            RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
         ROLLBACK TO rms_oi_cancel_po_savepoint;
      end if;
      --
      if ORDER_VALIDATE_SQL.VALIDATE_SHIP_QTY(L_error_message,
                                              L_ship_appt_qty,
                                              to_cancel_item_loc.order_no,
                                              to_cancel_item_loc.item,
                                              to_cancel_item_loc.location) = FALSE then
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
         O_result.fail_order_item_locs_tbl.extend;
         O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
            RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
         ROLLBACK TO rms_oi_cancel_po_savepoint;
      end if;
      --
      if L_ship_appt_qty >= to_cancel_item_loc.qty_ordered then 
         L_error_message := sql_lib.create_msg('ORD_SHIPSKU_EXIST');
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
         O_result.fail_order_item_locs_tbl.extend;
         O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
            RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
         ROLLBACK TO rms_oi_cancel_po_savepoint;
      end if;
      --
      if (NVL(L_ship_appt_qty, 0) > NVL(to_cancel_item_loc.qty_received, 0)) then
         L_exptd_recv_qty := NVL(L_ship_appt_qty, 0);
      else
         L_exptd_recv_qty := NVL(to_cancel_item_loc.qty_received, 0);
      end if;
      --
      L_qty_cancelled := to_cancel_item_loc.qty_ordered - L_exptd_recv_qty;
      if L_qty_cancelled > 0 then
         if CANCEL_ALLOC_SQL.CANCEL_ALLOC_ITEM(L_error_message,
                                               to_cancel_item_loc.order_no,
                                               to_cancel_item_loc.item,
                                               to_cancel_item_loc.location) = FALSE then
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
         O_result.fail_order_item_locs_tbl.extend;
         O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
            RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
         ROLLBACK TO rms_oi_cancel_po_savepoint;
         end if;
         --
         L_new_qty_ord := to_cancel_item_loc.qty_ordered - L_qty_cancelled;
         --
         if to_cancel_item_loc.original_repl_qty is not NULL then
            if REPLENISHMENT_SQL.GET_TOLERANCES (L_error_message,
                                                 L_pct_tolerance,
                                                 L_unit_tolerance,
                                                 to_cancel_item_loc.item,
                                                 to_cancel_item_loc.location) = FALSE then
               OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
               O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
               O_result.fail_order_item_locs_tbl.extend;
               O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
                  RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
               ROLLBACK TO rms_oi_cancel_po_savepoint;
            end if;
            ---
            L_pct_change := to_cancel_item_loc.original_repl_qty * (L_pct_tolerance / 100);
            ---
            if NVL(L_pct_change,0) > NVL(L_unit_tolerance,0) then
               L_min_repl_qty := to_cancel_item_loc.original_repl_qty - L_pct_change;
               L_max_repl_qty := to_cancel_item_loc.original_repl_qty + L_pct_change;
            else
               L_min_repl_qty := to_cancel_item_loc.original_repl_qty - L_unit_tolerance;
               L_max_repl_qty := to_cancel_item_loc.original_repl_qty + L_unit_tolerance;
            end if;
         else
            L_min_repl_qty := null;
            L_max_repl_qty := null;
         end if;
         --
         if ORDER_ITEM_VALIDATE_SQL.CHECK_QTY_CHANGE(L_error_message,
                                                     L_valid_qty_change,
                                                     to_cancel_item_loc.order_no,
                                                     to_cancel_item_loc.item,
                                                     to_cancel_item_loc.location,
                                                     to_cancel_item_loc.loc_type,
                                                     L_new_qty_ord,
                                                     to_cancel_item_loc.qty_ordered,
                                                     0,--qty_allocated
                                                     to_cancel_item_loc.qty_received,
                                                     L_qty_cancelled,
                                                     to_cancel_item_loc.original_repl_qty,
                                                     L_min_repl_qty,
                                                     L_max_repl_qty,
                                                     1,--L_supp_pack_size,
                                                     'C') = FALSE or L_valid_qty_change = FALSE then
            OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
            O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
            O_result.fail_order_item_locs_tbl.extend;
            O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
               RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
            ROLLBACK TO rms_oi_cancel_po_savepoint;
         end if;
         --
         update ordloc
            set qty_cancelled = L_qty_cancelled,
                qty_ordered   = L_new_qty_ord,
                cancel_date   = L_vdate,
                cancel_id     = L_user,     
                cancel_code   = I_reason_code
          where order_no = to_cancel_item_loc.order_no
            and item     = to_cancel_item_loc.item
            and location = to_cancel_item_loc.location;
         --
         if OTB_SQL.ORD_CANCEL_REINSTATE (L_error_message,
                                          to_cancel_item_loc.order_no,
                                          to_cancel_item_loc.item,
                                          to_cancel_item_loc.location,
                                          to_cancel_item_loc.loc_type,
                                          L_qty_cancelled,
                                          'C') = FALSE then
            OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
            O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
            O_result.fail_order_item_locs_tbl.extend;
            O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
               RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
            ROLLBACK TO rms_oi_cancel_po_savepoint;
         end if;
         --
         if to_cancel_item_loc.contract_no is not NULL then
            --Call function to get contract_type associated with this contract_no
            if CONTRACT_SQL.GET_CONTRACT_TYPE(L_error_message,
                                              L_contract_type,
                                              to_cancel_item_loc.contract_no) = FALSE then
               OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
               O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
               O_result.fail_order_item_locs_tbl.extend;
               O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
                  RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
               ROLLBACK TO rms_oi_cancel_po_savepoint;
            end if;
            --
            if CONTRACT_ORDER_SQL.UPDATE_ORDITEM(L_error_message,
                                                 L_adequate_sup_avail,
                                                 to_cancel_item_loc.contract_no,
                                                 to_cancel_item_loc.order_no,
                                                 to_cancel_item_loc.item,
                                                 L_qty_cancelled,
                                                 L_contract_type,
                                                 'D',
                                                 'Y',
                                                 FALSE) = FALSE or L_adequate_sup_avail = FALSE then
               OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
               O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
               O_result.fail_order_item_locs_tbl.extend;
               O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
                  RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
               ROLLBACK TO rms_oi_cancel_po_savepoint;
            end if;--UPDATE_ORDITEM
         end if;--contract_no is not NULL
         --
         if to_cancel_item_loc.deposit_item_type = 'E' then 
           if ORDER_SETUP_SQL.CANCEL_CONTAINER_ITEMS (L_error_message,
                                                      to_cancel_item_loc.order_no,
                                                      to_cancel_item_loc.container_item,
                                                      to_cancel_item_loc.location) = FALSE then
               OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
               O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
               O_result.fail_order_item_locs_tbl.extend;
               O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
                  RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
               ROLLBACK TO rms_oi_cancel_po_savepoint;
            end if;--CANCEL_CONTAINER_ITEMS
         end if;--deposit_item_type = 'E'
      end if; --L_qty_cancelled > 0
      --
      open C_GET_ORDLOC_QTY;
      fetch C_GET_ORDLOC_QTY bulk collect into L_total_ord_qty_table;
      close C_GET_ORDLOC_QTY;
      --
      if ROUNDING_SQL.ORDER_INTERFACE_WRP(L_error_message,
                                          L_covered_inbound,
                                          to_cancel_item_loc.order_no,
                                          to_cancel_item_loc.supplier,
                                          L_total_ord_qty_table,
                                          'N',
                                          'N') = OI_UTILITY.FAILURE then
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_order_item_locs_count := O_result.fail_order_item_locs_count + 1;
         O_result.fail_order_item_locs_tbl.extend;
         O_result.fail_order_item_locs_tbl(O_result.fail_order_item_locs_tbl.count) :=
            RMS_OI_PO_ITEM_LOC_FAIL_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location, L_error_message);
         ROLLBACK TO rms_oi_cancel_po_savepoint;
      end if;--ORDER_INTERFACE_WRP

      O_result.success_order_item_locs_count := O_result.success_order_item_locs_count + 1;
      O_result.success_order_item_locs_tbl.extend;
      O_result.success_order_item_locs_tbl(O_result.success_order_item_locs_tbl.count) := 
         RMS_OI_PO_ITEM_LOC_SUCCESS_REC(to_cancel_item_loc.order_no, to_cancel_item_loc.item, to_cancel_item_loc.location);

   end loop;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_VALIDATE_CODE%ISOPEN then close C_VALIDATE_CODE; end if;
      if C_GET_ORDLOC%ISOPEN then close C_GET_ORDLOC; end if;
      if C_GET_ORDLOC_QTY%ISOPEN then close C_GET_ORDLOC_QTY; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_VALIDATE_CODE%ISOPEN then close C_VALIDATE_CODE; end if;
      if C_GET_ORDLOC%ISOPEN then close C_GET_ORDLOC; end if;
      if C_GET_ORDLOC_QTY%ISOPEN then close C_GET_ORDLOC_QTY; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END CANCEL_PO_ITEM;
--------------------------------------------------------------------------------
FUNCTION DELETE_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT RMS_OI_PO_STATUS_REC,
                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61)         := 'RMS_OI_ACTION_PO.DELETE_PO';
   L_start_time          TIMESTAMP            := SYSTIMESTAMP;
   L_po_fail_tbl         RMS_OI_PO_FAIL_TBL   := RMS_OI_PO_FAIL_TBL();
   L_po_fail_rec         RMS_OI_PO_FAIL_REC;
   L_po_success_tbl      OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_orders              OBJ_NUMERIC_ID_TABLE;
   L_inv_deleted_status  RTK_ERRORS.RTK_TEXT%TYPE;
        
   cursor C_PO_TO_DELETE is
     select order_no,
            status
       from ordhead oh,
            table(cast(L_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where oh.order_no    = value(input_orders);
            
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   O_result := new RMS_OI_PO_STATUS_REC(0,null,0,null);
   --
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   L_inv_deleted_status := sql_lib.create_msg('INV_DELETED_STATUS');
   OI_UTILITY.HANDLE_ERRORS(L_inv_deleted_status, L_program);
   ---   
   if VALIDATE_ORDER_NO(O_error_message,
                        L_po_fail_tbl,
                        I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   --
   if LOCK_ORDERS(O_error_message,
                  L_po_fail_tbl,
                  I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   if L_po_fail_tbl is not NULL and L_po_fail_tbl.count > 0 then
      O_result.fail_orders_count := L_po_fail_tbl.count;
      if L_po_fail_tbl.count = I_orders.count then
         O_result.fail_orders_tbl   := L_po_fail_tbl;
         return OI_UTILITY.SUCCESS;
      end if;       
   end if;
   ---
   if GET_ORDER_WITH_NO_ERRORS(O_error_message,
                               L_po_fail_tbl,
                               L_orders,
                               I_orders) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   for to_delete in C_PO_TO_DELETE loop
      SAVEPOINT rms_oi_delete_po_savepoint;
      if to_delete.status not in ('W','S') then
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_delete.order_no,
                                                 L_inv_deleted_status);
         L_po_fail_tbl.extend;
         L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
         O_result.fail_orders_count := O_result.fail_orders_count +1;
      else
         if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                         to_delete.order_no,
                                         NULL,
                                         NULL,
                                         NULL,
                                         'ordhead') = FALSE then
            OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
            L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_delete.order_no,
                                                    O_error_message);
            L_po_fail_tbl.extend;
            L_po_fail_tbl(L_po_fail_tbl.count) := L_po_fail_rec;
            O_result.fail_orders_count := O_result.fail_orders_count +1;
            ROLLBACK TO rms_oi_delete_po_savepoint;
         else
            delete from ordhead
             where order_no = to_delete.order_no;
            --
            L_po_success_tbl.extend;
            L_po_success_tbl(L_po_success_tbl.count) := to_delete.order_no;
            O_result.success_orders_count := O_result.success_orders_count +1;
         end if;--end delete order
      end if;--end if status
   end loop;
   ---
   O_result.success_orders_tbl := L_po_success_tbl;
   O_result.fail_orders_tbl    := L_po_fail_tbl;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_PO_TO_DELETE%ISOPEN then close C_PO_TO_DELETE; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_PO_TO_DELETE%ISOPEN then close C_PO_TO_DELETE; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      RETURN OI_UTILITY.FAILURE;
END DELETE_PO;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                           I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.VALIDATE_ORDER_NO';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_po_fail_tbl         RMS_OI_PO_FAIL_TBL := RMS_OI_PO_FAIL_TBL();
   L_inv_order_no        RTK_ERRORS.RTK_TEXT%TYPE;
   
   cursor C_GET_INVALID_PO is
      select RMS_OI_PO_FAIL_REC(order_no,
                                error_msg)
        from (select value(input_orders) order_no,
                     CASE WHEN oh.order_no is NULL THEN
                              L_inv_order_no
                     END error_msg
                from ordhead oh,
                     table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
                where oh.order_no(+)    = value(input_orders))
        where error_msg is not NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   L_inv_order_no       := sql_lib.create_msg('INV_ORDER_NO');
   OI_UTILITY.HANDLE_ERRORS(L_inv_order_no, L_program);
   ---
   open C_GET_INVALID_PO;
   fetch C_GET_INVALID_PO bulk collect into O_po_fail_tbl;
   close C_GET_INVALID_PO;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_GET_INVALID_PO%ISOPEN then close C_GET_INVALID_PO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_GET_INVALID_PO%ISOPEN then close C_GET_INVALID_PO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END VALIDATE_ORDER_NO;
--------------------------------------------------------------------------------
FUNCTION LOCK_ORDERS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                     I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.LOCK_ORDERS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   L_po_fail_rec         RMS_OI_PO_FAIL_REC;
   L_rec_locked          RTK_ERRORS.RTK_TEXT%TYPE := sql_lib.create_msg('REC_LOCKED');
        
   cursor C_PO_TO_LOCK is
     select order_no
       from ordhead oh,
            table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where oh.order_no    = value(input_orders);

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   OI_UTILITY.HANDLE_ERRORS(L_rec_locked, L_program);
   ---
   for to_lock in C_PO_TO_LOCK loop
      SAVEPOINT rms_oi_lock_po_savepoint;
      --
      if ORDER_SETUP_SQL.LOCK_DETAIL(O_error_message,
                                     to_lock.order_no,
                                     NULL,
                                     NULL,
                                    'ordhead') = FALSE then
         if O_error_message like '%TABLE_LOCKED%' then
            O_error_message := L_rec_locked;
         else 
            OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         end if;
         L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_lock.order_no,
                                                 O_error_message);
         O_po_fail_tbl.extend;
         O_po_fail_tbl(O_po_fail_tbl.count) := L_po_fail_rec;
         --
         ROLLBACK TO rms_oi_lock_po_savepoint;
      else
         if ORDER_SETUP_SQL.LOCK_ORDHEAD(O_error_message,
                                         to_lock.order_no) = FALSE then
            if O_error_message like '%ORDER_LOCKED%' then
               O_error_message := L_rec_locked;
            else 
               OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
            end if;
            --
            OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
            L_po_fail_rec := new RMS_OI_PO_FAIL_REC(to_lock.order_no,
                                                    O_error_message);
            O_po_fail_tbl.extend;
            O_po_fail_tbl(O_po_fail_tbl.count) := L_po_fail_rec;
            --
            ROLLBACK TO rms_oi_lock_po_savepoint;
         end if;--end lock ordhead
      end if;--end lock detail
   end loop;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_PO_TO_LOCK%ISOPEN then close C_PO_TO_LOCK; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END LOCK_ORDERS;
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_WITH_NO_ERRORS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_po_fail_tbl   IN OUT RMS_OI_PO_FAIL_TBL,
                                  O_orders           OUT OBJ_NUMERIC_ID_TABLE,
                                  I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.GET_ORDER_WITH_NO_ERRORS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
           
  cursor C_VALID_PO is
     select value(input_orders)
       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where not exists (select 'x'
                          from table(cast(O_po_fail_tbl as RMS_OI_PO_FAIL_TBL)) invalid_orders
                         where invalid_orders.order_no = value(input_orders)
                           and rownum = 1);
                           
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   ---
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   open C_VALID_PO;
   fetch C_VALID_PO bulk collect into O_orders;
   close C_VALID_PO;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_VALID_PO%ISOPEN then close C_VALID_PO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END GET_ORDER_WITH_NO_ERRORS;
------------------------------------------------------------------------------------
FUNCTION UPDATE_PO_DATES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result             OUT RMS_OI_PO_STATUS_REC,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_not_before_date IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                         I_not_after_date  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                         I_otb_eow_date    IN     ORDHEAD.OTB_EOW_DATE%TYPE,
                         I_orders          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.UPDATE_PO_DATES';
   L_vdate               DATE := get_vdate;
   L_otb_eow_date        ORDHEAD.OTB_EOW_DATE%TYPE;
   L_eow_day             VARCHAR2(15);
   L_dd                  NUMBER(2);
   L_mm                  NUMBER(2);
   L_yyyy                NUMBER(4);
   L_ret_code            VARCHAR2(5);
   
   L_po_fail_tbl         RMS_OI_PO_FAIL_TBL := RMS_OI_PO_FAIL_TBL();
   L_po_fail_rec         RMS_OI_PO_FAIL_REC;
   L_orders_to_update    OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   
   L_date_nbd_not_before_today RTK_ERRORS.RTK_TEXT%TYPE;
   L_nbd_before_nad            RTK_ERRORS.RTK_TEXT%TYPE;
   L_nbd_before_otb            RTK_ERRORS.RTK_TEXT%TYPE;
   L_nad_before_otb            RTK_ERRORS.RTK_TEXT%TYPE;
   L_nad_after_pd              RTK_ERRORS.RTK_TEXT%TYPE;
   L_nad_after_today           RTK_ERRORS.RTK_TEXT%TYPE;
   L_no_chng_approv_deal       RTK_ERRORS.RTK_TEXT%TYPE;
   L_not_after_contract        RTK_ERRORS.RTK_TEXT%TYPE;
   L_not_before_contract       RTK_ERRORS.RTK_TEXT%TYPE;
   L_otb_not_bfr_today         RTK_ERRORS.RTK_TEXT%TYPE;
   L_inv_order_no              RTK_ERRORS.RTK_TEXT%TYPE;
   L_co_po_no_edit             RTK_ERRORS.RTK_TEXT%TYPE;
   L_f_po_no_edit              RTK_ERRORS.RTK_TEXT%TYPE;
   
   cursor C_GET_EOW_DAY is
      select to_char(last_eow_date, 'Day')
        from system_variables;
        
   cursor C_GET_PO_ERRORS is
      select RMS_OI_PO_FAIL_REC(order_no,
                                error_msg)
        from (select NVL(oh.order_no,value(input_orders)) order_no,
                     CASE WHEN oh.order_no is NULL THEN
                              L_inv_order_no
                          WHEN I_not_before_date is not NULL and
                               I_not_after_date is NULL and
                                         oh.not_after_date < I_not_before_date THEN
                                         L_nbd_before_nad
                          WHEN I_not_before_date is not NULL and
                               L_otb_eow_date is NULL and
                                         oh.otb_eow_date < I_not_before_date THEN 
                                         L_nbd_before_otb
                          WHEN I_not_before_date is not NULL and
                               oh.contract_no is not NULL and 
                                         (I_not_before_date < ch.start_date or
                                          I_not_before_date > ch.end_date) THEN 
                                         L_not_before_contract
                          WHEN I_not_after_date is not NULL and
                               I_not_before_date is NULL and
                                         I_not_after_date < oh.not_before_date THEN
                                         L_nbd_before_nad
                          WHEN I_not_after_date is not NULL and
                               oh.pickup_date   is not NULL and
                                         I_not_after_date < oh.pickup_date THEN 
                                         L_nad_after_pd
                          WHEN I_not_after_date is not NULL and
                               oh.contract_no is not NULL and
                                         (I_not_after_date < ch.start_date or
                                          I_not_after_date > ch.end_date) THEN
                                         L_not_after_contract
                          WHEN L_otb_eow_date  is not NULL and 
                               I_not_before_date is NULL and
                               L_otb_eow_date < oh.not_before_date THEN
                               L_nbd_before_otb
                          WHEN pco.oms_ind = 'Y' and 
                               oh.order_type = 'CO' then
                               L_co_po_no_edit
                          WHEN oh.wf_order_no is NOT NULL then
                               L_f_po_no_edit
                     END error_msg
                from ordhead oh,
                     contract_header ch,
                     table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders,
                     product_config_options pco
                where oh.order_no(+)    = value(input_orders)
                  and oh.contract_no = ch.contract_no(+)
              UNION ALL
              select order_no,
                     L_no_chng_approv_deal error_msg
                from deal_head,
                     table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
               where order_no = value(input_orders)
                 and status in ('A','S')
                 and active_date < L_vdate)
        where error_msg is not NULL;
        
  cursor C_VALID_PO is
     select value(input_orders)
       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where not exists (select 'x'
                          from table(cast(L_po_fail_tbl as RMS_OI_PO_FAIL_TBL)) invalid_orders
                         where invalid_orders.order_no = value(input_orders)
                           and rownum = 1);

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   ---
   O_result := new RMS_OI_PO_STATUS_REC(0,null,0,null);
   O_result.FAIL_ORDERS_TBL := new RMS_OI_PO_FAIL_TBL();
   O_result.SUCCESS_ORDERS_TBL := new OBJ_NUMERIC_ID_TABLE();
   --
   if (I_orders is null or I_orders.count = 0) then
      return OI_UTILITY.SUCCESS;
   end if;
   ---
   if GET_UPDATE_DATES_ERROR_TEXTS(O_error_message,
                                   L_date_nbd_not_before_today,
                                   L_nbd_before_nad,
                                   L_nbd_before_otb,
                                   L_nad_before_otb,
                                   L_nad_after_pd,
                                   L_nad_after_today,
                                   L_no_chng_approv_deal,
                                   L_not_after_contract,
                                   L_not_before_contract,
                                   L_otb_not_bfr_today,
                                   L_inv_order_no,
                                   L_co_po_no_edit,
                                   L_f_po_no_edit,
                                   L_program) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   --Input validation
   if I_not_after_date is not NULL then
      if I_not_after_date < L_vdate then
         O_error_message            := L_nad_after_today;
         O_result.fail_orders_count := I_orders.count;
         RAISE OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_not_before_date is not null then
        if I_not_after_date < I_not_before_date then
           O_error_message            := L_nbd_before_nad;
           O_result.fail_orders_count := I_orders.count;
           RAISE OI_UTILITY.PROGRAM_ERROR; 
        end if; 
      end if; 
      --- 
   end if;
   ---
   if I_otb_eow_date is not NULL then
      open C_GET_EOW_DAY;
      fetch C_GET_EOW_DAY into L_eow_day;
      close C_GET_EOW_DAY;
      --
      if to_char(I_otb_eow_date, 'Day') = L_eow_day then
         L_otb_eow_date := I_otb_eow_date;
      else
         L_dd   := TO_NUMBER(TO_CHAR(I_otb_eow_date, 'DD'),'09');
         L_mm   := TO_NUMBER(TO_CHAR(I_otb_eow_date, 'MM'), '09');
         L_yyyy := TO_NUMBER(TO_CHAR(I_otb_eow_date, 'YYYY'), '0999');
         --
         CAL_TO_454_LDOW(L_dd,
                         L_mm,
                         L_yyyy,
                         L_dd,
                         L_mm,
                         L_yyyy,
                         L_ret_code,
                         O_error_message);
         if L_ret_code = 'FALSE' then
            RAISE OI_UTILITY.PROGRAM_ERROR;
         end if;
         L_otb_eow_date := TO_DATE((to_char(L_dd)||'-'||
                                    TO_CHAR(L_mm)||'-'||
                                    TO_CHAR(L_yyyy)),'DD-MM-YYYY');
      end if;
      --
      if L_otb_eow_date < L_vdate then
         O_error_message := L_otb_not_bfr_today;
         O_result.fail_orders_count    := I_orders.count;
         RAISE OI_UTILITY.PROGRAM_ERROR;
      end if; 
      ---
      if I_not_before_date is NOT NULL then 
         if L_otb_eow_date < I_not_before_date then 
            O_error_message            := L_nbd_before_otb;
            O_result.fail_orders_count := I_orders.count;
            RAISE OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
      ---
      if I_not_after_date is NOT NULL then 
         if L_otb_eow_date < I_not_after_date then 
            O_error_message            := L_nad_before_otb;
            O_result.fail_orders_count := I_orders.count;
            RAISE OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
      ---
   end if;
   ---
   if I_not_before_date is not NULL then
      if I_not_before_date < L_vdate then
         O_error_message            := L_date_nbd_not_before_today;
         O_result.fail_orders_count := I_orders.count;
         RAISE OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   --PO level validations
   open C_GET_PO_ERRORS;
   fetch C_GET_PO_ERRORS bulk collect into L_po_fail_tbl;
   close C_GET_PO_ERRORS;
   ---
   open C_VALID_PO;
   fetch C_VALID_PO bulk collect into L_orders_to_update;
   close C_VALID_PO;
   --
   if L_po_fail_tbl is not NULL or L_po_fail_tbl.count > 0 then
      O_result.fail_orders_count  := I_orders.count - NVL(L_orders_to_update.count, 0);
      O_result.fail_orders_tbl := L_po_fail_tbl;
       --If all PO already failed return
       if O_result.fail_orders_count = I_orders.count then
          return OI_UTILITY.SUCCESS;
       end if;
   end if;
   ---Persist date changes
   if PERSIST_UPDATE_PO_DATES(O_error_message,
                              O_result,
                              I_not_before_date,
                              I_not_after_date,
                              L_otb_eow_date,
                              L_orders_to_update) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_GET_EOW_DAY%ISOPEN then close C_GET_EOW_DAY; end if;
      if C_GET_PO_ERRORS%ISOPEN then close C_GET_PO_ERRORS; end if;
      if C_VALID_PO%ISOPEN then close C_VALID_PO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      if C_GET_EOW_DAY%ISOPEN then close C_GET_EOW_DAY; end if;
      if C_GET_PO_ERRORS%ISOPEN then close C_GET_PO_ERRORS; end if;
      if C_VALID_PO%ISOPEN then close C_VALID_PO; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      return OI_UTILITY.FAILURE;
END UPDATE_PO_DATES;
--------------------------------------------------------------------------------
FUNCTION GET_UPDATE_DATES_ERROR_TEXTS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_date_nbd_not_before_today    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nbd_before_nad               OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nbd_before_otb               OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nad_before_otb               OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nad_after_pd                 OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_nad_after_today              OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_no_chng_approv_deal          OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_not_after_contract           OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_not_before_contract          OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_otb_not_bfr_today            OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_inv_order_no                 OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_co_po_no_edit                OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_f_po_no_edit                 OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_program                   IN     VARCHAR2)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_PO.GET_UPDATE_DATES_ERROR_TEXTS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   
   L_not_after_string    CODE_DETAIL.CODE_DESC%TYPE;
   L_not_before_string   CODE_DETAIL.CODE_DESC%TYPE;
   L_otb_eow_string      CODE_DETAIL.CODE_DESC%TYPE;
   L_pickup_string       CODE_DETAIL.CODE_DESC%TYPE;
   
BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   OI_UTILITY.LOG_TIME(L_program, L_start_time);
   
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'LABL',
                                 'NOTAFT',
                                 L_not_after_string) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'LABL',
                                 'NOTBEF',
                                 L_not_before_string) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if; 
   --
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                 'LABL', 
                                 'OTBEOW', 
                                 L_otb_eow_string) = FALSE then 
      RAISE OI_UTILITY.PROGRAM_ERROR; 
   end if;  
   --
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'LABL',
                                 'PICKUP',
                                 L_pickup_string) = FALSE then
      RAISE OI_UTILITY.PROGRAM_ERROR; 
   end if;
   ---
   O_date_nbd_not_before_today := sql_lib.create_msg('DATE_NBD_NOT_BEFORE_TODAY');
   O_nbd_before_nad            := sql_lib.create_msg('DATE_X_EARLIER_THAN_Y',L_not_before_string, L_not_after_string);
   O_nbd_before_otb            := sql_lib.create_msg('DATE_X_EARLIER_THAN_Y',L_not_before_string, L_otb_eow_string);
   O_nad_before_otb            := sql_lib.create_msg('DATE_X_EARLIER_THAN_Y',L_not_after_string, L_otb_eow_string);
   O_nad_after_pd              := sql_lib.create_msg('DATE_X_LATER_THAN_Y', L_not_after_string , L_pickup_string);
   O_nad_after_today           := sql_lib.create_msg('NAD_AFTER_TODAY');
   O_no_chng_approv_deal       := sql_lib.create_msg('NO_CHNG_APPROV_DEAL');
   O_not_after_contract        := sql_lib.create_msg('NOT_AFTER_CONTRACT');
   O_not_before_contract       := sql_lib.create_msg('NOT_BEFORE_CONTRACT');
   O_otb_not_bfr_today         := sql_lib.create_msg('OTB_NOT_BFR_TODAY');
   O_inv_order_no              := sql_lib.create_msg('INV_ORDER_NO');
   O_co_po_no_edit             := sql_lib.create_msg('CO_PO_NO_EDIT');
   O_f_po_no_edit              := sql_lib.create_msg('F_PO_NO_EDIT');
   --
   OI_UTILITY.HANDLE_ERRORS(O_date_nbd_not_before_today, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_nbd_before_nad, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_nbd_before_otb, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_nad_after_pd, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_nad_after_today, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_no_chng_approv_deal, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_not_after_contract, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_not_before_contract, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_otb_not_bfr_today, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_inv_order_no, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_co_po_no_edit, I_program);
   OI_UTILITY.HANDLE_ERRORS(O_f_po_no_edit, I_program);
   --
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END GET_UPDATE_DATES_ERROR_TEXTS;
------------------------------------------------------------------------------------
FUNCTION PERSIST_UPDATE_PO_DATES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_result          IN OUT RMS_OI_PO_STATUS_REC,
                                 I_not_before_date IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                                 I_not_after_date  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                                 I_otb_eow_date    IN     ORDHEAD.OTB_EOW_DATE%TYPE,
                                 I_orders          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'RMS_OI_ACTION_PO.PERSIST_UPDATE_PO_DATES';
   L_table              VARCHAR2(30);
   L_approve_otb_failed VARCHAR2(1)  := 'N';

   L_po_fail_tbl        RMS_OI_PO_FAIL_TBL := RMS_OI_PO_FAIL_TBL();
   L_po_fail_rec        RMS_OI_PO_FAIL_REC;
   L_orders_to_update   OBJ_NUMERIC_ID_TABLE := I_orders;

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where exists (select 'x'
                       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
                      where order_no = value(input_orders)
                        and rownum = 1)
         for update nowait;
   
   cursor C_LOCK_DEAL_HEAD is
      select 'x'
        from deal_head
       where status = 'W'
         and exists (select 'x'
                       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
                      where order_no = value(input_orders)
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_ALLOC_HEADER is
      select 'x'
        from alloc_header
       where exists (select 'x'
                       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
                      where order_no = value(input_orders)
                        and rownum = 1)
         for update nowait;

   cursor C_APPROVED_PO is
      select oh.order_no
        from ordhead oh,
             table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
       where oh.order_no = value(input_orders)
         and oh.status   = 'A';
          
   cursor C_VALID_PO is
     select value(input_orders)
       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input_orders
      where not exists (select 'x'
                          from table(cast(L_po_fail_tbl as RMS_OI_PO_FAIL_TBL)) invalid_orders
                         where invalid_orders.order_no = value(input_orders)
                           and rownum = 1);

BEGIN
   ---
   SAVEPOINT rms_oi_update_date_savepoint;
   --- lock table for update
   L_table := 'ORDHEAD';
   open C_LOCK_ORDHEAD;
   close C_LOCK_ORDHEAD;
   --
   L_table := 'DEAL_HEAD';
   open C_LOCK_DEAL_HEAD;
   close C_LOCK_DEAL_HEAD;
   --
   L_table := 'ALLOC_HEADER';
   open C_LOCK_ALLOC_HEADER;
   close C_LOCK_ALLOC_HEADER;
   ---
   --Remove OTB for Approved POs
   if I_otb_eow_date is not NULL then
      for approved_po in C_APPROVED_PO loop
          if OTB_SQL.ORD_UNAPPROVE(approved_po.order_no,
                                   O_error_message) = FALSE then
             OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
             L_po_fail_rec := new RMS_OI_PO_FAIL_REC(approved_po.order_no,
                                                     O_error_message);
             O_result.fail_orders_tbl.extend;
             O_result.fail_orders_tbl(O_result.fail_orders_tbl.count) := L_po_fail_rec;
             O_result.fail_orders_count := O_result.fail_orders_count +1;
          end if;
      end loop;
      --
      L_po_fail_tbl := O_result.fail_orders_tbl;
      open C_VALID_PO;
      fetch C_VALID_PO bulk collect into L_orders_to_update;
      close C_VALID_PO;
      --
      if L_orders_to_update is NULL or L_orders_to_update.count = 0 then
         return OI_UTILITY.SUCCESS;
      end if;
   end if;
   ---Persist date changes
   MERGE INTO ordhead oh
      USING (select h.order_no, 
                    NVL(I_not_before_date, h.not_before_date) not_before_date,
                    NVL(I_not_after_date, h.not_after_date) not_after_date,
                    NVL(I_otb_eow_date, h.otb_eow_date) otb_eow_date
               from ordhead h,
                    table(cast(L_orders_to_update as OBJ_NUMERIC_ID_TABLE)) input_orders
              where h.order_no = value(input_orders)) u
      ON (oh.order_no = u.order_no)
      WHEN MATCHED THEN UPDATE SET
         oh.not_before_date = u.not_before_date,
         oh.not_after_date  = u.not_after_date,
         oh.otb_eow_date    = u.otb_eow_date;
   --
   if I_not_before_date is not NULL then
      update deal_head
         set active_date = I_not_before_date
       where status = 'W'
         and exists (select 'x'
                       from table(cast(L_orders_to_update as OBJ_NUMERIC_ID_TABLE)) input_orders
                      where order_no = value(input_orders)
                        and rownum = 1);
      ---
      update alloc_header ah
         set release_date = I_not_before_date
       where exists (select 'x'
                       from table(cast(L_orders_to_update as OBJ_NUMERIC_ID_TABLE)) input_orders
                      where ah.order_no = value(input_orders)
                        and ah.doc_type = 'PO'
                        and rownum      = 1)
          or ah.alloc_parent in (select ah1.alloc_no
                                   from alloc_header ah1,
                                        table(cast(L_orders_to_update as OBJ_NUMERIC_ID_TABLE)) input_orders
                                  where ah1.order_no = value(input_orders)
                                    and ah1.doc_type = 'PO');
   end if;
   ---
   --Add OTB for Approved POs
   if I_otb_eow_date is not NULL then
      for approved_po in C_APPROVED_PO loop
         if OTB_SQL.ORD_APPROVE (approved_po.order_no,
                                 O_error_message) = FALSE then
            L_approve_otb_failed := 'Y';
            OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
            L_po_fail_rec := new RMS_OI_PO_FAIL_REC(approved_po.order_no,
                                                    O_error_message);
            O_result.fail_orders_tbl.extend;
            O_result.fail_orders_tbl(O_result.fail_orders_tbl.count) := L_po_fail_rec;
            O_result.fail_orders_count := O_result.fail_orders_count +1;
         end if;
      end loop;
      --
      if L_approve_otb_failed = 'Y' then
         L_po_fail_tbl := O_result.fail_orders_tbl;
         open C_VALID_PO;
         fetch C_VALID_PO bulk collect into L_orders_to_update;
         close C_VALID_PO;
         ROLLBACK TO rms_oi_update_date_savepoint;
         --
         if L_orders_to_update is NULL or L_orders_to_update.count = 0 then
            return OI_UTILITY.SUCCESS;
         else --retry for the PO that did not fail in OTB approved
            if PERSIST_UPDATE_PO_DATES(O_error_message,
                                       O_result,
                                       I_not_before_date,
                                       I_not_after_date,
                                       I_otb_eow_date,
                                       L_orders_to_update) = OI_UTILITY.FAILURE then
               return OI_UTILITY.FAILURE;
            end if;
         end if;
      end if;
   end if;
   ---
   O_result.success_orders_count := I_orders.count - NVL(O_result.fail_orders_count,0);
   O_result.success_orders_tbl := L_orders_to_update;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when RECORD_LOCKED then
      if C_VALID_PO%ISOPEN then close C_VALID_PO; end if;
      if C_APPROVED_PO%ISOPEN then close C_APPROVED_PO; end if;
      if C_LOCK_ORDHEAD%ISOPEN then close C_LOCK_ORDHEAD; end if;
      if C_LOCK_DEAL_HEAD%ISOPEN then close C_LOCK_DEAL_HEAD; end if;
      if C_LOCK_ALLOC_HEADER%ISOPEN then close C_LOCK_ALLOC_HEADER; end if;
      O_result.fail_orders_count := I_orders.count;
      O_error_message := SQL_LIB.CREATE_MSG('DELRECSB_REC_LOC',L_table);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return OI_UTILITY.FAILURE;
   when OTHERS then
      if C_VALID_PO%ISOPEN then close C_VALID_PO; end if;
      if C_APPROVED_PO%ISOPEN then close C_APPROVED_PO; end if;
      if C_LOCK_ORDHEAD%ISOPEN then close C_LOCK_ORDHEAD; end if;
      if C_LOCK_DEAL_HEAD%ISOPEN then close C_LOCK_DEAL_HEAD; end if;
      if C_LOCK_ALLOC_HEADER%ISOPEN then close C_LOCK_ALLOC_HEADER; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      O_result.fail_orders_count := I_orders.count;
      return OI_UTILITY.FAILURE;
END PERSIST_UPDATE_PO_DATES;
--------------------------------------------------------------------------------
FUNCTION ADD_ORDER_REF_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result               OUT RMS_OI_PO_ITEM_STATUS_REC,
                            I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_order_item_tbl    IN     RMS_OI_ORDER_ITEM_TBL)
RETURN NUMBER IS
   L_program        VARCHAR2(61) := 'RMS_OI_INV_ANALYST.ADD_ORDER_REF_ITEM';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;

   L_error_message  rtk_errors.rtk_text%type;
   L_order_no       ordhead.order_no%TYPE := null;
   L_item           item_master.item%TYPE := null;
   L_ref_item       item_master.item%TYPE := null;

   cursor c_ref_item is
      select im.item ref_item
        from item_master im
       where im.primary_ref_item_ind = 'Y'
         and im.item_parent          = L_item;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   O_result := RMS_OI_PO_ITEM_STATUS_REC(0,RMS_OI_PO_ITEM_SUCCESS_TBL(),0,RMS_OI_PO_ITEM_FAIL_TBL());

   --nothing to do
   if I_order_item_tbl is null or I_order_item_tbl.count < 1 then
      return 1;
   end if;

   open c_ref_item;
   fetch c_ref_item into L_ref_item;
   close c_ref_item;

   for i in 1..I_order_item_tbl.count loop

      L_order_no := I_order_item_tbl(i).order_no;
      L_item     := I_order_item_tbl(i).item;

      open c_ref_item;
      fetch c_ref_item into L_ref_item;
      close c_ref_item;

      if L_ref_item is null then

         L_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_REF_ITEM', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
         O_result.fail_order_items_count := O_result.fail_order_items_count + 1;
         O_result.fail_order_items_tbl.extend;
         O_result.fail_order_items_tbl(O_result.fail_order_items_tbl.count) := RMS_OI_PO_ITEM_FAIL_REC(L_order_no,
                                                                                                       L_item,
                                                                                                       L_error_message);
      elsif LOCK_ORDSKU(L_error_message,
                        I_session_id,
                        L_order_no,
                        L_item) = 0 then

         O_result.fail_order_items_count := O_result.fail_order_items_count + 1;
         O_result.fail_order_items_tbl.extend;
         O_result.fail_order_items_tbl(O_result.fail_order_items_tbl.count) := RMS_OI_PO_ITEM_FAIL_REC(L_order_no,
                                                                                                       L_item,
                                                                                                       L_error_message);

      else

         update ordsku os
            set os.ref_item = L_ref_item
          where os.order_no = L_order_no
            and os.item     = L_item;
   
         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' udpate ordsku ref item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   
         merge into ordcust_detail target
         using (select ocd.ordcust_no,
                       ocd.item,
                       ocd.original_item
                  from ordcust oc,
                       ordcust_detail ocd
                 where oc.order_no   = L_order_no
                   and oc.ordcust_no = ocd.ordcust_no
                   and ocd.item      = L_item) use_this
         on (    target.ordcust_no     = use_this.ordcust_no
             and target.item           = use_this.item
             and target.original_item  = use_this.original_item)
         when matched then update
          set target.ref_item = L_ref_item;
   
         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' merge ordcust_detail ref item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         O_result.success_order_items_count := O_result.success_order_items_count + 1;
         O_result.success_order_items_tbl.extend;
         O_result.success_order_items_tbl(O_result.success_order_items_tbl.count) := RMS_OI_PO_ITEM_SUCCESS_REC(L_order_no,
                                                                                                                L_item);

      end if;

   end loop;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END ADD_ORDER_REF_ITEM;
--------------------------------------------------------------------------------
FUNCTION LOCK_ORDSKU(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_session_id      IN     oi_session_id_log.session_id%TYPE,
                     I_order_no        IN     ordsku.order_no%type,
                     I_item            IN     ordsku.item%type)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.LOCK_ORDSKU';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);
   L_order_no     ordsku.order_no%type;
   L_ordcust_no   ordcust_detail.ordcust_no%type;

   cursor c_lock_os is
     select os.order_no
       from ordsku os
      where os.order_no = I_order_no
        and os.item     = I_item
    for update nowait;

   cursor c_lock_ocd is
     select ocd.ordcust_no
       from ordcust oc,
            ordcust_detail ocd
      where oc.order_no   = I_order_no
        and oc.ordcust_no = ocd.ordcust_no
        and ocd.item      = I_item
    for update of ocd.ref_item nowait;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_lock_os;
   fetch c_lock_os into L_order_no;
   close c_lock_os;

   open c_lock_ocd;
   fetch c_lock_ocd into L_ordcust_no;
   close c_lock_ocd;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when RECORD_LOCKED then
      if c_lock_os%isopen then
         close c_lock_os;
      end if;
      if c_lock_ocd%isopen then
         close c_lock_ocd;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            null,
                                            null,
                                            null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END LOCK_ORDSKU;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMS_DEPT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_dept             OUT ORDHEAD.DEPT%TYPE,
                             I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_items         IN     RMS_OI_ITEM_TBL)
RETURN NUMBER IS

   L_program           VARCHAR2(61) := 'RMS_OI_ACTION_PO.VALIDATE_ITEMS_DEPT';
   L_dept_level_orders PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE;
   L_count             NUMBER;
   
   cursor C_GET_SYS_OPT is
      select dept_level_orders
        from procurement_unit_options;
   
   cursor C_GET_DEPT is
      select distinct dept, 
             count(1)
        from item_master im,
             table(cast(I_items as RMS_OI_ITEM_TBL)) input_items
       where im.item = input_items.item
       group by im.dept;
         
BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   ---
   if I_items is null or I_items.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('SKU_NUM_REQ');
      raise OI_UTILITY.PROGRAM_ERROR;
   else
      for i in 1..I_items.count loop
         if I_items(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('SKU_NUM_REQ');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end loop;
   end if;
   ---
   open C_GET_SYS_OPT;
   fetch C_GET_SYS_OPT into L_dept_level_orders;
   close C_GET_SYS_OPT;
   --
   if L_dept_level_orders = 'Y' then
      open C_GET_DEPT;
      fetch C_GET_DEPT into O_dept,
                            L_count;
      close C_GET_DEPT;
      --
      if L_count != I_items.count then
         O_dept := NULL;
         O_error_message := SQL_LIB.CREATE_MSG('SAME_DEPT_ALL_ITEMS');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_GET_SYS_OPT%ISOPEN then close C_GET_SYS_OPT; end if;
      if C_GET_DEPT%ISOPEN then close C_GET_DEPT; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_GET_SYS_OPT%ISOPEN then close C_GET_SYS_OPT; end if;
      if C_GET_DEPT%ISOPEN then close C_GET_DEPT; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END VALIDATE_ITEMS_DEPT;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMS_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier      IN OUT ORDHEAD.SUPPLIER%TYPE,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_items         IN     RMS_OI_ITEM_TBL)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'RMS_OI_ACTION_PO.VALIDATE_ITEMS_SUPPLIER';
   L_found        VARCHAR2(1)  := NULL;
   L_item_count   NUMBER       := I_items.count;
   
   cursor C_VALID_SUP is
      select 'x'
        from (select isup.supplier,
                     rank() OVER (PARTITION BY isup.supplier
                                      ORDER BY isup.item) val_supp
                       from item_supplier isup,
                            table(cast(I_items as RMS_OI_ITEM_TBL)) input_items
                      where isup.supplier = O_supplier
                        and isup.item     = input_items.item)
       where val_supp = L_item_count;

   cursor C_GET_SUPPLIER is
      select supplier
        from (select isup.supplier supplier,
                     count(*) supp_count
                from item_supplier isup,
                     table(cast(I_items as RMS_OI_ITEM_TBL)) input_items
               where isup.item = input_items.item
               group by isup.supplier)
       where supp_count = L_item_count;

   cursor C_GET_PRIMARY_SUPP is
      select supplier
        from (select isup.supplier supplier,
                     count(*) supp_count
                from item_supplier isup,
                     table(cast(I_items as RMS_OI_ITEM_TBL)) input_items
               where isup.item             = input_items.item
                 and isup.primary_supp_ind = 'Y'
               group by isup.supplier)
       where supp_count = L_item_count;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   ---
   if I_items is null or I_items.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('SKU_NUM_REQ');
      raise OI_UTILITY.PROGRAM_ERROR;
   else
      for i in 1..I_items.count loop
         if I_items(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('SKU_NUM_REQ');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end loop;
   end if;
   ---
   if O_supplier is not NULL then
      open C_VALID_SUP;
      fetch C_VALID_SUP into L_found;
      close C_VALID_SUP;
      --
      if L_found is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER_SITE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   else
       open C_GET_PRIMARY_SUPP;
      fetch C_GET_PRIMARY_SUPP into O_supplier;
      close C_GET_PRIMARY_SUPP;
      --
      if O_supplier is NULL then
          open C_GET_SUPPLIER;
         fetch C_GET_SUPPLIER into O_supplier;
         close C_GET_SUPPLIER;
         --
         if O_supplier is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER_SITE');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_VALID_SUP%ISOPEN then close C_VALID_SUP; end if;
      if C_GET_SUPPLIER%ISOPEN then close C_GET_SUPPLIER; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_VALID_SUP%ISOPEN then close C_VALID_SUP; end if;
      if C_GET_SUPPLIER%ISOPEN then close C_GET_SUPPLIER; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END VALIDATE_ITEMS_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION CREATE_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_order_no         OUT ORDHEAD.ORDER_NO%TYPE,
                      I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_po_rec        IN     RMS_OI_CREATE_PO_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'RMS_OI_ACTION_PO.CREATE_ORDER';
   L_return_code        VARCHAR2(10) := 'FALSE';
   L_stockholding_ind   VARCHAR2(1);
   L_supp_access_exists BOOLEAN;
   L_supp_loc_same_ou   BOOLEAN;
   L_exists             BOOLEAN;
   L_closed             BOOLEAN;
   L_vadte              Date         := get_vdate;
   L_seq                ADDR.SEQ_NO%TYPE;
   L_not_after_string   CODE_DETAIL.CODE_DESC%TYPE;
   L_not_before_string  CODE_DETAIL.CODE_DESC%TYPE;
   L_loc_country_id     COUNTRY.COUNTRY_ID%TYPE := I_po_rec.import_country_id;
   L_supplier           ORDHEAD.SUPPLIER%TYPE := I_po_rec.supplier;
   L_dept               ORDHEAD.DEPT%TYPE;
   L_agent              ORDHEAD.AGENT%TYPE;
   L_lading_port        ORDHEAD.LADING_PORT%TYPE;
   L_discharge_port     ORDHEAD.DISCHARGE_PORT%TYPE;
   L_partner_type_1     ORDHEAD.PARTNER_TYPE_1%TYPE;
   L_partner1           ORDHEAD.PARTNER1%TYPE;
   L_partner_type_2     ORDHEAD.PARTNER_TYPE_2%TYPE;
   L_partner2           ORDHEAD.PARTNER2%TYPE;
   L_partner_type_3     ORDHEAD.PARTNER_TYPE_3%TYPE;
   L_partner3           ORDHEAD.PARTNER3%TYPE;
   L_factory            ORDHEAD.FACTORY%TYPE;
   L_exchange_rate      ORDHEAD.EXCHANGE_RATE%TYPE;
   L_loc_type           ORDHEAD.LOC_TYPE%TYPE;
   L_latest_ship_date   ORDHEAD.LATEST_SHIP_DATE%TYPE;
   L_store_type         STORE.STORE_TYPE%TYPE;
   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;
   L_purchase_type      SUP_INV_MGMT.PURCHASE_TYPE%TYPE;
   L_pickup_loc         SUP_INV_MGMT.PICKUP_LOC%TYPE;
   L_sups               V_SUPS%ROWTYPE;
   L_physical_wh        WH.PHYSICAL_WH%TYPE;
   
   cursor C_GET_LOC_TYPE is
      select 'S',
             NULL,
             store_type
        from store
      where store = I_po_rec.location
    union all
      select 'W',
             physical_wh,
             null
        from wh
      where wh = I_po_rec.location;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   ---
   if VALIDATE_ITEMS_SUPPLIER(O_error_message,
                              L_supplier,
                              I_session_id ,
                              I_po_rec.items_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   if VALIDATE_ITEMS_DEPT(O_error_message,
                          L_dept,
                          I_session_id,
                          I_po_rec.items_tbl) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;
   ---
   if I_po_rec.import_country_id is not NULL then
      if COUNTRY_VALIDATE_SQL.EXISTS_ON_TABLE(I_po_rec.import_country_id,
                                              'COUNTRY',
                                              O_error_message,
                                              L_exists) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COUNTRY');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   if I_po_rec.not_before_date is not NULL then
      if I_po_rec.not_before_date < L_vadte then
         O_error_message := SQL_LIB.CREATE_MSG('DATE_NBD_NOT_BEFORE_TODAY');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if I_po_rec.not_after_date is not null then
        if I_po_rec.not_after_date < I_po_rec.not_before_date then
           if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                         'LABL',
                                         'NOTAFT',
                                         L_not_after_string) = FALSE then
              raise OI_UTILITY.PROGRAM_ERROR;
           end if;
           --
           if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                         'LABL',
                                         'NOTBEF',
                                         L_not_before_string) = FALSE then
              raise OI_UTILITY.PROGRAM_ERROR; 
           end if;
           --
           O_error_message := SQL_LIB.CREATE_MSG('DATE_X_EARLIER_THAN_Y', L_not_before_string, L_not_after_string);
           raise OI_UTILITY.PROGRAM_ERROR; 
        end if; 
      end if;
      --
      L_latest_ship_date := I_po_rec.not_before_date + L_system_options_rec.latest_ship_days;
   end if;
   ---
   if I_po_rec.not_after_date is not NULL then
      if I_po_rec.not_after_date < L_vadte then
         O_error_message := SQL_LIB.CREATE_MSG('NAD_AFTER_TODAY');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      if L_latest_ship_date is NULL then
         L_latest_ship_date := I_po_rec.not_after_date;
      end if;
   end if;
   ---

   ---
   NEXT_ORDER_NUMBER(O_order_no,
                     L_return_code,
                     O_error_message);
   if L_return_code = 'FALSE' then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if SUPP_ATTRIB_SQL.GET_SUP_PRIMARY_ADDR_SEQ_NO(O_error_message,
                                                  L_seq,
                                                  I_po_rec.supplier,
                                                  '04') = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                L_supp_access_exists,
                                                L_sups,
                                                I_po_rec.supplier) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   --
   if L_sups.sup_status != 'A' then
      O_error_message := SQL_LIB.CREATE_MSG('INACTIVE_SUPPLIER');
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   ---if the system level ELC indicator = Y then retrieve defaults
   if L_system_options_rec.elc_ind = 'Y' then
      if SUPP_ATTRIB_SQL.ORDER_IMPORT_DETAILS(O_error_message,
                                              L_agent,
                                              L_lading_port,
                                              L_discharge_port,
                                              L_factory,
                                              L_partner_type_1,
                                              L_partner1,
                                              L_partner_type_2,
                                              L_partner2,
                                              L_partner_type_3,
                                              L_partner3,
                                              I_po_rec.supplier) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   ---
   ---retrieve the default exchange rate between the order currency and primary currency
   if CURRENCY_SQL.GET_RATE(O_error_message,
                            L_exchange_rate,
                            L_sups.currency_code,
                            'P',
                            NULL) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   if I_po_rec.location is not NULL then
      open C_GET_LOC_TYPE;
      fetch C_GET_LOC_TYPE into L_loc_type,
                                L_physical_wh,
                                L_store_type;
      close C_GET_LOC_TYPE;
      --
      if L_loc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      if LOCATION_ATTRIB_SQL.GET_LOC_COUNTRY(O_error_message,
                                             L_loc_country_id,
                                             I_po_rec.location) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      -- call function to check for stockholding location
      if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                L_stockholding_ind,
                                                I_po_rec.location,
                                                L_loc_type) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      -- For Customer Orders, check for Supplier's Direct Store Delivery capability
      -- If location is stockholding, supplier should allow DSD.
      if I_po_rec.order_type = 'CO' then
         if L_stockholding_ind = 'Y' and L_sups.dsd_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('SUP_MUST_BE_DSD');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      else
         if L_stockholding_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('STOCKHOLD_STORE_WAREHOUSE');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
      ---
      if l_loc_type = 'S' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_CLOSED_STORE(O_error_message,
                                                          L_closed,
                                                          I_po_rec.location) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         ---
         if L_closed = TRUE then
            O_error_message := SQL_LIB.CREATE_MSG('CLOSED_STORE', I_po_rec.location);
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         ---
         if L_store_type = 'F' then
            O_error_message := SQL_LIB.CREATE_MSG('F_STORE_INVALID');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;--l_loc_type = 'S'
      ---
      -- MSOB - Supp-LOC-Org Unit check
      if L_system_options_rec.org_unit_ind = 'Y' then
         ---
         if (L_loc_type = 'W' and
             L_physical_wh <> I_po_rec.location) or
            L_loc_type = 'S'  then       
            if SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC(O_error_message,
                                                      L_supp_loc_same_ou,
                                                      I_po_rec.supplier,
                                                      I_po_rec.location,
                                                      L_loc_type) = FALSE then
               raise OI_UTILITY.PROGRAM_ERROR;
            end if;
         end if;
         --
         if L_supp_loc_same_ou = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
      ---
   else
      if I_po_rec.import_country_id is not NULL then
         L_loc_country_id := I_po_rec.import_country_id;
      else
         L_loc_country_id := L_system_options_rec.base_country_id;
      end if;
   end if;
   ---
   if SUP_INV_MGMT_SQL.GET_PURCHASE_PICKUP(O_error_message,
                                           L_purchase_type,
                                           L_pickup_loc,
                                           I_po_rec.supplier,
                                           case when L_sups.inv_mgmt_lvl in ('L', 'D', 'A') 
                                                   then I_po_rec.dept
                                                else NULL end,
                                           case when L_sups.inv_mgmt_lvl in ('L', 'D', 'A') 
                                                   then I_po_rec.location
                                                else NULL end) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   insert into ORDHEAD (ORDER_NO,
                        ORDER_TYPE,
                        DEPT,
                        SUPPLIER,
                        SUPP_ADD_SEQ_NO,
                        LOC_TYPE,
                        LOCATION,
                        QC_IND,
                        WRITTEN_DATE,
                        NOT_BEFORE_DATE,
                        NOT_AFTER_DATE,
                        EARLIEST_SHIP_DATE,
                        LATEST_SHIP_DATE,
                        TERMS,
                        FREIGHT_TERMS,
                        ORIG_IND,
                        PAYMENT_METHOD,
                        SHIP_METHOD,
                        PURCHASE_TYPE,
                        STATUS,
                        FOB_TITLE_PASS,
                        FOB_TITLE_PASS_DESC,
                        EDI_SENT_IND,
                        EDI_PO_IND,
                        IMPORT_ORDER_IND,
                        IMPORT_COUNTRY_ID,
                        PO_ACK_RECVD_IND,
                        INCLUDE_ON_ORDER_IND,
                        EXCHANGE_RATE,
                        FACTORY,
                        AGENT,
                        DISCHARGE_PORT,
                        LADING_PORT,
                        PRE_MARK_IND,
                        CURRENCY_CODE,
                        PICKUP_LOC,
                        PARTNER_TYPE_1,
                        PARTNER1,
                        PARTNER_TYPE_2,
                        PARTNER2,
                        PARTNER_TYPE_3,
                        PARTNER3,
                        TRIANGULATION_IND,
                        CREATE_ID,
                        CREATE_DATETIME)
                VALUES (O_order_no,
                        NVL(I_po_rec.order_type,'N/B'),
                        I_po_rec.dept,
                        I_po_rec.supplier,
                        L_seq,
                        L_loc_type,
                        I_po_rec.location,
                        L_sups.qc_ind,
                        L_vadte,
                        I_po_rec.not_before_date,
                        I_po_rec.not_after_date,
                        I_po_rec.not_before_date,
                        L_latest_ship_date,
                        L_sups.terms,
                        L_sups.freight_terms,
                        2,
                        L_sups.payment_method,
                        L_sups.ship_method,
                        L_purchase_type,
                        'W',
                        L_system_options_rec.fob_title_pass,
                        L_system_options_rec.fob_title_pass_desc,
                        'N',
                        L_sups.edi_po_ind,
                        'N',
                        L_loc_country_id,
                        'N',
                        'Y',
                        L_exchange_rate,
                        L_factory,
                        L_agent,
                        L_discharge_port,
                        L_lading_port,
                        'N',
                        L_sups.currency_code,
                        L_pickup_loc,
                        L_partner_type_1,
                        L_partner1,
                        L_partner_type_2,
                        L_partner2,
                        L_partner_type_3,
                        L_partner3,
                        'N',
                        get_user,
                        sysdate);
   ---
   if PROCESS_ORDER_ITEMS(O_error_message,
                          O_order_no,
                          I_po_rec,
                          L_loc_type) = OI_UTILITY.FAILURE then
      RETURN OI_UTILITY.FAILURE;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_GET_LOC_TYPE%ISOPEN then close C_GET_LOC_TYPE; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_GET_LOC_TYPE%ISOPEN then close C_GET_LOC_TYPE; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END CREATE_ORDER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_ORDER_ITEMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                             I_po_rec        IN     RMS_OI_CREATE_PO_REC,
                             I_loc_type      IN     ORDHEAD.LOC_TYPE%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'RMS_OI_ACTION_PO.PROCESS_ORDER_ITEMS';
   L_del_item_ind     VARCHAR2(1)  := 'N';
   L_dummy            VARCHAR2(1);
   L_ranged_loc       VARCHAR2(1);
   L_pallet_desc      CODE_DETAIL.CODE_DESC%TYPE;
   L_case_desc        CODE_DETAIL.CODE_DESC%TYPE;
   L_inner_desc       CODE_DETAIL.CODE_DESC%TYPE;
   L_purchase_type    DEPS.PURCHASE_TYPE%TYPE;
   L_item             ITEM_MASTER.ITEM%TYPE;
   L_item_record      ITEM_MASTER%ROWTYPE;
   L_pallet_name      ITEM_SUPPLIER.PALLET_NAME%TYPE;
   L_case_name        ITEM_SUPPLIER.CASE_NAME%TYPE;
   L_inner_name       ITEM_SUPPLIER.INNER_NAME%TYPE;
   L_direct_ship_ind  ITEM_SUPPLIER.DIRECT_SHIP_IND%TYPE;
   L_ti               ITEM_SUPP_COUNTRY.TI%TYPE;
   L_hi               ITEM_SUPP_COUNTRY.HI%TYPE;
   L_supp_pack_size   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size  ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_default_uop      ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE;
   L_origin_country   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_uop              ORDLOC_WKSHT.UOP%TYPE;
   L_diff_1           ORDLOC_WKSHT.DIFF_1%TYPE;
   L_diff_2           ORDLOC_WKSHT.DIFF_2%TYPE;
   L_diff_3           ORDLOC_WKSHT.DIFF_3%TYPE;
   L_diff_4           ORDLOC_WKSHT.DIFF_4%TYPE;
   L_stockholding_ind STORE.STOCKHOLDING_IND%TYPE;

   cursor C_ITEM is
     select *
       from item_master im
       where im.item = L_item;

   cursor C_CHECK_DEL_ITEM is
      select 'Y'
        from daily_purge
       where key_value = L_item;

   cursor C_CHECK_PREPACK is
      select 'x'
        from packitem pi,
             pack_tmpl_head pth
       where pi.pack_no = L_item
         and pth.fash_prepack_ind = 'Y'
         and pi.pack_tmpl_id = pth.pack_tmpl_id;
   
BEGIN
   LOGGER.LOG_INFORMATION(L_program||' Start');
   ---
   for i in 1..I_po_rec.items_tbl.count loop
      L_item := I_po_rec.items_tbl(i).item;
      open C_ITEM;
      fetch C_ITEM into L_item_record;
      --
      if C_ITEM%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM_01',L_item);
         close C_ITEM;
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      close C_ITEM;
      ---
      open C_CHECK_DEL_ITEM;
      fetch C_CHECK_DEL_ITEM into L_del_item_ind;
      close C_CHECK_DEL_ITEM;
      --
      if L_del_item_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ORDER_FROM_DP',L_item);
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if L_item_record.orderable_ind != 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INCORRECT_ORDERABLE_IND');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      -- For Customer Orders, the item must be sellable.
      if I_po_rec.order_type = 'CO' then
         if L_item_record.sellable_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_NON_SELLABLE_CO');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         -- Catchweight and Deposit crate items cannot be added to a Customer Order
         if L_item_record.catch_weight_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_CATCHWEIGHT_CO');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         ---
         if L_item_record.deposit_item_type = 'Z' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DEPOSIT_CRATE_CO');
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         --For Drop Ship Customer Orders (location is a virtual store), the item should be
         --supported by Customer Direct Delivery by the supplier
         if NOT STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(O_error_message,
                                                      L_stockholding_ind,
                                                      I_po_rec.location) then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
         --
         if L_stockholding_ind = 'N' then
            if NOT ITEM_ATTRIB_SQL.GET_DIRECT_SHIP_IND(O_error_message,
                                                       L_direct_ship_ind,
                                                       L_item,
                                                       I_po_rec.supplier) then
               raise OI_UTILITY.PROGRAM_ERROR;
            end if;
            --
            if L_direct_ship_ind = 'N' then
               O_error_message := SQL_LIB.CREATE_MSG('NOT_DIRECT_SHIP',I_po_rec.supplier,L_item);
               raise OI_UTILITY.PROGRAM_ERROR;
            elsif L_item_record.container_item is not NULL then
                 if NOT ITEM_ATTRIB_SQL.GET_DIRECT_SHIP_IND(O_error_message,
                                                            L_direct_ship_ind,
                                                            L_item_record.container_item,
                                                            I_po_rec.supplier) then
                    raise OI_UTILITY.PROGRAM_ERROR;
                 end if;
                 --
                 if L_direct_ship_ind = 'N' then
                    O_error_message := SQL_LIB.CREATE_MSG('NOT_DIRECT_SHIP',I_po_rec.supplier,L_item_record.container_item);
                    raise OI_UTILITY.PROGRAM_ERROR;
                 end if;--L_direct_ship_ind = 'N'
              end if;--L_item_record.container_item is not NULL
         end if;--L_stockholding_ind = 'N'
      end if;--I_po_rec.order_type = 'CO'
      ---
      if L_item_record.deposit_item_type = 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_DEPOSIT_ITEM_TYPE');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if L_item_record.status != 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_APP_ORDER');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if L_item_record.item_level = 1 and L_item_record.tran_level = 3 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_GRAND_ON_ORD');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if L_item_record.item_level > L_item_record.tran_level then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM_01',L_item);
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if ITEM_ATTRIB_SQL.PURCHASE_TYPE(O_error_message,
                                       L_purchase_type,
                                       L_item) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      if L_purchase_type = 1 then
         O_error_message := SQL_LIB.CREATE_MSG('CSMT_ITEMS');
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      open C_CHECK_PREPACK;
      fetch C_CHECK_PREPACK into L_dummy;
      if C_CHECK_PREPACK%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PREPACK_ALLOWED',L_item);
         close C_CHECK_PREPACK;
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      close C_CHECK_PREPACK;
      ---
      if L_item_record.pack_ind = 'Y' and
         L_item_record.orderable_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('PACK_NOT_ORDERABLE',L_item);
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if L_item_record.item_level = L_item_record.tran_level then
         L_diff_1 := L_item_record.diff_1;
         L_diff_2 := L_item_record.diff_2;
         L_diff_3 := L_item_record.diff_3;
         L_diff_4 := L_item_record.diff_4;
      else
         L_diff_1 := I_po_rec.items_tbl(i).diff_1;
         L_diff_2 := I_po_rec.items_tbl(i).diff_2;
         L_diff_3 := I_po_rec.items_tbl(i).diff_3;
         L_diff_4 := I_po_rec.items_tbl(i).diff_4;
      end if;
      ---
      --Get the default origin country.
      if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                  L_origin_country,
                                                  L_item,
                                                  I_po_rec.supplier) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      if SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES(O_error_message,
                                             L_ti,
                                             L_hi,
                                             L_supp_pack_size,
                                             L_inner_pack_size,
                                             L_pallet_desc,
                                             L_case_desc,
                                             L_inner_desc,
                                             L_pallet_name,
                                             L_case_name,
                                             L_inner_name,
                                             L_item,
                                             I_po_rec.supplier,
                                             L_origin_country) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      ---
      -- For Customer Orders, Supplier Pack Size should also be defaulted to 1.
      if I_po_rec.order_type = 'CO' then
         L_supp_pack_size := 1;
      end if;
      ---
      if ITEM_SUPP_COUNTRY_SQL.GET_DEFAULT_UOP(O_error_message,
                                               L_default_uop,
                                               L_item,
                                               I_po_rec.supplier,
                                               L_origin_country) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      if (L_supp_pack_size = 1 and 
          L_default_uop    = 'EA') or
         L_default_uop != 'C' then
         L_uop := L_item_record.standard_uom;
      else
         L_uop := L_case_name;                                        
      end if;
      ---
      insert into ORDLOC_WKSHT (ORDER_NO,
                                ITEM_PARENT,
                                ITEM,
                                DIFF_1,
                                DIFF_2,
                                DIFF_3,
                                DIFF_4,
                                LOC_TYPE,
                                LOCATION,
                                STANDARD_UOM,
                                ORIGIN_COUNTRY_ID,
                                UOP,
                                SUPP_PACK_SIZE)
                        values (I_order_no,
                                CASE WHEN L_item_record.item_level = L_item_record.tran_level then
                                   L_item_record.item_parent
                                ELSE 
                                   L_item
                                END,--ITEM_PARENT
                                CASE WHEN L_item_record.item_level = L_item_record.tran_level then
                                   L_item
                                ELSE
                                   NULL
                                END,--ITEM
                                L_diff_1,
                                L_diff_2,
                                L_diff_3,
                                L_diff_4,
                                I_loc_type,
                                I_po_rec.location,
                                L_item_record.standard_uom,
                                L_origin_country,
                                L_uop,
                                L_supp_pack_size);
   end loop;
   ---
   if ORDER_SETUP_SQL.POP_ITEMS(O_error_message,
                                I_order_no,
                                I_po_rec.supplier) = FALSE then
      raise OI_UTILITY.PROGRAM_ERROR;
   end if;
   ---
   --Check if the item/s are ranged to the location/s in the order
   if I_po_rec.location is NOT NULL then
      if ORDER_SETUP_SQL.CHECK_RANGED_LOC(O_error_message,
                                          L_ranged_loc,
                                          I_order_no) = FALSE then
         raise OI_UTILITY.PROGRAM_ERROR;
      end if;
      --
      --Update the ITEM_LOC.RANGED_IND to 'Y' to make the item/loc intentionally ranged
      --if it was incidentaly ranged previously 
      if L_ranged_loc = 'N' then
         if ORDER_SETUP_SQL.UPDATE_RANGED_IND(O_error_message,
                                              I_order_no,
                                              NULL,
                                              NULL) = FALSE then
            raise OI_UTILITY.PROGRAM_ERROR;
         end if;
      end if;
   end if;
   ---
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      if C_ITEM%ISOPEN then close C_ITEM; end if;
      if C_CHECK_DEL_ITEM%ISOPEN then close C_CHECK_DEL_ITEM; end if;
      if C_CHECK_PREPACK%ISOPEN then close C_CHECK_PREPACK; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      if C_ITEM%ISOPEN then close C_ITEM; end if;
      if C_CHECK_DEL_ITEM%ISOPEN then close C_CHECK_DEL_ITEM; end if;
      if C_CHECK_PREPACK%ISOPEN then close C_CHECK_PREPACK; end if;
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END PROCESS_ORDER_ITEMS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COMMENT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                              I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                              I_comment       IN     ORDHEAD.COMMENT_DESC%TYPE)
RETURN NUMBER IS
   L_program           VARCHAR2(61) := 'RMS_OI_ACTION_PO.UPDATE_ORDER_COMMENT';
   L_start_time  TIMESTAMP    := SYSTIMESTAMP;
BEGIN
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   update ordhead
      set comment_desc = I_comment
    where order_no     = I_order_no;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END UPDATE_ORDER_COMMENT;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END RMS_OI_ACTION_PO;
/
