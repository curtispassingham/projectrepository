CREATE OR REPLACE PACKAGE  RMS_OI_ITEM_DETAIL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
-- FUNCTION: GET_ITEM_DETAIL_INFO
-- PURPOSE:  This function takes item, and optionally, supplier, origin country, location and
--           currency code as the input, and returns an item detail record with basic item
--           information including item image, VPN, item cost and retail, etc.. Cost and retail
--           are returned in I_currency_code; if input currency code is not specified, they are
--           returned in primary currency. The output is used to by the Item Detail contextual BI report. 
-----------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAIL_INFO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_result                 OUT RMS_OI_ITEM_DETAIL_REC,
                              I_item                IN     ITEM_MASTER.ITEM%TYPE,
                              I_supplier            IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                              I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                              I_location            IN     ITEM_LOC.LOC%TYPE,
                              I_currency_code       IN     CURRENCIES.CURRENCY_CODE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------
END;
/