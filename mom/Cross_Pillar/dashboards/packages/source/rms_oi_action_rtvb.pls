CREATE OR REPLACE PACKAGE BODY RMS_OI_ACTION_RTV AS
--------------------------------------------------------------------------------
FUNCTION LOCK_RTV_HEAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_status_ind      IN OUT rtv_head.status_ind%TYPE,
                       I_session_id      IN     oi_session_id_log.session_id%TYPE,
                       I_rtv_order_no    IN     rtv_head.rtv_order_no%type)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CANCEL_RTVS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result           OUT RMS_OI_RTV_STATUS_REC,
                     I_session_id    IN     oi_session_id_log.session_id%TYPE,
                     I_rtv_order_nos IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_RTV.CANCEL_RTVS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

   L_rtv_order_no        rtv_head.rtv_order_no%type;
   L_status              rtv_head.status_ind%type;
   L_error_message       rtk_errors.rtk_text%type;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   --nothing to do
   if I_rtv_order_nos is null or I_rtv_order_nos.count < 1 then
      return 1;
   end if;

   O_result :=  RMS_OI_RTV_STATUS_REC(0, OBJ_NUMERIC_ID_TABLE(), 0, RMS_OI_RTV_FAIL_TBL());

   for i in 1..I_rtv_order_nos.count loop

      L_rtv_order_no := I_rtv_order_nos(i);

      SAVEPOINT rtv_savepoint;

      L_status := null;

      if LOCK_RTV_HEAD(L_error_message,
                       L_status,
                       I_session_id,
                       L_rtv_order_no) = 0 then

         O_result.fail_rtv_count := O_result.fail_rtv_count + 1;
         O_result.fail_rtv_tbl.extend;
         O_result.fail_rtv_tbl(O_result.fail_rtv_tbl.count) := RMS_OI_RTV_FAIL_REC(L_rtv_order_no,
                                                                                   L_error_message);

      elsif L_status is null then --invalid rtv_order_no

         L_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT', 'RTV_ORDER_NO', L_rtv_order_no, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);

         O_result.fail_rtv_count := O_result.fail_rtv_count + 1;
         O_result.fail_rtv_tbl.extend;
         O_result.fail_rtv_tbl(O_result.fail_rtv_tbl.count) := RMS_OI_RTV_FAIL_REC(L_rtv_order_no,
                                                                                   L_error_message);
      elsif L_status = 15 then --shipped status

         L_error_message := SQL_LIB.CREATE_MSG('CANNOT_CANCEL_ORDER', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);

         O_result.fail_rtv_count := O_result.fail_rtv_count + 1;
         O_result.fail_rtv_tbl.extend;
         O_result.fail_rtv_tbl(O_result.fail_rtv_tbl.count) := RMS_OI_RTV_FAIL_REC(L_rtv_order_no,
                                                                                   L_error_message);
      else

         if L_status != 5 then

            if RTV_SQL.UPD_RTV_QTY(L_error_message,
                                   L_rtv_order_no,
                                   NULL,
                                   'C') = FALSE then   -- I_action_type

               OI_UTILITY.HANDLE_ERRORS(L_error_message, L_program);
               O_result.fail_rtv_count := O_result.fail_rtv_count + 1;
               O_result.fail_rtv_tbl.extend;
               O_result.fail_rtv_tbl(O_result.fail_rtv_tbl.count) := RMS_OI_RTV_FAIL_REC(L_rtv_order_no,
                                                                                         L_error_message);
               ROLLBACK TO SAVEPOINT rtv_savepoint;

            end if;

         end if;

         update rtv_head r
            set r.status_ind = 20
          where r.rtv_order_no = L_rtv_order_no;

         O_result.success_rtv_count := O_result.success_rtv_count + 1;
         O_result.success_rtv_tbl.extend;
         O_result.success_rtv_tbl(O_result.success_rtv_tbl.count) := L_rtv_order_no;

      end if;

   end loop;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END CANCEL_RTVS;
--------------------------------------------------------------------------------
FUNCTION SEED_RTV(O_error_message IN OUT rtk_errors.rtk_text%type,
                  O_rtv_order_no     OUT rtv_head.rtv_order_no%type,
                  I_session_id    IN     oi_session_id_log.session_id%TYPE,
                  I_ret_auth_num  IN     rtv_head.ret_auth_num%TYPE,
                  I_supplier_site IN     rtv_head.supplier%TYPE,
                  I_location      IN     item_loc.loc%type,
                  I_location_type IN     item_loc.loc_type%type,
                  I_items         IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_RTV.SEED_RTV';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

   L_exists              VARCHAR2(1)  := 'N';
   L_vdate               DATE := get_vdate;
   L_store               store.store%type;
   L_wh                  wh.wh%type;
   L_ret_allow           varchar2(1);
   L_seq                 NUMBER := 0;

   L_item                item_master.item%type;
   L_deposit_item_type   item_master.deposit_item_type%type;
   L_container_item      item_master.container_item%type;

   L_unit_cost_loc       shipsku.unit_cost%type;
   L_unit_cost_supp      shipsku.unit_cost%type;

   L_add_1               rtv_head.ship_to_add_1%type              := null;
   L_add_2               rtv_head.ship_to_add_2%type              := null;
   L_add_3               rtv_head.ship_to_add_3%type              := null;
   L_city                rtv_head.ship_to_city%type               := null;
   L_state               rtv_head.state%type                      := null;
   L_country             rtv_head.ship_to_country_id%type         := null;
   L_post                rtv_head.ship_to_pcode%type              := null;
   L_juris_code          rtv_head.ship_to_jurisdiction_code%type  := null;

   cursor c_valid_supplier is
      select 'Y'
        from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input_items
       where not exists(select 'x'
                          from item_supplier ips
                         where ips.supplier = I_supplier_site
                           and ips.item     = value(input_items));

   cursor c_pack_store is
      select 'Y'
        from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input_items
       where exists(select 'x'
                     from item_master im
                    where im.item     = value(input_items)
                      and im.pack_ind = 'Y');

   cursor c_non_ord_pack is
      select 'Y'
        from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input_items
       where exists(select 'x'
                     from item_master im
                    where im.item       = value(input_items)
                      and im.pack_ind   = 'Y'
                      and orderable_ind = 'N');

   cursor c_pack_rcv_type is
      select 'Y'
        from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input_items
       where exists(select 'x'
                     from item_master im,
                          item_loc il
                    where im.item                     = value(input_items)
                      and im.pack_ind                 = 'Y'
                      and im.item                     = il.item
                      and il.loc                      = I_location
                      and nvl(il.receive_as_type,'P') = 'E');

   cursor c_get_new_addr is
      select add_1,
             add_2,
             add_3,
             city,
             state,
             country_id,
             post,
             jurisdiction_code
        from addr
       where module = 'SUPP'
         and key_value_1 = I_supplier_site
         and (addr_type = '03' or
             (addr_type = '01' and primary_addr_ind = 'Y'))
         and rownum = 1
    order by addr_type desc;

   cursor c_item_info is
      select im.deposit_item_type,
             im.container_item
        from item_master im
       where im.item = L_item;

   cursor c_inv_status is
      select isq.inv_status,
             isq.qty
        from rms_oi_unexpected_inv ui,
             inv_status_qty isq
       where ui.session_id       = I_session_id
         and ui.item             = L_item
         and ui.loc              = I_location
         and ui.non_sellable_qty > 0
         and ui.item             = isq.item
         and ui.loc              = isq.location;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   if SUPP_ATTRIB_SQL.GET_RET_ALLOW_IND(O_error_message,
                                        I_supplier_site,
                                        L_ret_allow) = FALSE then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   end if;

   if L_ret_allow = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SUP_RTN', null, null, null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   end if;

   open c_valid_supplier;
   fetch c_valid_supplier into L_exists;
   close c_valid_supplier;

   if L_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('RTV_ITEM_SUPS', null, null, null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   end if;

   if I_location_type = 'S' then
      L_exists := 'N';
      open c_pack_store;
      fetch c_pack_store into L_exists;
      close c_pack_store;
      if L_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_RET_PACK_STORE', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         return 0;
      end if;
   end if;

   if I_location_type = 'W' then

      L_exists := 'N';
      open c_non_ord_pack;
      fetch c_non_ord_pack into L_exists;
      close c_non_ord_pack;
      if L_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ORD_PACK_RTV', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         return 0;
      end if;

      L_exists := 'N';
      open c_pack_rcv_type;
      fetch c_pack_rcv_type into L_exists;
      close c_pack_rcv_type;
      if L_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_RCV_EACH_RTV', null, null, null);
         OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
         return 0;
      end if;

   end if;

   select rtv_order_no_seq.nextval into O_rtv_order_no from dual;
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id|| ' O_rtv_order_no: '||O_rtv_order_no);

   open C_GET_NEW_ADDR;
   fetch C_GET_NEW_ADDR into L_add_1,
                             L_add_2,
                             L_add_3,
                             L_city,
                             L_state,
                             L_country,
                             L_post,
                             L_juris_code;
   close C_GET_NEW_ADDR;

   if I_location_type = 'S' then
      L_store := I_location;
      L_wh    := -1;
   else
      L_store := -1;
      L_wh    := I_location;
   end if;

   insert into rtv_head (rtv_order_no,
                         supplier,
                         status_ind,
                         store,
                         wh,
                         total_order_amt,
                         ship_to_add_1,
                         ship_to_add_2,
                         ship_to_add_3,
                         ship_to_city,
                         state,
                         ship_to_country_id,
                         ship_to_pcode,
                         ret_auth_num,
                         courier,
                         freight,
                         created_date,
                         completed_date,
                         ext_ref_no,
                         comment_desc,
                         mrt_no,
                         not_after_date,
                         restock_pct,
                         restock_cost,
                         item,
                         ship_to_jurisdiction_code,
                         dept)
   values (O_rtv_order_no,
           I_supplier_site,
           5,                --status_ind,
           L_store,
           L_wh,
           null,             --total_order_amt,
           L_add_1,
           L_add_2,
           L_add_3,
           L_city,
           L_state,
           L_country,
           L_post,
           I_ret_auth_num,
           null,             --courier,
           null,             --freight,
           L_vdate,          --created_date
           null,             --completed_date
           null,             --ext_ref_no
           null,             --comment_desc
           null,             --mrt_no
           null,             --not_after_date
           null,             --restock_pct
           null,             --restock_cost
           null,             --item
           L_juris_code,
           null);            --dept

      LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                             ' insert rtv_head - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
 
   if I_items is not null and I_items.count > 0 then
      for i in 1..I_items.count loop

         L_item := I_items(i);
         open c_item_info;
         fetch c_item_info into L_deposit_item_type,
                                L_container_item;
         close c_item_info;

         if RTV_SQL.DETERMINE_RTV_COST(O_error_message,
                                       L_unit_cost_loc,
                                       L_unit_cost_supp,
                                       I_items(i),
                                       I_location,
                                       I_location_type,
                                       I_supplier_site) = FALSE then
             OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
             return 0;
         end if;

         L_seq := L_seq + 1;

         insert into rtv_detail (rtv_order_no,
                                 seq_no,
                                 item,
                                 shipment,
                                 inv_status,
                                 qty_requested,
                                 qty_returned,
                                 qty_cancelled,
                                 unit_cost,
                                 reason,
                                 publish_ind,
                                 restock_pct,
                                 original_unit_cost,
                                 updated_by_rms_ind)
         select O_rtv_order_no,
                L_seq,
                I_items(i),
                null,              --shipment,
                null,              --inv_status,
                ui.unexpected_qty, --qty_requested,
                null,              --qty_returned,
                null,              --qty_cancelled,
                L_unit_cost_supp,
                'U',               --reason,
                'N',               --publish_ind,
                null,              --restock_pct,
                L_unit_cost_supp,
                'Y'
           from rms_oi_unexpected_inv ui
          where ui.session_id     = I_session_id
            and ui.item           = I_items(i)
            and ui.loc            = I_location
            and ui.unexpected_qty > 0;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert unexpected rtv_detail - Item: ' || I_items(i) ||
                                ' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         if L_deposit_item_type = 'E' then
            L_seq := L_seq + 1;
            if RTV_SQL.PROCESS_DEPOSIT_ITEM(O_error_message,
                                            L_seq,
                                            O_rtv_order_no,
                                            I_supplier_site,
                                            L_container_item,
                                            I_items(i),
                                            I_location,
                                            I_location_type,
                                            NULL,
                                            'U',
                                            NULL,
                                            'Y',
                                            'Y') = FALSE then
                OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                return 0;
            end if;
         end if;

         insert into rtv_detail (rtv_order_no,
                                 seq_no,
                                 item,
                                 shipment,
                                 inv_status,
                                 qty_requested,
                                 qty_returned,
                                 qty_cancelled,
                                 unit_cost,
                                 reason,
                                 publish_ind,
                                 restock_pct,
                                 original_unit_cost,
                                 updated_by_rms_ind)
         select O_rtv_order_no,
                L_seq+rownum,
                I_items(i),
                null,              --shipment,
                isq.inv_status,    --inv_status,
                isq.qty,           --qty_requested,
                null,              --qty_returned,
                null,              --qty_cancelled,
                L_unit_cost_supp,
                'U',               --reason,
                'N',               --publish_ind,
                null,              --restock_pct,
                L_unit_cost_supp,
                'Y'
           from rms_oi_unexpected_inv ui,
                inv_status_qty isq
          where ui.session_id       = I_session_id
            and ui.item             = I_items(i)
            and ui.loc              = I_location
            and ui.non_sellable_qty > 0
            and ui.item             = isq.item
            and ui.loc              = isq.location;

         LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                                ' insert non_sellable rtv_detail - Item: ' || I_items(i) ||
                                ' - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         select max(seq_no)
           into L_seq
          from rtv_detail 
         where rtv_order_no = O_rtv_order_no;

         if L_deposit_item_type = 'E' then

            --loop on inv status
            for rec in c_inv_status loop

               L_seq := L_seq + 1;
               if RTV_SQL.PROCESS_DEPOSIT_ITEM(O_error_message,
                                               L_seq,
                                               O_rtv_order_no,
                                               I_supplier_site,
                                               L_container_item,
                                               I_items(i),
                                               I_location,
                                               I_location_type,
                                               rec.qty,
                                               'U',
                                               rec.inv_status,
                                               'Y',
                                               'Y') = FALSE then
                   OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
                   return 0;
               end if;

            end loop;

         end if;

      end loop;

   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END SEED_RTV;
--------------------------------------------------------------------------------
FUNCTION LOCK_RTV_HEAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_status_ind      IN OUT rtv_head.status_ind%TYPE,
                       I_session_id      IN     oi_session_id_log.session_id%TYPE,
                       I_rtv_order_no    IN     rtv_head.rtv_order_no%type)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_ACTION_STKCOUNT.LOCK_RTV_HEAD';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor c_lock is
     select r.status_ind
       from rtv_head r
      where r.rtv_order_no = I_rtv_order_no
        for update nowait;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   open c_lock;
   fetch c_lock into O_status_ind;
   close c_lock;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return 1;

EXCEPTION
   when RECORD_LOCKED then
      if c_lock%isopen then
         close c_lock;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            null,
                                            null,
                                            null);
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      return 0;
END LOCK_RTV_HEAD;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_RTV;
/
