CREATE OR REPLACE PACKAGE BODY RMS_OI_MARGIN_IMPACT AS
------------------------------------------------------------------------------------------------
FUNCTION GET_MARGIN_IMPACT_INFO(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_result          OUT     RMS_OI_MARGIN_IMPACT_TBL,
                                I_item            IN      ITEM_MASTER.ITEM%TYPE,
                                I_supplier        IN      COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                                I_origin_country  IN      COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE,
                                I_location        IN      COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                                I_cost_change     IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_active_date     IN      COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                                I_currency_code   IN      CURRENCIES.CURRENCY_CODE%TYPE)
RETURN NUMBER AS 

   L_program     VARCHAR2(60) := 'RMS_OI_MARGIN_IMPACT.GET_MARGIN_IMPACT_INFO';
   L_mi_month_range     RMS_OI_SYSTEM_OPTIONS.MI_MONTH_RANGE%TYPE;
   L_cc_active_dates    DATE_TBL;
   L_cc_active_date     DATE;
   L_vdate              PERIOD.VDATE%TYPE := GET_VDATE();
   L_margin_impact_tbl  RMS_OI_MARGIN_IMPACT_TBL := RMS_OI_MARGIN_IMPACT_TBL();
   L_margin_impact_rec  RMS_OI_MARGIN_IMPACT_REC;
   L_location           ITEM_LOC.LOC%TYPE;
   L_unit_retail        ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_cost          ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_currency_code      CURRENCIES.CURRENCY_CODE%TYPE;
   L_ranged_ind         VARCHAR2(1) := 'N';

   --Get cost change active dates that fall in the date range
   cursor C_COST_CHG_ACTIVE_DATES is
   select distinct active_date
     from cost_susp_sup_head cch,
          cost_susp_sup_detail ccd
    where cch.cost_change = ccd.cost_change
      and ccd.item = I_item
      and ccd.supplier = NVL(I_supplier, ccd.supplier)
      and ccd.origin_country_id = NVL(I_origin_country, ccd.origin_country_id)
      and cch.active_date >= add_months(I_active_date, (-1)*L_mi_month_range)
      and cch.active_date <= add_months(I_active_date, L_mi_month_range)
    union
   select distinct active_date
     from cost_susp_sup_head cch,
          cost_susp_sup_detail_loc ccdl
    where cch.cost_change = ccdl.cost_change
      and ccdl.item = I_item
      and ccdl.supplier = NVL(I_supplier, ccdl.supplier)
      and ccdl.origin_country_id = NVL(I_origin_country, ccdl.origin_country_id)
      and ccdl.loc = I_location
      and cch.active_date >= add_months(I_active_date, (-1)*L_mi_month_range)
      and cch.active_date <= add_months(I_active_date, L_mi_month_range)
      and I_location is NOT NULL
    union
   select add_months(I_active_date, (-1)*L_mi_month_range) active_date from dual
    union 
   select add_months(I_active_date, L_mi_month_range) active_date from dual
    order by active_date;

   --Only retrieve unit_retail from price_hist for past active dates. unit_cost needs to come from FUTURE_COST. 
   --When I_location is NULL, use the same location retrieved from RPM for future retail.
   --Get the UNIT_RETAIL for the past active dates from PRICE_HIST
   cursor C_GET_PRICE_HIST(c_cc_active_date COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE) is
      with loc_currency as (select store loc,
                                   'S' loc_type,
                                   currency_code
                              from store
                             union all
                            select wh loc,
                                   'W' loc_type,
                                   currency_code
                              from wh
                             union all
                            select to_number(partner_id) loc,
                                   'E' loc_type,
                                   currency_code
                              from partner
                             where partner_type = 'E'),
           --view of price_hist drilled down to transactional level items
           --return the effective unit_retail for an item/loc on the cost change active_date
           prchst as (select v.item,
                             v.loc,
                             v.loc_type,
                             v.action_date,
                             v.unit_retail 
                        from (select ph.item, 
                                     ph.loc, 
                                     ph.loc_type, 
                                     ph.action_date, 
                                     ph.unit_retail,
                                     rank() over (partition by ph.item, ph.loc order by ph.action_date desc) date_rank
                                from price_hist ph,
                                     item_master im
                               where ph.item = im.item
                                 and im.item_level= im.tran_level
                                 and (im.item = I_item
                                  or  im.item_parent = I_item
                                  or  im.item_grandparent = I_item)
                                  --Include the PRICE_HIST record with loc=0, which will be used when L_location is not ranged to the item, 
                                 and (ph.loc = L_location
                                  or  ph.loc = 0 and ph.loc_type is NULL)    
                                 and ph.action_date <= c_cc_active_date) v
                       where v.date_rank = 1)
      select avg(retail)
        from (select --unit_retail is held in location currency when loc is defined
                    rms_oi_margin_impact.get_converted_curr_value(c.currency_code, I_currency_code, ph.unit_retail) retail
               from prchst ph,
                    loc_currency c
              where ph.loc = L_location
                and ph.loc = c.loc
                and ph.loc_type = c.loc_type
              union
             select --unit_retail is held in primary currency for the loc=0 record
                    rms_oi_margin_impact.get_converted_curr_value(s.currency_code, I_currency_code, ph.unit_retail) retail 
               from prchst ph,
                    system_options s
              where ph.loc = 0
                and ph.loc_type is NULL
                and not exists (select 'x'
                                  from prchst ph
                                 where ph.loc = L_location
                                   and rownum = 1));

   --Get the unit cost from FUTURE_COST for both past and future active dates
   --Costs on FUTURE_COST are held in supplier currency, which is the same as FUTURE_COST.CURRENCY_CODE
   cursor C_GET_FUTURE_COST(c_cc_active_date COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE) is
      select RMS_OI_MARGIN_IMPACT_REC(I_item,
                                      I_location,
                                      c_cc_active_date,
                                      cost, 
                                      NULL,
                                      NULL,
                                      I_currency_code)
        from (select avg(get_converted_curr_value(v.currency_code, I_currency_code, v.cost)) cost 
                from (select fc.net_cost cost,
                             fc.currency_code,
                             rank() over (partition by fc.item, fc.location order by fc.active_date desc) date_rank
                        from future_cost fc,
                             item_master im,
                             (select distinct item, 
                                     supplier,
                                     origin_country_id
                                from cost_susp_sup_detail
                               where item = I_item
                                 and supplier = NVL(I_supplier, supplier)
                                 and origin_country_id = NVL(I_origin_country, origin_country_id)
                               union
                              select distinct item, 
                                     supplier,
                                     origin_country_id
                                from cost_susp_sup_detail_loc
                               where item = I_item
                                 and loc = L_location
                                 and supplier = NVL(I_supplier, supplier)
                                 and origin_country_id = NVL(I_origin_country, origin_country_id)) cc
                       where fc.item = im.item
                         and im.item_level = im.tran_level
                         and (im.item = I_item
                          or  im.item_parent = I_item
                          or  im.item_grandparent = I_item)
                         and cc.supplier = fc.supplier
                         and cc.origin_country_id = fc.origin_country_id
                         and fc.location  = L_location
                         and fc.active_date <= c_cc_active_date) v
       where v.date_rank = 1);

   --Get UNIT_COST for the active_date (I_active_date) of the current cost change (I_cost_change)
   --from UNIT_COST on COST_SUSP_SUP_DETAIL_LOC (if I_location is defined) or from UNIT_COST on 
   --COST_SUSP_SUP_DETAIL (if I_location is NOT defined). Cost on these tables are in supplier currency.
   --If UNIT_COST is not found on FUTURE_COST for past or future active_dates, retrieve cost
   --from cost change tables as well.
   cursor C_GET_COST_CHG_COST(c_cc_active_date COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE) is
      select RMS_OI_MARGIN_IMPACT_REC(I_item,
                                      I_location,
                                      c_cc_active_date,
                                      unit_cost,
                                      NULL,
                                      NULL,
                                      I_currency_code)
        from (select avg(unit_cost) unit_cost
                from  (select rms_oi_margin_impact.get_converted_curr_value(currency_code, I_currency_code, ccd.unit_cost) unit_cost
                         from cost_susp_sup_head cch,
                              cost_susp_sup_detail ccd,
                              sups s
                        where I_location is NULL
                          and ccd.item = I_item
                          and ccd.supplier = NVL(I_supplier, ccd.supplier)
                          and ccd.origin_country_id = NVL(I_origin_country, ccd.origin_country_id)
                          and ccd.supplier = s.supplier
                          and cch.cost_change = I_cost_change
                          and cch.cost_change = ccd.cost_change
                          and cch.active_date = c_cc_active_date
                        union
                       select rms_oi_margin_impact.get_converted_curr_value(currency_code, I_currency_code, ccdl.unit_cost) unit_cost
                         from cost_susp_sup_head cch,
                              cost_susp_sup_detail_loc ccdl,
                              sups s
                        where I_location is NOT NULL
                          and ccdl.item = I_item
                          and ccdl.supplier = NVL(I_supplier, ccdl.supplier)
                          and ccdl.origin_country_id = NVL(I_origin_country, ccdl.origin_country_id)
                          and ccdl.supplier = s.supplier
                          and ccdl.loc = I_location
                          and cch.cost_change = I_cost_change
                          and cch.cost_change = ccdl.cost_change
                          and cch.active_date = c_cc_active_date));
                          
    cursor C_GET_COST_FOR_ITEM_NOT_RANGED(c_cc_active_date COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE) is
      select RMS_OI_MARGIN_IMPACT_REC(I_item,
                                      I_location,
                                      c_cc_active_date,
                                      unit_cost,
                                      NULL,
                                      NULL,
                                      I_currency_code)
        from (select avg(unit_cost) unit_cost
                from  (select unit_cost 
                         from (select rms_oi_margin_impact.get_converted_curr_value(currency_code, I_currency_code, ccd.unit_cost) unit_cost,
                                      rank() over (order by active_date desc) date_rank
                                 from cost_susp_sup_head cch,
                                      cost_susp_sup_detail ccd,
                                      sups s
                                where I_location is NULL
                                  and ccd.item = I_item
                                  and ccd.supplier = NVL(I_supplier, ccd.supplier)
                                  and ccd.origin_country_id = NVL(I_origin_country, ccd.origin_country_id)
                                  and ccd.supplier = s.supplier
                                  and cch.cost_change = ccd.cost_change
                                  and cch.active_date <= c_cc_active_date)
                                where date_rank = 1));

   --calculate margin pct based on the markup calc type of the item's department 
   cursor C_MARGIN_PCT is
   select RMS_OI_MARGIN_IMPACT_REC(rslt.item,
                                   rslt.loc,
                                   rslt.active_date,
                                   rslt.unit_cost, 
                                   rslt.unit_retail,
                                   (CASE WHEN rslt.unit_retail = 0 OR rslt.unit_cost = 0 THEN 
                                              0
                                         WHEN d.markup_calc_type = 'C' THEN
                                              ROUND((rslt.unit_retail - rslt.unit_cost) / rslt.unit_cost * 100, 4)
                                          ELSE
                                               ROUND((rslt.unit_retail - rslt.unit_cost) / rslt.unit_retail * 100, 4)
                                    END),
                                    rslt.currency_code)
     from TABLE(CAST(L_margin_impact_tbl AS RMS_OI_MARGIN_IMPACT_TBL)) rslt,
          item_master im,
          deps d
    where im.item = rslt.item
      and im.dept = d.dept
    order by rslt.active_date;
    
    -- cursor to get ranged virtual WH for the given physical_wh
   cursor GET_RANGED_VWH is
      select wh.wh
        from wh, item_loc il
       where physical_wh = I_location
         and item = I_item
         and il.loc = wh.wh
         and rownum = 1;
         
    cursor C_GET_RANGED_LOC is
      select loc
        from item_loc
       where item = I_item
         and rownum = 1;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_cost_change is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_change',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_active_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_active_date',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_currency_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_currency_code',
                                             L_program,
                                             NULL);
      RAISE OI_UTILITY.PROGRAM_ERROR;
   end if;

   if I_location is NOT NULL then
      open GET_RANGED_VWH;
      fetch GET_RANGED_VWH into L_location;
      close GET_RANGED_VWH;
      if L_location is NULL then
         L_location := I_location;
      end if;
   else
      if PM_API_SQL.GET_PRIMARY_ZONE_GROUP_LOC(O_error_message,
                                               L_location,
                                               I_item) = FALSE then
         RAISE OI_UTILITY.PROGRAM_ERROR;
      end if;
   end if;
   
   if L_location is NULL then
      open C_GET_RANGED_LOC;
      fetch C_GET_RANGED_LOC into L_location;
      close C_GET_RANGED_LOC;
   end if;
   if L_location is NOT NULL then
      L_ranged_ind := 'Y';
   end if;

   --Get a list of cost change dates based on I_item (or I_item/I_location if I_location is defined)
   --and the date range. I_item can be either a tran-level item or a parent/grandparent item.
   --These are the dates to be plotted on the line graphs.

   select oi.mi_month_range into L_mi_month_range
     from rms_oi_system_options oi;

   open C_COST_CHG_ACTIVE_DATES;
   fetch C_COST_CHG_ACTIVE_DATES bulk collect into L_cc_active_dates;
   close C_COST_CHG_ACTIVE_DATES;

   --loop through each cost change active_dates to retrieve unit_cost/unit_retail
   --For an active_date in the past, retrieve unit_cost from FUTURE_COST and unit_retail from PRICE_HIST;
   --For an active_date in the future, retrieve unit_cost from FUTURE_COST and unit_retail from RPM.
   --If FUTURE_COST doesn't exist (e.g. data already purged), retrieve unit_cost from the cost change tables.
   --For the active_date (I_active_date) of the current cost change (I_cost_change), retrieve unit_cost from the cost change tables.

   if L_cc_active_dates is not null and  L_cc_active_dates.count > 0 then
      for i in L_cc_active_dates.FIRST .. L_cc_active_dates.LAST loop 
         L_cc_active_date := L_cc_active_dates(i);
         
         --get Unit Cost from FUTURE_COST or cost change tables
         L_margin_impact_rec := NULL;
         if L_cc_active_date != I_active_date then
            if L_ranged_ind = 'Y' then
                open C_GET_FUTURE_COST(L_cc_active_date);
                fetch C_GET_FUTURE_COST into L_margin_impact_rec;
                close C_GET_FUTURE_COST;
            else
                open C_GET_COST_FOR_ITEM_NOT_RANGED(L_cc_active_date);
                fetch C_GET_COST_FOR_ITEM_NOT_RANGED into L_margin_impact_rec;
                close C_GET_COST_FOR_ITEM_NOT_RANGED;
            end if;
         end if;

         if L_margin_impact_rec is NULL then
            open C_GET_COST_CHG_COST(L_cc_active_date);
            fetch C_GET_COST_CHG_COST into L_margin_impact_rec;
            close C_GET_COST_CHG_COST;
         end if;

         --get Unit Retail from PRICE_HIST or RPM's future retail tables
         if L_cc_active_date <= L_vdate then
            open C_GET_PRICE_HIST(L_cc_active_date);
            fetch C_GET_PRICE_HIST into L_unit_retail;
            close C_GET_PRICE_HIST;

            L_margin_impact_rec.unit_retail := L_unit_retail;                                           
         else
            if L_ranged_ind = 'Y' then
               if PM_API_SQL.GET_ITEMLOC_RETAIL(O_error_message,
                                                L_unit_retail,
                                                L_currency_code,
                                                I_item,
                                                L_location,
                                                L_cc_active_date) = FALSE then
                   RAISE OI_UTILITY.PROGRAM_ERROR;
               end if;
               L_margin_impact_rec.unit_retail := get_converted_curr_value(L_currency_code, I_currency_code, L_unit_retail);                                           
            else
               open C_GET_PRICE_HIST(L_cc_active_date);
               fetch C_GET_PRICE_HIST into L_unit_retail;
               close C_GET_PRICE_HIST;
               L_margin_impact_rec.unit_retail := L_unit_retail;                                           
            end if;            
         end if;

         L_margin_impact_tbl.extend;
         L_margin_impact_tbl(L_margin_impact_tbl.count) := L_margin_impact_rec;
      end loop;

      open C_MARGIN_PCT;
      fetch C_MARGIN_PCT bulk collect into O_result;
      close C_MARGIN_PCT;
   end if;

   RETURN OI_UTILITY.SUCCESS;
EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END GET_MARGIN_IMPACT_INFO;
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: GET_CONVERTED_CURR_VALUE
-- Purpose      : This function converts an input cost/retail value in I_from_currency to 
--                I_to_currency and returns the converted value.
------------------------------------------------------------------------------------------------
FUNCTION GET_CONVERTED_CURR_VALUE(I_from_currency   IN   CURRENCIES.CURRENCY_CODE%TYPE,
                                  I_to_currency     IN   CURRENCIES.CURRENCY_CODE%TYPE,
                                  I_input_value     IN   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                  I_effective_date  IN   PERIOD.VDATE%TYPE DEFAULT NULL,
                                  I_functional_area IN   VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_return_value    NUMBER := null;
   L_effective_date  PERIOD.VDATE%TYPE;
   L_exchange_rate   mv_currency_conversion_rates.exchange_rate%TYPE;
   L_exchange_type   mv_currency_conversion_rates.exchange_type%TYPE;

BEGIN
   --Determine the exchange_type based on the functional area currency exchange is being performed.
   --Functional area exchange type takes precedence over 'C'(consoliated) and 'O'(operational)
   --exchanges types when fetching the exchange rate. Supported exchange types in addition to 
   --'C' and 'O' are defined in code_detail table under code_type 'EXTP'.

   if I_functional_area = 'LC' then
      L_exchange_type := 'L';
   elsif I_functional_area = 'PURCHASE' then
      L_exchange_type := 'P';
   elsif I_functional_area = 'CUSTOMS' then
      L_exchange_type := 'U';
   elsif I_functional_area = 'TRANSPORTATION' then
      L_exchange_type := 'T';        
   else
      L_exchange_type := null;
   end if;

   if I_effective_date is NULL then
      L_effective_date := GET_VDATE();
   else
      L_effective_date := I_effective_date;
   end if;

   if I_input_value is not null and I_from_currency is not null and I_to_currency is not null then

      select mv.exchange_rate into L_exchange_rate
        from mv_currency_conversion_rates mv,
             system_config_options so
       where from_currency = I_from_currency
         and to_currency   = I_to_currency
         and effective_date <= L_effective_date
         and ((so.consolidation_ind = 'Y' and mv.exchange_type in (L_exchange_type, 'C', 'O'))
          or  (so.consolidation_ind = 'N' and mv.exchange_type in (L_exchange_type, 'O')))
         and rownum = 1
       order by decode(mv.exchange_type, L_exchange_type, 1, 'C', 2, 'O', 3) asc,
                       mv.effective_date desc;

      if L_exchange_rate is not null then
          L_return_value := I_input_value * L_exchange_rate;
      end if;
   end if;

   return L_return_value;
EXCEPTION
   when OTHERS then
      RETURN OI_UTILITY.FAILURE;
END GET_CONVERTED_CURR_VALUE;
------------------------------------------------------------------------------------------------
END RMS_OI_MARGIN_IMPACT;
/
