CREATE OR REPLACE PACKAGE RMS_OI_ACTION_PO AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : UPDATE_PO_STATUS
--Purpose       : This function will take a list of order numbers
--                if the new status is 'A' it will check for validation and approve
--                the order no. If the new status is 'W' it will bring back the
--                order to work sheet status after checking validations and the
--                function will return sucess full proceed orders as well as rejected
--                orders. If the 
--------------------------------------------------------------------------------
FUNCTION APPROVE_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           OUT RMS_OI_PO_STATUS_REC,
                    I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                    I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REJECT_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT RMS_OI_PO_STATUS_REC,
                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                   I_comments      IN     ORDHEAD.COMMENT_DESC%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CANCEL_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT RMS_OI_PO_STATUS_REC,
                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                   I_reason_code   IN     CODE_DETAIL.CODE%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : CANCEL_PO_ITEM
--Purpose       : This function cancel items on approved PO.
--------------------------------------------------------------------------------
FUNCTION CANCEL_PO_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result           OUT RMS_OI_PO_ITEM_LOC_STATUS_REC,
                        I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                        I_reason_code   IN     CODE_DETAIL.CODE%TYPE,
                        I_order         IN     ORDHEAD.ORDER_NO%TYPE,
                        I_item          IN     ITEM_MASTER.ITEM%TYPE,
                        I_agg_diff_1    IN     ITEM_MASTER.DIFF_1%TYPE,
                        I_agg_diff_2    IN     ITEM_MASTER.DIFF_2%TYPE,
                        I_agg_diff_3    IN     ITEM_MASTER.DIFF_3%TYPE,
                        I_agg_diff_4    IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT RMS_OI_PO_STATUS_REC,
                   I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : UPDATE_PO_DATES
--Purpose       : This function will take a list of order numbers and update
--                the orders' dates.
------------------------------------------------------------------------------------
FUNCTION UPDATE_PO_DATES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result             OUT RMS_OI_PO_STATUS_REC,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_not_before_date IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                         I_not_after_date  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                         I_otb_eow_date    IN     ORDHEAD.OTB_EOW_DATE%TYPE,
                         I_orders          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : ADD_ORDER_REF_ITEM
--Purpose       : This function will take a order numbers and an item.
--                It will add the primary refereance item of the item to the order.
--------------------------------------------------------------------------------
FUNCTION ADD_ORDER_REF_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result               OUT RMS_OI_PO_ITEM_STATUS_REC,
                            I_session_id        IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_order_item_tbl    IN     RMS_OI_ORDER_ITEM_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : VALIDATE_ITEMS_DEPT
--Purpose       : When system option is set to dept level PO then validate all 
--                items have the same dept and return dept id otherwise return success
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMS_DEPT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_dept             OUT ORDHEAD.DEPT%TYPE,
                             I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                             I_items         IN     RMS_OI_ITEM_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : VALIDATE_ITEMS_SUPPLIER
--Purpose       : This function takes a list of item and send back a common supplier
--                and if supplier is sent in then will validate the supplier is 
--                valid for all the items
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMS_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier      IN OUT ORDHEAD.SUPPLIER%TYPE,
                                 I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_items         IN     RMS_OI_ITEM_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : CREATE_ORDER
--Purpose       : This function for creating the PO and returns order number 
--------------------------------------------------------------------------------
FUNCTION CREATE_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_order_no         OUT ORDHEAD.ORDER_NO%TYPE,
                      I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_po_rec        IN     RMS_OI_CREATE_PO_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : UPDATE_ORDER_COMMENT
--Purpose       : This function for updating the PO comments
--------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COMMENT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_session_id    IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                              I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                              I_comment       IN     ORDHEAD.COMMENT_DESC%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END RMS_OI_ACTION_PO;
/
