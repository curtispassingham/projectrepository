CREATE OR REPLACE PACKAGE RMS_OI_INV_ANALYST AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------

ORDER_ITEM_REF_ITEM_ERROR    CONSTANT VARCHAR(30) := 'ORDER_ITEM_REF_ITEM_ERROR';
ORDER_ITEM_HTS_ERROR         CONSTANT VARCHAR(30) := 'ORDER_ITEM_HTS_ERROR';
ORDER_ITEM_MARGIN_ERROR      CONSTANT VARCHAR(30) := 'ORDER_ITEM_MARGIN_ERROR';

--------------------------------------------------------------------------------
FUNCTION STORE_INVENTORY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result                  IN OUT RMS_OI_STORE_INV_TBL,
                         I_session_id              IN     oi_session_id_log.session_id%TYPE,
                         I_item                    IN     item_master.item%TYPE,
                         I_item_type               IN     item_master.alc_item_type%TYPE,
                         I_agg_diff_1              IN     item_master.diff_1%TYPE,
                         I_agg_diff_2              IN     item_master.diff_2%TYPE,
                         I_agg_diff_3              IN     item_master.diff_3%TYPE,
                         I_agg_diff_4              IN     item_master.diff_4%TYPE,
                         I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                         I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                         I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION WH_INVENTORY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_result                  IN OUT RMS_OI_WH_INV_TBL,
                      I_session_id              IN     oi_session_id_log.session_id%TYPE,
                      I_item                    IN     item_master.item%TYPE,
                      I_item_type               IN     item_master.alc_item_type%TYPE,
                      I_agg_diff_1              IN     item_master.diff_1%TYPE,
                      I_agg_diff_2              IN     item_master.diff_2%TYPE,
                      I_agg_diff_3              IN     item_master.diff_3%TYPE,
                      I_agg_diff_4              IN     item_master.diff_4%TYPE,
                      I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                      I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                      I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION OPEN_ORDERS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_session_id              IN     oi_session_id_log.session_id%TYPE,
                     I_item                    IN     item_master.item%TYPE,
                     I_item_type               IN     item_master.alc_item_type%TYPE,
                     I_agg_diff_1              IN     item_master.diff_1%TYPE,
                     I_agg_diff_2              IN     item_master.diff_2%TYPE,
                     I_agg_diff_3              IN     item_master.diff_3%TYPE,
                     I_agg_diff_4              IN     item_master.diff_4%TYPE,
                     I_supplier_sites          IN     OBJ_NUMERIC_ID_TABLE,
                     I_order_types             IN     OBJ_VARCHAR_ID_TABLE,
                     I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                     I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                     I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION LEAD_TIMES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result                  IN OUT RMS_OI_LEAD_TIME_TBL,
                    I_session_id              IN     oi_session_id_log.session_id%TYPE,
                    I_item                    IN     item_master.item%TYPE,
                    I_item_type               IN     item_master.alc_item_type%TYPE,
                    I_agg_diff_1              IN     item_master.diff_1%TYPE,
                    I_agg_diff_2              IN     item_master.diff_2%TYPE,
                    I_agg_diff_3              IN     item_master.diff_3%TYPE,
                    I_agg_diff_4              IN     item_master.diff_4%TYPE,
                    I_areas                   IN     OBJ_NUMERIC_ID_TABLE,
                    I_stores                  IN     OBJ_NUMERIC_ID_TABLE,
                    I_store_grade_group_id    IN     store_grade_group.store_grade_group_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DEFER_INV_VAR_FORECAST_ITEM(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id              IN     oi_session_id_log.session_id%TYPE,
                                     I_item                    IN     item_master.item%TYPE,
                                     I_item_type               IN     item_master.alc_item_type%TYPE,
                                     I_agg_diff_1              IN     item_master.diff_1%TYPE,
                                     I_agg_diff_2              IN     item_master.diff_2%TYPE,
                                     I_agg_diff_3              IN     item_master.diff_3%TYPE,
                                     I_agg_diff_4              IN     item_master.diff_4%TYPE,
                                     I_defer_to_date           IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION INV_VAR_FORECAST(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_session_id             IN     oi_session_id_log.session_id%TYPE,
                          I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                          I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                          I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                          I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                          I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                          I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_orders_past_nad             IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_orders_to_close             IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_not_approved_orders         IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_once_approved_orders        IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      O_missing_order_data          IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                      I_session_id                  IN     oi_session_id_log.session_id%TYPE,
                      I_depts                       IN     OBJ_NUMERIC_ID_TABLE,
                      I_classes                     IN     OBJ_NUMERIC_ID_TABLE,
                      I_subclasses                  IN     OBJ_NUMERIC_ID_TABLE,
                      I_supplier_sites              IN     OBJ_NUMERIC_ID_TABLE,
                      I_areas                       IN     OBJ_NUMERIC_ID_TABLE,
                      I_stores                      IN     OBJ_NUMERIC_ID_TABLE,
                      I_store_grade_group_id        IN     store_grade_group.store_grade_group_id%TYPE,
                      I_show_ord_err_past_nad       IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_ord_to_close   IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_never_apprv    IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_once_apprv     IN     VARCHAR2,   --'Y' or 'N'
                      I_show_ord_err_miss_ord_data  IN     VARCHAR2,   --'Y' or 'N'
                      I_brands                      IN     OBJ_VARCHAR_ID_TABLE default NULL,
                      I_order_types                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS_ITEM(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_missing_item_data      IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                           I_session_id             IN     oi_session_id_log.session_id%TYPE,
                           I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                           I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                           I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                           I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                           I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                           I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                           I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                           I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL,
                           I_order_types            IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS_DETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_session_id         IN     oi_session_id_log.session_id%TYPE,
                             I_orders             IN     OBJ_NUMERIC_ID_TABLE,
                             I_missing_order_data IN     VARCHAR2)   --'Y' or 'N'
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ORDER_ERRORS_ITEM_DETAIL(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                  I_orders                 IN     OBJ_NUMERIC_ID_TABLE,
                                  --
                                  I_depts                  IN     OBJ_NUMERIC_ID_TABLE default NULL,
                                  I_classes                IN     OBJ_NUMERIC_ID_TABLE default NULL,
                                  I_subclasses             IN     OBJ_NUMERIC_ID_TABLE default NULL,
                                  I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE default NULL,
                                  I_areas                  IN     OBJ_NUMERIC_ID_TABLE default NULL,
                                  I_stores                 IN     OBJ_NUMERIC_ID_TABLE default NULL,
                                  I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE default NULL,
                                  I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_VARIANCE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OPEN_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ORD_ERRS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_ORD_ITEM_ERRS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_INV_VAR_FORECAST(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                  I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                  I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                                  I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                                  I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                                  I_items                  IN     OBJ_VARCHAR_ID_TABLE,
                                  I_item_type              IN     item_master.alc_item_type%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ORD_PAST_NAD_ERR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_orders_past_nad       IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                  I_session_id            IN     oi_session_id_log.session_id%TYPE,
                                  I_order_ids             IN     OBJ_NUMERIC_ID_TABLE,
                                  I_order_ids_to_delete   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ORD_TO_CLOSE_ERR(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_orders_to_close        IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                  I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                  I_order_ids              IN     OBJ_NUMERIC_ID_TABLE,
                                  I_order_ids_to_delete    IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_NEVER_APPRV_ORD_ERR(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_not_approved_orders    IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                     I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                     I_order_ids              IN     OBJ_NUMERIC_ID_TABLE,
                                     I_order_ids_to_delete    IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_ONCE_APPRV_ORD_ERR(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_once_approved_orders    IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                    I_session_id              IN     oi_session_id_log.session_id%TYPE,
                                    I_order_ids               IN     OBJ_NUMERIC_ID_TABLE,
                                    I_order_ids_to_delete     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_MISS_ORD_DATA_ERR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_missing_order_data    IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                   I_session_id            IN     oi_session_id_log.session_id%TYPE,
                                   I_order_ids             IN     OBJ_NUMERIC_ID_TABLE,
                                   I_order_ids_to_delete   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_MISS_ITM_DATA_ERR(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_missing_item_data        IN OUT RMS_OI_ORDER_ERROR_TILE_REC,
                                   I_session_id               IN     oi_session_id_log.session_id%TYPE,
                                   I_order_ids                IN     OBJ_NUMERIC_ID_TABLE,
                                   I_ord_itm_ids_to_delete    IN     RMS_OI_ORDER_ITEM_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_INV_ANALYST;
/
