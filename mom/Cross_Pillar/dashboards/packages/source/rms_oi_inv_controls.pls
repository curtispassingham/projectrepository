CREATE OR REPLACE PACKAGE RMS_OI_INV_CONTROL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------

NEG_INV              CONSTANT VARCHAR(30) := 'NEG_INV';
NEG_INV_WARN         CONSTANT VARCHAR(30) := 'NEG_INV_WARN';
NEG_INV_CRITIAL      CONSTANT VARCHAR(30) := 'NEG_INV_CRITIAL';

GREEN                CONSTANT VARCHAR(30) := 'GREEN';
YELLOW               CONSTANT VARCHAR(30) := 'YELLOW';
RED                  CONSTANT VARCHAR(30) := 'RED';

--------------------------------------------------------------------------------
FUNCTION NEGATIVE_INVENTORY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_item_loc_count         IN OUT NUMBER,
                            O_report_level           IN OUT VARCHAR2,
                            I_session_id             IN     oi_session_id_log.session_id%TYPE,
                            I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                            I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                            I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                            I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                            I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                            I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                            I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION NEGATIVE_INVENTORY_DETAIL(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                   I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                   I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                                   I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                   I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_stores                 IN     OBJ_NUMERIC_ID_TABLE,
                                   I_store_grade_group_id   IN     store_grade_group.store_grade_group_id%TYPE,
                                   I_brands                 IN     OBJ_VARCHAR_ID_TABLE default NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION OVERDUE_SHIPMENT_TSF(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_transfer_count         IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION OVERDUE_SHIPMENT_ALLOC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_alloc_count            IN OUT NUMBER,
                                O_report_level           IN OUT VARCHAR2,
                                I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                                I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                                I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                                I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION OVERDUE_SHIPMENT_RTV(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rtv_count              IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION UNEXPECTED_INVENTORY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_loc_count         IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION TSF_PENDING_APPROVE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_transfer_count         IN OUT NUMBER,
                             O_report_level           IN OUT VARCHAR2,
                             I_session_id             IN     oi_session_id_log.session_id%TYPE,
                             I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                             I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                             I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                             I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                             I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_locations              IN     OBJ_NUMERIC_ID_TABLE,  
                             I_view_intracompany_priv IN     VARCHAR2,   --'Y' or 'N'
                             I_view_intercompany_priv IN     VARCHAR2)   --'Y' or 'N'

RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION STKORD_PENDING_CLOSE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stkord_count           IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                              I_view_intercompany_priv IN     VARCHAR2)  --'Y' or 'N'
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_MISSING(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_stock_cnt_count        IN OUT NUMBER,
                             O_report_level           IN OUT VARCHAR2,
                             I_session_id             IN     oi_session_id_log.session_id%TYPE,
                             I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                             I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                             I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                             I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                             I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stock_cnt_count        IN OUT NUMBER,
                              O_report_level           IN OUT VARCHAR2,
                              I_session_id             IN     oi_session_id_log.session_id%TYPE,
                              I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                              I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                              I_chains                 IN     OBJ_NUMERIC_ID_TABLE,
                              I_areas                  IN     OBJ_NUMERIC_ID_TABLE,
                              I_locations              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_TSF_PENDING_APPROVE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_transfer_count         IN OUT NUMBER,
                                     O_report_level           IN OUT VARCHAR2,
                                     I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                     I_tsf_nos                IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_OVERDUE_SHIPMENT_TSF(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_transfer_count         IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_tsf_nos                IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_OVERDUE_SHIPMENT_RTV(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_rtv_count              IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_rtv_order_nos          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_STOCK_COUNT_MISSING(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_stock_cnt_count        IN OUT NUMBER,
                                     O_report_level           IN OUT VARCHAR2,
                                     I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                     I_cycle_cnt_locs         IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_STOCK_COUNT_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_stock_cnt_count        IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_cycle_cnt_locs         IN     RMS_OI_CYCLE_COUNT_LOC_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_UNEXPECTED_INVENTORY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_item_loc_count         IN OUT NUMBER,
                                      O_report_level           IN OUT VARCHAR2,
                                      I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                      I_itemlocs               IN     OBJ_ITEMLOC_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_NEG_INV(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OVERDUE_TSF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OVERDUE_ALLOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_OVERDUE_RTV(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_UNEXPECTED_INV(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_TSF_PENDING_APP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STKORD_PENDING(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STKCNT_MISSING(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STKCNT_VARIANCE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_INV_CONTROL;
/
