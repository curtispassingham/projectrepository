CREATE OR REPLACE PACKAGE RESA_OI_ERROR_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name : TRAN_ERROR
--Purpose       : This function retrieves the transaction error count for the past
--                7 days for assigned stores.
--                To be used by the following reports:
--                OPEN TRANSACTION ERROR (DASHBOARD)
--                OPEN TRANSACTION ERROR (CONTEXTUAL)
--------------------------------------------------------------------------------
FUNCTION TRAN_ERROR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result        IN OUT RESA_OI_ERROR_TRAN_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : ERROR_HIST
--Purpose       : This function retrieves the error count for
--                the give store day and/or transaction.
--                To be used by the following reports:
--                ERROR HISTORY (CONTEXTUAL STORE DAY SEARCH)
--                ERROR HISTORY (CONTEXTUAL STORE DAY SUMMARY)
--                ERROR HISTORY (CONTEXTUAL TRANSACTION MAINTENANCE)
--------------------------------------------------------------------------------
FUNCTION ERROR_HIST(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           IN OUT RESA_OI_ERROR_HIST_TBL,
                    I_store            IN     SA_STORE_DAY.STORE%TYPE,
                    I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : ITEM_ERROR
--Purpose       : This function retrieves the error count for
--                the give store day and/or transaction.
--                To be used by the following report:
--                ITEM ERROR (CONTEXTUAL TRANSACTION MAINTENANCE)
--------------------------------------------------------------------------------
FUNCTION ITEM_ERROR(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           IN OUT RESA_OI_ERROR_HIST_TBL,
                    I_store            IN     SA_STORE_DAY.STORE%TYPE,
                    I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                    I_item_seq_no      IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RESA_OI_ERROR_SQL;
/