CREATE OR REPLACE PACKAGE BODY RESA_OI_ERROR_SQL AS
--------------------------------------------------------------------------------
FUNCTION TRAN_ERROR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result        IN OUT RESA_OI_ERROR_TRAN_TBL)
RETURN NUMBER IS
   L_program      VARCHAR2(61) := 'RESA_OI_ERROR_SQL.TRAN_ERROR';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   
   L_vdate        PERIOD.VDATE%TYPE := GET_VDATE();
   
   cursor c_get_tran_error is
      with v_sa_store_day as (select /*+ INDEX(ssd SA_STORE_DAY_I1)*/business_date,
                                     store_day_seq_no,
                                     store,
                                     day
                                from sa_store_day ssd
                               where business_date between (L_vdate-7) and (L_vdate-1)
                                 and exists (select 'x'
                                               from sa_user_loc_traits slt,
                                                    loc_traits_matrix ltm
                                              where slt.loc_trait = ltm.loc_trait
                                                and slt.user_id = get_user()
                                                and ssd.store = ltm.store
                                                and rownum = 1)),
             v_date_range as (select (L_vdate-7) + level -1 business_date
                                from dual
                             connect by level  <= 7),
                       v1 as (select  ssd.business_date,
                                      ssd.store_day_seq_no,
                                      sae.tran_seq_no,
                                      sae.error_seq_no,
                                      NVL(sac.short_desc, sac.error_code) short_desc,
                                      count(sae.error_code) over (partition by ssd.business_date, sae.error_code) error_count,
                                      count(sae.error_code) over (partition by ssd.business_date) total_errors,
                                      sae.rec_type,
                                      ssd.store,
                                      (select tran_no from sa_tran_head where store_day_seq_no = ssd.store_day_seq_no and tran_seq_no = sae.tran_seq_no) tran_no,
                                      (select  item from sa_tran_item where store = ssd.store and day = ssd.day and tran_seq_no = sae.tran_seq_no and item_seq_no = key_value_1) item
                                from  v_sa_store_day ssd,
                                      sa_error sae,
                                      sa_error_codes sac
                                where ssd.store_day_seq_no = sae.store_day_seq_no
                                  and sae.error_code = sac.error_code)
      select RESA_OI_ERROR_TRAN_REC (
                vdr.business_date,
                store_day_seq_no,
                error_seq_no,
                error_count,
                total_errors,
                short_desc,
                tran_seq_no,
                rec_type,
                store,
                (select store_name from v_store where store = v1.store),
                item,
                (select item_desc from v_item_master where item = v1.item),
                tran_no
             )
        from v1, 
             v_date_range vdr
       where vdr.business_date = v1.business_date (+)
       order by vdr.business_date desc;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' Start');
   
   open c_get_tran_error;

   fetch c_get_tran_error bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_tran_error - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_tran_error;   
 
   OI_UTILITY.LOG_TIME(L_program,L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END TRAN_ERROR;
--------------------------------------------------------------------------------
FUNCTION ERROR_HIST(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           IN OUT RESA_OI_ERROR_HIST_TBL,
                    I_store            IN     SA_STORE_DAY.STORE%TYPE,
                    I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE)
RETURN NUMBER IS
   L_program        VARCHAR2(61) := 'RESA_OI_ERROR_SQL.ERROR_HIST';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;
   
   L_vdate        PERIOD.VDATE%TYPE := GET_VDATE();
  
   cursor c_get_error_hist is
     with v_sa_store_day as (select /*+ INDEX(ssd SA_STORE_DAY_I1)*/business_date,
                                     store_day_seq_no,
                                     store,
                                     day
                                from sa_store_day ssd
                                where business_date between (L_vdate-7) and (L_vdate-1)
                                  and ssd.store = NVL(I_store,ssd.store)
                                  and exists (select 'x'
                                                from sa_user_loc_traits slt,
                                                     loc_traits_matrix ltm
                                               where slt.loc_trait = ltm.loc_trait
                                                 and slt.user_id = get_user()
                                                 and ssd.store = ltm.store
                                                 and rownum = 1)),
               v_sa_error as (select sae.tran_seq_no,
                                     sae.error_seq_no,
                                     sae.error_code,
                                     sae.rec_type,
                                     sae.store_day_seq_no,
                                     vsd.business_date,
                                     1 rev_no,
                                     sae.key_value_1
                                from sa_error sae,
                                     v_sa_store_day vsd
                               where sae.store_day_seq_no = vsd.store_day_seq_no
                                 and exists (select 'x' 
                                              from sa_error sae2 
                                              where (sae2.tran_seq_no = NVL(I_tran_seq_no,sae2.tran_seq_no) or 
                                                     sae2.tran_seq_no is NULL) 
                                                and sae.error_code = sae2.error_code
                                                and rownum = 1)
                               union all
                              select sar.tran_seq_no,
                                     sar.error_seq_no,
                                     sar.error_code,
                                     sar.rec_type,
                                     sar.store_day_seq_no,
                                     vsd.business_date,
                                     max(sar.rev_no) rev_no,
                                     sar.key_value_1
                                 from sa_error_rev sar,
                                      v_sa_store_day vsd
                                where sar.store_day_seq_no = vsd.store_day_seq_no 
                                  and exists (select 'x' 
                                              from sa_error_rev sar2
                                              where (sar2.tran_seq_no = NVL(I_tran_seq_no,sar2.tran_seq_no) or 
                                                     sar2.tran_seq_no is NULL) 
                                                and sar.error_code = sar2.error_code
                                                and rownum = 1)
                                  and not exists (select 'x'
                                                    from sa_error sae2
                                                   where sae2.error_seq_no = sar.error_seq_no
                                                     and rownum = 1)
                               group by sar.tran_seq_no,
                                        sar.error_seq_no,
                                        sar.error_code,
                                        sar.rec_type,
                                        sar.store_day_seq_no,
                                        sar.key_value_1,
                                        vsd.business_date),
             v_date_range as (select (L_vdate-7) + level -1 business_date
                                from dual
                             connect by level <= 7),
                       v1 as (select sae.business_date,
                                     sae.error_code,
                                     count(sae.error_code) error_count
                              from  v_sa_error sae
                              group by sae.business_date,
                                       sae.error_code)
      select RESA_OI_ERROR_HIST_REC (
                vdr.business_date,
                null,
                null,
                error_count,
                sum(error_count) over (partition by vdr.business_date),
                (select NVL(short_desc, error_code) from sa_error_codes sec where v1.error_code = sec.error_code),
                null,
                null,
                null,
                null,
                null
             )
        from v1, 
             v_date_range vdr
       where vdr.business_date = v1.business_date (+)
       order by vdr.business_date desc;

BEGIN
   LOGGER.LOG_INFORMATION(L_program||' Start');

   open c_get_error_hist;

   fetch c_get_error_hist bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_error_hist - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_error_hist;   
 
   OI_UTILITY.LOG_TIME(L_program,L_start_time);

   return OI_UTILITY.SUCCESS;
   
   OI_UTILITY.LOG_TIME(L_program,L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END ERROR_HIST;
--------------------------------------------------------------------------------
FUNCTION ITEM_ERROR(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           IN OUT RESA_OI_ERROR_HIST_TBL,
                    I_store            IN     SA_STORE_DAY.STORE%TYPE,
                    I_tran_seq_no      IN     SA_TRAN_HEAD.TRAN_SEQ_NO%TYPE,
                    I_item_seq_no      IN     SA_TRAN_ITEM.ITEM_SEQ_NO%TYPE)
RETURN NUMBER IS
   L_program        VARCHAR2(61) := 'RESA_OI_ERROR_SQL.ITEM_ERROR';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;
   
   L_vdate          PERIOD.VDATE%TYPE  := GET_VDATE();

   cursor c_get_item_error is
      with v_sa_store_day as (select /*+ INDEX(ssd SA_STORE_DAY_I1)*/business_date,
                                     store_day_seq_no,
                                     store,
                                     day
                                from sa_store_day ssd
                                where business_date between (L_vdate-7) and (L_vdate-1)
                                  and ssd.store = NVL(I_store,ssd.store)
                                  and exists (select 'x'
                                                from sa_user_loc_traits slt,
                                                     loc_traits_matrix ltm
                                               where slt.loc_trait = ltm.loc_trait
                                                 and slt.user_id = get_user()
                                                 and ssd.store = ltm.store
                                                 and rownum = 1)
                              ),
           v_sa_error as (select sae.tran_seq_no,
                                 sae.error_seq_no,
                                 sae.error_code,
                                 sae.rec_type,
                                 sae.store_day_seq_no,
                                 vsd.business_date,
                                 1 rev_no,
                                 sae.key_value_1
                            from sa_error sae,
                                 v_sa_store_day vsd
                           where sae.rec_type in ('TITEM','IDISC','IGTAX')
                             and sae.store_day_seq_no = vsd.store_day_seq_no
                             and sae.key_value_1 = NVL(I_item_seq_no,sae.key_value_1)
                             and exists (select 'x' 
                                           from sa_error sae2 
                                          where sae2.key_value_1 = NVL(I_item_seq_no,sae2.key_value_1)
                                            and (sae2.tran_seq_no = NVL(I_tran_seq_no, sae2.tran_seq_no) or 
                                                 sae2.tran_seq_no is NULL)
                                            and sae.error_code = sae2.error_code
                                            and rownum = 1)
                           union all
                          select sar.tran_seq_no,
                                 sar.error_seq_no,
                                 sar.error_code,
                                 sar.rec_type,
                                 sar.store_day_seq_no,
                                 vsd.business_date,
                                 max(sar.rev_no) rev_no,
                                 sar.key_value_1
                            from sa_error_rev sar,
                                 v_sa_store_day vsd
                           where sar.rec_type in ('TITEM','IDISC','IGTAX')
                             and sar.store_day_seq_no = vsd.store_day_seq_no
                             and sar.key_value_1 = NVL(I_item_seq_no,sar.key_value_1)
                             and exists (select 'x' 
                                           from sa_error_rev sar2 
                                          where sar2.key_value_1 = NVL(I_item_seq_no,sar2.key_value_1)
                                            and (sar2.tran_seq_no = NVL(I_tran_seq_no, sar2.tran_seq_no) or 
                                                 sar2.tran_seq_no is NULL)
                                            and sar.error_code = sar2.error_code
                                            and rownum = 1)
                             and not exists (select 'x'
                                               from sa_error sae2
                                              where sae2.error_seq_no = sar.error_seq_no)
                        group by sar.tran_seq_no,
                                 sar.error_seq_no,
                                 sar.error_code,
                                 sar.rec_type,
                                 sar.store_day_seq_no,
                                 sar.key_value_1,
                                 vsd.business_date),
           v_date_range as (select (L_vdate-7) + level -1 business_date
                              from dual
                           connect by level  <= 7),
                     v1 as (select sae.business_date,
                                   sae.error_code,
                                   count(sae.error_code) error_count
                             from  v_sa_error sae
                          group by sae.business_date,
                                   sae.error_code)
   select RESA_OI_ERROR_HIST_REC(
                vdr.business_date,
                null,
                null,
                error_count,
                sum(error_count) over (partition by vdr.business_date),
                (select NVL(short_desc, error_code) from sa_error_codes sec where v1.error_code = sec.error_code),
                null,
                null,
                null,
                null,
                null
        )
        from v1, 
             v_date_range vdr
       where vdr.business_date = v1.business_date (+)
       order by vdr.business_date desc;
   
BEGIN

   open c_get_item_error;

   fetch c_get_item_error bulk collect into O_result;
   LOGGER.LOG_INFORMATION(L_program||' cursor c_get_item_error - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   close c_get_item_error;   
 
   OI_UTILITY.LOG_TIME(L_program,L_start_time);

   return OI_UTILITY.SUCCESS;
   
   OI_UTILITY.LOG_TIME(L_program,L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END ITEM_ERROR;
--------------------------------------------------------------------------------
END RESA_OI_ERROR_SQL;
/