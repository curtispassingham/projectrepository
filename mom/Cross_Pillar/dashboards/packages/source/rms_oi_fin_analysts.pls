CREATE OR REPLACE PACKAGE RMS_OI_FIN_ANALYST AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
GREEN                CONSTANT VARCHAR(30) := 'GREEN';
YELLOW               CONSTANT VARCHAR(30) := 'YELLOW';
RED                  CONSTANT VARCHAR(30) := 'RED';
--------------------------------------------------------------------------------
FUNCTION WAC_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_itemloc_count          IN OUT NUMBER,
                      O_neg_wac_count          IN OUT NUMBER,
                      O_report_level           IN OUT VARCHAR2,
                      I_session_id             IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                      I_set_of_books_ids       IN     OBJ_NUMERIC_ID_TABLE,
                      I_org_unit_ids           IN     OBJ_NUMERIC_ID_TABLE,
                      I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                      I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                      I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                      I_subclasses             IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_CHAIN_AVERAGE_COST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_chain_average_cost    OUT NUMBER,
                                I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                I_item               IN     ITEM_LOC_SOH.ITEM%TYPE,
                                I_location           IN     ITEM_LOC_SOH.LOC%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CUMULATIVE_MARKON_PCT_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_cum_markon_count       IN OUT NUMBER,
                                        O_neg_cum_markon_count   IN OUT NUMBER,
                                        O_report_level           IN OUT VARCHAR2,
                                        I_session_id             IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                        I_set_of_books_ids       IN     OBJ_NUMERIC_ID_TABLE,
                                        I_org_unit_ids           IN     OBJ_NUMERIC_ID_TABLE,
                                        I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                                        I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                        I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                        I_subclasses             IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION STOCK_COUNT_VALUE_VARIANCE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_stk_cnt_value_var_count IN OUT NUMBER,
                                    O_report_level            IN OUT VARCHAR2,
                                    I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                    I_set_of_books_ids        IN     OBJ_NUMERIC_ID_TABLE,
                                    I_org_unit_ids            IN     OBJ_NUMERIC_ID_TABLE,
                                    I_locations               IN     OBJ_NUMERIC_ID_TABLE,
                                    I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                                    I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                                    I_subclasses              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SHRINKAGE_VARIANCE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_shrinkage_var_count     IN OUT NUMBER,
                            O_neg_shrinkage_var_count IN OUT NUMBER,
                            O_report_level            IN OUT VARCHAR2,
                            I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                            I_set_of_books_ids        IN     OBJ_NUMERIC_ID_TABLE,
                            I_org_unit_ids            IN     OBJ_NUMERIC_ID_TABLE,
                            I_locations               IN     OBJ_NUMERIC_ID_TABLE,
                            I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                            I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                            I_subclasses              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION LATE_POSTED_TRANSACTIONS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_late_posted_tran_count  IN OUT NUMBER,
                                  O_report_level            IN OUT VARCHAR2,
                                  I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_set_of_books_ids        IN     OBJ_NUMERIC_ID_TABLE,
                                  I_org_unit_ids            IN     OBJ_NUMERIC_ID_TABLE,
                                  I_locations               IN     OBJ_NUMERIC_ID_TABLE,
                                  I_depts                   IN     OBJ_NUMERIC_ID_TABLE,
                                  I_classes                 IN     OBJ_NUMERIC_ID_TABLE,
                                  I_subclasses              IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REFRESH_STK_CNT_VALUE_VARIANCE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_stock_cnt_count        IN OUT NUMBER,
                                        O_report_level           IN OUT VARCHAR2,
                                        I_session_id             IN     oi_session_id_log.session_id%TYPE,
                                        I_cylcnt_dcs_locs        IN     RMS_OI_CYLCNT_LOC_DCS_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_STK_CNT_VAL_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_WAC_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_CUM_MARKON_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_SHRINKAGE_VAR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_LATE_POST_TRANS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_FIN_ANALYST;
/
