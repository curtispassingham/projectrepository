CREATE OR REPLACE PACKAGE RMS_OI_ACTION_TSF AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name : CREATE_TRANSFER
--Purpose       : This function creates a transfer in RMS in worksheet status.
--                It returns the newly created transfer number.
--------------------------------------------------------------------------------
FUNCTION CREATE_TRANSFER(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_no                     OUT TSFHEAD.TSF_NO%TYPE,
                         I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_tsf_object              IN     RMS_OI_TSF_TSFHEAD_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : APPROVE_TRANSFERS
--Purpose       : This function attempts to approve a list of transfers already
--                in submitted status. It returns a list of successfully approved
--                transfers and a list of transfers failed to be approved. 
--                Transfers can only be approved if the user has the corresponding
--                approval privilege of an intercompany or intra-company transfer.
--------------------------------------------------------------------------------
FUNCTION APPROVE_TRANSFERS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result                     OUT RMS_OI_TSF_STATUS_REC,
                           I_session_id              IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                           I_tsf_nos                 IN     OBJ_NUMERIC_ID_TABLE,
                           I_appr_intracompany_priv  IN     VARCHAR2,   --'Y' or 'N'
                           I_appr_intercompany_priv  IN     VARCHAR2)   --'Y' or 'N'
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : DELETE_TRANSFERS
--Purpose       : This function attempts to delete a list of transfers already
--                in submitted status. Deleting a transfer will change the transfer
--                to Deleted status. It returns a list of transfers successully
--                deleted and a list of transfers failed to be deleted.  
--                Transfers can only be deleted if the user has the corresponding
--                maintain privilege of an intercompany or intra-company transfer.
--------------------------------------------------------------------------------
FUNCTION DELETE_TRANSFERS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_result                      OUT RMS_OI_TSF_STATUS_REC,
                          I_session_id               IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                          I_tsf_nos                  IN     OBJ_NUMERIC_ID_TABLE,
                          I_maint_intracompany_priv  IN     VARCHAR2,   --'Y' or 'N'
                          I_maint_intercompany_priv  IN     VARCHAR2)   --'Y' or 'N'
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_TSF;
/
