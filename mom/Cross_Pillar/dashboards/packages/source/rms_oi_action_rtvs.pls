CREATE OR REPLACE PACKAGE RMS_OI_ACTION_RTV AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
FUNCTION CANCEL_RTVS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result           OUT RMS_OI_RTV_STATUS_REC,
                     I_session_id    IN     oi_session_id_log.session_id%TYPE,
                     I_rtv_order_nos IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SEED_RTV(O_error_message IN OUT rtk_errors.rtk_text%type,
                  O_rtv_order_no     OUT rtv_head.rtv_order_no%type,
                  I_session_id    IN     oi_session_id_log.session_id%TYPE,
                  I_ret_auth_num  IN     rtv_head.ret_auth_num%TYPE,
                  I_supplier_site IN     rtv_head.supplier%TYPE,
                  I_location      IN     item_loc.loc%type,
                  I_location_type IN     item_loc.loc_type%type,
                  I_items         IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RMS_OI_ACTION_RTV;
/
