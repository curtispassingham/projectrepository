Pre Upgrade:

Ensure that the contents of each folder will be included when the rms upgrade controller is run:

\Cross_Pillar\upgrade_scripts\source\control_script
\Cross_Pillar\upgrade_scripts\source\db_change_scripts
\Cross_Pillar\upgrade_scripts\source\packages

Data Migration:

Data migration to RMS16 requires configuring the application users to have entries in the RMS security tables.

security.dat

security.dat is used to create the data level security for new application id used when loging in to the RMS16 application. 
Since RMS16 will not be using the database id as a login on the online application, new application id will need to be created instead.
The security.dat file will ensure that the created application id will have access at the minimum, the same data as the database id.
This is a mandatory upload process.

The security file contains headers which will determine which action to execute. The security.dat file has the following structure:

|RTK_ROLE|DEVELOPER|10|
|SEC_USER|RMS_ADMIN|Y|N|N|N||
|SEC_USER|RMS_DEVELOPER|Y|N|N|N|RMS_ADMIN|
|SEC_USER_ROLE|RMS_DEVELOPER|DEVELOPER|
|SEC_GROUP|1||||
|SEC_USER_GROUP|RMS_ADMIN|
|SEC_USER_GROUP|RMS_DEVELOPER|
|FILTER_MERCH|D|1111|||
|FILTER_ORG|A|1|
|SEC_LOC_MATRIX|LTXFRM|15|153|1531||Y|Y|

RTK_ROLE - This will insert records into the RTK_ROLE_PRIVS table. The user can create any role here, as long as it is unique. The user will also specify the order approval amount for the role.
   Fields: ROLE, ORD_APPR_AMT

SEC_USER - This will insert records into the SEC_USER table. It will create new application ids used when logging in to RMS16 online application. It is also used to mark the application id as either an RMS user, ReSA User, ReIM user or an Allocation User in the SEC_USER table. Also a manager can be associated to each of the application id. However, the manager specified should be a valid database id or application id in the SEC_USER table.
   Fields: APPLICATION_USER_ID, RMS_USER_IND, RESA_USER_IND, REIM_USER_IND, ALLOCATION_USER_IND, MANAGER

SEC_USER_ROLE - This will insert records into the SEC_USER_ROLE table. It will assign the created roles to the application ids. An application id can be associated with multiple roles.
   Fields: APPLICATION_USER_ID, ROLE

SEC_GROUP - This will reference records on the SEC_GROUP table. This line will be used to either create a new group or associate an existing group with data level security. If creating a new group, then the group id should not be specified as this will be system generated. Instead, the name for the group id and group role should be specified, with the name
            being mandatory.
   Fields: GROUP_ID, GROUP_NAME, ROLE, COMMENTS

SEC_USER_GROUP - This will insert records into the SEC_USER_GROUP table. This will associate an application id with a group. An application id can exist in multiple groups. (See Notes below)
   Fields: APPLICATION_USER_ID

FILTER_MERCH - This will insert records into the FILER_GROUP_MERCH table. Defined are the merchandise levels that are accessible to the group. If the group will have no restriction on merchandise data, then do not insert records here for the group. The merchandise levels that can be used are under the code type FLTM. (See Notes below)
   Fields: FILTER_MERCH_LEVEL, FILTER_MERCH_ID, FILTER_MERCH_ID_CLASS, FILTER_MERCH_ID_SUBCLASS

FILTER_ORG - This will insert records into the FILER_GROUP_ORG table. Defined are the organization levels that are accessible to the group. If the group will have no restriction on organization data, then do not insert records here for the group. The organization levels that can be used are under the code type FLOW. (See Notes below)
   Fields: FILTER_ORG_LEVEL, FILTER_ORG_ID

SEC_LOC_MATRIX - This will insert records into the SEC_GROUP_LOC_MATRIX table. This will define security for a specific organization and group. If the group will have no restriction, then do not insert records here for the group. The available functional areas are LTXFRM (Transfers from) and LTXFTO (Transfers to). The user can apply security at either the region, district, store or warehouse level. For region, district and store, ensure that the hierarhcy is specified and exists in STORE_HIERARCHY table. (See Notes below)
   Fields: COLUMN_CODE, REGION, DISTRICT , STORE, WH, SELECT_IND, UPDATE_IND


Depending on the DATA_LEVEL_SECURITY_IND in SECURITY_CONFIG_OPTIONS, the configuration on security.dat file will change. The configurations are specified below.

1. DATA_LEVEL_SECURITY_IND = 'N'

If DATA_LEVEL_SECURITY_IND is set to 'N', this would mean the RMS16 application do not implement data level security. The security.dat
file will be configured to only create and associate the roles and application id.

|RTK_ROLE|DEVELOPER|10|
|RTK_ROLE|MANAGER|9999999|
|RTK_ROLE|STAFF|1|
|SEC_USER|RMS_ADMIN|Y|N|N|N||
|SEC_USER|RMS_DEVELOPER|Y|N|N|N|RMS_ADMIN|
|SEC_USER|RMS_TESTER|Y|N|N|N||
|SEC_USER_ROLE|RMS_ADMIN|MANAGER|
|SEC_USER_ROLE|RMS_TESTER|DEVELOPER|

1. DATA_LEVEL_SECURITY_IND = 'Y'

If DATA_LEVEL_SECURITY_IND is set to 'Y', this would mean the RMS16 application will implement data level security. The security.dat
file will be configured to create and associate the roles, application id and groups. Assigining merchandise and organization levels to the groups
can also be configured, but is not mandatory. For associating a new or existing group with application id and merchandise or organization levels, see notes below.

|RTK_ROLE|DEVELOPER|10|
|RTK_ROLE|MANAGER|9999999|
|RTK_ROLE|STAFF|1|
|SEC_USER|RMS_ADMIN|Y|N|N|N||
|SEC_USER|RMS_DEVELOPER|Y|N|N|N|RMS_ADMIN|
|SEC_USER|RMS_TESTER|Y|N|N|N||
|SEC_USER_ROLE|RMS_ADMIN|MANAGER|
|SEC_USER_ROLE|RMS_TESTER|DEVELOPER|
|SEC_GROUP|1||||
|SEC_USER_GROUP|RMS_ADMIN|
|SEC_USER_GROUP|RMS_DEVELOPER|
|SEC_GROUP||SYSTEM LIMITED MERCH||Associated with Certain Merch Levels.|
|SEC_USER_GROUP|RMS_TESTER|
|FILTER_MERCH|D|1111|||
|FILTER_ORG|A|1|
|FILTER_MERCH|S|2101|2|4|
|SEC_LOC_MATRIX|LTXFRM||||10003|Y|Y|
|FILTER_ORG|W|10003|
|SEC_LOC_MATRIX|LTXFRM|15|153|1531||Y|Y|

Notes: The script will assign application id, merchandise, organization levels and locations to the security group preceding it. It will associate each line after the SEC_GROUP header the he current group.
When the reads another SEC_GROUP header, then that would mean succeding lines will now be assigned to that SEC_GROUP. Note that for the SEC_GROUP line, you can use an existing group by specifying the GROUP_ID from SEC_GROUP table.
For the above file template, the associations created will be as follows.

|SEC_GROUP|1|||| - This line will use an existing security group. The RMS_ADMIN and RMS_DEVELOPER application ids will be associated with this group.
|SEC_GROUP||SYSTEM LIMITED MERCH||Associated with Certain Merch Levels.| - This line will insert a new group in the SEC_GROUP table with the name SYSTEM LIMITED MERCH. The RMS_TESTER application id will be associated with this group,
and also all the lines below it, until the next SEC_GROUP header.