#!/bin/ksh


# General function written for processing TRIGGER,PROCEDURE,OBJECTS,CONTROL and DBC scripts.
function PROCESS
{
   OBJ_TYPE=$1
   #Setting the object directory based on the object type.
   if [ $OBJ_TYPE = "FUNCTIONS" ];
   then
      VALIDATE=0
      OBJ_DIR=$PROCEDURE_DIR
   elif [ $OBJ_TYPE = "TRIGGERS" ];
   then
      VALIDATE=0
      OBJ_DIR=$TRIGGER_DIR
   elif [ $OBJ_TYPE = "PROCEDURES" ];
   then
      VALIDATE=0
      OBJ_DIR=$PROCEDURE_DIR
   elif [ $OBJ_TYPE = "PACKAGES" ];
   then
      VALIDATE=0
      OBJ_DIR=$PACKAGE_DIR
  elif [ $OBJ_TYPE = "OBJECTS" ];
  then
      VALIDATE=0
      OBJ_DIR=$OBJECT_DIR
  elif [ $OBJ_TYPE = "LANGS" ];
  then
     VALIDATE=0
     OBJ_DIR=$LANG_DIR
  elif [ $OBJ_TYPE = "CONTROL" ];
   then
      VALIDATE=1
      OBJ_DIR=$CONTROL_DIR
  elif [ $OBJ_TYPE = "CLEANUP" ];
  then
     VALIDATE=0
     OBJ_DIR=$UTILITY_DIR
  else
      VALIDATE=1
      OBJ_DIR=$DBC_DIR
   fi
   echo "******************************"
   echo "Execution of $OBJ_TYPE scripts"

   echo "******************************" >> $LOG_FILE
   echo "Execution of $OBJ_TYPE scripts" >> $LOG_FILE

   if [ -f $FILE_DIR/$OBJ_TYPE.dat ]; then
      RECCNT=`cat $FILE_DIR/$OBJ_TYPE.dat | wc -l`
      if [ $RECCNT = 0 ]; then
         echo "There is nothing to upgrade for $OBJ_TYPE"
         echo "There is nothing to upgrade for $OBJ_TYPE" >> $LOG_FILE
      fi
   fi

   cat -b $FILE_DIR/$OBJ_TYPE.dat |while read ROW
   do
      OBJ_NAME=`echo $ROW |cut -d " " -f2`

      if [ $OBJ_TYPE = "LANGS" ]; then
         OBJ_NAME_HOLDER=`echo $ROW | cut -d " " -f2`
         FILENAME=`echo $OBJ_NAME_HOLDER | cut -d "." -f1`
         EXT=`echo $OBJ_NAME_HOLDER | cut -d "." -f2`
         PRIM_OR_SEC=`echo $FILENAME | cut -d "_" -f2`
         UNDERSCORE=`echo _`
         MY_LANG=`echo $LANGUAGE | tr '[A-Z]' '[a-z]'`
         OBJ_NAME="$FILENAME$UNDERSCORE$MY_LANG.$EXT"
         if [ $PRIM_LANG_IND -eq 1 ]; then
            if [ $PRIM_OR_SEC = "secondary" ]; then
               continue;
            fi
         else
            if [ $PRIM_OR_SEC = "primary" ]; then
               continue;
            fi 
         fi 
      fi
      #Building the error file name.
      ERR_FILE=`echo $OBJ_NAME | cut -d "." -f1`
      #Checking if the processed file is present in the processed directory,
      #If it is present the execution is skiped.
      if [ -f $PROCESSED_DIR/$OBJ_NAME.processed ];
      then
         echo "$OBJ_TYPE $OBJ_NAME has already been processed" 
         echo "$OBJ_TYPE $OBJ_NAME has already been processed" >> $LOG_FILE
      elif [ -f $OBJ_DIR/$OBJ_NAME ];
      then
         echo "Executing file $OBJ_NAME"
         echo "Executing file $OBJ_NAME" >> $LOG_FILE
         SCHEMA_OWNER=`echo $SCHEMA_OWNER | tr '[a-z]' '[A-Z]'`
         $ORACLE_HOME/bin/sqlplus -s $UP <<EOF > $ERROR_DIR/$ERR_FILE.err
         @"$OBJ_DIR/$OBJ_NAME" $SCHEMA_OWNER;
EOF

          if [ $VALIDATE -eq 1 ]; then

            if [ `grep "^ORA-" $ERROR_DIR/$ERR_FILE.err | wc -l` -gt 0 ] || [ `grep "^SP2-*" $ERROR_DIR/$ERR_FILE.err | wc -l` -gt 0 ];
            then
               echo "ORA Error while executing $OBJ_NAME" 
               echo "ORA Error while executing $OBJ_NAME" >> $LOG_FILE
               exit 1
            else
               rm -f $ERROR_DIR/$ERR_FILE.err
               touch $PROCESSED_DIR/$OBJ_NAME.processed
               echo "File $OBJ_NAME was successfully executed" 
               echo "File $OBJ_NAME was successfully executed" >> $LOG_FILE
            fi
         else 
            rm -f $ERROR_DIR/$ERR_FILE.err
            touch $PROCESSED_DIR/$OBJ_NAME.processed
            echo "File $OBJ_NAME was successfully executed" 
            echo "File $OBJ_NAME was successfully executed" >> $LOG_FILE
         fi
       else
          echo "File $OBJ_NAME is not available in the $OBJ_DIR directory to be executed" 
          echo "File $OBJ_NAME is not available in the $OBJ_DIR directory to be executed" >> $LOG_FILE
       fi
   done

   STATUS=$?
   
   if [ $STATUS = 0 ]; then      
      if [ $OBJ_TYPE != "LANGS" ]; then
         echo "$OBJ_TYPE completed"
         echo "******************************" 
         echo "$OBJ_TYPE completed" >> $LOG_FILE
         echo "******************************" >> $LOG_FILE
      else
         echo "Language $LANGUAGE completed"
         echo "******************************" 
         echo "Language $LANGUAGE completed" >> $LOG_FILE
         echo "******************************" >> $LOG_FILE
      fi
   else 
      exit 1
   fi
} 



function INV_OBJ_COMP
{
   echo "Execution of INV_OBJ_COMP script"

COUNT=1
CTR=0
while [[ $COUNT -ne 0 && $CTR -ne $MAX_LOOP ]] 
do
COUNT=`$ORACLE_HOME/bin/sqlplus -s $UP << EOF
    set heading off feedback off verify off pause off echo off pages 0
    select count(1) from user_objects where status = 'INVALID';
    exit
EOF
`
if [ $COUNT -ne 0 ]; then
   $ORACLE_HOME/bin/sqlplus -s $UP <<EOF > $ERROR_DIR/$ERR_FILE.err
   @"$UTILITY_DIR/inv_obj_comp.sql";
EOF
fi

CTR=`expr $CTR + 1`
done

if [ $COUNT -eq 0 ]; then
   touch $PROCESSED_DIR/packages.processed
   touch $PROCESSED_DIR/object_script.processed
   echo "All the objects compiled successfully"
   echo "All the objects compiled successfully" >> $LOG_FILE
else
   echo "Some of the objects has errors. Open a sql session and run the command below to find out the invalid objects"
   echo "************************************************************************************************************"
   echo "* select * from user_objects where status != 'VALID'                                                       *"
   echo "************************************************************************************************************"
   echo "Some of the objects has errors" >> $LOG_FILE
   exit 1
fi

}


function LANG
{
CNT=`cat -b $FILE_DIR/LANGS.dat | wc -l`

if [ $CNT -ne 0 ]; then
PRIM_LANG=`$ORACLE_HOME/bin/sqlplus -s $UP << EOF
    set heading off feedback off verify off pause off echo off pages 0
    select iso_code from system_options s, lang l where s.primary_lang = l.lang;
    exit
EOF
`

if [ $PRIM_LANG != 'EN' ]; then
   PRIM_LANG_IND=1
   LANGUAGE=$PRIM_LANG
   PROCESS LANGS
fi

if [ $PRIM_LANG = 'EN' ]; then   
for DB in ` $ORACLE_HOME/bin/sqlplus -s $UP <<EOF
set heading off feedback off verify off pause off echo off pages 0
select distinct iso_code from form_elements_langs f,lang l where f.lang = l.lang and f.lang !=1; 
EOF`
do
   PRIM_LANG_IND=0
   LANGUAGE=$DB
   PROCESS LANGS

done  
fi
fi

}
#-------------------------------------------------------------------------
### Main execution part starts here. ###
#-------------------------------------------------------------------------

if [ $# -lt 1 ]; then
   echo "*****************************PARAMETERS***********************************************"
   echo "* 1. Upgrade database without object compile run <script name> DBO N                 *"
   echo "* 2. Upgrade database with object compile run <script name> DBO Y                    *"
   echo "**************************************************************************************"
   exit 1
elif [ $# -eq 1 ]; then
   COMPILE=N
elif [ $# -eq 2 ]; then
   COMPILE=$2
fi

#Execution of main scripts,to create directories and set env variable.
. ./controller.cfg

STATUS=$?
if [ $STATUS != 0 ]; then
   echo  "Configuration error"
   echo "Configuration error" >> $LOG_FILE
   exit 1
fi

SUPPLIER_SITES_IND=`$ORACLE_HOME/bin/sqlplus -s $UP << EOF
    set heading off feedback off verify off pause off echo off pages 0
    select supplier_sites_ind from functional_config_options;
    exit
EOF
`
if [ $SUPPLIER_SITES_IND != "Y" ]; then   
   echo "*******************************UPGRADE FAILURE****************************************"
   echo "* RMS16 requires the FUNCTIONAL_CONFIG_OPTIONS.SUPPLIER_SITES_IND be set to 'Y'.     *"
   echo "* For clients that do not use the supplier site functionality, they need to set up a *"
   echo "* one-to-one relation between a supplier and supplier site.                          *"
   echo "**************************************************************************************"
   exit 1
EOF
fi

if [ $1 = "DBO" ];then

   PROCESS FUNCTIONS
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

   PROCESS OBJECTS
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

   PROCESS DBC
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

#  LANG
#  if [ $STATUS != 0 ]; then
#    exit 1
#  fi
   PROCESS PACKAGES
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

   PROCESS PROCEDURES
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

   PROCESS TRIGGERS
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

   PROCESS CONTROL
   STATUS=$?

   if [ $STATUS != 0 ]; then
     exit 1
   fi

   if [ $COMPILE = "Y" ]; then
     INV_OBJ_COMP
     STATUS=$?
     if [ $STATUS != 0 ]; then
        exit 1
     fi
   fi

   if [ -f $LOG_FILE ]; then
     CNT=`grep -il "End of run" $LOG_FILE | wc -l`
     CNT=`expr $CNT + 1`
   fi
   echo "All database objects were executed successfully"
   echo "***********************************************"
   echo "End of run # $CNT"
   echo "***********************************************"
   echo "All database objects were executed successfully" >> $LOG_FILE
   echo "***********************************************" >> $LOG_FILE
   echo "End of run # $CNT" >> $LOG_FILE
   echo "***********************************************" >> $LOG_FILE

else 
   echo  "Invalid upgrade parameter"
   echo  "Invalid upgrade parameter." >> $LOG_FILE
   exit 1
fi

exit 0

