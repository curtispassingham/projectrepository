set heading off;
set feedback on;
set pages 10000;

spool inv_obj_comp.log


select 'alter '||object_type||' "'|| object_name ||'" compile;'
  from user_objects
 where status = 'INVALID'
   and object_type not in ('PACKAGE BODY','TYPE BODY')
 union
select 'alter package "'|| object_name ||'" compile body;'
  from user_objects
 where status = 'INVALID'
   and object_type = 'PACKAGE BODY'
union
select 'alter type "'|| object_name ||'" compile body;'
  from user_objects
 where status = 'INVALID'
   and object_type = 'TYPE BODY';

spool off

@inv_obj_comp.log

undefine spoolfile; 

set heading on;

