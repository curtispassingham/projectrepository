#!/bin/ksh
dt=`date +"%G%m%d"`
dtStamp=`date +"%G%m%d%H%M%S"`
LOG_FILE=${LOG_DIR}/$0.log.$dt

echo "Execution of PRE VALIDATION scripts started at $dtStamp"  |tee -a $LOG_FILE

if [  -f $FILE_DIR/security.dat ]; then
   echo "security.dat file exists. Proceeding to the next step" |tee -a $LOG_FILE
else
   echo "ERROR : security.dat file does not exist. Please create a security.dat file before proceeding to the next step" |tee -a $LOG_FILE
   return 1
fi
#
#
echo "Execution of pre validation script ended successfully " |tee -a $LOG_FILE
return 0