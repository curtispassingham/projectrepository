--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ITEM_FORECAST_HIST
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------

DROP TABLE ITEM_FORECAST_HIST
/

DECLARE

   L_partition_high_value  VARCHAR2(10);
   L_create_sql     VARCHAR2(4000);

BEGIN

   --use a valid eow_date+1 as the partition high value
   select to_char(d.eow_date+1, 'YYYY-MM-DD')
     into L_partition_high_value
     from day_level_calendar d, period p
    where d.day = p.vdate;

   L_create_sql := 'CREATE TABLE ITEM_FORECAST_HIST '|| 
                   '(ITEM VARCHAR2(25) NOT NULL '||
                   ', LOC NUMBER(10, 0) NOT NULL '|| 
                   ', EOW_DATE DATE NOT NULL '||
                   ', FORECAST_SALES NUMBER(12, 4) NOT NULL '||
                   ', FORECAST_STD_DEV NUMBER(12, 4) NOT NULL ) '||
                   'INITRANS 12 '||
                   'LOGGING '||
                   'TABLESPACE RETAIL_DATA '||
                   'PARTITION BY RANGE (EOW_DATE) INTERVAL (NUMTODSINTERVAL(7,''DAY'')) '|| 
                   '(PARTITION ITEM_FORECAST_HIST_P1  VALUES LESS THAN (TO_DATE('' '||L_partition_high_value||' 00:00:00'', ''SYYYY-MM-DD HH24:MI:SS'', ''NLS_CALENDAR=GREGORIAN'')))';
   EXECUTE IMMEDIATE L_create_sql;

EXCEPTION
   when OTHERS then
      dbms_output.put_line ('PACKAGE_ERROR: '||SQLERRM||' '||to_char(SQLCODE));
END;
/

CREATE UNIQUE INDEX PK_ITEM_FORECAST_HIST ON ITEM_FORECAST_HIST (ITEM, LOC, EOW_DATE) 
  INITRANS 24 
  TABLESPACE RETAIL_INDEX  LOCAL;

ALTER TABLE ITEM_FORECAST_HIST ADD CONSTRAINT PK_ITEM_FORECAST_HIST PRIMARY KEY (ITEM, LOC, EOW_DATE) 
  USING INDEX INITRANS 24  
  TABLESPACE RETAIL_INDEX  LOCAL ENABLE;
    
COMMENT ON TABLE ITEM_FORECAST_HIST IS 'Holds 4 weeks of history of the item level forecasted sales information from the RDF extractions. It is used to support the Inventory Variance to Forecast dashboard report. This table should be partitioned according to the end of week date.';
COMMENT ON COLUMN ITEM_FORECAST_HIST.ITEM IS 'Contains the item id. Only forecasted items will be on this table.';
COMMENT ON COLUMN ITEM_FORECAST_HIST.LOC IS 'Contains the location corresponding to the forecasted sales information for the Item.';
COMMENT ON COLUMN ITEM_FORECAST_HIST.EOW_DATE IS 'Contains the end of the week date from which the forecasted sales totals are gathered. Since this history holds 4 weeks of history, it will be the previous eow_date with regard to the vdate and three previous to that.';
COMMENT ON COLUMN ITEM_FORECAST_HIST.FORECAST_SALES IS 'Contains forecasted sales units for the given item/store/date combination.  This field will only contain a value for sales_type = R (regular sales).';
COMMENT ON COLUMN ITEM_FORECAST_HIST.FORECAST_STD_DEV IS 'Contains the standard deviation value for the given item/store/date combination.  This value represents the confidence level in the sales forecast.  This field is used in the safety stock calculations for the dynamic replenishment method. This field will only contain a value for sales_type = R (regular).';

