--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_SEC_USER_GROUP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_SEC_USER_GROUP'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_SEC_USER_GROUP';
   end if;
END;
/

PROMPT Creating Table 'UPG_SEC_USER_GROUP'
CREATE TABLE UPG_SEC_USER_GROUP
 (GROUP_ID              NUMBER(4),
  APPLICATION_USER_ID   VARCHAR2(30)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON COLUMN UPG_SEC_USER_GROUP.GROUP_ID IS 'This column holds the security group the security user belongs to. It references the group id defined on the SEC_GROUP table.'
/

COMMENT ON COLUMN UPG_SEC_USER_GROUP.APPLICATION_USER_ID IS 'This field holds the login id used in the ADF RMS application screen.'
/

COMMENT ON TABLE UPG_SEC_USER_GROUP  IS 'This table holds the security group a security user belongs to.'
/

