CREATE OR REPLACE PACKAGE DATA_MIGRATION_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------
-- Function Name: SECURITY_UPGRADE
-- Purpose      : This function is used to ceate the data security for ADF RMS application users.
--------------------------------------------------------------------------------------------------
FUNCTION SECURITY_UPGRADE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
END DATA_MIGRATION_SQL;
/