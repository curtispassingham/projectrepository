CREATE OR REPLACE PACKAGE BODY DATA_MIGRATION_SQL AS
--------------------------------------------------------------------------------------------------
-- Function Name: SECURITY_UPGRADE
-- Purpose      : This function is used to ceate the data security for ADF RMS application users.
--------------------------------------------------------------------------------------------------
FUNCTION SECURITY_UPGRADE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(80) := 'DATA_MIGRATION_SQL.SECURITY_UPGRADE';

   L_seq_no           SEC_USER.USER_SEQ%TYPE;
   L_manager          SEC_USER.APPLICATION_USER_ID%TYPE;
   L_manager_seq_no   SEC_USER.USER_SEQ%TYPE;

   CURSOR c_application_user IS
   SELECT application_user_id,
          rms_user_ind,
          resa_user_ind,
          reim_user_ind,
          allocation_user_ind,
          manager
     FROM upg_application_user;

   CURSOR c_user_seq IS
   SELECT sec_user_sequence.nextval seq_no
     FROM dual;
     
   CURSOR c_manager IS
   SELECT user_seq
     FROM sec_user
    WHERE NVL(database_user_id, application_user_id) = L_manager;

BEGIN

   -- Create RTK_ROLES
   MERGE INTO rtk_role_privs rp
   USING upg_rtk_role v
      ON (rp.role = v.role)
    WHEN MATCHED THEN
         UPDATE
            SET rp.ord_appr_amt = v.ord_appr_amt
    WHEN NOT MATCHED THEN
         INSERT (role,
                 ord_appr_amt,
                 create_id,
                 create_datetime)
         VALUES (v.role,
                 v.ord_appr_amt,
                 user,
                 sysdate);

   -- Create new APPLICATION_ID
   FOR rec IN c_application_user LOOP

      OPEN c_user_seq;
      FETCH c_user_seq INTO L_seq_no;
      CLOSE c_user_seq;

      L_manager := rec.manager;
      L_manager_seq_no := null;

      OPEN c_manager;
      FETCH c_manager INTO L_manager_seq_no;
      CLOSE c_manager;

      MERGE
         INTO sec_user sec
        USING (SELECT L_seq_no user_seq,
                      rec.application_user_id app_id,
                      user create_id,
                      sysdate create_date,
                      rec.rms_user_ind rms_ind,
                      rec.resa_user_ind resa_ind,
                      rec.reim_user_ind reim_ind,
                      rec.allocation_user_ind alloc_ind,
                      L_manager_seq_no manager
                 FROM dual) v
           ON (sec.application_user_id = v.app_id)
         WHEN MATCHED THEN
              UPDATE
                 SET sec.rms_user_ind = v.rms_ind,
                     sec.resa_user_ind = v.resa_ind,
                     sec.reim_user_ind = v.reim_ind,
                     sec.allocation_user_ind = v.alloc_ind,
                     sec.manager = v.manager
         WHEN NOT MATCHED THEN
              INSERT (user_seq,
                      application_user_id,
                      create_id,
                      create_datetime,
                      rms_user_ind,
                      resa_user_ind,
                      reim_user_ind,
                      allocation_user_ind,
                      manager)
              VALUES (v.user_seq,
                      v.app_id,
                      v.create_id,
                      v.create_date,
                      v.rms_ind,
                      v.resa_ind,
                      v.reim_ind,
                      v.alloc_ind,
                      v.manager);
   END LOOP;

   -- Assign RTK_ROLES to new APPLICATION_ID
   MERGE
      INTO sec_user_role sur
     USING (SELECT rtk.role,
                   sec.user_seq user_seq,
                   user create_id,
                   sysdate create_date
              FROM upg_sec_user_role upg,
                   rtk_role_privs rtk,
                   sec_user sec
             WHERE upg.application_user_id = sec.application_user_id
               AND upg.role = rtk.role) v
        ON (sur.user_seq = v.user_seq AND
            sur.role = v.role)
      WHEN NOT MATCHED THEN
           INSERT (role,
                   user_seq,
                   create_id,
                   create_datetime)
           VALUES (v.role,
                   v.user_seq,
                   v.create_id,
                   v.create_date);

   -- Create new SEC_GROUP_ID
   MERGE
      INTO sec_group sec
     USING upg_sec_group upg
        ON (sec.group_id = upg.group_id)
      WHEN NOT MATCHED THEN
           INSERT (group_id,
                   group_name,
                   role,
                   comments,
                   create_id,
                   create_datetime)
           VALUES (upg.group_id,
                   upg.group_name,
                   upg.role,
                   upg.comments,
                   user,
                   sysdate);

   -- Assign APPLICATION_ID with SEC_GROUP_ID
   MERGE
      INTO sec_user_group sec
     USING (SELECT su.user_seq,
                   sg.group_id
              FROM upg_sec_user_group sg,
                   sec_user su
             WHERE sg.application_user_id = su.application_user_id) v
        ON (sec.user_seq = v.user_seq AND
            sec.group_id = v.group_id)
      WHEN NOT MATCHED THEN
           INSERT (group_id,
                   user_seq,
                   create_id,
                   create_datetime)
           VALUES (v.group_id,
                   v.user_seq,
                   user,
                   sysdate);

   -- Assign security filter for Organization Hierarchy
   MERGE
      INTO filter_group_org sec
     USING (SELECT upg.sec_group_id,
                   upg.filter_org_level,
                   upg.filter_org_id
              FROM upg_filter_org upg,
                   (SELECT 'C' org_level,
                            TO_CHAR(chain) org_id
                       FROM chain
                      UNION ALL
                     SELECT 'A' org_level,
                            TO_CHAR(area) org_id
                       FROM area
                      UNION ALL
                     SELECT 'R' org_level,
                            TO_CHAR(region) org_id
                       FROM region
                      UNION ALL
                     SELECT 'D' org_level,
                            TO_CHAR(district) org_id
                       FROM district
                      UNION ALL
                     SELECT distinct 'W' org_level,
                            TO_CHAR(wh.wh) org_id
                       FROM wh wh,
                            addr ad,
                            add_type_module at
                      WHERE wh.finisher_ind = 'N'
                        AND ad.key_value_1 = wh.physical_wh
                        AND ad.module = 'WH'
                        AND ad.module = at.module
                        AND at.primary_ind = 'Y'
                      UNION ALL
                     SELECT 'I' org_level,
                            TO_CHAR(wh) org_id
                       FROM wh
                      WHERE finisher_ind = 'Y'
                      UNION ALL
                     SELECT 'E' org_level,
                            partner_id org_id
                       FROM partner
                      WHERE partner_type = 'E'
                      UNION ALL
                     SELECT 'O' org_level,
                            TO_CHAR(org_unit_id) org_id
                       FROM org_unit) v
             WHERE upg.filter_org_level = v.org_level
               AND TO_CHAR(upg.filter_org_id) = v.org_id) v
        ON (sec.sec_group_id = v.sec_group_id AND
            sec.filter_org_level = v.filter_org_level AND
            sec.filter_org_id = v.filter_org_id)
      WHEN NOT MATCHED THEN
           INSERT (sec_group_id,
                   filter_org_level,
                   filter_org_id)
           VALUES (v.sec_group_id,
                   v.filter_org_level,
                   v.filter_org_id);

   -- Assign security filter for Merchandise Hierarchy
   MERGE
      INTO filter_group_merch sec
     USING (SELECT upg.sec_group_id,
                   upg.filter_merch_level,
                   upg.filter_merch_id,
                   upg.filter_merch_id_class,
                   upg.filter_merch_id_subclass
              FROM upg_filter_merch upg,
                   (SELECT 'D' merch_level,
                           division merch_id,
                           null class,
                           null subclass
                      FROM division
                     UNION ALL
                    SELECT 'G' merch_level,
                           group_no merch_id,
                           null class,
                           null subclass
                      FROM groups
                     UNION ALL
                    SELECT 'P' merch_level,
                           dept merch_id,
                           null class,
                           null subclass
                      FROM deps
                     UNION ALL
                    SELECT 'C' merch_level,
                           dept merch_id,
                           class class,
                           null subclass
                      FROM class
                     UNION ALL
                    SELECT 'S' merch_level,
                           dept merch_id,
                           class class,
                           subclass subclass
                      FROM subclass) v
             WHERE upg.filter_merch_level = v.merch_level
               AND upg.filter_merch_id = v.merch_id
               AND NVL(upg.filter_merch_id_class, -1) = NVL(v.class, -1)
               AND NVL(upg.filter_merch_id_subclass, -1) = NVL(v.subclass, -1)) v
        ON (sec.sec_group_id = v.sec_group_id AND
            sec.filter_merch_level = v.filter_merch_level AND
            sec.filter_merch_id = v.filter_merch_id AND
            NVL(sec.filter_merch_id_class, -1) = NVL(v.filter_merch_id_class, -1) AND
            NVL(sec.filter_merch_id_subclass, -1) = NVL(v.filter_merch_id_subclass, -1))
      WHEN NOT MATCHED THEN
           INSERT (sec_group_id,
                   filter_merch_level,
                   filter_merch_id,
                   filter_merch_id_class,
                   filter_merch_id_subclass)
           VALUES (v.sec_group_id,
                   v.filter_merch_level,
                   v.filter_merch_id,
                   v.filter_merch_id_class,
                   v.filter_merch_id_subclass);

  -- Assign security filter for Locations
   MERGE
      INTO sec_group_loc_matrix sec
     USING upg_sec_loc_matrix upg
        ON (sec.group_id = upg.group_id AND
            sec.column_code = upg.column_code AND
            NVL(sec.region, -1) = NVL(upg.region, -1) AND
            NVL(sec.district, -1) = NVL(upg.district, -1) AND
            NVL(sec.store, -1) = NVL(upg.store, -1) AND
            NVL(sec.wh, -1) = NVL(upg.wh, -1))
      WHEN NOT MATCHED THEN
           INSERT (group_id,
                   column_code,
                   region,
                   district,
                   store,
                   wh,
                   select_ind,
                   update_ind)
           VALUES (upg.group_id,
                   upg.column_code,
                   upg.region,
                   upg.district,
                   upg.store,
                   upg.wh,
                   upg.select_ind,
                   upg.update_ind);

   return TRUE;
                   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR',
                                                  SQLERRM,
                                                  L_program,
                                                  TO_CHAR(SQLCODE));
      return FALSE;

END SECURITY_UPGRADE;
----------------------------------------------------------------------------------------------------
END DATA_MIGRATION_SQL;
/