--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_FILTER_MERCH
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_FILTER_MERCH'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_FILTER_MERCH';
   end if;
END;
/

PROMPT Creating Table 'UPG_FILTER_MERCH'
CREATE TABLE UPG_FILTER_MERCH
 (SEC_GROUP_ID               NUMBER(4),
  FILTER_MERCH_LEVEL         VARCHAR2(1),
  FILTER_MERCH_ID            NUMBER(4),
  FILTER_MERCH_ID_CLASS      NUMBER(4),
  FILTER_MERCH_ID_SUBCLASS   NUMBER(4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_FILTER_MERCH  IS 'This table contains the Merchandise Hierarchy LOV filtering access information for a User Security.'/

COMMENT ON COLUMN UPG_FILTER_MERCH.SEC_GROUP_ID IS 'ID of the User Security group'
/

COMMENT ON COLUMN UPG_FILTER_MERCH.FILTER_MERCH_LEVEL IS 'The Merchandise hierarchy level assigned to the User Security Group.'
/

COMMENT ON COLUMN UPG_FILTER_MERCH.FILTER_MERCH_ID IS 'ID of the Merchandise hierarchy level assigned to the User Security Group'
/

COMMENT ON COLUMN UPG_FILTER_MERCH.FILTER_MERCH_ID_CLASS IS 'Class ID of the Merchandise hierarchy level assigned to the user security group'
/

COMMENT ON COLUMN UPG_FILTER_MERCH.FILTER_MERCH_ID_SUBCLASS IS 'Subclass ID of the Merchandise hierarchy level assigned to the user security group'
/

