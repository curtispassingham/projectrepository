OPTIONS (ERRORS=50,DIRECT=TRUE)
UNRECOVERABLE
LOAD DATA
TRUNCATE
INTO TABLE UPG_SEC_LOC_MATRIX
WHEN GROUP_ID != BLANKS and COLUMN_CODE != BLANKS and SELECT_IND != BLANKS and UPDATE_IND != BLANKS
  FIELDS TERMINATED BY '|'
  OPTIONALLY ENCLOSED BY '"'
  (
     REC_TYPE FILLER,
     GROUP_ID,
     COLUMN_CODE,
     REGION,
     DISTRICT ,
     STORE,
     WH,
     SELECT_IND,
     UPDATE_IND
  )
