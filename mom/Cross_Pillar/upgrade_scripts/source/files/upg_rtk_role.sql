--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_RTK_ROLE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_RTK_ROLE'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_RTK_ROLE';
   end if;
END;
/

PROMPT Creating Table 'UPG_RTK_ROLE'
CREATE TABLE UPG_RTK_ROLE
 (ROLE              VARCHAR2(30),
  ORD_APPR_AMT      NUMBER(20,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_RTK_ROLE is 'This is the temporary staging table for UPG_RTK_ROLE upgrade.'
/

COMMENT ON COLUMN UPG_RTK_ROLE.ROLE IS 'This field contains the Oracle role for which the record will pertain to.'
/

COMMENT ON COLUMN UPG_RTK_ROLE.ORD_APPR_AMT IS 'This field contains the upper limit that the role is able to approve on an order.  This value is expressed in primary currency.'
/
