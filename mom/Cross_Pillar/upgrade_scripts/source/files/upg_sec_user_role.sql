--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_SEC_USER_ROLE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_SEC_USER_ROLE'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_SEC_USER_ROLE';
   end if;
END;
/

PROMPT Creating Table 'UPG_SEC_USER_ROLE'
CREATE TABLE UPG_SEC_USER_ROLE
 (APPLICATION_USER_ID VARCHAR2(30),
  ROLE VARCHAR2(30)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_SEC_USER_ROLE is 'This table holds the roles a security user belongs to.'
/

COMMENT ON COLUMN UPG_SEC_USER_ROLE.ROLE is 'This field contains the role the user belongs to.'
/

COMMENT ON COLUMN UPG_SEC_USER_ROLE.APPLICATION_USER_ID is 'This filed holds the user assigned to the role. It references the application user id defined on the SEC_USER table.'
/
