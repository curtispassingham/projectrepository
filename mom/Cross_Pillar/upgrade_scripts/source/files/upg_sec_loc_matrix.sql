--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_SEC_LOC_MATRIX
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_SEC_LOC_MATRIX'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_SEC_LOC_MATRIX';
   end if;
END;
/

PROMPT Creating Table 'UPG_SEC_LOC_MATRIX'
CREATE TABLE UPG_SEC_LOC_MATRIX
 (GROUP_ID      NUMBER(4),
  COLUMN_CODE   VARCHAR2(6),
  REGION        NUMBER(10),
  DISTRICT      NUMBER(10),
  STORE         NUMBER(10),
  WH            NUMBER(10),
  SELECT_IND    VARCHAR2(1),
  UPDATE_IND    VARCHAR2(1)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_SEC_LOC_MATRIX  IS 'This table will be used to store the group location security attributes.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.COLUMN_CODE IS 'Contains a 6 digit code which identifies the functional area to which the security applies.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.GROUP_ID IS 'Contains the unique identifier associated with the group.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.REGION IS 'Contains the unique number identifying the region in the organisational hierarchy.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.DISTRICT IS 'Contains the unique number identifying the district in the organisational hierarchy.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.STORE IS 'Contains the unique number identifying the store in the organisational hierarchy.'


COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.WH IS 'Contains the unique number identifying the warehouse in the organisational hierarchy.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.SELECT_IND IS 'Value of this column indicates whether the user has select privileges.'
/

COMMENT ON COLUMN UPG_SEC_LOC_MATRIX.UPDATE_IND IS 'Value of this column indicates whether the user has  insert, update and delete privileges.'
/
