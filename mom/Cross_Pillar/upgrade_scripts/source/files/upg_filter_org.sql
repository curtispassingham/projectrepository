--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_FILTER_ORG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_FILTER_ORG'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_FILTER_ORG';
   end if;
END;
/

PROMPT Creating Table 'UPG_FILTER_ORG'
CREATE TABLE UPG_FILTER_ORG
 (SEC_GROUP_ID       NUMBER(4),
  FILTER_ORG_LEVEL   VARCHAR2(1),
  FILTER_ORG_ID      NUMBER(15)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_FILTER_ORG  IS 'This table contains the Organization Hierarchy LOV filtering access information for a User Security Group'
/

COMMENT ON COLUMN UPG_FILTER_ORG.SEC_GROUP_ID IS 'ID of the User Security group'
/

COMMENT ON COLUMN UPG_FILTER_ORG.FILTER_ORG_LEVEL IS 'The Organization hierarchy level assigned to the User Security Group.  Valid values are contained in the CODE_DETIAL table with a CODE_TYPE of FLOW.'
/

COMMENT ON COLUMN UPG_FILTER_ORG.FILTER_ORG_ID IS 'ID of the Organization hierarchy level assigned to the User Security Group.'
/

