--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_SEC_GROUP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_SEC_GROUP'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_SEC_GROUP';
   end if;
END;
/

PROMPT Creating Table 'UPG_SEC_GROUP'
CREATE TABLE UPG_SEC_GROUP
 (GROUP_ID     NUMBER(4),
  GROUP_NAME   VARCHAR2(40),
  ROLE         VARCHAR2(30),
  COMMENTS     VARCHAR2(2000)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_SEC_GROUP IS 'This table will be used to store group attributes.'
/

COMMENT ON COLUMN UPG_SEC_GROUP.GROUP_ID IS 'Contains the unique identifier associated with the group.'
/

COMMENT ON COLUMN UPG_SEC_GROUP.GROUP_NAME IS 'Contains the name of the security group.'
/

COMMENT ON COLUMN UPG_SEC_GROUP.ROLE IS 'This field contains the role that a client wants to assign to this group.  This field is referenced in the code type ROLE.  There are no pre-defined values for this field and it is completely user-defined.  This field does not have any functionality linking it to Oracle Roles or any other type of roles used within the RMS.  This field is used within the regionality dialogue for searching and reporting capabilities.'
/

COMMENT ON COLUMN UPG_SEC_GROUP.COMMENTS IS 'Comments.'
/

