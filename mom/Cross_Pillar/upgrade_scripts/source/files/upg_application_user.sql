--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UPG_APPLICATION_USER
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
   L_table_exists NUMBER := 0;   
BEGIN
   SELECT count(*) 
     INTO L_table_exists
     FROM USER_OBJECTS
    WHERE OBJECT_NAME = 'UPG_APPLICATION_USER'
      AND OBJECT_TYPE = 'TABLE';

   if (L_table_exists != 0) then
      EXECUTE IMMEDIATE 'DROP TABLE UPG_APPLICATION_USER';
   end if;
END;
/

PROMPT Creating Table 'UPG_APPLICATION_USER'
CREATE TABLE UPG_APPLICATION_USER
 (APPLICATION_USER_ID   VARCHAR2(30),
  RMS_USER_IND          VARCHAR2(1),
  RESA_USER_IND         VARCHAR2(1),
  REIM_USER_IND         VARCHAR2(1),
  ALLOCATION_USER_IND   VARCHAR2(1),
  MANAGER               VARCHAR2(30)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UPG_APPLICATION_USER is 'This is the temporary staging table for SEC_USER upgrade.'
/

COMMENT ON COLUMN UPG_APPLICATION_USER.APPLICATION_USER_ID IS 'This field holds the login id used in the ADF RMS application screen.'
/

COMMENT ON COLUMN UPG_APPLICATION_USER.RMS_USER_IND IS 'This field will indicate whether the user is a RMS User. Valid values are Y/N.'
/

COMMENT ON COLUMN UPG_APPLICATION_USER.RESA_USER_IND IS 'This field will indicate whether the user is a ReSA User. Valid values are Y/N.'
/

COMMENT ON COLUMN UPG_APPLICATION_USER.REIM_USER_IND IS 'This field will indicate whether the user is a ReIM User. Valid values are Y/N.'
/

COMMENT ON COLUMN UPG_APPLICATION_USER.ALLOCATION_USER_IND IS 'This field will indicate whether the user is an Allocation User. Valid values are Y/N.'
/

COMMENT ON COLUMN UPG_APPLICATION_USER.MANAGER IS 'This field contains the manager for the user. This should reference a valid value in DATABASE_USER_ID or APPLICATION_USER_ID.'
/
