#!/bin/ksh

# Common functions library and Configurations

# Script variables

logFile=${LOG_FILE}
errorDir=${ERROR_DIR}
fileDir=${FILE_DIR}
processedDir=${PROCESSED_DIR}

connectStr=${UP}

# Following files are generated after parsing security.dat

seqNoFile=${PROCESSED_DIR}/seqNo.dat
rtkroleFile=${PROCESSED_DIR}/rtk_role.dat
applicationUserFile=${PROCESSED_DIR}/application_user.dat
applicationUserRoleFile=${PROCESSED_DIR}/sec_user_role.dat
groupFile=${PROCESSED_DIR}/sec_group.dat
userGroupFile=${PROCESSED_DIR}/sec_user_group.dat
filterMerchFile=${PROCESSED_DIR}/filter_merch.dat
filterOrgFile=${PROCESSED_DIR}/filter_org.dat
secLocFile=${PROCESSED_DIR}/sec_loc_matrix.dat

connectStr=${UP}

function GET_GROUP_ID
{
   echo "SET TERM OFF;
      SET PAGES 0;
      SET HEADING OFF;
      SET FEEDBACK OFF;
      SET VERIFY OFF;
      SET TRIMSPOOL ON;

      spool ${seqNoFile}

      select to_char(sec_group_sequence.nextval) from dual;
      " | sqlplus -s ${connectStr} > /dev/null

      return $?
}

function PARSE_SECURITY_FILE
{
   # cleanup all existing dat files

   rm ${seqNoFile} 2> /dev/null
   rm ${rtkroleFile} 2> /dev/null
   rm ${applicationUserFile} 2> /dev/null
   rm ${applicationUserRoleFile} 2> /dev/null
   rm ${groupFile} 2> /dev/null
   rm ${userGroupFile} 2> /dev/null
   rm ${filterMerchFile} 2> /dev/null
   rm ${filterOrgFile} 2> /dev/null
   rm ${secLocFile} 2> /dev/null

   groupNo=""

   # Read through each line in the security file.
   # Append to each dat file depending on the line header.
   OLDIFS="${IFS}"
   IFS=$'\n'
   for fileLine in `cat ${fileDir}/security.dat`
   do
      IFS="${OLDIFS}"
      lineHead=$(echo "${fileLine}"| cut -f2 -d '|')
      if [[ "${lineHead}" = "RTK_ROLE" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|""${newLine}" >> ${rtkroleFile}
      elif [[ "${lineHead}" = "SEC_USER" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|""${newLine}" >> ${applicationUserFile}
      elif [[ "${lineHead}" = "SEC_USER_ROLE" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|""${newLine}" >> ${applicationUserRoleFile}
      elif [[ "${lineHead}" = "SEC_GROUP" ]]; then
         groupNo=$(echo "${fileLine}" | cut -f3 -d '|')
         if [[ "${groupNo}" = "" ]]; then
            GET_GROUP_ID
            STATUS=$?

            if [[ ${STATUS} != 0 ]]; then
               echo "Unable to generate GROUP_ID sequence." >> ${logFile}
               exit 1
            else
               groupNo=`cat ${seqNoFile}`
               newLine=$(echo $(echo "${fileLine}" | awk -v var=${groupNo} -F '|' -vOFS='|' '{$3 = var; print}') | cut -d '|' -f3-)
               echo "|""${newLine}" >> ${groupFile}
            fi
         fi
      elif [[ "${lineHead}" = "SEC_USER_GROUP" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|${groupNo}|${newLine}" >> ${userGroupFile}
      elif [[ "${lineHead}" = "FILTER_MERCH" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|${groupNo}|${newLine}" >> ${filterMerchFile}
      elif [[ "${lineHead}" = "FILTER_ORG" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|${groupNo}|${newLine}" >> ${filterOrgFile}
      elif [[ "${lineHead}" = "SEC_LOC_MATRIX" ]]; then
         newLine=$(echo "${fileLine}" | cut -d '|' -f3-)
         echo "|${groupNo}|${newLine}" >> ${secLocFile}
      fi
   done

   return $?
}

function TRANSFORM_SECURITY
{
   echo "set feedback off;
   set heading off;
   set long 100000

   VARIABLE GV_return_code    NUMBER;
   VARIABLE GV_script_error   CLOB; 

   EXEC :GV_return_code := 0;
   EXEC :GV_script_error := NULL;

   WHENEVER SQLERROR EXIT 1
   DECLARE
      L_error_message             CLOB;
      ERROR_IN_FUNCTION EXCEPTION;

   BEGIN

      if DATA_MIGRATION_SQL.SECURITY_UPGRADE(L_error_message) = FALSE then
         :GV_script_error := L_error_message;
         raise ERROR_IN_FUNCTION;
      else
         :GV_script_error := NULL;
         COMMIT;
      end if;

   EXCEPTION
    when ERROR_IN_FUNCTION then
       ROLLBACK;
       :GV_return_code := 1;
    when OTHERS then
       ROLLBACK;
       :GV_script_error := SQLERRM;
       :GV_return_code  := 1;
   END;
   /
   print :GV_script_error;
   exit  :GV_return_code;

   /" | sqlplus -s ${connectStr} > $errorDir/security.err

   return $?

}

if [ -f $errorDir/security.bad ]; then
   #Removing bad file if any.
   rm $errorDir/security.bad
fi
if [ -f $errorDir/security.dsc ]; then
   #Removing discard file if any.
   rm $errorDir/security.dsc
fi
      
PARSE_SECURITY_FILE
RETVAL=0

if [ -f $rtkroleFile ]; then
  if [ `ls $rtkroleFile | wc -l` -gt 0 ]; then
     sqlldr ${connectStr} silent=feedback,header control=$fileDir/rtk_role.ctl log=${LOG_DIR}/rtk_role.log data=$rtkroleFile bad=$errorDir/rtk_role.bad discard=$errorDir/rtk_role.dsc 
  
     STATUS=$?
     if [ $STATUS != 0 ]; then
         echo "Error while loading data for $rtkroleFile. Check bad/dsc file"
         echo "Error while loading data for $rtkroleFile. Check bad/dsc file" >> ${logFile}
         RETVAL=$STATUS
     fi
  fi
fi

if [ -f $applicationUserFile ]; then
   if [ `ls $applicationUserFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/application_user.ctl log=${LOG_DIR}/application_user.log data=$applicationUserFile bad=$errorDir/application_user.bad discard=$errorDir/application_user.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $applicationUserFile. Check bad/dsc file"
         echo "Error while loading data for $applicationUserFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ -f $applicationUserRoleFile ]; then
   if [ `ls $applicationUserRoleFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/sec_user_role.ctl log=${LOG_DIR}/sec_user_role.log data=$applicationUserRoleFile bad=$errorDir/sec_user_role.bad discard=$errorDir/sec_user_role.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $applicationUserRoleFile. Check bad/dsc file"
         echo "Error while loading data for $applicationUserRoleFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ -f $groupFile ]; then
   if [ `ls $groupFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/sec_group.ctl log=${LOG_DIR}/sec_group.log data=$groupFile bad=$errorDir/sec_group.bad discard=$errorDir/sec_group.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $groupFile. Check bad/dsc file"
         echo "Error while loading data for $groupFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ -f $userGroupFile ]; then
   if [ `ls $userGroupFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/sec_user_group.ctl log=${LOG_DIR}/sec_user_group.log data=$userGroupFile bad=$errorDir/sec_user_group.bad discard=$errorDir/sec_user_group.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $userGroupFile. Check bad/dsc file"
         echo "Error while loading data for $userGroupFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ -f $filterMerchFile ]; then
   if [ `ls $filterMerchFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/filter_merch.ctl log=${LOG_DIR}/filter_merch.log data=$filterMerchFile bad=$errorDir/filter_merch.bad discard=$errorDir/filter_merch.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $filterMerchFile. Check bad/dsc file"
         echo "Error while loading data for $filterMerchFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ -f $filterOrgFile ]; then
   if [ `ls $filterOrgFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/filter_org.ctl log=${LOG_DIR}/filter_org.log data=$filterOrgFile bad=$errorDir/filter_org.bad discard=$errorDir/filter_org.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $filterOrgFile. Check bad/dsc file"
         echo "Error while loading data for $filterOrgFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ -f $secLocFile ]; then
   if [ `ls $secLocFile | wc -l` -gt 0 ]; then
      sqlldr ${connectStr} silent=feedback,header control=$fileDir/sec_loc_matrix.ctl log=${LOG_DIR}/sec_loc_matrix.log data=$secLocFile bad=$errorDir/sec_loc_matrix.bad discard=$errorDir/sec_loc_matrix.dsc 
   
      STATUS=$?
      if [ $STATUS != 0 ]; then
         echo "Error while loading data for $secLocFile. Check bad/dsc file"
         echo "Error while loading data for $secLocFile. Check bad/dsc file" >> ${logFile} 
         RETVAL=$STATUS
      fi
   fi
fi

if [ $RETVAL != 0 ]; then
   echo "Errors encountered while loading the security.dat file. Please correct security.dat before continuing."
   echo "Errors encountered while loading the security.dat file. Please correct security.dat before continuing." >> ${logFile} 
   exit 1
fi

TRANSFORM_SECURITY
STATUS=$? 

if [ $STATUS != 0 ]; then
   echo "TRANSFORM_SECURITY function was not successfully processed for the records successfully loaded by sqlloader."
   echo "TRANSFORM_SECURITY function was not successfully processed for the records successfully loaded by sqlloader." >> ${logFile} 
   exit 1
else                                                                                        
   if [ `grep "^ORA-" $errorDir/security.err  | wc -l` -gt 0 ] || [ `grep "^SP2-*" $errorDir/security.err | wc -l` -gt 0 ]
   then
      echo "ORA error while executing function TRANSFORM_SECURITY."
      echo "ORA error while executing function TRANSFORM_SECURITY." >> ${logFile}
      exit 1
   else 
      if [ -f $errorDir/security.bad ] || [ -f $errorDir/security.dsc ]; then
         rm $errorDir/security.err
         exit 1
      else
         #Removing error file if success.
         rm $errorDir/security.err
         echo "TRANSFORM_SECURITY successfully processed"
         echo "TRANSFORM_SECURITY successfully processed" >> ${logFile}
      fi
   fi
fi
