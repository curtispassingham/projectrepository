#Function to execute the DATA migration scripts.
function DATA_MIGRATION
{
echo "**********************"
echo "DATA MIGRATION started"
echo "**********************" >> $LOG_FILE
echo "DATA MIGRATION started" >> $LOG_FILE

if [ -f $FILE_DIR/TRANSFORM.dat ]; then
   RECCNT=`cat $FILE_DIR/TRANSFORM.dat | wc -l`
   if [ $RECCNT = 0 ]; then
      echo "There is nothing to upgrade for DATA MIGRATION"
      echo "There is nothing to upgrade for DATA MIGRATION" >> $LOG_FILE
      return 0
   fi
fi

if [ -f $FILE_DIR/UPGRADE_DBC.dat ]; then
   RECCNT=`cat $FILE_DIR/UPGRADE_DBC.dat | wc -l`
   if [ $RECCNT != 0 ]; then
      for db_file in `cat $FILE_DIR/UPGRADE_DBC.dat`
      do
         ERR_FILE=`echo $db_file | cut -d "." -f1`
         if [ -f $FILE_DIR/$db_file ]; then
            ## Check if already processed
            if [ -f $PROCESSED_DIR/$db_file.processed ]; then
               echo "File $db_file has already been processed"
               echo "File $db_file has already been processed" >> $LOG_FILE
            else
               echo "Executing file $db_file"
               echo "Executing file $db_file" >> $LOG_FILE
               SCHEMA_OWNER=`echo $SCHEMA_OWNER | tr '[a-z]' '[A-Z]'`
               $ORACLE_HOME/bin/sqlplus -s $UP <<EOF > $ERROR_DIR/$ERR_FILE.err
               @"$FILE_DIR/$db_file" $SCHEMA_OWNER;
EOF

               if [ `grep "^ORA-" $ERROR_DIR/$ERR_FILE.err | wc -l` -gt 0 ] || [ `grep "^SP2-*" $ERROR_DIR/$ERR_FILE.err | wc -l` -gt 0 ]; then
                  echo "ORA Error while executing $db_file"
                  echo "ORA Error while executing $db_file" >> $LOG_FILE
                  exit 1
               else
                  rm -f $ERROR_DIR/$ERR_FILE.err
                  touch $PROCESSED_DIR/$db_file.processed
                  echo "File $db_file was successfully executed"
                  echo "File $db_file was successfully executed" >> $LOG_FILE
               fi
            fi
         else
            echo "File $db_file is not available in the $FILE_DIR directory to be executed"
            echo "File $db_file is not available in the $FILE_DIR directory to be executed" >> $LOG_FILE
            exit 1
         fi
      done
   fi
fi

cat $PATCH_DIR/files/TRANSFORM.dat | while read transform_script
do
   #Check if the file has been processed.
   if [ -f $PROCESSED_DIR/$transform_script.processed ];
   then
      echo  "File $transform_script has already been processed"
      echo  "File $transform_script has already been processed" >> $LOG_FILE
   elif [ -f $DATA_DIR/$transform_script ];
   then
      echo "Executing file $transform_script"
      echo "Executing file $transform_script" >> $LOG_FILE
      . $DATA_DIR/$transform_script
      STATUS=$?
      if [ $STATUS = 0 ];
      then
         touch $PROCESSED_DIR/$transform_script.processed
         echo "Transformation script $transform_script was succesfully executed."
      else
         echo  "Transformation script <<$transform_script>> execution error"
         echo "Transformation script <<$transform_script>> execution error" >> $LOG_FILE
         exit 1
      fi
   else
      echo "File $transform_script is not available in $DATA_DIR for execution." >> $LOG_FILE
   fi
done

STATUS=$?
if [ $STATUS = 0 ]; then
   echo "DATA MIGRATION completed"
   echo "************************"
   echo "DATA MIGRATION completed" >> $LOG_FILE
   echo "************************" >> $LOG_FILE
else
   exit 1
fi
}

#-------------------------------------------------------------------------
### Main execution part starts here. ###
#-------------------------------------------------------------------------

if [ $# -lt 1 ]; then
  echo "*****************************PARAMETERS***********************************************"
  echo "* 1. Run prevalidation scripts for data upgrade run <script name> PREVALIDATION      *"
  echo "* 2. Upgrade data run <script name> UPGRADE                                          *"
  echo "**************************************************************************************"
  exit 1
fi

#Execution of configuration file to set env variable.
. ./controller.cfg

STATUS=$?
if [ $STATUS != 0 ]; then
   echo  "Configuration error"
   echo "Configuration error" >> $LOG_FILE
   exit 1
fi

if [ $1 = "UPGRADE" ]; then
   DATA_MIGRATION
   STATUS=$?

   if [ $STATUS != 0 ]; then
     echo  "Error in executing  Data migration Scripts."
     echo  "Error in executing Data migration Scripts." >> $LOG_FILE
     exit 1
   fi

elif [ $1 = "PREVALIDATION" ]; then
    . $UTILITY_DIR/prevalidation.ksh
    STATUS=$?
    if [ $STATUS != 0 ]; then
       exit 1
    fi

elif [ $1 = "CLEANUP" ]; then

   cat  $UTILITY_DIR/CLEANUP.dat |while read ROW
   do
      ERR_FILE=$ROW
      OBJ_NAME=`echo $ROW |cut -d " " -f1`
      echo "Executing file $OBJ_NAME" >> $LOG_FILE
      SCHEMA_OWNER=`echo $SCHEMA_OWNER | tr '[a-z]' '[A-Z]'`
      $ORACLE_HOME/bin/sqlplus -s $UP <<EOF > $ERROR_DIR/$ERR_FILE.err
      @"$UTILITY_DIR/$OBJ_NAME" $SCHEMA_OWNER;
EOF

   if [ `grep "^ORA-" $ERROR_DIR/$ERR_FILE.err | wc -l` -gt 0 ] || [ `grep "^SP2-*" $ERROR_DIR/$ERR_FILE.err | wc -l` -gt 0 ];
   then
         error=`grep "^ORA-" $ERROR_DIR/$ERR_FILE.err` || `grep "^SP2-*" $ERROR_DIR/$ERR_FILE.err`
         echo "$error while executing $OBJ_NAME"
         echo "$error while executing $OBJ_NAME" >> $LOG_FILE
         exit 1
   else
      rm -f $ERROR_DIR/$ERR_FILE.err
      touch $PROCESSED_DIR/$OBJ_NAME.processed
      echo "File $OBJ_NAME was successfully executed"
      echo "File $OBJ_NAME was successfully executed" >> $LOG_FILE
   fi

   done


  STATUS=$?
  if [ $STATUS != 0 ]; then
     echo " Error in executing cleanup scripts"
     echo " Error in executing cleanup scripts" >> $LOG_FILE
     exit 1
  fi
else
   echo  "Invalid upgrade parameter"
   echo  "Invalid upgrade parameter." >> $LOG_FILE
   exit 1
fi

exit 0

