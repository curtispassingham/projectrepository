CREATE OR REPLACE PACKAGE L10N_ENTITY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
TYPE TYP_key_val_rec is RECORD
(
    l10n_ext_table EXT_ENTITY.L10N_EXT_TABLE%TYPE,
    base_rms_table EXT_ENTITY.BASE_RMS_TABLE%TYPE,
    key_col        EXT_ENTITY_KEY.KEY_COL%TYPE,
    key_number     EXT_ENTITY_KEY.KEY_NUMBER%TYPE,
    data_type      EXT_ENTITY_KEY.DATA_TYPE%TYPE,
    key_desc       EXT_ENTITY_KEY_DESCS.KEY_DESC%TYPE,
    key_val       VARCHAR2(60)
);

TYPE TBL_key_val_tbl is TABLE of TYP_key_val_rec INDEX BY VARCHAR2(25);

GP_key_val_tbl   TBL_key_val_tbl;
---
TYPE TYP_attr_val_rec is RECORD
(
    col_name     l10n_attrib.view_col_name%TYPE,
    data_type    l10n_attrib.data_type%TYPE,
    value_req    l10n_attrib.value_req%TYPE,
    attrib_value VARCHAR2(250),
    attrib_desc  l10n_attrib_descs.description%TYPE
);

TYPE TBL_attr_val_rec is TABLE of TYP_attr_val_rec INDEX BY VARCHAR2(25);
GP_attr_val_tbl   TBL_attr_val_rec;
-------------------------------------------------------------------------------------
GP_error_message RTK_ERRORS.RTK_TEXT%TYPE;
GP_return BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : BUILD_L10N_EXT_KEY
-- Purpose       : This function takes in either the base RMS table or the ext table
--                 and build the ext table keys as fetch from the key tables and the input
--                 values. Note the order of the key does not matter in this function.
--------------------------------------------------------------------------------------
FUNCTION BUILD_L10N_EXT_KEY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_key_tbl        IN OUT  TBL_KEY_VAL_TBL,
                            I_base_table     IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE DEFAULT NULL,
                            I_l10n_ext_table IN      EXT_ENTITY.L10N_EXT_TABLE%TYPE DEFAULT NULL,
                            I_key_name_1     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_1      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_2     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_2      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_3     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_3      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_4     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_4      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_5     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_5      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_6     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_6      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_7     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_7      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_8     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_8      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_9     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_9      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_10    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_10     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_11    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_11     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_12    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_12     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_13    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_13     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_14    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_14     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_15    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_15     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_16    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_16     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_17    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_17     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_18    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_18     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_19    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_19     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_20    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_20     IN      VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : CHECK_REQ_EXT
-- Purpose       : This function validate that all required fields are populated.
--------------------------------------------------------------------------------------
FUNCTION CHECK_REQ_EXT(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_base_table    IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                       I_country       IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                       I_key_tbl       IN      TBL_KEY_VAL_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : CHECK_REQ_EXT_WRP
-- Purpose       : This function is a wrapper for CHECK_REQ_EXT and uses DB type
--                 to allow calls from ADF.
--------------------------------------------------------------------------------------
FUNCTION CHECK_REQ_EXT_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_base_table      IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                           I_country         IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                           I_key_tbl         IN       KEY_VAL_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------------
-- Function Name : BUILD_L10N_EXT_KEY_WRP
-- Purpose       : This function is a wrapper for BUILD_L10N_EXT_KEY and uses DB type
--                 to allow calls from ADF.
--------------------------------------------------------------------------------------
FUNCTION BUILD_L10N_EXT_KEY_WRP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_key_tbl          IN OUT   KEY_VAL_TBL,
                                I_base_table       IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE DEFAULT NULL,
                                I_l10n_ext_table   IN       EXT_ENTITY.L10N_EXT_TABLE%TYPE DEFAULT NULL,
                                I_key_name_1       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_1        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_2       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_2        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_3       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_3        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_4       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_4        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_5       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_5        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_6       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_6        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_7       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_7        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_8       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_8        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_9       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_9        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_10      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_10       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_11      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_11       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_12      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_12       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_13      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_13       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_14      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_14       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_15      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_15       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_16      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_16       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_17      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_17       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_18      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_18       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_19      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_19       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_20      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_20       IN       VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------------
END L10N_ENTITY_SQL;
/