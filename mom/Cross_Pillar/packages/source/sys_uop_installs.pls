CREATE OR REPLACE PACKAGE SYS_UNIT_OPTIONS_SQL_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------
FUNCTION SYS_UOP_TABLES(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_prim_currency      IN       VARCHAR2,
                        I_default_tax_type   IN       VARCHAR2,
                        I_vat_class_ind      IN       VARCHAR2,
                        I_bracket_cost_ind   IN       VARCHAR2,
                        I_table_owner        IN       VARCHAR2,
                        I_base_country_id    IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
END SYS_UNIT_OPTIONS_SQL_SQL;
/
