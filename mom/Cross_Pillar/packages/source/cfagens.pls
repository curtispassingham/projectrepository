---------------------------------------------------------------------------------------------------
-- This package is used to generate the custome extended entity objects.
---------------------------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE CFA_GEN_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
TYPE TABLE_TBL IS TABLE OF ALL_TAB_COLUMNS.TABLE_NAME%TYPE INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------  
-- Function name:  STGS_EXISTS 
-- Purpose      :  This function find if the entity or group set staging table exist.
---------------------------------------------------------------------------------------------------
FUNCTION STGS_EXISTS (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_ext_entity_id  IN      CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE,
	                    I_group_set_id   IN      CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------                       
END CFA_GEN_SQL;
/