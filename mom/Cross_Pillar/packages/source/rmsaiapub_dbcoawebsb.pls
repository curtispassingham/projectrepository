CREATE OR REPLACE
PACKAGE BODY AIA_WEBSERVICE_SQL AS
--------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------------------------
FUNCTION GET_DRILL_FORWARD_URL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_psft_url             IN OUT   VARCHAR2,
                               I_reference_trace_id   IN       KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(50)  := 'AIA_WEBSERVICE_SQL.GET_DRILL_FORWARD_URL';
   L_ws_url                   RETAIL_SERVICE_REPORT_URL.URL%TYPE;
   L_sys_account              VARCHAR2(50);
   L_wsm                      VARCHAR2(100) := 'oracle.webservices.dii.interceptor.pipeline.port.config';
   L_config                   VARCHAR2(1000) := NULL;
   L_username                 VARCHAR2(25);
   L_password                 VARCHAR2(25);

   /*secure service variables*/
   L_keystoreName             RETAIL_SERVICE_REPORT_URL.KEYSTORE_NAME%TYPE;
   L_keystorePath             RETAIL_SERVICE_REPORT_URL.KEYSTORE_PATH%TYPE;
   L_keypassword              VARCHAR2(50);
   L_keyalias                 RETAIL_SERVICE_REPORT_URL.PRIVATE_KEY_ALIAS%TYPE;
   L_keyaliaspassword         VARCHAR2(50);

   L_OBJ_PAIR                 OBJ_PAIR;
   L_TBL_OBJ_PAIR             TBL_OBJ_PAIR;
   --
   L_S1                       VARCHAR2(256) := 'RETAIL';
   L_S2                       VARCHAR2(256) := 'PSFT';
   --
   L_ParameterName            VARCHAR2(256) := 'ACCOUNTING_ENTRY';
   L_ParameterValue           VARCHAR2(256) := I_reference_trace_id;
   --
   L_OBJ_REFERENCEID          OBJ_REFERENCEID;
   L_OBJ_INVOCATIONSUCCESS    OBJ_INVOCATIONSUCCESS;
   --

   cursor C_GET_AIA_URL is
      select url,
             decrypt_data(sys_account) sys_account,
             keystore_name,
             keystore_path,
             decrypt_data(keystore_passwd),
             private_key_alias,
             decrypt_data(private_key_alias_passwd)
        from retail_service_report_url
       where rs_code = 'RDF';

BEGIN

   open C_GET_AIA_URL;
   fetch C_GET_AIA_URL into L_ws_url,
                            L_sys_account,
                            L_keystoreName,
                            L_keystorePath,
                            L_keypassword,
                            L_keyalias,
                            L_keyaliaspassword;

   if C_GET_AIA_URL%NOTFOUND then
      close C_GET_AIA_URL;
      O_error_message := SQL_LIB.CREATE_MSG('NO_RDF_SERV_CODE',NULL,NULL,NULL);
      return FALSE;
   end if;

   close C_GET_AIA_URL;
   --
   L_OBJ_PAIR := OBJ_PAIR(L_ParameterName,
                          L_ParameterValue);

   L_TBL_OBJ_PAIR := TBL_OBJ_PAIR();
   L_TBL_OBJ_PAIR.extend;
   L_TBL_OBJ_PAIR(L_TBL_OBJ_PAIR.count) := L_OBJ_PAIR;
   --
   L_OBJ_REFERENCEID := OBJ_REFERENCEID(L_TBL_OBJ_PAIR,
                                        L_S1);
   --
   DrillBackForwardUrlServiceCons.setEndpoint(L_ws_url);

   /*Set system properties for secure service call*/
   if L_keystorename is NOT NULL then

      L_config := '<port-info>
                  <runtime enabled="security">
                  <security>
                  <key-store name="' ||L_keystorename || '" type="JKS" path="' || L_keystorepath || '" store-pass="' || L_keypassword || '" />
                  <signature-key alias="' || L_keyalias || '" key-pass="' || L_keyaliaspassword || '" />
                  <outbound>
                  <username-token name="" password="" add-created="true" add-nonce="true" />
                  <signature>
                  <signature-method>RSA-SHA1</signature-method>
                  <add-timestamp created="true" expiry="28800"/>
                  </signature>
                  </outbound>
                  </security>
                  </runtime>
                  </port-info>';

      DrillBackForwardUrlServiceCons.setSystemProperty('javax.net.ssl.trustStoreType','JKS');
      DrillBackForwardUrlServiceCons.setSystemProperty('javax.net.ssl.trustStorePassword',L_keypassword);
      GlAccountValidationServiceCons.setSystemProperty('javax.net.debug','all');
      DrillBackForwardUrlServiceCons.setSystemProperty('javax.net.ssl.trustStore',L_keystorepath);

   else /*unsecure service call*/

      L_config := '<port-info><runtime></runtime></port-info>';

   end if;

   DrillBackForwardUrlServiceCons.setProperty(L_wsm, L_config);


   if L_sys_account is NOT NULL then

      L_username  := substr(L_sys_account,1,instr(L_sys_account,'/')-1);
      L_password  := substr(L_sys_account,instr(L_sys_account,'/')+1);

      DrillBackForwardUrlServiceCons.setUsername(L_username);
      DrillBackForwardUrlServiceCons.setPassword(L_password);
   end if;

   L_OBJ_INVOCATIONSUCCESS := DrillBackForwardUrlServiceCons.getDrillBackForwardUrl(L_OBJ_REFERENCEID);
   O_psft_url := L_OBJ_INVOCATIONSUCCESS.success_message_;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DRILL_FORWARD_URL;
-----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ACCOUNT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_requesting_system    IN OUT   VARCHAR2,
                          O_set_of_books_id      IN OUT   ORG_UNIT.SET_OF_BOOKS_ID%TYPE,
                          O_ccid                 IN OUT   FIF_GL_CROSS_REF.DR_CCID%TYPE,
                          O_segment1             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment2             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment3             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment4             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment5             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment6             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment7             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment8             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment9             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment10            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment11            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment12            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment13            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment14            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment15            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment16            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment17            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment18            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment19            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment20            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_account_status       IN OUT   VARCHAR2,
                          I_requesting_system    IN       VARCHAR2,
                          I_set_of_books_id      IN       ORG_UNIT.SET_OF_BOOKS_ID%TYPE,
                          I_ccid                 IN       FIF_GL_CROSS_REF.DR_CCID%TYPE,
                          I_segment1             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment2             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment3             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment4             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment5             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment6             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment7             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment8             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment9             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment10            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment11            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment12            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment13            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment14            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment15            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment16            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment17            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment18            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment19            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment20            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'AIA_WEBSERVICE_SQL.VALIDATE_ACCOUNT';
   L_ws_url               RETAIL_SERVICE_REPORT_URL.URL%TYPE;
   L_sys_account          VARCHAR2(50);
   L_wsm                  VARCHAR2(100)  := 'oracle.webservices.dii.interceptor.pipeline.port.config';
   L_config               VARCHAR2(1000) := NULL;
   L_username             VARCHAR2(25);
   L_password             VARCHAR2(25);

   /*secure service variables*/
   L_keystoreName         RETAIL_SERVICE_REPORT_URL.KEYSTORE_NAME%TYPE;
   L_keystorePath         RETAIL_SERVICE_REPORT_URL.KEYSTORE_PATH%TYPE;
   L_keypassword          VARCHAR2(50);
   L_keyalias             RETAIL_SERVICE_REPORT_URL.PRIVATE_KEY_ALIAS%TYPE;
   L_keyaliaspassword     VARCHAR2(50);

   L_glacctcolref         OBJ_GLACCTCOLREF;
   L_glacctdesc           OBJ_GLACCTDESC;
   L_glacctcoldesc        OBJ_GLACCTCOLDESC;
   L_glacctdesc_TBL       TBL_OBJ_GLACCTDESC;

   cursor C_GET_AIA_URL is
      select url,
             decrypt_data(sys_account) sys_account,
             keystore_name,
             keystore_path,
             decrypt_data(keystore_passwd),
             private_key_alias,
             decrypt_data(private_key_alias_passwd)
        from retail_service_report_url
       where rs_code = 'RAV';

BEGIN

   open C_GET_AIA_URL;
   fetch C_GET_AIA_URL into L_ws_url,
                            L_sys_account,
                            L_keystoreName,
                            L_keystorePath,
                            L_keypassword,
                            L_keyalias,
                            L_keyaliaspassword;

   if C_GET_AIA_URL%NOTFOUND then
      close C_GET_AIA_URL;
      O_error_message := SQL_LIB.CREATE_MSG('NO_RAV_SERV_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_AIA_URL;

   L_glacctdesc := OBJ_GLACCTDESC(I_requesting_system,
                                  I_set_of_books_id,
                                  I_ccid,
                                  I_segment1,
                                  I_segment2,
                                  I_segment3,
                                  I_segment4,
                                  I_segment5,
                                  I_segment6,
                                  I_segment7,
                                  I_segment8,
                                  I_segment9,
                                  I_segment10,
                                  I_segment11,
                                  I_segment12,
                                  I_segment13,
                                  I_segment14,
                                  I_segment15,
                                  I_segment16,
                                  I_segment17,
                                  I_segment18,
                                  I_segment19,
                                  I_segment20);

   L_glacctdesc_TBL    := TBL_OBJ_GLACCTDESC();
   L_glacctdesc_TBL.extend;

   L_glacctdesc_TBL(1) := L_glacctdesc;
   L_glacctcoldesc     := OBJ_GLACCTCOLDESC(1, L_glacctdesc_TBL);

   /*Set system properties for secure service call*/
   if L_keystorename is NOT NULL then

      L_config := '<port-info>
                  <runtime enabled="security">
                  <security>
                  <key-store name="' ||L_keystorename || '" type="JKS" path="' || L_keystorepath || '" store-pass="' || L_keypassword || '" />
                  <signature-key alias="' || L_keyalias || '" key-pass="' || L_keyaliaspassword || '" />
                  <outbound>
                  <username-token name="" password=""  add-created="true" add-nonce="true" />
                  <signature>
                  <signature-method>RSA-SHA1</signature-method>
                  <add-timestamp created="true" expiry="28800"/>
                  </signature>
                  </outbound>
                  </security>
                  </runtime>
                  </port-info>';

      GlAccountValidationServiceCons.setSystemProperty('javax.net.ssl.trustStoreType','JKS');
      GlAccountValidationServiceCons.setSystemProperty('javax.net.ssl.trustStorePassword',L_keypassword);
      GlAccountValidationServiceCons.setSystemProperty('javax.net.debug','all');
      GlAccountValidationServiceCons.setSystemProperty('javax.net.ssl.trustStore',L_keystorepath);

   else /*unsercure service call*/

      L_config := '<port-info><runtime></runtime></port-info>';

   end if;

   GlAccountValidationServiceCons.setProperty(L_wsm, L_config);

   /*Substring the system_account to get the username and password.*/
   if L_sys_account is NOT NULL then

      L_username  := substr(L_sys_account,1,instr(L_sys_account,'/')-1);
      L_password  := substr(L_sys_account,instr(L_sys_account,'/')+1);

      GlAccountValidationServiceCons.setUsername(L_username);
      GlAccountValidationServiceCons.setPassword(L_password);
   end if;

   GlAccountValidationServiceCons.setEndpoint(L_ws_url);

   L_glacctcolref := GlAccountValidationServiceCons.validateGlAccount(L_glacctcoldesc);

   O_requesting_system := L_glacctcolref.gLAcctRef_(1).requesting_system_;
   O_set_of_books_id   := L_glacctcolref.gLAcctRef_(1).set_of_books_id_;
   O_ccid              := L_glacctcolref.gLAcctRef_(1).ccid_;
   O_segment1          := L_glacctcolref.gLAcctRef_(1).segment1_;
   O_segment2          := L_glacctcolref.gLAcctRef_(1).segment2_;
   O_segment3          := L_glacctcolref.gLAcctRef_(1).segment3_;
   O_segment4          := L_glacctcolref.gLAcctRef_(1).segment4_;
   O_segment5          := L_glacctcolref.gLAcctRef_(1).segment5_;
   O_segment6          := L_glacctcolref.gLAcctRef_(1).segment6_;
   O_segment7          := L_glacctcolref.gLAcctRef_(1).segment7_;
   O_segment8          := L_glacctcolref.gLAcctRef_(1).segment8_;
   O_segment9          := L_glacctcolref.gLAcctRef_(1).segment9_;
   O_segment10         := L_glacctcolref.gLAcctRef_(1).segment10_;
   O_segment11         := L_glacctcolref.gLAcctRef_(1).segment11_;
   O_segment12         := L_glacctcolref.gLAcctRef_(1).segment12_;
   O_segment13         := L_glacctcolref.gLAcctRef_(1).segment13_;
   O_segment14         := L_glacctcolref.gLAcctRef_(1).segment14_;
   O_segment15         := L_glacctcolref.gLAcctRef_(1).segment15_;
   O_segment16         := L_glacctcolref.gLAcctRef_(1).segment16_;
   O_segment17         := L_glacctcolref.gLAcctRef_(1).segment17_;
   O_segment18         := L_glacctcolref.gLAcctRef_(1).segment18_;
   O_segment19         := L_glacctcolref.gLAcctRef_(1).segment19_;
   O_segment20         := L_glacctcolref.gLAcctRef_(1).segment20_;
   O_account_status    := L_glacctcolref.gLAcctRef_(1).account_status_;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_ACCOUNT;
-----------------------------------------------------------------------------------------------------
END AIA_WEBSERVICE_SQL;
/
