CREATE OR REPLACE
PACKAGE mthread_restart_sql AUTHID CURRENT_USER
AS
  ------------------------------------------------------------------------------
  FUNCTION start_thread(
      O_error_message    IN OUT rtk_errors.rtk_text%type,
      I_program_name     IN restart_program_status.program_name%type,
      I_restart_name     IN restart_bookmark.restart_name%type,
      I_thread_val       IN restart_bookmark.thread_val%type,
      IO_is_restart      IN OUT BOOLEAN,
      IO_bookmark_params IN OUT VARR)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION commit_thread(
      O_error_message   IN OUT rtk_errors.rtk_text%type,
      I_restart_name    IN restart_bookmark.restart_name%type,
      I_thread_val      IN restart_bookmark.thread_val%type,
      I_bookmark_params IN VARR)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION finish_thread(
      O_error_message  IN OUT rtk_errors.rtk_text%type,
      I_restart_name   IN restart_bookmark.restart_name%type,
      I_thread_val     IN restart_bookmark.thread_val%type,
      I_success_flag   IN CHAR,
      I_NON_FATAL_FLAG IN CHAR,
      I_err_msg        IN restart_program_status.err_message%type)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION wait4jobs(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      IO_jobs_coll    IN OUT VARR,
      I_max_threads   IN NUMBER,
      I_wait_ms       IN NUMBER)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION comma2varr(
      I_str IN VARCHAR2)
    RETURN varr;
  ------------------------------------------------------------------------------
  FUNCTION varr2comma(
      I_varr IN VARR)
    RETURN VARCHAR2;
  ------------------------------------------------------------------------------
  FUNCTION submit_job(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      IO_jobs_coll    IN OUT VARR,
      I_job_name      IN VARCHAR2,
      I_pls_block     IN VARCHAR2,
      I_comments      IN VARCHAR2)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION upd_error(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_restart_name restart_bookmark.restart_name%type,
      I_thread_val IN restart_bookmark.thread_val%type,
      I_err_msg    IN restart_program_status.err_message%type)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  PROCEDURE sleep_ms(
      p_sleep IN NUMBER );
  ------------------------------------------------------------------------------
  FUNCTION check_unfinished(
      O_error_message    IN OUT rtk_errors.rtk_text%type,
      O_unfinished_count IN OUT NUMBER,
      I_restart_name     IN restart_bookmark.restart_name%type)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
END mthread_restart_sql;
/