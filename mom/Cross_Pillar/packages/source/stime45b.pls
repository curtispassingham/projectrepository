
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STANDARD_TIMER_45 AS

----------------------------------------------------------------------

LP_start_time		NUMBER;

----------------------------------------------------------------------

PROCEDURE START_TIMER IS
BEGIN

LP_start_time	:= DBMS_UTILITY.GET_TIME;

END START_TIMER;

----------------------------------------------------------------------

FUNCTION ELAPSED_TIME RETURN NUMBER IS
BEGIN

return ((DBMS_UTILITY.GET_TIME - LP_start_time)/100);

END ELAPSED_TIME;

----------------------------------------------------------------------

END STANDARD_TIMER_45;
/
