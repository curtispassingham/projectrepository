CREATE OR REPLACE PACKAGE L10N_FLEX_ATTRIB_VAL_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
TYPE TYP_attrib_val_rec is RECORD
(   group_id                L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
    view_col_name           L10N_ATTRIB.VIEW_COL_NAME%TYPE,
    data_type               L10N_ATTRIB.DATA_TYPE%TYPE,
    attrib_desc             L10N_ATTRIB_DESCS.DESCRIPTION%TYPE,
    attrib_storage_col      L10N_ATTRIB.ATTRIB_STORAGE_COL%TYPE,
    ui_widget               L10N_ATTRIB.UI_WIDGET%TYPE,
    value_req               L10N_ATTRIB.VALUE_REQ%TYPE,
    maximum_length          L10N_ATTRIB.MAXIMUM_LENGTH%TYPE,
    lowest_allowed_value    L10N_ATTRIB.LOWEST_ALLOWED_VALUE%TYPE,
    highest_allowed_value   L10N_ATTRIB.HIGHEST_ALLOWED_VALUE%TYPE,
    validation_code         L10N_ATTRIB.VALIDATION_CODE%TYPE,
    col_no                  NUMBER,
    field_value             VARCHAR2(250),
    field_desc              VARCHAR2(250),
    attrib_value_char       VARCHAR2(250),
    attrib_value_num        NUMBER,
    attrib_value_date       DATE
);

TYPE TBL_attrib_val_tbl is TABLE of TYP_attrib_val_rec INDEX BY VARCHAR2(25);

LP_attrib_val_tbl   TBL_attrib_val_tbl;
LP_date_format      VARCHAR2(20);

TYPE TYP_l10n_ext_key_rec is RECORD
(
   key_col                 EXT_ENTITY_KEY.KEY_COL%TYPE,
   data_type               EXT_ENTITY_KEY.DATA_TYPE%TYPE,
   key_value               VARCHAR2(60),
   key_value_desc          VARCHAR2(255)
);

TYPE TYP_l10n_ext_key_tbl is TABLE of TYP_l10n_ext_key_rec INDEX BY VARCHAR2(25);

LP_header_cfg_tbl   TYP_l10n_ext_key_tbl;

TYPE TYP_return_fields_rec is RECORD
(  field_name    L10N_ATTRIB.VIEW_COL_NAME%TYPE
);

TYPE TYP_return_fields_tbl is TABLE of TYP_return_fields_rec INDEX by BINARY_INTEGER;
LP_return_fields_tbl   TYP_return_fields_tbl;

--------------------------------------------------------------------------------------
FUNCTION VALIDATE_FIELD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_attrib_name       IN      L10N_ATTRIB.VIEW_COL_NAME%TYPE,
                        I_attrib_value      IN      VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_group_id          IN      L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_base_rms_table    IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                        I_base_ind          IN      L10N_ATTRIB_GROUP.BASE_IND%TYPE DEFAULT NULL)
        
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTITY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_country_id        IN      L10N_EXT_ENTITY_VAL.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION GET_VALIDATE_DATA(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_attrib_data_type     IN OUT  L10N_ATTRIB.DATA_TYPE%TYPE,
                           O_char_value           IN OUT  VARCHAR2,
                           O_num_value            IN OUT  NUMBER,
                           O_date_value           IN OUT  DATE,
                           I_attrib_rec           IN      OBJ_L10N_ATTRIB_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_INIT(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_attrib_cfg_tbl    IN OUT  L10N_FLEX_ATTRIB_SQL.TYP_L10N_ATTRIB_CFG_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION SET_FIELD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   IO_attrib_val_tbl   IN OUT  TBL_attrib_val_tbl,
                   I_attrib_name       IN      L10N_ATTRIB.VIEW_COL_NAME%TYPE,
                   I_attrib_value      IN      VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION SET_OUTPUT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_field              IN       VARCHAR2,
                    I_field_value        IN       VARCHAR2,
                    I_field_desc_value   IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION GET_VALUE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_field_value       IN OUT   VARCHAR2,
                   O_field_desc_value  IN OUT   VARCHAR2,
                   O_data_type         IN OUT   L10N_ATTRIB.DATA_TYPE%TYPE,
                   I_field_name        IN       L10N_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION REFRESH_VALUE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_field_name        IN       L10N_ATTRIB.VIEW_COL_NAME%TYPE,
                       I_field_value       IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION BUILD_KEY_TABLE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FUNC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   L_func_name         IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END L10N_FLEX_ATTRIB_VAL_SQL;
/