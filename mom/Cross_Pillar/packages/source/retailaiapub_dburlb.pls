CREATE OR REPLACE PACKAGE BODY AIA_PUB_REPORTS AS

--------------------------------------------------------------------------------------------------
FUNCTION GET_REPORT_URL (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rpt_url        IN OUT   RETAIL_SERVICE_REPORT_URL.URL%TYPE,
                         I_ref_key        IN       KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(200) := PACKAGE_NAME || '.GET_REPORT_URL';
   L_prefix              VARCHAR2(10);
   L_ref_key             KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE;
   
BEGIN
    L_prefix := substr(I_ref_key,1,instr(I_ref_key,DELIMITER)-1);
    L_ref_key := substr(I_ref_key,instr(I_ref_key,DELIMITER)+2);

    if L_prefix = APPLICATION_REIM then
      if reim_report_url(O_error_message,
                        O_rpt_url,
                        L_ref_key) = FALSE then
         return FALSE;
      end if;
    else 
      if rms_report_url(O_error_message,
                        O_rpt_url,
                        I_ref_key) = FALSE then
         return FALSE;
      end if;
    end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END GET_REPORT_URL;
--------------------------------------------------------------------------------------------------

FUNCTION RMS_REPORT_URL (O_error_message  OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rpt_url        OUT   RETAIL_SERVICE_REPORT_URL.URL%TYPE,
                         I_ref_key        IN    KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(60) := PACKAGE_NAME || '.RMS_REPORT_URL';
   L_ref_trace_type      KEY_MAP_GL.REFERENCE_TRACE_TYPE%TYPE;
   L_url                 RETAIL_SERVICE_REPORT_URL.URL%TYPE;

   cursor C_get_ref_trace_type is
      select reference_trace_type
        from key_map_gl
       where reference_trace_id = I_ref_key;

   cursor C_get_report_url is
      select url
        from retail_service_report_url
       where rs_code = L_ref_trace_type;
BEGIN

   if (I_ref_key is NULL)  then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_ref_key',L_program,NULL);
      return FALSE;
   end if;

   open C_get_ref_trace_type;
   fetch C_get_ref_trace_type into L_ref_trace_type;
   if C_get_ref_trace_type%NOTFOUND then
      close C_get_ref_trace_type;
      O_error_message := SQL_LIB.CREATE_MSG('NO_REF_TRACE_TYPE',L_ref_trace_type,L_program,NULL);
      return FALSE;
   end if;

   open C_get_report_url;
   fetch C_get_report_url into L_url;
   close C_get_report_url;

   O_rpt_url:=L_url|| QUERY_PARAM || PARAM_REF_KEY || EQUALS || I_ref_key;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END RMS_REPORT_URL;
--------------------------------------------------------------------------------------------------

FUNCTION REIM_REPORT_URL (O_error_message  OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_rpt_url        OUT   RETAIL_SERVICE_REPORT_URL.URL%TYPE,
                          I_ref_key        IN    VARCHAR)
RETURN BOOLEAN IS
   L_program             VARCHAR2(60) := PACKAGE_NAME || '.REIM_REPORT_URL';
   L_doc_type            VARCHAR2(15);
   L_param_id            NUMBER(20);
   L_url                 RETAIL_SERVICE_REPORT_URL.URL%TYPE;
   L_rwo_type CONSTANT VARCHAR(3) := 'RWO'; 

BEGIN
   
    L_doc_type := substr(I_ref_key,1,instr(I_ref_key,DELIMITER)-1);
    L_param_id := substr(I_ref_key,instr(I_ref_key,DELIMITER)+2);

    select url INTO L_url
        from retail_service_report_url
       where rs_code = L_doc_type;

   IF (I_ref_key IS NULL)  THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_ref_key',L_program,NULL);
      RETURN FALSE;
   end if;
   
   IF (L_url IS NULL)  THEN
      O_error_message := SQL_LIB.CREATE_MSG('BIP_REPORT_URL_IS_NULL','I_ref_key',L_program,NULL);
      RETURN FALSE;
   end if;
   
   IF (L_doc_type = L_rwo_type) THEN
      O_rpt_url :=L_url|| QUERY_PARAM || PARAM_RECEIPT_ID || EQUALS || L_param_id;
   ELSE
      O_rpt_url :=L_url || QUERY_PARAM || PARAM_DOC_ID || EQUALS || L_param_id;
   END IF;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END REIM_REPORT_URL;
--------------------------------------------------------------------------------------------------
END  AIA_PUB_REPORTS;
/
 
