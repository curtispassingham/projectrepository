CREATE OR REPLACE PACKAGE BODY L10N_ENTITY_SQL AS
--------------------------------------------------------------------------------------
-- Function Name : VAL_ENTITY
-- Purpose       : This function validate that all fields in the ext table.
--------------------------------------------------------------------------------------
FUNCTION VAL_ENTITY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_base_table    IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                    I_country       IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION BUILD_L10N_EXT_KEY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_key_tbl        IN OUT  TBL_KEY_VAL_TBL,
                            I_base_table     IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE DEFAULT NULL,
                            I_l10n_ext_table IN      EXT_ENTITY.L10N_EXT_TABLE%TYPE DEFAULT NULL,
                            I_key_name_1     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_1      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_2     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_2      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_3     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_3      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_4     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_4      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_5     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_5      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_6     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_6      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_7     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_7      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_8     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_8      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_9     IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_9      IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_10    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_10     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_11    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_11     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_12    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_12     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_13    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_13     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_14    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_14     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_15    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_15     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_16    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_16     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_17    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_17     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_18    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_18     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_19    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_19     IN      VARCHAR2 DEFAULT NULL,
                            I_key_name_20    IN      VARCHAR2 DEFAULT NULL,
                            I_key_val_20     IN      VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(62) := 'L10N_ENTITY_SQL.BUILD_L10N_EXT_KEY';
   L_cnt      NUMBER       := 0;

   cursor C_GET_HEADER_KEY is
      select ent.l10n_ext_table,
             ent.base_rms_table,
             ek.key_col,
             ek.key_number,
             ek.data_type,
             DECODE(kd.key_desc, NULL, kd2.key_desc, kd.key_desc) key_desc
        from ext_entity ent,
             ext_entity_key ek,
             ext_entity_key_descs kd,
             ext_entity_key_descs kd2
       where ent.base_rms_table   = ek.base_rms_table
         and ek.base_rms_table    = kd.base_rms_table(+)
         and ek.key_col           = kd.key_col(+)
         and ent.base_rms_table   = NVL(I_base_table, ent.base_rms_table)
         and ent.l10n_ext_table   = NVL(I_l10n_ext_table, ent.l10n_ext_table)
         and kd.lang(+)           = GET_USER_LANG
         and ek.base_rms_table    = kd2.base_rms_table
         and ek.key_col           = kd2.key_col
         and kd2.default_lang_ind = 'Y';

BEGIN

   if     I_base_table is NULL
      and I_l10n_ext_table is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('L10N_REQ_TBL_INPUT',NULL);
     return FALSE;
   end if;

   FOR rec in C_GET_HEADER_KEY LOOP
      L_cnt := L_cnt + 1;
      --
      O_key_tbl(L_cnt).l10n_ext_table := rec.l10n_ext_table;
      O_key_tbl(L_cnt).base_rms_table := rec.base_rms_table;
      O_key_tbl(L_cnt).key_col        := rec.key_col;
      O_key_tbl(L_cnt).key_number     := rec.key_number;
      O_key_tbl(L_cnt).data_type      := rec.data_type;
      O_key_tbl(L_cnt).key_desc       := rec.key_desc;
      --
      --The order of the input key_name and key_val does not matter in the following.
      --Only missing input key_name would raise an error, extra key_name are ignored.
      CASE upper(rec.key_col)
         WHEN upper(I_key_name_1)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_1;
         WHEN upper(I_key_name_2)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_2;
         WHEN upper(I_key_name_3)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_3;
         WHEN upper(I_key_name_4)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_4;
         WHEN upper(I_key_name_5)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_5;
         WHEN upper(I_key_name_6)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_6;
         WHEN upper(I_key_name_7)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_7;
         WHEN upper(I_key_name_8)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_8;
         WHEN upper(I_key_name_9)  THEN O_key_tbl(L_cnt).key_val  := I_key_val_9;
         WHEN upper(I_key_name_10) THEN O_key_tbl(L_cnt).key_val  := I_key_val_10;
         WHEN upper(I_key_name_11) THEN O_key_tbl(L_cnt).key_val  := I_key_val_11;
         WHEN upper(I_key_name_12) THEN O_key_tbl(L_cnt).key_val  := I_key_val_12;
         WHEN upper(I_key_name_13) THEN O_key_tbl(L_cnt).key_val  := I_key_val_13;
         WHEN upper(I_key_name_14) THEN O_key_tbl(L_cnt).key_val  := I_key_val_14;
         WHEN upper(I_key_name_15) THEN O_key_tbl(L_cnt).key_val  := I_key_val_15;
         WHEN upper(I_key_name_16) THEN O_key_tbl(L_cnt).key_val  := I_key_val_16;
         WHEN upper(I_key_name_17) THEN O_key_tbl(L_cnt).key_val  := I_key_val_17;
         WHEN upper(I_key_name_18) THEN O_key_tbl(L_cnt).key_val  := I_key_val_18;
         WHEN upper(I_key_name_19) THEN O_key_tbl(L_cnt).key_val  := I_key_val_19;
         WHEN upper(I_key_name_20) THEN O_key_tbl(L_cnt).key_val  := I_key_val_20;
         ELSE O_error_message := SQL_LIB.CREATE_MSG('L10N_MISSING_KEY',NULL); return FALSE;
      END CASE;

      if upper(rec.data_type) = 'DATE' then
         O_key_tbl(L_cnt).key_val := to_char(to_date(O_key_tbl(L_cnt).key_val), 'YYYYMMDD');
      end if;
      --
   END LOOP;


   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_L10N_EXT_KEY;
--------------------------------------------------------------------------------------
FUNCTION CHECK_REQ_EXT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_base_table     IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                       I_country        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                       I_key_tbl        IN      TBL_KEY_VAL_TBL)
RETURN BOOLEAN IS

   L_program           VARCHAR2(62)   := 'L10N_ENTITY_SQL.CHECK_REQ_EXT';
   L_query             VARCHAR2(3000);
   L_from_where        VARCHAR2(500);
   L_missing_att_list  VARCHAR2(3000) := NULL;
   L_missing_attr      l10n_attrib_descs.description%TYPE;
   L_localized_ind     country_attrib.localized_ind%TYPE := NULL;

   L_col_name     l10n_attrib.view_col_name%TYPE;
   L_data_type    l10n_attrib.data_type%TYPE;
   L_value_req    l10n_attrib.value_req%TYPE;
   L_attrib_value VARCHAR2(250);
   L_attrib_desc  l10n_attrib_descs.description%TYPE;
   L_index        VARCHAR2(25);

   TYPE CUR_attr_value  IS REF CURSOR;
   C_ATTR_VALUE         CUR_attr_value;

   cursor C_GET_LOCALIZED_IND is
      select localized_ind
        from country_attrib
       where country_id = I_country;


BEGIN

   open C_GET_LOCALIZED_IND;
   fetch C_GET_LOCALIZED_IND into L_localized_ind;
   close C_GET_LOCALIZED_IND;

   if L_localized_ind = 'Y' then
      if upper(I_key_tbl(1).base_rms_table) != upper(I_base_table) then
         O_error_message := SQL_LIB.CREATE_MSG('L10N_TBL_NOT_MATCH',NULL);
         return FALSE;
      end if;
      ---
      FOR i IN I_key_tbl.first..I_key_tbl.last LOOP
        if i = 1 then
           L_from_where := ' from ' || I_key_tbl(i).l10n_ext_table ||
                           ' where ' || I_key_tbl(i).key_col || ' = ';
        else
           L_from_where := L_from_where || ' and ' || I_key_tbl(i).key_col || ' = ';
        end if;
        --
        if upper(I_key_tbl(i).data_type) = 'NUMBER' then
           L_from_where := L_from_where || to_number(I_key_tbl(i).key_val);
        elsif upper(I_key_tbl(i).data_type) = 'DATE' then
           L_from_where := L_from_where || to_date(I_key_tbl(i).key_val);
        else
           L_from_where := L_from_where || '''' || I_key_tbl(i).key_val || '''';
        end if;
        --
        L_index := I_key_tbl(i).key_col;
        GP_attr_val_tbl(L_index).col_name     := I_key_tbl(i).key_col;
        GP_attr_val_tbl(L_index).data_type    := I_key_tbl(i).data_type;
        GP_attr_val_tbl(L_index).value_req    := NULL;
        GP_attr_val_tbl(L_index).attrib_value := I_key_tbl(i).key_val;
        GP_attr_val_tbl(L_index).attrib_desc  := NULL;
      END LOOP;
      ---
      L_query := 'select att.view_col_name, '||
                        'att.data_type, '||
                        'att.value_req, '||
                        'piv.attrib_value, '||
                        'decode(ad.description, NULL, ad2.description, ad.description) attrib_desc '||
                   'from ext_entity ext, '||
                        'l10n_attrib_group grp, '||
                        'l10n_attrib att, '||
                        'l10n_attrib_descs ad, '||
                        'l10n_attrib_descs ad2, '||
                        '(select group_id, '||
                                'attrib_col, '||
                                'attrib_value '||
                           'from (select group_id, '||
                                        'varchar2_1, '||
                                        'varchar2_2, '||
                                        'varchar2_3, '||
                                        'varchar2_4, '||
                                        'varchar2_5, '||
                                        'varchar2_6, '||
                                        'varchar2_7, '||
                                        'varchar2_8, '||
                                        'varchar2_9, '||
                                        'varchar2_10, '||
                                        'to_char(number_11) number_11, '||
                                        'to_char(number_12) number_12, '||
                                        'to_char(number_13) number_13, '||
                                        'to_char(number_14) number_14, '||
                                        'to_char(number_15) number_15, '||
                                        'to_char(number_16) number_16, '||
                                        'to_char(number_17) number_17, '||
                                        'to_char(number_18) number_18, '||
                                        'to_char(number_19) number_19, '||
                                        'to_char(number_20) number_20, '||
                                        'to_char(date_21, ''YYYYMMDD'') date_21, '||
                                        'to_char(date_22, ''YYYYMMDD'') date_22 '||
                                   L_from_where ||') ext '||
                                 'unpivot  '||
                                   '(attrib_value for attrib_col in (varchar2_1  as ''VARCHAR2_1'', '||
                                                                    'varchar2_2  as ''VARCHAR2_2'', '||
                                                                    'varchar2_3  as ''VARCHAR2_3'', '||
                                                                    'varchar2_4  as ''VARCHAR2_4'', '||
                                                                    'varchar2_5  as ''VARCHAR2_5'', '||
                                                                    'varchar2_6  as ''VARCHAR2_6'', '||
                                                                    'varchar2_7  as ''VARCHAR2_7'', '||
                                                                    'varchar2_8  as ''VARCHAR2_8'', '||
                                                                    'varchar2_9  as ''VARCHAR2_9'', '||
                                                                    'varchar2_10 as ''VARCHAR2_10'', '||
                                                                    'number_11   as ''NUMBER_11'', '||
                                                                    'number_12   as ''NUMBER_12'', '||
                                                                    'number_13   as ''NUMBER_13'', '||
                                                                    'number_14   as ''NUMBER_14'', '||
                                                                    'number_15   as ''NUMBER_15'', '||
                                                                    'number_16   as ''NUMBER_16'', '||
                                                                    'number_17   as ''NUMBER_17'', '||
                                                                    'number_18   as ''NUMBER_18'', '||
                                                                    'number_19   as ''NUMBER_19'', '||
                                                                    'number_20   as ''NUMBER_20'', '||
                                                                    'date_21     as ''DATE_21'', '||
                                                                    'date_22     as ''DATE_22''))) piv '||
                  'where ext.base_rms_table   = '''|| I_base_table || '''' ||
                    'and ext.ext_entity_id    = grp.ext_entity_id '||
                    'and grp.country_id       = NVL('''|| I_country || ''', grp.country_id) '||
                    'and att.group_id         = grp.group_id '||
                    'and ad.attrib_id (+)     = att.attrib_id '||
                    'and ad.lang (+)          = GET_USER_LANG '||
                    'and ad2.attrib_id        = att.attrib_id '||
                    'and ad2.default_lang_ind = ''Y'' '||
                    'and piv.attrib_col(+)    = att.attrib_storage_col '||
                    'and piv.group_id(+)      = att.group_id '||
                  'order by att.group_id ';
      ---
      open  C_ATTR_VALUE for L_query;
      LOOP
        fetch C_ATTR_VALUE INTO L_col_name,
                                L_data_type,
                                L_value_req,
                                L_attrib_value,
                                L_attrib_desc;
        exit when C_ATTR_VALUE%NOTFOUND;
        --
        --Check req field is not NULL
        if L_value_req = 'Y' and L_attrib_value is NULL then
           O_error_message := SQL_LIB.CREATE_MSG('L10N_MISSING_REQ_ATT', L_attrib_desc);
           return FALSE;
        end if;
        --
        L_index := L_col_name;
        GP_attr_val_tbl(L_index).col_name     := L_col_name;
        GP_attr_val_tbl(L_index).data_type    := L_data_type;
        GP_attr_val_tbl(L_index).value_req    := L_value_req;
        GP_attr_val_tbl(L_index).attrib_value := L_attrib_value;
        GP_attr_val_tbl(L_index).attrib_desc  := L_attrib_desc;
      END LOOP;
      close C_ATTR_VALUE;
      ---
      --Call validate records.
       if VAL_ENTITY(O_error_message,
                     I_base_table,
                     I_country) = FALSE then
         return FALSE;
      end if;
      ---
      GP_attr_val_tbl.delete;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQ_EXT;
--------------------------------------------------------------------------------------
FUNCTION CHECK_REQ_EXT_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_base_table      IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                           I_country         IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                           I_key_tbl         IN       KEY_VAL_TBL)
RETURN NUMBER IS

   L_program       VARCHAR2(62) := 'L10N_ENTITY_SQL.CHECK_REQ_EXT_WRP';
   L_key_val_tbl   TBL_KEY_VAL_TBL;
   L_key_val_rec   TYP_KEY_VAL_REC;

BEGIN
   if I_key_tbl is NOT NULL and I_key_tbl.COUNT > 0 then
      FOR i IN I_key_tbl.FIRST .. I_key_tbl.LAST
      LOOP
         L_key_val_rec.l10n_ext_table := I_key_tbl(i).l10n_ext_table;
         L_key_val_rec.base_rms_table := I_key_tbl(i).base_rms_table;
         L_key_val_rec.key_col        := I_key_tbl(i).key_col;
         L_key_val_rec.key_number     := I_key_tbl(i).key_number;
         L_key_val_rec.data_type      := I_key_tbl(i).data_type;
         L_key_val_rec.key_desc       := I_key_tbl(i).key_desc;
         L_key_val_rec.key_val        := I_key_tbl(i).key_val;
         L_key_val_tbl(i)             :=  L_key_val_rec;
      END LOOP;
   end if;

   if L10N_ENTITY_SQL.CHECK_REQ_EXT(O_error_message,
                                    I_base_table,
                                    I_country,
                                    L_key_val_tbl) = false then
      return 0;
   end if;
   ---
   return 1;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END CHECK_REQ_EXT_WRP;
--------------------------------------------------------------------------------------
FUNCTION VAL_ENTITY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_base_table    IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                    I_country       IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(62)   := 'L10N_ENTITY_SQL.VAL_ENTITY';
   L_val_fun      l10n_ext_entity_val.cross_group_validation_code%TYPE;
   L_bind         VARCHAR2(60);
   L_bind_type    VARCHAR2(2);
   L_bind_start   NUMBER;
   L_bind_end     NUMBER;
   L_cnt          NUMBER := 1;
   L_col          L10N_ATTRIB.VIEW_COL_NAME%TYPE;
   L_dynamic_stm  VARCHAR2(4000);


   cursor C_GET_VAL_FUNCT is
      select cross_group_validation_code
        from l10n_ext_entity_val val,
             ext_entity ext
       where val.ext_entity_id  = ext.ext_entity_id
         and val.country_id     = I_country
         and ext.base_rms_table = I_base_table;

BEGIN
   ---
   FOR rec IN C_GET_VAL_FUNCT LOOP
      EXIT WHEN rec.cross_group_validation_code is NULL;
      L_val_fun := rec.cross_group_validation_code;
      L_bind_start := instr(L_val_fun, ':', 1, L_cnt);

      L_dynamic_stm := ' DECLARE ';
      --
      while L_bind_start > 0 loop
         L_bind_end := instr(L_val_fun, ',',L_bind_start);
         --
         if L_bind_end <= 0 then
            L_bind_end := instr(L_val_fun, ')',L_bind_start);
         end if;
         L_bind      := substr(L_val_fun, L_bind_start, L_bind_end - L_bind_start );

         L_bind_type := substr(L_bind, 2, instr(L_bind, '_') - 2);
         if L_bind_type not in ('I', 'IO', 'O') then
            O_error_message := SQL_LIB.CREATE_MSG('L10N_INV_IN_OUT_BIND',NULL);
            return FALSE;
         end if;

         L_dynamic_stm := L_dynamic_stm || ' ' ||replace(replace(replace(L_bind||'<', '_DESCR<', NULL), '<', NULL), ':', NULL) || ' ';

         L_col := replace(replace(replace(L_bind||'<', '_DESCR<', NULL), '<', NULL), ':'||L_bind_type||'_', NULL);

         if L_col = 'ERROR' then
            L_dynamic_stm := replace(L_dynamic_stm, ' O_ERROR', NULL);
         elsif GP_attr_val_tbl.exists(L_col) then
            if upper(GP_attr_val_tbl(L_col).data_type) = 'NUMBER' then
               L_dynamic_stm := L_dynamic_stm || 'NUMBER := to_number(' || GP_attr_val_tbl(L_col).attrib_value || ');';
            elsif upper(GP_attr_val_tbl(L_col).data_type) = 'DATE' then
               L_dynamic_stm := L_dynamic_stm || 'DATE := to_date(' || GP_attr_val_tbl(L_col).attrib_value || ');';
            elsif upper(GP_attr_val_tbl(L_col).data_type) = 'VARCHAR2' then
               L_dynamic_stm := L_dynamic_stm || 'VARCHAR2(250) := ''' || GP_attr_val_tbl(L_col).attrib_value || ''';';
            else
               O_error_message := SQL_LIB.CREATE_MSG('L10N_INV_DATA_TYPE',GP_attr_val_tbl(L_col).data_type);
               return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('L10N_INV_PARM',L_col, I_base_table);
            return FALSE;
         end if;
         ---
         L_col := NULL;
         L_cnt := L_cnt + 1;
         L_bind_start := instr(L_val_fun, ':', 1, L_cnt);
      end loop;
      --
      L_dynamic_stm := L_dynamic_stm || ' BEGIN L10N_ENTITY_SQL.GP_return := ' || replace(L_val_fun, ':', NULL)|| '; END;';
      L_dynamic_stm := replace(L_dynamic_stm, 'O_ERROR','L10N_ENTITY_SQL.GP_error_message');
      ---
      EXECUTE IMMEDIATE (L_dynamic_stm);

      if L10N_ENTITY_SQL.GP_return then
         O_error_message := L10N_ENTITY_SQL.GP_error_message;
      else
         O_error_message := L10N_ENTITY_SQL.GP_error_message;
         return FALSE;
      end if;
      ---
   END LOOP;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VAL_ENTITY;
--------------------------------------------------------------------------------------------------
FUNCTION BUILD_L10N_EXT_KEY_WRP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_key_tbl          IN OUT   KEY_VAL_TBL,
                                I_base_table       IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE DEFAULT NULL,
                                I_l10n_ext_table   IN       EXT_ENTITY.L10N_EXT_TABLE%TYPE DEFAULT NULL,
                                I_key_name_1       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_1        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_2       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_2        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_3       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_3        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_4       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_4        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_5       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_5        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_6       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_6        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_7       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_7        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_8       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_8        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_9       IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_9        IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_10      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_10       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_11      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_11       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_12      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_12       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_13      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_13       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_14      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_14       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_15      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_15       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_16      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_16       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_17      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_17       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_18      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_18       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_19      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_19       IN       VARCHAR2 DEFAULT NULL,
                                I_key_name_20      IN       VARCHAR2 DEFAULT NULL,
                                I_key_val_20       IN       VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program       VARCHAR2(62)   := 'L10N_ENTITY_SQL.BUILD_L10N_EXT_KEY_WRP';
   L_key_val_tbl   TBL_KEY_VAL_TBL;
   L_key_val_rec   KEY_VAL_RECORD := KEY_VAL_RECORD(null,null,null,null,null,null,null);

BEGIN
   O_key_tbl := KEY_VAL_TBL();

   if L10N_ENTITY_SQL.BUILD_L10N_EXT_KEY(O_error_message,
                                         L_key_val_tbl,
                                         I_base_table,
                                         I_l10n_ext_table,
                                         I_key_name_1,
                                         I_key_val_1,
                                         I_key_name_2,
                                         I_key_val_2,
                                         I_key_name_3,
                                         I_key_val_3,
                                         I_key_name_4,
                                         I_key_val_4,
                                         I_key_name_5,
                                         I_key_val_5,
                                         I_key_name_6,
                                         I_key_val_6,
                                         I_key_name_7,
                                         I_key_val_7,
                                         I_key_name_8,
                                         I_key_val_8,
                                         I_key_name_9,
                                         I_key_val_9,
                                         I_key_name_10,
                                         I_key_val_10,
                                         I_key_name_11,
                                         I_key_val_11,
                                         I_key_name_12,
                                         I_key_val_12,
                                         I_key_name_13,
                                         I_key_val_13,
                                         I_key_name_14,
                                         I_key_val_14,
                                         I_key_name_15,
                                         I_key_val_15,
                                         I_key_name_16,
                                         I_key_val_16,
                                         I_key_name_17,
                                         I_key_val_17,
                                         I_key_name_18,
                                         I_key_val_18,
                                         I_key_name_19,
                                         I_key_val_19,
                                         I_key_name_20,
                                         I_key_val_20) = FALSE then
      return 0;
   end if;

   if L_key_val_tbl.COUNT > 0 then
      FOR i IN L_key_val_tbl.FIRST .. L_key_val_tbl.LAST
      LOOP
         L_key_val_rec.l10n_ext_table :=  L_key_val_tbl(i).l10n_ext_table;
         L_key_val_rec.base_rms_table :=  L_key_val_tbl(i).base_rms_table;
         L_key_val_rec.key_col        :=  L_key_val_tbl(i).key_col;
         L_key_val_rec.key_number     :=  L_key_val_tbl(i).key_number;
         L_key_val_rec.data_type      :=  L_key_val_tbl(i).data_type;
         L_key_val_rec.key_desc       :=  L_key_val_tbl(i).key_desc;
         L_key_val_rec.key_val        :=  L_key_val_tbl(i).key_val;
         O_key_tbl.extend;
         O_key_tbl(i) :=  L_key_val_rec;
      END LOOP;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END BUILD_L10N_EXT_KEY_WRP;
--------------------------------------------------------------------------------------------------
END L10N_ENTITY_SQL;
/