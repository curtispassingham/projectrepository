create or replace
PACKAGE CORESVC_ADMIN_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
-- Function Name : DELETE_PROCESSES
--  Description  : This function will delete entries from pocess tracker table and
--                 staging table.
-----------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES(
      O_error_message  IN OUT rtk_errors.rtk_text%type,
      I_process_id_tab IN num_tab)
    RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : INIT_PROCESS
--  Description  : This function will insert an entry in SVC_PROCESS_TRACKER table for
--                 each upload from Admin API  screen.

-----------------------------------------------------------------------------------
FUNCTION INIT_PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id    IN SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                      I_process_desc  IN SVC_PROCESS_TRACKER.PROCESS_DESC%TYPE,
                      I_template_key  IN SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE,
                      I_action_type   IN SVC_PROCESS_TRACKER.ACTION_TYPE%TYPE,
                      I_source        IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                      I_destination   IN SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                      I_rms_async_id  IN SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE,
                      I_file_id       IN SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                      I_file_path     IN SVC_PROCESS_TRACKER.FILE_PATH%TYPE,
                      I_user_id       IN SVC_PROCESS_TRACKER.USER_ID%TYPE)
    RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name : PURGE_ADMIN_SVC_TABLES
--  Description  : This function will be called from the script sa_admin_api_purge.ksh
---                to purge the admin api records
-----------------------------------------------------------------------------------
FUNCTION PURGE_ADMIN_SVC_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_ADMIN_API(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dnld_api        IN OUT   SVC_TMPL_API_MAP.DNLD_API%TYPE,
                       O_upld_api        IN OUT   SVC_TMPL_API_MAP.UPLD_API%TYPE,
                       O_process_api     IN OUT   SVC_TMPL_API_MAP.PROCESS_API%TYPE,
                       I_template_key    IN       SVC_TMPL_API_MAP.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION CALL_DNLD_API(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id              IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind    IN       CHAR DEFAULT 'N',
                       I_dnld_api             IN       SVC_TMPL_API_MAP.DNLD_API%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION CALL_UPLD_API(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_error_count          IN OUT   NUMBER,
                       I_file_id              IN       S9T_FOLDER.FILE_ID%TYPE,
                       I_process_id           IN       NUMBER,
                       I_upld_api             IN       SVC_TMPL_API_MAP.UPLD_API%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
FUNCTION CALL_PROCESS_API(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_count     IN OUT   NUMBER,
                          I_process_id      IN       NUMBER,
                          I_chunk_id        IN       NUMBER,
                          I_process_api     IN       SVC_TMPL_API_MAP.PROCESS_API%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES_WRP(
      O_error_message  IN OUT rtk_errors.rtk_text%type,
      I_process_id_tab IN num_tab)
RETURN NUMBER;
--------------------------------------------------------------
END CORESVC_ADMIN_SQL;
/