
CREATE OR REPLACE PACKAGE L10N_SQL AS
------------------------------------------------------------------------
GP_base_country  COUNTRY.COUNTRY_ID%TYPE := '-1';
GP_null_ind  VARCHAR2(3) := '-1';
------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
		            IO_l10n_obj     IN OUT L10N_OBJ)
   RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION CALL_EXEC_FUNC_FND(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_exists_ind    IN OUT VARCHAR2,
                            I_procedure_key  IN VARCHAR2,
                            I_country_id     IN VARCHAR2,
                            I_source_entity  IN VARCHAR2,
                            I_source_id      IN VARCHAR2,
                            I_source_type    IN VARCHAR2,
                            I_item           IN VARCHAR2)
  RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION CALL_EXEC_FUNC_DOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                             IO_exists_ind   IN OUT VARCHAR2, 
                             I_procedure_key IN VARCHAR2,
                             I_country_id    IN VARCHAR2 DEFAULT NULL,
                             I_doc_type      IN VARCHAR2 DEFAULT NULL,
                             I_doc_id        IN NUMBER   DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION REFRESH_MV_L10N_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_BATCH_FUNCTION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_function_name IN OUT VARCHAR2,
                            IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_CNTRY_LOCAIND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_country_id    OUT VARCHAR2,
                           O_localized_ind OUT VARCHAR2,
                           I_entity        IN  VARCHAR2,
                           I_type          IN  VARCHAR2,    
                           I_id            IN  VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION CALL_DEL_FUNC_DOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                            IO_exists_ind   IN OUT VARCHAR2,
                            I_procedure_key IN     VARCHAR2,
                            I_country_id    IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------ 
END L10N_SQL;
/