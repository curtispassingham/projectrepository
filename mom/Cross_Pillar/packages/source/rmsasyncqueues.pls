CREATE OR REPLACE PACKAGE RMS_ASYNC_QUEUE_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
-- Function Name: CREATE_QUEUE_SUBSCRIBER
-- Purpose      : This function creates Advanced Queuing objects for the specified job.
--                It contains the logic to drop and recreate the queue and queue table 
--                if one already exists. Depending on the input I_register_event_ind,
--                it optionally register the AQ event.
------------------------------------------------------------------------------------------------
FUNCTION CREATE_QUEUE_SUBSCRIBER(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_job_type            IN     RMS_ASYNC_JOB.JOB_TYPE%TYPE, 
                                 I_register_event_ind  IN     VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END RMS_ASYNC_QUEUE_SQL;
/
