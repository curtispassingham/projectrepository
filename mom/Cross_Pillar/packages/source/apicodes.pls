CREATE OR REPLACE PACKAGE API_CODES AUTHID CURRENT_USER AS
  ------------------------------------------------------
  -- This package contains the standard error Variables
  -- to be used thoroughout API's for SeeBeyond.  By
  -- using these variables, there is a standard way of
  -- returning errors so the SeeBeyond API knows what
  -- to expect instead of receiving random things.
  --
  -- Example,
  --   PACKAGE BODY RMSPUB_VENDOR AS
  --
  --     PROCEDURE GET_HEADER(
  --                      O_status_GH OUT INTEGER,
  --                      O_text_GH   OUT VARCHAR2) IS
  --     BEGIN
  --        Select ...
  --        INTO   ...
  --        FROM   SUPS
  --        WHERE  ...
  --     EXCEPTION
  --        WHEN NO_DATA_FOUND THEN
  --           O_status_GH := API_CODES.DONE;
  --     END;
  --
  ------------------------------------------------------

  NO_MSG            CONSTANT VARCHAR2(1)  := 'N'; -- No more messages to process
  UNHANDLED_ERROR   CONSTANT VARCHAR2(1)  := 'E'; -- Unclassified (fatal) Error
  NEW_MSG           CONSTANT VARCHAR2(1)  := 'S'; -- Successfully retrieved new message.
  OUT_OF_SEQUENCE   CONSTANT VARCHAR2(1)  := 'P'; -- Program Error
  SUCCESS           CONSTANT VARCHAR2(1)  := 'S'; -- Success
  LOCKED            CONSTANT VARCHAR2(1)  := 'L'; -- Table is Locked, send message later.
  DELETED           CONSTANT VARCHAR2(1)  := 'X'; -- Retrieved record has been deleted
  DONE              CONSTANT VARCHAR2(1)  := 'D'; -- Application is done processing
  HOSPITAL          CONSTANT VARCHAR2(1)  := 'H'; -- A non-fatal error has occurred.  Send the current
                                                  -- message to the hospital and continue processing messages.
  INCOMPLETE_MSG    CONSTANT VARCHAR2(1)  := 'I'; -- Successfully retrieved new message.  However,
                                                  -- because of size constraints, only part of the message can be sent
                                                  -- for the current procedure call.  Rerun the current procedure call
                                                  -- with the same parameters to get the rest of the message.
END API_CODES;
/
