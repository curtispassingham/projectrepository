
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ROUNDING_SQL AUTHID CURRENT_USER AS

/*** Global Variable Types ***/

-- Define a record type to store total order qtys by line item
TYPE total_ord_qty_rectype IS RECORD(item                item_master.item%TYPE,
                                     location            ordloc.location%TYPE,
                                     total_order_qty     ordloc.qty_ordered%TYPE);

-- Define a table type based upon the total order qtys record type defined above.
TYPE total_ord_qty_tabletype IS TABLE OF total_ord_qty_rectype
   INDEX BY BINARY_INTEGER;

/*** Function Specs ***/


---------------------------------------------------------------------
-- Function Name : ORDER_INTERFACE
-- Purpose       : This function interfaces with the internal round_line_items function for orders.
--                 It rounds an order taking into account multi-channel and inventory segregation.
---------------------------------------------------------------------
FUNCTION ORDER_INTERFACE(O_error_message        OUT VARCHAR2,
                         I_order_no             IN  ordhead.order_no%TYPE,
                         I_supplier             IN  ordhead.supplier%TYPE,
                         I_update_orig_repl_qty IN  VARCHAR2 DEFAULT 'N',
                         I_skip_alloc_round     IN  VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

FUNCTION ORDER_INTERFACE(O_error_message        OUT VARCHAR2,
                         O_covered_inbound      OUT BOOLEAN,
                         I_order_no             IN  ordhead.order_no%TYPE,
                         I_supplier             IN  ordhead.supplier%TYPE,
                         I_update_orig_repl_qty IN  VARCHAR2 DEFAULT 'N',
                         I_skip_alloc_round     IN  VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

FUNCTION ORDER_INTERFACE(O_error_message        OUT VARCHAR2,
                         O_covered_inbound      OUT BOOLEAN,
                         I_order_no             IN  ordhead.order_no%TYPE,
                         I_supplier             IN  ordhead.supplier%TYPE,
                         I_total_ord_qty_table  IN  rounding_sql.total_ord_qty_tabletype,
                         I_update_orig_repl_qty IN  VARCHAR2 DEFAULT 'N',
                         I_skip_alloc_round     IN  VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

FUNCTION ORDER_INTERFACE_WRP(O_error_message             OUT   VARCHAR2,
                             O_covered_inbound           OUT   INTEGER,
                             I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                             I_supplier               IN       ORDHEAD.SUPPLIER%TYPE,
                             I_total_ord_qty_table    IN       WRP_ROUNDING_ORDQTY_TBL,
                             I_update_orig_repl_qty   IN       VARCHAR2 DEFAULT 'N',
                             I_skip_alloc_round       IN       VARCHAR2 DEFAULT 'N')
RETURN INTEGER;
---------------------------------------------------------------------
-- Function Name : BUYER_WRKSHT_INTERFACE
-- Purpose       : This function interfaces with the internal round_line_items function for Buyer Worksheet line items.
--                 It rounds line items selected by the user taking into account multi-channel and inventory segregation.
---------------------------------------------------------------------
FUNCTION BUYER_WRKSHT_INTERFACE(O_error_message    OUT VARCHAR2,
                                I_audsid           IN  repl_results.audsid%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name : ORDER_MATRIX_INTERFACE
-- Purpose       : This function interfaces with the internal round_line_items function for ordloc_wksht.
--                 It rounds an order's wksht line items taking into account multi-channel and inventory segregation.
---------------------------------------------------------------------
FUNCTION ORDER_MATRIX_INTERFACE(O_error_message       OUT VARCHAR2,
                                I_order_no            IN  ordhead.order_no%TYPE,
                                I_supplier            IN  ordhead.supplier%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------
-- Function Name: GET_PACKSIZE
-- Purpose  : This function fetchs the pack size for a store depending on
--        the type of warehouse and the value in the store_ord_mult
--            field('C', 'I', or 'E') on win_skus/rag_style. If the warehouse
--        is not a breakpack warehouse, then this function fetches
--            the supp_pack_size from item_supp_country.  If the warehouse is a
--            breakpack warehouse, then it fetchs the pack size from
--            item_supp_country that corresponds to the store_ord_mult value for
--            the sku being passed in.
--            The calling program should pass supplier number in I_supplier and
--            country, if available,
--            otherwise, this function will get pack size using primary
--            supplier and country for the sku passed in.
--            If the calling program already knows system_ind of the item
--            passed in, then pass that info through I_system_in, otherwise,
--            pass in NULL, and this function will fetch system_ind.
--  NOTE : this function is called before calling either TO_INNER_CASE,
--         or CHECK_INNER_CASE.
---------------------------------------------------------------
FUNCTION GET_PACKSIZE(O_error_message   IN OUT VARCHAR2,
                      O_store_pack_size IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                      O_store_ord_mult  IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                      O_break_pack_ind  IN OUT WH.BREAK_PACK_IND%TYPE,
                      I_item            IN ITEM_MASTER.ITEM%TYPE,
                      I_wh              IN WH.WH%TYPE,
                      I_supplier        IN ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country  IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_loc             IN ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------
-- Function Name: TO_INNER_CASE
-- Purpose  : This function must be preceded by calling GET_PACKSIZE.
--     It is used for rounding qty to multiples of store pack size,
--       either for x-dock PO or stock allocation transfers.
--     When calling from PO dialog, ordsku.supp_pack_size should be
--       passed in the I_override_case_size input parameter,
--       when calling from transfer dialog, pass NULL in this parameter.
---------------------------------------------------------------------
FUNCTION TO_INNER_CASE(O_error_message      IN OUT VARCHAR2,
                       O_out_qty            IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       I_item               IN     ITEM_MASTER.ITEM%TYPE,
                       I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_origin_country     IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_location           IN     ITEM_LOC.LOC%TYPE,
                       I_in_qty             IN     ORDLOC.QTY_ORDERED%TYPE,
                       I_override_case_size IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_store_pack_size    IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_store_ord_mult     IN     ITEM_LOC.STORE_ORD_MULT%TYPE,
                       I_break_pack_ind     IN     WH.BREAK_PACK_IND%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name : CHECK_CASE_PALLET
-- Purpose       : This function will take the inputted item and case
--                 case pack size and determine if the case pack size
--                 divides evenly into the pallet size.  O_invalid_qty
--                 returns FALSE if it does not divide evenly.
---------------------------------------------------------------
FUNCTION CHECK_CASE_PALLET(O_error_message       IN OUT VARCHAR2,
                           O_need_origin_country IN OUT BOOLEAN,
                           O_invalid_qty         IN OUT BOOLEAN,
                           I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                           I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                           I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                           I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                           I_override_case_size  IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name : CALC_ROUNDED_GROUP_QTYS
-- Purpose       :
---------------------------------------------------------------------
FUNCTION CALC_ROUNDED_GROUP_QTYS(O_error_message OUT VARCHAR2,
                                 I_order_no      IN  ordhead.order_no%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
END ROUNDING_SQL;
/