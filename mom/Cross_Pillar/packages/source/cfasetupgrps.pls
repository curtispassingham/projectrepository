CREATE OR REPLACE PACKAGE CFA_SETUP_GROUP_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_GROUP_ID(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           O_group_id         IN OUT    CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GRP_DISP_ORD_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   VARCHAR2,
                            I_group_set_id    IN       CFA_ATTRIB_GROUP.GROUP_SET_ID%TYPE,
                            I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                            I_disp_seq        IN       CFA_ATTRIB_GROUP.DISPLAY_SEQ%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_GRP_LABEL_LANG(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_group_id       IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                                I_lang           IN       CFA_ATTRIB_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GROUP_VIEW_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist            IN OUT   VARCHAR2,
                           I_grp_view_name    IN       CFA_ATTRIB_GROUP.GROUP_VIEW_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_GROUP_LABEL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_group_id         IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--Function Name: CHK_NO_GROUP_SET_LABELS
--Purpose      : This function checks that the inputted group_id has labels set up for all attributes. 
--               If no group/lang are passed in, it will check across all groups.  If a group
--               is found that does not have associated labels, O_exist is set to Y, and  
--               IO_group_id will contain the group with missing labels.
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_GROUP_LABELS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist           IN OUT    VARCHAR2,
                             IO_group_id       IN OUT    CFA_ATTRIB_GROUP_LABELS.GROUP_ID%TYPE,
                             I_lang            IN        CFA_ATTRIB_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_GROUP_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_group_id        IN     CFA_ATTRIB_GROUP.GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_GROUP_INFO(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                               O_group_set_label     IN OUT    CFA_ATTRIB_GROUP_SET_LABELS.LABEL%TYPE,
                               O_base_rms_table      IN OUT    CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                               O_custom_ext_table    IN OUT    CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                               O_group_label         IN OUT    CFA_ATTRIB_GROUP_LABELS.LABEL%TYPE,
                               IO_group_set_id       IN OUT    CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                               IO_ext_entity_id      IN OUT    CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE,
                               I_group_id            IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_GROUP_REC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_group_rec      IN OUT  CFA_ATTRIB_GROUP%ROWTYPE,
                              I_group_id       IN      CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_GROUP_SQL;
/
