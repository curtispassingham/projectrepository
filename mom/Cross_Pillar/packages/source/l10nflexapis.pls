CREATE OR REPLACE PACKAGE L10N_FLEX_API_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
TYPE COLUMN_TBL IS TABLE OF ALL_TAB_COLUMNS.COLUMN_NAME%TYPE INDEX BY BINARY_INTEGER;
TYPE view_col_rec IS RECORD
(
   view_col_names    COLUMN_TBL,
   group_ids         ID_TBL,
   tab_col_names     COLUMN_TBL
);

TYPE TYP_col_type_rec is RECORD
(   
    col_name     l10n_attrib.view_col_name%TYPE,
    data_type    l10n_attrib.data_type%TYPE
);
TYPE TBL_col_type_tbl is TABLE of TYP_col_type_rec INDEX BY VARCHAR2(30);

---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
-- Function name:  VALIDATE_L10N_ATTRIB
-- Purpose      :  This function validates locationlization attributes on the RIB object.
--                 It uses decoupling strategy to call out to country-specific functions. 
-- Inputs       :  
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_message           IN      RIB_OBJECT,
                              I_message_type      IN      VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  PERSIST_L10N_ATTRIB
-- Purpose      :  This function persists localization attributes on the RIB object
--                 to entity extension tables (e.g. item_country_l10n_ext). It uses 
--                 decoupling strategy, dumps data to a localization staging table
--                 (e.g. stg_item_country_l10n_ext_br), and calls the generic function 
--                 L10N_FLEX_API_SQL.INSERT_L10N_EXT_TBL to do the insert.
-- Inputs       :  
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_message           IN      RIB_OBJECT,
                             I_message_type      IN      VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  INSERT_L10N_EXT_TBL
-- Purpose      :  This function generates a dynamic sql statement based on the extension attributes 
--                 to insert data from the staging table to the entity extention table.
-- Inputs       :  I_country_id (localization extension country id)
--                 I_base_table (RMS base table name)
--                 I_process_id (segregate data on stg table by process_id)
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                             I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                             I_process_id        IN      NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  UPDATE_STG_NAME_VALUE_PAIR
-- Purpose      :  This function builds a dynamic sql based on the entity extension metadata
--                 to update the staging table with non-base attributes.
--                 If any key column or attribute of data type DATE is used, it should be 
--                 passed in the format of 'YYYYMMDD'. 
-- Inputs       :  I_country_id (localization extension country id)
--                 I_base_table (RMS base table name)
--                 I_process_id (segregate data on stg table by process_id)
--                 I_entity_name_values (a collection of attribute name/value pair along with
--                                       its entity keys)
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_STG_NAME_VALUE_PAIR(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_country_id           IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                                    I_base_table           IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                                    I_process_id           IN      NUMBER,
                                    I_entity_name_values   IN      ENTITY_NAME_VALUE_PAIR_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  MERGE_L10N_EXT_TBL
-- Purpose      :  This function generates a dynamic sql statement based on the extension attributes 
--                 to insert/update data from the staging table to the entity extention table.
-- Inputs       :  I_country_id (localization extension country id)
--                 I_base_table (RMS base table name)
--                 I_process_id (segregate data on stg table by process_id)
---------------------------------------------------------------------------------------------------
FUNCTION MERGE_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                             I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                             I_process_id        IN      NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END L10N_FLEX_API_SQL;
/