CREATE OR REPLACE PACKAGE BODY CFA_SETUP_ATTRIB_SQL AS
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB_LABELS (IO_attrib_label_tbl  IN OUT    ATTRIB_LABEL_TBL,
                               I_group_id           IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_lang               IN        CFA_ATTRIB_LABELS.LANG%TYPE) IS

   L_cnt   NUMBER := 0;

   cursor C_GET_ATTRIB_LABELS is
      select att.attrib_id,
             att.view_col_name,
             ad.label,
             I_lang lang,
             ad.default_lang_ind
        from cfa_attrib_labels ad,
             cfa_attrib att
       where ad.attrib_id (+) = att.attrib_id
         and att.group_id = I_group_id
         and ad.lang (+) = I_lang
       order by att.attrib_id;

BEGIN
   ---
   FOR rec in C_GET_ATTRIB_LABELS LOOP
       L_cnt := L_cnt + 1;
       ---
       IO_attrib_label_tbl(L_cnt).attrib_id := rec.attrib_id;
       IO_attrib_label_tbl(L_cnt).view_col_name := rec.view_col_name;
       IO_attrib_label_tbl(L_cnt).label := rec.label;
       IO_attrib_label_tbl(L_cnt).lang := rec.lang;
       IO_attrib_label_tbl(L_cnt).default_lang_ind := rec.default_lang_ind;
   END LOOP;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_attrib_label_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          'CFA_SETUP_ATTRIB_SQL.QUERY_ATTRIB_LABELS',
                                                          to_char(SQLCODE));
      IO_attrib_label_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_label_tbl(1).error_message);

END QUERY_ATTRIB_LABELS;
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB_LABELS  (IO_attrib_label_tbl     IN OUT    ATTRIB_LABEL_TBL,
                               I_group_id              IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_attrib_id             IN        CFA_ATTRIB_LABELS.ATTRIB_ID%TYPE,
                               I_lang                  IN        CFA_ATTRIB_LABELS.LANG%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_ATTRIB_LABELS is
      select 'x'
        from cfa_attrib_labels
       where attrib_id = I_attrib_id
         and lang = I_lang
         for update nowait;

BEGIN
   ---
   open C_LOCK_ATTRIB_LABELS;
   close C_LOCK_ATTRIB_LABELS;
   ---
   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_attrib_label_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                 'CFA_ATTRIB_LABELS',
                                                                 I_attrib_id);
      IO_attrib_label_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_label_tbl(1).error_message);
   when OTHERS then
      IO_attrib_label_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                'CFA_SETUP_ATTRIB_SQL.LOCK_ATTRIB_LABELS',
                                                                to_char(SQLCODE));
      IO_attrib_label_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_label_tbl(1).error_message);

END LOCK_ATTRIB_LABELS;
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB_LABELS (IO_attrib_label_tbl    IN OUT    ATTRIB_LABEL_TBL,
                              I_group_id              IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_lang                  IN        CFA_ATTRIB_LABELS.LANG%TYPE,
                              I_default_lang_ind      IN        CFA_ATTRIB_LABELS.DEFAULT_LANG_IND%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_LABEL_DEFAULT is
      select ad.rowid
        from cfa_attrib_labels ad,
             cfa_attrib att
       where att.attrib_id = ad.attrib_id
         and att.group_id = I_group_id
         for update of ad.default_lang_ind nowait;

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid TYP_rowid;

BEGIN
   ---
   if I_default_lang_ind = 'Y' then
      open C_LOCK_LABEL_DEFAULT;
      fetch C_LOCK_LABEL_DEFAULT BULK COLLECT into TBL_rowid;
      close C_LOCK_LABEL_DEFAULT;

      FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
        update cfa_attrib_labels
           set default_lang_ind = decode(lang, I_lang, 'Y', 'N')
          where rowid = TBL_rowid(i);
   end if;
   ---
   FORALL i in IO_attrib_label_tbl.FIRST..IO_attrib_label_tbl.LAST
      merge into cfa_attrib_labels ad
         using (select IO_attrib_label_tbl(i).attrib_id attrib_id,
                       IO_attrib_label_tbl(i).lang lang,
                       IO_attrib_label_tbl(i).label label,
                       I_default_lang_ind default_lang_ind
                  from dual) tmp
         on (ad.attrib_id = tmp.attrib_id and
             ad.lang = tmp.lang)
         when matched then
            update
               set ad.label = tmp.label,
                   ad.default_lang_ind = tmp.default_lang_ind
         when not matched then
            insert(attrib_id,
                   lang,
                   label,
                   default_lang_ind)
            values(tmp.attrib_id,
                   tmp.lang,
                   tmp.label,
                   tmp.default_lang_ind);
   ---
   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_attrib_label_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                'CFA_ATTRIB_LABELS',
                                                                I_group_id);
      IO_attrib_label_tbl(1).return_code := 'FALSE';
      raise_application_error(-20002, IO_attrib_label_tbl(1).error_message);
   when OTHERS then
      IO_attrib_label_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                'CFA_SETUP_ATTRIB_SQL.MERGE_ATTRIB_LABELS',
                                                                 to_char(SQLCODE));
      IO_attrib_label_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_label_tbl(1).error_message);

END MERGE_ATTRIB_LABELS;
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIBS_DEFAULT_LANG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_group_id       IN     CFA_ATTRIB.GROUP_ID%TYPE)
RETURN BOOLEAN IS

    L_exists   VARCHAR2(1) :=NULL;

   cursor C_LANG_IND is
      select 'x'
        from cfa_attrib a
       where group_id = I_group_id
         and not exists (select 'x'
                           from cfa_attrib_labels l
                          where l.attrib_id = a.attrib_id
                            and l.default_lang_ind = 'Y');

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('MISSING_DEFAULT_LANG',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.CHK_ATTRIBS_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIBS_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION GET_AVAIL_METADATA_COL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_storage_col_name   IN OUT   CFA_ATTRIB.STORAGE_COL_NAME%TYPE,
                                I_group_id           IN       CFA_ATTRIB.GROUP_ID%TYPE,
                                I_data_type          IN       CFA_ATTRIB.DATA_TYPE%TYPE)
RETURN BOOLEAN IS

   L_col_no       NUMBER;
   L_attrib_count NUMBER :=0;
   L_key_count    NUMBER :=0;

   cursor C_GET_STORAGE_COL_NAME is
      select min(col_no) attrib_col
        from (select col col_no,
                     case
                        when col between 1 and 10 then
                          'VARCHAR2'
                        when col between 11 and 20 then
                          'NUMBER'
                        when col between 21 and 25 then
                          'DATE'
                     end data_type
                from (select col
                        from (select 1  col_1,  2  col_2,  3  col_3,  4  col_4,  5  col_5,
                                     6  col_6,  7  col_7,  8  col_8,  9  col_9,  10 col_10,
                                     11 col_11, 12 col_12, 13 col_13, 14 col_14, 15 col_15,
                                     16 col_16, 17 col_17, 18 col_18, 19 col_19, 20 col_20,
                                     21 col_21, 22 col_22, 23 col_23, 24 col_24, 25 col_25
                                from dual) p
                      unpivot
                         (col for coli in (col_1  as 'col_1',  col_2  as 'col_2',  col_3  as 'col_3',  col_4  as 'col_4',  col_5  as 'col_5',
                                           col_6  as 'col_6',  col_7  as 'col_7',  col_8  as 'col_8',  col_9  as 'col_9',  col_10 as 'col_10',
                                           col_11 as 'col_11', col_12 as 'col_12', col_13 as 'col_13', col_14 as 'col_14', col_15 as 'col_15',
                                           col_16 as 'col_16', col_17 as 'col_17', col_18 as 'col_18', col_19 as 'col_19', col_20 as 'col_20',
                                           col_21 as 'col_21', col_22 as 'col_22', col_23 as 'col_23', col_24 as 'col_24', col_25 as 'col_25')))) piv
               where piv.data_type = I_data_type
                 and not exists (select 'x'
                                   from cfa_attrib
                                  where group_id = I_group_id
                                    and data_type = I_data_type
                                    and to_number(substr(storage_col_name, instr(storage_col_name, '_') + 1)) = piv.col_no);

   cursor C_COUNT_ATTRIB is
      select count(attrib_id)
        from cfa_attrib
       where group_id = I_group_id;

   cursor C_COUNT_KEY is
      select count(ekey.key_col)
        from cfa_attrib_group grp,
             cfa_attrib_group_set grps,
             cfa_ext_entity ext,
             cfa_ext_entity_key ekey
       where grp.group_id       = I_group_id
         and grp.group_set_id   = grps.group_set_id
         and grps.ext_entity_id = ext.ext_entity_id
         and ext.base_rms_table = ekey.base_rms_table;

BEGIN
   ---
   open C_COUNT_ATTRIB;
   fetch C_COUNT_ATTRIB into L_attrib_count;
   close C_COUNT_ATTRIB;
   --
   open C_COUNT_KEY;
   fetch C_COUNT_KEY into L_key_count;
   close C_COUNT_KEY;
   --
   --The limit has been set to 18 fields between keys and attributes.
   if (L_attrib_count + L_key_count) >= 18 then
      O_error_message := SQL_LIB.CREATE_MSG('CFA_GUI_LIMIT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_STORAGE_COL_NAME;
   fetch C_GET_STORAGE_COL_NAME into L_col_no;
   close C_GET_STORAGE_COL_NAME;
   ---
   if L_col_no is NOT NULL then
      O_storage_col_name := I_data_type||'_'||L_col_no;
   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_MD_COL',
                                            I_data_type,
                                            NULL,
                                            NULL);
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.GET_AVAIL_METADATA_COL',
                                            to_char(SQLCODE));
      return FALSE;

END GET_AVAIL_METADATA_COL;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ATTRIB_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_attrib_id       IN OUT   CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN IS

   L_exists       VARCHAR2(1)  := 'N';
   L_attrib_id    CFA_ATTRIB.ATTRIB_ID%TYPE;
   L_wrap_seq_no  CFA_ATTRIB.ATTRIB_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select cfa_attrib_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from cfa_attrib
       where attrib_id = L_attrib_id;

BEGIN
   ---
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_attrib_id;
   close C_NEXTVAL;
   ---
   -- Save the first sequence value fetched.
   L_wrap_seq_no := L_attrib_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_attrib_id := L_attrib_id;
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_attrib_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_attrib_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ATTRIB_ID_EXIST',
                                               'Attribute ID',
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                            'Attribute ID',
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.GET_NEXT_ATTRIB_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_ATTRIB_ID;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_LABELS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_attrib_id       IN       CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_ATTRIB_LABELS is
      select rowid
        from cfa_attrib_labels
       where attrib_id = I_attrib_id
         for update nowait;

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;

   TBL_rowid TYP_rowid;

BEGIN
   ---
   open C_LOCK_ATTRIB_LABELS;
   fetch C_LOCK_ATTRIB_LABELS BULK COLLECT into TBL_rowid;
   close C_LOCK_ATTRIB_LABELS;
   ---
   FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
      delete
        from cfa_attrib_labels
       where rowid = TBL_rowid(i);
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'CFA_ATTRIB_LABELS',
                                            I_attrib_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.DELETE_ATTRIB_LABELS',
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ATTRIB_LABELS;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_LABEL_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_default_ind     IN OUT    CFA_ATTRIB_LABELS.DEFAULT_LANG_IND%TYPE,
                                       I_group_id        IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                                       I_lang            IN        CFA_ATTRIB_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   cursor C_CHK_DEFAULT is
      select upper(ad.default_lang_ind) default_lang_ind
        from cfa_attrib_labels ad,
             cfa_attrib att
       where att.attrib_id = ad.attrib_id
         and att.group_id  = I_group_id
         and ad.lang       = I_lang;

BEGIN
   ---
   O_default_ind := 'N';
   ---
   open C_CHK_DEFAULT;
   fetch C_CHK_DEFAULT into O_default_ind;
   close C_CHK_DEFAULT;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.CHK_ATTRIB_LABEL_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIB_LABEL_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ATTRIB_LABELS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist           IN OUT    VARCHAR2,
                              I_group_id        IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_lang            IN        CFA_ATTRIB_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_CHK_LABELS_EXIST is
      select 'x'
        from cfa_attrib att
       where att.group_id = I_group_id
         and not exists (select 'x'
                           from cfa_attrib_labels ad
                          where ad.attrib_id = att.attrib_id
                            and ad.lang = NVL(I_lang, ad.lang));

BEGIN
   ---
   open C_CHK_LABELS_EXIST;
   fetch C_CHK_LABELS_EXIST into L_exist;
   ---
   if C_CHK_LABELS_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_CHK_LABELS_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.CHK_NO_ATTRIB_LABELS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_ATTRIB_LABELS;
---------------------------------------------------------------------------------------------------
FUNCTION ATTRIB_DISP_SEQ_EXIST(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist            IN OUT VARCHAR2,
                               I_group_id         IN     CFA_ATTRIB.GROUP_ID%TYPE,
                               I_display_seq      IN     CFA_ATTRIB.DISPLAY_SEQ%TYPE,
                               I_storage_col_name IN     CFA_ATTRIB.STORAGE_COL_NAME%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_CHK_DISP_SEQ is
      select 'x'
        from cfa_attrib
       where group_id          = I_group_id
         and display_seq       = I_display_seq
         and storage_col_name != I_storage_col_name;

BEGIN
   ---
   open C_CHK_DISP_SEQ;
   fetch C_CHK_DISP_SEQ into L_exist;
   ---
   if C_CHK_DISP_SEQ%FOUND then
      O_exist := 'Y';
   else
      O_exist := 'N';
   end if;
   ---
   close C_CHK_DISP_SEQ;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.ATTRIB_DISP_SEQ_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END ATTRIB_DISP_SEQ_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION VIEW_COL_NAME_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist             IN OUT   VARCHAR2,
                             I_group_id          IN       CFA_ATTRIB.GROUP_ID%TYPE,
                             I_view_col_name     IN       CFA_ATTRIB.VIEW_COL_NAME%TYPE,
                             I_storage_col_name  IN       CFA_ATTRIB.STORAGE_COL_NAME%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_VIEW_COL_EXIST is
      select 'x'
        from cfa_attrib att,
             cfa_attrib_group grp,
             (select group_set_id
                from cfa_attrib_group
               where group_id = I_group_id) grpset
       where grp.group_set_id  = grpset.group_set_id
         and att.group_id      = grp.group_id
         and att.view_col_name = I_view_col_name
         and  not exists (select 'x'
                            from cfa_attrib att2
                           where att2.attrib_id        = att.attrib_id
                             and att2.group_id         = I_group_id
                             and att2.view_col_name    = I_view_col_name
                             and att2.storage_col_name = I_storage_col_name);

BEGIN

   ---
   open C_VIEW_COL_EXIST;
   fetch C_VIEW_COL_EXIST into L_exist;
   ---
   if C_VIEW_COL_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_VIEW_COL_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.VIEW_COL_NAME_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END VIEW_COL_NAME_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DISPLAY_SEQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_display_seq     IN OUT   CFA_ATTRIB.DISPLAY_SEQ%TYPE,
                              I_group_id        IN       CFA_ATTRIB.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_DISPLAY_SEQ is
      select min(col)
        from (select col
                from (select 1  col_1,  2  col_2,  3  col_3,  4  col_4,  5  col_5,
                             6  col_6,  7  col_7,  8  col_8,  9  col_9,  10 col_10,
                             11 col_11, 12 col_12, 13 col_13, 14 col_14, 15 col_15,
                             16 col_16, 17 col_17, 18 col_18, 19 col_19, 20 col_20,
                             21 col_21, 22 col_22
                        from dual) p
              unpivot
                 (col for coli in (col_1  as 'col_1',  col_2  as 'col_2',  col_3  as 'col_3',  col_4  as 'col_4',  col_5  as 'col_5',
                                   col_6  as 'col_6',  col_7  as 'col_7',  col_8  as 'col_8',  col_9  as 'col_9',  col_10 as 'col_10',
                                   col_11 as 'col_11', col_12 as 'col_12', col_13 as 'col_13', col_14 as 'col_14', col_15 as 'col_15',
                                   col_16 as 'col_16', col_17 as 'col_17', col_18 as 'col_18', col_19 as 'col_19', col_20 as 'col_20',
                                   col_21 as 'col_21', col_22 as 'col_22'))) piv
       where not exists (select 'x'
                           from cfa_attrib
                          where group_id    = I_group_id
                            and display_seq = piv.col);

BEGIN

   open C_GET_DISPLAY_SEQ;
   fetch C_GET_DISPLAY_SEQ into O_display_seq;
   close C_GET_DISPLAY_SEQ;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.GET_NEXT_DISPLAY_SEQ',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_DISPLAY_SEQ;
---------------------------------------------------------------------------------------------------
FUNCTION SET_FF_UI_PARAMETERS(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_ff_ui_param_rec    IN OUT   NOCOPY   FF_UI_PARAM_REC,
                              I_base_rms_table      IN                CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_select              VARCHAR2(2000) := 'select '''||I_base_rms_table||''', ';
   L_basetable_select   VARCHAR2(200) := 'select count(*) from '||I_base_rms_table;
   L_param_cnt           NUMBER := 20;
   L_cnt                 NUMBER := 1;
   L_basetable_count     NUMBER := 0;

   TYPE CUR_key          IS REF CURSOR;
   C_GET_KEY_VALUES      CUR_key;

   TYPE CUR_count        IS REF CURSOR;
   C_GET_BASETBL_COUNT   CUR_count;

   cursor C_GET_KEYS is
      select key_col
        from cfa_ext_entity_key ek
       where base_rms_table = I_base_rms_table
       order by key_number;

   TYPE TYP_key_col is TABLE of C_GET_KEYS%ROWTYPE INDEX BY BINARY_INTEGER;
   TBL_key_col   TYP_key_col;

BEGIN
   ---
   open C_GET_KEYS;
   fetch C_GET_KEYS BULK COLLECT into TBL_key_col;
   close C_GET_KEYS;

   open  C_GET_BASETBL_COUNT for L_basetable_select;
   fetch C_GET_BASETBL_COUNT INTO L_basetable_count;
   close C_GET_BASETBL_COUNT;
   ---
  
   if TBL_key_col is NOT NULL and TBL_key_col.count > 0 then
      if L_basetable_count > 0 then -- Records exist on the basetable, build a query to select key name + key value data
         FOR i in TBL_key_col.FIRST..TBL_key_col.LAST LOOP
            L_select := L_select || ''''||TBL_key_col(L_cnt).key_col||''', '||TBL_key_col(L_cnt).key_col ||', ';
            L_cnt := L_cnt + 1;
         END LOOP;
         ---
         if L_cnt < L_param_cnt then
            FOR i in L_cnt..L_param_cnt LOOP
               L_select := L_select || ' NULL key_name_' || i || ', NULL key_value_' || i || ', ';
            END LOOP;
         end if;
         ---
         L_select := substr(L_select, 1, length(L_select) -2) || ' from '|| I_base_rms_table ||
                     ' where rownum = 1';
         ---
         
         open  C_GET_KEY_VALUES for L_select;
         fetch C_GET_KEY_VALUES INTO IO_ff_ui_param_rec;
         close C_GET_KEY_VALUES;
         ---
      else -- basetable is empty, build a query with only the key names, not the values
         FOR i in TBL_key_col.FIRST..TBL_key_col.LAST LOOP
            L_select := L_select || ''''||TBL_key_col(L_cnt).key_col||''', NULL, ';
            L_cnt := L_cnt + 1;
         END LOOP;
         ---
         if L_cnt < L_param_cnt then
            FOR i in L_cnt..L_param_cnt LOOP
               L_select := L_select || ' NULL key_name_' || i || ', NULL key_value_' || i || ', ';
            END LOOP;
         end if;
         ---
         L_select := substr(L_select, 1, length(L_select) -2) || ' from dual';
         ---
         
         open  C_GET_KEY_VALUES for L_select;
         fetch C_GET_KEY_VALUES INTO IO_ff_ui_param_rec;
         close C_GET_KEY_VALUES;
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.SET_FF_UI_PARAMETERS',
                                            to_char(SQLCODE));
      return FALSE;

END SET_FF_UI_PARAMETERS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_LANG_LABELS (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_group_id       IN      CFA_ATTRIB.GROUP_ID%TYPE,
                                    I_lang           IN      CFA_ATTRIB_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   cursor C_LOCK_ATTRIB_LABEL is
      select 'x'
        from cfa_attrib_labels
       where lang      = I_lang
         and attrib_id in (select attrib_id
                             from cfa_attrib
                            where group_id = I_group_id)
         for update nowait;

BEGIN
   ---
   open C_LOCK_ATTRIB_LABEL;
   close C_LOCK_ATTRIB_LABEL;
   --
   delete
     from cfa_attrib_labels
    where lang      = I_lang
      and attrib_id in (select attrib_id
                          from cfa_attrib
                         where group_id = I_group_id);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_ATTRIB_SQL.DELETE_ATTRIB_LANG_LABELS',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ATTRIB_LANG_LABELS;
---------------------------------------------------------------------------------------------------
FUNCTION SET_FF_UI_PARAMETERS_WRP(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_ff_ui_param_rec_wrp    IN OUT                FF_UI_PARAM_REC_WRP,
                              I_base_rms_table      IN                CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN NUMBER IS
L_program                      VARCHAR2(64):= 'CFA_SETUP_ATTRIB_SQL.SET_FF_UI_PARAMETERS_WRP';
--------------------------------------------------------------------------------------------
IO_ff_ui_param_rec CFA_SETUP_ATTRIB_SQL.FF_UI_PARAM_REC;
BEGIN
---Call original function that uses Booleans
  
   if CFA_SETUP_ATTRIB_SQL.SET_FF_UI_PARAMETERS(O_error_message                 ,
                           IO_ff_ui_param_rec          ,
                           I_base_rms_table) = FALSE then
                
      return 0;
   end if;
---Return number value
	
	IO_ff_ui_param_rec_wrp.key_name_1 :=IO_ff_ui_param_rec.key_name_1;
	IO_ff_ui_param_rec_wrp.key_value_1 :=IO_ff_ui_param_rec.key_value_1;
	IO_ff_ui_param_rec_wrp.key_name_2 :=IO_ff_ui_param_rec.key_name_2;
	IO_ff_ui_param_rec_wrp.key_value_2 :=IO_ff_ui_param_rec.key_value_2;
	IO_ff_ui_param_rec_wrp.key_name_3 :=IO_ff_ui_param_rec.key_name_3;
	IO_ff_ui_param_rec_wrp.key_value_3 :=IO_ff_ui_param_rec.key_value_3;
	IO_ff_ui_param_rec_wrp.key_name_4 :=IO_ff_ui_param_rec.key_name_4;
	IO_ff_ui_param_rec_wrp.key_value_4 :=IO_ff_ui_param_rec.key_value_4;
	IO_ff_ui_param_rec_wrp.key_name_5 :=IO_ff_ui_param_rec.key_name_5;
	IO_ff_ui_param_rec_wrp.key_value_5 :=IO_ff_ui_param_rec.key_value_5;
	IO_ff_ui_param_rec_wrp.key_name_6 :=IO_ff_ui_param_rec.key_name_6;
	IO_ff_ui_param_rec_wrp.key_value_6 :=IO_ff_ui_param_rec.key_value_6;
	IO_ff_ui_param_rec_wrp.key_name_7 :=IO_ff_ui_param_rec.key_name_7;
	IO_ff_ui_param_rec_wrp.key_value_7 :=IO_ff_ui_param_rec.key_value_7;
	IO_ff_ui_param_rec_wrp.key_name_8 :=IO_ff_ui_param_rec.key_name_8;
	IO_ff_ui_param_rec_wrp.key_value_8 :=IO_ff_ui_param_rec.key_value_8;
	IO_ff_ui_param_rec_wrp.key_name_9 :=IO_ff_ui_param_rec.key_name_9;
	IO_ff_ui_param_rec_wrp.key_value_9 :=IO_ff_ui_param_rec.key_value_9;
	IO_ff_ui_param_rec_wrp.key_name_10 :=IO_ff_ui_param_rec.key_name_10;
	IO_ff_ui_param_rec_wrp.key_value_10 :=IO_ff_ui_param_rec.key_value_10;
	IO_ff_ui_param_rec_wrp.key_name_11 :=IO_ff_ui_param_rec.key_name_11;
	IO_ff_ui_param_rec_wrp.key_value_11 :=IO_ff_ui_param_rec.key_value_11;
	IO_ff_ui_param_rec_wrp.key_name_12 :=IO_ff_ui_param_rec.key_name_12;
	IO_ff_ui_param_rec_wrp.key_value_12 :=IO_ff_ui_param_rec.key_value_12;
	IO_ff_ui_param_rec_wrp.key_name_13 :=IO_ff_ui_param_rec.key_name_13;
	IO_ff_ui_param_rec_wrp.key_value_13 :=IO_ff_ui_param_rec.key_value_13;
	IO_ff_ui_param_rec_wrp.key_name_14 :=IO_ff_ui_param_rec.key_name_14;
	IO_ff_ui_param_rec_wrp.key_value_14 :=IO_ff_ui_param_rec.key_value_14;
	IO_ff_ui_param_rec_wrp.key_name_15 :=IO_ff_ui_param_rec.key_name_15;
	IO_ff_ui_param_rec_wrp.key_value_15 :=IO_ff_ui_param_rec.key_value_15;
	IO_ff_ui_param_rec_wrp.key_name_16 :=IO_ff_ui_param_rec.key_name_16;
	IO_ff_ui_param_rec_wrp.key_value_16 :=IO_ff_ui_param_rec.key_value_16;
	IO_ff_ui_param_rec_wrp.key_name_17 :=IO_ff_ui_param_rec.key_name_17;
	IO_ff_ui_param_rec_wrp.key_value_17 :=IO_ff_ui_param_rec.key_value_17;
	IO_ff_ui_param_rec_wrp.key_name_18 :=IO_ff_ui_param_rec.key_name_18;
	IO_ff_ui_param_rec_wrp.key_value_18 :=IO_ff_ui_param_rec.key_value_18;
	IO_ff_ui_param_rec_wrp.key_name_19 :=IO_ff_ui_param_rec.key_name_19;
	IO_ff_ui_param_rec_wrp.key_value_19 :=IO_ff_ui_param_rec.key_value_19;
	IO_ff_ui_param_rec_wrp.key_name_20 :=IO_ff_ui_param_rec.key_name_20;
	IO_ff_ui_param_rec_wrp.key_value_20 :=IO_ff_ui_param_rec.key_value_20;
	 RETURN 1;
--------------------------------------------------------------------------------------------
---Standard function error handling, with number return value
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;
END SET_FF_UI_PARAMETERS_WRP;
--------------------------------------------------------------------------------------------
FUNCTION P_CHK_VALID_NAME(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
					I_sql_name   IN   VARCHAR2) RETURN BOOLEAN IS

   L_valid_name   VARCHAR2(250);

BEGIN
               
       L_valid_name := dbms_assert.qualified_sql_name(I_sql_name);
	   return TRUE;
EXCEPTION
   when OTHERS then      
       O_error_message := SQL_LIB.CREATE_MSG('NAME_INV_CHR',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

END P_CHK_VALID_NAME;
----------------------------------------------------------------------------------------------- 
END CFA_SETUP_ATTRIB_SQL;
/