
CREATE OR REPLACE PACKAGE AIA_PUB_REPORTS AUTHID CURRENT_USER AS

PACKAGE_NAME  CONSTANT VARCHAR2(15) := 'AIA_PUB_REPORTS';

--Application name  --Note that RMS and RESA will not be sending a prefix, only the reference key number.
APPLICATION_REIM CONSTANT VARCHAR(4)  := 'REIM';

--Parameter name
PARAM_REF_KEY CONSTANT VARCHAR(10)      := 'PM_ref_key';
PARAM_DOC_ID  CONSTANT VARCHAR(10)      := 'PM_doc_id';
PARAM_RECEIPT_ID  CONSTANT VARCHAR(20)  := 'PM_receipt_id';

QUERY_PARAM CONSTANT VARCHAR(1)         := '?';
EQUALS      CONSTANT VARCHAR(1)         := '=';

--Delimiter
DELIMITER CONSTANT VARCHAR(1) := '|';
 
--------------------------------------------------------------------------------------------------
--- Function Name: GET_REPORT_URL
--- Purpose:       This function will have a reference key from AIA passed in.
---                It will determine from the prefix of the reference key which system the key is 
---                from (e.g. REIM||1234 and RMS/RESA will not be sending prefixes).  
---                If not prefixed at all, then call the RMS_REPORT_URL function to retrieve 
---                the appropriate URL for the reference key passed for both RMS and RESA.
---                If prefixed with REIM, call the ReIM_REPORT_URL function to retrieve the appropriate 
---                ReIM report URL. .

--------------------------------------------------------------------------------------------------
FUNCTION GET_REPORT_URL (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rpt_url        IN OUT   RETAIL_SERVICE_REPORT_URL.URL%TYPE,
                         I_ref_key        IN       KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
--- Function Name: RMS_REPORT_URL
--- Purpose:       This function will determine which of the three RMS drillback reports to return.
---                First get the  ref_trace_type from KEY_MAP_GL by matching I_ref_key with the 
---                KEY_MAP_GL.REFERENCE_TRACE_ID column.  
---                Once the ref type is determined, look on the RETAIL_SERVICE_REPORT_URL table to 
---                get the appropriate report URL for the matching ref_trace_type.
---                Append the value of I_ref_key to the end of the URL retrieved from the table and 
---                send back to the calling function. 
---                If I_ref_key does not exist on KEY_MAP_GL, send error message back to calling function. 
---                Note that our system does not store the reference_trace_ids prefixed with RMS:: and RESA::,
---                but the key sent from PSFT will have this prefix

--------------------------------------------------------------------------------------------------
FUNCTION RMS_REPORT_URL (O_error_message  OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rpt_url        OUT   RETAIL_SERVICE_REPORT_URL.URL%TYPE,
                         I_ref_key        IN    KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--- Function Name: REIM_REPORT_URL
--- Purpose:       This function will determine which of the four REIM drillback reports URL to return.
---                First get the document type and parameter id from I_ref_key.  
---                Use the document type to fetch the report URL from RETAIL_SERVICE_REPORT_URL table.      
---                Append the URL with the parameter id based on the document type
---                    If the document type is 'RWO' (Receipt Write off) append the URL with receipt id parameter
---                    else with the document id parameter.
--------------------------------------------------------------------------------------------------
FUNCTION REIM_REPORT_URL (O_error_message  OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rpt_url         OUT   RETAIL_SERVICE_REPORT_URL.URL%TYPE,
                         I_ref_key         IN    VARCHAR)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------

END  AIA_PUB_REPORTS;
/
