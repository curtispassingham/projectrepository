CREATE OR REPLACE PACKAGE CFA_SETUP_REC_GROUP_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_RECGRP_EXISTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT VARCHAR2,
                                  I_rec_grp_id      IN     CFA_REC_GROUP.REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_NEXT_RECGRP_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_rec_grp_id    IN OUT CFA_REC_GROUP.REC_GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION REC_GROUP_DELETE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_rec_grp_id    IN     CFA_REC_GROUP.REC_GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION REC_GROUP_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rec_grp_id      IN     CFA_REC_GROUP_LABELS.REC_GROUP_ID%TYPE,
                        I_lang            IN     CFA_REC_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_RG_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rec_grp_id      IN     CFA_REC_GROUP_LABELS.REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_RECGRP_LABELS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists           IN OUT	VARCHAR2,
                              IO_rec_grp_id      IN OUT CFA_REC_GROUP_LABELS.REC_GROUP_ID%TYPE,
                              I_lang             IN     CFA_REC_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_QUERY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   L_stmt             IN       CFA_REC_GROUP.QUERY%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: VALIDATE_TABLE 
-- Purpose:       Validates the given table exists in the owning schema 
--               
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TABLE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_table            IN       ALL_TABLES.TABLE_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: VALIDATE_COLUMN 
-- Purpose:       Validates if the given column exists on the provided table. 
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COLUMN(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_table            IN       ALL_TABLES.TABLE_NAME%TYPE,
                         I_column           IN       ALL_TAB_COLUMNS.COLUMN_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function Name: GET_COLUMN_DATATYPE 
-- Purpose:       This functionn will fetch the column's data type and length from ALL_TAB_COLUMNS.
---------------------------------------------------------------------------------------------------
FUNCTION GET_COLUMN_DATATYPE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_data_type        IN OUT   ALL_TAB_COLUMNS.DATA_TYPE%TYPE,
                             O_data_length      IN OUT   ALL_TAB_COLUMNS.DATA_LENGTH%TYPE,
                             I_table            IN       ALL_TABLES.TABLE_NAME%TYPE,
                             I_column           IN       ALL_TAB_COLUMNS.COLUMN_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function Name: GET_COLUMN_DATATYPE 
-- Purpose:       This function will fetch the "value" column's (column 1) data type and length
--                for the given record group id.
---------------------------------------------------------------------------------------------------
FUNCTION GET_REC_GROUP_DATATYPE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_data_type        IN OUT   ALL_TAB_COLUMNS.DATA_TYPE%TYPE,
                                O_data_length      IN OUT   ALL_TAB_COLUMNS.DATA_LENGTH%TYPE,
                                I_rec_group_id     IN       CFA_REC_GROUP.REC_GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function Name: BUILD_QUERY 
-- Purpose:       This function will build a simple query using the provided details.
---------------------------------------------------------------------------------------------------
FUNCTION BUILD_QUERY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_table_name       IN       CFA_REC_GROUP.TABLE_NAME%TYPE,
                     I_column_1         IN       CFA_REC_GROUP.COLUMN_1%TYPE,
                     I_column_2         IN       CFA_REC_GROUP.COLUMN_2%TYPE,
                     I_where_col_1      IN       CFA_REC_GROUP.WHERE_COL_1%TYPE,
                     I_where_col_1_datatype IN   CFA_REC_GROUP.COL_1_DATA_TYPE%TYPE,
                     I_where_operator_1 IN       CFA_REC_GROUP.WHERE_OPERATOR_1%TYPE,
                     I_where_cond_1     IN       CFA_REC_GROUP.WHERE_COND_1%TYPE,
                     I_where_col_2      IN       CFA_REC_GROUP.WHERE_COL_2%TYPE,
                     I_where_col_2_datatype IN   CFA_REC_GROUP.COL_1_DATA_TYPE%TYPE,
                     I_where_operator_2 IN       CFA_REC_GROUP.WHERE_OPERATOR_2%TYPE,
                     I_where_cond_2     IN       CFA_REC_GROUP.WHERE_COND_2%TYPE,
                     I_date_format      IN       VARCHAR2,
                     O_query            IN OUT   CFA_REC_GROUP.QUERY%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_REC_GROUP_SQL;
/
