CREATE OR REPLACE PACKAGE SVCPROV_CONTEXT AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------
--Public Variables
-----------------------------------------------------------------------------------------
USER_NAME       VARCHAR2(30) := NULL;
USER_COUNTRY    VARCHAR2(3)  := NULL;
USER_LANGUAGE   VARCHAR2(2)  := NULL;
-----------------------------------------------------------------------------------------
-- Function Name: SET_SVCPROV_CONTEXT
-- Purpose:       This function sets the context values.
-----------------------------------------------------------------------------------------
FUNCTION SET_SVCPROV_CONTEXT(O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC") 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
END SVCPROV_CONTEXT;
/