
CREATE OR REPLACE PACKAGE REPORTS_SQL AUTHID CURRENT_USER AS

-- Global variables used in the package body

   PM_business_date              DATE;
   PM_cycle_count                STAKE_HEAD.CYCLE_COUNT%TYPE;
   PM_transfer                   TSFHEAD.TSF_NO%TYPE;
   PM_buyer                      BUYER.BUYER%TYPE;
   PM_lc_ref_id                  LC_HEAD.LC_REF_ID%TYPE;
   PM_order_no                   ORDHEAD.ORDER_NO%TYPE;
   PM_store                      STORE.STORE%TYPE;
   PM_vtype                      POS_TENDER_TYPE_HEAD.TENDER_TYPE_ID%TYPE;
   PM_total_desc                 SA_TOTAL_HEAD.TOTAL_DESC%TYPE;
   PM_supplier                   SUPS.SUPPLIER%TYPE;
   PM_ord_curr                   CURRENCY_RATES.CURRENCY_CODE%TYPE;
   PM_sup_curr                   CURRENCY_RATES.CURRENCY_CODE%TYPE;
   PM_prim_curr                  CURRENCIES.CURRENCY_CODE%TYPE;
   PM_1_week                     DATE;
   PM_2_week                     DATE;
   PM_3_week                     DATE;
   PM_4_week                     DATE;
   PM_dept                       OTB.DEPT%TYPE;
   PM_class                      OTB.CLASS%TYPE;
   PM_subclass                   OTB.SUBCLASS%TYPE;
   
-- Global variables declared only in the XMLP admin tool, but not used in the package body

   PM_change_type                CODE_DETAIL.CODE_DESC%TYPE;
   PM_fiscal_year                NWP.FISCAL_YEAR%TYPE;
   PM_display_only               CODE_DETAIL.CODE_DESC%TYPE;
   PM_effect_date                MOD_ORDER_ITEM_HTS.EFFECT_FROM%TYPE;
   PM_unapprove                  CODE_DETAIL.CODE_DESC%TYPE;
   PM_redstore                   SA_VOUCHER.RED_STORE%TYPE;
   PM_isstore                    SA_VOUCHER.ISS_STORE%TYPE;
   PM_vouchno                    SA_VOUCHER.VOUCHER_NO%TYPE;
   PM_user_id                    VARCHAR2(30);
   PM_card_type                  POS_TENDER_TYPE_HEAD.TENDER_TYPE_DESC%TYPE;
   PM_cc_desc                    STAKE_HEAD.CYCLE_COUNT_DESC%TYPE;
   PM_date                       SA_FLASH_SALES.BUSINESS_DATE%TYPE;
   PM_ly_date                    SA_FLASH_SALES.BUSINESS_DATE%TYPE;
   PM_loc_list                   LOC_LIST_DETAIL.LOC_LIST%TYPE;
   PM_item                       ITEM_MASTER.ITEM%TYPE;
   PM_item_list                  SKULIST_DETAIL.SKULIST%TYPE;
   PM_start_date                 DATE;
   PM_end_date                   DATE;
   PM_location                   SHIPMENT.FROM_LOC%TYPE;
   PM_ship_date                  VARCHAR2(11);
   PM_start_month                DATE;
   PM_end_month                  DATE;
   PM_bol_no                     SHIPMENT.BOL_NO%TYPE;
   PM_pack_no                    ITEM_MASTER.ITEM%TYPE;
   PM_origin_country             ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   PM_wh                         WH.WH%TYPE;
   PM_alloc_no                   ALLOC_HEADER.ALLOC_NO%TYPE;
   PM_to_loc                     STORE.STORE%TYPE;
   PM_future                     CODE_DETAIL.CODE_TYPE%TYPE;
   PM_supplier_trait             SUP_TRAITS_MATRIX.SUP_TRAIT%TYPE;
   PM_date_basis                 CODE_DETAIL.CODE_TYPE%TYPE;
   PM_division                   DIVISION.DIVISION%TYPE;
   PM_group                      GROUPS.GROUP_NO%TYPE;
   PM_alloc_order_no             ALLOC_HEADER.ORDER_NO%TYPE;
   PM_customer_group             WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID%TYPE;
   PM_customer                   WF_CUSTOMER.WF_CUSTOMER_ID%TYPE;
   PM_ref_key                    KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE;
   PM_pgm_name                   MOD_ORDER_ITEM_HTS.PGM_NAME%TYPE;
   PM_exp_days                   NUMBER;
   PM_qty                        NUMBER;
   xdo_user_ui_oracle_lang       LANG.ISO_CODE%TYPE;
-------------------------------------------------------------------------------------------------
   FUNCTION CHECK_NULL_VALUE(I_value1 IN VARCHAR2, I_value2 IN VARCHAR2)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_UOM(I_item IN ITEM_MASTER.ITEM%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION CHECK_SA_STATUS(I_store IN STORE.STORE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_EOM_DATE
      RETURN DATE;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_PRIM_CURR
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_STAKE_DATE
      RETURN DATE;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_STAKE_DESC
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_TENDER_TYPE
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ORDER_BRKT_PCT_OFF
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ORDER_TOTAL_COSTS
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ORIGIN_COUNTRY(I_item     IN ITEM_MASTER.ITEM%TYPE,
                               I_order_no IN ORDHEAD.ORDER_NO%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ORDSKU_REF_ITEM(I_packdet_item IN ITEM_MASTER.ITEM%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ERROR_MESSAGE(I_error_desc IN DAILY_PURGE_ERROR_LOG.ERROR_DESC%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_UOT(I_item IN ITEM_MASTER.ITEM%TYPE,
                    I_loc  IN ITEM_LOC.LOC%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_UOT_SIZE(I_item IN ITEM_MASTER.ITEM%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------	------------------------------
   FUNCTION GET_RETAIL_COST_LOC(I_stake_cost_var  IN STAKE_PROD_LOC.ADJUST_COST%TYPE,
                                I_currency_code   IN CURRENCIES.CURRENCY_CODE%TYPE,
                                I_cost_retail_ind IN VARCHAR2)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CTRY_DESC(I_import_country_id IN HTS.IMPORT_COUNTRY_ID%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CURRENCY_CODE(I_location      IN ITEM_LOC.LOC%TYPE,
                              I_location_type IN ITEM_LOC.LOC_TYPE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_LANG_CODE_DESC(I_code_type IN CODE_DETAIL.CODE_TYPE%TYPE,
                               I_code      IN CODE_DETAIL.CODE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_AMENDED_TEXT(I_amended_field  IN LC_AMENDMENTS.AMENDED_FIELD%TYPE,
                             I_order_no       IN LC_AMENDMENTS.ORDER_NO%TYPE,
                             I_item           IN LC_AMENDMENTS.ITEM%TYPE,
                             I_original_value IN LC_AMENDMENTS.ORIGINAL_VALUE%TYPE,
                             I_new_value      IN LC_AMENDMENTS.NEW_VALUE%TYPE,
                             I_effect         IN LC_AMENDMENTS.EFFECT%TYPE,
                             I_currency_code  IN LC_HEAD.CURRENCY_CODE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_COMPANY_HEADER
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_STORE_UOP(I_pack_size       IN ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                          I_item            IN ITEM_MASTER.ITEM%TYPE,
                          I_orig_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                          I_supplier        IN SUPS.SUPPLIER%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_VENDOR_COST(I_item            IN ITEM_MASTER.ITEM%TYPE,
                            I_orig_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                            I_location        IN ORDLOC.LOCATION%TYPE,
                            I_ordloc_qty      IN ORDLOC.QTY_ORDERED%TYPE,
                            I_supplier        IN SUPS.SUPPLIER%TYPE,
                            I_sup_curr        IN CURRENCY_RATES.CURRENCY_CODE%TYPE,
                            I_ord_curr        IN CURRENCY_RATES.CURRENCY_CODE%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_DEPARTMENT_NAME(I_dept IN DEPS.DEPT%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_COST_CHANGE_DESC(I_cost_change IN COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_AMOUNT_TEXT(I_amount        IN LC_HEAD.AMOUNT%TYPE,
                            I_currency_code IN LC_HEAD.CURRENCY_CODE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_SUM_VENDOR_COST(I_item            IN   ITEM_MASTER.ITEM%TYPE,
                                I_orig_country_id IN   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                I_supplier        IN   SUPS.SUPPLIER%TYPE,
                                I_sup_curr        IN   CURRENCY_RATES.CURRENCY_CODE%TYPE,
                                I_ord_curr        IN   CURRENCY_RATES.CURRENCY_CODE%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_MERCH_HIER_NAME(I_stake_dept     IN STAKE_SKU_LOC.DEPT%TYPE,
                                I_stake_class    IN STAKE_SKU_LOC.CLASS%TYPE,
                                I_stake_subclass IN STAKE_SKU_LOC.SUBCLASS%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_ITEM_DESC(I_item IN ITEM_MASTER.ITEM%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_COST_AND_RETAILS(I_item            IN ITEM_MASTER.ITEM%TYPE,
                                 I_loc             IN TSFHEAD.FROM_LOC%TYPE,
                                 I_loc_type        IN TSFHEAD.FROM_LOC_TYPE%TYPE,
                                 I_tsf_qty         IN TSFDETAIL.TSF_QTY%TYPE,
                                 I_cost_retail_ind IN VARCHAR2)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_SUPP_PARTNER_NAME(I_supplier     IN SUPS.SUPPLIER%TYPE,
                                  I_partner_id   IN PARTNER.PARTNER_ID%TYPE,
                                  I_partner_type IN PARTNER.PARTNER_TYPE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CLIENT_NAME(I_value IN VARCHAR2)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_WH_NAME(I_loc_name IN WH.WH_NAME%TYPE, I_location IN WH.WH%TYPE, I_wh WH.WH%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_LOC_NAME(I_loc_type IN ITEM_LOC.LOC_TYPE%TYPE,
                         I_loc      IN ITEM_LOC.LOC%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_SELLING_UOM(I_item  IN ITEM_MASTER.ITEM%TYPE,
                            I_store IN STORE.STORE%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_TOTAL_VALUE(I_store         IN STORE.STORE%TYPE,
                            I_currency_code IN CURRENCY_RATES.CURRENCY_CODE%TYPE,
                            I_tender_amt    IN SA_TRAN_TENDER.TENDER_AMT%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_LDOM_DATE(I_fdom IN SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE)
      RETURN DATE;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_CURRENCY_VALUE(I_packdet_item     IN ITEM_MASTER.ITEM%TYPE,
                               I_supplier         IN SUPS.SUPPLIER%TYPE,
							   I_orig_country_id  IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_FORMATTED_WH_NAME (I_store IN STORE.STORE%TYPE,
                                   I_wh    IN WH.WH%TYPE)
      RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_FLASH_TOTAL_VALUE (I_store       IN STORE.STORE%TYPE,
                                   I_flash_sales IN SA_FLASH_SALES.NET_SALES%TYPE,
                                   I_total       IN SA_TOTAL.TOTAL_ID%TYPE,
                                   I_prim_curr   IN SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_PAST_DUE_AMT
      RETURN NUMBER;
-------------------------------------------------------------------------------------------------
   FUNCTION GET_LASTDATE
      RETURN DATE;
-------------------------------------------------------------------------------------------------
FUNCTION LANG_CODE_EXTRACT(I_bi_lang_code  IN      LANG.ISO_CODE%TYPE)
RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------
END REPORTS_SQL;
/
