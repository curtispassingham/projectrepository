CREATE OR REPLACE PACKAGE CFA_LOAD_SQL AUTHID CURRENT_USER AS 

--------------------------------------------------------------------------------
-- Name   : LOAD_ATTRIB
-- Purpose: This function generates a dynamic insert-select (load) statement 
--          from the defined passed in staging table to the  entity custom 
--          extension tables based on the metadata definitions
--------------------------------------------------------------------------------
FUNCTION LOAD_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_stg_table_name   IN       CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE,
                     I_process_id       IN       NUMBER DEFAULT NULL,
                     I_del_stg_rec      IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;  
--------------------------------------------------------------------------------
-- Name   : DELETE_STAGING_REC
-- Purpose: This function generates a dynamic delete for the records in the 
--          staging table.
--------------------------------------------------------------------------------
FUNCTION DELETE_STAGING_REC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_stg_table_name   IN       CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE,
                            I_process_id       IN       NUMBER DEFAULT NULL)
   RETURN BOOLEAN;  
--------------------------------------------------------------------------------
END CFA_LOAD_SQL;
/