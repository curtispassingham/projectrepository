CREATE OR REPLACE PACKAGE BODY DBG_SQL AS
--------------------------------------------------------------------------------
PROCEDURE INIT(O_dbg_obj_tbl   IN OUT   TBL_DBG_OBJ) IS

   cursor C_GET_DBG_OBJ is
      select dbg_object,
             ui_msg,
             log_to_tbl
        from debug_cfg
       where db_user = user;
BEGIN
    GP_DBG_OBJ.delete;
    
    for rec in C_GET_DBG_OBJ loop
       GP_DBG_OBJ(rec.dbg_object).dbg_object := rec.dbg_object;
       GP_DBG_OBJ(rec.dbg_object).ui_msg := NVL(rec.ui_msg, 'N');
       GP_DBG_OBJ(rec.dbg_object).log_to_tbl := NVL(rec.log_to_tbl, 'N');
    end loop;
    
    O_dbg_obj_tbl := GP_DBG_OBJ;         
    
END INIT;
--------------------------------------------------------------------------------
PROCEDURE MSG(I_dbg_obj    IN   DEBUG_CFG.DBG_OBJECT%TYPE,
              I_msg        IN   DEBUG_MSG.MSG%TYPE) IS

    PRAGMA AUTONOMOUS_TRANSACTION;
   
BEGIN
   
   if GP_DBG_OBJ.count = 0 then
      INIT(GP_DBG_OBJ);
   end if;
   ---
   if GP_DBG_OBJ.count > 0 and 
      GP_DBG_OBJ.exists(I_dbg_obj) then
      
      LP_seq := NVL(LP_seq, 0) + 1;
   
      insert into debug_msg values (user,
                                    I_dbg_obj,
                                    LP_seq,
                                    I_msg,
                                    DBMS_UTILITY.FORMAT_CALL_STACK,
                                    current_timestamp);
   
      commit;
   end if;

END MSG;
--------------------------------------------------------------------------------
END;
/