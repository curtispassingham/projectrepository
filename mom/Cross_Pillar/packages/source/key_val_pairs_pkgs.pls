CREATE OR REPLACE PACKAGE KEY_VAL_PAIRS_PKG AUTHID CURRENT_USER
AS
  -----------------------------------------------------------------------------
  FUNCTION get_map(
      I_pairs IN key_val_pairs)
    RETURN VARCHAR2 DETERMINISTIC;
  -----------------------------------------------------------------------------
  FUNCTION set_attr(
      I_pairs IN key_val_pairs,
      I_key   IN VARCHAR2,
      I_val   IN VARCHAR2)
    RETURN key_val_pairs;
  -----------------------------------------------------------------------------
  FUNCTION get_attr(
      I_pairs IN key_val_pairs,
      I_key   IN VARCHAR2)
    RETURN VARCHAR2;
  -----------------------------------------------------------------------------
  FUNCTION delete_key(
      I_pairs IN key_val_pairs,
      I_key   IN VARCHAR2)
    RETURN key_val_pairs;
  -----------------------------------------------------------------------------
END key_val_pairs_pkg;
/