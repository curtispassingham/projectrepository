
CREATE OR REPLACE PACKAGE BODY ASYNC_JOB_LOG_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION ASYNCH_ID_VALIDATE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_job_type                  OUT RMS_ASYNC_STATUS.JOB_TYPE%TYPE,
                            O_status                    OUT RMS_ASYNC_STATUS.STATUS%TYPE,
                            O_create_datetime           OUT RMS_ASYNC_STATUS.CREATE_DATETIME%TYPE,
                            O_create_id                 OUT RMS_ASYNC_STATUS.CREATE_ID%TYPE,
                            I_rms_async_id           IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'ASYNC_JOB_LOG_SQL.ASYNCH_ID_VALIDATE';

   cursor C_ASYNC_JOB_DETAILS is
      select job_type,
             status,
             create_datetime,
             create_id
        from rms_async_status
       where rms_async_id = I_rms_async_id;

BEGIN

   if I_rms_async_id is NOT NULL then

      SQL_LIB.SET_MARK('OPEN',
                       'C_ASYNC_JOB_DETAILS',
                       'RMS_ASYNC_STATUS',
                       'RMS_ASYNC_ID: '||to_char(I_rms_async_id));
      open C_ASYNC_JOB_DETAILS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_ASYNC_JOB_DETAILS',
                       'RMS_ASYNC_STATUS',
                       'RMS_ASYNC_ID: '||to_char(I_rms_async_id));
      fetch C_ASYNC_JOB_DETAILS into O_job_type,
                                     O_status,
                                     O_create_datetime,
                                     O_create_id;

      if C_ASYNC_JOB_DETAILS%NOTFOUND then
         O_error_message := 'INV_VALUES_LOV';

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ASYNC_JOB_DETAILS',
                          'RMS_ASYNC_STATUS',
                          'RMS_ASYNC_ID: '||to_char(I_rms_async_id));
         close C_ASYNC_JOB_DETAILS;
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_ASYNC_JOB_DETAILS',
                       'RMS_ASYNC_STATUS',
                       'RMS_ASYNC_ID: '||to_char(I_rms_async_id));
      close C_ASYNC_JOB_DETAILS;
   else

      O_error_message := 'FIELD_NOT_NULL';
      return FALSE;

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ASYNCH_ID_VALIDATE;
-------------------------------------------------------------------------------------------------------
FUNCTION JOB_TYPE_NAME(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_job_description           OUT RMS_ASYNC_JOB.JOB_DESCRIPTION%TYPE,
                       I_job_type               IN     RMS_ASYNC_JOB.JOB_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'ASYNC_JOB_LOG_SQL.JOB_TYPE_NAME';

   cursor C_ASYNC_JOB_DESC is
      select job_description
        from rms_async_job
       where job_type = I_job_type;

BEGIN

   if I_job_type is NOT NULL then

      SQL_LIB.SET_MARK('OPEN',
                       'C_ASYNC_JOB_DESC',
                       'RMS_ASYNC_JOB',
                       'JOB_TYPE: '||to_char(I_job_type));
      open C_ASYNC_JOB_DESC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_ASYNC_JOB_DESC',
                       'RMS_ASYNC_JOB',
                       'JOB_TYPE: '||to_char(I_job_type));
      fetch C_ASYNC_JOB_DESC into O_job_description;

      if C_ASYNC_JOB_DESC%NOTFOUND then
         O_error_message := 'INV_VALUES_LOV';

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ASYNC_JOB_DESC',
                          'RMS_ASYNC_JOB',
                          'JOB_TYPE: '||to_char(I_job_type));
         close C_ASYNC_JOB_DESC;
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_ASYNC_JOB_DESC',
                       'RMS_ASYNC_JOB',
                       'JOB_TYPE: '||to_char(I_job_type));
      close C_ASYNC_JOB_DESC;
   else

      O_error_message := 'FIELD_NOT_NULL';
      return FALSE;

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END JOB_TYPE_NAME;
-------------------------------------------------------------------------------------------------------
FUNCTION ASYNC_JOB_RETRY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_async_id           IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                         I_job_type               IN     RMS_ASYNC_STATUS.JOB_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'ASYNC_JOB_LOG_SQL.ASYNC_JOB_RETRY';

BEGIN

   /*Determine what the job type is, and then call the appropriate retry function. */
   if I_job_type = RMS_CONSTANTS.ASYNC_JOB_NEW_ITEM_LOC then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_NEW_ITEM_LOC_RETRY(O_error_message,
                                                          I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_UPDATE_ITEM_LOC then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_UPDATE_ITEM_LOC_RETRY(O_error_message,
                                                             I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_STORE_ADD then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_STORE_ADD_RETRY(O_error_message,
                                                       I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_WH_ADD then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_WH_ADD_RETRY(O_error_message,
                                                    I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_STKLGR_INSERT then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_STKLGR_INSERT_RETRY(O_error_message,
                                                           I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_ITEM_INDUCT then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_ITEM_INDUCT_RETRY(O_error_message,
                                                         I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_PO_INDUCT then
      if RMS_ASYNC_PROCESS_SQL.ENQUEUE_PO_INDUCT_RETRY(O_error_message,
                                                       I_rms_async_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ASYNC_JOB_RETRY;
-------------------------------------------------------------------------------------------------------
END ASYNC_JOB_LOG_SQL;
/
