CREATE OR REPLACE PACKAGE ASYNC_JOB_LOG_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------------------------------
-- Function Name:  ASYNCH_ID_VALIDATE
-- Purpose      :  Validates and returns details for a passed in async job id.
-------------------------------------------------------------------------------------------------------
FUNCTION ASYNCH_ID_VALIDATE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_job_type                  OUT RMS_ASYNC_STATUS.JOB_TYPE%TYPE,
                            O_status                    OUT RMS_ASYNC_STATUS.STATUS%TYPE,
                            O_create_datetime           OUT RMS_ASYNC_STATUS.CREATE_DATETIME%TYPE,
                            O_create_id                 OUT RMS_ASYNC_STATUS.CREATE_ID%TYPE,
                            I_rms_async_id           IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  JOB_TYPE_NAME
-- Purpose      :  Returns the job type's name.
-------------------------------------------------------------------------------------------------------
FUNCTION JOB_TYPE_NAME(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_job_description           OUT RMS_ASYNC_JOB.JOB_DESCRIPTION%TYPE,
                       I_job_type               IN     RMS_ASYNC_JOB.JOB_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ASYNC_JOB_RETRY
-- Purpose      :  Retries a job that encountered an error.
-------------------------------------------------------------------------------------------------------
FUNCTION ASYNC_JOB_RETRY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_async_id           IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                         I_job_type               IN     RMS_ASYNC_STATUS.JOB_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END ASYNC_JOB_LOG_SQL;
/
