CREATE OR REPLACE PACKAGE DBG_SQL AUTHID CURRENT_USER AS 

TYPE TYP_DBG_OBJ is RECORD
(
   dbg_object   DEBUG_CFG.DBG_OBJECT%TYPE,
   ui_msg       DEBUG_CFG.UI_MSG%TYPE,
   log_to_tbl   DEBUG_CFG.LOG_TO_TBL%TYPE
);

TYPE TBL_DBG_OBJ is TABLE of TYP_DBG_OBJ INDEX BY VARCHAR2(61);

GP_DBG_OBJ TBL_DBG_OBJ;

LP_seq   NUMBER;

--------------------------------------------------------------------------------
PROCEDURE INIT(O_dbg_obj_tbl   IN OUT  TBL_DBG_OBJ);
--------------------------------------------------------------------------------
PROCEDURE MSG(I_dbg_obj    IN   DEBUG_CFG.DBG_OBJECT%TYPE,
              I_msg        IN   DEBUG_MSG.MSG%TYPE);
-------------------------------------------------------------------------------- 

END;
/