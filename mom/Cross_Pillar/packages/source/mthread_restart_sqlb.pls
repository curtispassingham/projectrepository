CREATE OR REPLACE
PACKAGE body mthread_restart_sql
AS
  ------------------------------------------------------------------------------
PROCEDURE sleep_ms(
    p_sleep IN NUMBER )
AS
  language java name 'java.lang.Thread.sleep( long )';
  ------------------------------------------------------------------------------
FUNCTION start_thread(
    O_error_message    IN OUT rtk_errors.rtk_text%type,
    I_program_name     IN restart_program_status.program_name%type,
    I_restart_name     IN restart_bookmark.restart_name%type,
    I_thread_val       IN restart_bookmark.thread_val%type,
    IO_is_restart      IN OUT BOOLEAN,
    IO_bookmark_params IN OUT VARR )
  RETURN BOOLEAN
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_program VARCHAR2(255) := 'mthread_restart_sql.start_thread';
  CURSOR c_exists
  IS
    SELECT bookmark_string
    FROM restart_bookmark
    WHERE restart_name = I_restart_name
    AND thread_val     = I_thread_val;
  L_restart_flag CHAR := 'N';
  L_bookmark_string restart_bookmark.bookmark_string%type;
BEGIN
  OPEN c_exists;
  FETCH c_exists
  INTO L_bookmark_string;
  IO_bookmark_params := comma2varr(L_bookmark_string);
  IF c_exists%found THEN
    IO_is_restart  := true;
    L_restart_flag := 'Y';
  ELSE
    IO_is_restart  := false;
    L_restart_flag := 'N';
  END IF;
  CLOSE c_exists;
  IF NOT IO_is_restart THEN
    merge INTO restart_program_status rps USING
    (SELECT I_restart_name               AS restart_name,
      I_thread_val                       AS thread_val,
      I_program_name                     AS program_name,
      sys_context('userenv','sessionid') AS current_oracle_sid,
      sysdate                            AS start_time,
      'started'                          AS program_status
    FROM dual
    ) sq ON (rps.restart_name = sq.restart_name AND rps.thread_val = sq.thread_val)
  WHEN matched THEN
    UPDATE
    SET restart_flag     = L_restart_flag ,
      restart_time       = sq.start_time ,
      program_status     = sq.program_status,
      current_oracle_sid = sq.current_oracle_sid --
      WHEN NOT matched THEN
    INSERT
      (
        restart_name,
        thread_val,
        start_time,
        program_name,
        program_status,
        current_oracle_sid
      )
      VALUES
      (
        sq.restart_name,
        sq.thread_val,
        sq.start_time,
        sq.program_name,
        sq.program_status,
        sq.current_oracle_sid
      );
    --
    INSERT
    INTO restart_bookmark
      (
        RESTART_NAME,
        THREAD_VAL
      )
      VALUES
      (
        I_restart_name,
        I_thread_val
      );
  END IF;
  COMMIT;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  ROLLBACK;
  RETURN FALSE;
END start_thread;
------------------------------------------------------------------------------
FUNCTION commit_thread
  (
    O_error_message   IN OUT rtk_errors.rtk_text%type,
    I_restart_name    IN restart_bookmark.restart_name%type,
    I_thread_val      IN restart_bookmark.thread_val%type,
    I_bookmark_params IN VARR
  )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'mthread_restart_sql.commit_thread';
  L_bookmark_string restart_bookmark.bookmark_string%type;
BEGIN
  L_bookmark_string := varr2comma(I_bookmark_params);
  UPDATE restart_bookmark
  SET bookmark_string = L_bookmark_string ,
    num_commits       = NVL(num_commits,0)+1
  WHERE restart_name  = I_restart_name
  AND thread_val      = I_thread_val;
  --
  COMMIT;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END commit_thread;
------------------------------------------------------------------------------
FUNCTION finish_thread(
    O_error_message  IN OUT rtk_errors.rtk_text%type,
    I_restart_name   IN restart_bookmark.restart_name%type,
    I_thread_val     IN restart_bookmark.thread_val%type,
    I_success_flag   IN CHAR,
    I_NON_FATAL_FLAG IN CHAR,
    I_err_msg        IN restart_program_status.err_message%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'mthread_restart_sql.finish_thread';
BEGIN
  --
  UPDATE restart_program_status
  SET finish_time    = sysdate ,
    err_message      = I_err_msg
  WHERE restart_name = I_restart_name
  AND thread_val     = I_thread_val;
  --
  IF I_success_flag = 'Y' THEN
    INSERT
    INTO restart_program_history
      (
        RESTART_NAME,
        THREAD_VAL,
        START_TIME,
        PROGRAM_NAME,
        NUM_THREADS,
        COMMIT_MAX_CTR,
        RESTART_TIME,
        FINISH_TIME,
        SHADOW_PID,
        SUCCESS_FLAG,
        NON_FATAL_ERR_FLAG,
        NUM_COMMITS,
        AVG_TIME_BTWN_COMMITS
      )
    SELECT rps.restart_name,
      rps.thread_val,
      rps.start_time,
      rps.program_name,
      rc.num_threads,
      rc.commit_max_ctr,
      rps.restart_time,
      rps.finish_time,
      rps.current_shadow_pid,
      I_success_flag,
      rb.NON_FATAL_ERR_FLAG,
      rb.NUM_COMMITS,
      rb.AVG_TIME_BTWN_COMMITS
    FROM restart_control rc,
      restart_program_status rps,
      restart_bookmark rb
    WHERE rps.restart_name = I_restart_name
    AND rps.thread_val     = I_thread_val
    AND rc.program_name    = rps.program_name
    AND rps.restart_name   = rb.restart_name
    AND rps.thread_val     = rb.thread_val;
    --
    DELETE
    FROM restart_bookmark
    WHERE restart_name = I_restart_name
    AND thread_val     = I_thread_val;
    --
    DELETE
    FROM restart_program_status
    WHERE restart_name = I_restart_name
    AND thread_val     = I_thread_val;
    --
  END IF;
  COMMIT;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END finish_thread;
------------------------------------------------------------------------------
FUNCTION wait4jobs(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    IO_jobs_coll    IN OUT VARR,
    I_max_threads   IN NUMBER,
    I_wait_ms       IN NUMBER )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'mthread_restart_sql.wait4threads';
  CURSOR c_running_jobs
  IS
    SELECT job_name
    FROM USER_SCHEDULER_JOBS
    WHERE job_name IN
      (SELECT * FROM TABLE(IO_jobs_coll)
      );
  L_count   NUMBER;
  L_wait_ms NUMBER;
  L_temp_jobs VARR;
BEGIN
  IF NVL(I_wait_ms,0) <= 0 THEN
    L_wait_ms         := 1000;
  ELSE
    L_wait_ms := I_wait_ms;
  END IF;
  LOOP
    OPEN c_running_jobs;
    FETCH c_running_jobs BULK COLLECT INTO L_temp_jobs;
    CLOSE c_running_jobs;
    IO_jobs_coll := L_temp_jobs;
    EXIT
  WHEN L_temp_jobs.count < I_max_threads;
    sleep_ms(L_wait_ms);
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END wait4jobs;
------------------------------------------------------------------------------
FUNCTION comma2varr(
    I_str IN VARCHAR2)
  RETURN varr
IS
  O_varr varr          := NEW varr();
  l_delimiter CHAR     :=',';
  l_pos PLS_INTEGER    := INSTR(I_str, l_delimiter);
  l_str VARCHAR2(32000):=I_str;
BEGIN
  WHILE l_pos > 0
  LOOP
    O_varr.extend;
    O_varr(O_varr.count) := SUBSTR(l_str,1,l_pos-1);
    l_str                := SUBSTR(l_str,l_pos  +1);
    l_pos                := INSTR(l_str, l_delimiter);
  END LOOP;
  O_varr.extend;
  O_varr(O_varr.count) := l_str;
  RETURN O_varr;
END comma2varr;
------------------------------------------------------------------------------
FUNCTION varr2comma(
    I_varr IN VARR)
  RETURN VARCHAR2
IS
  O_comma_str VARCHAR2(4000);
  l_delimiter CHAR :=',';
BEGIN
  IF I_varr IS NOT NULL THEN
    FOR i IN 1..I_varr.count
    LOOP
      O_comma_str   := O_comma_str || I_varr(i);
      IF i          != I_varr.count THEN
        O_comma_str := O_comma_str || l_delimiter;
      END IF;
    END LOOP;
  END IF;
  RETURN O_comma_str;
END varr2comma;
------------------------------------------------------------------------------
FUNCTION submit_job(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    IO_jobs_coll    IN OUT VARR,
    I_job_name      IN VARCHAR2,
    I_pls_block     IN VARCHAR2,
    I_comments      IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'mthread_restart_sql.submit_job';
BEGIN
  dbms_scheduler.create_job(job_name => I_job_name, job_type => 'PLSQL_BLOCK', job_action => I_pls_block, start_date => SYSTIMESTAMP,comments => I_comments, enabled => true, auto_drop => true);
  IF IO_jobs_coll IS NULL THEN
    IO_jobs_coll  := NEW varr();
  END IF;
  IO_jobs_coll.extend;
  IO_jobs_coll(IO_jobs_coll.count):=I_job_name;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END submit_job;
------------------------------------------------------------------------------
FUNCTION upd_error(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_restart_name restart_bookmark.restart_name%type,
    I_thread_val IN restart_bookmark.thread_val%type,
    I_err_msg    IN restart_program_status.err_message%type)
  RETURN BOOLEAN
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  L_program VARCHAR2(255) := 'mthread_restart_sql.upd_error';
BEGIN
  UPDATE restart_program_status
  SET err_message    =I_err_msg
  WHERE restart_name = I_restart_name
  AND thread_val     = I_thread_val;
  COMMIT;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END upd_error;
------------------------------------------------------------------------------
FUNCTION check_unfinished(
    O_error_message    IN OUT rtk_errors.rtk_text%type,
    O_unfinished_count IN OUT NUMBER,
    I_restart_name     IN restart_bookmark.restart_name%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'mthread_restart_sql.upd_error';
  CURSOR c_unfinished_count
  IS
    SELECT COUNT(*) FROM restart_bookmark WHERE restart_name = I_restart_name;
BEGIN
  O_unfinished_count :=0;
  OPEN c_unfinished_count;
  FETCH c_unfinished_count INTO O_unfinished_count;
  CLOSE c_unfinished_count;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END check_unfinished;
------------------------------------------------------------------------------
END mthread_restart_sql;
/