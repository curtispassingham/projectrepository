create or replace PACKAGE RMS_NOTIFICATION_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
-- Function Name: WRITE_RAF_NOTIFICATION
-- Purpose      : 
------------------------------------------------------------------------------------------------
FUNCTION WRITE_RAF_NOTIFICATION(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_taskflow_url       IN OUT   RMS_ASYNC_JOB.TASKFLOW_URL%TYPE,
                                I_job_type            IN       VARCHAR2,
                                I_key_no              IN       NUMBER,
                                I_mode                IN       VARCHAR2 DEFAULT 'VIEW',
                                I_recipient           IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;                                 
------------------------------------------------------------------------------------------------
END RMS_NOTIFICATION_SQL;
/
