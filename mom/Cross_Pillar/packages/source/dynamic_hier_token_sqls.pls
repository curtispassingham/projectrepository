CREATE OR REPLACE PACKAGE DYNAMIC_HIER_TOKEN_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------
-- Name:    GET_MAPPING_STRING
-- Purpose: This function is used to return the dynamic hierarchy token to string  
--          mapping. This function is called during RMS application installation
--          to replace the dynamic hierarchy token in the application bundle (.xlf)
--          file. The application installer and patch installer will get the 
--          token to string mapping for every supported langauge by this function
--          and then the installation process will replace all tokens in the .xlf
--          files, including the language bundle
----------------------------------------------------------------------------
FUNCTION GET_MAPPING_STRING(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_token_mapping_string IN OUT VARCHAR2,
                            I_lang                 IN     LANG.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:    REPLACE_RTK_ERRORS_TOKENS
-- Purpose: This function replaces the dynamic hierarchy token in RTK_ERRORS and 
--          RTK_ERRORS_TL table. This function should be called during installation
--          and any patch installation in which the rtk_errors or its langauge pack
--          is applied. 
----------------------------------------------------------------------------
FUNCTION REPLACE_RTK_ERRORS_TOKENS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------
-- Name:    REPLACE_CODE_DETAIL_TOKENS
-- Purpose: This function replaces the dynamic hierarchy token in CODE_DETAIL and 
--          CODE_DETAIL_TL table. This function should be called during installation
--          and any patch installation in which the rtk_errors or its langauge pack
--          is applied. 
----------------------------------------------------------------------------
FUNCTION REPLACE_CODE_DETAIL_TOKENS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    REPLACE_TOKENS
-- Purpose: This function replaces the dynamic hierarchy token in all tables
--          which holds dynamic hierarchy tokens - Currently Code_detail and 
--          rtk_errors.
----------------------------------------------------------------------------
FUNCTION REPLACE_TOKENS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
END DYNAMIC_HIER_TOKEN_SQL;
/


