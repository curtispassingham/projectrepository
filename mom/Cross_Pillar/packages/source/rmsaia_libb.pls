CREATE OR REPLACE PACKAGE BODY RMSAIA_LIB AS

--------------------------------------------------------------------------------
--                            PRIVATE GLOBALS                                 --
--------------------------------------------------------------------------------

LP_IllegalArgument        VARCHAR2(255) :=
'com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException';
LP_EntityAlreadyExists    VARCHAR2(255) :=
'com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException';
LP_EntityNotFound         VARCHAR2(255) :=
'com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException';
LP_IllegalState           VARCHAR2(255) :=
'com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException';
LP_Validation             VARCHAR2(255) :=
'com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException';
LP_UnsupportedOperation   VARCHAR2(255) :=
'java.lang.UnsupportedOperationException';
--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------


PROCEDURE BUILD_SERVICE_OP_STATUS(O_ServiceOpStatus    OUT "RIB_ServiceOpStatus_REC",
                                  I_status_code     IN     VARCHAR2,
                                  I_error_message   IN     VARCHAR2,
                                  I_program         IN     VARCHAR2)
IS

L_program                 VARCHAR2(65) := 'RMSAIA_LIB.BUILD_SERVICE_OP_STATUS';
L_RIB_SuccessStatus_REC   "RIB_SuccessStatus_REC" := null;
L_RIB_SuccessStatus_TBL   "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
L_RIB_FailStatus_REC      "RIB_FailStatus_REC" := null;
L_RIB_FailStatus_TBL      "RIB_FailStatus_TBL" := "RIB_FailStatus_TBL"();

BEGIN

   if I_status_code = API_CODES.SUCCESS then

      L_RIB_SuccessStatus_REC := "RIB_SuccessStatus_REC"(0,
                                                         I_program);

      L_RIB_SuccessStatus_TBL.extend;
      L_RIB_SuccessStatus_TBL(1) := L_RIB_SuccessStatus_REC;

   else
      L_RIB_FailStatus_REC := "RIB_FailStatus_REC"(0,
                                                   LP_IllegalState,
                                                   substr(I_error_message, 1, 100),
                                                   substr(I_error_message, 1, 255));

      L_RIB_FailStatus_TBL.extend;
      L_RIB_FailStatus_TBL(1) := L_RIB_FailStatus_REC;

   end if;

   O_ServiceOpStatus := "RIB_ServiceOpStatus_REC"(0,
                                                  L_RIB_SuccessStatus_TBL,
                                                  L_RIB_FailStatus_TBL);

END BUILD_SERVICE_OP_STATUS;
--------------------------------------------------------------------------------
PROCEDURE BUILD_NOSERVICE_OP_STATUS(O_ServiceOpStatus OUT "RIB_ServiceOpStatus_REC",
                                    I_error_message   IN     VARCHAR2,
                                    I_program         IN     VARCHAR2)
IS

L_status_code              VARCHAR2(1) := API_CODES.UNHANDLED_ERROR;
L_program                  VARCHAR2(65) := 'RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS';
L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
L_message_text             VARCHAR2(190) := ' WebService is currently not supported';

L_RIB_SuccessStatus_REC   "RIB_SuccessStatus_REC" := null;
L_RIB_SuccessStatus_TBL   "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
L_RIB_FailStatus_REC      "RIB_FailStatus_REC" := null;
L_RIB_FailStatus_TBL      "RIB_FailStatus_TBL" := "RIB_FailStatus_TBL"();

BEGIN

   if L_status_code = API_CODES.UNHANDLED_ERROR then
      L_error_message := substr(I_program,28,65)||L_message_text; 
   
      L_RIB_FailStatus_REC := "RIB_FailStatus_REC"(0,
                                                   LP_IllegalState,
                                                   substr(L_error_message, 1, 100),
                                                   substr(L_error_message, 1, 255));

      L_RIB_FailStatus_TBL.extend;
      L_RIB_FailStatus_TBL(1) := L_RIB_FailStatus_REC;
   else 
      L_RIB_SuccessStatus_REC := "RIB_SuccessStatus_REC"(0,
                                                         I_program);

      L_RIB_SuccessStatus_TBL.extend;
      L_RIB_SuccessStatus_TBL(1) := L_RIB_SuccessStatus_REC;
 
   end if;

   O_ServiceOpStatus := "RIB_ServiceOpStatus_REC"(0,
                                                  L_RIB_SuccessStatus_TBL,
                                                  L_RIB_FailStatus_TBL);

END BUILD_NOSERVICE_OP_STATUS;
--------------------------------------------------------------------------------

END RMSAIA_LIB;

/
