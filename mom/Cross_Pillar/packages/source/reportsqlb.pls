



CREATE OR REPLACE PACKAGE BODY REPORTS_SQL AS
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_NULL_VALUE(I_value1 IN VARCHAR2, I_value2 IN VARCHAR2)
   RETURN VARCHAR2 IS

BEGIN

   if I_value1 is NULL then
      return I_value2;
   end if;

   return I_value1;

EXCEPTION
   when OTHERS then
      return -1;

END CHECK_NULL_VALUE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_UOM(I_item IN ITEM_MASTER.ITEM%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE         := NULL;
   L_standard_uom                UOM_CLASS.UOM%TYPE               := NULL;
   L_standard_class              UOM_CLASS.UOM_CLASS%TYPE         := NULL;
   L_uom_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE := NULL;

BEGIN
   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(L_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_uom_conv_factor,
                                       I_item,
                                       NULL) = FALSE then
      return -1;
   end if;

   return L_standard_uom;

EXCEPTION
   when OTHERS then
      return -1;

END GET_UOM;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_SA_STATUS(I_store IN STORE.STORE%TYPE)
   RETURN VARCHAR2 IS

   L_exist                       SA_EXPORT_LOG.STATUS%TYPE := 'Z';

   cursor C_DATA_STATUS is
      select e.status
        from sa_store_day s,
             sa_export_log e
       where s.business_date = PM_business_date
         and s.store = I_store
         and s.store_day_seq_no = e.store_day_seq_no
         and e.system_code = 'RMS';

BEGIN

   open C_DATA_STATUS;
   fetch C_DATA_STATUS into L_exist;
   close C_DATA_STATUS;

   if L_exist != 'E' then
      return '!';
   else
      return NULL;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END CHECK_SA_STATUS;
-------------------------------------------------------------------------------------------------
FUNCTION GET_EOM_DATE
   RETURN DATE IS

   L_eom_date                    SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE;

   cursor C_GET_EOM_DATE is
      select last_eom_date
        from system_variables;
BEGIN

   open C_GET_EOM_DATE;
   fetch C_GET_EOM_DATE into L_eom_date;
   close C_GET_EOM_DATE;

   L_eom_date := L_eom_date + 1;

   return L_eom_date;

EXCEPTION
   when OTHERS then
      return NULL;

END GET_EOM_DATE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_CURR
   RETURN VARCHAR2 IS

   L_prim_curr                   SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(L_error_message,
                                       L_prim_curr) = FALSE then
      return -1;
   end if;

   return L_prim_curr;

EXCEPTION
   when OTHERS then
      return -1;

END GET_PRIM_CURR;
-------------------------------------------------------------------------------------------------
FUNCTION GET_STAKE_DATE
   RETURN DATE IS

   L_stake_date                  STAKE_HEAD.STOCKTAKE_DATE%TYPE;

   cursor C_GET_STAKE_DATE is
      select stocktake_date
        from stake_head
       where cycle_count = PM_cycle_count;

BEGIN

   open C_GET_STAKE_DATE;
   fetch C_GET_STAKE_DATE into L_stake_date;
   close C_GET_STAKE_DATE;

   return L_stake_date;

EXCEPTION
   when OTHERS then
      return NULL;

END GET_STAKE_DATE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_STAKE_DESC
   RETURN VARCHAR2 IS

   L_stake_desc                  STAKE_HEAD.CYCLE_COUNT_DESC%TYPE;

   cursor C_GET_STAKE_DESC is
      select cycle_count_desc
        from stake_head
       where cycle_count = PM_cycle_count;

BEGIN

   open C_GET_STAKE_DESC;
   fetch C_GET_STAKE_DESC into L_stake_desc;
   close C_GET_STAKE_DESC;

   return L_stake_desc;

EXCEPTION
   when OTHERS then
      return -1;

END GET_STAKE_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_TENDER_TYPE
   RETURN VARCHAR2 IS

   L_tender_type_desc            POS_TENDER_TYPE_HEAD.TENDER_TYPE_DESC%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if TENDER_TYPE_SQL.GET_TENDER_TYPE_DESC(L_error_message,
                                           L_tender_type_desc,
                                           'VOUCH',
                                           PM_vtype) = FALSE then
      return -1;
   end if;

   return L_tender_type_desc;

EXCEPTION
   when OTHERS then
      return -1;

END GET_TENDER_TYPE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_BRKT_PCT_OFF
   RETURN NUMBER IS

   L_brkt_pct_off                NUMBER;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if ORDER_CALC_SQL.BRKT_PCT_OFF(L_error_message,
                                  L_brkt_pct_off,
                                  PM_order_no) = FALSE then
      return 0;
   end if;

   return L_brkt_pct_off;

EXCEPTION
   when OTHERS then
      return 0;

END GET_ORDER_BRKT_PCT_OFF;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_TOTAL_COSTS
   RETURN NUMBER IS

   L_total_cost     ORDLOC.UNIT_COST%TYPE;
   L_pres_cost      ORDLOC.UNIT_COST%TYPE;
   L_outs_cost      ORDLOC.UNIT_COST%TYPE;
   L_can_cost       ORDLOC.UNIT_COST%TYPE;
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if ORDER_CALC_SQL.TOTAL_COSTS(L_error_message,
                                 L_total_cost,
                                 L_pres_cost,
                                 L_outs_cost,
                                 L_can_cost,
                                 PM_order_no,
                                 NULL,
                                 NULL) = FALSE then
      return 0;
   end if;

   return L_total_cost;

EXCEPTION
   when OTHERS then
      return 0;

END GET_ORDER_TOTAL_COSTS;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORIGIN_COUNTRY(I_item     IN ITEM_MASTER.ITEM%TYPE,
                            I_order_no IN ORDHEAD.ORDER_NO%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists                      BOOLEAN;
   L_origin_country              COUNTRY.COUNTRY_ID%TYPE;

BEGIN

   if I_item is not NULL then
      if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(L_error_message,
                                                  L_exists,
                                                  L_origin_country,
                                                  I_order_no,
                                                  I_item) then
         if L_exists = TRUE then
            return L_origin_country;
         end if;
      end if;
   else
      if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(L_error_message,
                                                L_origin_country,
                                                I_order_no) then
         return L_origin_country;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_ORIGIN_COUNTRY;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDSKU_REF_ITEM(I_packdet_item IN ITEM_MASTER.ITEM%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_ref_item                    ORDSKU.REF_ITEM%TYPE;

BEGIN

   if ORDER_ITEM_ATTRIB_SQL.GET_REF_ITEM(L_error_message,
                                         L_ref_item,
                                         PM_order_no,
                                         I_packdet_item) = FALSE then
      return -1;
   end if;

   return L_ref_item;

EXCEPTION
   when OTHERS then
      return -1;

END GET_ORDSKU_REF_ITEM;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ERROR_MESSAGE(I_error_desc IN DAILY_PURGE_ERROR_LOG.ERROR_DESC%TYPE)
   RETURN VARCHAR2 IS

   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   L_error_message := SQL_LIB.GET_MESSAGE_TEXT(I_error_desc,
                                               NULL,
                                               NULL,
                                               NULL);
   return L_error_message;

EXCEPTION
   when OTHERS then
      return -1;

END GET_ERROR_MESSAGE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_UOT(I_item IN ITEM_MASTER.ITEM%TYPE,
                 I_loc  IN ITEM_LOC.LOC%TYPE)
   RETURN VARCHAR2 IS

   L_supp_pack_size              ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_uom                         ITEM_MASTER.STANDARD_UOM%TYPE;
   L_store_ord_mult              ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_uot                         CODE_DETAIL.CODE_DESC%TYPE;

   cursor C_STORE_ORD_MULT_AND_UOM is
      select ils.store_ord_mult,
             im.standard_uom
        from item_loc ils, 
             item_master im
       where ils.item = I_item
         and ils.loc = I_loc
         and ils.item = im.item;

   cursor C_SUPP_PACK_SIZE is
      select supp_pack_size
        from tsfdetail
       where tsf_no = PM_transfer
         and item = I_item;

   cursor C_CASE_NAME is
      select code_detail.code_desc
        from code_detail, item_supplier
       where item = item_supplier.item
         and item_supplier.primary_supp_ind = 'Y'
         and code_detail.code = item_supplier.case_name
         and code_detail.code_type = 'CASN';

   cursor C_INNER_NAME is
      select code_detail.code_desc
        from code_detail, item_supplier
       where I_item = item_supplier.item
         and item_supplier.primary_supp_ind = 'Y'
         and code_detail.code = item_supplier.inner_name
         and code_detail.code_type = 'INRN';

BEGIN

   open C_SUPP_PACK_SIZE;
   fetch C_SUPP_PACK_SIZE into L_supp_pack_size;
   close C_SUPP_PACK_SIZE;

   open C_STORE_ORD_MULT_AND_UOM;
   fetch C_STORE_ORD_MULT_AND_UOM into L_store_ord_mult,L_uom;
   close C_STORE_ORD_MULT_AND_UOM;

   if L_store_ord_mult = 'E' or (L_store_ord_mult = 'I' and L_supp_pack_size = 1) then
      L_uot := L_uom;
   elsif (L_store_ord_mult = 'I' and L_supp_pack_size > 1) then
      open C_INNER_NAME;
      fetch C_INNER_NAME into L_uot;
      close C_INNER_NAME;  
   elsif L_store_ord_mult = 'C' then
      open C_CASE_NAME;
      fetch C_CASE_NAME into L_uot;
      close C_CASE_NAME;
   end if;

   return L_uot;

EXCEPTION
   when OTHERS then
      return -1;

END GET_UOT;
-------------------------------------------------------------------------------------------------
FUNCTION GET_UOT_SIZE(I_item IN ITEM_MASTER.ITEM%TYPE)
   RETURN NUMBER IS

   L_supp_pack_size              TSFDETAIL.SUPP_PACK_SIZE%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_uom                         ITEM_MASTER.STANDARD_UOM%TYPE;
   L_store_ord_mult              ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_uot_size                    TSFDETAIL.SUPP_PACK_SIZE%TYPE;

   cursor C_SUPP_PACK_SIZE is
      select supp_pack_size
        from tsfdetail
       where tsf_no = PM_transfer
         and item = I_item;

BEGIN

   open C_SUPP_PACK_SIZE;
   fetch C_SUPP_PACK_SIZE into L_supp_pack_size;
   close C_SUPP_PACK_SIZE;

   L_uot_size := L_supp_pack_size;

   if ITEM_ATTRIB_SQL.GET_STORE_ORD_MULT_AND_UOM(L_error_message,
                                                 L_store_ord_mult,
                                                 L_uom,
                                                 I_item) = FALSE then
      return 0;
   end if;

   if L_store_ord_mult = 'E' or ( L_store_ord_mult = 'I' and L_supp_pack_size = 1) then
      L_uot_size := 1;
   end if;

   return L_uot_size;

EXCEPTION
   when OTHERS then
      return 0;

END GET_UOT_SIZE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_RETAIL_COST_LOC(I_stake_cost_var  IN STAKE_PROD_LOC.ADJUST_COST%TYPE,
                             I_currency_code   IN CURRENCIES.CURRENCY_CODE%TYPE,
                             I_cost_retail_ind IN VARCHAR2)
   RETURN NUMBER IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_retail_cost_loc             SYSTEM_OPTIONS.STAKE_RETAIL_VARIANCE%TYPE;

BEGIN

   if CURRENCY_SQL.CONVERT(L_error_message,
                           I_stake_cost_var,
                           NULL,
                           I_currency_code,
                           L_retail_cost_loc,
                           I_cost_retail_ind,
                           NULL,
                           NULL) = FALSE then

      L_retail_cost_loc := 0;
   end if;

   return L_retail_cost_loc;

EXCEPTION
   when OTHERS then
      return 0;

END GET_RETAIL_COST_LOC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CTRY_DESC(I_import_country_id IN HTS.IMPORT_COUNTRY_ID%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_country_desc                COUNTRY.COUNTRY_DESC%TYPE;
        
BEGIN

   if COUNTRY_VALIDATE_SQL.GET_NAME(L_error_message,
                                    I_import_country_id,
                                    L_country_desc ) = FALSE then
      return -1;
   end if;

   return L_country_desc ; 

EXCEPTION
   when OTHERS then
      return -1;

END GET_CTRY_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(I_location      IN ITEM_LOC.LOC%TYPE,
                           I_location_type IN ITEM_LOC.LOC_TYPE%TYPE)
   RETURN VARCHAR2 IS

   L_curr_code                   CURRENCIES.CURRENCY_CODE%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if I_location_type in ('S','W', 'E') then
      if CURRENCY_SQL.GET_CURR_LOC(L_error_message,
                                   I_location,
                                   I_location_type,
                                   NULL,
                                   L_curr_code) = FALSE then
         return -1;
      end if;
   else
      return -1;
   end if;

   return L_curr_code;

EXCEPTION
   when OTHERS then
      return -1;

END GET_CURRENCY_CODE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_LANG_CODE_DESC(I_code_type IN CODE_DETAIL.CODE_TYPE%TYPE,
                            I_code      IN CODE_DETAIL.CODE%TYPE)
   RETURN VARCHAR2 IS

   L_desc                        CODE_DETAIL.CODE_DESC%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if I_code_type is not NULL and I_code is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    I_code_type,
                                    I_code,
                                    L_desc) = FALSE then
         return -1;
      end if;

      return L_desc;

   else
      return -1;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_LANG_CODE_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_AMENDED_TEXT(I_amended_field  IN LC_AMENDMENTS.AMENDED_FIELD%TYPE,
                          I_order_no       IN LC_AMENDMENTS.ORDER_NO%TYPE,
                          I_item           IN LC_AMENDMENTS.ITEM%TYPE,
                          I_original_value IN LC_AMENDMENTS.ORIGINAL_VALUE%TYPE,
                          I_new_value      IN LC_AMENDMENTS.NEW_VALUE%TYPE,
                          I_effect         IN LC_AMENDMENTS.EFFECT%TYPE,
                          I_currency_code  IN LC_HEAD.CURRENCY_CODE%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_amend_text                  LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_field_decode                CODE_DETAIL.CODE_DESC%TYPE;

BEGIN

   if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                 'LCAF',
                                 I_amended_field,
                                 L_field_decode) = FALSE then
      return -1;
   end if;

   if LC_AMEND_SQL.AMEND_TEXT(L_error_message,
                              L_amend_text,
                              I_order_no,
                              I_item,
                              I_amended_field,
                              L_field_decode,
                              I_original_value,
                              I_new_value,
                              I_effect,
                              I_currency_code,
                              'N') = FALSE then
      return -1;
   end if;

   return L_amend_text;

EXCEPTION
   when OTHERS then
      return -1;

END GET_AMENDED_TEXT;
-------------------------------------------------------------------------------------------------
FUNCTION GET_COMPANY_HEADER
   RETURN VARCHAR2 IS

   L_company                     COMPHEAD.CO_NAME%TYPE;

   cursor C_COMPANY is
     select co_name
       from comphead; 

BEGIN

   open C_COMPANY;
   fetch C_COMPANY into L_company;
   close C_COMPANY;

   return L_company;

EXCEPTION
   when OTHERS then
      return -1;

END GET_COMPANY_HEADER;
-------------------------------------------------------------------------------------------------
FUNCTION GET_STORE_UOP(I_pack_size       IN ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_item            IN ITEM_MASTER.ITEM%TYPE, 
                       I_orig_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_supplier        IN SUPS.SUPPLIER%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_uom                         ITEM_SUPPLIER.CASE_NAME%TYPE;
   L_supp_pack_size              ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_pallet_size                 ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size             ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_pallet_desc                 CODE_DETAIL.CODE_DESC%TYPE;
   L_case_desc                   CODE_DETAIL.CODE_DESC%TYPE;
   L_inner_desc                  CODE_DETAIL.CODE_DESC%TYPE;
   L_pallet_name                 ITEM_SUPPLIER.PALLET_NAME%TYPE;
   L_case_name                   ITEM_SUPPLIER.CASE_NAME%TYPE;
   L_inner_name                  ITEM_SUPPLIER.INNER_NAME%TYPE;
   L_standard_uom                ITEM_MASTER.STANDARD_UOM%TYPE;
   L_store_ord_mult              ITEM_MASTER.STORE_ORD_MULT%TYPE;

BEGIN

   if I_pack_size = 1 then
      if ITEM_ATTRIB_SQL.GET_STORE_ORD_MULT_AND_UOM(L_error_message,
                                                    L_store_ord_mult,
                                                    L_standard_uom,
                                                    I_item) = FALSE then
         return 0;
      end if;

      L_uom := L_standard_uom;

   else
      if SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES(L_error_message,
                                             L_supp_pack_size,
                                             L_inner_pack_size,
                                             L_pallet_desc,
                                             L_case_desc,
                                             L_inner_desc,
                                             L_pallet_name,
                                             L_case_name,
                                             L_inner_name,
                                             I_item,
                                             I_supplier,
                                             I_orig_country_id) = FALSE then
         return 0;
      end if;

      L_uom := L_case_name;

   end if;

   return L_uom;

EXCEPTION
   when OTHERS then
      return 0;

END GET_STORE_UOP;
-------------------------------------------------------------------------------------------------
FUNCTION GET_VENDOR_COST(I_item            IN ITEM_MASTER.ITEM%TYPE,
                         I_orig_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_location        IN ORDLOC.LOCATION%TYPE,
                         I_ordloc_qty      IN ORDLOC.QTY_ORDERED%TYPE,
                         I_supplier        IN SUPS.SUPPLIER%TYPE,
                         I_sup_curr        IN CURRENCY_RATES.CURRENCY_CODE%TYPE,
                         I_ord_curr        IN CURRENCY_RATES.CURRENCY_CODE%TYPE)
   RETURN NUMBER IS

   L_unit_cost_ord               ORDLOC.UNIT_COST%TYPE;
   L_unit_cost_sup               ORDLOC.UNIT_COST%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if ITEM_SUPP_COUNTRY_LOC_SQL.GET_COST(L_error_message,
                                         L_unit_cost_sup,
                                         I_item,
                                         I_supplier,
                                         I_orig_country_id,
                                         I_location) = FALSE then
      return 0;
   end if;

   if PM_ord_curr = PM_sup_curr then
      L_unit_cost_ord := L_unit_cost_sup;
   elsif CURRENCY_SQL.CONVERT(L_error_message,
                              L_unit_cost_sup,
                              I_sup_curr,
                              I_ord_curr,
                              L_unit_cost_ord,
                              'C',
                              NULL,
                              NULL) = FALSE then
      return 0;
   end if;

   return (L_unit_cost_ord * I_ordloc_qty);

EXCEPTION
   when OTHERS then
      return 0;

END GET_VENDOR_COST;
-------------------------------------------------------------------------------------------------
FUNCTION GET_DEPARTMENT_NAME(I_dept IN DEPS.DEPT%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_dept_desc                   DEPS.DEPT_NAME%TYPE;

BEGIN

   if DEPT_ATTRIB_SQL.GET_NAME(L_error_message,
                               I_dept,
                               L_dept_desc) = FALSE then
      return -1;
   end if;

   return L_dept_desc;

EXCEPTION
   when OTHERS then
      return -1;

END GET_DEPARTMENT_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_COST_CHANGE_DESC(I_cost_change IN COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_cost_desc                   COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE;

BEGIN
   if PC_ATTRIB_SQL.GET_DESC(I_cost_change,
                             'C',
                             L_cost_desc,
                             L_error_message) = FALSE then
     return -1;
   end if;

   return L_cost_desc;

EXCEPTION
   when OTHERS then
      return -1;

END GET_COST_CHANGE_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_AMOUNT_TEXT(I_amount        IN LC_HEAD.AMOUNT%TYPE,
                         I_currency_code IN LC_HEAD.CURRENCY_CODE%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_amount_text                 VARCHAR2(2000);

BEGIN

   if LC_SQL.CONVERT_AMOUNT(L_error_message,
                            L_amount_text,
                            'L',
                            I_amount,
                            I_currency_code) = FALSE then
      return -1;
   end if;

   return L_amount_text;

EXCEPTION
   when OTHERS then
      return -1;

END GET_AMOUNT_TEXT;
-------------------------------------------------------------------------------------------------
FUNCTION GET_SUM_VENDOR_COST(I_item            IN   ITEM_MASTER.ITEM%TYPE,
                             I_orig_country_id IN   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_supplier        IN   SUPS.SUPPLIER%TYPE,
                             I_sup_curr        IN   CURRENCY_RATES.CURRENCY_CODE%TYPE,
                             I_ord_curr        IN   CURRENCY_RATES.CURRENCY_CODE%TYPE)
   RETURN NUMBER IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_cost_ord                    ORDLOC.UNIT_COST%TYPE := 0;
   L_cost_sup                    ORDLOC.UNIT_COST%TYPE := 0;

   cursor C_COST is
      select NVL(SUM(iscl.unit_cost * qty_ordered),0)
         from ordloc ol,
              item_supp_country_loc iscl,
              ordsku os
        where ol.order_no          = PM_order_no
          and ol.item              = iscl.item
          and ol.location          = iscl.loc
          and ol.item              = I_item
          and iscl.supplier        = I_supplier
          and os.order_no          = ol.order_no
          and os.item              = ol.item
          and os.origin_country_id = I_orig_country_id
          and os.origin_country_id = iscl.origin_country_id;

BEGIN

   open C_COST;
   fetch C_COST into L_cost_sup;
   close C_COST;

   if I_sup_curr != I_ord_curr then
      if CURRENCY_SQL.CONVERT(L_error_message,
                              L_cost_sup,
                              I_sup_curr,
                              I_ord_curr,
                              L_cost_ord,
                              'C',
                              NULL,
                              NULL) = FALSE then
         return 0;
      end if;
   else
      L_cost_ord := L_cost_sup;
   end if;

   return L_cost_ord;

EXCEPTION
   when OTHERS then
      return 0;

END GET_SUM_VENDOR_COST;
-------------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_HIER_NAME (I_stake_dept     IN STAKE_SKU_LOC.DEPT%TYPE,
                              I_stake_class    IN STAKE_SKU_LOC.CLASS%TYPE,
                              I_stake_subclass IN STAKE_SKU_LOC.SUBCLASS%TYPE)
   RETURN VARCHAR2 IS

   L_dept_name                   DEPS.DEPT_NAME%TYPE;
   L_class_name                  CLASS.CLASS_NAME%TYPE;
   L_subclass_name               SUBCLASS.SUB_NAME%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if MERCH_ATTRIB_SQL.GET_MERCH_HIER_NAMES(L_error_message,
                                            L_dept_name,
                                            L_class_name,
                                            L_subclass_name,
                                            I_stake_dept,
                                            I_stake_class,
                                            I_stake_subclass) = FALSE then
      return -1;
   else
     return(L_dept_name||'/'||L_class_name||'/'||L_subclass_name);
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_MERCH_HIER_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DESC (I_item IN ITEM_MASTER.ITEM%TYPE)
   RETURN VARCHAR2 IS

   L_item_desc                   ITEM_MASTER.ITEM_DESC%TYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if ITEM_ATTRIB_SQL.GET_DESC(L_error_message,
                               L_item_desc,
                               I_item) = FALSE then
      return -1;
   else
      return L_item_desc;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_ITEM_DESC;
-------------------------------------------------------------------------------------------------
FUNCTION GET_COST_AND_RETAILS(I_item            IN ITEM_MASTER.ITEM%TYPE,
                              I_loc             IN TSFHEAD.FROM_LOC%TYPE,
                              I_loc_type        IN TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_tsf_qty         IN TSFDETAIL.TSF_QTY%TYPE,
                              I_cost_retail_ind IN VARCHAR2)
   RETURN NUMBER IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_cost                        ITEM_LOC_SOH.UNIT_COST%TYPE := NULL;
   L_retail                      ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   L_av_cost                     ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_selling_ur                  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom                 ITEM_LOC.SELLING_UOM%TYPE;

BEGIN

   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(L_error_message,
                                               I_item,
                                               I_loc,
                                               I_loc_type,
                                               L_av_cost,
                                               L_cost,
                                               L_retail,
                                               L_selling_ur,
                                               L_selling_uom) = FALSE then
       return 0;
   end if;

   if I_cost_retail_ind = 'RETAIL' then
      return (L_retail * I_tsf_qty);
   elsif I_cost_retail_ind = 'COST' then
      return (L_cost * I_tsf_qty);
   else
      return 0;
   end if;

EXCEPTION
   when OTHERS then
      return 0;

END GET_COST_AND_RETAILS;
-------------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PARTNER_NAME(I_supplier     IN SUPS.SUPPLIER%TYPE,
                               I_partner_id   IN PARTNER.PARTNER_ID%TYPE,
                               I_partner_type IN PARTNER.PARTNER_TYPE%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_sup_name                    SUPS.SUP_NAME%TYPE;
   L_partner_desc                PARTNER.PARTNER_DESC%TYPE;

BEGIN

   if I_supplier is not NULL then
      if SUPP_ATTRIB_SQL.GET_SUPP_DESC(L_error_message,
                                       I_supplier,
                                       L_sup_name) = FALSE then
         return -1;
      end if;
      return L_sup_name;
   elsif I_partner_id is not NULL then
      if PARTNER_SQL.GET_DESC(L_error_message,
                              L_partner_desc,
                              I_partner_id,
                              I_partner_type) = FALSE then
         return -1;
      end if;
      return L_partner_desc;
   end if;

EXCEPTION
   when OTHERS then
      return -1;

END GET_SUPP_PARTNER_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CLIENT_NAME(I_value IN VARCHAR2)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_label                       CODE_DETAIL.CODE_DESC%TYPE;

   cursor C_LABEL is
      select code_desc 
         from v_code_detail_tl
        where code_type = 'LABL'
          and code = case when UPPER(I_value) = 'DIVISION' then 'DIV'
                          when UPPER(I_value) = 'GROUP' then 'GROUP'
                          when UPPER(I_value) = 'DEPARTMENT' then 'DEP'
                          when UPPER(I_value) = 'CLASS' then 'CLS'
                          when UPPER(I_value) = 'SUBCLASS' then 'SCLS'
                     end;
          
BEGIN
   ---
   open C_LABEL;
   fetch C_LABEL into L_label;
   close C_LABEL;
   ---
   return L_label;
   
END GET_CLIENT_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_WH_NAME(I_loc_name IN WH.WH_NAME%TYPE, I_location IN WH.WH%TYPE, I_wh WH.WH%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_name                        WH.WH_NAME%TYPE;
   L_pwh                         WH.WH%TYPE;

BEGIN

   if I_wh is NULL then
      if I_loc_name is NULL then
         if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_message,
                                          L_pwh,
                                          I_location) = FALSE then
            return -1;
         end if;
      end if;
      return L_pwh;
   elsif I_wh IS NOT NULL then
      if I_loc_name IS NULL then
         if WH_ATTRIB_SQL.GET_NAME(L_error_message,
                                   I_wh,
                                   L_name) = FALSE then
            return -1;
         end if;
      else
         return I_location;
      end if;
   end if;

   return L_name;

EXCEPTION
   when OTHERS then
      return -1;

END GET_WH_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_LOC_NAME(I_loc_type IN ITEM_LOC.LOC_TYPE%TYPE,
                      I_loc      IN ITEM_LOC.LOC%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_loc_name                    PARTNER.PARTNER_DESC%TYPE;
   L_currency_code               CURRENCIES.CURRENCY_CODE%TYPE;
   L_partner_type                PARTNER.PARTNER_TYPE%TYPE := 'E';
   L_status                      PARTNER.STATUS%TYPE;
   L_principle_country_id        PARTNER.PRINCIPLE_COUNTRY_ID%TYPE;
   L_mfg_id                      PARTNER.MFG_ID%TYPE;
   L_lang                        PARTNER.LANG%TYPE;

BEGIN

   if I_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_NAME(L_error_message,
                                   I_loc,
                                   L_loc_name) = FALSE then
         return -1;
      end if;
   elsif I_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_NAME(L_error_message,
                                I_loc,
                                L_loc_name) = FALSE then
         return -1;
      end if;
   elsif I_loc_type = 'E' then
      if PARTNER_SQL.GET_INFO(L_error_message,
                              L_loc_name,
                              L_currency_code,
                              L_status,
                              L_principle_country_id,
                              L_mfg_id,
                              L_lang,
                              I_loc,
                              L_partner_type) = FALSE then
         return -1;
      end if;
   else
      return -1;
   end if;

   return L_loc_name;

EXCEPTION
   when OTHERS then
      return -1;

END GET_LOC_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_SELLING_UOM(I_item  IN ITEM_MASTER.ITEM%TYPE,
                         I_store IN STORE.STORE%TYPE)
   RETURN VARCHAR2 IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_av_cost                     ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost                   ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_unit_retail                 ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail         ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom                 ITEM_LOC.SELLING_UOM%TYPE;

BEGIN

   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(L_error_message,
                                               I_item,
                                               I_store,
                                               'S',
                                               L_av_cost,
                                               L_unit_cost,
                                               L_unit_retail,
                                               L_selling_unit_retail,
                                               L_selling_uom)= FALSE then
      return -1;
   end if;

   return L_selling_uom;

EXCEPTION
   when OTHERS then
      return -1;

END GET_SELLING_UOM;
-------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_VALUE(I_store         IN STORE.STORE%TYPE,
                         I_currency_code IN CURRENCY_RATES.CURRENCY_CODE%TYPE,
                         I_tender_amt    IN SA_TRAN_TENDER.TENDER_AMT%TYPE)
   RETURN NUMBER IS

   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_loc_curr                    SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_new_value                   SA_POS_VALUE.POS_VALUE%TYPE;

BEGIN

   if I_store is NOT NULL then      
      if STORE_ATTRIB_SQL.GET_CURRENCY_CODE(L_error_message,
                                            L_loc_curr,
                                            I_store) = FALSE then
         return 0;
      end if;
      if I_currency_code != L_loc_curr then
         if CURRENCY_SQL.CONVERT(L_error_message,
                                 I_tender_amt,
                                 L_loc_curr,
                                 I_currency_code,
                                 L_new_value,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return 0;
         end if;
         return L_new_value;
      else
         return I_tender_amt;
      end if;
   end if;
   
   return I_tender_amt;

EXCEPTION
   when OTHERS then
      return 0;

END GET_TOTAL_VALUE;
------------------------------------------------------------------------------------------------- 
FUNCTION GET_LDOM_DATE(I_fdom IN SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE)
   RETURN DATE IS

   L_ldom                        DATE;
   L_fdom_day                    SYSTEM_VARIABLES.LAST_EOM_DAY%TYPE;
   L_fdom_month                  SYSTEM_VARIABLES.LAST_EOM_MONTH%TYPE;
   L_fdom_year                   SYSTEM_VARIABLES.LAST_EOM_YEAR%TYPE;
   L_cal                         VARCHAR2(1);
   L_day                         SYSTEM_VARIABLES.LAST_EOM_DAY%TYPE;
   L_month                       SYSTEM_VARIABLES.LAST_EOM_MONTH%TYPE;
   L_year                        SYSTEM_VARIABLES.LAST_EOM_YEAR%TYPE;
   L_return_code                 VARCHAR2(5);
   L_error_message               RTK_ERRORS.RTK_KEY%TYPE;

   cursor C_CALENDAR is
      select 'x'
        from system_options
       where calendar_454_ind = 'C';

BEGIN

   L_fdom_day   := TO_NUMBER(TO_CHAR(I_fdom,'DD'),'09');
   L_fdom_month := TO_NUMBER(TO_CHAR(I_fdom,'MM'),'09');
   L_fdom_year  := TO_NUMBER(TO_CHAR(I_fdom,'YYYY'),'0999');

   open C_CALENDAR;
   fetch C_CALENDAR into L_cal;

   if C_CALENDAR%FOUND then
      CAL_TO_CAL_LDOM(L_fdom_day,
                      L_fdom_month,
                      L_fdom_year,
                      L_day,
                      L_month,
                      L_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         close C_CALENDAR;
         return NULL;
      else
         L_ldom := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;
      close C_CALENDAR;
   else
      CAL_TO_454_LDOM(L_fdom_day,
                      L_fdom_month,
                      L_fdom_year,
                      L_day,
                      L_month,
                      L_year,
                      L_return_code,
                      L_error_message);
      if L_return_code = 'FALSE' then
         close C_CALENDAR;
         return NULL;
      else
         L_ldom := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;
      close C_CALENDAR;
   end if;

   return L_ldom;

EXCEPTION
   when OTHERS then
      return NULL;

END GET_LDOM_DATE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_VALUE(I_packdet_item     IN ITEM_MASTER.ITEM%TYPE,
                            I_supplier         IN SUPS.SUPPLIER%TYPE,
							I_orig_country_id  IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN NUMBER IS

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_cost_sup        ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE := 0;
   L_cost_ord        ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE := 0;

   cursor C_GET_COST is
       select unit_cost
        from item_supp_country_loc isc
       where isc.supplier          = I_supplier
         and isc.item              = I_packdet_item
         and isc.origin_country_id  = I_orig_country_id
         and isc.primary_loc_ind   = 'Y';
BEGIN

   open C_GET_COST;
   fetch C_GET_COST into L_cost_sup;
   close C_GET_COST;

   if CURRENCY_SQL.CONVERT_BY_LOCATION(L_error_message,
                                       I_supplier,
                                       'V',
                                       NULL,
                                       PM_order_no,
                                       'O',
                                       NULL,
                                       L_cost_sup,
                                       L_cost_ord,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return 0;
   end if;

   return L_cost_ord;

EXCEPTION
   when OTHERS then
      return 0; 

END GET_CURRENCY_VALUE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_FORMATTED_WH_NAME (I_store IN STORE.STORE%TYPE,
                                I_wh    IN WH.WH%TYPE)
   RETURN VARCHAR2 IS

   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_wh_name            WH.WH_NAME%TYPE;
   L_pwh                WH.PHYSICAL_WH%TYPE;

BEGIN
   if I_store = '-1' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_message,
                                       L_pwh,
                                       I_wh)= FALSE then
         return -1;
      end if;

     if WH_ATTRIB_SQL.GET_NAME(L_error_message,
                               L_pwh,
                               L_wh_name)= FALSE then
         return -1;
      end if;
   end if;

   return(L_pwh||'  '||L_wh_name);

EXCEPTION
   when OTHERS then
      return -1;

END GET_FORMATTED_WH_NAME;
-------------------------------------------------------------------------------------------------
FUNCTION GET_FLASH_TOTAL_VALUE (I_store       IN   STORE.STORE%TYPE,
                                I_flash_sales IN   SA_FLASH_SALES.NET_SALES%TYPE,
                                I_total       IN   SA_TOTAL.TOTAL_ID%TYPE,
                                I_prim_curr   IN   SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN NUMBER IS

   L_1_value           V_SA_TOTAL_VALUE.VALUE%TYPE;
   L_2_value           V_SA_TOTAL_VALUE.VALUE%TYPE;
   L_3_value           V_SA_TOTAL_VALUE.VALUE%TYPE;
   L_4_value           V_SA_TOTAL_VALUE.VALUE%TYPE;
   L_ave               V_SA_TOTAL_VALUE.VALUE%TYPE := NULL;
   L_new_value         V_SA_TOTAL_VALUE.VALUE%TYPE;
   L_count             NUMBER(1) := 0;
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_loc_curr          SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_count_sum_ind     SA_TOTAL_HEAD.COUNT_SUM_IND%TYPE;
   L_day               VARCHAR2(10);

   cursor C_GET_DAY is
      select to_char(PM_business_date,'DAY')
        from dual;
   cursor C_1_WEEK is
      select next_day(PM_business_date - 14, L_day)
        from calendar;
   cursor C_2_WEEK is
      select next_day(PM_1_week - 14, L_day)
        from calendar;      
   cursor C_3_WEEK is
      select next_day(PM_2_week - 14, L_day)
        from calendar;
   cursor C_4_WEEK is
      select next_day(PM_3_week - 14, L_day)
        from calendar;

   cursor C_CONVERT is
      select count_sum_ind
        from sa_total_head
       where total_id = I_total
         and total_rev_no = (select max(total_rev_no)
                               from sa_total_head
                              where sa_total_head.total_id = I_total);
   cursor C_1_VALUE is
      select v.value
        from v_sa_total_value v,
           sa_store_day s,
           sa_total t
       where s.store_day_seq_no = v.store_day_seq_no
         and t.total_id = I_total
         and t.total_seq_no = v.total_seq_no
         and s.store = I_store
         and s.business_date = PM_1_week
         and v.value_rev_no = (select max(value_rev_no)
                                 from v_sa_total_value
                                where v_sa_total_value.total_seq_no = t.total_seq_no
                                  and v_sa_total_value.store_day_seq_no = t.store_day_seq_no);
   cursor C_2_VALUE is
      select v.value
        from v_sa_total_value v,
             sa_store_day s,
             sa_total t
       where s.store_day_seq_no = v.store_day_seq_no
         and t.total_id = I_total
         and t.total_seq_no = v.total_seq_no
         and s.store = I_store
         and s.business_date = PM_2_week
         and v.value_rev_no = (select max(value_rev_no)
                                 from v_sa_total_value
                                where v_sa_total_value.total_seq_no = t.total_seq_no
                                  and v_sa_total_value.store_day_seq_no = t.store_day_seq_no);
   cursor C_3_VALUE is
      select v.value
        from v_sa_total_value v,
             sa_store_day s,
             sa_total t
       where s.store_day_seq_no = v.store_day_seq_no
         and t.total_id = I_total
         and t.total_seq_no = v.total_seq_no
         and s.store = I_store
         and s.business_date = PM_3_week
         and v.value_rev_no = (select max(value_rev_no)
                                 from v_sa_total_value
                                where v_sa_total_value.total_seq_no = t.total_seq_no
                                  and v_sa_total_value.store_day_seq_no = t.store_day_seq_no);
   cursor C_4_VALUE is
      select v.value
        from v_sa_total_value v,
             sa_store_day s,
             sa_total t
       where s.store_day_seq_no = v.store_day_seq_no
         and t.total_id = I_total
         and t.total_seq_no = v.total_seq_no
         and s.store = I_store
         and s.business_date = PM_4_week
         and v.value_rev_no = (select max(value_rev_no)
                                 from v_sa_total_value
                                where v_sa_total_value.total_seq_no = t.total_seq_no
                                  and v_sa_total_value.store_day_seq_no = t.store_day_seq_no);

BEGIN

   if I_flash_sales is NULL then
      open C_GET_DAY;
      fetch C_GET_DAY into L_day;
      close C_GET_DAY;
      ---
      open C_1_WEEK;
      fetch C_1_WEEK into PM_1_week;
      close C_1_WEEK;
      ---
      open C_2_WEEK;
      fetch C_2_WEEK into PM_2_week;
      close C_2_WEEK;
      ---
      open C_3_WEEK;
      fetch C_3_WEEK into PM_3_week;
      close C_3_WEEK;
      ---
      open C_4_WEEK;
      fetch C_4_WEEK into PM_4_week;
      close C_4_WEEK;
      ---
      open C_1_VALUE;
      fetch C_1_VALUE into L_1_value;
      close C_1_value;
      if nvl(L_1_value,-1) != -1 then
         L_count := 1;
      end if;
      ---
      open C_2_VALUE;
      fetch C_2_VALUE into L_2_value;
      close C_2_value;
      if nvl(L_2_value,-1) != -1 then
         L_count := L_count + 1;
      end if;
      ---
      open C_3_VALUE;
      fetch C_3_VALUE into L_3_value;
      close C_3_value;
      if nvl(L_3_value,-1) != -1 then
         L_count := L_count + 1;
      end if;
      ---
      open C_4_VALUE;
      fetch C_4_VALUE into L_4_value;
      close C_4_value;
      if nvl(L_4_value,-1)  != -1 then
         L_count := L_count + 1;
      end if;
      ---
      L_ave := (nvl(L_1_value,0) + nvl(L_2_value,0) + nvl(L_3_value,0) + nvl(L_4_value,0))/L_count;
   else
      L_ave := I_flash_sales;
   end if;

   if I_total is NOT NULL then
      ---
      open C_CONVERT;
      fetch C_CONVERT into L_count_sum_ind;
      close C_CONVERT;
      ---
      if L_count_sum_ind = 'S' then

         if STORE_ATTRIB_SQL.GET_CURRENCY_CODE(L_error_message,
                                               L_loc_curr,
                                               I_store) = FALSE then
            return(0);
         end if;

         if I_prim_curr != L_loc_curr then
            if CURRENCY_SQL.CONVERT(L_error_message,
                                    L_ave,
                                    L_loc_curr,
                                    I_prim_curr,
                                    L_new_value,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return(0);
            end if;

            return(L_new_value);
         end if;
      end if;
      return(L_ave);
   else
      if STORE_ATTRIB_SQL.GET_CURRENCY_CODE(L_error_message,
                                            L_loc_curr,
                                            I_store) = FALSE then
         return(0);
      end if;
      if I_prim_curr != L_loc_curr then
         if CURRENCY_SQL.CONVERT(L_error_message,
                                 L_ave,
                                 L_loc_curr,
                                 I_prim_curr,
                                 L_new_value,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return(0);
         end if;
         return(L_new_value);
      else
         return(L_ave);
      end if;
   end if;

EXCEPTION
   when OTHERS then
      return 0;

END GET_FLASH_TOTAL_VALUE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_PAST_DUE_AMT 
   RETURN NUMBER IS

   L_past_due   OTB.N_APPROVED_AMT%TYPE;
   ---
   cursor C_PAST_DUE is
      select sum(NVL(NVL(n_approved_amt,0) + NVL(a_approved_amt,0) + NVL(b_approved_amt,0)
             - NVL(n_receipts_amt,0) - NVL(a_receipts_amt,0) - NVL(b_receipts_amt, 0),0))
        from otb,
             system_variables
       where (eow_date < last_eow_date or eow_date = last_eow_date)
         and dept = NVL(PM_dept, dept)
         and ((class = PM_class and PM_class != -999) OR PM_class = -999)
         and ((subclass = PM_subclass and PM_subclass != -999) OR PM_subclass = -999);
         
BEGIN

   open C_PAST_DUE;
   fetch C_PAST_DUE into L_past_due;
   close C_PAST_DUE;
   
   return L_past_due;
   
EXCEPTION
   when OTHERS then
      return 0;

END GET_PAST_DUE_AMT;
-------------------------------------------------------------------------------------------------
FUNCTION GET_LASTDATE
   RETURN DATE IS

   L_last_year           DATE;

   cursor C_LDATE is
      select NEXT_DAY(TO_DATE(PM_business_date,'DD-MON-RRRR') - 366,
             TO_CHAR(TO_DATE(PM_business_date,'DD-MON-RRRR'),'DAY'))
        from dual;

BEGIN

   open C_LDATE;
   fetch C_LDATE into L_last_year;
   close C_LDATE;

   return L_last_year;

EXCEPTION
   when OTHERS then
      return NULL;

END GET_LASTDATE;
-------------------------------------------------------------------------------------------------
FUNCTION LANG_CODE_EXTRACT(I_bi_lang_code  IN      LANG.ISO_CODE%TYPE)
RETURN VARCHAR2 is

   L_lang_code     LANG.ISO_CODE%TYPE :=NULL;
   
BEGIN
   L_lang_code:= case I_bi_lang_code
                   when 'US'  then  'EN'
                   when 'D'   then  'DE'
                   when 'F'   then  'FR'
                   when 'E'   then  'ES'
                   when 'JA'  then  'JA'
                   when 'KO'  then  'KO'
                   when 'RU'  then  'RU'
                   when 'ZHS' then  'ZHS'
                   when 'TR'  then  'TR'
                   when 'HU'  then  'HU'
                   when 'PT'  then  'PTB'
                   when 'AR'  then  'AR'
                   when 'HR'  then  'HR'
                   when 'CS'  then  'CS'
                   when 'DK'  then  'DA'
                   when 'NL'  then  'NL'
                   when 'SF'  then  'FI'
                   when 'EL'  then  'EL'
                   when 'I'   then  'IT'
                   when 'N'   then  'NO'
                   when 'PL'  then  'PL'
                   when 'PT'  then  'PT'
                   when 'RO'  then  'RO'
                   when 'SK'  then  'SK'
                   when 'S'   then  'SV'
                   when 'TH'  then  'TH'
                   else 'EN'
                end;
      return L_lang_code;

EXCEPTION
   when OTHERS then
      return 0;
END LANG_CODE_EXTRACT;
------------------------------------------------------------------------------------------------------
END REPORTS_SQL;
/
