CREATE OR REPLACE PACKAGE CFA_VALIDATE_SQL AUTHID CURRENT_USER AS 

-- CFAS return value constants
CFA_TRUE             CONSTANT NUMBER := 1;
CFA_FALSE            CONSTANT NUMBER := 0;

GP_func              VARCHAR2(61);

--------------------------------------------------------------------------------
-- Name:    VALIDATE_FIELD
-- Purpose: This function validates the flex UI attributes
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_ATTRIB(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_set_fields_tbl  IN OUT   CFA_SQL.TYP_VALUE_TBL,
   I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
   I_attrib          IN       CFA_ATTRIB.VIEW_COL_NAME%TYPE,
   I_attrib_value    IN       CFA_SQL.GP_FIELD_VALUE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP
-- Purpose: This function validates the flex UI attributes
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_GROUP(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_err_field       IN OUT   VARCHAR2,
   I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP_SET
-- Purpose: This function validates the flex UI attributes
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_GROUP_SET(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_EXT_ENTITY
-- Purpose: This function validates the flex UI attributes
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_EXT_ENTITY(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_FUNCTION
-- Purpose: This function validates the functions entered
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_FUNCTION(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_func            IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Name:    DEFAULT_GROUP_SET
-- Purpose: This function is used to pre-populate attribute fields in the group set
--          upon startup of the Flex UI.
--------------------------------------------------------------------------------
FUNCTION  DEFAULT_GROUP_SET(
   O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table          IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
   I_group_set_id        IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE) 
RETURN BOOLEAN;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Name:    QUALIFY_GROUP_SET
-- Purpose: This function is used to check if the user is allowed to access the group set
--------------------------------------------------------------------------------
FUNCTION  QUALIFY_GROUP_SET(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- Name:    GET_KEY_DESC
-- Purpose: This function is used to check if the user is allowed to access the group set
--------------------------------------------------------------------------------
FUNCTION  GET_KEY_DESC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_desc            IN OUT   CFA_SQL.GP_FIELD_DESC_VALUE%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
   I_key_col         IN       CFA_EXT_ENTITY_KEY.KEY_COL%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Name:    GET_ATTRIB_VAL_FUNC
-- Purpose: This function is used to get the validation function name of the attribute
--------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB.VALIDATION_FUNC%TYPE,
   I_group_id        IN       CFA_ATTRIB.GROUP_ID%TYPE,
   I_attrib          IN       CFA_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_GROUP_VAL_FUNC
-- Purpose: This function is used to get the validation function name of the attribute group
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP.VALIDATION_FUNC%TYPE,
   I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_GROUP_SET_VAL_FUNC
-- Purpose: This function is used to get the validation function name of the group set
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP_SET.VALIDATION_FUNC%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_GROUP_SET_DEFAULT_FUNC
-- Purpose: This function is used to get the validation function name of the group set
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET_DEFAULT_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP_SET.DEFAULT_FUNC%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_GROUP_SET_QUALIFY_FUNC
-- Purpose: This function is used to get the validation function name of the group set
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET_QUALIFY_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP_SET.QUALIFIER_FUNC%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_EXT_ENTITY_VAL_FUNC
-- Purpose: This function is used to get the validation function name of the extended entity
--------------------------------------------------------------------------------
FUNCTION GET_EXT_ENTITY_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_EXT_ENTITY.VALIDATION_FUNC%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_KEY_DESC_FUNC
-- Purpose: This function is used to get the validation function name of the extended entity
--------------------------------------------------------------------------------
FUNCTION GET_KEY_DESC_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_EXT_ENTITY_KEY.DESCRIPTION_CODE%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
   I_key_col         IN       CFA_EXT_ENTITY_KEY.KEY_COL%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- Name:    EXEC_FUNC
-- Purpose: This function is used to dynamically execute metadata defined external functions
--------------------------------------------------------------------------------
FUNCTION EXEC_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_return_code     IN OUT   NUMBER,
   I_func            IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

END CFA_VALIDATE_SQL;
/