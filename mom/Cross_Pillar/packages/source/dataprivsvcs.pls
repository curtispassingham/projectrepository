CREATE OR REPLACE PACKAGE DATAPRIV_SVC AUTHID CURRENT_USER IS
----------------------------------------------------------------------------------
FUNCTION QUERY_DATA(IN_DATAPRIV_CTX_PARAMS   IN       RAF_DATAPRIV_CTX_PARAM_TBL,
                    OUT_ERROR_MESSAGE           OUT   VARCHAR2)
RETURN SYS_REFCURSOR;
----------------------------------------------------------------------------------
FUNCTION UPDATE_DATA(IN_DATAPRIV_CTX_PARAMS   IN       RAF_DATAPRIV_CTX_PARAM_TBL,
                     OUT_ERROR_MESSAGE           OUT   VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------------
END DATAPRIV_SVC;
/
