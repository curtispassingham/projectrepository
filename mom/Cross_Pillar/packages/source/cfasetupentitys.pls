CREATE OR REPLACE PACKAGE CFA_SETUP_ENTITY_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
ERRNUM_PACKAGE_CALL    NUMBER(6) := -20020;  -- Define error for raise_application_error
---------------------------------------------------------------------------------------------------
TYPE CFA_EXT_ENTITY_KEY_REC IS RECORD
(  base_rms_table       CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
   key_col              CFA_EXT_ENTITY_KEY_LABELS.KEY_COL%TYPE,
   key_number           CFA_EXT_ENTITY_KEY.KEY_NUMBER%TYPE,
   data_type            CFA_EXT_ENTITY_KEY.DATA_TYPE%TYPE,
   description_code     CFA_EXT_ENTITY_KEY.DESCRIPTION_CODE%TYPE,
   lang                 CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE,
   label                CFA_EXT_ENTITY_KEY_LABELS.LABEL%TYPE,
   default_lang_ind     CFA_EXT_ENTITY_KEY_LABELS.DEFAULT_LANG_IND%TYPE,
   error_message        RTK_ERRORS.RTK_TEXT%TYPE,
   return_code          VARCHAR2(5)
);

TYPE CFA_EXT_ENTITY_KEY_TBL IS TABLE OF CFA_EXT_ENTITY_KEY_REC INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ENTITY_KEY_LABELS (IO_cfa_ext_entity_tbl    IN OUT    CFA_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
                                  I_lang                    IN        CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ENTITY_KEY_LABELS (IO_cfa_ext_entity_tbl     IN OUT    CFA_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
                                  I_key                     IN        CFA_EXT_ENTITY_KEY_LABELS.KEY_COL%TYPE,
                                  I_lang                    IN        CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ENTITY_KEY_LABELS(IO_cfa_ext_entity_tbl     IN OUT    CFA_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                  I_lang                    IN        CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE,
                                  I_default_ind             IN        CFA_EXT_ENTITY_KEY_LABELS.DEFAULT_LANG_IND%TYPE);
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_INSERT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table        IN       CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_EXT_ID(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_entity_id        IN OUT   CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_UPDATE(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table        IN        CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_rms_table    IN        CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_ext_table    IN        CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                         I_ext_ent_id       IN        CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_DELETE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_base_rms_table    IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_ext_ent_id        IN       CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_EXT_TABLE_NAME (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_custom_ext_table IN OUT   CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                             I_base_rms_table   IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_EXT_KEY_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_default_ind     IN OUT    CFA_EXT_ENTITY_KEY_LABELS.DEFAULT_LANG_IND%TYPE,
                                  I_base_rms_table  IN        CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
                                  I_lang            IN        CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ENTITY_DEFAULT_LANG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists         IN OUT VARCHAR2,
                                    I_base_rms_table IN     CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION ENTITY_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exist            IN OUT   VARCHAR2,
                      I_ext_entity       IN       CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_EXT_TBL(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists              IN OUT    VARCHAR2, 
                            O_base_rms_table      IN OUT    CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                            O_custom_ext_table    IN OUT    CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                            I_ext_entity_id       IN        CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ENTITY_KEY_LANG(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_base_rms_table  IN      CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                I_lang            IN      CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_ENTITY_SQL;
/