CREATE OR REPLACE PACKAGE BODY TRANSLATION_SQL AS
FUNCTION GET_TL_ROW(
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_text_1            IN OUT VARCHAR2,
                    O_text_2            IN OUT VARCHAR2,
                    O_text_3            IN OUT VARCHAR2,
                    O_text_4            IN OUT VARCHAR2,
                    O_text_5            IN OUT VARCHAR2,
                    O_text_6            IN OUT VARCHAR2,
                    O_text_7            IN OUT VARCHAR2,
                    O_text_8            IN OUT VARCHAR2,
                    O_text_9            IN OUT VARCHAR2,
                    O_row_exist         IN OUT VARCHAR2,
                    I_translate_table   IN     VARCHAR2,
                    I_lang              IN     NUMBER,
                    I_key_number_1      IN     NUMBER,
                    I_key_number_2      IN     NUMBER,
                    I_key_number_3      IN     NUMBER,
                    I_key_number_4      IN     NUMBER,
                    I_key_varchar2_5    IN     VARCHAR2,
                    I_key_varchar2_6    IN     VARCHAR2,
                    I_key_varchar2_7    IN     VARCHAR2,
                    I_key_varchar2_8    IN     VARCHAR2,
                    I_key_varchar2_9    IN     VARCHAR2,
                    I_key_date_10       IN     DATE,
                    I_key_date_11       IN     DATE
                    )
RETURN BOOLEAN IS 

   L_dummy      VARCHAR2(1);
   L_lan        LANG.LANG%TYPE;
   L_program    VARCHAR2(100) := 'TRANSLATION_SQL.GET_TL_ROW';

   cursor C_LANGUAGE is
      select 'X'
        from lang 
       where lang.lang = I_lang;

   cursor C_LAN_EXIST is
      select DATA_INTEGRATION_LANG
        from SYSTEM_CONFIG_OPTIONS;

   cursor C_ADDR_TL is
    select ADD_1,
           ADD_2,
           ADD_3,
           CITY,
           CONTACT_NAME,
           COUNTY
      from ADDR_TL
     where Lang = I_lang 
       and ADDR_KEY = I_KEY_NUMBER_1;

   cursor C_AREA_TL is
    select AREA_NAME
      from AREA_TL
     where Lang = I_lang 
       and AREA = I_KEY_NUMBER_1;

   cursor C_CHAIN_TL is
    select CHAIN_NAME
      from CHAIN_TL
     where Lang = I_lang 
       and CHAIN = I_KEY_NUMBER_1;

   cursor C_CLASS_TL is
    select CLASS_NAME
      from CLASS_TL
     where Lang = I_lang 
       and DEPT = I_KEY_NUMBER_1
       and CLASS = I_KEY_NUMBER_2;

   cursor C_COST_ZONE_GROUP_TL is
    select DESCRIPTION
      from COST_ZONE_GROUP_TL
     where Lang = I_lang 
       and ZONE_GROUP_ID = I_KEY_NUMBER_1;

   cursor C_COST_ZONE_TL is
    select DESCRIPTION
      from COST_ZONE_TL
     where Lang = I_lang 
       and ZONE_GROUP_ID = I_KEY_NUMBER_1
       and ZONE_ID = I_KEY_NUMBER_2;

   cursor C_DEPS_TL is
    select DEPT_NAME
      from DEPS_TL
     where Lang = I_lang 
       and DEPT = I_KEY_NUMBER_1;

   cursor C_DIFF_GROUP_HEAD_TL is
    select DIFF_GROUP_DESC
      from DIFF_GROUP_HEAD_TL
     where Lang = I_lang 
       and DIFF_GROUP_ID = I_KEY_VARCHAR2_5;

   cursor C_DIFF_RANGE_HEAD_TL is
    select DIFF_RANGE_DESC
      from DIFF_RANGE_HEAD_TL
     where Lang = I_lang 
       and DIFF_RANGE = I_KEY_NUMBER_1;

   cursor C_DIFF_RATIO_HEAD_TL is
    select DESCRIPTION
      from DIFF_RATIO_HEAD_TL
     where Lang = I_lang 
       and DIFF_RATIO_ID = I_KEY_NUMBER_1;

   cursor C_DISTRICT_TL is
    select DISTRICT_NAME
      from DISTRICT_TL
     where Lang = I_lang 
       and DISTRICT = I_KEY_NUMBER_1;

   cursor C_DIVISION_TL is
    select DIV_NAME
      from DIVISION_TL
     where Lang = I_lang 
       and DIVISION = I_KEY_NUMBER_1;

   cursor C_GROUPS_TL is
    select GROUP_NAME
      from GROUPS_TL
     where Lang = I_lang 
       and GROUP_NO = I_KEY_NUMBER_1;

   cursor C_ITEM_IMAGE_TL is
    select IMAGE_DESC
      from ITEM_IMAGE_TL
     where Lang = I_lang 
       and ITEM = I_KEY_VARCHAR2_5
       and IMAGE_NAME = I_KEY_VARCHAR2_6;

   cursor C_ITEM_MASTER_TL is
    select ITEM_DESC,
           ITEM_DESC_SECONDARY,
           SHORT_DESC
      from ITEM_MASTER_TL
     where Lang = I_lang 
       and ITEM = I_KEY_VARCHAR2_5;

   cursor C_ITEM_SUPPLIER_TL is
    select SUPP_LABEL,
           SUPP_DIFF_1,
           SUPP_DIFF_2,
           SUPP_DIFF_3,
           SUPP_DIFF_4
      from ITEM_SUPPLIER_TL
     where Lang = I_lang 
       and ITEM = I_KEY_VARCHAR2_5
       and SUPPLIER = I_KEY_NUMBER_1;

   cursor C_ITEM_XFORM_HEAD_TL is
    select ITEM_XFORM_DESC
      from ITEM_XFORM_HEAD_TL
     where Lang = I_lang 
       and ITEM_XFORM_HEAD_ID = I_KEY_NUMBER_1;

   cursor C_LOC_LIST_HEAD_TL is
    select LOC_LIST_DESC
      from LOC_LIST_HEAD_TL
     where Lang = I_lang 
       and LOC_LIST = I_KEY_NUMBER_1;

   cursor C_PACK_TMPL_HEAD_TL is
    select PACK_TMPL_DESC
      from PACK_TMPL_HEAD_TL
     where Lang = I_lang 
       and PACK_TMPL_ID = I_KEY_NUMBER_1;

   cursor C_PARTNER_TL is
    select PARTNER_DESC,
           PARTNER_NAME_SECONDARY
      from PARTNER_TL
     where Lang = I_lang 
       and PARTNER_TYPE = I_KEY_VARCHAR2_5
       and PARTNER_ID = I_KEY_VARCHAR2_6;

   cursor C_PEND_MERCH_HIER_TL is
    select MERCH_HIER_NAME
      from PEND_MERCH_HIER_TL
     where Lang = I_lang 
       and MERCH_HIER_ID = I_KEY_NUMBER_1
       and NVL(MERCH_HIER_PARENT_ID,-1) = NVL(I_KEY_NUMBER_2,-1)
       and NVL(MERCH_HIER_GRANDPARENT_ID,-1) = NVL(I_KEY_NUMBER_3,-1)
       and HIER_TYPE = I_KEY_VARCHAR2_5;

   cursor C_POS_COUPON_HEAD_TL is
    select COUPON_DESC
      from POS_COUPON_HEAD_TL
     where Lang = I_lang 
       and COUPON_ID = I_KEY_NUMBER_1;

   cursor C_PRIORITY_GROUP_TL is
    select PRIORITY_GROUP_DESC
      from PRIORITY_GROUP_TL
     where Lang = I_lang 
       and PRIORITY_GROUP_ID = I_KEY_NUMBER_1;

   cursor C_RECLASS_HEAD_TL is
    select RECLASS_DESC
      from RECLASS_HEAD_TL
     where Lang = I_lang 
       and RECLASS_NO = I_KEY_NUMBER_1;

   cursor C_REGION_TL is
    select REGION_NAME
      from REGION_TL
     where Lang = I_lang 
       and REGION = I_KEY_NUMBER_1;

   cursor C_RELATED_ITEM_HEAD_TL is
    select RELATIONSHIP_NAME
      from RELATED_ITEM_HEAD_TL
     where Lang = I_lang 
       and RELATIONSHIP_ID = I_KEY_NUMBER_1;

   cursor C_SKULIST_HEAD_TL is
    select SKULIST_DESC
      from SKULIST_HEAD_TL
     where Lang = I_lang 
       and SKULIST = I_KEY_NUMBER_1;

   cursor C_STORE_ADD_TL is
    select STORE_NAME,
           STORE_NAME_SECONDARY
      from STORE_ADD_TL
     where Lang = I_lang 
       and STORE = I_KEY_NUMBER_1;

   cursor C_STORE_TL is
    select STORE_NAME,
           STORE_NAME_SECONDARY
      from STORE_TL
     where Lang = I_lang 
       and STORE = I_KEY_NUMBER_1;

   cursor C_SUBCLASS_TL is
    select SUB_NAME
      from SUBCLASS_TL
     where Lang = I_lang 
       and DEPT = I_KEY_NUMBER_1
       and CLASS = I_KEY_NUMBER_2
       and SUBCLASS = I_KEY_NUMBER_3;

   cursor C_SUPS_PACK_TMPL_DESC_TL is
    select SUPP_PACK_DESC
      from SUPS_PACK_TMPL_DESC_TL
     where Lang = I_lang 
       and SUPPLIER = I_KEY_NUMBER_1
       and PACK_TMPL_ID = I_KEY_NUMBER_2;

   cursor C_SUPS_TL is
    select SUP_NAME,
           SUP_NAME_SECONDARY,
           CONTACT_NAME
      from SUPS_TL
     where Lang = I_lang 
       and SUPPLIER = I_KEY_NUMBER_1;

   cursor C_UDA_ITEM_FF_TL is
    select UDA_TEXT_DESC
      from UDA_ITEM_FF_TL
     where Lang = I_lang 
       and ITEM = I_KEY_VARCHAR2_5
       and UDA_ID = I_KEY_NUMBER_1
       and UDA_TEXT = I_KEY_VARCHAR2_6;

   cursor C_WF_COST_BUILDUP_TMPL_DTL_TL is
    select DESCRIPTION
      from WF_COST_BUILDUP_TMPL_DTL_TL
     where Lang = I_lang 
       and TEMPL_ID = I_KEY_NUMBER_1
       and COST_COMP_ID = I_KEY_VARCHAR2_5;

   cursor C_WF_COST_BUILDUP_TMPL_HD_TL is
    select TEMPL_DESC
      from WF_COST_BUILDUP_TMPL_HD_TL
     where Lang = I_lang 
       and TEMPL_ID = I_KEY_NUMBER_1;

   cursor C_WH_TL is
    select WH_NAME,
           WH_NAME_SECONDARY
      from WH_TL
     where Lang = I_lang 
       and WH = I_KEY_NUMBER_1;


BEGIN 

   if I_lang is null then
      O_error_message := SQL_LIB.CREATE_MSG('LANG_IS_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE; 
   end if; 

   open  C_LANGUAGE; 
   fetch C_LANGUAGE INTO L_dummy;
   if C_LANGUAGE%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LANGUAGE;
      return FALSE; 
   end if; 
   close C_LANGUAGE;

   open  C_LAN_EXIST; 
   fetch C_LAN_EXIST INTO L_lan;
   if L_lan = I_lang then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LAN_EXIST;
      return FALSE; 
   end if; 
   close C_LAN_EXIST;

   if I_translate_table = 'ADDR_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_ADDR_TL;
      fetch C_ADDR_TL into O_TEXT_1,
                           O_TEXT_2,
                           O_TEXT_3,
                           O_TEXT_4,
                           O_TEXT_5,
                           O_TEXT_6;

      if C_ADDR_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_ADDR_TL;
   end if ; 

   if I_translate_table = 'AREA_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_AREA_TL;
      fetch C_AREA_TL into O_TEXT_1;

      if C_AREA_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_AREA_TL;
   end if ; 

   if I_translate_table = 'CHAIN_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_CHAIN_TL;
      fetch C_CHAIN_TL into O_TEXT_1;

      if C_CHAIN_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_CHAIN_TL;
   end if ; 

   if I_translate_table = 'CLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_CLASS_TL;
      fetch C_CLASS_TL into O_TEXT_1;

      if C_CLASS_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_CLASS_TL;
   end if ; 

   if I_translate_table = 'COST_ZONE_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_COST_ZONE_GROUP_TL;
      fetch C_COST_ZONE_GROUP_TL into O_TEXT_1;

      if C_COST_ZONE_GROUP_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_COST_ZONE_GROUP_TL;
   end if ; 

   if I_translate_table = 'COST_ZONE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_COST_ZONE_TL;
      fetch C_COST_ZONE_TL into O_TEXT_1;

      if C_COST_ZONE_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_COST_ZONE_TL;
   end if ; 

   if I_translate_table = 'DEPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_DEPS_TL;
      fetch C_DEPS_TL into O_TEXT_1;

      if C_DEPS_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_DEPS_TL;
   end if ; 

   if I_translate_table = 'DIFF_GROUP_HEAD_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_DIFF_GROUP_HEAD_TL;
      fetch C_DIFF_GROUP_HEAD_TL into O_TEXT_1;

      if C_DIFF_GROUP_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_DIFF_GROUP_HEAD_TL;
   end if ; 

   if I_translate_table = 'DIFF_RANGE_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_DIFF_RANGE_HEAD_TL;
      fetch C_DIFF_RANGE_HEAD_TL into O_TEXT_1;

      if C_DIFF_RANGE_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_DIFF_RANGE_HEAD_TL;
   end if ; 

   if I_translate_table = 'DIFF_RATIO_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_DIFF_RATIO_HEAD_TL;
      fetch C_DIFF_RATIO_HEAD_TL into O_TEXT_1;

      if C_DIFF_RATIO_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_DIFF_RATIO_HEAD_TL;
   end if ; 

   if I_translate_table = 'DISTRICT_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_DISTRICT_TL;
      fetch C_DISTRICT_TL into O_TEXT_1;

      if C_DISTRICT_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_DISTRICT_TL;
   end if ; 

   if I_translate_table = 'DIVISION_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_DIVISION_TL;
      fetch C_DIVISION_TL into O_TEXT_1;

      if C_DIVISION_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_DIVISION_TL;
   end if ; 

   if I_translate_table = 'GROUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_GROUPS_TL;
      fetch C_GROUPS_TL into O_TEXT_1;

      if C_GROUPS_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_GROUPS_TL;
   end if ; 

   if I_translate_table = 'ITEM_IMAGE_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_ITEM_IMAGE_TL;
      fetch C_ITEM_IMAGE_TL into O_TEXT_1;

      if C_ITEM_IMAGE_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_ITEM_IMAGE_TL;
   end if ; 

   if I_translate_table = 'ITEM_MASTER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_ITEM_MASTER_TL;
      fetch C_ITEM_MASTER_TL into O_TEXT_1,
                                  O_TEXT_2,
                                  O_TEXT_3;

      if C_ITEM_MASTER_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_ITEM_MASTER_TL;
   end if ; 

   if I_translate_table = 'ITEM_SUPPLIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_ITEM_SUPPLIER_TL;
      fetch C_ITEM_SUPPLIER_TL into O_TEXT_1,
                                    O_TEXT_2,
                                    O_TEXT_3,
                                    O_TEXT_4,
                                    O_TEXT_5;

      if C_ITEM_SUPPLIER_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_ITEM_SUPPLIER_TL;
   end if ; 

   if I_translate_table = 'ITEM_XFORM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_ITEM_XFORM_HEAD_TL;
      fetch C_ITEM_XFORM_HEAD_TL into O_TEXT_1;

      if C_ITEM_XFORM_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_ITEM_XFORM_HEAD_TL;
   end if ; 

   if I_translate_table = 'LOC_LIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_LOC_LIST_HEAD_TL;
      fetch C_LOC_LIST_HEAD_TL into O_TEXT_1;

      if C_LOC_LIST_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_LOC_LIST_HEAD_TL;
   end if ; 

   if I_translate_table = 'PACK_TMPL_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_PACK_TMPL_HEAD_TL;
      fetch C_PACK_TMPL_HEAD_TL into O_TEXT_1;

      if C_PACK_TMPL_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_PACK_TMPL_HEAD_TL;
   end if ; 

   if I_translate_table = 'PARTNER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_PARTNER_TL;
      fetch C_PARTNER_TL into O_TEXT_1,
                              O_TEXT_2;

      if C_PARTNER_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_PARTNER_TL;
   end if ; 

   if I_translate_table = 'PEND_MERCH_HIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL or (I_KEY_VARCHAR2_5 != 'V' and I_KEY_NUMBER_2 is NULL) or (I_KEY_VARCHAR2_5 = 'S' and I_KEY_NUMBER_3 is NULL) then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_PEND_MERCH_HIER_TL;
      fetch C_PEND_MERCH_HIER_TL into O_TEXT_1;

      if C_PEND_MERCH_HIER_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_PEND_MERCH_HIER_TL;
   end if ; 

   if I_translate_table = 'POS_COUPON_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_POS_COUPON_HEAD_TL;
      fetch C_POS_COUPON_HEAD_TL into O_TEXT_1;

      if C_POS_COUPON_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_POS_COUPON_HEAD_TL;
   end if ; 

   if I_translate_table = 'PRIORITY_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_PRIORITY_GROUP_TL;
      fetch C_PRIORITY_GROUP_TL into O_TEXT_1;

      if C_PRIORITY_GROUP_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_PRIORITY_GROUP_TL;
   end if ; 

   if I_translate_table = 'RECLASS_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_RECLASS_HEAD_TL;
      fetch C_RECLASS_HEAD_TL into O_TEXT_1;

      if C_RECLASS_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_RECLASS_HEAD_TL;
   end if ; 

   if I_translate_table = 'REGION_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_REGION_TL;
      fetch C_REGION_TL into O_TEXT_1;

      if C_REGION_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_REGION_TL;
   end if ; 

   if I_translate_table = 'RELATED_ITEM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_RELATED_ITEM_HEAD_TL;
      fetch C_RELATED_ITEM_HEAD_TL into O_TEXT_1;

      if C_RELATED_ITEM_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_RELATED_ITEM_HEAD_TL;
   end if ; 

   if I_translate_table = 'SKULIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_SKULIST_HEAD_TL;
      fetch C_SKULIST_HEAD_TL into O_TEXT_1;

      if C_SKULIST_HEAD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_SKULIST_HEAD_TL;
   end if ; 

   if I_translate_table = 'STORE_ADD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_STORE_ADD_TL;
      fetch C_STORE_ADD_TL into O_TEXT_1,
                                O_TEXT_2;

      if C_STORE_ADD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_STORE_ADD_TL;
   end if ; 

   if I_translate_table = 'STORE_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_STORE_TL;
      fetch C_STORE_TL into O_TEXT_1,
                            O_TEXT_2;

      if C_STORE_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_STORE_TL;
   end if ; 

   if I_translate_table = 'SUBCLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_KEY_NUMBER_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_SUBCLASS_TL;
      fetch C_SUBCLASS_TL into O_TEXT_1;

      if C_SUBCLASS_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_SUBCLASS_TL;
   end if ; 

   if I_translate_table = 'SUPS_PACK_TMPL_DESC_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_SUPS_PACK_TMPL_DESC_TL;
      fetch C_SUPS_PACK_TMPL_DESC_TL into O_TEXT_1;

      if C_SUPS_PACK_TMPL_DESC_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_SUPS_PACK_TMPL_DESC_TL;
   end if ; 

   if I_translate_table = 'SUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_SUPS_TL;
      fetch C_SUPS_TL into O_TEXT_1,
                           O_TEXT_2,
                           O_TEXT_3;

      if C_SUPS_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_SUPS_TL;
   end if ; 

   if I_translate_table = 'UDA_ITEM_FF_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_UDA_ITEM_FF_TL;
      fetch C_UDA_ITEM_FF_TL into O_TEXT_1;

      if C_UDA_ITEM_FF_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_UDA_ITEM_FF_TL;
   end if ; 

   if I_translate_table = 'WF_COST_BUILDUP_TMPL_DTL_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_WF_COST_BUILDUP_TMPL_DTL_TL;
      fetch C_WF_COST_BUILDUP_TMPL_DTL_TL into O_TEXT_1;

      if C_WF_COST_BUILDUP_TMPL_DTL_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_WF_COST_BUILDUP_TMPL_DTL_TL;
   end if ; 

   if I_translate_table = 'WF_COST_BUILDUP_TMPL_HD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_WF_COST_BUILDUP_TMPL_HD_TL;
      fetch C_WF_COST_BUILDUP_TMPL_HD_TL into O_TEXT_1;

      if C_WF_COST_BUILDUP_TMPL_HD_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_WF_COST_BUILDUP_TMPL_HD_TL;
   end if ; 

   if I_translate_table = 'WH_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      open C_WH_TL;
      fetch C_WH_TL into O_TEXT_1,
                         O_TEXT_2;

      if C_WH_TL%FOUND then
         O_row_exist := 'Y';
      else 
         O_row_exist := 'N';
      end if;

      close C_WH_TL;
   end if ; 


return TRUE ;

EXCEPTION 
   when OTHERS then 
   if C_ADDR_TL%ISOPEN then
      close C_ADDR_TL;
   end if;

   if C_AREA_TL%ISOPEN then
      close C_AREA_TL;
   end if;

   if C_CHAIN_TL%ISOPEN then
      close C_CHAIN_TL;
   end if;

   if C_CLASS_TL%ISOPEN then
      close C_CLASS_TL;
   end if;

   if C_COST_ZONE_GROUP_TL%ISOPEN then
      close C_COST_ZONE_GROUP_TL;
   end if;

   if C_COST_ZONE_TL%ISOPEN then
      close C_COST_ZONE_TL;
   end if;

   if C_DEPS_TL%ISOPEN then
      close C_DEPS_TL;
   end if;

   if C_DIFF_GROUP_HEAD_TL%ISOPEN then
      close C_DIFF_GROUP_HEAD_TL;
   end if;

   if C_DIFF_RANGE_HEAD_TL%ISOPEN then
      close C_DIFF_RANGE_HEAD_TL;
   end if;

   if C_DIFF_RATIO_HEAD_TL%ISOPEN then
      close C_DIFF_RATIO_HEAD_TL;
   end if;

   if C_DISTRICT_TL%ISOPEN then
      close C_DISTRICT_TL;
   end if;

   if C_DIVISION_TL%ISOPEN then
      close C_DIVISION_TL;
   end if;

   if C_GROUPS_TL%ISOPEN then
      close C_GROUPS_TL;
   end if;

   if C_ITEM_IMAGE_TL%ISOPEN then
      close C_ITEM_IMAGE_TL;
   end if;

   if C_ITEM_MASTER_TL%ISOPEN then
      close C_ITEM_MASTER_TL;
   end if;

   if C_ITEM_SUPPLIER_TL%ISOPEN then
      close C_ITEM_SUPPLIER_TL;
   end if;

   if C_ITEM_XFORM_HEAD_TL%ISOPEN then
      close C_ITEM_XFORM_HEAD_TL;
   end if;

   if C_LOC_LIST_HEAD_TL%ISOPEN then
      close C_LOC_LIST_HEAD_TL;
   end if;

   if C_PACK_TMPL_HEAD_TL%ISOPEN then
      close C_PACK_TMPL_HEAD_TL;
   end if;

   if C_PARTNER_TL%ISOPEN then
      close C_PARTNER_TL;
   end if;

   if C_PEND_MERCH_HIER_TL%ISOPEN then
      close C_PEND_MERCH_HIER_TL;
   end if;

   if C_POS_COUPON_HEAD_TL%ISOPEN then
      close C_POS_COUPON_HEAD_TL;
   end if;

   if C_PRIORITY_GROUP_TL%ISOPEN then
      close C_PRIORITY_GROUP_TL;
   end if;

   if C_RECLASS_HEAD_TL%ISOPEN then
      close C_RECLASS_HEAD_TL;
   end if;

   if C_REGION_TL%ISOPEN then
      close C_REGION_TL;
   end if;

   if C_RELATED_ITEM_HEAD_TL%ISOPEN then
      close C_RELATED_ITEM_HEAD_TL;
   end if;

   if C_SKULIST_HEAD_TL%ISOPEN then
      close C_SKULIST_HEAD_TL;
   end if;

   if C_STORE_ADD_TL%ISOPEN then
      close C_STORE_ADD_TL;
   end if;

   if C_STORE_TL%ISOPEN then
      close C_STORE_TL;
   end if;

   if C_SUBCLASS_TL%ISOPEN then
      close C_SUBCLASS_TL;
   end if;

   if C_SUPS_PACK_TMPL_DESC_TL%ISOPEN then
      close C_SUPS_PACK_TMPL_DESC_TL;
   end if;

   if C_SUPS_TL%ISOPEN then
      close C_SUPS_TL;
   end if;

   if C_UDA_ITEM_FF_TL%ISOPEN then
      close C_UDA_ITEM_FF_TL;
   end if;

   if C_WF_COST_BUILDUP_TMPL_DTL_TL%ISOPEN then
      close C_WF_COST_BUILDUP_TMPL_DTL_TL;
   end if;

   if C_WF_COST_BUILDUP_TMPL_HD_TL%ISOPEN then
      close C_WF_COST_BUILDUP_TMPL_HD_TL;
   end if;

   if C_WH_TL%ISOPEN then
      close C_WH_TL;
   end if;


   if C_LANGUAGE%ISOPEN then
      close C_LANGUAGE;
   end if;
   
   if C_LAN_EXIST%ISOPEN then
      close C_LAN_EXIST;
   end if;

   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE)); 
   return FALSE; 

END GET_TL_ROW;

----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_TL_ROW(
                     O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_translate_table   IN      VARCHAR2,
                     I_lang              IN      NUMBER,
                     I_key_number_1      IN      NUMBER,
                     I_key_number_2      IN      NUMBER,
                     I_key_number_3      IN      NUMBER,
                     I_key_number_4      IN      NUMBER,
                     I_key_varchar2_5    IN      VARCHAR2,
                     I_key_varchar2_6    IN      VARCHAR2,
                     I_key_varchar2_7    IN      VARCHAR2,
                     I_key_varchar2_8    IN      VARCHAR2,
                     I_key_varchar2_9    IN      VARCHAR2,
                     I_key_date_10       IN      DATE,
                     I_key_date_11       IN      DATE
                     )
RETURN BOOLEAN IS 
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   L_tab_name             TRANSLATE_METADATA.TRANSLATE_TABLE%TYPE;
   L_program              VARCHAR2(100) := 'TRANSLATION_SQL.LOCK_TL_ROW';
   cursor C_ADDR_TL is 
       select 'X' 
         from ADDR_TL
        where Lang = I_lang 
         and ADDR_KEY = I_KEY_NUMBER_1
         for update nowait;

   cursor C_AREA_TL is 
       select 'X' 
         from AREA_TL
        where Lang = I_lang 
         and AREA = I_KEY_NUMBER_1
         for update nowait;

   cursor C_CHAIN_TL is 
       select 'X' 
         from CHAIN_TL
        where Lang = I_lang 
         and CHAIN = I_KEY_NUMBER_1
         for update nowait;

   cursor C_CLASS_TL is 
       select 'X' 
         from CLASS_TL
        where Lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         and CLASS = I_KEY_NUMBER_2
         for update nowait;

   cursor C_COST_ZONE_GROUP_TL is 
       select 'X' 
         from COST_ZONE_GROUP_TL
        where Lang = I_lang 
         and ZONE_GROUP_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_COST_ZONE_TL is 
       select 'X' 
         from COST_ZONE_TL
        where Lang = I_lang 
         and ZONE_GROUP_ID = I_KEY_NUMBER_1
         and ZONE_ID = I_KEY_NUMBER_2
         for update nowait;

   cursor C_DEPS_TL is 
       select 'X' 
         from DEPS_TL
        where Lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         for update nowait;

   cursor C_DIFF_GROUP_HEAD_TL is 
       select 'X' 
         from DIFF_GROUP_HEAD_TL
        where Lang = I_lang 
         and DIFF_GROUP_ID = I_KEY_VARCHAR2_5
         for update nowait;

   cursor C_DIFF_RANGE_HEAD_TL is 
       select 'X' 
         from DIFF_RANGE_HEAD_TL
        where Lang = I_lang 
         and DIFF_RANGE = I_KEY_NUMBER_1
         for update nowait;

   cursor C_DIFF_RATIO_HEAD_TL is 
       select 'X' 
         from DIFF_RATIO_HEAD_TL
        where Lang = I_lang 
         and DIFF_RATIO_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_DISTRICT_TL is 
       select 'X' 
         from DISTRICT_TL
        where Lang = I_lang 
         and DISTRICT = I_KEY_NUMBER_1
         for update nowait;

   cursor C_DIVISION_TL is 
       select 'X' 
         from DIVISION_TL
        where Lang = I_lang 
         and DIVISION = I_KEY_NUMBER_1
         for update nowait;

   cursor C_GROUPS_TL is 
       select 'X' 
         from GROUPS_TL
        where Lang = I_lang 
         and GROUP_NO = I_KEY_NUMBER_1
         for update nowait;

   cursor C_ITEM_IMAGE_TL is 
       select 'X' 
         from ITEM_IMAGE_TL
        where Lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and IMAGE_NAME = I_KEY_VARCHAR2_6
         for update nowait;

   cursor C_ITEM_MASTER_TL is 
       select 'X' 
         from ITEM_MASTER_TL
        where Lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         for update nowait;

   cursor C_ITEM_SUPPLIER_TL is 
       select 'X' 
         from ITEM_SUPPLIER_TL
        where Lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and SUPPLIER = I_KEY_NUMBER_1
         for update nowait;

   cursor C_ITEM_XFORM_HEAD_TL is 
       select 'X' 
         from ITEM_XFORM_HEAD_TL
        where Lang = I_lang 
         and ITEM_XFORM_HEAD_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_LOC_LIST_HEAD_TL is 
       select 'X' 
         from LOC_LIST_HEAD_TL
        where Lang = I_lang 
         and LOC_LIST = I_KEY_NUMBER_1
         for update nowait;

   cursor C_PACK_TMPL_HEAD_TL is 
       select 'X' 
         from PACK_TMPL_HEAD_TL
        where Lang = I_lang 
         and PACK_TMPL_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_PARTNER_TL is 
       select 'X' 
         from PARTNER_TL
        where Lang = I_lang 
         and PARTNER_TYPE = I_KEY_VARCHAR2_5
         and PARTNER_ID = I_KEY_VARCHAR2_6
         for update nowait;

   cursor C_PEND_MERCH_HIER_TL is 
       select 'X' 
         from PEND_MERCH_HIER_TL
        where Lang = I_lang 
         and MERCH_HIER_ID = I_KEY_NUMBER_1
         and NVL(MERCH_HIER_PARENT_ID,-1) = NVL(I_KEY_NUMBER_2,-1)
         and NVL(MERCH_HIER_GRANDPARENT_ID,-1) = NVL(I_KEY_NUMBER_3,-1)
         and HIER_TYPE = I_KEY_VARCHAR2_5
         for update nowait;

   cursor C_POS_COUPON_HEAD_TL is 
       select 'X' 
         from POS_COUPON_HEAD_TL
        where Lang = I_lang 
         and COUPON_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_PRIORITY_GROUP_TL is 
       select 'X' 
         from PRIORITY_GROUP_TL
        where Lang = I_lang 
         and PRIORITY_GROUP_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_RECLASS_HEAD_TL is 
       select 'X' 
         from RECLASS_HEAD_TL
        where Lang = I_lang 
         and RECLASS_NO = I_KEY_NUMBER_1
         for update nowait;

   cursor C_REGION_TL is 
       select 'X' 
         from REGION_TL
        where Lang = I_lang 
         and REGION = I_KEY_NUMBER_1
         for update nowait;

   cursor C_RELATED_ITEM_HEAD_TL is 
       select 'X' 
         from RELATED_ITEM_HEAD_TL
        where Lang = I_lang 
         and RELATIONSHIP_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_SKULIST_HEAD_TL is 
       select 'X' 
         from SKULIST_HEAD_TL
        where Lang = I_lang 
         and SKULIST = I_KEY_NUMBER_1
         for update nowait;

   cursor C_STORE_ADD_TL is 
       select 'X' 
         from STORE_ADD_TL
        where Lang = I_lang 
         and STORE = I_KEY_NUMBER_1
         for update nowait;

   cursor C_STORE_TL is 
       select 'X' 
         from STORE_TL
        where Lang = I_lang 
         and STORE = I_KEY_NUMBER_1
         for update nowait;

   cursor C_SUBCLASS_TL is 
       select 'X' 
         from SUBCLASS_TL
        where Lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         and CLASS = I_KEY_NUMBER_2
         and SUBCLASS = I_KEY_NUMBER_3
         for update nowait;

   cursor C_SUPS_PACK_TMPL_DESC_TL is 
       select 'X' 
         from SUPS_PACK_TMPL_DESC_TL
        where Lang = I_lang 
         and SUPPLIER = I_KEY_NUMBER_1
         and PACK_TMPL_ID = I_KEY_NUMBER_2
         for update nowait;

   cursor C_SUPS_TL is 
       select 'X' 
         from SUPS_TL
        where Lang = I_lang 
         and SUPPLIER = I_KEY_NUMBER_1
         for update nowait;

   cursor C_UDA_ITEM_FF_TL is 
       select 'X' 
         from UDA_ITEM_FF_TL
        where Lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and UDA_ID = I_KEY_NUMBER_1
         and UDA_TEXT = I_KEY_VARCHAR2_6
         for update nowait;

   cursor C_WF_COST_BUILDUP_TMPL_DTL_TL is 
       select 'X' 
         from WF_COST_BUILDUP_TMPL_DTL_TL
        where Lang = I_lang 
         and TEMPL_ID = I_KEY_NUMBER_1
         and COST_COMP_ID = I_KEY_VARCHAR2_5
         for update nowait;

   cursor C_WF_COST_BUILDUP_TMPL_HD_TL is 
       select 'X' 
         from WF_COST_BUILDUP_TMPL_HD_TL
        where Lang = I_lang 
         and TEMPL_ID = I_KEY_NUMBER_1
         for update nowait;

   cursor C_WH_TL is 
       select 'X' 
         from WH_TL
        where Lang = I_lang 
         and WH = I_KEY_NUMBER_1
         for update nowait;


BEGIN 
   if I_translate_table = 'UDA_ITEM_FF_TL' then 

      L_tab_name := 'UDA_ITEM_FF_TL' ;
      open C_UDA_ITEM_FF_TL ;

      close C_UDA_ITEM_FF_TL ;

   end if ; 
   if I_translate_table = 'ADDR_TL' then 

      L_tab_name := 'ADDR_TL' ;
      open C_ADDR_TL ;

      close C_ADDR_TL ;

   end if ; 
   if I_translate_table = 'CLASS_TL' then 

      L_tab_name := 'CLASS_TL' ;
      open C_CLASS_TL ;

      close C_CLASS_TL ;

   end if ; 
   if I_translate_table = 'DEPS_TL' then 

      L_tab_name := 'DEPS_TL' ;
      open C_DEPS_TL ;

      close C_DEPS_TL ;

   end if ; 
   if I_translate_table = 'DIFF_GROUP_HEAD_TL' then 

      L_tab_name := 'DIFF_GROUP_HEAD_TL' ;
      open C_DIFF_GROUP_HEAD_TL ;

      close C_DIFF_GROUP_HEAD_TL ;

   end if ; 
   if I_translate_table = 'DIFF_RANGE_HEAD_TL' then 

      L_tab_name := 'DIFF_RANGE_HEAD_TL' ;
      open C_DIFF_RANGE_HEAD_TL ;

      close C_DIFF_RANGE_HEAD_TL ;

   end if ; 
   if I_translate_table = 'LOC_LIST_HEAD_TL' then 

      L_tab_name := 'LOC_LIST_HEAD_TL' ;
      open C_LOC_LIST_HEAD_TL ;

      close C_LOC_LIST_HEAD_TL ;

   end if ; 
   if I_translate_table = 'REGION_TL' then 

      L_tab_name := 'REGION_TL' ;
      open C_REGION_TL ;

      close C_REGION_TL ;

   end if ; 
   if I_translate_table = 'SUPS_TL' then 

      L_tab_name := 'SUPS_TL' ;
      open C_SUPS_TL ;

      close C_SUPS_TL ;

   end if ; 
   if I_translate_table = 'COST_ZONE_TL' then 

      L_tab_name := 'COST_ZONE_TL' ;
      open C_COST_ZONE_TL ;

      close C_COST_ZONE_TL ;

   end if ; 
   if I_translate_table = 'GROUPS_TL' then 

      L_tab_name := 'GROUPS_TL' ;
      open C_GROUPS_TL ;

      close C_GROUPS_TL ;

   end if ; 
   if I_translate_table = 'SUPS_PACK_TMPL_DESC_TL' then 

      L_tab_name := 'SUPS_PACK_TMPL_DESC_TL' ;
      open C_SUPS_PACK_TMPL_DESC_TL ;

      close C_SUPS_PACK_TMPL_DESC_TL ;

   end if ; 
   if I_translate_table = 'AREA_TL' then 

      L_tab_name := 'AREA_TL' ;
      open C_AREA_TL ;

      close C_AREA_TL ;

   end if ; 
   if I_translate_table = 'DIFF_RATIO_HEAD_TL' then 

      L_tab_name := 'DIFF_RATIO_HEAD_TL' ;
      open C_DIFF_RATIO_HEAD_TL ;

      close C_DIFF_RATIO_HEAD_TL ;

   end if ; 
   if I_translate_table = 'POS_COUPON_HEAD_TL' then 

      L_tab_name := 'POS_COUPON_HEAD_TL' ;
      open C_POS_COUPON_HEAD_TL ;

      close C_POS_COUPON_HEAD_TL ;

   end if ; 
   if I_translate_table = 'CHAIN_TL' then 

      L_tab_name := 'CHAIN_TL' ;
      open C_CHAIN_TL ;

      close C_CHAIN_TL ;

   end if ; 
   if I_translate_table = 'ITEM_IMAGE_TL' then 

      L_tab_name := 'ITEM_IMAGE_TL' ;
      open C_ITEM_IMAGE_TL ;

      close C_ITEM_IMAGE_TL ;

   end if ; 
   if I_translate_table = 'ITEM_SUPPLIER_TL' then 

      L_tab_name := 'ITEM_SUPPLIER_TL' ;
      open C_ITEM_SUPPLIER_TL ;

      close C_ITEM_SUPPLIER_TL ;

   end if ; 
   if I_translate_table = 'PEND_MERCH_HIER_TL' then 

      L_tab_name := 'PEND_MERCH_HIER_TL' ;
      open C_PEND_MERCH_HIER_TL ;

      close C_PEND_MERCH_HIER_TL ;

   end if ; 
   if I_translate_table = 'PRIORITY_GROUP_TL' then 

      L_tab_name := 'PRIORITY_GROUP_TL' ;
      open C_PRIORITY_GROUP_TL ;

      close C_PRIORITY_GROUP_TL ;

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_DTL_TL' then 

      L_tab_name := 'WF_COST_BUILDUP_TMPL_DTL_TL' ;
      open C_WF_COST_BUILDUP_TMPL_DTL_TL ;

      close C_WF_COST_BUILDUP_TMPL_DTL_TL ;

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_HD_TL' then 

      L_tab_name := 'WF_COST_BUILDUP_TMPL_HD_TL' ;
      open C_WF_COST_BUILDUP_TMPL_HD_TL ;

      close C_WF_COST_BUILDUP_TMPL_HD_TL ;

   end if ; 
   if I_translate_table = 'COST_ZONE_GROUP_TL' then 

      L_tab_name := 'COST_ZONE_GROUP_TL' ;
      open C_COST_ZONE_GROUP_TL ;

      close C_COST_ZONE_GROUP_TL ;

   end if ; 
   if I_translate_table = 'DIVISION_TL' then 

      L_tab_name := 'DIVISION_TL' ;
      open C_DIVISION_TL ;

      close C_DIVISION_TL ;

   end if ; 
   if I_translate_table = 'RECLASS_HEAD_TL' then 

      L_tab_name := 'RECLASS_HEAD_TL' ;
      open C_RECLASS_HEAD_TL ;

      close C_RECLASS_HEAD_TL ;

   end if ; 
   if I_translate_table = 'STORE_ADD_TL' then 

      L_tab_name := 'STORE_ADD_TL' ;
      open C_STORE_ADD_TL ;

      close C_STORE_ADD_TL ;

   end if ; 
   if I_translate_table = 'DISTRICT_TL' then 

      L_tab_name := 'DISTRICT_TL' ;
      open C_DISTRICT_TL ;

      close C_DISTRICT_TL ;

   end if ; 
   if I_translate_table = 'ITEM_XFORM_HEAD_TL' then 

      L_tab_name := 'ITEM_XFORM_HEAD_TL' ;
      open C_ITEM_XFORM_HEAD_TL ;

      close C_ITEM_XFORM_HEAD_TL ;

   end if ; 
   if I_translate_table = 'PACK_TMPL_HEAD_TL' then 

      L_tab_name := 'PACK_TMPL_HEAD_TL' ;
      open C_PACK_TMPL_HEAD_TL ;

      close C_PACK_TMPL_HEAD_TL ;

   end if ; 
   if I_translate_table = 'PARTNER_TL' then 

      L_tab_name := 'PARTNER_TL' ;
      open C_PARTNER_TL ;

      close C_PARTNER_TL ;

   end if ; 
   if I_translate_table = 'RELATED_ITEM_HEAD_TL' then 

      L_tab_name := 'RELATED_ITEM_HEAD_TL' ;
      open C_RELATED_ITEM_HEAD_TL ;

      close C_RELATED_ITEM_HEAD_TL ;

   end if ; 
   if I_translate_table = 'WH_TL' then 

      L_tab_name := 'WH_TL' ;
      open C_WH_TL ;

      close C_WH_TL ;

   end if ; 
   if I_translate_table = 'ITEM_MASTER_TL' then 

      L_tab_name := 'ITEM_MASTER_TL' ;
      open C_ITEM_MASTER_TL ;

      close C_ITEM_MASTER_TL ;

   end if ; 
   if I_translate_table = 'SKULIST_HEAD_TL' then 

      L_tab_name := 'SKULIST_HEAD_TL' ;
      open C_SKULIST_HEAD_TL ;

      close C_SKULIST_HEAD_TL ;

   end if ; 
   if I_translate_table = 'STORE_TL' then 

      L_tab_name := 'STORE_TL' ;
      open C_STORE_TL ;

      close C_STORE_TL ;

   end if ; 
   if I_translate_table = 'SUBCLASS_TL' then 

      L_tab_name := 'SUBCLASS_TL' ;
      open C_SUBCLASS_TL ;

      close C_SUBCLASS_TL ;

   end if ; 

return TRUE ;

EXCEPTION 
   when RECORD_LOCKED then 
   if C_ADDR_TL%ISOPEN then
      close C_ADDR_TL;
   end if;

   if C_AREA_TL%ISOPEN then
      close C_AREA_TL;
   end if;

   if C_CHAIN_TL%ISOPEN then
      close C_CHAIN_TL;
   end if;

   if C_CLASS_TL%ISOPEN then
      close C_CLASS_TL;
   end if;

   if C_COST_ZONE_GROUP_TL%ISOPEN then
      close C_COST_ZONE_GROUP_TL;
   end if;

   if C_COST_ZONE_TL%ISOPEN then
      close C_COST_ZONE_TL;
   end if;

   if C_DEPS_TL%ISOPEN then
      close C_DEPS_TL;
   end if;

   if C_DIFF_GROUP_HEAD_TL%ISOPEN then
      close C_DIFF_GROUP_HEAD_TL;
   end if;

   if C_DIFF_RANGE_HEAD_TL%ISOPEN then
      close C_DIFF_RANGE_HEAD_TL;
   end if;

   if C_DIFF_RATIO_HEAD_TL%ISOPEN then
      close C_DIFF_RATIO_HEAD_TL;
   end if;

   if C_DISTRICT_TL%ISOPEN then
      close C_DISTRICT_TL;
   end if;

   if C_DIVISION_TL%ISOPEN then
      close C_DIVISION_TL;
   end if;

   if C_GROUPS_TL%ISOPEN then
      close C_GROUPS_TL;
   end if;

   if C_ITEM_IMAGE_TL%ISOPEN then
      close C_ITEM_IMAGE_TL;
   end if;

   if C_ITEM_MASTER_TL%ISOPEN then
      close C_ITEM_MASTER_TL;
   end if;

   if C_ITEM_SUPPLIER_TL%ISOPEN then
      close C_ITEM_SUPPLIER_TL;
   end if;

   if C_ITEM_XFORM_HEAD_TL%ISOPEN then
      close C_ITEM_XFORM_HEAD_TL;
   end if;

   if C_LOC_LIST_HEAD_TL%ISOPEN then
      close C_LOC_LIST_HEAD_TL;
   end if;

   if C_PACK_TMPL_HEAD_TL%ISOPEN then
      close C_PACK_TMPL_HEAD_TL;
   end if;

   if C_PARTNER_TL%ISOPEN then
      close C_PARTNER_TL;
   end if;

   if C_PEND_MERCH_HIER_TL%ISOPEN then
      close C_PEND_MERCH_HIER_TL;
   end if;

   if C_POS_COUPON_HEAD_TL%ISOPEN then
      close C_POS_COUPON_HEAD_TL;
   end if;

   if C_PRIORITY_GROUP_TL%ISOPEN then
      close C_PRIORITY_GROUP_TL;
   end if;

   if C_RECLASS_HEAD_TL%ISOPEN then
      close C_RECLASS_HEAD_TL;
   end if;

   if C_REGION_TL%ISOPEN then
      close C_REGION_TL;
   end if;

   if C_RELATED_ITEM_HEAD_TL%ISOPEN then
      close C_RELATED_ITEM_HEAD_TL;
   end if;

   if C_SKULIST_HEAD_TL%ISOPEN then
      close C_SKULIST_HEAD_TL;
   end if;

   if C_STORE_ADD_TL%ISOPEN then
      close C_STORE_ADD_TL;
   end if;

   if C_STORE_TL%ISOPEN then
      close C_STORE_TL;
   end if;

   if C_SUBCLASS_TL%ISOPEN then
      close C_SUBCLASS_TL;
   end if;

   if C_SUPS_PACK_TMPL_DESC_TL%ISOPEN then
      close C_SUPS_PACK_TMPL_DESC_TL;
   end if;

   if C_SUPS_TL%ISOPEN then
      close C_SUPS_TL;
   end if;

   if C_UDA_ITEM_FF_TL%ISOPEN then
      close C_UDA_ITEM_FF_TL;
   end if;

   if C_WF_COST_BUILDUP_TMPL_DTL_TL%ISOPEN then
      close C_WF_COST_BUILDUP_TMPL_DTL_TL;
   end if;

   if C_WF_COST_BUILDUP_TMPL_HD_TL%ISOPEN then
      close C_WF_COST_BUILDUP_TMPL_HD_TL;
   end if;

   if C_WH_TL%ISOPEN then
      close C_WH_TL;
   end if;


   O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                         L_tab_name,
                                         NULL,
                                         NULL   ); 
return FALSE; 

   when OTHERS then 
   if C_ADDR_TL%ISOPEN then
      close C_ADDR_TL;
   end if;

   if C_AREA_TL%ISOPEN then
      close C_AREA_TL;
   end if;

   if C_CHAIN_TL%ISOPEN then
      close C_CHAIN_TL;
   end if;

   if C_CLASS_TL%ISOPEN then
      close C_CLASS_TL;
   end if;

   if C_COST_ZONE_GROUP_TL%ISOPEN then
      close C_COST_ZONE_GROUP_TL;
   end if;

   if C_COST_ZONE_TL%ISOPEN then
      close C_COST_ZONE_TL;
   end if;

   if C_DEPS_TL%ISOPEN then
      close C_DEPS_TL;
   end if;

   if C_DIFF_GROUP_HEAD_TL%ISOPEN then
      close C_DIFF_GROUP_HEAD_TL;
   end if;

   if C_DIFF_RANGE_HEAD_TL%ISOPEN then
      close C_DIFF_RANGE_HEAD_TL;
   end if;

   if C_DIFF_RATIO_HEAD_TL%ISOPEN then
      close C_DIFF_RATIO_HEAD_TL;
   end if;

   if C_DISTRICT_TL%ISOPEN then
      close C_DISTRICT_TL;
   end if;

   if C_DIVISION_TL%ISOPEN then
      close C_DIVISION_TL;
   end if;

   if C_GROUPS_TL%ISOPEN then
      close C_GROUPS_TL;
   end if;

   if C_ITEM_IMAGE_TL%ISOPEN then
      close C_ITEM_IMAGE_TL;
   end if;

   if C_ITEM_MASTER_TL%ISOPEN then
      close C_ITEM_MASTER_TL;
   end if;

   if C_ITEM_SUPPLIER_TL%ISOPEN then
      close C_ITEM_SUPPLIER_TL;
   end if;

   if C_ITEM_XFORM_HEAD_TL%ISOPEN then
      close C_ITEM_XFORM_HEAD_TL;
   end if;

   if C_LOC_LIST_HEAD_TL%ISOPEN then
      close C_LOC_LIST_HEAD_TL;
   end if;

   if C_PACK_TMPL_HEAD_TL%ISOPEN then
      close C_PACK_TMPL_HEAD_TL;
   end if;

   if C_PARTNER_TL%ISOPEN then
      close C_PARTNER_TL;
   end if;

   if C_PEND_MERCH_HIER_TL%ISOPEN then
      close C_PEND_MERCH_HIER_TL;
   end if;

   if C_POS_COUPON_HEAD_TL%ISOPEN then
      close C_POS_COUPON_HEAD_TL;
   end if;

   if C_PRIORITY_GROUP_TL%ISOPEN then
      close C_PRIORITY_GROUP_TL;
   end if;

   if C_RECLASS_HEAD_TL%ISOPEN then
      close C_RECLASS_HEAD_TL;
   end if;

   if C_REGION_TL%ISOPEN then
      close C_REGION_TL;
   end if;

   if C_RELATED_ITEM_HEAD_TL%ISOPEN then
      close C_RELATED_ITEM_HEAD_TL;
   end if;

   if C_SKULIST_HEAD_TL%ISOPEN then
      close C_SKULIST_HEAD_TL;
   end if;

   if C_STORE_ADD_TL%ISOPEN then
      close C_STORE_ADD_TL;
   end if;

   if C_STORE_TL%ISOPEN then
      close C_STORE_TL;
   end if;

   if C_SUBCLASS_TL%ISOPEN then
      close C_SUBCLASS_TL;
   end if;

   if C_SUPS_PACK_TMPL_DESC_TL%ISOPEN then
      close C_SUPS_PACK_TMPL_DESC_TL;
   end if;

   if C_SUPS_TL%ISOPEN then
      close C_SUPS_TL;
   end if;

   if C_UDA_ITEM_FF_TL%ISOPEN then
      close C_UDA_ITEM_FF_TL;
   end if;

   if C_WF_COST_BUILDUP_TMPL_DTL_TL%ISOPEN then
      close C_WF_COST_BUILDUP_TMPL_DTL_TL;
   end if;

   if C_WF_COST_BUILDUP_TMPL_HD_TL%ISOPEN then
      close C_WF_COST_BUILDUP_TMPL_HD_TL;
   end if;

   if C_WH_TL%ISOPEN then
      close C_WH_TL;
   end if;


   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE)); 
return FALSE; 

END LOCK_TL_ROW;

----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_TL_ROW(
                       O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_translate_table   IN      VARCHAR2,
                       I_lang              IN      NUMBER,
                       I_key_number_1      IN      NUMBER,
                       I_key_number_2      IN      NUMBER,
                       I_key_number_3      IN      NUMBER,
                       I_key_number_4      IN      NUMBER,
                       I_key_varchar2_5    IN      VARCHAR2,
                       I_key_varchar2_6    IN      VARCHAR2,
                       I_key_varchar2_7    IN      VARCHAR2,
                       I_key_varchar2_8    IN      VARCHAR2,
                       I_key_varchar2_9    IN      VARCHAR2,
                       I_key_date_10       IN      DATE,
                       I_key_date_11       IN      DATE
                       )
RETURN BOOLEAN IS 

   L_dummy      VARCHAR2(1);
   L_lan        LANG.LANG%TYPE;
   L_program    VARCHAR2(100) := 'TRANSLATION_SQL.DELETE_TL_ROW';

   cursor C_LANGUAGE is
      select 'X'
        from lang 
       where lang.lang = I_lang;

   cursor C_LAN_EXIST is
      select DATA_INTEGRATION_LANG
        from SYSTEM_CONFIG_OPTIONS;

BEGIN 

   if I_lang is null then
      O_error_message := SQL_LIB.CREATE_MSG('LANG_IS_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE; 
   end if; 

   open  C_LANGUAGE; 
   fetch C_LANGUAGE INTO L_dummy;
   if C_LANGUAGE%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LANGUAGE;
      return FALSE; 
   end if; 
   close C_LANGUAGE;

   open  C_LAN_EXIST; 
   fetch C_LAN_EXIST INTO L_lan;
   if L_lan = I_lang then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LAN_EXIST;
      return FALSE; 
   end if; 
   close C_LAN_EXIST;

   if I_translate_table = 'ADDR_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from ADDR_TL
       where lang = I_lang 
         and ADDR_KEY = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'AREA_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from AREA_TL
       where lang = I_lang 
         and AREA = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'CHAIN_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from CHAIN_TL
       where lang = I_lang 
         and CHAIN = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'CLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from CLASS_TL
       where lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         and CLASS = I_KEY_NUMBER_2;

   end if ; 
   if I_translate_table = 'COST_ZONE_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from COST_ZONE_GROUP_TL
       where lang = I_lang 
         and ZONE_GROUP_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'COST_ZONE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from COST_ZONE_TL
       where lang = I_lang 
         and ZONE_GROUP_ID = I_KEY_NUMBER_1
         and ZONE_ID = I_KEY_NUMBER_2;

   end if ; 
   if I_translate_table = 'DEPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from DEPS_TL
       where lang = I_lang 
         and DEPT = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DIFF_GROUP_HEAD_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from DIFF_GROUP_HEAD_TL
       where lang = I_lang 
         and DIFF_GROUP_ID = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'DIFF_RANGE_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from DIFF_RANGE_HEAD_TL
       where lang = I_lang 
         and DIFF_RANGE = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DIFF_RATIO_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from DIFF_RATIO_HEAD_TL
       where lang = I_lang 
         and DIFF_RATIO_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DISTRICT_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from DISTRICT_TL
       where lang = I_lang 
         and DISTRICT = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DIVISION_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from DIVISION_TL
       where lang = I_lang 
         and DIVISION = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'GROUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from GROUPS_TL
       where lang = I_lang 
         and GROUP_NO = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'ITEM_IMAGE_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from ITEM_IMAGE_TL
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and IMAGE_NAME = I_KEY_VARCHAR2_6;

   end if ; 
   if I_translate_table = 'ITEM_MASTER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from ITEM_MASTER_TL
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'ITEM_SUPPLIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from ITEM_SUPPLIER_TL
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and SUPPLIER = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'ITEM_XFORM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from ITEM_XFORM_HEAD_TL
       where lang = I_lang 
         and ITEM_XFORM_HEAD_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'LOC_LIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from LOC_LIST_HEAD_TL
       where lang = I_lang 
         and LOC_LIST = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'PACK_TMPL_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from PACK_TMPL_HEAD_TL
       where lang = I_lang 
         and PACK_TMPL_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'PARTNER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from PARTNER_TL
       where lang = I_lang 
         and PARTNER_TYPE = I_KEY_VARCHAR2_5
         and PARTNER_ID = I_KEY_VARCHAR2_6;

   end if ; 
   if I_translate_table = 'PEND_MERCH_HIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL  or (I_KEY_VARCHAR2_5 != 'V' and I_KEY_NUMBER_2 is NULL) or (I_KEY_VARCHAR2_5 = 'S' and I_KEY_NUMBER_3 is NULL)  then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from PEND_MERCH_HIER_TL
       where lang = I_lang 
         and MERCH_HIER_ID = I_KEY_NUMBER_1
         and NVL(MERCH_HIER_PARENT_ID,-1) = NVL(I_KEY_NUMBER_2,-1)
         and NVL(MERCH_HIER_GRANDPARENT_ID,-1) = NVL(I_KEY_NUMBER_3,-1)
         and HIER_TYPE = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'POS_COUPON_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from POS_COUPON_HEAD_TL
       where lang = I_lang 
         and COUPON_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'PRIORITY_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from PRIORITY_GROUP_TL
       where lang = I_lang 
         and PRIORITY_GROUP_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'RECLASS_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from RECLASS_HEAD_TL
       where lang = I_lang 
         and RECLASS_NO = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'REGION_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from REGION_TL
       where lang = I_lang 
         and REGION = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'RELATED_ITEM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from RELATED_ITEM_HEAD_TL
       where lang = I_lang 
         and RELATIONSHIP_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'SKULIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from SKULIST_HEAD_TL
       where lang = I_lang 
         and SKULIST = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'STORE_ADD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from STORE_ADD_TL
       where lang = I_lang 
         and STORE = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'STORE_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from STORE_TL
       where lang = I_lang 
         and STORE = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'SUBCLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_KEY_NUMBER_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from SUBCLASS_TL
       where lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         and CLASS = I_KEY_NUMBER_2
         and SUBCLASS = I_KEY_NUMBER_3;

   end if ; 
   if I_translate_table = 'SUPS_PACK_TMPL_DESC_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from SUPS_PACK_TMPL_DESC_TL
       where lang = I_lang 
         and SUPPLIER = I_KEY_NUMBER_1
         and PACK_TMPL_ID = I_KEY_NUMBER_2;

   end if ; 
   if I_translate_table = 'SUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from SUPS_TL
       where lang = I_lang 
         and SUPPLIER = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'UDA_ITEM_FF_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from UDA_ITEM_FF_TL
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and UDA_ID = I_KEY_NUMBER_1
         and UDA_TEXT = I_KEY_VARCHAR2_6;

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_DTL_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from WF_COST_BUILDUP_TMPL_DTL_TL
       where lang = I_lang 
         and TEMPL_ID = I_KEY_NUMBER_1
         and COST_COMP_ID = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_HD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from WF_COST_BUILDUP_TMPL_HD_TL
       where lang = I_lang 
         and TEMPL_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'WH_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      delete from WH_TL
       where lang = I_lang 
         and WH = I_KEY_NUMBER_1;

   end if ; 

return TRUE ;

EXCEPTION 
   when OTHERS then 
   if C_LANGUAGE%ISOPEN then
      close C_LANGUAGE;
   end if;
   if C_LAN_EXIST%ISOPEN then
      close C_LAN_EXIST;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE)); 
return FALSE; 

END DELETE_TL_ROW;

----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TL_ROW(
                       O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_translate_table   IN      VARCHAR2,
                       I_lang              IN      NUMBER,
                       I_key_number_1      IN      NUMBER,
                       I_key_number_2      IN      NUMBER,
                       I_key_number_3      IN      NUMBER,
                       I_key_number_4      IN      NUMBER,
                       I_key_varchar2_5    IN      VARCHAR2,
                       I_key_varchar2_6    IN      VARCHAR2,
                       I_key_varchar2_7    IN      VARCHAR2,
                       I_key_varchar2_8    IN      VARCHAR2,
                       I_key_varchar2_9    IN      VARCHAR2,
                       I_key_date_10       IN      DATE,
                       I_key_date_11       IN      DATE,
                       I_text_1            IN      VARCHAR2,
                       I_text_2            IN      VARCHAR2,
                       I_text_3            IN      VARCHAR2,
                       I_text_4            IN      VARCHAR2,
                       I_text_5            IN      VARCHAR2,
                       I_text_6            IN      VARCHAR2,
                       I_text_7            IN      VARCHAR2,
                       I_text_8            IN      VARCHAR2,
                       I_text_9            IN      VARCHAR2
                       )
RETURN BOOLEAN IS 

   L_dummy      VARCHAR2(1);
   L_lan        LANG.LANG%TYPE;
   L_program    VARCHAR2(100) := 'TRANSLATION_SQL.UPDATE_TL_ROW';

   cursor C_LANGUAGE is
      select 'X'
        from lang 
       where lang.lang = I_lang;

   cursor C_LAN_EXIST is
      select DATA_INTEGRATION_LANG
        from SYSTEM_CONFIG_OPTIONS;

BEGIN 

   if I_lang is null then
      O_error_message := SQL_LIB.CREATE_MSG('LANG_IS_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE; 
   end if; 

   open  C_LANGUAGE; 
   fetch C_LANGUAGE INTO L_dummy;
   if C_LANGUAGE%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LANGUAGE;
      return FALSE; 
   end if; 
   close C_LANGUAGE;

   open  C_LAN_EXIST; 
   fetch C_LAN_EXIST INTO L_lan;
   if L_lan = I_lang then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LAN_EXIST;
      return FALSE; 
   end if; 
   close C_LAN_EXIST;

   if I_translate_table = 'ADDR_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL OR I_TEXT_4 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_4),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_5),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_6),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update ADDR_TL
         set ADD_1 = I_TEXT_1,
             ADD_2 = I_TEXT_2,
             ADD_3 = I_TEXT_3,
             CITY = I_TEXT_4,
             CONTACT_NAME = I_TEXT_5,
             COUNTY = I_TEXT_6,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ADDR_KEY = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'AREA_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update AREA_TL
         set AREA_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and AREA = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'CHAIN_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update CHAIN_TL
         set CHAIN_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and CHAIN = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'CLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update CLASS_TL
         set CLASS_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         and CLASS = I_KEY_NUMBER_2;

   end if ; 
   if I_translate_table = 'COST_ZONE_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update COST_ZONE_GROUP_TL
         set DESCRIPTION = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ZONE_GROUP_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'COST_ZONE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update COST_ZONE_TL
         set DESCRIPTION = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ZONE_GROUP_ID = I_KEY_NUMBER_1
         and ZONE_ID = I_KEY_NUMBER_2;

   end if ; 
   if I_translate_table = 'DEPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update DEPS_TL
         set DEPT_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DEPT = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DIFF_GROUP_HEAD_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update DIFF_GROUP_HEAD_TL
         set DIFF_GROUP_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DIFF_GROUP_ID = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'DIFF_RANGE_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update DIFF_RANGE_HEAD_TL
         set DIFF_RANGE_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DIFF_RANGE = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DIFF_RATIO_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update DIFF_RATIO_HEAD_TL
         set DESCRIPTION = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DIFF_RATIO_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DISTRICT_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update DISTRICT_TL
         set DISTRICT_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DISTRICT = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'DIVISION_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update DIVISION_TL
         set DIV_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DIVISION = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'GROUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update GROUPS_TL
         set GROUP_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and GROUP_NO = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'ITEM_IMAGE_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 40 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update ITEM_IMAGE_TL
         set IMAGE_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and IMAGE_NAME = I_KEY_VARCHAR2_6;

   end if ; 
   if I_translate_table = 'ITEM_MASTER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_TEXT_1 IS NULL OR I_TEXT_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 250 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update ITEM_MASTER_TL
         set ITEM_DESC = I_TEXT_1,
             ITEM_DESC_SECONDARY = I_TEXT_2,
             SHORT_DESC = I_TEXT_3,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'ITEM_SUPPLIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 15 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_4),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_5),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update ITEM_SUPPLIER_TL
         set SUPP_LABEL = I_TEXT_1,
             SUPP_DIFF_1 = I_TEXT_2,
             SUPP_DIFF_2 = I_TEXT_3,
             SUPP_DIFF_3 = I_TEXT_4,
             SUPP_DIFF_4 = I_TEXT_5,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and SUPPLIER = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'ITEM_XFORM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update ITEM_XFORM_HEAD_TL
         set ITEM_XFORM_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ITEM_XFORM_HEAD_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'LOC_LIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update LOC_LIST_HEAD_TL
         set LOC_LIST_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and LOC_LIST = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'PACK_TMPL_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update PACK_TMPL_HEAD_TL
         set PACK_TMPL_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and PACK_TMPL_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'PARTNER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 240 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update PARTNER_TL
         set PARTNER_DESC = I_TEXT_1,
             PARTNER_NAME_SECONDARY = I_TEXT_2,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and PARTNER_TYPE = I_KEY_VARCHAR2_5
         and PARTNER_ID = I_KEY_VARCHAR2_6;

   end if ; 
   if I_translate_table = 'PEND_MERCH_HIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL  or (I_KEY_VARCHAR2_5 != 'V' and I_KEY_NUMBER_2 is NULL) or (I_KEY_VARCHAR2_5 = 'S' and I_KEY_NUMBER_3 is NULL)  OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update PEND_MERCH_HIER_TL
         set MERCH_HIER_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and MERCH_HIER_ID = I_KEY_NUMBER_1
         and NVL(MERCH_HIER_PARENT_ID,-1) = NVL(I_KEY_NUMBER_2,-1)
         and NVL(MERCH_HIER_GRANDPARENT_ID,-1) = NVL(I_KEY_NUMBER_3,-1)
         and HIER_TYPE = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'POS_COUPON_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update POS_COUPON_HEAD_TL
         set COUPON_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and COUPON_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'PRIORITY_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 100 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update PRIORITY_GROUP_TL
         set PRIORITY_GROUP_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and PRIORITY_GROUP_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'RECLASS_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update RECLASS_HEAD_TL
         set RECLASS_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and RECLASS_NO = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'REGION_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update REGION_TL
         set REGION_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and REGION = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'RELATED_ITEM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 255 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update RELATED_ITEM_HEAD_TL
         set RELATIONSHIP_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and RELATIONSHIP_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'SKULIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update SKULIST_HEAD_TL
         set SKULIST_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and SKULIST = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'STORE_ADD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update STORE_ADD_TL
         set STORE_NAME = I_TEXT_1,
             STORE_NAME_SECONDARY = I_TEXT_2,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and STORE = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'STORE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update STORE_TL
         set STORE_NAME = I_TEXT_1,
             STORE_NAME_SECONDARY = I_TEXT_2,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and STORE = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'SUBCLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_KEY_NUMBER_3 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update SUBCLASS_TL
         set SUB_NAME = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and DEPT = I_KEY_NUMBER_1
         and CLASS = I_KEY_NUMBER_2
         and SUBCLASS = I_KEY_NUMBER_3;

   end if ; 
   if I_translate_table = 'SUPS_PACK_TMPL_DESC_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update SUPS_PACK_TMPL_DESC_TL
         set SUPP_PACK_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and SUPPLIER = I_KEY_NUMBER_1
         and PACK_TMPL_ID = I_KEY_NUMBER_2;

   end if ; 
   if I_translate_table = 'SUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL OR I_TEXT_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update SUPS_TL
         set SUP_NAME = I_TEXT_1,
             SUP_NAME_SECONDARY = I_TEXT_2,
             CONTACT_NAME = I_TEXT_3,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and SUPPLIER = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'UDA_ITEM_FF_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update UDA_ITEM_FF_TL
         set UDA_TEXT_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and ITEM = I_KEY_VARCHAR2_5
         and UDA_ID = I_KEY_NUMBER_1
         and UDA_TEXT = I_KEY_VARCHAR2_6;

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_DTL_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_5 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update WF_COST_BUILDUP_TMPL_DTL_TL
         set DESCRIPTION = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and TEMPL_ID = I_KEY_NUMBER_1
         and COST_COMP_ID = I_KEY_VARCHAR2_5;

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_HD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update WF_COST_BUILDUP_TMPL_HD_TL
         set TEMPL_DESC = I_TEXT_1,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and TEMPL_ID = I_KEY_NUMBER_1;

   end if ; 
   if I_translate_table = 'WH_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if LOCK_TL_ROW(
                     O_error_message,
                     I_translate_table,
                     I_lang,
                     I_key_number_1,
                     I_key_number_2,
                     I_key_number_3,
                     I_key_number_4,
                     I_key_varchar2_5,
                     I_key_varchar2_6,
                     I_key_varchar2_7,
                     I_key_varchar2_8,
                     I_key_varchar2_9,
                     I_key_date_10,
                     I_key_date_11
                     ) = FALSE then 
         return FALSE; 
      end if; 

      update WH_TL
         set WH_NAME = I_TEXT_1,
             WH_NAME_SECONDARY = I_TEXT_2,
             LAST_UPDATE_ID = LP_USER,
             LAST_UPDATE_DATETIME = SYSDATE
       where lang = I_lang 
         and WH = I_KEY_NUMBER_1;

   end if ; 

return TRUE ;

EXCEPTION 
   when OTHERS then 
   if C_LANGUAGE%ISOPEN then
      close C_LANGUAGE;
   end if;
   if C_LAN_EXIST%ISOPEN then
      close C_LAN_EXIST;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE)); 
return FALSE; 

END UPDATE_TL_ROW;

----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_TL_ROW(
                       O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_translate_table   IN      VARCHAR2,
                       I_lang              IN      NUMBER,
                       I_key_number_1      IN      NUMBER,
                       I_key_number_2      IN      NUMBER,
                       I_key_number_3      IN      NUMBER,
                       I_key_number_4      IN      NUMBER,
                       I_key_varchar2_5    IN      VARCHAR2,
                       I_key_varchar2_6    IN      VARCHAR2,
                       I_key_varchar2_7    IN      VARCHAR2,
                       I_key_varchar2_8    IN      VARCHAR2,
                       I_key_varchar2_9    IN      VARCHAR2,
                       I_key_date_10       IN      DATE,
                       I_key_date_11       IN      DATE,
                       I_text_1            IN      VARCHAR2,
                       I_text_2            IN      VARCHAR2,
                       I_text_3            IN      VARCHAR2,
                       I_text_4            IN      VARCHAR2,
                       I_text_5            IN      VARCHAR2,
                       I_text_6            IN      VARCHAR2,
                       I_text_7            IN      VARCHAR2,
                       I_text_8            IN      VARCHAR2,
                       I_text_9            IN      VARCHAR2
                       )
RETURN BOOLEAN IS 

   DUP_VAL                EXCEPTION;
   PARENT_NOTFOUND        EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(DUP_VAL, -00001);
   PRAGMA                 EXCEPTION_INIT(PARENT_NOTFOUND, -02291);
   L_tab_name             TRANSLATE_METADATA.TRANSLATE_TABLE%TYPE;
   L_dummy      VARCHAR2(1);
   L_lan        LANG.LANG%TYPE;
   L_program    VARCHAR2(100) := 'TRANSLATION_SQL.INSERT_TL_ROW';

   cursor C_LANGUAGE is
      select 'X'
        from lang 
       where lang.lang = I_lang;

   cursor C_LAN_EXIST is
      select DATA_INTEGRATION_LANG
        from SYSTEM_CONFIG_OPTIONS;

BEGIN 

   if I_lang is null then
      O_error_message := SQL_LIB.CREATE_MSG('LANG_IS_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE; 
   end if; 

   open  C_LANGUAGE; 
   fetch C_LANGUAGE INTO L_dummy;
   if C_LANGUAGE%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LANGUAGE;
      return FALSE; 
   end if; 
   close C_LANGUAGE;

   open  C_LAN_EXIST; 
   fetch C_LAN_EXIST INTO L_lan;
   if L_lan = I_lang then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LAN_EXIST;
      return FALSE; 
   end if; 
   close C_LAN_EXIST;

   if I_translate_table = 'ADDR_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL OR I_TEXT_4 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_4),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_5),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_6),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'ADDR_TL';

      insert into ADDR_TL(
                          ADDR_KEY,
                          ADD_1,
                          ADD_2,
                          ADD_3,
                          CITY,
                          CONTACT_NAME,
                          COUNTY,
                          LANG,
                          CREATE_ID,
                          CREATE_DATETIME,
                          LAST_UPDATE_ID,
                          LAST_UPDATE_DATETIME
                          )
                  values (
                          I_KEY_NUMBER_1,
                          I_TEXT_1,
                          I_TEXT_2,
                          I_TEXT_3,
                          I_TEXT_4,
                          I_TEXT_5,
                          I_TEXT_6,
                          I_lang,
                          LP_USER,
                          sysdate,
                          LP_USER,
                          sysdate
                          );

   end if ; 
   if I_translate_table = 'AREA_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'AREA_TL';

      insert into AREA_TL(
                          AREA,
                          AREA_NAME,
                          LANG,
                          CREATE_ID,
                          CREATE_DATETIME,
                          LAST_UPDATE_ID,
                          LAST_UPDATE_DATETIME
                          )
                  values (
                          I_KEY_NUMBER_1,
                          I_TEXT_1,
                          I_lang,
                          LP_USER,
                          sysdate,
                          LP_USER,
                          sysdate
                          );

   end if ; 
   if I_translate_table = 'CHAIN_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'CHAIN_TL';

      insert into CHAIN_TL(
                           CHAIN,
                           CHAIN_NAME,
                           LANG,
                           CREATE_ID,
                           CREATE_DATETIME,
                           LAST_UPDATE_ID,
                           LAST_UPDATE_DATETIME
                           )
                   values (
                           I_KEY_NUMBER_1,
                           I_TEXT_1,
                           I_lang,
                           LP_USER,
                           sysdate,
                           LP_USER,
                           sysdate
                           );

   end if ; 
   if I_translate_table = 'CLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'CLASS_TL';

      insert into CLASS_TL(
                           DEPT,
                           CLASS,
                           CLASS_NAME,
                           LANG,
                           CREATE_ID,
                           CREATE_DATETIME,
                           LAST_UPDATE_ID,
                           LAST_UPDATE_DATETIME
                           )
                   values (
                           I_KEY_NUMBER_1,
                           I_KEY_NUMBER_2,
                           I_TEXT_1,
                           I_lang,
                           LP_USER,
                           sysdate,
                           LP_USER,
                           sysdate
                           );

   end if ; 
   if I_translate_table = 'COST_ZONE_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'COST_ZONE_GROUP_TL';

      insert into COST_ZONE_GROUP_TL(
                                     ZONE_GROUP_ID,
                                     DESCRIPTION,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_NUMBER_1,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'COST_ZONE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'COST_ZONE_TL';

      insert into COST_ZONE_TL(
                               ZONE_GROUP_ID,
                               ZONE_ID,
                               DESCRIPTION,
                               LANG,
                               CREATE_ID,
                               CREATE_DATETIME,
                               LAST_UPDATE_ID,
                               LAST_UPDATE_DATETIME
                               )
                       values (
                               I_KEY_NUMBER_1,
                               I_KEY_NUMBER_2,
                               I_TEXT_1,
                               I_lang,
                               LP_USER,
                               sysdate,
                               LP_USER,
                               sysdate
                               );

   end if ; 
   if I_translate_table = 'DEPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'DEPS_TL';

      insert into DEPS_TL(
                          DEPT,
                          DEPT_NAME,
                          LANG,
                          CREATE_ID,
                          CREATE_DATETIME,
                          LAST_UPDATE_ID,
                          LAST_UPDATE_DATETIME
                          )
                  values (
                          I_KEY_NUMBER_1,
                          I_TEXT_1,
                          I_lang,
                          LP_USER,
                          sysdate,
                          LP_USER,
                          sysdate
                          );

   end if ; 
   if I_translate_table = 'DIFF_GROUP_HEAD_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'DIFF_GROUP_HEAD_TL';

      insert into DIFF_GROUP_HEAD_TL(
                                     DIFF_GROUP_ID,
                                     DIFF_GROUP_DESC,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_VARCHAR2_5,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'DIFF_RANGE_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'DIFF_RANGE_HEAD_TL';

      insert into DIFF_RANGE_HEAD_TL(
                                     DIFF_RANGE,
                                     DIFF_RANGE_DESC,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_NUMBER_1,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'DIFF_RATIO_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'DIFF_RATIO_HEAD_TL';

      insert into DIFF_RATIO_HEAD_TL(
                                     DIFF_RATIO_ID,
                                     DESCRIPTION,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_NUMBER_1,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'DISTRICT_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'DISTRICT_TL';

      insert into DISTRICT_TL(
                              DISTRICT,
                              DISTRICT_NAME,
                              LANG,
                              CREATE_ID,
                              CREATE_DATETIME,
                              LAST_UPDATE_ID,
                              LAST_UPDATE_DATETIME
                              )
                      values (
                              I_KEY_NUMBER_1,
                              I_TEXT_1,
                              I_lang,
                              LP_USER,
                              sysdate,
                              LP_USER,
                              sysdate
                              );

   end if ; 
   if I_translate_table = 'DIVISION_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'DIVISION_TL';

      insert into DIVISION_TL(
                              DIVISION,
                              DIV_NAME,
                              LANG,
                              CREATE_ID,
                              CREATE_DATETIME,
                              LAST_UPDATE_ID,
                              LAST_UPDATE_DATETIME
                              )
                      values (
                              I_KEY_NUMBER_1,
                              I_TEXT_1,
                              I_lang,
                              LP_USER,
                              sysdate,
                              LP_USER,
                              sysdate
                              );

   end if ; 
   if I_translate_table = 'GROUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'GROUPS_TL';

      insert into GROUPS_TL(
                            GROUP_NO,
                            GROUP_NAME,
                            LANG,
                            CREATE_ID,
                            CREATE_DATETIME,
                            LAST_UPDATE_ID,
                            LAST_UPDATE_DATETIME
                            )
                    values (
                            I_KEY_NUMBER_1,
                            I_TEXT_1,
                            I_lang,
                            LP_USER,
                            sysdate,
                            LP_USER,
                            sysdate
                            );

   end if ; 
   if I_translate_table = 'ITEM_IMAGE_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 40 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'ITEM_IMAGE_TL';

      insert into ITEM_IMAGE_TL(
                                ITEM,
                                IMAGE_NAME,
                                IMAGE_DESC,
                                LANG,
                                CREATE_ID,
                                CREATE_DATETIME,
                                LAST_UPDATE_ID,
                                LAST_UPDATE_DATETIME
                                )
                        values (
                                I_KEY_VARCHAR2_5,
                                I_KEY_VARCHAR2_6,
                                I_TEXT_1,
                                I_lang,
                                LP_USER,
                                sysdate,
                                LP_USER,
                                sysdate
                                );

   end if ; 
   if I_translate_table = 'ITEM_MASTER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_TEXT_1 IS NULL OR I_TEXT_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 250 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'ITEM_MASTER_TL';

      insert into ITEM_MASTER_TL(
                                 ITEM,
                                 ITEM_DESC,
                                 ITEM_DESC_SECONDARY,
                                 SHORT_DESC,
                                 LANG,
                                 CREATE_ID,
                                 CREATE_DATETIME,
                                 LAST_UPDATE_ID,
                                 LAST_UPDATE_DATETIME
                                 )
                         values (
                                 I_KEY_VARCHAR2_5,
                                 I_TEXT_1,
                                 I_TEXT_2,
                                 I_TEXT_3,
                                 I_lang,
                                 LP_USER,
                                 sysdate,
                                 LP_USER,
                                 sysdate
                                 );

   end if ; 
   if I_translate_table = 'ITEM_SUPPLIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 15 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_4),LP_DUMMY) > 120 OR NVL(LENGTH(I_TEXT_5),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'ITEM_SUPPLIER_TL';

      insert into ITEM_SUPPLIER_TL(
                                   ITEM,
                                   SUPPLIER,
                                   SUPP_LABEL,
                                   SUPP_DIFF_1,
                                   SUPP_DIFF_2,
                                   SUPP_DIFF_3,
                                   SUPP_DIFF_4,
                                   LANG,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME
                                   )
                           values (
                                   I_KEY_VARCHAR2_5,
                                   I_KEY_NUMBER_1,
                                   I_TEXT_1,
                                   I_TEXT_2,
                                   I_TEXT_3,
                                   I_TEXT_4,
                                   I_TEXT_5,
                                   I_lang,
                                   LP_USER,
                                   sysdate,
                                   LP_USER,
                                   sysdate
                                   );

   end if ; 
   if I_translate_table = 'ITEM_XFORM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'ITEM_XFORM_HEAD_TL';

      insert into ITEM_XFORM_HEAD_TL(
                                     ITEM_XFORM_HEAD_ID,
                                     ITEM_XFORM_DESC,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_NUMBER_1,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'LOC_LIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'LOC_LIST_HEAD_TL';

      insert into LOC_LIST_HEAD_TL(
                                   LOC_LIST,
                                   LOC_LIST_DESC,
                                   LANG,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME
                                   )
                           values (
                                   I_KEY_NUMBER_1,
                                   I_TEXT_1,
                                   I_lang,
                                   LP_USER,
                                   sysdate,
                                   LP_USER,
                                   sysdate
                                   );

   end if ; 
   if I_translate_table = 'PACK_TMPL_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'PACK_TMPL_HEAD_TL';

      insert into PACK_TMPL_HEAD_TL(
                                    PACK_TMPL_ID,
                                    PACK_TMPL_DESC,
                                    LANG,
                                    CREATE_ID,
                                    CREATE_DATETIME,
                                    LAST_UPDATE_ID,
                                    LAST_UPDATE_DATETIME
                                    )
                            values (
                                    I_KEY_NUMBER_1,
                                    I_TEXT_1,
                                    I_lang,
                                    LP_USER,
                                    sysdate,
                                    LP_USER,
                                    sysdate
                                    );

   end if ; 
   if I_translate_table = 'PARTNER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 240 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'PARTNER_TL';

      insert into PARTNER_TL(
                             PARTNER_TYPE,
                             PARTNER_ID,
                             PARTNER_DESC,
                             PARTNER_NAME_SECONDARY,
                             LANG,
                             CREATE_ID,
                             CREATE_DATETIME,
                             LAST_UPDATE_ID,
                             LAST_UPDATE_DATETIME
                             )
                     values (
                             I_KEY_VARCHAR2_5,
                             I_KEY_VARCHAR2_6,
                             I_TEXT_1,
                             I_TEXT_2,
                             I_lang,
                             LP_USER,
                             sysdate,
                             LP_USER,
                             sysdate
                             );

   end if ; 
   if I_translate_table = 'PEND_MERCH_HIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL  or (I_KEY_VARCHAR2_5 != 'V' and I_KEY_NUMBER_2 is NULL) or (I_KEY_VARCHAR2_5 = 'S' and I_KEY_NUMBER_3 is NULL)  OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'PEND_MERCH_HIER_TL';

      insert into PEND_MERCH_HIER_TL(
                                     HIER_TYPE,
                                     MERCH_HIER_ID,
                                     MERCH_HIER_PARENT_ID,
                                     MERCH_HIER_GRANDPARENT_ID,
                                     MERCH_HIER_NAME,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_VARCHAR2_5,
                                     I_KEY_NUMBER_1,
                                     I_KEY_NUMBER_2,
                                     I_KEY_NUMBER_3,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'POS_COUPON_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'POS_COUPON_HEAD_TL';

      insert into POS_COUPON_HEAD_TL(
                                     COUPON_ID,
                                     COUPON_DESC,
                                     LANG,
                                     CREATE_ID,
                                     CREATE_DATETIME,
                                     LAST_UPDATE_ID,
                                     LAST_UPDATE_DATETIME
                                     )
                             values (
                                     I_KEY_NUMBER_1,
                                     I_TEXT_1,
                                     I_lang,
                                     LP_USER,
                                     sysdate,
                                     LP_USER,
                                     sysdate
                                     );

   end if ; 
   if I_translate_table = 'PRIORITY_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 100 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'PRIORITY_GROUP_TL';

      insert into PRIORITY_GROUP_TL(
                                    PRIORITY_GROUP_ID,
                                    PRIORITY_GROUP_DESC,
                                    LANG,
                                    CREATE_ID,
                                    CREATE_DATETIME,
                                    LAST_UPDATE_ID,
                                    LAST_UPDATE_DATETIME
                                    )
                            values (
                                    I_KEY_NUMBER_1,
                                    I_TEXT_1,
                                    I_lang,
                                    LP_USER,
                                    sysdate,
                                    LP_USER,
                                    sysdate
                                    );

   end if ; 
   if I_translate_table = 'RECLASS_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'RECLASS_HEAD_TL';

      insert into RECLASS_HEAD_TL(
                                  RECLASS_NO,
                                  RECLASS_DESC,
                                  LANG,
                                  CREATE_ID,
                                  CREATE_DATETIME,
                                  LAST_UPDATE_ID,
                                  LAST_UPDATE_DATETIME
                                  )
                          values (
                                  I_KEY_NUMBER_1,
                                  I_TEXT_1,
                                  I_lang,
                                  LP_USER,
                                  sysdate,
                                  LP_USER,
                                  sysdate
                                  );

   end if ; 
   if I_translate_table = 'REGION_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'REGION_TL';

      insert into REGION_TL(
                            REGION,
                            REGION_NAME,
                            LANG,
                            CREATE_ID,
                            CREATE_DATETIME,
                            LAST_UPDATE_ID,
                            LAST_UPDATE_DATETIME
                            )
                    values (
                            I_KEY_NUMBER_1,
                            I_TEXT_1,
                            I_lang,
                            LP_USER,
                            sysdate,
                            LP_USER,
                            sysdate
                            );

   end if ; 
   if I_translate_table = 'RELATED_ITEM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 255 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'RELATED_ITEM_HEAD_TL';

      insert into RELATED_ITEM_HEAD_TL(
                                       RELATIONSHIP_ID,
                                       RELATIONSHIP_NAME,
                                       LANG,
                                       CREATE_ID,
                                       CREATE_DATETIME,
                                       LAST_UPDATE_ID,
                                       LAST_UPDATE_DATETIME
                                       )
                               values (
                                       I_KEY_NUMBER_1,
                                       I_TEXT_1,
                                       I_lang,
                                       LP_USER,
                                       sysdate,
                                       LP_USER,
                                       sysdate
                                       );

   end if ; 
   if I_translate_table = 'SKULIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'SKULIST_HEAD_TL';

      insert into SKULIST_HEAD_TL(
                                  SKULIST,
                                  SKULIST_DESC,
                                  LANG,
                                  CREATE_ID,
                                  CREATE_DATETIME,
                                  LAST_UPDATE_ID,
                                  LAST_UPDATE_DATETIME
                                  )
                          values (
                                  I_KEY_NUMBER_1,
                                  I_TEXT_1,
                                  I_lang,
                                  LP_USER,
                                  sysdate,
                                  LP_USER,
                                  sysdate
                                  );

   end if ; 
   if I_translate_table = 'STORE_ADD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'STORE_ADD_TL';

      insert into STORE_ADD_TL(
                               STORE,
                               STORE_NAME,
                               STORE_NAME_SECONDARY,
                               LANG,
                               CREATE_ID,
                               CREATE_DATETIME,
                               LAST_UPDATE_ID,
                               LAST_UPDATE_DATETIME
                               )
                       values (
                               I_KEY_NUMBER_1,
                               I_TEXT_1,
                               I_TEXT_2,
                               I_lang,
                               LP_USER,
                               sysdate,
                               LP_USER,
                               sysdate
                               );

   end if ; 
   if I_translate_table = 'STORE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'STORE_TL';

      insert into STORE_TL(
                           STORE,
                           STORE_NAME,
                           STORE_NAME_SECONDARY,
                           LANG,
                           CREATE_ID,
                           CREATE_DATETIME,
                           LAST_UPDATE_ID,
                           LAST_UPDATE_DATETIME
                           )
                   values (
                           I_KEY_NUMBER_1,
                           I_TEXT_1,
                           I_TEXT_2,
                           I_lang,
                           LP_USER,
                           sysdate,
                           LP_USER,
                           sysdate
                           );

   end if ; 
   if I_translate_table = 'SUBCLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_KEY_NUMBER_3 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'SUBCLASS_TL';

      insert into SUBCLASS_TL(
                              DEPT,
                              CLASS,
                              SUBCLASS,
                              SUB_NAME,
                              LANG,
                              CREATE_ID,
                              CREATE_DATETIME,
                              LAST_UPDATE_ID,
                              LAST_UPDATE_DATETIME
                              )
                      values (
                              I_KEY_NUMBER_1,
                              I_KEY_NUMBER_2,
                              I_KEY_NUMBER_3,
                              I_TEXT_1,
                              I_lang,
                              LP_USER,
                              sysdate,
                              LP_USER,
                              sysdate
                              );

   end if ; 
   if I_translate_table = 'SUPS_PACK_TMPL_DESC_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'SUPS_PACK_TMPL_DESC_TL';

      insert into SUPS_PACK_TMPL_DESC_TL(
                                         SUPPLIER,
                                         PACK_TMPL_ID,
                                         SUPP_PACK_DESC,
                                         LANG,
                                         CREATE_ID,
                                         CREATE_DATETIME,
                                         LAST_UPDATE_ID,
                                         LAST_UPDATE_DATETIME
                                         )
                                 values (
                                         I_KEY_NUMBER_1,
                                         I_KEY_NUMBER_2,
                                         I_TEXT_1,
                                         I_lang,
                                         LP_USER,
                                         sysdate,
                                         LP_USER,
                                         sysdate
                                         );

   end if ; 
   if I_translate_table = 'SUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL OR I_TEXT_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 240 OR NVL(LENGTH(I_TEXT_3),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'SUPS_TL';

      insert into SUPS_TL(
                          SUPPLIER,
                          SUP_NAME,
                          SUP_NAME_SECONDARY,
                          CONTACT_NAME,
                          LANG,
                          CREATE_ID,
                          CREATE_DATETIME,
                          LAST_UPDATE_ID,
                          LAST_UPDATE_DATETIME
                          )
                  values (
                          I_KEY_NUMBER_1,
                          I_TEXT_1,
                          I_TEXT_2,
                          I_TEXT_3,
                          I_lang,
                          LP_USER,
                          sysdate,
                          LP_USER,
                          sysdate
                          );

   end if ; 
   if I_translate_table = 'UDA_ITEM_FF_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'UDA_ITEM_FF_TL';

      insert into UDA_ITEM_FF_TL(
                                 ITEM,
                                 UDA_ID,
                                 UDA_TEXT,
                                 UDA_TEXT_DESC,
                                 LANG,
                                 CREATE_ID,
                                 CREATE_DATETIME,
                                 LAST_UPDATE_ID,
                                 LAST_UPDATE_DATETIME
                                 )
                         values (
                                 I_KEY_VARCHAR2_5,
                                 I_KEY_NUMBER_1,
                                 I_KEY_VARCHAR2_6,
                                 I_TEXT_1,
                                 I_lang,
                                 LP_USER,
                                 sysdate,
                                 LP_USER,
                                 sysdate
                                 );

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_DTL_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_5 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 250 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'WF_COST_BUILDUP_TMPL_DTL_TL';

      insert into WF_COST_BUILDUP_TMPL_DTL_TL(
                                              TEMPL_ID,
                                              COST_COMP_ID,
                                              DESCRIPTION,
                                              LANG,
                                              CREATE_ID,
                                              CREATE_DATETIME,
                                              LAST_UPDATE_ID,
                                              LAST_UPDATE_DATETIME
                                              )
                                      values (
                                              I_KEY_NUMBER_1,
                                              I_KEY_VARCHAR2_5,
                                              I_TEXT_1,
                                              I_lang,
                                              LP_USER,
                                              sysdate,
                                              LP_USER,
                                              sysdate
                                              );

   end if ; 
   if I_translate_table = 'WF_COST_BUILDUP_TMPL_HD_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 120 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'WF_COST_BUILDUP_TMPL_HD_TL';

      insert into WF_COST_BUILDUP_TMPL_HD_TL(
                                             TEMPL_ID,
                                             TEMPL_DESC,
                                             LANG,
                                             CREATE_ID,
                                             CREATE_DATETIME,
                                             LAST_UPDATE_ID,
                                             LAST_UPDATE_DATETIME
                                             )
                                     values (
                                             I_KEY_NUMBER_1,
                                             I_TEXT_1,
                                             I_lang,
                                             LP_USER,
                                             sysdate,
                                             LP_USER,
                                             sysdate
                                             );

   end if ; 
   if I_translate_table = 'WH_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_TEXT_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      if NVL(LENGTH(I_TEXT_1),LP_DUMMY) > 150 OR NVL(LENGTH(I_TEXT_2),LP_DUMMY) > 150 then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_TO_LONG',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE; 
      end if; 

      L_tab_name := 'WH_TL';

      insert into WH_TL(
                        WH,
                        WH_NAME,
                        WH_NAME_SECONDARY,
                        LANG,
                        CREATE_ID,
                        CREATE_DATETIME,
                        LAST_UPDATE_ID,
                        LAST_UPDATE_DATETIME
                        )
                values (
                        I_KEY_NUMBER_1,
                        I_TEXT_1,
                        I_TEXT_2,
                        I_lang,
                        LP_USER,
                        sysdate,
                        LP_USER,
                        sysdate
                        );

   end if ; 

return TRUE ;

EXCEPTION 
   when DUP_VAL then 
   if C_LANGUAGE%ISOPEN then
      close C_LANGUAGE;
   end if;
   if C_LAN_EXIST%ISOPEN then
      close C_LAN_EXIST;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('DUPLICATE_RECORD',
                                         L_tab_name,
                                         NULL,
                                         NULL); 
return FALSE; 
   when PARENT_NOTFOUND then 
   if C_LANGUAGE%ISOPEN then
      close C_LANGUAGE;
   end if;
   if C_LAN_EXIST%ISOPEN then
      close C_LAN_EXIST;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('NO_PARENT_RECORD',
                                         L_tab_name,
                                         NULL,
                                         NULL); 
return FALSE; 
   when OTHERS then 
   if C_LANGUAGE%ISOPEN then
      close C_LANGUAGE;
   end if;
   if C_LAN_EXIST%ISOPEN then
      close C_LAN_EXIST;
   end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE)); 
return FALSE; 

END INSERT_TL_ROW;


----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TL_ROWS(
                     O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_translate_table   IN      VARCHAR2,
                     I_lang              IN      NUMBER,
                     I_key_number_1      IN      NUMBER,
                     I_key_number_2      IN      NUMBER,
                     I_key_number_3      IN      NUMBER,
                     I_key_number_4      IN      NUMBER,
                     I_key_varchar2_5    IN      VARCHAR2,
                     I_key_varchar2_6    IN      VARCHAR2,
                     I_key_varchar2_7    IN      VARCHAR2,
                     I_key_varchar2_8    IN      VARCHAR2,
                     I_key_varchar2_9    IN      VARCHAR2,
                     I_key_date_10       IN      DATE,
                     I_key_date_11       IN      DATE
                     )
RETURN SYS_REFCURSOR IS 
   O_cursor              SYS_REFCURSOR;
   L_primary_lang        LANG.LANG%TYPE;
   L_program             VARCHAR2(100) := 'TRANSLATION_SQL.GET_TL_ROWS';

   cursor C_LAN_EXIST is
      select DATA_INTEGRATION_LANG
        from SYSTEM_CONFIG_OPTIONS;

BEGIN 

--If input languange is provide fetch rows for the input language if it is null fetch all languages
--other than the primary language 

   open  C_LAN_EXIST; 
   fetch C_LAN_EXIST INTO L_primary_lang;
   if C_LAN_EXIST%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('LANG_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_LAN_EXIST;
      return NULL; 
   end if; 
   close C_LAN_EXIST;

   if I_translate_table = 'ADDR_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'ADDR_TL',
                                               'KEY_NUMBER_1,TEXT_1,TEXT_2,TEXT_3,TEXT_4,TEXT_5,TEXT_6',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,ADDR_KEY KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,ADD_1 TEXT_1
                               ,ADD_2 TEXT_2
                               ,ADD_3 TEXT_3
                               ,CITY TEXT_4
                               ,CONTACT_NAME TEXT_5
                               ,COUNTY TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from ADDR_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ADDR_KEY = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'AREA_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'AREA_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,AREA KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,AREA_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from AREA_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and AREA = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'CHAIN_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'CHAIN_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,CHAIN KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,CHAIN_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from CHAIN_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and CHAIN = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'CLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'CLASS_TL',
                                               'KEY_NUMBER_1,KEY_NUMBER_2,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DEPT KEY_NUMBER_1
                               ,CLASS KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,CLASS_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from CLASS_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DEPT = I_KEY_NUMBER_1
                           and CLASS = I_KEY_NUMBER_2;

      return O_cursor;

   end if;

   if I_translate_table = 'COST_ZONE_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'COST_ZONE_GROUP_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,ZONE_GROUP_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DESCRIPTION TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from COST_ZONE_GROUP_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ZONE_GROUP_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'COST_ZONE_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'COST_ZONE_TL',
                                               'KEY_NUMBER_1,KEY_NUMBER_2,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,ZONE_GROUP_ID KEY_NUMBER_1
                               ,ZONE_ID KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DESCRIPTION TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from COST_ZONE_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ZONE_GROUP_ID = I_KEY_NUMBER_1
                           and ZONE_ID = I_KEY_NUMBER_2;

      return O_cursor;

   end if;

   if I_translate_table = 'DEPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'DEPS_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DEPT KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DEPT_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from DEPS_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DEPT = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'DIFF_GROUP_HEAD_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'DIFF_GROUP_HEAD_TL',
                                               'KEY_VARCHAR2_5,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,NULL KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,DIFF_GROUP_ID KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DIFF_GROUP_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from DIFF_GROUP_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DIFF_GROUP_ID = I_KEY_VARCHAR2_5;

      return O_cursor;

   end if;

   if I_translate_table = 'DIFF_RANGE_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'DIFF_RANGE_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DIFF_RANGE KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DIFF_RANGE_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from DIFF_RANGE_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DIFF_RANGE = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'DIFF_RATIO_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'DIFF_RATIO_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DIFF_RATIO_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DESCRIPTION TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from DIFF_RATIO_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DIFF_RATIO_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'DISTRICT_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'DISTRICT_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DISTRICT KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DISTRICT_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from DISTRICT_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DISTRICT = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'DIVISION_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'DIVISION_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DIVISION KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DIV_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from DIVISION_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DIVISION = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'GROUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'GROUPS_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,GROUP_NO KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,GROUP_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from GROUPS_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and GROUP_NO = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'ITEM_IMAGE_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'ITEM_IMAGE_TL',
                                               'KEY_VARCHAR2_5,KEY_VARCHAR2_6,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,NULL KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,ITEM KEY_VARCHAR2_5
                               ,IMAGE_NAME KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,IMAGE_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from ITEM_IMAGE_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ITEM = I_KEY_VARCHAR2_5
                           and IMAGE_NAME = I_KEY_VARCHAR2_6;

      return O_cursor;

   end if;

   if I_translate_table = 'ITEM_MASTER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'ITEM_MASTER_TL',
                                               'KEY_VARCHAR2_5,TEXT_1,TEXT_2,TEXT_3',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,NULL KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,ITEM KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,ITEM_DESC TEXT_1
                               ,ITEM_DESC_SECONDARY TEXT_2
                               ,SHORT_DESC TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from ITEM_MASTER_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ITEM = I_KEY_VARCHAR2_5;

      return O_cursor;

   end if;

   if I_translate_table = 'ITEM_SUPPLIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'ITEM_SUPPLIER_TL',
                                               'KEY_VARCHAR2_5,KEY_NUMBER_1,TEXT_1,TEXT_2,TEXT_3,TEXT_4,TEXT_5',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,SUPPLIER KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,ITEM KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,SUPP_LABEL TEXT_1
                               ,SUPP_DIFF_1 TEXT_2
                               ,SUPP_DIFF_2 TEXT_3
                               ,SUPP_DIFF_3 TEXT_4
                               ,SUPP_DIFF_4 TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from ITEM_SUPPLIER_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ITEM = I_KEY_VARCHAR2_5
                           and SUPPLIER = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'ITEM_XFORM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'ITEM_XFORM_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,ITEM_XFORM_HEAD_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,ITEM_XFORM_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from ITEM_XFORM_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ITEM_XFORM_HEAD_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'LOC_LIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'LOC_LIST_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,LOC_LIST KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,LOC_LIST_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from LOC_LIST_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and LOC_LIST = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'PACK_TMPL_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'PACK_TMPL_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,PACK_TMPL_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,PACK_TMPL_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from PACK_TMPL_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and PACK_TMPL_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'PARTNER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'PARTNER_TL',
                                               'KEY_VARCHAR2_5,KEY_VARCHAR2_6,TEXT_1,TEXT_2',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,NULL KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,PARTNER_TYPE KEY_VARCHAR2_5
                               ,PARTNER_ID KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,PARTNER_DESC TEXT_1
                               ,PARTNER_NAME_SECONDARY TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from PARTNER_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and PARTNER_TYPE = I_KEY_VARCHAR2_5
                           and PARTNER_ID = I_KEY_VARCHAR2_6;

      return O_cursor;

   end if;

   if I_translate_table = 'PEND_MERCH_HIER_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL  or (I_KEY_VARCHAR2_5 != 'V' and I_KEY_NUMBER_2 is NULL) or (I_KEY_VARCHAR2_5 = 'S' and I_KEY_NUMBER_3 is NULL)  then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'PEND_MERCH_HIER_TL',
                                               'KEY_VARCHAR2_5,KEY_NUMBER_1,KEY_NUMBER_2,KEY_NUMBER_3,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,MERCH_HIER_ID KEY_NUMBER_1
                               ,MERCH_HIER_PARENT_ID KEY_NUMBER_2
                               ,MERCH_HIER_GRANDPARENT_ID KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,HIER_TYPE KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,MERCH_HIER_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from PEND_MERCH_HIER_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and MERCH_HIER_ID = I_KEY_NUMBER_1
                           and NVL(MERCH_HIER_PARENT_ID,-1) = NVL(I_KEY_NUMBER_2,-1)
                           and NVL(MERCH_HIER_GRANDPARENT_ID,-1) = NVL(I_KEY_NUMBER_3,-1)
                           and HIER_TYPE = I_KEY_VARCHAR2_5;

      return O_cursor;

   end if;

   if I_translate_table = 'POS_COUPON_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'POS_COUPON_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,COUPON_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,COUPON_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from POS_COUPON_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and COUPON_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'PRIORITY_GROUP_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'PRIORITY_GROUP_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,PRIORITY_GROUP_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,PRIORITY_GROUP_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from PRIORITY_GROUP_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and PRIORITY_GROUP_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'RECLASS_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'RECLASS_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,RECLASS_NO KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,RECLASS_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from RECLASS_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and RECLASS_NO = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'REGION_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'REGION_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,REGION KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,REGION_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from REGION_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and REGION = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'RELATED_ITEM_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'RELATED_ITEM_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,RELATIONSHIP_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,RELATIONSHIP_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from RELATED_ITEM_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and RELATIONSHIP_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'SKULIST_HEAD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'SKULIST_HEAD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,SKULIST KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,SKULIST_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from SKULIST_HEAD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and SKULIST = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'STORE_ADD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'STORE_ADD_TL',
                                               'KEY_NUMBER_1,TEXT_1,TEXT_2',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,STORE KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,STORE_NAME TEXT_1
                               ,STORE_NAME_SECONDARY TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from STORE_ADD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and STORE = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'STORE_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'STORE_TL',
                                               'KEY_NUMBER_1,TEXT_1,TEXT_2',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,STORE KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,STORE_NAME TEXT_1
                               ,STORE_NAME_SECONDARY TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from STORE_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and STORE = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'SUBCLASS_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL OR I_KEY_NUMBER_3 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'SUBCLASS_TL',
                                               'KEY_NUMBER_1,KEY_NUMBER_2,KEY_NUMBER_3,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,DEPT KEY_NUMBER_1
                               ,CLASS KEY_NUMBER_2
                               ,SUBCLASS KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,SUB_NAME TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from SUBCLASS_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and DEPT = I_KEY_NUMBER_1
                           and CLASS = I_KEY_NUMBER_2
                           and SUBCLASS = I_KEY_NUMBER_3;

      return O_cursor;

   end if;

   if I_translate_table = 'SUPS_PACK_TMPL_DESC_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_NUMBER_2 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'SUPS_PACK_TMPL_DESC_TL',
                                               'KEY_NUMBER_1,KEY_NUMBER_2,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,SUPPLIER KEY_NUMBER_1
                               ,PACK_TMPL_ID KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,SUPP_PACK_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from SUPS_PACK_TMPL_DESC_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and SUPPLIER = I_KEY_NUMBER_1
                           and PACK_TMPL_ID = I_KEY_NUMBER_2;

      return O_cursor;

   end if;

   if I_translate_table = 'SUPS_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'SUPS_TL',
                                               'KEY_NUMBER_1,TEXT_1,TEXT_2,TEXT_3',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,SUPPLIER KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,SUP_NAME TEXT_1
                               ,SUP_NAME_SECONDARY TEXT_2
                               ,CONTACT_NAME TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from SUPS_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and SUPPLIER = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'UDA_ITEM_FF_TL' then 
      if I_KEY_VARCHAR2_5 IS NULL OR I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_6 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'UDA_ITEM_FF_TL',
                                               'KEY_VARCHAR2_5,KEY_NUMBER_1,KEY_VARCHAR2_6,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,UDA_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,ITEM KEY_VARCHAR2_5
                               ,UDA_TEXT KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,UDA_TEXT_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from UDA_ITEM_FF_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and ITEM = I_KEY_VARCHAR2_5
                           and UDA_ID = I_KEY_NUMBER_1
                           and UDA_TEXT = I_KEY_VARCHAR2_6;

      return O_cursor;

   end if;

   if I_translate_table = 'WF_COST_BUILDUP_TMPL_DTL_TL' then 
      if I_KEY_NUMBER_1 IS NULL OR I_KEY_VARCHAR2_5 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'WF_COST_BUILDUP_TMPL_DTL_TL',
                                               'KEY_NUMBER_1,KEY_VARCHAR2_5,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,TEMPL_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,COST_COMP_ID KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,DESCRIPTION TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from WF_COST_BUILDUP_TMPL_DTL_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and TEMPL_ID = I_KEY_NUMBER_1
                           and COST_COMP_ID = I_KEY_VARCHAR2_5;

      return O_cursor;

   end if;

   if I_translate_table = 'WF_COST_BUILDUP_TMPL_HD_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'WF_COST_BUILDUP_TMPL_HD_TL',
                                               'KEY_NUMBER_1,TEXT_1',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,TEMPL_ID KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,TEMPL_DESC TEXT_1
                               ,NULL TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from WF_COST_BUILDUP_TMPL_HD_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and TEMPL_ID = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

   if I_translate_table = 'WH_TL' then 
      if I_KEY_NUMBER_1 IS NULL then 
         O_error_message := SQL_LIB.CREATE_MSG('TL_INV_INPUT_PARAMETER',
                                               'WH_TL',
                                               'KEY_NUMBER_1,TEXT_1,TEXT_2',
                                               NULL);
         return NULL; 
      end if; 

      open O_cursor for select I_translate_table TRANSLATE_TABLE
                               ,lang LANG
                               ,WH KEY_NUMBER_1
                               ,NULL KEY_NUMBER_2
                               ,NULL KEY_NUMBER_3
                               ,NULL KEY_NUMBER_4
                               ,NULL KEY_VARCHAR2_5
                               ,NULL KEY_VARCHAR2_6
                               ,NULL KEY_VARCHAR2_7
                               ,NULL KEY_VARCHAR2_8
                               ,NULL KEY_VARCHAR2_9
                               ,NULL KEY_DATE_10
                               ,NULL KEY_DATE_11
                               ,WH_NAME TEXT_1
                               ,WH_NAME_SECONDARY TEXT_2
                               ,NULL TEXT_3
                               ,NULL TEXT_4
                               ,NULL TEXT_5
                               ,NULL TEXT_6
                               ,NULL TEXT_7
                               ,NULL TEXT_8
                               ,NULL TEXT_9
                          from WH_TL
                         where Lang = NVL(I_lang,Lang)
                           and Lang <> L_primary_lang
                           and WH = I_KEY_NUMBER_1;

      return O_cursor;

   end if;

return NULL;

EXCEPTION 
   when OTHERS then 
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE)); 
   return NULL; 

END GET_TL_ROWS;

END TRANSLATION_SQL;
/
