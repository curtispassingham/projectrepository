CREATE OR REPLACE PACKAGE BODY DYNAMIC_HIER_TOKEN_SQL AS

FUNCTION GET_MAPPING_STRING(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_token_mapping_string IN OUT VARCHAR2,
                            I_lang                 IN     LANG.LANG%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'DYNAMIC_HIER_TOKEN_SQL.GET_MAPPING_STRING';
   ---
   cursor C_GET_TOKEN_MAPPING is
      select listagg(token || '=' || NVL(client_name,rms_name),',') WITHIN GROUP (order by token) 
        from dynamic_hier_token_map,
             system_config_options so
       where I_lang = so.data_integration_lang
      union
      select listagg(token || '=' || NVL(client_name,rms_name),',') WITHIN GROUP (order by token) 
        from dynamic_hier_token_map_tl,
             system_config_options so
       where I_lang <> so.data_integration_lang
         and lang = I_lang;
   
BEGIN
   ---
   open C_GET_TOKEN_MAPPING;
   fetch C_GET_TOKEN_MAPPING into O_token_mapping_string;
   close C_GET_TOKEN_MAPPING;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_MAPPING_STRING;
---------------------------------------------------------------------------
FUNCTION REPLACE_RTK_ERRORS_TOKENS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program               VARCHAR2(50) := 'DYNAMIC_HIER_TOKEN_SQL.REPLACE_RTK_ERRORS_TOKENS';
   L_lang                  LANG.LANG%TYPE                         := null;
   L_token                 DYNAMIC_HIER_TOKEN_MAP.TOKEN%TYPE      := null;
   L_string                DYNAMIC_HIER_TOKEN_MAP.RMS_NAME%TYPE   := null;
   L_table                 VARCHAR2(30);
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   LENGTH_ERROR            EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(LENGTH_ERROR, -12899);   

   cursor C_GET_TOKENS is
      select token,
             NVL(client_name,rms_name) str
        from dynamic_hier_token_map;

   cursor C_LOCK_RTK_ERRORS is
      select rtk_text
        from rtk_errors
       where INSTR(rtk_text,L_token) > 0
       for update nowait;
       
   
   cursor C_GET_TL_TOKENS is
      select lang,
             token,
             NVL(client_name,rms_name) str
        from dynamic_hier_token_map_tl;

   cursor C_LOCK_RTK_ERRORS_TL is
      select rtk_text
        from rtk_errors_tl
       where INSTR(rtk_text,L_token) > 0
         and lang = L_lang
       for update nowait;
    
BEGIN
   
   select data_integration_lang into L_lang from system_config_options;
   
   for rec in C_GET_TOKENS 
   LOOP
      L_token  := rec.token;
      L_string := rec.str;
      L_table  := 'RTK_ERRORS';
      ---
      open C_LOCK_RTK_ERRORS;
      close C_LOCK_RTK_ERRORS;
      ---
      update rtk_errors
         set rtk_text = RTRIM(SUBSTRB(REPLACE(rtk_text, rec.token, rec.str),1,255))
       where INSTR(rtk_text,rec.token) > 0;  
   END LOOP;
   ---
   for rec in C_GET_TL_TOKENS 
   LOOP
      L_token := rec.token;
      L_lang  := rec.lang;
      L_string := rec.str;
      L_table := 'RTK_ERRORS_TL';
      ---
      open C_LOCK_RTK_ERRORS_TL;
      close C_LOCK_RTK_ERRORS_TL;
      ---   
      update rtk_errors_tl
         set rtk_text = RTRIM(SUBSTRB(REPLACE(rtk_text, rec.token, rec.str),1,255))
       where INSTR(rtk_text,rec.token) > 0
         and lang = rec.lang;  
      ---
   END LOOP;
   ---
   return TRUE;
   ---
EXCEPTION
   when LENGTH_ERROR then
      O_error_message := SQL_LIB.CREATE_MSG('DYN_HIER_LENGTH_ERR',
                                             L_table,
                                             L_token || '-' || L_string,
                                             L_lang);
      return FALSE;
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Token - ' || L_token,
                                             'Lang - '  || L_lang);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REPLACE_RTK_ERRORS_TOKENS;
----------------------------------------------------------------------------
FUNCTION REPLACE_CODE_DETAIL_TOKENS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program               VARCHAR2(50) := 'DYNAMIC_HIER_TOKEN_SQL.REPLACE_CODE_DETAIL_TOKENS';
   L_lang                  LANG.LANG%TYPE                      := null;
   L_token                 DYNAMIC_HIER_TOKEN_MAP.TOKEN%TYPE   := null;
   L_string                DYNAMIC_HIER_TOKEN_MAP.RMS_NAME%TYPE   := null;
   L_table                 VARCHAR2(30);
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_locked, -54);
   LENGTH_ERROR            EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(LENGTH_ERROR, -12899);   
   
   cursor C_GET_TOKENS is
      select token,
             NVL(client_name,rms_name) str
        from dynamic_hier_token_map;

   cursor C_LOCK_CODE_DETAIL is
      select code_desc
        from code_detail
       where INSTR(code_desc,L_token) > 0
       for update nowait;

   cursor C_GET_TL_TOKENS is
      select lang,
             token,
             NVL(client_name,rms_name) str
        from dynamic_hier_token_map_tl;

   cursor C_LOCK_CODE_DETAIL_TL is
      select code_desc
        from code_detail_tl
       where INSTR(code_desc,L_token) > 0
         and lang = L_lang
       for update nowait;
    
BEGIN
   
   select data_integration_lang into L_lang from system_config_options;
      
   for rec in C_GET_TOKENS 
   LOOP
      L_token := rec.token;
      L_string := rec.str;
      L_table := 'CODE_DETAIL';
      ---
      open C_LOCK_CODE_DETAIL;
      close C_LOCK_CODE_DETAIL;
      ---
      update code_detail
         set code_desc = RTRIM(SUBSTRB(REPLACE(code_desc, rec.token, rec.str),1,250))
       where INSTR(code_desc,rec.token) > 0;
       ---
   END LOOP;
   ---
   for rec in C_GET_TL_TOKENS 
   LOOP
      L_token := rec.token;
      L_lang  := rec.lang;
      L_string := rec.str;
      L_table := 'CODE_DETAIL_TL';
      ---   
      open C_LOCK_CODE_DETAIL_TL;
      close C_LOCK_CODE_DETAIL_TL;
      ---
      update code_detail_tl
         set code_desc = RTRIM(SUBSTRB(REPLACE(code_desc, rec.token, rec.str),1,250))
       where INSTR(code_desc,rec.token) > 0
         and lang = rec.lang;  
      ---
   END LOOP;
   ---
   return TRUE;
   ---
EXCEPTION
   when LENGTH_ERROR then
      O_error_message := SQL_LIB.CREATE_MSG('DYN_HIER_LENGTH_ERR',
                                             L_table,
                                             L_token || '-' || L_string,
                                             L_lang);
      return FALSE;
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Token - ' || L_token,
                                             'Lang - '  || L_lang);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REPLACE_CODE_DETAIL_TOKENS;
---------------------------------------------------------------------------
FUNCTION REPLACE_TOKENS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program               VARCHAR2(50) := 'DYNAMIC_HIER_TOKEN_SQL.REPLACE_TOKENS';
    
BEGIN

   if DYNAMIC_HIER_TOKEN_SQL.REPLACE_CODE_DETAIL_TOKENS(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if DYNAMIC_HIER_TOKEN_SQL.REPLACE_RTK_ERRORS_TOKENS(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REPLACE_TOKENS;
----------------------------------------------------------------------------
END DYNAMIC_HIER_TOKEN_SQL;
/


