CREATE OR REPLACE PACKAGE API_LIBRARY AUTHID CURRENT_USER AS

WRONG_ORDER  CONSTANT  VARCHAR2(1) := 'O';
FATAL_ERROR  CONSTANT  VARCHAR2(1) := 'E';
TABLE_LOCKED CONSTANT  VARCHAR2(1) := 'L';
XML_ERROR    CONSTANT  VARCHAR2(1) := 'X';
NO_UPD_DONE  CONSTANT  VARCHAR2(1) := 'U';
HOSPITAL     CONSTANT  VARCHAR2(1) := 'H';

TRIGGER_EXCEPTION CONSTANT  NUMBER      := -20001;

-----------------------------------------------------------------------
FUNCTION CONVERT_STRING_TO_DATE(O_error_message   IN OUT VARCHAR2,
                                O_date            IN OUT DATE,
                                I_datestring      IN     VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION CONVERT_DATE_TO_STRING(O_error_message   IN OUT VARCHAR2,
                                O_datestring      IN OUT VARCHAR2,
                                I_date            IN     DATE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION CREATE_MESSAGE(O_status        OUT VARCHAR2,
                        O_text          OUT VARCHAR2,
                        root            IN  OUT xmldom.DOMElement,
                        I_message_name  IN  VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION WRITE_DOCUMENT(O_status        OUT    VARCHAR2,
                        O_text          OUT    VARCHAR2,
                        O_document      OUT    CLOB,
                        root            IN OUT xmldom.DOMElement)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION CREATE_MESSAGE_STR(O_status        OUT VARCHAR2,
                            O_text          OUT VARCHAR2,
                            root            IN  OUT rib_sxw.SXWHandle,
                            I_message_name  IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION WRITE_DOCUMENT_STR(O_status        OUT    VARCHAR2,
                            O_text          OUT    VARCHAR2,
                            O_document      IN OUT nocopy CLOB,
                            root            IN OUT rib_sxw.SXWHandle)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_status              IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  VARCHAR2,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2);
-----------------------------------------------------------------------
FUNCTION readRoot(message     in clob,
                  messageName in varchar2)
RETURN xmldom.DOMElement;
-----------------------------------------------------------------------
PROCEDURE GET_RIB_SETTINGS(O_status_code            IN OUT VARCHAR2,
                           O_error_msg              IN OUT VARCHAR2,
                           O_max_details_to_publish    OUT NUMBER,
                           O_num_threads               OUT NUMBER,
                           O_minutes_time_lag          OUT NUMBER,
                           I_family                 IN     VARCHAR2);
-----------------------------------------------------------------------
FUNCTION INIT(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
END API_LIBRARY;
/
