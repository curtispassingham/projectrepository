CREATE OR REPLACE PACKAGE TRANSLATION_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------------------------------------------------------------
   LP_USER   CONSTANT ITEM_MASTER_TL.CREATE_ID%TYPE := COALESCE(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),SYS_CONTEXT('USERENV', 'CLIENT_INFO'),SYS_CONTEXT('USERENV', 'SESSION_USER'));
   LP_DUMMY  CONSTANT NUMBER(10) := -99999;

----------------------------------------------------------------------------------------------------------------------------------------------------
-- Function Name: GET_TL_ROW
-- Purpose      : Fetch the record from the translation tables based on lang and primary keys
----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TL_ROW(
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_text_1            IN OUT VARCHAR2,
                    O_text_2            IN OUT VARCHAR2,
                    O_text_3            IN OUT VARCHAR2,
                    O_text_4            IN OUT VARCHAR2,
                    O_text_5            IN OUT VARCHAR2,
                    O_text_6            IN OUT VARCHAR2,
                    O_text_7            IN OUT VARCHAR2,
                    O_text_8            IN OUT VARCHAR2,
                    O_text_9            IN OUT VARCHAR2,
                    O_row_exist         IN OUT VARCHAR2,
                    I_translate_table   IN     VARCHAR2,
                    I_lang              IN     NUMBER,
                    I_key_number_1      IN     NUMBER,
                    I_key_number_2      IN     NUMBER,
                    I_key_number_3      IN     NUMBER,
                    I_key_number_4      IN     NUMBER,
                    I_key_varchar2_5    IN     VARCHAR2,
                    I_key_varchar2_6    IN     VARCHAR2,
                    I_key_varchar2_7    IN     VARCHAR2,
                    I_key_varchar2_8    IN     VARCHAR2,
                    I_key_varchar2_9    IN     VARCHAR2,
                    I_key_date_10       IN     DATE,
                    I_key_date_11       IN     DATE
                    )
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_TL_ROW
-- Purpose      : Get lock on translation tables based on lang and primary keys
----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_TL_ROW(
                     O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_translate_table   IN      VARCHAR2,
                     I_lang              IN      NUMBER,
                     I_key_number_1      IN      NUMBER,
                     I_key_number_2      IN      NUMBER,
                     I_key_number_3      IN      NUMBER,
                     I_key_number_4      IN      NUMBER,
                     I_key_varchar2_5    IN      VARCHAR2,
                     I_key_varchar2_6    IN      VARCHAR2,
                     I_key_varchar2_7    IN      VARCHAR2,
                     I_key_varchar2_8    IN      VARCHAR2,
                     I_key_varchar2_9    IN      VARCHAR2,
                     I_key_date_10       IN      DATE,
                     I_key_date_11       IN      DATE
                     )
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_TL_ROW
-- Purpose      : Delete's  translation table record based on lang and primary keys
----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_TL_ROW(
                       O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_translate_table   IN      VARCHAR2,
                       I_lang              IN      NUMBER,
                       I_key_number_1      IN      NUMBER,
                       I_key_number_2      IN      NUMBER,
                       I_key_number_3      IN      NUMBER,
                       I_key_number_4      IN      NUMBER,
                       I_key_varchar2_5    IN      VARCHAR2,
                       I_key_varchar2_6    IN      VARCHAR2,
                       I_key_varchar2_7    IN      VARCHAR2,
                       I_key_varchar2_8    IN      VARCHAR2,
                       I_key_varchar2_9    IN      VARCHAR2,
                       I_key_date_10       IN      DATE,
                       I_key_date_11       IN      DATE
                       )
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------------------------------------------
-- Function Name: UPDATE_TL_ROW
-- Purpose      : Update's  translation table record based on lang and primary keys
----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TL_ROW(
                       O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_translate_table   IN      VARCHAR2,
                       I_lang              IN      NUMBER,
                       I_key_number_1      IN      NUMBER,
                       I_key_number_2      IN      NUMBER,
                       I_key_number_3      IN      NUMBER,
                       I_key_number_4      IN      NUMBER,
                       I_key_varchar2_5    IN      VARCHAR2,
                       I_key_varchar2_6    IN      VARCHAR2,
                       I_key_varchar2_7    IN      VARCHAR2,
                       I_key_varchar2_8    IN      VARCHAR2,
                       I_key_varchar2_9    IN      VARCHAR2,
                       I_key_date_10       IN      DATE,
                       I_key_date_11       IN      DATE,
                       I_text_1            IN      VARCHAR2,
                       I_text_2            IN      VARCHAR2,
                       I_text_3            IN      VARCHAR2,
                       I_text_4            IN      VARCHAR2,
                       I_text_5            IN      VARCHAR2,
                       I_text_6            IN      VARCHAR2,
                       I_text_7            IN      VARCHAR2,
                       I_text_8            IN      VARCHAR2,
                       I_text_9            IN      VARCHAR2
                       )
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------------------------------------------
-- Function Name: INSERT_TL_ROW
-- Purpose      : Insert into  translation tables based on lang and primary keys
----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_TL_ROW(
                       O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_translate_table   IN      VARCHAR2,
                       I_lang              IN      NUMBER,
                       I_key_number_1      IN      NUMBER,
                       I_key_number_2      IN      NUMBER,
                       I_key_number_3      IN      NUMBER,
                       I_key_number_4      IN      NUMBER,
                       I_key_varchar2_5    IN      VARCHAR2,
                       I_key_varchar2_6    IN      VARCHAR2,
                       I_key_varchar2_7    IN      VARCHAR2,
                       I_key_varchar2_8    IN      VARCHAR2,
                       I_key_varchar2_9    IN      VARCHAR2,
                       I_key_date_10       IN      DATE,
                       I_key_date_11       IN      DATE,
                       I_text_1            IN      VARCHAR2,
                       I_text_2            IN      VARCHAR2,
                       I_text_3            IN      VARCHAR2,
                       I_text_4            IN      VARCHAR2,
                       I_text_5            IN      VARCHAR2,
                       I_text_6            IN      VARCHAR2,
                       I_text_7            IN      VARCHAR2,
                       I_text_8            IN      VARCHAR2,
                       I_text_9            IN      VARCHAR2
                       )
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------------------------------------------
-- Function Name: GET_TL_ROWS
-- Purpose      : Returns multiple rows using sys ref cursor for passed in translate table 
--                If input languange is provided, this will fetch rows for the input language
--                If input language is null, this will fetch rows for all languages other than the primary language 
----------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TL_ROWS(
                     O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_translate_table   IN      VARCHAR2,
                     I_lang              IN      NUMBER,
                     I_key_number_1      IN      NUMBER,
                     I_key_number_2      IN      NUMBER,
                     I_key_number_3      IN      NUMBER,
                     I_key_number_4      IN      NUMBER,
                     I_key_varchar2_5    IN      VARCHAR2,
                     I_key_varchar2_6    IN      VARCHAR2,
                     I_key_varchar2_7    IN      VARCHAR2,
                     I_key_varchar2_8    IN      VARCHAR2,
                     I_key_varchar2_9    IN      VARCHAR2,
                     I_key_date_10       IN      DATE,
                     I_key_date_11       IN      DATE
                     )
RETURN SYS_REFCURSOR ;
----------------------------------------------------------------------------------------------------------------------------------------------------
END TRANSLATION_SQL;
/
