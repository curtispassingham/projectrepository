CREATE OR REPLACE PACKAGE SVCPROV_UTILITY AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- package types
--------------------------------------------------------------------------------
TYPE ERROR_TBL IS TABLE OF VARCHAR(2000);
--------------------------------------------------------------------------------
-- public procedures/functions
--------------------------------------------------------------------------------
PROCEDURE BUILD_SERVICE_OP_STATUS(IO_ServiceOpStatus  IN OUT NOCOPY  "RIB_ServiceOpStatus_REC",
                                  I_status_code       IN             VARCHAR2,
                                  I_error_message     IN             VARCHAR2,
                                  I_program           IN             VARCHAR2);
--------------------------------------------------------------------------------
-- Function Name  : BUILD_FAIL_RECORD
-- Purpose        : This is a public function to be called from service 
--                  provider packages to add the error message to the return webserivce
--                  object. 
-- Input: I_error_message: Error String
--        I_rib_failstatus_rec : Record of type "RIB_FailStatus_REC". This can be null or
--                               may have other error message. The input error message will
--                               appened to the existing list of error. 
-- Output: RIB_FailStatus_REC  : Updated input RIB_FailStatus_REC with the input error message
--------------------------------------------------------------------------------
FUNCTION BUILD_FAIL_RECORD(I_error_message           IN   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rib_failstatus_rec      IN   "RIB_FailStatus_REC")
                           
RETURN "RIB_FailStatus_REC";
--------------------------------------------------------------------------------
-- Function Name  : PARSE_STAGED_ERR_MSG (OVERLOADED)
-- Purpose        : This is a public function to be called from service 
--                  provider packages to parse ';' seperated error messages on
--                  ERROR_MSG column of the input staging table and return them
--                  as individual error strings in "RIB_FailStatus_TBL". 
-- Input: I_stage_table_name: a valid service pattern staging table in RMS 
--                            owning schema with an ERROR_MSG column
--        I_process_id 
-- Output: O_error_message
-- Input/Output: IO_fail_tbl: collection of failure messages
--------------------------------------------------------------------------------
FUNCTION PARSE_STAGED_ERR_MSG(O_error_message    IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_fail_tbl        IN OUT  NOCOPY  "RIB_FailStatus_TBL",
                              I_stage_table_name IN              DBA_TABLES.TABLE_NAME%TYPE,
                              I_process_id       IN              NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name  : PARSE_STAGED_ERR_MSG (OVERLOADED)
-- Purpose        : This is a public function to be called from file processing
--                  to parse ';' seperated error messages on ERROR_MSG column of 
--                  the input staging table and return them as individual error 
--                  strings in OBJ_ERROR_TBL.
-- Input: I_stage_table_name: a valid service pattern staging table in RMS 
--                            owning schema with an ERROR_MSG column
--        I_process_id 
-- Output: O_error_message
-- Input/Output: IO_error_tbl: collection of failure messages
--------------------------------------------------------------------------------
FUNCTION PARSE_STAGED_ERR_MSG(O_error_message    IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_error_tbl       IN OUT  NOCOPY  OBJ_ERROR_TBL,
                              I_stage_table_name IN              DBA_TABLES.TABLE_NAME%TYPE,
                              I_process_id       IN              NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name  : PARSE_ERR_MSG (OVERLOADED)
-- Purpose        : This is a public function to parse ';' seperated error messages
--                  in the input and return them as individual error strings in 
--                  "RIB_FailStatus_TBL". 
-- Input: I_error_message_tbl: a collection of error messages
-- Output: O_error_message
-- Input/Output: IO_fail_tbl: collection of failure messages
--------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message     IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_fail_tbl         IN OUT  NOCOPY "RIB_FailStatus_TBL",
                       I_error_message_tbl IN             ERROR_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name  : PARSE_ERR_MSG (OVERLOADED)
-- Purpose        : This is a public function to parse ';' seperated error messages
--                  in the input and return them as individual error strings in 
--                  OBJ_ERROR_TBL. This is a overloaded function to be called from
--                  file based processing to dump validation error into file.
-- Input: I_error_message_tbl: a collection of error messages
-- Output: O_error_message
-- Input/Output: IO_error_tbl: collection of failure messages
--------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message     IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_error_tbl        IN OUT  NOCOPY OBJ_ERROR_TBL,
                       I_error_message_tbl IN             ERROR_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END SVCPROV_UTILITY;
/
