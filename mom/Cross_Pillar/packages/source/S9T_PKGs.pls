CREATE OR REPLACE PACKAGE S9T_PKG AUTHID CURRENT_USER
AS
  Lp_max_columns CONSTANT NUMBER:=255;
Type names_map_typ
IS
  TABLE OF NUMBER(5) INDEX BY VARCHAR2(255);
Type trans_map_typ
IS
  TABLE OF VARCHAR2(255) INDEX BY VARCHAR2(255);
Type s9t_errors_tab_typ
IS
  TABLE OF s9t_errors%rowtype;
  Lp_s9t_errors_tab s9t_errors_tab_typ:=NEW s9t_errors_tab_typ();
  ------------------------------------------------------------------------------
  PROCEDURE WRITE_S9T_ERROR(
      I_file_id IN s9t_errors.file_id%type,
      I_sheet   IN VARCHAR2,
      I_row_seq IN NUMBER,
      I_col     IN VARCHAR2,
      I_sqlcode IN NUMBER,
      I_sqlerrm IN VARCHAR2);
  PROCEDURE FLUSH_S9T_ERRORS;
  ------------------------------------------------------------------------------
  PROCEDURE init_file(
      I_file_id   IN NUMBER,
      I_file_name IN VARCHAR2);
  ------------------------------------------------------------------------------
  PROCEDURE init_obj(
      I_file_id IN NUMBER);
  ------------------------------------------------------------------------------
  PROCEDURE add_sheet(
      I_file_id    IN NUMBER,
      I_sheet_name IN VARCHAR2);
  ------------------------------------------------------------------------------
  PROCEDURE add_row(
      I_file_id    IN NUMBER,
      I_sheet_name IN VARCHAR2,
      I_row s9t_row);
  ------------------------------------------------------------------------------
  PROCEDURE add_rows(
      I_file_id    IN NUMBER,
      I_sheet_name IN VARCHAR2,
      I_rows s9t_row_tab);
  ------------------------------------------------------------------------------
  FUNCTION xml2row(
      I_xml          IN XMLTYPE,
      I_row_seq      IN NUMBER,
      I_column_count IN NUMBER)
    RETURN s9t_row;
  ------------------------------------------------------------------------------
  PROCEDURE ods2obj(
      I_file_id IN NUMBER);
  ------------------------------------------------------------------------------
  FUNCTION get_sheet_names(
      I_file_id IN NUMBER)
    RETURN names_map_typ;
  ------------------------------------------------------------------------------
  FUNCTION get_col_names(
      I_file_id    IN NUMBER,
      I_sheet_name IN VARCHAR2)
    RETURN names_map_typ;
  ------------------------------------------------------------------------------
  FUNCTION trim_row(
      I_var     IN s9t_cells,
      I_col_tab IN num_tab)
    RETURN s9t_cells;
  ------------------------------------------------------------------------------
  PROCEDURE delete_sheets(
      I_file_id IN NUMBER,
      I_sheet_names s9t_cells);
  ------------------------------------------------------------------------------
  PROCEDURE delete_columns(
      I_file_id IN NUMBER,
      I_sheet_name VARCHAR2,
      I_columns s9t_cells );
  ------------------------------------------------------------------------------
  PROCEDURE apply_template(
      I_file_id  IN NUMBER,
      I_template IN VARCHAR2);
  ------------------------------------------------------------------------------
  FUNCTION ADD_LIST_VALIDATION(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_file_id        IN       NUMBER,
                               I_sheet          IN       VARCHAR2,
                               I_column         IN       VARCHAR2,
                               I_code_type      IN       CODE_HEAD.CODE_TYPE%TYPE)
  return BOOLEAN;
  ------------------------------------------------------------------------------
  PROCEDURE ADD_LIST_VALIDATION(I_file_id   IN   NUMBER,
                                I_sheet     IN   VARCHAR2,
                                I_column    IN   VARCHAR2,
                                I_lov            s9t_cells);
  ------------------------------------------------------------------------------
  PROCEDURE translate_to_user_lang(
      I_file_id IN NUMBER);
  ------------------------------------------------------------------------------
  FUNCTION CODE2DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_templ_category   IN       VARCHAR2,
                     I_file             IN OUT   s9t_file,
                     I_reverse          IN       BOOLEAN DEFAULT FALSE)
  return BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION POPULATE_LISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_file_id          IN       NUMBER,
                          I_templ_category   IN       VARCHAR2)
  return BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION POPULATE_LISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_file_id          IN       NUMBER,
                          I_templ_category   IN       VARCHAR2,
                          I_template_key     IN       S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
  RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION sheet_trans(
      I_template_key IN s9t_template.template_key%type,
      I_lang         IN lang.lang%type)
    RETURN trans_map_typ;
  ------------------------------------------------------------------------------
  FUNCTION check_file_name(
      O_error_message IN OUT rtk_errors.rtk_text%TYPE,
      I_file_name     IN s9t_folder.file_name%type,
      O_found OUT BOOLEAN)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION check_template(
      O_error_message IN OUT rtk_errors.rtk_text%TYPE,
      I_template_key  IN s9t_template.template_key%type,
      O_found OUT BOOLEAN)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION validate_template(
      I_file_id IN NUMBER)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION basename(
      I_file_path IN VARCHAR2)
    RETURN VARCHAR2;
  ------------------------------------------------------------------------------
  FUNCTION BASENAME(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      O_basename OUT S9T_FOLDER.FILE_NAME%TYPE,
      I_file_path IN VARCHAR2)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  PROCEDURE load_template_config;
  ------------------------------------------------------------------------------
  FUNCTION calc_col_head(
      I_num IN NUMBER)
    RETURN VARCHAR2;
  ------------------------------------------------------------------------------
  FUNCTION get_clob(
      I_file IN S9t_file)
    RETURN CLOB;
  ------------------------------------------------------------------------------
  PROCEDURE SAVE_OBJ(
      I_file IN OUT NOCOPY s9t_file);
  ------------------------------------------------------------------------------
  PROCEDURE UPDATE_ODS(
      I_file IN S9t_file);
  ------------------------------------------------------------------------------
  FUNCTION get_obj(
      I_file_ID IN NUMBER,
      I_load    IN BOOLEAN DEFAULT FALSE )
    RETURN s9t_file;
  ------------------------------------------------------------------------------
  FUNCTION INSERT_DATA(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file_ID       IN NUMBER,
      I_file_name     IN VARCHAR2,
      I_template_key  IN S9T_TEMPLATE.TEMPLATE_KEY%TYPE,
      I_user_lang     IN LANG.LANG%TYPE,
      I_status        IN VARCHAR2,
      I_action        IN VARCHAR2,
      I_action_date   IN DATE)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  FUNCTION DELETE_DATA(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file_ID       IN NUMBER)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------
  PROCEDURE ADD_SHEET_FOR_SQL(
      I_file       IN NUMBER,
      I_sheet_name IN VARCHAR2,
      I_query      IN VARCHAR2);
  ------------------------------------------------------------------------------
  FUNCTION GET_COL_KEYS_MAP(
      I_file_id    IN NUMBER,
      I_sheet_name IN VARCHAR2)
    RETURN names_map_typ;
  ------------------------------------------------------------------------------
  FUNCTION GET_KEY_VAL_PAIRS_TAB(
      I_file_id    IN NUMBER,
      I_sheet_name IN VARCHAR2)
    RETURN key_val_pairs_tab PIPELINED;
  ------------------------------------------------------------------------------
  PROCEDURE copy_template(
      I_from_template IN s9t_template.template_key%type,
      I_to_template s9t_template.template_key%type);
  ------------------------------------------------------------------------------
END s9t_pkg;
/
