CREATE OR REPLACE PACKAGE BODY CFA_SETUP_ENTITY_SQL AS
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ENTITY_KEY_LABELS (IO_cfa_ext_entity_tbl  IN OUT  CFA_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table        IN      CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
                                  I_lang                  IN      CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE) IS

   L_program  VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.QUERY_ENTITY_KEY_LABELS';
   L_cnt      NUMBER       := 0;

   cursor C_GET_KEY_LABELS is
      select ex.base_rms_table,
             ex.key_col,
             ex.key_number,
             ex.data_type,
             ex.description_code,
             I_lang lang,
             kd.label,
             kd.default_lang_ind
        from cfa_ext_entity_key ex,
             cfa_ext_entity_key_labels kd
       where ex.base_rms_table = kd.base_rms_table (+)
         and ex.key_col = kd.key_col (+)
         and ex.base_rms_table = I_base_rms_table
         and kd.lang (+) = I_lang
       order by ex.key_number;
BEGIN
   ---
   FOR rec in C_GET_KEY_LABELS LOOP
      L_cnt := L_cnt + 1;
      ---
      IO_cfa_ext_entity_tbl(L_cnt).base_rms_table := rec.base_rms_table;
      IO_cfa_ext_entity_tbl(L_cnt).key_col := rec.key_col;
      IO_cfa_ext_entity_tbl(L_cnt).key_number := rec.key_number;
      IO_cfa_ext_entity_tbl(L_cnt).data_type := rec.data_type;
      IO_cfa_ext_entity_tbl(L_cnt).lang := rec.lang;
      IO_cfa_ext_entity_tbl(L_cnt).label := rec.label;
      IO_cfa_ext_entity_tbl(L_cnt).default_lang_ind := rec.default_lang_ind;
      IO_cfa_ext_entity_tbl(L_cnt).description_code := rec.description_code;

   END LOOP;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_cfa_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    to_char(SQLCODE));
      IO_cfa_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_cfa_ext_entity_tbl(1).error_message);

END QUERY_ENTITY_KEY_LABELS;
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ENTITY_KEY_LABELS (IO_cfa_ext_entity_tbl   IN OUT  CFA_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
                                  I_key                     IN        CFA_EXT_ENTITY_KEY_LABELS.KEY_COL%TYPE,
                                  I_lang                    IN        CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE) IS

   L_program      VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.LOCK_ENTITY_KEY_LABELS';
   record_locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_KEY_LABELS is
      select 'x'
        from cfa_ext_entity_key_labels
       where base_rms_table = I_base_rms_table
         and key_col = I_key
         and lang    = I_lang
         for update nowait;

BEGIN
   ---
   open C_LOCK_KEY_LABELS;
   close C_LOCK_KEY_LABELS;
   ---
   return;
   ---
EXCEPTION

   when RECORD_LOCKED then
      IO_cfa_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                    'CFA_EXT_ENTITY_KEY_LABELS',
                                                                    I_base_rms_table||', '||
                                                                    I_key||', '||
                                                                    I_lang);
      IO_cfa_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(-20002, IO_cfa_ext_entity_tbl(1).error_message);
   when OTHERS then
      IO_cfa_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                   SQLERRM,
                                                                   L_program,
                                                                   to_char(SQLCODE));
      IO_cfa_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_cfa_ext_entity_tbl(1).error_message);

END LOCK_ENTITY_KEY_LABELS;
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ENTITY_KEY_LABELS(IO_cfa_ext_entity_tbl  IN OUT  CFA_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table       IN      CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                  I_lang                 IN      CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE,
                                  I_default_ind          IN      CFA_EXT_ENTITY_KEY_LABELS.DEFAULT_LANG_IND%TYPE) IS

   L_program      VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.MERGE_ENTITY_KEY_LABELS';

   record_locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_LABEL_DEFAULT is
      select kd.rowid
        from cfa_ext_entity_key_labels kd
       where kd.base_rms_table = I_base_rms_table
         for update nowait;

  cursor C_LOCK_KEY_DEFAULT is
      select ek.rowid
        from cfa_ext_entity_key ek
       where ek.base_rms_table = I_base_rms_table
         for update nowait;

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid TYP_rowid;

   TYPE TYP_rowid_key is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid_key TYP_rowid_key;

BEGIN
   ---
   if I_default_ind = 'Y' then
      open C_LOCK_LABEL_DEFAULT;
      fetch C_LOCK_LABEL_DEFAULT BULK COLLECT into TBL_rowid;
      close C_LOCK_LABEL_DEFAULT;

      FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
        update cfa_ext_entity_key_labels
           set default_lang_ind = decode(lang, I_lang, 'Y', 'N')
          where rowid = TBL_rowid(i);
   end if;
   ---
   open C_LOCK_KEY_DEFAULT;
   fetch C_LOCK_KEY_DEFAULT BULK COLLECT into TBL_rowid_key;
   close C_LOCK_KEY_DEFAULT;

   FORALL i in IO_cfa_ext_entity_tbl.FIRST..IO_cfa_ext_entity_tbl.LAST
      update cfa_ext_entity_key
         set description_code = IO_cfa_ext_entity_tbl(i).description_code
       where base_rms_table   = IO_cfa_ext_entity_tbl(i).base_rms_table
         and key_col          = IO_cfa_ext_entity_tbl(i).key_col;


   FORALL i in IO_cfa_ext_entity_tbl.FIRST..IO_cfa_ext_entity_tbl.LAST
      merge into cfa_ext_entity_key_labels kd
         using (select IO_cfa_ext_entity_tbl(i).base_rms_table base_rms_table,
                       IO_cfa_ext_entity_tbl(i).key_col key_col,
                       IO_cfa_ext_entity_tbl(i).lang lang,
                       IO_cfa_ext_entity_tbl(i).label label,
                       I_default_ind default_lang_ind
                  from dual) tmp
         on (kd.base_rms_table = tmp.base_rms_table and
             kd.key_col = tmp.key_col and
             kd.lang = tmp.lang)
         when matched then
            update
               set kd.label = tmp.label,
                   kd.default_lang_ind = tmp.default_lang_ind
         when not matched then
            insert(base_rms_table,
                   key_col,
                   lang,
                   label,
                   default_lang_ind)
            values(tmp.base_rms_table,
                   tmp.key_col,
                   tmp.lang,
                   tmp.label,
                   tmp.default_lang_ind);
   ---
   return;
   ---
EXCEPTION
   when OTHERS then      
      IO_cfa_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                   SQLERRM,
                                                                   L_program,
                                                                   to_char(SQLCODE));
      IO_cfa_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_cfa_ext_entity_tbl(1).error_message);

END MERGE_ENTITY_KEY_LABELS;
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
--------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_EXT_ID(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_entity_id      IN OUT  CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.GET_NEXT_EXT_ID';
   L_exists          VARCHAR2(1)  := 'N';
   L_wrap_entity_id  CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE;
   L_entity_id       CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select ext_entity_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from cfa_ext_entity ext
       where ext.ext_entity_id = L_entity_id;

BEGIN
   ---
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_entity_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_entity_id := L_entity_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_entity_id := L_entity_id;         
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_entity_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_entity_id = L_wrap_entity_id then
         O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_ID_EXIST',
                                               L_entity_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_ID_EXIST',
                                            L_entity_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_EXT_ID;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_INSERT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table      IN      CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.EXT_KEYS_INSERT';

BEGIN
   ---
   insert into cfa_ext_entity_key(base_rms_table,
                                  key_col,
                                  key_number,
                                  data_type)
                           select tac.table_name,
                                  col.column_name ,
                                  col.position,
                                  tac.data_type
                             from all_cons_columns col,
                                  all_constraints con,
                                  all_tab_columns tac,
                                  system_options  opt
                            where con.constraint_name = col.constraint_name
                              and tac.owner       = con.owner
                              and tac.owner       = col.owner
                              and tac.table_name  = con.table_name
                              and tac.column_name = col.column_name
                              and con.owner       = opt.table_owner
                              and con.constraint_type = 'P'
                              and con.table_name = I_rms_table
                            order by position;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END EXT_KEYS_INSERT;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_UPDATE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table      IN      CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_rms_table  IN      CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_ext_table  IN      CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                         I_ext_ent_id     IN      CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.EXT_KEYS_UPDATE';
   
   cursor C_GROUP_SET_EXT_ENT is
      select 'x'
        from cfa_attrib_group_set
       where ext_entity_id = I_ext_ent_id
         for update nowait;

   cursor C_EXT_ENT_KEYS is
      select 'x'
        from cfa_ext_entity_key
       where base_rms_table = I_rms_table
         for update nowait;

   cursor C_EXT_ENT_KEYS_LABELS is
      select 'x'
        from cfa_ext_entity_key_labels
       where base_rms_table = I_rms_table
         for update nowait;

   cursor C_EXT_ENTITY is
      select 'x'
        from cfa_ext_entity
       where ext_entity_id = I_ext_ent_id
         for update nowait;

BEGIN
   ---
   open C_GROUP_SET_EXT_ENT;
   close C_GROUP_SET_EXT_ENT;
   
   delete
     from cfa_attrib_group_set
    where ext_entity_id = I_ext_ent_id;
   ---
   open C_EXT_ENT_KEYS;
   close C_EXT_ENT_KEYS;
   
   delete
     from ext_entity_key
    where base_rms_table = I_rms_table;
   ---
   open C_EXT_ENT_KEYS_LABELS;
   close C_EXT_ENT_KEYS_LABELS;
   
   delete
     from cfa_ext_entity_key_labels
    where base_rms_table = I_rms_table;
   ---
   open C_EXT_ENTITY;
   close C_EXT_ENTITY;
   
   update cfa_ext_entity
      set base_rms_table = I_new_rms_table,
          custom_ext_table = I_new_ext_table
    where ext_entity_id  = I_ext_ent_id;
   ---
   insert into cfa_ext_entity_key(base_rms_table,
                                  key_col,
                                  key_number,
                                  data_type)
                           select tac.table_name,
                                  col.column_name ,
                                  col.position,
                                  tac.data_type
                             from all_cons_columns col,
                                  all_constraints con,
                                  all_tab_columns tac,
                                  system_options  opt
                            where con.constraint_name = col.constraint_name
                              and tac.owner       = con.owner
                              and tac.owner       = col.owner
                              and tac.table_name  = con.table_name
                              and tac.column_name = col.column_name
                              and con.owner       = opt.table_owner
                              and con.constraint_type = 'P'
                              and con.table_name = I_new_rms_table
                            order by position;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;

END EXT_KEYS_UPDATE;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_DELETE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_base_rms_table  IN      CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_ext_ent_id      IN      CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.EXT_KEYS_DELETE';
   L_grp_exists VARCHAR2(1);

   cursor C_GROUP_SET_EXT_ENT is
      select 'x'
        from cfa_attrib_group_set
       where ext_entity_id = I_ext_ent_id
         for update nowait;

    cursor C_EXT_ENT_KEYS_LABELS is
      select 'x'
        from cfa_ext_entity_key_labels
       where base_rms_table = I_base_rms_table
         for update nowait;

   cursor C_EXT_ENT_KEYS is
      select 'x'
        from cfa_ext_entity_key
       where base_rms_table = I_base_rms_table
         for update nowait;

BEGIN
   ---
   open C_GROUP_SET_EXT_ENT;
   fetch C_GROUP_SET_EXT_ENT into L_grp_exists;
   close C_GROUP_SET_EXT_ENT;
   ---
   if L_grp_exists is NOT NULL then
       O_error_message := SQL_LIB.CREATE_MSG('GROUP_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
       return FALSE;

   end if;
   ---
   open C_EXT_ENT_KEYS_LABELS;
   close C_EXT_ENT_KEYS_LABELS;
   ---
   delete
     from cfa_ext_entity_key_labels
    where base_rms_table = I_base_rms_table;
   ---
   open C_EXT_ENT_KEYS;
   close C_EXT_ENT_KEYS;
   ---
   delete
     from cfa_ext_entity_key
    where base_rms_table = I_base_rms_table;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;

END EXT_KEYS_DELETE;
---------------------------------------------------------------------------------------------------
FUNCTION GET_EXT_TABLE_NAME(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_custom_ext_table IN OUT  CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                            I_base_rms_table   IN      CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'CFA_SETUP.GET_EXT_TABLE_NAME';
   L_owner        SYSTEM_OPTIONS.TABLE_OWNER%TYPE;
   L_table_name   ALL_TABLES.TABLE_NAME%TYPE;
   L_found_ent    VARCHAR2(1);
   L_found_ext    VARCHAR2(1);

   cursor C_CHK_RMS_TBL is
      select tab.table_name,
             tab.owner
        from all_tables tab,
             system_options opt
       where tab.owner = opt.table_owner
         and table_name = I_base_rms_table;

   cursor C_TBL_NOT_IN_ENT is
      select 'x'
        from cfa_ext_entity
       where base_rms_table = I_base_rms_table;
    
    cursor C_TBL_NOT_IN_EXT is
       select 'x'
         from cfa_ext_entity
        where custom_ext_table = I_base_rms_table;

   cursor C_GET_EXT_TBL is
      select c2.table_name
        from all_constraints c1,
             all_constraints c2
       where c2.owner = c1.owner
         and c2.constraint_type = 'R'
         and c2.r_constraint_name = c1.constraint_name
         and c1.table_name = I_base_rms_table
         and c1.owner = L_owner
         and c2.table_name like '%_CFA_EXT'
         and c2.owner = L_owner;

BEGIN
   ---
   open C_CHK_RMS_TBL;
   fetch C_CHK_RMS_TBL into L_table_name,
                            L_owner;
   close C_CHK_RMS_TBL;
   ---
   if L_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_RMS_TABLE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;

   end if;
   ---
   open C_TBL_NOT_IN_ENT;
   fetch C_TBL_NOT_IN_ENT into L_found_ent;
   close C_TBL_NOT_IN_ENT;
   
   if L_found_ent is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('TBL_ALREADY_EXT',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   --
   open C_TBL_NOT_IN_EXT;
   fetch C_TBL_NOT_IN_EXT into L_found_ext;
   close C_TBL_NOT_IN_EXT;
   
   if L_found_ext is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('EXT_TBL_CANNOT_EXT',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_GET_EXT_TBL;
   fetch C_GET_EXT_TBL into O_custom_ext_table;
   close C_GET_EXT_TBL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_EXT_TABLE_NAME;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_EXT_KEY_DEFAULT_LANG(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_default_ind     IN OUT  CFA_EXT_ENTITY_KEY_LABELS.DEFAULT_LANG_IND%TYPE,
                                  I_base_rms_table  IN      CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE,
                                  I_lang            IN      CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'CFA_SETUP.CHK_EXT_KEY_DEFAULT_LANG';
   
   cursor C_CHK_DEFAULT is
      select kd.default_lang_ind
        from cfa_ext_entity_key_labels kd
       where kd.base_rms_table = I_base_rms_table
         and kd.lang = I_lang
         and rownum = 1;

BEGIN
   ---
   O_default_ind := 'N';
   ---
   open C_CHK_DEFAULT;
   fetch C_CHK_DEFAULT into O_default_ind;
   close C_CHK_DEFAULT;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHK_EXT_KEY_DEFAULT_LANG;
--------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ENTITY_DEFAULT_LANG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists         IN OUT VARCHAR2,
                                    I_base_rms_table IN     CFA_EXT_ENTITY_KEY_LABELS.BASE_RMS_TABLE%TYPE)                              
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.CHK_NO_ENTITY_DEFAULT_LANG';
   L_exists   VARCHAR2(1)  := NULL;
   
   cursor C_LANG_IND is
      select 'x'
        from cfa_ext_entity_key_labels
       where base_rms_table = I_base_rms_table
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;
   ---      
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;

END CHK_NO_ENTITY_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION ENTITY_EXIST(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exist          IN OUT  VARCHAR2,
                      I_ext_entity     IN      CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.ENTITY_EXIST';
   L_exist   VARCHAR2(1);

   cursor C_ENTITY_EXIST is
      select 'x'
        from cfa_ext_entity
       where ext_entity_id = I_ext_entity;

BEGIN
   ---
   open C_ENTITY_EXIST;
   fetch C_ENTITY_EXIST into L_exist;
   ---
   if C_ENTITY_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_ENTITY_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ENTITY_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT  VARCHAR2,
                            O_base_rms_table    IN OUT  CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                            O_custom_ext_table  IN OUT  CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                            I_ext_entity_id     IN      CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.GET_ENTITY_EXT_TBL';
   
   cursor C_GET_TABLES is
      select base_rms_table,
             custom_ext_table
        from cfa_ext_entity ext
       where ext_entity_id = I_ext_entity_id;

BEGIN
   ---
   open C_GET_TABLES;
   fetch C_GET_TABLES into O_base_rms_table,
                           O_custom_ext_table;
   close C_GET_TABLES;
   ---
   if O_base_rms_table is NULL then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;
   ---        
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ENTITY_EXT_TBL;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ENTITY_KEY_LANG(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_base_rms_table  IN      CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                I_lang            IN      CFA_EXT_ENTITY_KEY_LABELS.LANG%TYPE)
RETURN BOOLEAN  IS

   L_program      VARCHAR2(61) := 'CFA_SETUP_ENTITY_SQL.DELETE_ENTITY_KEY_LANG';

   cursor C_LOCK_ENTITY_LABEL is
      select 'x'
        from cfa_ext_entity_key_labels
       where base_rms_table = I_base_rms_table
         and lang           = I_lang
         for update nowait;
         
   cursor C_LOCK_ENTITY_KEY is
      select 'x'
        from cfa_ext_entity_key
       where base_rms_table = I_base_rms_table
         for update nowait;

BEGIN

   open C_LOCK_ENTITY_KEY;
   close C_LOCK_ENTITY_KEY;
   
   update cfa_ext_entity_key k
      set description_code = NULL
    where base_rms_table = I_base_rms_table
      and exists (select 'x'
                     from cfa_ext_entity_key_labels
                     where base_rms_table   = k.base_rms_table
                       and key_col          = k.key_col
                       and lang             = I_lang
                       and default_lang_ind = 'Y');
   ---
   open C_LOCK_ENTITY_LABEL;
   close C_LOCK_ENTITY_LABEL;

   delete
     from cfa_ext_entity_key_labels
    where base_rms_table = I_base_rms_table
      and lang           = I_lang;
   ---       
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ENTITY_KEY_LANG;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_ENTITY_SQL;
/
