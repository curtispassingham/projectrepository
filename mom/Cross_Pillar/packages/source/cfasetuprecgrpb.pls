

create or replace PACKAGE BODY CFA_SETUP_REC_GROUP_SQL AS
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_RECGRP_EXISTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT VARCHAR2,
                                  I_rec_grp_id      IN     CFA_REC_GROUP.REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN IS
   
   L_exists VARCHAR2(1);

   cursor C_GET_REC_GRP is
      select 'x'
        from cfa_rec_group 
       where rec_group_id = I_rec_grp_id;
       
BEGIN
   ---
   open C_GET_REC_GRP;
   fetch C_GET_REC_GRP into L_exists;
   close C_GET_REC_GRP;
   ---
   if L_exists is NOT NULL then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_REC_GROUP_SQL.CHK_ATTRIB_RECGRP_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIB_RECGRP_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_NEXT_RECGRP_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_rec_grp_id    IN OUT CFA_REC_GROUP.REC_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_exists            VARCHAR2(1)  := 'N';
   L_wrap_recgrp_id    CFA_REC_GROUP.REC_GROUP_ID%TYPE;
   L_recgrp_id         CFA_REC_GROUP.REC_GROUP_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select cfa_rec_group_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from cfa_rec_group rg
       where rg.rec_group_id = L_recgrp_id;

BEGIN

   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_recgrp_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_recgrp_id := L_recgrp_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_rec_grp_id := L_recgrp_id;
         return TRUE;
      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_recgrp_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_recgrp_id = L_wrap_recgrp_id then
         O_error_message := SQL_LIB.CREATE_MSG('RECGRP_ID_EXIST',
                                               L_recgrp_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECGRP_ID_EXIST',
                                            L_recgrp_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_REC_GROUP_SQL.GEN_NEXT_RECGRP_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GEN_NEXT_RECGRP_ID;
---------------------------------------------------------------------------------------
FUNCTION REC_GROUP_DELETE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_rec_grp_id    IN     CFA_REC_GROUP.REC_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_rec_grp_exists VARCHAR2(1);
   L_attrib_exists  VARCHAR2(1);

   cursor C_GRP_EXISTS is
      select 'x'
        from cfa_rec_group_labels
       where rec_group_id = I_rec_grp_id
         for update nowait;

   cursor C_ATTRIB_EXISTS is
      select 'x'
        from cfa_attrib
       where rec_group_id = I_rec_grp_id
         for update nowait;

BEGIN
   ---
   open C_ATTRIB_EXISTS;
   fetch C_ATTRIB_EXISTS into L_attrib_exists;
   close C_ATTRIB_EXISTS;
   ---
   if L_attrib_exists is NOT NULL then
      o_error_message := SQL_LIB.CREATE_MSG('ASSOC_DTLS_DELETE',
                                            I_rec_grp_id,
                                            NULL,
                                            NULL);
         return FALSE;

   else
       open C_GRP_EXISTS;
       fetch C_GRP_EXISTS into L_rec_grp_exists;
       close C_GRP_EXISTS;
       ---
       if L_rec_grp_exists is NOT NULL then
          delete
            from cfa_rec_group_labels
           where rec_group_id = I_rec_grp_id;
       end if;
       ---
       return TRUE;
       ---
   end if;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_REC_GROUP_SQL.REC_GROUP_DELETE',
                                            to_char(SQLCODE));

      return FALSE;

END REC_GROUP_DELETE;
---------------------------------------------------------------------------------------
FUNCTION REC_GROUP_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rec_grp_id      IN     CFA_REC_GROUP_LABELS.REC_GROUP_ID%TYPE,
                        I_lang            IN     CFA_REC_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   L_lang_exists  VARCHAR2(1);

   cursor C_LANG_EXISTS is
      select 'x'
        from cfa_rec_group_labels
       where rec_group_id = I_rec_grp_id
         and lang = I_lang;

BEGIN
   ---
   open C_LANG_EXISTS;
   fetch C_LANG_EXISTS into L_lang_exists;
   close C_LANG_EXISTS;
   ---
   if L_lang_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_LANG_SELECT',
                                            NULL,
                                            NULL,
                                            NULL);
         return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_REC_GROUP_SQL.REC_GROUP_LANG',
                                            to_char(SQLCODE));

      return FALSE;

END REC_GROUP_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_RG_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rec_grp_id      IN     CFA_REC_GROUP_LABELS.REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN IS
   
    L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
     select 'x'
        from cfa_rec_group_labels rgd
       where rec_group_id = I_rec_grp_id
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_LANGUAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_REC_GROUP_SQL.CHK_RG_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_RG_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_RECGRP_LABELS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists           IN OUT	VARCHAR2,
                              IO_rec_grp_id      IN OUT CFA_REC_GROUP_LABELS.REC_GROUP_ID%TYPE,
                              I_lang             IN     CFA_REC_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   L_missing_rg   CFA_REC_GROUP.REC_GROUP_ID%TYPE;

   cursor C_RECGRP_LABEL_EXIST is
      select rec_group_id 
        from cfa_rec_group g
       where g.rec_group_id = NVL(IO_rec_grp_id, g.rec_group_id)
         and not exists (select 'x'
                           from cfa_rec_group_labels d
                          where d.rec_group_id = g.rec_group_id
                            and lang = NVL(I_lang, d.lang));

BEGIN
   ---
   open C_RECGRP_LABEL_EXIST;
   fetch C_RECGRP_LABEL_EXIST into L_missing_rg;
   ---
   if C_RECGRP_LABEL_EXIST%NOTFOUND then
      O_exists := 'N';
   else
      O_exists := 'Y';
      IO_rec_grp_id := L_missing_rg;
   end if;
   ---
   close C_RECGRP_LABEL_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_REC_GROUP_SQL.CHK_NO_RECGRP_LABELS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_RECGRP_LABELS;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_QUERY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   L_stmt             IN       CFA_REC_GROUP.QUERY%TYPE)
RETURN BOOLEAN IS

   L_query   CFA_REC_GROUP.QUERY%TYPE;

BEGIN
   ---
   L_query := regexp_replace(L_stmt, ':[[:print:]]*', 'NULL');

   EXECUTE IMMEDIATE L_query;
   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STMT',
                                            SQLERRM);
      return FALSE;

END CHK_QUERY;
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TABLE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_table            IN       ALL_TABLES.TABLE_NAME%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'CFA_SETUP_REC_GROUP_SQL.VALIDATE_TABLE'; 
   L_valid    VARCHAR2(1)  := 'N';
 

   cursor C_VALIDATE_TABLE is
      select 'Y'
        from all_tables,
             system_options
       where table_name = I_table
         and owner = table_owner; 

BEGIN
   if I_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_table',
                                             L_program);
      return FALSE;
   end if;

   open C_VALIDATE_TABLE;
   fetch C_VALIDATE_TABLE into L_valid;
   close C_VALIDATE_TABLE;

   if L_valid != 'Y' then
      O_error_message := 'INV_RETEK_TABLE'; 
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TABLE;
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COLUMN(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_table            IN       ALL_TABLES.TABLE_NAME%TYPE,
                         I_column           IN       ALL_TAB_COLUMNS.COLUMN_NAME%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'CFA_SETUP_REC_GROUP_SQL.VALIDATE_COLUMN'; 
   L_valid    VARCHAR2(1)  := 'N';
 

   cursor C_VALIDATE_COLUMN is
      select 'Y'
        from all_tab_columns,
             system_options
       where table_name = I_table
         and owner = table_owner
         and column_name = I_column; 

BEGIN
   if I_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_table',
                                             L_program);
      return FALSE;
   end if;
   if I_column is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_column',
                                             L_program);
      return FALSE;
   end if;

   open C_VALIDATE_COLUMN;
   fetch C_VALIDATE_COLUMN into L_valid;
   close C_VALIDATE_COLUMN;

   if L_valid != 'Y' then
      O_error_message := 'INV_RTK_COLUMN';
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_COLUMN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_COLUMN_DATATYPE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_data_type        IN OUT   ALL_TAB_COLUMNS.DATA_TYPE%TYPE,
                             O_data_length      IN OUT   ALL_TAB_COLUMNS.DATA_LENGTH%TYPE,
                             I_table            IN       ALL_TABLES.TABLE_NAME%TYPE,
                             I_column           IN       ALL_TAB_COLUMNS.COLUMN_NAME%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'CFA_SETUP_REC_GROUP_SQL.GET_COLUMN_DATATYPE'; 

   -- the data_precision + 1 in the below cursor is for the cfaattrsetup.fmb max length value.
   -- cflex.fmb does not handle precision and scale, so if the max length is set to the precision
   -- value, the cflex field will be unable to handle the max value including the decimal place (ex. 12345678.1234 for 12,4)
   cursor C_DATA_TYPE is
      select data_type,
             decode(DATA_TYPE,CFA_SQL.NUM_TYPE,
                                 decode(data_scale,0,DATA_PRECISION,DATA_PRECISION+1)
                              ,DATA_LENGTH) calced_length
        from all_tab_columns,
             system_options
       where table_name = I_table
         and owner = table_owner
         and column_name = I_column; 

BEGIN
   if I_table is NULL  then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_table',
                                             L_program);
      return FALSE;
   end if;
   if I_table is NOT NULL and I_column is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_column',
                                             L_program);
      return FALSE;
   end if;

   open C_DATA_TYPE;
   fetch C_DATA_TYPE into O_data_type,
                          O_data_length;
   close C_DATA_TYPE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_COLUMN_DATATYPE;
---------------------------------------------------------------------------------------------------
FUNCTION GET_REC_GROUP_DATATYPE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_data_type        IN OUT   ALL_TAB_COLUMNS.DATA_TYPE%TYPE,
                                O_data_length      IN OUT   ALL_TAB_COLUMNS.DATA_LENGTH%TYPE,
                                I_rec_group_id     IN       CFA_REC_GROUP.REC_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'CFA_SETUP_REC_GROUP_SQL.GET_REC_GROUP_DATATYPE'; 
 
   cursor C_GET_VALUE_COL_DATATYPE is
    select col_1_data_type,
           col_1_data_length
      from cfa_rec_group
     where rec_group_id = I_rec_group_id;

   cursor C_DATE_DISPLAY_LENGTH is
     select length(value)
       from v$nls_parameters 
      where parameter = 'NLS_DATE_FORMAT';

BEGIN
   if I_rec_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_rec_group_id',
                                             L_program);
      return FALSE;
   end if;
   ---
   open C_GET_VALUE_COL_DATATYPE;
   fetch C_GET_VALUE_COL_DATATYPE into O_data_type,
                                       O_data_length;
   close C_GET_VALUE_COL_DATATYPE;

   -- cfaattribsetup.fmb only allows 3 data types: NUMBER,VARCHAR2,DATE. anything else will be handled as a VARCHAR2.
   --  The below code contains a DATE workaround. cflex.fmb cannot handle an LOV widget and date formatting on the same flex field.
   -- If they have set up a record group that selects date values, they will handled as VARCHAR fields by cflex
   -- and the length will be set to the length of the default display date (nls_date_format)
   if O_data_type not in (CFA_SQL.CHAR_TYPE,CFA_SQL.NUM_TYPE) then 

      if O_data_type = CFA_SQL.DATE_TYPE then
         open C_DATE_DISPLAY_LENGTH;
         fetch C_DATE_DISPLAY_LENGTH into O_data_length;
         close C_DATE_DISPLAY_LENGTH;
       end if;
       ---
       O_data_type := CFA_SQL.CHAR_TYPE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_REC_GROUP_DATATYPE;
---------------------------------------------------------------------------------------------------
FUNCTION BUILD_QUERY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_table_name       IN       CFA_REC_GROUP.TABLE_NAME%TYPE,
                     I_column_1         IN       CFA_REC_GROUP.COLUMN_1%TYPE,
                     I_column_2         IN       CFA_REC_GROUP.COLUMN_2%TYPE,
                     I_where_col_1      IN       CFA_REC_GROUP.WHERE_COL_1%TYPE,
                     I_where_col_1_datatype IN   CFA_REC_GROUP.COL_1_DATA_TYPE%TYPE,
                     I_where_operator_1 IN       CFA_REC_GROUP.WHERE_OPERATOR_1%TYPE,
                     I_where_cond_1     IN       CFA_REC_GROUP.WHERE_COND_1%TYPE,
                     I_where_col_2      IN       CFA_REC_GROUP.WHERE_COL_2%TYPE,
                     I_where_col_2_datatype IN   CFA_REC_GROUP.COL_1_DATA_TYPE%TYPE,
                     I_where_operator_2 IN       CFA_REC_GROUP.WHERE_OPERATOR_2%TYPE,
                     I_where_cond_2     IN       CFA_REC_GROUP.WHERE_COND_2%TYPE,
                     I_date_format      IN       VARCHAR2,
                     O_query            IN OUT   CFA_REC_GROUP.QUERY%TYPE)
  RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'CFA_SETUP_REC_GROUP_SQL.BUILD_QUERY'; 
   L_query             VARCHAR2(4000);
   L_is_variable       NUMBER(2);

BEGIN
   if I_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_table_name',
                                             L_program);
      return FALSE;
   end if;
   if I_column_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_column_1',
                                             L_program);
      return FALSE;
   end if;

   if I_column_2 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_column_2',
                                             L_program);
      return FALSE;
   end if;


   L_query := 'select '||I_column_1||','||chr(10)
          ||'         '||I_column_2|| chr(10)
            ||'  from '||I_table_name||chr(10);

   if I_where_col_1 is NOT NULL and I_where_cond_1 is NOT NULL then
      L_is_variable := instr(I_where_cond_1, ':');

      if L_is_variable > 0 then
         -- I_where_cond_1 contains a : it is a variable, not a constant
            L_query := L_query ||'where '||I_where_col_1||' '||I_where_operator_1||' '||I_where_cond_1||' '||chr(10); 
      else 
         if I_where_col_1_datatype != 'DATE' then
            L_query := L_query ||'where '||I_where_col_1||' '||I_where_operator_1 ||''''||I_where_cond_1||'''' ||chr(10); 
         else
            L_query := L_query ||'where '||I_where_col_1||' '||I_where_operator_1||' to_date('||''''||I_where_cond_1||'''' ||', '||''''||I_date_format||''''||')'||chr(10);
         end if; 
      end if;
   elsif I_where_col_1 is NOT NULL and I_where_cond_1 is NULL then
      if I_where_operator_1 = 'NULL' then
         L_query := L_query ||'where '||I_where_col_1||' is NULL'||chr(10);  
      elsif I_where_operator_1 = '!NULL' then
         L_query := L_query ||'where '||I_where_col_1||' is NOT NULL'||chr(10);       
      end if;
   end if;

   if I_where_col_2 is NOT NULL and I_where_cond_2 is NOT NULL then

      L_is_variable := instr(I_where_cond_2, ':');

      if L_is_variable > 0 then
         -- I_where_cond_2 contains a : it is a variable, not a constant
            L_query := L_query ||'   and  '||I_where_col_2||' '||I_where_operator_2||' '||I_where_cond_2||' '||chr(10); 
      else 
         if I_where_col_2_datatype != 'DATE' then
            L_query := L_query ||'   and  '||I_where_col_2||' '||I_where_operator_2||''''||I_where_cond_2||'''' ||chr(10); 
         else
            L_query := L_query ||'   and  '||I_where_col_2||' '||I_where_operator_2||' to_date(' ||''''||I_where_cond_2||'''' ||', '||''''||I_date_format||''''||')'||chr(10); 
         end if;
      end if;
   elsif I_where_col_2 is NOT NULL and I_where_cond_2 is NULL then
      if I_where_operator_2 = 'NULL' then
         L_query := L_query ||'   and  '||I_where_col_2||' is NULL'||chr(10);
      elsif I_where_operator_2 = '!NULL' then
         L_query := L_query ||'   and  '||I_where_col_2||' is not NULL'||chr(10); 
      end if;
   end if;

   if CHK_QUERY(O_error_message,
                L_query) = FALSE then
      return FALSE;   
   end if;

   O_query := L_query;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_QUERY;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_REC_GROUP_SQL;
/
