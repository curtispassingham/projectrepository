CREATE OR REPLACE PACKAGE BODY LANGUAGE_SQL AS

   LP_data_integration_lang   LANG.LANG%TYPE        := NULL;
--------------------------------------------------------------------
FUNCTION CODE_TYPE_DESC(O_error_message IN OUT  VARCHAR2,
                        I_code_type     IN      VARCHAR2,
                        O_desc      IN OUT VARCHAR2)
RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select code_type_desc
     from code_head
    where code_type = I_code_type;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'CODE_HEAD', I_code_type);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB', 'CODE_HEAD', I_code_type);
   fetch C_ATTRIB into O_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_CODE_TYPE',I_code_type,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'CODE_HEAD',I_code_type);
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'CODE_HEAD', I_code_type);
   close C_ATTRIB;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LANGUAGE_SQL.CODE_TYPE_DESC',
                                            to_char(SQLCODE));
   RETURN FALSE;
END CODE_TYPE_DESC;
----------------------------------------------------------------------------------------
FUNCTION GET_CODE_DESC( O_error_message   IN OUT   VARCHAR2,
                        I_code_type       IN       VARCHAR2,
                        I_code            IN       VARCHAR2,
                        O_code_desc       IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_code_type_desc          CODE_HEAD.CODE_TYPE_DESC%TYPE      := NULL;
   ---
   cursor C_GET_CODE_DESC is
      select code_desc
        from v_code_detail_tl
       where code_type = I_code_type
         and code = I_code;
   ---
   cursor C_TYPE_DESC  is
      select code_type_desc
        from code_head
       where code_type = I_code_type;
BEGIN

   O_code_desc := NULL;
   
   if I_code is NULL then
      return TRUE;
   end if;
   ---
   sql_lib.set_mark('OPEN','C_GET_CODE_DESC','CODE_DETAIL','CODE TYPE: '||I_code_type||', CODE: '||I_code);
   open C_GET_CODE_DESC;
   sql_lib.set_mark('FETCH','C_GET_CODE_DESC','CODE_DETAIL','CODE TYPE: '||I_code_type||', CODE: '||I_code);
   fetch C_GET_CODE_DESC into O_code_desc;
   sql_lib.set_mark('CLOSE','C_GET_CODE_DESC','CODE_DETAIL','CODE TYPE: '||I_code_type||', CODE: '||I_code);
   close C_GET_CODE_DESC;

   -- if the code_desc is not found then fetch the code_type_desc to
   -- populate the error message and return FALSE
   if O_code_desc is NULL then
      sql_lib.set_mark('OPEN','C_type_desc','CODE_HEAD','CODE TYPE: '||I_code_type);
      open C_TYPE_DESC;
      sql_lib.set_mark('FETCH','C_type_desc','CODE_HEAD','CODE TYPE: '||I_code_type);
      fetch C_TYPE_DESC into L_code_type_desc;
      sql_lib.set_mark('CLOSE','C_type_desc','CODE_HEAD','CODE TYPE: '||I_code_type);
      close C_TYPE_DESC;
      O_error_message := sql_lib.create_msg('MISSING_CODE', I_code, L_code_type_desc , I_code_type);
      return FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LANGUAGE_SQL.GET_CODE_DESC',
                                            to_char(SQLCODE));
      return FALSE;
END GET_CODE_DESC;
----------------------------------------------------------------------------------------
FUNCTION GET_CODE_DESC( I_code_type       IN       VARCHAR2,
                        I_code            IN       VARCHAR2)
RETURN VARCHAR2 IS
   O_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   O_code_desc               CODE_DETAIL.CODE_DESC%TYPE  := NULL;

BEGIN

   if GET_CODE_DESC(O_error_message, 
                    I_code_type, 
                    I_code, 
                    O_code_desc) = FALSE then
      raise_application_error( -20000,O_error_message);
   end if;
   
   return O_code_desc;

END GET_CODE_DESC;
------------------------------------------------------------
FUNCTION GET_NAME(O_error_message   IN OUT  VARCHAR2,
                  I_lang          IN  NUMBER,
                  O_lang_desc         IN OUT  VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'LANGUAGE_SQL.GET_NAME';
   L_cursor   VARCHAR2(20) := NULL;

   cursor C_LANGUAGE is
      select description
        from v_lang_tl
       where lang = I_lang;

BEGIN
   sql_lib.set_mark('OPEN', 'C_language', 'lang', NULL);
   open C_LANGUAGE;
   sql_lib.set_mark('FETCH', 'C_language', 'lang', NULL);
   fetch C_LANGUAGE into O_lang_desc;
   if C_LANGUAGE%NOTFOUND then
      O_lang_desc := NULL;
      O_error_message := sql_lib.create_msg('LANGUAGE_NOT_FOUND',
                                             NULL,
                                             NULL,
                                             NULL);
      RETURN FALSE;
   else
      sql_lib.set_mark('CLOSE', 'C_language', 'lang', NULL);
      close C_LANGUAGE;
      RETURN TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
    RETURN FALSE;
END GET_NAME;
--------------------------------------------------------------------
FUNCTION GET_USER_LANGUAGE RETURN NUMBER IS

   L_user_lang     LANG.LANG%TYPE := NULL;

   cursor C_USER_LANG is
       SELECT lang
         FROM lang
        WHERE SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') IS NOT NULL
          AND iso_code= SYS_CONTEXT('RETAIL_CTX','APP_ISO_LANGUAGE');

BEGIN
   --To support connection pooling in Java applications, this function should NOT use
   --the cached value of user language. Always execute the cursor to fetch the value.

   SQL_LIB.SET_MARK('OPEN',
                    'C_USER_LANG',
                    NULL,
                    NULL);
   open C_USER_LANG;

   SQL_LIB.SET_MARK('FETCH',
                    'C_USER_LANG',
                    NULL,
                    NULL);
   fetch C_USER_LANG into L_user_lang;

   if C_USER_LANG%NOTFOUND then
      L_user_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_USER_LANG',
                    NULL,
                    NULL);
   close C_USER_LANG;

   return L_user_lang;

EXCEPTION
   when OTHERS then
      return -999;
END GET_USER_LANGUAGE;
-------------------------------------------------------------------
FUNCTION GET_PRIMARY_LANGUAGE RETURN NUMBER IS

   cursor C_DI_LANG is
      select data_integration_lang
        from system_options;

BEGIN
   if LP_data_integration_lang is NULL then
      open C_DI_LANG;
      fetch C_DI_LANG into LP_data_integration_lang;
      if C_DI_LANG%NOTFOUND then
         RETURN -999;
      end if;
      close C_DI_LANG;
   end if;
   RETURN LP_data_integration_lang;

EXCEPTION
   when OTHERS then
        RETURN -999;
END GET_PRIMARY_LANGUAGE;
---------------------------------------------------------------------
FUNCTION GET_CODE_OR_DESC(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          IO_code          IN OUT CODE_DETAIL.CODE%TYPE,
                          IO_code_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          I_code_type      IN     CODE_DETAIL.CODE_TYPE%TYPE)
   return BOOLEAN is
   ---
   L_program        VARCHAR2(64) := 'LANGUAGE_SQL.GET_CODE_OR_DESC';
   L_code_type_desc CODE_HEAD.CODE_TYPE_DESC%TYPE;
   ---
   -- Check if the IO_code is valid code or IO_code_desc is a valid description first in primary langauge and then in user language. 
   cursor GET_GET_CODE_DETAIL is
      select code, 
             code_desc
        from ( select code,
                      code_desc,
                      1 rnk
                 from code_detail
                where code_type = I_code_type
                  and UPPER(code) = UPPER(IO_code)
               UNION ALL
               select code,
                      code_desc,
                      2 rnk
                 from code_detail
                where code_type = I_code_type
                  and UPPER(code_desc) = UPPER(IO_code_desc)
               UNION ALL
               select code,
                      code_desc,
                      3 rnk
                 from v_code_detail_tl
                where code_type = I_code_type
                  and UPPER(code_desc) = UPPER(IO_code_desc))
        order by rnk asc;

BEGIN
   if IO_code is NULL and IO_code_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'IO_CODE and IO_CODE_DESC',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_code_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_CODE_TYPE',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   
   SQL_LIB.SET_MARK('OPEN', 'GET_GET_CODE_DETAIL', 'CODE_DETAIL', 'Code: '||IO_code||', Code type: '||I_code_type);
   open GET_GET_CODE_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'GET_GET_CODE_DETAIL', 'CODE_DETAIL', 'Code: '||IO_code||', Code type: '||I_code_type);
   fetch GET_GET_CODE_DETAIL into IO_code, 
                                  IO_code_desc;
   if GET_GET_CODE_DETAIL%NOTFOUND then
      O_exists       := FALSE;
   else 
      O_exists       := TRUE;
   end if;
   
   SQL_LIB.SET_MARK('CLOSE', 'GET_GET_CODE_DETAIL', 'CODE_DETAIL', 'Code: '||IO_code||', Code type: '||I_code_type);
   close GET_GET_CODE_DETAIL;
   
   if NOT O_exists then
      if CODE_TYPE_DESC(O_error_message,
                        I_code_type,
                        L_code_type_desc) = FALSE then
         return FALSE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('INV_CODE_OR_CODE_DESC',
                                            IO_code||' ',      
                                            IO_code_desc||' ', 
                                            L_code_type_desc||' ('||I_code_type||')');
   
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CODE_OR_DESC;
---------------------------------------------------------------------
FUNCTION GET_MANY_CODE_DESC(O_error_message                  IN OUT     VARCHAR2,
                            O_list_codetype_code_withdesc    IN OUT     LIST_CODETYPE_CODE_CODEDESC,
                            I_list_codetype_code_nodesc      IN OUT     LIST_CODETYPE_CODE_CODEDESC)
   return BOOLEAN is
   ---
   L_error_message                          RTK_ERRORS.RTK_TEXT%TYPE;
   L_program                                VARCHAR2(64) := 'LANGUAGE_SQL.GET_MANY_CODE_DESC';
   L_current_code_desc                      CODE_DETAIL.CODE_DESC%TYPE;
   ---
   input_table_index                        NUMBER := 0;
   L_list_codetype_code_nodesc              LIST_CODETYPE_CODE_CODEDESC;
   ---
   output_table_index                       NUMBER := 0;
   L_codetype_code_codedesc                 CODETYPE_CODE_CODEDESC_RECORD;
   L_list_codetype_code_codedesc            LIST_CODETYPE_CODE_CODEDESC;

BEGIN

   --Initialize the list variables.
   ---
   L_list_codetype_code_nodesc   := list_codetype_code_codedesc();
   L_list_codetype_code_codedesc := list_codetype_code_codedesc();
   ---

   L_list_codetype_code_nodesc := I_list_codetype_code_nodesc;
   ---
   for i in 1..L_list_codetype_code_nodesc.count loop

      L_current_code_desc    := NULL;
      ---
      if LANGUAGE_SQL.GET_CODE_DESC (L_error_message,
                                     L_list_codetype_code_nodesc(i).code_type,
                                     L_list_codetype_code_nodesc(i).code,
                                     L_current_code_desc) = FALSE then
         ---
         O_error_message := L_error_message;
         return FALSE;
         ---
      end if;
      ---
      --Create a record from the information
      ---
      L_codetype_code_codedesc.code_type         := L_list_codetype_code_nodesc(i).code_type;
      L_codetype_code_codedesc.code              := L_list_codetype_code_nodesc(i).code;
      L_codetype_code_codedesc.return_to         := L_list_codetype_code_nodesc(i).return_to;
      L_codetype_code_codedesc.code_desc         := L_current_code_desc;
            ---
      --Add this record to the list of records
      ---
      L_list_codetype_code_codedesc.extend;
      output_table_index  := output_table_index  +1;
      L_list_codetype_code_codedesc(output_table_index) := L_codetype_code_codedesc;
   end loop;
   ---

   O_list_codetype_code_withdesc := L_list_codetype_code_codedesc;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MANY_CODE_DESC;
--------------------------------------------------------------------
END LANGUAGE_SQL;
/
