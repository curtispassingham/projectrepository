CREATE OR REPLACE PACKAGE BODY L10N_FLEX_ATTRIB_VAL_SQL AS
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FIELD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_attrib_name       IN      L10N_ATTRIB.VIEW_COL_NAME%TYPE,
                        I_attrib_value      IN      VARCHAR2)
RETURN BOOLEAN AS

   L_program                 VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.VALIDATE_FIELD';

   L_index                   L10N_ATTRIB.VIEW_COL_NAME%TYPE;
   L_data_type               L10N_ATTRIB.DATA_TYPE%TYPE;
   L_max_length              L10N_ATTRIB.MAXIMUM_LENGTH%TYPE;
   L_lowest_allowed_value    L10N_ATTRIB.LOWEST_ALLOWED_VALUE%TYPE;
   L_highest_allowed_value   L10N_ATTRIB.HIGHEST_ALLOWED_VALUE%TYPE;
   ---
   L_validate_date           DATE;
   L_earliest_date           DATE;
   L_latest_date             DATE;
   ---
   L_date                    DATE;
   L_number                  NUMBER(30);
   L_func_name               L10N_ATTRIB.VALIDATION_CODE%TYPE;
BEGIN
   ---
   L_index := I_attrib_name;
   ---
   L_data_type             := LP_attrib_val_tbl(L_index).data_type;
   
   L_max_length            := LP_attrib_val_tbl(L_index).maximum_length;
   L_lowest_allowed_value  := LP_attrib_val_tbl(L_index).lowest_allowed_value;
   L_highest_allowed_value := LP_attrib_val_tbl(L_index).highest_allowed_value;
   L_func_name             := LP_attrib_val_tbl(L_index).validation_code;

   if L_data_type = L10N_FLEX_ATTRIB_SQL.LP_num_data_type then 
      BEGIN
         L_number := to_number(I_attrib_value);
      EXCEPTION
         when others then
            O_error_message := SQL_LIB.CREATE_MSG('NON_NUMERIC', --change
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
      END;
   end if;

   if L_data_type = L10N_FLEX_ATTRIB_SQL.LP_date_data_type then 
      BEGIN
         L_date := to_date(I_attrib_value);
      EXCEPTION
         when others then
            O_error_message := SQL_LIB.CREATE_MSG('ENTER_DATE', --change
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
      END;
   end if;

   if (L_data_type = L10N_FLEX_ATTRIB_SQL.LP_char_data_type and
      length(I_attrib_value) > L_max_length) or
      (L_data_type = L10N_FLEX_ATTRIB_SQL.LP_num_data_type and
      length(to_number(I_attrib_value)) > L_max_length) then
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_LENGTH', --change
                                            L_max_length,
                                            NULL,
                                            NULL);
      return FALSE;
   
   end if;
   ---
   if L_data_type = L10N_FLEX_ATTRIB_SQL.LP_num_data_type then
      if to_number(I_attrib_value) < L_lowest_allowed_value then
         O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_LOWEST', --change
                                               L_lowest_allowed_value,
                                               NULL,
                                               NULL);
         return FALSE;
         
       end if;
       ---
       if to_number(I_attrib_value) > L_highest_allowed_value then
          O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_HIGHEST', --change
                                                L_highest_allowed_value,
                                                NULL,
                                                NULL);
          return FALSE;
            
       end if;
   end if;      
   ---
   if L_data_type = L10N_FLEX_ATTRIB_SQL.LP_date_data_type then
      L_validate_date := I_attrib_value;
      L_earliest_date := to_date(to_char(L_lowest_allowed_value),'YYYYMMDD');
      L_latest_date   := to_date(to_char(L_highest_allowed_value),'YYYYMMDD');
      ---
      if L_validate_date < L_earliest_date then
         O_error_message := SQL_LIB.CREATE_MSG('DATE_NOT_BEFORE', --change
                                               L_earliest_date,
                                               NULL,
                                               NULL);
         return FALSE;
         
      end if;
      ---
      if L_validate_date > L_latest_date then
         O_error_message := SQL_LIB.CREATE_MSG('DATE_NOT_AFTER', --change
                                               L_latest_date,
                                               NULL,
                                               NULL);
         return FALSE;
      
      end if;
   
   end if;

   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END VALIDATE_FIELD;
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP(O_error_message          IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                        I_group_id               IN              L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN AS

   L_program         VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.VALIDATE_GROUP';

   L_index           VARCHAR2(25);
   L_desc            L10N_ATTRIB_DESCS.DESCRIPTION%TYPE;
   L_func_name       L10N_ATTRIB_GROUP.GROUP_VALIDATION_CODE%TYPE;

   cursor C_GET_GROUP_VAL_FUNC is
      select group_validation_code
        from l10n_attrib_group
       where group_id = I_group_id;
       
BEGIN
   ---
   L_index := LP_attrib_val_tbl.first;

   LOOP
      exit when L_index is NULL;
      ---
      if LP_attrib_val_tbl(L_index).group_id = I_group_id then
         if LP_attrib_val_tbl(L_index).value_req = 'Y' and LP_attrib_val_tbl(L_index).field_value is null then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                                  LP_attrib_val_tbl(L_index).attrib_desc,
                                                  NULL,
                                                  NULL);
            return FALSE;
            
         end if;
         
      end if;
      ---
      L_index := LP_attrib_val_tbl.next(L_index);
      --- 
   END LOOP;

   -- validate field using validation function
   open C_GET_GROUP_VAL_FUNC;
   fetch C_GET_GROUP_VAL_FUNC into L_func_name;
   close C_GET_GROUP_VAL_FUNC;
   ---
   if L_func_name is NOT NULL then

      if EXEC_FUNC(O_error_message,
                   L_func_name) = FALSE then
         return FALSE;
      end if;
      
   end if; 
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END VALIDATE_GROUP;
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_base_rms_table    IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                        I_base_ind          IN      L10N_ATTRIB_GROUP.BASE_IND%TYPE DEFAULT NULL)
        
RETURN BOOLEAN IS

   L_program         VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.VALIDATE_GROUP';

   cursor C_GET_GROUP is
      select group_id
        from l10n_attrib_group atg,
             ext_entity ent 
       where atg.ext_entity_id = ent.ext_entity_id
         and ent.base_rms_table = I_base_rms_table
         and atg.country_id = I_country_id
         and atg.base_ind = NVL(I_base_ind,atg.base_ind);
       
BEGIN
   ---
   for rec in C_GET_GROUP loop
    
      if VALIDATE_GROUP(O_error_message,
                        rec.group_id) = FALSE THEN
         return FALSE;  	    	                                                    
      end if;

   end loop;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END VALIDATE_GROUP;
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTITY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_country_id        IN      L10N_EXT_ENTITY_VAL.COUNTRY_ID%TYPE)

RETURN BOOLEAN AS

   L_program         VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.VALIDATE_ENTITY';

   L_index           VARCHAR2(25);
   L_desc            L10N_ATTRIB_DESCS.DESCRIPTION%TYPE;
   L_func_name       L10N_EXT_ENTITY_VAL.CUSTOM_VALIDATION_CODE%TYPE;

   cursor C_GET_ENTITY_VAL_FUNC is
      select custom_validation_code
        from l10n_ext_entity_val val,
             ext_entity ent
       where ent.base_rms_table = I_base_table
         and ent.ext_entity_id = val.ext_entity_id 
         and val.country_id = I_country_id;
       
BEGIN
   ---
   open C_GET_ENTITY_VAL_FUNC;
   fetch C_GET_ENTITY_VAL_FUNC into L_func_name;
   close C_GET_ENTITY_VAL_FUNC;
   ---
   if L_func_name is NOT NULL then

      if EXEC_FUNC(O_error_message,
                   L_func_name) = FALSE then
         return FALSE;
      end if;
      
   end if; 
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END VALIDATE_ENTITY;
--------------------------------------------------------------------------------------------------
FUNCTION GET_VALIDATE_DATA(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_attrib_data_type     IN OUT  L10N_ATTRIB.DATA_TYPE%TYPE,
                           O_char_value           IN OUT  VARCHAR2,
                           O_num_value            IN OUT  NUMBER,
                           O_date_value           IN OUT  DATE,
                           I_attrib_rec           IN      OBJ_L10N_ATTRIB_REC)
RETURN BOOLEAN IS

   L_program     VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.GET_VALIDATE_DATA';

BEGIN
   ---
/*
   if I_attrib_rec.val_attrib_col between 1 and 10 then
      O_attrib_data_type := L10N_FLEX_ATTRIB_SQL.LP_char_data_type;
   elsif I_attrib_rec.val_attrib_col between 11 and 20 then
      O_attrib_data_type := L10N_FLEX_ATTRIB_SQL.LP_num_data_type;
   elsif I_attrib_rec.val_attrib_col in (21,22) then
      O_attrib_data_type := L10N_FLEX_ATTRIB_SQL.LP_date_data_type;
   end if;
   ---

   if I_attrib_rec.val_attrib_col = 1 then
      O_char_value := I_attrib_rec.varchar2_1_value;
   elsif I_attrib_rec.val_attrib_col = 2 then
      O_char_value := I_attrib_rec.varchar2_2_value;
   elsif I_attrib_rec.val_attrib_col = 3 then
      O_char_value := I_attrib_rec.varchar2_3_value;
   elsif I_attrib_rec.val_attrib_col = 4 then
      O_char_value := I_attrib_rec.varchar2_4_value;
   elsif I_attrib_rec.val_attrib_col = 5 then
      O_char_value := I_attrib_rec.varchar2_5_value;
   elsif I_attrib_rec.val_attrib_col = 6 then
      O_char_value := I_attrib_rec.varchar2_6_value;
   elsif I_attrib_rec.val_attrib_col = 7 then
      O_char_value := I_attrib_rec.varchar2_7_value;
   elsif I_attrib_rec.val_attrib_col = 8 then
      O_char_value := I_attrib_rec.varchar2_8_value;
   elsif I_attrib_rec.val_attrib_col = 9 then
      O_char_value := I_attrib_rec.varchar2_9_value;
   elsif I_attrib_rec.val_attrib_col = 10 then
      O_char_value := I_attrib_rec.varchar2_10_value;
   elsif I_attrib_rec.val_attrib_col = 11 then
      O_num_value := I_attrib_rec.number_11_value;
   elsif I_attrib_rec.val_attrib_col = 12 then
      O_num_value := I_attrib_rec.number_12_value;
   elsif I_attrib_rec.val_attrib_col = 13 then
      O_num_value := I_attrib_rec.number_13_value;
   elsif I_attrib_rec.val_attrib_col = 14 then
      O_num_value := I_attrib_rec.number_14_value;
   elsif I_attrib_rec.val_attrib_col = 15 then
      O_num_value := I_attrib_rec.number_15_value;
   elsif I_attrib_rec.val_attrib_col = 16 then
      O_num_value := I_attrib_rec.number_16_value;
   elsif I_attrib_rec.val_attrib_col = 17 then
      O_num_value := I_attrib_rec.number_17_value;
   elsif I_attrib_rec.val_attrib_col = 18 then
      O_num_value := I_attrib_rec.number_18_value;
   elsif I_attrib_rec.val_attrib_col = 19 then
      O_num_value := I_attrib_rec.number_19_value;
   elsif I_attrib_rec.val_attrib_col = 20 then
      O_num_value := I_attrib_rec.number_20_value;
   elsif I_attrib_rec.val_attrib_col = 21 then
      O_date_value := I_attrib_rec.date_21_value;
   elsif I_attrib_rec.val_attrib_col = 22 then
      O_date_value := I_attrib_rec.date_22_value;
   end if;
*/
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_VALIDATE_DATA;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_INIT(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_attrib_cfg_tbl    IN OUT  L10N_FLEX_ATTRIB_SQL.TYP_L10N_ATTRIB_CFG_TBL)
RETURN BOOLEAN IS

   L_program             VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.VALIDATE_INIT';
   L_val_index           L10N_ATTRIB.VIEW_COL_NAME%TYPE;
   L_index               VARCHAR2(25);

BEGIN
   LP_attrib_val_tbl.delete;
  
   if IO_attrib_cfg_tbl.count > 0 then
      L_index := IO_attrib_cfg_tbl.first;
   	---
   	loop
   	   exit when L_index is NULL;   	 

         L_val_index := IO_attrib_cfg_tbl(L_index).view_col_name;

         LP_attrib_val_tbl(L_val_index).group_id                := IO_attrib_cfg_tbl(L_index).group_id;
         LP_attrib_val_tbl(L_val_index).view_col_name           := IO_attrib_cfg_tbl(L_index).view_col_name;
         LP_attrib_val_tbl(L_val_index).data_type               := IO_attrib_cfg_tbl(L_index).data_type;
         LP_attrib_val_tbl(L_val_index).attrib_desc             := IO_attrib_cfg_tbl(L_index).attrib_desc;
         LP_attrib_val_tbl(L_val_index).attrib_storage_col      := IO_attrib_cfg_tbl(L_index).attrib_storage_col;
         LP_attrib_val_tbl(L_val_index).ui_widget               := IO_attrib_cfg_tbl(L_index).ui_widget;
         LP_attrib_val_tbl(L_val_index).value_req               := IO_attrib_cfg_tbl(L_index).value_req;
         LP_attrib_val_tbl(L_val_index).maximum_length          := IO_attrib_cfg_tbl(L_index).maximum_length;
         LP_attrib_val_tbl(L_val_index).lowest_allowed_value    := IO_attrib_cfg_tbl(L_index).lowest_allowed_value;
         LP_attrib_val_tbl(L_val_index).highest_allowed_value   := IO_attrib_cfg_tbl(L_index).highest_allowed_value;
         LP_attrib_val_tbl(L_val_index).validation_code         := IO_attrib_cfg_tbl(L_index).attrib_validation_code;
         LP_attrib_val_tbl(L_val_index).field_value             := IO_attrib_cfg_tbl(L_index).attrib_value;

         L_index := IO_attrib_cfg_tbl.next(L_index); 
      end loop;
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_INIT;
--------------------------------------------------------------------------------------------------
FUNCTION SET_FIELD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   IO_attrib_val_tbl   IN OUT  TBL_attrib_val_tbl,
                   I_attrib_name       IN      L10N_ATTRIB.VIEW_COL_NAME%TYPE,
                   I_attrib_value      IN      VARCHAR2)
RETURN BOOLEAN AS
   L_index            L10N_ATTRIB.VIEW_COL_NAME%TYPE;
   L_func_name        L10N_ATTRIB.VALIDATION_CODE%TYPE := NULL;
   L_program          VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.SET_FIELD';

BEGIN

   LP_return_fields_tbl.delete;

   if REFRESH_VALUE(O_error_message,
                    I_attrib_name,
                    I_attrib_value) = FALSE then
      return FALSE;
   end if;

   L_index := I_attrib_name;
   LP_attrib_val_tbl(L_index).field_value := I_attrib_value;


   L_func_name := LP_attrib_val_tbl(L_index).validation_code;
   if L_func_name is NOT NULL then
 
      if EXEC_FUNC(O_error_message,
                   L_func_name) = FALSE then
         return FALSE;
      end if;

      if LP_return_fields_tbl is NOT NULL and LP_return_fields_tbl.count > 0 then
         for i in LP_return_fields_tbl.first..LP_return_fields_tbl.last loop
            L_index := LP_return_fields_tbl(i).field_name;

            IO_attrib_val_tbl(i).group_id                := LP_attrib_val_tbl(L_index).group_id;
            IO_attrib_val_tbl(i).view_col_name           := LP_attrib_val_tbl(L_index).view_col_name;
            IO_attrib_val_tbl(i).data_type               := LP_attrib_val_tbl(L_index).data_type;
            IO_attrib_val_tbl(i).attrib_desc             := LP_attrib_val_tbl(L_index).attrib_desc;
            IO_attrib_val_tbl(i).attrib_storage_col      := LP_attrib_val_tbl(L_index).attrib_storage_col;
            IO_attrib_val_tbl(i).ui_widget               := LP_attrib_val_tbl(L_index).ui_widget;
            IO_attrib_val_tbl(i).value_req               := LP_attrib_val_tbl(L_index).value_req;
            IO_attrib_val_tbl(i).maximum_length          := LP_attrib_val_tbl(L_index).maximum_length;
            IO_attrib_val_tbl(i).lowest_allowed_value    := LP_attrib_val_tbl(L_index).lowest_allowed_value;
            IO_attrib_val_tbl(i).highest_allowed_value   := LP_attrib_val_tbl(L_index).highest_allowed_value;
            IO_attrib_val_tbl(i).validation_code         := LP_attrib_val_tbl(L_index).validation_code;
            IO_attrib_val_tbl(i).field_value             := LP_attrib_val_tbl(L_index).field_value;
            IO_attrib_val_tbl(i).field_desc              := LP_attrib_val_tbl(L_index).field_desc;
         end loop;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SET_FIELD;
--------------------------------------------------------------------------------------------------
FUNCTION SET_OUTPUT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_field              IN       VARCHAR2,
                    I_field_value        IN       VARCHAR2,
                    I_field_desc_value   IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program        VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.SET_OUTPUT';
   L_error_message  rtk_errors.rtk_text%type;

BEGIN

   LP_attrib_val_tbl(I_field).field_value := I_field_value;
   LP_attrib_val_tbl(I_field).field_desc  := I_field_desc_value;

   LP_return_fields_tbl(LP_return_fields_tbl.count).field_name := I_field;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SET_OUTPUT; 
--------------------------------------------------------------------------------------------------
FUNCTION GET_VALUE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_field_value       IN OUT   VARCHAR2,
                   O_field_desc_value  IN OUT   VARCHAR2,
                   O_data_type         IN OUT   L10N_ATTRIB.DATA_TYPE%TYPE,
                   I_field_name        IN       L10N_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN AS

   L_program        VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.GET_VALUE';

BEGIN

   if LP_attrib_val_tbl.exists(I_field_name) then
      O_field_value      := LP_attrib_val_tbl(I_field_name).field_value;
      O_field_desc_value := LP_attrib_val_tbl(I_field_name).field_desc;
      O_data_type        := LP_attrib_val_tbl(I_field_name).data_type;
   elsif LP_header_cfg_tbl.exists(I_field_name) then
      O_field_value      := LP_header_cfg_tbl(I_field_name).key_value;
      O_field_desc_value := LP_header_cfg_tbl(I_field_name).key_value_desc;
      O_data_type        := LP_header_cfg_tbl(I_field_name).data_type;
   else
      O_error_message := SQL_LIB.CREATE_MSG('FIELD_NOT_DEFINED');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_VALUE; 
--------------------------------------------------------------------------------------
FUNCTION REFRESH_VALUE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_field_name        IN       L10N_ATTRIB.VIEW_COL_NAME%TYPE,
                       I_field_value       IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program        VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.REFRESH_VALUE';

BEGIN

   LP_attrib_val_tbl(I_field_name).field_value := I_field_value;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END REFRESH_VALUE;
--------------------------------------------------------------------------------------
FUNCTION BUILD_KEY_TABLE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN AS

   L_program       VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.BUILD_KEY_TABLE';
   L_index         L10N_ATTRIB.VIEW_COL_NAME%TYPE;

BEGIN

   if L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl is NOT NULL and L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl.count > 0 then

      for i in L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl.FIRST .. L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl.LAST loop
         if L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl(i).header_type = L10N_FLEX_ATTRIB_SQL.LP_key_widget then

            L_index := L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl(i).key_col;

            LP_header_cfg_tbl(L_index).key_col        := L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl(i).key_col;
            LP_header_cfg_tbl(L_index).data_type      := L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl(i).data_type;
            LP_header_cfg_tbl(L_index).key_value      := L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl(i).key_value;
            LP_header_cfg_tbl(L_index).key_value_desc := L10N_FLEX_ATTRIB_SQL.LP_header_cfg_tbl(i).key_value_desc;

         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_KEY_TABLE;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FUNC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   L_func_name         IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program          VARCHAR2(62) := 'L10N_FLEX_ATTRIB_VAL_SQL.EXEC_FUNC';
   L_statement        VARCHAR2(500);
   L_return_code      VARCHAR2(6);
BEGIN

      L_statement := 'BEGIN if '||L_func_name||'(:O_error_message) = FALSE then :L_return_code := ''FALSE''; end if; END;';

      EXECUTE IMMEDIATE L_statement
         USING IN OUT O_error_message,
               IN OUT L_return_code;
      ---
      if L_return_code = 'FALSE' then
         return FALSE;
         
      end if;
      --
      return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END EXEC_FUNC;
--------------------------------------------------------------------------------------
END L10N_FLEX_ATTRIB_VAL_SQL;
/