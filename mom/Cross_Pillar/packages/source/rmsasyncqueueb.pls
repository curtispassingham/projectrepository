CREATE OR REPLACE PACKAGE BODY RMS_ASYNC_QUEUE_SQL AS
------------------------------------------------------------------------------------------------
FUNCTION CREATE_QUEUE_SUBSCRIBER(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_job_type            IN     RMS_ASYNC_JOB.JOB_TYPE%TYPE, 
                                 I_register_event_ind  IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program              VARCHAR2(61)  := 'RMS_ASYNC_QUEUE_SQL.CREATE_QUEUE_SUBSCRIBER';

   L_queue_table          RMS_ASYNC_JOB.QUEUE_TABLE%TYPE;
   L_queue_name           RMS_ASYNC_JOB.QUEUE_NAME%TYPE;
   L_recipient            RMS_ASYNC_JOB.RECIPIENT_NAME%TYPE;
   L_queue_payload_type   RMS_ASYNC_JOB.QUEUE_PAYLOAD_TYPE%TYPE;
   L_event                RMS_ASYNC_JOB.DEQUEUE_EVENT%TYPE;

   L_queue_count          NUMBER        := 0;
   L_queue_table_count    NUMBER        := 0;
   L_subscriber_count     NUMBER        := 0;

   cursor C_JOB_QUEUE is
      select queue_table,
             queue_name,
             recipient_name,
             queue_payload_type,
             dequeue_event
        from rms_async_job,
             system_config_options
       where job_type = I_job_type;
   
   cursor C_SUBSCRIBER_EXISTS is
      select count(1)
        from dba_queue_subscribers  
       where owner = USER
         and queue_name = L_queue_name;

   cursor C_QUEUE_EXISTS is
      select count(1) 
        from dba_queues
       where owner = USER
         and name  = L_queue_name;

   cursor C_QUEUE_TABLE_EXISTS is
      select count(1)
        from dba_queue_tables
       where owner = USER
         and queue_table = L_queue_table;

BEGIN

   if I_job_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_job_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_JOB_QUEUE;
   fetch C_JOB_QUEUE into L_queue_table,
                          L_queue_name,
                          L_recipient,
                          L_queue_payload_type,
                          L_event;
   close C_JOB_QUEUE;

   if L_queue_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ASYNC_JOB',   
                                            I_job_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   ----------------------------------------------------------------------------------
   -- DROP EXISTING QUEUE SUBSCRIBERS
   ----------------------------------------------------------------------------------
   open C_SUBSCRIBER_EXISTS;
   fetch C_SUBSCRIBER_EXISTS into L_subscriber_count;
   close C_SUBSCRIBER_EXISTS;
      
   if L_subscriber_count > 0 then
      DBMS_AQADM.REMOVE_SUBSCRIBER
         (QUEUE_NAME => USER||'.'||L_queue_name,
          SUBSCRIBER => sys.aq$_agent(L_recipient,null,null));
   end if;

   ----------------------------------------------------------------------------------
   -- STOP AND DROP EXISTING QUEUES
   ----------------------------------------------------------------------------------
   open C_QUEUE_EXISTS;
   fetch C_QUEUE_EXISTS into L_queue_count;
   close C_QUEUE_EXISTS;
         
   if L_queue_count > 0 then
      DBMS_AQADM.STOP_QUEUE
         (QUEUE_NAME => USER||'.'||L_queue_name);
         
      DBMS_AQADM.DROP_QUEUE
         (QUEUE_NAME => USER||'.'||L_queue_name);
   end if;

   ----------------------------------------------------------------------------------
   -- DROP EXISTING QUEUE TABLE
   ----------------------------------------------------------------------------------
   open C_QUEUE_TABLE_EXISTS;
   fetch C_QUEUE_TABLE_EXISTS into L_queue_table_count;
   close C_QUEUE_TABLE_EXISTS;
      
   if L_queue_table_count > 0 then
      DBMS_AQADM.DROP_QUEUE_TABLE
         (QUEUE_TABLE => USER||'.'||L_queue_table);
   end if;
   
   ----------------------------------------------------------------------------------
   -- CREATE QUEUE TABLE
   ----------------------------------------------------------------------------------
   DBMS_AQADM.CREATE_QUEUE_TABLE
      (QUEUE_TABLE        => USER||'.'||L_queue_table,
       QUEUE_PAYLOAD_TYPE => USER||'.'||L_queue_payload_type,
       MULTIPLE_CONSUMERS => TRUE);

   ----------------------------------------------------------------------------------
   -- CREATE AND START QUEUES
   ----------------------------------------------------------------------------------
   DBMS_AQADM.CREATE_QUEUE
      (QUEUE_NAME  => USER||'.'||L_queue_name,
       QUEUE_TABLE => USER||'.'||L_queue_table);
     
   DBMS_AQADM.START_QUEUE
      (QUEUE_NAME => USER||'.'||L_queue_name);

   ----------------------------------------------------------------------------------
   -- CREATE AND REGISTER SUBSCRIBERS TO PL/SQL NOTIFY PACKAGES
   ----------------------------------------------------------------------------------
   DBMS_AQADM.ADD_SUBSCRIBER
      (QUEUE_NAME => USER||'.'||L_queue_name,
       SUBSCRIBER => sys.aq$_agent(L_recipient,null,null));

   --Only register events that will have a specific PL/SQL callback procedure.
   --Otherwise, do NOT register it. This is to handle things like notification queue,
   --which will be registered with rtkstrt on form runtime using the Events object property.         
   if I_register_event_ind = 'Y' then
      DBMS_AQ.REGISTER
         (sys.aq$_reg_info_list(
            sys.aq$_reg_info(USER||'.'||L_queue_name||':'||L_recipient,
                             DBMS_AQ.NAMESPACE_AQ,
                             'plsql://'||USER||'.'||L_event,
                             HEXTORAW('FF'))),
                             --null)),
          1);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_QUEUE_SUBSCRIBER;
-------------------------------------------------------------------------------
END RMS_ASYNC_QUEUE_SQL;
/
