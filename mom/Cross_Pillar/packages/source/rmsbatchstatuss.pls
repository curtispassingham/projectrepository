CREATE OR REPLACE PACKAGE RMS_BATCH_STATUS_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------

-- GP_ind_populated is set to TRUE the first time the rms_batch_status
-- table is hit in this session.  Later calls to this package will access
-- the following package level variables so the rms_batch_status table is
-- not accessed repeatedly.
-- GP_batch_running_ind is a package level variable that holds the value of
-- rms_batch_status which is fetched in the GET_BATCH_RUNNING_IND function.

GP_batch_running_ind    RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE; 

---------------------------------------------------------------------------------
-- Function Name: GET_BATCH_RUNNING_IND
-- Purpose:       Fetches the BATCH_RUNNING_IND from RMS_BATCH_STATUS. 
----------------------------------------------------------------------------------
FUNCTION GET_BATCH_RUNNING_IND(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_batch_running_ind      IN OUT RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END RMS_BATCH_STATUS_SQL;
/
