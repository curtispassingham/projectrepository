CREATE OR REPLACE PACKAGE SQL_LIB AUTHID CURRENT_USER IS

---------------------------------------------------------------------
-- Create message concatenates the message key and parameters
-- to pass the string back to be decoded by emessage. Create
-- message looks like emessage, except all four parameters must 
-- be passed even if the message key accepts fewer than three
-- parameters. In this case NULL parameters must be passed in the
-- unused parameter positions. The NULL defaulting implemented
-- here should allow the user to not have to pass NULLs, but an
-- Oracle bug seems to prevent that from working.
--
-- When create message is used in the OTHERS section of stored
-- procedure exceptions handling it should be called as:
-- O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM,
-- 'hard-code program.function name', SQLCODE). Create message
-- stores the SQLCODE and function name for later retrieval by
-- the batch message program discussed below.
---------------------------------------------------------------------

FUNCTION CREATE_MSG(I_key IN varchar2,
         I_txt_1 IN varchar2 := null,
         I_txt_2 IN varchar2 := null,
         I_txt_3 IN varchar2 := null) RETURN VARCHAR2;

---------------------------------------------------------------------
-- Set mark should be used before any OPEN, FETCH, CLOSE, INSERT
-- UPDATE, DELETE or SELECT statement to enable programs for batch
-- error handliing. All of these parameters will be hard-coded text
-- strings, which is acceptable because they are table names and other
-- words in the system not subject to internationalization.
--
-- Set mark populates several package variables for possible use by
-- the batch message program (desribed later) in getting useful info
-- back when an exception is raised in a stored program unit.
--
-- I_action can be up to six characters and should contain one of the
-- seven words OPEN, FETCH, CLOSE, UPDATE, INSERT, DELETE, SELETE.
--
-- I_cursor is NULL or the cursor name depending on whther a cursor is 
-- being used. The maximum length is 32 characters.
--
-- I_table is the list of relevant tables with a mximum length of 255.
--
-- I_keys contains the values from a where clause or primary key field
-- values on an insert. Potentially these are in a loop are otherwise
-- use variables. An example of I_keys might be 'SKU || to_char(I_sku)
-- || STORE || to_char(IO_store)'. The maximum capacity of this parameter
-- is 120.
---------------------------------------------------------------------

PROCEDURE SET_MARK ( I_action    IN    VARCHAR2,
         I_cursor    IN    VARCHAR2,
         I_table  IN    VARCHAR2,
         I_keys      IN    VARCHAR2);

---------------------------------------------------------------------
-- Batch message fetches the SQLCODE, table and err_data parameters
-- for the batch processing WRITE_ERROR message in the event that a
-- program unit called from batch returns FALSE. BATCH_MSG determines
-- whether the error is handled or unhandled by looking at whether the 
-- a SQLCODE has been returned (i.e., whether O_err_code is at it's
-- default value) and either decodes (handled) O_err_data similarly to 
-- emessage or populates (unhandled) O_err_data with a concatenation
-- of information from the most recent SET_MARK.
---------------------------------------------------------------------

PROCEDURE BATCH_MSG( O_err_code  IN OUT   NUMBER,
         O_table     IN OUT   VARCHAR2,
                        O_err_data  IN OUT   VARCHAR2);

---------------------------------------------------------------------
-- Called by the retek library (rmessage/emessage) and by the BATCH_MSG
-- procedure. Goes to the message key table and decodes the key and its
-- parameters into an intelligible message.
---------------------------------------------------------------------
FUNCTION GET_MESSAGE_TEXT    (   I_key varchar2,
            I_txt_1 varchar2 := null,
            I_txt_2 varchar2 := null,
            I_txt_3 varchar2 := null)
            RETURN VARCHAR2;

---------------------------------------------------------------------
-- Called when a user has a handled, fatal error (in addition to the
-- standard sql_lib.create_msg call) in a package which is called from
-- batch as well as on-line. It is used to get the package name into 
-- memory so that it can be concatenated to the front end of the handled
-- error string when it is returned to a batch program. The error code
-- passed in will generally be NULL, which means it will be changed to
-- 5 by the batch_msg program which represents a generic called package
-- error. Other errors than NULL could be passed, specifically ones
-- which represent genuine Oracle errors which would not otherwise
-- raise a PL/SQL 'OTHERS' exception (such as -1403, NO_DATA_FOUND) but
-- which are deemed 'fatal' (bail out and return FALSE) for the specific 
-- program.
---------------------------------------------------------------------

PROCEDURE HANDLED_ERR (I_package_name IN VARCHAR2,
              I_error_code   IN NUMBER);
---------------------------------------------------------------------
-- Function:   CHECK_NUMERIC
-- Purpose: Check if input field contains non-numeric character.  
--          If it does, this function returns false.  
---------------------------------------------------------------------
FUNCTION CHECK_NUMERIC(O_error_message IN OUT VARCHAR2,
                       I_value         IN     VARCHAR2)
RETURN BOOLEAN;   
---------------------------------------------------------------------

---------------------------------------------------------------------
-- Function: GET_USER_NAME
-- Purpose:  Gets the name of the current User.  
-- Created:  05-OCT-97  Steven B Olson
---------------------------------------------------------------------
FUNCTION GET_USER_NAME(O_error_message IN OUT VARCHAR2,
                       O_user_name     IN OUT VARCHAR2)
                       RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : GET_RF_MSG
-- Description   : Takes an RMS error message key and returns the message text parsed into
--                 up to 5 strings, each of length up to 18 characters,  for display
--                 on a 20 character wide screen (hand-held scanner terminals).  
--                 The 'I_substr#' arguments are the substrings to be inserted into the rtk_text
--                 message in our usual manner. (i.e. they replace %s1, etc.) 
--                 Any excess characters are truncated.  
--               : GET_MSG returns TRUE if processing ended normally.
-- Created by    : Rich Davies,  May 1998
---------------------------------------------------------------------------------------------
FUNCTION GET_RF_MSG (O_line1            OUT       VARCHAR2,
                     O_line2            OUT       VARCHAR2,
                     O_line3            OUT       VARCHAR2,
                     O_line4            OUT       VARCHAR2,
                     O_line5            OUT       VARCHAR2,
                     I_key              IN        rtk_errors.rtk_key%TYPE,
                     I_substr1          IN        varchar2 DEFAULT NULL,
                     I_substr2          IN        varchar2 DEFAULT NULL,
                     I_substr3          IN        varchar2 DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
PROCEDURE API_MSG(O_error_type    IN OUT VARCHAR2,
                  O_error_message IN OUT VARCHAR2);
---------------------------------------------------------------------------------------------
FUNCTION PARSE_MSG (IO_msg IN OUT VARCHAR2,
                    O_key  IN OUT VARCHAR2)
RETURN BOOLEAN ;
---------------------------------------------------------------------------------------------
-- Function Name : PARSE_MSG
--  Description  : This overloaded function will return error message from rtk_errors when a rtk key with 
--                 parameters are passed.. 
---------------------------------------------------------------------------------------------
FUNCTION PARSE_MSG (I_key IN  VARCHAR2)
RETURN VARCHAR2 ;
---------------------------------------------------------------------------------------------

END SQL_LIB;
/


