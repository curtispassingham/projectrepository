CREATE OR REPLACE PACKAGE L10N_FLEX_SETUP_SQL AS
---------------------------------------------------------------------------------------------------
ERRNUM_PACKAGE_CALL    NUMBER(6) := -20020;  -- Define error for raise_application_error

TYPE TABLE_TBL IS TABLE OF ALL_TAB_COLUMNS.TABLE_NAME%TYPE INDEX BY BINARY_INTEGER;

TYPE L10N_ATTRIB_DESC_REC IS RECORD
(  attrib_id            L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE,
   view_col_name        L10N_ATTRIB.VIEW_COL_NAME%TYPE,
   lang                 L10N_ATTRIB_DESCS.LANG%TYPE,
   description          L10N_ATTRIB_DESCS.DESCRIPTION%TYPE,
   default_lang_ind     L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE,
   error_message        RTK_ERRORS.RTK_TEXT%TYPE,
   return_code          VARCHAR2(5)
);

TYPE L10N_ATTRIB_DESC_TBL IS TABLE OF L10N_ATTRIB_DESC_REC INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
TYPE L10N_EXT_ENTITY_KEY_REC IS RECORD
(  base_rms_table       EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
   key_col              EXT_ENTITY_KEY_DESCS.KEY_COL%TYPE,
   key_number           EXT_ENTITY_KEY.KEY_NUMBER%TYPE,
   data_type            EXT_ENTITY_KEY.DATA_TYPE%TYPE,
   description_code     EXT_ENTITY_KEY.DESCRIPTION_CODE%TYPE,
   lang                 EXT_ENTITY_KEY_DESCS.LANG%TYPE,
   key_desc             EXT_ENTITY_KEY_DESCS.KEY_DESC%TYPE,
   default_lang_ind     EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE,
   error_message        RTK_ERRORS.RTK_TEXT%TYPE,
   return_code          VARCHAR2(5)
);

TYPE L10N_EXT_ENTITY_KEY_TBL IS TABLE OF L10N_EXT_ENTITY_KEY_REC INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
TYPE L10N_FF_UI_PARAM_REC IS RECORD
(
   base_rms_table       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
   key_value_1          VARCHAR2(60),
   key_value_2          VARCHAR2(60),
   key_value_3          VARCHAR2(60),
   key_value_4          VARCHAR2(60),
   key_value_5          VARCHAR2(60),
   key_value_6          VARCHAR2(60),
   key_value_7          VARCHAR2(60),
   key_value_8          VARCHAR2(60),
   key_value_9          VARCHAR2(60),
   key_value_10         VARCHAR2(60),
   key_value_11         VARCHAR2(60),
   key_value_12         VARCHAR2(60),
   key_value_13         VARCHAR2(60),
   key_value_14         VARCHAR2(60),
   key_value_15         VARCHAR2(60),
   key_value_16         VARCHAR2(60),
   key_value_17         VARCHAR2(60),
   key_value_18         VARCHAR2(60),
   key_value_19         VARCHAR2(60),
   key_value_20         VARCHAR2(60)
);

TYPE L10N_FF_UI_PARAM_TBL IS TABLE OF L10N_FF_UI_PARAM_REC INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ENTITY_KEY_DESCS (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
                                  I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ENTITY_KEY_DESCS  (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
                                  I_key                     IN        EXT_ENTITY_KEY_DESCS.KEY_COL%TYPE,
                                  I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ENTITY_KEY_DESCS (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                  I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE,
                                  I_default_ind             IN        EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE DELETE_ENTITY_KEY_DESCS (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table           IN        EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                  I_key_col                  IN        EXT_ENTITY_KEY_DESCS.KEY_COL%TYPE,
                                  I_lang                     IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB_DESCS (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_group_id              IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB_DESCS  (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_group_id              IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_attrib_id             IN        L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB_DESCS (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_group_id              IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE,
                              I_default_lang_ind      IN        L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE DELETE_ATTRIB_DESCS (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_attrib_id             IN        L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION GEN_EXT_TABLE (O_error_message     IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists            IN OUT    BOOLEAN,
                        I_table             IN        EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        I_ext_table         IN        EXT_ENTITY.L10N_EXT_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_VIEW (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                   I_table             IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                   I_ext_table         IN      EXT_ENTITY.L10N_EXT_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_VIEW_GROUP (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_group_id          IN      L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_INSERT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table        IN       EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_EXT_ID(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_entity_id        IN OUT   EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_UPDATE(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table        IN        EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_rms_table    IN        EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_ext_table    IN        EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                         I_ext_ent_id       IN        EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_DELETE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_base_rms_table    IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_ext_ent_id        IN       EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_EXT_TABLE_NAME (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_l10n_ext_table   IN OUT   EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                             I_base_rms_table   IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_EXT_KEY_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_default_ind     IN OUT    EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE,
                                  I_base_rms_table  IN        EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
                                  I_lang            IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ENTITY_DEFAULT_LANG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists         IN OUT VARCHAR2,
                                    I_base_rms_table IN     EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_GROUP_ID(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           O_group_id         IN OUT    L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GRP_DISP_ORD_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   VARCHAR2,
                            I_ext_entity_id   IN       L10N_ATTRIB_GROUP.EXT_ENTITY_ID%TYPE,
                            I_country_id      IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                            I_group_id        IN       L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                            I_disp_order      IN       L10N_ATTRIB_GROUP.DISPLAY_ORDER%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_GRP_DESC_LANG(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_group_id         IN       L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_lang             IN       L10N_ATTRIB_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GROUP_VIEW_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist            IN OUT   VARCHAR2,
                           I_grp_view_name    IN       L10N_ATTRIB_GROUP.GROUP_VIEW_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_GROUP_DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_group_id         IN       L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_GROUP_DESCS(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist            IN OUT    VARCHAR2,
                            I_group_id         IN        L10N_ATTRIB_GROUP_DESCS.GROUP_ID%TYPE,
                            I_lang             IN        L10N_ATTRIB_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_GROUP_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_group_id        IN     L10N_ATTRIB_GROUP.GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION ENTITY_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exist            IN OUT   VARCHAR2,
                      I_ext_entity       IN       EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_AVAIL_METADATA_COL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_attrib_storage_col   IN OUT   L10N_ATTRIB.ATTRIB_STORAGE_COL%TYPE,
                                I_group_id             IN       L10N_ATTRIB.GROUP_ID%TYPE,
                                I_data_type            IN       L10N_ATTRIB.DATA_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_EXT_TBL(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists              IN OUT    VARCHAR2, 
                            O_base_rms_table      IN OUT    EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                            O_l10n_ext_table      IN OUT    EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                            I_ext_entity_id       IN        EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_GROUP_INFO(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                               O_country_desc        IN OUT    COUNTRY.COUNTRY_DESC%TYPE,
                               O_base_rms_table      IN OUT    EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                               O_l10n_ext_table      IN OUT    EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                               O_group_desc          IN OUT    L10N_ATTRIB_GROUP_DESCS.DESCRIPTION%TYPE,
                               IO_country_id         IN OUT    COUNTRY.COUNTRY_ID%TYPE,
                               IO_ext_entity_id      IN OUT    EXT_ENTITY.EXT_ENTITY_ID%TYPE,
                               I_group_id            IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ATTRIB_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_attrib_id       IN OUT   L10N_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ATTRIB_DESCS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_id        IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                             I_lang            IN        L10N_ATTRIB_DESCS.LANG%TYPE,
                             I_default_ind     IN OUT    L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_DESCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_attrib_id       IN       L10N_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_DESC_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_default_ind     IN OUT    L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE,
                                      I_group_id        IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                                      I_lang            IN        L10N_ATTRIB_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ATTRIB_DESCS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist           IN OUT    VARCHAR2,
                             I_group_id        IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                             I_lang            IN        L10N_ATTRIB_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_attrib_id       IN     L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_RECGRP_EXISTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT VARCHAR2,
                                  I_rec_grp_id      IN     L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_CODETYPE_EXISTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists             IN OUT VARCHAR2,
                                    I_code_type_id       IN     L10N_CODE_HEAD.L10N_CODE_TYPE%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------

FUNCTION ATTRIB_DISP_SEQ_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist           IN OUT   VARCHAR2,
                               I_group_id        IN       L10N_ATTRIB.GROUP_ID%TYPE,
                               I_display_seq     IN       L10N_ATTRIB.DISPLAY_SEQ%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION VIEW_COL_NAME_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist             IN OUT   VARCHAR2,
                             I_group_id          IN       L10N_ATTRIB.GROUP_ID%TYPE,
                             I_view_col_name     IN       L10N_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DISPLAY_SEQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_display_seq     IN OUT   L10N_ATTRIB.DISPLAY_SEQ%TYPE,
                              I_group_id        IN       L10N_ATTRIB.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION SET_FF_UI_PARAMETERS(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_ff_ui_param_rec    IN OUT   NOCOPY   L10N_FF_UI_PARAM_REC,
                              I_base_rms_table      IN                EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CODE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_code_type      IN     L10N_CODE_HEAD.L10N_CODE_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CODE_DELETE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_code_type     IN     L10N_CODE_HEAD.L10N_CODE_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CODE_LANG_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_code_type     IN     L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE,
                          I_code          IN     L10N_CODE_DETAIL_DESCS.L10N_CODE%TYPE,
                          I_code_lang     IN     L10N_CODE_DETAIL_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CODE_DISP_SEQ_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_type     IN     L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE,
                              I_code_seq      IN      L10N_CODE_DETAIL_DESCS.SEQ_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_CODE_DEFAULT_LANG(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_code_type     IN     L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_CODE_DESCS(O_error_message    IN OUT 	RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT	VARCHAR2,
                           I_code_type        IN    	L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE,
                           I_lang             IN        L10N_CODE_DETAIL_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_NEXT_RECGRP_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_rec_grp_id    IN OUT L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION REC_GROUP_DELETE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_rec_grp_id    IN     L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION REC_GROUP_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rec_grp_id      IN     L10N_REC_GROUP_DESCS.L10N_REC_GROUP_ID%TYPE,
                        I_lang            IN     L10N_REC_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_RG_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_l10n_rg_id      IN     L10N_REC_GROUP_DESCS.L10N_REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_RECGRP_DESCS(O_error_message    IN OUT 	RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists           IN OUT	VARCHAR2,
                             I_recgrp_id        IN    	L10N_REC_GROUP_DESCS.L10N_REC_GROUP_ID%TYPE,
                             I_lang             IN          L10N_REC_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  GEN_STG_L10N_EXT_TBL
-- Purpose      :  This function generates a dynamic DDL statement based on the extension attributes 
--                 to create a country-specific staging table for a given entity. The staging table
--                 will be used to hold localization data when importing/exporting data to/from RMS.
-- Inputs       :  I_country_id (localization extension country id)
--                 I_base_table (RMS base table name)
---------------------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                              I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  GEN_STG_L10N_EXT_TBL
-- Purpose      :  This function generates DDL statements for creating localization staging tables 
--                 for all entities of a given localization extension country.
-- Inputs       :  I_country_id (localization extension country id)
---------------------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  GEN_L10N_EXT_SEQ
-- Purpose      :  This function generates DDL statements for creating process id sequences 
--                 for all entities of a given localization extension country.
-- Inputs       :  I_country_id (localization extension country id)
---------------------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_SEQ(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name: GET_REC_GROUP_QUERY
-- Purpose      : This function would accept the record group ID and pass the query as the output. 
-- Inputs       : I_l10n_rec_group_id and I_country_id
---------------------------------------------------------------------------------------------------
FUNCTION GET_REC_GROUP_QUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                             I_l10n_rec_group_id   IN     L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE,
                             I_country_id          IN     L10N_REC_GROUP.COUNTRY_ID%TYPE, 
                             O_query               OUT    L10N_REC_GROUP.QUERY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function name: GET_WIDGET
-- Purpose      : This function gets the type of widget and their their data type,max length etc.depending on the column name. 
--------------------------------------------------------------------------------------------------
FUNCTION GET_WIDGET(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_widget              IN OUT L10N_ATTRIB.UI_WIDGET%TYPE,
                    O_rec_group_id        IN OUT L10N_ATTRIB.L10N_REC_GROUP_ID%TYPE,
		        O_code_type           IN OUT L10N_ATTRIB.L10N_CODE_TYPE%TYPE,
                    O_data_type           IN OUT L10N_ATTRIB.DATA_TYPE%TYPE,
                    I_name                IN     L10N_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function name: CHK_CUSTOM_ATTRIB
-- Purpose      : This function checks if there are any custom attributes for an entity. 
--------------------------------------------------------------------------------------------------
FUNCTION CHK_CUSTOM_ATTRIB(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            IN OUT BOOLEAN,
                           I_base_table        IN     EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                           I_country_id        IN     L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                           I_view_col_name     IN     L10N_ATTRIB.VIEW_COL_NAME%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
END L10N_FLEX_SETUP_SQL;
/