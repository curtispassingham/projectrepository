CREATE OR REPLACE PACKAGE CFA_SETUP_GROUP_SET_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_GRP_SET_ID(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_group_set_id   IN OUT CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_GRP_SET_DISP_ORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist           IN OUT   VARCHAR2,
                              I_ext_entity_id   IN       CFA_ATTRIB_GROUP_SET.EXT_ENTITY_ID%TYPE,
                              I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                              I_disp_seq        IN       CFA_ATTRIB_GROUP_SET.DISPLAY_SEQ%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_GRP_SET_LABEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_label           IN OUT   CFA_ATTRIB_GROUP_SET_LABELS.LABEL%TYPE,
                           I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                           I_lang            IN       CFA_ATTRIB_GROUP_SET_LABELS.LANG%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--Function Name: CHK_NO_GROUP_SET_LABELS
--Purpose      : This function checks that the inputted group_set_id has labels set up for all attributes. 
--               If no group set/lang are passed in, it will check across all group sets.  If a group
--               set is found that does not have associated labels, O_exist is set to Y, and  
--               IO_group_set_id will contain the group set with missing labels.
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_GROUP_SET_LABELS(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exist            IN OUT    VARCHAR2,
                                 IO_group_set_id    IN OUT    CFA_ATTRIB_GROUP_SET_LABELS.GROUP_SET_ID%TYPE,
                                 I_lang             IN        CFA_ATTRIB_GROUP_SET_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_GRP_SET_LABEL_LANG(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_group_set_id   IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                                    I_lang           IN       CFA_ATTRIB_GROUP_SET_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GROUP_SET_VIEW_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist          IN OUT  VARCHAR2,
                               I_grp_view_name  IN      CFA_ATTRIB_GROUP_SET.GROUP_SET_VIEW_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GROUP_SET_STG_TBL_EXT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist          IN OUT  VARCHAR2,
                               I_grp_stg_name   IN      CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_GRP_SET_LABEL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_group_set_id   IN      CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_GRP_SET_DEFAULT_LANG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_group_set_id   IN     CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_GRP_SET_REC(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_attrib_group_set        IN OUT  CFA_ATTRIB_GROUP_SET%ROWTYPE,
                         O_attrib_group_set_label  IN OUT  CFA_ATTRIB_GROUP_SET_LABELS%ROWTYPE,
                         I_group_set_id            IN      CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
END CFA_SETUP_GROUP_SET_SQL;
/
