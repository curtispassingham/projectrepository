CREATE OR REPLACE PACKAGE BODY SVCPROV_UTILITY AS
--------------------------------------------------------------------------------
--                       PRIVATE GLOBALS VARIABLES                            --
--------------------------------------------------------------------------------

LP_IllegalState           VARCHAR2(255) :=
'com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException';

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURE                                  --
--------------------------------------------------------------------------------
PROCEDURE BUILD_SERVICE_OP_STATUS(IO_ServiceOpStatus  IN OUT NOCOPY  "RIB_ServiceOpStatus_REC",
                                  I_status_code       IN             VARCHAR2,
                                  I_error_message     IN             VARCHAR2,
                                  I_program           IN             VARCHAR2)
IS

   L_program                 VARCHAR2(65)            := 'SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS';

   L_RIB_SuccessStatus_REC   "RIB_SuccessStatus_REC" := NULL;
   L_RIB_FailStatus_REC      "RIB_FailStatus_REC"    := NULL;

BEGIN

   if IO_ServiceOpStatus is NULL then
      IO_ServiceOpStatus := "RIB_ServiceOpStatus_REC"(NULL, NULL);
   end if;     
   
   if I_status_code = API_CODES.SUCCESS then

      L_RIB_SuccessStatus_REC := "RIB_SuccessStatus_REC"(0,
                                                         I_program);

      if IO_ServiceOpStatus.SuccessStatus_TBL is NULL or IO_ServiceOpStatus.SuccessStatus_TBL.COUNT = 0 then
         IO_ServiceOpStatus.SuccessStatus_TBL := "RIB_SuccessStatus_TBL"();
         IO_ServiceOpStatus.SuccessStatus_TBL.extend;
      end if;
      
      -- Webservice expects only one record in SuccessStatus_TBL. Hence hardcoding index as 1 and not using count.
      IO_ServiceOpStatus.SuccessStatus_TBL(1) := L_RIB_SuccessStatus_REC;

   elsif I_error_message is NOT NULL then
   
      if IO_ServiceOpStatus.FailStatus_TBL is NULL or IO_ServiceOpStatus.FailStatus_TBL.COUNT = 0 then
         IO_ServiceOpStatus.FailStatus_TBL := "RIB_FailStatus_TBL"();
         IO_ServiceOpStatus.FailStatus_TBL.extend;
      end if;

      -- Webservice expects only one record in FailStatus_TBL. Hence hardcoding index as 1 and not using count.
      L_RIB_FailStatus_REC := BUILD_FAIL_RECORD(I_error_message,
                                                IO_ServiceOpStatus.FailStatus_TBL(1));
      
      IO_ServiceOpStatus.FailStatus_TBL(1) := L_RIB_FailStatus_REC;

   end if;

END BUILD_SERVICE_OP_STATUS;
--------------------------------------------------------------------------------
FUNCTION BUILD_FAIL_RECORD(I_error_message           IN   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rib_failstatus_rec      IN   "RIB_FailStatus_REC")
RETURN "RIB_FailStatus_REC" IS

   L_program          VARCHAR2(60)                 := 'SVCPROV_UTILITY.BUILD_FAIL_RECORD';

   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE     := I_error_message;
   L_error_type       VARCHAR2(255)                := NULL;
   L_exception_type   VARCHAR2(255)                := NULL;
   L_shorterrormessage VARCHAR2(255)               := NULL;
   L_errordescription  VARCHAR2(500)               := NULL;

   L_businessproblemdetail_tbl "RIB_BusinessProblemDetail_TBL" := NULL;
   L_problem_desc_tbl          "RIB_problemDescription_TBL"    := NULL;
   L_prob_detail_entry_tbl     "RIB_ProblemDetailEntry_TBL"    := NULL;
   
BEGIN
   
   if I_error_message is NULL then
      return I_rib_failstatus_rec;
   end if;
   
   SQL_LIB.API_MSG(L_error_type, L_error_message);
   --
   L_exception_type := LP_IllegalState;
   
   L_shorterrormessage := substr(L_error_message, 1, 255);
   L_errordescription  := substr(L_error_message, 1, 500);

      -- Append the error message to RIB_problemDescription_TBL by extending RIB_problemDescription_TBL
   if I_rib_failstatus_rec.businessproblemdetail_tbl is null or I_rib_failstatus_rec.businessproblemdetail_tbl.COUNT = 0 then
      L_problem_desc_tbl := "RIB_problemDescription_TBL"(substr(L_error_message,1,500));
   else
      -- The webservice allows multiple records in the collection businessproblemdetail_tbl. 
      -- However all the error messages will be appended to the first record of this collection. 
      L_problem_desc_tbl := I_rib_failstatus_rec.businessproblemdetail_tbl(1).problemdescription_tbl;
      if L_problem_desc_tbl is null or L_problem_desc_tbl.COUNT = 0 then
         L_problem_desc_tbl := "RIB_problemDescription_TBL"();
      end if;
      L_problem_desc_tbl.extend;
      L_problem_desc_tbl(L_problem_desc_tbl.COUNT) := substr(L_error_message,1,500);
   end if;
   
   L_businessproblemdetail_tbl := 
         "RIB_BusinessProblemDetail_TBL"(
            "RIB_BusinessProblemDetail_REC"(0, 
                                            SET(L_problem_desc_tbl),   -- using set() to remove duplicate error messages
                                            L_prob_detail_entry_tbl)); -- NULL. 

   return "RIB_FailStatus_REC"(0, 
                               L_exception_type,        --errorType varchar2(255)
                               L_shorterrormessage,     --shortErrorMessage varchar2(255)
                               L_errordescription,      --errorDescription varchar2(500)
                               L_businessproblemdetail_tbl);

END BUILD_FAIL_RECORD;
--------------------------------------------------------------------------------
FUNCTION PARSE_STAGED_ERR_MSG(O_error_message    IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_fail_tbl        IN OUT  NOCOPY "RIB_FailStatus_TBL",
                              I_stage_table_name IN             DBA_TABLES.TABLE_NAME%TYPE,
                              I_process_id       IN             NUMBER)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60)   := 'SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG';
   L_dynamic            VARCHAR2(2000) := NULL;
   L_exists             VARCHAR2(1);
   L_column_exists      NUMBER;
   L_fetch_tbl          ERROR_TBL;
   L_table_name         DBA_TABLES.TABLE_NAME%TYPE := UPPER(I_stage_table_name);

   cursor C_STG_TABLE is
      select 'x'
        from dba_tables, 
             system_config_options 
       where owner = table_owner
         and table_name = L_table_name
         and rownum = 1;

   cursor C_STG_COLUMN is
      select COUNT(DISTINCT column_name)
        from dba_tab_columns, 
             system_config_options 
       where owner = table_owner
         and table_name = L_table_name
         and column_name in ('PROCESS_ID','ERROR_MSG');

BEGIN
   -- validate required inputs
   if I_stage_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_stage_table_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- I_staging_table_name must be a valid table name
   open C_STG_TABLE;
   fetch C_STG_TABLE into L_exists;
   close C_STG_TABLE;
    
   if L_exists is NULL or L_exists != 'x' then 
      O_error_message := SQL_LIB.CREATE_MSG('INV_RETEK_TABLE',
                                            I_stage_table_name,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- I_staging_table_name must be a standard staging table with columns error_msg and process_id
   open C_STG_COLUMN;
   fetch C_STG_COLUMN into L_column_exists;
   close C_STG_COLUMN;
    
   if L_column_exists is NULL or L_column_exists < 2 then 
      O_error_message := SQL_LIB.CREATE_MSG('NONSTANDARD_STAGING_TABLE',
                                            I_stage_table_name,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- start processing error
   L_dynamic := 'select r.error_msg '||
                '  from '|| L_table_name ||' r'||
                ' where r.process_id = :1'||
                '   and r.error_msg is NOT NULL';
   ---
   EXECUTE IMMEDIATE L_dynamic bulk collect into L_fetch_tbl using I_process_id;

   if PARSE_ERR_MSG(O_error_message,
                    IO_fail_tbl,
                    L_fetch_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_STAGED_ERR_MSG;
--------------------------------------------------------------------------------
FUNCTION PARSE_STAGED_ERR_MSG(O_error_message    IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_error_tbl       IN OUT  NOCOPY OBJ_ERROR_TBL,
                              I_stage_table_name IN             DBA_TABLES.TABLE_NAME%TYPE,
                              I_process_id       IN             NUMBER)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60)   := 'SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG';
   L_dynamic            VARCHAR2(2000) := NULL;
   L_exists             VARCHAR2(1);
   L_column_exists      NUMBER;
   L_fetch_tbl          ERROR_TBL;
   L_table_name         DBA_TABLES.TABLE_NAME%TYPE := UPPER(I_stage_table_name);

   cursor C_STG_TABLE is
      select 'x'
        from dba_tables, 
             system_config_options 
       where owner = table_owner
         and table_name = L_table_name
         and rownum = 1;

   cursor C_STG_COLUMN is
      select COUNT(DISTINCT column_name)
        from dba_tab_columns, 
             system_config_options 
       where owner = table_owner
         and table_name = L_table_name
         and column_name in ('PROCESS_ID','ERROR_MSG');

BEGIN
   -- validate required inputs
   if I_stage_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_stage_table_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- I_staging_table_name must be a valid table name
   open C_STG_TABLE;
   fetch C_STG_TABLE into L_exists;
   close C_STG_TABLE;
    
   if L_exists is NULL or L_exists != 'x' then 
      O_error_message := SQL_LIB.CREATE_MSG('INV_RETEK_TABLE',
                                            I_stage_table_name,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- I_staging_table_name must be a standard staging table with columns error_msg and process_id
   open C_STG_COLUMN;
   fetch C_STG_COLUMN into L_column_exists;
   close C_STG_COLUMN;
    
   if L_column_exists is NULL or L_column_exists < 2 then 
      O_error_message := SQL_LIB.CREATE_MSG('NONSTANDARD_STAGING_TABLE',
                                            I_stage_table_name,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- start processing error
   L_dynamic := 'select r.error_msg '||
                '  from '|| L_table_name ||' r'||
                ' where r.process_id = :1'||
                '   and r.error_msg is NOT NULL';
   ---
   EXECUTE IMMEDIATE L_dynamic bulk collect into L_fetch_tbl using I_process_id;

   if PARSE_ERR_MSG(O_error_message,
                    IO_error_tbl,
                    L_fetch_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_STAGED_ERR_MSG;
--------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message     IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_fail_tbl         IN OUT  NOCOPY "RIB_FailStatus_TBL",
                       I_error_message_tbl IN             ERROR_TBL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60)   := 'SVCPROV_UTILITY.PARSE_ERR_MSG';
   L_ctr                NUMBER(4)      := 1;
   L_errordescription   VARCHAR2(255)  := NULL;
   L_working_start_pos  NUMBER(4)      := 1;
   L_working_end_pos    NUMBER(4)      := NULL;
   L_working_msg        RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_stage_table_msg    VARCHAR2(2000) := NULL;

BEGIN

   if I_error_message_tbl is NULL or I_error_message_tbl.COUNT <= 0 then 
      return TRUE;
   end if;
   
   -- If there are valid error message after parsing in the input error collection, 
   -- the shorterrormessage in the IO_fail_tbl will updated with a generic message.
   L_errordescription := SUBSTR(SQL_LIB.GET_MESSAGE_TEXT('INVALID_INPUT_DATA',
                                                          NULL,
                                                          NULL,
                                                          NULL),1,255);
   
   FOR i in I_error_message_tbl.first..I_error_message_tbl.last LOOP 
      L_stage_table_msg := I_error_message_tbl(i);

      L_ctr := 1;
      L_working_start_pos := 1;

      LOOP
         L_working_end_pos := INSTR(L_stage_table_msg, ';', 1, L_ctr);
         --
         if L_working_end_pos is NULL or L_working_end_pos = 0 then
            EXIT;
         end if;
         --
         L_working_msg := SUBSTR(SUBSTR(L_stage_table_msg,L_working_start_pos,L_working_end_pos-L_working_start_pos),1,255);
         --
         --put strings in RIB_FailStatus_TBL()
         if IO_fail_tbl is NULL or IO_fail_tbl.COUNT = 0 then
            IO_fail_tbl := "RIB_FailStatus_TBL"();
            IO_fail_tbl.extend;
         end if;
         
         -- Webservice expects only one record in RIB_FailStatus_TBL. Hence hardcoding 1 instead of using count
         IO_fail_tbl(1) := BUILD_FAIL_RECORD(L_working_msg,IO_fail_tbl(1));
         
         -- Overridding errorDescription
         IO_fail_tbl(1).errorDescription := L_errordescription;
         L_ctr := L_ctr +1;
         L_working_start_pos := L_working_end_pos+1;
      END LOOP;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ERR_MSG;
--------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message     IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_error_tbl        IN OUT  NOCOPY OBJ_ERROR_TBL,
                       I_error_message_tbl IN             ERROR_TBL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60)   := 'SVCPROV_UTILITY.PARSE_ERR_MSG';
   L_ctr                NUMBER(4)      := 1;
   L_working_start_pos  NUMBER(4)      := 1;
   L_working_end_pos    NUMBER(4)      := NULL;
   L_working_msg        RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_stage_table_msg    VARCHAR2(2000) := NULL;

BEGIN

   if I_error_message_tbl is NULL or I_error_message_tbl.COUNT <= 0 then 
      return TRUE;
   end if;
   
   FOR i in I_error_message_tbl.first..I_error_message_tbl.last LOOP 
      L_stage_table_msg := I_error_message_tbl(i);

      L_ctr := 1;
      L_working_start_pos := 1;

      LOOP
         L_working_end_pos := INSTR(L_stage_table_msg, ';', 1, L_ctr);
         --
         if L_working_end_pos is NULL or L_working_end_pos = 0 then
            EXIT;
         end if;
         --
         L_working_msg := SUBSTR(SUBSTR(L_stage_table_msg,L_working_start_pos,L_working_end_pos-L_working_start_pos),1,255);
         --
         if IO_error_tbl is NULL or IO_error_tbl.COUNT = 0 then
            IO_error_tbl := OBJ_ERROR_TBL();
         end if;
         
         IO_error_tbl.extend;
         IO_error_tbl(IO_error_tbl.COUNT) := L_working_msg;
         
         L_ctr := L_ctr +1;
         L_working_start_pos := L_working_end_pos+1;
      END LOOP;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ERR_MSG;
--------------------------------------------------------------------------------
END SVCPROV_UTILITY;
/
