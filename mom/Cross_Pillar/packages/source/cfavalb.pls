CREATE OR REPLACE PACKAGE BODY CFA_VALIDATE_SQL AS
---------------------------------------------------------------------------------
FUNCTION  VALIDATE_ATTRIB(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_set_fields_tbl  IN OUT   CFA_SQL.TYP_VALUE_TBL,
   I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
   I_attrib          IN       CFA_ATTRIB.VIEW_COL_NAME%TYPE,
   I_attrib_value    IN       CFA_SQL.GP_FIELD_VALUE%TYPE)
RETURN BOOLEAN AS

   L_program         VARCHAR2(62) := 'CFA_VALIDATE_SQL.VALIDATE_ATTRIB';
   L_func            GP_FUNC%TYPE;
   
   L_data_type       CFA_ATTRIB.DATA_TYPE%TYPE;
   L_max_length      CFA_ATTRIB.MAXIMUM_LENGTH%TYPE;
   L_lowest_value    CFA_ATTRIB.LOWEST_ALLOWED_VALUE%TYPE;
   L_highest_value   CFA_ATTRIB.HIGHEST_ALLOWED_VALUE%TYPE;
   ---
   L_index           VARCHAR2(60);
   L_return          NUMBER;
   ---
   L_validate_date   DATE;
   L_earliest_date   DATE;
   L_latest_date     DATE;

   L_old_value       CFA_SQL.GP_FIELD_VALUE%TYPE;
   L_old_desc_value  CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||chr(10)||
               'I_group_id: '||I_group_id||chr(10)||
               'I_attrib: '||I_attrib||chr(10)||
               'I_attrib_value: '||I_attrib_value);
   ---      
   if NOT CFA_SQL.CLEAR_OUTPUT_TBL(O_error_message) then
      return FALSE;
   end if;

   DBG_SQL.MSG(L_program,
               'After clear output tbl');   
   
   -------------------------------------------------------------------------------
   -- Simple validation check
   -------------------------------------------------------------------------------
   L_index := I_attrib;
   ---
   L_data_type     := CFA_SQL.GP_attrib_cfg_tbl(L_index).data_type;
   L_max_length    := CFA_SQL.GP_attrib_cfg_tbl(L_index).maximum_length;
   L_lowest_value  := CFA_SQL.GP_attrib_cfg_tbl(L_index).lowest_allowed_value;
   L_highest_value := CFA_SQL.GP_attrib_cfg_tbl(L_index).highest_allowed_value;

   DBG_SQL.MSG(L_program,
               'Simple validation properties:'||chr(10)||
               'L_data_type    : '||L_data_type||chr(10)||
               'L_max_length   : '||L_max_length||chr(10)||
               'L_lowest_value : '||L_lowest_value||chr(10)||
               'L_highest_value: '||L_highest_value); 

   -- Save the old value in case of errors
   if NOT CFA_SQL.GET_VALUE(O_error_message,
                            L_old_value,
                            L_old_desc_value,
                            L_data_type,
                            I_attrib) then
      return FALSE;
   end if;

   DBG_SQL.MSG(L_program,
               'Old field values'||chr(10)||
               'L_old_value      : '||L_old_value||chr(10)||
               'L_old_desc_value : '||L_old_desc_value); 

   if I_attrib_value is NOT NULL then               
      if (L_data_type = CFA_SQL.CHAR_TYPE and
          length(I_attrib_value) > L_max_length) or
         (L_data_type = CFA_SQL.NUM_TYPE and
          length(to_number(I_attrib_value)) > L_max_length) then
         O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_LENGTH', L_max_length);
         return FALSE;
      end if;
      ---
      DBG_SQL.MSG(L_program,
                  'passed max length'); 
      
      if L_data_type = CFA_SQL.NUM_TYPE then
         if to_number(I_attrib_value) < L_lowest_value then
            O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_LOWEST', L_lowest_value);
            return FALSE;
         end if;
         ---
         if to_number(I_attrib_value) > L_highest_value then
            O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_HIGHEST', L_highest_value);
            return FALSE;
         end if;
         ---
         DBG_SQL.MSG(L_program,
                  'passed numeric limits');
      end if;
      ---
      if L_data_type = CFA_SQL.DATE_TYPE then
         L_validate_date := to_date(I_attrib_value);
         ---
         if L_lowest_value is NOT NULL then
            L_earliest_date := to_date(to_char(L_lowest_value),'YYYYMMDD');
            ---
            if L_validate_date < L_earliest_date then
               O_error_message := SQL_LIB.CREATE_MSG('DATE_NOT_BEFORE',L_earliest_date);
               return FALSE;
            end if;
         end if;
         ---
         if L_highest_value is NOT NULL then
            L_latest_date   := to_date(to_char(L_highest_value),'YYYYMMDD');
            ---
            if L_validate_date > L_latest_date then
               O_error_message := SQL_LIB.CREATE_MSG('DATE_NOT_AFTER',L_latest_date);
               return FALSE;
            end if;
         end if;
         ---
         DBG_SQL.MSG(L_program,
                     'passed date limits'); 
      end if;
   end if;
   -----------------------------------------------------------------------------
   -- Complex validation
   -----------------------------------------------------------------------------
   DBG_SQL.MSG(L_program,
               'Start complex validation'); 

   -- Set the new value to the collection
   if NOT CFA_SQL.SET_VALUE(O_error_message,
                            CFA_SQL.CFA_EXT,
                            I_attrib,
                            I_attrib_value) then
      return FALSE;
   end if;

   if NOT GET_ATTRIB_VAL_FUNC(O_error_message,
                              L_func,
                              I_group_id,
                              I_attrib) then
      return FALSE;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'L_func: '||L_func); 

   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if;   

      DBG_SQL.MSG(L_program,
                  'after EXEC_FUNC');

      if L_return = CFA_FALSE then
         if NOT CFA_SQL.SET_VALUE(O_error_message,
                                  CFA_SQL.CFA_EXT,
                                  I_attrib,
                                  L_old_value,
                                  L_old_desc_value) then
            return FALSE;
         end if;
         ---
         return FALSE;                         
      end if;
      ---
      if CFA_SQL.GP_return_tbl is NOT NULL and CFA_SQL.GP_return_tbl.count > 0 then
         for i in CFA_SQL.GP_return_tbl.first..CFA_SQL.GP_return_tbl.last loop
             L_index := CFA_SQL.GP_return_tbl(i).field_name;
             ---
             O_set_fields_tbl(L_index).field_type       := CFA_SQL.GP_attrib_value_tbl(L_index).field_type;
             O_set_fields_tbl(L_index).field_name       := CFA_SQL.GP_attrib_value_tbl(L_index).field_name;
             O_set_fields_tbl(L_index).data_type        := CFA_SQL.GP_attrib_value_tbl(L_index).data_type;
             O_set_fields_tbl(L_index).storage_col      := CFA_SQL.GP_attrib_value_tbl(L_index).storage_col;
             O_set_fields_tbl(L_index).field_no         := CFA_SQL.GP_attrib_value_tbl(L_index).field_no;
             O_set_fields_tbl(L_index).field_value      := CFA_SQL.GP_attrib_value_tbl(L_index).field_value;
             O_set_fields_tbl(L_index).field_value_qry  := CFA_SQL.GP_attrib_value_tbl(L_index).field_value_qry;
             O_set_fields_tbl(L_index).field_desc_value := CFA_SQL.GP_attrib_value_tbl(L_index).field_desc_value;

             DBG_SQL.MSG(L_program,
                         'field_type      : '||O_set_fields_tbl(L_index).field_type     ||chr(10)||
                         'field_name      : '||O_set_fields_tbl(L_index).field_name     ||chr(10)||
                         'data_type       : '||O_set_fields_tbl(L_index).data_type      ||chr(10)||
                         'storage_col     : '||O_set_fields_tbl(L_index).storage_col    ||chr(10)||
                         'field_no        : '||O_set_fields_tbl(L_index).field_no       ||chr(10)||
                         'field_value     : '||O_set_fields_tbl(L_index).field_value    ||chr(10)||
                         'field_value_qry : '||O_set_fields_tbl(L_index).field_value_qry||chr(10)||
                         'field_desc_value: '||O_set_fields_tbl(L_index).field_desc_value);
         end loop;
         ---
      end if;
   end if;
   

   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END VALIDATE_ATTRIB;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_GROUP(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_err_field       IN OUT   VARCHAR2,
   I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.VALIDATE_GROUP';
   L_func         GP_FUNC%TYPE;
   L_return       NUMBER;
   
   L_index        CFA_SQL.GP_INDEX%TYPE;
   L_attrib_cfg   CFA_SQL.TYP_ATTRIB_CFG_TBL;
   L_attrib_value CFA_SQL.TYP_VALUE_TBL;
   
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---            
   if NOT CFA_SQL.CLEAR_OUTPUT_TBL(O_error_message) then
      return FALSE;
   end if;

   DBG_SQL.MSG(L_program,
               'after CFA_SQL.CLEAR_OUTPUT_TBL');
   -----------------------------------------------------------------------------
   -- Simple Validation - Check for missing required values within the group
   -----------------------------------------------------------------------------
   if NOT CFA_SQL.GET_ATTRIB_CFG(O_error_message,
                                 L_attrib_cfg) then
      return FALSE;
   end if;
   
  DBG_SQL.MSG(L_program,
              'after CFA_SQL.GET_ATTRIB_CFG'||L_attrib_cfg.count);
               
   if NOT CFA_SQL.GET_VALUE_TBL(O_error_message,
                                L_attrib_value,
                                CFA_SQL.CFA_EXT) then
      return FALSE;
   end if;
   
   DBG_SQL.MSG(L_program,
              'after CFA_SQL.GET_VALUE_TBL'||L_attrib_value.count);
              
   L_index := L_attrib_cfg.first;
   ---
   loop
      exit when L_index is NULL;
   
      if L_attrib_cfg(L_index).group_id = I_group_id then
         DBG_SQL.MSG(L_program,
                    'L_index: '||L_index||chr(10)||
                    'req    : '||L_attrib_cfg(L_index).value_req||chr(10)||
                    'value  : '||L_attrib_value(L_index).field_value);
              
         if L_attrib_cfg(L_index).value_req = 'Y' and 
            L_attrib_value(L_index).field_value is NULL then
            ---
            O_err_field := L_attrib_cfg(L_index).storage_col;
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                                  L_attrib_cfg(L_index).label);
            return FALSE;
         end if;
      end if;
      
      L_index := L_attrib_cfg.next(L_index);
   end loop;
   
   -----------------------------------------------------------------------------
   -- Optional Complex Validation at the group level executed in this section
   -----------------------------------------------------------------------------
   if NOT GET_GROUP_VAL_FUNC(O_error_message,
                             L_func,
                             I_group_id) then
      return FALSE;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'L_func: '||L_func);
                    
   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if; 
      
      if L_return = CFA_FALSE then
         L_index := CFA_SQL.GP_return_tbl(CFA_SQL.GP_return_tbl.first).field_name;
         O_err_field  := L_attrib_cfg(L_index).storage_col;   
         return FALSE;                         
      end if;
   end if; 
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END VALIDATE_GROUP;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_GROUP_SET(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.VALIDATE_GROUP_SET';
   L_func         GP_FUNC%TYPE;
   L_return       NUMBER;
   
   L_index        CFA_SQL.GP_INDEX%TYPE;
   L_attrib_cfg   CFA_SQL.TYP_ATTRIB_CFG_TBL;
   L_attrib_value CFA_SQL.TYP_VALUE_TBL;
   
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---            

   -----------------------------------------------------------------------------
   -- Simple Validation - Check for missing required values within the group
   -----------------------------------------------------------------------------
   if NOT CFA_SQL.GET_ATTRIB_CFG(O_error_message,
                                 L_attrib_cfg) then
      return FALSE;
   end if;
   
  DBG_SQL.MSG(L_program,
              'after CFA_SQL.GET_ATTRIB_CFG'||L_attrib_cfg.count);
               
   if NOT CFA_SQL.GET_VALUE_TBL(O_error_message,
                                L_attrib_value,
                                CFA_SQL.CFA_EXT) then
      return FALSE;
   end if;
   
   DBG_SQL.MSG(L_program,
              'after CFA_SQL.GET_VALUE_TBL'||L_attrib_value.count);
              
   L_index := L_attrib_cfg.first;
   ---
   loop
      exit when L_index is NULL;
   
      DBG_SQL.MSG(L_program,
                  'L_index: '||L_index||chr(10)||
                  'req    : '||L_attrib_cfg(L_index).value_req||chr(10)||
                  'value  : '||L_attrib_value(L_index).field_value);
              
      if L_attrib_cfg(L_index).value_req = 'Y' and 
         L_attrib_value(L_index).field_value is NULL then
         ---
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               L_attrib_cfg(L_index).label);
         return FALSE;
      end if;
      
      L_index := L_attrib_cfg.next(L_index);
   end loop;
   
   -----------------------------------------------------------------------------
   -- Optional Complex Validation at the group set level executed in this section
   -----------------------------------------------------------------------------
   if NOT GET_GROUP_SET_VAL_FUNC(O_error_message,
                                 L_func,
                                 I_group_set_id) then
      return FALSE;
   end if;
   ---
   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if;   
      ---
       if L_return = CFA_FALSE then
         return FALSE;                         
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END VALIDATE_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_EXT_ENTITY(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.VALIDATE_EXT_ENTITY';
   L_func         GP_FUNC%TYPE;
   L_return       NUMBER;
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---            

   -----------------------------------------------------------------------------
   -- Optional Complex Validation at the entity level executed in this section
   -----------------------------------------------------------------------------
   if NOT GET_EXT_ENTITY_VAL_FUNC(O_error_message,
                                  L_func,
                                  I_base_table) then
      return FALSE;
   end if;
   ---
   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if;  
      ---
      if L_return = CFA_FALSE then
         return FALSE;                         
      end if; 
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END VALIDATE_EXT_ENTITY;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_FUNCTION(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_func            IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.VALIDATE_FUNCTION';
   L_package      VARCHAR2(60);
   L_function     VARCHAR2(60);
   L_exists       VARCHAR2(1);
   L_tab_owner    SYSTEM_OPTIONS.TABLE_OWNER%TYPE;

   cursor C_FIND_PACKAGE_FUNC is
      select 'x'
        from all_procedures
       where owner          = L_tab_owner
         and object_type    = 'PACKAGE'
         and object_name    = L_package
         and procedure_name = L_function
         and rownum =1;
         
   cursor C_FIND_FUNC is
      select 'x'
        from all_procedures
       where owner       = L_tab_owner
         and object_type in ('FUNCTION', 'PROCEDURE')
         and object_name = L_function
         and rownum =1;

   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_tab_owner;
   close C_GET_TAB_OWNER;
   ---
   if (INSTR(I_func, '.')) != 0 then
      L_package  := UPPER(SUBSTR(I_func,1,INSTR(I_func, '.')-1));
      L_function := UPPER(SUBSTR(I_func,INSTR(I_func, '.')+1, LENGTH(I_func)));
      
      open C_FIND_PACKAGE_FUNC;
      fetch C_FIND_PACKAGE_FUNC into L_exists;
      
       if C_FIND_PACKAGE_FUNC%NOTFOUND then
          O_error_message := SQL_LIB.CREATE_MSG('FUNC_NOT_FOUND',
                                                I_func);
          close C_FIND_PACKAGE_FUNC;
          return FALSE;
       end if;
       
      close C_FIND_PACKAGE_FUNC;
   else
      L_function := UPPER(I_func);

      open C_FIND_FUNC;
      fetch C_FIND_FUNC into L_exists;
      
      if C_FIND_FUNC%NOTFOUND then
          O_error_message := SQL_LIB.CREATE_MSG('FUNC_NOT_FOUND',
                                                I_func);
          close C_FIND_FUNC;
          return FALSE;
       end if;
       
      close C_FIND_FUNC;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END VALIDATE_FUNCTION;
--------------------------------------------------------------------------------
FUNCTION  DEFAULT_GROUP_SET(
   O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table          IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
   I_group_set_id        IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE) 
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.DEFAULT_GROUP_SET';
   L_func         GP_FUNC%TYPE;
   L_index        CFA_SQL.GP_INDEX%TYPE;
   L_return       NUMBER;
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---            
   if NOT GET_GROUP_SET_DEFAULT_FUNC(O_error_message,
                                     L_func,
                                     I_group_set_id) then
      return FALSE;
   end if;
   ---
   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if; 
      ---
      if L_return = CFA_FALSE then
         return FALSE;
      end if;
      
      /*if NOT CFA_SQL.SET_DEFAULT_VALUES(O_error_message) then
         return FALSE;
      end if;*/
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END DEFAULT_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION  QUALIFY_GROUP_SET(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.QUALIFY_GROUP_SET';
   L_func         GP_FUNC%TYPE;
   L_return       NUMBER;
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---            
   if NOT GET_GROUP_SET_QUALIFY_FUNC(O_error_message,
                                     L_func,
                                     I_group_set_id) then
      return FALSE;
   end if;
   ---
   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if;   
   end if;
   ---
   if L_return = CFA_VALIDATE_SQL.CFA_FALSE then
      return FALSE;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END QUALIFY_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION  GET_KEY_DESC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_desc            IN OUT   CFA_SQL.GP_FIELD_DESC_VALUE%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
   I_key_col         IN       CFA_EXT_ENTITY_KEY.KEY_COL%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_KEY_DESC';
   L_func         VARCHAR2(61);
   L_index        CFA_SQL.GP_INDEX%TYPE;
   L_return       NUMBER;
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||
               'I_base_table: '||I_base_table||chr(10)||
               'I_key_col: '||I_key_col);
   ---   
   if NOT CFA_SQL.CLEAR_OUTPUT_TBL(O_error_message) then
      return FALSE;
   end if;
         
   DBG_SQL.MSG(L_program,
               'after CFA_SQL.CLEAR_OUTPUT_TBL');         
   if NOT GET_KEY_DESC_FUNC(O_error_message,
                            L_func,
                            I_base_table,
                            I_key_col) then
      return FALSE;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'L_func: '||L_func); 

   if L_func is NOT NULL then
      if NOT EXEC_FUNC(O_error_message,
                       L_return,
                       L_func) then
         return FALSE;
      end if;   
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'after exec_func'||
               'L_return: '||L_return); 

   -- Get description
   if CFA_SQL.GP_return_tbl.count > 0 then
      for i in CFA_SQL.GP_return_tbl.first..CFA_SQL.GP_return_tbl.last loop
         L_index := CFA_SQL.GP_return_tbl(i).field_name;
         ---
         if NOT CFA_SQL.GP_key_value_tbl.exists(L_index) then
            return TRUE;
         end if;

         O_desc := CFA_SQL.GP_key_value_tbl(L_index).field_desc_value;  
      end loop;
   end if;
      

   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_KEY_DESC;
--------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB.VALIDATION_FUNC%TYPE,
   I_group_id        IN       CFA_ATTRIB.GROUP_ID%TYPE,
   I_attrib          IN       CFA_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_ATTRIB_VAL_FUNC';

   L_index        CFA_SQL.GP_INDEX%TYPE;

   cursor C_GET_VAL_FUNC is
      select validation_func
        from cfa_attrib
       where view_col_name = I_attrib
         and group_id = I_group_id;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   L_index := I_attrib;

   if NOT CFA_SQL.GP_attrib_cfg_tbl.exists(L_index) then
      open C_GET_VAL_FUNC;
      fetch C_GET_VAL_FUNC into O_func;
      close C_GET_VAL_FUNC;
   else
      O_func := CFA_SQL.GP_attrib_cfg_tbl(L_index).validation_func;
   end if;  
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_ATTRIB_VAL_FUNC;
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP.VALIDATION_FUNC%TYPE,
   I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_GROUP_VAL_FUNC';

   cursor C_GET_VAL_FUNC is
      select validation_func
        from cfa_attrib_group
       where group_id = I_group_id;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   open C_GET_VAL_FUNC;
   fetch C_GET_VAL_FUNC into O_func;
   close C_GET_VAL_FUNC;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_GROUP_VAL_FUNC;
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP_SET.VALIDATION_FUNC%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_GROUP_SET_VAL_FUNC';

   cursor C_GET_VAL_FUNC is
      select validation_func
        from cfa_attrib_group_set
       where group_set_id = I_group_set_id;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   open C_GET_VAL_FUNC;
   fetch C_GET_VAL_FUNC into O_func;
   close C_GET_VAL_FUNC;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_GROUP_SET_VAL_FUNC;
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET_DEFAULT_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP_SET.DEFAULT_FUNC%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_GROUP_SET_DEFAULT_FUNC';

   cursor C_GET_DEFAULT_FUNC is
      select default_func
        from cfa_attrib_group_set
       where group_set_id = I_group_set_id;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   open C_GET_DEFAULT_FUNC;
   fetch C_GET_DEFAULT_FUNC into O_func;
   close C_GET_DEFAULT_FUNC;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_GROUP_SET_DEFAULT_FUNC;
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET_QUALIFY_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_ATTRIB_GROUP_SET.QUALIFIER_FUNC%TYPE,
   I_group_set_id    IN       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_GROUP_SET_QUALIFY_FUNC';

   cursor C_GET_QUALIFY_FUNC is
      select qualifier_func
        from cfa_attrib_group_set
       where group_set_id = I_group_set_id;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   open C_GET_QUALIFY_FUNC;
   fetch C_GET_QUALIFY_FUNC into O_func;
   close C_GET_QUALIFY_FUNC;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_GROUP_SET_QUALIFY_FUNC;
--------------------------------------------------------------------------------
FUNCTION GET_EXT_ENTITY_VAL_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_EXT_ENTITY.VALIDATION_FUNC%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_EXT_ENTITY_VAL_FUNC';

   cursor C_GET_VAL_FUNC is
      select validation_func
        from cfa_ext_entity ext
       where ext.base_rms_table = I_base_table;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   open C_GET_VAL_FUNC;
   fetch C_GET_VAL_FUNC into O_func;
   close C_GET_VAL_FUNC;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_EXT_ENTITY_VAL_FUNC;
--------------------------------------------------------------------------------
FUNCTION GET_KEY_DESC_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_func            IN OUT   CFA_EXT_ENTITY_KEY.DESCRIPTION_CODE%TYPE,
   I_base_table      IN       CFA_EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
   I_key_col         IN       CFA_EXT_ENTITY_KEY.KEY_COL%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.GET_KEY_DESC_FUNC';

   L_index        CFA_SQL.GP_INDEX%TYPE;

   cursor C_GET_DESC_FUNC is
      select description_code
        from cfa_ext_entity_key
       where base_rms_table = I_base_table
         and key_col = I_key_col;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---  
   L_index := I_key_col;

   if NOT CFA_SQL.GP_header_cfg_tbl.exists(L_index) then
      open C_GET_DESC_FUNC;
      fetch C_GET_DESC_FUNC into O_func;
      close C_GET_DESC_FUNC;
   else
      O_func := CFA_SQL.GP_header_cfg_tbl(L_index).desc_code;
   end if;  
   
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_KEY_DESC_FUNC;
--------------------------------------------------------------------------------
FUNCTION EXEC_FUNC(
   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_return_code     IN OUT   NUMBER,
   I_func            IN       VARCHAR2)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_VALIDATE_SQL.EXEC_FUNC';
   L_stmt         VARCHAR2(500);
   L_return       NUMBER;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---        
   L_stmt := 'BEGIN '||
                'if NOT '||I_func||'(:O_error_message) then '||
                   ':L_return := CFA_VALIDATE_SQL.CFA_FALSE; '||
                'else '||
                   ':L_return := CFA_VALIDATE_SQL.CFA_TRUE; '||
                'end if; '||
             'END;';
   
   DBG_SQL.MSG(L_program,
               L_stmt);

   EXECUTE IMMEDIATE L_stmt
      USING IN OUT O_error_message,
            IN OUT L_return;

   O_return_code := L_return;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END EXEC_FUNC;
--------------------------------------------------------------------------------

END CFA_VALIDATE_SQL;
/
