CREATE OR REPLACE PACKAGE RMSAIA_LIB AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

PROCEDURE BUILD_SERVICE_OP_STATUS(O_ServiceOpStatus OUT "RIB_ServiceOpStatus_REC",
                                  I_status_code     IN     VARCHAR2,
                                  I_error_message   IN     VARCHAR2,
                                  I_program         IN     VARCHAR2);

PROCEDURE BUILD_NOSERVICE_OP_STATUS(O_ServiceOpStatus OUT "RIB_ServiceOpStatus_REC",
                                    I_error_message   IN     VARCHAR2,
                                    I_program         IN     VARCHAR2);

--------------------------------------------------------------------------------

END RMSAIA_LIB;

/
