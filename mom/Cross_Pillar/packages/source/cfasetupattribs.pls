CREATE OR REPLACE PACKAGE CFA_SETUP_ATTRIB_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
ERRNUM_PACKAGE_CALL    NUMBER(6) := -20020;  -- Define error for raise_application_error

TYPE TABLE_TBL IS TABLE OF ALL_TAB_COLUMNS.TABLE_NAME%TYPE INDEX BY BINARY_INTEGER;

TYPE ATTRIB_LABEL_REC IS RECORD
(  attrib_id            CFA_ATTRIB_LABELS.ATTRIB_ID%TYPE,
   view_col_name        CFA_ATTRIB.VIEW_COL_NAME%TYPE,
   lang                 CFA_ATTRIB_LABELS.LANG%TYPE,
   label                CFA_ATTRIB_LABELS.LABEL%TYPE,
   default_lang_ind     CFA_ATTRIB_LABELS.DEFAULT_LANG_IND%TYPE,
   error_message        RTK_ERRORS.RTK_TEXT%TYPE,
   return_code          VARCHAR2(5)
);

TYPE ATTRIB_LABEL_TBL IS TABLE OF ATTRIB_LABEL_REC INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
TYPE FF_UI_PARAM_REC IS RECORD
(
   base_rms_table       CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
   key_name_1           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_1          VARCHAR2(60),
   key_name_2           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_2          VARCHAR2(60),
   key_name_3           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_3          VARCHAR2(60),
   key_name_4           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_4          VARCHAR2(60),
   key_name_5           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_5          VARCHAR2(60),
   key_name_6           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_6          VARCHAR2(60),
   key_name_7           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_7          VARCHAR2(60),
   key_name_8           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_8          VARCHAR2(60),
   key_name_9           CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_9          VARCHAR2(60),
   key_name_10          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_10         VARCHAR2(60),
   key_name_11          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_11         VARCHAR2(60),
   key_name_12          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_12         VARCHAR2(60),
   key_name_13          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_13         VARCHAR2(60),
   key_name_14          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_14         VARCHAR2(60),
   key_name_15          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_15         VARCHAR2(60),
   key_name_16          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_16         VARCHAR2(60),
   key_name_17          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_17         VARCHAR2(60),
   key_name_18          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_18         VARCHAR2(60),
   key_name_19          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_19         VARCHAR2(60),
   key_name_20          CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_value_20         VARCHAR2(60)
);

TYPE FF_UI_PARAM_TBL IS TABLE OF FF_UI_PARAM_REC INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB_LABELS (IO_attrib_label_tbl      IN OUT    ATTRIB_LABEL_TBL,
                               I_group_id              IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_lang                  IN        CFA_ATTRIB_LABELS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB_LABELS  (IO_attrib_label_tbl      IN OUT    ATTRIB_LABEL_TBL,
                               I_group_id              IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_attrib_id             IN        CFA_ATTRIB_LABELS.ATTRIB_ID%TYPE,
                               I_lang                  IN        CFA_ATTRIB_LABELS.LANG%TYPE);
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB_LABELS (IO_attrib_label_tbl      IN OUT    ATTRIB_LABEL_TBL,
                               I_group_id              IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_lang                  IN        CFA_ATTRIB_LABELS.LANG%TYPE,
                               I_default_lang_ind      IN        CFA_ATTRIB_LABELS.DEFAULT_LANG_IND%TYPE);

---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ATTRIB_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_attrib_id       IN OUT   CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_LABELS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_attrib_id       IN       CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_LABEL_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_default_ind     IN OUT    CFA_ATTRIB_LABELS.DEFAULT_LANG_IND%TYPE,
                                       I_group_id        IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                                       I_lang            IN        CFA_ATTRIB_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ATTRIB_LABELS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist           IN OUT    VARCHAR2,
                             I_group_id        IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                             I_lang            IN        CFA_ATTRIB_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIBS_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_group_id        IN     CFA_ATTRIB.GROUP_ID%TYPE)                              
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION ATTRIB_DISP_SEQ_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist            IN OUT   VARCHAR2,
                               I_group_id         IN       CFA_ATTRIB.GROUP_ID%TYPE,
                               I_display_seq      IN       CFA_ATTRIB.DISPLAY_SEQ%TYPE,
                               I_storage_col_name IN       CFA_ATTRIB.STORAGE_COL_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION VIEW_COL_NAME_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist             IN OUT   VARCHAR2,
                             I_group_id          IN       CFA_ATTRIB.GROUP_ID%TYPE,
                             I_view_col_name     IN       CFA_ATTRIB.VIEW_COL_NAME%TYPE,
                             I_storage_col_name  IN       CFA_ATTRIB.STORAGE_COL_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DISPLAY_SEQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_display_seq     IN OUT   CFA_ATTRIB.DISPLAY_SEQ%TYPE,
                              I_group_id        IN       CFA_ATTRIB.GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION SET_FF_UI_PARAMETERS(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_ff_ui_param_rec    IN OUT   NOCOPY   FF_UI_PARAM_REC,
                              I_base_rms_table      IN                CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_AVAIL_METADATA_COL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_storage_col_name     IN OUT   CFA_ATTRIB.STORAGE_COL_NAME%TYPE,
                                I_group_id             IN       CFA_ATTRIB.GROUP_ID%TYPE,
                                I_data_type            IN       CFA_ATTRIB.DATA_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_LANG_LABELS (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_group_id       IN      CFA_ATTRIB.GROUP_ID%TYPE,
                                    I_lang           IN      CFA_ATTRIB_LABELS.LANG%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION SET_FF_UI_PARAMETERS_WRP(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_ff_ui_param_rec_wrp    IN OUT   			   FF_UI_PARAM_REC_WRP,
                              I_base_rms_table      IN                CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN NUMBER;
---------------------------------------------------------------------------------------------------
FUNCTION P_CHK_VALID_NAME(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
					I_sql_name   IN   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_ATTRIB_SQL;
/