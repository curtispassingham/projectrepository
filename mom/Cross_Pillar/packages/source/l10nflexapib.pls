CREATE OR REPLACE PACKAGE BODY L10N_FLEX_API_SQL AS
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                             I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE, 
                             I_process_id        IN      NUMBER)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'L10N_FLEX_API_SQL.INSERT_L10N_EXT_TBL';

   L_ext_table          VARCHAR2(30) := NULL;
   L_stg_table          VARCHAR2(30) := NULL;
   L_insert_string      VARCHAR2(4000) := NULL;
   L_col_list_string    VARCHAR2(4000) := NULL;
   L_value_list_string  VARCHAR2(4000) := NULL;

   L_groups      ID_TBL;
   L_key_cols    COLUMN_TBL;
   L_view_cols   view_col_rec;

   cursor C_EXT_TABLE is
      select leev.staging_table,
			 ee.l10n_ext_table
        from ext_entity ee,
             l10n_ext_entity_val leev
       where ee.base_rms_table = I_base_table
         and ee.ext_entity_id = leev.ext_entity_id;
   cursor C_KEY_COLS is
      select key_col
        from ext_entity_key
       where base_rms_table = I_base_table
    order by key_number;

   cursor C_ATTRIB_GROUPS is
      select g.group_id 
        from l10n_attrib_group g,
             ext_entity e
       where e.ext_entity_id = g.ext_entity_id
         and g.country_id = I_country_id
         and e.base_rms_table = I_base_table;

   cursor C_VIEW_COLS(I_group_id in L10N_ATTRIB_GROUP.GROUP_ID%TYPE) is
      select view_col_name, 
             group_id,
             attrib_storage_col tab_col_name
        from l10n_attrib
       where group_id = I_group_id
    order by attrib_storage_col;    

BEGIN
   --check input
   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_country_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
  
   if I_base_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_base_table',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_process_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- start building sql
   open C_EXT_TABLE;
   fetch C_EXT_TABLE into L_stg_table,L_ext_table;
   close C_EXT_TABLE;
   if L_ext_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   L_stg_table := 'STG_' || L_ext_table || '_' || I_country_id;  -- staging table name, eg. STG_STORE_L10N_EXT_BR
if L_stg_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_KEY_COLS;
   fetch C_KEY_COLS bulk collect into L_key_cols;
   close C_KEY_COLS;

   if L_key_cols is NULL or L_key_cols.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_KEY_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_ATTRIB_GROUPS;
   fetch C_ATTRIB_GROUPS bulk collect into L_groups;
   close C_ATTRIB_GROUPS;

   if L_groups is NULL or L_groups.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('ATTR_GRP_NOT_FOUND',
                                            I_base_table,
                                            I_country_id,
                                            NULL);
      return FALSE;
   end if;

   -- build 1 insert/select statement for each group
   for k in L_groups.first .. L_groups.last loop
      -- builds the 'insert' portion of the dynamic statement
      L_insert_string := 'insert into ' || L_ext_table || '(';
      for i in L_key_cols.first .. L_key_cols.last loop
         L_insert_string := L_insert_string || L_key_cols(i) || ', ';
      end loop;
      L_insert_string := L_insert_string || 'l10n_country_id, group_id';
   
      open C_VIEW_COLS(L_groups(k));
      fetch C_VIEW_COLS bulk collect into L_view_cols.view_col_names,
                                          L_view_cols.group_ids,
                                          L_view_cols.tab_col_names;
      close C_VIEW_COLS;
   
      for i in L_view_cols.tab_col_names.FIRST .. L_view_cols.tab_col_names.LAST loop
         L_insert_string := L_insert_string || ', ' || L_view_cols.tab_col_names(i);
      end loop;
      L_insert_string := L_insert_string || ') select ';

      -- builds the 'select' portion of the dynamic statement
      for i in L_key_cols.first .. L_key_cols.last loop
         L_insert_string := L_insert_string || L_key_cols(i) || ', ';
      end loop;
      L_insert_string := L_insert_string || '''' || I_country_id  || '''' ||', ' || L_groups(k);

      for i in L_view_cols.view_col_names.FIRST .. L_view_cols.view_col_names.LAST loop
         L_insert_string := L_insert_string || ', ' || L_view_cols.view_col_names(i);
      end loop;
      L_insert_string := L_insert_string || 
                         ' from ' || L_stg_table ||
                         ' where process_id = ' || I_process_id;
      EXECUTE IMMEDIATE L_insert_string;
   end loop;
                
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END INSERT_L10N_EXT_TBL;   
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_STG_NAME_VALUE_PAIR(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_country_id           IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                                    I_base_table           IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                                    I_process_id           IN      NUMBER,
                                    I_entity_name_values   IN      ENTITY_NAME_VALUE_PAIR_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_FLEX_API_SQL.UPDATE_STG_NAME_VALUE_PAIR';

   L_ext_table             VARCHAR2(30) := NULL;
   L_stg_table             VARCHAR2(30) := NULL;
   L_update_string         VARCHAR2(4000) := NULL;
   L_set_string            VARCHAR2(4000) := NULL;

   L_key_col_type_tbl      TBL_col_type_tbl;
   L_attrib_col_type_tbl   TBL_col_type_tbl;
   L_index                 EXT_ENTITY_KEY.KEY_COL%TYPE := NULL;

   L_key_col_name          EXT_ENTITY_KEY.KEY_COL%TYPE := NULL;
   L_key_data_type         EXT_ENTITY_KEY.DATA_TYPE%TYPE := NULL;
   L_attrib_col_name       L10N_ATTRIB.VIEW_COL_NAME%TYPE := NULL;
   L_attrib_data_type      L10N_ATTRIB.DATA_TYPE%TYPE := NULL;

 cursor C_EXT_TABLE is
      select leev.staging_table,
			 ee.l10n_ext_table
        from ext_entity ee,
             l10n_ext_entity_val leev
       where ee.base_rms_table = I_base_table
         and ee.ext_entity_id = leev.ext_entity_id;
   cursor C_key_cols is 
      select upper(key_col) key_col, 
             data_type
        from ext_entity_key
       where upper(base_rms_table) = upper(I_base_table);

   cursor C_attrib_cols is
      select upper(a.view_col_name) view_col_name,
             a.data_type
        from l10n_attrib a,
             l10n_attrib_group g,
             ext_entity e
       where a.group_id = g.group_id
         and g.ext_entity_id = e.ext_entity_id
         and upper(e.base_rms_table) = upper(I_base_table)
         and g.base_ind = 'N'
         and g.country_id = I_country_id;

BEGIN
   --check input
   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_country_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
  
   if I_base_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_base_table',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_process_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_entity_name_values is NULL or I_entity_name_values.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_entity_name_values',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- start building sql
   open C_EXT_TABLE;
   fetch C_EXT_TABLE into L_stg_table,L_ext_table;
   close C_EXT_TABLE;
   
    if L_stg_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   if L_ext_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   L_stg_table := 'STG_' || L_ext_table || '_' || I_country_id;  -- staging table name, eg. STG_STORE_L10N_EXT_BR

   -- populate a collection of entity key columns indexed by key column name
   for rec in C_key_cols loop
      L_index := rec.key_col; 
      L_key_col_type_tbl(L_index).col_name := rec.key_col;
      L_key_col_type_tbl(L_index).data_type := rec.data_type;
   end loop;
   if L_key_col_type_tbl.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_KEY_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- populate a collection of attribte view columns indexed by view column name
   for rec in C_attrib_cols loop
      L_index := rec.view_col_name; 
      L_attrib_col_type_tbl(L_index).col_name := rec.view_col_name;
      L_attrib_col_type_tbl(L_index).data_type := rec.data_type;
   end loop;

   -- each entity in the input collection will result in a separate update statement
   for i in I_entity_name_values.FIRST .. I_entity_name_values.LAST loop
      if I_entity_name_values(i).key_col_tbl is NULL or I_entity_name_values(i).key_col_tbl.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'I_entity_name_values('||i||').key_col_tbl',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_entity_name_values(i).key_col_tbl.COUNT != L_key_col_type_tbl.COUNT then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_ENTITY_KEY',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_entity_name_values(i).attrib_col_tbl is NULL or I_entity_name_values(i).attrib_col_tbl.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'I_entity_name_values('||i||').attrib_col_tbl',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      --build the 'update' portion of the dynamic statement
      L_update_string := NULL;
      L_update_string := 'update ' || L_stg_table || ' set ';

      --build the 'set' portion of the dynamic statement
      for j in I_entity_name_values(i).attrib_col_tbl.FIRST .. I_entity_name_values(i).attrib_col_tbl.LAST loop
         if j != 1 then
            L_update_string := L_update_string || ', ';
         end if; 

         -- get the attribute data type from the collection
         L_attrib_data_type := NULL;
         L_attrib_col_name := upper(I_entity_name_values(i).attrib_col_tbl(j).name);
         L_attrib_data_type := L_attrib_col_type_tbl(L_attrib_col_name).data_type;
         ---
         if L_attrib_data_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('CUST_ATTR_INVALID',
                                                  I_base_table,
                                                  L_attrib_col_name,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if L_attrib_data_type NOT in (L10N_FLEX_ATTRIB_SQL.LP_char_data_type,
                                       L10N_FLEX_ATTRIB_SQL.LP_num_data_type,
                                       L10N_FLEX_ATTRIB_SQL.LP_date_data_type) then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_DATA_TYPE',
                                                  L_attrib_data_type,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         -- build the update stmt
         if I_entity_name_values(i).attrib_col_tbl(j).value is NULL then
            L_update_string := L_update_string || I_entity_name_values(i).attrib_col_tbl(j).name || '=NULL';
         else
            if L_attrib_data_type = L10N_FLEX_ATTRIB_SQL.LP_char_data_type then
               L_update_string := L_update_string || I_entity_name_values(i).attrib_col_tbl(j).name || '=' || '''' || I_entity_name_values(i).attrib_col_tbl(j).value || '''';
            elsif L_attrib_data_type = L10N_FLEX_ATTRIB_SQL.LP_num_data_type then
               L_update_string := L_update_string || I_entity_name_values(i).attrib_col_tbl(j).name || '=' || I_entity_name_values(i).attrib_col_tbl(j).value;
            elsif L_attrib_data_type = L10N_FLEX_ATTRIB_SQL.LP_date_data_type then
               L_update_string := L_update_string || I_entity_name_values(i).attrib_col_tbl(j).name || 
                                     '= to_date(' || '''' || I_entity_name_values(i).attrib_col_tbl(j).value || '''' || ', ' || '''' || 'YYYYMMDD' || '''' || ')';
            end if;
         end if;
      end loop;

      --build the 'where' portion of the dynamic statement
      L_update_string := L_update_string || ' where process_id = ' || I_process_id;   
      for j in I_entity_name_values(i).key_col_tbl.FIRST .. I_entity_name_values(i).key_col_tbl.LAST loop

         -- get the key data type from the collection
         L_key_data_type := NULL;
         L_key_col_name := upper(I_entity_name_values(i).key_col_tbl(j).name);
         L_key_data_type := L_key_col_type_tbl(L_key_col_name).data_type;
         ---
         if L_key_data_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_KEY_INVALID',
                                                  I_base_table,
                                                  L_key_col_name,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if L_key_data_type NOT in (L10N_FLEX_ATTRIB_SQL.LP_char_data_type,
                                    L10N_FLEX_ATTRIB_SQL.LP_num_data_type,
                                    L10N_FLEX_ATTRIB_SQL.LP_date_data_type) then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_DATA_TYPE',
                                                  L_key_data_type,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if I_entity_name_values(i).key_col_tbl(j).value is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                                  'key column value',
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         -- build the update stmt
         if L_key_data_type = L10N_FLEX_ATTRIB_SQL.LP_char_data_type then
            L_update_string := L_update_string || ' and ' || I_entity_name_values(i).key_col_tbl(j).name || '=' || '''' || I_entity_name_values(i).key_col_tbl(j).value || '''';
         elsif L_key_data_type = L10N_FLEX_ATTRIB_SQL.LP_num_data_type then
            L_update_string := L_update_string || ' and ' || I_entity_name_values(i).key_col_tbl(j).name || '=' || I_entity_name_values(i).key_col_tbl(j).value;
         elsif L_key_data_type = L10N_FLEX_ATTRIB_SQL.LP_date_data_type then
            L_update_string := L_update_string || ' and ' || I_entity_name_values(i).key_col_tbl(j).name || 
                                  '= to_date(' || '''' || I_entity_name_values(i).key_col_tbl(j).value || '''' || ', ' || '''' || 'YYYYMMDD' || '''' || ')';
         end if;
      end loop;

      EXECUTE IMMEDIATE L_update_string;
   end loop;
                
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END UPDATE_STG_NAME_VALUE_PAIR; 
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_message           IN      RIB_OBJECT,
                              I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_FLEX_API_SQL.VALIDATE_L10N_ATTRIB';

   L_10n_rib_rec       L10N_RIB_REC := NULL;

   cursor C_localized_ctry is
      select country_id
        from country_attrib
       where localized_ind = 'Y';

BEGIN

   for rec in C_localized_ctry loop
      -- Call out to countries that are supported for localization, e.g. 'BR' for Brazil.
      L_10n_rib_rec := L10N_RIB_REC(); 

      L_10n_rib_rec.procedure_key := 'VALIDATE_RIB_L10N_ATTRIB';   
      L_10n_rib_rec.doc_type := NULL;
      L_10n_rib_rec.doc_id := NULL;
      L_10n_rib_rec.country_id := rec.country_id;
      L_10n_rib_rec.rib_msg := I_message;
      L_10n_rib_rec.rib_msg_type := I_message_type;

      --- Make a call to the l10n function dump l10n attributes
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_10n_rib_rec) = FALSE then
         return FALSE;                
      end if;   
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END VALIDATE_L10N_ATTRIB;   
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_L10N_ATTRIB(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_message           IN      RIB_OBJECT,
                             I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'L10N_FLEX_API_SQL.PERSIST_L10N_ATTRIB';

   L_10n_rib_rec       L10N_RIB_REC := NULL;

   cursor C_localized_ctry is
      select country_id
        from country_attrib
       where localized_ind = 'Y';

BEGIN

   for rec in C_localized_ctry loop
      -- Call out to countries that are supported for localization, e.g. 'BR' for Brazil.
      L_10n_rib_rec := L10N_RIB_REC(); 

      L_10n_rib_rec.procedure_key := 'PERSIST_RIB_L10N_ATTRIB';   
      L_10n_rib_rec.doc_type := NULL;
      L_10n_rib_rec.doc_id := NULL;
      L_10n_rib_rec.country_id := rec.country_id;
      L_10n_rib_rec.rib_msg := I_message;
      L_10n_rib_rec.rib_msg_type := I_message_type;

      --- Make a call to the l10n function dump l10n attributes
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_10n_rib_rec) = FALSE then
         return FALSE;                
      end if;   
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END PERSIST_L10N_ATTRIB;
---------------------------------------------------------------------------------------------------
FUNCTION MERGE_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                            I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE, 
                            I_process_id        IN      NUMBER)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'L10N_FLEX_API_SQL.MERGE_L10N_EXT_TBL';

   L_ext_table          VARCHAR2(30) := NULL;
   L_stg_table          VARCHAR2(30) := NULL;
   L_statment_string    VARCHAR2(4000) := NULL;
   L_col_list_string    VARCHAR2(4000) := NULL;
   L_value_list_string  VARCHAR2(4000) := NULL;

   L_groups      ID_TBL;
   L_key_cols    COLUMN_TBL;
   L_view_cols   view_col_rec;

  cursor C_EXT_TABLE is
      select leev.staging_table,
			 ee.l10n_ext_table
        from ext_entity ee,
             l10n_ext_entity_val leev
       where ee.base_rms_table = I_base_table
         and ee.ext_entity_id = leev.ext_entity_id;


   cursor C_KEY_COLS is
      select key_col
        from ext_entity_key
       where base_rms_table = I_base_table
    order by key_number;

   cursor C_ATTRIB_GROUPS is
      select g.group_id 
        from l10n_attrib_group g,
             ext_entity e
       where e.ext_entity_id  = g.ext_entity_id
         and g.country_id     = I_country_id
         and e.base_rms_table = I_base_table;

   cursor C_VIEW_COLS(I_group_id in L10N_ATTRIB_GROUP.GROUP_ID%TYPE) is
      select view_col_name, 
             group_id,
             attrib_storage_col tab_col_name
        from l10n_attrib
       where group_id = I_group_id
    order by attrib_storage_col;    

BEGIN

   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_country_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
  
   if I_base_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_base_table',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'I_process_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- 
   open C_EXT_TABLE;
   fetch C_EXT_TABLE into L_stg_table,L_ext_table;
   close C_EXT_TABLE;
   if L_stg_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if L_ext_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   L_stg_table := 'STG_' || L_ext_table || '_' || I_country_id;  -- staging table name, eg. STG_STORE_L10N_EXT_BR

   open C_KEY_COLS;
   fetch C_KEY_COLS bulk collect into L_key_cols;
   close C_KEY_COLS;

   if L_key_cols is NULL or L_key_cols.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_KEY_NOT_FOUND',
                                            I_base_table,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_ATTRIB_GROUPS;
   fetch C_ATTRIB_GROUPS bulk collect into L_groups;
   close C_ATTRIB_GROUPS;

   if L_groups is NULL or L_groups.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('ATTR_GRP_NOT_FOUND',
                                            I_base_table,
                                            I_country_id,
                                            NULL);
      return FALSE;
   end if;

   -- build 1 merge statement for each group
   for k in L_groups.first .. L_groups.last loop
      L_statment_string := 'merge into ' || L_ext_table || ' ext using ( select ';
      
      for i in L_key_cols.first .. L_key_cols.last loop
         L_statment_string := L_statment_string || L_key_cols(i) || ', ';
      end loop;
      
      open C_VIEW_COLS(L_groups(k));
      fetch C_VIEW_COLS bulk collect into L_view_cols.view_col_names,
                                          L_view_cols.group_ids,
                                          L_view_cols.tab_col_names;
      close C_VIEW_COLS;
      L_statment_string := L_statment_string || '''' || I_country_id  || '''' ||' l10n_country_id, ' || L_groups(k) || ' group_id';
      
      
      for i in L_view_cols.view_col_names.FIRST .. L_view_cols.view_col_names.LAST loop
         L_statment_string := L_statment_string || ', ' || L_view_cols.view_col_names(i);
      end loop;
      
      L_statment_string := L_statment_string || 
                         ' from ' || L_stg_table ||
                         ' where process_id = ' || I_process_id;
                         
      L_statment_string := L_statment_string || ') stg on ( ';
      
     for i in L_key_cols.first .. L_key_cols.last loop
         if i = 1 then
            L_statment_string := L_statment_string || ' stg.' || L_key_cols(i) || ' = ext.' || L_key_cols(i);
         else
            L_statment_string := L_statment_string || ' and stg.' || L_key_cols(i) || ' = ext.' || L_key_cols(i);
         end if;
      end loop;
      L_statment_string := L_statment_string || ' and stg.l10n_country_id = ext.l10n_country_id and stg.group_id = ext.group_id';
      L_statment_string := L_statment_string || ') when matched then update set';
      
      open C_VIEW_COLS(L_groups(k));
      fetch C_VIEW_COLS bulk collect into L_view_cols.view_col_names,
                                          L_view_cols.group_ids,
                                          L_view_cols.tab_col_names;
      close C_VIEW_COLS;
   
      for i in L_view_cols.tab_col_names.FIRST .. L_view_cols.tab_col_names.LAST loop
         if i = 1 then
            L_statment_string := L_statment_string || ' ext.' || L_view_cols.tab_col_names(i) || ' = stg.' || L_view_cols.view_col_names(i);
         else
            L_statment_string := L_statment_string || ', ext.' || L_view_cols.tab_col_names(i) || ' = stg.' || L_view_cols.view_col_names(i);
         end if;
      end loop;
      
      L_statment_string := L_statment_string || ' when not matched then insert (';
      
      for i in L_key_cols.first .. L_key_cols.last loop
         L_statment_string := L_statment_string || 'ext.' || L_key_cols(i) || ', ';
      end loop;
      
      L_statment_string := L_statment_string || 'ext.l10n_country_id, ext.group_id';
      
      for i in L_view_cols.tab_col_names.FIRST .. L_view_cols.tab_col_names.LAST loop
         L_statment_string := L_statment_string || ', ext.' || L_view_cols.tab_col_names(i);
      end loop;
      
      L_statment_string := L_statment_string || ') values (';
      
      for i in L_key_cols.first .. L_key_cols.last loop
         L_statment_string := L_statment_string || 'stg.' || L_key_cols(i) || ', ';
      end loop;
      
      L_statment_string := L_statment_string || 'stg.l10n_country_id, stg.group_id';
      
      for i in L_view_cols.view_col_names.FIRST .. L_view_cols.view_col_names.LAST loop
         L_statment_string := L_statment_string || ', stg.' || L_view_cols.view_col_names(i);
      end loop;
      
      L_statment_string := L_statment_string || ')';

      EXECUTE IMMEDIATE L_statment_string;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END MERGE_L10N_EXT_TBL;   
---------------------------------------------------------------------------------------------------
END L10N_FLEX_API_SQL;
/