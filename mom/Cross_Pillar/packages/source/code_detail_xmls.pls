
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CODE_DETAIL_XML AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
FUNCTION BUILD_MESSAGE(O_status       	     		OUT VARCHAR2,
                       O_text         	   	  	OUT VARCHAR2,
                       O_code_detail_msg 	            OUT rib_sxw.SXWHandle,
                       I_code_detail_rec           	IN  CODE_DETAIL%ROWTYPE,
                       I_action_type  	     		IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END CODE_DETAIL_XML;
/
