create or replace 
PACKAGE CFA_FUNCTIONS_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- CFAS return value constants
--------------------------------------------------------------------------------
CFA_TRUE    CONSTANT NUMBER := 1;
CFA_FALSE   CONSTANT NUMBER := 0;
---------
CFA_DEFAULT             CONSTANT VARCHAR2(20) := 'DEFAULT';
CFA_QUALIFY             CONSTANT VARCHAR2(20) := 'QUALIFY';
CFA_VALIDATE_ENTITY     CONSTANT VARCHAR2(20) := 'VALIDATE_ENTITY';
CFA_VALIDATE_GROUP_SET  CONSTANT VARCHAR2(20) := 'VALIDATE_GROUP_SET';
CFA_VALIDATE_ATTRIBUTE  CONSTANT VARCHAR2(20) := 'VALIDATE_ATTRIBUTE';
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Name:    VALIDATE_EXT_ENTITY
-- Purpose: This function calls the custom validate function for validating all 
--          the entity�s custom attributes
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_EXT_ENTITY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Name:    DEFAULT_GROUP_SET
-- Purpose: This function is used to pre-populate attribute fields in the group set
--          upon startup of the Flex UI.
--------------------------------------------------------------------------------
FUNCTION  DEFAULT_GROUP_SET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                            I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Name:    QUALIFY_GROUP_SET
-- Purpose: This function is used to check if the user is allowed to access the group set
--------------------------------------------------------------------------------
FUNCTION QUALIFY_GROUP_SET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC,
                           I_group_set_id  IN     CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP_SET
-- Purpose: This function validates the flex UI attributes within a group set
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_GROUP_SET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                             I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_ATTRIBUTE
-- Purpose: This function validates a flex UI attribute.
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_ATTRIBUTE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                             I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC,
                             I_attrib_id     IN     CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Name:    GET_CUSTOM_FUNCTION_NAME
-- Purpose: This function retrieves the custom function
--------------------------------------------------------------------------------
FUNCTION GET_CUSTOM_FUNCTION_NAME(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_function         OUT CFA_ATTRIB_GROUP_SET.QUALIFIER_FUNC%TYPE,
                                  I_function_type IN     VARCHAR2,
                                  I_base_table    IN     CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                                  I_group_set_id  IN     CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                                  I_attrib_id     IN     CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Name:    EXEC_FUNCTION
-- Purpose: This function is used to dynamically execute metadata defined external functions
--------------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_return_code   IN OUT NUMBER,
                       I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                       I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC,
                       I_function      IN     VARCHAR2,
                       I_action        IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_FUNCTION
-- Purpose: This function validates the custom functions entered
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_FUNCTION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_function      IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Name:    GET_GROUP_SETS
-- Purpose: This function get all the active group sets for a given base table 
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SETS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_base_table     IN     CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        O_group_sets_tbl    OUT CFA_ENTITY_GROUP_SETS_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END CFA_FUNCTIONS_SQL;
/