CREATE OR REPLACE PACKAGE BODY L10N_FLEX_SETUP_SQL AS
---------------------------------------------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ENTITY_KEY_DESCS (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
                                  I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE) IS

   L_cnt    NUMBER := 0;

   cursor C_GET_KEY_DESCS is
      select ex.base_rms_table,
             ex.key_col,
             ex.key_number,
             ex.data_type,
             ex.l10n_dependency_ind,
             ex.description_code,
             I_lang lang,
             kd.key_desc,
             kd.default_lang_ind
        from ext_entity_key ex,
             ext_entity_key_descs kd
       where ex.base_rms_table = kd.base_rms_table (+)
         and ex.key_col = kd.key_col (+)
         and ex.base_rms_table = I_base_rms_table
         and kd.lang (+) = I_lang
       order by ex.key_number;
BEGIN
   ---
   FOR rec in C_GET_KEY_DESCS LOOP
      L_cnt := L_cnt + 1;
      ---
      IO_l10n_ext_entity_tbl(L_cnt).base_rms_table := rec.base_rms_table;
      IO_l10n_ext_entity_tbl(L_cnt).key_col := rec.key_col;
      IO_l10n_ext_entity_tbl(L_cnt).key_number := rec.key_number;
      IO_l10n_ext_entity_tbl(L_cnt).data_type := rec.data_type;
      IO_l10n_ext_entity_tbl(L_cnt).lang := rec.lang;
      IO_l10n_ext_entity_tbl(L_cnt).key_desc := rec.key_desc;
      IO_l10n_ext_entity_tbl(L_cnt).default_lang_ind := rec.default_lang_ind;
      IO_l10n_ext_entity_tbl(L_cnt).description_code := rec.description_code;
   END LOOP;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_l10n_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    'L10N_FLEX_SETUP_SQL.QUERY_ENTITY_KEY_DESCS',
                                                                    to_char(SQLCODE));
      IO_l10n_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_l10n_ext_entity_tbl(1).error_message);

END QUERY_ENTITY_KEY_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ENTITY_KEY_DESCS  (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
                                  I_key                     IN        EXT_ENTITY_KEY_DESCS.KEY_COL%TYPE,
                                  I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_KEY_DESCS is
      select 'x'
        from ext_entity_key_descs
       where base_rms_table = I_base_rms_table
         and key_col = I_key
         and lang = I_lang
         for update nowait;

BEGIN
   ---
   open C_LOCK_KEY_DESCS;
   close C_LOCK_KEY_DESCS;
   ---
   return;
   ---
EXCEPTION

   when RECORD_LOCKED then
      IO_l10n_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                    'EXT_ENTITY_KEY_DESCS',
                                                                    I_base_rms_table||', '||
                                                                    I_key||', '||
                                                                    I_lang);
      IO_l10n_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(-20002, IO_l10n_ext_entity_tbl(1).error_message);
   when OTHERS then
      IO_l10n_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          'L10N_FLEX_SETUP_SQL.LOCK_ENTITY_KEY_DESCS',
                                                          to_char(SQLCODE));
      IO_l10n_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_l10n_ext_entity_tbl(1).error_message);

END LOCK_ENTITY_KEY_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ENTITY_KEY_DESCS (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                  I_base_rms_table          IN        EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                  I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE,
                                  I_default_ind             IN        EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_DESC_DEFAULT is
      select kd.rowid
        from ext_entity_key_descs kd
       where kd.base_rms_table = I_base_rms_table
         for update nowait;

  cursor C_LOCK_KEY_DEFAULT is
      select ek.rowid
        from ext_entity_key ek
       where ek.base_rms_table = I_base_rms_table
         for update nowait;

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid TYP_rowid;

   TYPE TYP_rowid_key is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid_key TYP_rowid_key;

BEGIN
   ---
   if I_default_ind = 'Y' then
      open C_LOCK_DESC_DEFAULT;
      fetch C_LOCK_DESC_DEFAULT BULK COLLECT into TBL_rowid;
      close C_LOCK_DESC_DEFAULT;

      FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
        update ext_entity_key_descs
           set default_lang_ind = 'N'
          where rowid = TBL_rowid(i);
   end if;
   ---
   open C_LOCK_KEY_DEFAULT;
   fetch C_LOCK_KEY_DEFAULT BULK COLLECT into TBL_rowid_key;
   close C_LOCK_KEY_DEFAULT;

   FORALL i in IO_l10n_ext_entity_tbl.FIRST..IO_l10n_ext_entity_tbl.LAST
      update ext_entity_key
         set description_code = IO_l10n_ext_entity_tbl(i).description_code
       where base_rms_table   = IO_l10n_ext_entity_tbl(i).base_rms_table
         and key_col          = IO_l10n_ext_entity_tbl(i).key_col;


   FORALL i in IO_l10n_ext_entity_tbl.FIRST..IO_l10n_ext_entity_tbl.LAST
      merge into ext_entity_key_descs kd
         using (select IO_l10n_ext_entity_tbl(i).base_rms_table base_rms_table,
                       IO_l10n_ext_entity_tbl(i).key_col key_col,
                       IO_l10n_ext_entity_tbl(i).lang lang,
                       IO_l10n_ext_entity_tbl(i).key_desc key_desc,
                       I_default_ind default_lang_ind
                  from dual) tmp
         on (kd.base_rms_table = tmp.base_rms_table and
             kd.key_col = tmp.key_col and
             kd.lang = tmp.lang)
         when matched then
            update
               set kd.key_desc = tmp.key_desc,
                   kd.default_lang_ind = tmp.default_lang_ind
         when not matched then
            insert(base_rms_table,
                   key_col,
                   lang,
                   key_desc,
                   default_lang_ind)
            values(tmp.base_rms_table,
                   tmp.key_col,
                   tmp.lang,
                   tmp.key_desc,
                   tmp.default_lang_ind);
   ---
   return;
   ---
EXCEPTION
   when OTHERS then      
      IO_l10n_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          'L10N_FLEX_SETUP_SQL.MERGE_ENTITY_KEY_DESCS',
                                                          to_char(SQLCODE));
      IO_l10n_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_l10n_ext_entity_tbl(1).error_message);

END MERGE_ENTITY_KEY_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE DELETE_ENTITY_KEY_DESCS (IO_l10n_ext_entity_tbl    IN OUT    L10N_EXT_ENTITY_KEY_TBL,
                                   I_base_rms_table          IN        EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE,
                                   I_key_col                 IN        EXT_ENTITY_KEY_DESCS.KEY_COL%TYPE,
                                   I_lang                    IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_ENTITY_DESC is
      select 'x'
        from ext_entity_key_descs
       where base_rms_table = I_base_rms_table
         and key_col        = I_key_col
         and lang           = I_lang
         for update nowait;
         
   cursor C_LOCK_ENTITY_KEY is
      select 'x'
        from ext_entity_key
       where base_rms_table = I_base_rms_table
         and key_col        = I_key_col         
         for update nowait;

BEGIN
   ---
   open C_LOCK_ENTITY_DESC;
   close C_LOCK_ENTITY_DESC;

   delete
     from ext_entity_key_descs
    where base_rms_table = I_base_rms_table
      and key_col        = I_key_col
      and lang           = I_lang;
   ---
   open C_LOCK_ENTITY_KEY;
   close C_LOCK_ENTITY_KEY;
   
   update ext_entity_key
      set  description_code = NULL
    where base_rms_table = I_base_rms_table
      and key_col        = I_key_col;
   
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_l10n_ext_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                          'L10N_FLEX_SETUP_SQL.DELETE_ENTITY_KEY_DESCS',
                                                          to_char(SQLCODE));
      IO_l10n_ext_entity_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_l10n_ext_entity_tbl(1).error_message);

END DELETE_ENTITY_KEY_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB_DESCS (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_group_id              IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE) IS

   L_cnt   NUMBER := 0;

   cursor C_GET_ATTRIB_DESCS is
      select att.attrib_id,
             att.view_col_name,
             ad.description,
             I_lang lang,
             ad.default_lang_ind
        from l10n_attrib_descs ad,
             l10n_attrib att
       where ad.attrib_id (+) = att.attrib_id
         and att.group_id = I_group_id
         and ad.lang (+) = I_lang
       order by att.attrib_id;

BEGIN
   ---
   FOR rec in C_GET_ATTRIB_DESCS LOOP
       L_cnt := L_cnt + 1;
       ---
       IO_attrib_desc_tbl(L_cnt).attrib_id := rec.attrib_id;
       IO_attrib_desc_tbl(L_cnt).view_col_name := rec.view_col_name;
       IO_attrib_desc_tbl(L_cnt).description := rec.description;
       IO_attrib_desc_tbl(L_cnt).lang := rec.lang;
       IO_attrib_desc_tbl(L_cnt).default_lang_ind := rec.default_lang_ind;
   END LOOP;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          'L10N_FLEX_SETUP_SQL.QUERY_ATTRIB_DESCS',
                                                          to_char(SQLCODE));
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_desc_tbl(1).error_message);

END QUERY_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB_DESCS  (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_group_id              IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_attrib_id             IN        L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_ATTRIB_DESCS is
      select 'x'
        from l10n_attrib_descs
       where attrib_id = I_attrib_id
         and lang = I_lang
         for update nowait;

BEGIN
   ---
   open C_LOCK_ATTRIB_DESCS;
   close C_LOCK_ATTRIB_DESCS;
   ---
   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                'L10N_ATTRIB_DESCS',
                                                                I_attrib_id);
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_desc_tbl(1).error_message);
   when OTHERS then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                'L10N_FLEX_SETUP_SQL.LOCK_ATTRIB_DESCS',
                                                                to_char(SQLCODE));
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_desc_tbl(1).error_message);

END LOCK_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB_DESCS (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_group_id              IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE,
                              I_default_lang_ind      IN        L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_DESC_DEFAULT is
      select ad.rowid
        from l10n_attrib_descs ad,
             l10n_attrib att
       where att.attrib_id = ad.attrib_id
         and att.group_id = I_group_id
         for update of ad.default_lang_ind nowait;

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid TYP_rowid;

BEGIN
   ---
   if I_default_lang_ind = 'Y' then
      open C_LOCK_DESC_DEFAULT;
      fetch C_LOCK_DESC_DEFAULT BULK COLLECT into TBL_rowid;
      close C_LOCK_DESC_DEFAULT;

      FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
        update l10n_attrib_descs
           set default_lang_ind = 'N'
          where rowid = TBL_rowid(i);
   end if;
   ---
   FORALL i in IO_attrib_desc_tbl.FIRST..IO_attrib_desc_tbl.LAST
      merge into l10n_attrib_descs ad
         using (select IO_attrib_desc_tbl(i).attrib_id attrib_id,
                       IO_attrib_desc_tbl(i).lang lang,
                       IO_attrib_desc_tbl(i).description description,
                       I_default_lang_ind default_lang_ind
                  from dual) tmp
         on (ad.attrib_id = tmp.attrib_id and
             ad.lang = tmp.lang)
         when matched then
            update
               set ad.description = tmp.description,
                   ad.default_lang_ind = tmp.default_lang_ind
         when not matched then
            insert(attrib_id,
                   lang,
                   description,
                   default_lang_ind)
            values(tmp.attrib_id,
                   tmp.lang,
                   tmp.description,
                   tmp.default_lang_ind);
   ---
   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                'L10N_ATTRIB_DESCS',
                                                                I_group_id);
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(-20002, IO_attrib_desc_tbl(1).error_message);
   when OTHERS then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                'L10N_FLEX_SETUP_SQL.MERGE_ATTRIB_DESCS',
                                                                 to_char(SQLCODE));
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_desc_tbl(1).error_message);

END MERGE_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
PROCEDURE DELETE_ATTRIB_DESCS (IO_attrib_desc_tbl      IN OUT    L10N_ATTRIB_DESC_TBL,
                              I_attrib_id              IN       L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE,
                              I_lang                  IN        L10N_ATTRIB_DESCS.LANG%TYPE) IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);
  
   cursor C_LOCK_ATTRIB_DESC is
      select 'x'
        from l10n_attrib_descs 
       where attrib_id = I_attrib_id
         and lang      = I_lang         
         for update nowait;
         
BEGIN
   ---  
   open C_LOCK_ATTRIB_DESC;     
   close C_LOCK_ATTRIB_DESC;

   delete
     from l10n_attrib_descs       
    where attrib_id = I_attrib_id
      and lang      = I_lang;
  
   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                'L10N_ATTRIB_DESCS',
                                                                I_attrib_id);
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(-20002, IO_attrib_desc_tbl(1).error_message);
   when OTHERS then
      IO_attrib_desc_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                'L10N_FLEX_SETUP_SQL.DELETE_ATTRIB_DESCS',
                                                                 to_char(SQLCODE));
      IO_attrib_desc_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_desc_tbl(1).error_message);

END DELETE_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
-- FUNCTIONS
---------------------------------------------------------------------------------------------------
FUNCTION GEN_EXT_TABLE (O_error_message     IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists            IN OUT    BOOLEAN,
                        I_table             IN        EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        I_ext_table         IN        EXT_ENTITY.L10N_EXT_TABLE%TYPE)
RETURN BOOLEAN IS
   
   L_program           VARCHAR2(60) := 'FLEX_SQL.GEN_EXT_TAB_DDL';
   L_tab_ddl           VARCHAR2(4000);
   L_tab_owner         SYSTEM_OPTIONS.TABLE_OWNER%TYPE;
   L_exists            VARCHAR2(1);
   L_prim_key_ddl      VARCHAR2(500);
   L_foreign_key_ddl   VARCHAR2(500);
   L_prim_key_cols     VARCHAR2(500);

   L_misc_cols VARCHAR2(1000) := 'COUNTRY_ID VARCHAR2(3) NOT NULL,'||
                                 'GROUP_NO   NUMBER(10) NOT NULL,'||
                                 'VARCHAR2_1  VARCHAR2(250),'||
                                 'VARCHAR2_2  VARCHAR2(250),'||
                                 'VARCHAR2_3  VARCHAR2(250),'||
                                 'VARCHAR2_4  VARCHAR2(250),'||
                                 'VARCHAR2_5  VARCHAR2(250),'||
                                 'VARCHAR2_6  VARCHAR2(250),'||
                                 'VARCHAR2_7  VARCHAR2(250),'||
                                 'VARCHAR2_8  VARCHAR2(250),'||
                                 'VARCHAR2_9  VARCHAR2(250),'||
                                 'VARCHAR2_10 VARCHAR2(250),'||
                                 'NUMBER_11  NUMBER,'||
                                 'NUMBER_12  NUMBER,'||
                                 'NUMBER_13  NUMBER,'||
                                 'NUMBER_14  NUMBER,'||
                                 'NUMBER_15  NUMBER,'||
                                 'NUMBER_16  NUMBER,'||
                                 'NUMBER_17  NUMBER,'||
                                 'NUMBER_18  NUMBER,'||
                                 'NUMBER_19  NUMBER,'||
                                 'NUMBER_20  NUMBER,'||
                                 'DATE_21    DATE,'||
                                 'DATE_22    DATE';
   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;
/*
   cursor C_HEADER is
      select 'create table '||L_tab_owner||'.'||I_ext_table||'( ' text  from dual;*/

   cursor C_HEADER is
      select 'create table '||I_ext_table||'( ' text from dual;

   cursor C_PRIM_KEY is
      select 'alter table '||I_ext_table||' add primary key (' text from dual;

   cursor C_FOREIGN_KEY is
      select 'alter table '||I_ext_table||' add foreign key (' text from dual;

   cursor C_GET_COL_DEF is
      select a.column_name||' '||a.data_type||decode(a.data_type,'NUMBER','('||nvl(a.data_precision, 0)||')','VARCHAR2','('||a.data_length||')','DATE',NULL)|| decode (nullable,'N',' NOT NULL')||',' col,
             a.column_name
        from all_tab_columns a, all_constraints b, all_cons_columns c
       where a.table_name  = I_table
         and a.owner       = L_tab_owner
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'P'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
       order by c.position;

/*
   cursor C_CHECK_TABLE is
      select 'Y'
        from all_tables
       where owner = L_tab_owner
         and table_name = I_ext_table;*/

   cursor C_CHECK_TABLE is
      select 'Y'
        from all_tables
       where owner = USER
         and table_name = I_ext_table;

BEGIN
   ---
   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_tab_owner;
   close C_GET_TAB_OWNER;
   ---
   open C_CHECK_TABLE;
   fetch C_CHECK_TABLE into L_exists;
   close C_CHECK_TABLE;
   ---
   if L_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      O_exists := TRUE;
      return TRUE;
   end if;
   ---
   FOR i in C_HEADER LOOP
      L_tab_ddl := L_tab_ddl ||i.text;
   END LOOP;
   ---
   FOR i in C_PRIM_KEY LOOP
      L_prim_key_ddl := i.text;
   END LOOP;
   ---
   FOR i in C_FOREIGN_KEY LOOP
      L_foreign_key_ddl := i.text;
   END LOOP;
   ---
   FOR i in C_GET_COL_DEF LOOP
      L_tab_ddl := L_tab_ddl ||i.col;
      L_prim_key_cols := L_prim_key_cols||i.column_name||',';
   END LOOP;
   ---
   L_prim_key_cols := substr(L_prim_key_cols,1,length(L_prim_key_cols)-1);
   L_prim_key_ddl := L_prim_key_ddl||L_prim_key_cols||',COUNTRY_ID,GROUP_NO)';
   L_foreign_key_ddl := L_foreign_key_ddl||L_prim_key_cols||')'||' REFERENCES '||I_table||'('||L_prim_key_cols||')';
   L_tab_ddl := L_tab_ddl || L_misc_cols||')';
   ---
   --insert_query(L_tab_ddl);
   --insert_query(L_prim_key_ddl);
   --insert_query(L_foreign_key_ddl);
   ---
   EXECUTE IMMEDIATE L_tab_ddl;
   EXECUTE IMMEDIATE L_prim_key_ddl;
   EXECUTE IMMEDIATE L_foreign_key_ddl;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GEN_EXT_TABLE;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_VIEW (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                   I_table             IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                   I_ext_table         IN      EXT_ENTITY.L10N_EXT_TABLE%TYPE)
RETURN BOOLEAN IS

   L_prim_col           VARCHAR2(4000);
   L_view_def           VARCHAR2(4000);
   L_view_sel           VARCHAR2(4000);
   L_group_id           NUMBER;
   L_attrib_id          NUMBER;
   L_attrib_desc        VARCHAR2(50);
   L_exists             VARCHAR2(1);
   L_col                VARCHAR2(500);
   L_column_list        VARCHAR2(500);
   L_prev_column_list   VARCHAR2(500);
   L_table_owner        SYSTEM_OPTIONS.TABLE_OWNER%TYPE;
   L_program            VARCHAR2(60) := 'FLEX_SQL.GEN_VIEW';

   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;

   cursor C_PRIM_KEY is
      select a.column_name
        from all_tab_columns a,
               all_constraints b,
               all_cons_columns c
       where a.table_name  =I_table
         and a.owner       = L_table_owner
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'P'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
       order by c.position;

   cursor C_GROUPS is
       select distinct grp.group_id group_id
        from l10n_attrib attr,
             l10n_attrib_group grp,
             ext_entity ent
       where ent.ext_entity_id = grp.ext_entity_id
         and grp.group_id = attr.group_id
         and ent.base_rms_table = I_table
         and grp.country_id = I_country_id;

   cursor C_VIEW_COLS is
       select grp.group_id,
              attr.attrib_id,
              view_col_name,
              attrib_storage_col
        from l10n_attrib attr,
             l10n_attrib_group grp,
             ext_entity ent
       where ent.ext_entity_id = grp.ext_entity_id
         and grp.group_id = attr.group_id
         and ent.base_rms_table = I_table
         and grp.country_id = I_country_id
         and grp.group_id = L_group_id;

BEGIN
   ---
   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_table_owner;
   close C_GET_TAB_OWNER;
   ---
   L_view_def := 'CREATE OR REPLACE VIEW '||I_table||'_'||I_country_id||'_FV ( ';
   FOR i in C_PRIM_KEY LOOP
          L_prim_col := L_prim_col||i.column_name||',';
   END LOOP;
   ---
   L_view_def := L_view_def || L_prim_col;
   L_column_list := L_prim_col;
   ---
   FOR j in C_GROUPS LOOP
      L_group_id := j.group_id;
      L_column_list := NULL;
      FOR i in C_VIEW_COLS LOOP
         L_attrib_desc := i.view_col_name;
         L_view_def := L_view_def ||L_attrib_desc||',';
         L_column_list := L_column_list ||i.attrib_storage_col||' '||L_attrib_desc||',';
      END LOOP;
      ---
      if L_column_list is NOT NULL then
         L_column_list := substr(L_column_list,1,length(L_column_list)-1);
         if L_prev_column_list is  NOT NULL then
            L_view_sel := L_view_sel||' natural join '||'(SELECT ' || L_prim_col || L_column_list || ' from '||I_ext_table||' where group_id ='||L_group_id||' and country_id = '||''''||I_country_id||''''||')';
         else
            L_view_sel := L_view_sel||'(SELECT ' || L_prim_col || L_column_list || ' from '||I_ext_table||' where group_id ='||L_group_id||' and country_id = '||''''||I_country_id||''''||')';
         end if;
         L_prev_column_list := L_column_list;
      end if;
      ---
   END LOOP;
   ---
   L_view_def := substr(L_view_def,1,length(L_view_def)-1) || ' ) AS SELECT * FROM ';
   L_column_list := substr(L_column_list,1,length(L_column_list)-1);
   ---
   L_view_def := L_view_def || L_view_sel;
   --insert_query(l_view_def);
   ---
   EXECUTE IMMEDIATE L_view_def;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GEN_VIEW;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_VIEW_GROUP (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_group_id          IN      L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_prim_col           VARCHAR2(4000);
   L_view_def           VARCHAR2(4000);
   L_view_sel           VARCHAR2(4000);
   L_view_name          L10N_ATTRIB_GROUP.GROUP_VIEW_NAME%TYPE;
   L_group_id           NUMBER;
   L_attrib_id          NUMBER;
   L_attrib_desc        VARCHAR2(50);
   L_exists             VARCHAR2(1);
   L_col                VARCHAR2(500);
   L_column_list        VARCHAR2(500);
   L_prev_column_list   VARCHAR2(500);
   L_table              EXT_ENTITY.BASE_RMS_TABLE%TYPE;
   L_ext_table          EXT_ENTITY.L10N_EXT_TABLE%TYPE;
   L_country_id         L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE;
   L_table_owner        SYSTEM_OPTIONS.TABLE_OWNER%TYPE;
   L_program            VARCHAR2(60) := 'FLEX_SQL.GEN_VIEW_GROUP';

   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;

   cursor C_PRIM_KEY is
      select a.column_name
        from all_tab_columns a,
             all_constraints b,
             all_cons_columns c
       where a.table_name  =L_table
         and a.owner       = L_table_owner
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'P'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
       order by c.position;

   cursor C_GROUPS is
      select ent.base_rms_table,
             ent.l10n_ext_table,
             grp.group_view_name,
             grp.country_id
        from l10n_attrib_group grp,
             ext_entity ent
       where ent.ext_entity_id = grp.ext_entity_id
         and grp.group_id = I_group_id;

   cursor C_VIEW_COLS is
      select grp.group_id,
             attr.attrib_id,
             view_col_name,
             attrib_storage_col
        from l10n_attrib attr,
             l10n_attrib_group grp
       where grp.group_id = attr.group_id
         and grp.group_id = I_group_id;

BEGIN
   ---
   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_table_owner;
   close C_GET_TAB_OWNER;
   ---
   open C_GROUPS;
   fetch C_GROUPS into L_table,
                       L_ext_table,
                       L_view_name,
                       L_country_id;
   close C_GROUPS;
   ---
   L_view_def := 'CREATE OR REPLACE VIEW '||L_view_name||'( ';
   
   FOR i in C_PRIM_KEY LOOP
      L_prim_col := L_prim_col||i.column_name||',';
   END LOOP;
   ---
   L_view_def := L_view_def || L_prim_col;
   L_column_list := NULL;
   ---
   FOR i in C_VIEW_COLS LOOP
      L_attrib_desc := i.view_col_name;
      L_view_def := L_view_def ||L_attrib_desc||',';
      L_column_list := L_column_list ||i.attrib_storage_col||' '||L_attrib_desc||',';
   END LOOP;
   ---
   if L_column_list is NOT NULL then
      L_column_list := substr(L_column_list,1,length(L_column_list)-1);
      if L_prev_column_list is NOT NULL then
         L_view_sel := L_view_sel||' natural join '||'(SELECT ' || L_prim_col || L_column_list || ' from '||L_ext_table||' where group_id ='||I_group_id||' and country_id = '||''''||L_country_id||''''||')';
      else
         L_view_sel := L_view_sel||'(SELECT ' || L_prim_col || L_column_list || ' from '||L_ext_table||' where group_id ='||I_group_id||' and country_id = '||''''||L_country_id||''''||')';
      end if;

      L_prev_column_list := L_column_list;
   end if;
   ---
   L_view_def := substr(L_view_def,1,length(L_view_def)-1) || ' ) AS SELECT * FROM ';
   L_column_list := substr(L_column_list,1,length(L_column_list)-1);
   L_view_def := L_view_def || L_view_sel;
   --insert_query(l_view_def);
   ---
   EXECUTE IMMEDIATE L_view_def;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END GEN_VIEW_GROUP;
--------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_EXT_ID(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_entity_id        IN OUT   EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS


   L_exists            VARCHAR2(1)  := 'N';
   L_wrap_entity_id    EXT_ENTITY.EXT_ENTITY_ID%TYPE;
   L_entity_id         EXT_ENTITY.EXT_ENTITY_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select ext_entity_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from ext_entity ext
       where ext.ext_entity_id = L_entity_id;

BEGIN
   ---
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_entity_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_entity_id := L_entity_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_entity_id := L_entity_id;         
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_entity_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_entity_id = L_wrap_entity_id then
         O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_ID_EXIST',
                                               L_entity_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('EXT_ENTITY_ID_EXIST',
                                            L_entity_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_NEXT_EXT_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_EXT_ID;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_INSERT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table        IN       EXT_ENTITY_KEY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

BEGIN
   ---
   insert into ext_entity_key(base_rms_table,
                              key_col,
                              key_number,
                              data_type,
                              l10n_dependency_ind)
                       select tac.table_name,
                              col.column_name ,
                              col.position,
                              tac.data_type,
                              'N'
                         from all_cons_columns col,
                              all_constraints con,
                              all_tab_columns tac,
                              system_options  opt
                        where con.constraint_name = col.constraint_name
                          and tac.owner       = con.owner
                          and tac.owner       = col.owner
                          and tac.table_name  = con.table_name
                          and tac.column_name = col.column_name
                          and con.owner       = opt.table_owner
                          and con.constraint_type = 'P'
                          and con.table_name = I_rms_table
                        order by position;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.EXT_KEYS_INSERT',
                                            to_char(SQLCODE));
      return FALSE;

END EXT_KEYS_INSERT;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_UPDATE(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_table        IN        EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_rms_table    IN        EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_new_ext_table    IN        EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                         I_ext_ent_id       IN        EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GROUP_EXT_ENT is
      select 'x'
        from l10n_attrib_group
       where ext_entity_id = I_ext_ent_id
         for update nowait;

   cursor C_EXT_ENT_KEYS is
      select 'x'
        from ext_entity_key
       where base_rms_table = I_rms_table
         for update nowait;

   cursor C_EXT_ENT_KEYS_DESCS is
      select 'x'
        from ext_entity_key_descs
       where base_rms_table = I_rms_table
         for update nowait;

   cursor C_EXT_ENTITY is
      select 'x'
        from ext_entity
       where ext_entity_id = I_ext_ent_id
         for update nowait;

BEGIN
   ---
   open C_GROUP_EXT_ENT;
   close C_GROUP_EXT_ENT;
   
   delete
     from l10n_attrib_group
    where ext_entity_id = I_ext_ent_id;
   ---
   open C_EXT_ENT_KEYS;
   close C_EXT_ENT_KEYS;
   
   delete
     from ext_entity_key
    where base_rms_table = I_rms_table;
   ---
   open C_EXT_ENT_KEYS_DESCS;
   close C_EXT_ENT_KEYS_DESCS;
   
   delete
     from ext_entity_key_descs
    where base_rms_table = I_rms_table;
   ---
   open C_EXT_ENTITY;
   close C_EXT_ENTITY;
   
   update ext_entity
      set base_rms_table = I_new_rms_table,
          l10n_ext_table = I_new_ext_table
    where ext_entity_id  = I_ext_ent_id;
   ---
   insert into ext_entity_key(base_rms_table,
                              key_col,
                              key_number,
                              data_type,
                              l10n_dependency_ind)
                       select tac.table_name,
                              col.column_name ,
                              col.position,
                              tac.data_type,
                              'N'
                         from all_cons_columns col,
                              all_constraints con,
                              all_tab_columns tac,
                              system_options  opt
                        where con.constraint_name = col.constraint_name
                          and tac.owner       = con.owner
                          and tac.owner       = col.owner
                          and tac.table_name  = con.table_name
                          and tac.column_name = col.column_name
                          and con.owner       = opt.table_owner
                          and con.constraint_type = 'P'
                          and con.table_name = I_new_rms_table
                        order by position;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.EXT_KEYS_UPDATE',
                                            to_char(SQLCODE));
   return FALSE;

END EXT_KEYS_UPDATE;
---------------------------------------------------------------------------------------------------
FUNCTION EXT_KEYS_DELETE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_base_rms_table    IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_ext_ent_id        IN       EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_grp_exists VARCHAR2(1);

   cursor C_GROUP_EXT_ENT is
      select 'x'
        from l10n_attrib_group
       where ext_entity_id = I_ext_ent_id
         for update nowait;

    cursor C_EXT_ENT_KEYS_DESCS is
      select 'x'
        from ext_entity_key_descs
       where base_rms_table = I_base_rms_table
         for update nowait;

   cursor C_EXT_ENT_KEYS is
      select 'x'
        from ext_entity_key
       where base_rms_table = I_base_rms_table
         for update nowait;

BEGIN
   ---
   open C_GROUP_EXT_ENT;
   fetch C_GROUP_EXT_ENT into L_grp_exists;
   close C_GROUP_EXT_ENT;
   ---
   if L_grp_exists is NOT NULL then
       O_error_message := SQL_LIB.CREATE_MSG('GROUP_EXISTS',
                                            I_base_rms_table,
                                            NULL,
                                            NULL);
       return FALSE;

   end if;
   ---
   open C_EXT_ENT_KEYS_DESCS;
   close C_EXT_ENT_KEYS_DESCS;
   ---
   delete
     from ext_entity_key_descs
    where base_rms_table = I_base_rms_table;
   ---
   open C_EXT_ENT_KEYS;
   close C_EXT_ENT_KEYS;
   ---
   delete
     from ext_entity_key
    where base_rms_table = I_base_rms_table;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.EXT_KEYS_DELETE',
                                            to_char(SQLCODE));
   return FALSE;

END EXT_KEYS_DELETE;
---------------------------------------------------------------------------------------------------
FUNCTION GET_EXT_TABLE_NAME(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_l10n_ext_table   IN OUT   EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                            I_base_rms_table   IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_owner        SYSTEM_OPTIONS.TABLE_OWNER%TYPE;
   L_table_name   ALL_TABLES.TABLE_NAME%TYPE;

   cursor C_CHK_RMS_TBL is
      select tab.table_name,
             tab.owner
        from all_tables tab,
             system_options opt
       where tab.owner = opt.table_owner
         and table_name = I_base_rms_table;

   cursor C_GET_EXT_TBL is
      select c2.table_name
        from all_constraints c1,
             all_constraints c2
       where c2.owner = c1.owner
         and c2.constraint_type = 'R'
         and c2.r_constraint_name = c1.constraint_name
         and c1.table_name = I_base_rms_table
         and c1.owner = L_owner
         and c2.table_name like '%_L10N_EXT'
         and c2.owner = L_owner;

BEGIN
   ---
   open C_CHK_RMS_TBL;
   fetch C_CHK_RMS_TBL into L_table_name,
                            L_owner;
   close C_CHK_RMS_TBL;
   ---
   if L_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_RMS_TABLE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;

   end if;
   ---
   open C_GET_EXT_TBL;
   fetch C_GET_EXT_TBL into O_l10n_ext_table;
   close C_GET_EXT_TBL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'L10N_FLEX_SETUP.GET_EXT_TABLE_NAME',
                                             to_char(SQLCODE));
      return FALSE;

END GET_EXT_TABLE_NAME;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_EXT_KEY_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_default_ind     IN OUT    EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE,
                                  I_base_rms_table  IN        EXT_ENTITY_KEY_DESCS.BASE_RMS_TABLE%TYPE,
                                  I_lang            IN        EXT_ENTITY_KEY_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   cursor C_CHK_DEFAULT is
      select kd.default_lang_ind
        from ext_entity_key_descs kd
       where kd.base_rms_table = I_base_rms_table
         and kd.lang = I_lang
         and rownum = 1;

BEGIN
   ---
   O_default_ind := 'N';
   ---
   open C_CHK_DEFAULT;
   fetch C_CHK_DEFAULT into O_default_ind;
   close C_CHK_DEFAULT;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'L10N_FLEX_SETUP.CHK_EXT_KEY_DEFAULT_LANG',
                                             to_char(SQLCODE));
      return FALSE;

END CHK_EXT_KEY_DEFAULT_LANG;
--------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ENTITY_DEFAULT_LANG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists         IN OUT VARCHAR2,
                                    I_base_rms_table IN     EXT_ENTITY_KEY_DESCS.DEFAULT_LANG_IND%TYPE)                              
RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
      select 'x'
        from ext_entity_key_descs
       where base_rms_table = I_base_rms_table
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;
   ---      
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_NO_ENTITY_DEFAULT_LANG',
                                            to_char(SQLCODE));
   return FALSE;

END CHK_NO_ENTITY_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION GET_EXT_TABLE_NAME (O_error_message    IN OUT   VARCHAR2,
                             O_l10n_ext_table   IN OUT   EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                             I_base_rms_table   IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_owner        SYSTEM_OPTIONS.TABLE_OWNER%TYPE;
   L_table_name   ALL_TABLES.TABLE_NAME%TYPE;

   cursor C_CHK_RMS_TBL is
      select tab.table_name,
             tab.owner
        from all_tables tab,
             system_options opt
       where tab.owner = opt.table_owner
         and table_name = I_base_rms_table;

   cursor C_GET_EXT_TBL is
      select c2.table_name
        from all_constraints c1,
             all_constraints c2
       where c2.owner = c1.owner
         and c2.constraint_type = 'R'
         and c2.r_constraint_name = c1.constraint_name
         and c1.table_name = I_base_rms_table
         and c1.owner = L_owner
         and c2.table_name like '%_L10N_EXT'
         and c2.owner = L_owner;

BEGIN
   ---
   open C_CHK_RMS_TBL;
   fetch C_CHK_RMS_TBL into L_table_name,
                            L_owner;
   close C_CHK_RMS_TBL;
   ---
   if L_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_RMS_TABLE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;

   end if;
   ---
   open C_GET_EXT_TBL;
   fetch C_GET_EXT_TBL into O_l10n_ext_table;
   close C_GET_EXT_TBL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'L10N_FLEX_SETUP.GET_EXT_TABLE_NAME',
                                             to_char(SQLCODE));
      return FALSE;

END GET_EXT_TABLE_NAME;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_GROUP_ID(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           O_group_id         IN OUT    L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_exists            VARCHAR2(1)  := 'N';
   L_wrap_group_id     L10N_ATTRIB_GROUP.GROUP_ID%TYPE;
   L_group_id          L10N_ATTRIB_GROUP.GROUP_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select l10n_attrib_group_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from l10n_attrib_group ag
       where ag.group_id = L_group_id;

BEGIN
   ---
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_group_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_group_id := L_group_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_group_id := L_group_id;
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_group_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_group_id = L_wrap_group_id then
         O_error_message := SQL_LIB.CREATE_MSG('GROUP_ID_EXIST',
                                               L_group_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('GROUP_ID_EXIST',
                                            L_group_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_NEXT_GROUP_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_GROUP_ID;
---------------------------------------------------------------------------------------------------
FUNCTION GRP_DISP_ORD_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   VARCHAR2,
                            I_ext_entity_id   IN       L10N_ATTRIB_GROUP.EXT_ENTITY_ID%TYPE,
                            I_country_id      IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                            I_group_id        IN       L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                            I_disp_order      IN       L10N_ATTRIB_GROUP.DISPLAY_ORDER%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_DISP_ORD is
      select 'x'
        from l10n_attrib_group
       where ext_entity_id = I_ext_entity_id
         and country_id = I_country_id
         and display_order = I_disp_order
         and group_id != I_group_id;

BEGIN
   ---
   open C_DISP_ORD;
   fetch C_DISP_ORD into L_exist;
   close C_DISP_ORD;
   ---
   if L_exist is NOT NULL then
      O_exist := 'Y';
   else
      O_exist := 'N';
   end if;  
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GRP_DISP_ORD_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END GRP_DISP_ORD_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_GRP_DESC_LANG(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_group_id         IN       L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                               I_lang             IN       L10N_ATTRIB_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   L_exists    VARCHAR2(1) := NULL;

   cursor C_GET_LANG is
      select 'x'
        from l10n_attrib_group_descs
       where group_id = I_group_id
         and lang = I_lang;

BEGIN
   ---
   open C_GET_LANG;
   fetch C_GET_LANG into L_exists;
   close C_GET_LANG;
   ---
   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_LANG_SELECT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

   end if;  
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_DUP_GRP_DESC_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_DUP_GRP_DESC_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION GROUP_VIEW_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist            IN OUT   VARCHAR2,
                           I_grp_view_name    IN       L10N_ATTRIB_GROUP.GROUP_VIEW_NAME%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_GRP_VIEW is
      select 'x'
        from l10n_attrib_group
       where group_view_name = I_grp_view_name;

BEGIN
   ---
   open C_GRP_VIEW;
   fetch C_GRP_VIEW into L_exist;
   close C_GRP_VIEW;
   ---
   if L_exist is NOT NULL then
      O_exist := 'Y';
   else
      O_exist := 'N';
   end if;  
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.GROUP_VIEW_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END GROUP_VIEW_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_GROUP_DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_group_id         IN       L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   L_exists       VARCHAR2(1);

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid TYP_rowid;

   cursor C_LOCK_GROUP_DESCS is
      select rowid
        from l10n_attrib_group_descs
       where group_id = I_group_id
         for update nowait;

   cursor C_CHK_ATTRIB_EXIST is
      select 'x'
        from l10n_attrib
       where group_id = I_group_id
         for update nowait;

BEGIN
   ---
   open C_CHK_ATTRIB_EXIST;
   fetch C_CHK_ATTRIB_EXIST into L_exists;
   close C_CHK_ATTRIB_EXIST;
   ---
   if L_exists is NOT NULL then      
      O_error_message := SQL_LIB.CREATE_MSG('ATTRIB_EXISTS',
                                            I_group_id,
                                            NULL,
                                            NULL);
      return FALSE;
      
   end if;   
   ---
   open C_LOCK_GROUP_DESCS;
   fetch C_LOCK_GROUP_DESCS BULK COLLECT into TBL_rowid;
   close C_LOCK_GROUP_DESCS;
   ---
   FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
      delete
        from l10n_attrib_group_descs
       where rowid = TBL_rowid(i);
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'L10N_ATTRIB_GROUP_DESCS',
                                            I_group_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'L10N_FLEX_SETUP_SQL.DELETE_ATTRIB_GROUP_DESC',
                                             to_char(SQLCODE));
      return FALSE;

END DELETE_ATTRIB_GROUP_DESC;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_GROUP_DESCS(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist            IN OUT    VARCHAR2,
                            I_group_id         IN        L10N_ATTRIB_GROUP_DESCS.GROUP_ID%TYPE,
                            I_lang             IN        L10N_ATTRIB_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_GROUP_DESC_EXIST is
      select 'x'
        from l10n_attrib_group g
       where g.group_id = NVL(I_group_id, g.group_id)
         and not exists (select 'x'
                           from l10n_attrib_group_descs d
                          where d.group_id = g.group_id
                            and lang = NVL(I_lang, d.lang));

BEGIN
   ---
   open C_GROUP_DESC_EXIST;
   fetch C_GROUP_DESC_EXIST into L_exist;
   ---
   if C_GROUP_DESC_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_GROUP_DESC_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.CHK_NO_GROUP_DESCS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_GROUP_DESCS;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_attrib_id       IN     L10N_ATTRIB_DESCS.ATTRIB_ID%TYPE)                              
RETURN BOOLEAN IS
   
    L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
     select 'x'
        from l10n_attrib_descs 
       where attrib_id = I_attrib_id
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_LANGUAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
      
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_ATTRIB_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIB_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_RECGRP_EXISTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT VARCHAR2,
                                  I_rec_grp_id      IN     L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN IS
   
   L_exists VARCHAR2(1);

   cursor C_GET_REC_GRP is
      select 'x'
        from l10n_rec_group 
       where l10n_rec_group_id = I_rec_grp_id;
       
BEGIN
   ---
   open C_GET_REC_GRP;
   fetch C_GET_REC_GRP into L_exists;
   close C_GET_REC_GRP;
   ---
   if L_exists is NOT NULL then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_ATTRIB_RECGRP_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIB_RECGRP_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_CODETYPE_EXISTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists             IN OUT VARCHAR2,
                                    I_code_type_id       IN     L10N_CODE_HEAD.L10N_CODE_TYPE%TYPE)                              
RETURN BOOLEAN IS

   L_exists VARCHAR2(1);

   cursor C_GET_CODE_TYPE is
      select 'x'
        from l10n_code_head 
       where l10n_code_type = I_code_type_id;
       
BEGIN
   ---
   open C_GET_CODE_TYPE;
   fetch C_GET_CODE_TYPE into L_exists;
   close C_GET_CODE_TYPE;
   ---
   if L_exists is NOT NULL then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_ATTRIB_CODETYPE_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIB_CODETYPE_EXISTS;
-------------------------------------------------------------------------------------------------
FUNCTION CHK_GROUP_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_group_id     IN     L10N_ATTRIB_GROUP.GROUP_ID%TYPE)                              
RETURN BOOLEAN IS
   
    L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
     select 'x'
        from l10n_attrib_group_descs 
       where group_id = I_group_id
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_LANGUAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
      
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_GROUP_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_GROUP_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION ENTITY_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exist            IN OUT   VARCHAR2,
                      I_ext_entity       IN       EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_ENTITY_EXIST is
      select 'x'
        from ext_entity
       where ext_entity_id = I_ext_entity;

BEGIN
   ---
   open C_ENTITY_EXIST;
   fetch C_ENTITY_EXIST into L_exist;
   ---
   if C_ENTITY_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_ENTITY_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.ENTITY_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END ENTITY_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION GET_AVAIL_METADATA_COL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_attrib_storage_col   IN OUT   L10N_ATTRIB.ATTRIB_STORAGE_COL%TYPE,
                                I_group_id             IN       L10N_ATTRIB.GROUP_ID%TYPE,
                                I_data_type            IN       L10N_ATTRIB.DATA_TYPE%TYPE)
RETURN BOOLEAN IS

   L_col_no   NUMBER;

   cursor C_GET_ATTRIB_STORAGE_COL is
      select min(col_no) attrib_col
        from (select col col_no,
                     case
                        when col between 1 and 10 then
                          'VARCHAR2'
                        when col between 11 and 20 then
                          'NUMBER'
                        when col between 21 and 22 then
                          'DATE'
                     end data_type
                from (select col
                        from (select 1  col_1,  2  col_2,  3  col_3,  4  col_4,  5  col_5,
                                     6  col_6,  7  col_7,  8  col_8,  9  col_9,  10 col_10,
                                     11 col_11, 12 col_12, 13 col_13, 14 col_14, 15 col_15,
                                     16 col_16, 17 col_17, 18 col_18, 19 col_19, 20 col_20,
                                     21 col_21, 22 col_22
                                from dual) p
                      unpivot
                         (col for coli in (col_1  as 'col_1',  col_2  as 'col_2',  col_3  as 'col_3',  col_4  as 'col_4',  col_5  as 'col_5',
                                           col_6  as 'col_6',  col_7  as 'col_7',  col_8  as 'col_8',  col_9  as 'col_9',  col_10 as 'col_10',
                                           col_11 as 'col_11', col_12 as 'col_12', col_13 as 'col_13', col_14 as 'col_14', col_15 as 'col_15',
                                           col_16 as 'col_16', col_17 as 'col_17', col_18 as 'col_18', col_19 as 'col_19', col_20 as 'col_20',
                                           col_21 as 'col_21', col_22 as 'col_22')))) piv
               where piv.data_type = I_data_type
                 and not exists (select 'x'
                                   from l10n_attrib
                                  where group_id = I_group_id
                                    and data_type = I_data_type
                                    and to_number(substr(attrib_storage_col, instr(attrib_storage_col, '_') + 1)) = piv.col_no);

BEGIN
   ---
   open C_GET_ATTRIB_STORAGE_COL;
   fetch C_GET_ATTRIB_STORAGE_COL into L_col_no;
   close C_GET_ATTRIB_STORAGE_COL;
   ---
   if L_col_no is NOT NULL then
      O_attrib_storage_col := I_data_type||'_'||L_col_no;
   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_MD_COL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_AVAIL_METADATA_COL',
                                            to_char(SQLCODE));
      return FALSE;

END GET_AVAIL_METADATA_COL;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_EXT_TBL(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists              IN OUT    VARCHAR2,
                            O_base_rms_table      IN OUT    EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                            O_l10n_ext_table      IN OUT    EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                            I_ext_entity_id       IN        EXT_ENTITY.EXT_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_TABLES is
      select base_rms_table,
             l10n_ext_table
        from ext_entity ext
       where ext_entity_id = I_ext_entity_id;

BEGIN
   ---
   open C_GET_TABLES;
   fetch C_GET_TABLES into O_base_rms_table,
                           O_l10n_ext_table;
   close C_GET_TABLES;
   ---
   if O_base_rms_table is NULL then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;
   ---        
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_AVAIL_METADATA_COL',
                                            to_char(SQLCODE));
      return FALSE;

END GET_ENTITY_EXT_TBL;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_GROUP_INFO(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                               O_country_desc        IN OUT    COUNTRY.COUNTRY_DESC%TYPE,
                               O_base_rms_table      IN OUT    EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                               O_l10n_ext_table      IN OUT    EXT_ENTITY.L10N_EXT_TABLE%TYPE,
                               O_group_desc          IN OUT    L10N_ATTRIB_GROUP_DESCS.DESCRIPTION%TYPE,
                               IO_country_id         IN OUT    COUNTRY.COUNTRY_ID%TYPE,
                               IO_ext_entity_id      IN OUT    EXT_ENTITY.EXT_ENTITY_ID%TYPE,
                               I_group_id            IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_ENTITY_GRP_INFO is
      select ag.country_id,
             ext.ext_entity_id,
             ext.base_rms_table,
             ext.l10n_ext_table,
             gd.description
        from l10n_attrib_group ag,
             l10n_attrib_group_descs gd,
             ext_entity ext,
             country cn
       where ag.group_id = gd.group_id
         and ag.group_id = I_group_id
         and ag.ext_entity_id = ext.ext_entity_id
         and cn.country_id = ag.country_id
         and ag.country_id = NVL(IO_country_id, ag.country_id)
         and ag.ext_entity_id = NVL(IO_ext_entity_id, ag.ext_entity_id)
         and (gd.lang = GET_USER_LANG or
              gd.default_lang_ind  = 'Y'
              and not exists (select 'x'
                                from l10n_attrib_group_descs gd2
                               where gd.group_id = gd2.group_id
                                 and gd.lang = GET_USER_LANG));

BEGIN
   ---
   open C_GET_ENTITY_GRP_INFO;
   fetch C_GET_ENTITY_GRP_INFO into IO_country_id,
                                    IO_ext_entity_id,
                                    O_base_rms_table,
                                    O_l10n_ext_table,
                                    O_group_desc;
   ---
   if C_GET_ENTITY_GRP_INFO%NOTFOUND then
      close C_GET_ENTITY_GRP_INFO;
      ---
      if IO_country_id is NULL and IO_ext_entity_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ATTRIB_GROUP',
                                               NULL,
                                               NULL,
                                               NULL);
          return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP_ENTITY_CTRY',
                                               NULL,
                                               NULL,
                                               NULL);
          return FALSE;
      end if;
      ---
   end if;
   ---
   close C_GET_ENTITY_GRP_INFO;
   ---
   if IO_country_id is NOT NULL then
      if NOT COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                           IO_country_id,
                                           O_country_desc) then
         return FALSE;
         
      end if;
      
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_ATTRIB_GROUP_INFO',
                                            to_char(SQLCODE));
      return FALSE;

END GET_ATTRIB_GROUP_INFO;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ATTRIB_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_attrib_id       IN OUT   L10N_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN IS

   L_exists       VARCHAR2(1)  := 'N';
   L_attrib_id    L10N_ATTRIB.ATTRIB_ID%TYPE;
   L_wrap_seq_no  L10N_ATTRIB.ATTRIB_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select l10n_attrib_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from l10n_attrib
       where attrib_id = L_attrib_id;

BEGIN
   ---
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_attrib_id;
   close C_NEXTVAL;
   ---
   -- Save the first sequence value fetched.
   L_wrap_seq_no := L_attrib_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_attrib_id := L_attrib_id;
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_attrib_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_attrib_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('ATTRIB_ID_EXIST',
                                               L_attrib_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('ATTRIB_ID_EXIST',
                                            L_attrib_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_NEXT_ATTRIB_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_ATTRIB_ID;
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ATTRIB_DESCS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_id        IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                             I_lang            IN        L10N_ATTRIB_DESCS.LANG%TYPE,
                             I_default_ind     IN OUT    L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE)
RETURN BOOLEAN IS

BEGIN
   ---
   insert into l10n_attrib_descs (attrib_id,
                                  lang,
                                  description,
                                  default_lang_ind)
                           select att.attrib_id,
                                  I_lang,
                                  initcap(replace(att.view_col_name, '_', ' ')) description,
                                  I_default_ind
                            from l10n_attrib att
                           where att.group_id = I_group_id
                             and not exists (select 'x'
                                               from l10n_attrib_descs ad
                                              where ad.attrib_id = att.attrib_id
                                                and ad.lang = I_lang);
   ---
   --logm(SQL%ROWCOUNT);
   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.INSERT_ATTRIB_DESCS',
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_DESCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_attrib_id       IN       L10N_ATTRIB.ATTRIB_ID%TYPE)
RETURN BOOLEAN IS

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_ATTRIB_DESCS is
      select rowid
        from l10n_attrib_descs
       where attrib_id = I_attrib_id
         for update nowait;

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;

   TBL_rowid TYP_rowid;

BEGIN
   ---
   open C_LOCK_ATTRIB_DESCS;
   fetch C_LOCK_ATTRIB_DESCS BULK COLLECT into TBL_rowid;
   close C_LOCK_ATTRIB_DESCS;
   ---
   FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
      delete
        from l10n_attrib_descs
       where rowid = TBL_rowid(i);
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'L10N_ATTRIB_DESCS',
                                            I_attrib_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.DELETE_ATTRIB_DESCS',
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_ATTRIB_DESC_DEFAULT_LANG(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_default_ind     IN OUT    L10N_ATTRIB_DESCS.DEFAULT_LANG_IND%TYPE,
                                      I_group_id        IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                                      I_lang            IN        L10N_ATTRIB_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   cursor C_CHK_DEFAULT is
      select upper(ad.default_lang_ind) default_lang_ind
        from l10n_attrib_descs ad,
             l10n_attrib att
       where att.attrib_id = ad.attrib_id
         and att.group_id = I_group_id
         and ad.lang = I_lang;

BEGIN
   ---
   O_default_ind := 'N';
   ---
   open C_CHK_DEFAULT;
   fetch C_CHK_DEFAULT into O_default_ind;
   close C_CHK_DEFAULT;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.CHK_ATTRIB_DESC_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_ATTRIB_DESC_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_ATTRIB_DESCS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist           IN OUT    VARCHAR2,
                             I_group_id        IN        L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                             I_lang            IN        L10N_ATTRIB_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_CHK_DESCS_EXIST is
      select 'x'
        from l10n_attrib att
       where att.group_id = I_group_id
         and not exists (select 'x'
                           from l10n_attrib_descs ad
                          where ad.attrib_id = att.attrib_id
                            and ad.lang = NVL(I_lang, ad.lang));

BEGIN
   ---
   open C_CHK_DESCS_EXIST;
   fetch C_CHK_DESCS_EXIST into L_exist;
   ---
   if C_CHK_DESCS_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_CHK_DESCS_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.CHK_NO_ATTRIB_DESCS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_ATTRIB_DESCS;
---------------------------------------------------------------------------------------------------
FUNCTION ATTRIB_DISP_SEQ_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist           IN OUT   VARCHAR2,
                               I_group_id        IN       L10N_ATTRIB.GROUP_ID%TYPE,
                               I_display_seq     IN       L10N_ATTRIB.DISPLAY_SEQ%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_CHK_DISP_SEQ is
      select 'x'
        from l10n_attrib
       where group_id = I_group_id
         and display_seq = I_display_seq;

BEGIN
   ---
   open C_CHK_DISP_SEQ;
   fetch C_CHK_DISP_SEQ into L_exist;
   ---
   if C_CHK_DISP_SEQ%FOUND then
      O_exist := 'Y';
   else
      O_exist := 'N';
   end if;
   ---
   close C_CHK_DISP_SEQ;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.ATTRIB_DISP_SEQ_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END ATTRIB_DISP_SEQ_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION VIEW_COL_NAME_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist             IN OUT   VARCHAR2,
                             I_group_id          IN       L10N_ATTRIB.GROUP_ID%TYPE,
                             I_view_col_name     IN       L10N_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_VIEW_COL_EXIST is
      select 'x'
        from l10n_attrib att
       where att.group_id = I_group_id
         and att.view_col_name = I_view_col_name;

BEGIN

   ---
   open C_VIEW_COL_EXIST;
   fetch C_VIEW_COL_EXIST into L_exist;
   ---
   if C_VIEW_COL_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
   end if;
   ---
   close C_VIEW_COL_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.VIEW_COL_NAME_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END VIEW_COL_NAME_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DISPLAY_SEQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_display_seq     IN OUT   L10N_ATTRIB.DISPLAY_SEQ%TYPE,
                              I_group_id        IN       L10N_ATTRIB.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_DISPLAY_SEQ is
      select min(col)
        from (select col
                from (select 1  col_1,  2  col_2,  3  col_3,  4  col_4,  5  col_5,
                             6  col_6,  7  col_7,  8  col_8,  9  col_9,  10 col_10,
                             11 col_11, 12 col_12, 13 col_13, 14 col_14, 15 col_15,
                             16 col_16, 17 col_17, 18 col_18, 19 col_19, 20 col_20,
                             21 col_21, 22 col_22
                        from dual) p
              unpivot
                 (col for coli in (col_1  as 'col_1',  col_2  as 'col_2',  col_3  as 'col_3',  col_4  as 'col_4',  col_5  as 'col_5',
                                   col_6  as 'col_6',  col_7  as 'col_7',  col_8  as 'col_8',  col_9  as 'col_9',  col_10 as 'col_10',
                                   col_11 as 'col_11', col_12 as 'col_12', col_13 as 'col_13', col_14 as 'col_14', col_15 as 'col_15',
                                   col_16 as 'col_16', col_17 as 'col_17', col_18 as 'col_18', col_19 as 'col_19', col_20 as 'col_20',
                                   col_21 as 'col_21', col_22 as 'col_22'))) piv
       where not exists (select 'x'
                           from l10n_attrib
                          where group_id = I_group_id
                            and to_number(substr(attrib_storage_col, instr(attrib_storage_col, '_') + 1)) = piv.col);
                            
BEGIN

   open C_GET_DISPLAY_SEQ;
   fetch C_GET_DISPLAY_SEQ into O_display_seq;
   close C_GET_DISPLAY_SEQ;
   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GET_NEXT_DISPLAY_SEQ',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_DISPLAY_SEQ;
---------------------------------------------------------------------------------------------------
FUNCTION SET_FF_UI_PARAMETERS(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_ff_ui_param_rec    IN OUT   NOCOPY   L10N_FF_UI_PARAM_REC,
                              I_base_rms_table      IN                EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_select              VARCHAR2(2000) := 'select '''||I_base_rms_table||''', ';
   L_param_cnt           NUMBER := 20;
   L_cnt                 NUMBER := 1;

   TYPE CUR_key          IS REF CURSOR;
   C_GET_KEY_VALUES      CUR_key;

   cursor C_GET_KEYS is
      select key_col
        from ext_entity_key ek
       where base_rms_table = I_base_rms_table
       order by key_number;

   TYPE TYP_key_col is TABLE of C_GET_KEYS%ROWTYPE INDEX BY BINARY_INTEGER;
   TBL_key_col   TYP_key_col;

BEGIN
   ---
   open C_GET_KEYS;
   fetch C_GET_KEYS BULK COLLECT into TBL_key_col;
   close C_GET_KEYS;
   ---
   if TBL_key_col is NOT NULL and TBL_key_col.count > 0 then
      FOR i in TBL_key_col.FIRST..TBL_key_col.LAST LOOP
         L_select := L_select || TBL_key_col(L_cnt).key_col ||', ';
         L_cnt := L_cnt + 1;
      END LOOP;
      ---
      if L_cnt < L_param_cnt then
         FOR i in L_cnt..L_param_cnt LOOP
            L_select := L_select || ' NULL parameter_' || i || ', ';
         END LOOP;
      end if;
      ---
      L_select := substr(L_select, 1, length(L_select) -2) || ' from '|| I_base_rms_table ||
                  ' where rownum = 1';
      ---
      open  C_GET_KEY_VALUES for L_select;
      fetch C_GET_KEY_VALUES INTO IO_ff_ui_param_rec;
      close C_GET_KEY_VALUES;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.SET_FF_UI_PARAMETERS',
                                            to_char(SQLCODE));
      return FALSE;

END SET_FF_UI_PARAMETERS;
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CODE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_code_type     IN     L10N_CODE_HEAD.L10N_CODE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_exists   VARCHAR2(1)   := 'N';

   cursor C_CHECK_CODE is
      select 'Y'
        from l10n_code_head
       where l10n_code_type = I_code_type;

BEGIN
   ---
   open C_CHECK_CODE;
   fetch C_CHECK_CODE into L_exists;
   close C_CHECK_CODE;
   ---
   if L_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('REC_EXIST',
                                            I_code_type,
                                            NULL,
                                            NULL);
         return FALSE;

      end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.VALIDATE_CODE',
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CODE;
---------------------------------------------------------------------------------------
FUNCTION CODE_DELETE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_code_type     IN     L10N_CODE_HEAD.L10N_CODE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_exists VARCHAR2(1) := NULL;

   cursor C_GET_ATTRIB is
     select 'x'
       from l10n_code_head cdd
      where cdd.l10n_code_type = I_code_type
        and exists (select 'x'
                      from l10n_attrib att
                     where att.l10n_code_type =cdd.l10n_code_type);
       
   cursor C_DETAIL_EXISTS is
      select 'x'
        from l10n_code_detail_descs cdd
       where cdd.l10n_code_type = I_code_type
         for  update nowait;       

BEGIN
   ---
   open C_GET_ATTRIB;
   fetch C_GET_ATTRIB into L_exists;
   close C_GET_ATTRIB;
   ---
   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ATTRIB_REF_EXISTS',
                                            I_code_type,
                                            NULL,
                                            NULL);
      return FALSE;
      
   end if;
   ---
   open C_DETAIL_EXISTS;  
   close C_DETAIL_EXISTS;
   ---  
   delete
     from l10n_code_detail_descs
    where l10n_code_type = I_code_type;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CODE_DELETE',
                                            to_char(SQLCODE));
   return FALSE;

END CODE_DELETE;
---------------------------------------------------------------------------------------
FUNCTION CODE_LANG_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_code_type     IN     L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE,
                          I_code          IN     L10N_CODE_DETAIL_DESCS.L10N_CODE%TYPE,
                          I_code_lang     IN     L10N_CODE_DETAIL_DESCS.LANG%TYPE)
RETURN BOOLEAN IS
   
   L_exists VARCHAR2(1) :=NULL; 
     
   cursor C_LANG_EXISTS is
      select 'x'
        from l10n_code_detail_descs cdd
       where l10n_code_type = I_code_type
         and l10n_code      = I_code
         and lang           = I_code_lang;

BEGIN
   ---
   open C_LANG_EXISTS;
   fetch C_LANG_EXISTS into L_exists;
   close C_LANG_EXISTS;
   ---
   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CODE_LANG_EXISTS',
                                            I_code_type,
                                            NULL,
                                            NULL);
      return FALSE;
      
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CODE_LANG_EXISTS',
                                            to_char(SQLCODE));
   return FALSE;

END CODE_LANG_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION CODE_DISP_SEQ_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_type     IN     L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE,
                              I_code_seq     IN      L10N_CODE_DETAIL_DESCS.SEQ_NO%TYPE)
RETURN BOOLEAN IS
 
    L_exists VARCHAR2(1) :=NULL; 
     
   cursor C_DISP_SEQ is
      select 'x'
        from l10n_code_detail_descs cdd
       where l10n_code_type = I_code_type
         and seq_no         = I_code_seq;
         
BEGIN
   ---
   open C_DISP_SEQ;
   fetch C_DISP_SEQ into L_exists;
   close C_DISP_SEQ;
   ---
   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENT_SEQ_NUMB',
                                            I_code_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CODE_DISP_SEQ_EXISTS',
                                            to_char(SQLCODE));
   return FALSE;

END CODE_DISP_SEQ_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_CODE_DEFAULT_LANG(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_type     IN     L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE)                              
RETURN BOOLEAN IS
   
   L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
     select 'x'
        from l10n_code_detail_descs cdd
       where l10n_code_type = I_code_type
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_LANGUAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_CODE_DEFAULT_LANG',
                                            to_char(SQLCODE));
   return FALSE;

END CHK_CODE_DEFAULT_LANG;
---------------------------------------------------------------------------------------
FUNCTION CHK_NO_CODE_DESCS(O_error_message    IN OUT 	RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT	VARCHAR2,
                           I_code_type        IN    	L10N_CODE_DETAIL_DESCS.L10N_CODE_TYPE%TYPE,
                           I_lang             IN      L10N_CODE_DETAIL_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_CODE_DESCS_EXIST is
      select 'x'
        from l10n_code_head chd
       where chd.l10n_code_type = NVL(I_code_type,chd.l10n_code_type)
         and not exists (select 'x'
                           from l10n_code_detail_descs cdd
                          where cdd.l10n_code_type = chd.l10n_code_type
                            and cdd.lang = NVL(I_lang, cdd.lang));

BEGIN
   ---
   open C_CODE_DESCS_EXIST;
   fetch C_CODE_DESCS_EXIST into L_exist;
   ---
   if C_CODE_DESCS_EXIST%NOTFOUND then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;
   ---
   close C_CODE_DESCS_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.CHK_NO_CODE_DESCS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_CODE_DESCS;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_NEXT_RECGRP_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_rec_grp_id    IN OUT L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_exists            VARCHAR2(1)  := 'N';
   L_wrap_recgrp_id    L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE;
   L_recgrp_id         L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select l10n_rec_group_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from l10n_rec_group rg
       where rg.l10n_rec_group_id = L_recgrp_id;

BEGIN

   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_recgrp_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_recgrp_id := L_recgrp_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_rec_grp_id := L_recgrp_id;
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_recgrp_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_recgrp_id = L_wrap_recgrp_id then
         O_error_message := SQL_LIB.CREATE_MSG('RECGRP_ID_EXIST',
                                               L_recgrp_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECGRP_ID_EXIST',
                                            L_recgrp_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.GEN_NEXT_RECGRP_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GEN_NEXT_RECGRP_ID;
---------------------------------------------------------------------------------------
FUNCTION REC_GROUP_DELETE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_rec_grp_id    IN     L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_rec_grp_exists VARCHAR2(1);
   L_attrib_exists  VARCHAR2(1);

   cursor C_GRP_EXISTS is
      select 'x'
        from l10n_rec_group_descs
       where l10n_rec_group_id = I_rec_grp_id
         for update nowait;

   cursor C_ATTRIB_EXISTS is
      select 'x'
        from l10n_attrib
       where l10n_rec_group_id = I_rec_grp_id
         for update nowait;

BEGIN
   ---
   open C_ATTRIB_EXISTS;
   fetch C_ATTRIB_EXISTS into L_attrib_exists;
   close C_ATTRIB_EXISTS;
   ---
   if L_attrib_exists is NOT NULL then
      o_error_message := SQL_LIB.CREATE_MSG('ASSOC_DTLS_DELETE',
                                            I_rec_grp_id,
                                            NULL,
                                            NULL);
         return FALSE;

   else
       open C_GRP_EXISTS;
       fetch C_GRP_EXISTS into L_rec_grp_exists;
       close C_GRP_EXISTS;
       ---
       if L_rec_grp_exists is NOT NULL then
          delete
            from  l10n_rec_group_descs
           where l10n_rec_group_id = I_rec_grp_id;
       end if;
       ---
       return TRUE;
       ---
   end if;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.REC_GROUP_DELETE',
                                            to_char(SQLCODE));

      return FALSE;

END REC_GROUP_DELETE;
---------------------------------------------------------------------------------------
FUNCTION REC_GROUP_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rec_grp_id      IN     L10N_REC_GROUP_DESCS.L10N_REC_GROUP_ID%TYPE,
                        I_lang            IN     L10N_REC_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

   L_lang_exists  VARCHAR2(1);

   cursor C_LANG_EXISTS is
      select 'x'
        from l10n_rec_group_descs
       where l10n_rec_group_id = I_rec_grp_id
         and lang = I_lang;

BEGIN
   ---
   open C_LANG_EXISTS;
   fetch C_LANG_EXISTS into L_lang_exists;
   close C_LANG_EXISTS;
   ---
   if L_lang_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_RECGRP_LANG',
                                            I_lang,
                                            NULL,
                                            NULL);
         return FALSE;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.REC_GROUP_LANG',
                                            to_char(SQLCODE));

      return FALSE;

END REC_GROUP_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_RG_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_l10n_rg_id     IN     L10N_REC_GROUP_DESCS.L10N_REC_GROUP_ID%TYPE)                              
RETURN BOOLEAN IS
   
    L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
     select 'x'
        from l10n_rec_group_descs rgd
       where l10n_rec_group_id = I_l10n_rg_id
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_LANGUAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLEX_SQL.CHK_RG_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_RG_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_RECGRP_DESCS(O_error_message    IN OUT 	RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists           IN OUT	VARCHAR2,
                             I_recgrp_id        IN    	L10N_REC_GROUP_DESCS.L10N_REC_GROUP_ID%TYPE,
                             I_lang             IN      L10N_REC_GROUP_DESCS.LANG%TYPE)
RETURN BOOLEAN IS

    L_exist   VARCHAR2(1);

   cursor C_RECGRP_DESC_EXIST is
      select 'x'
        from l10n_rec_group g
       where g.l10n_rec_group_id = NVL(I_recgrp_id, g.l10n_rec_group_id)
         and not exists (select 'x'
                           from l10n_rec_group_descs d
                          where d.l10n_rec_group_id = g.l10n_rec_group_id
                            and lang = NVL(I_lang, d.lang));

BEGIN
   ---
   open C_RECGRP_DESC_EXIST;
   fetch C_RECGRP_DESC_EXIST into L_exist;
   ---
   if C_RECGRP_DESC_EXIST%NOTFOUND then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;
   ---
   close C_RECGRP_DESC_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L10N_FLEX_SETUP_SQL.CHK_NO_RECGRP_DESCS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_RECGRP_DESCS;
---------------------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_COMMENTS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                                   I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'L10N_FLEX_SETUP_SQL.GEN_STG_L10N_EXT_COMMENTS';

   L_ext_table          VARCHAR2(30) := NULL;
   L_stg_table          VARCHAR2(30) := NULL;
   L_tab_comment_string VARCHAR2(4000) := NULL;
   L_col_comment_string VARCHAR2(4000) := NULL;

   cursor C_EXT_TABLE is
      select l10n_ext_table
        from ext_entity
       where base_rms_table = I_base_table;

   cursor C_ENTITY_KEY is
      select key_col
        from ext_entity_key
       where base_rms_table = I_base_table
    order by key_number;

   cursor C_ATTRIB_COLS is
      select a.view_col_name column_name,
             d.description -- for column comment part of ddl
        from l10n_attrib a,
             l10n_attrib_descs d,
             l10n_attrib_group g,
             ext_entity e
       where a.attrib_id = d.attrib_id
         and d.default_lang_ind = 'Y'
         and a.group_id = g.group_id
         and g.country_id = I_country_id      
         and e.ext_entity_id = g.ext_entity_id
         and e.base_rms_table = I_base_table
    order by g.group_id, a.attrib_id;    
   
BEGIN

   open C_EXT_TABLE;
   fetch C_EXT_TABLE into L_ext_table;
   close C_EXT_TABLE;
   L_stg_table := 'STG_' || L_ext_table || '_' || I_country_id;  -- staging table name, eg. STG_STORE_L10N_EXT_BR

   L_col_comment_string := 'COMMENT ON COLUMN '||L_stg_table||'.PROCESS_ID IS '||''''||'Process id for segregating the data on the staging table.'||''''||';';
   dbms_output.put_line(L_col_comment_string);

   for rec in C_ENTITY_KEY loop
      L_col_comment_string := 'COMMENT ON COLUMN '||L_stg_table||'.'||rec.key_col||' IS '''||rec.key_col||''''||';';
      dbms_output.put_line(L_col_comment_string);
   end loop;

   for rec in C_ATTRIB_COLS loop
      L_col_comment_string := 'COMMENT ON COLUMN '||L_stg_table||'.'||rec.column_name||' IS '''||rec.description||''''||';';
      dbms_output.put_line(L_col_comment_string);
   end loop;

   L_tab_comment_string := 'COMMENT ON TABLE '||L_stg_table||' IS '||''''||'Staging table for Brazil localization extension.'||''''||';';
   dbms_output.put_line(L_tab_comment_string);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GEN_STG_L10N_EXT_COMMENTS;   
---------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                              I_base_table        IN      EXT_ENTITY.BASE_RMS_TABLE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'L10N_FLEX_SETUP_SQL.GEN_STG_L10N_EXT_TBL';

   L_table_owner        SYSTEM_OPTIONS.TABLE_OWNER%TYPE := NULL;

   L_ext_table           VARCHAR2(30) := NULL;
   L_stg_table           VARCHAR2(30) := NULL;
   L_drop_string         VARCHAR2(200) := NULL;
   L_create_string       VARCHAR2(4000):= NULL;
   L_tab_storage_string  VARCHAR2(500) := NULL;
   L_constraint_string   VARCHAR2(500) := NULL;
   L_pk_storage_string   VARCHAR2(500) := NULL;

   L_attrib_col          VARCHAR2(4000) := NULL;
   L_prim_col            VARCHAR2(4000) := NULL;
   L_prim_cnstr          VARCHAR2(4000) := NULL;
   L_nullable_key_exists BOOLEAN := FALSE;

   cursor C_EXT_TABLE is
      select l10n_ext_table
        from ext_entity
       where base_rms_table = I_base_table;

   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;

   --entity key columns can be PK or UK (nullable or not null columns)
   cursor C_ENTITY_KEY is
      select k.key_col column_name,
             case
                when a.data_type = 'VARCHAR2' then
                   a.data_type||'('||a.data_length||')'
                when a.data_type = 'NUMBER' then
                   decode(a.data_scale, 0, a.data_type||'('||a.data_precision||')',
                                        a.data_type||'('||a.data_precision||','||a.data_scale||')')
                when a.data_type = 'DATE' then
                   a.data_type
                else
                   a.data_type
             end column_type,
             decode(a.nullable, 'Y', NULL, 
                                'N', 'NOT NULL', NULL) nullable
        from ext_entity_key k,
             all_tab_columns a
       where k.base_rms_table = I_base_table
         and a.table_name = I_base_table
         and k.key_col = a.column_name
         and a.owner = L_table_owner
    order by k.key_number;

   cursor C_ATTRIB_COLS is
      select a.view_col_name,
             decode(a.data_type, 'VARCHAR2', a.data_type||'('||a.maximum_length||')',
                                 'NUMBER', a.data_type||'('||a.maximum_length||')',
                                 'DATE', a.data_type,
                                 a.data_type) column_type
        from l10n_attrib a,
             l10n_attrib_group g,
             ext_entity e
       where e.ext_entity_id = g.ext_entity_id
         and e.base_rms_table = I_base_table
         and a.group_id = g.group_id
         and g.country_id = I_country_id
    order by g.group_id, a.attrib_id;    

BEGIN
   ---
   open C_EXT_TABLE;
   fetch C_EXT_TABLE into L_ext_table;
   close C_EXT_TABLE;

   L_stg_table := 'STG_' || L_ext_table || '_' || I_country_id;  -- staging table name, eg. STG_STORE_L10N_EXT_BR

   L_drop_string := '--DROP TABLE '||L_stg_table||';';
   dbms_output.put_line(L_drop_string);

   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_table_owner;
   close C_GET_TAB_OWNER;

   L_create_string := 'CREATE TABLE '||L_stg_table || ' (PROCESS_ID NUMBER(10) NOT NULL';
   for rec in C_ENTITY_KEY loop
      if rec.nullable is NULL then
         L_nullable_key_exists := TRUE;
      end if;
      L_prim_col := L_prim_col||', '||rec.column_name||' '||rec.column_type||' '||rec.nullable;
      L_prim_cnstr := L_prim_cnstr||', '||rec.column_name;
   end loop;
   L_create_string := L_create_string || L_prim_col;

   for rec in C_ATTRIB_COLS loop
      L_attrib_col := L_attrib_col||', '||rec.view_col_name||' '||rec.column_type;
   end loop;
   L_create_string := L_create_string || L_attrib_col || ')';
   dbms_output.put_line(L_create_string);   

   L_tab_storage_string := 'INITRANS 6 STORAGE (FREELISTS 6) TABLESPACE RETEK_DATA; ';
   dbms_output.put_line(L_tab_storage_string);   

   if L_nullable_key_exists = TRUE then
      -- create unique key clause if any of the entity key is nullable. No storage clause.
      L_constraint_string := 'ALTER TABLE '||L_stg_table||
                             ' ADD (CONSTRAINT UK_'||L_ext_table||'_'||I_country_id||' UNIQUE (PROCESS_ID'||L_prim_cnstr||'));';
      dbms_output.put_line(L_constraint_string);
   else
      -- create primary key clause if no entity key is nullable. Add storage clause for the PK.
      L_constraint_string := 'ALTER TABLE '||L_stg_table||
                             ' ADD (CONSTRAINT PK_'||L_ext_table||'_'||I_country_id||' PRIMARY KEY (PROCESS_ID'||L_prim_cnstr||') ';
      L_pk_storage_string := 'USING INDEX INITRANS 12 STORAGE (FREELISTS 6) TABLESPACE RETEK_INDEX); ';
      dbms_output.put_line(L_constraint_string);
      dbms_output.put_line(L_pk_storage_string);   
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GEN_STG_L10N_EXT_TBL;
---------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_TBL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'L10N_FLEX_SETUP_SQL.GEN_STG_L10N_EXT_TBL';
   L_base_rms_tables    TABLE_TBL;

   cursor C_BASE_RMS_TABLE is
      select base_rms_table
        from ext_entity e
       where exists (select 'x'
                       from l10n_attrib_group g
                      where g.ext_entity_id = e.ext_entity_id
                        and g.country_id = I_country_id
                        and rownum = 1);
BEGIN
   open C_BASE_RMS_TABLE;
   fetch C_BASE_RMS_TABLE bulk collect into L_base_rms_tables;
   close C_BASE_RMS_TABLE;

   -- create staging table for ALL entities localized for the country
   for i in L_base_rms_tables.FIRST .. L_base_rms_tables.LAST loop
      if GEN_STG_L10N_EXT_TBL(O_error_message,
                              I_country_id,
                              L_base_rms_tables(i)) = FALSE then
         return FALSE;
      end if;
      ---
      if GEN_STG_L10N_EXT_COMMENTS(O_error_message,
                                   I_country_id,
                                   L_base_rms_tables(i)) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GEN_STG_L10N_EXT_TBL;   
---------------------------------------------------------------------------------------------------
FUNCTION GEN_STG_L10N_EXT_SEQ(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_country_id        IN      L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'L10N_FLEX_SETUP_SQL.GEN_STG_L10N_EXT_SEQ';

   L_l10n_ext_tables    TABLE_TBL;
   L_drop_string        VARCHAR2(4000) := NULL;
   L_create_string      VARCHAR2(4000) := NULL;

   cursor C_L10N_EXT_TABLE is
      select l10n_ext_table
        from ext_entity e
       where exists (select 'x'
                       from l10n_attrib_group g
                      where g.ext_entity_id = e.ext_entity_id
                        and g.country_id = I_country_id
                        and rownum = 1);

BEGIN
   open C_L10N_EXT_TABLE;
   fetch C_L10N_EXT_TABLE bulk collect into L_l10n_ext_tables;
   close C_L10N_EXT_TABLE;

   -- create staging table for ALL entities localized for the country
   for i in L_l10n_ext_tables.FIRST .. L_l10n_ext_tables.LAST loop

      L_drop_string := '--DROP SEQUENCE '||L_l10n_ext_tables(i)|| '_SEQ ';
      dbms_output.put_line(L_drop_string);   

      L_create_string := 'CREATE SEQUENCE '||L_l10n_ext_tables(i)|| '_SEQ ';
      L_create_string := L_create_string || 'MINVALUE 1 MAXVALUE 9999999999 ';
      L_create_string := L_create_string || 'INCREMENT BY 1 START WITH 1 ';
      L_create_string := L_create_string || 'CACHE 20 ORDER  NOCYCLE ';
      dbms_output.put_line(L_create_string);   
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GEN_STG_L10N_EXT_SEQ;   
---------------------------------------------------------------------------------------------------
FUNCTION GET_REC_GROUP_QUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                             I_l10n_rec_group_id   IN     L10N_REC_GROUP.L10N_REC_GROUP_ID%TYPE,
                             I_country_id          IN     L10N_REC_GROUP.COUNTRY_ID%TYPE, 
                             O_query               OUT    L10N_REC_GROUP.QUERY%TYPE)
RETURN BOOLEAN IS
 
   L_program  VARCHAR2(62) := 'L10N_FLEX_SETUP_SQL.GET_REC_GROUP_QUERY';
    
   cursor C_get_query is
      select query
         from l10n_rec_group
        where country_id  = I_country_id
          and l10n_rec_group_id = I_l10n_rec_group_id;
    
BEGIN
   open C_get_query;
   fetch C_get_query into O_query;
   close C_get_query;
   return TRUE;
    
EXCEPTION
 when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    return FALSE;  
END  GET_REC_GROUP_QUERY ; 
------------------------------------------------------------------------------------------------
FUNCTION GET_WIDGET(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_widget              IN OUT L10N_ATTRIB.UI_WIDGET%TYPE,
                    O_rec_group_id        IN OUT L10N_ATTRIB.L10N_REC_GROUP_ID%TYPE,
		        O_code_type           IN OUT L10N_ATTRIB.L10N_CODE_TYPE%TYPE,
                    O_data_type           IN OUT L10N_ATTRIB.DATA_TYPE%TYPE,
                    I_name                IN     L10N_ATTRIB.VIEW_COL_NAME%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(62) := 'L10N_FLEX_SETUP_SQL.GET_WIDGET';
  
   cursor C_get_uiwidget is
      select attb.ui_widget,
             attb.l10n_rec_group_id,
             attb.l10n_code_type,
             attb.data_type
        from l10n_attrib attb,
             l10n_attrib_group grp,
             ext_entity enty
      where  attb.group_id = grp.group_id
        and  enty.base_rms_table = 'ITEM_COUNTRY'
        and  enty.ext_entity_id = grp.ext_entity_id 
        and upper(attb.view_col_name) = upper(I_name);
   
BEGIN
  open C_get_uiwidget;
  fetch C_get_uiwidget into O_widget,O_rec_group_id,O_code_type,O_data_type;
  close C_get_uiwidget;
  return true;
EXCEPTION
 when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;  
END GET_WIDGET;
-------------------------------------------------------------------------------------------------------
FUNCTION CHK_CUSTOM_ATTRIB(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            IN OUT BOOLEAN,
                           I_base_table        IN     EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                           I_country_id        IN     L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE,
                           I_view_col_name     IN     L10N_ATTRIB.VIEW_COL_NAME%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'L10N_FLEX_SETUP_SQL.CHK_CUSTOM_ATTRIB';
   L_dummy   VARCHAR2(1)  := NULL;


   cursor C_EXISTS is
      select 'x'
        from l10n_attrib att,
             l10n_attrib_group atg ,
             ext_entity ext
       where ext.base_rms_table = I_base_table
         and atg.ext_entity_id = ext.ext_entity_id
         and atg.country_id = I_country_id
         and att.group_id = atg.group_id
         and atg.base_ind = 'N'
         and att.view_col_name = NVL(I_view_col_name,att.view_col_name)
         and rownum = 1;

BEGIN
   O_exists := FALSE;

   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_country_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_base_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_base_table,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHK_CUSTOM_ATTRIB;
--------------------------------------------------------------------------------------------------
END L10N_FLEX_SETUP_SQL;
/
 