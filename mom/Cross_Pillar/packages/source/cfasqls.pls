CREATE OR REPLACE PACKAGE CFA_SQL AUTHID CURRENT_USER AS

GP_base_table              CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE;
GP_ext_table               CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE; -- define for custom table


-- Data type constants
CHAR_TYPE                  CONSTANT CFA_ATTRIB.DATA_TYPE%TYPE := 'VARCHAR2';
NUM_TYPE                   CONSTANT CFA_ATTRIB.DATA_TYPE%TYPE := 'NUMBER';
DATE_TYPE                  CONSTANT CFA_ATTRIB.DATA_TYPE%TYPE := 'DATE';

-- Widget type constants
TI_WDGT                    CONSTANT CFA_ATTRIB.UI_WIDGET%TYPE := 'TI';
RG_WDGT                    CONSTANT CFA_ATTRIB.UI_WIDGET%TYPE := 'RG';
LI_WDGT                    CONSTANT CFA_ATTRIB.UI_WIDGET%TYPE := 'LI';
CB_WDGT                    CONSTANT CFA_ATTRIB.UI_WIDGET%TYPE := 'CB';
DT_WDGT                    CONSTANT CFA_ATTRIB.UI_WIDGET%TYPE := 'DT';
---
PB_WDGT                    CONSTANT CFA_ATTRIB.UI_WIDGET%TYPE := 'PB';
DESC_WDGT                  CONSTANT VARCHAR2(10) := 'TI_DESC';
KEY_WDGT                   CONSTANT VARCHAR2(5)  := 'KEY';

-- Function type constants
QUALIFIER_FUNC             CONSTANT VARCHAR2(1) := 'Q';
VALIDATION_FUNC            CONSTANT VARCHAR2(1) := 'V';
DEFAULT_FUNC               CONSTANT VARCHAR2(1) := 'D';

-- Field type constants
CFA_KEY                    CONSTANT  VARCHAR2(5) := 'KEY';
CFA_BASE                   CONSTANT  VARCHAR2(5) := 'BASE';
CFA_EXT                    CONSTANT  VARCHAR2(5) := 'EXT';


---
GP_date_format             VARCHAR2(20);
GP_data_type               CFA_ATTRIB.DATA_TYPE%TYPE;
GP_widget_type             VARCHAR2(10);

GP_label                   CFA_ATTRIB_LABELS.LABEL%TYPE;
GP_query                   CFA_REC_GROUP.QUERY%TYPE;

GP_pos                     NUMBER;
GP_dim                     NUMBER;
GP_limit                   NUMBER;
GP_index                   VARCHAR2(60);

GP_field_type              VARCHAR2(15);
GP_field_item              VARCHAR2(60);
GP_field_value             VARCHAR2(250);
GP_field_value_qry         VARCHAR2(260);
GP_field_desc_value        VARCHAR2(255);
GP_field_no                NUMBER;


-- Header Variables
GP_group_set               VARCHAR2(10);
GP_group_id                CFA_ATTRIB_GROUP.GROUP_ID%TYPE;

-- Attribute Variables
GP_ext_varchar             VARCHAR2(250);
GP_ext_number              NUMBER;
GP_ext_date                DATE;
GP_rec_group_id            NUMBER; --to be changed



--------------------------------------------------------------------------------
-- Data Collections
--------------------------------------------------------------------------------
TYPE TYP_group_rec is RECORD
(
   group_id                CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
   group_desc              CFA_ATTRIB_GROUP_LABELS.LABEL%TYPE
);
TYPE TYP_group_tbl is TABLE of TYP_group_rec INDEX by BINARY_INTEGER;

TYPE TYP_attrib_rec IS RECORD
(
   group_id                CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
   varchar2_1              GP_EXT_VARCHAR%TYPE,
   varchar2_2              GP_EXT_VARCHAR%TYPE,
   varchar2_3              GP_EXT_VARCHAR%TYPE,
   varchar2_4              GP_EXT_VARCHAR%TYPE,
   varchar2_5              GP_EXT_VARCHAR%TYPE,
   varchar2_6              GP_EXT_VARCHAR%TYPE,
   varchar2_7              GP_EXT_VARCHAR%TYPE,
   varchar2_8              GP_EXT_VARCHAR%TYPE,
   varchar2_9              GP_EXT_VARCHAR%TYPE,
   varchar2_10             GP_EXT_VARCHAR%TYPE,
   number_11               GP_EXT_NUMBER%TYPE,
   number_12               GP_EXT_NUMBER%TYPE,
   number_13               GP_EXT_NUMBER%TYPE,
   number_14               GP_EXT_NUMBER%TYPE,
   number_15               GP_EXT_NUMBER%TYPE,
   number_16               GP_EXT_NUMBER%TYPE,
   number_17               GP_EXT_NUMBER%TYPE,
   number_18               GP_EXT_NUMBER%TYPE,
   number_19               GP_EXT_NUMBER%TYPE,
   number_20               GP_EXT_NUMBER%TYPE,
   date_21                 GP_EXT_DATE%TYPE,
   date_22                 GP_EXT_DATE%TYPE,
   date_23                 GP_EXT_DATE%TYPE,
   date_24                 GP_EXT_DATE%TYPE,
   date_25                 GP_EXT_DATE%TYPE
);
TYPE TYP_attrib_tbl is TABLE of TYP_attrib_rec INDEX BY BINARY_INTEGER;

------------------------------------------------------------------------------------------
TYPE TYP_store_map_rec is RECORD
(
 st_group_id     CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
 stadd_group_id  CFA_ATTRIB_GROUP.GROUP_ID%TYPE
);
------------------------------------------------------------------------------------------

TYPE TYP_store_map_tbl is TABLE of TYP_store_map_rec  INDEX BY BINARY_INTEGER; 
------------------------------------------------------------------------------------------ 
LP_TYP_store_map_tbl  TYP_store_map_tbl;

--------------------------------------------------------------------------------
-- UI Configuration Collections
--------------------------------------------------------------------------------
TYPE TYP_group_set_rec is RECORD
(
   group_set_id	           CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
   ext_entity_id	         CFA_ATTRIB_GROUP_SET.EXT_ENTITY_ID%TYPE,
   display_seq	           CFA_ATTRIB_GROUP_SET.DISPLAY_SEQ%TYPE,
   active_ind	             CFA_ATTRIB_GROUP_SET.ACTIVE_IND%TYPE,
   label                   CFA_ATTRIB_GROUP_SET_LABELS.LABEL%TYPE
);
TYPE TBL_group_set_tbl is TABLE of TYP_group_set_rec INDEX BY BINARY_INTEGER;

TYPE TYP_header_cfg_rec is RECORD
(
   header_type             GP_FIELD_TYPE%TYPE,
   key_col                 CFA_EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_no                  CFA_EXT_ENTITY_KEY.KEY_NUMBER%TYPE,
   data_type               CFA_EXT_ENTITY_KEY.DATA_TYPE%TYPE,
   label                   CFA_EXT_ENTITY_KEY_LABELS.LABEL%TYPE,
   desc_code               CFA_EXT_ENTITY_KEY.DESCRIPTION_CODE%TYPE,
   key_desc_value          GP_FIELD_DESC_VALUE%TYPE, -- key description field value
   ui_xpos                 GP_POS%TYPE,
   ui_ypos                 GP_POS%TYPE
);
TYPE TYP_header_cfg_tbl is TABLE of TYP_header_cfg_rec INDEX BY GP_INDEX%TYPE;

TYPE TYP_attrib_cfg_rec is RECORD
(
   attrib_id               CFA_ATTRIB.ATTRIB_ID%TYPE,
   group_id                CFA_ATTRIB.GROUP_ID%TYPE,
   view_col                CFA_ATTRIB.VIEW_COL_NAME%TYPE,
   storage_col             CFA_ATTRIB.STORAGE_COL_NAME%TYPE,
   data_type               CFA_ATTRIB.DATA_TYPE%TYPE,
   ui_widget               CFA_ATTRIB.UI_WIDGET%TYPE,
   display_seq             CFA_ATTRIB.DISPLAY_SEQ%TYPE,
   label                   CFA_ATTRIB_LABELS.LABEL%TYPE,
   value_req               CFA_ATTRIB.VALUE_REQ%TYPE,
   enable_ind              CFA_ATTRIB.ENABLE_IND%TYPE,
   editor_req              CFA_ATTRIB.EDITOR_REQ%TYPE,
   maximum_length          CFA_ATTRIB.MAXIMUM_LENGTH%TYPE,
   lowest_allowed_value    CFA_ATTRIB.LOWEST_ALLOWED_VALUE%TYPE,
   highest_allowed_value   CFA_ATTRIB.HIGHEST_ALLOWED_VALUE%TYPE,
   validation_func         CFA_ATTRIB.VALIDATION_FUNC%TYPE,
   rec_group_id            CFA_ATTRIB.REC_GROUP_ID%TYPE,
   code_type               CFA_ATTRIB.CODE_TYPE%TYPE,
   attrib_no               GP_FIELD_NO%TYPE,
   ---
   ui_xpos                 GP_POS%TYPE,
   ui_ypos                 GP_POS%TYPE,
   ---
   next_nav_item           GP_FIELD_ITEM%TYPE,
   prev_nav_item           GP_FIELD_ITEM%TYPE

);
TYPE TYP_attrib_cfg_tbl is TABLE of TYP_attrib_cfg_rec INDEX BY GP_INDEX%TYPE;

TYPE TYP_value_rec is RECORD
(
    field_type             GP_FIELD_TYPE%TYPE,
    field_name             GP_FIELD_ITEM%TYPE,
    data_type              GP_DATA_TYPE%TYPE,
    storage_col            GP_FIELD_ITEM%TYPE,
    field_no               GP_FIELD_NO%TYPE,
    field_value            GP_FIELD_VALUE%TYPE,
    field_value_qry        GP_FIELD_VALUE_QRY%TYPE,
    field_desc_value       GP_FIELD_DESC_VALUE%TYPE
);
TYPE TYP_value_tbl is TABLE of TYP_value_rec INDEX BY CFA_SQL.GP_INDEX%TYPE;


TYPE TYP_ui_var_rec is RECORD
(
   -- Size
   header_field_wd         GP_DIM%TYPE,
   header_field_ht         GP_DIM%TYPE,
   header_field_desc_wd    GP_DIM%TYPE,
      --
   attrib_field_wd         GP_DIM%TYPE,
   attrib_field_ht         GP_DIM%TYPE,
   attrib_field_desc_wd    GP_DIM%TYPE,
   attrib_field_pb_wd      GP_DIM%TYPE,
   --
   min_attrib_canvas_wd    GP_DIM%TYPE,
   min_attrib_canvas_ht    GP_DIM%TYPE,

   -- Spacing
   field_to_field_ysp      GP_DIM%TYPE,
   field_to_field_xsp      GP_DIM%TYPE,
   canvas_edge_to_field    GP_DIM%TYPE,
   attrib_offset_ysp       GP_DIM%TYPE,
   header_offset_xsp       GP_DIM%TYPE,
   main_to_stack           GP_DIM%TYPE,
   stack_to_stack          GP_DIM%TYPE,
   ---
   max_header_row_wd       GP_DIM%TYPE,
   header_row_cnt          GP_DIM%TYPE,
   max_header_label        GP_DIM%TYPE,
   max_attrib_row_wd       GP_DIM%TYPE,
   attrib_set_cnt          GP_DIM%TYPE,
   ---
   first_field_init_xpos   GP_POS%TYPE,
   first_field_init_ypos   GP_POS%TYPE,
   ---
   lang                    CFA_ATTRIB_LABELS.LANG%TYPE,

   -- Types
   char_data_type          CFA_ATTRIB.DATA_TYPE%TYPE,
   num_data_type           CFA_ATTRIB.DATA_TYPE%TYPE,
   date_data_type          CFA_ATTRIB.DATA_TYPE%TYPE,
   ---
   text_item_widget        CFA_ATTRIB.UI_WIDGET%TYPE,
   rec_group_widget        CFA_ATTRIB.UI_WIDGET%TYPE,
   list_item_widget        CFA_ATTRIB.UI_WIDGET%TYPE,
   check_box_widget        CFA_ATTRIB.UI_WIDGET%TYPE,
   date_widget             CFA_ATTRIB.UI_WIDGET%TYPE,
   ---
   pb_widget               CFA_ATTRIB.UI_WIDGET%TYPE,
   desc_widget             GP_WIDGET_TYPE%TYPE,
   key_widget              GP_WIDGET_TYPE%TYPE

);

TYPE TYP_return_fields_rec is RECORD
(
   field_name    CFA_SQL.GP_FIELD_ITEM%TYPE
);

TYPE TYP_return_fields_tbl is TABLE of TYP_return_fields_rec INDEX by BINARY_INTEGER;


GP_header_cfg_tbl          TYP_header_cfg_tbl;
GP_attrib_cfg_tbl          TYP_attrib_cfg_tbl;
GP_key_value_tbl           TYP_value_tbl;
GP_attrib_value_tbl        TYP_value_tbl; 
GP_return_tbl              TYP_return_fields_tbl;

--------------------------------------------------------------------------------
-- Flex UI Data Block Procedures
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Name   : QUERY_GROUP
-- Purpose: Used to query the groups associated to the selected entity group set
--          Used in the CFAS UI procedure based block
--------------------------------------------------------------------------------
PROCEDURE QUERY_GROUP(
   IO_group_tbl     IN OUT  NOCOPY  TYP_GROUP_TBL,
   I_base_table     IN              GP_BASE_TABLE%TYPE,
   I_group_set      IN              GP_GROUP_SET%TYPE);  
--------------------------------------------------------------------------------
-- Name   : QUERY_ATTRIB
-- Purpose: Used to query the attributes associated to the selected group. The 
--          data source comes from the extension table of the entity. Used in the
--          CFAS UI procedure based block
--------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB(
   IO_attrib_tbl    IN OUT  NOCOPY  TYP_ATTRIB_TBL,
   I_group_id       IN              CFA_ATTRIB_GROUP.GROUP_ID%TYPE);
--------------------------------------------------------------------------------
-- Name   : MERGE_ATTRIB
-- Purpose: Used to insert or update attribute values in CFAS extension table for
--          the entity. Used in the CFAS UI procedure based block
--------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB(
   IO_attrib_tbl    IN OUT  NOCOPY  TYP_ATTRIB_TBL,
   I_group_id       IN              CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
   I_group_set      IN              GP_GROUP_SET%TYPE);
--------------------------------------------------------------------------------
-- Name   : LOCK_ATTRIB
-- Purpose: Used to lock the record at the attribute group level in the extension
--          table
--------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB(
   IO_attrib_tbl    IN OUT  NOCOPY  TYP_ATTRIB_TBL,
   I_group_id       IN              CFA_ATTRIB_GROUP.GROUP_ID%TYPE);
--------------------------------------------------------------------------------

                      
--------------------------------------------------------------------------------
-- Flex UI Functions
--------------------------------------------------------------------------------
FUNCTION INITIALIZE(
   O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table       IN      GP_BASE_TABLE%TYPE,
   I_group_set        IN      GP_GROUP_SET%TYPE,
   I_lang             IN      LANG.LANG%TYPE,
   I_simulate_ind     IN      VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BUILD_KEY_TBL(
   O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table       IN      GP_BASE_TABLE%TYPE,
   I_key_name_1       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_1        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_2       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_2        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_3       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_3        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_4       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_4        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_5       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_5        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_6       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_6        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_7       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_7        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_8       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_8        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_9       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_9        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_10      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_10       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_11      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_11       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_12      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_12       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_13      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_13       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_14      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_14       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_15      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_15       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_16      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_16       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_17      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_17       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_18      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_18       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_19      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_19       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_20      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_20       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    BUILD_ATTRIB_TBL
-- Purpose: This function is used to get the attribute values from the DB
--------------------------------------------------------------------------------
FUNCTION BUILD_ATTRIB_TBL(
   O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table         IN      GP_BASE_TABLE%TYPE,
   I_group_set          IN      GP_GROUP_SET%TYPE,
   I_simulate_ind       IN      VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_HEADER_CFG(
   O_error_message      IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_header_cfg_tbl    IN OUT  NOCOPY   TYP_HEADER_CFG_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_CFG(
   O_error_message      IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_attrib_cfg_tbl    IN OUT  NOCOPY   TYP_ATTRIB_CFG_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET(
   O_error_message      IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_group_set_tbl     IN OUT  NOCOPY   TBL_group_set_tbl,
   I_base_table         IN               GP_BASE_TABLE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_UI_VARIABLES(
   O_error_message    IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_ui_var_rec      IN OUT  NOCOPY   TYP_UI_VAR_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PERSIST_DATA(
   O_error_message    IN OUT           RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;   
--------------------------------------------------------------------------------   

--------------------------------------------------------------------------------
-- Name:    GET_VALUE_TBL
-- Purpose: This function is used to retrieve the key and attribute values via
--          PL/SQL collection
--------------------------------------------------------------------------------
FUNCTION GET_VALUE_TBL(
   O_error_message    IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_value_tbl       IN OUT  NOCOPY   TYP_VALUE_TBL,
   I_value_type       IN               VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    GET_VALUE
-- Purpose: This function is used to retrieve a value in VARCHAR2 for a specific
--          key or attribute. It also returns the data type and description if 
--          any
--------------------------------------------------------------------------------
FUNCTION GET_VALUE(
   O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_field_value       OUT      GP_FIELD_VALUE%TYPE,
   O_field_desc_value  OUT      GP_FIELD_DESC_VALUE%TYPE,
   O_data_type         OUT      GP_DATA_TYPE%TYPE,
   I_field_name        IN       CFA_SQL.GP_INDEX%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    SET_VALUE
-- Purpose: This function is used to set the GP_attrib_value_tbl for validation
--------------------------------------------------------------------------------
FUNCTION SET_VALUE(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_field_type         IN       GP_FIELD_TYPE%TYPE,
   I_field              IN       GP_FIELD_ITEM%TYPE,
   I_field_value        IN       GP_FIELD_VALUE%TYPE       DEFAULT NULL,
   I_field_desc_value   IN       GP_FIELD_DESC_VALUE%TYPE  DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    SET_OUTPUT
-- Purpose: This function is used to define the output parameters of the dynamic function
--          executed.
--------------------------------------------------------------------------------
FUNCTION SET_OUTPUT(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_field_type         IN       GP_FIELD_TYPE%TYPE,
   I_field              IN       GP_FIELD_ITEM%TYPE,
   I_field_value        IN       GP_FIELD_VALUE%TYPE       DEFAULT NULL,
   I_field_desc_value   IN       GP_FIELD_DESC_VALUE%TYPE  DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    CLEAR_OUTPUT_TBL
-- Purpose: This function is used to clear the output tbl parameter in prep for 
--          next validation
--------------------------------------------------------------------------------                    
FUNCTION CLEAR_OUTPUT_TBL(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    SET_RETURN_FIELD
-- Purpose: Used by the UI to set the focus to the target field e.g. for 
--          populating UI fields w/o requerying the block or setting the focus to
--          an error field when the group is applied.
-------------------------------------------------------------------------------- 
FUNCTION SET_RETURN_FIELD(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_field              IN       GP_FIELD_ITEM%TYPE)
RETURN BOOLEAN;   
--------------------------------------------------------------------------------
FUNCTION CLEAR_TBLS(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION ADD_STORE_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN;                           
------------------------------------------------------------------------------------------
END CFA_SQL;
/
