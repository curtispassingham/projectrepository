CREATE OR REPLACE PACKAGE BODY RMS_BATCH_STATUS_SQL AS
-------------------------------------------------------------------
FUNCTION GET_BATCH_RUNNING_IND(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_batch_running_ind   IN OUT   RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'RMS_BATCH_STATUS_SQL.GET_BATCH_RUNNING_IND';

   cursor C_GET_BATCH_IND is
      select batch_running_ind
        from rms_batch_status;

BEGIN

   open C_GET_BATCH_IND;
   fetch C_GET_BATCH_IND into O_batch_running_ind;
   close C_GET_BATCH_IND;

   -- This table is required to have a record, if the ind is null then there was a problem
   if O_batch_running_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            'C_GET_BATCH_IND',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM, 
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_BATCH_RUNNING_IND;
-----------------------------------------------------------------------------------
END RMS_BATCH_STATUS_SQL;
/
