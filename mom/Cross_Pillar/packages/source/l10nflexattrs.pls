CREATE OR REPLACE PACKAGE L10N_FLEX_ATTRIB_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------
ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

LP_lang                       L10N_ATTRIB_DESCS.LANG%TYPE;

LP_char_data_type             L10N_ATTRIB.DATA_TYPE%TYPE := 'VARCHAR2';
LP_num_data_type              L10N_ATTRIB.DATA_TYPE%TYPE := 'NUMBER';
LP_date_data_type             L10N_ATTRIB.DATA_TYPE%TYPE := 'DATE';
---
LP_text_item_widget           L10N_ATTRIB.UI_WIDGET%TYPE := 'TI';
LP_rec_group_widget           L10N_ATTRIB.UI_WIDGET%TYPE := 'RG';
LP_list_item_widget           L10N_ATTRIB.UI_WIDGET%TYPE := 'LI';
LP_check_box_widget           L10N_ATTRIB.UI_WIDGET%TYPE := 'CB';
LP_date_widget                L10N_ATTRIB.UI_WIDGET%TYPE := 'DT';
---
LP_pb_widget                  VARCHAR2(5)  := 'PB';
LP_desc_widget                VARCHAR2(10) := 'TI_DESC';
LP_key_widget                 VARCHAR2(5)  := 'KEY';

LP_l10n_ctry                  VARCHAR2(15) := 'L10N_CTRY_ID';
LP_l10n_depend_exist          VARCHAR2(1)  := 'Y';

---
LP_min_attrib_canvas_wd       NUMBER := 115; -- minimum attrib canvas width
LP_min_attrib_canvas_ht       NUMBER := 0;   -- minimum attrib canvas height

LP_header_field_wd            NUMBER := 68;  -- header field width (updated by UI at runtime)
LP_header_field_ht            NUMBER := 15;  -- header field height (standard)
LP_header_field_desc_wd       NUMBER := 130; -- header description field width (updated by UI at runtime)

LP_attrib_field_wd            NUMBER := 90;  -- attrib field width (updated by UI at runtime)
LP_attrib_field_ht            NUMBER := 15;  -- attrib field height (standard)
LP_attrib_field_desc_wd       NUMBER := 180; -- attrib description field width (updated by UI at runtime)
LP_attrib_field_pb_cb_wd      NUMBER := 14;  -- lov pushbutton/checkbox width (standard)

LP_field_to_field_ysp         NUMBER := 18;  -- field to field vertical spacing
LP_field_to_field_xsp         NUMBER := 10;  -- field to field horizontal spacing
LP_canvas_edge_to_field       NUMBER := 8;   -- space between the canvas edge and the field (or prompt)
LP_main_to_stack              NUMBER := 8;   -- main canvas edge to stacked canvas edge spacing
LP_stack_to_stack             NUMBER := 5;   -- edge to edge space between stacked canvas
LP_prompt_offset              NUMBER := 3;   -- Prompt to field spacing

LP_char_pt_size_large         NUMBER := 5;   -- Prompt character size
LP_char_pt_size_mid           NUMBER := 4.25;-- Prompt character size
LP_char_pt_size_small         NUMBER := 3;   -- Prompt character size

LP_attrib_init_ysp            NUMBER := 25;  -- Field initial Y position on attrib canvas
LP_header_init_xsp            NUMBER := 150;
LP_header_min_field_xsp       NUMBER := 67;  -- Minimum spacing between header fields

LP_group_canvas_wd            NUMBER := 166;


--------------------------------------------------------------------------------------------
-- Procedure-based Data Block objects
--------------------------------------------------------------------------------------------
TYPE TYP_l10n_attrib_group_rec is RECORD
(
   group_id                L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
   group_desc              L10N_ATTRIB_GROUP_DESCS.DESCRIPTION%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);
TYPE TYP_l10n_attrib_group_tbl is TABLE of TYP_l10n_attrib_group_rec INDEX by BINARY_INTEGER;

TYPE TYP_l10n_attrib_rec IS RECORD
(
   group_id                L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
   varchar2_1              VARCHAR2(250),
   varchar2_2              VARCHAR2(250),
   varchar2_3              VARCHAR2(250),
   varchar2_4              VARCHAR2(250),
   varchar2_5              VARCHAR2(250),
   varchar2_6              VARCHAR2(250),
   varchar2_7              VARCHAR2(250),
   varchar2_8              VARCHAR2(250),
   varchar2_9              VARCHAR2(250),
   varchar2_10             VARCHAR2(250),
   number_11               NUMBER,
   number_12               NUMBER,
   number_13               NUMBER,
   number_14               NUMBER,
   number_15               NUMBER,
   number_16               NUMBER,
   number_17               NUMBER,
   number_18               NUMBER,
   number_19               NUMBER,
   number_20               NUMBER,
   date_21                 DATE,
   date_22                 DATE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);
TYPE TYP_l10n_attrib_tbl is TABLE of TYP_l10n_attrib_rec INDEX BY BINARY_INTEGER;

--------------------------------------------------------------------------------------------
-- Helper objects to facilitate dynamic processes in both DB and UI
--------------------------------------------------------------------------------------------
TYPE TYP_l10n_ext_key_rec is RECORD
(
   header_type             VARCHAR2(15),
   l10n_ext_table          EXT_ENTITY.L10N_EXT_TABLE%TYPE,
   base_rms_table          EXT_ENTITY.BASE_RMS_TABLE%TYPE,
   key_col                 EXT_ENTITY_KEY.KEY_COL%TYPE,
   key_number              EXT_ENTITY_KEY.KEY_NUMBER%TYPE,
   data_type               EXT_ENTITY_KEY.DATA_TYPE%TYPE,
   key_desc                EXT_ENTITY_KEY_DESCS.KEY_DESC%TYPE, -- key label/prompt
   description_code        EXT_ENTITY_KEY.DESCRIPTION_CODE%TYPE,
   key_value               VARCHAR2(60),
   key_value_qry           VARCHAR(65),
   key_value_desc          VARCHAR2(255), -- key description field value
   ui_xpos                 NUMBER,
   ui_ypos                 NUMBER
);
TYPE TYP_l10n_ext_key_tbl is TABLE of TYP_l10n_ext_key_rec INDEX BY BINARY_INTEGER;

LP_header_cfg_tbl   TYP_l10n_ext_key_tbl;

TYPE TYP_l10n_attrib_cfg_rec is RECORD
(
   attrib_id               L10N_ATTRIB.ATTRIB_ID%TYPE,
   group_id                L10N_ATTRIB.GROUP_ID%TYPE,
   view_col_name           L10N_ATTRIB.VIEW_COL_NAME%TYPE,
   data_type               L10N_ATTRIB.DATA_TYPE%TYPE,
   attrib_storage_col      L10N_ATTRIB.ATTRIB_STORAGE_COL%TYPE,
   attrib_desc             L10N_ATTRIB_DESCS.DESCRIPTION%TYPE,
   value_req               L10N_ATTRIB.VALUE_REQ%TYPE,
   display_seq             L10N_ATTRIB.DISPLAY_SEQ%TYPE,
   ui_widget               L10N_ATTRIB.UI_WIDGET%TYPE,
   maximum_length          L10N_ATTRIB.MAXIMUM_LENGTH%TYPE,
   lowest_allowed_value    L10N_ATTRIB.LOWEST_ALLOWED_VALUE%TYPE,
   highest_allowed_value   L10N_ATTRIB.HIGHEST_ALLOWED_VALUE%TYPE,
   l10n_rec_group_id       L10N_ATTRIB.L10N_REC_GROUP_ID%TYPE,
   l10n_code_type          L10N_ATTRIB.L10N_CODE_TYPE%TYPE,
   attrib_validation_code  L10N_ATTRIB.VALIDATION_CODE%TYPE,
   enable_ind              L10N_ATTRIB.ENABLE_IND%TYPE, 
   attrib_value            VARCHAR2(250),
   col_no                  NUMBER,
   ---
   ui_xpos                 NUMBER,
   ui_ypos                 NUMBER,
   ---
   next_nav_item           VARCHAR2(30),
   prev_nav_item           VARCHAR2(30)

);
TYPE TYP_l10n_attrib_cfg_tbl is TABLE of TYP_l10n_attrib_cfg_rec INDEX BY VARCHAR2(25);

LP_attrib_cfg_tbl   TYP_l10n_attrib_cfg_tbl;

--------------------------------------------------------------------------------------------
-- DB-UI miscellaneous variable record
--------------------------------------------------------------------------------------------
TYPE TYP_ui_variables_rec is RECORD
(
   -- Size
   header_field_wd         NUMBER,
   header_field_ht         NUMBER,
   header_field_desc_wd    NUMBER,
      --
   attrib_field_wd         NUMBER,
   attrib_field_ht         NUMBER,
   attrib_field_desc_wd    NUMBER,
   attrib_field_pb_wd      NUMBER,
   --
   min_attrib_canvas_wd    NUMBER,
   min_attrib_canvas_ht    NUMBER,

   -- Spacing
   field_to_field_ysp      NUMBER,
   field_to_field_xsp      NUMBER,
   canvas_edge_to_field    NUMBER,
   attrib_offset_ysp       NUMBER,
   header_offset_xsp       NUMBER,
   main_to_stack           NUMBER,
   stack_to_stack          NUMBER,
   ---
   max_header_row_wd       NUMBER,
   header_row_cnt          NUMBER,
   max_header_label        NUMBER,
   max_attrib_row_wd       NUMBER,
   attrib_set_cnt          NUMBER,
   ---
   first_field_init_xpos   NUMBER,
   first_field_init_ypos   NUMBER,
   ---
   country_select_ind      VARCHAR2(1),
   lang                    L10N_ATTRIB_DESCS.LANG%TYPE,

   -- Types
   char_data_type          L10N_ATTRIB.DATA_TYPE%TYPE,
   num_data_type           L10N_ATTRIB.DATA_TYPE%TYPE,
   date_data_type          L10N_ATTRIB.DATA_TYPE%TYPE,
   ---
   text_item_widget        L10N_ATTRIB.UI_WIDGET%TYPE,
   rec_group_widget        L10N_ATTRIB.UI_WIDGET%TYPE,
   list_item_widget        L10N_ATTRIB.UI_WIDGET%TYPE,
   check_box_widget        L10N_ATTRIB.UI_WIDGET%TYPE,
   date_widget             L10N_ATTRIB.UI_WIDGET%TYPE,
   ---
   pb_widget               VARCHAR2(5),
   desc_widget             VARCHAR2(10),
   key_widget              VARCHAR2(5)

);
------------------------------------------------------------------------------------------
TYPE TYP_store_map_rec is RECORD
(
 st_group_id     L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
 stadd_group_id  L10N_ATTRIB_GROUP.GROUP_ID%TYPE
);
------------------------------------------------------------------------------------------

TYPE TYP_store_map_tbl is TABLE of TYP_store_map_rec  INDEX BY BINARY_INTEGER; 
------------------------------------------------------------------------------------------ 
LP_TYP_store_map_tbl  TYP_store_map_tbl;

------------------------------------------------------------------------------------------
PROCEDURE QUERY_GROUP(IO_group_tbl     IN OUT  NOCOPY  TYP_L10N_ATTRIB_GROUP_TBL,
                      I_base_table     IN              EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                      I_country_id     IN              L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE);
------------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB(IO_attrib_tbl     IN OUT  NOCOPY  TYP_L10N_ATTRIB_TBL,
                       I_group_id        IN              L10N_ATTRIB_GROUP.GROUP_ID%TYPE);
------------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB(IO_attrib_tbl     IN OUT  NOCOPY  TYP_L10N_ATTRIB_TBL,
                       I_group_id        IN              L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                       I_country_id      IN              L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE);
------------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB(IO_attrib_tbl     IN OUT  NOCOPY  TYP_L10N_ATTRIB_TBL,
                      I_group_id        IN             L10N_ATTRIB_GROUP.GROUP_ID%TYPE);
------------------------------------------------------------------------------------------
FUNCTION GET_KEY_DESC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_key_desc          IN OUT  VARCHAR2,
                      I_key_col           IN      EXT_ENTITY_KEY.KEY_COL%TYPE,
                      I_key_value         IN      VARCHAR2,
                      I_key_type          IN      VARCHAR2,
                      I_desc_plsql        IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION PREFETCH_DATA (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_base_rms_table   IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        I_country_id       IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_DATA(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_attrib_cfg_tbl     IN OUT   TYP_L10N_ATTRIB_CFG_TBL,
                         I_base_rms_table      IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_country_id          IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION ADD_STORE_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN;                           
------------------------------------------------------------------------------------------                      
END L10N_FLEX_ATTRIB_SQL;
/