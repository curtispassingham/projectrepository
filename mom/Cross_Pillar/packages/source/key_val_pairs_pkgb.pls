CREATE OR REPLACE PACKAGE body key_val_pairs_pkg
AS
  -----------------------------------------------------------------------------
FUNCTION get_map(
    I_pairs IN key_val_pairs)
  RETURN VARCHAR2 DETERMINISTIC
IS
  O_map VARCHAR2(4000):='/';
BEGIN
  FOR rec IN (SELECT * FROM TABLE(I_pairs) ORDER BY a_key)
  LOOP
    IF O_map IS NULL THEN
      O_map  := '/';
    END IF;
    IF LENGTH(O_map)+LENGTH(rec.a_val) <= 4000 THEN
      O_map                                   := O_map ||'/'||rec.a_val;
    END IF;
  END LOOP;
  RETURN O_map;
END get_map;
-----------------------------------------------------------------------------
FUNCTION set_attr(
    I_pairs IN key_val_pairs,
    I_key   IN VARCHAR2,
    I_val   IN VARCHAR2)
  RETURN key_val_pairs
IS
  O_pairs key_val_pairs:=I_pairs;
  l_found BOOLEAN      :=false;
BEGIN
  FOR i IN 1..O_pairs.count
  LOOP
    IF O_pairs(i).a_key = I_key THEN
      O_pairs(i).a_val := I_val;
      l_found          := true;
    END IF;
  END LOOP;
  IF NOT l_found THEN
    O_pairs.extend;
    O_pairs(O_pairs.count) := NEW key_value_pair(I_key,I_val);
  END IF;
  RETURN O_pairs;
END set_attr;
-----------------------------------------------------------------------------
FUNCTION get_attr(
    I_pairs IN key_val_pairs,
    I_key   IN VARCHAR2)
  RETURN VARCHAR2
IS
BEGIN
  FOR i IN 1..I_pairs.count
  LOOP
    IF I_pairs(i).a_key = I_key THEN
      RETURN I_pairs(i).a_val;
    END IF;
  END LOOP;
  RETURN NULL;
END get_attr;
-----------------------------------------------------------------------------
FUNCTION delete_key(
    I_pairs IN key_val_pairs,
    I_key   IN VARCHAR2)
  RETURN key_val_pairs
IS
  O_pairs key_val_pairs:=NEW key_val_pairs();
BEGIN
  FOR i IN 1..I_pairs.count
  LOOP
    IF I_pairs(i).a_key <> I_key THEN
      O_pairs.extend;
      O_pairs(O_pairs.count):=I_pairs(i);
    END IF;
  END LOOP;
  return O_pairs;
END delete_key;
-----------------------------------------------------------------------------
END key_val_pairs_pkg;
/