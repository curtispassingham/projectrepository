---------------------------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY CFA_GEN_SQL AS
---------------------------------------------------------------------------------------------------
FUNCTION STGS_EXISTS (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_ext_entity_id  IN      CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE,
	                    I_group_set_id   IN      CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN BOOLEAN  IS

   L_program          VARCHAR2(60) := 'CFA_GEN_SQL.STGS_EXISTS';
   L_table_owner      SYSTEM_OPTIONS.TABLE_OWNER%TYPE := NULL;
   L_existing_stg_tbl VARCHAR2(30) := NULL;
   
   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;

   cursor C_CHECK_TABLE is
      select table_name
        from all_tables
       where owner   in  (L_table_owner, USER)
         and table_name in (select grps.staging_table_name
                              from cfa_attrib_group_set grps
                             where grps.ext_entity_id = NVL(I_ext_entity_id, grps.ext_entity_id)
                               and grps.group_set_id  = NVL(I_group_set_id, grps.group_set_id)
                               and grps.staging_table_name is NOT NULL
                               and exists (select 'x'
                                             from cfa_attrib_group grp,
                                                  cfa_attrib att
                                            where att.group_id     = grp.group_id
                                              and grp.group_set_id = grps.group_set_id
                                              and att.active_ind   = 'N'));
         
BEGIN    
   ---
   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_table_owner;
   close C_GET_TAB_OWNER;
   ---
   open C_CHECK_TABLE;
   fetch C_CHECK_TABLE into L_existing_stg_tbl;
   close C_CHECK_TABLE;
   ---
   if L_existing_stg_tbl IS NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_EXISTS', L_existing_stg_tbl);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STGS_EXISTS;
---------------------------------------------------------------------------------------------------
END CFA_GEN_SQL;
/