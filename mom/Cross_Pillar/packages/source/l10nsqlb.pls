CREATE OR REPLACE PACKAGE BODY L10N_SQL AS
------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_l10n_obj     IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'L10N_SQL.EXEC_FUNCTION';
   L_procedure_name     VARCHAR2(120) := NULL;
   L_return             VARCHAR2(30);
   L_dynamic_call       VARCHAR2(300);
   L_country_id         COUNTRY_ATTRIB.COUNTRY_ID%TYPE := NULL;
   L_localized_ind      COUNTRY_ATTRIB.LOCALIZED_IND%TYPE := NULL;
   L_source_type        VARCHAR2 (6);
   L_source_id          VARCHAR2 (10);
       
   cursor C_GET_COUNTRY (l_entity IN VARCHAR2,
                         l_type IN VARCHAR2,
                         l_id IN VARCHAR2) is
      select l10n_country_id
        from mv_l10n_entity
       where entity=l_entity
         and nvl(entity_type,GP_null_ind)=nvl(l_type,GP_null_ind)
         and entity_id=l_id;   
       
   cursor C_GET_PO_COUNTRY is
      select case 
                when ca.localized_ind='Y' then 
                   oh.import_country_id 
                else 
                   NULL 
             end L10N_country_id
        from ordhead oh,
             country_attrib ca
       where oh.order_no = IO_l10n_obj.doc_id
         and oh.import_country_id=ca.country_id;
   
   cursor C_GET_L10N_PROC is
      select schema_name||'.'||package_name||'.'||function_name procedure_name
        from l10n_pkg_config
       where nvl(country_id,GP_base_country) = nvl(L_country_id,GP_base_country)
         and procedure_key = IO_l10n_obj.procedure_key;

   cursor C_GET_PHYSICAL_WH is
      select physical_wh 
        from wh
       where wh=IO_l10n_obj.source_id;
            
BEGIN
   if IO_l10n_obj.country_id is NULL then
      if IO_l10n_obj.doc_type = 'PO' then
         open C_GET_PO_COUNTRY;
         fetch C_GET_PO_COUNTRY into L_country_id;
         close C_GET_PO_COUNTRY;
      else  
         if IO_l10n_obj.source_entity='LOC' then
            L_source_type:=NULL;

            open C_GET_PHYSICAL_WH;
            fetch C_GET_PHYSICAL_WH into L_source_id;
            if (C_GET_PHYSICAL_WH %NOTFOUND) then
               L_source_id:=IO_l10n_obj.source_id;
            end if;
            close C_GET_PHYSICAL_WH;
            
	   else
            L_source_type:=IO_l10n_obj.source_type;
            L_source_id:=IO_l10n_obj.source_id;
         end if;      
      
         open C_GET_COUNTRY(IO_l10n_obj.source_entity,L_source_type,L_source_id);
   	   fetch C_GET_COUNTRY into L_country_id;
         close C_GET_COUNTRY;
      end if;
      IO_l10n_obj.country_id:=L_country_id;       
   else
      L_country_id := IO_l10n_obj.country_id;
   end if;

   open C_GET_L10N_PROC;
   fetch C_GET_L10N_PROC into L_procedure_name;
   close C_GET_L10N_PROC;

   if L_procedure_name is null then
      L_country_id := GP_base_country;

      open C_GET_L10N_PROC;
      fetch C_GET_L10N_PROC into L_procedure_name;
      close C_GET_L10N_PROC;

      if L_procedure_name is null then
         return TRUE;
      end if;
   end if;
 
   -- Dynamically execute the function.
   L_dynamic_call := 'BEGIN if '||L_procedure_name||' (:O_error_message, :IO_l10n_obj) = FALSE then :L_return := ''N''; end if; END;';
   -- Generic execution - O_error_message,IO_l10n_obj will be common to all scripts
   EXECUTE IMMEDIATE L_dynamic_call using in out O_error_message, in out IO_l10n_obj, out L_return;
      
   if L_return = 'N' then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FUNCTION;
---------------------------------------------------------------------
FUNCTION CALL_EXEC_FUNC_FND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_exists_ind   IN OUT          VARCHAR2,
                            I_procedure_key IN              VARCHAR2,
                            I_country_id    IN              VARCHAR2,
                            I_source_entity IN              VARCHAR2,
                            I_source_id     IN              VARCHAR2,
                            I_source_type   IN              VARCHAR2,
                            I_item          IN              VARCHAR2) 
RETURN BOOLEAN is

   L_program VARCHAR2(64) := 'L10N_SQL.CALL_EXEC_FUNC_FND';
   IO_l10n_obj_sub      L10N_EXISTS_REC := L10N_EXISTS_REC();
 
BEGIN

   IO_l10n_obj_sub.procedure_key := I_procedure_key;
   IO_l10n_obj_sub.country_id    := I_country_id;
   IO_l10n_obj_sub.source_entity := I_source_entity;
   IO_l10n_obj_sub.source_id     := I_source_id;
   IO_l10n_obj_sub.source_type   := I_source_type;
   IO_l10n_obj_sub.item          := I_item;
   IO_l10n_obj_sub.exists_ind    := 'Y';

   if L10N_SQL.EXEC_FUNCTION(O_error_message,
                             IO_l10n_obj_sub)= FALSE then
       return FALSE;                  
   end if;  	  
   
   IO_exists_ind := IO_l10n_obj_sub.exists_ind; 
   	  
   return TRUE;
   	  
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;   	     
END CALL_EXEC_FUNC_FND;
---------------------------------------------------------------------
FUNCTION CALL_EXEC_FUNC_DOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                             IO_exists_ind   IN OUT VARCHAR2,  
                             I_procedure_key IN VARCHAR2,
                             I_country_id    IN VARCHAR2 DEFAULT NULL,
                             I_doc_type      IN VARCHAR2 DEFAULT NULL,
                             I_doc_id        IN NUMBER   DEFAULT NULL)
RETURN BOOLEAN IS 

   L_program VARCHAR2(30) := 'L10N_SQL.CALL_EXEC_FUNC_DOC';
   IO_l10n_obj        L10N_OBJ := L10N_OBJ();
   IO_l10n_exists_rec L10N_EXISTS_REC := L10N_EXISTS_REC();

   cursor C_country is 
         select distinct country_id, doc_type 
       from L10N_DOC_DETAILS_GTT;

   cursor C_order_no is
      select distinct country_id, doc_id , doc_type
        from L10N_DOC_DETAILS_GTT;
      

BEGIN
   if I_PROCEDURE_KEY in ('CREATE_TRAN_UTIL_CODE','COPY_TRAN_UTIL_CODE') then
      for C_rec in C_country loop
         IO_l10n_obj.procedure_key := I_procedure_key; 
         IO_l10n_obj.doc_type      := C_rec.doc_type; 
         IO_l10n_obj.country_id    := C_rec.country_id; 

         --- Make a call to the wrapper function
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   IO_l10n_obj) = FALSE then
            return FALSE; 
         end if; 
      end loop; 
   elsif I_PROCEDURE_KEY = 'CHECK_UTIL_CODE_EXISTS' then
       IO_l10n_exists_rec.procedure_key := I_procedure_key;
       IO_l10n_exists_rec.country_id    := I_country_id;
       IO_l10n_exists_rec.doc_type      := I_doc_type;
       IO_l10n_exists_rec.doc_id        := I_doc_id;
       IO_l10n_exists_rec.exists_ind    := 'Y';

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                IO_l10n_exists_rec)= FALSE then
         return FALSE;                  
      end if;  	  
   
      IO_EXISTS_IND := IO_l10n_exists_rec.exists_ind; 
   elsif I_procedure_key = 'LOAD_ORDER_TAX_OBJECT' then
      IO_l10n_obj.procedure_key := I_procedure_key; 
      IO_l10n_obj.doc_type      := I_doc_type; 
      IO_l10n_obj.doc_id        := I_doc_id;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                IO_l10n_obj)= FALSE then
         return FALSE;                     
      end if;
   elsif I_procedure_key = 'DEAL_ORDER_DISCOUNT' then
      for C_rec in C_order_no loop
         IO_l10n_obj.procedure_key := I_procedure_key;
         IO_l10n_obj.doc_type      := C_rec.doc_type;
         IO_l10n_obj.doc_id        := C_rec.doc_id;
         IO_l10n_obj.country_id    := C_rec.country_id;
        
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   IO_l10n_obj) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CALL_EXEC_FUNC_DOC;
----------------------------------------------------------------------
FUNCTION REFRESH_MV_L10N_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'L10N_SQL.REFRESH_MV_L10N_ENTITY';

BEGIN
   DBMS_MVIEW.REFRESH('mv_l10n_entity','c');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END REFRESH_MV_L10N_ENTITY;

---------------------------------------------------------------------------
FUNCTION GET_BATCH_FUNCTION(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_function_name IN OUT VARCHAR2,
                            IO_l10n_obj      IN OUT L10N_OBJ)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'L10N_SQL.GET_BATCH_FUNCTION';
   L_function_name      VARCHAR2(30) := NULL;
   L_return             VARCHAR2(30);
   L_country_id         COUNTRY_ATTRIB.COUNTRY_ID%TYPE := NULL;
   L_localized_ind      COUNTRY_ATTRIB.LOCALIZED_IND%TYPE := NULL;
   L_source_type        VARCHAR2(6);
       
   cursor C_GET_COUNTRY (l_entity IN VARCHAR2,
                         l_type IN VARCHAR2,
                         l_id IN VARCHAR2) is
      select l10n_country_id
        from mv_l10n_entity
       where entity=l_entity
         and nvl(entity_type,GP_null_ind)=nvl(l_type,GP_null_ind)
         and entity_id=l_id;   
       
   cursor C_GET_PO_COUNTRY is
      select case 
                when ca.localized_ind='Y' then 
                   oh.import_country_id 
                else 
                   NULL 
             end L10N_country_id
        from ordhead oh,
             country_attrib ca
       where oh.order_no = IO_l10n_obj.doc_id
         and oh.import_country_id=ca.country_id;
   
   cursor C_GET_L10N_PROC is
      select function_name
        from l10n_batch_config
       where nvl(country_id,GP_base_country) = nvl(L_country_id,GP_base_country)
         and function_key = IO_l10n_obj.procedure_key;
            
BEGIN
   if IO_l10n_obj.country_id is NULL then
      if IO_l10n_obj.doc_type = 'PO' then
         open C_GET_PO_COUNTRY;
         fetch C_GET_PO_COUNTRY into L_country_id;
         close C_GET_PO_COUNTRY;
      else 
         if IO_l10n_obj.source_entity='LOC' then
            L_source_type:=NULL;
         else
            L_source_type:=IO_l10n_obj.source_type;
         end if;
       
         open C_GET_COUNTRY(IO_l10n_obj.source_entity,L_source_type,IO_l10n_obj.source_id);
   	   fetch C_GET_COUNTRY into L_country_id;
         close C_GET_COUNTRY;
      end if;         
   else
      L_country_id := IO_l10n_obj.country_id;
   end if;
            
   open C_GET_L10N_PROC;
   fetch C_GET_L10N_PROC into L_function_name;
   close C_GET_L10N_PROC;
   
   if L_function_name is null then
      L_country_id := GP_base_country;

      open C_GET_L10N_PROC;
      fetch C_GET_L10N_PROC into L_function_name;
      close C_GET_L10N_PROC;
   end if;
   
   IO_function_name:=L_function_name;
 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BATCH_FUNCTION;
------------------------------------------------------------------------------
FUNCTION GET_CNTRY_LOCAIND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_country_id    OUT VARCHAR2,
                           O_localized_ind OUT VARCHAR2,
                           I_entity        IN  VARCHAR2,
                           I_type          IN  VARCHAR2,    
                           I_id            IN  VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'L10N_SQL.GET_CNTRY_LOCAIND';
   L_type               VARCHAR2(6);   
       
   cursor C_GET_CNTRY_LOCAIND is
      select country_id,localized_ind
        from mv_l10n_entity
       where entity = I_entity
         and nvl(entity_type,GP_null_ind) = nvl(L_type,GP_null_ind)
         and entity_id = I_id;
         
   BEGIN   
    
    if I_entity = 'LOC' then
      L_type := NULL;
    else
      L_type := I_type;
    end if;
         
    open C_GET_CNTRY_LOCAIND;
    fetch C_GET_CNTRY_LOCAIND into O_country_id,O_localized_ind;
    close C_GET_CNTRY_LOCAIND;
    
    return TRUE;
    
    EXCEPTION
       when OTHERS then
          O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
          return FALSE;
   END GET_CNTRY_LOCAIND;
------------------------------------------------------------------------------

FUNCTION CALL_DEL_FUNC_DOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                            IO_exists_ind   IN OUT VARCHAR2,
                            I_procedure_key IN     VARCHAR2,
                            I_country_id    IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS 
   L_program VARCHAR2(30) := 'L10N_SQL.CALL_DEL_FUNC_DOC';
   IO_l10n_obj        L10N_OBJ := L10N_OBJ();
   
BEGIN
   if I_procedure_key in ('DELETE_TSFHEAD_L10N_EXT') then
      IO_l10n_obj.procedure_key  := I_procedure_key; 
      IO_l10n_obj.country_id := I_country_id; 
      --- Make a call to the wrapper function
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                IO_l10n_obj) = FALSE then
         return false;
      end if;
   end if;
   return TRUE;
EXCEPTION 
      when OTHERS then
          O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
          return false;
END CALL_DEL_FUNC_DOC;
------------------------------------------------------------------------------------------------- 

END L10N_SQL;
/
