create or replace and compile java source named "ZIPImpl"  
 AS  
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import java.sql.SQLException;

import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZIPImpl {
    public static void getFileFromZip(oracle.sql.BLOB srcBlob,
                                      oracle.sql.CLOB[] dstClob) throws SQLException, IOException {
        Writer outBuffer = dstClob[0].setCharacterStream(0);
        InputStream inBuffer = srcBlob.getBinaryStream();
        ZipInputStream zip = new ZipInputStream(inBuffer);
        Reader reader = new InputStreamReader(zip, "UTF-8");
        ZipEntry entry;
        char[] tmpBuffer = new char[4096];
        while ((entry = zip.getNextEntry()) != null) {
            int n;
            if (entry.getName().equals("content.xml")) {
                while ((n = reader.read(tmpBuffer)) >= 0)
                    outBuffer.write(new String(tmpBuffer, 0, n));
            }
        }
        outBuffer.close();
    }

    public static void putFileToZip(oracle.sql.BLOB tmplBlob, oracle.sql.CLOB dataClob,
                                    oracle.sql.BLOB[] lBlob,
                                    oracle.sql.BLOB[] dstBlob) throws IOException, SQLException {
        ZipOutputStream outBuffer = new ZipOutputStream(dstBlob[0].setBinaryStream(0));
        OutputStream lBlobOutBuffer;
        InputStream lBlobInBuffer;
        Reader dataIn = dataClob.getCharacterStream();
        InputStream inBuffer = tmplBlob.getBinaryStream();
        ZipInputStream zip = new ZipInputStream(inBuffer);
        ZipEntry entry;
        char[] tmpCharBuffer = new char[4096];
        byte[] tmpByteBuffer = new byte[4096];
        while ((entry = zip.getNextEntry()) != null) {
            int n;
            outBuffer.putNextEntry(entry);
            lBlob[0].truncate(0);
            lBlobOutBuffer = lBlob[0].setBinaryStream(0);
            if (entry.getName().equals("content.xml")) {
                while ((n = dataIn.read(tmpCharBuffer)) >= 0) {
                    lBlobOutBuffer.write(new String(tmpCharBuffer, 0, n).getBytes("UTF-8"));
                }
            } else {
                while ((n = zip.read(tmpByteBuffer)) >= 0)
                    lBlobOutBuffer.write(tmpByteBuffer, 0, n);
            }
            lBlobOutBuffer.close();
            lBlobInBuffer = lBlob[0].getBinaryStream();
            while ((n = lBlobInBuffer.read(tmpByteBuffer)) >= 0)
                outBuffer.write(tmpByteBuffer, 0, n);
        }
        outBuffer.close();
    }
};   
/
    
 create or replace package zip as  
    procedure unzip(   
       p_src       in          blob  
    ,  p_dst       in out      clob  );   
    procedure Zip(   
       p_tmpl       in          blob  
    ,  p_data        in          clob 
    ,  p_lblob       in out          blob  
    ,  p_dst       in out      blob  );   
 end;   
 /  
    
 create or replace package body zip as  
    procedure unzip(   
       p_src       in          blob  
    ,  p_dst       in out      clob)
    as language java   
    name 'ZIPImpl.getFileFromZip(oracle.sql.BLOB, oracle.sql.CLOB[])';   
    procedure Zip(   
       p_tmpl       in          blob  
    ,  p_data        in          clob 
    ,  p_lblob       in out          blob  
    ,  p_dst       in out      blob  )
    as language java   
    name 'ZIPImpl.putFileToZip(oracle.sql.BLOB,oracle.sql.CLOB,oracle.sql.BLOB[],oracle.sql.BLOB[])';   
 end;   
/
