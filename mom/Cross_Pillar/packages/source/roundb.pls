CREATE OR REPLACE PACKAGE BODY ROUNDING_SQL AS

/*** Package Variable Declarations***/

LP_round_to_inner_pct    system_options.round_to_inner_pct%TYPE;
LP_round_to_case_pct     system_options.round_to_case_pct%TYPE;
LP_round_to_pallet_pct   system_options.round_to_pallet_pct%TYPE;

-- Define a record type to store a line item
TYPE line_item_rectype IS RECORD(item                 item_master.item%TYPE,
                                 location             ordloc.location%TYPE,
                                 loc_type             ordloc.loc_type%TYPE,
                                 xdock_wh_ind         VARCHAR2(1),
                                 supplier             ordhead.supplier%TYPE,
                                 orig_cntry           ordsku.origin_country_id%TYPE,
                                 rounding_seq         wh.rounding_seq%TYPE,
                                 physical_wh          wh.physical_wh%TYPE,
                                 init_qty             ordloc_wksht.calc_qty%TYPE,
                                 qty_ordered          ordloc.qty_ordered%TYPE,
                                 last_rounded_qty     ordloc.last_rounded_qty%TYPE,
                                 last_grp_rounded_qty ordloc.last_grp_rounded_qty%TYPE,
                                 qty_received         ordloc.qty_received%TYPE,
                                 qty_cancelled        ordloc.qty_cancelled%TYPE,
                                 cancel_code          ordloc.cancel_code%TYPE,
                                 cancel_date          ordloc.cancel_date%TYPE,
                                 cancel_id            ordloc.cancel_id%TYPE,
                                 total_order_qty      ordloc.qty_ordered%TYPE,
                                 receipts_ind         VARCHAR2(1),
                                 total_alloc_qty      NUMBER,
                                 supp_pack_size       ordsku.supp_pack_size%TYPE,
                                 uop                  ordloc_wksht.uop%TYPE,
                                 standard_uom         item_master.standard_uom%TYPE,
                                 ti                   item_supp_country.ti%TYPE,
                                 hi                   item_supp_country.hi%TYPE,
                                 round_lvl            item_supp_country_loc.round_lvl%TYPE,
                                 round_to_pallet_pct  item_supp_country_loc.round_to_pallet_pct%TYPE,
                                 round_to_layer_pct   item_supp_country_loc.round_to_layer_pct%TYPE,
                                 round_to_case_pct    item_supp_country_loc.round_to_case_pct%TYPE,
                                 source_type          repl_results.source_type%TYPE,
                                 tsf_linked_ind       VARCHAR2(1),
                                 changed_ind          VARCHAR2(1),
                                 line_item_rowid      ROWID);

-- Define a table type based upon the line item record defined above.
TYPE line_item_tabletype IS TABLE OF line_item_rectype
   INDEX BY BINARY_INTEGER;

-- Define a record type to hold a rounded qty and a rounded grp qty for a line item.
TYPE rounded_qtys_rectype IS RECORD(item                  item_master.item%TYPE,
                                    line_rowid            rowid,
                                    rounding_seq          wh.rounding_seq%TYPE,
                                    last_rounded_qty      ordloc.last_rounded_qty%TYPE,
                                    last_grp_rounded_qty  ordloc.last_grp_rounded_qty%TYPE);

-- Define a table that will store rounded qty records.
TYPE rounded_qtys_tabletype IS TABLE OF rounded_qtys_rectype
   INDEX BY BINARY_INTEGER;

/*** Private Function Declarations ***/

-------------------------------------------------------------------------------
-- Function Name: ROUND_ALLOCATIONS
-- Purpose      : This package will round any allocation qty's for stores associated with an xdock wh.
--                It will also adjust the ordloc qty for the wh if the total rounded allocated qty is
--                greater than the qty ordered to the wh.
-------------------------------------------------------------------------------
FUNCTION ROUND_ALLOCATIONS(O_error_message       OUT    VARCHAR2,
                           I_line_item_table     IN OUT NOCOPY line_item_tabletype,
                           I_order_no            IN     ordhead.order_no%TYPE,
                           I_skip_alloc_round    IN     VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
FUNCTION PROCESS_XDOCK_ORDERS(O_error_message       OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              I_line_item_table     IN OUT NOCOPY line_item_tabletype,
                              I_order_no            IN     ordhead.order_no%TYPE,
                              I_skip_alloc_round    IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: PRORATE_ALLOCATIONS
-- Purpose      : This package will distribute the excess order quantity among allocation based on store order multiples for xdock orders.
-------------------------------------------------------------------------------
FUNCTION PRORATE_ALLOCATIONS(O_error_message       OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no            IN     ordhead.order_no%TYPE,
                             I_item                IN     item_master.item%TYPE,
                             I_total_order_qty     IN     ordloc.qty_ordered%TYPE,
                             I_total_alloc_qty     IN     ordloc.qty_ordered%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: ROUND_LINE_ITEMS
-- Purpose      : This function will be a private function. It will be interfaced by one of several
--                interface programs in this package. It accepts a table of line item records. If
--                multi-channel is off, this function will simply loop through the table, round
--                each line item, and update the table. If it is a multi-channel environment, this
--                function will need to group virtual warehouses by rounding group. It will round
--                the 'combined' line item.
-------------------------------------------------------------------------------
FUNCTION ROUND_LINE_ITEMS(O_error_message        OUT    VARCHAR2,
                          I_line_item_table      IN OUT NOCOPY line_item_tabletype,
                          I_order_no             IN     ordhead.order_no%TYPE DEFAULT NULL,
                          I_orig_approval_date   IN     ordhead.orig_approval_date%TYPE DEFAULT NULL)
RETURN BOOLEAN;


FUNCTION ROUND_LINE_ITEMS(O_error_message           OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_covered_inbound         OUT   BOOLEAN,
                          I_line_item_table      IN OUT   NOCOPY LINE_ITEM_TABLETYPE,
                          I_order_no             IN       ORDHEAD.ORDER_NO%TYPE DEFAULT NULL,
                          I_orig_approval_date   IN       ORDHEAD.ORIG_APPROVAL_DATE%TYPE DEFAULT NULL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: APPLY_ROUND_DIFFERENCE
-- Purpose      : This private function will take an unrounded qty for a set of locations
--                and apply any increase or decrease necessary to round the total order
--                quantity for the set. If this is a single location, the increment or
--                decrement will be applied to that location. If this is a set of locations,
--                if a 'round leader' was passed, the increment or decrement will be applied
--                to the round leader. If not, it will be applied to the lowest location number
--                of the set.
--
--                In the case of a decrement, if there is not enough order to qty to cover the
--                decrement for the 'designated' location, the function will attempt to apply
--                the decrement to another location within the set. If no locations in the set can
--                cover the decrement, it will re-round the 'raw' qty forcing the rounding logic
--                to round up. It will then apply the increment to the round leader if one was
--                passed and to the lowest location number within the set otherwise.
-------------------------------------------------------------------------------
FUNCTION APPLY_ROUND_DIFFERENCE(O_error_message         OUT    VARCHAR2,
                                I_line_item_table       IN OUT NOCOPY line_item_tabletype,
                                I_raw_qty               IN     NUMBER,
                                I_rounded_qty           IN OUT NUMBER,
                                I_round_leader          IN     INT,
                                I_lowest_loc_rec        IN     INT,
                                I_highest_loc_rec       IN     INT)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: CALC_TOTAL_ORDER_QTYS
-- Purpose      : This function calculates the total order qty including cancelled qtys.
--                After rounding, this qty will be used to adjust the cancel qty to reflect
--                rounding (total qty - new order qty = new cancel qty).
-------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_ORDER_QTYS(O_error_message       OUT    VARCHAR2,
                               I_line_item_table     IN OUT NOCOPY line_item_tabletype,
                               I_total_ord_qty_table IN     rounding_sql.total_ord_qty_tabletype)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: UPDATE_CANCEL_QTYS
-- Purpose      : Calculates the qty cancelled based upon the total original order qty (orig order qty + cancelled qty)
--                and the new rounded order qty. The funtion should only be used when an order has been approved previously.
-------------------------------------------------------------------------------
FUNCTION UPDATE_CANCEL_QTYS(O_error_message   OUT    VARCHAR2,
                            I_line_item_table IN OUT NOCOPY line_item_tabletype,
                            I_order_no        IN     ordhead.order_no%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDLOC
-- Purpose      : This private function updates the ordloc table with the rounded qty
--                if an update is required. It also updates cancel and prescale qtys.
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_line_item_table        IN OUT   LINE_ITEM_TABLETYPE,
                       I_order_no               IN       ORDLOC.ORDER_NO%TYPE,
                       I_update_orig_repl_qty   IN       VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: UPDATE_BYR_WRSKT_LINE_ITEMS
-- Purpose      : This private function updates the appropriate line item table
--                with rounded order qtys where an update is required.
-------------------------------------------------------------------------------
FUNCTION UPDATE_BYR_WRSKT_LINE_ITEMS(O_error_message   OUT VARCHAR2,
                                     I_line_item_table IN  line_item_tabletype,
                                     I_audsid          IN  repl_results.audsid%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDER_MATRIX_QTYS
-- Purpose      : This private function updates the ordloc_wksht table with the rounded qty
--                if an update is required
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_MATRIX_QTYS(O_error_message       OUT VARCHAR2,
                                  I_order_no            IN  ordloc.order_no%TYPE,
                                  I_line_item_table     IN  line_item_tabletype)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_STANDARD_CLASS
-- Purpose      : This function gets the uom class for the item's standard uom.
--                A standard uom may or may not be passed. If one is passed, the
--                uom class is retrieved for the passed standard uom.
-------------------------------------------------------------------------------
FUNCTION GET_STANDARD_CLASS(O_error_message  OUT VARCHAR2,
                            O_standard_class OUT uom_class.uom_class%TYPE,
                            I_standard_uom   IN  item_master.standard_uom%TYPE,
                            I_item           IN  item_master.item%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: ROUND_TO_INT
-- Purpose      : For some reason, the PL/SQL built in round function does not compile
--                in a database package. Therefore, this internal round function was written to
--                round real numbers to integers.
-------------------------------------------------------------------------------
FUNCTION ROUND_TO_INT(I_real_number IN NUMBER)
RETURN NUMBER;

---------------------------------------------------------------------
-- Function Name : ROUND_BY_LEVEL
-- Purpose           : This function will be used to round the passed-in Quantity parameter.
--                 Rounding will be based on the Rounding and Size (Ti, Hi, Pack Size) values
--                 passed in.  If any one of these parameters isn't passed in,
--                 they will all be retrieved from the database, based on the Supplier,
--                 Country and Location info. passed in.
--                 The Must Round Up indicator will force the function to round upwards only.
--                 The Override Failed output indicator will return a 'Y' if the passed-in
--                 Override Case Size isn't a factor of the Layer Size (and hence of the Pallet
--                 Size), and the Round Level doesn't specify Case Rounding (i.e. Round Level
--                 isn't 'C', 'CL', or 'CLP').  The 'Y' will indicate that Rounding couldn't
--                 be performed for this reason.
--
--                 **** NOTE: This package should not be called publicly. If it is double rounding
--                 ****       may occur. It should only be accessed through one of the three interface
--                 ****       programs which will prevent double rounding.
---------------------------------------------------------------
FUNCTION ROUND_BY_LEVEL(O_error_message       IN OUT VARCHAR2,
                        O_out_qty             IN OUT ORDLOC.QTY_ORDERED%TYPE,
                        O_override_fail_ind   IN OUT VARCHAR2,
                        I_in_qty              IN     ORDLOC.QTY_ORDERED%TYPE,
                        I_override_case_size  IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        I_item                IN     ITEM_SUPPLIER.ITEM%TYPE,
                        I_supplier            IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_origin_country      IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_location            IN     ITEM_LOC.LOC%TYPE,
                        I_round_lvl           IN     ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                        I_round_to_case_pct   IN     ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                        I_round_to_layer_pct  IN     ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE,
                        I_round_to_pallet_pct IN     ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE,
                        I_ti                  IN     ITEM_SUPP_COUNTRY.TI%TYPE,
                        I_hi                  IN     ITEM_SUPP_COUNTRY.HI%TYPE,
                        I_supp_pack_size      IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        I_must_roundup_ind    IN     VARCHAR2)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: VALIDATE_PHYSICAL_QTY
-- Purpose      : This internal function performs any needed validation for the physical
--                location which may encompass several rounding groups. In particular, the
--                order qty for the physical cannot be lower than the outstanding appointment
--                or shipment qty, whichever is greater, plus the qty already recevied for the
--                physical item/location.
-------------------------------------------------------------------------------
FUNCTION VALIDATE_PHYSICAL_QTY(O_error_message      OUT    VARCHAR2,
                               O_covered_inbound    OUT    BOOLEAN,
                               I_order_no           IN     ordhead.order_no%TYPE,
                               I_orig_approval_date IN     ordhead.orig_approval_date%TYPE,
                               I_line_item_table    IN OUT NOCOPY line_item_tabletype,
                               I_rounded_qty        IN     NUMBER,
                               I_phys_rounded_qty   IN     NUMBER,
                               I_round_leader       IN     INT,
                               I_lowest_loc_rec     IN     INT)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_OUTSTANDING_SHIP_QTY
-- Purpose      : This internal function retrieves the qty shipped minus that
--                which has already been received and not cancelled for an
--                item/physical location.
-------------------------------------------------------------------------------
FUNCTION GET_OUTSTANDING_SHIP_QTY(O_error_message        OUT VARCHAR2,
                                  O_outstanding_ship_qty OUT shipsku.qty_expected%TYPE,
                                  I_order_no             IN  ordhead.order_no%TYPE,
                                  I_line_item_rec        IN  line_item_rectype)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_OUTSTANDING_APPT_QTY
-- Purpose      : This internal function retrieves the qty appointed minus that
--                which has already been received for an item/physical location.
-------------------------------------------------------------------------------
FUNCTION GET_OUTSTANDING_APPT_QTY(O_error_message        OUT VARCHAR2,
                                  O_outstanding_appt_qty OUT appt_detail.qty_appointed%TYPE,
                                  I_order_no             IN  ordhead.order_no%TYPE,
                                  I_line_item_rec        IN  line_item_rectype)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: APPLY_PHYS_QTY_SHORTFALL
-- Purpose      : When rounding up, smallest component of the original round lvl should be used.
--                So in the case of CLP, the round up should be to the next case. This function
--                given a round lvl, determines what the round up round lvl should be and returns
--                that.
-------------------------------------------------------------------------------
FUNCTION APPLY_PHYS_QTY_SHORTFALL(O_error_message     OUT    VARCHAR2,
                                  I_line_item_table   IN OUT NOCOPY line_item_tabletype,
                                  I_rounded_qty       IN     NUMBER,
                                  I_phys_rounded_qty  IN     NUMBER,
                                  I_inbound_qty       IN     NUMBER,
                                  I_round_leader      IN     INT,
                                  I_lowest_loc_rec    IN     INT)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_ROUNDUP_ROUNDLVL
-- Purpose      :  When rounding up, smallest component of the original round lvl should be used.
--                 So in the case of CLP, the round up should be to the next case. This function
--                 given a round lvl, determines what the round up round lvl should be and returns
--                 that.
-------------------------------------------------------------------------------
FUNCTION GET_ROUNDUP_ROUNDLVL(O_error_message    OUT VARCHAR2,
                              O_roundup_roundlvl OUT item_supp_country_loc.round_lvl%TYPE,
                              I_round_lvl        IN  item_supp_country_loc.round_lvl%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: CALC_ROUNDED_GROUP_QTYS
-- Purpose      : This function calculates the last grp rounded qty.
-------------------------------------------------------------------------------
FUNCTION CALC_ROUNDED_GROUP_QTYS(O_error_message     OUT VARCHAR2,
                                 IO_line_item_table IN OUT NOCOPY line_item_tabletype)
RETURN BOOLEAN;

FUNCTION CALC_ROUNDED_GROUP_QTYS(O_error_message       OUT    VARCHAR2,
                                 IO_rounded_qtys_table IN OUT NOCOPY rounded_qtys_tabletype)
RETURN BOOLEAN;

/*** Function Bodies ***/

-------------------------------------------------------------------------------
FUNCTION ORDER_INTERFACE(O_error_message        OUT VARCHAR2,
                         I_order_no             IN  ordhead.order_no%TYPE,
                         I_supplier             IN  ordhead.supplier%TYPE,
                         I_update_orig_repl_qty IN  VARCHAR2 DEFAULT 'N',
                         I_skip_alloc_round     IN  VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

L_dummy               rounding_sql.total_ord_qty_tabletype;
L_covered_inbound     BOOLEAN;

BEGIN

   /*
    * This overloaded version of the order interface is for use if the
    * calling program has no knowledge of the original order qty's
    * and does not wish to be notified if rounding adjusted an order qty up
    * to meet inbound qtys.
    */

   if ORDER_INTERFACE(O_error_message,
                      L_covered_inbound,
                      I_order_no,
                      I_supplier,
                      L_dummy,
                      I_update_orig_repl_qty,
                      I_skip_alloc_round) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ORDER_INTERFACE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ORDER_INTERFACE;
-------------------------------------------------------------------------------
FUNCTION ORDER_INTERFACE(O_error_message        OUT VARCHAR2,
                         O_covered_inbound      OUT BOOLEAN,
                         I_order_no             IN  ordhead.order_no%TYPE,
                         I_supplier             IN  ordhead.supplier%TYPE,
                         I_update_orig_repl_qty IN  VARCHAR2 DEFAULT 'N',
                         I_skip_alloc_round     IN  VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

L_dummy               rounding_sql.total_ord_qty_tabletype;

BEGIN

   /*
    * This overloaded version of the order interface is for use if the
    * calling program has no knowledge of the original order qty's.
    */

   if ORDER_INTERFACE(O_error_message,
                      O_covered_inbound,
                      I_order_no,
                      I_supplier,
                      L_dummy,
                      I_update_orig_repl_qty,
                      I_skip_alloc_round) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ORDER_INTERFACE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ORDER_INTERFACE;
-------------------------------------------------------------------------------
FUNCTION ORDER_INTERFACE_WRP(O_error_message             OUT   VARCHAR2,
                             O_covered_inbound           OUT   INTEGER,
                             I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                             I_supplier               IN       ORDHEAD.SUPPLIER%TYPE,
                             I_total_ord_qty_table    IN       WRP_ROUNDING_ORDQTY_TBL,
                             I_update_orig_repl_qty   IN       VARCHAR2 DEFAULT 'N',
                             I_skip_alloc_round       IN       VARCHAR2 DEFAULT 'N')
RETURN INTEGER IS

   L_temp_ord_qty_table   ROUNDING_SQL.TOTAL_ORD_QTY_TABLETYPE;
   L_covered_inbound      BOOLEAN;

BEGIN

   if I_total_ord_qty_table is NOT NULL then
      FOR i in 1..I_total_ord_qty_table.COUNT
      LOOP
         L_temp_ord_qty_table(i).item            := I_total_ord_qty_table(i).item;
         L_temp_ord_qty_table(i).location        := I_total_ord_qty_table(i).location;
         L_temp_ord_qty_table(i).total_order_qty := I_total_ord_qty_table(i).total_order_qty;
      END LOOP;
   end if;
   if ORDER_INTERFACE(O_error_message,
                      L_covered_inbound,
                      I_order_no,
                      I_supplier,
                      L_temp_ord_qty_table,
                      I_update_orig_repl_qty,
                      I_skip_alloc_round) = FALSE then
      return 0;
   end if;
   O_covered_inbound := SYS.DIUTIL.BOOL_TO_INT(L_covered_inbound);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ORDER_INTERFACE_WRP',
                                            TO_CHAR(SQLCODE));
      return 0;
END ORDER_INTERFACE_WRP;
-------------------------------------------------------------------------------
FUNCTION ORDER_INTERFACE(O_error_message        OUT VARCHAR2,
                         O_covered_inbound      OUT BOOLEAN,
                         I_order_no             IN  ordhead.order_no%TYPE,
                         I_supplier             IN  ordhead.supplier%TYPE,
                         I_total_ord_qty_table  IN  rounding_sql.total_ord_qty_tabletype,
                         I_update_orig_repl_qty IN  VARCHAR2 DEFAULT 'N',
                         I_skip_alloc_round     IN  VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   /*
    * This function will serve as an interface with the enhanced rounding
    * logic used for multi-channel environments. It will also round an order
    * in a non-multi-channel environment. It prepares a data structure of line items
    * to be passed to the ROUND_LINE_ITEM function where the enhanced logic lies. It
    * accepts the data structure from the ROUND_LINE_ITEM function and updates the
    * ordering tables based upon the newly rounded line items contained within the
    * data structure.
    */

   i                     NUMBER         := 0; -- Loop Index.

   L_orig_ind            ordhead.orig_ind%TYPE;
   L_orig_approval_date  ordhead.orig_approval_date%TYPE;
   L_default_cancel_code code_detail.code%TYPE;

   -- Define a table of records to store the returned line items.
   L_line_item_table     line_item_tabletype;

   -- Define a cursor that will group line items by virtual locations.
   cursor C_GET_LINE_ITEMS is
      select os.origin_country_id,
             ol.item,
             ol.location,
             w.rounding_seq,
             w.physical_wh,
             ol.loc_type,
             ol.qty_ordered,
             ol.last_rounded_qty,
             ol.last_grp_rounded_qty,
             ol.qty_received,
             ol.qty_cancelled,
             ol.cancel_code,
             os.supp_pack_size,
             isc.ti,
             isc.hi,
             iscl.round_lvl,
             iscl.round_to_pallet_pct,
             iscl.round_to_layer_pct,
             iscl.round_to_case_pct,
             ol.tsf_po_link_no,
             ol.rowid
        from ordloc ol,
             ordsku os,
             wh w,
             item_supp_country isc,
             item_supp_country_loc iscl,
             item_master im
       where ol.location           = w.wh (+)
         and ol.order_no           = os.order_no
         and im.item               = ol.item
         and ol.item               = os.item
         and os.item               = isc.item
         and isc.supplier          = I_supplier
         and os.origin_country_id  = isc.origin_country_id
         and isc.item              = iscl.item
         and isc.supplier          = iscl.supplier
         and isc.origin_country_id = iscl.origin_country_id
         and ol.location           = iscl.loc
         and ol.order_no           = I_order_no
         and exists (select 'Y'
                       from uom_class uc
                      where uc.uom = im.standard_uom
                        and uc.uom_class = 'QTY')
    order by ol.item, w.physical_wh, w.rounding_seq, ol.location;

   cursor C_GET_HEADER_INFO is
      select orig_ind,
             orig_approval_date
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_DEFAULT_CANCEL_CODE is
      select code
        from code_detail
       where code_type = 'ORCA'
         and code_seq = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_HEADER_INFO','ORDHEAD', 'order_no: '||to_char(I_order_no));
   open C_GET_HEADER_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_HEADER_INFO','ORDHEAD', 'order_no: '||to_char(I_order_no));
   fetch C_GET_HEADER_INFO into L_orig_ind,
                                L_orig_approval_date;
   SQL_LIB.SET_MARK('CLOSE','C_GET_HEADER_INFO','ORDHEAD', 'order_no: '||to_char(I_order_no));
   close C_GET_HEADER_INFO;

   SQL_LIB.SET_MARK('OPEN','C_GET_DEFAULT_CANCEL_CODE','CODEDETAIL', NULL);
   open C_GET_DEFAULT_CANCEL_CODE;
   SQL_LIB.SET_MARK('FETCH','C_GET_DEFAULT_CANCEL_CODE','CODEDETAIL', NULL);
   fetch C_GET_DEFAULT_CANCEL_CODE into L_default_cancel_code;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DEFAULT_CANCEL_CODE','CODEDETAIL', NULL);
   close C_GET_DEFAULT_CANCEL_CODE;

   SQL_LIB.SET_MARK('LOOP',
                    'C_GET_LINE_ITEMS',
                    'ordloc, ordsku, wh, item_supp_country, item_supp_country_loc',
                    'order_no = '||to_char(I_order_no));
   FOR line_item_rec in C_GET_LINE_ITEMS LOOP
      i := i + 1;

      -- Populate the current record in the table with the queried line item.
      L_line_item_table(i).orig_cntry           := line_item_rec.origin_country_id;
      L_line_item_table(i).item                 := line_item_rec.item;
      L_line_item_table(i).rounding_seq         := line_item_rec.rounding_seq;
      L_line_item_table(i).physical_wh          := line_item_rec.physical_wh;
      L_line_item_table(i).location             := line_item_rec.location;
      L_line_item_table(i).loc_type             := line_item_rec.loc_type;
      L_line_item_table(i).qty_ordered          := line_item_rec.qty_ordered;
      L_line_item_table(i).last_rounded_qty     := line_item_rec.last_rounded_qty;
      L_line_item_table(i).last_grp_rounded_qty := line_item_rec.last_grp_rounded_qty;
      L_line_item_table(i).supp_pack_size       := line_item_rec.supp_pack_size;
      L_line_item_table(i).ti                   := line_item_rec.ti;
      L_line_item_table(i).hi                   := line_item_rec.hi;
      L_line_item_table(i).round_lvl            := line_item_rec.round_lvl;
      L_line_item_table(i).round_to_pallet_pct  := line_item_rec.round_to_pallet_pct;
      L_line_item_table(i).round_to_layer_pct   := line_item_rec.round_to_layer_pct;
      L_line_item_table(i).round_to_case_pct    := line_item_rec.round_to_case_pct;
      L_line_item_table(i).line_item_rowid      := line_item_rec.rowid;

      L_line_item_table(i).supplier             := I_supplier;

      L_line_item_table(i).changed_ind          := 'N';
      L_line_item_table(i).xdock_wh_ind         := 'N';

      if nvl(line_item_rec.qty_received, 0) > 0 then
         L_line_item_table(i).qty_received := line_item_rec.qty_received;
         L_line_item_table(i).receipts_ind := 'Y';
      else
         L_line_item_table(i).receipts_ind := 'N';
      end if;

      -- Check if the line item contains a po-linked transfer qty.
      if line_item_rec.tsf_po_link_no is NOT NULL then
         L_line_item_table(i).tsf_linked_ind := 'Y';
      else
         L_line_item_table(i).tsf_linked_ind := 'N';
      end if;

      -- Cancelled qty's only need maintenance if the order has been approved in the past.
      if L_orig_approval_date is not NULL then
         L_line_item_table(i).cancel_code     := nvl(line_item_rec.cancel_code, L_default_cancel_code);
         L_line_item_table(i).qty_cancelled   := line_item_rec.qty_cancelled;
      end if;
   END LOOP;

   if L_orig_approval_date is not NULL then
      if CALC_TOTAL_ORDER_QTYS(O_error_message,
                               L_line_item_table,
                               I_total_ord_qty_table) = FALSE then
         return FALSE;
      end if;
   end if;

   -- An order may have cross-dock locations. If so, they need to be rounded
   -- to the store order multiple prior to rounding the wh line item. This is
   -- to ensure that there is enough quantity on the line item to cover the allocations
   -- which may increase due to rounding.

   if ROUND_ALLOCATIONS(O_error_message,
                        L_line_item_table,
                        I_order_no,
                        I_skip_alloc_round) = FALSE then
      return FALSE;
   end if;

   -- Round the order.
   if ROUND_LINE_ITEMS(O_error_message,
                       O_covered_inbound,
                       L_line_item_table,
                       I_order_no,
                       L_orig_approval_date) = FALSE then
      return FALSE;
   end if;

   if L_orig_approval_date is not NULL then
      if UPDATE_CANCEL_QTYS(O_error_message,
                            L_line_item_table,
                            I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Update the order with the rounded quantities.
   if UPDATE_ORDLOC(O_error_message,
                    L_line_item_table,
                    I_order_no,
                    I_update_orig_repl_qty) = FALSE then
      return FALSE;
   end if;
   
   -- Only XDOCK replenishment orders having excess qty then allocated qty, allocations should be prorated.
   -- 0 = current repl generated and 1 = past repl generated.
   if L_orig_ind = 0 or L_orig_ind = 1 then
      if PROCESS_XDOCK_ORDERS(O_error_message,
                              L_line_item_table,
                              I_order_no,
                              I_skip_alloc_round) = FALSE then
         
      RETURN FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ORDER_INTERFACE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ORDER_INTERFACE;
-------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_ORDER_QTYS(O_error_message       OUT    VARCHAR2,
                               I_line_item_table     IN OUT NOCOPY line_item_tabletype,
                               I_total_ord_qty_table IN     rounding_sql.total_ord_qty_tabletype)
RETURN BOOLEAN IS

   /*
    * This function calculates the total order qty including cancelled qtys.
    * After rounding, this qty will be used to adjust the cancel qty to reflect
    * rounding (total qty - new order qty = new cancel qty).
    */

   j                  INT; -- loop index
   L_itemloc_notfound BOOLEAN;

BEGIN

   /*
    * During an online session, cancellations will be deducted from the total order qty at the beginning of the
    * session. Since this module can be called multiple times during a session, it's neccessary for the calling
    * module to track what the original total order qty was at the beginning of the session. It then passed in a table
    * of records for each line item on the order with each line item's original total order qty. Within the while loop,
    * this function attempts to find the line item matching the current line item within the FOR loop in order to set
    * that line item's total order qty within the I_line_item_table.
    *
    * If an empty total order qty table is passed in, the total order qty will be calculated for each execution of this
    * function using the current order qty and the current cancelled qty.
    */
   FOR i IN 1..I_line_item_table.COUNT LOOP

      if I_total_ord_qty_table.COUNT = 0 then
         I_line_item_table(i).total_order_qty := I_line_item_table(i).qty_ordered + nvl(I_line_item_table(i).qty_cancelled, 0);
      else
         j := 0;
         L_itemloc_notfound := TRUE;

         WHILE (L_itemloc_notfound) and (j < I_total_ord_qty_table.COUNT) LOOP
            j := j + 1;

            if (I_total_ord_qty_table(j).item = I_line_item_table(i).item) and
               (I_total_ord_qty_table(j).location = I_line_item_table(i).location) then

               L_itemloc_notfound := FALSE;
            end if;
         END LOOP;

         if ( NOT L_itemloc_notfound ) then
            I_line_item_table(i).total_order_qty := I_total_ord_qty_table(j).total_order_qty;
         end if;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.CALC_TOTAL_ORDER_QTYS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END CALC_TOTAL_ORDER_QTYS;
-------------------------------------------------------------------------------
FUNCTION UPDATE_CANCEL_QTYS(O_error_message   OUT    VARCHAR2,
                            I_line_item_table IN OUT NOCOPY line_item_tabletype,
                            I_order_no        IN     ordhead.order_no%TYPE)
RETURN BOOLEAN IS

BEGIN
   FOR i IN 1..I_line_item_table.COUNT LOOP
      if (I_line_item_table(i).changed_ind = 'Y' and (NVL(I_line_item_table(i).qty_cancelled,0) != 0)) then

         if (I_line_item_table(i).total_order_qty - I_line_item_table(i).qty_ordered) <= 0 then
            I_line_item_table(i).qty_cancelled := NULL;
            I_line_item_table(i).cancel_code   := NULL;
            I_line_item_table(i).cancel_date   := NULL;
            I_line_item_table(i).cancel_id     := NULL;
         else
            I_line_item_table(i).qty_cancelled := (I_line_item_table(i).total_order_qty - I_line_item_table(i).qty_ordered);
            I_line_item_table(i).cancel_date   := GET_VDATE;
            I_line_item_table(i).cancel_id     := GET_USER;
         end if;

      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORD_INV_MGMT_SQL.UPDATE_CANCEL_QTYS',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_CANCEL_QTYS;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_line_item_table        IN OUT   LINE_ITEM_TABLETYPE,
                       I_order_no               IN       ORDLOC.ORDER_NO%TYPE,
                       I_update_orig_repl_qty   IN       VARCHAR2)
RETURN BOOLEAN IS

   /*
    * This private function updates the ordloc table with the rounded qty
    * if an update is required
    */

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where order_no = I_order_no
       for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ORDLOC',
                    'ORDLOC',
                    'order_no: '||to_char(I_order_no));
   open C_LOCK_ORDLOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ORDLOC',
                    'ORDLOC',
                    'order_no: '||to_char(I_order_no));
   close C_LOCK_ORDLOC;

   FOR i in 1..I_line_item_table.COUNT LOOP

      if NVL(I_line_item_table(i).qty_cancelled,0)= 0 then
             I_line_item_table(i).cancel_code := NULL;
             I_line_item_table(i).cancel_date := NULL;
             I_line_item_table(i).cancel_id := NULL;
      end if;
      -- There is no need to round if the all the items have been cancelled from the order
      -- (ORDLOC.QTY_CANCELLED > 0 and ORDLOC.QTY_ORDERED = 0)
      -- Also, update ORDLOC where the updated_ind = 'Y'
      if I_line_item_table(i).qty_ordered > 0 and I_line_item_table(i).changed_ind = 'Y' then

         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ORDLOC',
                          'order_no: '||to_char(I_order_no));
         update ordloc
            set qty_ordered          = I_line_item_table(i).qty_ordered,
                original_repl_qty    = DECODE(I_update_orig_repl_qty, 'Y', I_line_item_table(i).qty_ordered,
                                                                           original_repl_qty),
                last_rounded_qty     = I_line_item_table(i).qty_ordered,
                last_grp_rounded_qty = I_line_item_table(i).last_grp_rounded_qty,
                qty_cancelled        = I_line_item_table(i).qty_cancelled,
                cancel_code          = I_line_item_table(i).cancel_code,
                cancel_id            = I_line_item_table(i).cancel_id,
                cancel_date          = I_line_item_table(i).cancel_date
          where rowid = I_line_item_table(i).line_item_rowid;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ORDLOC',
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.UPDATE_ORDLOC',
                                            to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_ORDLOC;
---------------------------------------------------------------------------------------
FUNCTION BUYER_WRKSHT_INTERFACE(O_error_message    OUT VARCHAR2,
                                I_audsid           IN  repl_results.audsid%TYPE)
RETURN BOOLEAN IS

   /*
    * This function will serve as an interface with the enhanced rounding
    * logic used for multi-channel environments. It will also round line items
    * in a non-multi-channel environment. It prepares a data structure of line items
    * to be passed to the ROUND_LINE_ITEM function where the enhanced logic lies. It
    * accepts the data structure from the ROUND_LINE_ITEM function and updates the
    * line items' tables based upon the newly rounded line items contained within the
    * data structure.
    */

   i number := 0; -- Loop Index

   -- Define a table of records to store the returned line items.
   L_line_item_table  line_item_tabletype;

   -- Define a unioned cursor to return line items from the three
   -- line items' tables and group virtual locs.
   cursor C_GET_MULTICHANNEL_LINE_ITEMS is
      select m.supplier,
             m.origin_country_id,
             m.item,
             m.location,
             w.rounding_seq,
             m.loc_type,
             'M' source_type,
             m.order_roq,
             m.last_rounded_qty,
             m.last_grp_rounded_qty,
             m.case_size,
             m.ti,
             m.hi,
             iscl.round_lvl,
             iscl.round_to_pallet_pct,
             iscl.round_to_layer_pct,
             iscl.round_to_case_pct,
             -999 tsf_po_link_no,
             m.rowid
        from buyer_wksht_manual m,
             wh w,
             item_supp_country_loc iscl
       where m.location             = w.wh (+)
         and iscl.item              = m.item
         and iscl.supplier          = m.supplier
         and iscl.origin_country_id = m.origin_country_id
         and iscl.loc               = m.location
         and m.audsid               = I_audsid
         and m.status               = 'W'
         and m.order_roq > 0
    UNION ALL
      select i.supplier,
             i.origin_country_id,
             i.item,
             i.location,
             w.rounding_seq,
             i.loc_type,
             'I' source_type,
             i.order_roq,
             i.last_rounded_qty,
             i.last_grp_rounded_qty,
             i.case_size,
             i.ti,
             i.hi,
             iscl.round_lvl,
             iscl.round_to_pallet_pct,
             iscl.round_to_layer_pct,
             iscl.round_to_case_pct,
             -999 tsf_po_link_no,
             i.rowid
        from ib_results i,
             wh w,
             item_supp_country_loc iscl
       where i.location             = w.wh (+)
         and iscl.item              = i.item
         and iscl.supplier          = i.supplier
         and iscl.origin_country_id = i.origin_country_id
         and iscl.loc               = i.location
         and i.audsid               = I_audsid
         and i.status               = 'W'
         and i.order_roq > 0
    UNION ALL
      select r.primary_repl_supplier supplier,
             r.origin_country_id,
             r.item,
             r.location,
             w.rounding_seq,
             r.loc_type,
             'R' source_type,
             r.order_roq,
             r.last_rounded_qty,
             r.last_grp_rounded_qty,
             r.case_size,
             r.ti,
             r.hi,
             iscl.round_lvl,
             iscl.round_to_pallet_pct,
             iscl.round_to_layer_pct,
             iscl.round_to_case_pct,
             r.tsf_po_link_no,
             r.rowid
        from repl_results r,
             wh w,
             item_supp_country_loc iscl
       where r.location             = w.wh (+)
         and iscl.item              = r.item
         and iscl.supplier          = r.primary_repl_supplier
         and iscl.origin_country_id = r.origin_country_id
         and iscl.loc               = r.location
         and r.audsid               = I_audsid
         and r.status               = 'W'
         and r.order_roq > 0
    order by 3, -- item
             1, -- supplier
             2, -- origin country
             5, -- rounding_seq
             4; -- location

BEGIN

   FOR line_item_rec in C_GET_MULTICHANNEL_LINE_ITEMS LOOP
      i := i + 1;

      -- Populate the current record in the table with the queried line item.
      L_line_item_table(i).orig_cntry           := line_item_rec.origin_country_id;
      L_line_item_table(i).item                 := line_item_rec.item;
      L_line_item_table(i).rounding_seq         := line_item_rec.rounding_seq;
      L_line_item_table(i).location             := line_item_rec.location;
      L_line_item_table(i).loc_type             := line_item_rec.loc_type;
      L_line_item_table(i).qty_ordered          := line_item_rec.order_roq;
      L_line_item_table(i).last_rounded_qty     := line_item_rec.last_rounded_qty;
      L_line_item_table(i).last_grp_rounded_qty := line_item_rec.last_grp_rounded_qty;
      L_line_item_table(i).supplier             := line_item_rec.supplier;
      L_line_item_table(i).source_type          := line_item_rec.source_type;
      L_line_item_table(i).supp_pack_size       := line_item_rec.case_size;
      L_line_item_table(i).ti                   := line_item_rec.ti;
      L_line_item_table(i).hi                   := line_item_rec.hi;
      L_line_item_table(i).round_lvl            := line_item_rec.round_lvl;
      L_line_item_table(i).round_to_pallet_pct  := line_item_rec.round_to_pallet_pct;
      L_line_item_table(i).round_to_layer_pct   := line_item_rec.round_to_layer_pct;
      L_line_item_table(i).round_to_case_pct    := line_item_rec.round_to_case_pct;
      L_line_item_table(i).line_item_rowid      := line_item_rec.rowid;

      -- Check if the line item contains a po-linked transfer qty.
      if (line_item_rec.tsf_po_link_no = -999
          or line_item_rec.tsf_po_link_no is NULL) then
         L_line_item_table(i).tsf_linked_ind := 'N';
      else
         L_line_item_table(i).tsf_linked_ind := 'Y';
      end if;

      L_line_item_table(i).changed_ind         := 'N';
      L_line_item_table(i).xdock_wh_ind        := 'N';
      L_line_item_table(i).receipts_ind        := 'N';
   END LOOP;

   -- Round the line items.
   if ROUND_LINE_ITEMS(O_error_message,
                       L_line_item_table) = FALSE then
      return FALSE;
   end if;

   -- Update the various line items tables
   if UPDATE_BYR_WRSKT_LINE_ITEMS(O_error_message,
                                  L_line_item_table,
                                  I_audsid) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.BUYER_WRKSHT_INTERFACE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END BUYER_WRKSHT_INTERFACE;
-------------------------------------------------------------------------------
FUNCTION UPDATE_BYR_WRSKT_LINE_ITEMS(O_error_message   OUT VARCHAR2,
                                     I_line_item_table IN  line_item_tabletype,
                                     I_audsid          IN  repl_results.audsid%TYPE)
RETURN BOOLEAN IS

   /*
    * This private function updates the appropriate line item table
    * with rounded order qtys where an update is required.
    */

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(50);

   cursor C_LOCK_REPL_RESULTS is
      select 'x'
        from repl_results
       where audsid = I_audsid
         for update nowait;

   cursor C_LOCK_IB_RESULTS is
      select 'x'
        from ib_results
       where audsid = I_audsid
         for update nowait;

   cursor C_LOCK_BUYER_WKSHT_MANUAL is
      select 'x'
        from buyer_wksht_manual
       where audsid = I_audsid
         for update nowait;

BEGIN
   L_table := 'repl_results';
   open C_LOCK_REPL_RESULTS;
   close C_LOCK_REPL_RESULTS;

   L_table := 'ib_results';
   open C_LOCK_IB_RESULTS;
   close C_LOCK_IB_RESULTS;

   L_table := 'buyer_wksht_manual';
   open C_LOCK_BUYER_WKSHT_MANUAL;
   close C_LOCK_BUYER_WKSHT_MANUAL;

   FOR i in 1..I_line_item_table.COUNT LOOP

      -- update line item tables where the updated_ind = 'Y'
      if I_line_item_table(i).changed_ind = 'Y' then

         if I_line_item_table(i).source_type = 'R' then

            update repl_results
               set order_roq            = I_line_item_table(i).qty_ordered,
                   last_rounded_qty     = I_line_item_table(i).qty_ordered,
                   last_grp_rounded_qty = I_line_item_table(i).last_grp_rounded_qty
             where rowid = I_line_item_table(i).line_item_rowid;

         elsif I_line_item_table(i).source_type = 'I' then

            update ib_results
               set order_roq            = I_line_item_table(i).qty_ordered,
                   last_rounded_qty     = I_line_item_table(i).qty_ordered,
                   last_grp_rounded_qty = I_line_item_table(i).last_grp_rounded_qty
             where rowid = I_line_item_table(i).line_item_rowid;

         elsif I_line_item_table(i).source_type = 'M' then

            update buyer_wksht_manual
               set order_roq            = I_line_item_table(i).qty_ordered,
                   last_rounded_qty     = I_line_item_table(i).qty_ordered,
                   last_grp_rounded_qty = I_line_item_table(i).last_grp_rounded_qty
             where rowid = I_line_item_table(i).line_item_rowid;

         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.UPDATE_BYR_WRSKT_LINE_ITEMS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_BYR_WRSKT_LINE_ITEMS;
-------------------------------------------------------------------------------
FUNCTION ORDER_MATRIX_INTERFACE(O_error_message       OUT VARCHAR2,
                                I_order_no            IN  ordhead.order_no%TYPE,
                                I_supplier            IN  ordhead.supplier%TYPE)
RETURN BOOLEAN IS

   /*
    * This function will serve as an interface with the enhanced rounding
    * logic used for multi-channel environments. It will also round an order
    * in a non-multi-channel environment. It prepares a data structure of line items
    * to be passed to the ROUND_LINE_ITEM function where the enhanced logic lies. It
    * accepts the data structure from the ROUND_LINE_ITEM function and updates the
    * ordering tables based upon the newly rounded line items contained within the
    * data structure.
    */

   i                  NUMBER         := 0; -- Loop Index.

   -- Define a table of records to store the returned line items.
   L_line_item_table  line_item_tabletype;

   -- Define a cursor that will group line items by virtual locations.
   cursor C_GET_LINE_ITEMS is
      select o.origin_country_id,
             o.item,
             o.location,
             w.rounding_seq,
             o.loc_type,
             o.calc_qty,
             o.last_rounded_qty,
             o.last_grp_rounded_qty,
             o.supp_pack_size,
             o.uop,
             o.standard_uom,
             isc.ti,
             isc.hi,
             isc.round_lvl,
             isc.round_to_pallet_pct,
             isc.round_to_layer_pct,
             isc.round_to_case_pct,
             o.rowid
        from ordloc_wksht o,
             wh w,
             item_supp_country isc
       where o.location            = w.wh (+)
         and o.item                = isc.item
         and isc.supplier          = I_supplier
         and o.origin_country_id   = isc.origin_country_id
         and o.order_no            = I_order_no
         and o.location is not NULL
    order by o.item, w.rounding_seq, o.location;

    cursor C_GET_LOC_ROUNDING_INFO(C_item              item_supp_country_loc.item%TYPE,
                                   C_origin_country_id item_supp_country_loc.origin_country_id%TYPE,
                                   C_loc               item_supp_country_loc.loc%TYPE) is
      select round_lvl,
             round_to_pallet_pct,
             round_to_layer_pct,
             round_to_case_pct
        from item_supp_country_loc
       where item              = C_item
         and supplier          = I_supplier
         and origin_country_id = C_origin_country_id
         and loc               = C_loc;

BEGIN

   FOR line_item_rec in C_GET_LINE_ITEMS LOOP
      i := i + 1;

      -- Populate the current record in the table with the queried line item.
      L_line_item_table(i).orig_cntry           := line_item_rec.origin_country_id;
      L_line_item_table(i).item                 := line_item_rec.item;
      L_line_item_table(i).rounding_seq         := line_item_rec.rounding_seq;
      L_line_item_table(i).location             := line_item_rec.location;
      L_line_item_table(i).loc_type             := line_item_rec.loc_type;
      L_line_item_table(i).init_qty             := line_item_rec.calc_qty;
      L_line_item_table(i).qty_ordered          := line_item_rec.calc_qty;
      L_line_item_table(i).last_rounded_qty     := line_item_rec.last_rounded_qty;
      L_line_item_table(i).last_grp_rounded_qty := line_item_rec.last_grp_rounded_qty;
      L_line_item_table(i).supp_pack_size       := line_item_rec.supp_pack_size;
      L_line_item_table(i).uop                  := line_item_rec.uop;
      L_line_item_table(i).standard_uom         := line_item_rec.standard_uom;
      L_line_item_table(i).ti                   := line_item_rec.ti;
      L_line_item_table(i).hi                   := line_item_rec.hi;
      L_line_item_table(i).line_item_rowid      := line_item_rec.rowid;

      L_line_item_table(i).supplier             := I_supplier;

      L_line_item_table(i).tsf_linked_ind       := 'N';
      L_line_item_table(i).changed_ind          := 'N';
      L_line_item_table(i).xdock_wh_ind         := 'N';
      L_line_item_table(i).receipts_ind         := 'N';

      /* Within the order matrix, when orders are being created, an item/location relationship
       * is not required prior to ordering product to a location. The item/location relationship
       * on item_loc is created on the fly upon order creation. Therefore, when rouning line items
       * from ordloc wksht, rounding info may not exist at the location level. The below cursor attempts
       * to retrieve needed rounding info from item_supp_country_loc. If it does not exist, the info is defaulted
       * from the item_supp_country level.
       */
      open C_GET_LOC_ROUNDING_INFO(L_line_item_table(i).item,
                                   L_line_item_table(i).orig_cntry,
                                   L_line_item_table(i).location);

      fetch C_GET_LOC_ROUNDING_INFO into L_line_item_table(i).round_lvl,
                                         L_line_item_table(i).round_to_pallet_pct,
                                         L_line_item_table(i).round_to_layer_pct,
                                         L_line_item_table(i).round_to_case_pct;

      if C_GET_LOC_ROUNDING_INFO%NOTFOUND then
         L_line_item_table(i).round_lvl           := line_item_rec.round_lvl;
         L_line_item_table(i).round_to_pallet_pct := line_item_rec.round_to_pallet_pct;
         L_line_item_table(i).round_to_layer_pct  := line_item_rec.round_to_layer_pct;
         L_line_item_table(i).round_to_case_pct   := line_item_rec.round_to_case_pct;
      end if;

      close C_GET_LOC_ROUNDING_INFO;

   END LOOP;

   -- Round the order.
   if ROUND_LINE_ITEMS(O_error_message,
                       L_line_item_table) = FALSE then
      return FALSE;
   end if;

   -- Update the order with the rounded quantities.
   if UPDATE_ORDER_MATRIX_QTYS(O_error_message,
                               I_order_no,
                               L_line_item_table) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ORDER_MATRIX_INTERFACE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ORDER_MATRIX_INTERFACE;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_MATRIX_QTYS(O_error_message       OUT VARCHAR2,
                                  I_order_no            IN  ordloc.order_no%TYPE,
                                  I_line_item_table     IN  line_item_tabletype)
RETURN BOOLEAN IS

   /*
    * This private function updates the ordloc_wksht table with the rounded qty
    * if an update is required
    */
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_wksht_qty      ordloc_wksht.wksht_qty%TYPE;
   L_variance       ordloc_wksht.variance_qty%TYPE;

   cursor C_LOCK_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         for update nowait;

BEGIN
   open C_LOCK_ORDLOC_WKSHT;
   close C_LOCK_ORDLOC_WKSHT;

   FOR i in 1..I_line_item_table.COUNT LOOP

      -- update ordloc+wksht where the updated_ind = 'Y'
      if I_line_item_table(i).changed_ind = 'Y' then

         -- The worksheet qty is expressed in unit of purchase. The qty ordered is expressed in
         -- standard uom. If the UOP is the same as the SUOM, then no conversion is necessary.
         -- However, if they are different, only 'cases' may be allowed as an alternative UOP .
         -- Therefore the wksht qty must be converted into cases prior to updating.
       /*
        *if I_line_item_table(i).uop != I_line_item_table(i).standard_uom then
        */
     if I_line_item_table(i).uop != I_line_item_table(i).standard_uom
      and NVL(I_line_item_table(i).qty_ordered,0) > 0 then
            L_wksht_qty := (I_line_item_table(i).qty_ordered/I_line_item_table(i).supp_pack_size);
         else
            L_wksht_qty := I_line_item_table(i).qty_ordered;
         end if;

         -- Calculcate the variance between the qty prior to rounding and the actual rounded qty.
         if NVL((I_line_item_table(i).qty_ordered - I_line_item_table(i).init_qty),0) > 0 then
            L_variance := ((I_line_item_table(i).qty_ordered - I_line_item_table(i).init_qty)/I_line_item_table(i).init_qty) * 100;
         end if;

         update ordloc_wksht
            set act_qty              = I_line_item_table(i).qty_ordered,
                wksht_qty            = L_wksht_qty,
                variance_qty         = L_variance,
                last_rounded_qty     = I_line_item_table(i).qty_ordered,
                last_grp_rounded_qty = I_line_item_table(i).last_grp_rounded_qty
          where rowid = I_line_item_table(i).line_item_rowid;

      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ordloc_wksht',
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.UPDATE_ORDER_MATRIX_QTYS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_ORDER_MATRIX_QTYS;
-------------------------------------------------------------------------------
FUNCTION ROUND_ALLOCATIONS(O_error_message       OUT    VARCHAR2,
                           I_line_item_table     IN OUT NOCOPY line_item_tabletype,
                           I_order_no            IN     ordhead.order_no%TYPE,
                           I_skip_alloc_round    IN     VARCHAR2)
RETURN BOOLEAN IS

   /* This package will round any allocation qty's for stores associated with an xdock wh.
    * It will also adjust the ordloc qty for the wh if the total rounded allocated qty is
    * greater than the qty ordered to the wh.
    */

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_total_qty_allocated         NUMBER := 0;
   L_qty_allocated_rounded       NUMBER := 0;

   L_store_pack_size             item_supp_country.supp_pack_size%TYPE;
   L_store_ord_mult              item_master.store_ord_mult%TYPE;
   L_break_pack_ind              wh.break_pack_ind%TYPE;

   cursor C_ALLOC_DETAIL(C_from_wh wh.wh%TYPE,
                         C_item    alloc_header.item%TYPE) is
      select ad.to_loc,
             ad.qty_allocated
        from alloc_header ah,
             alloc_detail ad
       where ah.order_no = I_order_no
         and ah.alloc_no = ad.alloc_no
         and ah.wh       = C_from_wh
         and ah.item     = C_item
         and origin_ind <> 'ALC'
         for update of ad.qty_allocated;

   cursor C_GET_TOTAL_ALLOC_QTY(C_from_wh wh.wh%TYPE,
                                C_item    alloc_header.item%TYPE) is
      select NVL(SUM(ad.qty_allocated), -999)
        from alloc_header ah,
             alloc_detail ad
       where ah.order_no = I_order_no
         and ah.alloc_no = ad.alloc_no
         and ah.wh       = C_from_wh
         and ah.item     = C_item;

BEGIN

   FOR i IN 1..I_line_item_table.COUNT LOOP

      if I_line_item_table(i).loc_type = 'W' then
         L_total_qty_allocated   := 0;

         if I_skip_alloc_round = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_ALLOC_QTY','ALLOC_DETAIL', 'order_no: '||to_char(I_order_no));
            open C_GET_TOTAL_ALLOC_QTY(I_line_item_table(i).location,
                                       I_line_item_table(i).item) ;

            SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_ALLOC_QTY','ALLOC_DETAIL', 'order_no: '||to_char(I_order_no));
            fetch C_GET_TOTAL_ALLOC_QTY into L_total_qty_allocated;
               if L_total_qty_allocated != -999 then
                   -- If there are allocations, then this is a xdock wh.
                   I_line_item_table(i).xdock_wh_ind := 'Y';
               end if;

            SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_ALLOC_QTY','ALLOC_DETAIL', 'order_no: '||to_char(I_order_no));
            close C_GET_TOTAL_ALLOC_QTY;
         else
            SQL_LIB.SET_MARK('LOOP',
                             'C_ALLOC_DETAIL',
                             'alloc_header, alloc_detail',
                             'order_no = '||to_char(I_order_no));

            FOR alloc_rec in C_ALLOC_DETAIL(I_line_item_table(i).location,
                                            I_line_item_table(i).item) LOOP

               -- If there are allocations, then this is a xdock wh.
               I_line_item_table(i).xdock_wh_ind := 'Y';

               -- Round the qty_allocated field on alloc_detail to the inner packsize.
               if ROUNDING_SQL.GET_PACKSIZE(O_error_message,
                                            L_store_pack_size,
                                            L_store_ord_mult,
                                            L_break_pack_ind,
                                            I_line_item_table(i).item,
                                            I_line_item_table(i).location,
                                            I_line_item_table(i).supplier,
                                            I_line_item_table(i).orig_cntry,
                                            alloc_rec.to_loc) = FALSE then
                  return FALSE;
               end if;

               if ROUNDING_SQL.TO_INNER_CASE(O_error_message,
                                             L_qty_allocated_rounded,
                                             I_line_item_table(i).item,
                                             I_line_item_table(i).supplier,
                                             I_line_item_table(i).orig_cntry,
                                             alloc_rec.to_loc,
                                             alloc_rec.qty_allocated,
                                             I_line_item_table(i).supp_pack_size,
                                             L_store_pack_size,
                                             L_store_ord_mult,
                                             L_break_pack_ind) = FALSE then
                  return FALSE;
               end if;

            -- There is no need to round if the item and xdock allocation has been cancelled
            -- (ALLOC_DETAIL.QTY_CANCELLED > 0 and ALLOC_DETAIL.QTY_ALLOCATED = 0)
               if (alloc_rec.qty_allocated > 0) AND (alloc_rec.qty_allocated != L_qty_allocated_rounded) then

                  SQL_LIB.SET_MARK('UPDATE',NULL,'alloc_detail',NULL);
                  update alloc_detail
                     set qty_allocated = L_qty_allocated_rounded
                   where current of C_ALLOC_DETAIL;

                  L_total_qty_allocated   := L_total_qty_allocated + L_qty_allocated_rounded;

               end if;
            END LOOP;
         end if;

         -- Store the total allocation qty for the warehouse.
         I_line_item_table(i).total_alloc_qty := L_total_qty_allocated;

         -- If the rounding of values in alloc_detail has rounded the total qty_allocated
         -- above the qty_ordered for the current wh, then we need to set the
         -- qty_ordered record equal to the total quantity allocated for that
         -- location, otherwise, use the qty_ordered does not change.  (same logic for qty_prescaled)

         if L_total_qty_allocated > I_line_item_table(i).qty_ordered then
            I_line_item_table(i).qty_ordered := L_total_qty_allocated;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'alloc_detail',
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ROUND_ALLOCATIONS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ROUND_ALLOCATIONS;
-------------------------------------------------------------------------------
FUNCTION ROUND_LINE_ITEMS(O_error_message        OUT    VARCHAR2,
                          I_line_item_table      IN OUT NOCOPY line_item_tabletype,
                          I_order_no             IN     ordhead.order_no%TYPE DEFAULT NULL,
                          I_orig_approval_date   IN     ordhead.orig_approval_date%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_covered_inbound BOOLEAN;

BEGIN

   if ROUND_LINE_ITEMS(O_error_message,
                       L_covered_inbound,
                       I_line_item_table,
                       I_order_no,
                       I_orig_approval_date) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ROUND_LINE_ITEMS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ROUND_LINE_ITEMS;
-------------------------------------------------------------------------------
FUNCTION ROUND_LINE_ITEMS(O_error_message           OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_covered_inbound         OUT   BOOLEAN,
                          I_line_item_table      IN OUT   NOCOPY LINE_ITEM_TABLETYPE,
                          I_order_no             IN       ORDHEAD.ORDER_NO%TYPE DEFAULT NULL,
                          I_orig_approval_date   IN       ORDHEAD.ORIG_APPROVAL_DATE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   /*
    * This function will be a private function. It will be interfaced by one of several
    * interface programs in this package. It accepts a table of line item records. If
    * multi-channel is off, this function will simply loop through the table, round
    * each line item, and update the table. If it is a multi-channel environment, this
    * function will need to group virtual warehouses by rounding group. It will round
    * the 'combined' line item.
    */

   L_grouped_qty           NUMBER      := 0;
   L_rounded_qty           NUMBER      := 0;
   L_phys_rounded_qty      NUMBER      := 0;
   L_outstanding_qty       NUMBER      := 0;
   L_lowest_loc_rec        INT         := NULL;
   L_round_leader          INT         := NULL;
   L_last_line_item        INT         := 0;
   L_total_qty_allocated   ALLOC_DETAIL.QTY_ALLOCATED%TYPE;

   L_override_fail_ind     VARCHAR2(1) := 'N';

   L_standard_class        uom_class.uom_class%TYPE;

   -- This variable stores whether VALIDATE_PHYSICAL_QTYS function
   -- adjusted an order qty up to cover inbound qty's.
   L_covered_inbound       BOOLEAN     := FALSE;

BEGIN
   -- This parameter indicates whether rounding adjusted an order qty up
   -- in order to cover inbound qty's. It's initialized to FALSE.
   O_covered_inbound := FALSE;

   FOR i in 1..I_line_item_table.COUNT LOOP

      /*
       * If this is not the first time through the loop and the item, supplier, department,
       * origin country, or rounding group change, then a new group has a occurred and
       * the previous rounding group must be proccessed. Rounding group is sufficient to
       * group virtuals within a physical because this value will also be a virtual wh and
       * therefore (because it has a record for itself on the wh table) will be unique across
       * physicals.
       *
       * Stores must be processed individually. Since stores have no rounding sequence, the NVL within the
       * the rounding sequence comparison will ensure that each store gets processed individually.
       *
       * The rounding will not occur for the first time through the loop because rounding will be performed on
       * sets of virtuals. So, rounding is performed on the previous item/location or item/locations.
       */
      if (i > 1) and
         ((I_line_item_table(i).item != I_line_item_table(i - 1).item)
          or
          (I_line_item_table(i).supplier != I_line_item_table(i - 1).supplier)
          or
          (I_line_item_table(i).orig_cntry != I_line_item_table(i - 1).orig_cntry)
          or
          (nvl(I_line_item_table(i).rounding_seq, -99) != nvl(I_line_item_table(i - 1).rounding_seq, -999) and
           I_line_item_table(i).location != I_line_item_table(i - 1).location)) then

            FOR j in L_lowest_loc_rec..(i-1) LOOP
               I_line_item_table(j).changed_ind := 'Y';
            END LOOP;

            -- Zeroed out qty's will not be rounded. They are assumed to be cancellations by the user.
            if L_grouped_qty != 0 and L_outstanding_qty != 0 and NVL((I_line_item_table(NVL(L_round_leader, L_lowest_loc_rec)).cancel_code),-99) != 'V'then
               -- Round the last group of locations.
               if ROUND_BY_LEVEL(O_error_message,
                                 L_rounded_qty,
                                 L_override_fail_ind,
                                 L_grouped_qty,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).item,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supplier,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).orig_cntry,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).location,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_lvl,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_case_pct,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_layer_pct,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_pallet_pct,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).ti,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).hi,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                                 'N'/*Must round up ind*/) = FALSE then
                  return FALSE;
               end if;

               if ALLOC_ATTRIB_SQL.GET_TOTAL_ALLOC_QTY(O_error_message,
                                                       L_total_qty_allocated,
                                                       I_order_no,
                                                       I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).location,
                                                       I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).item) = FALSE then
                  return FALSE;
               end if;
               if L_total_qty_allocated is NOT NULL and L_rounded_qty < L_total_qty_allocated then
                  if ROUND_BY_LEVEL(O_error_message,
                                    L_rounded_qty,
                                    L_override_fail_ind,
                                    L_grouped_qty,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).item,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supplier,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).orig_cntry,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).location,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_lvl,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_case_pct,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_layer_pct,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_pallet_pct,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).ti,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).hi,
                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                                    'Y'/*Must round up ind*/) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if L_grouped_qty != L_rounded_qty then
                  if APPLY_ROUND_DIFFERENCE(O_error_message,
                                            I_line_item_table,
                                            L_grouped_qty,
                                            L_rounded_qty,
                                            L_round_leader,
                                            L_lowest_loc_rec,
                                            (i - 1)) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if; -- L_grouped_qty != 0

         -- Keep a running total of the rounded qty for the physical location.
         L_phys_rounded_qty := L_phys_rounded_qty + nvl(L_rounded_qty, 0);

         /*** Perform physical location validation *****/
         if (nvl(I_line_item_table(i).physical_wh, -99) != nvl(I_line_item_table(i-1).physical_wh, -999)) then

            if VALIDATE_PHYSICAL_QTY(O_error_message,
                                     L_covered_inbound,
                                     I_order_no,
                                     I_orig_approval_date,
                                     I_line_item_table,
                                     L_rounded_qty,
                                     L_phys_rounded_qty,
                                     L_round_leader,
                                     L_lowest_loc_rec) = FALSE then
                return FALSE;
             end if;

             if L_covered_inbound then
                O_covered_inbound := TRUE;
             end if;

            -- Reset the rounded and received qty for the physical location.
            L_phys_rounded_qty  := 0;
         end if;

         -- Reset the grouped qty.
         L_grouped_qty := 0;

         -- Reset the lowest location number of the set.
         L_lowest_loc_rec := NULL;

         -- Reset the round leader.
         L_round_leader := NULL;

      end if; -- group has changed, process previous group


      /** process current line item **/

      -- If this is the fisrt iteration of the loop or the item has changed, the
      -- item's standard uom class must be retrieved. If the class is QTY, then the
      -- qty ordered must be a whole number.
      if (i = 1) or
         (I_line_item_table(i).item != I_line_item_table(i - 1).item) then

         if GET_STANDARD_CLASS(O_error_message,
                               L_standard_class,
                               I_line_item_table(i).standard_uom,
                               I_line_item_table(i).item) = FALSE then
            return FALSE;
         end if;
      end if;

      -- If the qty is not a whole number and the standard class is 'QTY',
      -- then the qty ordered needs to be rounded to a whole number using
      -- standard rounding rules (> .5 round up, < .5 round down).
      if (mod(I_line_item_table(i).qty_ordered,1) != 0) and
         (L_standard_class = 'QTY') then

         I_line_item_table(i).qty_ordered := ROUND_TO_INT(I_line_item_table(i).qty_ordered);
         I_line_item_table(i).changed_ind := 'Y';
      end if;

      -- keep a running total of the order quantity for a set of locs.
      -- this running total should be devoid of any receipts that may exist.
      -- Received qtys should not be rounded.
      L_grouped_qty := L_grouped_qty + I_line_item_table(i).qty_ordered;
      L_outstanding_qty := I_line_item_table(i).qty_ordered - nvl(I_line_item_table(i).qty_received,0);
      -- store the index for the lowest numerical location amongst
      -- the current set of locations.
      if L_lowest_loc_rec is NULL then
         L_lowest_loc_rec := i;
      end if;

      -- Capture the round leader if it is apart of the set of virtuals.
      if I_line_item_table(i).location = I_line_item_table(i).rounding_seq then
         L_round_leader := i;
      end if;

      L_last_line_item := i;
   END LOOP;

   /** Process the last set of item/locs from the loop **/

   -- If the loop has been executed at least once.
   if L_last_line_item != 0 then

      FOR j in L_lowest_loc_rec..L_last_line_item LOOP
         I_line_item_table(j).changed_ind := 'Y';
      END LOOP;

         -- Zeroed out qty's will not be rounded. They are assumed to be cancellations by the user.
         if L_grouped_qty != 0 and L_outstanding_qty != 0 and NVL((I_line_item_table(NVL(L_round_leader,L_lowest_loc_rec)).cancel_code),-99)!='V' then
            -- Call round function.
            if ROUND_BY_LEVEL(O_error_message,
                              L_rounded_qty,
                              L_override_fail_ind,
                              L_grouped_qty,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).item,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supplier,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).orig_cntry,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).location,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_lvl,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_case_pct,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_layer_pct,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_pallet_pct,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).ti,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).hi,
                              I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                              'N'/*Must round up ind*/) = FALSE then
               return FALSE;
            end if;
            if ALLOC_ATTRIB_SQL.GET_TOTAL_ALLOC_QTY(O_error_message,
                                                    L_total_qty_allocated,
                                                    I_order_no,
                                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).location,
                                                    I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).item) = FALSE then
               return FALSE;
            end if;

            if L_total_qty_allocated is NOT NULL and L_rounded_qty < L_total_qty_allocated then
               if ROUND_BY_LEVEL(O_error_message,
                                 L_rounded_qty,
                                 L_override_fail_ind,
                                 L_grouped_qty,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).item,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supplier,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).orig_cntry,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).location,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_lvl,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_case_pct,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_layer_pct,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).round_to_pallet_pct,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).ti,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).hi,
                                 I_line_item_table(nvl(L_round_leader,L_lowest_loc_rec)).supp_pack_size,
                                 'Y'/*Must round up ind*/) = FALSE then
                  return FALSE;
               end if;
            end if;

            if L_grouped_qty != L_rounded_qty then
               if APPLY_ROUND_DIFFERENCE(O_error_message,
                                         I_line_item_table,
                                         L_grouped_qty,
                                         L_rounded_qty,
                                         L_round_leader,
                                         L_lowest_loc_rec,
                                         L_last_line_item) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

      -- Update the total rounded qty for the physical location with the last group's qty.
      L_phys_rounded_qty := L_phys_rounded_qty + nvl(L_rounded_qty, 0);

      /*** Perform physical location validation *****/
      if VALIDATE_PHYSICAL_QTY(O_error_message,
                               L_covered_inbound,
                               I_order_no,
                               I_orig_approval_date,
                               I_line_item_table,
                               L_rounded_qty,
                               L_phys_rounded_qty,
                               L_round_leader,
                               L_lowest_loc_rec) = FALSE then
         return FALSE;
      end if;

      if L_covered_inbound then
         O_covered_inbound := TRUE;
      end if;

   end if;

   -- Calculate the last group rounded qtys.
   if CALC_ROUNDED_GROUP_QTYS(O_error_message,
                              I_line_item_table) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ROUND_LINE_ITEMS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ROUND_LINE_ITEMS;
-------------------------------------------------------------------------------
FUNCTION GET_STANDARD_CLASS(O_error_message  OUT VARCHAR2,
                            O_standard_class OUT uom_class.uom_class%TYPE,
                            I_standard_uom   IN  item_master.standard_uom%TYPE,
                            I_item           IN  item_master.item%TYPE)
RETURN BOOLEAN IS

   L_standard_uom item_master.standard_uom%TYPE := I_standard_uom;
   L_conv_factor  NUMBER;

BEGIN

   -- If the standard uom is passed, only the class needs to be retrieved
   -- reducing the number of tables to be hit.
   if L_standard_uom is NULL then
      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_standard_uom,
                                          O_standard_class,
                                          L_conv_factor,
                                          I_item,
                                          'Y' /* Get Class Ind */) = FALSE then
         return FALSE;
      end if;
   else
      if UOM_SQL.GET_CLASS(O_error_message,
                           O_standard_class,
                           L_standard_uom) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.GET_STANDARD_CLASS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END GET_STANDARD_CLASS;
-------------------------------------------------------------------------------
FUNCTION ROUND_TO_INT(I_real_number IN NUMBER)
RETURN NUMBER IS

   /*
    * For some reason, the PL/SQL built in round function does not compile
    * in a database package. Therefore, this internal round function was written to
    * round real numbers to integers.
    */

   L_remainder number;

BEGIN
   L_remainder := mod(I_real_number,1);

   if ((I_real_number > 0) and
       (I_real_number < 1)) then
      return ceil(I_real_number);
   end if;

   if L_remainder != 0 then
      if L_remainder < .5 then
         return floor(I_real_number);
      else
         return ceil(I_real_number);
      end if;
   else
      return I_real_number;
   end if;
END ROUND_TO_INT;
-------------------------------------------------------------------------------
FUNCTION APPLY_ROUND_DIFFERENCE(O_error_message         OUT    VARCHAR2,
                                I_line_item_table       IN OUT NOCOPY line_item_tabletype,
                                I_raw_qty               IN     NUMBER,
                                I_rounded_qty           IN OUT NUMBER,
                                I_round_leader          IN     INT,
                                I_lowest_loc_rec        IN     INT,
                                I_highest_loc_rec       IN     INT)
RETURN BOOLEAN IS

   /*
    * This private function will take an unrounded qty for a set of locations
    * and apply any increase or decrease necessary to round the total order
    * quantity for the set. If this is a single location, the increment or
    * decrement will be applied to that location. If this is a set of locations,
    * if a 'round leader' was passed, the increment or decrement will be applied
    * to the round leader. If not, it will be applied to the lowest location number
    * of the set.
    *
    * In the case of a decrement, if there is not enough order to qty to cover the
    * decrement for the 'designated' location, the function will attempt to apply
    * the decrement to another location within the set. If no locations in the set can
    * cover the decrement, it will re-round the 'raw' qty forcing the rounding logic
    * to round up. It will then apply the increment to the round leader if one was
    * passed and to the lowest location number within the set otherwise.
    */

i                    NUMBER      := 0; -- An index variable for loops
L_difference_applied BOOLEAN     := FALSE;
L_lineitem_updated   BOOLEAN     := FALSE;
L_rounded_up_qty     NUMBER      := 0;
L_override_fail_ind  VARCHAR2(1) := 'N';

-- This variable will hold the round lvl for any forced round up.
L_roundup_roundlvl   item_supp_country.round_lvl%TYPE;

BEGIN

   /*
    * If the round difference is an increment or if it's a decrement and the round leader or lowest loc #
    * has enough order qty to cover the decrement, then go ahead and apply the change to the round leader
    * or the lowest loc # if the round leader is not apart of the set. However, if the location is a cross-dock
    * warehouse or if the line item contains a po-linked transfer qty, then the order qty must not be
    * reduced below the total qty allocated to the stores. Likewise,
    * if the line item has receipts against it, the order qty must not be reduced below the qty received. If a
    * line item has 0 order qty initially, it is assumed that this is a user entered qty and that the user desires
    * to 'zero out' or cancel that item. Therefore line items with an initial qty of zero will not be increased.
    */

   if (I_raw_qty - I_rounded_qty < I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered)
      and
      (I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered != 0)
      and
      ((I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).xdock_wh_ind = 'N')
        or
       (I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).xdock_wh_ind = 'Y'
        and
        (I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered + I_rounded_qty - I_raw_qty >
         I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).total_alloc_qty)))
      and
      ((I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).receipts_ind = 'N')
        or
        ((I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).receipts_ind = 'Y')
          and
          (I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered + I_rounded_qty - I_raw_qty >
           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_received)))
       and
       ((I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).tsf_linked_ind = 'N')
         or
         (I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).tsf_linked_ind = 'Y'
          and
          (I_raw_qty <= I_rounded_qty))) then

      I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered :=
         I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered + (I_rounded_qty - I_raw_qty);

      I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).changed_ind := 'Y';

   else

      /*
       * This is a decrement and there is not enough order qty for the round leader/lowest loc # to cover
       * the decrement without that line item being rounded to 0 or less. Therefore, loop through the set of locs
       * to determine whether any have enough order qty to cover the decrement. However, if the location is a cross-dock
       * warehouse, then the order qty must not be reduced below the total qty allocated to the stores.
       * Apply the decrement to the first loc found that can cover the decrement and not reduce any allocations.
       * Likewise, the order qty should not be reduced below any receipts. If no locations within the set can cover
       * the decrement, round the raw qty again forcing a round up. Apply the increment to the round leader or the
       * lowest loc #.
       */

      i := I_lowest_loc_rec - 1;

      LOOP
         i := i + 1;

         if (I_raw_qty - I_rounded_qty < I_line_item_table(i).qty_ordered)
            and
            (I_line_item_table(i).qty_ordered != 0)
            and
            ((I_line_item_table(i).xdock_wh_ind = 'N')
             or
             (I_line_item_table(i).xdock_wh_ind = 'Y'
              and
              (I_line_item_table(i).qty_ordered + I_rounded_qty - I_raw_qty > I_line_item_table(i).total_alloc_qty)))
            and
            ((I_line_item_table(i).receipts_ind = 'N')
              or
              ((I_line_item_table(i).receipts_ind = 'Y')
                and
                (I_line_item_table(i).qty_ordered + I_rounded_qty - I_raw_qty >
                 I_line_item_table(i).qty_received)))
            and
            ((I_line_item_table(i).tsf_linked_ind = 'N')
              or
              (I_line_item_table(i).tsf_linked_ind = 'Y'
               and
               (I_raw_qty <= I_rounded_qty))) then

            I_line_item_table(i).qty_ordered := I_line_item_table(i).qty_ordered + (I_rounded_qty - I_raw_qty);
            I_line_item_table(i).changed_ind := 'Y';
            L_difference_applied := TRUE;
         end if;

         EXIT WHEN (L_difference_applied) or (i = I_highest_loc_rec);
      END LOOP;

      /*
       * If none of the locations in the set have enough order qty to cover the
       * decrement, the raw qty should be re-rounded forcing the round logic to round up.
       * The increment should be applied to the round leader or the lowest loc # of
       * the set if the round leader is NULL.
       */

      if not L_difference_applied then

         if GET_ROUNDUP_ROUNDLVL(O_error_message,
                                 L_roundup_roundlvl,
                                 I_line_item_table(nvl(I_round_leader,I_lowest_loc_rec)).round_lvl) = FALSE then
            return FALSE;
         end if;

         -- Call round function.
         if ROUND_BY_LEVEL(O_error_message,
                           L_rounded_up_qty,
                           L_override_fail_ind,
                           I_raw_qty,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).supp_pack_size,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).item,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).supplier,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).orig_cntry,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).location,
                           L_roundup_roundlvl,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).round_to_pallet_pct,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).round_to_layer_pct,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).round_to_case_pct,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).ti,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).hi,
                           I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).supp_pack_size,
                           'Y'/*Must round up ind*/) = FALSE then
            return FALSE;
         end if;

         I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered :=
            I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered + (L_rounded_up_qty - I_raw_qty);

         I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).changed_ind := 'Y';

         -- Pass back the new rounded qty for the group.
         I_rounded_qty := L_rounded_up_qty;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.APPLY_ROUND_DIFFERENCE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END APPLY_ROUND_DIFFERENCE;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_PHYSICAL_QTY(O_error_message      OUT    VARCHAR2,
                               O_covered_inbound    OUT    BOOLEAN,
                               I_order_no           IN     ordhead.order_no%TYPE,
                               I_orig_approval_date IN     ordhead.orig_approval_date%TYPE,
                               I_line_item_table    IN OUT NOCOPY line_item_tabletype,
                               I_rounded_qty        IN     NUMBER,
                               I_phys_rounded_qty   IN     NUMBER,
                               I_round_leader       IN     INT,
                               I_lowest_loc_rec     IN     INT)
RETURN BOOLEAN IS

   L_outstanding_ship_qty shipsku.qty_expected%TYPE := 0;
   L_outstanding_appt_qty appt_detail.qty_appointed%TYPE := 0;

   L_inbound_qty  NUMBER := 0;

BEGIN
   O_covered_inbound := FALSE;

   -- Only orders that have been approved previously will have any receipts or inbound qtys.
   if I_orig_approval_date is not NULL then
      if GET_OUTSTANDING_SHIP_QTY(O_error_message,
                                  L_outstanding_ship_qty,
                                  I_order_no,
                                  I_line_item_table(I_lowest_loc_rec)) = FALSE then
         return FALSE;
      end if;

      if GET_OUTSTANDING_APPT_QTY(O_error_message,
                                  L_outstanding_appt_qty,
                                  I_order_no,
                                  I_line_item_table(I_lowest_loc_rec)) = FALSE then
         return FALSE;
      end if;

      -- Shipped and appointed qty both represent the same inbound qty. However,
      -- they may arrive in the system at different times, therefore the greater of
      -- the two is used here to calculate the inbound in order to reflect the most
      -- current picture of what is in-transit to the physical location.
      if L_outstanding_ship_qty > L_outstanding_appt_qty then
         L_inbound_qty := L_outstanding_ship_qty;
      else
         L_inbound_qty := L_outstanding_appt_qty;
      end if;

      -- The total order qty for the physical item location cannot be rounded below the
      -- inbound qty. If so, the order qty needs to be adjusted up to cover the shortfall.
      if  I_phys_rounded_qty < L_inbound_qty then
         if APPLY_PHYS_QTY_SHORTFALL(O_error_message,
                                     I_line_item_table,
                                     I_rounded_qty,
                                     I_phys_rounded_qty,
                                     L_inbound_qty,
                                     I_round_leader,
                                     I_lowest_loc_rec) = FALSE then
            return FALSE;
         end if;

         -- The order qty was adjusted up to cover inbound qty's.
         -- The O_covered_inbound parameter is set to TRUE to indicate this.
         O_covered_inbound := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.VALIDATE_PHYSICAL_QTY',
                                            to_char(SQLCODE));
      RETURN FALSE;
END VALIDATE_PHYSICAL_QTY;
-------------------------------------------------------------------------------
FUNCTION GET_OUTSTANDING_SHIP_QTY(O_error_message        OUT VARCHAR2,
                                  O_outstanding_ship_qty OUT shipsku.qty_expected%TYPE,
                                  I_order_no             IN  ordhead.order_no%TYPE,
                                  I_line_item_rec        IN  line_item_rectype)
RETURN BOOLEAN IS

    cursor C_GET_QTY_SHIPPED is
      select nvl(sum(nvl(s.qty_expected,0) - nvl(s.qty_received,0)), 0)
        from shipsku s,
             shipment h
       where s.shipment     = h.shipment
         and h.order_no     = I_order_no
         and h.to_loc       = nvl(I_line_item_rec.physical_wh, -999)
         and s.item         = I_line_item_rec.item
         and h.status_code <> 'C'; -- 'C'losed

BEGIN

   open C_GET_QTY_SHIPPED;
   fetch C_GET_QTY_SHIPPED into O_outstanding_ship_qty;
   close C_GET_QTY_SHIPPED;

   -- When calculating the qty shipped or appointed, the greater of (qty expected - qty received, 0)
   -- should be used to cover the scenario where the qty received is greater than the qty expected.
   if nvl(O_outstanding_ship_qty, -1) < 0 then
      O_outstanding_ship_qty := 0;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.GET_OUTSTANDING_SHIP_QTY',
                                            to_char(SQLCODE));
      RETURN FALSE;
END GET_OUTSTANDING_SHIP_QTY;
-------------------------------------------------------------------------------
FUNCTION GET_OUTSTANDING_APPT_QTY(O_error_message        OUT VARCHAR2,
                                  O_outstanding_appt_qty OUT appt_detail.qty_appointed%TYPE,
                                  I_order_no             IN  ordhead.order_no%TYPE,
                                  I_line_item_rec        IN  line_item_rectype)
RETURN BOOLEAN IS

   cursor C_GET_QTY_APPOINTED is
      select nvl(sum(nvl(qty_appointed,0) - nvl(qty_received,0)), 0)
        from appt_detail
       where doc      = I_order_no
         and loc      = nvl(I_line_item_rec.physical_wh, -999)
         and item     = I_line_item_rec.item;

BEGIN

   open C_GET_QTY_APPOINTED;
   fetch C_GET_QTY_APPOINTED into O_outstanding_appt_qty;
   close C_GET_QTY_APPOINTED;

   -- When calculating the qty shipped or appointed, the greater of (qty expected - qty received, 0)
   -- should be used to cover the scenario where the qty received is greater than the qty expected.
   if nvl(O_outstanding_appt_qty, -1) < 0 then
      O_outstanding_appt_qty := 0;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.GET_OUTSTANDING_APPT_QTY',
                                            to_char(SQLCODE));
      RETURN FALSE;
END GET_OUTSTANDING_APPT_QTY;
-------------------------------------------------------------------------------
FUNCTION APPLY_PHYS_QTY_SHORTFALL(O_error_message     OUT    VARCHAR2,
                                  I_line_item_table   IN OUT NOCOPY line_item_tabletype,
                                  I_rounded_qty       IN     NUMBER,
                                  I_phys_rounded_qty  IN     NUMBER,
                                  I_inbound_qty       IN     NUMBER,
                                  I_round_leader      IN     INT,
                                  I_lowest_loc_rec    IN     INT)
RETURN BOOLEAN IS

   L_shortfall          NUMBER      := 0;
   L_rounded_up_qty     NUMBER      := 0;
   L_override_fail_ind  VARCHAR2(1) := 'N';

   -- This variable will hold the round lvl for any forced round up.
   L_roundup_roundlvl   item_supp_country.round_lvl%TYPE;

BEGIN

   L_shortfall := I_inbound_qty - I_phys_rounded_qty;

   if GET_ROUNDUP_ROUNDLVL(O_error_message,
                           L_roundup_roundlvl,
                           I_line_item_table(nvl(I_round_leader,I_lowest_loc_rec)).round_lvl) = FALSE then
      return FALSE;
   end if;

   -- Call round function.
   if ROUND_BY_LEVEL(O_error_message,
                     L_rounded_up_qty,
                     L_override_fail_ind,
                     (I_rounded_qty + L_shortfall),
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).supp_pack_size,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).item,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).supplier,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).orig_cntry,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).location,
                     L_roundup_roundlvl,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).round_to_pallet_pct,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).round_to_layer_pct,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).round_to_case_pct,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).ti,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).hi,
                     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).supp_pack_size,
                     'Y'/*Must round up ind*/) = FALSE then
      return FALSE;
   end if;

   -- Apply the round difference.
   I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered :=
     I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).qty_ordered + L_rounded_up_qty - I_rounded_qty;

   I_line_item_table(nvl(I_round_leader, I_lowest_loc_rec)).changed_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.APPLY_PHYS_QTY_SHORTFALL',
                                            to_char(SQLCODE));
      RETURN FALSE;
END APPLY_PHYS_QTY_SHORTFALL;
-------------------------------------------------------------------------------
FUNCTION GET_ROUNDUP_ROUNDLVL(O_error_message    OUT VARCHAR2,
                              O_roundup_roundlvl OUT item_supp_country_loc.round_lvl%TYPE,
                              I_round_lvl        IN  item_supp_country_loc.round_lvl%TYPE)
RETURN BOOLEAN IS

BEGIN

   /*
    * When rounding up, smallest component of the original round lvl should be used.
    * So in the case of CLP, the round up should be to the next case. This function
    * given a round lvl, determines what the round up round lvl should be and returns
    * that.
    */
   if I_round_lvl = 'CLP' or I_round_lvl = 'CL' then
      O_roundup_roundlvl := 'C';
   elsif I_round_lvl = 'LP' then
      O_roundup_roundlvl := 'L';
   else
      O_roundup_roundlvl := I_round_lvl;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.GET_ROUNDUP_ROUNDLVL',
                                            to_char(SQLCODE));
      RETURN FALSE;
END GET_ROUNDUP_ROUNDLVL;
-------------------------------------------------------------------------------
FUNCTION ROUND(O_error_message       IN OUT VARCHAR2,
               O_out_qty             IN OUT ORDLOC.QTY_ORDERED%TYPE,
               O_rounded_ind         IN OUT VARCHAR2,
               I_in_qty              IN     ORDLOC.QTY_ORDERED%TYPE,
               I_round_pct           IN     SYSTEM_OPTIONS.ROUND_TO_INNER_PCT%TYPE,
               I_round_to_size       IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
               I_must_roundup_ind    IN     VARCHAR2,
               I_final_round_ind     IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_remainder  NUMBER;
   L_floor      NUMBER;
   L_threshold  NUMBER;

BEGIN
   /* Evaluate Requisite
      Terms For Rounding */
   L_remainder := MOD(I_in_qty, I_round_to_size);
   L_floor     := I_in_qty - L_remainder;
   L_threshold := (I_round_pct/100) * I_round_to_size;
   ---
   /* Perform
      Rounding */
   O_out_qty := I_in_qty; -- default if rounding doesn't occur
   O_rounded_ind := 'N';  -- default if rounding doesn't occur
   ---
   if I_round_pct = 0 and L_remainder = 0 then
      O_out_qty := I_in_qty;
   else
      if (I_in_qty < I_round_to_size and I_final_round_ind = 'Y') then
         -- cannot round to zero, so go up to 1 unit
         O_out_qty := I_round_to_size;
         O_rounded_ind := 'Y';
      else
         if (   (L_remainder >= L_threshold)
             or (I_must_roundup_ind = 'Y' and L_remainder >0)) then
            -- threshold exceeded; round up to next unit OR forced upward rounding

             BEGIN
                O_out_qty := L_floor + I_round_to_size;
             EXCEPTION
                when VALUE_ERROR then
                 O_out_qty := L_floor;
             END;

            O_rounded_ind := 'Y';
         else
            if I_final_round_ind = 'Y' then
               -- threshold not met AND final round; round down to next unit
               O_out_qty := L_floor;
               O_rounded_ind := 'Y';
            end if;
         end if;
      end if;
   end if;
   ---
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ROUND',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ROUND;
--------------------------------------------------------------------------------
FUNCTION GET_PACKSIZE(O_error_message   IN OUT VARCHAR2,
                      O_store_pack_size IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                      O_store_ord_mult  IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                      O_break_pack_ind  IN OUT WH.BREAK_PACK_IND%TYPE,
                      I_item            IN     ITEM_MASTER.ITEM%TYPE,
                      I_wh              IN     WH.WH%TYPE,
                      I_supplier        IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_loc             IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)  := 'ROUNDING_SQL.GET_PACKSIZE';

   cursor C_BREAK_PACK_IND is
      select break_pack_ind
        from wh
       where wh = I_wh;

   cursor C_SUPP_PACK_SIZE is
      select supp_pack_size
        from item_supp_country
       where item = I_item
         and (supplier = I_supplier
          or  (I_supplier is NULL and primary_supp_ind = 'Y'))
         and (origin_country_id = I_origin_country
          or  (I_origin_country is NULL and primary_country_ind = 'Y'));

   cursor C_BREAK_PACKSIZE is
      select decode(il.store_ord_mult,
                    'I', ic.inner_pack_size,
                    'C', ic.supp_pack_size,
                    'E', 1),
             il.store_ord_mult
        from item_loc il, item_supp_country ic
       where il.item = I_item
         and il.loc  = I_loc
         and il.item = ic.item
         and (supplier = I_supplier
          or (I_supplier is NULL and ic.primary_supp_ind = 'Y'))
         and (ic.origin_country_id = I_origin_country
          or (I_origin_country is NULL and ic.primary_country_ind = 'Y'));

BEGIN
   /* Check Input Values */
   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_wh',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_loc',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_BREAK_PACK_IND', 'WH', 'Warehouse:
                                              '||to_char(I_wh));
   open C_BREAK_PACK_IND;
   SQL_LIB.SET_MARK('FETCH', 'C_BREAK_PACK_IND', 'WH', 'Warehouse:
                                              '||to_char(I_wh));
   fetch C_BREAK_PACK_IND into O_break_pack_ind;
   if C_BREAK_PACK_IND%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_BREAK_PACK_IND', 'WH', 'Warehouse:
                                              '||to_char(I_wh));
      close C_BREAK_PACK_IND;
      O_error_message := sql_lib.create_msg('INV_WH', null, null, null);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_BREAK_PACK_IND', 'WH', 'Warehouse:
                                              '||to_char(I_wh));
   close C_BREAK_PACK_IND;

   if O_break_pack_ind = 'N' then
      SQL_LIB.SET_MARK('OPEN', 'C_SUPP_PACK_SIZE', 'ITEM_SUPP_COUNTRY',
            'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      open C_SUPP_PACK_SIZE;
      SQL_LIB.SET_MARK('FETCH', 'C_SUPP_PACK_SIZE', 'ITEM_SUPP_COUNTRY',
            'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      fetch C_SUPP_PACK_SIZE into O_store_pack_size;
      if C_supp_pack_size%NOTFOUND then
         O_error_message :=
              SQL_LIB.CREATE_MSG('INV_ITEM/SUPPLIER/COUNTRY',null,null,null);
         SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_PACK_SIZE', 'ITEM_SUPP_COUNTRY',
             'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
         close C_SUPP_PACK_SIZE;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_PACK_SIZE', 'ITEM_SUPP_COUNTRY',
             'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_SUPP_PACK_SIZE;
   else --O_break_pack_ind = 'Y'
      SQL_LIB.SET_MARK('OPEN', 'C_BREAK_PACKSIZE', 'ITEM_SUPP_COUNTRY',
            'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      open C_BREAK_PACKSIZE;
      SQL_LIB.SET_MARK('FETCH', 'C_BREAK_PACKSIZE', 'ITEM_SUPP_COUNTRY',
            'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      fetch C_BREAK_PACKSIZE into O_store_pack_size, O_store_ord_mult;
      if C_BREAK_PACKSIZE%NOTFOUND then
         O_error_message :=
              SQL_LIB.CREATE_MSG('INV_ITEM/SUPPLIER/COUNTRY',null,null,null);
         SQL_LIB.SET_MARK('CLOSE', 'C_BREAK_PACKSIZE', 'ITEM_SUPP_COUNTRY',
             'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
         close C_BREAK_PACKSIZE;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_BREAK_PACKSIZE', 'ITEM_SUPP_COUNTRY',
             'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_BREAK_PACKSIZE;
   end if;
   return  TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                                        SQLERRM,
                                                        L_program,
                                                        to_char(SQLCODE));
      return FALSE;
END GET_PACKSIZE;
---------------------------------------------------------------------
FUNCTION TO_INNER_CASE(O_error_message      IN OUT VARCHAR2,
                       O_out_qty            IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       I_item               IN     ITEM_MASTER.ITEM%TYPE,
                       I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_origin_country     IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_location           IN     ITEM_LOC.LOC%TYPE,
                       I_in_qty             IN     ORDLOC.QTY_ORDERED%TYPE,
                       I_override_case_size IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_store_pack_size    IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_store_ord_mult     IN     ITEM_LOC.STORE_ORD_MULT%TYPE,
                       I_break_pack_ind     IN     WH.BREAK_PACK_IND%TYPE)
RETURN BOOLEAN IS
   CURSOR C_isc_loc_round_info is
      select round_to_inner_pct, round_to_case_pct
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country
         and loc               = I_location;

   CURSOR C_item_supp_ctry_round_info is
      select round_to_inner_pct, round_to_case_pct
        from item_supp_country
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country;

   CURSOR C_item_loc_round_info is
      select iscl.round_to_inner_pct, iscl.round_to_case_pct
        from item_supp_country_loc iscl, item_supp_country isc, item_loc il
       where (    iscl.item = I_item
              and iscl.loc  = I_location
              and il.item   = I_item
              and il.loc    = I_location
              and (   (    il.primary_supp is not NULL
                       and il.primary_cntry is not NULL
                       and il.primary_supp  = iscl.supplier
                       and il.primary_cntry = iscl.origin_country_id)
                   or (    il.primary_supp         is NULL
                       and il.primary_cntry        is NULL
                       and isc.item                = I_item
                       and isc.supplier            = iscl.supplier
                       and isc.origin_country_id   = iscl.origin_country_id
                       and isc.primary_supp_ind    = 'Y'
                       and isc.primary_country_ind = 'Y')));

   CURSOR C_item_round_info is
      select round_to_inner_pct, round_to_case_pct
        from item_supp_country
       where item                = I_item
         and primary_supp_ind    = 'Y'
         and primary_country_ind = 'Y';

   CURSOR C_store_ord_mult_loc is
      select store_ord_mult
        from item_loc
       where item = I_item
         and loc  = I_location;

   CURSOR C_store_ord_mult_item is
      select store_ord_mult
        from item_master
       where item = I_item;

L_case_size       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NVL(I_override_case_size, I_store_pack_size);
L_check           NUMBER(4);
L_to_inner_pct    SYSTEM_OPTIONS.ROUND_TO_INNER_PCT%TYPE;
L_to_case_pct     SYSTEM_OPTIONS.ROUND_TO_CASE_PCT%TYPE;
L_store_ord_mult  ITEM_LOC.STORE_ORD_MULT%TYPE          := I_store_ord_mult;
L_rounded_ind     VARCHAR2(1);

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   /* Fetch Rounding Information,
      Defaulting As Necessary    */
   if I_supplier is NULL and I_origin_country is NULL and I_location is NULL then
     SQL_LIB.SET_MARK('OPEN', 'C_item_round_info', 'item_supp_country', NULL);
      open C_item_round_info;
     SQL_LIB.SET_MARK('FETCH', 'C_item_round_info', 'item_supp_country', NULL);
     fetch C_item_round_info into L_to_inner_pct, L_to_case_pct;
     ---
     if C_item_round_info%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ITEM_PRIM',null,null,null);
        SQL_LIB.SET_MARK('CLOSE', 'C_item_round_info', 'item_supp_country', NULL);
        close C_item_round_info;
        return FALSE;
     end if;
     ---
     SQL_LIB.SET_MARK('CLOSE', 'C_item_round_info', 'item_supp_country', NULL);
     close C_item_round_info;
   elsif I_supplier is NULL and I_origin_country is NULL then
     SQL_LIB.SET_MARK('OPEN', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
      open C_item_loc_round_info;
     SQL_LIB.SET_MARK('FETCH', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
     fetch C_item_loc_round_info into L_to_inner_pct, L_to_case_pct;
     ---
     if C_item_loc_round_info%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ITEM_LOC',null,null,null);
        SQL_LIB.SET_MARK('CLOSE', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
        close C_item_loc_round_info;
        return FALSE;
     end if;
     ---
     SQL_LIB.SET_MARK('CLOSE', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
     close C_item_loc_round_info;
   elsif I_location is NULL then
     SQL_LIB.SET_MARK('OPEN', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
      open C_item_supp_ctry_round_info;
     SQL_LIB.SET_MARK('FETCH', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
     fetch C_item_supp_ctry_round_info into L_to_inner_pct, L_to_case_pct;
     ---
     if C_item_supp_ctry_round_info%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ISC',null,null,null);
        SQL_LIB.SET_MARK('CLOSE', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
        close C_item_supp_ctry_round_info;
        return FALSE;
     end if;
     ---
     SQL_LIB.SET_MARK('CLOSE', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
     close C_item_supp_ctry_round_info;
   else
     SQL_LIB.SET_MARK('OPEN', 'C_isc_loc_round_info', 'item_supp_country_loc', NULL);
      open C_isc_loc_round_info;
     SQL_LIB.SET_MARK('FETCH', 'C_isc_loc_round_info', 'item_supp_country_loc', NULL);
     fetch C_isc_loc_round_info into L_to_inner_pct, L_to_case_pct;
     ---
     if C_isc_loc_round_info%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ISCL',null,null,null);
        SQL_LIB.SET_MARK('CLOSE', 'C_isc_loc_round_info', 'item_supp_country_loc', NULL);
        close C_isc_loc_round_info;
        return FALSE;
     end if;
     ---
     SQL_LIB.SET_MARK('CLOSE', 'C_isc_loc_round_info', 'item_supp_country_loc', NULL);
     close C_isc_loc_round_info;
   end if;
   ---
   /* Fetch Store Order Multiple Information,
      Defaulting As Necessary    */
   if I_store_ord_mult is not NULL then
      if I_location is not NULL then
        SQL_LIB.SET_MARK('OPEN', 'C_store_ord_mult_loc', 'item_loc', NULL);
         open C_store_ord_mult_loc;
        SQL_LIB.SET_MARK('FETCH', 'C_store_ord_mult_loc', 'item_loc', NULL);
        fetch C_store_ord_mult_loc into L_store_ord_mult;
        ---
        if C_store_ord_mult_loc%NOTFOUND then
           O_error_message := SQL_LIB.CREATE_MSG('NO_SOM_ITEM_LOC',null,null,null);
        SQL_LIB.SET_MARK('CLOSE', 'C_store_ord_mult_loc', 'item_loc', NULL);
           close C_store_ord_mult_loc;
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE', 'C_store_ord_mult_loc', 'item_loc', NULL);
        close C_store_ord_mult_loc;
      else
        SQL_LIB.SET_MARK('OPEN', 'C_store_ord_mult_item', 'item_master', NULL);
         open C_store_ord_mult_item;
        SQL_LIB.SET_MARK('FETCH', 'C_store_ord_mult_item', 'item_master', NULL);
        fetch C_store_ord_mult_item into L_store_ord_mult;
        ---
        if C_store_ord_mult_item%NOTFOUND then
           O_error_message := SQL_LIB.CREATE_MSG('NO_SOM_ITEM',null,null,null);
           SQL_LIB.SET_MARK('CLOSE', 'C_store_ord_mult_item', 'item_master', NULL);
           close C_store_ord_mult_item;
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE', 'C_store_ord_mult_item', 'item_master', NULL);
        close C_store_ord_mult_item;
      end if;
   end if;
   /* Perform Rounding,
      Based On Store Order Multiple
      And Break Pack Indicator  */
   if I_break_pack_ind = 'N' or L_store_ord_mult = 'C' then
      if ROUND(O_error_message,
               O_out_qty,
               L_rounded_ind,
               I_in_qty,
               L_to_case_pct,
               L_case_size,
               'N',
               'Y')= FALSE then
         return FALSE;
      end if;
   else
      if L_store_ord_mult = 'E' then
         O_out_qty := I_in_qty;
         return TRUE;
      elsif L_store_ord_mult = 'I' then
         if I_override_case_size is not NULL then
            -- check if I_override_case_size is evenly divisible by I_store_pack_size
            L_check := mod(I_override_case_size, I_store_pack_size);
         end if;
         if I_override_case_size is NULL or L_check = 0 then
            if ROUND(O_error_message,
                     O_out_qty,
                     L_rounded_ind,
                     I_in_qty,
                     L_to_inner_pct,
                     I_store_pack_size,
                     'N',
                     'Y')   = FALSE then
               return FALSE;
            end if;
         else
            if ROUND(O_error_message,
                     O_out_qty,
                     L_rounded_ind,
                     I_in_qty,
                     L_to_case_pct,
                     I_override_case_size,
                     'N',
                     'Y')   = FALSE then
                return FALSE;
            end if;
         end if;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.TO_INNER_CASE',
                                            to_char(SQLCODE));
      RETURN FALSE;
END TO_INNER_CASE;
---------------------------------------------------------------------
FUNCTION CHECK_CASE_PALLET(O_error_message       IN OUT VARCHAR2,
                           O_need_origin_country IN OUT BOOLEAN,
                           O_invalid_qty         IN OUT BOOLEAN,
                           I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                           I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                           I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                           I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                           I_override_case_size  IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN IS

   L_supplier       ordhead.supplier%TYPE := I_supplier;
   L_pallet_size    NUMBER                := NULL;
   L_hi             ITEM_SUPP_COUNTRY.HI%TYPE := NULL;
   L_ti             ITEM_SUPP_COUNTRY.TI%TYPE := NULL;
   L_supp_pack_size ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_remainder      NUMBER                := NULL;
   L_item           ITEM_MASTER.ITEM%TYPE := I_item;

   cursor C_GET_ORD_SUPPLIER is
      select supplier
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_PALLET_SIZE is
      select (ti * hi * supp_pack_size) pallet_size
        from item_supp_country
       where item = L_item
         and supplier = L_supplier
         and origin_country_id = I_origin_country_id;

   cursor C_GET_ITEMS is
      select item
        from item_master
       where item = I_item
          or item_parent = I_item
          or item_grandparent = I_item;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;

   if I_override_case_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_override_case_size',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_need_origin_country := TRUE;
      return TRUE;
   end if;

   O_need_origin_country := FALSE;
   ---
   if I_supplier is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ORD_SUPPLIER',
                       'ordhead',
                       'Order No.: '||to_char(I_order_no));
      open C_GET_ORD_SUPPLIER;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ORD_SUPPLIER',
                       'ordhead',
                       'Order No.: '||to_char(I_order_no));
      fetch C_GET_ORD_SUPPLIER into L_supplier;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ORD_SUPPLIER',
                       'ordhead',
                       'Order No.: '||to_char(I_order_no));
      close C_GET_ORD_SUPPLIER;
   end if;

   for C_item_rec in C_GET_ITEMS loop
      L_item := C_item_rec.item;

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PALLET_SIZE',
                       'item_supp_country',
                       'Item: '||L_item||
                       ', Supplier: '||to_char(L_supplier)||
                       ', Country ID: '||I_origin_country_id);
      open C_GET_PALLET_SIZE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PALLET_SIZE',
                       'item_supp_country',
                       'Item: '||L_item||
                       ', Supplier: '||to_char(L_supplier)||
                       ', Country ID: '||I_origin_country_id);
      fetch C_GET_PALLET_SIZE into L_pallet_size;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PALLET_SIZE',
                       'item_supp_country',
                       'Item: '||L_item||
                       ', Supplier: '||to_char(L_supplier)||
                       ', Country ID: '||I_origin_country_id);
      close C_GET_PALLET_SIZE;
      ---
      L_remainder := MOD(L_pallet_size, I_override_case_size);

      if L_remainder != 0 then  -- pallet not evenly divisible by case size
         O_invalid_qty := TRUE;
         return TRUE;
      end if;
   end loop;
   ---

   O_invalid_qty := FALSE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.CHECK_CASE_PALLET',
                                            to_char(SQLCODE));
      RETURN FALSE;
END CHECK_CASE_PALLET;
---------------------------------------------------------------
FUNCTION ROUND_BY_LEVEL(O_error_message       IN OUT VARCHAR2,
                        O_out_qty             IN OUT ORDLOC.QTY_ORDERED%TYPE,
                        O_override_fail_ind   IN OUT VARCHAR2,
                        I_in_qty              IN     ORDLOC.QTY_ORDERED%TYPE,
                        I_override_case_size  IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        I_item                IN     ITEM_SUPPLIER.ITEM%TYPE,
                        I_supplier            IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_origin_country      IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_location            IN     ITEM_LOC.LOC%TYPE,
                        I_round_lvl           IN     ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                        I_round_to_case_pct   IN     ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                        I_round_to_layer_pct  IN     ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE,
                        I_round_to_pallet_pct IN     ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE,
                        I_ti                  IN     ITEM_SUPP_COUNTRY.TI%TYPE,
                        I_hi                  IN     ITEM_SUPP_COUNTRY.HI%TYPE,
                        I_supp_pack_size      IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        I_must_roundup_ind    IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_round_lvl           ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE                       := I_round_lvl;
   L_to_case_pct         ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE               := I_round_to_case_pct;
   L_to_layer_pct        ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE      := I_round_to_layer_pct;
   L_to_pallet_pct       ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE     := I_round_to_pallet_pct;
   L_ti                  ITEM_SUPP_COUNTRY.TI%TYPE                      := I_ti;
   L_hi                  ITEM_SUPP_COUNTRY.HI%TYPE                      := I_hi;
   L_case_size           ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE          := I_supp_pack_size;
   L_layer_size          ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_pallet_size         ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_rounded_ind         VARCHAR2(1)                                    := 'N';
   L_final_round_ind     VARCHAR2(1)                                    := 'N';

   CURSOR C_isc_loc_round_info is
      select round_lvl, round_to_case_pct, round_to_layer_pct, round_to_pallet_pct
        from item_supp_country_loc
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country
         and loc = I_location;

   CURSOR C_item_supp_ctry_round_info is
      select round_lvl, round_to_case_pct, round_to_layer_pct, round_to_pallet_pct
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country;

   CURSOR C_item_loc_round_info is
      select iscl.round_lvl, iscl.round_to_case_pct, iscl.round_to_layer_pct, iscl.round_to_pallet_pct
        from item_supp_country_loc iscl, item_supp_country isc, item_loc il
       where (    iscl.item = I_item
              and iscl.loc = I_location
              and il.item = I_item
              and il.loc = I_location
              and (   (    il.primary_supp is not NULL
                       and il.primary_cntry is not NULL
                       and il.primary_supp = iscl.supplier
                       and il.primary_cntry = iscl.origin_country_id)
                   or (    il.primary_supp is NULL
                       and il.primary_cntry is NULL
                       and isc.item = I_item
                       and isc.supplier = iscl.supplier
                       and isc.origin_country_id = iscl.origin_country_id
                       and isc.primary_supp_ind = 'Y'
                       and isc.primary_country_ind = 'Y')));

   CURSOR C_item_round_info is
      select round_lvl, round_to_case_pct, round_to_layer_pct, round_to_pallet_pct
        from item_supp_country
       where item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';

   CURSOR C_unit_size_info is
      select supp_pack_size, ti, hi
        from item_supp_country
       where item = I_item
         and (supplier = I_supplier
          or  (I_supplier is NULL and primary_supp_ind = 'Y'))
         and (origin_country_id = I_origin_country
          or  (I_origin_country is NULL and primary_country_ind = 'Y'));

BEGIN
   /* Check Input Values
      And Set Defaults   */
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_in_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_in_qty',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   O_out_qty := I_in_qty;
   O_override_fail_ind := 'N';
   ---
   /* If Any Of The Rounding
      Parameters Isn't Passed In,
      Fetch Rounding Information,
      Defaulting As Necessary    */
   if (   I_round_lvl           is NULL
       or I_round_to_case_pct   is NULL
       or I_round_to_layer_pct  is NULL
       or I_round_to_pallet_Pct is NULL) then
      if I_supplier is NULL and I_origin_country is NULL and I_location is NULL then
        SQL_LIB.SET_MARK('OPEN', 'C_item_round_info', 'item_supp_country', NULL);
         open C_item_round_info;
        SQL_LIB.SET_MARK('FETCH', 'C_item_round_info', 'item_supp_country', NULL);
        fetch C_item_round_info into L_round_lvl, L_to_case_pct, L_to_layer_pct, L_to_pallet_pct;
        ---
        if C_item_round_info%NOTFOUND then
           O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ITEM_PRIM',null,null,null);
           SQL_LIB.SET_MARK('CLOSE', 'C_item_round_info', 'item_supp_country', NULL);
           close C_item_round_info;
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE', 'C_item_round_info', 'item_supp_country', NULL);
        close C_item_round_info;
      elsif I_supplier is NULL and I_origin_country is NULL then
        SQL_LIB.SET_MARK('OPEN', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
         open C_item_loc_round_info;
        SQL_LIB.SET_MARK('FETCH', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
        fetch C_item_loc_round_info into L_round_lvl, L_to_case_pct, L_to_layer_pct, L_to_pallet_pct;
        ---
        if C_item_loc_round_info%NOTFOUND then
           O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ITEM_LOC',null,null,null);
           SQL_LIB.SET_MARK('CLOSE', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
           close C_item_loc_round_info;
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE', 'C_item_loc_round_info', 'item_supp_country_loc, item_supp_country, item_loc', NULL);
        close C_item_loc_round_info;
      elsif I_location is NULL then
        SQL_LIB.SET_MARK('OPEN', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
         open C_item_supp_ctry_round_info;
        SQL_LIB.SET_MARK('FETCH', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
        fetch C_item_supp_ctry_round_info into L_round_lvl, L_to_case_pct, L_to_layer_pct, L_to_pallet_pct;
        ---
        if C_item_supp_ctry_round_info%NOTFOUND then
           O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ISC',null,null,null);
           SQL_LIB.SET_MARK('CLOSE', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
           close C_item_supp_ctry_round_info;
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE', 'C_item_supp_ctry_round_info', 'item_supp_country', NULL);
        close C_item_supp_ctry_round_info;
      else
        SQL_LIB.SET_MARK('OPEN', 'C_item_loc_round_info', 'item_supp_country_loc', NULL);
         open C_isc_loc_round_info;
        SQL_LIB.SET_MARK('FETCH', 'C_item_loc_round_info', 'item_supp_country_loc', NULL);
        fetch C_isc_loc_round_info into L_round_lvl, L_to_case_pct, L_to_layer_pct, L_to_pallet_pct;
        ---
        if C_isc_loc_round_info%NOTFOUND then
           O_error_message := SQL_LIB.CREATE_MSG('NO_ROUND_INFO_ISCL',null,null,null);
           SQL_LIB.SET_MARK('CLOSE', 'C_item_loc_round_info', 'item_supp_country_loc', NULL);
           close C_isc_loc_round_info;
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('CLOSE', 'C_item_loc_round_info', 'item_supp_country_loc', NULL);
        close C_isc_loc_round_info;
      end if;
   end if;
   ---
   /* If Any Size Parameter
      Isn't Passed In,
      Determine Unit Sizes
      As Appropriate    */
   if (   I_ti             is NULL
       or I_hi             is NULL
       or I_supp_pack_size is NULL) then
      SQL_LIB.SET_MARK('OPEN', 'C_unit_size_info', 'item_supp_country', NULL);
       open C_unit_size_info;
      SQL_LIB.SET_MARK('FETCH', 'C_unit_size_info', 'item_supp_country', NULL);
      fetch C_unit_size_info into L_case_size, L_ti, L_hi;
      SQL_LIB.SET_MARK('CLOSE', 'C_unit_size_info', 'item_supp_country', NULL);
      close C_unit_size_info;
   end if;
   ---
   L_layer_size  := L_case_size * L_ti;
   L_pallet_size := L_case_size * L_ti * L_hi;
   ---
   /* Handle The
      Override Case */
   if I_override_case_size is not NULL then
      L_case_size := I_override_case_size;
      ---
      --- Check if the Override Case Size is a factor of the Layer and Pallet Sizes.
      --- If not, then only Case rounding is possible.
      ---
      if MOD(L_layer_size, I_override_case_size) != 0 then
         ---
         --- Since only Case Rounding is possible, check if the Round Level includes
         --- Case Rounding.  If it doesn't, then no Rounding can be performed.
         ---
         if L_round_lvl in ('L', 'P', 'LP') then
            O_override_fail_ind := 'Y';
            RETURN TRUE;
         else
            ---
            --- The Round Level must be either 'C', 'CL' or 'CLP'.  Only the Case
            --- Rounding portion of these Levels can be performed, so we force
            --- Case Rounding.
            ---
            L_round_lvl := 'C';
         end if;
      end if;
   end if;
   ---
   /* Call Rounding Fns.
      As Appropriate     */
   if L_round_lvl in ('P', 'LP', 'CLP') then
      if L_round_lvl = 'P' then
         L_final_round_ind := 'Y';
      end if;
      ---
      if ROUND(O_error_message,
               O_out_qty,
               L_rounded_ind,
               I_in_qty,
               L_to_pallet_pct,
               L_pallet_size,
               I_must_roundup_ind,
               L_final_round_ind) = FALSE then
         RETURN FALSE;
      end if;
   end if;
   ---
   --- the following code will only execute if the Quantity wasn't rounded in the previous logic branch
   ---
   if L_round_lvl in ('L', 'LP', 'CL', 'CLP') and L_rounded_ind = 'N' then
      if L_round_lvl in ('L', 'LP') then
         L_final_round_ind := 'Y';
      end if;
      ---
      if ROUND(O_error_message,
               O_out_qty,
               L_rounded_ind,
               I_in_qty,
               L_to_layer_pct,
               L_layer_size,
               I_must_roundup_ind,
               L_final_round_ind) = FALSE then
         RETURN FALSE;
      end if;
   end if;
   ---
   --- the following code will only execute if the Quantity wasn't rounded in the previous logic branch
   ---
   if L_round_lvl in ('C', 'CL', 'CLP') and L_rounded_ind = 'N' then
      if ROUND(O_error_message,
               O_out_qty,
               L_rounded_ind,
               I_in_qty,
               L_to_case_pct,
               L_case_size,
               I_must_roundup_ind,
               'Y') = FALSE then
         RETURN FALSE;
      end if;
   end if;
   ---
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.ROUND_BY_LEVEL',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ROUND_BY_LEVEL;
--------------------------------------------------------------------
FUNCTION CALC_ROUNDED_GROUP_QTYS(O_error_message OUT VARCHAR2,
                                 I_order_no      IN  ordhead.order_no%TYPE)
RETURN BOOLEAN IS

   L_rounded_qtys_table   rounded_qtys_tabletype;
   i                      INT := 0; -- Loop index

   cursor C_GET_ROUNDED_QTYS IS
      select o.item,
             o.rowid,
             w.rounding_seq,
             o.last_rounded_qty
        from ordloc o,
             wh w
       where o.location = w.wh (+)
         and o.order_no = I_order_no
    order by o.item, w.rounding_seq;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where order_no = I_order_no
       for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC','ORDLOC', 'order_no: '||to_char(I_order_no));
   open C_LOCK_ORDLOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC','ORDLOC', 'order_no: '||to_char(I_order_no));
   close C_LOCK_ORDLOC;

   SQL_LIB.SET_MARK('OPEN','C_GET_ROUNDED_QTYS','ORDLOC', 'order_no: '||to_char(I_order_no));
   FOR rec IN C_GET_ROUNDED_QTYS LOOP

      i := i + 1;

      L_rounded_qtys_table(i).item             := rec.item;
      L_rounded_qtys_table(i).line_rowid       := rec.rowid;
      L_rounded_qtys_table(i).rounding_seq     := rec.rounding_seq;
      L_rounded_qtys_table(i).last_rounded_qty := rec.last_rounded_qty;

   END LOOP;

   if CALC_ROUNDED_GROUP_QTYS(O_error_message,
                              L_rounded_qtys_table) = FALSE then
      return FALSE;
   end if;

   FOR i IN 1..L_rounded_qtys_table.COUNT LOOP

      SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC', 'order_no: '||to_char(I_order_no));
      update ordloc
         set last_grp_rounded_qty = L_rounded_qtys_table(i).last_grp_rounded_qty
       where rowid = L_rounded_qtys_table(i).line_rowid;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ordloc',
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.CALC_ROUNDED_GROUP_QTYS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END CALC_ROUNDED_GROUP_QTYS;
-------------------------------------------------------------------------------
FUNCTION CALC_ROUNDED_GROUP_QTYS(O_error_message    OUT    VARCHAR2,
                                 IO_line_item_table IN OUT NOCOPY line_item_tabletype)
RETURN BOOLEAN IS

   L_rounded_qtys_table rounded_qtys_tabletype;

BEGIN

   FOR i IN 1..IO_line_item_table.COUNT LOOP
      L_rounded_qtys_table(i).item             := IO_line_item_table(i).item;
      L_rounded_qtys_table(i).line_rowid       := IO_line_item_table(i).line_item_rowid;
      L_rounded_qtys_table(i).rounding_seq     := IO_line_item_table(i).rounding_seq;
      L_rounded_qtys_table(i).last_rounded_qty := IO_line_item_table(i).qty_ordered;
   END LOOP;

   if CALC_ROUNDED_GROUP_QTYS(O_error_message,
                              L_rounded_qtys_table) = FALSE then
      return FALSE;
   end if;

   FOR i IN 1..IO_line_item_table.COUNT LOOP
      IO_line_item_table(i).last_grp_rounded_qty := L_rounded_qtys_table(i).last_grp_rounded_qty;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.CALC_ROUNDED_GROUP_QTYS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END CALC_ROUNDED_GROUP_QTYS;
-------------------------------------------------------------------------------
FUNCTION CALC_ROUNDED_GROUP_QTYS(O_error_message       OUT    VARCHAR2,
                                 IO_rounded_qtys_table IN OUT NOCOPY rounded_qtys_tabletype)
RETURN BOOLEAN IS

   L_last_grp_rounded_qty   ordloc.last_grp_rounded_qty%TYPE := 0;

   L_prev_item              item_master.item%TYPE;
   L_prev_rounding_seq      wh.rounding_seq%TYPE;

   -- This variable stores the PL/SQL table index for the start of the
   -- current rounding group.
   L_grp_start              INT := 1;

   -- This variable stores the number of line items in the current rounding group.
   L_number_in_grp          INT := 0;

BEGIN

   FOR i IN 1..IO_rounded_qtys_table.COUNT LOOP

      if (i > 1) and
         (nvl(L_prev_rounding_seq, -999) != nvl(IO_rounded_qtys_table(i).rounding_seq, -99) or
          L_prev_item != IO_rounded_qtys_table(i).item) then

         FOR j IN 1..L_number_in_grp LOOP
            IO_rounded_qtys_table(L_grp_start + (j - 1)).last_grp_rounded_qty := L_last_grp_rounded_qty;
         END LOOP;

         -- Reset group variables
         L_grp_start            := i;
         L_number_in_grp        := 0;
         L_last_grp_rounded_qty := 0;
      end if;

      L_number_in_grp        := L_number_in_grp + 1;
      L_last_grp_rounded_qty := L_last_grp_rounded_qty + IO_rounded_qtys_table(i).last_rounded_qty;

      L_prev_item         := IO_rounded_qtys_table(i).item;
      L_prev_rounding_seq := IO_rounded_qtys_table(i).rounding_seq;

   END LOOP;

   FOR j IN 1..L_number_in_grp LOOP
       IO_rounded_qtys_table(L_grp_start + (j - 1)).last_grp_rounded_qty := L_last_grp_rounded_qty;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.CALC_ROUNDED_GROUP_QTYS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END CALC_ROUNDED_GROUP_QTYS;
-------------------------------------------------------------------------------
FUNCTION PRORATE_ALLOCATIONS(O_error_message       OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no            IN     ordhead.order_no%TYPE,
                             I_item                IN     item_master.item%TYPE,
                             I_total_order_qty     IN     ordloc.qty_ordered%TYPE,
                             I_total_alloc_qty     IN     ordloc.qty_ordered%TYPE)
RETURN BOOLEAN IS



L_remaining_order_qty      ordloc.qty_ordered%TYPE :=0; 
L_remaining_alloc_qty      ordloc.qty_ordered%TYPE :=0;
L_excess_ord_qty           ordloc.qty_ordered%TYPE := I_total_order_qty - I_total_alloc_qty;
L_rounded_alloc_qty_in_som ordloc.qty_ordered%TYPE :=0;

 CURSOR c_get_xdock_allocs IS
          SELECT oh.order_no,
                 os.item,
                 CASE rr.store_ord_mult
                    WHEN 'I' THEN rr.inner_size
                    WHEN 'C' THEN rr.case_size
                    ELSE 1 
                 END store_ord_mult_size,
                 ol.qty_ordered,
                 ad.qty_allocated,
                 SUM(qty_allocated) over (partition by oh.order_no,os.item)  total_alloc_qty,
                 SUM(
                 CASE 
                    WHEN rr.store_ord_mult ='I' and rr.inner_size > L_excess_ord_qty then
                          ad.qty_allocated
                    WHEN  rr.store_ord_mult='C' and rr.case_size > L_excess_ord_qty THEN 
                          ad.qty_allocated
                    ELSE 0 
                 END)over (partition by oh.order_no,os.item) skip_alloc_qty,
                 rr.store_ord_mult,
                 ad.rowid
            FROM ordhead oh,
                 ordsku os,
                 ordloc ol, 
                 alloc_header ah,
                 alloc_detail ad,
                 repl_results rr
           WHERE oh.order_no = ol.order_no
             AND oh.order_no = os.order_no
             AND oh.order_no = ah.order_no
             AND oh.order_no = rr.order_no
             AND os.order_no = ol.order_no
             AND os.order_no = rr.order_no
             AND os.order_no = ah.order_no
             AND rr.alloc_no = ah.alloc_no
             AND ah.alloc_no = ad.alloc_no
             AND rr.location = ad.to_loc
             AND rr.source_wh = ol.location
             AND os.item = ol.item
             AND os.item = ah.item
             AND os.item = rr.item
             AND rr.item = ah.item
             AND rr.stock_cat='C'
             AND oh.order_no = I_order_no
             AND os.item     = I_item
        ORDER BY 1, /* Order number */
                 2, /* Item number */
                 3 DESC; /* descending order based on store_ord_mult_size  */
 
TYPE xdock_alloc_tab IS TABLE OF c_get_xdock_allocs%ROWTYPE;
tab_alloc xdock_alloc_tab;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_XDOCK_ALLOCS','ORDHEAD,ORDSKU,ORDLOC,ALLOC_HEADER,ALLOC_DETAIL,REPL_RESULTS', 'order_no: '||to_char(I_order_no)||', item: '||to_char(I_item));
   OPEN c_get_xdock_allocs;
   SQL_LIB.SET_MARK('FETCH','C_GET_XDOCK_ALLOCS','ORDHEAD,ORDSKU,ORDLOC,ALLOC_HEADER,ALLOC_DETAIL,REPL_RESULTS', 'order_no: '||to_char(I_order_no)||', item: '||to_char(I_item));
   FETCH c_get_xdock_allocs BULK COLLECT INTO tab_alloc;
   SQL_LIB.SET_MARK('CLOSE','C_GET_XDOCK_ALLOCS','ORDHEAD,ORDSKU,ORDLOC,ALLOC_HEADER,ALLOC_DETAIL,REPL_RESULTS', 'order_no: '||to_char(I_order_no)||', item: '||to_char(I_item));
   CLOSE c_get_xdock_allocs;
   
   for i in tab_alloc.FIRST..tab_alloc.LAST loop
      IF i = 1 then 
        L_remaining_order_qty := I_total_order_qty - tab_alloc(i).SKIP_ALLOC_QTY;
        L_remaining_alloc_qty := tab_alloc(i).TOTAL_ALLOC_QTY - tab_alloc(i).SKIP_ALLOC_QTY;
      END IF;
      
      IF tab_alloc(i).store_ord_mult_size <= L_excess_ord_qty THEN
         
         select  round((tab_alloc(i).qty_allocated * L_remaining_order_qty)/(L_remaining_alloc_qty * tab_alloc(i).store_ord_mult_size),0)
         into L_rounded_alloc_qty_in_som from dual;
 
         /* Decrease Total order and Total allocation buckets */
         L_remaining_alloc_qty := L_remaining_alloc_qty - tab_alloc(i).qty_allocated;
         L_remaining_order_qty := L_remaining_order_qty - (L_rounded_alloc_qty_in_som * tab_alloc(i).store_ord_mult_size);
   
         tab_alloc(i).qty_allocated := L_rounded_alloc_qty_in_som * tab_alloc(i).store_ord_mult_size;
      END IF;
   end loop;
   
   FORALL j in tab_alloc.FIRST..tab_alloc.LAST 
      UPDATE alloc_detail set qty_allocated = tab_alloc(j).qty_allocated 
          where rowid=tab_alloc(j).rowid;
   
   RETURN TRUE;   
EXCEPTION
when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.PRORATE_ALLOCATIONS',
                                            to_char(SQLCODE));
      RETURN FALSE;

END;
-------------------------------------------------------------------------------
FUNCTION PROCESS_XDOCK_ORDERS(O_error_message       OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              I_line_item_table     IN OUT NOCOPY line_item_tabletype,
                              I_order_no            IN     ordhead.order_no%TYPE,
                              I_skip_alloc_round    IN     VARCHAR2)
RETURN BOOLEAN IS

   /* This package will round any allocation qty's for stores associated with an xdock wh.
    * It will also adjust the ordloc qty for the wh if the total rounded allocated qty is
    * greater than the qty ordered to the wh.
    */

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_total_qty_allocated         NUMBER := 0;

   cursor C_GET_TOTAL_ALLOC_QTY(C_from_wh wh.wh%TYPE,
                                C_item    alloc_header.item%TYPE) is
      select NVL(SUM(ad.qty_allocated), -999)
        from alloc_header ah,
             alloc_detail ad
       where ah.order_no = I_order_no
         and ah.alloc_no = ad.alloc_no
         and ah.wh       = C_from_wh
         and ah.item     = C_item;

BEGIN
   if I_skip_alloc_round != 'Y' then 
      return TRUE;
   end if;
   
   FOR i IN 1..I_line_item_table.COUNT LOOP

      if I_line_item_table(i).xdock_wh_ind = 'Y' and I_line_item_table(i).loc_type = 'W' then
         L_total_qty_allocated   := 0;
         SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_ALLOC_QTY','ALLOC_HEADER,ALLOC_DETAIL', 'order_no: '||to_char(I_order_no));
         open C_GET_TOTAL_ALLOC_QTY(I_line_item_table(i).location,
                                    I_line_item_table(i).item) ;

         SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_ALLOC_QTY','ALLOC_HEADER,ALLOC_DETAIL', 'order_no: '||to_char(I_order_no));
         fetch C_GET_TOTAL_ALLOC_QTY into L_total_qty_allocated;
           
           if L_total_qty_allocated != -999 and I_line_item_table(i).qty_ordered > L_total_qty_allocated then 
              if PRORATE_ALLOCATIONS(O_error_message,
                                     I_order_no,
                                     I_line_item_table(i).item,
                                     I_line_item_table(i).qty_ordered,
                                     L_total_qty_allocated) = FALSE THEN
                    return FALSE;
               end if;                    
            end if;

         SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_ALLOC_QTY','ALLOC_HEADER,ALLOC_DETAIL', 'order_no: '||to_char(I_order_no));
         close C_GET_TOTAL_ALLOC_QTY;
       end if;
    END LOOP;
    return TRUE;
EXCEPTION
when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ROUNDING_SQL.PRORATE_ALLOCATIONS',
                                            to_char(SQLCODE));
      return FALSE;

END;            
-------------------------------------------------------------------------------
END ROUNDING_SQL;
/