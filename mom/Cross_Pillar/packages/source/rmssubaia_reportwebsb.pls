CREATE OR REPLACE PACKAGE BODY ReportLocatorServiceProviderIm AS
--------------------------------------------------------------------------------------------------------
PROCEDURE publishReportLocDesc(I_serviceOperationContext IN     "RIB_ServiceOpContext_REC",
                               I_businessObject          IN     "RIB_ReportLocDesc_REC",
                               O_serviceOperationStatus     OUT "RIB_ServiceOpStatus_REC",
                               O_businessObject             OUT "RIB_ReportLocRef_REC") IS

   L_program         VARCHAR2(64) := 'ReportLocatorServiceProviderIm.publishReportLocDesc';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_rpt_url         RETAIL_SERVICE_REPORT_URL.URL%TYPE;
   L_status_code     VARCHAR2(1) := NULL;

BEGIN
   if NOT AIA_PUB_REPORTS.GET_REPORT_URL(L_error_message,
                                         L_rpt_url,
                                         I_businessObject.report_ref_key)= FALSE then
      L_status_code := API_CODES.SUCCESS;
   end if;


   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

   O_businessObject := "RIB_ReportLocRef_REC"(0,                              --- rib_oid
                                              I_businessObject.report_ref_key, --- report_ref_key
                                              L_rpt_url                        --- report_url
                                             );

EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END publishReportLocDesc;

-------------------------------------------------------------------------------------------------------
PROCEDURE findReportLocDesc(I_serviceOperationContext IN     "RIB_ServiceOpContext_REC",
                            I_businessObject          IN     "RIB_ReportLocRef_REC",
                            O_serviceOperationStatus     OUT "RIB_ServiceOpStatus_REC",
                            O_businessObject             OUT "RIB_ReportLocDesc_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.findReportLocDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_serviceoperationstatus := NULL;
   O_businessObject := NULL;
   
   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
                                        
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END findReportLocDesc;
-------------------------------------------------------------------------------------------------------
END ReportLocatorServiceProviderIm;
/
