
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INTERFACE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------
-- Function Name : INSERT_INTERFACE_ERROR
-- Purpose       : Inserts errors into if_errors.  This function will 
--			 be called from both batch and other packages when
--                 information from staging or notification tables is
--                 inserted into base tables.  
-- Created On    : 2-19-97 for Retek 7.0 Interface
---------------------------------------------------------------------
FUNCTION INSERT_INTERFACE_ERROR (O_error_message IN OUT VARCHAR2,
                                 I_error         IN     IF_ERRORS.ERROR%TYPE,
                                 I_program_name  IN     IF_ERRORS.PROGRAM_NAME%TYPE, 
                                 I_unit_of_work  IN     IF_ERRORS.UNIT_OF_WORK%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
END INTERFACE_SQL;
/


