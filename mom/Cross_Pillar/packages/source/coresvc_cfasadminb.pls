create or replace PACKAGE BODY CORESVC_CFAS_ADMIN_SQL AS
  
TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
LP_s9t_errors_tab                   s9t_errors_tab_typ;

TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
LP_errors_tab errors_tab_typ;


L_error   BOOLEAN := FALSE;
L_GROUP_SET_table           VARCHAR2(255)    :='CFA_ATTRIB_GROUP_SET';
L_GROUP_SET_LABELS_table    VARCHAR2(255)    :='CFA_ATTRIB_GROUP_SET_LABELS';
L_GROUP_table               VARCHAR2(255)    :='CFA_ATTRIB_GROUP';
L_GROUP_LABELS_table        VARCHAR2(255)    :='CFA_ATTRIB_GROUP_LABELS';
L_REC_GROUP_table           VARCHAR2(255)    :='CFA_REC_GROUP';
L_REC_GROUP_LABELS_table    VARCHAR2(255)    :='CFA_REC_GROUP_LABELS';
L_date_format               VARCHAR2(16)     := 'YYYYMMDDHH24MISS';
LP_user                     SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
-----------------------------------------------------------------------------------

 cursor C_SVC_EXT_ENTITY(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
   select cfa_ext_entity_uk1.rowid  AS cfa_ext_entity_uk1_rid,
          st.rowid AS st_rid,
          upper(st.validation_func) as validation_func,
          upper(st.base_rms_table) as base_rms_table,
          st.process_id,
          st.row_seq,
          st.chunk_id,
          upper(st.action) AS action,
          st.process$status,
          upper(cfa_ext_entity_uk1.validation_func) as ext_entity_uk_validation_func
     from svc_ext_entity st,
          cfa_ext_entity cfa_ext_entity_uk1
    where st.process_id               = I_process_id
      and st.chunk_id                 = I_chunk_id
      and upper(st.base_rms_table)    = cfa_ext_entity_uk1.base_rms_table(+);

 cursor C_SVC_GROUP_SET(I_process_id NUMBER,
                        I_chunk_id NUMBER) is
   
   select t.fk_cfa_attrib_group_set_rid,
          t.fk_lang_rid,
          t.ext_entity_rid,
          t.stgs_action,
          t.stgs_row_seq,
          t.stgs_rid,
          t.stgs_group_set_id,
         -- LEAD(t.stgs_group_set_id, 1, 0) OVER (ORDER BY t.stgs_group_set_id) as next_GROUP_SET_ID,
          t.stgs_base_rms_table,
          t.stgs_display_seq,
          t.stgs_grp_set_view_name,
          t.stgs_qualifier_func,
          t.stgs_validation_func,
          t.stgs_default_func,
          t.stgs_stg_tbl_name,
          t.ext_entity_id,
          t.stgs_process_id,
          t.stgs_chunk_id,
          t.stgs_process$status,
          t.stgslbl_action,
          t.stgslbl_row_seq,
          t.stgslbl_rid,
          t.stgslbl_group_set_id,
          t.stgslbl_lang,
          t.stgslbl_label,
          t.stgslbl_process_id,
          t.stgslbl_chunk_id,
          row_number() over (partition by t.stgs_group_set_id order by t.stgs_group_set_id) as grp_set_rank,
          t.old_ext_entity_id,
          t.old_display_seq,
          t.old_group_set_view_name,
          t.old_staging_table_name,
          t.old_qualifier_function,
          t.old_validation_func,
          t.old_default_func,
          t.old_base_ind,
          t.old_active_ind,
          t.fk_cfa_grp_set_labels_rid,
          t.oldlbl_group_set_id,
          t.oldlbl_lang,
          t.oldlbl_label,
          t.data_integration_lang
   from 
        (-- Fetch SVC_ATTRIB_GROUP_SET records)
         select  cfa_attrib_group_set_fk.rowid                       as fk_cfa_attrib_group_set_rid,
                 lang.rowid                                          as fk_lang_rid,
                 ext_entity.rowid                                    as ext_entity_rid,
                 upper(stgs.action)                                  as stgs_action,
                 stgs.row_seq                                        as stgs_row_seq,
                 stgs.rowid                                          as stgs_rid,
                 stgs.group_set_id                                   as stgs_group_set_id,
                 upper(stgs.base_rms_table)                          as stgs_base_rms_table,
                 stgs.display_seq                                    as stgs_display_seq, 
                 upper(stgs.group_set_view_name)                     as stgs_grp_set_view_name,
                 upper(stgs.qualifier_func)                          as stgs_qualifier_func,
                 upper(stgs.validation_func)                         as stgs_validation_func,
                 upper(stgs.default_func)                            as stgs_default_func,
                 upper(stgs.staging_table_name)                      as stgs_stg_tbl_name,
                 ext_entity.ext_entity_id                            as ext_entity_id,
                 stgs.process_id                                     as stgs_process_id,
                 stgs.chunk_id                                       as stgs_chunk_id,
                 stgs.process$status                                 as stgs_process$status,
                 upper(stgslbl.action)                               as stgslbl_action,
                 stgslbl.row_seq                                     as stgslbl_row_seq,
                 stgslbl.rowid                                       as stgslbl_rid,
                 stgslbl.group_set_id                                as stgslbl_group_set_id,
                 stgslbl.lang                                        as stgslbl_lang,
                 stgslbl.label                                       as stgslbl_label,
                 stgslbl.process_id                                  as stgslbl_process_id,
                 stgslbl.chunk_id                                    as stgslbl_chunk_id,
                 cfa_attrib_group_set_fk.ext_entity_id               as old_ext_entity_id,
                 cfa_attrib_group_set_fk.display_seq                 as old_display_seq,
                 upper(cfa_attrib_group_set_fk.group_set_view_name)  as old_group_set_view_name,
                 upper(cfa_attrib_group_set_fk.staging_table_name)   as old_staging_table_name,
                 upper(cfa_attrib_group_set_fk.qualifier_func)       as old_qualifier_function,
                 upper(cfa_attrib_group_set_fk.validation_func)      as old_validation_func,
                 upper(cfa_attrib_group_set_fk.default_func)         as old_default_func,
                 cfa_attrib_group_set_fk.base_ind                    as old_base_ind,
                 cfa_attrib_group_set_fk.active_ind                  as old_active_ind,
                 cfa_grp_set_labels_fk.rowid                         as fk_cfa_grp_set_labels_rid,
                 cfa_grp_set_labels_fk.group_set_id                  as oldlbl_group_set_id,
                 cfa_grp_set_labels_fk.lang                          as oldlbl_lang,
                 cfa_grp_set_labels_fk.label                         as oldlbl_label,
                 sysopt.data_integration_lang                        as data_integration_lang
            from svc_attrib_group_set                stgs,
                 svc_attrib_group_set_labels         stgslbl,
                 cfa_attrib_group_set                cfa_attrib_group_set_fk,
                 cfa_ext_entity                      ext_entity,
                 cfa_attrib_group_set_labels         cfa_grp_set_labels_fk,
                 lang                                lang,
                 system_options                      sysopt
              where stgs.process_id        = I_process_id
           and stgs.chunk_id               = I_chunk_id
           and stgs.process_id             = stgslbl.process_id (+)
           and stgs.chunk_id               = stgslbl.chunk_id (+)
           and stgs.group_set_id           = stgslbl.group_set_id(+) 
           and stgs.group_set_id           = cfa_attrib_group_set_fk.group_set_id (+)
           and UPPER(stgs.base_rms_table)  = ext_entity.BASE_RMS_TABLE(+)
           and stgslbl.group_set_id        = cfa_grp_set_labels_fk.group_set_id(+)
           and stgslbl.lang                = cfa_grp_set_labels_fk.lang(+)
           and stgslbl.lang                = lang.lang(+)
           and (stgs.process$status         = 'N' or stgs.process$status is NULL)
    and (stgslbl.process$status      = 'N' or stgslbl.process$status  is NULL)

    UNION ALL
         -- Fetch SVC_ATTRIB_GROUP_SET_LABELS records)
       select  cfa_attrib_group_set_fk.rowid                                  as fk_cfa_attrib_group_set_rid,
               lang.rowid                                                     as fk_lang_rid,
               ext_entity.rowid                                               as ext_entity_rid,
               UPPER(stgs.action)                                             as stgs_action,
               stgs.row_seq                                                   as stgs_row_seq,
               stgs.rowid                                                     as stgs_rid,
               NVL(stgs.group_set_id,cfa_attrib_group_set_fk.group_set_id)    as stgs_group_set_id,
               UPPER(stgs.base_rms_table)                                     as stgs_base_rms_table,
               stgs.display_seq                                               as stgs_display_seq, 
               UPPER(stgs.group_set_view_name)                                as stgs_grp_set_view_name,
               UPPER(stgs.qualifier_func)                                     as stgs_qualifier_func,
               UPPER(stgs.validation_func)                                    as stgs_validation_func,
               UPPER(stgs.default_func)                                       as stgs_default_func,
               UPPER(stgs.staging_table_name)                                 as stgs_stg_tbl_name,
               ext_entity.ext_entity_id                                       as ext_entity_id,
               stgs.process_id                                                as stgs_process_id,
               stgs.chunk_id                                                  as stgs_chunk_id,
               stgs.process$status                                            as stgs_process$status,
               UPPER(stgslbl.action)                                          as stgslbl_action,
               stgslbl.row_seq                                                as stgslbl_row_seq,
               stgslbl.rowid                                                  as stgslbl_rid,
               stgslbl.group_set_id                                           as stgslbl_group_set_id,
               stgslbl.lang                                                   as stgslbl_lang,
               stgslbl.label                                                  as stgslbl_label,
               stgslbl.process_id                                             as stgslbl_process_id,
               stgslbl.chunk_id                                               as stgslbl_chunk_id,
               cfa_attrib_group_set_fk.ext_entity_id                          as old_ext_entity_id,
               cfa_attrib_group_set_fk.display_seq                            as old_display_seq,
               UPPER(cfa_attrib_group_set_fk.group_set_view_name)             as old_group_set_view_name,
               UPPER(cfa_attrib_group_set_fk.staging_table_name)              as old_staging_table_name,
               UPPER(cfa_attrib_group_set_fk.qualifier_func)                  as old_qualifier_function,
               UPPER(cfa_attrib_group_set_fk.validation_func)                 as old_validation_func,
               UPPER(cfa_attrib_group_set_fk.default_func)                    as old_default_func,
               cfa_attrib_group_set_fk.base_ind                               as old_base_ind,
               cfa_attrib_group_set_fk.active_ind                             as old_active_ind,
               cfa_grp_set_labels_fk.rowid                                    as fk_cfa_grp_set_labels_rid,
               cfa_grp_set_labels_fk.group_set_id                             as oldlbl_group_set_id,
               cfa_grp_set_labels_fk.lang                                     as oldlbl_lang,
               cfa_grp_set_labels_fk.label                                    as oldlbl_label,
                sysopt.data_integration_lang                                  as data_integration_lang
          from svc_attrib_group_set_labels        stgslbl,
               svc_attrib_group_set               stgs,
               cfa_attrib_group_set               cfa_attrib_group_set_fk,
               cfa_ext_entity                     ext_entity,
               cfa_attrib_group_set_labels        cfa_grp_set_labels_fk,
               lang                               lang,
               system_options                     sysopt
         where stgslbl.process_id          = I_process_id
           and stgslbl.chunk_id            = I_chunk_id
           and stgslbl.process_id          = stgs.process_id (+)
           and stgslbl.chunk_id            = stgs.chunk_id(+)
           and stgslbl.group_set_id        = stgs.group_set_id(+)
           and stgs.group_set_id           = cfa_attrib_group_set_fk.group_set_id (+)
           and UPPER(stgs.base_rms_table)  = ext_entity.base_rms_table(+)
           and stgslbl.group_set_id        = cfa_grp_set_labels_fk.group_set_id(+)
           and stgslbl.lang                = cfa_grp_set_labels_fk.lang(+)
           and stgslbl.lang                = lang.lang(+)
           and stgs.group_set_id is null
           and (stgs.process$status         = 'N' or stgs.process$status is NULL)
           and (stgslbl.process$status      = 'N' or stgslbl.process$status  is NULL)) t;
      
      
cursor C_SVC_ATTRIB_GROUP(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
select t.fk_cfa_attrib_group_rid,
       t.fk_lang_rid,
       t.attrib_group_set_rid,
       t.stgrp_action,
       t.stgrp_row_seq,
       t.stgrp_rid,
       t.stgrp_group_id,
       t.stgrp_group_set_id,
       t.stgrp_grp_view_name,
       t.stgrp_display_seq, 
       t.group_set_id,
       t.stgrp_process_id,
       t.stgrp_chunk_id,
       t.stgrp_process$status,
       t.stgrplbl_action,
       t.stgrplbl_row_seq,
       t.stgrplbl_rid,
       t.stgrplbl_group_id,
       t.stgrplbl_lang,
       t.stgrplbl_label,
       t.stgrplbl_process_id,
       t.stgrplbl_chunk_id,
       t.old_group_id,
       t.old_group_set_id,
       t.old_display_seq,
       t.old_group_view_name,
       t.old_base_ind,
       t.old_active_ind,
       t.fk_cfa_grp_labels_rid,
       t.oldlbl_group_id,
       t.oldlbl_lang,
       t.oldlbl_label,
       t.data_integration_lang,
       row_number() over (partition by t.stgrp_group_id order by t.stgrp_group_id) as grp_rank
from 
     (-- Fetch SVC_ATTRIB_GROUP records)
      select  cfa_attrib_group_fk.rowid                           as fk_cfa_attrib_group_rid,
              lang.rowid                                          as fk_lang_rid,
              cfa_attrib_group_set.rowid                          as attrib_group_set_rid,
              upper(stgrp.action)                                 as stgrp_action,
              stgrp.row_seq                                       as stgrp_row_seq,
              stgrp.rowid                                         as stgrp_rid,
              stgrp.group_id                                      as stgrp_group_id,
              stgrp.group_set_id       as stgrp_group_set_id,
              upper(stgrp.group_view_name)                        as stgrp_grp_view_name,
              stgrp.display_seq                                   as stgrp_display_seq, 
              cfa_attrib_group_set.group_set_id                   as group_set_id,
              stgrp.process_id                                    as stgrp_process_id,
              stgrp.chunk_id                                      as stgrp_chunk_id,
              stgrp.process$status                                as stgrp_process$status,
              upper(stgrplbl.action)                              as stgrplbl_action,
              stgrplbl.row_seq                                    as stgrplbl_row_seq,
              stgrplbl.rowid                                      as stgrplbl_rid,
              stgrplbl.group_id                                   as stgrplbl_group_id,
              stgrplbl.lang                                       as stgrplbl_lang,
              stgrplbl.label                                      as stgrplbl_label,
              stgrplbl.process_id                                 as stgrplbl_process_id,
              stgrplbl.chunk_id                                   as stgrplbl_chunk_id,
              cfa_attrib_group_fk.group_id                        as old_group_id,
              cfa_attrib_group_fk.group_set_id                    as old_group_set_id,
              cfa_attrib_group_fk.display_seq                     as old_display_seq,
              upper(cfa_attrib_group_fk.group_view_name)          as old_group_view_name,
              cfa_attrib_group_fk.base_ind                        as old_base_ind,    
              cfa_attrib_group_fk.active_ind                      as old_active_ind,    
              cfa_grp_labels_fk.rowid                             as fk_cfa_grp_labels_rid,
              cfa_grp_labels_fk.group_id                          as oldlbl_group_id,
              cfa_grp_labels_fk.lang                              as oldlbl_lang,
              cfa_grp_labels_fk.label                             as oldlbl_label,
              sysopt.data_integration_lang                        as data_integration_lang
        from  svc_attrib_group                        stgrp,
              svc_attrib_group_labels                 stgrplbl,
              cfa_attrib_group                        cfa_attrib_group_fk,
              cfa_attrib_group_set                    cfa_attrib_group_set,
              cfa_attrib_group_labels                 cfa_grp_labels_fk,
              lang                                    lang,
              system_options                          sysopt
        where stgrp.process_id              = I_process_id
          and stgrp.chunk_id                = I_chunk_id
          and stgrp.process_id              = stgrplbl.process_id (+)
          and stgrp.chunk_id                = stgrplbl.chunk_id (+)
          and stgrp.group_id                = stgrplbl.group_id(+) 
          and stgrp.group_id                = cfa_attrib_group_fk.group_id (+)
          and stgrp.group_set_id            = cfa_attrib_group_set.group_set_id(+)
          and stgrplbl.group_id             = cfa_grp_labels_fk.group_id(+)
          and stgrplbl.lang                 = cfa_grp_labels_fk.lang(+)
          and stgrplbl.lang                 = lang.lang(+)
          and (stgrplbl.process$status       = 'N' or stgrplbl.process$status is NULL)
          and (stgrp.process$status          = 'N' or stgrp.process$status is NULL)
 UNION ALL
      -- Fetch SVC_ATTRIB_GROUP_LABELS records)
     select  cfa_attrib_group_fk.rowid                            as fk_cfa_attrib_group_rid,
              lang.rowid                                          as fk_lang_rid,
              cfa_attrib_group_set.rowid                          as attrib_group_set_rid,
              upper(stgrp.action)                                 as stgrp_action,
              stgrp.row_seq                                       as stgrp_row_seq,
              stgrp.rowid                                         as stgrp_rid,
              stgrp.group_id                                      as stgrp_group_id,
              stgrp.group_set_id       as stgrp_group_set_id,
              upper(stgrp.group_view_name)                        as stgrp_grp_view_name,
              stgrp.display_seq                                   as stgrp_display_seq, 
              cfa_attrib_group_set.group_set_id                   as group_set_id,
              stgrp.process_id                                    as stgrp_process_id,
              stgrp.chunk_id                                      as stgrp_chunk_id,
              stgrp.process$status                                as stgrp_process$status,
              upper(stgrplbl.action)                               as stgrplbl_action,
              stgrplbl.row_seq                                    as stgrplbl_row_seq,
              stgrplbl.rowid                                      as stgrplbl_rid,
              stgrplbl.group_id                                   as stgrplbl_group_id,
              stgrplbl.lang                                       as stgrplbl_lang,
              stgrplbl.label                                      as stgrplbl_label,
              stgrplbl.process_id                                 as stgrplbl_process_id,
              stgrplbl.chunk_id                                   as stgrplbl_chunk_id,
              cfa_attrib_group_fk.group_id                        as old_group_id,
              cfa_attrib_group_fk.display_seq                     as old_display_seq,
              cfa_attrib_group_fk.group_set_id                    as old_group_set_id,
              upper(cfa_attrib_group_fk.group_view_name)          as old_group_view_name,
              cfa_attrib_group_fk.base_ind                        as old_base_ind,    
       cfa_attrib_group_fk.active_ind                      as old_active_ind,    
              cfa_grp_labels_fk.rowid                             as fk_cfa_grp_labels_rid,
              cfa_grp_labels_fk.group_id                          as oldlbl_group__id,
              cfa_grp_labels_fk.lang                              as oldlbl_lang,
              cfa_grp_labels_fk.label                             as oldlbl_label,
              sysopt.data_integration_lang                        as data_integration_lang
         from svc_attrib_group_labels            stgrplbl,
              svc_attrib_group                   stgrp,
              cfa_attrib_group                   cfa_attrib_group_fk,
              cfa_attrib_group_set               cfa_attrib_group_set,
              cfa_attrib_group_labels            cfa_grp_labels_fk,
              lang                               lang,
              system_options                     sysopt
      where stgrplbl.process_id          = I_process_id
        and stgrplbl.chunk_id            = I_chunk_id
        and stgrplbl.process_id          = stgrp.process_id (+)
        and stgrplbl.chunk_id            = stgrp.chunk_id(+)
        and stgrplbl.group_id        = stgrp.group_id(+)
        and stgrplbl.group_id        = cfa_attrib_group_fk.group_id (+)
        and stgrp.group_set_id            = cfa_attrib_group_set.group_set_id(+)
        and stgrplbl.group_id        = cfa_grp_labels_fk.group_id(+)
        and stgrplbl.lang                = cfa_grp_labels_fk.lang(+)
        and stgrplbl.lang                = lang.lang(+)
        and stgrp.group_id is null
        and (stgrplbl.process$status       = 'N' or stgrplbl.process$status is NULL)
        and (stgrp.process$status          = 'N' or stgrp.process$status is NULL)) t;
        
cursor C_SVC_REC_GROUP(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
select  t.fk_cfa_rec_group_rid,
 t.fk_lang_rid,
 t.stgrecgrp_action,
 t.stgrecgrp_row_seq,
 t.stgrecgrp_rid,
 t.stgrecgrp_rec_group_id,
 t.stgrecgrp_rec_group_name,
 t.stgrecgrp_query_type,
 t.stgrecgrp_table_name,
 t.stgrecgrp_column_1,
 t.stgrecgrp_column_2,
 t.stgrecgrp_where_col_1,
 t.stgrecgrp_where_operator_1,
 t.stgrecgrp_where_cond_1,
 t.stgrecgrp_where_col_2,
 t.stgrecgrp_where_operator_2,
 t.stgrecgrp_where_cond_2,
 t.stgrecgrp_process_id,
 t.stgrecgrp_chunk_id,
 t.stgrecgrp_process$status,
 t.stgreclabel_action,
 t.stgreclabel_row_seq,
 t.stgreclabel_rid,
 t.stgreclabel_rec_group_id,
 t.stgreclabel_lang,
 t.stgreclabel_lov_title,
 t.stgreclabel_lov_col1_header,
 t.stgreclabel_lov_col2_header,
 t.stgreclabel_process_id,
 t.stgreclabel_chunk_id,
 t.stgreclabel_process$status,
 t.old_rec_group_name,
 t.old_query_type,
 t.old_query,
 t.old_table_name,
 t.old_column_1,
 t.old_column_2,
 t.old_where_col_1,
 t.old_where_operator_1,
 t.old_where_cond_1,
 t.old_where_col_2,
 t.old_where_operator_2,
 t.old_where_cond_2,
 t.old_base_ind,
 t.fk_cfa_rec_group_labels_rid,
 t.oldlbl_rec_group_id,
 t.oldlbl_lang,
 t.oldlbl_lov_title,
 t.oldlbl_col1_header,
 t.oldlbl_col2_header,
 t.data_integration_lang,
        row_number() over (partition by T.stgrecgrp_rec_group_id order by T.stgrecgrp_rec_group_id) as recgrp_rank
   from ( select  cfa_rec_group_fk.rowid                    as fk_cfa_rec_group_rid,
                  lang.rowid                                as fk_lang_rid,
                  upper(stgrecgrp.action)                   as stgrecgrp_action,
                  stgrecgrp.row_seq                         as stgrecgrp_row_seq,
            stgrecgrp.rowid                           as stgrecgrp_rid,
           stgrecgrp.rec_group_id       as stgrecgrp_rec_group_id,
           upper(stgrecgrp.rec_group_name)           as stgrecgrp_rec_group_name,
           upper(stgrecgrp.query_type)               as stgrecgrp_query_type,
           upper(stgrecgrp.table_name)               as stgrecgrp_table_name,
           upper(stgrecgrp.column_1)                 as stgrecgrp_column_1,
           upper(stgrecgrp.column_2)      as stgrecgrp_column_2,
           upper(stgrecgrp.where_col_1)              as stgrecgrp_where_col_1,
           upper(stgrecgrp.where_operator_1)         as stgrecgrp_where_operator_1,
           upper(stgrecgrp.where_cond_1)             as stgrecgrp_where_cond_1,
                  upper(stgrecgrp.where_col_2)              as stgrecgrp_where_col_2,
                  upper(stgrecgrp.where_operator_2)         as stgrecgrp_where_operator_2,
                  upper(stgrecgrp.where_cond_2)             as stgrecgrp_where_cond_2,
           stgrecgrp.process_id                      as stgrecgrp_process_id,
           stgrecgrp.chunk_id                        as stgrecgrp_chunk_id,
           stgrecgrp.process$status                  as stgrecgrp_process$status,
           upper(stgreclabel.action)                 as stgreclabel_action,
           stgreclabel.row_seq                       as stgreclabel_row_seq,
           stgreclabel.rowid                         as stgreclabel_rid,
           stgreclabel.rec_group_id                  as stgreclabel_rec_group_id,
           stgreclabel.lang                          as stgreclabel_lang,
           stgreclabel.lov_title                     as stgreclabel_lov_title,
           stgreclabel.lov_col1_header               as stgreclabel_lov_col1_header,
           stgreclabel.lov_col2_header               as stgreclabel_lov_col2_header,
           stgreclabel.process_id                    as stgreclabel_process_id,
           stgreclabel.chunk_id                      as stgreclabel_chunk_id,
           stgreclabel.process$status                as stgreclabel_process$status,
           cfa_rec_group_fk.rec_group_name           as old_rec_group_name,
           cfa_rec_group_fk.query_type               as old_query_type,
           cfa_rec_group_fk.query             as old_query,
           cfa_rec_group_fk.table_name               as old_table_name,
           cfa_rec_group_fk.column_1                 as old_column_1,
           cfa_rec_group_fk.column_2                 as old_column_2,
           cfa_rec_group_fk.where_col_1              as old_where_col_1,
           cfa_rec_group_fk.where_operator_1         as old_where_operator_1,
           cfa_rec_group_fk.where_cond_1             as old_where_cond_1,
           cfa_rec_group_fk.where_col_2              as old_where_col_2,
           cfa_rec_group_fk.where_operator_2         as old_where_operator_2,
                  cfa_rec_group_fk.where_cond_2             as old_where_cond_2,
                  cfa_rec_group_fk.base_ind                 as old_base_ind,
                  cfa_rec_group_labels_fk.rowid             as fk_cfa_rec_group_labels_rid,
                  cfa_rec_group_labels_fk.rec_group_id      as oldlbl_rec_group_id,
                  cfa_rec_group_labels_fk.lang              as oldlbl_lang,
                  cfa_rec_group_labels_fk.lov_title         as oldlbl_lov_title,
                  cfa_rec_group_labels_fk.lov_col1_header   as oldlbl_col1_header,
                  cfa_rec_group_labels_fk.lov_col2_header   as oldlbl_col2_header,
                  sysopt.data_integration_lang              as data_integration_lang
             from svc_rec_group           stgrecgrp,
                  svc_rec_group_labels    stgreclabel,
                  cfa_rec_group               cfa_rec_group_fk,
                  cfa_rec_group_labels        cfa_rec_group_labels_fk,
                  lang                        lang,
                  system_options              sysopt
            where stgrecgrp.process_id            = I_process_id
              and stgrecgrp.chunk_id              = I_chunk_id
              and stgrecgrp.process_id            = stgreclabel.process_id(+)
              and stgrecgrp.chunk_id              = stgreclabel.chunk_id(+)
              and stgrecgrp.rec_group_id          =stgreclabel.rec_group_id(+)
              and stgrecgrp.rec_group_id          = cfa_rec_group_fk.rec_group_id(+)
              and stgreclabel.rec_group_id    = cfa_rec_group_labels_fk.rec_group_id(+)
              and stgreclabel.lang            = cfa_rec_group_labels_fk.lang(+)
              and stgreclabel.lang            = lang.lang(+)
              and (stgreclabel.process$status  = 'N' or stgreclabel.process$status is NULL)
              and (stgrecgrp.process$status  = 'N' or stgrecgrp.process$status is NULL)
      union all
          select  cfa_rec_group_fk.rowid                    as fk_cfa_rec_group_rid,
                  lang.rowid                                as fk_lang_rid,
                  upper(stgrecgrp.action)                   as stgrecgrp_action,
                  stgrecgrp.row_seq                         as stgrecgrp_row_seq,
                  stgrecgrp.rowid                           as stgrecgrp_rid,
                  stgrecgrp.rec_group_id              as stgrecgrp_rec_group_id,
                  upper(stgrecgrp.rec_group_name)           as stgrecgrp_rec_group_name,
                  upper(stgrecgrp.query_type)               as stgrecgrp_query_type,
                  upper(stgrecgrp.table_name)               as stgrecgrp_table_name,
                  upper(stgrecgrp.column_1)                 as stgrecgrp_column_1,
                  upper(stgrecgrp.column_2)      as stgrecgrp_column_2,
                  upper(stgrecgrp.where_col_1)              as stgrecgrp_where_col_1,
                  upper(stgrecgrp.where_operator_1)         as stgrecgrp_where_operator_1,
                  upper(stgrecgrp.where_cond_1)             as stgrecgrp_where_cond_1,
                  upper(stgrecgrp.where_col_2)              as stgrecgrp_where_col_2,
                  upper(stgrecgrp.where_operator_2)         as stgrecgrp_where_operator_2,
                  upper(stgrecgrp.where_cond_2)             as stgrecgrp_where_cond_2,
           stgrecgrp.process_id                      as stgrecgrp_process_id,
           stgrecgrp.chunk_id                        as stgrecgrp_chunk_id,
           stgrecgrp.process$status                  as stgrecgrp_process$status,
           upper(stgreclabel.action)                 as stgreclabel_action,
           stgreclabel.row_seq                       as stgreclabel_row_seq,
           stgreclabel.rowid                         as stgreclabel_rid,
           stgreclabel.rec_group_id                  as stgreclabel_rec_group_id,
           stgreclabel.lang                          as stgreclabel_lang,
           stgreclabel.lov_title                     as stgreclabel_lov_title,
           stgreclabel.lov_col1_header               as stgreclabel_lov_col1_header,
           stgreclabel.lov_col2_header               as stgreclabel_lov_col2_header,
           stgreclabel.process_id                    as stgreclabel_process_id,
           stgreclabel.chunk_id                      as stgreclabel_chunk_id,
           stgreclabel.process$status                as stgreclabel_process$status,
           cfa_rec_group_fk.rec_group_name           as old_rec_group_name,
           cfa_rec_group_fk.query_type               as old_query_type,
           cfa_rec_group_fk.query             as old_query,
           cfa_rec_group_fk.table_name               as old_table_name,
           cfa_rec_group_fk.column_1                 as old_column_1,
           cfa_rec_group_fk.column_2                 as old_column_2,
           cfa_rec_group_fk.where_col_1              as old_where_col_1,
           cfa_rec_group_fk.where_operator_1         as old_where_operator_1,
           cfa_rec_group_fk.where_cond_1             as old_where_cond_1,
           cfa_rec_group_fk.where_col_2              as old_where_col_2,
           cfa_rec_group_fk.where_operator_2         as old_where_operator_2,
                  cfa_rec_group_fk.where_cond_2             as old_where_cond_2,
                  cfa_rec_group_fk.base_ind                 as old_base_ind,
                  cfa_rec_group_labels_fk.rowid             as fk_cfa_rec_group_labels_rid,
                  cfa_rec_group_labels_fk.rec_group_id      as oldlbl_rec_group_id,
                  cfa_rec_group_labels_fk.lang              as oldlbl_lang,
                  cfa_rec_group_labels_fk.lov_title         as oldlbl_lov_title,
                  cfa_rec_group_labels_fk.lov_col1_header   as oldlbl_col1_header,
                  cfa_rec_group_labels_fk.lov_col2_header   as oldlbl_col2_header,
                  sysopt.data_integration_lang              as data_integration_lang
             from svc_rec_group_labels        stgreclabel,
                  svc_rec_group               stgrecgrp,
                  cfa_rec_group               cfa_rec_group_fk,
                  cfa_rec_group_labels        cfa_rec_group_labels_fk,
                  lang                        lang,
                  system_options              sysopt
            where stgreclabel.process_id          = I_process_id
              and stgreclabel.chunk_id            = I_chunk_id
              and stgreclabel.process_id          = stgrecgrp.process_id (+)
              and stgreclabel.chunk_id            = stgrecgrp.chunk_id(+)
              and stgreclabel.rec_group_id        = stgrecgrp.rec_group_id(+)
              and stgrecgrp.rec_group_id              = cfa_rec_group_fk.rec_group_id (+)
              and stgreclabel.rec_group_id        = cfa_rec_group_labels_fk.rec_group_id(+)
              and stgreclabel.lang                = cfa_rec_group_labels_fk.lang(+)
              and stgreclabel.lang                = lang.lang(+)
              and stgrecgrp.rec_group_id is null
              and (stgreclabel.process$status  = 'N' or stgreclabel.process$status is NULL)
              and (stgrecgrp.process$status  = 'N' or stgrecgrp.process$status is NULL)) t;
          
        
-------------------------------------------------------------   
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
-----------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets                      s9t_pkg.names_map_typ;
   EXT_ENTITY_cols               s9t_pkg.names_map_typ;
   GROUP_SET_LABELS_cols         S9T_PKG.NAMES_MAP_TYP;
   GROUP_SET_cols                S9T_PKG.NAMES_MAP_TYP;
   ATTRIB_GROUP_LABELS_cols      S9T_PKG.NAMES_MAP_TYP;
   ATTRIB_GROUP_cols             S9T_PKG.NAMES_MAP_TYP;
   REC_LABELS_cols               s9t_pkg.names_map_typ;
   REC_GROUP_cols                s9t_pkg.names_map_typ;
BEGIN
   L_sheets                                    := s9t_pkg.get_sheet_names(I_file_id);
   EXT_ENTITY_cols                             := s9t_pkg.get_col_names(I_file_id,EXT_ENTITY_sheet);
   EXT_ENTITY$ACTION                           := EXT_ENTITY_cols('ACTION');
   EXT_ENTITY$BASE_RMS_TABLE                   := EXT_ENTITY_cols('BASE_RMS_TABLE'); 
   EXT_ENTITY$VALIDATION_FUNC                  := EXT_ENTITY_cols('VALIDATION_FUNC');
   ----------------------------------------------------------------------------------------------
   GROUP_SET_LABELS_cols                       := s9t_pkg.get_col_names(I_file_id,GROUP_SET_LABELS_sheet);
   GROUP_SET_LABELS$ACTION                     := GROUP_SET_LABELS_cols('ACTION');
   GROUP_SET_LABELS$GROUP_SET_ID               := GROUP_SET_LABELS_cols('GROUP_SET_ID');
   GROUP_SET_LABELS$LANG                       := GROUP_SET_LABELS_cols('LANG');
   GROUP_SET_LABELS$LABEL                      := GROUP_SET_LABELS_cols('LABEL');
   GROUP_SET_cols                              := s9t_pkg.get_col_names(I_file_id,GROUP_SET_sheet);
   GROUP_SET$ACTION                            := GROUP_SET_cols('ACTION');
   GROUP_SET$GROUP_SET_ID                      := GROUP_SET_cols('GROUP_SET_ID');
   GROUP_SET$BASE_RMS_TABLE                    := GROUP_SET_cols('BASE_RMS_TABLE');
   GROUP_SET$DISPLAY_SEQ                       := GROUP_SET_cols('DISPLAY_SEQ');
   GROUP_SET$GROUP_SET_VIEW_NAME               := GROUP_SET_cols('GROUP_SET_VIEW_NAME');
   GROUP_SET$STAGING_TABLE_NAME                := GROUP_SET_cols('STAGING_TABLE_NAME');
   GROUP_SET$QUALIFIER_FUNC                    := GROUP_SET_cols('QUALIFIER_FUNC');
   GROUP_SET$VALIDATION_FUNC                   := GROUP_SET_cols('VALIDATION_FUNC');
   GROUP_SET$DEFAULT_FUNC                      := GROUP_SET_cols('DEFAULT_FUNC');
   --------------------------------------------------------------------------------------
   ATTRIB_GROUP_LABELS_cols                    := s9t_pkg.get_col_names(I_file_id,ATTRIB_GROUP_LABELS_sheet);
   ATTRIB_GROUP_LABELS$ACTION                  := ATTRIB_GROUP_LABELS_cols('ACTION');
   ATTRIB_GROUP_LABELS$GROUP_ID                := ATTRIB_GROUP_LABELS_cols('GROUP_ID');
   ATTRIB_GROUP_LABELS$LANG                    := ATTRIB_GROUP_LABELS_cols('LANG');
   ATTRIB_GROUP_LABELS$LABEL                   := ATTRIB_GROUP_LABELS_cols('LABEL');   
   ATTRIB_GROUP_cols                           := s9t_pkg.get_col_names(I_file_id,ATTRIB_GROUP_sheet);
   ATTRIB_GROUP$ACTION                         := ATTRIB_GROUP_cols('ACTION');
   ATTRIB_GROUP$GROUP_ID                       := ATTRIB_GROUP_cols('GROUP_ID');
   ATTRIB_GROUP$GROUP_SET_ID          := ATTRIB_GROUP_cols('GROUP_SET_ID');
   ATTRIB_GROUP$GROUP_VIEW_NAME                := ATTRIB_GROUP_cols('GROUP_VIEW_NAME');
   ATTRIB_GROUP$DISPLAY_SEQ                    := ATTRIB_GROUP_cols('DISPLAY_SEQ');
   --ATTRIB_GROUP$VALIDATION_FUNC                := ATTRIB_GROUP_cols('VALIDATION_FUNC');
   -------------------------------------------------------------------------------------
   REC_LABELS_cols                      := s9t_pkg.get_col_names(I_file_id,REC_LABELS_sheet);
   REC_LABELS$ACTION                    := REC_LABELS_cols('ACTION');
   REC_LABELS$LANG                      := REC_LABELS_cols('LANG');
   REC_LABELS$LOV_TITLE                        := REC_LABELS_cols('LOV_TITLE');
   REC_LABELS$LOV_COL1_HEADER                  := REC_LABELS_cols('LOV_COL1_HEADER');
   REC_LABELS$LOV_COL2_HEADER                  := REC_LABELS_cols('LOV_COL2_HEADER');
   REC_LABELS$REC_GROUP_ID                     := REC_LABELS_cols('REC_GROUP_ID');
   REC_GROUP_cols                              := s9t_pkg.get_col_names(I_file_id,REC_GROUP_sheet);
   REC_GROUP$ACTION                     := REC_GROUP_cols('ACTION');
   REC_GROUP$REC_GROUP_ID                      := REC_GROUP_cols('REC_GROUP_ID');
   REC_GROUP$REC_GROUP_NAME                    := REC_GROUP_cols('REC_GROUP_NAME');
   REC_GROUP$TABLE_NAME                        := REC_GROUP_cols('TABLE_NAME');
   REC_GROUP$QUERY_TYPE                        := REC_GROUP_cols('QUERY_TYPE');
   REC_GROUP$COLUMN_1                          := REC_GROUP_cols('COLUMN_1');
   REC_GROUP$COLUMN_2                          := REC_GROUP_cols('COLUMN_2');
   REC_GROUP$WHERE_COL_1                       := REC_GROUP_cols('WHERE_COL_1');
   REC_GROUP$WHERE_OPERATOR_1                  := REC_GROUP_cols('WHERE_OPERATOR_1');   
   REC_GROUP$WHERE_COND_1                      := REC_GROUP_cols('WHERE_COND_1');
   REC_GROUP$WHERE_COL_2                       := REC_GROUP_cols('WHERE_COL_2');
   REC_GROUP$WHERE_OPERATOR_2                  := REC_GROUP_cols('WHERE_OPERATOR_2');
   REC_GROUP$WHERE_COND_2                      := REC_GROUP_cols('WHERE_COND_2');   
      
END POPULATE_NAMES;
------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||LP_user||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   
   L_file.add_sheet(EXT_ENTITY_sheet);
   L_file.sheets(l_file.get_sheet_index(EXT_ENTITY_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                        ,'BASE_RMS_TABLE'
                                                                                        ,'VALIDATION_FUNC');
   L_file.add_sheet(GROUP_SET_sheet);
   L_file.sheets(l_file.get_sheet_index(GROUP_SET_sheet)).column_headers := s9t_cells( 'ACTION',
                    'GROUP_SET_ID',
                                                                                       'BASE_RMS_TABLE',
                                                                                       'DISPLAY_SEQ',
                                                                                       'GROUP_SET_VIEW_NAME',
                                                                                       'STAGING_TABLE_NAME',
                                                                                       'QUALIFIER_FUNC',
                                                                                       'VALIDATION_FUNC',
                                                                                       'DEFAULT_FUNC'
                                                                                      );
 
   L_file.add_sheet(GROUP_SET_LABELS_sheet);
   L_file.sheets(l_file.get_sheet_index(GROUP_SET_LABELS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                              'GROUP_SET_ID',
                                                                                              'LANG',
                                                                                              'LABEL'
                                                                                         );
   L_file.add_sheet(ATTRIB_GROUP_sheet);
   L_file.sheets(l_file.get_sheet_index(ATTRIB_GROUP_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'GROUP_ID',
                                                                                          'GROUP_SET_ID',
                                                                                          'GROUP_VIEW_NAME',
                                                                                          'DISPLAY_SEQ'
                                                                                         );   
   L_file.add_sheet(ATTRIB_GROUP_LABELS_sheet);
   L_file.sheets(l_file.get_sheet_index(ATTRIB_GROUP_LABELS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                                 'GROUP_ID',
                                                                                                 'LANG',
                                                                                                 'LABEL');                      
   L_file.add_sheet(REC_GROUP_sheet);
   L_file.sheets(l_file.get_sheet_index(REC_GROUP_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                       'REC_GROUP_ID',
                                                                                       'REC_GROUP_NAME',
                 'QUERY_TYPE',
                                                                                       'TABLE_NAME',
                                                                                       'COLUMN_1',
                                                                                       'COLUMN_2',
                                                                                       'WHERE_COL_1',
                                                                                       'WHERE_OPERATOR_1',
                                                                                       'WHERE_COND_1',
                 'WHERE_COL_2',            
                                                                                       'WHERE_OPERATOR_2',
                                                                                       'WHERE_COND_2');
   L_file.add_sheet(REC_LABELS_sheet);
   L_file.sheets(l_file.get_sheet_index(REC_LABELS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                  'REC_GROUP_ID',
           'LANG',
           'LOV_TITLE',
           'LOV_COL1_HEADER',
                                                                                        'LOV_COL2_HEADER'
                                                                                       );
      
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_EXT_ENTITY( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = EXT_ENTITY_sheet )
   select s9t_row(s9t_cells(action_mod ,
                            base_rms_table,
                            validation_func))
     from cfa_ext_entity ;
END POPULATE_EXT_ENTITY;
--------------------------------------------------------------------
PROCEDURE POPULATE_GROUP_SET_LABELS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = GROUP_SET_LABELS_sheet )
      select s9t_row(s9t_cells(action_mod ,
                               group_set_id,
                               lang,
                               label
                             )
                     )
     from cfa_attrib_group_set_labels ;
END POPULATE_GROUP_SET_LABELS;
------------------------------------------------------------------------
PROCEDURE POPULATE_GROUP_SET( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name =  GROUP_SET_sheet)
       select s9t_row(s9t_cells(action_mod ,
                                attrib_group_set.group_set_id,
                                ext_entity.base_rms_table,
                                attrib_group_set.display_seq,
                                attrib_group_set.group_set_view_name,
                                attrib_group_set.staging_table_name,
                                attrib_group_set.qualifier_func,
                                attrib_group_set.validation_func,
                                attrib_group_set.default_func
                               )
                      )
          from cfa_attrib_group_set attrib_group_set,
               cfa_ext_entity ext_entity
     where attrib_group_set.EXT_ENTITY_ID = ext_entity.EXT_ENTITY_ID ;
END POPULATE_GROUP_SET;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ATTRIB_GROUP_LABELS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = ATTRIB_GROUP_LABELS_sheet )
   select s9t_row(s9t_cells(action_mod,
                           group_id,
                           lang,
                           label
                           )
                  )
     from cfa_attrib_group_labels ;
END POPULATE_ATTRIB_GROUP_LABELS;
-----------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ATTRIB_GROUP( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = ATTRIB_GROUP_sheet )
     select s9t_row(s9t_cells(action_mod,
                              attrib_group.group_id,
                              attrib_group.group_set_id,
                              attrib_group.group_view_name,
                              attrib_group.display_seq
                            )
                    )
       from cfa_attrib_group     attrib_group;
    -- cfa_attrib_group_set attrib_group_set
      --where attrib_group.group_set_id = attrib_group_set.group_set_id;
END POPULATE_ATTRIB_GROUP;
----------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_REC_GROUP_LABELS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = REC_LABELS_sheet )
   select s9t_row(s9t_cells(action_mod ,
       rec_group_id,
       lang,
       lov_title,
       lov_col1_header,
                            lov_col2_header                                                 
                           )
                   )
     from cfa_rec_group_labels ;
END POPULATE_REC_GROUP_LABELS;
---------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_REC_GROUP( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = REC_GROUP_sheet )
   select s9t_row(s9t_cells(action_mod ,
       rec_group_id,
       rec_group_name,
                            query_type,
       table_name,
       column_1,
       column_2,
                            where_col_1,
                            where_operator_1,
       where_cond_1,
       where_col_2,
                            where_operator_2,
                            where_cond_2                              
                           )
                   )
     from cfa_rec_group ;
END POPULATE_REC_GROUP;
---------------------------------------------------------------------------------------------------------------------

 FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id            IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind  IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
  L_file s9t_file;
  L_program VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.CREATE_S9T';
 BEGIN
   INIT_S9T(O_file_id);
   commit;
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;  
   if I_template_only_ind = 'N' then
      POPULATE_EXT_ENTITY(O_file_id);
      POPULATE_GROUP_SET(O_file_id);
      POPULATE_GROUP_SET_LABELS(O_file_id);
      POPULATE_ATTRIB_GROUP(O_file_id);
      POPULATE_ATTRIB_GROUP_LABELS(O_file_id);   
      POPULATE_REC_GROUP(O_file_id);
      POPULATE_REC_GROUP_LABELS(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;      
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;  
END CREATE_S9T;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_EXT_ENTITY(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_file_id    IN   s9t_folder.file_id%TYPE,
                                 I_process_id IN   svc_ext_entity.process_id%TYPE) IS
   TYPE svc_EXT_ENTITY_col_typ IS TABLE OF SVC_EXT_ENTITY%ROWTYPE;
   L_temp_rec SVC_EXT_ENTITY%ROWTYPE := null;
   svc_EXT_ENTITY_col svc_EXT_ENTITY_col_typ :=NEW svc_EXT_ENTITY_col_typ();
   L_process_id SVC_EXT_ENTITY.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_program            VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_EXT_ENTITY';
   L_default_rec SVC_EXT_ENTITY%ROWTYPE;
   L_pk_cols   VARCHAR2(30) := 'Base RMS Table';
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
   L_error_code    NUMBER;
   
   cursor C_MANDATORY_IND is
      select VALIDATION_FUNC_mi,
             BASE_RMS_TABLE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'CFA_EXT_ENTITY'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('VALIDATION_FUNC' AS VALIDATION_FUNC,
                                            'BASE_RMS_TABLE' AS BASE_RMS_TABLE,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN

  -- Get default values.
   FOR rec IN (select  VALIDATION_FUNC_dv,
                       BASE_RMS_TABLE_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'CFA_EXT_ENTITY'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'VALIDATION_FUNC' AS VALIDATION_FUNC,
                                                      'BASE_RMS_TABLE' AS BASE_RMS_TABLE,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.VALIDATION_FUNC := rec.VALIDATION_FUNC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_EXT_ENTITY ' ,
                            NULL,
                           'VALIDATION_FUNC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.BASE_RMS_TABLE := rec.BASE_RMS_TABLE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_EXT_ENTITY ' ,
                            NULL,
                           'BASE_RMS_TABLE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
     
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   
   FOR rec IN 
   
  (select r.get_cell(EXT_ENTITY$ACTION)               AS ACTION,
          r.get_cell(EXT_ENTITY$VALIDATION_FUNC)      AS VALIDATION_FUNC,
          r.get_cell(EXT_ENTITY$BASE_RMS_TABLE)       AS BASE_RMS_TABLE,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(EXT_ENTITY_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
     
      L_error := FALSE;
      BEGIN
          L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            EXT_ENTITY_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
      
         L_temp_rec.VALIDATION_FUNC := rec.VALIDATION_FUNC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       EXT_ENTITY_sheet,
                            rec.row_seq,
                            'VALIDATION_FUNC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
      
         L_temp_rec.BASE_RMS_TABLE := rec.BASE_RMS_TABLE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            EXT_ENTITY_sheet,
                            rec.row_seq,
                            'BASE_RMS_TABLE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
     
      if L_temp_rec.BASE_RMS_TABLE is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         EXT_ENTITY_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_cols));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_EXT_ENTITY_col.extend();
         svc_EXT_ENTITY_col(svc_EXT_ENTITY_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_EXT_ENTITY_col.COUNT SAVE EXCEPTIONS
      insert into SVC_EXT_ENTITY(process_id,      
                                     chunk_id,
                                     row_seq,
                                     action,
                                     process$status,
                                     base_rms_table,
                                     validation_func,
                                     create_id,
                                     create_datetime,
                                     last_upd_id,
                                     last_upd_datetime
                                     )
        select svc_EXT_ENTITY_col(i).process_id,
               svc_EXT_ENTITY_col(i).chunk_id ,
               svc_EXT_ENTITY_col(i).row_seq ,
               svc_EXT_ENTITY_col(i).action ,
               svc_EXT_ENTITY_col(i).process$status ,
               sq.base_rms_table,
               sq.validation_func,
               svc_EXT_ENTITY_col(i).create_id,
               svc_EXT_ENTITY_col(i).create_datetime,
               svc_EXT_ENTITY_col(i).last_upd_id ,
               svc_EXT_ENTITY_col(i).last_upd_datetime
          from(select            
                  (case        
                   when l_mi_rec.VALIDATION_FUNC_mi    = 'N'             
                    and svc_EXT_ENTITY_col(i).action = action_mod
                    and s1.VALIDATION_FUNC IS NULL
                   then mt.VALIDATION_FUNC
                   else s1.VALIDATION_FUNC
                   end) AS VALIDATION_FUNC,
                  (case
                   when l_mi_rec.BASE_RMS_TABLE_mi    = 'N'
                    and svc_EXT_ENTITY_col(i).action = action_mod
                    and s1.BASE_RMS_TABLE IS NULL
                   then mt.BASE_RMS_TABLE
                   else s1.BASE_RMS_TABLE
                   end) AS BASE_RMS_TABLE,
                   null as dummy
              from (select
                          svc_EXT_ENTITY_col(i).VALIDATION_FUNC AS VALIDATION_FUNC,
                          svc_EXT_ENTITY_col(i).BASE_RMS_TABLE AS BASE_RMS_TABLE,
                          null as dummy
                      from dual ) s1,
                    CFA_EXT_ENTITY mt
          where    mt.base_rms_table (+) = s1.base_rms_table )sq;
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
           L_error_code:=sql%bulk_exceptions(i).error_code;
	   if L_error_code=1 then
	      L_error_code:=NULL;
	      L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_cols);
           end if;
            WRITE_S9T_ERROR(I_file_id,
              EXT_ENTITY_sheet,
                            svc_EXT_ENTITY_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
         when others then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                             TO_CHAR(SQLCODE));
   END;
END PROCESS_S9T_EXT_ENTITY;
-----------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_GROUP_SET_LABELS(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_file_id    IN   s9t_folder.file_id%TYPE,
                                       I_process_id IN   SVC_ATTRIB_GROUP_SET_LABELS.process_id%TYPE) IS
   
   TYPE GROUP_LABELS_COL_TYP IS TABLE OF SVC_ATTRIB_GROUP_SET_LABELS%ROWTYPE;
   L_temp_rec               SVC_ATTRIB_GROUP_SET_LABELS%ROWTYPE := null ;
   svc_group_set_labels_col GROUP_LABELS_col_typ :=NEW GROUP_LABELS_col_typ();
   L_process_id             SVC_ATTRIB_GROUP_SET_LABELS.process_id%TYPE;
   L_error                  BOOLEAN:=FALSE;
   L_default_rec            SVC_ATTRIB_GROUP_SET_LABELS%ROWTYPE;
   L_program                VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_GROUP_SET_LABELS';
   L_pk_cols                VARCHAR2(30) := 'Group Set Id, Language';
   cursor C_MANDATORY_IND is
      select GROUP_SET_ID_mi,
             LANG_mi,
             LABEL_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = template_key
                 and wksht_key         = 'CFA_ATTRIB_GROUP_SET_LABELS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('GROUP_SET_ID' AS GROUP_SET_ID,
                                            'LANG' AS LANG,
                                            'LABEL' AS LABEL,
                                            null as dummy));
      l_mi_rec       C_MANDATORY_IND%ROWTYPE;
      dml_errors     EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns   VARCHAR2(255) := 'Group Set Id,Language';
      L_error_code   NUMBER;
      L_error_msg    RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select GROUP_SET_ID_dv,
                      LANG_dv,
                      LABEL_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key    = template_key
                          and wksht_key       = 'CFA_ATTRIB_GROUP_SET_LABELS'
                       ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('GROUP_SET_ID' AS GROUP_SET_ID,
                                                                              'LANG' AS LANG,
                                                                              'LABEL' AS LABEL,
                                                                              NULL AS dummy)))
 

   LOOP
      BEGIN
        L_default_rec.GROUP_SET_ID := rec.GROUP_SET_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CFA_ATTRIB_GROUP_SET_LABELS ' ,
                            NULL,
                            'GROUP_SET_ID ' ,
                            'INV_DEFAULT',
                             SQLERRM);
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET_LABELS ' ,
                            NULL,
                           'LANG ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LABEL := rec.LABEL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET_LABELS ' ,
                            NULL,
                           'LABEL ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(GROUP_SET_LABELS$ACTION)                    AS ACTION,
                      r.get_cell(GROUP_SET_LABELS$GROUP_SET_ID)              AS GROUP_SET_ID,
                      r.get_cell(GROUP_SET_LABELS$LANG)                      AS LANG,
                      r.get_cell(GROUP_SET_LABELS$LABEL)                     AS LABEL,
                      r.get_row_seq()                                        AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(GROUP_SET_LABELS_sheet)
              )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_LABELS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_SET_ID := rec.GROUP_SET_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_LABELS_sheet,
                            rec.row_seq,
                            'GROUP_SET_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_LABELS_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
  
      BEGIN
         L_temp_rec.LABEL := rec.LABEL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_LABELS_sheet,
                            rec.row_seq,
                            'LABEL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action in (action_new) then
         L_temp_rec.GROUP_SET_ID := NVL( L_temp_rec.GROUP_SET_ID,L_default_rec.GROUP_SET_ID);
         L_temp_rec.LANG         := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.LABEL        := NVL( L_temp_rec.LABEL,L_default_rec.LABEL);
      end if;
      if (L_temp_rec.GROUP_SET_ID is NULL and L_temp_rec.LANG is  NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         GROUP_SET_LABELS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_cols));
          L_error := TRUE;
      end if;
      if NOT L_error then
         svc_GROUP_SET_LABELS_col.extend();
         svc_GROUP_SET_LABELS_col(svc_GROUP_SET_LABELS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_GROUP_SET_LABELS_col.COUNT SAVE EXCEPTIONS
      
        insert into SVC_ATTRIB_GROUP_SET_LABELS(PROCESS_ID,
                                                CHUNK_ID ,
                        ROW_SEQ ,
                        ACTION ,
                        PROCESS$STATUS ,
                 GROUP_SET_ID ,
                 LANG ,
                 LABEL ,
                 CREATE_ID ,
                 CREATE_DATETIME ,
                 LAST_UPD_ID ,
                 LAST_UPD_DATETIME)
           select svc_GROUP_SET_LABELS_col(i).process_id ,
                  svc_GROUP_SET_LABELS_col(i).chunk_id ,
                  svc_GROUP_SET_LABELS_col(i).row_seq ,
                  svc_GROUP_SET_LABELS_col(i).action ,
                  svc_GROUP_SET_LABELS_col(i).process$status ,
                  sq.group_set_id ,
                  sq.lang ,
                  sq.label ,
                  svc_GROUP_SET_LABELS_col(i).create_id ,
                  svc_GROUP_SET_LABELS_col(i).create_datetime ,
                  svc_GROUP_SET_LABELS_col(i).last_upd_id ,
                  svc_GROUP_SET_LABELS_col(i).last_upd_datetime
            from(select (case
                         when l_mi_rec.GROUP_SET_ID_mi    = 'N'
                          and svc_GROUP_SET_LABELS_col(i).action = action_mod
                          and s1.GROUP_SET_ID IS NULL
                         then mt.GROUP_SET_ID
                         else s1.GROUP_SET_ID
                         end) AS GROUP_SET_ID,
                        (case
                         when l_mi_rec.LANG_mi    = 'N'
                          and svc_GROUP_SET_LABELS_col(i).action = action_mod
                          and s1.LANG IS NULL
                         then mt.LANG
                          else s1.LANG
                         end) AS LANG,
                        (case
                         when l_mi_rec.LABEL_mi    = 'N'
                          and svc_GROUP_SET_LABELS_col(i).action = action_mod
                          and s1.LABEL IS NULL
                         then mt.LABEL
                         else s1.LABEL
                         end) AS LABEL,
                         null as dummy
                    from (select svc_GROUP_SET_LABELS_col(i).GROUP_SET_ID AS GROUP_SET_ID,
                                 svc_GROUP_SET_LABELS_col(i).LANG AS LANG,
                                 svc_GROUP_SET_LABELS_col(i).LABEL AS LABEL,
                                 null as dummy
                            from dual ) s1,
                         CFA_ATTRIB_GROUP_SET_LABELS mt
                   where mt.GROUP_SET_ID (+)     = s1.GROUP_SET_ID   and
                         mt.LANG (+)             = s1.LANG   and
                         1 = 1 )sq;
   EXCEPTION
      when DML_ERRORS then
           FOR i IN 1..sql%bulk_exceptions.count
           LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
           if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_cols);
            end if;           
              WRITE_S9T_ERROR(I_file_id,
                              GROUP_SET_LABELS_sheet,
                              svc_GROUP_SET_LABELS_col(sql%bulk_exceptions(i).error_index).row_seq,
                              NULL,
                              L_error_code,
                              L_error_msg);
           END LOOP;
      when others then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                             TO_CHAR(SQLCODE));     
END;
END PROCESS_S9T_GROUP_SET_LABELS;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_GROUP_SET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_file_id    IN   s9t_folder.file_id%TYPE,
                                I_process_id IN   SVC_ATTRIB_GROUP_SET.process_id%TYPE) IS
                                
   TYPE GROUP_SET_COL_TYP  IS TABLE OF SVC_ATTRIB_GROUP_SET%ROWTYPE;
   L_temp_rec                 SVC_ATTRIB_GROUP_SET%ROWTYPE := null;
   svc_ATTRIB_GROUP_SET_col   GROUP_SET_col_typ :=NEW GROUP_SET_col_typ();
   L_process_id               SVC_ATTRIB_GROUP_SET.process_id%TYPE;
   L_error                    BOOLEAN:=FALSE;
   L_default_rec              SVC_ATTRIB_GROUP_SET%ROWTYPE;
   L_error_code               NUMBER;
   L_error_msg                RTK_ERRORS.RTK_TEXT%type;
   L_program                  VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_GROUP_SET';
   
   cursor C_MANDATORY_IND is
      select GROUP_SET_ID_mi,
             BASE_RMS_TABLE_mi,
             DISPLAY_SEQ_mi,
             GROUP_SET_VIEW_NAME_mi,
             STAGING_TABLE_NAME_mi,
             QUALIFIER_FUNC_mi,
             VALIDATION_FUNC_mi,
             DEFAULT_FUNC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              =template_key
                 and wksht_key                                 = 'CFA_ATTRIB_GROUP_SET'
              ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('GROUP_SET_ID'        AS GROUP_SET_ID,
                                                                 'BASE_RMS_TABLE'      AS BASE_RMS_TABLE,
                                                                 'DISPLAY_SEQ'         AS DISPLAY_SEQ,
                                                                 'GROUP_SET_VIEW_NAME' AS GROUP_SET_VIEW_NAME,
                                                                 'STAGING_TABLE_NAME'  AS STAGING_TABLE_NAME,
                                                                 'QUALIFIER_FUNC'      AS QUALIFIER_FUNC,
                                                                 'VALIDATION_FUNC'     AS VALIDATION_FUNC,
                                                                 'DEFAULT_FUNC'        AS DEFAULT_FUNC,
                                                                 null as dummy));
      l_mi_rec     C_MANDATORY_IND%ROWTYPE;
      dml_errors   EXCEPTION;
      PRAGMA       EXCEPTION_INIT(dml_errors, -24381);
      L_pk_columns VARCHAR2(255) := 'Group Set Id';
BEGIN
  -- Get default values.
   FOR rec IN (select  GROUP_SET_ID_dv,
                       BASE_RMS_TABLE_dv,
                       DISPLAY_SEQ_dv,
                       GROUP_SET_VIEW_NAME_dv,
                       STAGING_TABLE_NAME_dv,
                       QUALIFIER_FUNC_dv,
                       VALIDATION_FUNC_dv,
                       DEFAULT_FUNC_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'CFA_ATTRIB_GROUP_SET'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'GROUP_SET_ID'        AS GROUP_SET_ID,
                                                      'BASE_RMS_TABLE'      AS BASE_RMS_TABLE,
                                                      'DISPLAY_SEQ'         AS DISPLAY_SEQ,
                                                      'GROUP_SET_VIEW_NAME' AS GROUP_SET_VIEW_NAME,
                                                      'STAGING_TABLE_NAME'  AS STAGING_TABLE_NAME,
                                                      'QUALIFIER_FUNC'      AS QUALIFIER_FUNC,
                                                      'VALIDATION_FUNC'     AS VALIDATION_FUNC,
                                                      'DEFAULT_FUNC'        AS DEFAULT_FUNC,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.GROUP_SET_ID := rec.GROUP_SET_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'GROUP_SET_ID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.BASE_RMS_TABLE := rec.BASE_RMS_TABLE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'BASE_RMS_TABLE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.GROUP_SET_VIEW_NAME := rec.GROUP_SET_VIEW_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'GROUP_SET_VIEW_NAME ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DISPLAY_SEQ := rec.DISPLAY_SEQ_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'DISPLAY_SEQ ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.QUALIFIER_FUNC := rec.QUALIFIER_FUNC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'QUALIFIER_FUNC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DEFAULT_FUNC := rec.DEFAULT_FUNC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'DEFAULT_FUNC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.VALIDATION_FUNC := rec.VALIDATION_FUNC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'VALIDATION_FUNC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.STAGING_TABLE_NAME := rec.STAGING_TABLE_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_SET ' ,
                            NULL,
                           'STAGING_TABLE_NAME ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(GROUP_SET$ACTION)               AS ACTION,
                      r.get_cell(GROUP_SET$GROUP_SET_ID)         AS GROUP_SET_ID,
                      r.get_cell(GROUP_SET$BASE_RMS_TABLE)       AS BASE_RMS_TABLE,
                      r.get_cell(GROUP_SET$GROUP_SET_VIEW_NAME)  AS GROUP_SET_VIEW_NAME,
                      r.get_cell(GROUP_SET$DISPLAY_SEQ)          AS DISPLAY_SEQ,
                      r.get_cell(GROUP_SET$QUALIFIER_FUNC)       AS QUALIFIER_FUNC,
                      r.get_cell(GROUP_SET$DEFAULT_FUNC)         AS DEFAULT_FUNC,
                      r.get_cell(GROUP_SET$VALIDATION_FUNC)      AS VALIDATION_FUNC,
                      r.get_cell(GROUP_SET$STAGING_TABLE_NAME)   AS STAGING_TABLE_NAME,
                      r.get_row_seq()                            AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(GROUP_SET_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_SET_ID := rec.GROUP_SET_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'GROUP_SET_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.BASE_RMS_TABLE := rec.BASE_RMS_TABLE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                             rec.row_seq,
                            'BASE_RMS_TABLE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
        L_temp_rec.GROUP_SET_VIEW_NAME := rec.GROUP_SET_VIEW_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'GROUP_SET_VIEW_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISPLAY_SEQ := rec.DISPLAY_SEQ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'DISPLAY_SEQ',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.QUALIFIER_FUNC := rec.QUALIFIER_FUNC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'QUALIFIER_FUNC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_FUNC := rec.DEFAULT_FUNC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'DEFAULT_FUNC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.VALIDATION_FUNC := rec.VALIDATION_FUNC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'VALIDATION_FUNC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.STAGING_TABLE_NAME := rec.STAGING_TABLE_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            GROUP_SET_sheet,
                            rec.row_seq,
                            'STAGING_TABLE_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.GROUP_SET_ID          := NVL( L_temp_rec.GROUP_SET_ID,L_default_rec.GROUP_SET_ID);
         L_temp_rec.BASE_RMS_TABLE        := NVL( L_temp_rec.BASE_RMS_TABLE,L_default_rec.BASE_RMS_TABLE);
         L_temp_rec.GROUP_SET_VIEW_NAME   := NVL( L_temp_rec.GROUP_SET_VIEW_NAME,L_default_rec.GROUP_SET_VIEW_NAME);
         L_temp_rec.DISPLAY_SEQ           := NVL( L_temp_rec.DISPLAY_SEQ,L_default_rec.DISPLAY_SEQ);
         L_temp_rec.QUALIFIER_FUNC        := NVL( L_temp_rec.QUALIFIER_FUNC,L_default_rec.QUALIFIER_FUNC);
         L_temp_rec.DEFAULT_FUNC          := NVL( L_temp_rec.DEFAULT_FUNC,L_default_rec.DEFAULT_FUNC);
         L_temp_rec.VALIDATION_FUNC       := NVL( L_temp_rec.VALIDATION_FUNC,L_default_rec.VALIDATION_FUNC);
         L_temp_rec.STAGING_TABLE_NAME    := NVL( L_temp_rec.STAGING_TABLE_NAME,L_default_rec.STAGING_TABLE_NAME);
      end if;
      if L_temp_rec.GROUP_SET_ID is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         GROUP_SET_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
     end if;
      if NOT L_error then
         svc_ATTRIB_GROUP_SET_col.extend();
         svc_ATTRIB_GROUP_SET_col(svc_ATTRIB_GROUP_SET_col.COUNT()):=l_temp_rec;
      end if;
      
   END LOOP;
   BEGIN
      forall i IN 1..svc_ATTRIB_GROUP_SET_col.COUNT SAVE EXCEPTIONS
      
      insert into SVC_ATTRIB_GROUP_SET(PROCESS_ID,
                                       CHUNK_ID,
                                       ROW_SEQ,
                                       ACTION,
                                       PROCESS$STATUS,
                                       GROUP_SET_ID,
                                       BASE_RMS_TABLE,
                                       DISPLAY_SEQ,
                                       GROUP_SET_VIEW_NAME,
                                       STAGING_TABLE_NAME,
                                       QUALIFIER_FUNC,
                                       VALIDATION_FUNC,
                                       DEFAULT_FUNC,
                                       CREATE_ID,
                  CREATE_DATETIME,
           LAST_UPD_ID,
                                       LAST_UPD_DATETIME)
        select  svc_ATTRIB_GROUP_SET_col(i).process_id ,
                svc_ATTRIB_GROUP_SET_col(i).chunk_id ,
                svc_ATTRIB_GROUP_SET_col(i).row_seq ,
                svc_ATTRIB_GROUP_SET_col(i).action ,
                svc_ATTRIB_GROUP_SET_col(i).process$status ,
                sq.group_set_id ,
                sq.base_rms_table ,
                sq.display_seq ,
                sq.group_set_view_name ,
                sq.staging_table_name ,
                sq.qualifier_func ,
                sq.validation_func ,
                sq.default_func ,
                svc_ATTRIB_GROUP_SET_col(i).create_id ,
                svc_ATTRIB_GROUP_SET_col(i).create_datetime ,
                svc_ATTRIB_GROUP_SET_col(i).last_upd_id ,
                svc_ATTRIB_GROUP_SET_col(i).last_upd_datetime
         from   (select
                           (case
                            when l_mi_rec.GROUP_SET_ID_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.GROUP_SET_ID IS NULL
                            then mt.GROUP_SET_ID
                            else s1.GROUP_SET_ID
                            end) AS GROUP_SET_ID,
                           (case
                            when l_mi_rec.BASE_RMS_TABLE_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.BASE_RMS_TABLE IS NULL
                            then mt.BASE_RMS_TABLE
                            else s1.BASE_RMS_TABLE
                            end) AS BASE_RMS_TABLE,
                           (case
                            when l_mi_rec.GROUP_SET_VIEW_NAME_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.GROUP_SET_VIEW_NAME IS NULL
                            then mt.GROUP_SET_VIEW_NAME
                            else s1.GROUP_SET_VIEW_NAME
                            end) AS GROUP_SET_VIEW_NAME,
                           (case
                            when l_mi_rec.DISPLAY_SEQ_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.DISPLAY_SEQ IS NULL
                            then mt.DISPLAY_SEQ
                            else s1.DISPLAY_SEQ
                            end) AS DISPLAY_SEQ,
                           (case
                            when l_mi_rec.QUALIFIER_FUNC_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.QUALIFIER_FUNC IS NULL
                            then mt.QUALIFIER_FUNC
                            else s1.QUALIFIER_FUNC
                            end) AS QUALIFIER_FUNC,
                           (case
                            when l_mi_rec.DEFAULT_FUNC_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.DEFAULT_FUNC IS NULL
                            then mt.DEFAULT_FUNC
                            else s1.DEFAULT_FUNC
                            end) AS DEFAULT_FUNC,
                           (case
                            when l_mi_rec.VALIDATION_FUNC_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.VALIDATION_FUNC IS NULL
                            then mt.VALIDATION_FUNC
                            else s1.VALIDATION_FUNC
                            end) AS VALIDATION_FUNC,
                           (case
                            when l_mi_rec.STAGING_TABLE_NAME_mi    = 'N'
                             and svc_ATTRIB_GROUP_SET_col(i).action = action_mod
                             and s1.STAGING_TABLE_NAME IS NULL
                            then mt.STAGING_TABLE_NAME
                            else s1.STAGING_TABLE_NAME
                            end) AS STAGING_TABLE_NAME,
                            null as dummy
                       from (select svc_ATTRIB_GROUP_SET_col(i).GROUP_SET_ID AS GROUP_SET_ID,
                                    svc_ATTRIB_GROUP_SET_col(i).BASE_RMS_TABLE AS BASE_RMS_TABLE,
                                    svc_ATTRIB_GROUP_SET_col(i).GROUP_SET_VIEW_NAME AS GROUP_SET_VIEW_NAME,
                                    svc_ATTRIB_GROUP_SET_col(i).DISPLAY_SEQ AS DISPLAY_SEQ,
                                    svc_ATTRIB_GROUP_SET_col(i).QUALIFIER_FUNC AS QUALIFIER_FUNC,
                                    svc_ATTRIB_GROUP_SET_col(i).DEFAULT_FUNC AS DEFAULT_FUNC,
                                    svc_ATTRIB_GROUP_SET_col(i).VALIDATION_FUNC AS VALIDATION_FUNC,
                                    svc_ATTRIB_GROUP_SET_col(i).STAGING_TABLE_NAME AS STAGING_TABLE_NAME,
                                    null as dummy
                               from dual ) s1,
                            (select cags.GROUP_SET_ID,
                                    cfaext.BASE_RMS_TABLE,
                                    cags.GROUP_SET_VIEW_NAME,
                                    cags.DISPLAY_SEQ,
                                    cags.QUALIFIER_FUNC,
                                    cags.DEFAULT_FUNC,
                                    cags.VALIDATION_FUNC,
                                    cags.STAGING_TABLE_NAME
                              from  CFA_ATTRIB_GROUP_SET cags,
                                    CFA_EXT_ENTITY cfaext
                              where cags.EXT_ENTITY_ID = cfaext.EXT_ENTITY_ID)mt
                       where  mt.GROUP_SET_ID (+)       = s1.GROUP_SET_ID)sq;
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
         
            WRITE_S9T_ERROR( I_file_id,
                             GROUP_SET_sheet,
                             svc_ATTRIB_GROUP_SET_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
        
      END LOOP;
      when others then
              O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                             TO_CHAR(SQLCODE));
   END;
END PROCESS_S9T_GROUP_SET;
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ATTRIB_LABELS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_file_id    IN   s9t_folder.file_id%TYPE,
                                     I_process_id IN   SVC_ATTRIB_GROUP_LABELS.process_id%TYPE) IS
   TYPE svc_ATTRIB_LABELS_col_typ IS TABLE OF SVC_ATTRIB_GROUP_LABELS%ROWTYPE;
   L_temp_rec                                 SVC_ATTRIB_GROUP_LABELS%ROWTYPE := null;
   svc_ATTRIB_LABELS_col svc_ATTRIB_LABELS_col_typ := NEW svc_ATTRIB_LABELS_col_typ();
   
   L_process_id                               SVC_ATTRIB_GROUP_LABELS.process_id%TYPE;
   L_error                                    BOOLEAN:= FALSE;
   L_default_rec                              SVC_ATTRIB_GROUP_LABELS%ROWTYPE;
   L_program            VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_ATTRIB_LABELS';
   cursor C_MANDATORY_IND is
      select
             GROUP_ID_mi,
             LANG_mi,
             LABEL_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key             = template_key
                 and wksht_key                = 'CFA_ATTRIB_GROUP_LABELS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('GROUP_ID'   AS GROUP_ID,
                                            'LANG'   AS LANG,
                                            'LABEL'   AS LABEL,
                                            null   as dummy));
   l_mi_rec            c_mandatory_ind%ROWTYPE;
   dml_errors          EXCEPTION;
   PRAGMA              exception_init(dml_errors, -24381);
   L_pk_columns        VARCHAR2(255) := 'Group Id,Language';
   L_error_code        NUMBER;
   L_error_msg         RTK_ERRORS.RTK_TEXT%type;
BEGIN
   -----------------------------------------------------------------------------------------------  
   -- Get default values from spreadsheet 
   -----------------------------------------------------------------------------------------------
   FOR rec IN (select
                       GROUP_ID_dv,
                       LANG_dv,
                       LABEL_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'CFA_ATTRIB_GROUP_LABELS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'GROUP_ID'    AS GROUP_ID,
                                                      'LANG'     AS LANG,
                                                      'LABEL'     AS LABEL,
                                                      NULL     AS dummy)))
  
   LOOP
      BEGIN
         L_default_rec.GROUP_ID := rec.GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_LABELS ',
                            NULL,
                           'GROUP_ID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_LABELS ' ,
                            NULL,
                           'LANG ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LABEL := rec.LABEL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP_LABELS',
                            NULL,
                           'LABEL',
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
   -----------------------------------------------------------------------------------------
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(ATTRIB_GROUP_LABELS$ACTION)        AS ACTION,
                      r.get_cell(ATTRIB_GROUP_LABELS$GROUP_ID)                  AS GROUP_ID,
                      r.get_cell(ATTRIB_GROUP_LABELS$LANG)                AS LANG,
                      r.get_cell(ATTRIB_GROUP_LABELS$LABEL)                     AS LABEL,
                      r.get_row_seq()                                           AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = sheet_name_trans(ATTRIB_GROUP_LABELS_sheet)
              )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_LABELS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_ID := rec.GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_LABELS_sheet,
                            rec.row_seq,
                            'GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ATTRIB_GROUP_LABELS_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LABEL := rec.LABEL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
          ATTRIB_GROUP_LABELS_sheet,
                            rec.row_seq,
                            'LABEL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.GROUP_ID    := NVL(L_temp_rec.GROUP_ID,L_default_rec.GROUP_ID);
         L_temp_rec.LANG    := NVL(L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.LABEL    := NVL(L_temp_rec.LABEL,L_default_rec.LABEL);
      end if;
      if L_temp_rec.GROUP_ID is NULL and L_temp_rec.LANG is NULL then
         WRITE_S9T_ERROR(I_file_id,
    ATTRIB_GROUP_LABELS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_ATTRIB_LABELS_col.extend();
         svc_ATTRIB_LABELS_col(svc_ATTRIB_LABELS_col.COUNT()):= l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_ATTRIB_LABELS_col.COUNT SAVE EXCEPTIONS
        insert into SVC_ATTRIB_GROUP_LABELS(PROCESS_ID,
                                                CHUNK_ID ,
                              ROW_SEQ ,
                              ACTION ,
                              PROCESS$STATUS ,
                       GROUP_ID ,
                       LANG ,
                       LABEL ,
                       CREATE_ID ,
                       CREATE_DATETIME ,
                       LAST_UPD_ID ,
                       LAST_UPD_DATETIME)
                 select svc_ATTRIB_LABELS_col(i).process_id ,
                        svc_ATTRIB_LABELS_col(i).chunk_id ,
                        svc_ATTRIB_LABELS_col(i).row_seq ,
                        svc_ATTRIB_LABELS_col(i).action ,
                        svc_ATTRIB_LABELS_col(i).process$status ,
                        sq.group_id ,
                        sq.lang ,
                        sq.label ,
                        svc_ATTRIB_LABELS_col(i).create_id ,
                        svc_ATTRIB_LABELS_col(i).create_datetime ,
                        svc_ATTRIB_LABELS_col(i).last_upd_id ,
                        svc_ATTRIB_LABELS_col(i).last_upd_datetime
                  from(select
                        (case
                         when l_mi_rec.GROUP_ID_mi    = 'N'
                          and svc_ATTRIB_LABELS_col(i).action = action_mod
                          and s1.GROUP_ID IS NULL
                         then mt.GROUP_ID
                         else s1.GROUP_ID
                         end) AS GROUP_ID,
                        (case
                         when l_mi_rec.LANG_mi    = 'N'
                          and svc_ATTRIB_LABELS_col(i).action = action_mod
                          and s1.LANG IS NULL
                         then mt.LANG
                         else s1.LANG
                         end) AS LANG,
                        (case
                         when l_mi_rec.LABEL_mi    = 'N'
                          and svc_ATTRIB_LABELS_col(i).action = action_mod
                          and s1.LABEL IS NULL
                         then mt.LABEL
                         else s1.LABEL
                         end) AS LABEL,
                        null as dummy
                    from (select svc_ATTRIB_LABELS_col(i).GROUP_ID AS GROUP_ID,
                                 svc_ATTRIB_LABELS_col(i).LANG AS LANG,
                                 svc_ATTRIB_LABELS_col(i).LABEL AS LABEL,
                                 null as dummy
                           from dual ) s1,
                         CFA_ATTRIB_GROUP_LABELS mt
                   where
                        mt.GROUP_ID (+)         = s1.GROUP_ID   and
                        mt.LANG (+)             = s1.LANG   and
                        1 = 1 )sq;
      
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                     ATTRIB_GROUP_LABELS_sheet,
                     svc_ATTRIB_LABELS_col(sql%bulk_exceptions(i).error_index).row_seq,
                     NULL,
                     L_error_code,
                     L_error_msg);
         END LOOP;
         when others then
          O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                             TO_CHAR(SQLCODE));
   END;
END PROCESS_S9T_ATTRIB_LABELS;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ATTRIB_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_file_id    IN   s9t_folder.file_id%TYPE,
                                   I_process_id IN   SVC_ATTRIB_GROUP.process_id%TYPE) IS
   TYPE svc_ATTRIB_GROUP_col_typ IS TABLE OF SVC_ATTRIB_GROUP%ROWTYPE;
   L_temp_rec                                    SVC_ATTRIB_GROUP%ROWTYPE := null;
   svc_ATTRIB_GROUP_col svc_ATTRIB_GROUP_col_typ := NEW svc_ATTRIB_GROUP_col_typ();
   L_process_id             SVC_ATTRIB_GROUP.process_id%TYPE;
   L_error                  BOOLEAN:= FALSE;
   L_default_rec            SVC_ATTRIB_GROUP%ROWTYPE;
   L_program            VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_ATTRIB_GROUP';
        
   
   cursor C_MANDATORY_IND is
      select
             GROUP_ID_mi,
             GROUP_SET_ID_mi,
             GROUP_VIEW_NAME_mi,
             DISPLAY_SEQ_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key        = template_key
                 and wksht_key           = 'CFA_ATTRIB_GROUP'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('GROUP_ID'    AS GROUP_ID,
                                            'GROUP_SET_ID'          AS GROUP_SET_ID,
                                            'GROUP_VIEW_NAME'   AS GROUP_VIEW_NAME,
                                            'DISPLAY_SEQ'            AS DISPLAY_SEQ,
                                             null    as dummy));
      l_mi_rec            c_mandatory_ind%ROWTYPE; 
      dml_errors          EXCEPTION;
      PRAGMA              exception_init(dml_errors, -24381);
      L_pk_columns        VARCHAR2(255) := 'Group Id';
      L_error_code        NUMBER;
      L_error_msg         RTK_ERRORS.RTK_TEXT%type;
         
BEGIN
  -- Get default values.
  
   FOR rec IN (select
                       GROUP_ID_dv,
                       GROUP_SET_ID_dv,
                       GROUP_VIEW_NAME_dv,
                       DISPLAY_SEQ_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key        = template_key
                          and wksht_key           = 'CFA_ATTRIB_GROUP'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'GROUP_ID'      AS GROUP_ID,
                                                      'GROUP_SET_ID'             AS GROUP_SET_ID,
                                                      'GROUP_VIEW_NAME'     AS GROUP_VIEW_NAME,
                                                      'DISPLAY_SEQ'      AS DISPLAY_SEQ,
                                                      NULL       AS dummy)))
   LOOP
      BEGIN
         L_default_rec.GROUP_ID := rec.GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
              WRITE_S9T_ERROR(I_file_id,
                              'CFA_ATTRIB_GROUP ' ,
                              NULL,
                              'GROUP_ID ' ,
                              'INV_DEFAULT',
                              SQLERRM);
      END;
      BEGIN
         L_default_rec.GROUP_SET_ID := rec.GROUP_SET_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP' ,
                            NULL,
                           'GROUP_SET_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.GROUP_VIEW_NAME := rec.GROUP_VIEW_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP ' ,
                            NULL,
                           'GROUP_VIEW_NAME ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DISPLAY_SEQ := rec.DISPLAY_SEQ_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_ATTRIB_GROUP ' ,
                            NULL,
                           'DISPLAY_SEQ ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(ATTRIB_GROUP$ACTION)          AS ACTION,
          r.get_cell(ATTRIB_GROUP$GROUP_ID)               AS GROUP_ID,
          r.get_cell(ATTRIB_GROUP$GROUP_SET_ID)            AS GROUP_SET_ID,
          r.get_cell(ATTRIB_GROUP$GROUP_VIEW_NAME)        AS GROUP_VIEW_NAME,
          r.get_cell(ATTRIB_GROUP$DISPLAY_SEQ)            AS DISPLAY_SEQ,
          r.get_row_seq()                                 AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id    = I_file_id
       and ss.sheet_name = sheet_name_trans(ATTRIB_GROUP_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_ID := rec.GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_sheet,
                            rec.row_seq,
                            'GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_SET_ID := rec.GROUP_SET_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_sheet,
                            rec.row_seq,
                            'GROUP_SET_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_VIEW_NAME := rec.GROUP_VIEW_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_sheet,
                            rec.row_seq,
                            'GROUP_VIEW_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISPLAY_SEQ := rec.DISPLAY_SEQ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       ATTRIB_GROUP_sheet,
                            rec.row_seq,
                            'DISPLAY_SEQ',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      
      if rec.action = action_new then
         L_temp_rec.GROUP_ID    := NVL( L_temp_rec.GROUP_ID,L_default_rec.GROUP_ID);
         L_temp_rec.GROUP_SET_ID          := NVL( L_temp_rec.GROUP_SET_ID,L_default_rec.GROUP_SET_ID);
         L_temp_rec.GROUP_VIEW_NAME   := NVL( L_temp_rec.GROUP_VIEW_NAME,L_default_rec.GROUP_VIEW_NAME);
         L_temp_rec.DISPLAY_SEQ          := NVL( L_temp_rec.DISPLAY_SEQ,L_default_rec.DISPLAY_SEQ);
      end if;
 
      if L_temp_rec.GROUP_ID is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         ATTRIB_GROUP_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_ATTRIB_GROUP_col.extend();
         svc_ATTRIB_GROUP_col(svc_ATTRIB_GROUP_col.COUNT()):=l_temp_rec; 
      end if;
   END LOOP;
   BEGIN
     forall i IN 1..svc_ATTRIB_GROUP_col.COUNT SAVE EXCEPTIONS
      
      insert into SVC_ATTRIB_GROUP(PROCESS_ID,
                                       CHUNK_ID ,
                                       ROW_SEQ ,
                                       ACTION ,
                                       PROCESS$STATUS ,
                                       GROUP_ID ,
                                       GROUP_SET_ID ,
                                       GROUP_VIEW_NAME ,
                                       DISPLAY_SEQ,
                                       CREATE_ID ,
                                       CREATE_DATETIME ,
                                       LAST_UPD_ID ,
                                       LAST_UPD_DATETIME)
      select svc_ATTRIB_GROUP_col(i).process_id ,
             svc_ATTRIB_GROUP_col(i).chunk_id ,
             svc_ATTRIB_GROUP_col(i).row_seq ,
             svc_ATTRIB_GROUP_col(i).action ,
             svc_ATTRIB_GROUP_col(i).process$status ,
             sq.group_id ,
             sq.group_set_id ,
             sq.group_view_name ,
             sq.display_seq ,
             svc_ATTRIB_GROUP_col(i).create_id ,
             svc_ATTRIB_GROUP_col(i).create_datetime ,
             svc_ATTRIB_GROUP_col(i).last_upd_id ,
             svc_ATTRIB_GROUP_col(i).last_upd_datetime 
       from( select (case
              when l_mi_rec.GROUP_ID_mi     = 'N'
               and svc_ATTRIB_GROUP_col(i).action = action_mod
               and s1.GROUP_ID IS NULL
              then mt.GROUP_ID
               else s1.GROUP_ID
              end) AS GROUP_ID,
             (case
              when l_mi_rec.GROUP_SET_ID_mi    = 'N'
               and svc_ATTRIB_GROUP_col(i).action = action_mod
               and s1.GROUP_SET_ID IS NULL
              then mt.GROUP_SET_ID
               else s1.GROUP_SET_ID
               end) AS GROUP_SET_ID,
             (case
              when l_mi_rec.GROUP_VIEW_NAME_mi = 'N'
               and svc_ATTRIB_GROUP_col(i).action = action_mod
               and s1.GROUP_VIEW_NAME IS NULL
              then mt.GROUP_VIEW_NAME
               else s1.GROUP_VIEW_NAME
              end) AS GROUP_VIEW_NAME,
             (case
              when l_mi_rec.DISPLAY_SEQ_mi     = 'N'
               and svc_ATTRIB_GROUP_col(i).action = action_mod
               and s1.DISPLAY_SEQ IS NULL
              then mt.DISPLAY_SEQ
               else s1.DISPLAY_SEQ
              end) AS DISPLAY_SEQ,
              null as dummy
         from (select svc_ATTRIB_GROUP_col(i).GROUP_ID            AS GROUP_ID,
                      svc_ATTRIB_GROUP_col(i).GROUP_SET_ID           AS GROUP_SET_ID,
                      svc_ATTRIB_GROUP_col(i).GROUP_VIEW_NAME    AS GROUP_VIEW_NAME,
                      svc_ATTRIB_GROUP_col(i).DISPLAY_SEQ           AS DISPLAY_SEQ,
                      null        as dummy
                 from dual ) s1,
             CFA_ATTRIB_GROUP mt 
         where mt.GROUP_ID (+)    = s1.GROUP_ID)sq;
    
   EXCEPTION
     when DML_ERRORS then
       FOR i IN 1..sql%bulk_exceptions.COUNT
        LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;        
          WRITE_S9T_ERROR( I_file_id,
                     ATTRIB_GROUP_sheet,
                     svc_ATTRIB_GROUP_col(sql%bulk_exceptions(i).error_index).row_seq,
                     NULL,
                     L_error_code,
                     L_error_msg);
        END LOOP;
        when others then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                     SQLERRM,
                                                     L_program,
                                             TO_CHAR(SQLCODE));
   END;
END PROCESS_S9T_ATTRIB_GROUP;
-------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_REC_GROUP_LABELS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_file_id    IN   s9t_folder.file_id%TYPE,
                                        I_process_id IN   SVC_REC_GROUP_LABELS.process_id%TYPE) IS

   TYPE svc_REC_LABELS_col_typ IS TABLE OF SVC_REC_GROUP_LABELS%ROWTYPE;
   L_temp_rec                      SVC_REC_GROUP_LABELS%ROWTYPE := null;
   svc_REC_GROUP_LABELS_col    svc_REC_LABELS_col_typ :=NEW svc_REC_LABELS_col_typ();
   L_process_id                    SVC_REC_GROUP_LABELS.process_id%TYPE;
   L_error                         BOOLEAN:=FALSE;
   L_default_rec                   SVC_REC_GROUP_LABELS%ROWTYPE;
   L_program            VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_REC_GROUP_LABELS';
   
   cursor C_MANDATORY_IND is
      select REC_GROUP_ID_mi,
             LANG_mi,
             LOV_TITLE_mi,
             LOV_COL1_HEADER_mi,
             LOV_COL2_HEADER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'CFA_REC_GROUP_LABELS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ( 'REC_GROUP_ID' AS REC_GROUP_ID,
                                             'LANG' AS LANG,
                                             'LOV_TITLE' AS LOV_TITLE,
                                             'LOV_COL1_HEADER' AS LOV_COL1_HEADER,
                                             'LOV_COL2_HEADER' AS LOV_COL2_HEADER,
                                             null as dummy));
   l_mi_rec       c_mandatory_ind%ROWTYPE;
   dml_errors     EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_pk_columns   VARCHAR2(255) := 'Record Group Id,Language';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select REC_GROUP_ID_dv,
                      LANG_dv,
                      LOV_TITLE_dv,
                      LOV_COL2_HEADER_dv,
                      LOV_COL1_HEADER_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'CFA_REC_GROUP_LABELS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'REC_GROUP_ID' AS REC_GROUP_ID,
                                                      'LANG' AS LANG,
                                                      'LOV_COL2_HEADER' AS LOV_COL2_HEADER,
                                                      'LOV_COL1_HEADER' AS LOV_COL1_HEADER,
                                                      'LOV_TITLE' AS LOV_TITLE,
                                                      NULL AS dummy)))
   LOOP
     BEGIN
         L_default_rec.REC_GROUP_ID := rec.REC_GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP_LABELS ' ,
                            NULL,
                           'REC_GROUP_ID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP_LABELS ' ,
                            NULL,
                           'LANG ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;      
      BEGIN
         L_default_rec.LOV_TITLE := rec.LOV_TITLE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP_LABELS ' ,
                            NULL,
                           'LOV_TITLE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LOV_COL1_HEADER := rec.LOV_COL1_HEADER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP_LABELS ' ,
                            NULL,
                           'LOV_COL1_HEADER ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;      
      BEGIN
         L_default_rec.LOV_COL2_HEADER := rec.LOV_COL2_HEADER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP_LABELS ' ,
                            NULL,
                           'LOV_COL2_HEADER ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
     
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(REC_LABELS$ACTION)            AS ACTION,
          r.get_cell(REC_LABELS$REC_GROUP_ID)      AS REC_GROUP_ID,
          r.get_cell(REC_LABELS$LANG)              AS LANG,
          r.get_cell(REC_LABELS$LOV_TITLE)         AS LOV_TITLE,
          r.get_cell(REC_LABELS$LOV_COL1_HEADER)   AS LOV_COL1_HEADER,
          r.get_cell(REC_LABELS$LOV_COL2_HEADER)   AS LOV_COL2_HEADER,
          r.get_row_seq()                          AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(REC_LABELS_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_LABELS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REC_GROUP_ID := rec.REC_GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_LABELS_sheet,
                            rec.row_seq,
                            'REC_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_LABELS_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOV_TITLE := rec.LOV_TITLE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
              REC_LABELS_sheet,
                            rec.row_seq,
                            'LOV_TITLE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOV_COL1_HEADER := rec.LOV_COL1_HEADER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_LABELS_sheet,
                            rec.row_seq,
                            'LOV_COL1_HEADER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOV_COL2_HEADER := rec.LOV_COL2_HEADER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_LABELS_sheet,
                            rec.row_seq,
                            'LOV_COL2_HEADER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.REC_GROUP_ID := NVL( L_temp_rec.REC_GROUP_ID,L_default_rec.REC_GROUP_ID);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.LOV_TITLE := NVL( L_temp_rec.LOV_TITLE,L_default_rec.LOV_TITLE);
         L_temp_rec.LOV_COL1_HEADER := NVL( L_temp_rec.LOV_COL1_HEADER,L_default_rec.LOV_COL1_HEADER);
         L_temp_rec.LOV_COL2_HEADER := NVL( L_temp_rec.LOV_COL2_HEADER,L_default_rec.LOV_COL2_HEADER);
      end if;
      if (L_temp_rec.REC_GROUP_ID is NULL and L_temp_rec.LANG is NULL) then
         WRITE_S9T_ERROR(I_file_id,
           REC_LABELS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_REC_GROUP_LABELS_col.extend();
         svc_REC_GROUP_LABELS_col(svc_REC_GROUP_LABELS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_REC_GROUP_LABELS_col.COUNT SAVE EXCEPTIONS
      insert into SVC_REC_GROUP_LABELS(PROCESS_ID,
                                           CHUNK_ID ,
                                           ROW_SEQ ,
                                           ACTION ,
                                           PROCESS$STATUS ,
                                           REC_GROUP_ID ,
                                           LANG ,
                                           LOV_TITLE ,
                                           LOV_COL1_HEADER ,
                                           LOV_COL2_HEADER ,
                                           CREATE_ID ,
                                           CREATE_DATETIME ,
                                           LAST_UPD_ID ,
                                           LAST_UPD_DATETIME)
             select svc_REC_GROUP_LABELS_col(i).process_id ,
                    svc_REC_GROUP_LABELS_col(i).chunk_id ,
                    svc_REC_GROUP_LABELS_col(i).row_seq ,
                    svc_REC_GROUP_LABELS_col(i).action ,
                    svc_REC_GROUP_LABELS_col(i).process$status ,
                    sq.rec_group_id ,
                    sq.lang ,
                    sq.lov_title ,
                    sq.lov_col1_header ,
                    sq.lov_col2_header ,
                    svc_REC_GROUP_LABELS_col(i).create_id ,
                    svc_REC_GROUP_LABELS_col(i).create_datetime ,
                    svc_REC_GROUP_LABELS_col(i).last_upd_id ,
                    svc_REC_GROUP_LABELS_col(i).last_upd_datetime 
               from(select (case
                            when l_mi_rec.REC_GROUP_ID_mi    = 'N'
                             and svc_REC_GROUP_LABELS_col(i).action = action_mod
                             and s1.REC_GROUP_ID IS NULL
                            then mt.REC_GROUP_ID
                            else s1.REC_GROUP_ID
                            end) AS REC_GROUP_ID,
                           (case
                            when l_mi_rec.LANG_mi    = 'N'
                             and svc_REC_GROUP_LABELS_col(i).action = action_mod
                             and s1.LANG IS NULL
                            then mt.LANG
                            else s1.LANG
                            end) AS LANG,
                           (case
                            when l_mi_rec.LOV_TITLE_mi    = 'N'
                             and svc_REC_GROUP_LABELS_col(i).action = action_mod
                             and s1.LOV_TITLE IS NULL
                            then mt.LOV_TITLE
                            else s1.LOV_TITLE
                            end) AS LOV_TITLE,
                           (case
                            when l_mi_rec.LOV_COL1_HEADER_mi    = 'N'
                             and svc_REC_GROUP_LABELS_col(i).action = action_mod
                             and s1.LOV_COL1_HEADER IS NULL
                            then mt.LOV_COL1_HEADER
                            else s1.LOV_COL1_HEADER
                            end) AS LOV_COL1_HEADER,
                           (case
                            when l_mi_rec.LOV_COL2_HEADER_mi    = 'N'
                             and svc_REC_GROUP_LABELS_col(i).action = action_mod
                             and s1.LOV_COL2_HEADER IS NULL
                            then mt.LOV_COL2_HEADER
                            else s1.LOV_COL2_HEADER
                            end) AS LOV_COL2_HEADER,
                            null as dummy
                      from (select svc_REC_GROUP_LABELS_col(i).REC_GROUP_ID AS REC_GROUP_ID,
                                   svc_REC_GROUP_LABELS_col(i).LANG AS LANG,
                                   svc_REC_GROUP_LABELS_col(i).LOV_TITLE AS LOV_TITLE,
                                   svc_REC_GROUP_LABELS_col(i).LOV_COL1_HEADER AS LOV_COL1_HEADER,
                                   svc_REC_GROUP_LABELS_col(i).LOV_COL2_HEADER AS LOV_COL2_HEADER,
                                   null as dummy
                              from dual ) s1,
                           CFA_REC_GROUP_LABELS mt
                      where mt.REC_GROUP_ID (+)     = s1.REC_GROUP_ID   and
                            mt.LANG (+)             = s1.LANG   and
                            1 = 1 )sq;                     
                                           
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;           
            WRITE_S9T_ERROR(I_file_id,
                            REC_LABELS_sheet,
                            svc_REC_GROUP_LABELS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when others then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                    SQLERRM,
                                                    L_program,
                                             TO_CHAR(SQLCODE));         
   END;
END PROCESS_S9T_REC_GROUP_LABELS;
--------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_REC_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_file_id    IN   s9t_folder.file_id%TYPE,
                                I_process_id IN   SVC_REC_GROUP.process_id%TYPE) IS
   TYPE svc_REC_GROUP_col_typ IS TABLE OF SVC_REC_GROUP%ROWTYPE ;
   L_temp_rec                                 SVC_REC_GROUP%ROWTYPE := null;
   svc_REC_GROUP_col                          svc_REC_GROUP_col_typ :=NEW svc_REC_GROUP_col_typ();
   L_process_id                               SVC_REC_GROUP.process_id%TYPE;
   L_error                                    BOOLEAN:=FALSE;
   L_default_rec                              SVC_REC_GROUP%ROWTYPE;
   L_program            VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T_REC_GROUP';
   cursor C_MANDATORY_IND is
      select REC_GROUP_ID_mi,
             REC_GROUP_NAME_mi,
             QUERY_TYPE_mi,
             TABLE_NAME_mi,
             COLUMN_1_mi,
             COLUMN_2_mi,
             WHERE_COL_1_mi,
             WHERE_OPERATOR_1_mi,
             WHERE_COND_1_mi,
             WHERE_COL_2_mi,
             WHERE_OPERATOR_2_mi,
             WHERE_COND_2_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'CFA_REC_GROUP'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('REC_GROUP_ID'        AS REC_GROUP_ID,
                                            'REC_GROUP_NAME'      AS REC_GROUP_NAME,
                                            'QUERY_TYPE'          AS QUERY_TYPE,
                                            'TABLE_NAME'          AS TABLE_NAME,
                                            'COLUMN_1'            AS COLUMN_1, 
                                            'COLUMN_2'            AS COLUMN_2,
                                            'WHERE_COL_1'         AS WHERE_COL_1,
                                            'WHERE_OPERATOR_1'    AS WHERE_OPERATOR_1,
                                            'WHERE_COND_1'        AS WHERE_COND_1,
                                            'WHERE_COL_2'         AS WHERE_COL_2,
                                            'WHERE_OPERATOR_2'    AS WHERE_OPERATOR_2,
                                            'WHERE_COND_2'        AS WHERE_COND_2,
                                            null as dummy));
      l_mi_rec       c_mandatory_ind%ROWTYPE;
      dml_errors     EXCEPTION;
      PRAGMA         exception_init(dml_errors, -24381);
      L_pk_columns   VARCHAR2(255) := 'Record Group Id';
      L_error_code   NUMBER;
      L_error_msg    RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  REC_GROUP_ID_dv,
                       REC_GROUP_NAME_dv,
                       QUERY_TYPE_dv,
                       TABLE_NAME_dv,
                       COLUMN_1_dv,
                       COLUMN_2_dv, 
                       WHERE_COL_1_dv,
                       WHERE_OPERATOR_1_dv,
                       WHERE_COND_1_dv,
                       WHERE_COL_2_dv,
                       WHERE_OPERATOR_2_dv,
                       WHERE_COND_2_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'CFA_REC_GROUP'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'REC_GROUP_ID'        AS REC_GROUP_ID,
                                                      'REC_GROUP_NAME'      AS REC_GROUP_NAME,
                                                      'QUERY_TYPE'          AS QUERY_TYPE,
                                                      'TABLE_NAME'          AS TABLE_NAME,
                                                      'COLUMN_1'            AS COLUMN_1,
                                                      'COLUMN_2'            AS COLUMN_2,
                                                      'WHERE_COL_1'         AS WHERE_COL_1,
                                                      'WHERE_OPERATOR_1'    AS WHERE_OPERATOR_1,
                                                      'WHERE_COND_1'        AS WHERE_COND_1,
                                                      'WHERE_COL_2'         AS WHERE_COL_2,
                                                      'WHERE_OPERATOR_2'    AS WHERE_OPERATOR_2,
                                                      'WHERE_COND_2'        AS WHERE_COND_2,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.REC_GROUP_ID := rec.REC_GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'REC_GROUP_ID ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.REC_GROUP_NAME := rec.REC_GROUP_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'REC_GROUP_NAME ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.QUERY_TYPE := rec.QUERY_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'QUERY_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.TABLE_NAME := rec.TABLE_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'TABLE_NAME ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.COLUMN_1 := rec.COLUMN_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP ' ,
                            NULL,
                           'COLUMN_1 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;      
      BEGIN
         L_default_rec.COLUMN_2 := rec.COLUMN_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP ' ,
                            NULL,
                           'COLUMN_2 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.WHERE_COL_1 := rec.WHERE_COL_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP ' ,
                            NULL,
                           'WHERE_COL_1 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.WHERE_OPERATOR_1 := rec.WHERE_OPERATOR_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'WHERE_OPERATOR_1 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;      
      BEGIN
         L_default_rec.WHERE_COND_1 := rec.WHERE_COND_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'WHERE_COND_1 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.WHERE_COL_2 := rec.WHERE_COL_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'WHERE_COL_2 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.WHERE_OPERATOR_2 := rec.WHERE_OPERATOR_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'WHERE_OPERATOR_2 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
                
      BEGIN
         L_default_rec.WHERE_COND_2 := rec.WHERE_COND_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CFA_REC_GROUP' ,
                            NULL,
                           'WHERE_COND_2 ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(REC_GROUP$ACTION)             AS ACTION,
          r.get_cell(REC_GROUP$REC_GROUP_ID)       AS REC_GROUP_ID,
          r.get_cell(REC_GROUP$REC_GROUP_NAME)     AS REC_GROUP_NAME,
          r.get_cell(REC_GROUP$QUERY_TYPE)         AS QUERY_TYPE,
          r.get_cell(REC_GROUP$TABLE_NAME)         AS TABLE_NAME,
          r.get_cell(REC_GROUP$COLUMN_1)           AS COLUMN_1,
          r.get_cell(REC_GROUP$COLUMN_2)           AS COLUMN_2,
          r.get_cell(REC_GROUP$WHERE_COL_1)        AS WHERE_COL_1,
          r.get_cell(REC_GROUP$WHERE_OPERATOR_1)   AS WHERE_OPERATOR_1,
          r.get_cell(REC_GROUP$WHERE_COND_1)       AS WHERE_COND_1,
          r.get_cell(REC_GROUP$WHERE_COL_2)        AS WHERE_COL_2,
          r.get_cell(REC_GROUP$WHERE_OPERATOR_2)   AS WHERE_OPERATOR_2,
          r.get_cell(REC_GROUP$WHERE_COND_2)       AS WHERE_COND_2,
          r.get_row_seq()                          AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(REC_GROUP_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
              REC_GROUP_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REC_GROUP_ID := rec.REC_GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'REC_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REC_GROUP_NAME := rec.REC_GROUP_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'REC_GROUP_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.QUERY_TYPE := rec.QUERY_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
              REC_GROUP_sheet,
                            rec.row_seq,
                            'QUERY_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;            
      BEGIN
         L_temp_rec.TABLE_NAME := rec.TABLE_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'TABLE_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COLUMN_1 := rec.COLUMN_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'COLUMN_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;      
      BEGIN
         L_temp_rec.COLUMN_2 := rec.COLUMN_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
              REC_GROUP_sheet,
                            rec.row_seq,
                            'COLUMN_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WHERE_COL_1 := rec.WHERE_COL_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
              REC_GROUP_sheet,
                            rec.row_seq,
                            'WHERE_COL_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WHERE_OPERATOR_1 := rec.WHERE_OPERATOR_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'WHERE_OPERATOR_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WHERE_COND_1 := rec.WHERE_COND_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'WHERE_COND_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;      
      BEGIN
         L_temp_rec.WHERE_COL_2 := rec.WHERE_COL_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'WHERE_COL_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
         
      BEGIN
         L_temp_rec.WHERE_COND_2 := rec.WHERE_COND_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'WHERE_COND_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WHERE_OPERATOR_2 := rec.WHERE_OPERATOR_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
       REC_GROUP_sheet,
                            rec.row_seq,
                            'WHERE_OPERATOR_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.REC_GROUP_ID         := NVL( L_temp_rec.REC_GROUP_ID,L_default_rec.REC_GROUP_ID);
         L_temp_rec.REC_GROUP_NAME       := NVL( L_temp_rec.REC_GROUP_NAME,L_default_rec.REC_GROUP_NAME);
         L_temp_rec.QUERY_TYPE           := NVL( L_temp_rec.QUERY_TYPE,L_default_rec.QUERY_TYPE);
         L_temp_rec.TABLE_NAME           := NVL( L_temp_rec.TABLE_NAME,L_default_rec.TABLE_NAME);
         L_temp_rec.COLUMN_1             := NVL( L_temp_rec.COLUMN_1,L_default_rec.COLUMN_1);
         L_temp_rec.COLUMN_2             := NVL( L_temp_rec.COLUMN_2,L_default_rec.COLUMN_2);
         L_temp_rec.WHERE_COL_1          := NVL( L_temp_rec.WHERE_COL_1,L_default_rec.WHERE_COL_1);
         L_temp_rec.WHERE_OPERATOR_1     := NVL( L_temp_rec.WHERE_OPERATOR_1,L_default_rec.WHERE_OPERATOR_1);
         L_temp_rec.WHERE_COND_1         := NVL( L_temp_rec.WHERE_COND_1,L_default_rec.WHERE_COND_1);
         L_temp_rec.WHERE_COL_2          := NVL( L_temp_rec.WHERE_COL_2,L_default_rec.WHERE_COL_2);
         L_temp_rec.WHERE_OPERATOR_2     := NVL( L_temp_rec.WHERE_OPERATOR_2,L_default_rec.WHERE_OPERATOR_2);
         L_temp_rec.WHERE_COND_2         := NVL( L_temp_rec.WHERE_COND_2,L_default_rec.WHERE_COND_2);
      end if;
      if L_temp_rec.REC_GROUP_ID is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         REC_GROUP_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_REC_GROUP_col.extend();
         svc_REC_GROUP_col(svc_REC_GROUP_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_REC_GROUP_col.COUNT SAVE EXCEPTIONS
      INSERT INTO SVC_REC_GROUP(PROCESS_ID,
                                CHUNK_ID ,
                                ROW_SEQ ,
                                ACTION ,
                                PROCESS$STATUS ,
                                REC_GROUP_ID ,
                                REC_GROUP_NAME ,
                                QUERY_TYPE ,
                                TABLE_NAME , 
                                COLUMN_1 ,
                                COLUMN_2 ,
                                WHERE_COL_1 ,
                                WHERE_OPERATOR_1 ,
                                WHERE_COND_1 ,
                                WHERE_COL_2 ,
                                WHERE_OPERATOR_2 ,
                                WHERE_COND_2 ,
                                CREATE_ID ,
                                CREATE_DATETIME ,
                                LAST_UPD_ID ,
                                LAST_UPD_DATETIME)
             SELECT svc_REC_GROUP_col(i).process_id ,
                    svc_REC_GROUP_col(i).chunk_id ,
                    svc_REC_GROUP_col(i).row_seq ,
                    svc_REC_GROUP_col(i).action ,
                    svc_REC_GROUP_col(i).process$status ,
                    sq.rec_group_id ,
                    sq.rec_group_name ,
                    sq.query_type ,
                    sq.table_name ,
                    sq.column_1 ,
                    sq.column_2 ,
                    sq.where_col_1 ,
                    sq.where_operator_1 ,
                    sq.where_cond_1 ,
                    sq.where_col_2 ,
                    sq.where_operator_2 ,
                    sq.where_cond_2 ,
                    svc_REC_GROUP_col(i).create_id ,
                    svc_REC_GROUP_col(i).create_datetime ,
                    svc_REC_GROUP_col(i).last_upd_id ,
                    svc_REC_GROUP_col(i).last_upd_datetime
              FROM (select (case
                            when l_mi_rec.REC_GROUP_ID_mi    = 'N'
                             and svc_REC_GROUP_col(i).action = action_mod
                             and s1.REC_GROUP_ID IS NULL
                            then mt.REC_GROUP_ID
                            else s1.REC_GROUP_ID
                            end) AS REC_GROUP_ID,
                           (case
                            when l_mi_rec.REC_GROUP_NAME_mi    = 'N'
                             and svc_REC_GROUP_col(i).action = action_mod
                             and s1.REC_GROUP_NAME IS NULL
                            then mt.REC_GROUP_NAME
                            else s1.REC_GROUP_NAME
                            end) AS REC_GROUP_NAME,
                           (case
                            when l_mi_rec.QUERY_TYPE_mi    = 'N'
                             and svc_REC_GROUP_col(i).action = action_mod
                             and s1.QUERY_TYPE IS NULL
                            then mt.QUERY_TYPE
                            else s1.QUERY_TYPE
                            end) AS QUERY_TYPE,                            
                           (case
                            when l_mi_rec.TABLE_NAME_mi    = 'N'
                             and svc_REC_GROUP_col(i).action = action_mod
                             and s1.TABLE_NAME IS NULL
                            then mt.TABLE_NAME
                            else s1.TABLE_NAME
                            end) AS TABLE_NAME,
                           (case
                            when l_mi_rec.COLUMN_1_mi    = 'N'
                             and svc_REC_GROUP_col(i).action = action_mod
                             and s1.COLUMN_1 IS NULL
                            then mt.COLUMN_1
                            else s1.COLUMN_1
                            end) AS COLUMN_1,
                           (case
                            when l_mi_rec.COLUMN_2_mi    = 'N'
                             and svc_REC_GROUP_col(i).action = action_mod
                             and s1.COLUMN_2 IS NULL
                            then mt.COLUMN_2
                            else s1.COLUMN_2
                            end) AS COLUMN_2,
                           (case
       when l_mi_rec.WHERE_COL_1_mi    = 'N'
        and svc_REC_GROUP_col(i).action = action_mod
        and s1.WHERE_COL_1 IS NULL
       then mt.WHERE_COL_1
       else s1.WHERE_COL_1
       end) AS WHERE_COL_1,
                           (case
              when l_mi_rec.WHERE_OPERATOR_1_mi    = 'N'
               and svc_REC_GROUP_col(i).action = action_mod
               and s1.WHERE_OPERATOR_1 IS NULL
              then mt.WHERE_OPERATOR_1
              else s1.WHERE_OPERATOR_1
                           end) AS WHERE_OPERATOR_1,
                          (case
                           when l_mi_rec.WHERE_COND_1_mi    = 'N'
                            and svc_REC_GROUP_col(i).action = action_mod
                            and s1.WHERE_COND_1 IS NULL
                           then mt.WHERE_COND_1
                           else s1.WHERE_COND_1
                           end) AS WHERE_COND_1,
                          (case
                           when l_mi_rec.WHERE_COL_2_mi    = 'N'
                            and svc_REC_GROUP_col(i).action = action_mod
                            and s1.WHERE_COL_2 IS NULL
                           then mt.WHERE_COL_2
                           else s1.WHERE_COL_2
                           end) AS WHERE_COL_2,
                         (case
                          when l_mi_rec.WHERE_OPERATOR_2_mi    = 'N'
                           and svc_REC_GROUP_col(i).action = action_mod
                           and s1.WHERE_OPERATOR_2 IS NULL
                          then mt.WHERE_OPERATOR_2
                          else s1.WHERE_OPERATOR_2
                          end) AS WHERE_OPERATOR_2,
                         (case
                   when l_mi_rec.WHERE_COND_2_mi    = 'N'
                    and svc_REC_GROUP_col(i).action = action_mod
                    and s1.WHERE_COND_2 IS NULL
                   then mt.WHERE_COND_2
                   else s1.WHERE_COND_2
                          end) AS WHERE_COND_2,
                          null as dummy
                    from (select svc_REC_GROUP_col(i).REC_GROUP_ID AS REC_GROUP_ID,
                                 svc_REC_GROUP_col(i).REC_GROUP_NAME AS REC_GROUP_NAME,
                                 svc_REC_GROUP_col(i).QUERY_TYPE AS QUERY_TYPE,
                                 svc_REC_GROUP_col(i).TABLE_NAME AS TABLE_NAME,
                                 svc_REC_GROUP_col(i).COLUMN_1 AS COLUMN_1,
                                 svc_REC_GROUP_col(i).COLUMN_2 AS COLUMN_2,
                                 svc_REC_GROUP_col(i).WHERE_COL_1 AS WHERE_COL_1,
                                 svc_REC_GROUP_col(i).WHERE_OPERATOR_1 AS WHERE_OPERATOR_1,
                                 svc_REC_GROUP_col(i).WHERE_COND_1 AS WHERE_COND_1,
                                 svc_REC_GROUP_col(i).WHERE_COL_2 AS WHERE_COL_2,
                                 svc_REC_GROUP_col(i).WHERE_OPERATOR_2 AS WHERE_OPERATOR_2,
                                 svc_REC_GROUP_col(i).WHERE_COND_2 AS WHERE_COND_2,
                                 null as dummy
                            from dual ) s1,
                         CFA_REC_GROUP mt
                   where mt.REC_GROUP_ID (+)     = s1.REC_GROUP_ID   and
                         1 = 1 )sq;
EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;           
            WRITE_S9T_ERROR(I_file_id,
                            REC_GROUP_sheet,
                            svc_REC_GROUP_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when others then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));   
   END;
END PROCESS_S9T_REC_GROUP;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file               s9t_file;
   L_sheets             s9t_pkg.names_map_typ;
   L_program            VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_S9T';
   L_process_status     SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT       EXCEPTION;
   PRAGMA               EXCEPTION_INIT(INVALID_FORMAT, -31011);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);

   if s9t_pkg.validate_template(I_file_id)=false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_EXT_ENTITY(O_error_message,
                             I_file_id,
                             I_process_id);
                             
      PROCESS_S9T_GROUP_SET_LABELS(O_error_message,I_file_id,I_process_id);
      PROCESS_S9T_GROUP_SET(O_error_message,I_file_id,I_process_id);
      PROCESS_S9T_ATTRIB_LABELS(O_error_message,I_file_id,I_process_id);
      PROCESS_S9T_ATTRIB_GROUP(O_error_message,I_file_id,I_process_id);
      PROCESS_S9T_REC_GROUP_LABELS(O_error_message,I_file_id,I_process_id);
      PROCESS_S9T_REC_GROUP(O_error_message,I_file_id,I_process_id);
   end if;
   
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   
   FORALL i IN 1..O_error_COUNT
      insert into s9t_errors values LP_s9t_errors_tab(i);
   
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------
/* BEGIN  Process EXT_ENTITY records */
-------------------------------------------------------------------------------
FUNCTION PROCESS_EXT_ENTITY_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_EXT_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.PROCESS_CFA_EXT_ENTITY_VAL';
   L_exist_ent               VARCHAR2(1);
   INV_SQL_NAME              EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(INV_SQL_NAME, -44004);
   L_valid_name              VARCHAR2(250);
   L_table                   VARCHAR2(255):= 'SVC_EXT_ENTITY';
   
BEGIN
   if I_rec.base_rms_table is NOT NULL then
      if instr(I_rec.base_rms_table,'CFA_EXT') <> 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'BASE_RMS_TABLE',
                     'CANNOT_EXND_TBL');
         O_error := TRUE;
      end if;
   end if;   
   if  I_rec.validation_func is NOT NULL then
       BEGIN
         L_valid_name := DBMS_ASSERT.QUALIFIED_SQL_NAME(I_rec.validation_func);
       EXCEPTION
         when INV_SQL_NAME then
              WRITE_ERROR(I_rec.process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_rec.chunk_id,
                          L_table,
                          I_rec.row_seq,
                          'VALIDATION_FUNC',
                          'NAME_INV_CHR');
              O_error := TRUE;
       END;
       
       if NOT CFA_VALIDATE_SQL.VALIDATE_FUNCTION(O_error_message,
                                                 I_rec.validation_func) then
          WRITE_ERROR(I_rec.process_id,
        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
        I_rec.chunk_id,
        L_table,
        I_rec.row_seq,
        'VALIDATION_FUNC',
                      O_error_message);
          O_error := TRUE;             
      end if;
   end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_EXT_ENTITY_VAL;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_EXT_ENTITY_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             L_ext_entity_temp_rec   IN       CFA_EXT_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_EXT_ENTITY_UPD';
   L_table   VARCHAR2(255):= 'SVC_EXT_ENTITY';
BEGIN
   update cfa_ext_entity
      set validation_func = L_ext_entity_temp_rec.validation_func
    where base_rms_table = L_ext_entity_temp_rec.base_rms_table;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_EXT_ENTITY_UPD;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_EXT_ENTITY( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN   SVC_EXT_ENTITY.PROCESS_ID%TYPE,
                             I_chunk_id        IN   SVC_EXT_ENTITY.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_process_error              BOOLEAN := FALSE;
   L_program                    VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_EXT_ENTITY';
   L_error_message              RTK_ERRORS.RTK_TEXT%TYPE;
   L_ext_entity_temp_rec        CFA_EXT_ENTITY%ROWTYPE;
   L_table                      VARCHAR2(255)    :='CFA_EXT_ENTITY';
   
BEGIN
   FOR rec IN C_SVC_EXT_ENTITY(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or (rec.action != action_mod) then
         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action =action_mod
         and rec.CFA_EXT_ENTITY_UK1_rid is NULL then
          WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      'BASE_RMS_TABLE',
                      'INVALID_RMS_TABLE');
          L_error :=TRUE;
      end if;
     if rec.BASE_RMS_TABLE IS NULL  then
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'BASE_RMS_TABLE',
                    'MUST_ENTER_VALUE');
        L_error :=TRUE;
     end if;
      if  rec.action =action_mod
          and rec.CFA_EXT_ENTITY_UK1_rid is NOT NULL then 
          if PROCESS_EXT_ENTITY_VAL(L_error_message,
                                    L_error,
                                    rec) = FALSE then
             WRITE_ERROR(I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         NULL,
                         L_error_message);
             L_error :=TRUE;
          end if;
      end if;   
      if NOT L_error then
         L_ext_entity_temp_rec.validation_func             := rec.validation_func;
         L_ext_entity_temp_rec.base_rms_table              := rec.base_rms_table;
         if (rec.action = action_mod and  nvl(rec.validation_func,'X') <> nvl(rec.ext_entity_uk_validation_func,'X'))  then
            if EXEC_EXT_ENTITY_UPD(L_error_message,
                                   L_ext_entity_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
      if NOT L_process_error or NOT L_error then
         update svc_ext_entity st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_ext_entity st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_EXT_ENTITY;
-------------------------------------------------------------------------------
/* END  Process EXT_ENTITY records */
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
/* BEGIN  Process ATTRIB_GROUP_SET records */
-------------------------------------------------------------------------------
FUNCTION GEN_SEQ_GROUP_SET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_ATTRIB_GROUP_SET.CHUNK_ID%TYPE)
   return BOOLEAN iS
   L_program             VARCHAR2(75) := 'CORESVC_CFAS_ADMIN_SQL.GEN_SEQ_GROUP_SET'; 
   L_error               BOOLEAN:=false;
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   
   
   CURSOR c_new_grpset is
   select grpset.group_set_id as orig_group_set_id,
          grpset.rowid rid,
          grpset.row_seq,
          grpset.group_set_id         
     from svc_attrib_group_set grpset
     where grpset.process_id  = I_process_id
       and grpset.chunk_id = I_chunk_id
       and grpset.action = action_new
       and grpset.group_set_id  is not null
       and grpset.process$status = 'N';
   Type c_new_grp_set_typ is TABLE OF c_new_grpset%rowtype;
   c_new_grp_set_tab     c_new_grp_set_typ;
   
  
BEGIN  

  OPEN c_new_grpset;
  FETCH c_new_grpset bulk collect INTO c_new_grp_set_tab;
  CLOSE c_new_grpset;
  FOR i IN 1..c_new_grp_set_tab.count()
  LOOP
     if CFA_SETUP_GROUP_SET_SQL.GET_NEXT_GRP_SET_ID(L_error_message,
                                                    c_new_grp_set_tab(i).group_set_id) = FALSE then
        WRITE_ERROR(I_process_id, 
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id, 
                    L_GROUP_SET_table,
                    c_new_grp_set_tab(i).row_seq, 
                    c_new_grp_set_tab(i).orig_group_set_id,
                    L_error_message);
      
       UPDATE svc_attrib_group_set
           SET process$status = 'E'
        WHERE rowid = c_new_grp_set_tab(i).rid;
        
        UPDATE svc_attrib_group_set_labels
    SET process$status = 'E'
        WHERE  process_id  = I_process_id
           and chunk_id = I_chunk_id
           and action = action_new
           and group_set_id = c_new_grp_set_tab(i).orig_group_set_id
           and process$status = 'N';
    
     END IF;        
  END LOOP;
  
  --update labels table with generated group_set_id
   if c_new_grp_set_tab is NOT NULL and c_new_grp_set_tab.COUNT > 0 then
  forall i in 1..c_new_grp_set_tab.count()
  UPDATE svc_attrib_group_set_labels
     set group_set_id = c_new_grp_set_tab(i).group_set_id
   where process_id   = I_process_id
    AND chunk_id      = I_chunk_id
    AND action        = action_new
    AND group_set_id  = c_new_grp_set_tab(i).orig_group_set_id;
    
  forall i in 1..c_new_grp_set_tab.count()
  UPDATE svc_attrib_group_set
     set group_set_id = c_new_grp_set_tab(i).group_set_id
   where process_id   = I_process_id
    AND chunk_id      = I_chunk_id
    AND action        = action_new
    AND group_set_id  = c_new_grp_set_tab(i).orig_group_set_id;   
    
    forall i in 1..c_new_grp_set_tab.count()
    UPDATE svc_attrib_group
     set group_set_id = c_new_grp_set_tab(i).group_set_id
       where process_id   = I_process_id
        AND chunk_id      = I_chunk_id
        AND group_set_id  = c_new_grp_set_tab(i).orig_group_set_id;
  end if;        
    return true;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GEN_SEQ_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION EXEC_GROUP_SET_LABELS_INS(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_goroup_set_labels_temp_rec   IN       CFA_ATTRIB_GROUP_SET_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_GROUP_SET_LABELS_INS';
   L_table   VARCHAR2(255):= 'SVC_ATTRIB_GROUP_SET_LABELS';
BEGIN
   insert into cfa_attrib_group_set_labels  values L_goroup_set_labels_temp_rec;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_GROUP_SET_LABELS_INS;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_GROUP_SET_LABELS_UPD(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_group_set_labels_temp_rec    IN       CFA_ATTRIB_GROUP_SET_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_ATTRIB_GROUP_SET_LABELS_UPD';
   L_table   VARCHAR2(255):= 'SVC_ATTRIB_GROUP_SET_LABELS';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   
   cursor C_ATTRIB_GROUP_SET_LABELS_LOCK is
         select 'X'
           from cfa_attrib_group_set_labels
          where group_set_id = L_group_set_labels_temp_rec.group_set_id
            and lang = L_group_set_labels_temp_rec.lang
         for update nowait;
BEGIN
  open C_ATTRIB_GROUP_SET_LABELS_LOCK;
  close C_ATTRIB_GROUP_SET_LABELS_LOCK;
  
   update cfa_attrib_group_set_labels
      set label = L_group_set_labels_temp_rec.label
    where group_set_id = L_group_set_labels_temp_rec.group_set_id
      and lang = L_group_set_labels_temp_rec.lang;
   return TRUE;
EXCEPTION
 when RECORD_LOCKED then
     if C_ATTRIB_GROUP_SET_LABELS_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_SET_LABELS_LOCK;
     end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP_SET_LABELS',
                                                                L_group_set_labels_temp_rec.group_set_id,
                                                                NULL);
      return FALSE;
   when OTHERS then
     if C_ATTRIB_GROUP_SET_LABELS_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_SET_LABELS_LOCK;
     end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_GROUP_SET_LABELS_UPD;
----------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_GROUP_SET_LABELS_DEL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_group_set_labels_temp_rec   IN       CFA_ATTRIB_GROUP_SET_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_ATTRIB_GROUP_SET_LABELS_DEL';
   L_table   VARCHAR2(255):= 'SVC_ATTRIB_GROUP_SET';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_ATTRIB_GROUP_SET_LABELS_LOCK is
         select 'X'
           from cfa_attrib_group_set_labels
          where group_set_id = L_group_set_labels_temp_rec.group_set_id
            and lang = L_group_set_labels_temp_rec.lang
         for update nowait;     
   
BEGIN
   open C_ATTRIB_GROUP_SET_LABELS_LOCK;
   close C_ATTRIB_GROUP_SET_LABELS_LOCK;
   
   delete from cfa_attrib_group_set_labels 
   where GROUP_SET_ID = L_group_set_labels_temp_rec.group_set_id
     and LANG = L_group_set_labels_temp_rec.lang;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
     if C_ATTRIB_GROUP_SET_LABELS_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_SET_LABELS_LOCK;
      end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               L_group_set_labels_temp_rec.group_set_id,
                                                               NULL);
      return FALSE;
   when OTHERS then
      if C_ATTRIB_GROUP_SET_LABELS_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_SET_LABELS_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_GROUP_SET_LABELS_DEL;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_ATTRIB_GROUP_SET_INS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       L_group_set_temp_rec   IN   CFA_ATTRIB_GROUP_SET%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_ATTRIB_GROUP_SET_INS';
   L_table   VARCHAR2(255):= 'SVC_ATTRIB_GROUP_SET';
BEGIN
   insert
     into cfa_attrib_group_set
   values L_group_set_temp_rec;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_ATTRIB_GROUP_SET_INS;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_ATTRIB_GROUP_SET_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       L_group_set_temp_rec   IN       CFA_ATTRIB_GROUP_SET%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_ATTRIB_GROUP_SET_UPD';
   L_table   VARCHAR2(255):= 'SVC_ATTRIB_GROUP_SET';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_ATTRIB_GROUP_SET_LOCK is
         select 'X'
           from cfa_attrib_group_set
          where group_set_id = L_group_set_temp_rec.group_set_id
         for update nowait;
BEGIN
   open C_ATTRIB_GROUP_SET_LOCK;
   close C_ATTRIB_GROUP_SET_LOCK;
   update cfa_attrib_group_set
      set row = L_group_set_temp_rec
    where 1 = 1
      and group_set_id = L_group_set_temp_rec.group_set_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_ATTRIB_GROUP_SET_LOCK%ISOPEN then
            close C_ATTRIB_GROUP_SET_LOCK;
      end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP_SET',
                                                                L_group_set_temp_rec.group_set_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_ATTRIB_GROUP_SET_LOCK%ISOPEN then
         close C_ATTRIB_GROUP_SET_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_ATTRIB_GROUP_SET_UPD;
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ON_DELETE_GROUP_SET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error          IN OUT BOOLEAN,
                                   I_rec            IN OUT C_SVC_GROUP_SET%ROWTYPE)
RETURN BOOLEAN IS 

   L_program               VARCHAR2(64)                      :='CORESVC_CFAS_ADMIN_SQL.CHECK_ON_DELETE_GROUP_SET';
   L_delete_ok             BOOLEAN;
  
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_ATTRIB_GROUP_SET';
   L_exists   VARCHAR2(1)   := 'N';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   
    cursor C_CHK_ATTRIB_GROUP_DEL is
       select rowid rid,
              action,
              group_id           
         from svc_attrib_group
    where group_set_id = I_rec.stgs_group_set_id;
    
    cursor C_ATTRIB_GROUP_LOCK is
         select 'X'
           from cfa_attrib_group
          where group_set_id = I_rec.stgs_group_set_id
         for update nowait;        
BEGIN
   FOR rec in  C_CHK_ATTRIB_GROUP_DEL loop
     savepoint BEGIN_EXIT;
     if rec.action = action_del and rec.group_id is not null then
        if CFA_SETUP_GROUP_SQL.DELETE_ATTRIB_GROUP_LABEL(O_error_message,
                                                         rec.group_id) = FALSE then
           WRITE_ERROR(I_rec.stgs_process_id,
                       svc_admin_upld_er_seq.NEXTVAL,
                       I_rec.stgs_chunk_id,
                       L_table,
                       I_rec.stgs_row_seq,
                       NULL,
                       O_error_message);                                                 
              
           O_error := true;
        else
           OPEN C_ATTRIB_GROUP_LOCK;
           CLOSE C_ATTRIB_GROUP_LOCK;
         
           delete from cfa_attrib_group where group_id = rec.group_id; 
        
           update svc_attrib_group
              set process$status ='P'
            where rowid = rec.rid;
           update svc_attrib_group_labels
              set process$status ='P' 
            where group_id =rec.group_id;
        end if;                                               
     end if;
   
   end loop;
   if NOT O_error then
      if CFA_SETUP_GROUP_SET_SQL.DELETE_ATTRIB_GRP_SET_LABEL(O_error_message,
                                                             I_rec.stgs_group_set_id) = FALSE then
         WRITE_ERROR(I_rec.stgs_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.stgs_chunk_id,
                     L_table,
                     I_rec.stgs_row_seq,
                     NULL,
                     O_error_message);                                                 
         O_error := true;                                               
      end if;        
   end if;   
        
   return TRUE;
EXCEPTION   
   when RECORD_LOCKED then
        rollback to Begin_Exit;
        WRITE_ERROR(I_rec.stgs_process_id,
                    svc_admin_upld_er_seq.NEXTVAL,
                    I_rec.stgs_chunk_id,
                    L_table,
                    I_rec.stgs_row_seq,
                    'GROUP_SET_ID',
                    'TABLE_IS_LOCKED');
         O_error :=TRUE;
   when OTHERS then
      if C_CHK_ATTRIB_GROUP_DEL%ISOPEN then
         close C_CHK_ATTRIB_GROUP_DEL;
      end if;
      if C_ATTRIB_GROUP_LOCK%ISOPEN then
         close C_ATTRIB_GROUP_LOCK;
      end if;
    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_ON_DELETE_GROUP_SET;
------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_ATTRIB_GROUP_SET_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       L_group_set_temp_rec   IN       CFA_ATTRIB_GROUP_SET%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_ATTRIB_GROUP_SET_DEL';
   L_table   VARCHAR2(255):= 'SVC_ATTRIB_GROUP_SET';
   L_action  VARCHAR2(255);
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
    
   cursor C_ATTRIB_GROUP_LOCK is
         select 'X'
           from cfa_attrib_group
          where group_set_id = L_group_set_temp_rec.group_set_id
         for update nowait;    

BEGIN
   open C_ATTRIB_GROUP_LOCK;
   close C_ATTRIB_GROUP_LOCK;
   delete from cfa_attrib_group_set
    where group_set_id = L_group_set_temp_rec.group_set_id;
        
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
    if C_ATTRIB_GROUP_LOCK%ISOPEN then
           close C_ATTRIB_GROUP_LOCK;
     end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP_SET',
                                                                L_group_set_temp_rec.group_set_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
     if C_ATTRIB_GROUP_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_LOCK;
     end if;
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_ATTRIB_GROUP_SET_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_GROUP_SET_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error           IN OUT   BOOLEAN,
                               I_rec             IN       C_SVC_GROUP_SET%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.PROCESS_ATTRIB_GROUP_VAL';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists            VARCHAR2(1);
   L_table             VARCHAR2(255):='CFA_ATTRIB_GROUP_SET';
   L_valid_name        VARCHAR2(250); 
   INV_SQL_NAME        EXCEPTION;
   PRAGMA              EXCEPTION_INIT(INV_SQL_NAME, -44004);
BEGIN
   --DO validation for NEW and MOD records
   if I_rec.stgs_action in (action_new,action_mod) then
   -----------------------------------------------------------------------------------------------
   --Validation for DISPLAY_SEQ 
      --If the DISPLAY_SEQ is > 99
     if I_rec.stgs_display_seq is not NULL and (I_rec.stgs_display_seq <=0 or I_rec.stgs_display_seq > 99)  then
         WRITE_ERROR(I_rec.stgs_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.stgs_chunk_id,
                     L_GROUP_SET_table,
                     I_rec.stgs_row_seq,
                     'DISPLAY_SEQ',
                     'INV_DISP_SEQ_1_99');
          O_error:=true;
      end if;
      --If the DISPLAY_SEQ is already exist for the Group Set Id and ext_entity_id
      if I_rec.stgs_display_seq is NOT NULL then
         if CFA_SETUP_GROUP_SET_SQL.CHK_GRP_SET_DISP_ORD(L_error_message,
                                                         L_exists,
                                                         I_rec.ext_entity_id,
                                                         I_rec.stgs_group_set_id,
                                                         I_rec.stgs_display_seq) = FALSE then
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'DISPLAY_SEQ',
                        L_error_message);
            O_error:=true;
         end if;
         if L_exists = 'Y' then
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'DISPLAY_SEQ',
                        'DISPLAY_SEQ_USED');

            O_error:=true;
         end if;
      end if;
      -----------------------------------------------------------------------------------------------
     --Validation for GROP_SET_VIEW_NAME 

        --Check if the GROUP_SET_VIEW_NAME is a qualified SQL Name
      if I_rec.stgs_grp_set_view_name is NOT NULL then
         BEGIN
            L_valid_name:=DBMS_ASSERT.QUALIFIED_SQL_NAME(I_rec.stgs_grp_set_view_name);
         EXCEPTION
           when INV_SQL_NAME then
                WRITE_ERROR(I_rec.stgs_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_rec.stgs_chunk_id,
                            L_GROUP_SET_table,
                            I_rec.stgs_row_seq,
                            'GROUP_SET_VIEW_NAME',
                            'NAME_INV_CHR');
                O_error:=true;
           when others then
                WRITE_ERROR(I_rec.stgs_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_rec.stgs_chunk_id,
                            L_GROUP_SET_table,
                            I_rec.stgs_row_seq,
                            'GROUP_SET_VIEW_NAME',
                            SQLERRM);
                O_error:=true;
         END;
      end if;    
      --Check if GROUP_SET_VIEW_NAME is already used in other object
      if ((I_rec.stgs_action = action_new and  I_rec.stgs_grp_set_view_name is NOT NULL )or
          (I_rec.stgs_action = action_mod and  nvl(I_rec.stgs_grp_set_view_name,'X') <> nvl(I_rec.old_group_set_view_name,'X')) ) then
         if CFA_SETUP_GROUP_SET_SQL.GROUP_SET_VIEW_EXISTS(L_error_message,
                                                          L_exists,
                                                          I_rec.stgs_grp_set_view_name) = FALSE then   
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'GROUP_SET_VIEW_NAME',
                        L_error_message);
            O_error:=true;
         end if;
         if L_exists = 'Y' then
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'GROUP_SET_VIEW_NAME',
                        'VIEW_NAME_USED');
            O_error:=true;
         end if;
      end if;
------------------------------------------------------------------------------------------
      --Validation for STAGING_TABLE_NAME
      if I_rec.stgs_stg_tbl_name is not null then 
         --Check if STAGING_TABLE_NAME is a valid SQL name
         BEGIN
            L_valid_name:=dbms_assert.qualified_sql_name(I_rec.stgs_stg_tbl_name);
         EXCEPTION
            when INV_SQL_NAME then
                 WRITE_ERROR(I_rec.stgs_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_rec.stgs_chunk_id,
                             L_GROUP_SET_table,
                             I_rec.stgs_row_seq,
                             'STAGING_TABLE_NAME',
                             'NAME_INV_CHR');
                O_error:=true;
            when others then
                WRITE_ERROR(I_rec.stgs_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_rec.stgs_chunk_id,
                            L_GROUP_SET_table,
                            I_rec.stgs_row_seq,
                            'STAGING_TABLE_NAME',
                            SQLERRM);
                O_error:=true;
         END;
      end if;
      --Check if STAGING_TABLE_NAME is already used in other object
      if ((I_rec.stgs_action = action_new and  I_rec.stgs_stg_tbl_name is NOT NULL )or
          (I_rec.stgs_action = action_mod and  nvl(I_rec.stgs_stg_tbl_name,'X') <> nvl(I_rec.old_staging_table_name,'X')) ) then
       
         if CFA_SETUP_GROUP_SET_SQL.GROUP_SET_STG_TBL_EXT(L_error_message,
                                                          L_exists,
                                                          I_rec.stgs_stg_tbl_name) = FALSE then   
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'STAGING_TABLE_NAME',
                        L_error_message);
            O_error:=true;
         end if;
         if L_exists = 'Y' then
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'STAGING_TABLE_NAME',
                        'TBL_NAME_USED');
            O_error:=true;
         end if;
      end if;
------------------------------------------------------------------------------------------
 --Validation for QUALIFIER_FUNC
      if I_rec.stgs_qualifier_func is not null then 
         --Check if QUALIFIER_FUNC is a valid SQL name
         BEGIN
            L_valid_name:=DBMS_ASSERT.QUALIFIED_SQL_NAME(I_rec.stgs_qualifier_func);
         EXCEPTION
            when INV_SQL_NAME then
                 WRITE_ERROR(I_rec.stgs_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_rec.stgs_chunk_id,
                             L_GROUP_SET_table,
                             I_rec.stgs_row_seq,
                             'QUALIFIER_FUNC',
                             'NAME_INV_CHR');
                 O_error:=true;
            when others then
                 WRITE_ERROR(I_rec.stgs_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_rec.stgs_chunk_id,
                             L_GROUP_SET_table,
                             I_rec.stgs_row_seq,
                             'QUALIFIER_FUNC',
                             SQLERRM);
                 O_error:=true;
         END;
     
         if NOT CFA_VALIDATE_SQL.VALIDATE_FUNCTION(L_error_message,
                                                   I_rec.stgs_qualifier_func) then   
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'QUALIFIER_FUNC',
                        L_error_message);
            O_error:=true;
         end if;
      end if;
------------------------------------------------------------------------------------------
     --Validation for VALIDATION_FUNC
      if I_rec.stgs_validation_func is not null then 
         --Check if VALIDATION_FUNC is a valid SQL name
         BEGIN
            L_valid_name:=DBMS_ASSERT.QUALIFIED_SQL_NAME(I_rec.stgs_validation_func);
         EXCEPTION
            when INV_SQL_NAME then
                 WRITE_ERROR(I_rec.stgs_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_rec.stgs_chunk_id,
                             L_GROUP_SET_table,
                             I_rec.stgs_row_seq,
                             'VALIDATION_FUNC',
                             'NAME_INV_CHR');
                 O_error:=true;
            when others then
                 WRITE_ERROR(I_rec.stgs_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_rec.stgs_chunk_id,
                             L_GROUP_SET_table,
                             I_rec.stgs_row_seq,
                             'VALIDATION_FUNC',
                             SQLERRM);
                 O_error:=true;
         END;
     
         if NOT CFA_VALIDATE_SQL.VALIDATE_FUNCTION(L_error_message,
                                                   I_rec.stgs_validation_func) then   
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'VALIDATION_FUNC',
                        L_error_message);
            O_error:=true;
         end if;
      end if;
      ------------------------------------------------------------------------------------------
     --Validation for DEFAULT_FUNC
      if I_rec.stgs_default_func is not null then 
         --Check if DEFAULT_FUNC is a valid SQL name
         BEGIN
           L_valid_name:=DBMS_ASSERT.QUALIFIED_SQL_NAME(I_rec.stgs_default_func);
         EXCEPTION
           when INV_SQL_NAME then
                WRITE_ERROR(I_rec.stgs_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_rec.stgs_chunk_id,
                            L_GROUP_SET_table,
                            I_rec.stgs_row_seq,
                            'DEFAULT_FUNC',
                            'NAME_INV_CHR');
                O_error:=true;
           when others then
                WRITE_ERROR(I_rec.stgs_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_rec.stgs_chunk_id,
                            L_GROUP_SET_table,
                            I_rec.stgs_row_seq,
                            'DEFAULT_FUNC',
                            SQLERRM);
                O_error:=true;
         END;
       
         if NOT CFA_VALIDATE_SQL.VALIDATE_FUNCTION(L_error_message,
                                                   I_rec.stgs_default_func) then   
            WRITE_ERROR(I_rec.stgs_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgs_chunk_id,
                        L_GROUP_SET_table,
                        I_rec.stgs_row_seq,
                        'DEFAULT_FUNC',
                        L_error_message);
            O_error:=true;
         end if;
      end if;
   ------------------------------------------------------------------------------------------      
   end if;    
  
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_GROUP_SET_VAL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CFA_ATTRIB_GROUP_SET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id      IN       SVC_ATTRIB_GROUP_SET.PROCESS_ID%TYPE,
                                      I_chunk_id        IN       SVC_ATTRIB_GROUP_SET.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_group_set_error           BOOLEAN;
   L_group_set_labels_error    BOOLEAN;
   L_groupsetlbl_process_error BOOLEAN;
   L_error                     BOOLEAN;
   L_program                   VARCHAR2(255)    :='CORESVC_CFAS_ADMIN_SQL.PROCESS_CFA_ATTRIB_GROUP_SET';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_GROUP_SET_temp_rec        CFA_ATTRIB_GROUP_SET%ROWTYPE;
   L_GROUP_SET_LABELS_temp_rec CFA_ATTRIB_GROUP_SET_LABELS%ROWTYPE; 
   L_grp_set_id    NUMBER;
   L_table             VARCHAR2(255):='SVC_ATTRIB_GROUP_SET';
  
   L_exist_primary_label       NUMBER;
         
    cursor C_CHECK_PRIMARY_LANG_EXIST is
    select group_set_id,
           row_seq
      from svc_attrib_group_set grpset
     where action ='NEW' 
       and group_set_id is NOT NULL
       and process_id = I_process_id
       and chunk_id = I_chunk_id
       and not exists(select *
                  from svc_attrib_group_set_labels labels,
                       system_options sysopt
                 where labels.action ='NEW'
                   and labels.lang = sysopt.data_integration_lang
                   and labels.group_set_id =grpset.group_set_id
                   and labels.process_id = grpset.process_id
                   and labels.chunk_id = grpset.chunk_id);
     
    
BEGIN
   --Check if GROUP_SET is a INS and there is no primary_lang exist in LABEL sheet
   for rec in  C_CHECK_PRIMARY_LANG_EXIST
   LOOP
   
     WRITE_ERROR(I_process_id,
                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                 I_chunk_id,
                 L_GROUP_SET_LABELS_table,
                 rec.row_seq,
                 'GROUP_SET_ID',
                 'PRIMARY_LANG_REQ');    -- Atleast one label with Primary language required.
     if rec.group_set_id is not null then
        update svc_attrib_group_set_labels
           set process$status='E'
         where action ='NEW'
           and process_id =I_process_id  
           and chunk_id =I_chunk_id
           and group_set_id = rec.group_set_id;
           
           update svc_attrib_group_set
               set process$status='E'
      where action ='NEW'
               and process_id =I_process_id  
               and chunk_id =I_chunk_id
           and group_set_id = rec.group_set_id;
     end if;
   END LOOP;
   
   if GEN_SEQ_GROUP_SET(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_GROUP_SET_table,
                   0,
                   NULL,
                   O_error_message);      
             
   end if;   

   FOR rec IN C_SVC_GROUP_SET(I_process_id,I_chunk_id)
   LOOP
      L_group_set_error               := FALSE;
      L_group_set_labels_error        := FALSE;
      L_error                         := FALSE;  
      L_groupsetlbl_process_error     := FALSE;
      -------------------------------------------------------------------------      
      --Validation for GROUP_SET  if rank is 1
      -------------------------------------------------------------------------      
      if rec.grp_set_rank = 1 then
         ----------------------------------------------------------------------------- 
         --Check if both actions are NULL or GROUP_SET action is other than NEW,MOD,DEL
         if ((rec.stgs_action is NULL and rec.stgslbl_action is NULL)
             or rec.stgs_action NOT IN (action_new,action_mod,action_del)) then
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_GROUP_SET_table,
                          rec.stgs_row_seq,
                          'ACTION',
                          'INV_ACT');
              L_error :=true;
         end if;
      
         if (rec.stgs_action is NULL and rec.stgs_rid is NOT NULL) then
             WRITE_ERROR( I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_GROUP_SET_table,
                          rec.stgs_row_seq,
                         'ACTION',
                         'INV_ACT');
             L_error :=TRUE;
         end if;
      
         if (rec.stgslbl_action IS NULL and rec.fk_cfa_grp_set_labels_rid IS NOT NULL)then  
             WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.nextval,
                         I_chunk_id,
                         L_GROUP_SET_table,
                         rec.stgslbl_row_seq,
                         'ACTION',
                         'INV_ACT');
            L_error :=TRUE;
         end if;
         --------------------------------------------------------------------------------------------      
         -- Check if the newly entered Group Set Id is already exist
         if rec.stgs_action = action_new and rec.fk_cfa_attrib_group_set_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'GROUP_SET_ID',
                        'DUP_RECORD');
            L_error :=TRUE;
         end if;
         --------------------------------------------------------------------------------------------
         --Check if the given GROUP_SET_ID is missing during action MOD,DEL
         if rec.stgs_action IN (action_mod,action_del) and rec.fk_cfa_attrib_group_set_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'GROUP_SET_ID',
                        'NO_RECORD');
            L_error :=TRUE;
         end if;
         -----------------------------------------------------------------------------------------------------   
         --Check if the GROUP_SET_ID is missing while action is NEW
         if rec.stgs_action = action_new and rec.stgs_GROUP_SET_ID is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'GROUP_SET_ID',
                        'MUST_ENTER_VALUE');
           L_error :=TRUE;
         end if;
         ---------------------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and BASE_RMS_TABLE is not exist in EXT_ENTITY table
         if rec.stgs_action IN (action_new,action_mod) and rec.ext_entity_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'BASE_RMS_TABLE',
                        'INV_ENTITY'); 
            L_error :=TRUE;
         end if;
         ---------------------------------------------------------------------------------------------
         --Check if the active ind is Y then do not update the following
         if (rec.stgs_action = action_mod and rec.old_active_ind = 'Y' ) then
            if(rec.ext_entity_id <> rec.old_ext_entity_id) then
              WRITE_ERROR(I_process_id,
		          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
		          I_chunk_id,
		          L_GROUP_SET_table,
		          rec.stgs_row_seq,
                          'EXT_ENTITY_ID' ,  
		          'CAN_NOT_UPDATE_ACTIVE'); 
               L_error :=TRUE;      
            end if;
	    if(nvl(rec.stgs_grp_set_view_name,'X') <> nvl(rec.old_group_set_view_name,'X')) then
               WRITE_ERROR(I_process_id,
		           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
		           I_chunk_id,
		           L_GROUP_SET_table,
		           rec.stgs_row_seq,
		           'GROUP_SET_VIEW_NAME',
		           'CAN_NOT_UPDATE_ACTIVE'); 
                L_error :=TRUE;      
            end if; 
	   if (nvl(rec.stgs_stg_tbl_name,'X') <> nvl(rec.old_staging_table_name,'X'))  then
               WRITE_ERROR(I_process_id,
		           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
		           I_chunk_id,
		           L_GROUP_SET_table,
		           rec.stgs_row_seq,
		           'STAGING_TABLE_NAME',
		           'CAN_NOT_UPDATE_ACTIVE'); 
                L_error :=TRUE;      
            end if;
	 end if;
         ---------------------------------------------------------------------------------------------
         -- Check if the active ind is Y and the operation is delete,then do not allow to delete
         if (rec.stgs_action = action_del and rec.old_active_ind = 'Y' ) then
              WRITE_ERROR(I_process_id,
	                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
	                  I_chunk_id,
	                  L_GROUP_SET_table,
	                  rec.stgs_row_seq,
	                  NULL,
	                  'CAN_NOT_DEL_ACTIVE'); 
              L_error :=TRUE;      
         end if;       
         -------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and BASE_RMS_TABLE is NULL 
         if ((rec.stgs_action IN (action_new,action_mod)) and (rec.stgs_base_rms_table is NULL)) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                       'BASE_RMS_TABLE',
                       'MUST_ENTER_VALUE'); 
            L_error :=TRUE;
         end if;
         ----------------------------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and GROUP_SET_VIEW_NAME is NULL
         if (rec.stgs_action IN (action_new,action_mod) and rec.stgs_grp_set_view_name  IS NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'GROUP_SET_VIEW_NAME',
                        'ENT_GRPST_VW_NAME');
            L_error :=TRUE;
         end if; 
         ----------------------------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and DISPLAY_SEQ is NULL
         if (rec.stgs_action IN (action_new,action_mod) and rec.stgs_display_seq  IS NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'DISPLAY_SEQ',
                        'ENT_CODE_SEQ');
            L_error :=TRUE;
         end if;
         if not L_error then
            --Validation for each GROUP_SET table columns
            if PROCESS_GROUP_SET_VAL(L_error_message,
                                     L_error,
                                     rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_GROUP_SET_table,
                           rec.stgs_row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;
         end if; 
         ---------------------------------------------------------------------------
         --Validation for Deleting ATTRIB_GROUP_SET
      end if; -- Rank =1        
      ---------------------------------------------------------------------------------------
      --Validation for GROUP_SET_LABELS record
      --------------------------------------------------------------------------------------
      --Check if action other than NEW,MOD,DEL
      if rec.stgslbl_action NOT IN (action_new,action_mod,action_del) then     
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_SET_LABELS_table,
                     rec.stgslbl_row_seq,
                     'ACTION',
                     'INV_ACT');
          L_group_set_labels_error :=true;
      end if;
      -----------------------------------------------------------------------------------  
      -- Check if action is NEW and if GROUP_SET_LABEL exist for the group_set_id
      if rec.stgslbl_action = action_new and rec.fk_cfa_grp_set_labels_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
              svc_admin_upld_er_seq.nextval,
              I_chunk_id,
              L_GROUP_SET_LABELS_table,
              rec.stgslbl_row_seq,
              'GROUP_SET_ID',
              'DUP_LANG_SELECT');
         L_group_set_labels_error :=true;
      end if;
      ------------------------------------------------------------------------------------
      -- Check if LABEL action is MOD , DEL and if GROUP_SET_LABEL doesnt exists
      if rec.stgslbl_action IN (action_mod,action_del) and rec.fk_cfa_grp_set_labels_rid is NULL then     
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_SET_LABELS_table,
                     rec.stgslbl_row_seq,
                     'GROUP_SET_ID',
                     'NO_RECORD');
         L_group_set_labels_error :=true;
      end if;
      ----------------------------------------------------------------------------------
   
      ---------------------------------------------------------------------------------
      --Check if LABEL action is NEW,MOD and LANG or LABEL value is NULL
      if rec.stgslbl_action IN (action_new,action_mod) then
      
         if rec.stgslbl_group_set_id IS NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_SET_LABELS_table,
                        rec.stgslbl_row_seq,
                        'GROUP_SET_ID',
                        'MUST_ENTER_VALUE');

             L_group_set_labels_error :=true;
         end if;
         
         if rec.stgslbl_lang is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_SET_LABELS_table,
                        rec.stgslbl_row_seq,
                        'LANG',
                        'LANG_REQ');

            L_group_set_labels_error :=true; 
         end if;
         if rec.stgslbl_label is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_SET_LABELS_table,
                        rec.stgslbl_row_seq,
                        'LABEL',
                        'LABEL_REQ');
            L_group_set_labels_error :=true;        
         end if;
      end if;
      ---------------------------------------------------------------------------
      if rec.stgslbl_action = action_del and rec.stgslbl_group_set_id IS NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_SET_LABELS_table,
                     rec.stgslbl_row_seq,
                     'GROUP_SET_ID',
                     'MUST_ENTER_VALUE');
         L_group_set_labels_error :=true;
      end if;    
      --------------------------------------------------------------------------- 
      --Check LABEL action is MOD and Lang is modified
      if rec.stgslbl_action = action_mod and rec.stgslbl_lang <> rec.oldlbl_lang then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_SET_LABELS_table,
                     rec.stgslbl_row_seq,
                     'LANG',
                     'CANNOT_MODIFY_LANG');
            
         L_group_set_labels_error :=true;
      end if;
      ---------------------------------------------------------------------------------
      if rec.stgslbl_action = action_new or rec.stgslbl_action = action_mod then
         if rec.fk_lang_rid is null then
            WRITE_ERROR(I_process_id,
           svc_admin_upld_er_seq.nextval,
          I_chunk_id,
          L_GROUP_SET_LABELS_table,
          rec.stgslbl_row_seq,
          'LANG',
        'LANGUAGE_NOT_FOUND');
            
            L_group_set_labels_error :=true;        
         end if;
      end if; 
      -------------------------------------------------------------------------------
      --Check if action is DEL and the lang to be deleted is equal to DATA_INTEGRATION_LANG
      if rec.stgslbl_action =action_del and rec.stgslbl_lang = rec.data_integration_lang 
         and (rec.stgs_action != action_del or rec.stgs_rid is NULL) then
         WRITE_ERROR(I_process_id,
              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
        I_chunk_id,
        L_GROUP_SET_LABELS_table,
        rec.stgslbl_row_seq,
        'LANG',
              'PRIMARY_LANG_REQ');
   L_group_set_labels_error :=true;             
      
      end if;      
      if not L_error and rec.stgs_action = action_del then
         if CHECK_ON_DELETE_GROUP_SET(O_error_message,
                                      L_error,
                                      rec)=FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.stgs_row_seq,
                        NULL,
                        O_error_message);
            L_error :=TRUE;
          end if;
       end if;   
      ---------------------------------------------------------------------------------- 
      --Check if any GROUP_SET reocrd is having error
    /*  if L_error then      
         if rec.stgslbl_row_seq IS NOT NULL then              
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_SET_table,
                        rec.stgs_row_seq,
                        'GROUP_SET_ID',
                        'ERROR_IN_GROUP_SET');
             L_group_set_error :=true;             
         end if;    
      end if;*/
      -----------------------------------------------------------------------------------
      --begin SVC_GROUP_SET processing
      ---------------------------------------------------------------------------------
      if NOT L_error then
         -- process only the first header record for a given GROUP_SET
         if rec.stgs_action is NOT NULL and rec.grp_set_rank = 1 then
            L_GROUP_SET_temp_rec.GROUP_SET_ID          := rec.stgs_group_set_id;
            L_GROUP_SET_temp_rec.EXT_ENTITY_ID         := rec.ext_entity_id;
            L_GROUP_SET_temp_rec.GROUP_SET_VIEW_NAME   := rec.stgs_grp_set_view_name;
            L_GROUP_SET_temp_rec.DISPLAY_SEQ           := rec.stgs_display_seq;
            L_GROUP_SET_temp_rec.QUALIFIER_FUNC        := rec.stgs_qualifier_func;
            L_GROUP_SET_temp_rec.DEFAULT_FUNC          := rec.stgs_default_func;
            L_GROUP_SET_temp_rec.VALIDATION_FUNC       := rec.stgs_validation_func;
            L_GROUP_SET_temp_rec.STAGING_TABLE_NAME    := rec.stgs_stg_tbl_name;
            if rec.stgs_action = action_new then
        L_GROUP_SET_temp_rec.base_ind              := 'N';
        L_GROUP_SET_temp_rec.active_ind            := 'N';
            else
               L_GROUP_SET_temp_rec.base_ind              := rec.old_base_ind;
        L_GROUP_SET_temp_rec.active_ind            := rec.old_active_ind;
            end if;
            -----------------------------------------------------------------------------
            --Insert New GROUP_SET Records.
            if rec.stgs_action = action_new then
               L_GROUP_SET_temp_rec.base_ind              := 'N';
               L_GROUP_SET_temp_rec.active_ind            := 'N';
               if EXEC_CFA_ATTRIB_GROUP_SET_INS(L_error_message,
                                                L_GROUP_SET_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_GROUP_SET_table,
                              rec.stgs_row_seq,
                              NULL,
                              L_error_message);
                  L_group_set_error :=TRUE;
               end if;                                             
            end if;
            -------------------------------------------------------------------------------- 
            ---Modify only if there is change from old value to new   
            if rec.stgs_action = action_mod then
               if rec.stgs_display_seq <> rec.old_display_seq  or 
                  rec.ext_entity_id <> rec.old_ext_entity_id or
                  nvl(rec.stgs_grp_set_view_name,'X') <> nvl(rec.old_group_set_view_name,'X') or
                  nvl(rec.stgs_qualifier_func,'X') <> nvl(rec.old_qualifier_function,'X') or
                  nvl(rec.stgs_validation_func,'X') <> nvl(rec.old_validation_func,'X') or
                  nvl(rec.stgs_default_func,'X') <> nvl(rec.old_default_func,'X') or
                  nvl(rec.stgs_stg_tbl_name,'X') <> nvl(rec.old_staging_table_name,'X')  then
                  if EXEC_CFA_ATTRIB_GROUP_SET_UPD(L_error_message,
                                                   L_GROUP_SET_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                          L_GROUP_SET_table,
                          rec.stgs_row_seq,
                          NULL,
                          L_error_message);
              L_group_set_error :=TRUE;
                  end if; 
               end if;
            end if;
            ----------------------------------------------------------------------------------
            --Delete the GROUP_SET record if action is DEL
            if rec.stgs_action = action_del then
               if EXEC_CFA_ATTRIB_GROUP_SET_DEL(L_error_message,
                                                L_group_set_temp_rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_GROUP_SET_table,
                              rec.stgs_row_seq,
                              NULL,
                              L_error_message);
                  L_group_set_error :=TRUE;
               end if;            
            end if;
            -----------------------------------------------------------------------------------
         /*   if L_group_set_error then
               if rec.stgslbl_row_seq is NOT NULL then
                  WRITE_ERROR(I_process_id,
                       svc_admin_upld_er_seq.nextval,
                       I_chunk_id,
                       L_GROUP_SET_table,
                       rec.stgslbl_row_seq,
                       'GROUP_SET',
                              'ERROR_IN_GROUP_SET');
                  L_group_set_error :=TRUE;            
               end if;
            end if;    */
         end if;---- Process only the first GROUP_SET record for a GROUP_SET_ID
         -------------------------------------------------------------------------------------
         --Check LABEL action is NEW,and no existing reocrd in CFA_ATTRIB_GROUP_SET table.
	 if rec.stgslbl_action = action_new  then
	    select count(group_set_id)
	    into L_grp_set_id
	    from cfa_attrib_group_set
	    where group_set_id = rec.stgslbl_group_set_id;
	    if L_grp_set_id = 0 then
	       WRITE_ERROR(I_process_id,
	                   svc_admin_upld_er_seq.nextval,
	                   I_chunk_id,
	                   L_GROUP_SET_LABELS_table,
	                   rec.stgslbl_row_seq,
	                   'GROUP_SET_ID',
	                   'INV_VALUE');
	       L_group_set_labels_error :=true;
            end if;
         end if;
         if rec.stgs_action IN (action_new,action_mod) or
            NVL(rec.stgs_action,'X') = NVL(NULL,'X') then
            if rec.stgslbl_action is NOT NULL and NOT L_group_set_error and NOT L_group_set_labels_error then
               L_GROUP_SET_LABELS_temp_rec.GROUP_SET_ID       :=  rec.stgslbl_group_set_id;
               L_GROUP_SET_LABELS_temp_rec.LANG               :=  rec.stgslbl_lang;
               L_GROUP_SET_LABELS_temp_rec.LABEL              :=  rec.stgslbl_label;
               if rec.stgslbl_lang = rec.data_integration_lang then
                  L_GROUP_SET_LABELS_temp_rec.DEFAULT_LANG_IND   :=  'Y';
               else
                  L_GROUP_SET_LABELS_temp_rec.DEFAULT_LANG_IND   :=  'N';
               end if;
               
               ----------------------------------------------------------------------------------
               --BEGIN SVC_ATTRIB_GROUP_SET_LABELS processing
               --------------------------------------------------------------------------------
               if rec.stgslbl_action = action_new then
                  if EXEC_GROUP_SET_LABELS_INS(L_error_message,
                                               L_GROUP_SET_LABELS_temp_rec) = FALSE  then
                     WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_GROUP_SET_LABELS_table,
                          rec.stgslbl_row_seq,
                          NULL,
                                 L_error_message);
                     L_groupsetlbl_process_error :=TRUE;                                      
                  end if;
               end if;   
               ---------------------------------------------------------------------------------
               --Modify only if there is change from old value to new 
               if rec.stgslbl_action = action_mod and nvl(rec.stgslbl_label,'X') <> nvl(rec.oldlbl_label,'X') then
                  if EXEC_GROUP_SET_LABELS_UPD(L_error_message,
                                               L_GROUP_SET_LABELS_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_GROUP_SET_LABELS_table,
                          rec.stgslbl_row_seq,
                          NULL,
                          L_error_message);
                     L_groupsetlbl_process_error :=TRUE;                                                                              
                  end if;
               end if;
               ----------------------------------------------------------------------------------- 
               if rec.stgslbl_action = action_del  then
                  if EXEC_GROUP_SET_LABELS_DEL(L_error_message,
                                        L_GROUP_SET_LABELS_temp_rec) = FALSE then
              WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                       L_GROUP_SET_LABELS_table,
                         rec.stgslbl_row_seq,
                         NULL,
                          L_error_message);
                     L_groupsetlbl_process_error :=TRUE;                                                                              
                  end if;
               end if;
               ----------------------------------------------------------------------------------
            end if;   --Process LABELS record
      /*  else
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_GROUP_SET_LABELS_table,
                       rec.stgslbl_row_seq,
                       'CFA_ATTRIB_GROUP_SET_LABELS',
                       'GROUP_SET_DEL_REC'); --Associated GROUP_SET header has been deleted.
           L_groupsetlbl_process_error :=TRUE;     */   
        end if; -- GROUP_SET action in NEW or MOD
     end if; -- If NOT L_error
     ----------------------------------------------------------------------------------
     if NOT L_error or  NOT L_group_set_error then
        update svc_attrib_group_set st
    set process$status ='P'
         where rowid = rec.stgs_rid
           and st.process$status != 'E';
     else
        update svc_attrib_group_set st
           set process$status ='E'
         where rowid = rec.stgs_rid;
     
     end if;
     if NOT L_group_set_labels_error or L_groupsetlbl_process_error then
        update svc_attrib_group_set_labels st
           set process$status ='P'
         where rowid = rec.stgslbl_rid
           and st.process$status != 'E';
     else
         update svc_attrib_group_set_labels st
            set process$status ='E'
          where rowid = rec.stgslbl_rid;
     end if;
  
  END LOOP;
  return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_PRIMARY_LANG_EXIST%ISOPEN  then
         close C_CHECK_PRIMARY_LANG_EXIST;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CFA_ATTRIB_GROUP_SET;
-------------------------------------------------------------------------------
/* END  Process ATTRIB_GROUP_SET records */
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
/* BEGIN Process ATTRIB_GROUP */
-------------------------------------------------------------------------------
FUNCTION GEN_SEQ_ATTRIB_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_ATTRIB_GROUP.CHUNK_ID%TYPE)
   return BOOLEAN iS
   L_program VARCHAR2(75) := 'CORESVC_CFAS_ADMIN_SQL.GEN_SEQ_ATTRIB_GROUP';   
   
   CURSOR C_new_attribgrp is
   select attribgrp.group_id as orig_group_id,
          attribgrp.rowid rid,
          attribgrp.row_seq,
          attribgrp.group_id         
     from svc_attrib_group attribgrp
     where attribgrp.process_id  = I_process_id
       and attribgrp.chunk_id = I_chunk_id
       and attribgrp.action = action_new
       and attribgrp.group_id  is not null
       and attribgrp.process$status = 'N';
    
    Type c_new_attrib_grp_typ is TABLE OF C_new_attribgrp%rowtype;
    c_new_attrib_grp_tab   c_new_attrib_grp_typ;
    L_error BOOLEAN:=false;
    L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN  
  OPEN C_new_attribgrp;
  FETCH C_new_attribgrp bulk collect INTO c_new_attrib_grp_tab;
  CLOSE C_new_attribgrp;
  FOR i IN 1..c_new_attrib_grp_tab.count()
  LOOP
     if CFA_SETUP_GROUP_SQL.GET_NEXT_GROUP_ID(L_error_message,
                                              c_new_attrib_grp_tab(i).group_id) = FALSE then
        L_error := true;                                                   
     end if;
     IF L_error THEN
        UPDATE svc_attrib_group
        SET process$status = 'E'
        WHERE rowid        = c_new_attrib_grp_tab(i).rid;
        WRITE_ERROR(I_process_id, 
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id, 
                    L_GROUP_table,
                    c_new_attrib_grp_tab(i).row_seq, 
                    c_new_attrib_grp_tab(i).orig_group_id,
                    L_error_message);
     END IF;        
  END LOOP;
  --update labels table with generated record_group_id
   if c_new_attrib_grp_tab is NOT NULL and c_new_attrib_grp_tab.COUNT > 0 then
  forall i in 1..c_new_attrib_grp_tab.count()
  UPDATE svc_attrib_group_labels
     set group_id     = c_new_attrib_grp_tab(i).group_id
   where process_id   = I_process_id
    AND chunk_id      = I_chunk_id
    AND action        = action_new
    AND group_id      = c_new_attrib_grp_tab(i).orig_group_id;
    
  forall i in 1..c_new_attrib_grp_tab.count()
  UPDATE svc_attrib_group
     set group_id     = c_new_attrib_grp_tab(i).group_id
   where process_id   = I_process_id
    AND chunk_id      = I_chunk_id
    AND action        = action_new
    AND group_id      = c_new_attrib_grp_tab(i).orig_group_id;   
end if;    
    return true;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GEN_SEQ_ATTRIB_GROUP;
-------------------------------------------------------------------------------------
FUNCTION EXEC_ATTRIB_GROUP_LABELS_INS( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       L_attrib_labels_temp_rec   IN   CFA_ATTRIB_GROUP_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_ATTRIB_LABELS_INS';
   L_exists         VARCHAR2(1);
   grp_id   NUMBER;
   l_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   insert into cfa_attrib_group_labels  values L_attrib_labels_temp_rec; 
   
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ATTRIB_GROUP_LABELS_INS;
--------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ATTRIB_GROUP_LABELS_UPD( O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       L_attrib_labels_temp_rec   IN       CFA_ATTRIB_GROUP_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_ATTRIB_LABELS_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   CURSOR C_ATTRIB_GROUP_LABEL_LOCK is
     select 'X'
       from cfa_attrib_group_labels
      where group_id = L_attrib_labels_temp_rec.group_id
        and lang = L_attrib_labels_temp_rec.lang;  
  
BEGIN
  open C_ATTRIB_GROUP_LABEL_LOCK;
  close C_ATTRIB_GROUP_LABEL_LOCK;
   update cfa_attrib_group_labels
      set label = L_attrib_labels_temp_rec.label
    where group_id = L_attrib_labels_temp_rec.group_id
      and lang = L_attrib_labels_temp_rec.lang;
      
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_ATTRIB_GROUP_LABEL_LOCK%ISOPEN then
         close C_ATTRIB_GROUP_LABEL_LOCK;
      end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP_LABELS',
                                                                L_attrib_labels_temp_rec.group_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
     if C_ATTRIB_GROUP_LABEL_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_LABEL_LOCK;
     end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ATTRIB_GROUP_LABELS_UPD;
------------------------------------------------------------------------------------------------------------------ 
FUNCTION EXEC_ATTRIB_GROUP_LABELS_DEL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      L_attrib_labels_temp_rec      IN       CFA_ATTRIB_GROUP_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_ATTRIB_GROUP_SET_LABELS_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   CURSOR C_ATTRIB_GROUP_LABEL_LOCK is
     select 'X'
       from cfa_attrib_group_labels
      where group_id = L_attrib_labels_temp_rec.group_id
        and lang = L_attrib_labels_temp_rec.lang;   
BEGIN
   open C_ATTRIB_GROUP_LABEL_LOCK;
   close C_ATTRIB_GROUP_LABEL_LOCK;
   delete from cfa_attrib_group_labels 
   where GROUP_ID = L_attrib_labels_temp_rec.group_id
     and LANG = L_attrib_labels_temp_rec.lang;
     
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_ATTRIB_GROUP_LABEL_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_LABEL_LOCK;
      end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP_LABELS',
                                                                L_attrib_labels_temp_rec.group_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
     if C_ATTRIB_GROUP_LABEL_LOCK%ISOPEN then
        close C_ATTRIB_GROUP_LABEL_LOCK;
     end if;
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ATTRIB_GROUP_LABELS_DEL;
--------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ATTRIB_GROUP_INS(  O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 L_attrib_group_temp_rec       IN       CFA_ATTRIB_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_ATTRIB_GROUP_INS';
   

BEGIN
   insert
     into cfa_attrib_group
   values L_attrib_group_temp_rec;
   return TRUE;
   EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ATTRIB_GROUP_INS;
-------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ATTRIB_GROUP_UPD( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_attrib_group_temp_rec       IN   CFA_ATTRIB_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_ATTRIB_GROUP_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
 
   cursor C_ATTRIB_GROUP_LOCK is
     select 'X'
       from cfa_attrib_group
      where group_id = L_attrib_group_temp_rec.group_id;
BEGIN
   open C_ATTRIB_GROUP_LOCK;
   close C_ATTRIB_GROUP_LOCK;
   update cfa_attrib_group
      set row = L_attrib_group_temp_rec
    where 1 = 1
      and group_id = L_attrib_group_temp_rec.group_id;
   return TRUE; 
EXCEPTION
    when RECORD_LOCKED then
       if C_ATTRIB_GROUP_LOCK%ISOPEN then
          close C_ATTRIB_GROUP_LOCK;
       end if;  
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP',
                                                                L_attrib_group_temp_rec.group_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_ATTRIB_GROUP_LOCK%ISOPEN then
         close C_ATTRIB_GROUP_LOCK;
      end if;  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ATTRIB_GROUP_UPD;
----------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ON_DELETE_ATTRIB_GROUP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error          IN OUT BOOLEAN,
                                      I_rec            IN OUT C_SVC_ATTRIB_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.CHECK_ON_DELETE_ATTRIB_GROUP';
   L_table             VARCHAR2(255):='SVC_ATTRIB_GROUP';
BEGIN
   if CFA_SETUP_GROUP_SQL.DELETE_ATTRIB_GROUP_LABEL(O_error_message,
                                                    I_rec.stgrp_group_id) = FALSE then
      WRITE_ERROR(I_rec.stgrp_process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.stgrp_chunk_id,
                  L_table,
                  I_rec.stgrp_row_seq,
                  NULL,
                  O_error_message);        
  end if;
  return true;
EXCEPTION

 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_ON_DELETE_ATTRIB_GROUP;
--------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ATTRIB_GROUP_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               L_attrib_group_temp_rec   IN       CFA_ATTRIB_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_ATTRIB_GROUP_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

  
  cursor C_ATTRIB_GROUP_LOCK is
      select 'X'
        from cfa_attrib_group
       where group_id = L_attrib_group_temp_rec.group_id;
 BEGIN
    open C_ATTRIB_GROUP_LOCK;
   close C_ATTRIB_GROUP_LOCK;
 
   delete
     from cfa_attrib_group
    where group_id = L_attrib_group_temp_rec.group_id;
return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_ATTRIB_GROUP_LOCK%ISOPEN then
         close C_ATTRIB_GROUP_LOCK;
      end if;  
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_ATTRIB_GROUP',
                                                                L_attrib_group_temp_rec.group_id,
                                                                NULL);
      return FALSE;
   when OTHERS then
      if C_ATTRIB_GROUP_LOCK%ISOPEN then
         close C_ATTRIB_GROUP_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ATTRIB_GROUP_DEL;
-----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ATTRIB_GROUP_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error           IN OUT   BOOLEAN,
                                  I_rec             IN       C_SVC_ATTRIB_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(255) := 'CORESVC_CFAS_ADMIN_SQL.PROCESS_CFA_ATTRIB_GROUP_VAL';
   L_exists         VARCHAR2(1);
   L_valid_name    VARCHAR2(250); 
   INV_SQL_NAME         EXCEPTION;
   grp_id   NUMBER;
   PRAGMA               EXCEPTION_INIT(INV_SQL_NAME, -44004);
   l_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   
BEGIN
   ----------------------------------------------------------------------------
   -- CHECK IF THE GROUP_VIEW_NAME EXISTS AND IS A VAILD NAME
   ----------------------------------------------------------------------------
   if I_rec.stgrp_grp_view_name is NOT NULL then
      BEGIN
         L_valid_name:= DBMS_ASSERT.QUALIFIED_SQL_NAME(I_rec.stgrp_grp_view_name);
      EXCEPTION
      when INV_SQL_NAME then
     WRITE_ERROR(I_rec.stgrp_process_id,
          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
            I_rec.stgrp_chunk_id,
            L_GROUP_table,
            I_rec.stgrp_row_seq,
            NULL,
            'NAME_INV_CHR');
       O_error:=true;
      when others then
       WRITE_ERROR(I_rec.stgrp_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_rec.stgrp_chunk_id,
                       L_GROUP_table,
                       I_rec.stgrp_row_seq,
                       NULL,
                       sqlerrm);
            O_error:=true;
      END;
   end if;
   if ((I_rec.stgrp_action = action_new and  I_rec.stgrp_grp_view_name is NOT NULL )or
       (I_rec.stgrp_action = action_mod and  nvl(I_rec.stgrp_grp_view_name,'X') <> nvl(I_rec.old_group_view_name,'X'))) then
       if CFA_SETUP_GROUP_SQL.GROUP_VIEW_EXISTS(L_error_message,
                                                L_exists,
                                  I_rec.stgrp_grp_view_name) = FALSE then         
          WRITE_ERROR(I_rec.stgrp_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.stgrp_chunk_id,
                      L_GROUP_table,
                      I_rec.stgrp_row_seq,
                      'GROUP_VIEW_NAME',
                      L_error_message);
          O_error:=true;                      
       end if;
       ---
       if L_exists = 'Y' then
          WRITE_ERROR(I_rec.stgrp_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.stgrp_chunk_id,
                      L_GROUP_table,
                      I_rec.stgrp_row_seq,
                      'GROUP_VIEW_NAME',
                      'VIEW_NAME_USED');
          O_error:=true;                      
      end if;
   
   end if;
   ------------------------------------------------------------------------
   -- CHECK IF THE ST_VALIDATION_FUNC IS A VAILD NAME
   ----------------------------------------------------------------------------
   --------------------------------------------------------------------------------
   -- CHECK IF DISPLAY_SEQ IS UNIQUE
   ------------------------------------------------------------------------------------
   if I_rec.stgrp_display_seq is not NULL then 
      L_exists := NULL;
      if CFA_SETUP_GROUP_SQL.GRP_DISP_ORD_EXIST(L_error_message,
                                               L_exists,
      I_rec.stgrp_group_set_id,
                                            I_rec.stgrp_group_id,
                                         I_rec.stgrp_display_seq) = FALSE then
         WRITE_ERROR(I_rec.stgrp_process_id,
         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
       I_rec.stgrp_chunk_id,
       L_GROUP_table,
       I_rec.stgrp_row_seq,
       NULL,
       L_error_message);
         O_error := TRUE;
      end if;
      if L_exists = 'Y' then
  WRITE_ERROR(I_rec.stgrp_process_id,
         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
       I_rec.stgrp_chunk_id,
       L_GROUP_table,
       I_rec.stgrp_row_seq,
       NULL,
       'DISPLAY_SEQ_USED');
         O_error := TRUE;
      end if;
   end if;
   -----------------------------------------------------------------------------------------
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_ATTRIB_GROUP_VAL;
-------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CFA_ATTRIB_GROUP( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id   IN   SVC_ATTRIB_GROUP.PROCESS_ID%TYPE,
                                   I_chunk_id     IN   SVC_ATTRIB_GROUP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS

   L_attribgroup_error                BOOLEAN;
   L_attribgroup_labels_error         BOOLEAN;
   L_atrbgplbl_process_error          BOOLEAN;
   L_error                            BOOLEAN;
   L_process_error                    BOOLEAN         := FALSE;
   L_program                          VARCHAR2(255)   :='CORESVC_CFAS_ADMIN_SQL.PROCESS_CFA_ATTRIB_GROUP';
   L_error_message                    VARCHAR2(600);
   L_ATTRIB_GROUP_temp_rec            CFA_ATTRIB_GROUP%ROWTYPE;
   L_ATTRIB_GROUP_LABELS_temp_rec     CFA_ATTRIB_GROUP_LABELS%ROWTYPE;
   L_grp_id                           NUMBER;
   L_table             VARCHAR2(255):='SVC_ATTRIB_GROUP';
   
   L_exist_primary_label              NUMBER;
   
   cursor C_CHECK_PRIMARY_LANG_EXIST is
    select group_id,row_seq
      from svc_attrib_group atribgrp 
     where action ='NEW' 
     and group_id is NOT NULL
     and process_id = I_process_id
     and chunk_id = I_chunk_id
     and not exists(select *
                  from svc_attrib_group_labels labels,
                       system_options sysopt
                 where labels.action ='NEW'
                   and labels.lang = sysopt.data_integration_lang
                   and labels.group_id =atribgrp.group_id
                   and labels.process_id = atribgrp.process_id
                   and labels.chunk_id = atribgrp.chunk_id);
            
  
BEGIN
   --Check if ATTRIB_GROUP is a INS and there is no primary_lang exist in LABEL sheet
   FOR rec in  C_CHECK_PRIMARY_LANG_EXIST
   LOOP

       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_GROUP_table,
                   rec.row_seq,
                   'CFA_ATTRIB_GROUP',
                   'PRIMARY_LANG_REQ');
                   -- Atleast one label with Primary language required.
  
       if rec.group_id is not null then
          update svc_attrib_group_labels
             set process$status='E'
           where action ='NEW'
             and process_id =I_process_id  
             and chunk_id =I_chunk_id
             and group_id = rec.group_id;
             
          update svc_attrib_group
           set process$status='E'
  where action ='NEW'
           and process_id =I_process_id  
           and chunk_id =I_chunk_id
           and group_id = rec.group_id;               
       end if;
   END LOOP;
   
   if GEN_SEQ_ATTRIB_GROUP(L_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   0,
                   NULL,
                   O_error_message); 
   end if;   
   
   FOR rec IN C_SVC_ATTRIB_GROUP(I_process_id,
                                 I_chunk_id)
   LOOP
      L_error                         := FALSE;
      L_attribgroup_error             := FALSE;
      L_attribgroup_labels_error      := FALSE;
      L_atrbgplbl_process_error       := FALSE;
      -------------------------------------------------------------------------      
            --Validation for GROUP_SET  if rank is 1
      -------------------------------------------------------------------------      
      if rec.grp_rank = 1 then
         if ((rec.stgrp_action is NULL and rec.stgrplbl_action is NULL)
             or rec.stgrp_action NOT IN (action_new,action_mod,action_del)) then
             WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_GROUP_table,
                  rec.stgrp_row_seq,
                  'ACTION',
                  'INV_ACT');
              L_error :=true;
  
         end if;
         
         if (rec.stgrp_action is NULL and rec.stgrp_rid is NOT NULL) then
      WRITE_ERROR( I_process_id,
                   svc_admin_upld_er_seq.nextval,
                   I_chunk_id,
                   L_GROUP_table,
                   rec.stgrp_row_seq,
                   'ACTION',
                   'INV_ACT');
      L_error :=TRUE;
  end if;
         if (rec.stgrplbl_action IS NULL and rec.fk_cfa_grp_labels_rid IS NOT NULL)then  
             WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.nextval,
                         I_chunk_id,
                         L_GROUP_table,
                         rec.stgrplbl_row_seq,
                         'ACTION',
                         'INV_ACT');
            L_error :=TRUE;
         end if;  
         -------------------------------------------------------------------------------
         -- Check if the newly entered Group Id is already exist
         if rec.stgrp_action = action_new and rec.fk_cfa_attrib_group_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                        'GROUP_ID',
                        'DUP_RECORD');
            L_error :=TRUE;
         end if;
         --------------------------------------------------------------------------------------------
         --Check if the given GROUP_ID is missing during action MOD,DEL
         if rec.stgrp_action IN (action_mod,action_del) and rec.fk_cfa_attrib_group_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                        'GROUP_ID',
                        'NO_RECORD');
            L_error :=TRUE;
         end if; 
         --------------------------------------------------------------
         --Check if the GROUP_ID is missing while action is NEW
         if rec.stgrp_action = action_new and rec.stgrp_GROUP_ID is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                        'GROUP_ID',
                        'MUST_ENTER_VALUE');
           L_error :=TRUE;
         end if;
         ---------------------------------------------------------------
          --Check if the action is NEW and MOD and GROUP_SET_ID is not exist in CFA_ATTRIB_GROUP_SET table
         if rec.stgrp_action IN (action_new,action_mod) and rec.attrib_group_set_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                       'GROUP_SET_ID',
                       'INV_VALUE'); 
            L_error :=TRUE;
         end if;         
         --------------------------------------------------------------------
         --Check if the action is NEW and MOD and GROUP_SET_ID is NULL 
         if ((rec.stgrp_action IN (action_new,action_mod)) and (rec.stgrp_group_set_id is NULL)) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                       'GROUP_SET_ID',
                       'MUST_ENTER_VALUE'); 
            L_error :=TRUE;
         end if;
         ----------------------------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and GROUP_VIEW_NAME is NULL
         if (rec.stgrp_action IN (action_new,action_mod) and rec.stgrp_grp_view_name  IS NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                       'GROUP_VIEW_NAME',
                       'ENT_GRP_VW_NAME');
            L_error :=TRUE;
         end if;
         ----------------------------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and DISPLAY_SEQ is NULL
         if (rec.stgrp_action IN (action_new,action_mod) and rec.stgrp_display_seq  IS NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                        'DISPLAY_SEQ',
                        'ENT_CODE_SEQ');
            L_error :=TRUE;
         end if;
         ---------------------------------------------------------------------------
         --Check if the active ind is Y then do not update the following
         if (rec.stgrp_action = action_mod and rec.old_active_ind = 'Y' ) then
            if( rec.stgrp_group_set_id <> rec.old_group_set_id) then
              WRITE_ERROR(I_process_id,
		          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
		          I_chunk_id,
		          L_GROUP_table,
		          rec.stgrp_row_seq,
                          'GROUP_SET_ID',
		          'CAN_NOT_UPDATE_ACTIVE'); 
               L_error :=TRUE;      
            end if;
	    if (nvl(rec.stgrp_grp_view_name,'X') <> nvl(rec.old_group_view_name,'X'))  then
               WRITE_ERROR(I_process_id,
		           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
		           I_chunk_id,
		           L_GROUP_table,
		           rec.stgrp_row_seq,
		           'GROUP_VIEW_NAME',
		           'CAN_NOT_UPDATE_ACTIVE'); 
                L_error :=TRUE;      
            end if; 
	 end if;
	 --------------------------------------------------------------------------------
         -- Check if the active ind is Y and the operation is delete,then do not allow to delete
         if (rec.stgrp_action = action_del and rec.old_active_ind = 'Y' ) then
              WRITE_ERROR(I_process_id,
	                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
	                  I_chunk_id,
	                  L_GROUP_table,
	                  rec.stgrp_row_seq,
	                  NULL,
	                  'CAN_NOT_DEL_ACTIVE'); 
              L_error :=TRUE;      
         end if;       
         ---------------------------------------------------------------------------------------------
         if not L_error then
            --Validation for each ATTRIB_GROUP table columns
            if PROCESS_ATTRIB_GROUP_VAL(L_error_message,
                                        L_error,
                                        rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_GROUP_table,
                           rec.stgrp_row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;
         end if; 
         -----------------------------------------------------------------------------------
      end if; -- rank =1
      --------------------------------------------------------------------------------------
      --Validation for ATTRIB_GROUP_LABELS record
      --------------------------------------------------------------------------------------
      
      --Check if action other than NEW,MOD,DEL
      if rec.stgrplbl_action NOT IN (action_new,action_mod,action_del) then     
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_LABELS_table,
                     rec.stgrplbl_row_seq,
                     'ACTION',
                     'INV_ACT');
         L_attribgroup_labels_error :=true;
      end if;
      -----------------------------------------------------------------------------------  
      
      -- Check if action is NEW and if ATTRIB_GROUP_LABEL exist for the group_id
      if rec.stgrplbl_action = action_new and rec.fk_cfa_grp_labels_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
              svc_admin_upld_er_seq.nextval,
              I_chunk_id,
              L_GROUP_LABELS_table,
              rec.stgrplbl_row_seq,
              'GROUP_ID',
              'DUP_LANG_SELECT');
         L_attribgroup_labels_error :=true;
      end if;  
      -----------------------------------------------------------------------------
      -- Check if LABEL action is MOD , DEL and if GROUP_SET_LABEL doesnt exists
      
      if rec.stgrplbl_action IN (action_mod,action_del) and rec.fk_cfa_grp_labels_rid is NULL then     
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_LABELS_table,
                     rec.stgrplbl_row_seq,
                     'GROUP_ID',
                     'NO_RECORD');
         L_attribgroup_labels_error :=true;
      end if;
      ---------------------------------------------------------------------------
      ---------------------------------------------------------------------------------
      --Check if LABEL action is NEW,MOD and LANG or LABEL value is NULL
      
      if rec.stgrplbl_action IN (action_new,action_mod) then
         
         if rec.stgrplbl_group_id IS NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_LABELS_table,
                        rec.stgrplbl_row_seq,
                        'GROUP_ID',
                        'GROUP_ID_REQ');

             L_attribgroup_labels_error :=true;
         end if;
         
         if rec.stgrplbl_lang is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_LABELS_table,
                        rec.stgrplbl_row_seq,
                        'LANG',
                        'LANG_REQ');

            L_attribgroup_labels_error :=true; 
         end if;
         
         if rec.stgrplbl_label is NULL then
         
     WRITE_ERROR(I_process_id,
                 svc_admin_upld_er_seq.nextval,
                 I_chunk_id,
                 L_GROUP_LABELS_table,
                 rec.stgrplbl_row_seq,
                 'LABEL',
                 'LABEL_REQ');
     L_attribgroup_labels_error :=true;        
         end if;
      end if;      
      -----------------------------------------------------------------------------
      --if action is DEL and GROUP_ID is NULL
      
      if rec.stgrplbl_action = action_del and rec.stgrplbl_group_id IS NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_LABELS_table,
                     rec.stgrplbl_row_seq,
                     'GROUP_ID',
                     'MUST_ENTER_VALUE');
         L_attribgroup_labels_error :=true;
      end if;
      --------------------------------------------------------------------------- 
      --Check LABEL action is MOD and Lang is modified
      
      if rec.stgrplbl_action = action_mod and rec.stgrplbl_lang <> rec.oldlbl_lang then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_GROUP_LABELS_table,
                     rec.stgrplbl_row_seq,
                     'LANG',
                     'CANNOT_MODIFY_LANG');
            
         L_attribgroup_labels_error :=true;
      end if;
      ---------------------------------------------------------------------------------
      
      if rec.stgrplbl_action = action_new or rec.stgrplbl_action = action_mod then
         if rec.fk_lang_rid is null then
            WRITE_ERROR(I_process_id,
           svc_admin_upld_er_seq.nextval,
          I_chunk_id,
          L_GROUP_LABELS_table,
          rec.stgrplbl_row_seq,
          'LANG',
        'LANGUAGE_NOT_FOUND');
            
            L_attribgroup_labels_error :=true;        
         end if;
      end if; 
      -------------------------------------------------------------------------------
      --Check if action is DEL and the lang to be deleted is equal to DATA_INTEGRATION_LANG
      
      if rec.stgrplbl_action =action_del and rec.stgrplbl_lang = rec.data_integration_lang 
         and (rec.stgrp_action != action_del or rec.stgrp_rid is NULL) then
         WRITE_ERROR(I_process_id,
              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
        I_chunk_id,
        L_GROUP_LABELS_table,
        rec.stgrplbl_row_seq,
        'LANG',
              'PRIMARY_LANG_REQ');
   L_attribgroup_labels_error :=true;             
      end if;      
    if NOT L_error and rec.stgrp_action = action_del then
       if CHECK_ON_DELETE_ATTRIB_GROUP(O_error_message,
                                       L_error,
                                      rec)=FALSE then
           WRITE_ERROR(I_process_id,
                       svc_admin_upld_er_seq.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.stgrp_row_seq,
                       NULL,
                       O_error_message);
            L_error :=TRUE;
        end if;    
    end if;
      ------------------------------------------------------------------------  
      --Check if any ATTRIB_GROUP reocrd is having error
      
      /*if L_error then      
         if rec.stgrplbl_row_seq IS NOT NULL then              
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_GROUP_table,
                        rec.stgrp_row_seq,
                        'GROUP_ID',
                        'ERROR_IN_ATTRIB_GROUP');
            L_attribgroup_error :=true;             
         end if;    
      end if;*/
      ---------------------------------------------------------------------------
      --begin SVC_ATTRIB_GROUP processing
      --------------------------------------------------------------------------
      if NOT L_error then
         -- process only the first header record for a given ATTRIB_GROUP
         if rec.stgrp_action is NOT NULL and rec.grp_rank = 1 then
            L_ATTRIB_GROUP_temp_rec.GROUP_ID          := rec.stgrp_group_id;
     L_ATTRIB_GROUP_temp_rec.GROUP_SET_ID      := rec.stgrp_group_set_id;
     L_ATTRIB_GROUP_temp_rec.GROUP_VIEW_NAME   := rec.stgrp_grp_view_name;
     L_ATTRIB_GROUP_temp_rec.DISPLAY_SEQ       := rec.stgrp_display_seq;
     if rec.stgrp_action = action_new then
        L_ATTRIB_GROUP_temp_rec.base_ind       := 'N';
        L_ATTRIB_GROUP_temp_rec.active_ind     := 'N';
     else
        L_ATTRIB_GROUP_temp_rec.base_ind       := rec.old_base_ind;
        L_ATTRIB_GROUP_temp_rec.active_ind     := rec.old_active_ind;     
     end if;
         
     --------------------------------------------------------------------
     --Insert New ATTRIB_GROUP records
     
     if rec.stgrp_action = action_new then
        if EXEC_ATTRIB_GROUP_INS(L_error_message,
                                        L_ATTRIB_GROUP_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_GROUP_table,
                              rec.stgrp_row_seq,
                              NULL,
                              L_error_message);
                  L_attribgroup_error :=TRUE;
               end if;        
            end if;
            ------------------------------------------------------------------------
            ---Modify only if there is change from old value to new   
            if rec.stgrp_action = action_mod then
               if rec.stgrp_display_seq <> rec.old_display_seq  or 
           rec.stgrp_group_set_id <> rec.old_group_set_id or
           nvl(rec.stgrp_grp_view_name,'X') <> nvl(rec.old_group_view_name,'X')    then
                  if EXEC_ATTRIB_GROUP_UPD(L_error_message,
                                           L_ATTRIB_GROUP_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                          L_GROUP_table,
                          rec.stgrp_row_seq,
                          NULL,
                          L_error_message);
              L_attribgroup_error :=TRUE;
                  end if;           
        end if;
            end if;
            -----------------------------------------------------------------------
            --Delete the ATTRIB_GROUP record if action is DEL   
            
            if rec.stgrp_action = action_del then
               if EXEC_ATTRIB_GROUP_DEL(L_error_message,
                                 L_ATTRIB_GROUP_temp_rec)=FALSE then
                 WRITE_ERROR(I_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_GROUP_table,
                              rec.stgrp_row_seq,
                              NULL,
                             L_error_message);
                  L_attribgroup_error :=TRUE;
               end if;
            end if;
            
            ------------------------------------------------------------------------
           /* if L_attribgroup_error then
               if rec.stgrplbl_row_seq is NOT NULL then
           WRITE_ERROR(I_process_id,
                       svc_admin_upld_er_seq.nextval,
                       I_chunk_id,
                       L_GROUP_table,
                       rec.stgrplbl_row_seq,
                       'ATTRIB_GROUP_LABELS',
                       'ERROR_IN_GROUP_SET');
           L_attribgroup_error :=TRUE;            
               end if;
            end if;*/
            ------------------------------------------------------------------------
         end if;--process only the first header record for a given ATTRIB_GROUP
         ---------------------------------------------------------------------------
         --Check LABEL action is NEW,and ATTRIB_GROUP action is NULL and no existing reocrd in CFA_ATTRIB_GROUP_SET table.
	    if rec.stgrplbl_action = action_new then 
	        select count(group_id)
	       	    into L_grp_id
	       	    from cfa_attrib_group
	    where group_id = rec.stgrplbl_group_id;
	      if L_grp_id = 0 then
	         WRITE_ERROR(I_process_id,
	                     svc_admin_upld_er_seq.nextval,
	                     I_chunk_id,
	                     L_GROUP_LABELS_table,
	                     rec.stgrplbl_row_seq,
	                     'GROUP_ID',
	                     'INV_VALUE');
	         L_attribgroup_labels_error :=true;
	       end if;
	   end if;    
    
         if rec.stgrp_action IN (action_new,action_mod) or
            NVL(rec.stgrp_action,'X') = NVL(NULL,'X') then
            if rec.stgrplbl_action is NOT NULL and NOT L_attribgroup_error and NOT L_attribgroup_labels_error then
               L_ATTRIB_GROUP_LABELS_temp_rec.GROUP_ID           :=  rec.stgrplbl_group_id;
               L_ATTRIB_GROUP_LABELS_temp_rec.LANG               :=  rec.stgrplbl_lang;
               L_ATTRIB_GROUP_LABELS_temp_rec.LABEL              :=  rec.stgrplbl_label;
               L_ATTRIB_GROUP_LABELS_temp_rec.DEFAULT_LANG_IND   :=  'N';
               ----------------------------------------------------------------------
               --BEGIN SVC_ATTRIB_GROUP_SET_LABELS processing
               --------------------------------------------------------------------------------
               if rec.stgrplbl_action = action_new then
               
                  if EXEC_ATTRIB_GROUP_LABELS_INS(L_error_message,
                                                  L_ATTRIB_GROUP_LABELS_temp_rec) = FALSE  then
                     WRITE_ERROR(I_process_id,
                                 svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_GROUP_LABELS_table,
                          rec.stgrplbl_row_seq,
                          NULL,
                                 L_error_message);
                     L_atrbgplbl_process_error :=TRUE;                                      
                  end if;
               end if;                               
               ---------------------------------------------------------------------
               --Modify only if there is change from old value to new 
               
               if rec.stgrplbl_action = action_mod and nvl(rec.stgrplbl_label,'X') <> nvl(rec.oldlbl_label,'X') then
                  if EXEC_ATTRIB_GROUP_LABELS_UPD(L_error_message,
                                                  L_ATTRIB_GROUP_LABELS_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_GROUP_LABELS_table,
                          rec.stgrplbl_row_seq,
                          NULL,
                                 L_error_message);
                      L_atrbgplbl_process_error :=TRUE;                                                                              
                  end if;
               end if;
               -------------------------------------------------------------------
               
               if rec.stgrplbl_action = action_del then
                  if EXEC_ATTRIB_GROUP_LABELS_DEL(L_error_message,
                                           L_ATTRIB_GROUP_LABELS_temp_rec) = FALSE then
              WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_GROUP_LABELS_table,
                          rec.stgrplbl_row_seq,
                    NULL,
                         L_error_message);
              L_atrbgplbl_process_error :=TRUE;                                                                              
           end if;
               end if;
               -------------------------------------------------------------------
            end if;
         /*else
         
            WRITE_ERROR(I_process_id,
                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                 I_chunk_id,
                 L_GROUP_LABELS_table,
                 rec.stgrplbl_row_seq,
                 'CFA_ATTRIB_GROUP_LABELS',
                 'GROUP_DEL_REC'); --Associated GROUP header has been deleted.
            L_atrbgplbl_process_error :=TRUE;     */   
         end if;
         --------------------------------------------------------------------------
      end if;-- if NOT L_error
      --------------------------------------------------------------------------
      if NOT L_error or  NOT L_attribgroup_error then
         update svc_attrib_group st
            set process$status ='P'
          where rowid = rec.stgrp_rid
            and st.process$status != 'E';
      else
         update svc_attrib_group
            set process$status ='E'
         where rowid = rec.stgrp_rid;
      end if;
      if NOT L_attribgroup_labels_error or L_atrbgplbl_process_error then
         update svc_attrib_group_labels  
            set process$status ='P'
          where rowid = rec.stgrplbl_rid
            and process$status != 'E';
      else
         update svc_attrib_group_labels 
            set process$status ='E'
          where rowid = rec.stgrplbl_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_PRIMARY_LANG_EXIST%ISOPEN  then
         close C_CHECK_PRIMARY_LANG_EXIST;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
      
END PROCESS_CFA_ATTRIB_GROUP;
------------------------------------------------------------------------------
/* END ATTRIB_GROUP */
-------------------------------------------------------------------------------
-----------------------------------------------------------------------------
/* BEGIN REC_GROUP*/
-----------------------------------------------------------------------------
FUNCTION GEN_SEQ_REC_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_REC_GROUP.CHUNK_ID%TYPE)
   return BOOLEAN iS
   L_program VARCHAR2(75) := 'CORESVC_CFAS_ADMIN_SQL.GEN_SEQUENCE';   
   
   CURSOR c_new_recgrp is
   select recgrp.rec_group_id as orig_rec_group_id,
          recgrp.rowid rid,
          recgrp.row_seq,
          recgrp.rec_group_id         
     from svc_rec_group recgrp
     where recgrp.process_id  = I_process_id
       and recgrp.chunk_id = I_chunk_id
       and recgrp.action = action_new
       and recgrp.rec_group_id  is not null
       and recgrp.process$status = 'N';
    
    Type c_new_rec_grp_typ is TABLE OF c_new_recgrp%rowtype;
    c_new_rec_grp_tab   c_new_rec_grp_typ;
    L_error BOOLEAN:=false;
    L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN  
  OPEN c_new_recgrp;
  FETCH c_new_recgrp bulk collect INTO c_new_rec_grp_tab;
  CLOSE c_new_recgrp;
  FOR i IN 1..c_new_rec_grp_tab.count()
  LOOP
     if CFA_SETUP_REC_GROUP_SQL.GEN_NEXT_RECGRP_ID(L_error_message,
                                                   c_new_rec_grp_tab(i).rec_group_id) = FALSE then
        L_error := true;                                                   
     end if;
     IF L_error THEN
        UPDATE svc_rec_group
        SET process$status = 'E'
        WHERE rowid        = c_new_rec_grp_tab(i).rid;
        WRITE_ERROR(I_process_id, 
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id, 
                    L_REC_GROUP_table,
                    c_new_rec_grp_tab(i).row_seq, 
                    c_new_rec_grp_tab(i).orig_rec_group_id,
                    L_error_message);
     END IF;        
  END LOOP;
  --update labels table with generated record_group_id
  if c_new_rec_grp_tab is NOT NULL and c_new_rec_grp_tab.COUNT > 0 then
  forall i in 1..c_new_rec_grp_tab.count()
  UPDATE svc_rec_group_labels
     set rec_group_id = c_new_rec_grp_tab(i).rec_group_id
   where process_id   = I_process_id
    AND chunk_id      = I_chunk_id
    AND action        = action_new
    AND rec_group_id  = c_new_rec_grp_tab(i).orig_rec_group_id;
    
  forall i in 1..c_new_rec_grp_tab.count()
  UPDATE svc_rec_group
     set rec_group_id = c_new_rec_grp_tab(i).rec_group_id
   where process_id   = I_process_id
    AND chunk_id      = I_chunk_id
    AND action        = action_new
    AND rec_group_id          = c_new_rec_grp_tab(i).orig_rec_group_id;  
    end if;
    return true;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GEN_SEQ_REC_GROUP;
--------------------------------------------------------------------------------
FUNCTION EXEC_CFA_REC_LABELS_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 L_rec_labels_temp_rec   IN   CFA_REC_GROUP_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_REC_LABELS_INS';
   L_table   VARCHAR2(255):= 'SVC_REC_GROUP_LABELS';
BEGIN
   insert
     into cfa_rec_group_labels
   values L_rec_labels_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_REC_LABELS_INS;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_REC_LABELS_UPD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       L_rec_labels_temp_rec   IN   CFA_REC_GROUP_LABELS%ROWTYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_REC_LABELS_UPD';
   L_table   VARCHAR2(255):= 'SVC_REC_GROUP_LABELS';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_REC_GROUP_LABELS_LOCK is
     select 'X'
       from cfa_rec_group_labels
     where rec_group_id = L_rec_labels_temp_rec.rec_group_id
       and lang = L_rec_labels_temp_rec.lang;   
     
BEGIN
   open C_REC_GROUP_LABELS_LOCK;
   close C_REC_GROUP_LABELS_LOCK;
   update cfa_rec_group_labels
      set row = L_rec_labels_temp_rec
    where 1 = 1
      and rec_group_id = L_rec_labels_temp_rec.rec_group_id
      and lang = L_rec_labels_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_REC_GROUP_LABELS_LOCK%ISOPEN then
         close C_REC_GROUP_LABELS_LOCK;
      end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_REC_GROUP_LABELS',
                                                                L_rec_labels_temp_rec.rec_group_id,
                                                                NULL);
      return FALSE;
   when OTHERS then
      if C_REC_GROUP_LABELS_LOCK%ISOPEN then
         close C_REC_GROUP_LABELS_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_REC_LABELS_UPD;
------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_REC_LABELS_DEL( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              L_rec_labels_temp_rec  IN       CFA_REC_GROUP_LABELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_REC_LABELS_DEL';
   L_table   VARCHAR2(255):= 'SVC_REC_GROUP_LABELS';
   RECORD_LOCKED EXCEPTION;
      PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_REC_GROUP_LABELS_LOCK is
     select 'X'
       from cfa_rec_group_labels
     where rec_group_id = L_rec_labels_temp_rec.rec_group_id
       and lang = L_rec_labels_temp_rec.lang;   
     
BEGIN
    open C_REC_GROUP_LABELS_LOCK;
    close C_REC_GROUP_LABELS_LOCK;
    delete
     from cfa_rec_group_labels
    where 1 = 1
      and rec_group_id =L_rec_labels_temp_rec.rec_group_id
      and LANG = L_rec_labels_temp_rec.lang;
        
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_REC_GROUP_LABELS_LOCK%ISOPEN then
         close C_REC_GROUP_LABELS_LOCK;
      end if;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_REC_GROUP_LABELS',
                                                                L_rec_labels_temp_rec.rec_group_id,
                                                                NULL);
      return FALSE;
   when OTHERS then
     if C_REC_GROUP_LABELS_LOCK%ISOPEN then
        close C_REC_GROUP_LABELS_LOCK;
     end if;
    
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_REC_LABELS_DEL;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_REC_GROUP_VAL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error               IN OUT   BOOLEAN,
                               O_col_1_data_type     IN OUT   CFA_REC_GROUP.COL_1_DATA_TYPE%TYPE,
                              O_col_1_data_length   IN OUT   CFA_REC_GROUP.COL_1_DATA_LENGTH%TYPE,
                              O_query               IN OUT   CFA_REC_GROUP.QUERY%TYPE,
                               I_rec                 IN       C_SVC_REC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.PROCESS_REC_GROUP_VAL';
   L_table             VARCHAR2(255):='SVC_REC_GROUP';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE; 
   L_valid             VARCHAR2(1);
   L_date_format       VARCHAR2(16) := 'YYYYMMDDHH24MISS';
   Query_Gen           VARCHAR2(2000);
   L_wherecol1_datatyp VARCHAR2(255); 
   L_length            ALL_TAB_COLUMNS.DATA_LENGTH%TYPE; 
   L_quote_check       NUMBER(2);
   L_is_variable       NUMBER(2);
   L_trial             DATE;
   L_wherecol2_datatyp VARCHAR2(255); 
   L_query             CFA_REC_GROUP.QUERY%TYPE;
   L_valid_name        VARCHAR2(255);
   INV_SQL_NAME        EXCEPTION;
   PRAGMA              EXCEPTION_INIT(INV_SQL_NAME, -44004);
   L_dummy        NUMBER;
BEGIN
   ------------------------------------------------------------------------------------*/
   if I_rec.stgrecgrp_query_type = 'S' then
      --TABLE_NAME is NOT NULL
      if I_rec.stgrecgrp_table_name is not NULL then
         if CFA_SETUP_REC_GROUP_SQL.VALIDATE_TABLE(L_error_message,
                                                   I_rec.stgrecgrp_table_name)= false then
     WRITE_ERROR(I_rec.stgrecgrp_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stgrecgrp_chunk_id,
                        L_REC_GROUP_table,
                        I_rec.stgrecgrp_row_seq,
                        'TABLE_NAME',
                        L_error_message);
      O_error :=TRUE;
  end if;
         --COLUMN_1 is not null
         if I_rec.stgrecgrp_column_1 is not null then
            if CFA_SETUP_REC_GROUP_SQL.VALIDATE_COLUMN(L_error_message,
                                         I_rec.stgrecgrp_table_name,
                                                I_rec.stgrecgrp_column_1) = FALSE then
        WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'COLUMN_1',
                    L_error_message);
         O_error :=TRUE;
            end if;
            if CFA_SETUP_REC_GROUP_SQL.GET_COLUMN_DATATYPE(L_error_message,
                                                          O_col_1_data_type,
                                                          O_col_1_data_length,
                                                          I_rec.stgrecgrp_table_name,
                                                          I_rec.stgrecgrp_column_1) = FALSE then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                    'COLUMN_1',
                    L_error_message);
         O_error :=TRUE;
            end if; 
         else
            O_col_1_data_type := NULL;
           O_col_1_data_length := NULL;
         end if;
         
         --COLUMN_2 is not null
         if I_rec.stgrecgrp_column_2 is not null then
            if CFA_SETUP_REC_GROUP_SQL.VALIDATE_COLUMN(L_error_message,
                                                I_rec.stgrecgrp_table_name,
                                                I_rec.stgrecgrp_column_2) = FALSE then
        WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'COLUMN_2',
                    L_error_message);
        O_error :=TRUE;
            end if; 
         end if;
         -- WHERE_COLUMN1 is NOT NULL
         if I_rec.stgrecgrp_where_col_1 is not null then
            if CFA_SETUP_REC_GROUP_SQL.VALIDATE_COLUMN(L_error_message,
                                                I_rec.stgrecgrp_table_name,
                                                I_rec.stgrecgrp_where_col_1) = FALSE then
        WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_COL_1',
                    L_error_message);
        O_error :=TRUE;
            end if;
            if CFA_SETUP_REC_GROUP_SQL.GET_COLUMN_DATATYPE(L_error_message,
                                                          L_wherecol1_datatyp,
                                                          L_length,
                                                          I_rec.stgrecgrp_table_name,
                                                          I_rec.stgrecgrp_where_col_1) = FALSE then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_COL_1',
                    L_error_message);
        O_error :=TRUE; 
            end if;
            if I_rec.stgrecgrp_where_operator_1 is null then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_OPERATOR_1',
                           'MUST_ENTER_VALUE');
               O_error:=true;                        
            
            end if;
            if(I_rec.stgrecgrp_where_operator_1 in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_1 is not null) then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_COND_1',
                           'FIELD_NOT_ALLOWED'); -- it should be null.
               O_error:=true;          
            end if;
            if(I_rec.stgrecgrp_where_operator_1 not in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_1 is null) then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_COND_1',
                           'MUST_ENTER_VALUE');
               O_error:=true;          
            end if; 
            --Validate WHERE_COND_1 is a valid condition
            if(I_rec.stgrecgrp_where_operator_1 not in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_1 is not null) then
               L_quote_check := instr(I_rec.stgrecgrp_where_cond_1, '''');
               if L_quote_check != 0 then
                  WRITE_ERROR(I_rec.stgrecgrp_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.stgrecgrp_chunk_id,
                              L_REC_GROUP_table,
                              I_rec.stgrecgrp_row_seq,
                              'WHERE_COND_1',
                              'APOSTROPHE_NOT_ALLOWED'); 
                  O_error:=true;                  
               end if;
               L_is_variable := instr(I_rec.stgrecgrp_where_cond_1, ':');
               -- if L_is_variable > 0 user has entered a global variable such as :item to 
        -- be substituted when the rg is rendered. don't do date/number format validation on variables
               if L_is_variable = 0 then
                  -- ensure the condition value matches with the data type and length
                  if L_wherecol1_datatyp ='NUMBER' then
                     BEGIN
          L_dummy := to_number(I_rec.stgrecgrp_where_cond_1);
       EXCEPTION
        WHEN VALUE_ERROR THEN
                        WRITE_ERROR(I_rec.stgrecgrp_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_rec.stgrecgrp_chunk_id,
                                    L_REC_GROUP_table,
                                    I_rec.stgrecgrp_row_seq,
                                    'WHERE_COND_1',
                                    'COND_MUST_BE_NUMBER');
                       O_error:=true;                                      
              END;
                                         
                  elsif L_wherecol1_datatyp ='DATE' then
                     BEGIN
                        L_trial := to_date(I_rec.stgrecgrp_where_cond_1,L_date_format);
                     EXCEPTION
                        WHEN OTHERS THEN
                           --L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',I_rec.stgrecgrp_where_cond_1,L_date_format);
                           WRITE_ERROR(I_rec.stgrecgrp_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_rec.stgrecgrp_chunk_id,
                                       L_REC_GROUP_table,
                                       I_rec.stgrecgrp_row_seq,
                                       I_rec.stgrecgrp_where_cond_1,
                                       'INVALID_DATE'); 
                           O_error:=true;                            
                     END;   
                  end if;
               end if;
            end if;--Validate WHERE_COND_1 is a valid condition 
         end if;--WHERC_COL_1 is not null
         
         --if WHERE_COLUMN_2 is not null then check if the Where_column_1,operator1 and wherecond1 is not null
         if I_rec.stgrecgrp_where_col_2 is not null then
            if CFA_SETUP_REC_GROUP_SQL.VALIDATE_COLUMN(L_error_message,
                                                I_rec.stgrecgrp_table_name,
                                                I_rec.stgrecgrp_where_col_2) = FALSE then
        WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_COL_2',
                    L_error_message);
        O_error :=TRUE;
            end if;
            if CFA_SETUP_REC_GROUP_SQL.GET_COLUMN_DATATYPE(L_error_message,
                                                          L_wherecol2_datatyp,
                                                          L_length,
                                                          I_rec.stgrecgrp_table_name,
                                                          I_rec.stgrecgrp_where_col_2) = FALSE then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_COL_2',
                    L_error_message);
        O_error :=TRUE; 
            
            end if;
            if I_rec.stgrecgrp_where_col_1 is null then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_COL_1',
                           'MUST_ENTER_VALUE');
               O_error:=true;                        
            
            end if;            
                        
            if I_rec.stgrecgrp_where_operator_1 is null then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_OPERATOR_1',
                           'MUST_ENTER_VALUE');
               O_error:=true;                        
            
            end if;            
            if(I_rec.stgrecgrp_where_operator_1 in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_1 is not null) then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_COND_1',
                           'FIELD_NOT_ALLOWED'); -- it should be null.
               O_error:=true;          
            end if;  
            if(I_rec.stgrecgrp_where_operator_1 not in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_1 is null) then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.stgrecgrp_chunk_id,
                           L_REC_GROUP_table,
                           I_rec.stgrecgrp_row_seq,
                           'WHERE_COND_1',
                           'MUST_ENTER_VALUE');
               O_error:=true;          
            end if; 
            if I_rec.stgrecgrp_where_operator_2 is null then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_OPERATOR_2',
                    'MUST_ENTER_VALUE'); 
        O_error:=true;                 
            end if;  
            if (I_rec.stgrecgrp_where_operator_2 not in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_2 is null) then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_COND_2',
                    'MUST_ENTER_VALUE'); 
        O_error:=true;                 
            end if;
            if (I_rec.stgrecgrp_where_operator_2  in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_2 is not null) then
               WRITE_ERROR(I_rec.stgrecgrp_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_rec.stgrecgrp_chunk_id,
                    L_REC_GROUP_table,
                    I_rec.stgrecgrp_row_seq,
                    'WHERE_COND_2',
                    'FIELD_NOT_ALLOWED'); 
        O_error:=true;                 
            end if; 
            --Validate WHERE_COND_2
            if (I_rec.stgrecgrp_where_operator_2 not in('NULL','!NULL') and I_rec.stgrecgrp_where_cond_2 is not null) then 
               L_quote_check := instr(I_rec.stgrecgrp_where_cond_2, '''');
               if L_quote_check != 0 then
                  WRITE_ERROR(I_rec.stgrecgrp_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.stgrecgrp_chunk_id,
                              L_REC_GROUP_table,
                              I_rec.stgrecgrp_row_seq,
                              'WHERE_COND_2',
                              'APOSTROPHE_NOT_ALLOWED'); 
                  O_error:=true;                  
               end if;
               L_is_variable := instr(I_rec.stgrecgrp_where_cond_2, ':');
               -- if L_is_variable > 0 user has entered a global variable such as :item to 
        -- be substituted when the rg is rendered. don't do date/number format validation on variables
               if L_is_variable = 0 then
                  -- ensure the condition value matches with the data type and length
                  if L_wherecol2_datatyp ='NUMBER' then
                     BEGIN
          L_dummy := to_number(I_rec.stgrecgrp_where_cond_2);
       EXCEPTION
        WHEN VALUE_ERROR THEN
                        WRITE_ERROR(I_rec.stgrecgrp_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_rec.stgrecgrp_chunk_id,
                                    L_REC_GROUP_table,
                                    I_rec.stgrecgrp_row_seq,
                                    'WHERE_COND_2',
                                    'COND_MUST_BE_NUMBER');
                       O_error:=true;                                      
              END;                  
                    
                  elsif L_wherecol2_datatyp ='DATE' then
                     BEGIN
                        L_trial := to_date(I_rec.stgrecgrp_where_cond_2,L_date_format);
                     EXCEPTION
                        WHEN OTHERS THEN
                           --L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',I_rec.stgrecgrp_where_cond_1,L_date_format);
                           WRITE_ERROR(I_rec.stgrecgrp_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_rec.stgrecgrp_chunk_id,
                                       L_REC_GROUP_table,
                                       I_rec.stgrecgrp_row_seq,
                                       'WHERE_COND_2',
                                       'INVALID_DATE'); 
                           O_error:=true;                            
                     END;   
                  end if;
               end if;            
            end if;
                       
         end if; --if I_rec.stgrecgrp_where_col_2 is not null        
         
      end if;--I_rec.stgrecgrp_table_name not null
      if NOT O_error then
        if CFA_SETUP_REC_GROUP_SQL.BUILD_QUERY(L_error_message,
                                            I_rec.stgrecgrp_table_name,
                                            I_rec.stgrecgrp_column_1,
                                            I_rec.stgrecgrp_column_2,
                                            I_rec.stgrecgrp_where_col_1,
                                            L_wherecol1_datatyp,
                                            I_rec.stgrecgrp_where_operator_1,
                                            I_rec.stgrecgrp_where_cond_1,
                                            I_rec.stgrecgrp_where_col_2,
                                            L_wherecol2_datatyp,
                                            I_rec.stgrecgrp_where_operator_2,
                                            I_rec.stgrecgrp_where_cond_2,
                                            L_date_format,
                                            L_query) = FALSE then
             WRITE_ERROR(I_rec.stgrecgrp_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.stgrecgrp_chunk_id,
                         L_REC_GROUP_table,
                         I_rec.stgrecgrp_row_seq,
                         NULL,
                         L_error_message); 
     O_error:=true;                                            
         else
            O_query :=L_query;
         end if; 
      end if;
   end if;--QUERY_TYPE 'S'

  
   if I_rec.stgrecgrp_query_type = 'C' then
      if I_rec.stgrecgrp_table_name is not NULL or I_rec.stgrecgrp_column_1 is not NULL or I_rec.stgrecgrp_column_2 is not NULL or
         I_rec.stgrecgrp_where_col_1 is not NULL or I_rec.stgrecgrp_where_operator_1 is not NULL or I_rec.stgrecgrp_where_cond_1 is not NULL or 
         I_rec.stgrecgrp_where_col_2 is not NULL or I_rec.stgrecgrp_where_operator_2 is not NULL or I_rec.stgrecgrp_where_cond_2 is not NULL then
         
         WRITE_ERROR(I_rec.stgrecgrp_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.stgrecgrp_chunk_id,
                     L_REC_GROUP_table,
                     I_rec.stgrecgrp_row_seq,
                     NULL,
                     'COMPLEX_QUERY_WARNING'); 
  O_error:=true;             
      end if;
  
      if I_rec.stgrecgrp_action = action_new then
         O_query:= '/* this is the default query.';
      else
         O_query := I_rec.old_query;
      end if;
   end if;

  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_REC_GROUP_VAL;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_REC_GROUP_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_cfa_rec_group_temp_rec   IN       CFA_REC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_REC_GROUP_INS';
   L_table   VARCHAR2(255):= 'SVC_REC_GROUP';
BEGIN
   insert
     into cfa_rec_group
   values L_cfa_rec_group_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_REC_GROUP_INS;
-------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_REC_GROUP_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_cfa_rec_group_temp_rec   IN       CFA_REC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_REC_GROUP_UPD';
   L_table   VARCHAR2(255):= 'SVC_REC_GROUP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
  
   cursor C_REC_GROUP_LOCK is
      select 'X'
        from cfa_rec_group
       where rec_group_id = L_cfa_rec_group_temp_rec.rec_group_id;
BEGIN
   open C_REC_GROUP_LOCK;
   close C_REC_GROUP_LOCK;
   update cfa_rec_group
      set row = L_cfa_rec_group_temp_rec
    where 1 = 1
      and rec_group_id = L_cfa_rec_group_temp_rec.rec_group_id
;
   return TRUE;
 EXCEPTION
    when RECORD_LOCKED then
         if C_REC_GROUP_LOCK%ISOPEN then
            close C_REC_GROUP_LOCK;
         end if;  
         O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                  'SVC_REC_GROUP',
                                                                   L_cfa_rec_group_temp_rec.rec_group_id,
                                                                   NULL);
         return FALSE;
   when OTHERS then
       if C_REC_GROUP_LOCK%ISOPEN then
          close C_REC_GROUP_LOCK;
       end if;  
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_REC_GROUP_UPD;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ON_DELETE_REC_GROUP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error          IN OUT BOOLEAN,
                                   I_rec            IN OUT C_SVC_REC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.CHECK_ON_DELETE_REC_GROUP';
BEGIN
   if CFA_SETUP_REC_GROUP_SQL.REC_GROUP_DELETE(O_error_message,
                                               I_rec.stgrecgrp_rec_group_id) = FALSE then          
      WRITE_ERROR(I_rec.stgrecgrp_process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.stgrecgrp_chunk_id,
                  'SVC_REC_GROUP',
                  I_rec.stgrecgrp_row_seq,
                  NULL,
                  O_error_message); 
     O_error := true;                  
    end if;
  
   return true;
EXCEPTION
  when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ON_DELETE_REC_GROUP;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CFA_REC_GROUP_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_cfa_rec_group_temp_rec   IN       CFA_REC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(255):= 'CORESVC_CFAS_ADMIN_SQL.EXEC_CFA_REC_GROUP_DEL';
   L_table          VARCHAR2(255):= 'SVC_REC_GROUP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

  cursor C_REC_GROUP_LOCK is
   select 'X'
     from cfa_rec_group
    where rec_group_id = L_cfa_rec_group_temp_rec.rec_group_id;
   
BEGIN
   open C_REC_GROUP_LOCK;
   close C_REC_GROUP_LOCK;
   delete
     from cfa_rec_group
    where 1 = 1
      and rec_group_id = L_cfa_rec_group_temp_rec.rec_group_id;
   return TRUE;
EXCEPTION
  when RECORD_LOCKED then
      if C_REC_GROUP_LOCK%ISOPEN then
         close C_REC_GROUP_LOCK;
      end if;   
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               L_cfa_rec_group_temp_rec.rec_group_id,
                                                               NULL);
      return FALSE;
   when OTHERS then
      if C_REC_GROUP_LOCK%ISOPEN then
         close C_REC_GROUP_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CFA_REC_GROUP_DEL;
-------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CFA_REC_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_REC_GROUP.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_REC_GROUP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_recgrp_error                 BOOLEAN;
   L_recgrplbl_error              BOOLEAN;
   L_recgrplbl_process_error      BOOLEAN;
   L_error                        BOOLEAN;
   L_program                      VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS_CFA_REC_GROUP';
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;
   L_REC_GROUP_temp_rec           CFA_REC_GROUP%ROWTYPE;
   L_REC_GROUP_LABEL_temp_rec     CFA_REC_GROUP_LABELS%ROWTYPE;
   L_table                        VARCHAR2(255)          :='SVC_REC_GROUP';
   L_exist_primary_label          NUMBER;
   L_col_1_data_type              CFA_REC_GROUP.COL_1_DATA_TYPE%TYPE;
   L_col_1_data_length            CFA_REC_GROUP.COL_1_DATA_LENGTH%TYPE; 
   L_query                        CFA_REC_GROUP.QUERY%TYPE;
   L_rec_grp_id                   NUMBER;
   
   
   
   cursor C_CHECK_PRIMARY_LANG_EXIST is
       select rec_group_id,row_seq
         from svc_rec_group recgrp
        where action ='NEW' 
          and rec_group_id is NOT NULL
          and process_id = I_process_id
          and chunk_id = I_chunk_id
          and not exists(select *
                     from svc_rec_group_labels labels,
                          system_options sysopt
                    where labels.action ='NEW'
                      and labels.lang = sysopt.data_integration_lang
                      and labels.rec_group_id =recgrp.rec_group_id
                      and labels.process_id = recgrp.process_id
                     and labels.chunk_id = recgrp.chunk_id);
    
BEGIN
--Check if RECORD_GROUP is a INS and there is no primary_lang exist in LABEL sheet
   for rec in  C_CHECK_PRIMARY_LANG_EXIST
   LOOP
     WRITE_ERROR(I_process_id,
                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                 I_chunk_id,
                 L_REC_GROUP_table,
                 rec.row_seq,
                 'CFA_RECORD_GROUP',
                 'PRIMARY_LANG_REQ');    -- Atleast one label with Primary language required.
     if rec.rec_group_id is not null then
        update svc_rec_group_labels
           set process$status='E'
  where action ='NEW'
           and process_id =I_process_id  
           and chunk_id =I_chunk_id
           and rec_group_id = rec.rec_group_id;
           
          update svc_rec_group
           set process$status='E'
  where action ='NEW'
           and process_id =I_process_id  
           and chunk_id =I_chunk_id
           and rec_group_id = rec.rec_group_id;  
     end if;
   END LOOP;
   if GEN_SEQ_REC_GROUP(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  0,
                  NULL,
                  O_error_message); 
   end if;   

   FOR rec IN c_svc_REC_GROUP(I_process_id,I_chunk_id)
   LOOP
      L_recgrp_error                := FALSE;
      L_recgrplbl_error             := FALSE;
      L_recgrplbl_process_error     := FALSE;
      L_error                       := FALSE;
      --------------------------------------------------------------
      --Validation for GROUP_SET  if rank is 1
      -------------------------------------------------------------
      if rec.recgrp_rank = 1 then
         -------------------------------------------------------------------------------------------
         --Check if both actions are NULL or GROUP_SET action is other than NEW,MOD,DEL
         if ((rec.stgrecgrp_action is NULL and rec.stgreclabel_action is NULL)
             or rec.stgrecgrp_action NOT IN (action_new,action_mod,action_del)) then
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_REC_GROUP_table,
                          rec.stgrecgrp_row_seq,
                          'ACTION',
                          'INV_ACT');
              L_error :=true;
         end if;  
         if (rec.stgrecgrp_action is NULL and rec.stgrecgrp_rid is NOT NULL) then
             WRITE_ERROR( I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_REC_GROUP_table,
                          rec.stgrecgrp_row_seq,
                         'ACTION',
                         'INV_ACT');
             L_error :=TRUE;
         end if;
         
         if (rec.stgreclabel_action IS NULL and rec.fk_cfa_rec_group_labels_rid IS NOT NULL)then  
             WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.nextval,
                         I_chunk_id,
                         L_REC_GROUP_table,
                         rec.stgrecgrp_row_seq,
                         'ACTION',
                         'INV_ACT');
            L_error :=TRUE;
         end if;         
         -----------------------------------------------------------------------------------------
         -- Check if the newly entered Record Group Id is already exist
         if rec.stgrecgrp_action = action_new and rec.fk_cfa_rec_group_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                        'REC_GROUP_ID',
                        'DUP_RECORD');
            L_error :=TRUE;
         end if;  
         ------------------------------------------------------------------------------------------
         --Check if the given RECORD_GROUP_ID is missing during action MOD,DEL
         if rec.stgrecgrp_action IN (action_mod,action_del) and rec.fk_cfa_rec_group_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                        'REC_GROUP_ID',
                        'NO_RECORD');
            L_error :=TRUE;
         end if;
         -----------------------------------------------------------------------------------------
         --Check if the RECORD_GROUP_ID is missing while action is NEW
         if rec.stgrecgrp_action = action_new and rec.stgrecgrp_rec_group_id is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                        'REC_GROUP_ID',
                       'MUST_ENTER_VALUE');
            L_error :=TRUE;
         end if; 
         -----------------------------------------------------------------------------------------
         --Check if the action is NEW and MOD and RECORD_GROUP_NAME is NULL
         if (rec.stgrecgrp_action IN (action_new,action_mod) and (rec.stgrecgrp_rec_group_name  IS NULL or
                                                                  rec.stgrecgrp_query_type is NULL)) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                       'RECORD_GROUP_NAME , QUERY_TYPE',
                       'ENTER_VALUE');
            L_error :=TRUE;
         end if;         
         ------------------------------------------------------------------------------------------
         --Validation for Query type
         if (rec.stgrecgrp_action IN (action_new,action_mod) and rec.stgrecgrp_query_type not in('S','C')) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                       'QUERY_TYPE',
                       'ENTER_VALID_QUERY_TYPE');
            L_error :=TRUE;            
         end if;
         ------------------------------------------------------------------------------------------
         if (rec.stgrecgrp_action = action_mod and rec.stgrecgrp_query_type <> rec.old_query_type) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                       'QUERY_TYPE',
                       'FIELD_NO_MOD');
            L_error :=TRUE;             
         end if;
         --Validation of column and table fields if Query Type is 'Simple'.
         if(rec.stgrecgrp_action IN (action_new,action_mod) and rec.stgrecgrp_query_type ='S') then
            if rec.stgrecgrp_table_name is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'TABLE_NAME',
                           'TABLE_REQ');
               L_error :=TRUE;                           
            end if;
            if rec.stgrecgrp_column_1 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'COLUMN_1',
                           'DESC_COL_REQ');  
               L_error :=TRUE;                                                      
            end if;
            if rec.stgrecgrp_column_2 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'COLUMN_2',
                           'VAL_COL_REQ'); 
               L_error :=TRUE;                                                      
            end if; 
            -- if the operator or condition for statement 1 is not null, the where column 1 is required.
            -- if condition 2 is entered, condition 1 is required
            if (rec.stgrecgrp_where_col_2 is not null or rec.stgrecgrp_where_operator_1 is not null or
               rec.stgrecgrp_where_cond_1 is not null) and rec.stgrecgrp_where_col_1 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'WHERE_COL_1',
                           'WHERE_COL_1_REQ');   
               L_error :=TRUE;                           
            
            end if;
            if rec.stgrecgrp_where_col_1 is not null then
               if rec.stgrecgrp_where_operator_1 is null then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_REC_GROUP_table,
                              rec.stgrecgrp_row_seq,
                              'WHERE_OPERATOR_1',
                              'OPERATOR_1_REQ');                                  
                   L_error :=TRUE;                                          
               end if;
            end if;
            if rec.stgrecgrp_where_operator_1 not in ('NULL','!NULL')
        and rec.stgrecgrp_where_cond_1 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'WHERE_COND_1',
                           'COND_1_REQ');  
               L_error :=TRUE;                                                      
     end if;
     -- if operator or condition 2 are not null, then where col 2 is required
            if (rec.stgrecgrp_where_operator_2 is NOT NULL or rec.stgrecgrp_where_cond_2 is NOT NULL)
              and rec.stgrecgrp_where_col_2 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'WHERE_COL_2',
                           'WHERE_COL_2_REQ');   
               L_error :=TRUE;                                                      
            end if;
            if rec.stgrecgrp_where_col_2 is NOT NULL then
               if rec.stgrecgrp_where_operator_2 is NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_REC_GROUP_table,
                              rec.stgrecgrp_row_seq,
                              'WHERE_OPERATOR_2',
                              'OPERATOR_2_REQ');  
                  L_error :=TRUE;                                                         
               end if;
            end if;
            if rec.stgrecgrp_where_operator_2 not in ('NULL','!NULL') 
               and rec.stgrecgrp_where_cond_2 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_REC_GROUP_table,
                           rec.stgrecgrp_row_seq,
                           'WHERE_COND_2',
                           'COND_2_REQ');    
               L_error :=TRUE;                                                         
            end if;     
         end if;--QUERY Type 'S'imple
         ------------------------------------------------------------------------------------------- 
         if not L_error then
           --Validation for each RECORD GROUP table columns
            if rec.stgrecgrp_action in (action_new,action_mod) then
               if PROCESS_REC_GROUP_VAL(L_error_message,
                                        L_error,
                                        L_col_1_data_type,
                                        L_col_1_data_length,
                                        L_query,
                                        rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_REC_GROUP_table,
                              rec.stgrecgrp_row_seq,
                              NULL,
                              L_error_message);
                  L_error :=TRUE;
               end if;
            end if; 
         end if; 
      end if;-- rank ==1
      --------------------------------------------------------------------
      --VALIDATION FOR RECORD_GROUP_LABELS record
      --------------------------------------------------------------------
      if rec.stgreclabel_action NOT IN (action_new,action_mod,action_del) then     
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_REC_GROUP_LABELS_table,
                     rec.stgreclabel_row_seq,
                     'ACTION',
                     'INV_ACT');
          L_recgrplbl_error :=true;
      end if;
      ----------------------------------------------------------------------
      -- Check if action is NEW and if RECORD_GROUP_LABEL exist for the record_group_id
      if rec.stgreclabel_action = action_new and rec.fk_cfa_rec_group_labels_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
              svc_admin_upld_er_seq.nextval,
              I_chunk_id,
              L_REC_GROUP_LABELS_table,
              rec.stgreclabel_row_seq,
              'RECORD_GROUP_ID',
              'DUP_RECORD');
         L_recgrplbl_error :=true;
      end if;
      ------------------------------------------------------------------------------------
      -- Check if LABEL action is MOD , DEL and if RECORD_GROUP_LABEL doesnt exists
      if rec.stgreclabel_action IN (action_mod,action_del) and rec.fk_cfa_rec_group_labels_rid is NULL then     
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_REC_GROUP_LABELS_table,
                     rec.stgreclabel_row_seq,
                     'RECORD_GROUP_ID',
                     'NO_RECORD');
         L_recgrplbl_error :=true;
      end if;      
      ----------------------------------------------------------------------------------      
      --Check if LABEL action is NEW,MOD and LANG  value is NULL
      if rec.stgreclabel_action IN (action_new,action_mod) then
         if rec.stgreclabel_rec_group_id IS NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_REC_GROUP_LABELS_table,
                        rec.stgreclabel_row_seq,
                        'RECORD_GROUP_ID',
                        'MUST_ENTER_VALUE');

             L_recgrplbl_error :=true;
         end if;
         if rec.stgreclabel_lang is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_REC_GROUP_LABELS_table,
                        rec.stgreclabel_row_seq,
                        'LANG',
                        'LANG_REQ');

             L_recgrplbl_error :=true; 
         end if;
         if rec.stgreclabel_lov_title is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_REC_GROUP_LABELS_table,
                        rec.stgreclabel_row_seq,
                        'LOV_TITLE',
                        'MUST_ENTER_VALUE');

             L_recgrplbl_error :=true;          
         end if;
         if rec.stgreclabel_lov_col1_header is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_REC_GROUP_LABELS_table,
                        rec.stgreclabel_row_seq,
                        'LOV_COL1_HEADER',
                        'MUST_ENTER_VALUE');

             L_recgrplbl_error :=true;          
         end if;         
         if rec.stgreclabel_lov_col2_header is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_REC_GROUP_LABELS_table,
                        rec.stgreclabel_row_seq,
                        'LOV_COL2_HEADER',
                        'MUST_ENTER_VALUE');
            L_recgrplbl_error :=true;          
         end if;           
      end if;
      ---------------------------------------------------------------------------
     /* if rec.stgrecgrp_action = action_del and rec.stgreclabel_rec_group_id IS NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_REC_GROUP_LABELS_table,
                     rec.stgreclabel_row_seq,
                     'RECORD_GROUP_ID',
                     'MUST_ENTER_VALUE');
         L_recgrplbl_error :=true;
      end if;  */  
      ---------------------------------------------------------------------------       
      --Check LABEL action is MOD and Lang is modified
      if rec.stgreclabel_action = action_mod and rec.stgreclabel_lang <> rec.oldlbl_lang then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_REC_GROUP_LABELS_table,
                     rec.stgreclabel_row_seq,
                     'LANG',
                     'CANNOT_MODIFY_LANG');
         L_recgrplbl_error :=true;
      end if;
      if rec.stgrecgrp_action = action_new or rec.stgreclabel_action = action_mod then
         if rec.fk_lang_rid is null then
            WRITE_ERROR(I_process_id,
           svc_admin_upld_er_seq.nextval,
          I_chunk_id,
          L_REC_GROUP_LABELS_table,
          rec.stgreclabel_row_seq,
          'LANG',
        'LANGUAGE_NOT_FOUND');
            
            L_recgrplbl_error :=true;        
         end if; 
      end if;
      
      --Check if action is DEL and the lang to be deleted is equal to DATA_INTEGRATION_LANG
      if rec.stgreclabel_action =action_del and rec.stgreclabel_lang = rec.data_integration_lang 
         and (rec.stgrecgrp_action != action_del or rec.stgrecgrp_rid is NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_REC_GROUP_LABELS_table,
                     rec.stgreclabel_row_seq,
                     'LANG',
                     'PRIMARY_LANG_REQ');
         L_recgrplbl_error :=true;             
      end if;
      if (rec.stgrecgrp_action = action_del) then
         if CHECK_ON_DELETE_REC_GROUP(O_error_message,
                                      L_error,
                                      rec)=FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.stgrecgrp_row_seq,
                        NULL,
                        O_error_message);
             L_error :=TRUE;
         end if;
      end if;
      ------------------------------------------------------------------------------------------------
     /* --Check if any RECORD_GROUP is having error
      if L_error then      
         if rec.stgreclabel_row_seq IS NOT NULL then              
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_REC_GROUP_table,
                        rec.stgrecgrp_row_seq,
                        'RECORD_GROUP_ID',
                        'ERROR_IN_RECORD_GROUP');
            L_recgrp_error :=true;             
         end if;    
      end if;*/
      ------------------------------------------------------------------------------------------
      --BEGIN CFA_RECORD_GROUP Processing
      ------------------------------------------------------------------------------------------
      if NOT L_error then
         -- process only the first header record for a given RECORD_GROUP
         if rec.stgrecgrp_action is NOT NULL and rec.recgrp_rank = 1 then
            L_REC_GROUP_temp_rec.rec_group_id            := rec.stgrecgrp_rec_group_id; 
            L_REC_GROUP_temp_rec.rec_group_name          := rec.stgrecgrp_rec_group_name;
            L_REC_GROUP_temp_rec.query_type              := rec.stgrecgrp_query_type;
            L_REC_GROUP_temp_rec.query                   := L_query; --***
            L_REC_GROUP_temp_rec.table_name              := rec.stgrecgrp_table_name;
            L_REC_GROUP_temp_rec.column_1                := rec.stgrecgrp_column_1;
            L_REC_GROUP_temp_rec.col_1_data_length       := L_col_1_data_length;--***
            L_REC_GROUP_temp_rec.col_1_data_type         := L_col_1_data_type;--***
            L_REC_GROUP_temp_rec.column_2                := rec.stgrecgrp_column_2;
            L_REC_GROUP_temp_rec.where_col_1             := rec.stgrecgrp_where_col_1;
            L_REC_GROUP_temp_rec.where_operator_1        := rec.stgrecgrp_where_operator_1;
            L_REC_GROUP_temp_rec.where_cond_1            := rec.stgrecgrp_where_cond_1;
            L_REC_GROUP_temp_rec.where_col_2             := rec.stgrecgrp_where_col_2;
            L_REC_GROUP_temp_rec.where_operator_2        := rec.stgrecgrp_where_operator_2;
            L_REC_GROUP_temp_rec.where_cond_2            := rec.stgrecgrp_where_cond_2;
            if rec.stgrecgrp_action = action_new then
        L_REC_GROUP_temp_rec.base_ind             := 'N';
     else
        L_REC_GROUP_temp_rec.base_ind             := rec.old_base_ind;
     end if;
            
            
            --------------------------------------------------------------------
            --Insert New RECORD_GROUP records
            if rec.stgrecgrp_action = action_new then
               L_REC_GROUP_temp_rec.base_ind                := 'N';
               if EXEC_CFA_REC_GROUP_INS(L_error_message,
                                         L_REC_GROUP_temp_rec) = FALSE then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_REC_GROUP_table,
                               rec.stgrecgrp_row_seq,
                               NULL,
                               L_error_message);
                   L_recgrp_error :=TRUE;
               end if;                
            end if; -- INSERT ends
            ----------------------------------------------------------------------
            ---Modify only if there is change from old value to new 
            if rec.stgrecgrp_action = action_mod then
               if nvl(rec.stgrecgrp_rec_group_name,'X') <> nvl(rec.old_rec_group_name,'X') or
                  (rec.stgrecgrp_query_type = 'S' and (rec.stgrecgrp_table_name <> rec.old_table_name or
                   rec.stgrecgrp_column_1 <> rec.old_column_1 or nvl(rec.stgrecgrp_column_2,'X') <> nvl(rec.old_column_2,'X') or
                   nvl(rec.stgrecgrp_where_col_1,'X') <> nvl(rec.old_where_col_1,'X') or
                   nvl(rec.stgrecgrp_where_operator_1,'X')<> nvl(rec.old_where_operator_1,'X') or
                   nvl(rec.stgrecgrp_where_cond_1,'X') <> nvl(rec.old_where_cond_1,'X') or
                   nvl(rec.stgrecgrp_where_col_2,'X') <> nvl(rec.old_where_col_2,'X') or
                   nvl(rec.stgrecgrp_where_operator_2,'X') <> nvl(rec.old_where_operator_2,'X') or 
                   nvl(rec.stgrecgrp_where_cond_2,'X') <> nvl(rec.old_where_cond_2,'X'))) then
                  if EXEC_CFA_REC_GROUP_UPD(L_error_message,
                                            L_REC_GROUP_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_REC_GROUP_table,
                                 rec.stgrecgrp_row_seq,
                                 NULL,
                                 L_error_message);
                     L_recgrp_error :=TRUE;
                  end if;               
               end if;   
            end if;
            --------------------------------------------------------------------
            --Delete the RECORD GROUP record if action is DEL
            if rec.stgrecgrp_action = action_del then
               if EXEC_CFA_REC_GROUP_DEL(L_error_message,
                                         L_REC_GROUP_temp_rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_REC_GROUP_table,
                       rec.stgrecgrp_row_seq,
                       NULL,
                       L_error_message);
           L_recgrp_error :=TRUE;
               end if;
            end if;            
            --------------------------------------------------------------------
         end if;-- process only the first header record for a given RECORD_GROUP
         -----------------------------------------------------------------------
         --Check LABEL action is NEW,and REC_GROUP action is NULL and no existing reocrd in CFA_REC_GROUP table.
         if rec.stgreclabel_action = action_new then     
            select count(rec_group_id)
            into L_rec_grp_id
            from cfa_rec_group
            where rec_group_id = rec.stgreclabel_rec_group_id;
            if L_rec_grp_id = 0 then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.nextval,
                           I_chunk_id,
                           L_REC_GROUP_LABELS_table,
                           rec.stgreclabel_row_seq,
                           'RECORD_GROUP_ID',
                           'INV_VALUE');
               L_recgrplbl_error :=true;
            end if;   
         end if; 
        ----------------------------------------------------------------------------------
             
         if rec.stgrecgrp_action IN (action_new,action_mod) or
            NVL(rec.stgrecgrp_action,'X') = NVL(NULL,'X') then
            if rec.stgreclabel_action is NOT NULL and NOT L_recgrp_error and NOT L_recgrplbl_error then
               L_REC_GROUP_LABEL_temp_rec.REC_GROUP_ID := rec.stgreclabel_rec_group_id;
               L_REC_GROUP_LABEL_temp_rec.LANG := rec.stgreclabel_lang;
               L_REC_GROUP_LABEL_temp_rec.LOV_TITLE := rec.stgreclabel_lov_title;
               L_REC_GROUP_LABEL_temp_rec.LOV_COL1_HEADER := rec.stgreclabel_lov_col1_header;
               L_REC_GROUP_LABEL_temp_rec.LOV_COL2_HEADER := rec.stgreclabel_lov_col2_header;
               if rec.stgreclabel_lang = rec.data_integration_lang then
                  L_REC_GROUP_LABEL_temp_rec.DEFAULT_LANG_IND   :=  'Y';
               else
                  L_REC_GROUP_LABEL_temp_rec.DEFAULT_LANG_IND   :=  'N';
               end if;
               ----------------------------------------------------------------------
               --BEGIN SVC_RECORD_GROUP_LABEL Processing
               ----------------------------------------------------------------------
               if rec.stgreclabel_action = action_new then
                  if EXEC_CFA_REC_LABELS_INS(L_error_message,
                                             L_REC_GROUP_LABEL_temp_rec) = FALSE  then
                     WRITE_ERROR(I_process_id,
                                 svc_admin_upld_er_seq.nextval,
                                 I_chunk_id,
                                 L_REC_GROUP_LABELS_table,
                                 rec.stgreclabel_row_seq,
                          NULL,
                                 L_error_message);
                     L_recgrplbl_process_error :=TRUE;                                      
                  end if;               
               end if;
               -------------------------------------------------------------------------
               ----Modify only if there is change from old value to new 
               if rec.stgreclabel_action = action_mod and(
                  rec.stgreclabel_lov_title <> rec.oldlbl_lov_title or
                  rec.stgreclabel_lov_col1_header <> rec.oldlbl_col1_header or
                  rec.stgreclabel_lov_col2_header <> rec.oldlbl_col2_header) then
                  if EXEC_CFA_REC_LABELS_UPD(L_error_message,
                                             L_REC_GROUP_LABEL_temp_rec)=FALSE then
                     WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_REC_GROUP_LABELS_table,
                          rec.stgreclabel_row_seq,
                          NULL,
                                 L_error_message);
                     L_recgrplbl_process_error :=TRUE;                                                                              
                  end if;
               end if;            
               -----------------------------------------------------------------
               if rec.stgreclabel_action = action_del  then
                  if EXEC_REC_LABELS_DEL(L_error_message,
                                      L_REC_GROUP_LABEL_temp_rec) = FALSE then
              WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_REC_GROUP_LABELS_table,
                         rec.stgreclabel_row_seq,
                         NULL,
                          L_error_message);
                      L_recgrplbl_process_error :=TRUE;                                                                              
                   end if;
                end if;            
                --------------------------------------------------------------------
            end if;    
            
        /* else    
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_REC_GROUP_LABELS_table,
                        rec.stgreclabel_row_seq,
                        'CFA_REC_GROUP_LABELS',
                        'RECORD_GROUP_DEL_REC'); --Associated RECORD_GROUP header has been deleted.
            L_recgrplbl_process_error :=TRUE;  */              
         end if;   
      end if;--NOT L_error
       ----------------------------------------------------------------------------------
      if NOT L_error or  NOT L_recgrp_error then
        update svc_rec_group st
     set process$status ='P'
          where rowid = rec.stgrecgrp_rid
           and st.process$status != 'E';
      else
        update svc_rec_group st
            set process$status ='E'
          where rowid = rec.stgrecgrp_rid;
     
      end if;
      if NOT L_recgrplbl_error or L_recgrplbl_process_error then
        update svc_rec_group_labels st
           set process$status ='P'
         where rowid = rec.stgreclabel_rid
           and st.process$status != 'E';
      else
         update svc_rec_group_labels st
            set process$status ='E'
          where rowid = rec.stgreclabel_rid;
      end if;
        
   END LOOP;
   ---------------------------------------------------------------------------------------
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CFA_REC_GROUP;
--------------------------------------------------------------------------------
/* END RECORD_GROUP*/
-----------------------------------------------------------------------------
--This procedure will clear the staging table data 
-----------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_ext_entity 
         where process_id = I_process_id;
   delete from svc_attrib_group_set_labels 
      where process_id = I_process_id;
   delete from svc_attrib_group_set   
      where process_id = I_process_id;   
   delete from svc_attrib_group   
      where process_id = I_process_id;   
   delete from svc_attrib_group_labels 
      where process_id = I_process_id;
   delete from svc_rec_group   
      where process_id = I_process_id;
   delete from svc_rec_group_labels 
      where process_id = I_process_id;
      commit;
      
END;
-----------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_CFAS_ADMIN_SQL.PROCESS';
   L_process_status    VARCHAR2(2);
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_EXT_ENTITY(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then

      return FALSE;
   end if;
  
   if PROCESS_CFA_ATTRIB_GROUP_SET(O_error_message,
                                   I_process_id,
                                   I_chunk_id)=FALSE then

      return FALSE;
   end if;
  
    if PROCESS_CFA_ATTRIB_GROUP(O_error_message,
                                I_process_id,
                                I_chunk_id)=FALSE then
                                
       return FALSE;
   end if;
  
   if PROCESS_CFA_REC_GROUP(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   
   update svc_process_tracker
          set status = (CASE when status = 'PE' then 'PE'
                             else L_process_status 
                        END),
              action_date = SYSDATE
        where process_id = I_process_id; 
  --- Clear staging tables for this process_id
  CLEAR_STAGING_DATA(I_process_id);
           
   commit;
   return TRUE;
EXCEPTION
   when OTHERS then
    CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------
END CORESVC_CFAS_ADMIN_SQL;
/