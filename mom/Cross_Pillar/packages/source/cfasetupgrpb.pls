CREATE OR REPLACE PACKAGE BODY CFA_SETUP_GROUP_SQL AS
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_GROUP_ID(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           O_group_id         IN OUT    CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_exists            VARCHAR2(1)  := 'N';
   L_wrap_group_id     CFA_ATTRIB_GROUP.GROUP_ID%TYPE;
   L_group_id          CFA_ATTRIB_GROUP.GROUP_ID%TYPE;

   SEQ_MAXVAL     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(SEQ_MAXVAL,-08004);

   cursor C_NEXTVAL is
      select cfa_attrib_group_seq.NEXTVAL
        from dual;

   cursor C_SEQ_EXISTS is
      select 'x'
        from cfa_attrib_group ag
       where ag.group_id = L_group_id;

BEGIN
   ---
   open  C_NEXTVAL;
   fetch C_NEXTVAL into L_group_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_group_id := L_group_id;
   ---
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         O_group_id := L_group_id;
         return TRUE;

      end if;
      ---
      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_group_id;
      close C_NEXTVAL;
      ---
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_group_id = L_wrap_group_id then
         O_error_message := SQL_LIB.CREATE_MSG('GROUP_ID_EXIST',
                                               L_group_id,
                                               NULL,
                                               NULL);
         return FALSE;

      end if;
      ---
   END LOOP;
   ---
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('GROUP_ID_EXIST',
                                            L_group_id,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_NEXTVAL%ISOPEN then
         close C_NEXTVAL;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.GET_NEXT_GROUP_ID',
                                            to_char(SQLCODE));
      return FALSE;

END GET_NEXT_GROUP_ID;
---------------------------------------------------------------------------------------------------
FUNCTION GRP_DISP_ORD_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   VARCHAR2,
                            I_group_set_id    IN       CFA_ATTRIB_GROUP.GROUP_SET_ID%TYPE,
                            I_group_id        IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                            I_disp_seq        IN       CFA_ATTRIB_GROUP.DISPLAY_SEQ%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_DISP_ORD is
      select 'x'
        from cfa_attrib_group
       where group_set_id = I_group_set_id
         and display_seq   = I_disp_seq
         and group_id     != I_group_id;

BEGIN
   ---
   open C_DISP_ORD;
   fetch C_DISP_ORD into L_exist;
   close C_DISP_ORD;
   ---
   if L_exist is NOT NULL then
      O_exist := 'Y';
   else
      O_exist := 'N';
   end if;  
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.GRP_DISP_ORD_EXIST',
                                            to_char(SQLCODE));
      return FALSE;

END GRP_DISP_ORD_EXIST;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_GRP_LABEL_LANG(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_group_id       IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                                I_lang           IN       CFA_ATTRIB_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   L_exists    VARCHAR2(1) := NULL;

   cursor C_GET_LANG is
      select 'x'
        from cfa_attrib_group_labels
       where group_id = I_group_id
         and lang     = I_lang;

BEGIN
   ---
   open C_GET_LANG;
   fetch C_GET_LANG into L_exists;
   close C_GET_LANG;
   ---
   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_LANG_SELECT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;  
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.CHK_DUP_GRP_LABEL_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_DUP_GRP_LABEL_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION GROUP_VIEW_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist            IN OUT   VARCHAR2,
                           I_grp_view_name    IN       CFA_ATTRIB_GROUP.GROUP_VIEW_NAME%TYPE)
RETURN BOOLEAN IS

   L_exist   VARCHAR2(1);

   cursor C_GRP_VIEW is
      select 'x'
        from cfa_attrib_group
       where group_view_name = I_grp_view_name
      UNION ALL
      select 'x'
        from cfa_attrib_group_set
       where group_set_view_name = I_grp_view_name
          or staging_table_name  = I_grp_view_name
      UNION ALL
      select 'x'
        from all_objects obj,
             system_options so
       where obj.owner       = so.table_owner
         and obj.object_name = I_grp_view_name
         and rownum          = 1;

BEGIN
   ---
   open C_GRP_VIEW;
   fetch C_GRP_VIEW into L_exist;
   close C_GRP_VIEW;
   ---
   if L_exist is NOT NULL then
      O_exist := 'Y';
   else
      O_exist := 'N';
   end if;  
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.GROUP_VIEW_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END GROUP_VIEW_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ATTRIB_GROUP_LABEL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_group_id         IN       CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   record_locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(record_locked, -54);

   L_exists       VARCHAR2(1);

   TYPE TYP_rowid is TABLE of ROWID INDEX BY BINARY_INTEGER;
   TBL_rowid TYP_rowid;

   cursor C_LOCK_GROUP_LABELS is
      select rowid
        from cfa_attrib_group_labels
       where group_id = I_group_id
         for update nowait;

   cursor C_CHK_ATTRIB_EXIST is
      select 'x'
        from cfa_attrib
       where group_id = I_group_id
         for update nowait;

BEGIN
   ---
   open C_CHK_ATTRIB_EXIST;
   fetch C_CHK_ATTRIB_EXIST into L_exists;
   close C_CHK_ATTRIB_EXIST;
   ---
   if L_exists is NOT NULL then      
      O_error_message := SQL_LIB.CREATE_MSG('ATTRIB_EXISTS',
                                            I_group_id,
                                            NULL,
                                            NULL);
      return FALSE;
      
   end if;   
   ---
   open C_LOCK_GROUP_LABELS;
   fetch C_LOCK_GROUP_LABELS BULK COLLECT into TBL_rowid;
   close C_LOCK_GROUP_LABELS;
   ---
   FORALL i in TBL_rowid.FIRST..TBL_rowid.LAST
      delete
        from cfa_attrib_group_labels
       where rowid = TBL_rowid(i);
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'CFA_ATTRIB_GROUP_LABELS',
                                            I_group_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'CFA_SETUP_GROUP_SQL.DELETE_ATTRIB_GROUP_LABEL',
                                             to_char(SQLCODE));
      return FALSE;

END DELETE_ATTRIB_GROUP_LABEL;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_NO_GROUP_LABELS(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist            IN OUT    VARCHAR2,
                             IO_group_id        IN OUT    CFA_ATTRIB_GROUP_LABELS.GROUP_ID%TYPE,
                             I_lang             IN        CFA_ATTRIB_GROUP_LABELS.LANG%TYPE)
RETURN BOOLEAN IS

   L_no_labels_group   CFA_ATTRIB_GROUP_LABELS.GROUP_ID%TYPE;

   cursor C_GROUP_LABEL_EXIST is
      select group_id 
        from cfa_attrib_group g
       where g.group_id = NVL(IO_group_id, g.group_id)
         and not exists (select 'x'
                           from cfa_attrib_group_labels d
                          where d.group_id = g.group_id
                            and lang = NVL(I_lang, d.lang))
         and rownum = 1;

BEGIN
   ---
   open C_GROUP_LABEL_EXIST;
   fetch C_GROUP_LABEL_EXIST into L_no_labels_group;
   ---
   if C_GROUP_LABEL_EXIST%NOTFOUND then
      O_exist := 'N';
   else
      O_exist := 'Y';
      if IO_group_id is NULL then
         IO_group_id := L_no_labels_group;
      end if;
   end if;
   ---
   close C_GROUP_LABEL_EXIST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.CHK_NO_GROUP_LABELS',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_NO_GROUP_LABELS;
-------------------------------------------------------------------------------------------------
FUNCTION CHK_GROUP_DEFAULT_LANG(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_group_id     IN     CFA_ATTRIB_GROUP.GROUP_ID%TYPE)                              
RETURN BOOLEAN IS
   
    L_exists   VARCHAR2(1) :=NULL;
   
   cursor C_LANG_IND is
     select 'x'
        from cfa_attrib_group_labels 
       where group_id = I_group_id
         and default_lang_ind ='Y';

BEGIN
  ---
   open C_LANG_IND;
   fetch C_LANG_IND into L_exists;
   close C_LANG_IND;
   ---
   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_LANGUAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.CHK_GROUP_DEFAULT_LANG',
                                            to_char(SQLCODE));
      return FALSE;

END CHK_GROUP_DEFAULT_LANG;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_GROUP_INFO(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                               O_group_set_label     IN OUT    CFA_ATTRIB_GROUP_SET_LABELS.LABEL%TYPE,
                               O_base_rms_table      IN OUT    CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                               O_custom_ext_table    IN OUT    CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE,
                               O_group_label         IN OUT    CFA_ATTRIB_GROUP_LABELS.LABEL%TYPE,
                               IO_group_set_id       IN OUT    CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                               IO_ext_entity_id      IN OUT    CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE,
                               I_group_id            IN        CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_ENTITY_GRP_INFO is
      select ag.group_set_id,
             gsl.label group_set_label,
             ext.ext_entity_id,
             ext.base_rms_table,
             ext.custom_ext_table,
             gd.label
        from cfa_attrib_group ag,
             cfa_attrib_group_labels gd,
             cfa_ext_entity ext,
             cfa_attrib_group_set gs,
             cfa_attrib_group_set_labels gsl
       where ag.group_id = gd.group_id
         and ag.group_id = I_group_id
         and ag.group_set_id = gs.group_set_id
         and gs.ext_entity_id = ext.ext_entity_id
         and ag.group_set_id = NVL(IO_group_set_id, ag.group_set_id)
         and gs.ext_entity_id = NVL(IO_ext_entity_id, gs.ext_entity_id)
         and (gd.lang = GET_USER_LANG or
              gd.default_lang_ind  = 'Y'
              and not exists (select 'x'
                                from cfa_attrib_group_labels gd2
                               where gd.group_id = gd2.group_id
                                 and gd.lang = GET_USER_LANG))
         and ag.group_set_id = gsl.group_set_id
         and (gsl.lang = GET_USER_LANG or
              (gsl.default_lang_ind  = 'Y'
               and not exists (select 'x'
                                from cfa_attrib_group_set_labels gsl2
                               where gsl.group_set_id = gsl2.group_set_id
                                 and gsl.lang = GET_USER_LANG)));

BEGIN
   ---
   open C_GET_ENTITY_GRP_INFO;
   fetch C_GET_ENTITY_GRP_INFO into IO_group_set_id,
                                    O_group_set_label,
                                    IO_ext_entity_id,
                                    O_base_rms_table,
                                    O_custom_ext_table,
                                    O_group_label;
   ---
   if C_GET_ENTITY_GRP_INFO%NOTFOUND then
      close C_GET_ENTITY_GRP_INFO;
      ---
      if IO_group_set_id is NULL and IO_ext_entity_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CFA_GROUP',
                                               NULL,
                                               NULL,
                                               NULL);
          return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_CFA_GROUP_FOR_SET',
                                               NULL,
                                               NULL,
                                               NULL);
          return FALSE;
      end if;
      ---
   end if;
   ---
   close C_GET_ENTITY_GRP_INFO;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.GET_ATTRIB_GROUP_INFO',
                                            to_char(SQLCODE));
      return FALSE;

END GET_ATTRIB_GROUP_INFO;
---------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_GROUP_REC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_group_rec      IN OUT  CFA_ATTRIB_GROUP%ROWTYPE,
                              I_group_id       IN      CFA_ATTRIB_GROUP.GROUP_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_GRP_REC is
      select *
        from cfa_attrib_group
       where group_id = I_group_id;

BEGIN
   
   open C_GET_GRP_REC;
   fetch C_GET_GRP_REC into O_group_rec;
   close C_GET_GRP_REC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CFA_SETUP_GROUP_SQL.GGET_ATTRIB_GROUP_REC',
                                            to_char(SQLCODE));
      return FALSE;

END GET_ATTRIB_GROUP_REC;
---------------------------------------------------------------------------------------------------
END CFA_SETUP_GROUP_SQL;
/
 
