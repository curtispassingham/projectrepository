CREATE OR REPLACE PACKAGE BODY L10N_FLEX_ATTRIB_SQL AS
------------------------------------------------------------------------------------------
--Public
------------------------------------------------------------------------------------------
PROCEDURE QUERY_GROUP(IO_group_tbl     IN OUT  NOCOPY  TYP_l10n_attrib_group_tbl,
                      I_base_table     IN              EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                      I_country_id     IN              L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE) AS

   L_program   VARCHAR2(61) :=  'L10N_FLEX_ATTRIB_SQL.QUERY_GROUP';
   L_cnt       NUMBER := 0;

   cursor C_GET_ATTRIB_GROUP is
      select grp.group_id,
             gd.description group_desc
        from l10n_attrib_group grp,
             l10n_attrib_group_descs gd,
             ext_entity ext
       where grp.ext_entity_id = ext.ext_entity_id
         and grp.group_id = gd.group_id
         and gd.lang = LP_lang
         and ext.base_rms_table = I_base_table
         and grp.country_id = I_country_id
       order by grp.display_order;

BEGIN
   ---
   FOR rec in C_GET_ATTRIB_GROUP LOOP
       L_cnt := L_cnt + 1;
       IO_group_tbl(L_cnt).group_id := rec.group_id;
       IO_group_tbl(L_cnt).group_desc := rec.group_desc;
   END LOOP;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_group_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                          SQLERRM,
                                                          L_program,
                                                          to_char(SQLCODE));
      IO_group_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_group_tbl(1).error_message);
      
END QUERY_GROUP;
------------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB(IO_attrib_tbl     IN OUT  NOCOPY  TYP_l10n_attrib_tbl,
                       I_group_id        IN              L10N_ATTRIB_GROUP.GROUP_ID%TYPE) AS

   L_program   VARCHAR2(62) :=  'L10N_FLEX_ATTRIB_SQL.QUERY_ATTRIB';

   TYPE CUR_attrib       IS REF CURSOR;
   C_get_ext_attrib      CUR_attrib;

   L_attrib_rec          TYP_l10n_attrib_rec;

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_query               VARCHAR2(2000);
   L_select              VARCHAR2(400);
   L_from                VARCHAR2(100);
   L_where               VARCHAR2(1500);

BEGIN
   ---
   L_select := 'select ext.group_id,'||
                     ' ext.varchar2_1,'||
                     ' ext.varchar2_2,'||
                     ' ext.varchar2_3,'||
                     ' ext.varchar2_4,'||
                     ' ext.varchar2_5,'||
                     ' ext.varchar2_6,'||
                     ' ext.varchar2_7,'||
                     ' ext.varchar2_8,'||
                     ' ext.varchar2_9,'||
                     ' ext.varchar2_10,'||
                     ' ext.number_11,'||
                     ' ext.number_12,'||
                     ' ext.number_13,'||
                     ' ext.number_14,'||
                     ' ext.number_15,'||
                     ' ext.number_16,'||
                     ' ext.number_17,'||
                     ' ext.number_18,'||
                     ' ext.number_19,'||
                     ' ext.number_20,'||
                     ' ext.date_21,'||
                     ' ext.date_22,'||
                     ' NULL,'||
                     ' ''TRUE''';

   L_where := ' where ext.group_id = ' ||I_group_id;   

   if LP_header_cfg_tbl is NOT NULL and LP_header_cfg_tbl.count > 0 then
      L_from := ' from ' || LP_header_cfg_tbl(1).l10n_ext_table ||' ext';
      ---
      FOR i in LP_header_cfg_tbl.FIRST..LP_header_cfg_tbl.LAST LOOP
         if LP_header_cfg_tbl(i).header_type = LP_key_widget then
            L_where := L_where || ' and ' || LP_header_cfg_tbl(i).key_col ||' = '||LP_header_cfg_tbl(i).key_value_qry;
         end if;
      END LOOP;
   end if;

   L_query := L_select || L_from || L_where;

   open  C_get_ext_attrib for L_query;
   LOOP
      fetch C_get_ext_attrib into L_attrib_rec;
      exit when C_get_ext_attrib%NOTFOUND;
      IO_attrib_tbl(1) := L_attrib_rec;
   END LOOP;

   close C_get_ext_attrib;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_attrib_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
      IO_attrib_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_tbl(1).error_message);
      
END QUERY_ATTRIB;
------------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB(IO_attrib_tbl   IN OUT  NOCOPY TYP_l10n_attrib_tbl,
                       I_group_id      IN             L10N_ATTRIB_GROUP.GROUP_ID%TYPE,
                       I_country_id    IN             L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE) AS

   L_program            VARCHAR2(61) :=  'L10N_FLEX_ATTRIB_SQL.MERGE_ATTRIB';
   L_merge              VARCHAR2(10000);
   L_insert             VARCHAR2(1000);
   L_values             VARCHAR2(1000);
   L_using              VARCHAR2(3000);
   L_on                 VARCHAR2(1000);
   L_when_matched       VARCHAR2(3000);
   L_when_not_matched   VARCHAR2(3000);

BEGIN
   ---
   L_merge := 'merge into '||LP_header_cfg_tbl(1).l10n_ext_table || ' ext ';
 
   L_using := 'using (select '||I_group_id ||' group_id, '||
                             case when IO_attrib_tbl(1).varchar2_1  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_1  ||'''' end||' varchar2_1, ' ||
                             case when IO_attrib_tbl(1).varchar2_2  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_2  ||'''' end||' varchar2_2, ' ||
                             case when IO_attrib_tbl(1).varchar2_3  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_3  ||'''' end||' varchar2_3, ' ||
                             case when IO_attrib_tbl(1).varchar2_4  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_4  ||'''' end||' varchar2_4, ' ||
                             case when IO_attrib_tbl(1).varchar2_5  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_5  ||'''' end||' varchar2_5, ' ||
                             case when IO_attrib_tbl(1).varchar2_6  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_6  ||'''' end||' varchar2_6, ' ||
                             case when IO_attrib_tbl(1).varchar2_7  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_7  ||'''' end||' varchar2_7, ' ||
                             case when IO_attrib_tbl(1).varchar2_8  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_8  ||'''' end||' varchar2_8, ' ||
                             case when IO_attrib_tbl(1).varchar2_9  is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_9  ||'''' end||' varchar2_9, ' ||
                             case when IO_attrib_tbl(1).varchar2_10 is NULL then 'NULL' else ''''||IO_attrib_tbl(1).varchar2_10 ||'''' end||' varchar2_10, '||
                             case when IO_attrib_tbl(1).number_11   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_11   ||'''' end||' number_11, '  ||
                             case when IO_attrib_tbl(1).number_12   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_12   ||'''' end||' number_12, '  ||
                             case when IO_attrib_tbl(1).number_13   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_13   ||'''' end||' number_13, '  ||
                             case when IO_attrib_tbl(1).number_14   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_14   ||'''' end||' number_14, '  ||
                             case when IO_attrib_tbl(1).number_15   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_15   ||'''' end||' number_15, '  ||
                             case when IO_attrib_tbl(1).number_16   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_16   ||'''' end||' number_16, '  ||
                             case when IO_attrib_tbl(1).number_17   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_17   ||'''' end||' number_17, '  ||
                             case when IO_attrib_tbl(1).number_18   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_18   ||'''' end||' number_18, '  ||
                             case when IO_attrib_tbl(1).number_19   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_19   ||'''' end||' number_19, '  ||
                             case when IO_attrib_tbl(1).number_20   is NULL then 'NULL' else ''''||IO_attrib_tbl(1).number_20   ||'''' end||' number_20, '  ||
                             case when IO_attrib_tbl(1).date_21     is NULL then 'NULL' else ''''||IO_attrib_tbl(1).date_21     ||'''' end||' date_21, '    ||
                             case when IO_attrib_tbl(1).date_22     is NULL then 'NULL' else ''''||IO_attrib_tbl(1).date_22     ||'''' end||' date_22 '     ||
                       'from dual) tmp ';

   L_on := 'on (';

   FOR i in LP_header_cfg_tbl.FIRST..LP_header_cfg_tbl.LAST LOOP
      if LP_header_cfg_tbl(i).header_type = LP_key_widget then
         L_on := L_on ||LP_header_cfg_tbl(i).key_col||' = '||LP_header_cfg_tbl(i).key_value_qry || ' and ';
         ---
         L_insert := L_insert || LP_header_cfg_tbl(i).key_col || ', ';
         L_values := L_values || LP_header_cfg_tbl(i).key_value_qry || ', ';
      end if;
   END LOOP;

   if LP_l10n_depend_exist = 'N' then
      L_insert := L_insert || 'l10n_country_id, ';
      L_values := L_values || ''''||I_country_id||''',';
   end if;
   
   L_on := L_on ||'ext.group_id = tmp.group_id) ';

   L_when_matched := 'when matched then '||
                         'update '||
                             'set '||'ext.varchar2_1 = tmp.varchar2_1, ' ||
                                     'ext.varchar2_2 = tmp.varchar2_2, ' ||
                                     'ext.varchar2_3 = tmp.varchar2_3, ' ||
                                     'ext.varchar2_4 = tmp.varchar2_4, ' ||
                                     'ext.varchar2_5 = tmp.varchar2_5, ' ||
                                     'ext.varchar2_6 = tmp.varchar2_6, ' ||
                                     'ext.varchar2_7 = tmp.varchar2_7, ' ||
                                     'ext.varchar2_8 = tmp.varchar2_8, ' ||
                                     'ext.varchar2_9 = tmp.varchar2_9, ' ||
                                     'ext.varchar2_10= tmp.varchar2_10, '||
                                     'ext.number_11  = tmp.number_11, '  ||
                                     'ext.number_12  = tmp.number_12, '  ||
                                     'ext.number_13  = tmp.number_13, '  ||
                                     'ext.number_14  = tmp.number_14, '  ||
                                     'ext.number_15  = tmp.number_15, '  ||
                                     'ext.number_16  = tmp.number_16, '  ||
                                     'ext.number_17  = tmp.number_17, '  ||
                                     'ext.number_18  = tmp.number_18, '  ||
                                     'ext.number_19  = tmp.number_19, '  ||
                                     'ext.number_20  = tmp.number_20, '  ||
                                     'ext.date_21    = tmp.date_21, '    ||
                                     'ext.date_22    = tmp.date_22 ';

   L_when_not_matched := 'when not matched then '||
                            'insert ('||L_insert||
                                     'group_id, '||
                                     'varchar2_1, '||
                                     'varchar2_2, '||
                                     'varchar2_3, '||
                                     'varchar2_4, '||
                                     'varchar2_5, '||
                                     'varchar2_6, '||
                                     'varchar2_7, '||
                                     'varchar2_8, '||
                                     'varchar2_9, '||
                                     'varchar2_10,'||
                                     'number_11, ' ||
                                     'number_12, ' ||
                                     'number_13, ' ||
                                     'number_14, ' ||
                                     'number_15, ' ||
                                     'number_16, ' ||
                                     'number_17, ' ||
                                     'number_18, ' ||
                                     'number_19, ' ||
                                     'number_20, ' ||
                                     'date_21, '   ||
                                     'date_22)'    ||
                             'values('||L_values||
                                     I_group_id||', '||
                                     'tmp.varchar2_1, '||
                                     'tmp.varchar2_2, '||
                                     'tmp.varchar2_3, '||
                                     'tmp.varchar2_4, '||
                                     'tmp.varchar2_5, '||
                                     'tmp.varchar2_6, '||
                                     'tmp.varchar2_7, '||
                                     'tmp.varchar2_8, '||
                                     'tmp.varchar2_9, '||
                                     'tmp.varchar2_10,'||
                                     'tmp.number_11, ' ||
                                     'tmp.number_12, ' ||
                                     'tmp.number_13, ' ||
                                     'tmp.number_14, ' ||
                                     'tmp.number_15, ' ||
                                     'tmp.number_16, ' ||
                                     'tmp.number_17, ' ||
                                     'tmp.number_18, ' ||
                                     'tmp.number_19, ' ||
                                     'tmp.number_20, ' ||
                                     'tmp.date_21, '   ||
                                     'tmp.date_22)';

   L_merge := L_merge || L_using || L_on || L_when_matched || L_when_not_matched;
   ---
   EXECUTE IMMEDIATE L_merge;
   ---
   return;
   ---
EXCEPTION
   when OTHERS then
      IO_attrib_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
      IO_attrib_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_tbl(1).error_message);
      
END MERGE_ATTRIB;
------------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB(IO_attrib_tbl     IN OUT  NOCOPY  TYP_l10n_attrib_tbl,
                      I_group_id        IN              L10N_ATTRIB_GROUP.GROUP_ID%TYPE) AS

   L_program   VARCHAR2(61) :=  'L10N_FLEX_ATTRIB_SQL.UPDATE_ATTRIB';

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   TYPE CUR_attrib    IS REF CURSOR;
   C_LOCK_ATTRIB      CUR_attrib;

   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_query            VARCHAR2(2000);
   L_select           VARCHAR2(400);
   L_from             VARCHAR2(100);
   L_where            VARCHAR2(1500);

BEGIN
   ---
   L_select := 'select ''x''';

   L_where := ' where ext.group_id = ' ||I_group_id;

   if LP_header_cfg_tbl is NOT NULL and LP_header_cfg_tbl.count > 0 then
      L_from := ' from ' || LP_header_cfg_tbl(1).l10n_ext_table ||' ext';
      ---
      FOR i in LP_header_cfg_tbl.FIRST..LP_header_cfg_tbl.LAST LOOP
         if LP_header_cfg_tbl(i).header_type = LP_key_widget then
            L_where := L_where || ' and ' || LP_header_cfg_tbl(i).key_col ||' = '||LP_header_cfg_tbl(i).key_value_qry;
         end if;
      END LOOP;
   end if;

   L_query := L_select || L_from || L_where || ' for update nowait';
   ---
   open  C_LOCK_ATTRIB for L_query;
   close C_LOCK_ATTRIB;
   ---
   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_attrib_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                           LP_header_cfg_tbl(1).l10n_ext_table,
                                                           I_group_id);
      IO_attrib_tbl(1).return_code := 'FALSE';
      raise_application_error(-20002, IO_attrib_tbl(1).error_message);

   when OTHERS then
      IO_attrib_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
      IO_attrib_tbl(1).return_code := 'FALSE';
      raise_application_error(ERRNUM_PACKAGE_CALL, IO_attrib_tbl(1).error_message);
      
END LOCK_ATTRIB;
------------------------------------------------------------------------------------------
FUNCTION GET_KEY_DESC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_key_desc          IN OUT  VARCHAR2,
                      I_key_col           IN      EXT_ENTITY_KEY.KEY_COL%TYPE,
                      I_key_value         IN      VARCHAR2,
                      I_key_type          IN      VARCHAR2,
                      I_desc_plsql        IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program      VARCHAR2(62)   := 'L10N_FLEX_ATTRIB_SQL.GET_KEY_DESC';
   L_statement    VARCHAR2(2000);
   L_return       VARCHAR2(6) := 'TRUE';
   
BEGIN
   ---
   L_statement := 'BEGIN if NOT '||I_desc_plsql||'(:O_error_message,:O_desc, :I_key_value, :I_type) then :L_answer := ''FALSE''; end if; END;';

   EXECUTE IMMEDIATE L_statement USING IN OUT O_error_message,
                                       IN OUT O_key_desc,
                                       IN     I_key_value,
                                       IN     I_key_type,
                                       IN OUT L_return;
   ---
   if L_return = 'FALSE' then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END GET_KEY_DESC;
------------------------------------------------------------------------------------------
FUNCTION PREFETCH_DATA (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_base_rms_table   IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        I_country_id       IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(62)   := 'L10N_FLEX_ATTRIB_SQL.PREFETCH_DATA';
   L_statement         VARCHAR2(2000);
   L_query             VARCHAR2(3000);
   L_where             VARCHAR2(1000);
   L_index             VARCHAR2(25);
   L_value             VARCHAR2(250);
   L_return            VARCHAR2(6) := 'TRUE';
   TYPE CUR_prequery   IS REF CURSOR;
   C_GET_ATTRIB_VAL    CUR_prequery;
   L_ext_table         EXT_ENTITY.L10N_EXT_TABLE%TYPE := NULL;
   L_group_id          L10N_ATTRIB.GROUP_ID%TYPE;
   L_attrib_col        L10N_ATTRIB.ATTRIB_STORAGE_COL%TYPE;
BEGIN
   ---
   if LP_header_cfg_tbl is NOT NULL and
      LP_header_cfg_tbl.count > 0 then
      L_ext_table := LP_header_cfg_tbl(1).l10n_ext_table;
      ---
      FOR i in LP_header_cfg_tbl.FIRST..LP_header_cfg_tbl.LAST LOOP
         if LP_header_cfg_tbl(i).header_type = LP_key_widget then
            L_where   := L_where || ' and ' || LP_header_cfg_tbl(i).key_col ||' = '||LP_header_cfg_tbl(i).key_value_qry;
         end if;
      END LOOP;
      ---
      L_where := substr(L_where, 5);
      L_query := 'select att.group_id, '||
                        'att.attrib_storage_col, '||
                        'piv.attrib_value '||
                   'from l10n_attrib att, '||
                        'l10n_attrib_group grp, '||
                        'ext_entity ee, '||
                        '(select group_id, '||
                                'attrib_col, '||
                                'attrib_value '||
                           'from (select group_id, '||
                                        'varchar2_1, '||
                                        'varchar2_2, '||
                                        'varchar2_3, '||
                                        'varchar2_4, '||
                                        'varchar2_5, '||
                                        'varchar2_6, '||
                                        'varchar2_7, '||
                                        'varchar2_8, '||
                                        'varchar2_9, '||
                                        'varchar2_10, '||
                                        'to_char(number_11) number_11, '||
                                        'to_char(number_12) number_12, '||
                                        'to_char(number_13) number_13, '||
                                        'to_char(number_14) number_14, '||
                                        'to_char(number_15) number_15, '||
                                        'to_char(number_16) number_16, '||
                                        'to_char(number_17) number_17, '||
                                        'to_char(number_18) number_18, '||
                                        'to_char(number_19) number_19, '||
                                        'to_char(number_20) number_20, '||
                                        'to_char(date_21, ''YYYYMMDD'') date_21, '||
                                        'to_char(date_22, ''YYYYMMDD'') date_22 '||
                                   'from '|| L_ext_table ||
                                 ' where '|| L_where ||') ext '||
                                'unpivot  '||
                                   '(attrib_value for attrib_col in (varchar2_1  as ''VARCHAR2_1'', '||
                                                                    'varchar2_2  as ''VARCHAR2_2'', '||
                                                                    'varchar2_3  as ''VARCHAR2_3'', '||
                                                                    'varchar2_4  as ''VARCHAR2_4'', '||
                                                                    'varchar2_5  as ''VARCHAR2_5'', '||
                                                                    'varchar2_6  as ''VARCHAR2_6'', '||
                                                                    'varchar2_7  as ''VARCHAR2_7'', '||
                                                                    'varchar2_8  as ''VARCHAR2_8'', '||
                                                                    'varchar2_9  as ''VARCHAR2_9'', '||
                                                                    'varchar2_10 as ''VARCHAR2_10'', '||
                                                                    'number_11   as ''NUMBER_11'', '||
                                                                    'number_12   as ''NUMBER_12'', '||
                                                                    'number_13   as ''NUMBER_13'', '||
                                                                    'number_14   as ''NUMBER_14'', '||
                                                                    'number_15   as ''NUMBER_15'', '||
                                                                    'number_16   as ''NUMBER_16'', '||
                                                                    'number_17   as ''NUMBER_17'', '||
                                                                    'number_18   as ''NUMBER_18'', '||
                                                                    'number_19   as ''NUMBER_19'', '||
                                                                    'number_20   as ''NUMBER_20'', '||
                                                                    'date_21     as ''DATE_21'', '||
                                                                    'date_22     as ''DATE_22''))) piv '||
                  'where grp.group_id = att.group_id '||
                    'and grp.country_id = ''' || I_country_id ||''' '||
                    'and ee.ext_entity_id = grp.ext_entity_id '||
                    'and ee.base_rms_table = ''' || I_base_rms_table||''' '||
                    'and piv.group_id (+) = att.group_id '||
                    'and piv.attrib_col (+) = att.attrib_storage_col';
                    
      open  C_GET_ATTRIB_VAL for L_query;
      LOOP
        fetch C_GET_ATTRIB_VAL into L_group_id,
                                    L_attrib_col,
                                    L_value;
        ---
        EXIT WHEN C_GET_ATTRIB_VAL%NOTFOUND;
        ---
        L_index := L_group_id||':'||L_attrib_col;
        LP_attrib_cfg_tbl(L_index).attrib_value := L_value;        
      END LOOP;
      close C_GET_ATTRIB_VAL;
   end if;
      ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PREFETCH_DATA;
------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_DATA(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_attrib_cfg_tbl     IN OUT   TYP_L10N_ATTRIB_CFG_TBL,
                         I_base_rms_table      IN       EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                         I_country_id          IN       L10N_ATTRIB_GROUP.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(62)   := 'L10N_FLEX_ATTRIB_SQL.GET_ATTRIB_DATA';

BEGIN

   if NOT PREFETCH_DATA(O_error_message,
                        I_base_rms_table,
                        I_country_id) then
      return FALSE;
   end if;
   
   IO_attrib_cfg_tbl := LP_attrib_cfg_tbl;
   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ATTRIB_DATA;
------------------------------------------------------------------------------------------
FUNCTION ADD_STORE_ATTRIB(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_store               IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS
 
   L_program             VARCHAR2(62)   := 'L10N_FLEX_ATTRIB_SQL.ADD_STORE_ATTRIB';
      
   cursor c_store_map is 
   select st.group_id st_grp_id,
       stadd.group_id stadd_grp_id
  from (select lag.group_id,   
               max(la.view_col_name) view_col_name,
               ee.ext_entity_id   ext_entity_id  
          from ext_entity ee,
               l10n_attrib_group lag,
               l10n_Attrib la 
         where ee.base_rms_table = 'STORE'
           and ee.ext_entity_id = lag.ext_entity_id
           and la.group_id = lag.group_id     
               group by lag.group_id, ee.ext_entity_id) st,
        (select lag.group_id,
               max(la.view_col_name) view_col_name,
               ee.ext_entity_id   ext_entity_id  
          from ext_entity ee,
               l10n_attrib_group lag,
               l10n_Attrib la 
         where ee.base_rms_table = 'STORE_ADD'
           and ee.ext_entity_id = lag.ext_entity_id
           and la.group_id = lag.group_id     
               group by lag.group_id, ee.ext_entity_id) stadd
  where stadd.view_col_name = st.view_col_name;  
  
BEGIN
   
   if LP_TYP_store_map_tbl.count = 0 then 
      open c_store_map;
      fetch c_store_map bulk collect into  LP_TYP_store_map_tbl;
      close c_store_map;
   end if ; 
   
   if LP_TYP_store_map_tbl.count > 0 then
      for rec in LP_TYP_store_map_tbl.first..LP_TYP_store_map_tbl.last loop 
         INSERT INTO store_l10n_ext(store,
                                    l10n_country_id,
                                    group_id,
                                    varchar2_1,
                                    varchar2_2,
                                    varchar2_3,
                                    varchar2_4,
                                    varchar2_5,
                                    varchar2_6,
                                    varchar2_7,
                                    varchar2_8,
                                    varchar2_9,
                                    varchar2_10,
                                    number_11,
                                    number_12,
                                    number_13,
                                    number_14,
                                    number_15,
                                    number_16,
                                    number_17,
                                    number_18,
                                    number_19,
                                    number_20,
                                    date_21,
                                    date_22)
                            (SELECT store,
                                    l10n_country_id,
                                    LP_TYP_store_map_tbl(rec).st_group_id,
                                    varchar2_1,
                                    varchar2_2,
                                    varchar2_3,
                                    varchar2_4,
                                    varchar2_5,
                                    varchar2_6,
                                    varchar2_7,
                                    varchar2_8,
                                    varchar2_9,
                                    varchar2_10,
                                    number_11,
                                    number_12,
                                    number_13,
                                    number_14,
                                    number_15,
                                    number_16,
                                    number_17,
                                    number_18,
                                    number_19,
                                    number_20,
                                    date_21,
                                    date_22
                               FROM store_add_l10n_ext 
                              WHERE store = I_store
                                and group_id = LP_TYP_store_map_tbl(rec).stadd_group_id); 
      end loop;
   end if;
   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_STORE_ATTRIB;
------------------------------------------------------------------------------------------
END L10N_FLEX_ATTRIB_SQL;
/