-- File Name : CORESVC_EXT_ENTITY_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_CFAS_ADMIN_SQL AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(255)         :='CFAS_ATTRIBUTE_DATA';
   template_category               CODE_DETAIL.CODE%TYPE := 'RMSADM';
   sheet_name_trans                S9T_PKG.trans_map_typ;
   action_column    	           VARCHAR2(255)         :='ACTION';
   action_new            	   VARCHAR2(25)          := 'NEW';
   action_mod            	   VARCHAR2(25)          := 'MOD';
   action_del            	   VARCHAR2(25)          := 'DEL';
     
   EXT_ENTITY_sheet                VARCHAR2(255)         := 'CFA_EXT_ENTITY';
   EXT_ENTITY$ACTION               NUMBER                :=1;
   EXT_ENTITY$BASE_RMS_TABLE       NUMBER                :=2;
   EXT_ENTITY$VALIDATION_FUNC      NUMBER                :=3;
   
   ------------------------------------------------------------------------
   GROUP_SET_LABELS_sheet          VARCHAR2(255)         := 'CFA_ATTRIB_GROUP_SET_LABELS';
   GROUP_SET_LABELS$ACTION         NUMBER                :=1;
   GROUP_SET_LABELS$GROUP_SET_ID   NUMBER                :=2;
   GROUP_SET_LABELS$LANG           NUMBER                :=3;
   GROUP_SET_LABELS$LABEL          NUMBER                :=4;
   --------------------------------------------------------------------------
   GROUP_SET_sheet                  VARCHAR2(255)         := 'CFA_ATTRIB_GROUP_SET';
   GROUP_SET$ACTION          	    NUMBER                :=1;
   GROUP_SET$GROUP_SET_ID           NUMBER                :=2;
   GROUP_SET$BASE_RMS_TABLE         NUMBER                :=3;
   GROUP_SET$DISPLAY_SEQ            NUMBER                :=4;
   GROUP_SET$GROUP_SET_VIEW_NAME    NUMBER                :=5;
   GROUP_SET$QUALIFIER_FUNC         NUMBER                :=6;
   GROUP_SET$DEFAULT_FUNC           NUMBER                :=7;
   GROUP_SET$VALIDATION_FUNC        NUMBER                :=8;
   GROUP_SET$STAGING_TABLE_NAME     NUMBER                :=9;
   -----------------------------------------------------------------------------   
   ATTRIB_GROUP_LABELS_sheet        VARCHAR2(255)         := 'CFA_ATTRIB_GROUP_LABELS';
   ATTRIB_GROUP_LABELS$ACTION       NUMBER                :=1;
   ATTRIB_GROUP_LABELS$GROUP_ID     NUMBER                :=2;
   ATTRIB_GROUP_LABELS$LANG         NUMBER                :=3;
   ATTRIB_GROUP_LABELS$LABEL        NUMBER                :=4;
   ---------------------------------------------------------------------------   
   ATTRIB_GROUP_sheet               VARCHAR2(255)         := 'CFA_ATTRIB_GROUP';
   ATTRIB_GROUP$ACTION              NUMBER                :=1;
   ATTRIB_GROUP$GROUP_ID            NUMBER                :=2;
   ATTRIB_GROUP$GROUP_SET_ID	    NUMBER                :=3;
   ATTRIB_GROUP$DISPLAY_SEQ         NUMBER                :=4;
   ATTRIB_GROUP$GROUP_VIEW_NAME     NUMBER                :=5;
  -- ATTRIB_GROUP$VALIDATION_FUNC     NUMBER                :=6;
   --------------------------------------------------------------------------
   REC_LABELS_sheet                 VARCHAR2(255)         := 'CFA_REC_GROUP_LABELS';
   REC_LABELS$ACTION                NUMBER                :=1;
   REC_LABELS$REC_GROUP_ID          NUMBER                :=2;
   REC_LABELS$LANG                  NUMBER                :=3;
   REC_LABELS$LOV_TITLE             NUMBER                :=4;
   REC_LABELS$LOV_COL1_HEADER       NUMBER                :=5;
   REC_LABELS$LOV_COL2_HEADER       NUMBER                :=6;
   ------------------------------------------------------------------------------      
   REC_GROUP_sheet                 VARCHAR2(255)          := 'CFA_REC_GROUP';
   REC_GROUP$ACTION                NUMBER                 :=1;
   REC_GROUP$REC_GROUP_ID          NUMBER                 :=2;
   REC_GROUP$REC_GROUP_NAME        NUMBER                 :=3;
   REC_GROUP$QUERY_TYPE            NUMBER                 :=4;
   REC_GROUP$TABLE_NAME            NUMBER                 :=5;
   REC_GROUP$COLUMN_1              NUMBER                 :=6;
   REC_GROUP$COLUMN_2              NUMBER                 :=7;
   REC_GROUP$WHERE_COL_1           NUMBER                 :=8;
   REC_GROUP$WHERE_OPERATOR_1      NUMBER                 :=9;
   REC_GROUP$WHERE_COND_1          NUMBER                 :=10;
   REC_GROUP$WHERE_COL_2           NUMBER                 :=11;
   REC_GROUP$WHERE_OPERATOR_2      NUMBER                 :=12;
   REC_GROUP$WHERE_COND_2          NUMBER                 :=13;
  ------------------------------------------------------------------------------          
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   -----------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
   ------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   ------------------------------------------------------------------------
  /* FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;*/
   ------------------------------------------------------------------------
END CORESVC_CFAS_ADMIN_SQL;
/
