CREATE OR REPLACE PACKAGE BODY SYS_UNIT_OPTIONS_SQL_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION SYS_UOP_TABLES(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_prim_currency      IN       VARCHAR2,
                        I_default_tax_type   IN       VARCHAR2,
                        I_vat_class_ind      IN       VARCHAR2,
                        I_bracket_cost_ind   IN       VARCHAR2,
                        I_table_owner        IN       VARCHAR2,
                        I_base_country_id    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SYS_UNIT_OPTIONS_SQL_SQL.SYS_UOP_TABLES';

BEGIN

   update system_config_options
      set currency_code   = UPPER(I_prim_currency),
          base_country_id = UPPER(I_base_country_id),
          table_owner     = UPPER(I_table_owner);

   update localization_config_options
      set default_vat_region  = DECODE(UPPER(I_default_tax_type), 'SVAT', 1000, NULL),
          class_level_vat_ind = UPPER(I_vat_class_ind);

   update foundation_unit_options
      set bracket_costing_ind = UPPER(I_bracket_cost_ind);

   update financial_unit_options
      set stkldgr_vat_incl_retl_ind = DECODE(UPPER(I_default_tax_type), 'SALES', NULL, 'SVAT', 'Y', 'N');

   update currency_rates
      set exchange_rate = 1
    where currency_code = UPPER(I_prim_currency);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SYS_UOP_TABLES;
--------------------------------------------------------------------------------------------
END SYS_UNIT_OPTIONS_SQL_SQL;
/