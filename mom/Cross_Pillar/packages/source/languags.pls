CREATE OR REPLACE PACKAGE LANGUAGE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Function Name: GET_NAME
-- Purpose  : Get language description
-- Input Values   : I_lang         --an IN parameter
--    : O_lang_desc    --an IN OUT parameter 
-- Created  : 24-Sep-96  Carrie Kavanaugh 
----------------------------------------------------------------
FUNCTION GET_NAME(O_error_message  IN OUT VARCHAR2,
                  I_lang           IN     NUMBER,
                  O_lang_desc      IN OUT VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: GET_PRIMARY_LANGUAGE
-- Purpose  : Retrieve the system's primary language.
-- Called From  : GET_PRIMARY_LANG function
-- Created  : 12-NOV-96 Richard Stockton
----------------------------------------------------------------
FUNCTION GET_PRIMARY_LANGUAGE RETURN NUMBER;
----------------------------------------------------------------
-- Function Name: GET_USER_LANGUAGE
-- Purpose  : Retrieve the user's primary language.
-- Called From  : GET_PRIMARY_LANG function
-- Created  : 12-NOV-96 Richard Stockton
----------------------------------------------------------------
FUNCTION GET_USER_LANGUAGE RETURN NUMBER;
----------------------------------------------------------------
-- Function: GET_CODE_DESC
-- Purpose: Retrieve the decode value of a code for a given code type
-- Created: 15-NOV-96 Richard Stockton
---------------------------------------------------------------------
FUNCTION GET_CODE_DESC(O_error_message IN OUT VARCHAR2,
                       I_code_type     IN     VARCHAR2,
                       I_code          IN     VARCHAR2,
                       O_code_desc     IN OUT VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: GET_CODE_DESC
-- Purpose: Retrieve the decode value of a code for a given code type
--          which can be directly used in select statements
---------------------------------------------------------------------
FUNCTION GET_CODE_DESC( I_code_type       IN       VARCHAR2,
                        I_code            IN       VARCHAR2)
RETURN VARCHAR2;
---------------------------------------------------------------------
-- Function: CODE_TYPE_DESC
-- Purpose: Retrieve the decode value of a code for a given code type
-- Created: 21-NOV-96 Richard Stockton
---------------------------------------------------------------------
FUNCTION CODE_TYPE_DESC(O_error_message  IN OUT VARCHAR2,
                        I_code_type      IN     VARCHAR2,
                        O_desc           IN OUT VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function: GET_CODE_OR_DESC
-- Purpose:  This function will output the code and code description from
--           the code_detail table for the specified code type and either 
--           a specified code or code description. The purpose of this
--           function is to allow the user to specify a description and
--           have the function determine the code. If there is ever a 
--           situation where multiple codes for a specified code type have
--           identical descriptions, this function should only be called
--           if any of the codes for the description can then be used. This
--           function will also populate the description if the code is 
--           specified. In situations where a user may enter either a code
--           or the description in a text field, the form should call this
--           function inputting the value in the text field for both the 
--           code and description. If the inputted value corresponds to a code,
--           the function will get the description for the code. If the 
--           inputted value corresponds to ONLY a description, the function will
--           get the code for the description. If the inputted value corresponds
--           to BOTH a code and description, the function will use the code input
--           value to get the description.
--
--           Note: This function does not support dynamic hierarchy naming.
--
---------------------------------------------------------------------
FUNCTION GET_CODE_OR_DESC(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          IO_code          IN OUT CODE_DETAIL.CODE%TYPE,
                          IO_code_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                          I_code_type      IN     CODE_DETAIL.CODE_TYPE%TYPE)
return BOOLEAN;
---------------------------------------------------------------------  
TYPE codetype_code_codedesc_record is RECORD(CODE_TYPE             CODE_DETAIL.CODE_TYPE%TYPE,
                                             CODE                  CODE_DETAIL.CODE%TYPE,
                                             RETURN_TO             VARCHAR2(50),
                                             CODE_DESC             CODE_DETAIL.CODE_DESC%TYPE);                                        
TYPE list_codetype_code_codedesc is TABLE of codetype_code_codedesc_record; 
---------------------------------------------------------------------
FUNCTION GET_MANY_CODE_DESC(O_error_message 	             IN OUT     VARCHAR2,
                            O_list_codetype_code_withdesc    IN OUT     LIST_CODETYPE_CODE_CODEDESC,
                            I_list_codetype_code_nodesc      IN OUT     LIST_CODETYPE_CODE_CODEDESC)

RETURN BOOLEAN;
----------------------------------------------------------------
END LANGUAGE_SQL;
/

