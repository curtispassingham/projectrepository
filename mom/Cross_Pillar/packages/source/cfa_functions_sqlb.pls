create or replace 
PACKAGE BODY CFA_FUNCTIONS_SQL AS

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_EXT_ENTITY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC)
RETURN NUMBER AS

   L_program      VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.VALIDATE_EXT_ENTITY';
   L_function     VARCHAR2(61);
   L_return       NUMBER;
   L_group_set_rec CFA_GROUP_SET_REC;
   
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if GET_CUSTOM_FUNCTION_NAME(O_error_message,
                               L_function,
                               CFA_VALIDATE_ENTITY,
                               I_base_table_pk.base_table,
                               NULL,
                               NULL) = CFA_FALSE then
      return CFA_FALSE;
   end if;
   ---
   if L_function is NOT NULL then
      if EXEC_FUNCTION(O_error_message,
                       L_return,
                       L_group_set_rec,
                       I_base_table_pk,
                       L_function,
                       CFA_VALIDATE_ENTITY) = CFA_FALSE then
         return CFA_FALSE;
      end if;
      --
      if L_return = CFA_FALSE then
         return CFA_FALSE;
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return CFA_FALSE;
END VALIDATE_EXT_ENTITY;

--------------------------------------------------------------------------------
FUNCTION  DEFAULT_GROUP_SET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                            I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC)
RETURN NUMBER AS

   L_program      VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.DEFAULT_GROUP_SET';
   L_function     VARCHAR2(61);
   L_return       NUMBER;
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if GET_CUSTOM_FUNCTION_NAME(O_error_message,
                               L_function,
                               CFA_DEFAULT,
                               NULL,
                               I_group_set_rec.group_set_id,
                               NULL) = CFA_FALSE then
      return CFA_FALSE;
   end if;
   ---
   if L_function is NOT NULL then
      if EXEC_FUNCTION(O_error_message,
                       L_return,
                       I_group_set_rec,
                       I_base_table_pk,
                       L_function,
                       CFA_DEFAULT) = CFA_FALSE then
         return CFA_FALSE;
      end if;
      ---
      if L_return = CFA_FALSE then
         return CFA_FALSE;
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return CFA_FALSE;
END DEFAULT_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION QUALIFY_GROUP_SET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC,
                           I_group_set_id  IN     CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE)
RETURN NUMBER AS

   L_program       VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.QUALIFY_GROUP_SET';
   L_function      VARCHAR2(61);
   L_return        NUMBER;
   L_group_set_rec CFA_GROUP_SET_REC;
   
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if GET_CUSTOM_FUNCTION_NAME(O_error_message,
                               L_function,
                               CFA_QUALIFY,
                               NULL,
                               I_group_set_id,
                               NULL) = CFA_FALSE then
      return CFA_FALSE;
   end if;
   ---
   if L_function is NOT NULL then
      if EXEC_FUNCTION(O_error_message,
                       L_return,
                       L_group_set_rec,
                       I_base_table_pk,
                       L_function,
                       CFA_QUALIFY) = CFA_FALSE then
         return CFA_FALSE;
      end if;
      --
      if L_return = CFA_FALSE then
         return CFA_FALSE;
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return CFA_FALSE;
END QUALIFY_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_GROUP_SET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                             I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC)
RETURN NUMBER AS

   L_program       VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.VALIDATE_GROUP_SET';
   L_function      VARCHAR2(61);
   L_return        NUMBER;
   

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if GET_CUSTOM_FUNCTION_NAME(O_error_message,
                               L_function,
                               CFA_VALIDATE_GROUP_SET,
                               NULL,
                               I_group_set_rec.group_set_id,
                               NULL) = CFA_FALSE then
      return CFA_FALSE;
   end if;
   ---
   if L_function is NOT NULL then
      if EXEC_FUNCTION(O_error_message,
                       L_return,
                       I_group_set_rec,
                       I_base_table_pk,
                       L_function,
                       CFA_VALIDATE_GROUP_SET) = CFA_FALSE then
         return CFA_FALSE;
      end if;
      --
      if L_return = CFA_FALSE then
         return CFA_FALSE;
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return CFA_FALSE;
END VALIDATE_GROUP_SET;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_ATTRIBUTE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                             I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC,
                             I_attrib_id     IN     CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN NUMBER AS

   L_program   VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.VALIDATE_ATTRIBUTE';
   L_function  VARCHAR2(61);
   L_return    NUMBER;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if GET_CUSTOM_FUNCTION_NAME(O_error_message,
                               L_function,
                               CFA_VALIDATE_ATTRIBUTE,
                               NULL,
                               NULL,
                               I_attrib_id) = CFA_FALSE then
      return CFA_FALSE;
   end if;
   ---
   if L_function is NOT NULL then
      if EXEC_FUNCTION(O_error_message,
                       L_return,
                       I_group_set_rec,
                       I_base_table_pk,
                       L_function,
                       CFA_VALIDATE_ATTRIBUTE) = CFA_FALSE then
         return CFA_FALSE;
      end if;
      --
      if L_return = CFA_FALSE then
         return CFA_FALSE;
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return CFA_FALSE;
END VALIDATE_ATTRIBUTE;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION GET_CUSTOM_FUNCTION_NAME(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_function         OUT CFA_ATTRIB_GROUP_SET.QUALIFIER_FUNC%TYPE,
                                  I_function_type IN     VARCHAR2,
                                  I_base_table    IN     CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                                  I_group_set_id  IN     CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE,
                                  I_attrib_id     IN     CFA_ATTRIB.ATTRIB_ID%TYPE)
RETURN NUMBER AS

   L_program      VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.GET_CUSTOM_FUNCTION_NAME';

   cursor C_GET_ENTITY_VAL_FUNC is
      select validation_func
        from cfa_ext_entity ext
       where ext.base_rms_table = I_base_table;

   cursor C_GET_QUALIFY_FUNC is
      select qualifier_func
        from cfa_attrib_group_set
       where group_set_id = I_group_set_id;
   
   cursor C_GET_DEFAULT_FUNC is
      select default_func
        from cfa_attrib_group_set
       where group_set_id = I_group_set_id;

   cursor C_GET_GROUP_SET_VAL_FUNC is
      select validation_func
        from cfa_attrib_group_set
       where group_set_id = I_group_set_id;
       
   cursor C_GET_ATTRIB_VAL_FUNC is
      select validation_func
        from cfa_attrib
       where attrib_id = I_attrib_id;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if I_function_type = CFA_VALIDATE_ENTITY then
      open  C_GET_ENTITY_VAL_FUNC;
      fetch C_GET_ENTITY_VAL_FUNC into O_function;
      close C_GET_ENTITY_VAL_FUNC;
   elsif I_function_type = CFA_QUALIFY then
      open  C_GET_QUALIFY_FUNC;
      fetch C_GET_QUALIFY_FUNC into O_function;
      close C_GET_QUALIFY_FUNC;
   elsif I_function_type = CFA_DEFAULT then
      open  C_GET_DEFAULT_FUNC;
      fetch C_GET_DEFAULT_FUNC into O_function;
      close C_GET_DEFAULT_FUNC;
   elsif I_function_type = CFA_VALIDATE_GROUP_SET then
      open  C_GET_GROUP_SET_VAL_FUNC;
      fetch C_GET_GROUP_SET_VAL_FUNC into O_function;
      close C_GET_GROUP_SET_VAL_FUNC;
   elsif I_function_type = CFA_VALIDATE_ATTRIBUTE then
      open  C_GET_ATTRIB_VAL_FUNC;
      fetch C_GET_ATTRIB_VAL_FUNC into O_function;
      close C_GET_ATTRIB_VAL_FUNC;
   else
       O_function := NULL;
   end if;
   
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_ENTITY_VAL_FUNC%ISOPEN    then close C_GET_ENTITY_VAL_FUNC;    end if;
      if C_GET_QUALIFY_FUNC%ISOPEN       then close C_GET_QUALIFY_FUNC;       end if;
      if C_GET_DEFAULT_FUNC%ISOPEN       then close C_GET_DEFAULT_FUNC;       end if;
      if C_GET_GROUP_SET_VAL_FUNC%ISOPEN then close C_GET_GROUP_SET_VAL_FUNC; end if;
      if C_GET_ATTRIB_VAL_FUNC%ISOPEN    then close C_GET_ATTRIB_VAL_FUNC;    end if;
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return CFA_FALSE;
END GET_CUSTOM_FUNCTION_NAME;
--------------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_return_code   IN OUT NUMBER,
                       I_group_set_rec IN OUT CFA_GROUP_SET_REC,
                       I_base_table_pk IN     CFA_BASE_TABLE_PRIMARY_KEY_REC,
                       I_function      IN     VARCHAR2,
                       I_action        IN     VARCHAR2)
RETURN NUMBER AS

   L_program      VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.EXEC_FUNCTION';
   L_stmt         VARCHAR2(500);
   L_return       NUMBER;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if I_action = CFA_QUALIFY then
      L_stmt := 'BEGIN '||
                'if NOT '||I_function||'(:O_error_message, :I_base_table_pk) then '||
                   ':L_return := CFA_FUNCTIONS_SQL.CFA_FALSE; '||
                'else '||
                   ':L_return := CFA_FUNCTIONS_SQL.CFA_TRUE; '||
                'end if; '||
             'END;';
      --
      DBG_SQL.MSG(L_program,
                 L_stmt);
      --
      EXECUTE IMMEDIATE L_stmt
        USING IN OUT O_error_message,
              IN     I_base_table_pk,
              IN OUT L_return;
              
   elsif I_action = CFA_DEFAULT then
     L_stmt := 'BEGIN '||
                  'if NOT '||I_function||'(:O_error_message, :I_group_set_rec, :I_base_table_pk) then '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_FALSE; '||
                  'else '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_TRUE; '||
                  'end if; '||
               'END;';
     --
     DBG_SQL.MSG(L_program,
                 L_stmt);
    --
     EXECUTE IMMEDIATE L_stmt
        USING IN OUT O_error_message,
              IN OUT I_group_set_rec,
              IN     I_base_table_pk,
              IN OUT L_return;
              
   elsif I_action = CFA_VALIDATE_ENTITY then
     L_stmt := 'BEGIN '||
                  'if NOT '||I_function||'(:O_error_message, :I_base_table_pk) then '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_FALSE; '||
                  'else '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_TRUE; '||
                  'end if; '||
               'END;';
     --
     DBG_SQL.MSG(L_program,
                 L_stmt);
    --
     EXECUTE IMMEDIATE L_stmt
        USING IN OUT O_error_message,
              IN     I_base_table_pk,
              IN OUT L_return;
              
   elsif I_action = CFA_VALIDATE_GROUP_SET then
     L_stmt := 'BEGIN '||
                  'if NOT '||I_function||'(:O_error_message, :I_group_set_rec, :I_base_table_pk) then '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_FALSE; '||
                  'else '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_TRUE; '||
                  'end if; '||
               'END;';
     --
     DBG_SQL.MSG(L_program,
                 L_stmt);
    --
     EXECUTE IMMEDIATE L_stmt
        USING IN OUT O_error_message,
              IN     I_group_set_rec,
              IN     I_base_table_pk,
              IN OUT L_return;
              
   elsif I_action = CFA_VALIDATE_ATTRIBUTE then
     L_stmt := 'BEGIN '||
                  'if NOT '||I_function||'(:O_error_message, :I_group_set_rec, :I_base_table_pk) then '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_FALSE; '||
                  'else '||
                     ':L_return := CFA_FUNCTIONS_SQL.CFA_TRUE; '||
                  'end if; '||
               'END;';
     --
     DBG_SQL.MSG(L_program,
                 L_stmt);
    --
     EXECUTE IMMEDIATE L_stmt
        USING IN OUT O_error_message,
              IN     I_group_set_rec,
              IN     I_base_table_pk,
              IN OUT L_return;
              
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACT', I_action, NULL, NULL);
      return CFA_FALSE;
   end if;
   --
   O_return_code := L_return;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return CFA_FALSE;
END EXEC_FUNCTION;
--------------------------------------------------------------------------------
FUNCTION  VALIDATE_FUNCTION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_function      IN     VARCHAR2)
RETURN NUMBER AS

   L_program      VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.VALIDATE_FUNCTION';
   L_package      VARCHAR2(60);
   L_function     VARCHAR2(60);
   L_exists       VARCHAR2(1);
   L_tab_owner    SYSTEM_OPTIONS.TABLE_OWNER%TYPE;

   cursor C_FIND_PACKAGE_FUNC is
      select 'x'
        from all_procedures
       where owner          = L_tab_owner
         and object_type    = 'PACKAGE'
         and object_name    = L_package
         and procedure_name = L_function
         and rownum =1;

   cursor C_FIND_FUNC is
      select 'x'
        from all_procedures
       where owner       = L_tab_owner
         and object_type in ('FUNCTION', 'PROCEDURE')
         and object_name = L_function
         and rownum =1;

   cursor C_GET_TAB_OWNER is
      select table_owner
        from system_options;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   open C_GET_TAB_OWNER;
   fetch C_GET_TAB_OWNER into L_tab_owner;
   close C_GET_TAB_OWNER;
   ---
   if (INSTR(I_function, '.')) != 0 then
      L_package  := UPPER(SUBSTR(I_function,1,INSTR(I_function, '.')-1));
      L_function := UPPER(SUBSTR(I_function,INSTR(I_function, '.')+1, LENGTH(I_function)));

      open C_FIND_PACKAGE_FUNC;
      fetch C_FIND_PACKAGE_FUNC into L_exists;

       if C_FIND_PACKAGE_FUNC%NOTFOUND then
          O_error_message := SQL_LIB.CREATE_MSG('FUNC_NOT_FOUND',
                                                I_function);
          close C_FIND_PACKAGE_FUNC;
          return CFA_FALSE;
       end if;

      close C_FIND_PACKAGE_FUNC;
   else
      L_function := UPPER(I_function);

      open C_FIND_FUNC;
      fetch C_FIND_FUNC into L_exists;

      if C_FIND_FUNC%NOTFOUND then
          O_error_message := SQL_LIB.CREATE_MSG('FUNC_NOT_FOUND',
                                                I_function);
          close C_FIND_FUNC;
          return CFA_FALSE;
       end if;

      close C_FIND_FUNC;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      if C_FIND_PACKAGE_FUNC%ISOPEN then close C_FIND_PACKAGE_FUNC; end if;
      if C_FIND_FUNC%ISOPEN         then close C_FIND_FUNC;         end if;
      if C_GET_TAB_OWNER%ISOPEN     then close C_GET_TAB_OWNER;     end if;
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return CFA_FALSE;
END VALIDATE_FUNCTION;
--------------------------------------------------------------------------------
FUNCTION GET_GROUP_SETS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_base_table     IN     CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE,
                        O_group_sets_tbl    OUT CFA_ENTITY_GROUP_SETS_TBL)
RETURN NUMBER AS

   L_program      VARCHAR2(62) := 'CFA_FUNCTIONS_SQL.GET_GROUP_SETS';
   
   cursor GET_GROUP_SETS is
      SELECT CFA_ENTITY_GROUP_SETS_REC(ags.group_set_id,
                                       ags.display_seq,
                                       NVL(agsl.label, agsl2.label))
        FROM cfa_ext_entity ent,
             cfa_attrib_group_set ags,
             cfa_attrib_group_set_labels agsl,
             cfa_attrib_group_set_labels agsl2
       WHERE ent.base_rms_table     = I_base_table
         AND ent.ext_entity_id      = ags.ext_entity_id
         AND ags.active_ind         = 'Y'
         AND ags.group_set_id       = agsl.group_set_id(+)
         AND agsl.lang(+)           = GET_USER_LANG
         AND ags.group_set_id       = agsl2.group_set_id
         AND agsl2.default_lang_ind = 'Y'
       order by ags.display_seq;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   open GET_GROUP_SETS;
   fetch GET_GROUP_SETS BULK COLLECT into O_group_sets_tbl;
   close GET_GROUP_SETS;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return CFA_TRUE;
EXCEPTION
   when OTHERS then
      if GET_GROUP_SETS%ISOPEN then close GET_GROUP_SETS; end if;
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return CFA_FALSE;
END GET_GROUP_SETS;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END CFA_FUNCTIONS_SQL;
/