CREATE OR REPLACE PACKAGE BODY CALL_CUSTOM_SQL AS
------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_custom_obj_rec     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'CALL_CUSTOM_SQL.EXEC_FUNCTION';
   L_function_name      VARCHAR2(120) := NULL;
   L_return             VARCHAR2(30);
   L_dynamic_call       VARCHAR2(300);
   L_call_seq_no        VARCHAR2 (3):=NULL;
   L_source_type        VARCHAR2 (6);
   L_source_id          VARCHAR2 (10);

   cursor C_GET_CUSTOM_PROC is
      select decode(package_name,null,schema_name||'.'||function_name,schema_name||'.'||package_name||'.'||function_name) function_name
        from custom_pkg_config
       where function_key = IO_custom_obj_rec.function_key
         and call_seq_no = IO_custom_obj_rec.call_seq_no;

BEGIN

   --Return error if  CUSTOM_OBJ_REC.Function_key is null or call_seq_no is NULL

   if IO_custom_obj_rec.function_key is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'function_key', NULL, NULL);
      return FALSE;
   end if;

   if IO_custom_obj_rec.call_seq_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'call_seq_no', NULL, NULL);
      return FALSE;
   end if;

   --Check if a record exists on CUSTOM_PKG_CONFIG table with the function key and call_seq_no. If no record exists then return true and exit.

   open C_GET_CUSTOM_PROC;
   fetch C_GET_CUSTOM_PROC into L_function_name;
   close C_GET_CUSTOM_PROC;

    if L_function_name is null then
       return TRUE;
    end if;

   -- Dynamically execute the function.

   L_dynamic_call := 'BEGIN if '||L_function_name||' (:O_error_message, :IO_custom_obj_rec) = FALSE then :L_return := ''N''; end if; END;';
   -- Generic execution - O_error_message,IO_custom_obj_rec will be common to all scripts
   EXECUTE IMMEDIATE L_dynamic_call using in out O_error_message, in out IO_custom_obj_rec, out L_return;
   if L_return = 'N' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_CUSTOM_PROC%ISOPEN then
         close C_GET_CUSTOM_PROC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FUNCTION;
------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_ord_apprerr_exist IN OUT BOOLEAN,
                       IO_custom_obj_rec   IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'CALL_CUSTOM_SQL.EXEC_FUNCTION';
   L_function_name      VARCHAR2(120) := NULL;
   L_return             VARCHAR2(30);
   L_dynamic_call       VARCHAR2(300);
   L_call_seq_no        VARCHAR2 (3):= NULL;
   L_source_type        VARCHAR2 (6);
   L_source_id          VARCHAR2 (10);

   cursor C_GET_CUSTOM_PROC is
      select decode(package_name,null,schema_name||'.'||function_name,schema_name||'.'||package_name||'.'||function_name) function_name
        from custom_pkg_config
       where function_key = IO_custom_obj_rec.function_key
         and call_seq_no = IO_custom_obj_rec.call_seq_no;

BEGIN

   --Return error if  CUSTOM_OBJ_REC.Function_key is null or call_seq_no is NULL

   if IO_custom_obj_rec.function_key is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'function_key', NULL, NULL);
      return FALSE;
   end if;

   if IO_custom_obj_rec.call_seq_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'call_seq_no', NULL, NULL);
      return FALSE;
   end if;

   --Check if a record exists on CUSTOM_PKF_CONFIG table with the function key and call_seq_no. If not record exists then return true and exit.

   open C_GET_CUSTOM_PROC;
   fetch C_GET_CUSTOM_PROC into L_function_name;
   close C_GET_CUSTOM_PROC;

   if L_function_name is null then
      return TRUE;
   end if;

   -- Dynamically execute the function.

   L_dynamic_call := 'BEGIN if '||L_function_name||' (:O_error_message, :O_ord_apprerr_exist, :IO_custom_obj_rec) = FALSE then :L_return := ''N''; end if; END;';
   EXECUTE IMMEDIATE L_dynamic_call
      using in out O_error_message,
            in out O_ord_apprerr_exist,
            in out IO_custom_obj_rec,
            out L_return;
   if L_return = 'N' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_CUSTOM_PROC%ISOPEN then
         close C_GET_CUSTOM_PROC;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FUNCTION;
------------------------------------------------------------------------------
END CALL_CUSTOM_SQL;
/
