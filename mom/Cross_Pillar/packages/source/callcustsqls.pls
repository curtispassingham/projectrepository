SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CALL_CUSTOM_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------------------------
-- Function Name:  EXEC_FUNCTION
-- Purpose      :  This function will allow the customers to call their 
--                 custom functions dynamically from RMS base code, through 
--                 which they can perform the customize validation during 
--                 approval of item. 
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_custom_obj_rec  IN OUT  CUSTOM_OBJ_REC )
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
-- Function Name:  EXEC_FUNCTION
-- Purpose      :  This function will allow the customers to call their 
--                 purchase order approval custom functions dynamically 
--                 from RMS base code.
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FUNCTION(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_ord_apprerr_exist   IN OUT   BOOLEAN,
                       IO_custom_obj_rec     IN OUT   CUSTOM_OBJ_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END CALL_CUSTOM_SQL;
/