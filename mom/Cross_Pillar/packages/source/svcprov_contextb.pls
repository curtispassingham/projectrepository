CREATE OR REPLACE PACKAGE BODY SVCPROV_CONTEXT AS
--------------------------------------------------------------------------------
FUNCTION SET_SVCPROV_CONTEXT(O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC")
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_fail_tbl        "RIB_FailStatus_TBL"  := "RIB_FailStatus_TBL"();
   L_svcprov_name    VARCHAR2(10)          := NULL;
   L_country_type    CONSTANT VARCHAR2(10) := 'country';
   L_language_type   CONSTANT VARCHAR2(10) := 'language';
   

BEGIN
   --Reset variables
   SVCPROV_CONTEXT.USER_NAME     := NULL;
   SVCPROV_CONTEXT.USER_COUNTRY  := NULL;
   SVCPROV_CONTEXT.USER_LANGUAGE := NULL;
   --
   --Set variables
   if I_serviceOperationContext is not NULL then
      if I_serviceOperationContext.principal.name is not NULL then
         SVCPROV_CONTEXT.USER_NAME := UPPER(I_serviceOperationContext.principal.name);
      else
         L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_serviceOperationContext.principal.name',
                                               L_program,
                                               NULL);
         if L_fail_tbl.COUNT = 0 then
            L_fail_tbl.EXTEND;
         end if;
         L_fail_tbl(1) := SVCPROV_UTILITY.BUILD_FAIL_RECORD(L_error_message,L_fail_tbl(1));
      end if;
      ---
      if I_serviceOperationContext.messagecontext.property_tbl is NOT NULL and
         I_serviceOperationContext.messagecontext.property_tbl.count > 0 then
         FOR j in I_serviceOperationContext.messagecontext.property_tbl.first..I_serviceOperationContext.messagecontext.property_tbl.last LOOP
            if I_serviceOperationContext.messagecontext.property_tbl(j).name is not NULL and
               I_serviceOperationContext.messagecontext.property_tbl(j).value is not NULL then  
               L_svcprov_name := SUBSTR(I_serviceOperationContext.messagecontext.property_tbl(j).name,1,10);
               --
               CASE LOWER(L_svcprov_name)
                  WHEN L_language_type THEN
                     SVCPROV_CONTEXT.USER_LANGUAGE := LOWER(SUBSTR(I_serviceOperationContext.messagecontext.property_tbl(j).value,1,2));
                  WHEN L_country_type THEN
                     SVCPROV_CONTEXT.USER_COUNTRY  := LOWER(SUBSTR(I_serviceOperationContext.messagecontext.property_tbl(j).value,1,3));
                  ELSE
                     NULL;
               END CASE;
            end if;
         END LOOP;
      else
         L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_serviceOperationContext.messagecontext.property_tbl',
                                            L_program,
                                            NULL);
         if L_fail_tbl.COUNT = 0 then
            L_fail_tbl.EXTEND;
         end if;
         L_fail_tbl(1) := SVCPROV_UTILITY.BUILD_FAIL_RECORD(L_error_message,L_fail_tbl(1));
      end if;
      ---
   else
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_serviceOperationContext',
                                            L_program,
                                            NULL);
      if L_fail_tbl.COUNT = 0 then
         L_fail_tbl.EXTEND;
      end if;
      L_fail_tbl(1) := SVCPROV_UTILITY.BUILD_FAIL_RECORD(L_error_message,L_fail_tbl(1));
   end if;
   ---
   if L_fail_tbl.count != 0 then
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, NULL, L_fail_tbl);
      return FALSE;
   end if;
   
   --- Initialize O_serviceOperationStatus
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, NULL, NULL);
   return TRUE;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      if L_fail_tbl.COUNT = 0 then
         L_fail_tbl.EXTEND;
      end if;
      L_fail_tbl(1) := SVCPROV_UTILITY.BUILD_FAIL_RECORD(L_error_message,L_fail_tbl(1));
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, NULL, L_fail_tbl);
   return FALSE;
END SET_SVCPROV_CONTEXT;
--------------------------------------------------------------------------------
END SVCPROV_CONTEXT;
/