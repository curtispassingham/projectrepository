CREATE OR REPLACE PACKAGE BODY CFA_LOAD_SQL AS
--------------------------------------------------------------------------------
FUNCTION LOAD_STORE_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_stg_table_name   IN       CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE,
                           I_process_id       IN       NUMBER,
                           I_del_stg_rec      IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION LOAD_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_stg_table_name   IN       CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE,
                     I_process_id       IN       NUMBER DEFAULT NULL,
                     I_del_stg_rec      IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN AS
   
   L_program         VARCHAR2(62) := 'CFA_LOAD_SQL.LOAD_ATTRIB';  
   L_ext_entity_id   CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE;
   L_ext_tbl         CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE;
   L_group_set_id    CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
   L_group_id        CFA_ATTRIB_GROUP.GROUP_ID%TYPE;
   
   L_stmt            VARCHAR2(4000);
   L_insert          VARCHAR2(1000);
   L_key             VARCHAR2(500);
   L_select          VARCHAR2(1000);
   L_from            VARCHAR2(100);
   L_where           VARCHAR2(200);
   
   cursor C_GET_GROUP_SET is
      select group_set_id,
             ext_entity_id
        from cfa_attrib_group_set
       where UPPER(staging_table_name) = UPPER(I_stg_table_name);
       
   cursor C_GET_KEYS is
      select key_col,
             custom_ext_table
        from cfa_ext_entity_key ek,
             cfa_ext_entity ext
       where ext.ext_entity_id = L_ext_entity_id
         and ext.base_rms_table = ek.base_rms_table;
         
   cursor C_GET_GROUP is
      select group_id
        from cfa_attrib_group grp
       where grp.group_set_id = L_group_set_id;
       
   cursor C_GET_ATTRIB_COL is
      select view_col_name,
             storage_col_name
        from cfa_attrib attr
       where attr.group_id = L_group_id
         and active_ind    = 'Y';        
        
      
BEGIN

   open C_GET_GROUP_SET;
   fetch C_GET_GROUP_SET into L_group_set_id,
                              L_ext_entity_id;
   if C_GET_GROUP_SET%NOTFOUND then
      close C_GET_GROUP_SET;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STG_TBL', UPPER(I_stg_table_name));
      return FALSE;
   end if;
   close C_GET_GROUP_SET;
   
   -- Check if loading to store tables
   open C_GET_KEYS;
   fetch C_GET_KEYS into L_key, L_ext_tbl;
   close C_GET_KEYS;
   --
   if L_ext_tbl = 'STORE_CFA_EXT' then
      if LOAD_STORE_ATTRIB(O_error_message,
                           I_stg_table_name,
                           I_process_id,
                           I_del_stg_rec) = FALSE then
        return FALSE;
      end if;
      --
      return TRUE;
   else 
      L_ext_tbl := NULL;
      L_key     := NULL;
   end if;
   
   -- Build the key columns
   for rec in C_GET_KEYS loop
       if L_key is NULL then
          L_key := rec.key_col;
       else
          L_key := L_key ||', '||rec.key_col;
       end if;
       
       if L_ext_tbl is NULL then
          L_ext_tbl := rec.custom_ext_table;   
       end if;
   end loop;

   -- compose insert select statement
   L_from  := ' from '||I_stg_table_name||' ';
   
   if I_process_id is NOT NULL then
     L_where := 'where process_id = '||I_process_id;
   end if;
   
   for group_rec in C_GET_GROUP loop
      L_group_id := group_rec.group_id;
      
      L_insert := 'insert into '||L_ext_tbl||'('||L_key||', GROUP_ID';
      L_select := 'select '||L_key||', '||L_group_id;
      ---
      for attrib_rec in C_GET_ATTRIB_COL loop
         L_insert := L_insert||', '||attrib_rec.storage_col_name;
         L_select := L_select||', '||attrib_rec.view_col_name;
      end loop;
      
      L_insert := L_insert||')';
      
      L_stmt := L_insert||L_select||L_from||L_where;
      
      dbg_sql.msg(L_program,
                  L_stmt);
                  
      EXECUTE IMMEDIATE L_stmt;                  
      
   end loop;
   
   if I_del_stg_rec = 'Y' then
      if not DELETE_STAGING_REC(O_error_message,
                                I_stg_table_name,
                                I_process_id) then
         return FALSE;
      end if;
   end if;
 
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_KEYS%ISOPEN       then close C_GET_KEYS;       end if;
      if C_GET_GROUP_SET%ISOPEN  then close C_GET_GROUP_SET;  end if;
      if C_GET_GROUP%ISOPEN      then close C_GET_GROUP;      end if;
      if C_GET_ATTRIB_COL%ISOPEN then close C_GET_ATTRIB_COL; end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END LOAD_ATTRIB;
--------------------------------------------------------------------------------
FUNCTION LOAD_STORE_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_stg_table_name   IN       CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE,
                           I_process_id       IN       NUMBER,
                           I_del_stg_rec      IN       VARCHAR2)
   RETURN BOOLEAN AS
   
   L_program           VARCHAR2(62) := 'CFA_LOAD_SQL.LOAD_STORE_ATTRIB';  
   L_ext_entity_id     CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE;
   L_ext_tbl_store     CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE := 'STORE_CFA_EXT';
   L_ext_tbl_store_add CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE := 'STORE_ADD_CFA_EXT';
   L_group_set_id      CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
   L_group_id          CFA_ATTRIB_GROUP.GROUP_ID%TYPE;
   
   L_stmt_store      VARCHAR2(4000);
   L_stmt_store_add  VARCHAR2(4000);
   L_insert          VARCHAR2(1000);
   L_key             VARCHAR2(10) := 'STORE';
   L_select          VARCHAR2(1000);
   L_from            VARCHAR2(100);
   L_to_store        VARCHAR2(100);
   L_to_store_add    VARCHAR2(100);
   L_where_store     VARCHAR2(200);
   L_where_store_add VARCHAR2(200);
   
   cursor C_GET_GROUP_SET is
      select group_set_id
        from cfa_attrib_group_set
       where UPPER(staging_table_name) = UPPER(I_stg_table_name);
                
   cursor C_GET_GROUP is
      select group_id
        from cfa_attrib_group grp
       where grp.group_set_id = L_group_set_id;
       
   cursor C_GET_ATTRIB_COL is
      select view_col_name,
             storage_col_name
        from cfa_attrib attr
       where attr.group_id = L_group_id
         and active_ind    = 'Y';        
        
      
BEGIN

   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   open C_GET_GROUP_SET;
   fetch C_GET_GROUP_SET into L_group_set_id;
   if C_GET_GROUP_SET%NOTFOUND then
      close C_GET_GROUP_SET;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STG_TBL', UPPER(I_stg_table_name));
      return FALSE;
   end if;
   close C_GET_GROUP_SET;
   --
   L_to_store     := 'insert into '||L_ext_tbl_store;
   L_to_store_add := 'insert into '||L_ext_tbl_store_add;

   -- compose insert select statement
   L_from  := ' from '||I_stg_table_name||' stg ';
   
   if I_process_id is NOT NULL then
     L_where_store     := 'where stg.process_id = '||I_process_id||' and  exists (select store from store st where st.store = stg.store) ';
     L_where_store_add := 'where stg.process_id = '||I_process_id||' and  exists (select store from store_add st where st.store = stg.store) ';
   else
     L_where_store     := 'where exists (select store from store st where st.store = stg.store) ';
     L_where_store_add := 'where exists (select store from store_add st where st.store = stg.store) ';
   end if;
   
   for group_rec in C_GET_GROUP loop
      L_group_id := group_rec.group_id;
      
      L_insert := '('||L_key||', GROUP_ID';
      L_select := 'select '||L_key||', '||L_group_id;
      ---
      for attrib_rec in C_GET_ATTRIB_COL loop
         L_insert := L_insert||', '||attrib_rec.storage_col_name;
         L_select := L_select||', '||attrib_rec.view_col_name;
      end loop;
      
      L_insert := L_insert||')';
      
      L_stmt_store := L_to_store||L_insert||L_select||L_from||L_where_store;
      L_stmt_store_add := L_to_store_add||L_insert||L_select||L_from||L_where_store_add;
      
      dbg_sql.msg(L_program,
                  L_stmt_store);
                  
      EXECUTE IMMEDIATE L_stmt_store; 
      
      dbg_sql.msg(L_program,
                  L_stmt_store_add);
                  
      EXECUTE IMMEDIATE L_stmt_store_add;                  
      
   end loop;
   
   if I_del_stg_rec = 'Y' then
      if not DELETE_STAGING_REC(O_error_message,
                                I_stg_table_name,
                                I_process_id) then
         return FALSE;
      end if;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);
 
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_GROUP_SET%ISOPEN  then close C_GET_GROUP_SET;  end if;
      if C_GET_GROUP%ISOPEN      then close C_GET_GROUP;      end if;
      if C_GET_ATTRIB_COL%ISOPEN then close C_GET_ATTRIB_COL; end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END LOAD_STORE_ATTRIB;
--------------------------------------------------------------------------------
FUNCTION DELETE_STAGING_REC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_stg_table_name   IN       CFA_ATTRIB_GROUP_SET.STAGING_TABLE_NAME%TYPE,
                            I_process_id       IN       NUMBER DEFAULT NULL)
   RETURN BOOLEAN AS
   
   L_program    VARCHAR2(62) := 'CFA_LOAD_SQL.DELETE_STAGING_REC';
   L_stmt       VARCHAR2(100);

BEGIN
   
   L_stmt := 'delete from '||I_stg_table_name;
   --
   if I_process_id is NOT NULL then
     L_stmt := L_stmt ||' where process_id = '||I_process_id;
   end if;
   --
    dbg_sql.msg(L_program,
                L_stmt);
   --        
   EXECUTE IMMEDIATE L_stmt;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END DELETE_STAGING_REC;
--------------------------------------------------------------------------------
END CFA_LOAD_SQL;
/
