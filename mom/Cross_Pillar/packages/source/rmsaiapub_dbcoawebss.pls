CREATE OR REPLACE
PACKAGE AIA_WEBSERVICE_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------------------------
-- Function    : GET_DRILL_FORWARD_URL
-- Purpose     : This public function get the PSFT-URL from RETAIL_SERVICE_REPORT_URL table.
--------------------------------------------------------------------------------------------------
FUNCTION GET_DRILL_FORWARD_URL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_psft_url             IN OUT   VARCHAR2,
                               I_reference_trace_id   IN       KEY_MAP_GL.REFERENCE_TRACE_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function    : VALIDATE_ACCOUNT
-- Purpose     : This public function will validate the account entered.
--------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ACCOUNT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_requesting_system    IN OUT   VARCHAR2,
                          O_set_of_books_id      IN OUT   ORG_UNIT.SET_OF_BOOKS_ID%TYPE,
                          O_ccid                 IN OUT   FIF_GL_CROSS_REF.DR_CCID%TYPE,
                          O_segment1             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment2             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment3             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment4             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment5             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment6             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment7             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment8             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment9             IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment10            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment11            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment12            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment13            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment14            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment15            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment16            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment17            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment18            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment19            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_segment20            IN OUT   FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          O_account_status       IN OUT   VARCHAR2,
                          I_requesting_system    IN       VARCHAR2,
                          I_set_of_books_id      IN       ORG_UNIT.SET_OF_BOOKS_ID%TYPE,
                          I_ccid                 IN       FIF_GL_CROSS_REF.DR_CCID%TYPE,
                          I_segment1             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment2             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment3             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment4             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment5             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment6             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment7             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment8             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment9             IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment10            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment11            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment12            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment13            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment14            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment15            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment16            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment17            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment18            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment19            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE,
                          I_segment20            IN       FIF_GL_CROSS_REF.DR_SEQUENCE1%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
END AIA_WEBSERVICE_SQL;
/
