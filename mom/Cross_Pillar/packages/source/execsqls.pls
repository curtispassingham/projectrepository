
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE EXECUTE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------
--FUNCTION EXECUTE_SQL executes SQL statments on the server side.  For example
--an insert statement can be passed in and executed.  The forms version of SQL
--does not support dynamic SQL, so this package can be used to execute dynamic SQL
--on the server side.
----------------------------------------------------------------------------------

FUNCTION EXECUTE_SQL  (O_ERROR_MESSAGE	IN OUT		VARCHAR2,
                       I_STATEMENT      IN              VARCHAR2)


RETURN BOOLEAN;
----------------------------------------------------------------------------------
END EXECUTE_SQL;
/



