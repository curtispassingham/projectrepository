/******************************************************************************
* Service Name     : ReportLocatorService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/ReportLocatorService/v1
* Description      : $service.documentation
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE REPORTLOCATORSERVICEPROVIDERIM AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : findReportLocDesc
 * Description     : Find the location of the Report. 
 * 
 * Input           : "RIB_ReportLocRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ReportLocRef/v1
 * Description     :  ReportLocationRef object consists of input
				information for finding the Report.This includes service provider
				name and URL.
 * 
 * Output          : "RIB_ReportLocDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ReportLocDesc/v1
 * Description     :  ReportLocationDesc object consists of inputs to
				Report location description.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
				message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the report does not
				exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
				"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE findReportLocDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_ReportLocRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_ReportLocDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : publishReportLocDesc
 * Description     : Publish the Reports. 
 * 
 * Input           : "RIB_ReportLocDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ReportLocDesc/v1
 * Description     :  ReportLocationDesc object consists of inputs to
				Report location description.
 * 
 * Output          : "RIB_ReportLocRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ReportLocRef/v1
 * Description     :  Returnt ReportLocationRef object consists of
				information for finding the Report.This includes service provider
				name and URL.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
				message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the Report has already
				been published.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
				"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE publishReportLocDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_ReportLocDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_ReportLocRef_REC"
                          );
/******************************************************************************/



END ReportLocatorServiceProviderIm;
/
 