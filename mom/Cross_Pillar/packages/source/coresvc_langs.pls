CREATE OR REPLACE PACKAGE CORESVC_LANG AUTHID CURRENT_USER AS
   template_key    CONSTANT VARCHAR2(255)         := 'LANG_DATA';
   action_new               VARCHAR2(25)          := 'NEW';
   action_mod               VARCHAR2(25)          := 'MOD';
   action_del               VARCHAR2(25)          := 'DEL';
   LANG_SHEET               VARCHAR2(255)         := 'LANG';
   LANG$ACTION              NUMBER                :=1;
   LANG$APP_SERVER          NUMBER                :=8;
   LANG$ISO_CODE            NUMBER                :=7;
   LANG$WEBREPORTS_SERVER   NUMBER                :=6;
   LANG$REPORTS_SERVER      NUMBER                :=5;
   LANG$WEBHELP_SERVER      NUMBER                :=4;
   LANG$DESCRIPTION         NUMBER                :=3;
   LANG$LANG                NUMBER                :=2;
   
   LANG_TL_SHEET            VARCHAR2(255)         := 'LANG_TL';
   LANG_TL$ACTION           NUMBER                :=1;
   LANG_TL$LANG             NUMBER                :=2;
   LANG_TL$LANG_LANG        NUMBER                :=3;
   LANG_TL$DESCRIPTION      NUMBER                :=4;

   sheet_name_trans         S9T_PKG.TRANS_MAP_TYP;
   action_column            VARCHAR2(255)         := 'ACTION';
   template_category        CODE_DETAIL.CODE%TYPE := 'RMSADM';
   TYPE lang_rec_tab IS TABLE OF LANG%ROWTYPE;
   
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
   
END CORESVC_LANG;
/