/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Tiago Torres / Paulo Mamede                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Synonyms                                                     */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  rms_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&rms_schema_owner'));
  adeo_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&adeo_schema_owner'));
begin
  execute immediate 'CREATE OR REPLACE SYNONYM '||adeo_owner||'.XXADEO_SUPP_CUST_RULES_CFA_SQL FOR '||rms_owner||'.XXADEO_SUPP_CUST_RULES_CFA_SQL';
end;
/