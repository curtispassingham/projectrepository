/*---------------------------------------------------------------------*/
/*
* Description:   Synonyms for Rib objects
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

set serveroutput on size unlimited
set escape on

declare
  adeo_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&adeo_schema_owner'));
  rms_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&rms_schema_owner'));
begin
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrDivDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrDivDesc_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrGrpDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrGrpDesc_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrGrpRef_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrGrpRef_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrDeptDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrDeptDesc_REC"';  
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrDeptRef_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrDeptRef_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrClsDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrClsDesc_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrClsRef_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrClsRef_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrSclsDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrSclsDesc_REC"';  
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfMrchHrSclsRef_REC" FOR '||adeo_owner||'."RIB_ExtOfMrchHrSclsRef_REC"';  
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfXItemDesc_TBL" FOR '||adeo_owner||'."RIB_ExtOfXItemDesc_TBL"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfItemSupDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfItemSupDesc_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfItemHdrDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfItemHdrDesc_REC"';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfItemSupCtyDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfItemSupCtyDesc_REC"';  
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'."RIB_ExtOfVendorHdrDesc_REC" FOR '||adeo_owner||'."RIB_ExtOfVendorHdrDesc_REC"';
  --
end;
/
