/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Synonyms                                                     */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  rms_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&1'));
  adeo_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&2'));
begin
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.XXADEO_RPM_CUSTOM_OPTIONS FOR '||adeo_owner||'.XXADEO_RPM_CUSTOM_OPTIONS';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.XXADEO_RMS_ASYNC_PROCESS_SQL FOR '||adeo_owner||'.XXADEO_RMS_ASYNC_PROCESS_SQL';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.XXADEO_V_CFA_ATTRIB_GROUP_SET FOR '||adeo_owner||'.XXADEO_V_CFA_ATTRIB_GROUP_SET';
end;
/
    