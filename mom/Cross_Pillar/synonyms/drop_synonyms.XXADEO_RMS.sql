/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Synonyms                                                     */
/******************************************************************************/
declare
  synonym_schema      varchar2(30);
  owning_schema       varchar2(30);
  run_schema          varchar2(30);
  prefix1             varchar2(128);
  prefix2             varchar2(128);
  
  --
begin
  --
  synonym_schema := sys.dbms_assert.schema_name(upper('&xxadeo_schema_owner'));
  owning_schema  := sys.dbms_assert.schema_name(upper('&rms_schema_owner'));
  run_schema     := sys.dbms_assert.schema_name('&_USER');
  
  IF synonym_schema <> run_schema THEN
    prefix1:=sys.dbms_assert.enquote_name(synonym_schema,FALSE)||'.';
  ELSE
    prefix1:='';
  END IF;
  
  IF owning_schema <> run_schema THEN
    prefix2:=sys.dbms_assert.enquote_name(owning_schema,FALSE)||'.';
  ELSE
    prefix2:='';
  END IF;
  --
  begin
    --
    execute immediate 'drop synonym '||prefix2||'XXADEO_CREATE_COST_IL_SQL';
    dbms_output.put_line('Drop synonym XXADEO_CREATE_COST_IL_SQL '||SQLCODE||' - '||SQLERRM);
	--
  exception
    --
    when OTHERS then
      --
      dbms_output.put_line('Drop synonym FAILED '||SQLCODE||' - '||SQLERRM);
      --
  END;
  --
EXCEPTION
  WHEN OTHERS THEN
    raise;
end;