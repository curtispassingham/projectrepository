/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Filipa Neves                                                 */
/* PROJECT     - ADEO - O1C                                                   */
/* DESCRIPTION - Workaroud to mitigate SR 3-18630300141                       */
/******************************************************************************/

update cfa_attrib
   set lowest_allowed_value = null
 where  attrib_id in (244,
                      284,
                      324,
                      124,
                      164,
                      204,
                      364,
                      125,
                      165,
                      205,
                      245,
                      285,
                      325,
                      365)
  and lowest_allowed_value is not null; 
