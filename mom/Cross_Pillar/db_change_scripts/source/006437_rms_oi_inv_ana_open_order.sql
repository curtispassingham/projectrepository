--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_ANA_OPEN_ORDER
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_ANA_OPEN_ORDER'
CREATE TABLE RMS_OI_INV_ANA_OPEN_ORDER
 (SESSION_ID NUMBER(15,0),
  ORDER_NO NUMBER(10,0),
  SUPPLIER_SITE NUMBER(10,0),
  SUP_NAME VARCHAR2(240 ),
  NOT_BEFORE_DATE DATE,
  NOT_AFTER_DATE DATE,
  OTB_EOW_DATE DATE,
  QTY_ORDERED NUMBER(12,4),
  QTY_RECEIVED NUMBER(12,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_INV_ANA_OPEN_ORDER is 'This table holds data for the Inventory Analyst Order Errors Detail Report.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.SESSION_ID is 'The session_id for the data'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.ORDER_NO is 'The order.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.SUPPLIER_SITE is 'The supplier of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.SUP_NAME is 'The supplier name.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.NOT_BEFORE_DATE is 'The not before date of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.NOT_AFTER_DATE is 'The not after date of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.OTB_EOW_DATE is 'the otb eow date of the order'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.QTY_ORDERED is 'The quantity ordered.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_OPEN_ORDER.QTY_RECEIVED is 'Teh quantity received.'
/

PROMPT Creating Index on 'RMS_OI_INV_ANA_OPEN_ORDER'
 CREATE INDEX RMS_OI_INV_ANA_OPEN_ORDER_I1 on RMS_OI_INV_ANA_OPEN_ORDER
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
