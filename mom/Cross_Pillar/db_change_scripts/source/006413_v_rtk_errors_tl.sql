--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW CREATE:           V_RTK_ERRORS_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_RTK_ERRORS_TL'
CREATE OR REPLACE FORCE VIEW V_RTK_ERRORS_TL (RTK_KEY, RTK_TEXT, LANG ) AS
SELECT  b.rtk_key,
        case when tl.lang is not null then tl.rtk_text else b.rtk_text end rtk_text,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  rtk_errors b,
        rtk_errors_tl tl
 WHERE  b.rtk_key = tl.rtk_key (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_RTK_ERRORS_TL is 'This is the translation view for base table RTK_ERRORS. This view fetches data in user langauge either from translation table RTK_ERRORS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_RTK_ERRORS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_RTK_ERRORS_TL.RTK_KEY is 'Contains the error key.'
/

COMMENT ON COLUMN V_RTK_ERRORS_TL.RTK_TEXT is 'This Column holds the error message.'
/

