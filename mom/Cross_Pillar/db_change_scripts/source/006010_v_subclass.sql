--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	VIEW UPDATED:				V_SUBCLASS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_SUBCLASS'

CREATE OR REPLACE FORCE VIEW V_SUBCLASS
 (DEPT 
 ,CLASS
 ,SUBCLASS
 ,SUB_NAME)
 AS SELECT  SUB.DEPT DEPT
           ,SUB.CLASS CLASS
           ,SUB.SUBCLASS SUBCLASS
           ,LANGUAGE_SQL.TRANSLATE(SUB.SUB_NAME) SUB_NAME
 FROM 	 SUBCLASS SUB
		,CLASS CLA
 WHERE CLA.DEPT = SUB.DEPT
 AND CLA.CLASS = SUB.CLASS
/

COMMENT ON TABLE V_SUBCLASS IS 'This view will be used to display the subclass information using a security policy to filter user access.'
/
COMMENT ON COLUMN V_SUBCLASS."DEPT" IS 'Contains the department number of which the subclass belongs to.';
/
COMMENT ON COLUMN V_SUBCLASS."CLASS" IS 'Contains the class number of which the subclass belongs to.';
/
COMMENT ON COLUMN V_SUBCLASS."SUBCLASS" IS 'Contains the number which uniquely identifies the subclass within a department and class.';
/
COMMENT ON COLUMN V_SUBCLASS."SUB_NAME" IS 'Contains the translated name of the subclass.';
/
