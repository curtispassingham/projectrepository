--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE DROPPED:             INVC_DEFAULT_TOLERANCE
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       TABLE DROP
--------------------------------------
PROMPT dropping 'INVC_DEFAULT_TOLERANCE'

DECLARE
  FS_InvoiceDefaultTolerance number := 0;
BEGIN
  SELECT count(*) INTO FS_InvoiceDefaultTolerance
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'INVC_DEFAULT_TOLERANCE';

  if (FS_InvoiceDefaultTolerance != 0) then
      execute immediate 'DROP TABLE INVC_DEFAULT_TOLERANCE CASCADE CONSTRAINTS';
  end if;
end;
/
