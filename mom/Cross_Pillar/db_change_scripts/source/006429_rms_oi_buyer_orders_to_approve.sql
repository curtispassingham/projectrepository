--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_BUYER_ORDERS_TO_APPROVE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_BUYER_ORDERS_TO_APPROVE'
CREATE TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE
 (SESSION_ID NUMBER(15) NOT NULL,
  ORDER_NO NUMBER(12) NOT NULL,
  SUP_NAME VARCHAR2(240 ),
  NOT_BEFORE_DATE DATE,
  NOT_AFTER_DATE DATE,
  OTB_EOW_DATE DATE,
  TOTAL_COST NUMBER(20,4),
  TOTAL_RETAIL NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 ) NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  COMMENT_DESC VARCHAR2(2000 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE is 'This table is used to display unapproved orders in the the Buyer Dashboard. '
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.SESSION_ID is 'Uniquely identifies a user session opening the report.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.ORDER_NO is 'Holds the purchase order no that meets the filter criteria.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.SUP_NAME is 'Holds the translated name of the order supplier.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.NOT_BEFORE_DATE is 'Holds the not before date of the purchase order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.NOT_AFTER_DATE is 'Holds the not after date of the purchase order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.OTB_EOW_DATE is 'Holds the end of week date of the open to buy.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.TOTAL_COST is 'Holds the total cost of the order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.TOTAL_RETAIL is 'Holds the total retail of the order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.CURRENCY_CODE is 'Holds the currency code the total_cost and total_retail are expressed in.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.CREATE_ID is 'Holds the ID of the person created the purchase order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.COMMENT_DESC is 'Holds any user comment.'
/

