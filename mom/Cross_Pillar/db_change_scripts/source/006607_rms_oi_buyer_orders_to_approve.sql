--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_BUYER_ORDERS_TO_APPROVE'
ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD STATUS VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.STATUS is 'Purchase order status'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD STATUS_DESC VARCHAR2 (25 ) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.STATUS_DESC is 'This field contains a description for the PO status.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD SUPPLIER NUMBER (10,0) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.SUPPLIER is 'Contains the vendor number who will provide the merchandise specified in the order.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD DEPT NUMBER (4,0) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.DEPT is 'Contains the department number for orders limited to a single department and will be Null for orders involving items in more than one department.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD DEPT_NAME VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.DEPT_NAME is 'Department name'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD PO_TYPE VARCHAR2 (4 ) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.PO_TYPE is 'Contains the value associated with the PO_TYPE for the order.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD PO_TYPE_DESC VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.PO_TYPE_DESC is 'This field contains a description for the specific order type.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD MASTER_PO_NO NUMBER (12,0) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.MASTER_PO_NO is 'A number that is used to reference the master order number from which child records were created.  Orders with the same Master_PO number are grouped together using the same delivery date. '
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD WRITTEN_DATE DATE  NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.WRITTEN_DATE is 'Contains the date the order was created within the system.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD TOTAL_UNITS NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.TOTAL_UNITS is 'Contains the total number of items ordered'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD MULTIPLE_UOM_IND VARCHAR2 (1 ) DEFAULT 'N' NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.MULTIPLE_UOM_IND is 'Indicates if the order has more than one unit of measure.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD MARKUP_PERCENT NUMBER (5,2) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.MARKUP_PERCENT is 'Markup is the difference between the cost of a good or service and its selling price'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD PO_LAST_UPDATE_DATETIME DATE  NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.PO_LAST_UPDATE_DATETIME is 'Holds the date time stamp of the most recent update by the last_update_id.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE ADD PO_LAST_UPDATE_ID VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.PO_LAST_UPDATE_ID is 'Holds the Oracle user-id of the user who most recently updated this record.'
/

