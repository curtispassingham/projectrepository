--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'FUNCTIONAL_CONFIG_OPTIONS'

COMMENT ON COLUMN FUNCTIONAL_CONFIG_OPTIONS.ORG_UNIT_IND is 'When a company is divided into different ledgers or a company has operations in different countries, it may use different sets of books for each division. An org unit is a subdivision within a set of books which could represent a sales office, division, or department. Org units are not associated with legal entities. This column will be used to determine if organizational units are used within the system. Valid value is Y-Yes.'
/
