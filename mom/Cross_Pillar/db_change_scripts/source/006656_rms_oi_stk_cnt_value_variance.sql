--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_STK_CNT_VALUE_VARIANCE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_STK_CNT_VALUE_VARIANCE'
CREATE TABLE RMS_OI_STK_CNT_VALUE_VARIANCE
 (SESSION_ID NUMBER(15),
  CYCLE_COUNT NUMBER(8),
  CYCLE_COUNT_DESC VARCHAR2(250 ),
  STOCKTAKE_TYPE VARCHAR2(1 ),
  STOCKTAKE_DATE DATE,
  DEPT NUMBER(4),
  DEPT_NAME VARCHAR2(120 ),
  CLASS NUMBER(4),
  CLASS_NAME VARCHAR2(120 ),
  SUBCLASS NUMBER(4),
  SUBCLASS_NAME VARCHAR2(120 ),
  LOC NUMBER(10),
  LOC_TYPE VARCHAR2(1 ),
  LOC_NAME VARCHAR2(150 ),
  TOTAL_UNIT_VARIANCE_PCT NUMBER(20,10),
  OVER_UNIT_VARIANCE_PCT NUMBER(20,10),
  SHORT_UNIT_VARIANCE_PCT NUMBER(20,10),
  TOTAL_VARIANCE_PCT NUMBER(20,10),
  OVER_VARIANCE_PCT NUMBER(20,10),
  SHORT_VARIANCE_PCT NUMBER(20,10),
  BOOK_VALUE NUMBER(20,4),
  ACTUAL_VALUE NUMBER(20,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_STK_CNT_VALUE_VARIANCE is 'This table holds the information about Stock Count unit and value variances for the Stock Count Value Variance Report.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.SESSION_ID is 'Session id'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.CYCLE_COUNT is 'Stock count'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.CYCLE_COUNT_DESC is 'Stock count description'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.STOCKTAKE_TYPE is 'Type of stock count'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.STOCKTAKE_DATE is 'Date on which Stock count was taken'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.DEPT is 'Department number'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.DEPT_NAME is 'Department name'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.CLASS is 'Class number'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.CLASS_NAME is 'Class name'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.SUBCLASS is 'Subclass number'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.SUBCLASS_NAME is 'Subclass name'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.LOC is 'Location'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.LOC_TYPE is 'Location Type'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.LOC_NAME is 'Location Name'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.TOTAL_UNIT_VARIANCE_PCT is ' Total unit variance percent.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.OVER_UNIT_VARIANCE_PCT is 'Over unit variance percent.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.SHORT_UNIT_VARIANCE_PCT is 'Short unit variance percent.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.TOTAL_VARIANCE_PCT is 'Total variance percent.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.OVER_VARIANCE_PCT is 'Over variance percent.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.SHORT_VARIANCE_PCT is 'Short variance percent.'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.BOOK_VALUE is 'Book value for the count location/merchandise hierarchy'
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.ACTUAL_VALUE is 'Book value for the count location/merchandise hierarchy'
/


PROMPT Creating Index on 'RMS_OI_STK_CNT_VALUE_VARIANCE'
 CREATE INDEX RMS_OI_STK_CNT_VALUE_VAR_I1 on RMS_OI_STK_CNT_VALUE_VARIANCE
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

