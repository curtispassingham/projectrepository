--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_DEPT_OPTIONS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_DEPT_OPTIONS'
CREATE TABLE RMS_OI_DEPT_OPTIONS
 (DEPT NUMBER(4) NOT NULL,
  IC_TOLERANCE_QTY NUMBER(12,4) DEFAULT 0 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_DEPT_OPTIONS is 'This table holds department level configurations for the Dashboard Reports.'
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DEPT is 'The department.'
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_TOLERANCE_QTY is 'Defines a tolerance level of unexpected inventory for an item location to be shown in the Inventory Control Unexpected Inventory Report and the level of negative inventory for an item location to be shown in the Inventory Control Negative Inventory Report.'
/


PROMPT Creating Primary Key on 'RMS_OI_DEPT_OPTIONS'
ALTER TABLE RMS_OI_DEPT_OPTIONS
 ADD CONSTRAINT PK_RMS_OI_DEPT_OPTIONS PRIMARY KEY
  (DEPT
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

