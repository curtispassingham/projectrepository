--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       Dropping Table  POS_MONEY_ORD_HEAD   
-------------------------------------------
PROMPT DROPPING Table 'POS_MONEY_ORD_HEAD'
DECLARE
  L_pos_money_ord_head number := 0;
BEGIN
  SELECT count(*) INTO L_pos_money_ord_head
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'POS_MONEY_ORD_HEAD';

  if (L_pos_money_ord_head != 0) then
      execute immediate 'drop table POS_MONEY_ORD_HEAD';
  end if;
end;
/
