--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_SHRINKAGE_VARIANCE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_SHRINKAGE_VARIANCE'
CREATE TABLE RMS_OI_SHRINKAGE_VARIANCE
 (SESSION_ID NUMBER(15),
  REGION NUMBER(10),
  REGION_NAME VARCHAR2(150 ),
  DISTRICT NUMBER(10),
  DISTRICT_NAME VARCHAR2(150 ),
  LOC NUMBER(10),
  LOC_TYPE VARCHAR2(1 ),
  LOC_NAME VARCHAR2(150 ),
  DEPT NUMBER(4),
  DEPT_NAME VARCHAR2(120 ),
  CLASS NUMBER(4),
  CLASS_NAME VARCHAR2(120 ),
  SUBCLASS NUMBER(4),
  SUBCLASS_NAME VARCHAR2(120 ),
  BUDGETED_SHRINKAGE NUMBER(20,4),
  BUDGETED_SHRINKAGE_RATE NUMBER(20,4),
  ACTUAL_SHRINKAGE NUMBER(20,4),
  ACTUAL_SHRINKAGE_RATE NUMBER(20,4),
  TOTAL_SALES NUMBER(12,4),
  VARIANCE_PCT NUMBER(20,4),
  UNIT_AND_VALUE_COUNT NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_SHRINKAGE_VARIANCE is 'This table holds the information about shrinkage differences between the actual and budgeted shrinkage rate falling outside the defined tolerence for the Shrinkage Variance Report.'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.SESSION_ID is 'Session id'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.REGION is 'Region '
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.REGION_NAME is 'Region name'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.DISTRICT is 'District '
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.DISTRICT_NAME is 'District name'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.LOC is 'Location'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.LOC_TYPE is 'Location Type'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.LOC_NAME is 'Location Name'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.DEPT is 'Department number'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.DEPT_NAME is 'Department name'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.CLASS is 'Class number'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.CLASS_NAME is 'Class name'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.SUBCLASS is 'Subclass number'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.SUBCLASS_NAME is 'Subclass name'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.BUDGETED_SHRINKAGE is 'Budgeted shrinkage'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.BUDGETED_SHRINKAGE_RATE is 'Budgeted_Shrinkage rate'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.ACTUAL_SHRINKAGE is 'Actual shrinkage'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.ACTUAL_SHRINKAGE_RATE is 'Actual_Shrinkage rate'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.TOTAL_SALES is 'Total sales'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.VARIANCE_PCT is 'Variance pct'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.UNIT_AND_VALUE_COUNT is 'Unit_And_Value count'
/

COMMENT ON COLUMN RMS_OI_SHRINKAGE_VARIANCE.CURRENCY_CODE is 'Currency code'
/

PROMPT Creating Index 'RMS_OI_SHRINKAGE_VARIANCE_I1'
CREATE INDEX RMS_OI_SHRINKAGE_VARIANCE_I1 ON RMS_OI_SHRINKAGE_VARIANCE
(SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
