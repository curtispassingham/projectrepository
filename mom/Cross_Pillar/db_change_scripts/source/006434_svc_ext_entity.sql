--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-----------------------------------------------------
--       Modifying Table        SVC_EXT_ENTITY       
-----------------------------------------------------

PROMPT Modifying Table 'SVC_EXT_ENTITY'
ALTER TABLE SVC_EXT_ENTITY MODIFY ACTION NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

ALTER TABLE SVC_EXT_ENTITY MODIFY PROCESS$STATUS DEFAULT 'N' NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

ALTER TABLE SVC_EXT_ENTITY MODIFY BASE_RMS_TABLE NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.BASE_RMS_TABLE is 'The physical database table in RMS to be extended.'
/

ALTER TABLE SVC_EXT_ENTITY MODIFY CREATE_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.CREATE_ID is 'User who created the record.'
/

ALTER TABLE SVC_EXT_ENTITY MODIFY CREATE_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.CREATE_DATETIME is 'Date time when record was inserted.'
/

ALTER TABLE SVC_EXT_ENTITY MODIFY LAST_UPD_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.LAST_UPD_ID is 'User who last updated the record.'
/

ALTER TABLE SVC_EXT_ENTITY MODIFY LAST_UPD_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_EXT_ENTITY.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/

