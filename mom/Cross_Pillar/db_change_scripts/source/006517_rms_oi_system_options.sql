--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IC_UNEXP_INV_INCL_QTY
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IC_NEG_INV_INCLUDE
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_TOLERANCE_QTY NUMBER (12,4) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_TOLERANCE_QTY is 'Defines a tolerance level of unexpected inventory for an item location to be shown in the Inventory Control Unexpected Inventory Report and the level of negative inventory for an item location to be shown in the Inventory Control Negative Inventory Report.  This will be used when it the configuration is not defined at the dept level on RMS_OI_DEPT_OPTIONS.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_UNEXP_INV_WARN_COUNT NUMBER (10,0) DEFAULT 5 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_UNEXP_INV_WARN_COUNT is 'Controls the number of item/locs when exceeded causes the unexpected inventory report to be considered warning in the inventory control unexpected inventory report.'
/

