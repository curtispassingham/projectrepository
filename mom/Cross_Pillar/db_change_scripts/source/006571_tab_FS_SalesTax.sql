--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      TABLES to be DROPPED:    
							GEOCODE_STORE
							GEOCODE_TEMP
							GEOCODE_TXCDE
							DISTRICT_GEOCODES
							CITY_GEOCODES
							COUNTY_GEOCODES
							STATE_GEOCODES
							COUNTRY_GEOCODES
							PRODUCT_TAX_CODE
							TAX_RATES
							TAX_CODES
							TAX_CODE_TEMP
							TAX_JURISDICTIONS

*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TABLES
--------------------------------------
PROMPT DROPPING Table 'GEOCODE_STORE'
DECLARE
  L_table_exists1 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'GEOCODE_STORE';

if (L_table_exists1 != 0) then
      execute immediate 'DROP TABLE GEOCODE_STORE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'GEOCODE_TEMP'
DECLARE
  L_table_exists2 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists2
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'GEOCODE_TEMP';

if (L_table_exists2 != 0) then
      execute immediate 'DROP TABLE GEOCODE_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'GEOCODE_TXCDE'
DECLARE
  L_table_exists3 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists3
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'GEOCODE_TXCDE';

if (L_table_exists3 != 0) then
      execute immediate 'DROP TABLE GEOCODE_TXCDE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'DISTRICT_GEOCODES'
DECLARE
  L_table_exists4 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists4
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'DISTRICT_GEOCODES';

if (L_table_exists4 != 0) then
      execute immediate 'DROP TABLE DISTRICT_GEOCODES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'CITY_GEOCODES'
DECLARE
  L_table_exists5 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists5
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CITY_GEOCODES';

if (L_table_exists5 != 0) then
      execute immediate 'DROP TABLE CITY_GEOCODES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'COUNTY_GEOCODES'
DECLARE
  L_table_exists6 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists6
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'COUNTY_GEOCODES';

if (L_table_exists6 != 0) then
      execute immediate 'DROP TABLE COUNTY_GEOCODES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'STATE_GEOCODES'
DECLARE
  L_table_exists7 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists7
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STATE_GEOCODES';

if (L_table_exists7 != 0) then
      execute immediate 'DROP TABLE STATE_GEOCODES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'COUNTRY_GEOCODES'
DECLARE
  L_table_exists8 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists8
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'COUNTRY_GEOCODES';

if (L_table_exists8 != 0) then
      execute immediate 'DROP TABLE COUNTRY_GEOCODES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'PRODUCT_TAX_CODE'
DECLARE
  L_table_exists9 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists9
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'PRODUCT_TAX_CODE';

if (L_table_exists9 != 0) then
      execute immediate 'DROP TABLE PRODUCT_TAX_CODE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'TAX_RATES'
DECLARE
  L_table_exists10 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists10
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_RATES';

if (L_table_exists10 != 0) then
      execute immediate 'DROP TABLE TAX_RATES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'TAX_CODES'
DECLARE
  L_table_exists11 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists11
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_CODES';

if (L_table_exists11 != 0) then
      execute immediate 'DROP TABLE TAX_CODES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'TAX_CODE_TEMP'
DECLARE
  L_table_exists12 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists12
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_CODE_TEMP';

if (L_table_exists12 != 0) then
      execute immediate 'DROP TABLE TAX_CODE_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'TAX_JURISDICTIONS'
DECLARE
  L_table_exists13 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists13
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_JURISDICTIONS';

if (L_table_exists13 != 0) then
      execute immediate 'DROP TABLE TAX_JURISDICTIONS CASCADE CONSTRAINTS';
  end if;
end;
/
