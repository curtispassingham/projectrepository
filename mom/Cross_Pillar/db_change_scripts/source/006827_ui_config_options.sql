--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'UI_CONFIG_OPTIONS'
ALTER TABLE UI_CONFIG_OPTIONS ADD DEFAULT_ITEM_IMAGE VARCHAR (255 ) NULL
/

COMMENT ON COLUMN UI_CONFIG_OPTIONS.DEFAULT_ITEM_IMAGE is 'This holds default image name for item image and will be displayed in reports when the Item image is not found in ITEM_IMAGE table.'
/

