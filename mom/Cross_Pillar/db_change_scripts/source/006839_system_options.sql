--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Created:						 SYSTEM_OPTIONS
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
CREATE OR REPLACE FORCE VIEW SYSTEM_OPTIONS
 (AIP_IMPL_DATE
 ,AIP_IND
 ,AIP_VERS
 ,ALLOCATION_IMPL_DATE
 ,ALLOCATION_IND
 ,ALLOCATION_VERS
 ,ALLOC_METHOD
 ,APPLY_PROF_PRES_STOCK
 ,AUTO_APPROVE_CHILD_IND
 ,AUTO_EAN13_PREFIX
 ,AUTO_RCV_STORE
 ,BACKPOST_RCA_RUA_IND
 ,BASE_COUNTRY_ID
 ,BRACKET_COSTING_IND
 ,BUD_SHRINK_IND
 ,CALC_NEGATIVE_INCOME
 ,CALENDAR_454_IND
 ,CD_MODULUS
 ,CD_WEIGHT_1
 ,CD_WEIGHT_2
 ,CD_WEIGHT_3
 ,CD_WEIGHT_4
 ,CD_WEIGHT_5
 ,CD_WEIGHT_6
 ,CD_WEIGHT_7
 ,CD_WEIGHT_8
 ,CHECK_DIGIT_IND
 ,CLASS_LEVEL_VAT_IND
 ,CLOSE_MTH_WITH_OPN_CNT_IND
 ,CLOSE_OPEN_SHIP_DAYS
 ,COMP_LIST_DAYS
 ,COMP_PRICE_MONTHS
 ,CONSOLIDATION_IND
 ,CONTRACT_INACTIVE_MONTHS
 ,CONTRACT_IND
 ,COPY_PO_CURR_RATE
 ,COST_EVENT_HIST_DAYS
 ,COST_LEVEL
 ,COST_MONEY
 ,COST_OUT_STORAGE
 ,COST_OUT_STORAGE_MEAS
 ,COST_OUT_STORAGE_UOM
 ,COST_PRIOR_CREATE_DAYS
 ,COST_WH_STORAGE
 ,COST_WH_STORAGE_MEAS
 ,COST_WH_STORAGE_UOM
 ,CREDIT_MEMO_LEVEL
 ,CURRENCY_CODE
 ,CUST_ORDER_HISTORY_MONTHS
 ,CYCLE_COUNT_LAG_DAYS
 ,DAILY_SALES_DISC_MTHS
 ,DATA_INTEGRATION_LANG
 ,DATA_LEVEL_SECURITY_IND
 ,DATE_ENTRY
 ,DEAL_AGE_PRIORITY
 ,DEAL_HISTORY_MONTHS
 ,DEAL_LEAD_DAYS
 ,DEAL_TYPE_PRIORITY
 ,DEFAULT_ALLOC_CHRG_IND
 ,DEFAULT_CASE_NAME
 ,DEFAULT_DIMENSION_UOM
 ,DEFAULT_INNER_NAME
 ,DEFAULT_ORDER_TYPE
 ,DEFAULT_PACKING_METHOD
 ,DEFAULT_PALLET_NAME
 ,DEFAULT_SIZE_PROFILE
 ,DEFAULT_STANDARD_UOM
 ,DEFAULT_TAX_TYPE
 ,DEFAULT_UOP
 ,DEFAULT_VAT_REGION
 ,DEFAULT_WEIGHT_UOM
 ,DEPT_LEVEL_ORDERS
 ,DEPT_LEVEL_TRANSFERS
 ,DIFF_GROUP_MERCH_LEVEL_CODE
 ,DIFF_GROUP_ORG_LEVEL_CODE
 ,DISTRIBUTION_RULE
 ,DOMAIN_LEVEL
 ,DUPLICATE_RECEIVING_IND
 ,EDI_COST_OVERRIDE_IND
 ,EDI_DAILY_RPT_LAG
 ,EDI_REV_DAYS
 ,ELC_INCLUSIVE_IND_COMP_STORE
 ,ELC_INCLUSIVE_IND_WF_STORE
 ,ELC_IND
 ,EXPIRY_DELAY_PRE_ISSUE
 ,FDN_STG_RETENTION_DAYS
 ,FINANCIAL_AP
 ,FOB_TITLE_PASS
 ,FOB_TITLE_PASS_DESC
 ,FORECAST_IND
 ,FUTURE_COST_HISTORY_DAYS
 ,GEN_CONSIGNMENT_INVC_FREQ
 ,GEN_CON_INVC_ITM_SUP_LOC_IND
 ,GL_ROLLUP
 ,GROCERY_ITEMS_IND
 ,HTS_TRACKING_LEVEL
 ,HTS_UPLD_ORDER_STATUS
 ,IB_RESULTS_PURGE_DAYS
 ,IMAGE_PATH
 ,DEFAULT_ITEM_IMAGE
 ,IMPORT_HTS_DATE
 ,IMPORT_IND
 ,INCREASE_TSF_QTY_IND
 ,INTERCOMPANY_TRANSFER_BASIS
 ,INV_ADJ_MONTHS
 ,INV_HIST_LEVEL
 ,ITEM_HISTORY_MONTHS
 ,LATEST_SHIP_DAYS
 ,LC_EXP_DAYS
 ,LC_FORM_TYPE
 ,LC_TYPE
 ,LOC_ACTIVITY_IND
 ,LOC_CLOSE_HIST_MONTHS
 ,LOC_DLVRY_IND
 ,LOC_LIST_ORG_LEVEL_CODE
 ,LOC_TRAIT_ORG_LEVEL_CODE
 ,LOOK_AHEAD_DAYS
 ,MARGIN_IMPACT_HIST_RECS
 ,MAX_CUM_MARKON_PCT
 ,MAX_QTY_PCT_DEC
 ,MAX_SCALING_ITERATIONS
 ,MAX_WEEKS_SUPPLY
 ,MERCH_HIER_AUTO_GEN_IND
 ,MIN_CUM_MARKON_PCT
 ,MIN_QTY_PCT_DEC
 ,MULTI_CURRENCY_IND
 ,NUM_HIST_EXCHANGE_RATE
 ,NWP_IND
 ,NWP_RETENTION_PERIOD
 ,OMS_IND
 ,ORDER_BEFORE_DAYS
 ,ORDER_EXCH_IND
 ,ORDER_HISTORY_MONTHS
 ,ORDER_HTS_IND
 ,ORD_APPR_AMT_CODE
 ,ORD_APPR_CLOSE_DELAY
 ,ORD_AUTO_CLOSE_PART_RCVD_IND
 ,ORD_PART_RCVD_CLOSE_DELAY
 ,ORD_WORKSHEET_CLEAN_UP_DELAY
 ,ORG_UNIT_IND
 ,OTB_SYSTEM_IND
 ,PRICE_HIST_RETENTION_DAYS
 ,PROC_DATA_RETENTION_DAYS
 ,RAC_RTV_TSF_IND
 ,RCV_COST_ADJ_TYPE
 ,RECLASS_APPR_ORDER_IND
 ,REDIST_FACTOR
 ,REIM_IMPL_DATE
 ,REIM_IND
 ,REIM_VERS
 ,REJECT_STORE_ORD_IND
 ,REPL_ATTR_HIST_RETENTION_WEEKS
 ,REPL_ORDER_DAYS
 ,REPL_ORDER_HISTORY_DAYS
 ,REPL_RESULTS_PURGE_DAYS
 ,RESA_IMPL_DATE
 ,RESA_VERS
 ,RETENTION_OF_REJECTED_COST_CHG
 ,RETN_SCHED_UPD_DAYS
 ,RMS_IMPL_DATE
 ,RMS_VERS
 ,ROUND_LVL
 ,ROUND_TO_CASE_PCT
 ,ROUND_TO_INNER_PCT
 ,ROUND_TO_LAYER_PCT
 ,ROUND_TO_PALLET_PCT
 ,RPM_IMPL_DATE
 ,RPM_IND
 ,RPM_VERS
 ,RTM_SIMPLIFIED_IND
 ,RTM_TRNSPRT_OBL_ALLOC_METHOD
 ,RTV_NAD_LEAD_TIME
 ,RTV_ORDER_HISTORY_MONTHS
 ,RTV_UNIT_COST_IND
 ,RWMS_IMPL_DATE
 ,RWMS_IND
 ,RWMS_VERS
 ,SALES_AUDIT_IND
 ,SEASON_MERCH_LEVEL_CODE
 ,SEASON_ORG_LEVEL_CODE
 ,SHIP_RCV_STORE
 ,SHIP_RCV_WH
 ,SHIP_SCHED_HISTORY_MTHS
 ,SIM_FORCE_CLOSE_IND
 ,SIM_IMPL_DATE
 ,SIM_IND
 ,SIM_VERS
 ,SKULIST_ORG_LEVEL_CODE
 ,SOFT_CONTRACT_IND
 ,SS_AUTO_CLOSE_DAYS
 ,STAKE_AUTO_PROCESSING
 ,STAKE_COST_VARIANCE
 ,STAKE_LOCKOUT_DAYS
 ,STAKE_RETAIL_VARIANCE
 ,STAKE_REVIEW_DAYS
 ,STAKE_UNIT_VARIANCE
 ,START_OF_HALF_MONTH
 ,STD_AV_IND
 ,STKLDGR_VAT_INCL_RETL_IND
 ,STOCK_LEDGER_TIME_LEVEL_CODE
 ,STORAGE_TYPE
 ,STORE_ORDERS_PURGE_DAYS
 ,STORE_PACK_COMP_RCV_IND
 ,SUPPLIER_SITES_IND
 ,SUPP_PART_AUTO_GEN_IND
 ,SW_AUTO_CLOSE_DAYS
 ,TABLE_OWNER
 ,TARGET_ROI
 ,TICKET_TYPE_MERCH_LEVEL_CODE
 ,TICKET_TYPE_ORG_LEVEL_CODE
 ,TIME_DISPLAY
 ,TIME_ENTRY
 ,TRAN_DATA_RETAINED_DAYS_NO
 ,TSF_AUTO_CLOSE_STORE
 ,TSF_AUTO_CLOSE_WH
 ,TSF_CLOSE_OVERDUE
 ,TSF_FORCE_CLOSE_IND
 ,TSF_HISTORY_MTHS
 ,TSF_MD_STORE_TO_STORE_SND_RCV
 ,TSF_MD_STORE_TO_WH_SND_RCV
 ,TSF_MD_WH_TO_STORE_SND_RCV
 ,TSF_MD_WH_TO_WH_SND_RCV
 ,TSF_MRT_RETENTION_DAYS
 ,TSF_OVER_RECEIPT_IND
 ,TSF_PRICE_EXCEED_WAC_IND
 ,UDA_MERCH_LEVEL_CODE
 ,UDA_ORG_LEVEL_CODE
 ,UPDATE_ITEM_HTS_IND
 ,UPDATE_ORDER_HTS_IND
 ,WAC_RECALC_ADJ_IND
 ,WF_DEFAULT_WH
 ,WF_HISTORY_MONTHS
 ,WF_NON_STOCK_SALES_HIST_DAYS
 ,WF_ORDER_LEAD_DAYS
 ,WH_CROSS_LINK_IND
 ,WH_STORE_ASSIGN_TYPE
 ,WRONG_ST_RECEIPT_IND
 ,WS_AUTO_CLOSE_DAYS
 ,WW_AUTO_CLOSE_DAYS)
 AS SELECT PV.AIP_IMPL_DATE AIP_IMPL_DATE
          ,PC.AIP_IND AIP_IND
          ,PV.AIP_VERS AIP_VERS
          ,PV.ALLOCATION_IMPL_DATE ALLOCATION_IMPL_DATE
          ,PC.ALLOCATION_IND ALLOCATION_IND
          ,PV.ALLOCATION_VERS ALLOCATION_VERS
          ,IM.ALLOC_METHOD ALLOC_METHOD
          ,IM.APPLY_PROF_PRES_STOCK APPLY_PROF_PRES_STOCK
          ,FO.AUTO_APPROVE_CHILD_IND AUTO_APPROVE_CHILD_IND
          ,FO.AUTO_EAN13_PREFIX AUTO_EAN13_PREFIX
          ,IM.AUTO_RCV_STORE AUTO_RCV_STORE
          ,PO.BACKPOST_RCA_RUA_IND BACKPOST_RCA_RUA_IND
          ,SY.BASE_COUNTRY_ID BASE_COUNTRY_ID
          ,FO.BRACKET_COSTING_IND BRACKET_COSTING_IND
          ,FI.BUD_SHRINK_IND BUD_SHRINK_IND
          ,PO.CALC_NEGATIVE_INCOME CALC_NEGATIVE_INCOME
          ,SY.CALENDAR_454_IND CALENDAR_454_IND
          ,DF.CD_MODULUS CD_MODULUS
          ,DF.CD_WEIGHT_1 CD_WEIGHT_1
          ,DF.CD_WEIGHT_2 CD_WEIGHT_2
          ,DF.CD_WEIGHT_3 CD_WEIGHT_3
          ,DF.CD_WEIGHT_4 CD_WEIGHT_4
          ,DF.CD_WEIGHT_5 CD_WEIGHT_5
          ,DF.CD_WEIGHT_6 CD_WEIGHT_6
          ,DF.CD_WEIGHT_7 CD_WEIGHT_7
          ,DF.CD_WEIGHT_8 CD_WEIGHT_8
          ,DF.CHECK_DIGIT_IND CHECK_DIGIT_IND
          ,LO.CLASS_LEVEL_VAT_IND CLASS_LEVEL_VAT_IND
          ,FI.CLOSE_MTH_WITH_OPN_CNT_IND CLOSE_MTH_WITH_OPN_CNT_IND
          ,IM.CLOSE_OPEN_SHIP_DAYS CLOSE_OPEN_SHIP_DAYS
          ,PU.COMP_LIST_DAYS COMP_LIST_DAYS
          ,PU.COMP_PRICE_MONTHS COMP_PRICE_MONTHS
          ,SY.CONSOLIDATION_IND CONSOLIDATION_IND
          ,PU.CONTRACT_INACTIVE_MONTHS CONTRACT_INACTIVE_MONTHS
          ,FC.CONTRACT_IND CONTRACT_IND
          ,PO.COPY_PO_CURR_RATE COPY_PO_CURR_RATE
          ,FO.COST_EVENT_HIST_DAYS COST_EVENT_HIST_DAYS
          ,PO.COST_LEVEL COST_LEVEL
          ,IM.COST_MONEY COST_MONEY
          ,IM.COST_OUT_STORAGE COST_OUT_STORAGE
          ,IM.COST_OUT_STORAGE_MEAS COST_OUT_STORAGE_MEAS
          ,IM.COST_OUT_STORAGE_UOM COST_OUT_STORAGE_UOM
          ,FO.COST_PRIOR_CREATE_DAYS COST_PRIOR_CREATE_DAYS
          ,IM.COST_WH_STORAGE COST_WH_STORAGE
          ,IM.COST_WH_STORAGE_MEAS COST_WH_STORAGE_MEAS
          ,IM.COST_WH_STORAGE_UOM COST_WH_STORAGE_UOM
          ,PO.CREDIT_MEMO_LEVEL CREDIT_MEMO_LEVEL
          ,SY.CURRENCY_CODE CURRENCY_CODE
          ,PU.CUST_ORDER_HISTORY_MONTHS CUST_ORDER_HISTORY_MONTHS
          ,IT.CYCLE_COUNT_LAG_DAYS CYCLE_COUNT_LAG_DAYS
          ,PU.DAILY_SALES_DISC_MTHS DAILY_SALES_DISC_MTHS
          ,SY.DATA_INTEGRATION_LANG DATA_INTEGRATION_LANG
          ,SC.DATA_LEVEL_SECURITY_IND DATA_LEVEL_SECURITY_IND
          ,UC.DATE_ENTRY DATE_ENTRY
          ,PO.DEAL_AGE_PRIORITY DEAL_AGE_PRIORITY
          ,PU.DEAL_HISTORY_MONTHS DEAL_HISTORY_MONTHS
          ,PO.DEAL_LEAD_DAYS DEAL_LEAD_DAYS
          ,PO.DEAL_TYPE_PRIORITY DEAL_TYPE_PRIORITY
          ,IM.DEFAULT_ALLOC_CHRG_IND DEFAULT_ALLOC_CHRG_IND
          ,DU.DEFAULT_CASE_NAME DEFAULT_CASE_NAME
          ,DF.DEFAULT_DIMENSION_UOM DEFAULT_DIMENSION_UOM
          ,DU.DEFAULT_INNER_NAME DEFAULT_INNER_NAME
          ,IM.DEFAULT_ORDER_TYPE DEFAULT_ORDER_TYPE
          ,FO.DEFAULT_PACKING_METHOD DEFAULT_PACKING_METHOD
          ,DU.DEFAULT_PALLET_NAME DEFAULT_PALLET_NAME
          ,IM.DEFAULT_SIZE_PROFILE DEFAULT_SIZE_PROFILE
          ,DF.DEFAULT_STANDARD_UOM DEFAULT_STANDARD_UOM
          ,LO.DEFAULT_TAX_TYPE DEFAULT_TAX_TYPE
          ,DF.DEFAULT_UOP DEFAULT_UOP
          ,LO.DEFAULT_VAT_REGION DEFAULT_VAT_REGION
          ,DF.DEFAULT_WEIGHT_UOM DEFAULT_WEIGHT_UOM
          ,PO.DEPT_LEVEL_ORDERS DEPT_LEVEL_ORDERS
          ,IM.DEPT_LEVEL_TRANSFERS DEPT_LEVEL_TRANSFERS
          ,SC.DIFF_GROUP_MERCH_LEVEL_CODE DIFF_GROUP_MERCH_LEVEL_CODE
          ,SC.DIFF_GROUP_ORG_LEVEL_CODE DIFF_GROUP_ORG_LEVEL_CODE
          ,IM.DISTRIBUTION_RULE DISTRIBUTION_RULE
          ,FO.DOMAIN_LEVEL DOMAIN_LEVEL
          ,IM.DUPLICATE_RECEIVING_IND DUPLICATE_RECEIVING_IND
          ,PO.EDI_COST_OVERRIDE_IND EDI_COST_OVERRIDE_IND
          ,PU.EDI_DAILY_RPT_LAG EDI_DAILY_RPT_LAG
          ,PU.EDI_REV_DAYS EDI_REV_DAYS
          ,FI.ELC_INCLUSIVE_IND_COMP_STORE ELC_INCLUSIVE_IND_COMP_STORE
          ,FI.ELC_INCLUSIVE_IND_WF_STORE ELC_INCLUSIVE_IND_WF_STORE
          ,FC.ELC_IND ELC_IND
          ,PO.EXPIRY_DELAY_PRE_ISSUE EXPIRY_DELAY_PRE_ISSUE
          ,PU.FDN_STG_RETENTION_DAYS FDN_STG_RETENTION_DAYS
          ,PC.FINANCIAL_AP FINANCIAL_AP
          ,RU.FOB_TITLE_PASS FOB_TITLE_PASS
          ,RU.FOB_TITLE_PASS_DESC FOB_TITLE_PASS_DESC
          ,PC.FORECAST_IND FORECAST_IND
          ,PU.FUTURE_COST_HISTORY_DAYS FUTURE_COST_HISTORY_DAYS
          ,PO.GEN_CONSIGNMENT_INVC_FREQ GEN_CONSIGNMENT_INVC_FREQ
          ,PO.GEN_CON_INVC_ITM_SUP_LOC_IND GEN_CON_INVC_ITM_SUP_LOC_IND
          ,FI.GL_ROLLUP GL_ROLLUP
          ,FO.GROCERY_ITEMS_IND GROCERY_ITEMS_IND
          ,RU.HTS_TRACKING_LEVEL HTS_TRACKING_LEVEL
          ,RU.HTS_UPLD_ORDER_STATUS HTS_UPLD_ORDER_STATUS
          ,PU.IB_RESULTS_PURGE_DAYS IB_RESULTS_PURGE_DAYS
          ,UC.IMAGE_PATH IMAGE_PATH
          ,UC.DEFAULT_ITEM_IMAGE DEFAULT_ITEM_IMAGE
          ,RU.IMPORT_HTS_DATE IMPORT_HTS_DATE
          ,FC.IMPORT_IND IMPORT_IND
          ,IM.INCREASE_TSF_QTY_IND INCREASE_TSF_QTY_IND
          ,IM.INTERCOMPANY_TRANSFER_BASIS INTERCOMPANY_TRANSFER_BASIS
          ,PU.INV_ADJ_MONTHS INV_ADJ_MONTHS
          ,IM.INV_HIST_LEVEL INV_HIST_LEVEL
          ,PU.ITEM_HISTORY_MONTHS ITEM_HISTORY_MONTHS
          ,PO.LATEST_SHIP_DAYS LATEST_SHIP_DAYS
          ,RU.LC_EXP_DAYS LC_EXP_DAYS
          ,RU.LC_FORM_TYPE LC_FORM_TYPE
          ,RU.LC_TYPE LC_TYPE
          ,IM.LOC_ACTIVITY_IND LOC_ACTIVITY_IND
          ,PU.LOC_CLOSE_HIST_MONTHS LOC_CLOSE_HIST_MONTHS
          ,IM.LOC_DLVRY_IND LOC_DLVRY_IND
          ,SC.LOC_LIST_ORG_LEVEL_CODE LOC_LIST_ORG_LEVEL_CODE
          ,SC.LOC_TRAIT_ORG_LEVEL_CODE LOC_TRAIT_ORG_LEVEL_CODE
          ,IM.LOOK_AHEAD_DAYS LOOK_AHEAD_DAYS
          ,FI.MARGIN_IMPACT_HIST_RECS MARGIN_IMPACT_HIST_RECS
          ,FI.MAX_CUM_MARKON_PCT MAX_CUM_MARKON_PCT
          ,UC.MAX_QTY_PCT_DEC MAX_QTY_PCT_DEC
          ,IM.MAX_SCALING_ITERATIONS MAX_SCALING_ITERATIONS
          ,IM.MAX_WEEKS_SUPPLY MAX_WEEKS_SUPPLY
          ,FO.MERCH_HIER_AUTO_GEN_IND MERCH_HIER_AUTO_GEN_IND
          ,FI.MIN_CUM_MARKON_PCT MIN_CUM_MARKON_PCT
          ,UC.MIN_QTY_PCT_DEC MIN_QTY_PCT_DEC
          ,SY.MULTI_CURRENCY_IND MULTI_CURRENCY_IND
          ,UC.NUM_HIST_EXCHANGE_RATE NUM_HIST_EXCHANGE_RATE
          ,LO.NWP_IND NWP_IND
          ,PU.NWP_RETENTION_PERIOD NWP_RETENTION_PERIOD
          ,PC.OMS_IND OMS_IND
          ,PO.ORDER_BEFORE_DAYS ORDER_BEFORE_DAYS
          ,PO.ORDER_EXCH_IND ORDER_EXCH_IND
          ,PU.ORDER_HISTORY_MONTHS ORDER_HISTORY_MONTHS
          ,RU.ORDER_HTS_IND ORDER_HTS_IND
          ,PO.ORD_APPR_AMT_CODE ORD_APPR_AMT_CODE
          ,PO.ORD_APPR_CLOSE_DELAY ORD_APPR_CLOSE_DELAY
          ,PO.ORD_AUTO_CLOSE_PART_RCVD_IND ORD_AUTO_CLOSE_PART_RCVD_IND
          ,PO.ORD_PART_RCVD_CLOSE_DELAY ORD_PART_RCVD_CLOSE_DELAY
          ,IM.ORD_WORKSHEET_CLEAN_UP_DELAY ORD_WORKSHEET_CLEAN_UP_DELAY
          ,FC.ORG_UNIT_IND ORG_UNIT_IND
          ,PO.OTB_SYSTEM_IND OTB_SYSTEM_IND
          ,PU.PRICE_HIST_RETENTION_DAYS PRICE_HIST_RETENTION_DAYS
          ,PU.PROC_DATA_RETENTION_DAYS PROC_DATA_RETENTION_DAYS
          ,IM.RAC_RTV_TSF_IND RAC_RTV_TSF_IND
          ,PO.RCV_COST_ADJ_TYPE RCV_COST_ADJ_TYPE
          ,PO.RECLASS_APPR_ORDER_IND RECLASS_APPR_ORDER_IND
          ,PO.REDIST_FACTOR REDIST_FACTOR
          ,PV.REIM_IMPL_DATE REIM_IMPL_DATE
          ,PC.REIM_IND REIM_IND
          ,PV.REIM_VERS REIM_VERS
          ,IM.REJECT_STORE_ORD_IND REJECT_STORE_ORD_IND
          ,PU.REPL_ATTR_HIST_RETENTION_WEEKS REPL_ATTR_HIST_RETENTION_WEEKS
          ,IM.REPL_ORDER_DAYS REPL_ORDER_DAYS
          ,PU.REPL_ORDER_HISTORY_DAYS REPL_ORDER_HISTORY_DAYS
          ,PU.REPL_RESULTS_PURGE_DAYS REPL_RESULTS_PURGE_DAYS
          ,PV.RESA_IMPL_DATE RESA_IMPL_DATE
          ,PV.RESA_VERS RESA_VERS
          ,PU.RETENTION_OF_REJECTED_COST_CHG RETENTION_OF_REJECTED_COST_CHG
          ,PU.RETN_SCHED_UPD_DAYS RETN_SCHED_UPD_DAYS
          ,PV.RMS_IMPL_DATE RMS_IMPL_DATE
          ,PV.RMS_VERS RMS_VERS
          ,FO.ROUND_LVL ROUND_LVL
          ,FO.ROUND_TO_CASE_PCT ROUND_TO_CASE_PCT
          ,FO.ROUND_TO_INNER_PCT ROUND_TO_INNER_PCT
          ,FO.ROUND_TO_LAYER_PCT ROUND_TO_LAYER_PCT
          ,FO.ROUND_TO_PALLET_PCT ROUND_TO_PALLET_PCT
          ,PV.RPM_IMPL_DATE RPM_IMPL_DATE
          ,FC.RPM_IND RPM_IND
          ,PV.RPM_VERS RPM_VERS
          ,RU.RTM_SIMPLIFIED_IND RTM_SIMPLIFIED_IND
          ,RU.RTM_TRNSPRT_OBL_ALLOC_METHOD RTM_TRNSPRT_OBL_ALLOC_METHOD
          ,IM.RTV_NAD_LEAD_TIME RTV_NAD_LEAD_TIME
          ,PU.RTV_ORDER_HISTORY_MONTHS RTV_ORDER_HISTORY_MONTHS
          ,IM.RTV_UNIT_COST_IND RTV_UNIT_COST_IND
          ,PV.RWMS_IMPL_DATE RWMS_IMPL_DATE
          ,PC.RWMS_IND RWMS_IND
          ,PV.RWMS_VERS RWMS_VERS
          ,PC.SALES_AUDIT_IND SALES_AUDIT_IND
          ,SC.SEASON_MERCH_LEVEL_CODE SEASON_MERCH_LEVEL_CODE
          ,SC.SEASON_ORG_LEVEL_CODE SEASON_ORG_LEVEL_CODE
          ,IM.SHIP_RCV_STORE SHIP_RCV_STORE
          ,IM.SHIP_RCV_WH SHIP_RCV_WH
          ,PU.SHIP_SCHED_HISTORY_MTHS SHIP_SCHED_HISTORY_MTHS
          ,IM.SIM_FORCE_CLOSE_IND SIM_FORCE_CLOSE_IND
          ,PV.SIM_IMPL_DATE SIM_IMPL_DATE
          ,PC.SIM_IND SIM_IND
          ,PV.SIM_VERS SIM_VERS
          ,SC.SKULIST_ORG_LEVEL_CODE SKULIST_ORG_LEVEL_CODE
          ,PO.SOFT_CONTRACT_IND SOFT_CONTRACT_IND
          ,IM.SS_AUTO_CLOSE_DAYS SS_AUTO_CLOSE_DAYS
          ,IT.STAKE_AUTO_PROCESSING STAKE_AUTO_PROCESSING
          ,IT.STAKE_COST_VARIANCE STAKE_COST_VARIANCE
          ,IT.STAKE_LOCKOUT_DAYS STAKE_LOCKOUT_DAYS
          ,IT.STAKE_RETAIL_VARIANCE STAKE_RETAIL_VARIANCE
          ,IT.STAKE_REVIEW_DAYS STAKE_REVIEW_DAYS
          ,IT.STAKE_UNIT_VARIANCE STAKE_UNIT_VARIANCE
          ,FI.START_OF_HALF_MONTH START_OF_HALF_MONTH
          ,FI.STD_AV_IND STD_AV_IND
          ,FI.STKLDGR_VAT_INCL_RETL_IND STKLDGR_VAT_INCL_RETL_IND
          ,FI.STOCK_LEDGER_TIME_LEVEL_CODE STOCK_LEDGER_TIME_LEVEL_CODE
          ,IM.STORAGE_TYPE STORAGE_TYPE
          ,PU.STORE_ORDERS_PURGE_DAYS STORE_ORDERS_PURGE_DAYS
          ,IM.STORE_PACK_COMP_RCV_IND STORE_PACK_COMP_RCV_IND
          ,FC.SUPPLIER_SITES_IND SUPPLIER_SITES_IND
          ,FO.SUPP_PART_AUTO_GEN_IND SUPP_PART_AUTO_GEN_IND
          ,IM.SW_AUTO_CLOSE_DAYS SW_AUTO_CLOSE_DAYS
          ,SY.TABLE_OWNER TABLE_OWNER
          ,IM.TARGET_ROI TARGET_ROI
          ,SC.TICKET_TYPE_MERCH_LEVEL_CODE TICKET_TYPE_MERCH_LEVEL_CODE
          ,SC.TICKET_TYPE_ORG_LEVEL_CODE TICKET_TYPE_ORG_LEVEL_CODE
          ,UC.TIME_DISPLAY TIME_DISPLAY
          ,UC.TIME_ENTRY TIME_ENTRY
          ,PU.TRAN_DATA_RETAINED_DAYS_NO TRAN_DATA_RETAINED_DAYS_NO
          ,IM.TSF_AUTO_CLOSE_STORE TSF_AUTO_CLOSE_STORE
          ,IM.TSF_AUTO_CLOSE_WH TSF_AUTO_CLOSE_WH
          ,IM.TSF_CLOSE_OVERDUE TSF_CLOSE_OVERDUE
          ,IM.TSF_FORCE_CLOSE_IND TSF_FORCE_CLOSE_IND
          ,PU.TSF_HISTORY_MTHS TSF_HISTORY_MTHS
          ,IM.TSF_MD_STORE_TO_STORE_SND_RCV TSF_MD_STORE_TO_STORE_SND_RCV
          ,IM.TSF_MD_STORE_TO_WH_SND_RCV TSF_MD_STORE_TO_WH_SND_RCV
          ,IM.TSF_MD_WH_TO_STORE_SND_RCV TSF_MD_WH_TO_STORE_SND_RCV
          ,IM.TSF_MD_WH_TO_WH_SND_RCV TSF_MD_WH_TO_WH_SND_RCV
          ,PU.TSF_MRT_RETENTION_DAYS TSF_MRT_RETENTION_DAYS
          ,IM.TSF_OVER_RECEIPT_IND TSF_OVER_RECEIPT_IND
          ,IM.TSF_PRICE_EXCEED_WAC_IND TSF_PRICE_EXCEED_WAC_IND
          ,SC.UDA_MERCH_LEVEL_CODE UDA_MERCH_LEVEL_CODE
          ,SC.UDA_ORG_LEVEL_CODE UDA_ORG_LEVEL_CODE
          ,RU.UPDATE_ITEM_HTS_IND UPDATE_ITEM_HTS_IND
          ,RU.UPDATE_ORDER_HTS_IND UPDATE_ORDER_HTS_IND
          ,PO.WAC_RECALC_ADJ_IND WAC_RECALC_ADJ_IND
          ,IM.WF_DEFAULT_WH WF_DEFAULT_WH
          ,PU.WF_HISTORY_MONTHS WF_HISTORY_MONTHS
          ,PU.WF_NON_STOCK_SALES_HIST_DAYS WF_NON_STOCK_SALES_HIST_DAYS
          ,IM.WF_ORDER_LEAD_DAYS WF_ORDER_LEAD_DAYS
          ,IM.WH_CROSS_LINK_IND WH_CROSS_LINK_IND
          ,FO.WH_STORE_ASSIGN_TYPE WH_STORE_ASSIGN_TYPE
          ,IM.WRONG_ST_RECEIPT_IND WRONG_ST_RECEIPT_IND
          ,IM.WS_AUTO_CLOSE_DAYS WS_AUTO_CLOSE_DAYS
          ,IM.WW_AUTO_CLOSE_DAYS WW_AUTO_CLOSE_DAYS
FROM SYSTEM_CONFIG_OPTIONS SY
    ,SECURITY_CONFIG_OPTIONS SC
    ,FUNCTIONAL_CONFIG_OPTIONS FC
    ,DEFAULT_FUNC_CONFIG_OPTIONS DF
    ,UI_CONFIG_OPTIONS UC
    ,DEFAULT_UI_CONFIG_OPTIONS DU
    ,PURGE_CONFIG_OPTIONS PU
    ,LOCALIZATION_CONFIG_OPTIONS LO
    ,FOUNDATION_UNIT_OPTIONS FO
    ,PROCUREMENT_UNIT_OPTIONS PO
    ,INV_MOVE_UNIT_OPTIONS IM
    ,INV_TRACK_UNIT_OPTIONS IT
    ,FINANCIAL_UNIT_OPTIONS FI
    ,RTM_UNIT_OPTIONS RU
    ,PRODUCT_CONFIG_OPTIONS PC
    ,PRODUCT_VERS_CONFIG_OPTIONS PV
/


COMMENT ON COLUMN SYSTEM_OPTIONS.AIP_IMPL_DATE IS 'Implementation date of the current version of AIP (Advanced Inventory Planning) product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.AIP_IND IS 'The Advance Inventory Planning (AIP) indicator determines whether RMS is integrated with the Oracle Retail AIP application.  It also drives some different behavior in RMS, such as requiring a primary case type for item and restricting the number of simple packs of the same quantity that can be created for an item.  Valid values are ''Y'' or ''N'', with the default value if null of ''N''.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.AIP_VERS IS 'Version number of AIP installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ALLOCATION_IMPL_DATE IS 'Implementation date of the current version of Allocation product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ALLOCATION_IND IS 'The Allocation indicator determines if RMS is integrated with the Oracle Retail Allocation application.  Valid values are ''Y'' or ''N'', with the default value if null of ''N''.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ALLOCATION_VERS IS 'Version number of Allocations installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ALLOC_METHOD IS 'The allocation method that will be used as the default for transfers and cross-dock purchase order allocations: Pro-rate (P), Allocation Quantity (A), or Custom (C).  Note: the Custom option is included for customization.  It does not drive any process in RMS if selected.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.APPLY_PROF_PRES_STOCK IS 'The Presentation Stock Size Profile indicator determines whether the size profile concept will be applied to the presentation stock. Size profile refers to the ratio derived out of historical sales figures to give an estimate of the number of items of different sizes that need to be allocated to the destination store and applies only to fashion items within Allocation. Valid values are Y and N . Default value is ''N''.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.AUTO_APPROVE_CHILD_IND IS 'Determines if the children of approved parents will be created automatically in approved status. If Y, children of approved parents will be created in approved status. This means that the item will immediately be available for any transactions in RMS.  If N, children of approved parents will be created in worksheet status. This means that the child items must be manually approved before the item is available for any transactions in RMS. Care should be taken when setting this value. If all critical item information for transaction level items can be inherited from the parent, Y is a possible setting. If transaction level items are unique and require different data (i.e. different suppliers, locations, etc.), N is the suggested setting.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.AUTO_EAN13_PREFIX IS 'If the system option has been set such that EANs are auto generated, then this system option will indicate the prefix that will be used, if populated.  This field must be either NULL or contain a 6-digit long prefix.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.AUTO_RCV_STORE IS 'Determines if RMS will automatically record the receipt of merchandise from a PO, warehouse allocation or transfer to a store based on notification from an external warehouse system of an incoming shipment. Valid values are Y and N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.BACKPOST_RCA_RUA_IND IS 'Determines if cost or unit adjustments will affect backposted deals processing, where any matched shipment after unmatched shipment will be used to calculate the correct on hand and adjust quantity for WAC calculation.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.BASE_COUNTRY_ID IS 'This column stores the base country code for the system. This field is validated from the COUNTRY table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.BRACKET_COSTING_IND IS 'Controls if an organization will allow vendors with bracket costing pricing structures.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.BUD_SHRINK_IND IS 'This column determines whether budgeted shrinkage will be used in the calculation of period ending inventory value in the stock ledger. If BUD_SHRINK_IND = N, then shrinkage will be calculated based on inventory adjustments (it is assumed that SIM/WMS would perform regular cycle counts to report the shrinkage). If BUD_SHRINK_IND = Y, then shrinkage will be calculated as a percentage of sales. Then when the Unit and Dollar stock count is performed, the estimated shrinkage will be corrected according the the count results.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CALC_NEGATIVE_INCOME IS 'The Calculate Negative Income for Deals indicator determines if negative income will be calculated in case of RTV and sales return transactions during deal period. Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CALENDAR_454_IND IS 'Determines whether the normal (Gregorian) or the 4-5-4 calendar is going to be used as the financial calendar in RMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_MODULUS IS 'Indicates the modulus number used in the formula for check digit calculations in RMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_1 IS 'Check Digit Weight 1 is the value to be used as a weight for the check digit calculation in the rightmost position.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_2 IS 'Check Digit Weight 2 is the value to be used as a weight for the check digit calculation in the 2nd digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_3 IS 'Check Digit Weight 3 is the value to be used as a weight for the check digit calculation in the 3rd digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_4 IS 'Check Digit Weight 4 is the value to be used as a weight for the check digit calculation in the 4th digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_5 IS 'Check Digit Weight 5 is the value to be used as a weight for the check digit calculation in the 5th digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_6 IS 'Check Digit Weight 6 is the value to be used as a weight for the check digit calculation in the 6th digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_7 IS 'Check Digit Weight 7 is the value to be used as a weight for the check digit calculation in the 7th digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CD_WEIGHT_8 IS 'Check Digit Weight 8 is the value to be used as a weight for the check digit calculation in the 8th digit position from the right.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CHECK_DIGIT_IND IS 'Determines if check digits will be used on item number fields when the item number type is an Oracle Retail Item Number. This field will contain Y if check digits are to be used, otherwise it will contain N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND IS 'Determines whether the retailer wants the ability to control at the class level whether retail values in RMS are displayed as VAT inclusive or not.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CLOSE_MTH_WITH_OPN_CNT_IND IS 'Indicator if a month with open Unit and Value stock counts can be closed.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CLOSE_OPEN_SHIP_DAYS IS 'Number of days that shipments can stay open in Unmatched status (invc_match_status) before they are closed.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COMP_LIST_DAYS IS 'Indicates the number of days of competitive shopping list history will remain in RMS.  All history older than this number of days will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COMP_PRICE_MONTHS IS 'Indicates the number of months of competitive pricing information will remain in RMS.  All history older than this number of months will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CONSOLIDATION_IND IS 'Indicator used to determine what type of consolidation exchange rate is used as default for all currency conversion within Oracle Retail.  If this field contains Y, then the consolidation exchange rate maintenance will be supported, and consolidation exchange rate is used as default for all currency conversion within Oracle Retail.  If this field is N, then the consolidation exchange rate maintenance will not be supported, and operational exchange rate is used as default for all currency conversion within Oracle Retail. Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CONTRACT_INACTIVE_MONTHS IS 'Indicates the number of months that inactive contracts will remain in RMS.  All inactive contracts older than this number of months will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CONTRACT_IND IS 'Specifies if contracting is being used within Oracle Retail. Default value is N if NULL.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COPY_PO_CURR_RATE IS 'When copying a PO from an existing PO, this indicator determines if the exchange rate will be copied on the new PO. When Y, the exchange rate will be copied from existing POs.  If N, then the latest exchange rate is used for POs.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_EVENT_HIST_DAYS IS 'Dictates how long cost event records are maintained on the cost event tables.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_LEVEL IS 'Indicates which cost bucket is used when calculating the return on investment for investment buy opportunities and also for deals.  Valid values are ''N'' for net cost, ''NN'' for net net cost and ''DNN'' for dead net net cost.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_MONEY IS 'The cost of money, defined as the annualized percentage cost to borrow capital for investing. This value is currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_OUT_STORAGE IS 'The default cost of outside storage, expressed as the weekly cost per the unit of measure specified in COST_OUT_STORAGE_UOM.   This value is held in the primary system currency and currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_OUT_STORAGE_MEAS IS 'Defines the default measure for the cost of outside storage (3rd Party).  This is used by investment buy functionality only. Valid values are Mass, Volume, Pallet, Case, Each, and Stat Case.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_OUT_STORAGE_UOM IS 'Contains the unit of measure to which the default cost of outside storage is applicable. The unit of measure may only be pallet or from the volume class.  This value is currently used by the investment buy functionality only. Valid values are found on the UOM_CLAS table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_PRIOR_CREATE_DAYS IS 'Indicates the minimum number of days prior to the cost change effective date that a cost change can be entered.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_WH_STORAGE IS 'The default cost of warehouse storage, expressed as the weekly cost per the unit of measure specified in COST_WH_STORAGE_UOM. This value is held in the primary system currency and currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_WH_STORAGE_MEAS IS 'Defines the default measure for cost of warehouse storage (company-owned). This is used by investment buy functionality only. Valid values are Mass, Volume, Pallet, Case, Each and Stat Case.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.COST_WH_STORAGE_UOM IS 'The unit of measure to which the default cost of warehouse storage is applicable.  The UOM may only be pallet or from the volume class as defined on the UOM_CLASS table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CREDIT_MEMO_LEVEL IS 'This indicator decides at what level the credit/debit memos will be posted to ReIM, valid values can be L (Location level), S (Set of Books level), T (Transfer entity level), D (Deal level). Default value if NULL is D.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CURRENCY_CODE IS 'Indicates the base currency for RMS. This field is validated from the CURRENCIES table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CUST_ORDER_HISTORY_MONTHS IS 'Specifies the number of months after which the fulfilled customer reservation records and rejected purchase or transfer leg customer fulfillment records are purged.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.CYCLE_COUNT_LAG_DAYS IS 'The Days Prior to Stock Count Variance Processing parameter indicates the number of days before stock count variances will be automatically processed. A value of zero specifies that variances will be processed immediately.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DAILY_SALES_DISC_MTHS IS 'Indicates the number of months of sales discount information will be retained in RMS on the DAILY_SALES_DISCOUNT table.  All history older than this number of months will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DATA_INTEGRATION_LANG IS 'This is the language for which information will be entered into RMS and integrated to other systems.  Foreign key to this column references to LANG.lang.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DATA_LEVEL_SECURITY_IND IS 'Indicates if data level security is enabled in RMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DATE_ENTRY IS 'This column specifies the format in which dates will be entered throughout the system. Example: ''MMDDRR''.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEAL_AGE_PRIORITY IS 'The Deal Age Priority parameter drives if older or newer deals have a higher priority when more than one deal component applies to the same item/location.  Valid values are ''O'' for older deals or ''N'' for newer deals.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEAL_HISTORY_MONTHS IS 'Indicates the number of months that closed deals will be retained in RMS.  All deals older than this number of months will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEAL_LEAD_DAYS IS 'This parameter determines the earliest date that a deal created today can become active in RMS, which is calculated by taking the current date plus the value in this parameter.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEAL_TYPE_PRIORITY IS 'The Deal Type Priority parameter controls if annual deals or promotional deals have a higher priority when more than one deal component applies to the same item/location.  Valid values are ''A'' for annual deals or ''P'' for promotional deals.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_ALLOC_CHRG_IND IS 'Specifies if Up-Charges should be defaulted to allocations.	'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_CASE_NAME IS 'This is the code referencing the name/label to refer to the CASE pack size. Valid codes are defined in the CASN code type (i.e. pack, box, bag).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM IS 'Indicates the default dimension Unit of Measure that will be used throughout RMS. It should have a valid UOM value used in the system which exists in UOM_CLASS table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_INNER_NAME IS 'This is the code referencing the name/label to refer to the INNER pack size. Valid codes are defined in the INRN code type (i.e.  sub-case, sub-pack).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_ORDER_TYPE IS 'This is the default order type associated with transfers and stand-alone allocations.   Valid values include AUTOMATIC, WAVE, and MANUAL which are stored on the ORDER_TYPES table with a po_ind = N.  This value is sent to WMS and determines how stock orders that are downloaded are processed if not otherwise specified. Wave uses the pre-defined Shipping Schedule and proceeds without intervention. AUTOMATIC does not require destinations to have a pre-defined shipping schedule, but does proceed without inter-vention. MANUAL allows intervention by selecting orders to be included in a wave.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD IS 'This specifies if the default packing method that will be assigned to an item when it is created in RMS.  Valid values are stored on the code_detail table with the code_type PKMT'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_PALLET_NAME IS 'This is the code referencing the name/label used to refer to PALLET  pack size. Valid codes are defined in the PALN code type (i.e. pallet, flat)'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_SIZE_PROFILE IS 'This field defines if the size profile should be defaulted in replenishment, when set to Y the Apply Size Profile checkbox on replenishment attributes screen is checked. When set to Y, replenishment applies size profiles for stores. Default value = N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM IS 'Indicates the Standard UOM that will be the default for all new items created in RMS.  This can be changed as part of the item creation process. It should have a valid UOM value used in the system which exists in UOM_CLASS table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_TAX_TYPE IS 'This determines what type of taxation is being used by the company. Valid values are: ''SVAT'' – Simple VAT (VAT information is configured in RMS) ''GTAX'' – Global Taxation (Used for Brazil localized solution having external tax engine) ''SALES'' – Sales and Use Tax'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_UOP IS 'Indicates the default Unit of Purchase that will be used for all POs created in RMS.  Valid values are: S - Standard Unit of Measure and C - Case or case equivalent.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_VAT_REGION IS 'The default VAT region is used when a VAT region cannot be determined for a transaction.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM IS 'Indicates the Weight UOM that will be used as a default throughout RMS. It should have a valid UOM value used in the system which exists in UOM_CLASS table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEPT_LEVEL_ORDERS IS 'Indicates whether multiple department orders will be allowed in the system.  If this parameter is set to Y, then only one department is allowed on an order when created via RMS screens or via replenishment.  If the value is N, then one or more departments are allowed on orders.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEPT_LEVEL_TRANSFERS IS 'Specifies whether to enforce department level transfers. If this field is set to Y, then each transfer must contain only items within a single department. If this field is set to N, then items from multiple departments can appear on a single transfer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DIFF_GROUP_MERCH_LEVEL_CODE IS 'Indicates the merchandise hierarchy level (e.g. division) at which data level security will be controlled for Diff Groups.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DIFF_GROUP_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for Diff Groups.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DISTRIBUTION_RULE IS 'Distribution rule to use when the actual quantity to be distributed among virtual warehouses is different from the expected quantity.  For example, if the quantity received is greater than quantity ordered. The valid values are determined by code_type DRUL on the code_detail table, and includes Proration, Minimum to Maximum and Maximum to Minimum.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DOMAIN_LEVEL IS 'Domain level defines the level at which RDF and other RPAS applications do planning and forecasting.  RMS uses this to determine the level at which data will be sent. Valid values are: D - Department C - Class S - Subclass	'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DUPLICATE_RECEIVING_IND IS 'Determine whether or not duplicate receiving will be allowed against the same carton on the same receipt.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.EDI_COST_OVERRIDE_IND IS 'This parameter indicates whether RMS will allow costs sent through EDI855 for vendor generated orders to be overridden in RMS.  If this field is set to Y, then the unit cost on the PO can be updated by applying deals the unit cost of the item. If this field is set to N, then the cost sent by the supplier will be used.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.EDI_DAILY_RPT_LAG IS 'Indicates the number of days sales information will be held in RMS prior to transmitting to suppliers via EDI 852.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.EDI_REV_DAYS IS 'Indicates the number of days versions and revisions to an order will be retained in RMS after an order has been completed or cancelled.  All records older than this number of days will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ELC_INCLUSIVE_IND_COMP_STORE IS 'Indicates whether pricing and acquisition costs should be inclusive of ELC for company stores. If no value is specified, it is assume to be N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ELC_INCLUSIVE_IND_WF_STORE IS 'Indicates whether pricing and acquisition costs should be inclusive of ELC for franchise stores. If no value is specified, it is assume to be N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ELC_IND IS 'This column controls whether landed cost will be used within the system. Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.EXPIRY_DELAY_PRE_ISSUE IS 'Indicates the number of days pre-issued order numbers remain valid in RMS before being deleted.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.FDN_STG_RETENTION_DAYS IS 'Specifies the number of days after which the records in staging tables used by Foundation modules are purged.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.FINANCIAL_AP IS 'This attribute indicates the external financial system being used by the business. Valid values are ''O'' (Oracle EBS Financials) and ''A'' (Peoplesoft) and NULL is any other financial system is used.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.FOB_TITLE_PASS IS 'Indicator used to determine where the title for goods is passed from the vendor to the purchaser. Examples include city, factory, or origin. This can be changed at the PO level.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.FOB_TITLE_PASS_DESC IS 'Contains the default description that should be used when title pass location is defaulted onto a PO.  This can be changed at the PO level.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.FORECAST_IND IS 'This attribute is used to indicate whether RMS is integrated to a forecasting system, such as Oracle Retail Demand Forecasting (RDF). Valid values are ''Y''-Yes or ''N''-No.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.FUTURE_COST_HISTORY_DAYS IS 'Indicates the number of days that cost events are retained on the FUTURE_COST table in RMS.  All records older than this number of days will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.GEN_CONSIGNMENT_INVC_FREQ IS 'The frequency that PO (and invoices if applicable) are generated for consignment goods. Valid values include are P - Multiple, W - Weekly, M - Monthly or D - Daily.  If it is set to P, then only one PO/Invoice will be generated for each unique level (as defined by the gen_consignment_invc_item_sup_loc_ind ) per run of the sales upload program. If W, then only one PO/Invoice will be generated for each unique level per week. If M, then only one PO/Invoice will be generated for each unique level per month. If D, then only one PO/Invoice will be generated for each unique level per day.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.GEN_CON_INVC_ITM_SUP_LOC_IND IS 'Identifies the level at which consignment POs and its consignment invoices are generated.  Valid values include: S - Supplier/Department, L -Supplier/Department/Loc, I - Supplier/Location/Item.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.GL_ROLLUP IS 'The roll up level of Oracle Retails general ledger information when bridged to a financial system. Valid values are: D - Department, C -Class, S - Subclass.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.GROCERY_ITEMS_IND IS 'Determines whether or not the retailer handles grocery merchandise. When this is Y, then RMS enables the user to enter grocery related info like catch weight order, sale type, wastage details, perishable info, etc. during item creation. When set to N all these fields are hidden from the screens.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.HTS_TRACKING_LEVEL IS 'Indicates the level at which HTS is tracked in RMS.  Valid values are M or S.  If the value is M, HTS is tracked at item/supplier/country of manufacture level. If the value is S, HTS is tracked at item/supplier/country of sourcing level.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.HTS_UPLD_ORDER_STATUS IS 'Indicates the order status. Valid values are W(Worksheet) and A(Approved).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.IB_RESULTS_PURGE_DAYS IS 'Indicates the number of days that investment buy results are held in RMS.  All records older than this number of days will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.IMAGE_PATH IS 'This holds path name for where images linked to items are stored.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.DEFAULT_ITEM_IMAGE IS 'This holds default image name for item image and will be displayed in reports when the Item image is not found in ITEM_IMAGE table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.IMPORT_HTS_DATE IS 'It is the date that is used to calculate the effective date range for applicability of HTS on a PO. Valid values are W (written date of the PO), and N (not before date of the PO).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.IMPORT_IND IS 'This field will indicate if Oracle Retail Trade Management is used to manage importing within RMS.	'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.INCREASE_TSF_QTY_IND IS 'Controls if the transfer quantity can be increased once a transfer has been approved.  If Y, then transfer quantity can be changed if there is outstanding quantity on the transfer (e.g. transfer qty > distributed/selected/shipped qty). When N, a user can only update the transfer to indicate total stock on hand should be shipped instead of a manually entered quantity.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_BASIS IS 'Intercompany Transfer Basis determines if intercompany transfers are based on ''T''-transfer entities or ''B'' - set of books indicator.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.INV_ADJ_MONTHS IS 'Indicates the number of months that inventory adjustment history is held in RMS.  All records older than this number of months will be deleted by a batch process.  If a value is not entered in this field, inventory adjustment records will never be removed from RMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.INV_HIST_LEVEL IS 'History Level specifies how inventory history should be captured in the weekly inventory history programs.  Valid values for the field are: ''N'' - for no inventory history, ''I'' -  end of week inventory for items that have had sales over the past week, and ''A'' - to capture end of week inventory for all items regardless of whether they have had sales.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ITEM_HISTORY_MONTHS IS 'Indicates the number of months of sales history is held in RMS.  All records older than this number of months will be deleted from RMS by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LATEST_SHIP_DAYS IS 'The number of days after which the latest ship date should default from the earliest ship date on POs.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LC_EXP_DAYS IS 'Indicates the default number of days after the latest ship date that the Letter of Credit will expire.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LC_FORM_TYPE IS 'Contains the default Letter of Credit Form Type which determines the level of detail the Letter of Credit will send to the issuing bank. Valid values include L = Long form or S = Short form.  A long form LC contains item level details, a short form LC is at a PO level.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LC_TYPE IS 'Indicates the default Letter of Credit type that will be used during creation of LCs. Valid values are M(Master), N(Normal), R(Revolving) and O(Open).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LOC_ACTIVITY_IND IS 'Specifies if the location activity schedules are used in the replenishment calculations. Activity schedule defines when stores and warehouses are closed or not available e.g, stock take, public holidays, Sunday, etc.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LOC_CLOSE_HIST_MONTHS IS 'Indicates the number of months that activity schedules will be retained in RMS on the LOCATION_CLOSED, COMPANY_CLOSED and COMPANY_CLOSED_EXCEP tables.  All records older than this number of months will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LOC_DLVRY_IND IS 'Specifies if the location delivery schedules are used in the replenishment calculations. Delivery schedules defines on what days suppliers and warehouse will make deliveries.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LOC_LIST_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for Location Lists.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LOC_TRAIT_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for Location Traits.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.LOOK_AHEAD_DAYS IS 'Number of days before a cost event (deal end or cost increase) that the investment buy opportunity calculation will begin considering the event.   This value is currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MARGIN_IMPACT_HIST_RECS IS 'This column controls how many historical future cost records will be displayed for each item/supplier/country/location on the Margin Impact screen.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MAX_CUM_MARKON_PCT IS 'Indicates the maximum value that will be allowed for Cumulative Mark-On Percentage, when recalculated in the monthly stock ledger batch processes.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MAX_QTY_PCT_DEC IS 'The maximum decimal digits for quantity or percentage precision. The value is between 0 and 4.';
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MAX_SCALING_ITERATIONS IS 'The maximum number of iterations the constraint scaling process should perform when attempting to find a valid solution for an order before the process should stop.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MAX_WEEKS_SUPPLY IS 'The default maximum weeks of supply to use in the investment buy opportunity calculation. The investment buy opportunity calculation will not recommend an order quantity that would stock the associated location (currently warehouses only) beyond this number of weeks. This value is currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MERCH_HIER_AUTO_GEN_IND IS 'This determines if the department, class and subclass IDs will be automatically generated. If indicator is set to N then the user can manually enter a department, class, and subclass ID when new are created.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MIN_CUM_MARKON_PCT IS 'Indicates the minimum value that will be allowed for Cumulative Mark-On Percentage, when recalculated in the monthly stock ledger batch processes.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MIN_QTY_PCT_DEC IS 'The minimum decimal digits for quantity or percentage precision. The value is between 0 and 4.';
/

COMMENT ON COLUMN SYSTEM_OPTIONS.MULTI_CURRENCY_IND IS 'Determines if the company can use more than one currency in the system. Default value if NULL is Y.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.NUM_HIST_EXCHANGE_RATE IS 'This field is used to control how many historical exchange rate is presented to the user in list of value while selecting the exchange rate. The currently active exchange rate and future effective rates are included in the list of value by default. A value of N for this field will in addition also include upto N number of historical exchange rates prior to the currently active exchange rate effective date. The default value is 5.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.NWP_IND IS 'Indicates if NWP processing is turned on in the system. This feature will enable a retailer to determine the end of year inventory values in each store as of December 31st of the previous year position with the assumption that a stock count must be executed'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.NWP_RETENTION_PERIOD IS 'Indicates the number of years that NWP data will be retained in RMS.  All records older than this number of years will be deleted from RMS by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.OMS_IND IS 'This field indicates whether or not RMS is integrated with an external order management system.  Valid values are Y and N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORDER_BEFORE_DAYS IS 'The number of days prior to the planned ready date for which the system automatically generates the next order specified on the production plan for type B contracts.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORDER_EXCH_IND IS 'This indicator is used if the order exchange rate can be overwritten. When set to Y, a user can update the exchange rate used for order/location level expense to the exchange rate on the order, which would result in expenses and assessments being recalculated . Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORDER_HISTORY_MONTHS IS 'Indicates the number of months that completed orders will remain in RMS.  All orders, along with the associated order information (e.g. shipments, expenses, etc.) older than this number of months will be removed from RMS by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORDER_HTS_IND IS 'Indicates whether HTS should be required for import orders. Valid values are Y and N. If set to Y, then all the items on order should have HTS code associated prior to PO approval.  For packs, all the component items should have HTS code associated.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORD_APPR_AMT_CODE IS 'Determines whether the order approval upper limit amount by user role is based on total cost or total retail of the order. Valid values for this field are: C - Cost R - Retail.  Limits by user role are set on the RTK_ROLE_PRIVS table.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORD_APPR_CLOSE_DELAY IS 'This parameter drives closure of shipped but unreceived orders once the latest ship date crosses the delay period through Order Auto Close batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORD_AUTO_CLOSE_PART_RCVD_IND IS 'Indicates if partially received orders should be auto closed through Order Auto Close batch.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORD_PART_RCVD_CLOSE_DELAY IS 'This parameter drives closure of partially received orders once the not after date crosses the delay period and ORD_AUTO_CLOSE_PART_RCVD_IND=Y through the Order Auto Close batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORD_WORKSHEET_CLEAN_UP_DELAY IS 'Specifies the number of days after an order is created that that an order in Worksheet status that has never been approved will remain in the system.  After this number of days, the order will be purged from the system.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ORG_UNIT_IND IS 'When a company is divided into different ledgers or a company has operations in different countries, it may use different sets of books for each division. An org unit is a subdivision within a set of books which could represent a sales office, division, or department. Org units are not associated with legal entities.  This column will be used to determine if organizational units are used within the system. Valid values are Y-Yes and N - No.  Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.OTB_SYSTEM_IND IS 'This determines whether client intends to use the Open-to-Buy functionality within RMS. This is also used to control whether to re-calculate OTB during reclassification process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.PRICE_HIST_RETENTION_DAYS IS 'Indicates the number of days of price history that will be retained in RMS.  All records older than this number of days will be deleted from RMS by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.PROC_DATA_RETENTION_DAYS IS 'The maximum number of days to hold data induction related information in the svc_* staging tables.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RAC_RTV_TSF_IND IS 'Determines whether RTV (created throught MRT functionality) and RAC (Reallocation Transfer) transfer types will be allowed to be created with locations that result in intercompany transfers.  If set to A (Intra-Company), the system will not allow users to select locations on a transfer that cross entities.  If set to E (Intercompany) then there is not a system level restriction on locations crossing entities for these types of transfers.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RCV_COST_ADJ_TYPE IS 'Drives the type of Receiver cost adjustment. Valid values are S(Standard) and F(FIFO). The standard adjustment will recalculate WAC using all units on the receivers.  The FIFO adjustment will layer the receipts for the PO and create adjust WAC for only those units in stock whose receivers have not been matched.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RECLASS_APPR_ORDER_IND IS 'Specifies whether item reclassification can take place if the item exists on an approved order. When this indicator is Y reclass is done as follows: * when department level PO is turned on and PO is in worksheet/submitted - reclass will be allowed * when department level PO is turned on and PO is in approved - reclass will not be allowed if multiple items on PO * when department level PO is turned off and PO is approved and partially received - reclass will not be allowed * when department level PO is turned off and PO is approved  - reclass will be allowed. Default value if NULL is Y.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REDIST_FACTOR IS 'The percentage by which the supplier lead time and warehouse lead time should be incremented to come up with the number of days prior to the not before date that an order should show up on a report listing approved orders requiring redistribution.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REIM_IMPL_DATE IS 'Implementation date of the current version of REIM product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REIM_IND IS 'This attribute is used to indicate whether RMS is integrated with the Oracle Retail Invoice Match (ReIM) application.  Valid values are Y or N, with N used as the default if null.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REIM_VERS IS 'Version number of ReIM installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REJECT_STORE_ORD_IND IS 'The Reject Store Orders indicator determines if uploaded store orders should ever be rejected. If the indicator is N, then store orders for all need dates are valid. If set to ''Y'', then only store orders with needs date on or after the NEXT_DELIVERY_DATE on REPL_ITEM_LOC are valid.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REPL_ATTR_HIST_RETENTION_WEEKS IS 'Indicates the number of weeks that replenishment attribute history will be retained in RMS.  All records older than this number of weeks will be deleted from RMS by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REPL_ORDER_DAYS IS 'The number of days after the replenishment date plus the maximum lead time of the item/locations on the order that the not after date should fall for orders created by the replenishment process (i.e. not after date = replenishment date + max. lead time + replenishment order days).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REPL_ORDER_HISTORY_DAYS IS 'Indicates the number of days that replenishment orders in worksheet status will remain in RMS.  All orders that are older than this number of days will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.REPL_RESULTS_PURGE_DAYS IS 'Indicates the number of days of replenishment results (REPL_RESULTS) will remain in RMS.  All records older than this number of days will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RESA_IMPL_DATE IS 'Implementation date of the current version of RESA product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RESA_VERS IS 'Version number of ReSA installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RETENTION_OF_REJECTED_COST_CHG IS 'Indicates the number of days that rejected cost changes will remain in RMS.  All rejected cost changes older than this number of days will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RETN_SCHED_UPD_DAYS IS 'Indicates the number of days that scheduled replenishment update history should remain in RMS.  All records older than this number of days will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RMS_IMPL_DATE IS 'Implementation date of the current version of RMS product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RMS_VERS IS 'Version number of RMS installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ROUND_LVL IS 'Used to indicate the smallest supplier orderable unit and controls how rounding is executed for purchase orders. For example, it will determine if order quantities should be rounded to the nearest case, to the nearest pallet, or to case and then pallet. Valid values can be found on the CODE_DETAIL table with a code type of ORL.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ROUND_TO_CASE_PCT IS 'Percentage to round from inners to a case. During rounding, this value is used to determine whether to round partial case quantities up or down.  If the case-fraction in question is less than the percentage defined, it is rounded down; if not, it is rounded up.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ROUND_TO_INNER_PCT IS 'Percentage to round from eaches to an inner. During rounding, this value is used to determine whether to round partial inner quantities up or down.  If the inner fraction in question is less than the percentage defined, it is rounded down; if not, it is rounded up.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ROUND_TO_LAYER_PCT IS 'Percentage to round from partial layer quantities in the ordering process.  If the layer fraction in question is less than the percentage defined, it is rounded down; if not, it is rounded up.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.ROUND_TO_PALLET_PCT IS 'Percentage to round from partial pallet quantities in the ordering process.  If the pallet fraction in question is less than the percentage defined, it is rounded down; if not, it is rounded up.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RPM_IMPL_DATE IS 'Implementation date of the current version of RPM product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RPM_IND IS 'Indicates whether RPM is used as a pricing application or not.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RPM_VERS IS 'Version number of RPM installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RTM_SIMPLIFIED_IND IS 'Indicates if full RTM functionality is available or limited functionality.  Limited or simplified RTM functionality means the user will not have access to transportation, customs entry, ALC, Freight maintenance, SCAC codes. The user will have access to HTS and Assessments. Letter of credit information will be accessible but no longer required.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RTM_TRNSPRT_OBL_ALLOC_METHOD IS 'RTM Transportation Obligation Allocation Method describes how the merchandise is transported from the manufacturers through the customs clearance in the importing country. The transport type used for an allocation from obligations. Valid values are (ASN,TRNSPRT). If the value is TRNSPRT, the transportation quantity will drive the allocation process. This is appropriate if the transportation quantities will be maintained at the item level to accurately reflect the quantities passing through customs. If the value is ASN, the receipt quantity will drive the allocation process. This is appropriate if the accurate transportation quantities will not be attained in the system. In this case, it will not be possible to accurately allocate obligations at the Container or BOL/AWB(air way bill) level since these identifiers are not present on the ASN.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RTV_NAD_LEAD_TIME IS 'Determines the number of days between the Tsf Not After Date and the RTV Not After Date for MRT generated RTVs and transfers.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RTV_ORDER_HISTORY_MONTHS IS 'Indicates the number of months that completed RTVs will remain in RMS.  All records older than this number of months and whose invoices are posted to ReIM will be deleted in a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RTV_UNIT_COST_IND IS 'The value used to set the default unit cost on an RTV (Return to Vendor). Valid values are R (last receipt cost), S (standard cost), or A (average cost).'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RWMS_IMPL_DATE IS 'Implementation date of the current version of RWMS product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RWMS_IND IS 'This attribute is used to indicate whether RMS is integrated with a Warehouse Management System. Valid values are Y or N, with N used as the default if null.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.RWMS_VERS IS 'Version number of RWMS installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SALES_AUDIT_IND IS 'This attribute is used to indicate whether ReSA functionality is being used.  Valid values are Y or N, with N used as the default if null.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SEASON_MERCH_LEVEL_CODE IS 'Indicates the merchandise hierarchy level (e.g. division) at which data level security will be controlled for Seasons.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SEASON_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for Seasons.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SHIP_RCV_STORE IS 'Determines if RMS online will be used for shipping and receiving at store locations.  If so, online access to these functions will be enabled for store locations.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SHIP_RCV_WH IS 'Determines if RMS online will be used for shipping and receiving at warehouse locations.  If so, online access to these functions will be enabled for warehouse locations.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SHIP_SCHED_HISTORY_MTHS IS 'Indicates the number of months of store ship schedule history should be retained by RMS.  All records older than this number of months will be deleted from the STORE_SHIP_SCHEDULE table by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SIM_FORCE_CLOSE_IND IS 'If this value is not null, it will override the value of TSF_FORCE_CLOSE_IND for store-to-store transfers only.  Valid values are: * BL (Both Locations): The sending location on-hand quantity will be increased and 30/32 records will be written to reverse the shipment by the quantity which is not received.  Note: the functionality described for BL will be applied for all over receipts of store-to-store transfers, regardless of the setting of this parameter for consistency with how SIM handles over receipts. * NL (No Loss): The sending location on-hand quantity will be increased and 22 records will be written at both locations for the quantity which is not received. *RL (Receiving Location): The sending location on-hand quantity will not be changed. A 22 record will be written at the receiving location to account for the quantity which is not received. *SL (Sending Location): The sending location on-hand quantity will not be changed. 30/32 records will be written to reverse the shipment by the quantity which is not received and a 22 record will be written at the sending location to account for the same quantity. This option is provided for compatibility with the options available in the SIM application for store-to-store transfers.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SIM_IMPL_DATE IS 'Implementation date of the current version of SIM product installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SIM_IND IS 'This attribute is used to indicate whether RMS is integrated with a Store Inventory Management  Valid values are Y or N, with N used as the default if null.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SIM_VERS IS 'Version number of SIM installed at a customer.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SKULIST_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for Item Lists.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SOFT_CONTRACT_IND IS 'Soft contracting is a option to allow fashion retailer to commit to a above transaction level item (e.g. style) without specifying the diffs that bring the item to transaction level.  When set to N, all items on contracts must be indicated at the transaction level before the contract can be used. When set to Y, above transaction level items can be in included in contracts.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SS_AUTO_CLOSE_DAYS IS 'The numbers of days after the last receipt that store to store stock orders will be closed automatically.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STAKE_AUTO_PROCESSING IS 'The Stock Count Automatic Processing indicator is used to indicate whether stock counts will be automatically processed after the designated number of elapsed days.  Valid values are Y, N, S or NULL.  When auto-processing is ''Y'' and the count occurred x days ago, based on the CYCLE_COUNT_LAG_DAYS, then the Stock Count Stock on Hand Updates (stkvar) batch process will automatically process stock counts that have a snapshot and count.  When the value is set to ''S'', then RMS will automatically process both the unit and value components of the count.  When value is set to N or NULL, user has to manually ""approve"" the count on-line so that it is processed through this batch process.  When integrating with SIM and RWMS, it is recommended that this be set to Y or S in order to keep RMS in synch with inventory levels in SIM and RWMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STAKE_COST_VARIANCE IS 'Contains the inventory value variance at cost that will trigger exception reporting in the Stock Count module. Cost variances (plus or minus) smaller than this value will not be reflected in the Stock Count Variance report, unless one of the other criteria (see retail and unit variances parameters) is exceeded. However, all variances will be reflected in inventory updates and shrinkage calculations on the stock ledger.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS IS 'Contains the number of days before a stock count date that changes must stop for that stock count. This parameter is also used to determine the date (= stock count date - stock count lockout days) that the stock count will be exploded to SKU level from department, class or subclass levels for a ""Unit and Value"" type of stock count.  After this date, the SKU level information can then be used to generate stock count worksheet or downloaded to stores and warehouses.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STAKE_RETAIL_VARIANCE IS 'Contains the inventory value variance at retail that will trigger exception reporting in the Stock Count module. Retail variances (plus or minus) smaller than this value will not be reflected in the report, unless one of the other criteria (see cost and unit variance parameters) is exceeded. However, all variances will be reflected in inventory updates and shrinkage calculations on the stock ledger.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STAKE_REVIEW_DAYS IS 'This field contains the minimum number of days before a scheduled stock count that the stock count actually gets generated in the system.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STAKE_UNIT_VARIANCE IS 'Contains the number of units of variance that will trigger exception reporting in the Stock Count module. Unit variances (plus or minus) smaller than this value will not be reflected in the report, unless one of the other criteria (see cost and retail variance parameters) is exceeded. However, all variances will be reflected in inventory updates and shrinkage calculations on the stock ledger.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.START_OF_HALF_MONTH IS 'The month number of the first month of the first half of any fiscal year. It is set during system installation and cannot be changed. The month number must be from -12 to 12, excluding 0 and -1. A negative number indicates that the first half of a year starts in the previous year with -2 = February, -3 =March etc. A positive number corresponds to the normal calendar month numbers.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STD_AV_IND IS 'Indicates whether standard cost or weighted average cost will be used for inventory and gross profit calculations.  Valid values are ''S'' for standard and ''A'' for average.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND IS 'This column determines whether retail value in stock ledger is VAT inclusive or not. This field is only applicable when VAT is used in the system (when DEFAULT_TAX_TYPE is ''SVAT'' or ''GTAX'') . If this field contains Y, then all retail value in the stock ledger (e.g. sales retail, purchase retail and gross margin) are VAT inclusive.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STOCK_LEDGER_TIME_LEVEL_CODE IS 'The lowest time interval for the stock ledger: Month (M) or Week (W). If a client is running on 454 calendar, stock ledger is available to run for both weekly and monthly levels, which means that inventory and gross margin are available at both weekly and monthly levels. However, a client is not required to run the weekly level stock ledger, if it is not needed. If a client is running on a normal calendar, only monthly level stock ledger is available.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STORAGE_TYPE IS 'Indicates the type of storage that should be used as the default storage cost when calculating investment buy opportunities. Valid values are Warehouse and Outside.   This value is currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STORE_ORDERS_PURGE_DAYS IS 'Indicates the number of days that store orders (STORE_ORDERS) will be retained by RMS.  All records older than this number of days will be purged by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.STORE_PACK_COMP_RCV_IND IS 'Store pack component receiving, if set to Y, stores will do receiving at the component level, not the carton/pack level.  This flag is used in conjunction with the tampered carton functionality in RMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SUPPLIER_SITES_IND IS 'Suppliers usually have multiple sites from which they supply merchandise. Each of these sites can have different supplier control parameters like lead times, payment terms, etc. If the ''Supplier Site'' parameter is set to ''Y'' then many functions in RMS that involve suppliers, such as purchasing, will display the supplier site details. If the parameter is set to ''N'', then only suppliers will be visible. Default value is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SUPP_PART_AUTO_GEN_IND IS 'This column determines whether the Supplier/Partner ID will be automatically generated upon creation. If not checked then the user can manually enter an ID.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.SW_AUTO_CLOSE_DAYS IS 'The number of days after the last receipt that store to warehouse stock orders will be closed automatically.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TABLE_OWNER IS 'Contains the name of the schema owner of the RMS database.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TARGET_ROI IS 'The default return on investment that must be met or exceeded for the investment buy opportunity to recommend an order quantity.   This value is currently used by the investment buy functionality only.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TICKET_TYPE_MERCH_LEVEL_CODE IS 'Indicates the merchandise hierarchy level (e.g. division) at which data level security will be controlled for Ticket Types.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TICKET_TYPE_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for Ticket Types.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TIME_DISPLAY IS 'Format mask used throughout the system to display time values.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TIME_ENTRY IS 'Format mask used for entering time values within the system.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TRAN_DATA_RETAINED_DAYS_NO IS 'Indicates the number of days that stock ledger transaction data (TRAN_DATA_HISTORY) is retained in RMS. All records older than this number of days will be purged by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_AUTO_CLOSE_STORE IS 'Indicates whether or not, when a transfer receiving location is store, a shipment is closed after receipt.  If set to N, then the Document Close (DOCCLOSE) batch process will apply rules to determine whether the transfer can be closed.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_AUTO_CLOSE_WH IS 'Indicates whether or not, when a transfer receiving location is warehouse, a shipment is closed after receipt.  If set to N, then the Document Close (DOCCLOSE) batch process will apply rules to determine whether the transfer can be closed.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_CLOSE_OVERDUE IS 'Indicator to close the unshipped/partially shipped transfers via the batch process tsfclose. A transfer is said to be overdue if it remains unprocessed for more than specified number of days. The number of days after which the transfer will be considered overdue are configured in WS_AUTO_CLOSE_DAYS, WW_AUTO_CLOSE_DAYS, SS_AUTO_CLOSE_DAYS and SW_AUTO_CLOSE_DAYS.  Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND IS 'This determines the inventory updates are made for under received quantities on receipt. Valid values include: * BL (Both Locations): The sending location on-hand quantity will be increased and 30/32 records will be written to reverse the shipment by the quantity which is not received. * NL (No Loss): The sending location on-hand quantity will be increased and 22 records will be written at both locations for the quantity which is not received. * RL (Receiving Location): The sending location on-hand quantity will not be changed. A 22 record will be written at the receiving location to account for the quantity which is not received. * SL (Sending Location): The sending location on-hand quantity will not be changed. 30/32 records will be written to reverse the shipment by the quantity which is not received and a 22 record will be written at the sending location to account for the same quantity.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_HISTORY_MTHS IS 'Indicates the number of months that completed transfers are retained in RMS.  All records older than this number of months will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_MD_STORE_TO_STORE_SND_RCV IS 'Indicates, for a store to store transfer, which location would take the markdown when the retail price of an item is different at the sending and receiving locations.  Valid values are S - Sending or R - Receiving.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_MD_STORE_TO_WH_SND_RCV IS 'Indicates, for a store to warehouse transfer, which location would take the markdown when the retail price of an item is different at the sending and receiving locations.  Valid values are S - Sending or R - Receiving.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_MD_WH_TO_STORE_SND_RCV IS 'Indicates, for a warehouse to store transfer, which location would take the markdown when the retail price of an item is different at the sending and receiving locations.  Valid values are S - Sending or R - Receiving.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_MD_WH_TO_WH_SND_RCV IS 'Indicates, for a warehouse to warehouse transfer, which location would take the markdown when the retail price of an item is different at the sending and receiving locations.  Valid values are S - Sending or R - Receiving.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_MRT_RETENTION_DAYS IS 'Indicates the number of days that completed mass return transfers (MRT) are retained by RMS.  All records older than this number of days will be deleted by a batch process.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_OVER_RECEIPT_IND IS 'This determines the inventory updates are made for overage quantities on receipt. Valid values include: * BL (Both Locations): The sending location on-hand quantity will be reduced and 30/32 records will be written to increase the shipment by the overage qty. * NL (No Loss): The sending location on-hand quantity will be reduced and 22 records will be written at both locations for the overage quantity. * RL (Receiving Location): The sending location on-hand quantity will not be changed. A 22 record will be written at the receiving location to account for the overage. * SL (Sending Location): The sending location on-hand quantity will not be changed. 30/32 records will be written to increase the shipment by the overage qty and a 22 record will be written at the sending location to account for the overage.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.TSF_PRICE_EXCEED_WAC_IND IS 'Determines if the transfer price can exceed the WAC of the from location on intercompany transfers.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.UDA_MERCH_LEVEL_CODE IS 'Indicates the merchandise hierarchy level (e.g. division) at which data level security will be controlled for UDAs.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.UDA_ORG_LEVEL_CODE IS 'Indicates the organizational hierarchy level (e.g. region) at which data level security will be controlled for UDAs.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.UPDATE_ITEM_HTS_IND IS 'Indicates whether, during HTS upload, if items should be automatically updated using new HTS information. This column is used only if the IMPORT_IND is Y. If the UPDATE_ITEM_HTS_IND is Y the items will be updated during HTS upload. If it is set to N, the items will not be updated during HTS upload.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.UPDATE_ORDER_HTS_IND IS 'Indicates if, during HTS upload, order/items should be automatically updated using new HTS information. This field is used only if the IMPORT_IND on SYSTEM_OPTIONS table is Y. If the UPDATE_ORDER_HTS_IND is Y then order/items will be updated during HTS upload. If it is set to N, then order/items will not be updated during HTS upload.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WAC_RECALC_ADJ_IND IS 'This wil determine if WAC will be recalculated when for adjustments on purchase order during receiving. Default value if NULL is N.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WF_DEFAULT_WH IS 'Warehouse that will be used as the default warehouse in the Franchise returns screen.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WF_HISTORY_MONTHS IS 'Specifies the number of months after which the Franchise Order and Return records are purged.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WF_NON_STOCK_SALES_HIST_DAYS IS 'This column determines how long non-stockholding franchise store sales information will be retained in the system. Any information that has exceeded this history days limit will be purged.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WF_ORDER_LEAD_DAYS IS 'The number of days in advance of the need date that franchise orders should be processed.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WH_CROSS_LINK_IND IS 'Indicates if the WH/Cross Link stock category will be used in replenishment. If the indicator is Y, then RMS will allow orders to be created for warehouses above the quantity currently planned to be available for in a warehouse, by creating a PO into the warehouse to ensure quantity will be available.  If N, this will not be displayed to users as a valid stock category for replenishment in RMS.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WH_STORE_ASSIGN_TYPE IS 'Specifies based on the replenishment stock category, which stores will be assigned to the specified warehouse by the warehouse store assignment batch program. Valid values include: W, Warehouse Stocked, to update the source warehouse for only the warehouse-store replenishment records. C, Cross-Docked, to update the source warehouse for only the cross-docked replenishment records. L, WH/Cross Link, to update the source warehouse for only the WH/Cross Link replenishment records. A, All, to update the source warehouse for all replenishment records.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WRONG_ST_RECEIPT_IND IS 'Determines whether or not the system will handle wrong store receipts, meaning the a store other than the intended store received a transfer.  Valid values are Y and N. When this indicator is ''Y'', if stock order is received at the wrong store, then RMS will adjust inventory and stock ledger transactions at both the actual receiving store and the intended location as though the actual receiving store was always the intended location.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WS_AUTO_CLOSE_DAYS IS 'This is the number of days after the create date or shipped date that warehouse to store stock orders will be closed automatically if not fully received.'
/

COMMENT ON COLUMN SYSTEM_OPTIONS.WW_AUTO_CLOSE_DAYS IS 'This is the number of days after the create date or shipped date that warehouse to warehouse stock orders will be closed automatically if not fully received.'
/
