--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE DROPPED:             CFA_CODE_DETAIL_DESCS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       TABLE DROP
--------------------------------------
PROMPT dropping 'CFA_CODE_DETAIL_DESCS'

DECLARE
  L_CFACodeDetailDescs_exists number := 0; 
BEGIN
  SELECT count(*) INTO L_CFACodeDetailDescs_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CFA_CODE_DETAIL_DESCS';

  if (L_CFACodeDetailDescs_exists != 0) then
      execute immediate 'DROP TABLE CFA_CODE_DETAIL_DESCS CASCADE CONSTRAINTS';
  end if;
end;
/
