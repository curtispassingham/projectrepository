--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
---------------------------------------------------------------------------
--  Table Dropped :				ORPOS_PRELOAD_RELATEDITEM_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Table               
--------------------------------------

PROMPT Dropping Table 'ORPOS_PRELOAD_RELATEDITEM_TEMP' 
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ORPOS_PRELOAD_RELATEDITEM_TEMP';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE ORPOS_PRELOAD_RELATEDITEM_TEMP';
  end if;
end;
/
