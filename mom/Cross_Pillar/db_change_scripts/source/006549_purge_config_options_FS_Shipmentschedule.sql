--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'PURGE_CONFIG_OPTIONS'
COMMENT ON COLUMN PURGE_CONFIG_OPTIONS.SHIP_SCHED_HISTORY_MTHS is 'Indicates the number of months of store ship schedule history should be retained by RMS.  All records older than this number of months will be deleted from the STORE_SHIP_SCHEDULE table by a batch process.'
/
