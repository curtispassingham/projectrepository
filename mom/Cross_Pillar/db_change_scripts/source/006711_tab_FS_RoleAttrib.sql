--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Table ROLE_ATTRIB              
--------------------------------------

PROMPT Dropping Foreign Key on 'ROLE_ATTRIB'
DECLARE
  l_table_role_attrib_exists number := 0;
BEGIN
  SELECT count(*) INTO l_table_role_attrib_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ROLE_ATTRIB';

  if (l_table_role_attrib_exists != 0) then
      execute immediate 'DROP TABLE ROLE_ATTRIB CASCADE CONSTRAINTS ';
  end if;
end;
/
