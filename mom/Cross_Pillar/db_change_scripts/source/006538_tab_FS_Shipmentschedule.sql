--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      TABLES to be DROPPED:    
                          STORE_SHIP_DATE
                          STORE_SHIP_DATE_TEMP
                          WH_BLACKOUT
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TABLES
--------------------------------------
PROMPT DROPPING Table 'STORE_SHIP_DATE'
DECLARE
  L_table_exists1 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_SHIP_DATE';

if (L_table_exists1 != 0) then
      execute immediate 'DROP TABLE STORE_SHIP_DATE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'STORE_SHIP_DATE_TEMP'
DECLARE
  L_table_exists2 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists2
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_SHIP_DATE_TEMP';

if (L_table_exists2 != 0) then
      execute immediate 'DROP TABLE STORE_SHIP_DATE_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'WH_BLACKOUT'
DECLARE
  L_table_exists3 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists3
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WH_BLACKOUT';

if (L_table_exists3 != 0) then
      execute immediate 'DROP TABLE WH_BLACKOUT CASCADE CONSTRAINTS';
  end if;
end;
/
