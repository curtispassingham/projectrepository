--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_BUYER_ORDERS_TO_APPROVE'
ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE MODIFY MARKUP_PERCENT NUMBER (20,4)
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.MARKUP_PERCENT is 'Markup is the difference between the cost of a good or service and its selling price'
/

