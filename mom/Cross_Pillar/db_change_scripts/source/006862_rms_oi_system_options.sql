--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_DAYS_AFTER_ITEM_CREATE NUMBER (3) DEFAULT 3 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_DAYS_AFTER_ITEM_CREATE is 'Defines the number of days after item creation after which the Item will appear in INCOMPLETE ITEMS report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_REF_ITEM VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_REF_ITEM is 'Configured to show Reference Items in Incomplete Items Report. Valid values are Y - to show Reference Items and N - do not show Reference Items.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_VAT VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_VAT is 'Configured to show VAT in Incomplete Items Report. Valid values are Y - to show VAT and N - do not show VAT.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_SPACK VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_SPACK is 'Configured to show Simple Pack in Incomplete Items Report. Valid values are Y - to show Simple Pack and N - do not show Simple pack.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_UDA VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_UDA is 'Configured to show UDA in Incomplete Items Report. Valid values are Y - to show UDA and N - do not show UDA.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_LOC VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_LOC is 'Configured to show Locations in Incomplete Items Report. Valid values are Y - to show Locations and N - do not show Locations.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_SEASONS VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_SEASONS is 'Configured to show Seasons/Phases in Incomplete Items Report. Valid values are Y - to show Seasons/Phases and N - do not show Seasons/Phases.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_REPL VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_REPL is 'Configured to show Replenishment in Incomplete Items Report. Valid values are Y - to show Replenishment and N - do not show Replenishment.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_SUBS_ITEM VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_SUBS_ITEM is 'Configured to show Substitute Items in Incomplete Items Report. Valid values are Y - to show Substitute Items and N - do not show Substitute Items.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_DIMEN VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_DIMEN is 'Configured to show Dimensions in Incomplete Items Report. Valid values are Y - to show Dimensions and N - do not show Dimensions.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_REL_ITEM VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_REL_ITEM is 'Configured to show Releated Itesm in Incomplete Items Report. Valid values are Y - to show Related Items and N - do not show Related Items.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_TICKETS VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_TICKETS is 'Configured to show Tickets in Incomplete Items Report. Valid values are Y - to show Tickets and N - do not show Tickets.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_HTS VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_HTS is 'Configured to show HTS in Incomplete Items Report. Valid values are Y - to show HTS and N - do not show HTS.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_IMP_ATTR VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_IMP_ATTR is 'Configured to show Import Attributes in Incomplete Items Report. Valid values are Y - to show Import Attributes and N - do not show Import Attributes.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD DS_SHOW_INCOMP_ITEM_IMAGES VARCHAR2 (1 ) DEFAULT 'R' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_IMAGES is 'Configured to show Images in Incomplete Items Report. Valid values are Y - to show Images and N - do not show Images.'
/


PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_REF_ITEM'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_REF_ITEM CHECK (DS_SHOW_INCOMP_ITEM_REF_ITEM IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_VAT'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_VAT CHECK (DS_SHOW_INCOMP_ITEM_VAT IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_SPACK'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_SPACK CHECK (DS_SHOW_INCOMP_ITEM_SPACK IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_UDA'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_UDA CHECK (DS_SHOW_INCOMP_ITEM_UDA IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_LOC'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_LOC CHECK (DS_SHOW_INCOMP_ITEM_LOC IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_SEASONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_SEASONS CHECK (DS_SHOW_INCOMP_ITEM_SEASONS IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_REPL'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_REPL CHECK (DS_SHOW_INCOMP_ITEM_REPL IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_SUBS_ITEM'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_SUBS_ITEM CHECK (DS_SHOW_INCOMP_ITEM_SUBS_ITEM IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_DIMEN'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_DIMEN CHECK (DS_SHOW_INCOMP_ITEM_DIMEN IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_REL_ITEM'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_REL_ITEM CHECK (DS_SHOW_INCOMP_ITEM_REL_ITEM IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_TICKETS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_TICKETS CHECK (DS_SHOW_INCOMP_ITEM_TICKETS IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_HTS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_HTS CHECK (DS_SHOW_INCOMP_ITEM_HTS IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_IMP_ATTR'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_IMP_ATTR CHECK (DS_SHOW_INCOMP_ITEM_IMP_ATTR IN ('R','N'))
/

PROMPT ADDING CONSTRAINT 'CHK_ROSO_DS_SHOW_IMAGES'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_IMAGES CHECK (DS_SHOW_INCOMP_ITEM_IMAGES IN ('R','N'))
/
