--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING SYNONYMS for RMS_ASYNC_USER
-------------------------------------------
PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$RMS_NOTIFICATION_QTAB';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$RMS_NOTIFICATION_QTAB';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_R'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$RMS_NOTIFICATION_QTAB_R';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$RMS_NOTIFICATION_QTAB_R';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_S'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$RMS_NOTIFICATION_QTAB_S';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$RMS_NOTIFICATION_QTAB_S';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_F'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_F';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_F';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_G'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_G';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_G';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_H'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_H';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_H';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_I'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_I';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_I';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_L'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_L';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_L';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_N'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_N';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_N';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_S'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_S';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_S';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'AQ$RMS_NOTIFICATION_QTAB_T'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'AQ$_RMS_NOTIFICATION_QTAB_T';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM AQ$_RMS_NOTIFICATION_QTAB_T';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'RMS_NOTIFICATION_QTAB'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'RMS_NOTIFICATION_QTAB';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM RMS_NOTIFICATION_QTAB';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'RMS_NOTIFICATION_REC'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'RMS_NOTIFICATION_REC';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM RMS_NOTIFICATION_REC';
  end if;
end;
/

PROMPT DROPPING SYNONYM 'SYS_IOT_OVER_35249354'
DECLARE
  L_synonym_exists number := 0;
BEGIN
  SELECT count(*) INTO L_synonym_exists
    FROM USER_SYNONYMS
   WHERE SYNONYM_NAME = 'SYS_IOT_OVER_35249354';

  if (L_synonym_exists != 0) then
      execute immediate 'DROP SYNONYM SYS_IOT_OVER_35249354';
  end if;
end;
/
