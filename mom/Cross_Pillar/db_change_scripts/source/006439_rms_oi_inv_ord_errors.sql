--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_ORD_ERRORS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_ORD_ERRORS'
CREATE TABLE RMS_OI_INV_ORD_ERRORS
 (SESSION_ID NUMBER(15,0),
  ORDERS_PAST_NAD_IND VARCHAR2(1 ),
  ORDERS_TO_CLOSE_IND VARCHAR2(1 ),
  NOT_APPROVED_ORDERS_IND VARCHAR2(1 ),
  ONCE_APPROVED_ORDERS_IND VARCHAR2(1 ),
  MISSING_ORDER_DATA_IND VARCHAR2(1 ),
  MISSING_ITEM_DATA_IND VARCHAR2(1 ),
  ORDER_NO NUMBER(12,0),
  SUPPLIER_SITE NUMBER(10,0),
  STATUS VARCHAR2(1 ),
  NOT_BEFORE_DATE DATE,
  NOT_AFTER_DATE DATE,
  OTB_EOW_DATE DATE,
  TOTAL_COST NUMBER(20,4),
  RECEIVED_PERCENT NUMBER(20,10),
  IMPORT_ORDER_IND VARCHAR2(1 ),
  MASTER_PO_NO NUMBER(12,0)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_INV_ORD_ERRORS is 'This table holds data for the Inventory Analyst Order Errors Detail Report.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.ORDERS_PAST_NAD_IND is 'Indicates if order is in not after date tile.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.ORDERS_TO_CLOSE_IND is 'Indicates if order is in to close tile.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.NOT_APPROVED_ORDERS_IND is 'Indicates if order is in not approved tile.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.ONCE_APPROVED_ORDERS_IND is 'Indicates if order is in once approved tile.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.MISSING_ORDER_DATA_IND is 'Indicates if order is in missing data tile.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.MISSING_ITEM_DATA_IND is 'Indicates if order is in missing item data tile.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.ORDER_NO is 'The order number.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.SUPPLIER_SITE is 'The supplier of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.STATUS is 'The status of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.NOT_BEFORE_DATE is 'The not before date of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.NOT_AFTER_DATE is 'The not after date of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.OTB_EOW_DATE is 'The otb_eow_date of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.TOTAL_COST is 'The total cost of the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.RECEIVED_PERCENT is 'The received percent for the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.IMPORT_ORDER_IND is 'The import order indicator for the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.MASTER_PO_NO is 'The master PO for the order.'
/


PROMPT Creating Index on 'RMS_OI_INV_ORD_ERRORS'
 CREATE INDEX RMS_OI_INV_ORD_ERRORS_I1 on RMS_OI_INV_ORD_ERRORS
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

