--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               RTK_ERRORS_TL
--      Modifying Table:  							   RTK_ERRORS	
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT Creating table 'RTK_ERRORS_TL'
CREATE TABLE RTK_ERRORS_TL(
LANG NUMBER(6) NOT NULL,
RTK_KEY VARCHAR2(25) NOT NULL,
RTK_TEXT VARCHAR2(255) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RTK_ERRORS_TL is 'This is the translation table for RTK_ERRORS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN RTK_ERRORS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN RTK_ERRORS_TL.RTK_KEY is 'Contains a key for the erorr message.'
/

COMMENT ON COLUMN RTK_ERRORS_TL.RTK_TEXT is 'Contains the actual text of the message.'
/

COMMENT ON COLUMN RTK_ERRORS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN RTK_ERRORS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN RTK_ERRORS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN RTK_ERRORS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

PROMPT Creating Primary Key on 'RTK_ERRORS_TL'
ALTER TABLE RTK_ERRORS_TL ADD CONSTRAINT PK_RTK_ERRORS_TL PRIMARY KEY (
LANG,
RTK_KEY
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/


PROMPT Creating Foreign Key on 'RTK_ERRORS_TL'
ALTER TABLE RTK_ERRORS_TL
 ADD CONSTRAINT RETL_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT COPYING NON-PRIMARY LANGUAGE ENTRY FROM RTK_ERRORS TO RTK_ERRORS_TL
INSERT INTO RTK_ERRORS_TL (LANG,RTK_KEY,RTK_TEXT,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID)
   select rtk_lang, 
          rtk_key, 
          RTK_TEXT, 
          sysdate, 
          user, 
          sysdate, 
          user
     from rtk_errors re,
          system_config_options sco
    where re.rtk_lang <> sco.data_integration_lang
      and exists (select 1 
                    from rtk_errors re2
                   where re2.rtk_key = re.rtk_key
                     and re2.rtk_lang = sco.data_integration_lang)
/

commit
/

PROMPT DROPPING PRIMARY KEY ON TABLE 'RTK_ERRORS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_RTK_ERRORS'
     AND constraint_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RTK_ERRORS DROP CONSTRAINT PK_RTK_ERRORS DROP INDEX';
  end if;
end;
/

PROMPT DELETE NON-PRIMARY RECORDS FROM 'RTK_ERRORS' TABLE.
delete from rtk_errors re
 where exists (select 1 
                 from system_config_options sco
                where re.rtk_lang <> sco.data_integration_lang)
/                

commit
/

PROMPT DROPPING RTK_LANG COLUMN FROM TABLE 'RTK_ERRORS'
ALTER TABLE RTK_ERRORS DROP COLUMN RTK_LANG
/

PROMPT CREATING PRIMARY KEY ON 'RTK_ERRORS'
ALTER TABLE RTK_ERRORS ADD CONSTRAINT PK_RTK_ERRORS PRIMARY KEY (
RTK_KEY
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING FOREIGN KEY ON 'RTK_ERRORS_TL'
ALTER TABLE RTK_ERRORS_TL ADD CONSTRAINT RETL_RER_FK FOREIGN KEY (
RTK_KEY
) REFERENCES RTK_ERRORS (
RTK_KEY
)
/



