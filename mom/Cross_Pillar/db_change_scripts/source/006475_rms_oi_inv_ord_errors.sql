--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ERRORS DROP COLUMN ORDERS_PAST_NAD_IND
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS DROP COLUMN ORDERS_TO_CLOSE_IND
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS DROP COLUMN NOT_APPROVED_ORDERS_IND
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS DROP COLUMN ONCE_APPROVED_ORDERS_IND
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS DROP COLUMN MISSING_ORDER_DATA_IND
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS DROP COLUMN MISSING_ITEM_DATA_IND
/

