CREATE OR REPLACE FORCE VIEW V_LANG_TL (LANG_LANG, DESCRIPTION, LANG ) AS
SELECT  b.lang lang_lang,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  LANG b,
        LANG_TL tl
 WHERE  b.lang = tl.lang_lang (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_LANG_TL is 'This is the translation view for base table LANG. This view fetches data in user langauge either from translation table LANG_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_LANG_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_LANG_TL.LANG_LANG is 'Contains a number which uniquely identifies a language.'
/

COMMENT ON COLUMN V_LANG_TL.DESCRIPTION is 'Contains a description or name for the language.'
/

