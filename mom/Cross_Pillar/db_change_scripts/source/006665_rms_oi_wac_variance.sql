--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_WAC_VARIANCE'
ALTER TABLE RMS_OI_WAC_VARIANCE ADD NUM_LOCS_WAC_ISSUE_ITEM NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.NUM_LOCS_WAC_ISSUE_ITEM is 'Number of locations with WAC variance issue for this item '
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD NUM_ITEMS_WAC_ISSUE_LOC NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.NUM_ITEMS_WAC_ISSUE_LOC is 'Number of items with WAC variance issue for this location'
/

