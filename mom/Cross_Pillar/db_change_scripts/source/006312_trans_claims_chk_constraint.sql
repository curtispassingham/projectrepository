--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  MODIFYING CONSTARINT:               CHK_TRANS_CLAIMS_DAMAGE_CODE
----------------------------------------------------------------------------

whenever sqlerror exit

------------------------------------------------------
--  TABLE 				TRANS_CLAIMS
------------------------------------------------------
DECLARE
  L_constraints_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_TRANS_CLAIMS_DAMAGE_CODE'
     AND constraint_TYPE = 'C';

  if (L_constraints_exists != 0) then
      execute immediate 'ALTER TABLE TRANS_CLAIMS drop CONSTRAINT CHK_TRANS_CLAIMS_DAMAGE_CODE';
  end if;
end;
/

DELETE FROM TRANS_CLAIMS WHERE DAMAGE_CODE = 'A'
/

ALTER TABLE TRANS_CLAIMS ADD CONSTRAINT CHK_TRANS_CLAIMS_DAMAGE_CODE CHECK (damage_code in ('S','E','C'))
/