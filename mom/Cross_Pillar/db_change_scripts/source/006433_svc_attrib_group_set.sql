--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ATTRIB_GROUP_SET'
ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY ACTION NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY PROCESS$STATUS DEFAULT 'N' NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY GROUP_SET_ID NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.GROUP_SET_ID is 'Unique idenfier for the for the attribute group set.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY BASE_RMS_TABLE NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.BASE_RMS_TABLE is 'RMS table where the group set belongs to.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY DISPLAY_SEQ NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.DISPLAY_SEQ is 'The order at which the group set entries are displayed on the UI.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY GROUP_SET_VIEW_NAME NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.GROUP_SET_VIEW_NAME is 'This column holds the name of the database view that will be generated to make access to user entered data easier.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY CREATE_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.CREATE_ID is 'User who created the record.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY CREATE_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.CREATE_DATETIME is 'Date time when record was inserted.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY LAST_UPD_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.LAST_UPD_ID is 'User who last updated the record.'
/

ALTER TABLE SVC_ATTRIB_GROUP_SET MODIFY LAST_UPD_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/

