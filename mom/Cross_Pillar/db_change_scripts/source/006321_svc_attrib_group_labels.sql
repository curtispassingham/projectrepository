--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ATTRIB_GROUP_LABELS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ATTRIB_GROUP_LABELS'
CREATE TABLE SVC_ATTRIB_GROUP_LABELS
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ) NOT NULL,
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N' NOT NULL,
  GROUP_ID NUMBER(10) NOT NULL,
  LANG NUMBER(6) NOT NULL,
  LABEL VARCHAR2(255 ) NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  LAST_UPD_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPD_DATETIME DATE NOT NULL
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

COMMENT ON TABLE SVC_ATTRIB_GROUP_LABELS is 'This is the staging table for CFAS attrib group labels information.It is used to temporarily hold data before it is uploaded/updated in CFA_ATTRIB_GROUP_LABELS.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.CHUNK_ID is 'Uniquely identifies a chunk.The value will always be 1.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.ROW_SEQ is 'The rows sequence. Should be unique within a process-ID.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.GROUP_ID is 'This column holds a generated ID that distinguishes the custom attribute group.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LANG is 'The attribute group labels language.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LABEL is 'The attribute group label as displayed on the UI.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_ATTRIB_GROUP_LABELS'
ALTER TABLE SVC_ATTRIB_GROUP_LABELS
 ADD CONSTRAINT SVC_ATTRIB_GROUP_LABELS_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_ATTRIB_GROUP_LABELS'
ALTER TABLE SVC_ATTRIB_GROUP_LABELS
 ADD CONSTRAINT SVC_ATTRIB_GROUP_LABELS_UK UNIQUE
  (GROUP_ID,
   LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

