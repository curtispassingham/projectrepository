--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

------------------------------------------------------
--...Deleting records with obsolete pos_config_type..-
------------------------------------------------------

DELETE FROM POS_STORE WHERE POS_CONFIG_TYPE IN ('SPAY', 'MORD', 'PYIO', 'TTYP', 'BTTN')
/

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'POS_STORE'

PROMPT MODIFYING CONSTRAINT 'CHK_POS_STORE_POS_CONFIG_TYPE'
ALTER TABLE POS_STORE DROP CONSTRAINT  CHK_POS_STORE_POS_CONFIG_TYPE
/
ALTER TABLE POS_STORE ADD CONSTRAINT
 CHK_POS_STORE_POS_CONFIG_TYPE CHECK (POS_CONFIG_TYPE IN ('COUP', 'MORD', 'PYIO', 'TTYP', 'BTTN', 'PRES'))
/
