--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'FUNCTIONAL_CONFIG_OPTIONS'
ALTER TABLE FUNCTIONAL_CONFIG_OPTIONS ADD RPM_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN FUNCTIONAL_CONFIG_OPTIONS.RPM_IND is 'Indicates whether RPM is used as a pricing application or not.'
/


PROMPT ADDING CONSTRAINT 'CHK_FUN_CONG_OPT_RPM_IND'
ALTER TABLE FUNCTIONAL_CONFIG_OPTIONS ADD CONSTRAINT
 CHK_FUN_CONG_OPT_RPM_IND CHECK (RPM_IND IN ('Y','N'))
/
