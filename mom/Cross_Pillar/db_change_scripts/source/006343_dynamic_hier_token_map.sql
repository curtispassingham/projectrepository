--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 DYNAMIC_HIER_TOKEN_MAP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'DYNAMIC_HIER_TOKEN_MAP'
CREATE TABLE DYNAMIC_HIER_TOKEN_MAP
 (TOKEN VARCHAR2(10 ) NOT NULL,
  RMS_NAME VARCHAR2(40 ) NOT NULL,
  CLIENT_NAME VARCHAR2(40 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DYNAMIC_HIER_TOKEN_MAP is 'This table contains the mapping of dynamic tokens to its name. This table is used by the installation script during initial install and during patch install for database and the application. If the client wants to maintain the hierarchy name different than the default RMS name, they will have to update the client name for the applicable tokens.'
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP.TOKEN is 'This field contains the hierarchy token present in the seed data and in application bundle file. '
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP.RMS_NAME is 'This is the default RMS name for this token'
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP.CLIENT_NAME is 'The client can define their own name for this token. The value in this column will take precedence to RMS_NAME column when tokens gets replaced. '
/

PROMPT CREATING PRIMARY KEY ON 'DYNAMIC_HIER_TOKEN_MAP'
ALTER TABLE DYNAMIC_HIER_TOKEN_MAP
 ADD CONSTRAINT PK_DYNAMIC_HIER_TOKEN_MAP PRIMARY KEY
  (TOKEN
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
