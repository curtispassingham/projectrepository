--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      SEQUENCES to be DROPPED:    
                          STOP_SHIP_NO_SEQUENCE
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING SEQUENCES
--------------------------------------
PROMPT DROPPING sequence STOP_SHIP_NO_SEQUENCE
DECLARE
  L_seq_exists number := 0;
BEGIN
  SELECT count(*) INTO L_seq_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STOP_SHIP_NO_SEQUENCE';

  if (L_seq_exists != 0) then
      execute immediate 'DROP SEQUENCE STOP_SHIP_NO_SEQUENCE';
  end if;
end;
/