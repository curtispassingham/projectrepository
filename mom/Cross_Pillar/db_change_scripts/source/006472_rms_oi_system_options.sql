--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_NEG_INV_INCLUDE NUMBER (12,4) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_NEG_INV_INCLUDE is 'Controls if an item/loc will be included in the inventory control negative retail report.  Item/Locs with an inventory level less that this value will be included.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_NEG_INV_WARN NUMBER (12,4) DEFAULT 20 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_NEG_INV_WARN is 'Controls the inventory level an item/loc needs to be under to be classified in the warning level in the inventory control negative retail report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_NEG_INV_CRITIAL NUMBER (12,4) DEFAULT 40 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_NEG_INV_CRITIAL is 'Controls the inventory level an item/loc needs to be under to be classified in the critical level in the inventory control negative retail report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_OVERDUE_SHIP_DAYS NUMBER (5,0) DEFAULT 3 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_OVERDUE_SHIP_DAYS is 'Controls if an shipment will be classified as overdue in the overdue shipment reports.  Controls level of overdue.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_OVERDUE_SHIP_COUNT NUMBER (5,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_OVERDUE_SHIP_COUNT is 'Controls if the level of overdue in the overdue shipment reports.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_UNEXP_INV_INACTIVE_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_UNEXP_INV_INACTIVE_IND is 'Controls if inactive item/locs are included in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_UNEXP_INV_DISCONTINUE_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_UNEXP_INV_DISCONTINUE_IND is 'Controls if discontinued item/locs are included in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_UNEXP_INV_DELETE_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_UNEXP_INV_DELETE_IND is 'Controls if deleted item/locs are included in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_UNEXP_INV_INCL_QTY NUMBER (12,4) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_UNEXP_INV_INCL_QTY is 'Controls the inventory level for item/locs to be included in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_UNEXP_INV_CRITIAL_COUNT NUMBER (10,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_UNEXP_INV_CRITIAL_COUNT is 'Controls the number of item/locs when exceeded causes the unexpected inventory report to be considered critical in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_TSF_APPRV_PEND_CRITIAL_DAYS NUMBER (10,0) DEFAULT 3 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_TSF_APPRV_PEND_CRITIAL_DAYS is 'Controls the age of transfers to be included in the inventory control transfer pending approval report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_TSF_APPRV_PEND_CRITIAL_CNT NUMBER (10,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_TSF_APPRV_PEND_CRITIAL_CNT is 'Controls the number of transfers when exceeded causes the inventory control transfer pending approval report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_STKORD_CLS_PEND_CRIT_DAYS NUMBER (10,0) DEFAULT 5 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_STKORD_CLS_PEND_CRIT_DAYS is 'Controls the age of stock orders to be included in the inventory control stock order pending closure report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_STKORD_CLS_PEND_CRIT_CNT NUMBER (10,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_STKORD_CLS_PEND_CRIT_CNT is 'Controls the number of transfers when exceeded causes the inventory control stock order pending closure report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_MISS_STK_COUNT_CRIT_DAYS NUMBER (10,0) DEFAULT 3 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_MISS_STK_COUNT_CRIT_DAYS is 'Controls the age of stock count to be included in the inventory control missing stock count report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_MISS_STK_COUNT_CRIT_CNT NUMBER (10,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_MISS_STK_COUNT_CRIT_CNT is 'Controls the number of stock counts when exceeded causes the inventory control missing stock count report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_STK_COUNT_VAR_PCT NUMBER (10,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_STK_COUNT_VAR_PCT is 'Controls the variance percent for stock counts to be included in the inventory control stock count variance report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IC_STK_COUNT_VAR_LOC_CNT NUMBER (10,0) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IC_STK_COUNT_VAR_LOC_CNT is 'Controls the number of locations on a stock count that exceed the IC_STK_COUNT_VAR_PCT that when exceeded causes the inventory control stock count variance report to be considered critical.'
/

