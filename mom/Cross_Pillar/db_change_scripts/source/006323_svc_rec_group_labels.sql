--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_REC_GROUP_LABELS'
ALTER TABLE SVC_REC_GROUP_LABELS MODIFY LANG NUMBER (6) 
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.LANG is 'The LOV labels language.'
/

