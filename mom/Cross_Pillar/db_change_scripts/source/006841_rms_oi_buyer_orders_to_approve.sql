--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_BUYER_ORDERS_TO_APPROVE'
ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE MODIFY TOTAL_UNITS NUMBER (*,4)
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.TOTAL_UNITS is 'Contains the total number of items ordered. Declared as NUMBER(*,4) to allow greater number of digits for the precision because it holds a total value.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE MODIFY TOTAL_COST NUMBER (*,4)
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.TOTAL_COST is 'Holds the total cost of the order. Declared as NUMBER(*,4) to allow greater number of digits for the precision because it holds a total value.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE MODIFY TOTAL_RETAIL NUMBER (*,4)
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.TOTAL_RETAIL is 'Holds the total retail of the order. Declared as NUMBER(*,4) to allow greater number of digits for the precision because it holds a total value.'
/

ALTER TABLE RMS_OI_BUYER_ORDERS_TO_APPROVE MODIFY MARKUP_PERCENT NUMBER (*,4)
/

COMMENT ON COLUMN RMS_OI_BUYER_ORDERS_TO_APPROVE.MARKUP_PERCENT is 'Markup is the difference between the cost of a good or service and its selling price. Declared as NUMBER(*,4) to allow greater number of digits for the precision because it holds a value based on the total.'
/

