--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_OVERDUE_SHIP_ALLOC
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_OVERDUE_SHIP_ALLOC'
CREATE TABLE RMS_OI_OVERDUE_SHIP_ALLOC
 (SESSION_ID NUMBER(15,0),
  ALLOC_NO NUMBER(10,0),
  FROM_LOC NUMBER(10,0),
  FROM_LOC_NAME VARCHAR2(150 ),
  FROM_LOC_TYPE VARCHAR2(1 ),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  RELEASE_DATE DATE,
  ALLOC_COST NUMBER(20,4),
  ALLOC_RETAIL NUMBER(20,4),
  ALLOC_QTY NUMBER(12,4),
  CURRENCY_CODE VARCHAR2(3 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_OVERDUE_SHIP_ALLOC is 'This table holds the information for the Allocations Pending Close Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.ALLOC_NO is 'The allocation.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.FROM_LOC is 'The allocation from location.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.FROM_LOC_NAME is 'The allocation from location name.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.FROM_LOC_TYPE is 'The allocation from location type.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.ITEM is 'The item.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.ITEM_DESC is 'The item description.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.RELEASE_DATE is 'The release date.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.ALLOC_COST is 'The allocation cost.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.ALLOC_RETAIL is 'The allocation retail.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.ALLOC_QTY is 'The allocation quantity.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_ALLOC.CURRENCY_CODE is 'The currency code.'
/


PROMPT Creating Index on 'RMS_OI_OVERDUE_SHIP_ALLOC'
 CREATE INDEX RMS_OI_OVERDUE_SHIP_ALLOC_I1 on RMS_OI_OVERDUE_SHIP_ALLOC
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

