--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RESA_OI_SYSTEM_OPTIONS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RESA_OI_SYSTEM_OPTIONS'
CREATE TABLE RESA_OI_SYSTEM_OPTIONS
 (OVER_THRESHOLD NUMBER(5) DEFAULT 0 NOT NULL,
  SHORT_THRESHOLD NUMBER(5) DEFAULT 0 NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RESA_OI_SYSTEM_OPTIONS is 'This table holds the configurations related to ReSA dashboards.'
/

COMMENT ON COLUMN RESA_OI_SYSTEM_OPTIONS.OVER_THRESHOLD is 'Controls the over amount threshold'
/

COMMENT ON COLUMN RESA_OI_SYSTEM_OPTIONS.SHORT_THRESHOLD is 'Controls the short amout threshold'
/

