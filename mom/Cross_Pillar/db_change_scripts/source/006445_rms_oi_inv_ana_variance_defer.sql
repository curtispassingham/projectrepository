--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_ANA_VARIANCE_DEFER
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_ANA_VARIANCE_DEFER'
CREATE TABLE RMS_OI_INV_ANA_VARIANCE_DEFER
 (SESSION_ID NUMBER(15,0) NOT NULL,
  DEFER_TO_DATE DATE NOT NULL,
  DEFER_USER VARCHAR2(30 ) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  ALC_ITEM_TYPE VARCHAR2(15 ),
  AGG_DIFF_1 VARCHAR2(10 ),
  AGG_DIFF_2 VARCHAR2(10 ),
  AGG_DIFF_3 VARCHAR2(10 ),
  AGG_DIFF_4 VARCHAR2(10 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_INV_ANA_VARIANCE_DEFER is 'This table holds defered item data for the Inventory Analyst Inventory Variance to Forecast Report.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.DEFER_TO_DATE is 'The date to defer the item to.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.DEFER_USER is 'The user who is defering the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.ITEM is 'The item for the row.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.ALC_ITEM_TYPE is 'The alc_item_type of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.AGG_DIFF_1 is 'The aggregate diff 1 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.AGG_DIFF_2 is 'The aggregate diff 2 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.AGG_DIFF_3 is 'The aggregate diff 3 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE_DEFER.AGG_DIFF_4 is 'The aggregate diff 4 for the item.'
/


PROMPT Creating Index on 'RMS_OI_INV_ANA_VARIANCE_DEFER'
 CREATE INDEX RMS_OI_INV_ANA_VAR_DEFER_I1 on RMS_OI_INV_ANA_VARIANCE_DEFER
 (DEFER_USER,
   DEFER_TO_DATE
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

