--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------
 
 
----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
--    The customer DBA is responsible to review this script to ensure
--    data is preserved as desired.
--
----------------------------------------------------------------------------
--    PACKAGE DROPPED:                POS_RESEND_TO_STORE_SQL
----------------------------------------------------------------------------
 
whenever sqlerror exit
 
--------------------------------------------
--      DROPPING PACKAGE
--------------------------------------------
 
DECLARE
  L_package_exists number := 0;
BEGIN
 SELECT count(*)INTO L_package_exists
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='POS_RESEND_TO_STORE_SQL'
    AND OBJECT_TYPE ='PACKAGE';

  
  if (L_package_exists != 0)then
      execute immediate'DROP PACKAGE POS_RESEND_TO_STORE_SQL' ;
 end if;
end;
/
 