--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RETAIL_SERVICE_REPORT_URL'

COMMENT ON TABLE RETAIL_SERVICE_REPORT_URL is 'This new table will be used to hold the retail service code, retail service name, retail service type and URL for the new web services for ORFI. '
/