--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop Table         TL_COLUMNS      
--------------------------------------
PROMPT dropping Table 'TL_COLUMNS'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TL_COLUMNS';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE TL_COLUMNS CASCADE CONSTRAINTS';
  end if;
end;
/

