--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_REC_GROUP'
ALTER TABLE SVC_REC_GROUP MODIFY ACTION NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

ALTER TABLE SVC_REC_GROUP MODIFY PROCESS$STATUS DEFAULT 'N' NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

ALTER TABLE SVC_REC_GROUP MODIFY REC_GROUP_ID NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.REC_GROUP_ID is 'Unique identifier for the reference record group query.'
/

ALTER TABLE SVC_REC_GROUP MODIFY REC_GROUP_NAME NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.REC_GROUP_NAME is 'The name of the record group query. This is optional.'
/

ALTER TABLE SVC_REC_GROUP MODIFY QUERY_TYPE NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.QUERY_TYPE is 'Contains the query type value.  Valid values are Simple and Complex.  Simple queries can be created/maintained online.  Complex queries must be entered/maintained by a DBA and can be viewed online.'
/

ALTER TABLE SVC_REC_GROUP MODIFY CREATE_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.CREATE_ID is 'User who created the record.'
/

ALTER TABLE SVC_REC_GROUP MODIFY CREATE_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.CREATE_DATETIME is 'Date time when record was inserted.'
/

ALTER TABLE SVC_REC_GROUP MODIFY LAST_UPD_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.LAST_UPD_ID is 'User who last updated the record.'
/

ALTER TABLE SVC_REC_GROUP MODIFY LAST_UPD_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_REC_GROUP.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/

