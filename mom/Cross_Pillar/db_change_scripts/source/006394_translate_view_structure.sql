--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 TRANSLATE_VIEW_STRUCTURE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'TRANSLATE_VIEW_STRUCTURE'
CREATE TABLE TRANSLATE_VIEW_STRUCTURE
 (TRANSLATE_TABLE VARCHAR2(30 ) NOT NULL,
  LANG NUMBER(6) NOT NULL,
  KEY_NUMBER_1 NUMBER(20),
  KEY_NUMBER_2 NUMBER(20),
  KEY_NUMBER_3 NUMBER(20),
  KEY_NUMBER_4 NUMBER(20),
  KEY_VARCHAR2_5 VARCHAR2(255 ),
  KEY_VARCHAR2_6 VARCHAR2(255 ),
  KEY_VARCHAR2_7 VARCHAR2(255 ),
  KEY_VARCHAR2_8 VARCHAR2(255 ),
  KEY_VARCHAR2_9 VARCHAR2(255 ),
  KEY_DATE_10 DATE,
  KEY_DATE_11 DATE,
  TEXT_1 VARCHAR2(2000 ),
  TEXT_2 VARCHAR2(2000 ),
  TEXT_3 VARCHAR2(2000 ),
  TEXT_4 VARCHAR2(2000 ),
  TEXT_5 VARCHAR2(2000 ),
  TEXT_6 VARCHAR2(2000 ),
  TEXT_7 VARCHAR2(2000 ),
  TEXT_8 VARCHAR2(2000 ),
  TEXT_9 VARCHAR2(2000 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TRANSLATE_VIEW_STRUCTURE is 'This is dummy table and will always be empty. This table along with translate_metadata table is used by the global translation screen to dynamically render the screen. This table defines the structure of how the translation data is held by the programmatic view object. The actual data is fetched through package call in the application code. '
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TRANSLATE_TABLE is 'Contains the translation table name'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.LANG is 'Indicates the language code. '
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_NUMBER_1 is 'Numeric key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_NUMBER_2 is 'Numeric key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_NUMBER_3 is 'Numeric key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_NUMBER_4 is 'Numeric key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_VARCHAR2_5 is 'Text key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_VARCHAR2_6 is 'Text key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_VARCHAR2_7 is 'Text key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_VARCHAR2_8 is 'Text key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_VARCHAR2_9 is 'Text key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_DATE_10 is 'Date key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.KEY_DATE_11 is 'Date key field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_1 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_2 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_3 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_4 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_5 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_6 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_7 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_8 is 'Text field'
/

COMMENT ON COLUMN TRANSLATE_VIEW_STRUCTURE.TEXT_9 is 'Text field'
/


PROMPT Creating Primary Key on 'TRANSLATE_VIEW_STRUCTURE'
ALTER TABLE TRANSLATE_VIEW_STRUCTURE
 ADD CONSTRAINT PK_TRANSLATE_VIEW_STRUCTURE PRIMARY KEY
  (TRANSLATE_TABLE,
   LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

