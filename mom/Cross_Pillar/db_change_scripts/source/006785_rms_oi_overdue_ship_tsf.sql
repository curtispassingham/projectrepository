--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_OVERDUE_SHIP_TSF'
ALTER TABLE RMS_OI_OVERDUE_SHIP_TSF DROP COLUMN TSF_COST
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_TSF DROP COLUMN TSF_RETAIL
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_TSF DROP COLUMN CURRENCY_CODE
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_TSF ADD TSF_QTY NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TSF_QTY is 'The total quantity for the transfer.'
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_TSF ADD MULTI_UOM_IND VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.MULTI_UOM_IND is 'Indicates if the items on the transfer have more than 1 standard UOM between them.  Possible values are Y or N.'
/

