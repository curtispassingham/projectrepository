--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 GTT_INCOMP_ITEMS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'GTT_INCOMP_ITEMS'
CREATE GLOBAL TEMPORARY TABLE GTT_INCOMP_ITEMS
 (REQD_COMPLETION NUMBER(3),
  ITEM_PARENT VARCHAR2(25 ),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  STATUS VARCHAR2(1 ),
  ITEM_LEVEL NUMBER(1),
  TRAN_LEVEL NUMBER(1),
  DISPLAY_IND VARCHAR2(1 ),
  DEPT NUMBER(4),
  DEPT_NAME VARCHAR2(120 ),
  CLASS NUMBER(4),
  CLASS_NAME VARCHAR2(120 ),
  SUBCLASS NUMBER(4),
  SUBCLASS_NAME VARCHAR2(120 ),
  CREATE_DATE DATE,
  CREATE_ID VARCHAR2(30 ),
  PRIM_SUPPLIER NUMBER(10),
  PRIM_SUPPLIER_NAME VARCHAR2(240 ),
  PRIM_COUNTRY VARCHAR2(3 ),
  UNIT_COST NUMBER(20,4),
  SELLING_RETAIL NUMBER(20,4),
  SUP_CURRENCY VARCHAR2(3 ),
  VPN VARCHAR2(30 ),
  PACK_IND VARCHAR2(1 ),
  REF_ITEMS NUMBER(3),
  VAT NUMBER(3),
  SIMPLE_PACK NUMBER(3),
  UDA NUMBER(3),
  LOCATION NUMBER(3),
  SEASONS NUMBER(3),
  REPLENISHMENT NUMBER(3),
  SUBS_ITEMS NUMBER(3),
  DIMENSIONS NUMBER(3),
  RELATED_ITEMS NUMBER(3),
  TICKET NUMBER(3),
  HTS NUMBER(3),
  IMPORT_ATTR NUMBER(3),
  IMAGES NUMBER(3)
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE GTT_INCOMP_ITEMS is 'This global temporary table is used to populate data for Incomplete Items report.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.REQD_COMPLETION is 'Indicates the percentage of completion for the all parameteres configured as ''Required''.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.ITEM_PARENT is 'Parent Item.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.ITEM is 'Item.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.ITEM_DESC is 'Item description.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.STATUS is 'Item status.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.ITEM_LEVEL is 'Item Level.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.TRAN_LEVEL is 'Transaction Level.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.DISPLAY_IND is 'Whether the item is to be displayed or not.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.DEPT is 'Department to which item belongs.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.DEPT_NAME is 'Department name.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.CLASS is 'Class to which item belongs.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.CLASS_NAME is 'Class name.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SUBCLASS is 'Subclass to which item belongs.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SUBCLASS_NAME is 'Subclass name.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.CREATE_DATE is 'Item Create Date.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.CREATE_ID is 'Item Create ID.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.PRIM_SUPPLIER is 'Primary Supplier Site of the Item.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.PRIM_SUPPLIER_NAME is 'Primary Supplier Site name.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.PRIM_COUNTRY is 'Primary Country of the item.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.UNIT_COST is 'Unit cost of the item.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SELLING_RETAIL is 'Selling retail.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SUP_CURRENCY is 'Supplier Currency.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.VPN is 'VPN.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.PACK_IND is 'Indicates if the Item is a pack.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.REF_ITEMS is 'Reference Items.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.VAT is 'Value Added Tax.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SIMPLE_PACK is 'Simple Pack.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.UDA is 'UDA.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.LOCATION is 'Item Location.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SEASONS is 'Seasons/Phases.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.REPLENISHMENT is 'Replenishment.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.SUBS_ITEMS is 'Substitute Items.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.DIMENSIONS is 'Dimensions.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.RELATED_ITEMS is 'Related Items.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.TICKET is 'Ticket.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.HTS is 'HTS.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.IMPORT_ATTR is 'Import Attributes.'
/

COMMENT ON COLUMN GTT_INCOMP_ITEMS.IMAGES is 'Item Images.'
/

