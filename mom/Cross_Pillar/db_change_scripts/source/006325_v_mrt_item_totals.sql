--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_MRT_ITEM_TOTALS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_MRT_ITEM_TOTALS'
CREATE OR REPLACE FORCE VIEW V_MRT_ITEM_TOTALS ("MRT_NO", "ITEM_PARENT", "ITEM","ITEM_DESC", "DIFF_1", "DIFF_2", "DIFF_3", "DIFF_4", "RESTOCK_PCT", "RTV_COST", "SELECTED_IND", "AVAIL_RETURN_QTY", "TSF_QTY", "TOTAL_RETAIL", "TOTAL_COST", "TOTAL_PRICE", "RECEIVED_QTY", "LOCATION", "SUPPLIER", "SUPPLIER_TOTAL_UNIT_RETAIL", "PRIMARY_TOTAL_UNIT_RETAIL", "SUPPLIER_TOTAL_COST", "PRIMARY_TOTAL_COST", "SUPPLIER_TOTAL_PRICE", "PRIMARY_TOTAL_PRICE", "SUPPLIER_RTV_COST", "PRIMARY_RTV_COST", "FINAL_TSF_COST", "SUPPLIER_FINAL_TSF_COST", "PRIMARY_FINAL_TSF_COST")
AS
  SELECT inline.mrt_no,
    inline.item_parent item_parent,
    inline.item item,
    inline.item_desc item_desc,
    inline.diff_1,
    inline.diff_2,
    inline.diff_3,
    inline.diff_4,
    inline.restock_pct,
    inline.rtv_cost,
    inline.selected_ind,
    inline.avail_return_qty,
    inline.tsf_qty,
    inline.total_retail,
    inline.total_cost,
    inline.total_price,
    inline.received_qty,
    mr.wh location,
    mr.supplier supplier,
    CURRENCY_SQL.CONVERT_VALUE('R', su.currency_code, mr.currency_code, inline.total_retail ) supplier_total_unit_retail,
    CURRENCY_SQL.CONVERT_VALUE('R', so.currency_code, mr.currency_code, inline.total_retail ) primary_total_unit_retail,
    CURRENCY_SQL.CONVERT_VALUE('C', su.currency_code, mr.currency_code, inline.total_cost ) supplier_total_cost,
    CURRENCY_SQL.CONVERT_VALUE('C', so.currency_code, mr.currency_code, inline.total_cost ) primary_total_cost,
    CURRENCY_SQL.CONVERT_VALUE('R', su.currency_code, mr.currency_code, inline.total_price ) supplier_total_price,
    CURRENCY_SQL.CONVERT_VALUE('R', so.currency_code, mr.currency_code, inline.total_price ) primary_total_price,
    CURRENCY_SQL.CONVERT_VALUE('C', su.currency_code, mr.currency_code, NVL(inline.rtv_cost,0) ) supplier_rtv_cost,
    CURRENCY_SQL.CONVERT_VALUE('C', so.currency_code, mr.currency_code, NVL(inline.rtv_cost,0) ) primary_rtv_cost,
    (NVL(inline.rtv_cost,0)                                                                     - (NVL(inline.rtv_cost,0)*(NVL(inline.restock_pct,0)/100)))final_tsf_cost,
    CURRENCY_SQL.CONVERT_VALUE('C', su.currency_code, mr.currency_code, (NVL(inline.rtv_cost,0) - (NVL(inline.rtv_cost,0)*(NVL(inline.restock_pct,0)/100))) ) supplier_final_tsf_cost,
    CURRENCY_SQL.CONVERT_VALUE('C', so.currency_code, mr.currency_code, (NVL(inline.rtv_cost,0) - (NVL(inline.rtv_cost,0)*(NVL(inline.restock_pct,0)/100))) ) primary_final_tsf_cost
  FROM system_options so,
    sups su,
    mrt mr,
    (SELECT mri.mrt_no mrt_no ,
      vir.item_parent ,
      mri.item item ,
      vir.item_desc ,
      vir.diff_1 ,
      vir.diff_2 ,
      vir.diff_3 ,
      vir.diff_4 ,
      NVL(mri.restock_pct,0) restock_pct ,
      NVL(mri.rtv_cost,0) rtv_cost ,
      mri.selected_ind selected_ind ,
      SUM(NVL(mil.return_avail_qty,0)) avail_return_qty ,
      SUM(NVL(mil.tsf_qty,0)) tsf_qty ,
      SUM(NVL(mil.tsf_qty,0) * NVL(mil.unit_retail,0)) total_retail ,
      SUM(NVL(mil.tsf_qty,0) *(NVL(mil.tsf_cost,0) * (1 - (NVL(mri.restock_pct,0)/100)))) total_cost ,
      SUM(NVL(mil.tsf_qty,0) *(NVL(mil.tsf_price,0) * (1 - (NVL(mri.restock_pct,0)/100)))) total_price ,
      SUM(NVL(mil.received_qty,0)) received_qty
    FROM mrt_item mri,
      mrt_item_loc mil,
      v_item_master vir
    WHERE mri.item    = vir.item
    AND mil.mrt_no(+) = mri.mrt_no
    AND mil.item(+)   = mri.item
    GROUP BY mri.mrt_no,
      vir.item_parent ,
      mri.item ,
      vir.item_desc ,
      vir.diff_1 ,
      vir.diff_2 ,
      vir.diff_3 ,
      vir.diff_4 ,
      mri.restock_pct ,
      mri.rtv_cost ,
      mri.selected_ind
    ) inline
  WHERE MR.MRT_NO    = INLINE.MRT_NO
  AND SU.SUPPLIER(+) = MR.SUPPLIER 
/

