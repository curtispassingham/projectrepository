--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

ALTER TABLE TRANSLATE_METADATA DROP COLUMN LABEL
/

ALTER TABLE TRANSLATE_METADATA ADD LABEL VARCHAR2 (6) DEFAULT 'COLUMN' NOT NULL
/

COMMENT ON COLUMN TRANSLATE_METADATA.LABEL is 'This contains the code of default UI label to be used for this column in UI. Refer to code_detail code_type TLLC'
/
