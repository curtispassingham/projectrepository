--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD NUM_DAYS_BOW_EAD NUMBER (2) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.NUM_DAYS_BOW_EAD is 'Number of days between between beginning of week and EAD to determine if order qualifies as issue'
/

