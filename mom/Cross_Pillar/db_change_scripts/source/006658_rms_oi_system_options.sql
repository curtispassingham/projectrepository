--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_WAC_VAR_TOLERANCE_PCT NUMBER (12,4) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_WAC_VAR_TOLERANCE_PCT is 'Defines the tolerance level outside of which if the variance between unit cost and average cost falls, the  item/locations combinations will be shown in the financial analyst WAC variance report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_WAC_VAR_ITEMLOC_CNT NUMBER (10) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_WAC_VAR_ITEMLOC_CNT is 'Controls the number of item/locations which when exceeded causes the financial analyst WAC variance report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_CUM_MARKON_MIN_VAR_PCT NUMBER (12,4) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_CUM_MARKON_MIN_VAR_PCT is 'Defines the tolerance level outside of which if the cumulative mark on % variance falls, the  subclass/locations combinations will be shown in the financial analyst cumulative markon % variance report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_CUM_MARKON_VAR_CRITICAL_CNT NUMBER (10) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_CUM_MARKON_VAR_CRITICAL_CNT is 'Controls the number of item/locations that exceed the FA_CUM_MARKON_MIN_VAR_PCT which when exceeded causes the financial analyst Cumulative markon% variance report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_STK_CNT_VALUE_VAR_CRIT_CNT NUMBER (10) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_STK_CNT_VALUE_VAR_CRIT_CNT is 'Controls the number of locations on a stock count that exceed the FA_STK_CNT_VALUE_VAR_TOLERANCE_PCT that when exceeded causes the financial analyst Stock count value variance report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_STK_CNT_VALUE_TOLERENCE_PCT NUMBER (10) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_STK_CNT_VALUE_TOLERENCE_PCT is 'Controls the variance percent for stock counts to be included in the financial analyst stock count value variance report.
'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_SHRINKAGE_VAR_TOLERANCE_PCT NUMBER (12,4) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_SHRINKAGE_VAR_TOLERANCE_PCT is 'Defines the tolerance outside of which if the variance between budgeted shrinkage and actual shrinkage falls, that subclass/location will be shown in the financial analyst Shrinkage variance report'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_SHRINKAGE_VAR_CRITICAL_CNT NUMBER (10) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_SHRINKAGE_VAR_CRITICAL_CNT is 'Controls the number of subclass/locations that exceed FA_SHRINKAGE_VAR_TOLERANCE_PCT which when exceeded causes the financial analyst  Shrinkage report to be considered critical.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_LATE_POST_THRESHOLD_TRN_CNT NUMBER (10) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_THRESHOLD_TRN_CNT is 'Define the threshold value for the count of late transactions per location which when executed causes the financial analyst Late posted transactions report to be considered critial'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_LATE_POST_THRESHOLD_LOC_CNT NUMBER (10) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_THRESHOLD_LOC_CNT is 'Defines the threshold value for the number of locations which when executed causes the financial analyst Late posted transactions report to be considered critial'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_LATE_POST_ORG_HIER_LEVEL VARCHAR2 (1 ) DEFAULT '1' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_ORG_HIER_LEVEL is 'Defines the level of organization hierarchy that would be used for chart display in the financial analyst late posted transactions report'
/

