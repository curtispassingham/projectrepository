--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop View         V_TSF_ENTITY_LOCATIONS      
--------------------------------------
PROMPT dropping View 'V_TSF_ENTITY_LOCATIONS'
DECLARE
  L_view_exists number := 0;
BEGIN
   SELECT count(*) INTO L_view_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'V_TSF_ENTITY_LOCATIONS';

if (L_view_exists != 0) then
      execute immediate 'DROP VIEW V_TSF_ENTITY_LOCATIONS';
  end if;
end;
/