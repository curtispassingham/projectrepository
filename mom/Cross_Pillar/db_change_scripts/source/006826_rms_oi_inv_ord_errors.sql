--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD TOTAL_UNITS NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.TOTAL_UNITS is 'The total order quantity.'
/

