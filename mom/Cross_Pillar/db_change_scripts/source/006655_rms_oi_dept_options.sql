--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_DEPT_OPTIONS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD FA_CUM_MARKON_MIN_VAR_PCT NUMBER (12,4) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.FA_CUM_MARKON_MIN_VAR_PCT is 'Defines the tolerance level outside of which if the cumulative mark on % variance falls, the  subclass/locations combinations will be shown in the financial analyst cumulative markon % variance report.'
/

