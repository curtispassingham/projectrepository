--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'UI_CONFIG_OPTIONS'
ALTER TABLE UI_CONFIG_OPTIONS ADD NUM_HIST_EXCHANGE_RATE NUMBER (2) DEFAULT 5 NOT NULL
/

COMMENT ON COLUMN UI_CONFIG_OPTIONS.NUM_HIST_EXCHANGE_RATE is 'This field is used to control how many historical exchange rate is presented to the user in list of value while selecting the exchange rate. The currently active exchange rate and future effective rates are included in the list of value by default. A value of N for this field will in addition also include upto N number of historical exchange rates prior to the currently active exchange rate effective date. The default value is 5.'
/


PROMPT ADDING CONSTRAINT 'CHK_UI_CONG_OPT_NUM_HI_EX_RATE'
ALTER TABLE UI_CONFIG_OPTIONS ADD CONSTRAINT
 CHK_UI_CONG_OPT_NUM_HI_EX_RATE CHECK (NUM_HIST_EXCHANGE_RATE >= 0)
/
