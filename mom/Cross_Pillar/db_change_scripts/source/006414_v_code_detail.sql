--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_CODE_DETAIL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Updating View 'V_CODE_DETAIL'
CREATE OR REPLACE FORCE VIEW "V_CODE_DETAIL"
("CODE_TYPE",
 "CODE",
 "REQUIRED_IND",
 "CODE_SEQ",
 "CODE_DESC")
 AS (SELECT code_type,
            code,
            required_ind,
            code_seq,
            code_desc
       FROM v_code_detail_tl)
/

COMMENT ON TABLE V_CODE_DETAIL is 'This view is same as v_code_detail_tl and provides the code description in user langauge. This view was created before introduction of v_code_detail_tl and is retained because this view is extensively used.'
/

COMMENT ON COLUMN V_CODE_DETAIL."CODE" IS 'Contains the code for the row.'
/

COMMENT ON COLUMN V_CODE_DETAIL."CODE_TYPE" IS 'Contains the valid code type for the row. Valid values are defined in the table code_head.'
/

COMMENT ON COLUMN V_CODE_DETAIL."CODE_SEQ" IS 'This is a number used to order the elements so that they appear consistently when using them to populate a list.'
/

COMMENT ON COLUMN V_CODE_DETAIL."CODE_DESC" IS 'This field contains the description associated with the code and code type.'
/

COMMENT ON COLUMN V_CODE_DETAIL."REQUIRED_IND" IS 'This field indicates whether or not the code is required.'
/
