--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 DYNAMIC_HIER_TOKEN_MAP_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'DYNAMIC_HIER_TOKEN_MAP_TL'
CREATE TABLE DYNAMIC_HIER_TOKEN_MAP_TL
 (TOKEN VARCHAR2(10 ) NOT NULL,
  LANG NUMBER(6) NOT NULL,
  RMS_NAME VARCHAR2(40 ) NOT NULL,
  CLIENT_NAME VARCHAR2(40 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DYNAMIC_HIER_TOKEN_MAP_TL is 'This is the translation table for DYNAMIC_HIER_TOKEN_MAP table. The token to name mapping for data integration langauge is maintained in the base table. The token mapping in other languages are maintained in this table.'
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP_TL.TOKEN is 'This field contains the hierarchy token present in the seed data and in application bundle file. '
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP_TL.LANG is 'This field contains the language in which mapped name is maintained.'
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP_TL.RMS_NAME is 'This is the default RMS name for this token'
/

COMMENT ON COLUMN DYNAMIC_HIER_TOKEN_MAP_TL.CLIENT_NAME is 'The client can define their own name for this token. The value in this column will take precedence to RMS_NAME column when tokens gets replaced. '
/

PROMPT CREATING PRIMARY KEY ON 'DYNAMIC_HIER_TOKEN_MAP_TL'
ALTER TABLE DYNAMIC_HIER_TOKEN_MAP_TL
 ADD CONSTRAINT PK_DYNAMIC_HIER_TOKEN_MAP_TL PRIMARY KEY
  (TOKEN,
  LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

PROMPT Creating FK on 'DYNAMIC_HIER_TOKEN_MAP_TL'
 ALTER TABLE DYNAMIC_HIER_TOKEN_MAP_TL
  ADD CONSTRAINT DHTMT_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT Creating FK on 'DYNAMIC_HIER_TOKEN_MAP_TL'
 ALTER TABLE DYNAMIC_HIER_TOKEN_MAP_TL
  ADD CONSTRAINT DHTMT_DHTM_FK
  FOREIGN KEY (TOKEN)
 REFERENCES DYNAMIC_HIER_TOKEN_MAP (TOKEN)
/

