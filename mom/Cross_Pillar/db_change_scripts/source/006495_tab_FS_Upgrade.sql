--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop Table               
--------------------------------------
PROMPT dropping Table 'SEC_FORM_ACTION_ROLE'
DECLARE
  L_table_exists1 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'SEC_FORM_ACTION_ROLE'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists1 != 0) then
      execute immediate 'DROP TABLE SEC_FORM_ACTION_ROLE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_ELEMENT_MODE_BASE'
DECLARE
  L_table_exists2 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists2
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_ELEMENT_MODE_BASE'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists2 != 0) then
      execute immediate 'DROP TABLE NAV_ELEMENT_MODE_BASE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_ELEMENT_MODE_ROLE'
DECLARE
  L_table_exists3 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists3
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_ELEMENT_MODE_ROLE'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists3 != 0) then
      execute immediate 'DROP TABLE NAV_ELEMENT_MODE_ROLE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_ELEMENT_MODE'
DECLARE
  L_table_exists4 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists4
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_ELEMENT_MODE'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists4 != 0) then
      execute immediate 'DROP TABLE NAV_ELEMENT_MODE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_FOLDER'
DECLARE
  L_table_exists5 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists5
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_FOLDER'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists5 != 0) then
      execute immediate 'DROP TABLE NAV_FOLDER CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_USER_OPTIONS'
DECLARE
  L_table_exists6 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists6
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_USER_OPTIONS'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists6 != 0) then
      execute immediate 'DROP TABLE NAV_USER_OPTIONS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_FAVORITES'
DECLARE
  L_table_exists7 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists7
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_FAVORITES'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists7 != 0) then
      execute immediate 'DROP TABLE NAV_FAVORITES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_ELEMENTS_LANGS'
DECLARE
  L_table_exists8 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists8
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_ELEMENTS_LANGS'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists8 != 0) then
      execute immediate 'DROP TABLE FORM_ELEMENTS_LANGS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_LINKS_ROLE'
DECLARE
  L_table_exists9 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists9
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_LINKS_ROLE'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists9 != 0) then
      execute immediate 'DROP TABLE FORM_LINKS_ROLE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_MENU_LINK'
DECLARE
  L_table_exists10 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists10
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_MENU_LINK'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists10 != 0) then
      execute immediate 'DROP TABLE FORM_MENU_LINK CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_METADATA'
DECLARE
  L_table_exists11 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists11
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_METADATA'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists11 != 0) then
      execute immediate 'DROP TABLE FORM_METADATA CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_ELEMENTS_LANGS_TEMP'
DECLARE
  L_table_exists12 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists12
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_ELEMENTS_LANGS_TEMP'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists12 != 0) then
      execute immediate 'DROP TABLE FORM_ELEMENTS_LANGS_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_ELEMENTS_TEMP'
DECLARE
  L_table_exists13 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists13
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_ELEMENTS_TEMP'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists13 != 0) then
      execute immediate 'DROP TABLE FORM_ELEMENTS_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'SEC_FORM_ACTION'
DECLARE
  L_table_exists14 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists14
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'SEC_FORM_ACTION'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists14 != 0) then
      execute immediate 'DROP TABLE SEC_FORM_ACTION CASCADE CONSTRAINTS';
  end if;
end;
/
