--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 TRANSLATE_METADATA_TABLE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'TRANSLATE_METADATA_TABLE'
CREATE TABLE TRANSLATE_METADATA_TABLE
 (BASE_TABLE VARCHAR2(30 ) NOT NULL,
  LABEL VARCHAR2(6 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TRANSLATE_METADATA_TABLE is 'This contains the code of default UI label to be used for base system of record RMS table for this entity.'
/

COMMENT ON COLUMN TRANSLATE_METADATA_TABLE.BASE_TABLE is 'Contains the base system of record RMS table for this entity.'
/

COMMENT ON COLUMN TRANSLATE_METADATA_TABLE.LABEL is 'Contains the code of default UI label to be used for base system of record RMS table for this entity.'
/


PROMPT Creating Primary Key on 'TRANSLATE_METADATA_TABLE'
ALTER TABLE TRANSLATE_METADATA_TABLE
 ADD CONSTRAINT PK_TRANSLATE_METADATA_TABLE PRIMARY KEY
  (BASE_TABLE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

