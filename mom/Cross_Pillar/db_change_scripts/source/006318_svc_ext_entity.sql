--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_EXT_ENTITY
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_EXT_ENTITY'
CREATE TABLE SVC_EXT_ENTITY
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ) NOT NULL,
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N' NOT NULL,
  BASE_RMS_TABLE VARCHAR2(30 ) NOT NULL,
  VALIDATION_FUNC VARCHAR2(61 ),
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  LAST_UPD_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPD_DATETIME DATE NOT NULL
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

COMMENT ON TABLE SVC_EXT_ENTITY is 'This is the staging table for CFAS ext entity information.It is used to temporarily hold data before it is uploaded/updated in CFA_EXT_ENTITY.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.CHUNK_ID is 'Uniquely identifies a chunk.The value will always be 1.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.ROW_SEQ is 'The rows sequence. Should be unique within a process-ID.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.BASE_RMS_TABLE is 'The physical database table in RMS to be extended.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.VALIDATION_FUNC is 'This contains the name of the stored validation procedure (package.function name) executed in the entitys main UI to check  data consistencies covering all extended attributes defined under the entity.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_EXT_ENTITY.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_EXT_ENTITY'
ALTER TABLE SVC_EXT_ENTITY
 ADD CONSTRAINT SVC_EXT_ENTITY_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_EXT_ENTITY'
ALTER TABLE SVC_EXT_ENTITY
 ADD CONSTRAINT SVC_EXT_ENTITY_UK UNIQUE
  (BASE_RMS_TABLE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

