--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING CONSTRAINTS               
--------------------------------------
PROMPT DROPPING CONSTRAINT 'CHK_DOMAIN_DELIMITER'
DECLARE
  L_delimiter_exists number := 0;
BEGIN
  SELECT count(*) INTO L_delimiter_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_DOMAIN_DELIMITER'
     AND CONSTRAINT_TYPE = 'C';
	 
	 if (L_delimiter_exists != 0) then
      execute immediate 'ALTER TABLE DOMAIN DROP CONSTRAINT CHK_DOMAIN_DELIMITER DROP INDEX';
  end if;
end;
/

PROMPT DROPPING CONSTRAINT 'CHK_DOMAIN_PROTOCOL_SEQ'
DECLARE
  L_seq_exists number := 0;
BEGIN
  SELECT count(*) INTO L_seq_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_DOMAIN_PROTOCOL_SEQ'
     AND CONSTRAINT_TYPE = 'C';
	 
	 if (L_seq_exists != 0) then
      execute immediate 'ALTER TABLE DOMAIN DROP CONSTRAINT CHK_DOMAIN_PROTOCOL_SEQ DROP INDEX';
  end if;
end;
/

PROMPT DROPPING CONSTRAINT 'CHK_DOMAIN_UNIX_FIELDS'
DECLARE
  L_fields_exists number := 0;
BEGIN
  SELECT count(*) INTO L_fields_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_DOMAIN_UNIX_FIELDS'
     AND CONSTRAINT_TYPE = 'C';
	 
	 if (L_fields_exists != 0) then
      execute immediate 'ALTER TABLE DOMAIN DROP CONSTRAINT CHK_DOMAIN_UNIX_FIELDS DROP INDEX';
  end if;
end;
/

PROMPT DROPPING CONSTRAINT 'CHK_DOMAIN_UNIX_LOGIN'
DECLARE
  L_login_exists number := 0;
BEGIN
  SELECT count(*) INTO L_login_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_DOMAIN_UNIX_LOGIN'
     AND CONSTRAINT_TYPE = 'C';
	 
	 if (L_login_exists != 0) then
      execute immediate 'ALTER TABLE DOMAIN DROP CONSTRAINT CHK_DOMAIN_UNIX_LOGIN DROP INDEX';
  end if;
end;
/
--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'DOMAIN'
ALTER TABLE DOMAIN DROP COLUMN DOMAIN_LABEL
/

ALTER TABLE DOMAIN DROP COLUMN DELIMITER
/

ALTER TABLE DOMAIN DROP COLUMN PROTOCOL_SEQ
/

ALTER TABLE DOMAIN DROP COLUMN ENDPOINT
/

ALTER TABLE DOMAIN DROP COLUMN NETWORK_ADDR
/

ALTER TABLE DOMAIN DROP COLUMN PATH
/

ALTER TABLE DOMAIN DROP COLUMN UNIX_LOGIN
/

ALTER TABLE DOMAIN DROP COLUMN UNIX_ID
/

ALTER TABLE DOMAIN DROP COLUMN UNIX_PASSWD
/

ALTER TABLE DOMAIN DROP COLUMN COMMAND_LINE
/



