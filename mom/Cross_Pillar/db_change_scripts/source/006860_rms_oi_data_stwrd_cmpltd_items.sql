--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_DATA_STWRD_CMPLTD_ITEMS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_DATA_STWRD_CMPLTD_ITEMS'
CREATE TABLE RMS_OI_DATA_STWRD_CMPLTD_ITEMS
 (ITEM VARCHAR2(25 ) NOT NULL,
  COMPLETED VARCHAR2(1 ) DEFAULT 'N' NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_DATA_STWRD_CMPLTD_ITEMS is 'This tables holds data for the items marked as completed in the Data Steward Dashboard Incomplete Items Report.'
/

COMMENT ON COLUMN RMS_OI_DATA_STWRD_CMPLTD_ITEMS.ITEM is 'Item number.'
/

COMMENT ON COLUMN RMS_OI_DATA_STWRD_CMPLTD_ITEMS.COMPLETED is 'Marks the completed flag to ''''Y'''' for all items that are marked as COMPLETED in the Incomplete Items report, though not completed in the base tables.'
/


PROMPT Creating Primary Key on 'RMS_OI_DATA_STWRD_CMPLTD_ITEMS'
ALTER TABLE RMS_OI_DATA_STWRD_CMPLTD_ITEMS
 ADD CONSTRAINT PK_RMS_OI_DS_COMP_ITEM PRIMARY KEY
  (ITEM
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RMS_OI_DATA_STWRD_CMPLTD_ITEMS'
 ALTER TABLE RMS_OI_DATA_STWRD_CMPLTD_ITEMS
  ADD CONSTRAINT FK_RMS_OI_DS_COMP_ITEM
  FOREIGN KEY (ITEM)
 REFERENCES ITEM_MASTER (ITEM)
/

