--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      PACKAGES to be DROPPED:    
                          WH_STORE_ASSIGN_WRP
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Package
-------------------------------------- 
PROMPT DROPPING Package 'WH_STORE_ASSIGN_WRP'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WH_STORE_ASSIGN_WRP';

  if (L_pkg_exists != 0) then
      execute immediate 'DROP PACKAGE WH_STORE_ASSIGN_WRP';
  end if;
end;
/
