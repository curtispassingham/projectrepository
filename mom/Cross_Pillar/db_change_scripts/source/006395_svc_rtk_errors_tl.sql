--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_RTK_ERRORS_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_RTK_ERRORS_TL'
CREATE TABLE SVC_RTK_ERRORS_TL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  RTK_KEY VARCHAR2(25 ),
  RTK_TEXT VARCHAR2(255 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_RTK_ERRORS_TL is 'This is a staging table used for rtk_errors_tl spreadsheet upload process. It is used to temporarily hold data before it is uploaded/updated in RTK_ERRORS_TL.'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.LANG is 'Refer to RTK_ERRORS_TL.LANG column'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.RTK_KEY is 'Refer to RTK_ERRORS_TL.RTK_KEY column'
/

COMMENT ON COLUMN SVC_RTK_ERRORS_TL.RTK_TEXT is 'Refer to RTK_ERRORS_TL.RTK_TEXT column'
/


PROMPT Creating Primary Key on 'SVC_RTK_ERRORS_TL'
ALTER TABLE SVC_RTK_ERRORS_TL
 ADD CONSTRAINT SVC_RTK_ERRORS_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_RTK_ERRORS_TL'
ALTER TABLE SVC_RTK_ERRORS_TL
 ADD CONSTRAINT SVC_RTK_ERRORS_TL_UK UNIQUE
  (LANG,
   RTK_KEY
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

