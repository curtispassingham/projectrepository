--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW ADDED:          V_SKULIST_HEAD
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING VIEW
--------------------------------------
PROMPT Creating View 'V_SKULIST_HEAD'
CREATE OR REPLACE FORCE VIEW V_SKULIST_HEAD (SKULIST, SKULIST_DESC, CREATE_DATE, CREATE_ID, STATIC_IND, LAST_REBUILD_DATE, USER_SECURITY_IND, COMMENT_DESC, FILTER_ORG_ID) AS
SELECT SKH.SKULIST,
       V.SKULIST_DESC,
       SKH.CREATE_DATE,
       SKH.CREATE_ID,
       SKH.STATIC_IND,
       SKH.LAST_REBUILD_DATE,
       SKH.USER_SECURITY_IND,
       SKH.COMMENT_DESC,
       SKH.FILTER_ORG_ID
  FROM SKULIST_HEAD SKH,
       V_SKULIST_HEAD_TL V
 WHERE SKH.SKULIST = V.SKULIST
/


COMMENT ON TABLE V_SKULIST_HEAD IS 'This view will be used to display the header information for the item list using a security policy to filter user access. The description is translated based on user language.'
/
