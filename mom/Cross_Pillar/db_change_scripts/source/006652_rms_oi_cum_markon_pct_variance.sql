--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_CUM_MARKON_PCT_VARIANCE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_CUM_MARKON_PCT_VARIANCE'
CREATE TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE
 (SESSION_ID NUMBER(15),
  DEPT NUMBER(4),
  DEPT_NAME VARCHAR2(120 ),
  CLASS NUMBER(4),
  CLASS_NAME VARCHAR2(120 ),
  SUBCLASS NUMBER(4),
  SUBCLASS_NAME VARCHAR2(120 ),
  LOC NUMBER(10),
  LOC_TYPE VARCHAR2(1 ),
  LOC_NAME VARCHAR2(150 ),
  CALCULATED_MARKON_PCT NUMBER(20,10),
  POSTED_MARKON_PCT NUMBER(20,10),
  BUDGETED_MARKON_PCT NUMBER(20,10),
  VARIANCE_MARKON_PCT NUMBER(20,10)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE is 'This table holds the information about subclass/location combinations having a cumulative markon variance that falls outside of a defined tolerance for the Cumulative markon% Variance Report.'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.SESSION_ID is 'Session id'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.DEPT is 'Department number'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.DEPT_NAME is 'Department name'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CLASS is 'Class number'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CLASS_NAME is 'Class name'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.SUBCLASS is 'Subclass number'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.SUBCLASS_NAME is 'Subclass name'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.LOC is 'Location'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.LOC_TYPE is 'Location Type'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.LOC_NAME is 'Location Name'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CALCULATED_MARKON_PCT is 'Calculated mark on percentage'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.POSTED_MARKON_PCT is 'Posted  mark on percentage'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.BUDGETED_MARKON_PCT is 'Budgeted mark on percentage'
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.VARIANCE_MARKON_PCT is 'Variance mark on percentage'
/

PROMPT Creating Index 'RMS_OI_CUM_MARKON_PCT_VAR_I1'
CREATE INDEX RMS_OI_CUM_MARKON_PCT_VAR_I1 ON RMS_OI_CUM_MARKON_PCT_VARIANCE
(SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
