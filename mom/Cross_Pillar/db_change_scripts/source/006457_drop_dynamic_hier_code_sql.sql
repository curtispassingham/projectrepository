--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	PACKAGE DROPPED:				DYNAMIC_HIER_CODE_SQL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------------
--       DROPPING PACKAGE
--------------------------------------------
PROMPT Dropping Package 'DYNAMIC_HIER_CODE_SQL'
DECLARE
  L_package_exists number := 0;
BEGIN
 SELECT count(*) INTO L_package_exists
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='DYNAMIC_HIER_CODE_SQL'
    AND OBJECT_TYPE ='PACKAGE';
	
  if (L_package_exists != 0)then
      execute immediate 'DROP PACKAGE DYNAMIC_HIER_CODE_SQL' ;
 end if;
end;
/
