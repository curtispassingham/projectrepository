--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING Table EVIEW_LOCATION
-------------------------------------------
PROMPT DROPPING table EVIEW_LOCATION

DECLARE
  L_eview_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_eview_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'EVIEW_LOCATION';

  if (L_eview_table_exists != 0) then
      execute immediate 'DROP TABLE EVIEW_LOCATION CASCADE CONSTRAINTS';
  end if;
end;
/