--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IA_SHOW_ORD_ERR_PAST_NAD
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IA_SHOW_ORD_ERR_ORD_TO_CLOSE
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IA_SHOW_ORD_ERR_NEVER_APPRV
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IA_SHOW_ORD_ERR_ONCE_APPRV
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IA_SHOW_ORD_ERR_MISS_ORD_DATA
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN IA_SHOW_ORD_ERR_MISS_ITEM_DATA
/

