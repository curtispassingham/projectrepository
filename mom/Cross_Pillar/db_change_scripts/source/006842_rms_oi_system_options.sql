--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD
DISPLAY_CONTEXTUAL_PAYLOAD VARCHAR2 (1 ) DEFAULT 'N' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DISPLAY_CONTEXTUAL_PAYLOAD is '�Y� -   Display the payload parameters passed in the screen''s contextual area.    This is intended as a troubleshooting aid for customers building their own contextual reports,  to view the parameters passed to the contextual area on specified user action in the screen.
                                                                                                                                   �N�  -  Hide the payload parameters passed in the screen''s contextual area.'
/

