--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'DAILY_PURGE'
COMMENT ON TABLE DAILY_PURGE is 'This table is used to hold the table name and a key value for a record that needs to be deleted. Records are inserted into this table on-line as part of the delete logic. The actual deletes are performed in a nightly batch process by running the dlypurge.pc program.'
/

COMMENT ON COLUMN DAILY_PURGE.DELETE_TYPE is 'The value in this column indicates what type of delete this record represents, from pressing the delete button, D, or from pressing the cancel button, C.  This field will only have a value if different logic is performed determined by which button was pressed. '
/

