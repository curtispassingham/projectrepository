--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_ANA_VARIANCE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_ANA_VARIANCE'
CREATE TABLE RMS_OI_INV_ANA_VARIANCE
 (SESSION_ID NUMBER(15,0),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  ALC_ITEM_TYPE VARCHAR2(15 ),
  AGG_DIFF_1 VARCHAR2(10 ),
  AGG_DIFF_2 VARCHAR2(10 ),
  AGG_DIFF_3 VARCHAR2(10 ),
  AGG_DIFF_4 VARCHAR2(10 ),
  SALES_UNITS NUMBER(20,4),
  SALES_VAR_TO_FORECAST_PCT NUMBER(20,4),
  INV_UNITS NUMBER(20,4),
  INV_VAR_VALUE NUMBER(20,4),
  INV_VAR_TO_FORECAST_PCT NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_INV_ANA_VARIANCE is 'This table holds data for the Inventory Analyst Inventory Variance to Forecast Report.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.ITEM is 'The item for the row.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.ITEM_DESC is 'The item description.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.ALC_ITEM_TYPE is 'The alc_item_type of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.AGG_DIFF_1 is 'The aggregate diff 1 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.AGG_DIFF_2 is 'The aggregate diff 2 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.AGG_DIFF_3 is 'The aggregate diff 3 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.AGG_DIFF_4 is 'The aggregate diff 4 for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.SALES_UNITS is 'The sales units for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.SALES_VAR_TO_FORECAST_PCT is 'The sales variance to forecast percent.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.INV_UNITS is 'The inventory for the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.INV_VAR_VALUE is 'The Inventory Variance value.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.INV_VAR_TO_FORECAST_PCT is 'The inventory variance to forecast percent.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_VARIANCE.CURRENCY_CODE is 'The currency code of the inv_var_value column.  Will be base currency.'
/


PROMPT Creating Index on 'RMS_OI_INV_ANA_VARIANCE'
 CREATE INDEX RMS_OI_INV_ANA_VARIANCE_I1 on RMS_OI_INV_ANA_VARIANCE
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

