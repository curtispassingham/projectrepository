--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_REC_GROUP_LABELS'
ALTER TABLE SVC_REC_GROUP_LABELS MODIFY ACTION NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY PROCESS$STATUS DEFAULT 'N' NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY REC_GROUP_ID NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.REC_GROUP_ID is 'Unique identifier to reference the record group.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY LANG NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.LANG is 'The LOV labels language.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY LOV_TITLE NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.LOV_TITLE is 'Holds the LOV title label.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY LOV_COL1_HEADER NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.LOV_COL1_HEADER is 'Holds the LOVs first column header label.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY CREATE_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.CREATE_ID is 'User who created the record.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY CREATE_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.CREATE_DATETIME is 'Date time when record was inserted.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY LAST_UPD_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.LAST_UPD_ID is 'User who last updated the record.'
/

ALTER TABLE SVC_REC_GROUP_LABELS MODIFY LAST_UPD_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_REC_GROUP_LABELS.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/

