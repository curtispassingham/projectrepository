--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit


--------------------------------------
--       Drop View         V_POS_ITEM_BUTTON      
--------------------------------------
PROMPT dropping View 'V_POS_ITEM_BUTTON'
DECLARE
  L_view_exists number := 0;
BEGIN
   SELECT count(*) INTO L_view_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'V_POS_ITEM_BUTTON';

if (L_view_exists != 0) then
      execute immediate 'DROP VIEW V_POS_ITEM_BUTTON';
  end if;
end;
/
