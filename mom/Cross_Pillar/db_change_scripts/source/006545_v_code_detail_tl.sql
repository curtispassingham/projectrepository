--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- View Updated:      V_CODE_DETAIL_TL 
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating View
--------------------------------------
PROMPT UPDATING VIEW 'V_CODE_DETAIL_TL'
CREATE OR REPLACE FORCE VIEW V_CODE_DETAIL_TL (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ, LANG ) AS
SELECT  b.code_type,
        b.code,
        case when tl.lang is not null then tl.code_desc else b.code_desc end code_desc,
        b.required_ind,
        b.code_seq,
        NVL(tl.lang,lng.prim_lang) lang
  FROM  CODE_DETAIL b,
        CODE_DETAIL_TL tl,
        (select GET_PRIMARY_LANG() prim_lang, LANGUAGE_SQL.GET_USER_LANGUAGE() user_lang from dual where rownum = 1) lng
 WHERE  b.code_type = tl.code_type (+)
   AND  b.code = tl.code (+)
   AND  tl.lang (+) = lng.user_lang
/

COMMENT ON TABLE V_CODE_DETAIL_TL is 'This is the translation view for base table CODE_DETAIL. This view fetches data in user langauge either from translation table CODE_DETAIL_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CODE_DETAIL_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CODE_DETAIL_TL.CODE_TYPE is 'Contains the valid code type for the row. Valid values are defined in the table code_head'
/

COMMENT ON COLUMN V_CODE_DETAIL_TL.CODE is 'Contains the code for the row.'
/

COMMENT ON COLUMN V_CODE_DETAIL_TL.CODE_DESC is 'This field contains the description associated with the code and code type.'
/

COMMENT ON COLUMN V_CODE_DETAIL_TL.REQUIRED_IND is 'This field indicates whether or not the code is required.'
/

COMMENT ON COLUMN V_CODE_DETAIL_TL.CODE_SEQ is 'This is a number used to order the elements so that they appear consistently when using them to populate a list.'
/