--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 OI_SESSION_ID_LOG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'OI_SESSION_ID_LOG'
CREATE TABLE OI_SESSION_ID_LOG
 (SESSION_ID NUMBER(15) NOT NULL,
  USER_ID VARCHAR2(30 BYTE) DEFAULT 'get_user' NOT NULL,
  REQUESTED_DATE DATE DEFAULT sysdate NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE OI_SESSION_ID_LOG is 'Tracking session ID requests to be used in table cleanup operation'
/

COMMENT ON COLUMN OI_SESSION_ID_LOG.SESSION_ID is 'The new session id given to user'
/

COMMENT ON COLUMN OI_SESSION_ID_LOG.USER_ID is 'User requested the next session id'
/

COMMENT ON COLUMN OI_SESSION_ID_LOG.REQUESTED_DATE is 'Date and time session id requested'
/


PROMPT Creating Primary Key on 'OI_SESSION_ID_LOG'
ALTER TABLE OI_SESSION_ID_LOG
 ADD CONSTRAINT OI_SESSION_ID_LOG_PK PRIMARY KEY
  (SESSION_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

