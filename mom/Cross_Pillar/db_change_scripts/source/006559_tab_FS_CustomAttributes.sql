--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      TABLES to be DROPPED:    
                          ITEM_ATTRIBUTES,
                          STORE_ATTRIBUTES,
                          SUP_ATTRIBUTES,
                          WH_ATTRIBUTES,
                          OTB_FWD_LIMIT
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TABLES
--------------------------------------
PROMPT DROPPING Table 'ITEM_ATTRIBUTES'
DECLARE
  L_table_exists1 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ITEM_ATTRIBUTES';

if (L_table_exists1 != 0) then
      execute immediate 'DROP TABLE ITEM_ATTRIBUTES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'STORE_ATTRIBUTES'
DECLARE
  L_table_exists2 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists2
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_ATTRIBUTES';

if (L_table_exists2 != 0) then
      execute immediate 'DROP TABLE STORE_ATTRIBUTES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'SUP_ATTRIBUTES'
DECLARE
  L_table_exists3 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists3
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'SUP_ATTRIBUTES';

if (L_table_exists3 != 0) then
      execute immediate 'DROP TABLE SUP_ATTRIBUTES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'WH_ATTRIBUTES'
DECLARE
  L_table_exists4 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists4
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WH_ATTRIBUTES';

if (L_table_exists4 != 0) then
      execute immediate 'DROP TABLE WH_ATTRIBUTES CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'OTB_FWD_LIMIT'
DECLARE
  L_table_exists5 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists5
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'OTB_FWD_LIMIT';

if (L_table_exists5 != 0) then
      execute immediate 'DROP TABLE OTB_FWD_LIMIT CASCADE CONSTRAINTS';
  end if;
end;
/