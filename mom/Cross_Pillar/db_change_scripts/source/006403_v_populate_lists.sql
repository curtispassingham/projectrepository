--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_POPULATE_LISTS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Updating View 'V_POPULATE_LISTS'
CREATE OR REPLACE FORCE VIEW "V_POPULATE_LISTS"
("CODE",
 "CODE_TYPE",
 "CODE_SEQ",
 "CODE_DESC")
 AS (SELECT code,
            code_type,
            code_seq,
            code_desc
       FROM v_code_detail_tl)
/

COMMENT ON TABLE V_POPULATE_LISTS is 'This view was created before introduction of v_code_detail_tl and got used in initial RMS ADF application. This view should not be used and existing usage should be replaced with v_code_detail_tl and this view should be dropped.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE" IS 'This field contains the code used in Oracle Retail which must be decoded for display in the on-line forms.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE_TYPE" IS 'This field will contain a valid code type for the row.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE_SEQ" IS 'This is a number used to order the elements so that they appear consistently when using them to populate a list.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE_DESC" IS 'This field contains the description associated with the code and code type.'
/
