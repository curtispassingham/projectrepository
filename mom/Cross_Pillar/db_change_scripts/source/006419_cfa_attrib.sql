--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Dropping Constraint 'CHK_CFA_ATTRIB_DISPLAY_SEQ'
DECLARE
  L_constraints_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_CFA_ATTRIB_DISPLAY_SEQ'
     AND CONSTRAINT_TYPE = 'C';

  if (L_constraints_exists != 0) then
      execute immediate 'ALTER TABLE CFA_ATTRIB DROP CONSTRAINT CHK_CFA_ATTRIB_DISPLAY_SEQ';
  end if;
end;
/

PROMPT Modifying Table 'CFA_ATTRIB'
ALTER TABLE CFA_ATTRIB ADD CONSTRAINT
 CHK_CFA_ATTRIB_DISPLAY_SEQ CHECK (DISPLAY_SEQ BETWEEN 1 AND 25)
/

PROMPT Dropping CONSTRAINT 'CHK_CFA_ATTRIB_STORAGE_COL_NAM'
DECLARE
  L_constraints_exists_1 number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists_1
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_CFA_ATTRIB_STORAGE_COL_NAM'
     AND CONSTRAINT_TYPE = 'C';

  if (L_constraints_exists_1 != 0) then
      execute immediate 'ALTER TABLE CFA_ATTRIB DROP CONSTRAINT CHK_CFA_ATTRIB_STORAGE_COL_NAM';
  end if;
end;
/

PROMPT MODIFYING CONSTRAINT 'CHK_CFA_ATTRIB_STORAGE_COL_NAM'
ALTER TABLE CFA_ATTRIB ADD CONSTRAINT
 CHK_CFA_ATTRIB_STORAGE_COL_NAM CHECK (STORAGE_COL_NAME IN ('VARCHAR2_1', 'VARCHAR2_2', 'VARCHAR2_3','VARCHAR2_4', 'VARCHAR2_5', 'VARCHAR2_6', 'VARCHAR2_7', 'VARCHAR2_8', 'VARCHAR2_9' , 'VARCHAR2_10', 'NUMBER_11', 'NUMBER_12', 'NUMBER_13', 'NUMBER_14' , 'NUMBER_15', 'NUMBER_16', 'NUMBER_17', 'NUMBER_18', 'NUMBER_19', 'NUMBER_20', 'DATE_21', 'DATE_22','DATE_23','DATE_24','DATE_25'))
/
