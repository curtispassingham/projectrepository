--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_STOCK_COUNT_VARIANCE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_STOCK_COUNT_VARIANCE'
CREATE TABLE RMS_OI_STOCK_COUNT_VARIANCE
 (SESSION_ID NUMBER(15,0),
  CYCLE_COUNT NUMBER(8,0),
  CYCLE_COUNT_DESC VARCHAR2(250 ),
  STOCKTAKE_TYPE VARCHAR2(1 ),
  STOCKTAKE_DATE DATE,
  LOC NUMBER(10,0),
  LOC_TYPE VARCHAR2(1 ),
  LOC_NAME VARCHAR2(150 ),
  TOTAL_VARIANCE_PCT NUMBER(20,10),
  OVER_VARIANCE_PCT NUMBER(20,10),
  SHORT_VARIANCE_PCT NUMBER(20,10),
  SNAPSHOT_ON_HAND_QTY NUMBER(20,4),
  SNAPSHOT_IN_TRANSIT_QTY NUMBER(20,4),
  PHYSICAL_COUNT_QTY NUMBER(20,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_STOCK_COUNT_VARIANCE is 'This table holds the information for the Stock Count Variance Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.CYCLE_COUNT is 'The stock count.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.CYCLE_COUNT_DESC is 'The stock count description.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.STOCKTAKE_TYPE is 'The type of stock count.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.STOCKTAKE_DATE is 'The date of the stock count.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.LOC is 'The location.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.LOC_TYPE is 'The location type.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.LOC_NAME is 'The location name.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.TOTAL_VARIANCE_PCT is 'The total variance percent.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.OVER_VARIANCE_PCT is 'The over variance percent.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.SHORT_VARIANCE_PCT is 'The short variance percent.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.SNAPSHOT_ON_HAND_QTY is 'The snapshot stock on hand value.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.SNAPSHOT_IN_TRANSIT_QTY is 'The snapshot in transit quanty.'
/

COMMENT ON COLUMN RMS_OI_STOCK_COUNT_VARIANCE.PHYSICAL_COUNT_QTY is 'The physical count quanty.'
/


PROMPT Creating Index on 'RMS_OI_STOCK_COUNT_VARIANCE'
 CREATE INDEX RMS_OI_STOCK_COUNT_VARIANCE_I1 on RMS_OI_STOCK_COUNT_VARIANCE
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

