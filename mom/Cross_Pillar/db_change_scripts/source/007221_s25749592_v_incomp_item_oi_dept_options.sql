--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW CREATED:               V_INCOMP_ITEM_OI_DEPT_OPTIONS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_INCOMP_ITEM_OI_DEPT_OPTIONS'
create or replace view V_INCOMP_ITEM_OI_DEPT_OPTIONS as 
select 
ro.DEPT , dp.DEPT_NAME,
DS_DAYS_AFTER_ITEM_CREATE,
DS_SHOW_INCOMP_ITEM_REF_ITEM,
DS_SHOW_INCOMP_ITEM_VAT,
DS_SHOW_INCOMP_ITEM_SPACK,
DS_SHOW_INCOMP_ITEM_UDA,
DS_SHOW_INCOMP_ITEM_LOC,
DS_SHOW_INCOMP_ITEM_SEASONS,
DS_SHOW_INCOMP_ITEM_REPL,
DS_SHOW_INCOMP_ITEM_SUBS_ITEM,
DS_SHOW_INCOMP_ITEM_DIMEN,
DS_SHOW_INCOMP_ITEM_REL_ITEM,
DS_SHOW_INCOMP_ITEM_TICKETS,
DS_SHOW_INCOMP_ITEM_HTS,
DS_SHOW_INCOMP_ITEM_IMP_ATTR,
DS_SHOW_INCOMP_ITEM_IMAGES
FROM RMS_OI_DEPT_OPTIONS ro , DEPS dp
where ro.DEPT = dp.DEPT AND
(DS_DAYS_AFTER_ITEM_CREATE IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_REF_ITEM IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_VAT IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_SPACK IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_UDA IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_LOC IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_SEASONS IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_REPL IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_SUBS_ITEM IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_DIMEN IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_REL_ITEM IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_TICKETS IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_HTS IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_IMP_ATTR IS NOT NULL OR
DS_SHOW_INCOMP_ITEM_IMAGES IS NOT NULL )
/

COMMENT ON TABLE V_INCOMP_ITEM_OI_DEPT_OPTIONS IS 'This view returns department level report option for incomplete items report.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DEPT IS 'Deprtment.'
/
COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DEPT_NAME IS 'Department Name'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_DAYS_AFTER_ITEM_CREATE 
IS 'Defines the number of days after item creation after which the Item will appear in INCOMPLETE ITEMS report.'
/
COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_REF_ITEM 
IS 'Configured to show Reference Items in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/
COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_VAT 
IS 'Configured to show VAT in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_SPACK 
IS 'Configured to show Simple Pack in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_UDA 
IS 'Configured to show UDA in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_LOC 
IS 'Configured to show Locations in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_SEASONS 
IS 'Configured to show Seasons/Phases in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_REPL 
IS 'Configured to show Replenishment in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_SUBS_ITEM 
IS 'Configured to show Substitute Items in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_DIMEN 
IS 'Configured to show Dimensions in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_REL_ITEM 
IS 'Configured to show Related Items in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_TICKETS 
IS 'Configured to show Tickets in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_HTS 
IS 'Configured to show HTS in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_IMP_ATTR 
IS 'Configured to show Import Attributes in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

COMMENT ON COLUMN V_INCOMP_ITEM_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_IMAGES 
IS 'Configured to show Images in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/
