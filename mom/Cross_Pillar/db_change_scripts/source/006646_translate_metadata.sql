--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'TRANSLATE_METADATA'
ALTER TABLE TRANSLATE_METADATA ADD LABEL VARCHAR2 (250 ) DEFAULT 'TRANSLATE_COLUMN' NOT NULL
/

COMMENT ON COLUMN TRANSLATE_METADATA.LABEL is 'This contains the default UI label to be used for this field if the label is not available in the UI layer.'
/

