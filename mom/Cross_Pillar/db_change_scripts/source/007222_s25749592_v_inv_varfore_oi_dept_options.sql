--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW CREATED:               V_INV_VARFORE_OI_DEPT_OPTIONS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATE VIEW
--------------------------------------
PROMPT Creating View 'V_INV_VARFORE_OI_DEPT_OPTIONS'
Create or replace view 
V_INV_VARFORE_OI_DEPT_OPTIONS
AS
select
 ro.DEPT
,dp.DEPT_NAME
,IA_VARIANCE_RANGE_PCT_1
,IA_VARIANCE_RANGE_PCT_2
,IA_VARIANCE_RANGE_PCT_3
,IA_VARIANCE_RANGE_PCT_4
from RMS_OI_DEPT_OPTIONS ro , DEPS dp
where ro.DEPT = dp.DEPT AND (
IA_VARIANCE_RANGE_PCT_1 is not null
or IA_VARIANCE_RANGE_PCT_2 is not null
or IA_VARIANCE_RANGE_PCT_3 is not null
or IA_VARIANCE_RANGE_PCT_4 is not null
)
/

COMMENT ON TABLE V_INV_VARFORE_OI_DEPT_OPTIONS IS 'This view returns department level report option for Inventory Variance to Forecast report.'
/

COMMENT ON COLUMN V_INV_VARFORE_OI_DEPT_OPTIONS.DEPT IS 'Deprtment.'
/
COMMENT ON COLUMN V_INV_VARFORE_OI_DEPT_OPTIONS.DEPT_NAME IS 'Department Name'
/
COMMENT ON COLUMN V_INV_VARFORE_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_1 IS 'Configuring first % value for Inventory Variance to Forecast tile.'
/
COMMENT ON COLUMN V_INV_VARFORE_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_2 IS 'Configuring second % value for Inventory Variance to Forecast tile.'
/
COMMENT ON COLUMN V_INV_VARFORE_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_3 IS 'Configuring third % value for Inventory Variance to Forecast tile.'
/
COMMENT ON COLUMN V_INV_VARFORE_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_4 IS 'Configuring fourth % value for Inventory Variance to Forecast tile.'
/
