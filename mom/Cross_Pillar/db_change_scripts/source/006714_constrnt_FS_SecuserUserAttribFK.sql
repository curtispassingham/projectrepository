--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SEC_USER'

PROMPT Dropping Foreign Key on 'SEC_USER'
DECLARE
  L_constraints_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'SEU_UAT_FK'
     AND CONSTRAINT_TYPE = 'R';

  if (L_constraints_exists != 0) then
      execute immediate 'ALTER TABLE SEC_USER DROP CONSTRAINT SEU_UAT_FK';
  end if;
end;
/
