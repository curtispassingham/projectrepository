--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
whenever sqlerror exit

----------------------------------------------------------
--       Drop Package         RMS_NOTIFICATION_SQL
----------------------------------------------------------
PROMPT dropping Package 'RMS_NOTIFICATION_SQL'

DECLARE
  L_package_exists number := 0;
BEGIN
   SELECT count(1) INTO L_package_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'RMS_NOTIFICATION_SQL'
   AND OBJECT_TYPE ='PACKAGE';
   
if (L_package_exists != 0) then
      execute immediate 'DROP PACKAGE RMS_NOTIFICATION_SQL';
  end if;
end;
/
