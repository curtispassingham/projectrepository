--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ATTRIB_GROUP_SET
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ATTRIB_GROUP_SET'
CREATE TABLE SVC_ATTRIB_GROUP_SET
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ) NOT NULL,
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N' NOT NULL,
  GROUP_SET_ID NUMBER(30) NOT NULL,
  BASE_RMS_TABLE VARCHAR2(30 ) NOT NULL,
  DISPLAY_SEQ NUMBER(2) NOT NULL,
  GROUP_SET_VIEW_NAME VARCHAR2(30 ) NOT NULL,
  STAGING_TABLE_NAME VARCHAR2(30 ),
  QUALIFIER_FUNC VARCHAR2(61 ),
  VALIDATION_FUNC VARCHAR2(61 ),
  DEFAULT_FUNC VARCHAR2(61 ),
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  LAST_UPD_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPD_DATETIME DATE NOT NULL
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

COMMENT ON TABLE SVC_ATTRIB_GROUP_SET is 'This is the staging table for CFAS attribute group set setup information.It is used to temporarily hold data before it is uploaded/updated in CFA_ATTRIB_GROUP_SET.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.CHUNK_ID is 'Uniquely identifies a chunk.The value will always be 1.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.ROW_SEQ is 'The rows sequence. Should be unique within a process-ID.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.GROUP_SET_ID is 'Unique idenfier for the for the attribute group set.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.BASE_RMS_TABLE is 'RMS table where the group set belongs to.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.DISPLAY_SEQ is 'The order at which the group set entries are displayed on the UI.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.GROUP_SET_VIEW_NAME is 'This column holds the name of the database view that will be generated to make access to user entered data easier.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.STAGING_TABLE_NAME is 'The name of the staging area where data from an external source can be stored and exported to the CFA extension table linked to this group set.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.QUALIFIER_FUNC is 'This column holds the name of the stored procedure (package and function) that should be called to check if required information is supplied to the base UI to access the attributes within the group set.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.VALIDATION_FUNC is 'This column holds the name of the stored procedure (package and function) that should be called to validate the attribute group set.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.DEFAULT_FUNC is 'This column holds the name of the stored procedure (package and function)  that should be called on startup of the CFAS UI to pre-populate attribute fields with default values (can be in any group within the set).'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_SET.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_ATTRIB_GROUP_SET'
ALTER TABLE SVC_ATTRIB_GROUP_SET
 ADD CONSTRAINT SVC_ATTRIB_GROUP_SET_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_ATTRIB_GROUP_SET'
ALTER TABLE SVC_ATTRIB_GROUP_SET
 ADD CONSTRAINT SVC_ATTRIB_GROUP_SET_UK UNIQUE
  (GROUP_SET_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

