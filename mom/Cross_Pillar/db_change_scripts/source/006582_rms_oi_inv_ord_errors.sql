--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD CURRENCY_CODE VARCHAR2 (3 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.CURRENCY_CODE is 'The currency of the order.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD ERROR_MESSAGE VARCHAR2 (255 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.ERROR_MESSAGE is 'The description of the error associated with the order'
/

