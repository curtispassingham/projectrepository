--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_LATE_POST_TRANSACTIONS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_LATE_POST_TRANSACTIONS'
CREATE TABLE RMS_OI_LATE_POST_TRANSACTIONS
 (SESSION_ID NUMBER(15),
  REGION NUMBER(10),
  REGION_NAME VARCHAR2(150 ),
  DISTRICT NUMBER(10),
  DISTRICT_NAME VARCHAR2(150 ),
  LOC NUMBER(10),
  LOC_NAME VARCHAR2(150 ),
  DEPT NUMBER(4),
  DEPT_NAME VARCHAR2(120 ),
  CLASS NUMBER(4),
  CLASS_NAME VARCHAR2(120 ),
  SUBCLASS NUMBER(4),
  SUBCLASS_NAME VARCHAR2(120 ),
  TRAN_TYPE VARCHAR2(4 ),
  TRAN_TYPE_DESC VARCHAR2(250 ),
  TOTAL_COST NUMBER(20,4),
  TOTAL_RETAIL NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_LATE_POST_TRANSACTIONS is 'This table holds the information about sof any late posted transactions that have occurred since the last month close for total company and by location for the Late posted transaction Variance Report.'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.SESSION_ID is 'Session id'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.REGION is 'Region '
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.REGION_NAME is 'Region name'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.DISTRICT is 'District '
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.DISTRICT_NAME is 'District name'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.LOC is 'Location'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.LOC_NAME is 'Location Name'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.DEPT is 'Department number'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.DEPT_NAME is 'Department name'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.CLASS is 'Class number'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.CLASS_NAME is 'Class name'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.SUBCLASS is 'Subclass number'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.SUBCLASS_NAME is 'Subclass name'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.TRAN_TYPE is 'Transaction type'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.TRAN_TYPE_DESC is 'Transaction type description'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.TOTAL_COST is 'Total cost'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.TOTAL_RETAIL is 'Total retail'
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.CURRENCY_CODE is 'Currency code'
/

PROMPT Creating Index 'RMS_OI_LATE_POST_TRANS_I1'
CREATE INDEX RMS_OI_LATE_POST_TRANS_I1 ON RMS_OI_LATE_POST_TRANSACTIONS
(SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
