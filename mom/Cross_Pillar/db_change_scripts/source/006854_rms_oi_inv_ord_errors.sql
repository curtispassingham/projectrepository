--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD ORDER_TYPE VARCHAR2 (3 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.ORDER_TYPE is 'Indicates the type of order and which Open To Buy bucket will be updated.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD WF_ORDER_NO NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.WF_ORDER_NO is 'This column contains the franchise order number for which the purchase order was created.'
/

