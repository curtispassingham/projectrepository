--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_REF_ITEM is ' Configured to show Reference Items in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_VAT is ' Configured to show VAT in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_SPACK is ' Configured to show Simple Pack in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_UDA is ' Configured to show UDA in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_LOC is ' Configured to show Locations in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_SEASONS is ' Configured to show Seasons/Phases in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_REPL is ' Configured to show Replenishment in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_SUBS_ITEM is ' Configured to show Substitute Items in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_DIMEN is ' Configured to show Dimensions in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_REL_ITEM is ' Configured to show Related Items in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_TICKETS is ' Configured to show Tickets in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_HTS is ' Configured to show HTS in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_IMP_ATTR is ' Configured to show Import Attributes in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.DS_SHOW_INCOMP_ITEM_IMAGES is ' Configured to show Images in Incomplete Items Report. Valid values are ''R''equired or ''O''ptional or ''N''ot required. '
/


PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_REF_ITEM'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_REF_ITEM'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_REF_ITEM';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_REF_ITEM CHECK (DS_SHOW_INCOMP_ITEM_REF_ITEM IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_VAT'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_VAT'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_VAT';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_VAT CHECK (DS_SHOW_INCOMP_ITEM_VAT IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_SPACK'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_SPACK'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_SPACK';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_SPACK CHECK (DS_SHOW_INCOMP_ITEM_SPACK IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_UDA'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_UDA'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_UDA';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_UDA CHECK (DS_SHOW_INCOMP_ITEM_UDA IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_LOC'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_LOC'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_LOC';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_LOC CHECK (DS_SHOW_INCOMP_ITEM_LOC IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_SEASONS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_SEASONS'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_SEASONS';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_SEASONS CHECK (DS_SHOW_INCOMP_ITEM_SEASONS IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_REPL'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_REPL'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_REPL';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_REPL CHECK (DS_SHOW_INCOMP_ITEM_REPL IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_SUBS_ITEM'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_SUBS_ITEM'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_SUBS_ITEM';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_SUBS_ITEM CHECK (DS_SHOW_INCOMP_ITEM_SUBS_ITEM IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_DIMEN'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_DIMEN'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_DIMEN';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_DIMEN CHECK (DS_SHOW_INCOMP_ITEM_DIMEN IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_REL_ITEM'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_REL_ITEM'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_REL_ITEM';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_REL_ITEM CHECK (DS_SHOW_INCOMP_ITEM_REL_ITEM IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_TICKETS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_TICKETS'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_TICKETS';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_TICKETS CHECK (DS_SHOW_INCOMP_ITEM_TICKETS IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_HTS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_HTS'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_HTS';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_HTS CHECK (DS_SHOW_INCOMP_ITEM_HTS IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_IMP_ATTR'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_IMP_ATTR'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_IMP_ATTR';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_IMP_ATTR CHECK (DS_SHOW_INCOMP_ITEM_IMP_ATTR IN ('R','O','N'))
/

PROMPT MODIFYING CONSTRAINT 'CHK_ROSO_DS_SHOW_IMAGES'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ROSO_DS_SHOW_IMAGES'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_ROSO_DS_SHOW_IMAGES';
  end if;
end;
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_ROSO_DS_SHOW_IMAGES CHECK (DS_SHOW_INCOMP_ITEM_IMAGES IN ('R','O','N'))
/
