--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       LANG_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE LANG_TL(
LANG NUMBER(6) NOT NULL,
LANG_LANG NUMBER(6) NOT NULL,
DESCRIPTION VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE LANG_TL is 'This is the translation table for LANG table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN LANG_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN LANG_TL.LANG_LANG is 'Contains a number which uniquely identifies a language.'
/

COMMENT ON COLUMN LANG_TL.DESCRIPTION is 'Contains a description or name for the language.'
/

COMMENT ON COLUMN LANG_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN LANG_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN LANG_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN LANG_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE LANG_TL ADD CONSTRAINT PK_LANG_TL PRIMARY KEY (
LANG,
LANG_LANG
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE LANG_TL
 ADD CONSTRAINT LANGT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE LANG_TL ADD CONSTRAINT LANGT1_LANG_FK FOREIGN KEY (
LANG_LANG
) REFERENCES LANG (
LANG
)
/

