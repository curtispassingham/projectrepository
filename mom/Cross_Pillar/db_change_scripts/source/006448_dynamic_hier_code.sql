--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Dropping Table: 		 DYNAMIC_HIER_CODE
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Dropping Table               
--------------------------------------
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_TABLES
   WHERE TABLE_NAME = 'DYNAMIC_HIER_CODE';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE DYNAMIC_HIER_CODE CASCADE CONSTRAINTS';
  end if;
end;
/