--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Tables 
-------------------------------------- 
PROMPT DROPPING Table 'LOC_AREA_TRAITS'
DECLARE
  L_table_loc_area_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_loc_area_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'LOC_AREA_TRAITS';

  if (L_table_loc_area_exists != 0) then
      execute immediate 'drop table LOC_AREA_TRAITS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'LOC_REGION_TRAITS'
DECLARE
  L_table_loc_region_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_loc_region_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'LOC_REGION_TRAITS';

  if (L_table_loc_region_exists != 0) then
      execute immediate 'drop table LOC_REGION_TRAITS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'LOC_DISTRICT_TRAITS'
DECLARE
  L_table_loc_district_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_loc_district_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'LOC_DISTRICT_TRAITS';

  if (L_table_loc_district_exists != 0) then
      execute immediate 'drop table LOC_DISTRICT_TRAITS CASCADE CONSTRAINTS';
  end if;
end;
/
