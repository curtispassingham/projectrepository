--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'CORESVC_COSTCHG_CONFIG'
ALTER TABLE CORESVC_COSTCHG_CONFIG ADD VARIANCE_COMPARISON_BASIS VARCHAR2 (1 ) DEFAULT 'B' NOT NULL
/

COMMENT ON COLUMN CORESVC_COSTCHG_CONFIG.VARIANCE_COMPARISON_BASIS is 'The basis by which the variance between the new and old unit cost is compared against the supplier''s set limits.  Valid values are Both (B) - new cost must fall within both percent and dollar variance of the old cost, Either (E) - new cost must fall within either percent or dollar variance of the old cost, Dollar (D) - new cost must fall within dollar variance of the old cost, Percent (P) - new cost must fall within percent variance of the old cost.'
/

