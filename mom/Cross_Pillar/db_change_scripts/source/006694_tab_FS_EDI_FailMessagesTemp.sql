--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to rEDI_FAIL_MESSAGES_TEMP this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING Table EDI_FAIL_MESSAGES_TEMP
-------------------------------------------
PROMPT DROPPING table EDI_FAIL_MESSAGES_TEMP

DECLARE
  L_group_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_group_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'EDI_FAIL_MESSAGES_TEMP';

  if (L_group_table_exists != 0) then
      execute immediate 'DROP TABLE EDI_FAIL_MESSAGES_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/