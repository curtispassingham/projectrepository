--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ATTRIB_GROUP'
ALTER TABLE SVC_ATTRIB_GROUP MODIFY ACTION NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY PROCESS$STATUS DEFAULT 'N' NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY GROUP_ID NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.GROUP_ID is 'This column holds a generated ID that distinguishes the custom attribute group.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY GROUP_SET_ID NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.GROUP_SET_ID is 'This column holds id of the set where the group belongs to.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY GROUP_VIEW_NAME NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.GROUP_VIEW_NAME is 'This column holds the name of the database view that will be generated to make access to user entered data easier.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY DISPLAY_SEQ NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.DISPLAY_SEQ is 'This column holds the order the attribute groups will be displayed in on the CFAS UI when multiple groups exist for a single attribute group set.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY CREATE_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.CREATE_ID is 'User who created the record.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY CREATE_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.CREATE_DATETIME is 'Date time when record was inserted.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY LAST_UPD_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.LAST_UPD_ID is 'User who last updated the record.'
/

ALTER TABLE SVC_ATTRIB_GROUP MODIFY LAST_UPD_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/

