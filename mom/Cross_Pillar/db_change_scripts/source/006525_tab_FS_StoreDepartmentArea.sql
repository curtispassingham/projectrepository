--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop Table               
--------------------------------------
PROMPT dropping Table 'STORE_DEPT_AREA'
DECLARE
  L_table_exists1 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_DEPT_AREA'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists1 != 0) then
      execute immediate 'DROP TABLE STORE_DEPT_AREA CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'STORE_DEPT_AREA_TEMP'
DECLARE
  L_table_exists2 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists2
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_DEPT_AREA_TEMP'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists2 != 0) then
      execute immediate 'DROP TABLE STORE_DEPT_AREA_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/
