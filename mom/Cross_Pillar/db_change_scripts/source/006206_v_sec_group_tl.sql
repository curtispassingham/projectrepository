CREATE OR REPLACE FORCE VIEW V_SEC_GROUP_TL (GROUP_ID, GROUP_NAME, LANG ) AS
SELECT  b.group_id,
        case when tl.lang is not null then tl.group_name else b.group_name end group_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SEC_GROUP b,
        SEC_GROUP_TL tl
 WHERE  b.group_id = tl.group_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SEC_GROUP_TL is 'This is the translation view for base table SEC_GROUP. This view fetches data in user langauge either from translation table SEC_GROUP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SEC_GROUP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SEC_GROUP_TL.GROUP_ID is 'Contains the unique identifier associated with the group.'
/

COMMENT ON COLUMN V_SEC_GROUP_TL.GROUP_NAME is 'Contains the name of the security group.'
/

