--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_UNEXPECTED_INV
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_UNEXPECTED_INV'
CREATE TABLE RMS_OI_UNEXPECTED_INV
 (SESSION_ID NUMBER(15,0),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  ITEM_PARENT VARCHAR2(25 ),
  ITEM_PARENT_DESC VARCHAR2(250 ),
  LOC NUMBER(10,0),
  LOC_NAME VARCHAR2(150 ),
  LOC_TYPE VARCHAR2(150 ),
  STATUS VARCHAR2(1 ),
  RANGED_IND VARCHAR2(1 ),
  STOCK_ON_HAND NUMBER(12,4),
  RESERVED_QTY NUMBER(12,4),
  NON_SELLABLE_QTY NUMBER(12,4),
  UNEXPECTED_QTY NUMBER(12,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_UNEXPECTED_INV is 'This table holds the information for the Unexpected Inventory Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.ITEM is 'The item.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.ITEM_DESC is 'The item description.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.ITEM_PARENT is 'The parent item.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.ITEM_PARENT_DESC is 'The parent item description.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.LOC is 'The location.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.LOC_NAME is 'The location name.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.LOC_TYPE is 'The location type.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.STATUS is 'The item/location status.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.RANGED_IND is 'The item/location ranged indicator.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.STOCK_ON_HAND is 'The stock on hand quantity.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.RESERVED_QTY is 'The reserved quantity.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.NON_SELLABLE_QTY is 'The non sellable quantity.'
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.UNEXPECTED_QTY is 'The unexpected quantity.'
/


PROMPT Creating Index on 'RMS_OI_UNEXPECTED_INV'
 CREATE INDEX RMS_OI_UNEXPECTED_INV_I1 on RMS_OI_UNEXPECTED_INV
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

