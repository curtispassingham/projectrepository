--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ITEM_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ITEM_ERRORS DROP COLUMN ERROR_CODE
/

ALTER TABLE RMS_OI_INV_ORD_ITEM_ERRORS ADD ERROR_MESSAGE VARCHAR2 (255 ) 
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ERROR_MESSAGE is 'The error message associated with the order/item.'
/

