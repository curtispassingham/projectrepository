--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_CTL_NEG_INV
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_CTL_NEG_INV'
CREATE TABLE RMS_OI_INV_CTL_NEG_INV
 (SESSION_ID NUMBER(15,0),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  STANDARD_UOM VARCHAR2(4 ),
  LOC NUMBER(10,0),
  LOC_NAME VARCHAR2(150 ),
  LOC_TYPE VARCHAR2(150 ),
  STOCK_ON_HAND NUMBER(12,4),
  IN_PROGRESS_PO_COUNT NUMBER(10,0),
  IN_PROGRESS_TSF_COUNT NUMBER(10,0),
  IN_PROGRESS_ALLOC_COUNT NUMBER(10,0)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_INV_CTL_NEG_INV is 'This table holds the information for the Negative Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.ITEM is 'The item.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.ITEM_DESC is 'The item description'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.STANDARD_UOM is 'The standard UOM for the item'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.LOC is 'The location'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.LOC_NAME is 'The location name.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.LOC_TYPE is 'The type of location'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.STOCK_ON_HAND is 'The stock on hand for the item/location.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.IN_PROGRESS_PO_COUNT is 'The number of in process orders.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.IN_PROGRESS_TSF_COUNT is 'The number of in process transfers.'
/

COMMENT ON COLUMN RMS_OI_INV_CTL_NEG_INV.IN_PROGRESS_ALLOC_COUNT is 'The number of in process allocations.'
/


PROMPT Creating Index on 'RMS_OI_INV_CTL_NEG_INV'
 CREATE INDEX RMS_OI_INV_CTL_NEG_INV_I1 on RMS_OI_INV_CTL_NEG_INV
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

