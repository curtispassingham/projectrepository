--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_OVERDUE_SHIP_ALLOC'
ALTER TABLE RMS_OI_OVERDUE_SHIP_ALLOC DROP COLUMN ALLOC_COST
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_ALLOC DROP COLUMN ALLOC_RETAIL
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_ALLOC DROP COLUMN CURRENCY_CODE
/

