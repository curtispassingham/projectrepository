--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      PROCEDURES to be DROPPED:    
                          NEXT_STOP_SHIP_NO
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING PROCEDURES
--------------------------------------
PROMPT DROPPING procedure 'NEXT_STOP_SHIP_NO'
DECLARE
  L_prc_exists number := 0;
BEGIN
  SELECT count(*) INTO L_prc_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NEXT_STOP_SHIP_NO';

  if (L_prc_exists != 0) then
      execute immediate 'DROP PROCEDURE NEXT_STOP_SHIP_NO';
  end if;
end;
/