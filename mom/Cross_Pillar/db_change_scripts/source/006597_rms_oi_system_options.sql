--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_VARIANCE_TO_FORECAST_IND VARCHAR2 (1 BYTE) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_VARIANCE_TO_FORECAST_IND is 'Indicates if the Inventory Variance to Forecast report in the Inventory Analyst dashboard is supported. If yes, the system will preserve 4 weeks of item weekly forecasted sales data before loading the next set of forecasting data. '
/

