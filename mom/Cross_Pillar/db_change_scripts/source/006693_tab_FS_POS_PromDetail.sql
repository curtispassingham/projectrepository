--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to rPOS_PROM_DETAIL this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING Table POS_PROM_DETAIL
-------------------------------------------
PROMPT DROPPING table POS_PROM_DETAIL

DECLARE
  L_group_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_group_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'POS_PROM_DETAIL';

  if (L_group_table_exists != 0) then
      execute immediate 'DROP TABLE POS_PROM_DETAIL CASCADE CONSTRAINTS';
  end if;
end;
/