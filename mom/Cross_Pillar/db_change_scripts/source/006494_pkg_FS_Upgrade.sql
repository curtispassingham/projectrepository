--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------
 
 
----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
--    The customer DBA is responsible to review this script to ensure
--    data is preserved as desired.
--
----------------------------------------------------------------------------
--    PACKAGE DROPPED:                
----------------------------------------------------------------------------
 
whenever sqlerror exit
 
--------------------------------------------
--      DROPPING PACKAGE
--------------------------------------------
 
DECLARE
  L_table_exists1 number := 0;
BEGIN
 SELECT count(*)INTO L_table_exists1
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='MULTIVIEW_45_SQL'
    AND OBJECT_TYPE ='PACKAGE';

  
  if (L_table_exists1 != 0)then
      execute immediate'DROP PACKAGE MULTIVIEW_45_SQL' ;
 end if;
end;
/

DECLARE
  L_table_exists2 number := 0;
BEGIN
 SELECT count(*)INTO L_table_exists2
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='GUI_TRANSLATION_SQL'
    AND OBJECT_TYPE ='PACKAGE';

  
  if (L_table_exists2 != 0)then
      execute immediate'DROP PACKAGE GUI_TRANSLATION_SQL' ;
 end if;
end;
/

DECLARE
  L_table_exists3 number := 0;
BEGIN
 SELECT count(*)INTO L_table_exists3
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='RMS_BATCH_STATUS_WRP'
    AND OBJECT_TYPE ='PACKAGE';

  
  if (L_table_exists3 != 0)then
      execute immediate'DROP PACKAGE RMS_BATCH_STATUS_WRP' ;
 end if;
end;
/

DECLARE
  L_table_exists4 number := 0;
BEGIN
 SELECT count(*)INTO L_table_exists4
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='RMS2FIN_WRP'
    AND OBJECT_TYPE ='PACKAGE';

  
  if (L_table_exists4 != 0)then
      execute immediate'DROP PACKAGE RMS2FIN_WRP' ;
 end if;
end;
/

