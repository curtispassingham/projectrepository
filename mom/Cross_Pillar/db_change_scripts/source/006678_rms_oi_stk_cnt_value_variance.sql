--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_STK_CNT_VALUE_VARIANCE'
ALTER TABLE RMS_OI_STK_CNT_VALUE_VARIANCE DROP COLUMN OVER_VARIANCE_PCT
/

ALTER TABLE RMS_OI_STK_CNT_VALUE_VARIANCE DROP COLUMN SHORT_VARIANCE_PCT
/

ALTER TABLE RMS_OI_STK_CNT_VALUE_VARIANCE RENAME COLUMN BOOK_VALUE to Snapshot_Value
/

COMMENT ON COLUMN RMS_OI_STK_CNT_VALUE_VARIANCE.Snapshot_Value is 'Book value for the count location/merchandise hierarchy'
/

