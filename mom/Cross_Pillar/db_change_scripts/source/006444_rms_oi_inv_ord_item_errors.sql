--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_ORD_ITEM_ERRORS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_ORD_ITEM_ERRORS'
CREATE TABLE RMS_OI_INV_ORD_ITEM_ERRORS
 (SESSION_ID NUMBER(15,0),
  ORDER_NO NUMBER(12,0),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  ITEM_PARENT VARCHAR2(25 ),
  ITEM_PARENT_DESC VARCHAR2(250 ),
  VPN VARCHAR2(30 ),
  REF_ITEM VARCHAR2(25 ),
  REF_ITEM_DESC VARCHAR2(250 ),
  IMPORT_ORDER_IND VARCHAR2(1 ),
  MARGIN NUMBER(20,10),
  ERROR_CODE VARCHAR2(25 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_INV_ORD_ITEM_ERRORS is 'This table holds data for the Inventory Analyst Order Errors Item Detail Report.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ORDER_NO is 'The order number.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ITEM is 'The item on the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ITEM_DESC is 'The description of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ITEM_PARENT is 'The item parent.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ITEM_PARENT_DESC is 'The item parent description.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.VPN is 'The of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.REF_ITEM is 'The ref item.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.REF_ITEM_DESC is 'The ref item description.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.IMPORT_ORDER_IND is 'The import_order_ind for the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.MARGIN is 'The margin for the order.'
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.ERROR_CODE is 'The error code for this row.'
/

PROMPT Creating Index on 'RMS_OI_INV_ORD_ITEM_ERRORS'
 CREATE INDEX RMS_OI_INV_ORD_ITEM_ERRORS_I1 on RMS_OI_INV_ORD_ITEM_ERRORS
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
