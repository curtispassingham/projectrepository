--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 S9T_TEMPLATE_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'S9T_TEMPLATE_TL'
CREATE TABLE S9T_TEMPLATE_TL
 (TEMPLATE_KEY VARCHAR2(255 ) NOT NULL,
  LANG NUMBER(6,0) NOT NULL,
  TEMPLATE_NAME VARCHAR2(255 ) NOT NULL,
  TEMPLATE_DESC VARCHAR2(4000 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE S9T_TEMPLATE_TL is 'This table contains translations by language for the template definition contained in the S9T_TEMPLATE table.'
/

COMMENT ON COLUMN S9T_TEMPLATE_TL.TEMPLATE_KEY is 'The template key. Maps to s9t_template.template_key.'
/

COMMENT ON COLUMN S9T_TEMPLATE_TL.LANG is 'The RMS language code the template name and description is in. Maps to lang.lang.'
/

COMMENT ON COLUMN S9T_TEMPLATE_TL.TEMPLATE_NAME is 'The translated name of the template.'
/

COMMENT ON COLUMN S9T_TEMPLATE_TL.TEMPLATE_DESC is 'The translated description of the template.'
/


PROMPT Creating Primary Key on 'S9T_TEMPLATE_TL'
ALTER TABLE S9T_TEMPLATE_TL
 ADD CONSTRAINT S9T_TEMPLATE_TL_PK PRIMARY KEY
  (TEMPLATE_KEY,
   LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'S9T_TEMPLATE_TL'
 ALTER TABLE S9T_TEMPLATE_TL
  ADD CONSTRAINT S9T_TEMPLATE_TL_S9T_T_FK
  FOREIGN KEY (TEMPLATE_KEY)
 REFERENCES S9T_TEMPLATE (TEMPLATE_KEY)
/


PROMPT Creating FK on 'S9T_TEMPLATE_TL'
 ALTER TABLE S9T_TEMPLATE_TL
  ADD CONSTRAINT S9T_TEMPLATE_TL_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


