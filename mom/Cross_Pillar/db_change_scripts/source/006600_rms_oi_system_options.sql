--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD B_PO_PENDING_APPROVAL_LEVEL VARCHAR2 (1 ) DEFAULT 'S' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.B_PO_PENDING_APPROVAL_LEVEL is 'When set to S only submitted Purchase Orders will be fetch. When set to W then both worksheet and submitted Purchase Orders will be fetch.'
/


PROMPT ADDING CONSTRAINT 'CHK_B_PO_PENDING_APPROVAL_LVL'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_B_PO_PENDING_APPROVAL_LVL CHECK (B_PO_PENDING_APPROVAL_LEVEL IN ('W','S'))
/
