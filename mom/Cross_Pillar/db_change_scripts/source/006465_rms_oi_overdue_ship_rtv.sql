--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_OVERDUE_SHIP_RTV
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_OVERDUE_SHIP_RTV'
CREATE TABLE RMS_OI_OVERDUE_SHIP_RTV
 (SESSION_ID NUMBER(15,0),
  RTV_ORDER_NO NUMBER(10,0),
  FROM_LOC NUMBER(10,0),
  FROM_LOC_NAME VARCHAR2(150 ),
  FROM_LOC_TYPE VARCHAR2(1 ),
  SUPPLIER_SITE NUMBER(10,0),
  SUPPLIER_SITE_NAME VARCHAR2(240 ),
  CREATED_DATE DATE,
  NOT_AFTER_DATE DATE,
  RTV_COST NUMBER(20,4),
  RTV_RETAIL NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_OVERDUE_SHIP_RTV is 'This table holds the information for the RTVs Pending Close Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.RTV_ORDER_NO is 'The RTV id.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.FROM_LOC is 'The RTV from location.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.FROM_LOC_NAME is 'The RTV from location name.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.FROM_LOC_TYPE is 'The RTV from location type.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.SUPPLIER_SITE is 'The supplier site for the RTV.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.SUPPLIER_SITE_NAME is 'The supplier name.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.CREATED_DATE is 'The create date.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.NOT_AFTER_DATE is 'The not after date.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.RTV_COST is 'The RTV cost.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.RTV_RETAIL is 'The RTV retial.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.CURRENCY_CODE is 'The currency code.'
/


PROMPT Creating Index on 'RMS_OI_OVERDUE_SHIP_RTV'
 CREATE INDEX RMS_OI_OVERDUE_SHIP_RTV_I1 on RMS_OI_OVERDUE_SHIP_RTV
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

