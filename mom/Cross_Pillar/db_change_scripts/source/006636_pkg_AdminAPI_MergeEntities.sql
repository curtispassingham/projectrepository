--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--				Dropping AdminAPI_MergeEntities Packages
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Drop Packages              
--------------------------------------
PROMPT Dropping Package 'CORESVC_VAT_REGION'
DECLARE
  l_package_region_exists number := 0;
BEGIN
  SELECT count(*) INTO l_package_region_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_VAT_REGION';

  if (l_package_region_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_VAT_REGION';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_VAT_CODES'
DECLARE
  l_package_codes_exists number := 0;
BEGIN
  SELECT count(*) INTO l_package_codes_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_VAT_CODES';

  if (l_package_codes_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_VAT_CODES';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_WO_ACTIVITY'
DECLARE
  l_package_activity_exists number := 0;
BEGIN
  SELECT count(*) INTO l_package_activity_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_WO_ACTIVITY';

  if (l_package_activity_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_WO_ACTIVITY';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_WO_TMPL'
DECLARE
  l_package_tmpl_exists number := 0;
BEGIN
  SELECT count(*) INTO l_package_tmpl_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_WO_TMPL';

  if (l_package_tmpl_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_WO_TMPL';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_BUDGETS'
DECLARE
  l_budgets_exists number := 0;
BEGIN
  SELECT count(*) INTO l_budgets_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_BUDGETS';

  if (l_budgets_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_BUDGETS';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_FREIGHT_SIZE'
DECLARE
  l_size_exists number := 0;
BEGIN
  SELECT count(*) INTO l_size_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_FREIGHT_SIZE';

  if (l_size_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_FREIGHT_SIZE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_FREIGHT_TYPE'
DECLARE
  l_type_exists number := 0;
BEGIN
  SELECT count(*) INTO l_type_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_FREIGHT_TYPE';

  if (l_type_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_FREIGHT_TYPE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_INV_STATUS_TYPES'
DECLARE
  l_status_exists number := 0;
BEGIN
  SELECT count(*) INTO l_status_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_INV_STATUS_TYPES';

  if (l_status_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_INV_STATUS_TYPES';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_INV_ADJ_REASON'
DECLARE
  l_adj_exists number := 0;
BEGIN
  SELECT count(*) INTO l_adj_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_INV_ADJ_REASON';

  if (l_adj_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_INV_ADJ_REASON';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_BANNER'
DECLARE
  l_banner_exists number := 0;
BEGIN
  SELECT count(*) INTO l_banner_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_BANNER';

  if (l_banner_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_BANNER';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_CHANNELS'
DECLARE
  l_channels_exists number := 0;
BEGIN
  SELECT count(*) INTO l_channels_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_CHANNELS';

  if (l_channels_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_CHANNELS';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_BUYER'
DECLARE
  l_buyer_exists number := 0;
BEGIN
  SELECT count(*) INTO l_buyer_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_BUYER';

  if (l_buyer_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_BUYER';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_MERCHANT'
DECLARE
  l_merchant_exists number := 0;
BEGIN
  SELECT count(*) INTO l_merchant_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_MERCHANT';

  if (l_merchant_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_MERCHANT';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_COMPANY_CLOSED_EXCEP'
DECLARE
  l_company_exists number := 0;
BEGIN
  SELECT count(*) INTO l_company_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_COMPANY_CLOSED_EXCEP';

  if (l_company_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_COMPANY_CLOSED_EXCEP';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_UID'
DECLARE
  l_uid_exists number := 0;
BEGIN
  SELECT count(*) INTO l_uid_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_UID';

  if (l_uid_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_UID';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_COMP_STORE'
DECLARE
  l_store_exists number := 0;
BEGIN
  SELECT count(*) INTO l_store_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_COMP_STORE';

  if (l_store_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_COMP_STORE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_COMP_STORE_LINK'
DECLARE
  l_link_exists number := 0;
BEGIN
  SELECT count(*) INTO l_link_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_COMP_STORE_LINK';

  if (l_link_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_COMP_STORE_LINK';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_COMP_SHOPPER'
DECLARE
  l_shopper_exists number := 0;
BEGIN
  SELECT count(*) INTO l_shopper_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_COMP_SHOPPER';

  if (l_shopper_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_COMP_SHOPPER';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_CUSTOMER_SEGMENT_TYPES'
DECLARE
  l_segment_exists number := 0;
BEGIN
  SELECT count(*) INTO l_segment_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_CUSTOMER_SEGMENT_TYPES';

  if (l_segment_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_CUSTOMER_SEGMENT_TYPES';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_WF_CUST_GRP'
DECLARE
  l_coresvc_exists number := 0;
BEGIN
  SELECT count(*) INTO l_coresvc_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_WF_CUST_GRP';

  if (l_coresvc_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_WF_CUST_GRP';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_DIFF_IDS'
DECLARE
  l_diff_exists number := 0;
BEGIN
  SELECT count(*) INTO l_diff_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_DIFF_IDS';

  if (l_diff_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_DIFF_IDS';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_DIFF_TYPE'
DECLARE
  l_diff_type_exists number := 0;
BEGIN
  SELECT count(*) INTO l_diff_type_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_DIFF_TYPE';

  if (l_diff_type_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_DIFF_TYPE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_TIME_STEPS'
DECLARE
  l_time_exists number := 0;
BEGIN
  SELECT count(*) INTO l_time_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_TIME_STEPS';

  if (l_time_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_TIME_STEPS';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_TIMESTEP_COMP'
DECLARE
  l_timestamp_exists number := 0;
BEGIN
  SELECT count(*) INTO l_timestamp_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_TIMESTEP_COMP';

  if (l_timestamp_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_TIMESTEP_COMP';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_AD'
DECLARE
  l_hts_exists number := 0;
BEGIN
  SELECT count(*) INTO l_hts_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_AD';

  if (l_hts_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_AD';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_CVD'
DECLARE
  l_hts_cvd_exists number := 0;
BEGIN
  SELECT count(*) INTO l_hts_cvd_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_CVD';

  if (l_hts_cvd_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_CVD';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_FEE_ZONE'
DECLARE
  l_fee_exists number := 0;
BEGIN
  SELECT count(*) INTO l_fee_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_FEE_ZONE';

  if (l_fee_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_FEE_ZONE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_FEE'
DECLARE
  l_fee_zone_exists number := 0;
BEGIN
  SELECT count(*) INTO l_fee_zone_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_FEE';

  if (l_fee_zone_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_FEE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_OGA'
DECLARE
  l_oga_exists number := 0;
BEGIN
  SELECT count(*) INTO l_oga_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_OGA';

  if (l_oga_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_OGA';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_REFERENCE'
DECLARE
  l_reference_exists number := 0;
BEGIN
  SELECT count(*) INTO l_reference_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_REFERENCE';

  if (l_reference_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_REFERENCE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_TARIFF_TREATMENT'
DECLARE
  l_tariff_exists number := 0;
BEGIN
  SELECT count(*) INTO l_tariff_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_TARIFF_TREATMENT';

  if (l_tariff_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_TARIFF_TREATMENT';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_TAX_ZONE'
DECLARE
  l_tax_zone_exists number := 0;
BEGIN
  SELECT count(*) INTO l_tax_zone_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_TAX_ZONE';

  if (l_tax_zone_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_TAX_ZONE';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_TAX'
DECLARE
  l_tax_exists number := 0;
BEGIN
  SELECT count(*) INTO l_tax_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_TAX';

  if (l_tax_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_TAX';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_TRF_TRTMNT_ZN'
DECLARE
  l_hts_trf_exists number := 0;
BEGIN
  SELECT count(*) INTO l_hts_trf_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_TRF_TRTMNT_ZN';

  if (l_hts_trf_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_TRF_TRTMNT_ZN';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS'
DECLARE
  l_coresvc_hts_exists number := 0;
BEGIN
  SELECT count(*) INTO l_coresvc_hts_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS';

  if (l_coresvc_hts_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_COST_COMP_UPD_STG'
DECLARE
  l_cost_exists number := 0;
BEGIN
  SELECT count(*) INTO l_cost_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_COST_COMP_UPD_STG';

  if (l_cost_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_COST_COMP_UPD_STG';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_ELC_COMP'
DECLARE
  l_elc_exists number := 0;
BEGIN
  SELECT count(*) INTO l_elc_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_ELC_COMP';

  if (l_elc_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_ELC_COMP';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_CHAPTER'
DECLARE
  l_chapter_exists number := 0;
BEGIN
  SELECT count(*) INTO l_chapter_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_CHAPTER';

  if (l_chapter_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_CHAPTER';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_HTS_CHAPTER_RESTRAINTS'
DECLARE
  l_restraints_exists number := 0;
BEGIN
  SELECT count(*) INTO l_restraints_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_HTS_CHAPTER_RESTRAINTS';

  if (l_restraints_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_HTS_CHAPTER_RESTRAINTS';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_QUOTA_CATEGORY' 
DECLARE
  l_quota_exists number := 0;
BEGIN
  SELECT count(*) INTO l_quota_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_QUOTA_CATEGORY';

  if (l_quota_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_QUOTA_CATEGORY';
end if;
end;
/

PROMPT Dropping Package 'CORESVC_COUNTRY_TARIFF_TR'
DECLARE
  l_country_exists number := 0;
BEGIN
  SELECT count(*) INTO l_country_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CORESVC_COUNTRY_TARIFF_TR';

  if (l_country_exists != 0) then
      execute immediate 'DROP PACKAGE CORESVC_COUNTRY_TARIFF_TR';
end if;
end;
/
