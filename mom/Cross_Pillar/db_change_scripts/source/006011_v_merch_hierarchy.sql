--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	VIEW UPDATED:				V_MERCH_HIERARCHY
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_MERCH_HIERARCHY'
CREATE OR REPLACE FORCE VIEW V_MERCH_HIERARCHY
    (DIVISION 
    ,DIV_NAME
    ,GROUP_NO
    ,GROUP_NAME
    ,DEPT
    ,DEPT_NAME
    ,CLASS
    ,CLASS_NAME
    ,SUBCLASS
    ,SUB_NAME)
    AS SELECT    D.DIVISION
                ,D.DIV_NAME
                ,G.GROUP_NO
                ,G.GROUP_NAME
                ,DE.DEPT
                ,DE.DEPT_NAME
                ,C.CLASS
                ,C.CLASS_NAME
                ,SC.SUBCLASS
                ,SC.SUB_NAME
    FROM    V_DIVISION D
            ,V_GROUPS G
            ,V_DEPS DE
            ,V_CLASS C
            ,V_SUBCLASS SC
  WHERE D.DIVISION = G.DIVISION
  AND   G.GROUP_NO   = DE.GROUP_NO
  AND   DE.DEPT      = C.DEPT   
  AND   C.DEPT      = SC.DEPT     
  AND   C.CLASS      = SC.CLASS
/

  
COMMENT ON TABLE V_MERCH_HIERARCHY IS 'This view will return merchidise hierarchies and the translated hierachy names. Security policy is also applied to filter user access.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.DIVISION IS 'Contains the number which uniquely identifies the division.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.DIV_NAME IS 'Contains the translated name of the division.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.GROUP_NO IS 'Contains the number which uniquely identifies the group.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.GROUP_NAME IS 'Contains the translated name of the group.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.DEPT IS 'Contains the number which uniquely identifies the department.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.DEPT_NAME IS 'Contains the translated name of the department.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.CLASS IS 'Contains the number which uniquely identifies the class within a department.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.CLASS_NAME IS 'Contains the translated name of the class.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.SUBCLASS IS 'Contains the number which uniquely identifies the subclass within a department and class.'
/
COMMENT ON COLUMN V_MERCH_HIERARCHY.SUB_NAME IS 'Contains the translated name of the subclass.'
/