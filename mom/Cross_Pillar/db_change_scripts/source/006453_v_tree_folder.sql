--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Dropping VIEW: 		 V_TREE_FOLDER
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Dropping VIEW               
--------------------------------------
DECLARE
  L_view_exists number := 0;
BEGIN
  SELECT count(*) INTO L_view_exists
    FROM USER_VIEWS
   WHERE VIEW_NAME = 'V_TREE_FOLDER';

  if (L_view_exists != 0) then
      execute immediate 'DROP VIEW V_TREE_FOLDER';
  end if;
end;
/
