--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_DEPT_OPTIONS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_UNEXP_INV_TOLERANCE_QTY NUMBER (12,4) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_TOLERANCE_QTY is 'Defines a tolerance level of unexpected inventory for an item location to be shown in the Inventory Control Unexpected Inventory Report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_NEG_INV_TOLERANCE_QTY NUMBER (12,4) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_NEG_INV_TOLERANCE_QTY is 'Defines a tolerance level of negative inventory for an item location to be shown in the Inventory Control Negative Inventory Report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_UNEXP_INV_INACTIVE_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_INACTIVE_IND is 'Controls if inactive item/locs are included in the inventory control unexpected inventory report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_UNEXP_INV_DISCONTINUE_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_DISCONTINUE_IND is 'Controls if discontinued item/locs are included in the inventory control unexpected inventory report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_UNEXP_INV_DELETE_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_DELETE_IND is 'Controls if deleted item/locs are included in the inventory control unexpected inventory report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS DROP COLUMN IC_TOLERANCE_QTY
/

