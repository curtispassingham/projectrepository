--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD TOTAL_RETAIL NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.TOTAL_RETAIL is 'Holds the total retail of the order.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD RECEIVED_UNITS NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.RECEIVED_UNITS is 'Contains the total received number of items ordered'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD OUTSTANDING_UNITS NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.OUTSTANDING_UNITS is 'Contains the total outstanding number of items ordered'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD MULTIPLE_UOM_IND VARCHAR2 (1 ) DEFAULT 'N' NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.MULTIPLE_UOM_IND is 'Indicates if the order has more than one unit of measure.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD OUTSTANDING_COST NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.OUTSTANDING_COST is 'Holds the cost of the order outstanding units.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD OUTSTANDING_RETAIL NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.OUTSTANDING_RETAIL is 'Holds the retail of the order outstanding units.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD DEPT NUMBER (4,0) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.DEPT is 'Contains the department number for orders limited to a single department and will be Null for orders involving items in more than one department.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD DEPT_NAME VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.DEPT_NAME is 'Department name'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD PO_TYPE VARCHAR2 (4 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.PO_TYPE is 'Contains the value associated with the PO_TYPE for the order.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD PO_TYPE_DESC VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.PO_TYPE_DESC is 'This field contains a description for the specific order type.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD COMMENT_DESC VARCHAR2 (2000 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.COMMENT_DESC is 'Holds user comment.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD PO_LAST_UPDATE_DATETIME DATE  NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.PO_LAST_UPDATE_DATETIME is 'Holds the date time stamp of the most recent update by the last_update_id.'
/

ALTER TABLE RMS_OI_INV_ORD_ERRORS ADD PO_LAST_UPDATE_ID VARCHAR2 (30 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ERRORS.PO_LAST_UPDATE_ID is 'Holds the Oracle user-id of the user who most recently updated this record.'
/

