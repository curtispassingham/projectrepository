--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      PACKAGES to be DROPPED:    
                          GUI_TRANSLATION_WRP
*/
----------------------------------------------------------------------------\

whenever sqlerror exit

--------------------------------------
--       Dropping Package
-------------------------------------- 
PROMPT DROPPING Package 'GUI_TRANSLATION_WRP'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'GUI_TRANSLATION_WRP';

  if (L_pkg_exists != 0) then
      execute immediate 'drop package GUI_TRANSLATION_WRP';
  end if;
end;
/