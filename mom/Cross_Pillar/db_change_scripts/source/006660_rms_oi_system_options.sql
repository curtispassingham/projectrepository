--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_WAC_VAR_MAXIMUM_PCT NUMBER (12,4) DEFAULT 15 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_WAC_VAR_MAXIMUM_PCT is 'Defines the maximul level which exceeds the FA_WAC_VAR_TOLERANCE_PCT the  item/locations combinations will be considered critical in the financial analyst WAC variance report.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD FA_SHRINKAGE_VAR_MAX_PCT NUMBER (12,4) DEFAULT 15 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_SHRINKAGE_VAR_MAX_PCT is 'Defines the maximum variance which exceeds FA_SHRINKAGE_VAR_TOLERANCE_PCT, that subclass/location will be considered critical in the financial analyst Shrinkage variance report'
/

