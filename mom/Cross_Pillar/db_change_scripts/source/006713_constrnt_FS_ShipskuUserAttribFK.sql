--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SHIPSKU'

PROMPT Dropping Foreign Key on 'SHIPSKU'
DECLARE
  L_constraints_shipsku_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_shipsku_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'SHS_UAT_FK'
     AND CONSTRAINT_TYPE = 'R';

  if (L_constraints_shipsku_exists != 0) then
      execute immediate 'ALTER TABLE SHIPSKU DROP CONSTRAINT SHS_UAT_FK';
  end if;
end;
/
