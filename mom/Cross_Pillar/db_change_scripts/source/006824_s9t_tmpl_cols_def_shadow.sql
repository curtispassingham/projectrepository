--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING Table S9T_TMPL_COLS_DEF_SHADOW
-------------------------------------------
PROMPT COPYING THE DATA FROM S9T_TMPL_COLS_DEF_SHADOW to S9T_TMPL_COLS_DEF_TL
INSERT INTO S9T_TMPL_COLS_DEF_TL (TEMPLATE_KEY,
                                        WKSHT_KEY,
                                        COLUMN_KEY,
                                        LANG,
                                        COLUMN_NAME
                                       )
                                 SELECT s9s.TEMPLATE_KEY,
                                        s9s.WKSHT_KEY,
                                        s9s.COLUMN_KEY,
                                        s9s.LANG,
                                        s9s.COLUMN_NAME
                                   FROM S9T_TMPL_COLS_DEF_SHADOW s9s
                                  WHERE NOT EXISTS(SELECT '1' 
                                                     FROM S9T_TMPL_COLS_DEF_TL s9t
                                                    WHERE s9t.TEMPLATE_KEY = s9s.TEMPLATE_KEY
                                                      AND s9t.WKSHT_KEY = s9s.WKSHT_KEY
                                                      AND s9t.COLUMN_KEY = s9s.COLUMN_KEY
                                                      AND s9t.LANG = s9s.LANG
                                            );

PROMPT DROPPING table S9T_TMPL_COLS_DEF_SHADOW
DECLARE
  L_group_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_group_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'S9T_TMPL_COLS_DEF_SHADOW';

  if (L_group_table_exists != 0) then
      execute immediate 'DROP TABLE S9T_TMPL_COLS_DEF_SHADOW CASCADE CONSTRAINTS';
  end if;
end;
/


