--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Tables 
-------------------------------------- 
PROMPT DROPPING Table 'AUDIT_FLD'
DECLARE
  L_table_audit_fld_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_audit_fld_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'AUDIT_FLD';

  if (L_table_audit_fld_exists != 0) then
      execute immediate 'drop table AUDIT_FLD CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'AUDIT_TBL'
DECLARE
  L_table_audit_tbl_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_audit_tbl_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'AUDIT_TBL';

  if (L_table_audit_tbl_exists != 0) then
      execute immediate 'drop table AUDIT_TBL CASCADE CONSTRAINTS';
  end if;
end;
/
