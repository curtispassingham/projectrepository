--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      TABLES to be DROPPED:    
                          STOP_SHIP 
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TABLES
--------------------------------------
PROMPT DROPPING Table 'STOP_SHIP'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STOP_SHIP';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE STOP_SHIP CASCADE CONSTRAINTS';
  end if;
end;
/