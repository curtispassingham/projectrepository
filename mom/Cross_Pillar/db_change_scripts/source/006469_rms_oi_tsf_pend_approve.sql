--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_TSF_PEND_APPROVE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_TSF_PEND_APPROVE'
CREATE TABLE RMS_OI_TSF_PEND_APPROVE
 (SESSION_ID NUMBER(15,0),
  TSF_NO NUMBER(12,0),
  TSF_TYPE VARCHAR2(6 ),
  FROM_LOC NUMBER(10,0),
  FROM_LOC_NAME VARCHAR2(150 ),
  FROM_LOC_TYPE VARCHAR2(1 ),
  TO_LOC NUMBER(10,0),
  TO_LOC_NAME VARCHAR2(150 ),
  TO_LOC_TYPE VARCHAR2(1 ),
  DELIVERY_DATE DATE,
  CREATE_DATE DATE,
  TSF_COST NUMBER(20,4),
  TSF_RETAIL NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 ),
  FINISHER NUMBER(10,0),
  FINISHER_NAME VARCHAR2(150 ),
  INTERCOMPANY_IND VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_TSF_PEND_APPROVE is 'This table holds the information for the Submitted Transfers Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TSF_NO is 'The transfer.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TSF_TYPE is 'The transfer type.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.FROM_LOC is 'The transfer from location.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.FROM_LOC_NAME is 'The transfer from location name.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.FROM_LOC_TYPE is 'The transfer from location type.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TO_LOC is 'The transfer to location.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TO_LOC_NAME is 'The transfer to location name.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TO_LOC_TYPE is 'The transfer to location type.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.DELIVERY_DATE is 'The transfer delivery date.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.CREATE_DATE is 'The transfer create date.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TSF_COST is 'The transfer cost.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.TSF_RETAIL is 'The transfer retail.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.CURRENCY_CODE is 'The currency code.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.FINISHER is 'The finisher.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.FINISHER_NAME is 'The finisher name.'
/

COMMENT ON COLUMN RMS_OI_TSF_PEND_APPROVE.INTERCOMPANY_IND is 'Indications if the transfer is inter-company.'
/


PROMPT Creating Index on 'RMS_OI_TSF_PEND_APPROVE'
 CREATE INDEX RMS_OI_TSF_PEND_APPROVE_I1 on RMS_OI_TSF_PEND_APPROVE
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

