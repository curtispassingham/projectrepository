--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_STORE_GRADE_OR_AREA_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_STORE_GRADE_OR_AREA_IND is 'Indicator to show store grade group or Area in inventory analyst dashboard prompt.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_ITEM_PARENT_FILTER VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_ITEM_PARENT_FILTER is 'Indicator to display all Item filters (Item/Item Parent/Parent Diff) in inventory analyst dashboard prompt.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_VARIANCE_RANGE_PCT_1 NUMBER (12,4) DEFAULT -10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_VARIANCE_RANGE_PCT_1 is 'Configuring first % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_VARIANCE_RANGE_PCT_2 NUMBER (12,4) DEFAULT -5 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_VARIANCE_RANGE_PCT_2 is 'Configuring second % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_VARIANCE_RANGE_PCT_3 NUMBER (12,4) DEFAULT 5 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_VARIANCE_RANGE_PCT_3 is 'Configuring third % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD IA_VARIANCE_RANGE_PCT_4 NUMBER (12,4) DEFAULT 10 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_VARIANCE_RANGE_PCT_4 is 'Configuring fourth % value for Inventory Variance to Forecast tile.'
/

