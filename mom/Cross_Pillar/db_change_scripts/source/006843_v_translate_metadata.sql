--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW CREATE:           V_TRANSLATE_METADATA
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_TRANSLATE_METADATA'
CREATE OR REPLACE FORCE VIEW V_TRANSLATE_METADATA (BASE_TABLE, BASE_COLUMN, TRANSLATE_TABLE, TRANSLATE_COLUMN, VARIABLE_NAME, KEY, COLUMN_SEQUENCE, NULLABLE, DATA_TYPE, DATA_LENGTH, LABEL, TABLE_LABEL, LANG) AS
   SELECT  b.base_table,
           b.base_column,
           b.translate_table,
           b.translate_column,
           b.variable_name,
           b.key,
           b.column_sequence,
           b.nullable,
           b.data_type,
           b.data_length,
           NVL(v2.code_desc,INITCAP(REPLACE(b.base_column,'_',' '))) label,
           NVL(v1.code_desc,INITCAP(REPLACE(b.base_table,'_',' '))) table_label,
           NVL(v1.lang,v2.lang) lang
      FROM translate_metadata b,
           translate_metadata_table t,
           v_code_detail_tl v1,
           v_code_detail_tl v2
     WHERE b.base_table      = t.base_table (+)
       AND t.label           = v1.code (+)
       AND v1.code_type(+)   = 'TLLT'
       AND b.label           = v2.code (+)
       AND v2.code_type (+)  = 'TLLC'
/

COMMENT ON TABLE V_TRANSLATE_METADATA is 'This view on top of translate_metadata and translate_metadata_table handles translation for the label column and table.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.BASE_TABLE is 'Refer to translate_metadata.base_table.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.BASE_COLUMN is 'Refer to translate_metadata.base_column.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.translate_table is 'Refer to translate_metadata.translate_table.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.translate_column is 'Refer to translate_metadata.translate_column.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.variable_name is 'Refer to translate_metadata.variable_name.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.key is 'Refer to translate_metadata.key.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.column_sequence is 'Refer to translate_metadata.column_sequence.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.nullable is 'Refer to translate_metadata.nullable.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.data_type is 'Refer to translate_metadata.data_type.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.data_length is 'Refer to translate_metadata.data_length.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.label is 'The column label is in user language if entry exists in code_detail_tl for user language. Else, it is in primary language.'
/

COMMENT ON COLUMN V_TRANSLATE_METADATA.table_label is 'The table label is in user language if entry exists in code_detail_tl for user language. Else, it is in primary language.'
/
