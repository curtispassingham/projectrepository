--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop Table               
--------------------------------------
PROMPT dropping Table 'NAV_FOLDER_BASE'
DECLARE
  L_table_exists1 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_FOLDER_BASE'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists1 != 0) then
      execute immediate 'DROP TABLE NAV_FOLDER_BASE CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_ICON'
DECLARE
  L_table_exists2 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists2
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_ICON'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists2 != 0) then
      execute immediate 'DROP TABLE NAV_ICON CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'FORM_ELEMENTS'
DECLARE
  L_table_exists3 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists3
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FORM_ELEMENTS'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists3 != 0) then
      execute immediate 'DROP TABLE FORM_ELEMENTS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_ELEMENT'
DECLARE
  L_table_exists4 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists4
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_ELEMENT'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists4 != 0) then
      execute immediate 'DROP TABLE NAV_ELEMENT CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_COMPONENT'
DECLARE
  L_table_exists5 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists5
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_COMPONENT'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists5 != 0) then
      execute immediate 'DROP TABLE NAV_COMPONENT CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'NAV_SERVER'
DECLARE
  L_table_exists6 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists6
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NAV_SERVER'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists6 != 0) then
      execute immediate 'DROP TABLE NAV_SERVER CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'MULTIVIEW_DEFAULT_45'
DECLARE
  L_table_exists7 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists7
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'MULTIVIEW_DEFAULT_45'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists7 != 0) then
      execute immediate 'DROP TABLE MULTIVIEW_DEFAULT_45 CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'MULTIVIEW_SAVED_45'
DECLARE
  L_table_exists8 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists8
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'MULTIVIEW_SAVED_45'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists8 != 0) then
      execute immediate 'DROP TABLE MULTIVIEW_SAVED_45 CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'MENU_ELEMENTS'
DECLARE
  L_table_exists9 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists9
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'MENU_ELEMENTS'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists9 != 0) then
      execute immediate 'DROP TABLE MENU_ELEMENTS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'MENU_ELEMENTS_LANGS_TEMP'
DECLARE
  L_table_exists10 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists10
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'MENU_ELEMENTS_LANGS_TEMP'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists10 != 0) then
      execute immediate 'DROP TABLE MENU_ELEMENTS_LANGS_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'MENU_ELEMENTS_LANGS'
DECLARE
  L_table_exists11 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists11
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'MENU_ELEMENTS_LANGS'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists11 != 0) then
      execute immediate 'DROP TABLE MENU_ELEMENTS_LANGS CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'MENU_ELEMENTS_TEMP'
DECLARE
  L_table_exists12 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists12
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'MENU_ELEMENTS_TEMP'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists12 != 0) then
      execute immediate 'DROP TABLE MENU_ELEMENTS_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'WIZARD_TEXT'
DECLARE
  L_table_exists13 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists13
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WIZARD_TEXT'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists13 != 0) then
      execute immediate 'DROP TABLE WIZARD_TEXT CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT dropping Table 'RTK_REPORTS'
DECLARE
  L_table_exists14 number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists14
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'RTK_REPORTS'
   AND OBJECT_TYPE ='TABLE';

if (L_table_exists14 != 0) then
      execute immediate 'DROP TABLE RTK_REPORTS CASCADE CONSTRAINTS';
  end if;
end;
/
