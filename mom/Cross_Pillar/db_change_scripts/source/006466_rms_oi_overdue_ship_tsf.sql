--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_OVERDUE_SHIP_TSF
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_OVERDUE_SHIP_TSF'
CREATE TABLE RMS_OI_OVERDUE_SHIP_TSF
 (SESSION_ID NUMBER(15,0),
  TSF_NO NUMBER(12,0),
  TSF_TYPE VARCHAR2(6 ),
  FROM_LOC NUMBER(10,0),
  FROM_LOC_NAME VARCHAR2(150 ),
  FROM_LOC_TYPE VARCHAR2(1 ),
  TO_LOC NUMBER(10,0),
  TO_LOC_NAME VARCHAR2(150 ),
  TO_LOC_TYPE VARCHAR2(1 ),
  APPROVAL_DATE DATE,
  NOT_AFTER_DATE DATE,
  DELIVERY_DATE DATE,
  TSF_COST NUMBER(20,4),
  TSF_RETAIL NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 ),
  FINISHER NUMBER(10,0),
  FINISHER_NAME VARCHAR2(150 ),
  INTERCOMPANY_IND VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_OVERDUE_SHIP_TSF is 'This table holds the information for the Transfers Pending Close Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TSF_NO is 'The transfer.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TSF_TYPE is 'The transfer type.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.FROM_LOC is 'The transfer from location.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.FROM_LOC_NAME is 'The transfer from location name.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.FROM_LOC_TYPE is 'The transfer from location type.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TO_LOC is 'The transfer to location.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TO_LOC_NAME is 'The transfer to location name.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TO_LOC_TYPE is 'The transfer to location type.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.APPROVAL_DATE is 'The tranfer approval date.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.NOT_AFTER_DATE is 'The tranfer not after date.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.DELIVERY_DATE is 'The transfer delivery date.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TSF_COST is 'The transfer cost.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.TSF_RETAIL is 'The transfer retail.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.CURRENCY_CODE is 'The currency code.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.FINISHER is 'The finisher.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.FINISHER_NAME is 'The finisher name.'
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_TSF.INTERCOMPANY_IND is 'Indications if the transfer is inter-company.'
/


PROMPT Creating Index on 'RMS_OI_OVERDUE_SHIP_TSF'
 CREATE INDEX RMS_OI_OVERDUE_SHIP_TSF_I1 on RMS_OI_OVERDUE_SHIP_TSF
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

