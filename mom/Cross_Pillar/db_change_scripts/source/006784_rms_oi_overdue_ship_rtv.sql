--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_OVERDUE_SHIP_RTV'
ALTER TABLE RMS_OI_OVERDUE_SHIP_RTV DROP COLUMN RTV_COST
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_RTV DROP COLUMN RTV_RETAIL
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_RTV DROP COLUMN CURRENCY_CODE
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_RTV ADD RTV_QTY NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.RTV_QTY is 'The total quantity for the RTV.'
/

ALTER TABLE RMS_OI_OVERDUE_SHIP_RTV ADD MULTI_UOM_IND VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_OVERDUE_SHIP_RTV.MULTI_UOM_IND is 'Indicates if the items on the RTV have more than 1 standard UOM between them.  Possible values are Y or N.'
/

