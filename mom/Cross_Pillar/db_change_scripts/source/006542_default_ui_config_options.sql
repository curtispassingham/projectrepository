--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'DEFAULT_UI_CONFIG_OPTIONS'
ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN LEVEL_1_NAME
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN LEVEL_2_NAME
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN LEVEL_3_NAME
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN NOM_FLAG_1_LABEL
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN NOM_FLAG_2_LABEL
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN NOM_FLAG_3_LABEL
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN NOM_FLAG_4_LABEL
/

ALTER TABLE DEFAULT_UI_CONFIG_OPTIONS DROP COLUMN NOM_FLAG_5_LABEL
/

