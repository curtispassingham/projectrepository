--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SEC_USER_ROLE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SEC_USER_ROLE'
CREATE TABLE SEC_USER_ROLE
 (ROLE VARCHAR2(30 ) NOT NULL,
  USER_SEQ NUMBER(15) NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SEC_USER_ROLE is 'This table holds the roles a security user belongs to.'
/

COMMENT ON COLUMN SEC_USER_ROLE.ROLE is 'This field contains the role the user belongs to.'
/

COMMENT ON COLUMN SEC_USER_ROLE.USER_SEQ is 'This filed holds the user assigned to the role. It references the user sequence defined on the SEC_USER table.'
/

COMMENT ON COLUMN SEC_USER_ROLE.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SEC_USER_ROLE.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/


PROMPT Creating Primary Key on 'SEC_USER_ROLE'
ALTER TABLE SEC_USER_ROLE
 ADD CONSTRAINT PK_SEC_USER_ROLE PRIMARY KEY
  (ROLE,
   USER_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'SEC_USER_ROLE'
 ALTER TABLE SEC_USER_ROLE
  ADD CONSTRAINT SUR_SEU_FK
  FOREIGN KEY (USER_SEQ)
 REFERENCES SEC_USER (USER_SEQ)
/


PROMPT Creating FK on 'SEC_USER_ROLE'
 ALTER TABLE SEC_USER_ROLE
  ADD CONSTRAINT SUR_RRP_FK
  FOREIGN KEY (ROLE)
 REFERENCES RTK_ROLE_PRIVS (ROLE)
/

