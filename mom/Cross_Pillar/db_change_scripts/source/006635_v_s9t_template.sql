--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Created:						 V_S9T_TEMPLATE
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_S9T_TEMPLATE';
CREATE OR REPLACE FORCE  VIEW V_S9T_TEMPLATE 
("TEMPLATE_KEY", 
 "TEMPLATE_NAME", 
 "TEMPLATE_DESC", 
 "TEMPLATE_TYPE", 
 "TEMPLATE_CATEGORY") 
AS 
SELECT t.template_key template_key,
       NVL(s.template_name, t.template_name) AS template_name,
       NVL(s.template_desc, t.template_desc) AS template_desc,
       t.template_type template_type,
       t.template_category template_category
  FROM s9t_template t,
       s9t_template_tl s
  WHERE t.template_key = s.template_key(+)
    AND s.lang (+) = get_user_lang;

COMMENT ON TABLE V_S9T_TEMPLATE IS 'This view holds the s9t template name and description in user language. If not defined for the user language, it returns a row with name and description in the primary language.'
/

COMMENT ON COLUMN V_S9T_TEMPLATE.TEMPLATE_KEY IS 'Contains the template key.'
/

COMMENT ON COLUMN V_S9T_TEMPLATE.TEMPLATE_NAME IS 'Contains the template name in user language, or in primary language if not defined for the user language.'
/

COMMENT ON COLUMN V_S9T_TEMPLATE.TEMPLATE_DESC IS 'Contains the template description in user language, or in primary language if not defined for the user language.'
/

COMMENT ON COLUMN V_S9T_TEMPLATE.TEMPLATE_TYPE IS 'Contains the template type.'
/

COMMENT ON COLUMN V_S9T_TEMPLATE.TEMPLATE_CATEGORY IS 'Contains the template category.'
/

