--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_WAC_VARIANCE'
ALTER TABLE RMS_OI_WAC_VARIANCE ADD DEPT NUMBER (4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.DEPT is 'Number identifying the department to which the item is attached.  The items department will be the same as that of its parent (and, by transitivity, to that of its grandparent).  Valid values for this field are located on the deps table.'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD CLASS NUMBER (4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.CLASS is 'Number identifying the class to which the item is attached.  The items class will be the same as that of its parent (and, by transitivity, to that of its grandparent).  Valid values for this field are located on the class table.'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD SUBCLASS NUMBER (4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.SUBCLASS is 'Number identifying the subclass to which the item is attached.  The items subclass will be the same as that of its parent (and, by transitivity, to that of its grandparent).  Valid values for this field are located on the subclass table.'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD SUBCLASS_NAME VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.SUBCLASS_NAME is 'Subclass name'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD SUPPLIER NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.SUPPLIER is 'Primary supplier ID'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD SUPPLIER_NAME VARCHAR2 (240 ) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.SUPPLIER_NAME is 'Primary supplier name'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD UNIT_RETAIL NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.UNIT_RETAIL is 'Contains the unit retail price in the standard unit of measure for the item/location combination. This field is stored in the local currency.'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD CHAIN_AVERGE NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.CHAIN_AVERGE is 'Chain WAC'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD WH NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.WH is 'Source or defulat Warehouse ID'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD WH_NAME VARCHAR2 (150 ) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.WH_NAME is 'Warehouse name'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD WH_AVERAGE_COST NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.WH_AVERAGE_COST is 'Warehouse WAC'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD UNIT_ELC_PRIMARY NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.UNIT_ELC_PRIMARY is 'ELC in retailer primary currancy'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD AVERAGE_COST_PRIMARY NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.AVERAGE_COST_PRIMARY is 'WAC in retailer primary currancy'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD INVENTORY_PRIMARY NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.INVENTORY_PRIMARY is 'Inventory value in retailer primary currancy'
/

ALTER TABLE RMS_OI_WAC_VARIANCE ADD ITEM_IMAGE VARCHAR2 (255 ) NULL
/

