--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ATTRIB_GROUP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ATTRIB_GROUP'
CREATE TABLE SVC_ATTRIB_GROUP
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ) NOT NULL,
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N' NOT NULL,
  GROUP_ID NUMBER(10) NOT NULL,
  GROUP_SET_ID NUMBER(10) NOT NULL,
  GROUP_VIEW_NAME VARCHAR2(30 ) NOT NULL,
  DISPLAY_SEQ NUMBER(2) NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  LAST_UPD_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPD_DATETIME DATE NOT NULL
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

COMMENT ON TABLE SVC_ATTRIB_GROUP is 'This is the staging table for CFAS attribute group information.It is used to temporarily hold data before it is uploaded/updated in CFA_ATTRIB_GROUP.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.CHUNK_ID is 'Uniquely identifies a chunk.The value will always be 1.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.ROW_SEQ is 'The rows sequence. Should be unique within a process-ID.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.GROUP_ID is 'This column holds a generated ID that distinguishes the custom attribute group.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.GROUP_SET_ID is 'This column holds id of the set where the group belongs to.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.GROUP_VIEW_NAME is 'This column holds the name of the database view that will be generated to make access to user entered data easier.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.DISPLAY_SEQ is 'This column holds the order the attribute groups will be displayed in on the CFAS UI when multiple groups exist for a single attribute group set.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_ATTRIB_GROUP'
ALTER TABLE SVC_ATTRIB_GROUP
 ADD CONSTRAINT SVC_ATTRIB_GROUP_PK PRIMARY KEY
  (PROCESS_ID,ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_ATTRIB_GROUP'
ALTER TABLE SVC_ATTRIB_GROUP
 ADD CONSTRAINT SVC_ATTRIB_GROUP_UK UNIQUE
  (GROUP_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

