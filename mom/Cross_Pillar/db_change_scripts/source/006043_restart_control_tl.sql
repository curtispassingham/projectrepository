--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       RESTART_CONTROL_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE RESTART_CONTROL_TL(
LANG NUMBER(6) NOT NULL,
PROGRAM_NAME VARCHAR2(25) NOT NULL,
PROGRAM_DESC VARCHAR2(120) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RESTART_CONTROL_TL is 'This is the translation table for RESTART_CONTROL table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN RESTART_CONTROL_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN RESTART_CONTROL_TL.PROGRAM_NAME is 'This field contains the batch program name.'
/

COMMENT ON COLUMN RESTART_CONTROL_TL.PROGRAM_DESC is 'This field contains a description of the batch program.'
/

COMMENT ON COLUMN RESTART_CONTROL_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN RESTART_CONTROL_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN RESTART_CONTROL_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN RESTART_CONTROL_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE RESTART_CONTROL_TL ADD CONSTRAINT PK_RESTART_CONTROL_TL PRIMARY KEY (
LANG,
PROGRAM_NAME
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE RESTART_CONTROL_TL
 ADD CONSTRAINT RCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE RESTART_CONTROL_TL ADD CONSTRAINT RCT_RC_FK FOREIGN KEY (
PROGRAM_NAME
) REFERENCES RESTART_CONTROL (
PROGRAM_NAME
)
/

