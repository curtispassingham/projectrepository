--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_LEAD_TIME_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_LEAD_TIME_GTT'
CREATE GLOBAL TEMPORARY TABLE RMS_OI_LEAD_TIME_GTT
 (ITEM VARCHAR2(25 ),
  LOC NUMBER(10,0),
  PLAN_SUP_LEAD_TIME NUMBER(10,0),
  PLAN_WH_LEAD_TIME NUMBER(10,0),
  PLAN_REVIEW_LEAD_TIME NUMBER(10,0),
  ACTUAL_SUP_LEAD_TIME NUMBER(10,0),
  ACTUAL_WH_LEAD_TIME NUMBER(10,0),
  ACTUAL_REVIEW_LEAD_TIME NUMBER(10,0)
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE RMS_OI_LEAD_TIME_GTT is 'Helper table for Inventory Analyst Dashboard.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ITEM is 'The item of the row.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.LOC is 'The location of the row.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.PLAN_SUP_LEAD_TIME is 'The planned suppleir lead time.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.PLAN_WH_LEAD_TIME is 'The planned wh lead time.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.PLAN_REVIEW_LEAD_TIME is 'The planned review lead time.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ACTUAL_SUP_LEAD_TIME is 'The actual suppleir lead time.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ACTUAL_WH_LEAD_TIME is 'The actual wh lead time.'
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ACTUAL_REVIEW_LEAD_TIME is 'The actual review lead time.'
/

