--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD MI_MONTH_RANGE NUMBER (10) DEFAULT 2 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.MI_MONTH_RANGE is 'Defines a Month Range for Margin Impact Contextual BI Report'
/

