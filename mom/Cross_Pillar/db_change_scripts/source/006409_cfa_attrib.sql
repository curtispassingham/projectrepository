--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Modifying Table 'CFA_ATTRIB'

PROMPT Dropping Foreign Key on 'CFA_ATTRIB'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CFA_CHD_FK'
     AND constraint_TYPE = 'R';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE CFA_ATTRIB DROP CONSTRAINT CFA_CHD_FK';
  end if;
end;
/

PROMPT Transferring data from 'CFA_CODE_HEAD' to 'CODE_HEAD' 
DECLARE
L_SeqNo CODE_DETAIL.CODE_SEQ%TYPE;
L_CODE_HEAD_TYPE varchar(1);
L_CODE_DETAIL_TYPE CODE_DETAIL.CODE_TYPE%TYPE;
L_SEQ_NO CODE_DETAIL.CODE_SEQ%TYPE;
L_PRIMARY_LANG SYSTEM_OPTIONS.DATA_INTEGRATION_LANG%TYPE;
L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
CURSOR C_CFA_CODEHEAD IS
SELECT cfacodehead.Code_type,cfacodehead.Code_type_desc
from CFA_CODE_HEAD cfacodehead;

CURSOR C_CFA_CODEDETAIL IS
SELECT cfacodedetail.Code,cfacodedetail.Code_Type,cfacodedetail.Code_Desc,cfacodedetail.LANG 
from CFA_CODE_DETAIL_DESCS cfacodedetail;


CURSOR C_CODE_DETAIL_SEQ_NO(L_code_type code_detail.code_type%type,L_code code_detail.code%type) IS
SELECT NVL(MAX(CODE_SEQ)+1,1) FROM CODE_DETAIL
WHERE CODE_TYPE=L_code_type;

CURSOR SYS_OPT_CURSOR IS 
SELECT DATA_INTEGRATION_LANG FROM SYSTEM_OPTIONS;

BEGIN

OPEN SYS_OPT_CURSOR;

FETCH SYS_OPT_CURSOR INTO L_PRIMARY_LANG;
CLOSE SYS_OPT_CURSOR;

FOR rec in C_CFA_CODEHEAD LOOP
INSERT INTO Code_Head(CODE_TYPE,CODE_TYPE_DESC) values (rec.Code_Type,rec.Code_Type_Desc);  
END LOOP;
IF C_CFA_CODEHEAD%ISOPEN THEN
CLOSE C_CFA_CODEHEAD;
END IF;

FOR rec in C_CFA_CODEDETAIL LOOP
    
    open C_CODE_DETAIL_SEQ_NO(rec.code_type,rec.code);
    fetch C_CODE_DETAIL_SEQ_NO into L_SEQ_NO;
    close C_CODE_DETAIL_SEQ_NO;
	
  
    IF L_PRIMARY_LANG = rec.LANG THEN 
      INSERT into Code_Detail(CODE_TYPE,CODE,CODE_DESC,REQUIRED_IND,CODE_SEQ) values (rec.Code_Type,rec.Code,rec.Code_Desc,'N',L_SEQ_NO);
    ELSE        
      INSERT into Code_Detail_TL(LANG,CODE_TYPE,CODE,CODE_DESC,CREATE_DATETIME,CREATE_ID,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) VALUES (rec.LANG,rec.Code_Type,rec.Code,rec.Code_Desc,SYSDATE(),USER,SYSDATE(),USER);
    END IF; 
  L_CODE_HEAD_TYPE := NULL;
END LOOP;
IF C_CFA_CODEHEAD%ISOPEN THEN
CLOSE C_CFA_CODEHEAD;
END IF;

IF C_CFA_CODEDETAIL%ISOPEN THEN
CLOSE C_CFA_CODEDETAIL;
END IF;
COMMIT;

EXCEPTION
  
	when OTHERS then      
  
	IF C_CODE_DETAIL_SEQ_NO%ISOPEN THEN
		CLOSE C_CODE_DETAIL_SEQ_NO;
	END IF;
	IF C_CFA_CODEDETAIL%ISOPEN THEN
		CLOSE C_CFA_CODEDETAIL;
	END IF;
	IF C_CFA_CODEHEAD%ISOPEN THEN
	CLOSE C_CFA_CODEHEAD;
	END IF;
	
	dbms_output.put_line ('*** SQL Error *** '||SQLERRM);
	return;

END;
/

PROMPT Creating Foreign Key on 'CFA_ATTRIB'
ALTER TABLE CFA_ATTRIB ADD CONSTRAINT CFA_CHD_FK FOREIGN KEY ( CODE_TYPE )
REFERENCES CODE_HEAD ( CODE_TYPE ) NOT DEFERRABLE 
/