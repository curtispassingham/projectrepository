--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-----------------------------------------------
--       Dropping Table  POS_SUP_PAY_CRITERIA   
----------------------------------------------- 
PROMPT DROPPING Table 'POS_SUP_PAY_CRITERIA'
DECLARE
  L_pos_sup_pay_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pos_sup_pay_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'POS_SUP_PAY_CRITERIA';

  if (L_pos_sup_pay_exists != 0) then
      execute immediate 'drop table POS_SUP_PAY_CRITERIA';
  end if;
end;
/
