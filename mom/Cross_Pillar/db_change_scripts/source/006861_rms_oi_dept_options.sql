--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_DEPT_OPTIONS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_DAYS_AFTER_ITEM_CREATE NUMBER (3) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_DAYS_AFTER_ITEM_CREATE is 'Defines the number of days after item creation after which the Item will appear in INCOMPLETE ITEMS report.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_REF_ITEM VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_REF_ITEM is 'Configured to show Reference Items in Incomplete Items Report. Valid values are Y - to show Reference Items, N - do not show Reference Items and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_VAT VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_VAT is 'Configured to show VAT in Incomplete Items Report. Valid values are Y - to show VAT, N - do not show VAT and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table. '
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_SPACK VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_SPACK is 'Configured to show Simple Pack in Incomplete Items Report. Valid values are Y - to show Simple Pack, N - do not show Simple pack and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_UDA VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_UDA is 'Configured to show UDA in Incomplete Items Report. Valid values are Y - to show UDA, N - do not show UDA and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_LOC VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_LOC is 'Configured to show Locations in Incomplete Items Report. Valid values are Y - to show Locations, N - do not show Locations and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_SEASONS VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_SEASONS is 'Configured to show Seasons/Phases in Incomplete Items Report. Valid values are Y - to show Seasons/Phases, N - do not show Seasons/Phases and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_REPL VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_REPL is 'Configured to show Replenishment in Incomplete Items Report. Valid values are Y - to show Replenishment, N - do not show Replenishment and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_SUBS_ITEM VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_SUBS_ITEM is 'Configured to show Substitute Items in Incomplete Items Report. Valid values are Y - to show Substitute Items, N - do not show Substitute Items and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_DIMEN VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_DIMEN is 'Configured to show Dimensions in Incomplete Items Report. Valid values are Y - to show Dimensions, N - do not show Dimensions and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_REL_ITEM VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_REL_ITEM is 'Configured to show Releated Itesm in Incomplete Items Report. Valid values are Y - to show Related Items, N - do not show Related Items and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_TICKETS VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_TICKETS is 'Configured to show Tickets in Incomplete Items Report. Valid values are Y - to show Tickets, N - do not show Tickets and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_HTS VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_HTS is 'Configured to show HTS in Incomplete Items Report. Valid values are Y - to show HTS, N - do not show HTS and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_IMP_ATTR VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_IMP_ATTR is 'Configured to show Import Attributes in Incomplete Items Report. Valid values are Y - to show Import Attributes, N - do not show Import Attributes and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD DS_SHOW_INCOMP_ITEM_IMAGES VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.DS_SHOW_INCOMP_ITEM_IMAGES is 'Configured to show Images in Incomplete Items Report. Valid values are Y - to show Images, N - do not show Images and Null - Default to be taken from RMS_OI_SYSTEM_OPTIONS table.'
/


PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_REF_ITEM'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_REF_ITEM CHECK (DS_SHOW_INCOMP_ITEM_REF_ITEM IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_VAT'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_VAT CHECK (DS_SHOW_INCOMP_ITEM_VAT IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_SPACK'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_SPACK CHECK (DS_SHOW_INCOMP_ITEM_SPACK IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_UDA'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_UDA CHECK (DS_SHOW_INCOMP_ITEM_UDA IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_LOC'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_LOC CHECK (DS_SHOW_INCOMP_ITEM_LOC IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_SEASONS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_SEASONS CHECK (DS_SHOW_INCOMP_ITEM_SEASONS IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_REPL'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_REPL CHECK (DS_SHOW_INCOMP_ITEM_REPL IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_SUBS_ITEM'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_SUBS_ITEM CHECK (DS_SHOW_INCOMP_ITEM_SUBS_ITEM IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_DIMEN'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_DIMEN CHECK (DS_SHOW_INCOMP_ITEM_DIMEN IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_REL_ITEM'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_REL_ITEM CHECK (DS_SHOW_INCOMP_ITEM_REL_ITEM IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_TICKETS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_TICKETS CHECK (DS_SHOW_INCOMP_ITEM_TICKETS IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_HTS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_HTS CHECK (DS_SHOW_INCOMP_ITEM_HTS IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_IMP_ATTR'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_IMP_ATTR CHECK (DS_SHOW_INCOMP_ITEM_IMP_ATTR IN ('Y','N', NULL))
/

PROMPT ADDING CONSTRAINT 'CHK_RODO_DS_SHOW_IMAGES'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD CONSTRAINT
 CHK_RODO_DS_SHOW_IMAGES CHECK (DS_SHOW_INCOMP_ITEM_IMAGES IN ('Y','N', NULL))
/
