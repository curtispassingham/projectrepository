--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_STK_ORD_PEND_CLOSE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_STK_ORD_PEND_CLOSE'
CREATE TABLE RMS_OI_STK_ORD_PEND_CLOSE
 (SESSION_ID NUMBER(15,0),
  STOCK_ORDER_ID NUMBER(12,0),
  STOCK_ORDER_TYPE VARCHAR2(1 ),
  FROM_LOC NUMBER(10,0),
  FROM_LOC_NAME VARCHAR2(150 ),
  FROM_LOC_TYPE VARCHAR2(1 ),
  TO_LOC NUMBER(10,0),
  TO_LOC_NAME VARCHAR2(150 ),
  TO_LOC_TYPE VARCHAR2(1 ),
  RECEIPT_DATE DATE,
  MULTI_UOM_IND VARCHAR2(1 ),
  RECEIVED_PCT NUMBER(20,10),
  RECONCILED_QTY NUMBER(12,4),
  STOCK_ORDER_QTY NUMBER(12,4),
  SHIPPED_QTY NUMBER(12,4),
  RECEIVED_QTY NUMBER(12,4),
  CANCELLED_QTY NUMBER(12,4),
  FINISHER NUMBER(10,0),
  FINISHER_NAME VARCHAR2(150 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_STK_ORD_PEND_CLOSE is 'This table holds the information for the Stock Order Pending Close Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.STOCK_ORDER_ID is 'The Allocation or Transfer ID.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.STOCK_ORDER_TYPE is 'A for Allocation - T for Transfer.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.FROM_LOC is 'The transfer from location.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.FROM_LOC_NAME is 'The transfer from location name.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.FROM_LOC_TYPE is 'The transfer from location type.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.TO_LOC is 'The transfer to location.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.TO_LOC_NAME is 'The transfer to location name.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.TO_LOC_TYPE is 'The transfer to location type.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.RECEIPT_DATE is 'The receipt date'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.MULTI_UOM_IND is 'Indications if there are items with varrying UOMs on the transaction.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.RECEIVED_PCT is 'The received percent.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.RECONCILED_QTY is 'The reconciled quantity.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.STOCK_ORDER_QTY is 'The stock order order quantity.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.SHIPPED_QTY is 'The shipped quantity.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.RECEIVED_QTY is 'The received quantity.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.CANCELLED_QTY is 'The cancelled quantity.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.FINISHER is 'The finisher.'
/

COMMENT ON COLUMN RMS_OI_STK_ORD_PEND_CLOSE.FINISHER_NAME is 'The finisher name.'
/


PROMPT Creating Index on 'RMS_OI_STK_ORD_PEND_CLOSE'
 CREATE INDEX RMS_OI_STK_ORD_PEND_CLOSE_I1 on RMS_OI_STK_ORD_PEND_CLOSE
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

