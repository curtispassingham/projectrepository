--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING Table FIND_DEFAULT
-------------------------------------------
PROMPT DROPPING table FIND_DEFAULT

DECLARE
  L_find_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_find_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FIND_DEFAULT';

  if (L_find_table_exists != 0) then
      execute immediate 'DROP TABLE FIND_DEFAULT CASCADE CONSTRAINTS';
  end if;
end;
/