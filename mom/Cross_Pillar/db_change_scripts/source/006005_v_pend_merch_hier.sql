CREATE OR REPLACE FORCE VIEW V_PEND_MERCH_HIER AS
(
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          NULL          AS GROUP_NO,
          NULL          AS GROUP_NAME,
          NULL          AS DEPT,
          NULL          AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI
    WHERE PEND.HIER_TYPE = 'V'
      AND PEND.MERCH_HIER_ID = DI.DIVISION (+)
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          NULL          AS DEPT,
          NULL          AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS
    WHERE PEND.HIER_TYPE = 'G'
      AND PEND.MERCH_HIER_ID = GS.GROUP_NO (+)
      AND DI.DIVISION (+)    = GS.DIVISION
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          DS.DEPT       AS DEPT,
          DS.DEPT_NAME  AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS,
          DEPS DS
    WHERE PEND.HIER_TYPE = 'D'
      AND PEND.MERCH_HIER_ID = DS.DEPT (+)
      AND DI.DIVISION (+) = GS.DIVISION
      AND GS.GROUP_NO (+) = DS.GROUP_NO
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          DS.DEPT       AS DEPT,
          DS.DEPT_NAME  AS DEPT_NAME,
          CL.CLASS      AS CLASS,
          CL.CLASS_NAME AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS,
          DEPS DS,
          CLASS CL
    WHERE PEND.HIER_TYPE = 'C'
      AND PEND.MERCH_HIER_ID        = CL.CLASS (+)
      AND PEND.MERCH_HIER_PARENT_ID = CL.DEPT (+)
      AND DI.DIVISION        (+)    = GS.DIVISION
      AND GS.GROUP_NO        (+)    = DS.GROUP_NO
      AND DS.DEPT            (+)    = CL.DEPT
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          DS.DEPT       AS DEPT,
          DS.DEPT_NAME  AS DEPT_NAME,
          CL.CLASS      AS CLASS,
          CL.CLASS_NAME AS CLASS_NAME,
          SC.SUBCLASS   AS SUBCLASS,
          SC.SUB_NAME   AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS,
          DEPS DS,
          CLASS CL,
          SUBCLASS SC
    WHERE PEND.HIER_TYPE = 'S'
      AND PEND.MERCH_HIER_ID             = SC.SUBCLASS (+)
      AND PEND.MERCH_HIER_PARENT_ID      = SC.CLASS (+)
      AND PEND.MERCH_HIER_GRANDPARENT_ID = SC.DEPT (+)
      AND DI.DIVISION (+) = GS.DIVISION
      AND GS.GROUP_NO (+) = DS.GROUP_NO
      AND DS.DEPT     (+) = CL.DEPT
      AND CL.DEPT     (+) = SC.DEPT
      AND CL.CLASS    (+) = SC.CLASS
)
/
