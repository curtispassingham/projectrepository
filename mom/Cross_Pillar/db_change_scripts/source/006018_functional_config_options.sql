--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'FUNCTIONAL_CONFIG_OPTIONS'
ALTER TABLE FUNCTIONAL_CONFIG_OPTIONS MODIFY SUPPLIER_SITES_IND VARCHAR2 (1 ) DEFAULT 'Y'
/

COMMENT ON COLUMN FUNCTIONAL_CONFIG_OPTIONS.SUPPLIER_SITES_IND is 'Suppliers usually have multiple sites from which they supply merchandise. Each of these sites can have different supplier control parameters like lead times, payment terms, etc. When the Supplier Site parameter is set to Y then many functions in RMS that involve suppliers, such as purchasing, will display the supplier site details. RMS only supports the supplier site is Y configuration. For clients that do not use the supplier site functionality, they need to set up a one-to-one relation between a supplier and supplier site.'
/


PROMPT MODIFYING CONSTRAINT 'CHK_FUN_CONG_OPT_SUPPLIER_SITE'
ALTER TABLE FUNCTIONAL_CONFIG_OPTIONS DROP CONSTRAINT  CHK_FUN_CONG_OPT_SUPPLIER_SITE
/
ALTER TABLE FUNCTIONAL_CONFIG_OPTIONS ADD CONSTRAINT
 CHK_FUN_CONG_OPT_SUPPLIER_SITE CHECK (SUPPLIER_SITES_IND IN ('Y'))
/
