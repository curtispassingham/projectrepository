--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 TRANSLATE_METADATA_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'TRANSLATE_METADATA_TL'
CREATE TABLE TRANSLATE_METADATA_TL
 (LANG NUMBER(6) NOT NULL,
  BASE_TABLE VARCHAR2(30 ) NOT NULL,
  BASE_COLUMN VARCHAR2(30 ) NOT NULL,
  LABEL VARCHAR2(250 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TRANSLATE_METADATA_TL is 'This is the translation table for TRANSLATE_METADATA table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table.'
/

COMMENT ON COLUMN TRANSLATE_METADATA_TL.LANG is 'This field contains the language in which the translated text is maintained.'
/

COMMENT ON COLUMN TRANSLATE_METADATA_TL.BASE_TABLE is 'Contains the base system of record RMS table for this entity.'
/

COMMENT ON COLUMN TRANSLATE_METADATA_TL.BASE_COLUMN is 'The base column name in translate table for this field. '
/

COMMENT ON COLUMN TRANSLATE_METADATA_TL.LABEL is 'This contains the default UI label to be used for this field in the translation screen if the label is not available in the UI layer.'
/


PROMPT Creating FK on 'TRANSLATE_METADATA_TL'
 ALTER TABLE TRANSLATE_METADATA_TL
  ADD CONSTRAINT TMT_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT Creating FK on 'TRANSLATE_METADATA_TL'
 ALTER TABLE TRANSLATE_METADATA_TL
  ADD CONSTRAINT TMT_TM_FK
  FOREIGN KEY (BASE_TABLE, BASE_COLUMN)
 REFERENCES TRANSLATE_METADATA (BASE_TABLE, BASE_COLUMN)
/

