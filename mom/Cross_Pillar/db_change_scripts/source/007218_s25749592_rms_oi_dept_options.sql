--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_DEPT_OPTIONS'
ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_UNEXP_INV_WARN_COUNT NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_WARN_COUNT is 'Controls the number of item/locs when exceeded causes the unexpected inventory report to be considered critical in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IC_UNEXP_INV_CRITIAL_COUNT NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_CRITIAL_COUNT is 'Controls the number of item/locs when exceeded causes the unexpected inventory report to be considered warning in the inventory control unexpected inventory report.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IA_VARIANCE_RANGE_PCT_1 NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_1 is 'Configuring first % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IA_VARIANCE_RANGE_PCT_2 NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_2 is 'Configuring second % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IA_VARIANCE_RANGE_PCT_3 NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_3 is 'Configuring third % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS ADD IA_VARIANCE_RANGE_PCT_4 NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IA_VARIANCE_RANGE_PCT_4 is 'Configuring forth % value for Inventory Variance to Forecast tile.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS MODIFY FA_CUM_MARKON_MIN_VAR_PCT NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.FA_CUM_MARKON_MIN_VAR_PCT is 'Defines the tolerance level outside of which if the cumulative mark on % variance falls, the  subclass/locations combinations will be shown in the financial analyst cumulative markon % variance report.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS MODIFY IC_UNEXP_INV_TOLERANCE_QTY NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_TOLERANCE_QTY is 'Defines a tolerance level of unexpected inventory for an item location to be shown in the Inventory Control Unexpected Inventory Report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS MODIFY IC_NEG_INV_TOLERANCE_QTY NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_NEG_INV_TOLERANCE_QTY is 'Defines a tolerance level of negative inventory for an item location to be shown in the Inventory Control Negative Inventory Report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS MODIFY IC_UNEXP_INV_INACTIVE_IND NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_INACTIVE_IND is 'Controls if inactive item/locs are included in the inventory control unexpected inventory report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS MODIFY IC_UNEXP_INV_DISCONTINUE_IND NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_DISCONTINUE_IND is 'Controls if discontinued item/locs are included in the inventory control unexpected inventory report at Department level.'
/

ALTER TABLE RMS_OI_DEPT_OPTIONS MODIFY IC_UNEXP_INV_DELETE_IND NULL
/

COMMENT ON COLUMN RMS_OI_DEPT_OPTIONS.IC_UNEXP_INV_DELETE_IND is 'Controls if deleted item/locs are included in the inventory control unexpected inventory report at Department level.'
/

