--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Tables 
-------------------------------------- 
PROMPT DROPPING Table 'WH_STORE_ASSIGN'
DECLARE
  L_table_wh_store_assign_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_wh_store_assign_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WH_STORE_ASSIGN';

  if (L_table_wh_store_assign_exists != 0) then
      execute immediate 'drop table WH_STORE_ASSIGN CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'WH_STORE_ASSIGN_CONFLICT_TEMP'
DECLARE
  L_table_conflict_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_conflict_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WH_STORE_ASSIGN_CONFLICT_TEMP';

  if (L_table_conflict_exists != 0) then
      execute immediate 'drop table WH_STORE_ASSIGN_CONFLICT_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'WH_STORE_ASSIGN_TEMP'
DECLARE
  L_table_assign_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_assign_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'WH_STORE_ASSIGN_TEMP';

  if (L_table_assign_exists != 0) then
      execute immediate 'drop table WH_STORE_ASSIGN_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/
