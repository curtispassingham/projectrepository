--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	PACKAGE DROPPED:				STKLEDGR_QUERY_WRP
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------------
--       DROPPING PACKAGE
--------------------------------------------
PROMPT Dropping Package 'STKLEDGR_QUERY_WRP'
DECLARE
  L_package_exists number := 0;
BEGIN
 SELECT count(*) INTO L_package_exists
   FROM USER_OBJECTS
  WHERE OBJECT_NAME ='STKLEDGR_QUERY_WRP'
    AND OBJECT_TYPE ='PACKAGE';
	
  if (L_package_exists != 0)then
      execute immediate 'DROP PACKAGE STKLEDGR_QUERY_WRP' ;
 end if;
end;
/
