--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  MODIFYING CONSTARINT:               CHK_TRANS_PACK_CARTON_PACK_QTY
----------------------------------------------------------------------------

whenever sqlerror exit

------------------------------------------------------
--  TABLE 				TRANS_PACKING
------------------------------------------------------
DECLARE
  L_constraints_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraints_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_TRANS_PACK_CARTON_PACK_QTY'
     AND constraint_TYPE = 'C';

  if (L_constraints_exists != 0) then
      execute immediate 'ALTER TABLE TRANS_PACKING drop CONSTRAINT CHK_TRANS_PACK_CARTON_PACK_QTY drop INDEX';
  end if;
end;
/

UPDATE TRANS_PACKING SET CARTON_PACK_QTY = NULL WHERE CARTON_PACK_QTY IS NOT NULL AND CARTON_PACK_QTY_UOM IS NULL
/

ALTER TABLE TRANS_PACKING ADD CONSTRAINT CHK_TRANS_PACK_CARTON_PACK_QTY CHECK ((carton_pack_qty IS NOT NULL AND carton_pack_qty_uom IS NOT NULL) OR carton_pack_qty IS NULL)
/
