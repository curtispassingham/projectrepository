--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------


PROMPT Creating Index 'RESTART_PROGRAM_HISTORY_I2'
CREATE INDEX RESTART_PROGRAM_HISTORY_I2 ON RESTART_PROGRAM_HISTORY
 (START_TIME
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/