--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ADMIN_UPLD_ER  '
UPDATE SVC_ADMIN_UPLD_ER SET ERROR_TYPE='E' WHERE ERROR_TYPE IS NULL
/

ALTER TABLE SVC_ADMIN_UPLD_ER MODIFY ERROR_TYPE DEFAULT ON NULL 'E'
/
