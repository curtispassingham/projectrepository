--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_LATE_POST_TRANSACTIONS'
ALTER TABLE RMS_OI_LATE_POST_TRANSACTIONS ADD TRAN_COUNT NUMBER (10) NULL
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.TRAN_COUNT is 'Count of late transactions for a particular transaction type belonging to a particular location/subclass'
/

