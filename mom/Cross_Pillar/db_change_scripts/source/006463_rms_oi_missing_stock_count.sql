--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_MISSING_STOCK_COUNT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_MISSING_STOCK_COUNT'
CREATE TABLE RMS_OI_MISSING_STOCK_COUNT
 (SESSION_ID NUMBER(15,0),
  CYCLE_COUNT NUMBER(8,0),
  CYCLE_COUNT_DESC VARCHAR2(250 ),
  LOC NUMBER(10,0),
  LOC_TYPE VARCHAR2(1 ),
  LOC_NAME VARCHAR2(150 ),
  STOCKTAKE_DATE DATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_MISSING_STOCK_COUNT is 'This table holds the information for the Missing Stock Count Inventory Control Report.'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.CYCLE_COUNT is 'The stock count id.'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.CYCLE_COUNT_DESC is 'The stock count description.'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.LOC is 'The location'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.LOC_TYPE is 'The location type.'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.LOC_NAME is 'The location name.'
/

COMMENT ON COLUMN RMS_OI_MISSING_STOCK_COUNT.STOCKTAKE_DATE is 'The stock count date.'
/


PROMPT Creating Index on 'RMS_OI_MISSING_STOCK_COUNT'
 CREATE INDEX RMS_OI_MISSING_STOCK_COUNT_I1 on RMS_OI_MISSING_STOCK_COUNT
 (SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

