--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Sequence      
-------------------------------------- 
PROMPT DROPPING Sequence 'POS_BUTTON_CONFIG_SEQUENCE'
DECLARE
  L_pos_money_seq_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pos_money_seq_exists
    FROM USER_SEQUENCES
   WHERE SEQUENCE_NAME = 'POS_BUTTON_CONFIG_SEQUENCE';

  if (L_pos_money_seq_exists != 0) then
      execute immediate 'DROP SEQUENCE POS_BUTTON_CONFIG_SEQUENCE';
  end if;
end;
/

PROMPT DROPPING Sequence 'POS_BUTTON_ID_SEQUENCE'
DECLARE
  L_pos_money_seq_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pos_money_seq_exists
    FROM USER_SEQUENCES
   WHERE SEQUENCE_NAME = 'POS_BUTTON_ID_SEQUENCE';

  if (L_pos_money_seq_exists != 0) then
      execute immediate 'DROP SEQUENCE POS_BUTTON_ID_SEQUENCE';
  end if;
end;
/
