--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       Dropping Table POS_PAYINOUT_HEAD     
-------------------------------------------
PROMPT DROPPING Table 'POS_PAYINOUT_HEAD'
DECLARE
  L_pos_payinout_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pos_payinout_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'POS_PAYINOUT_HEAD';

  if (L_pos_payinout_exists != 0) then
      execute immediate 'drop table POS_PAYINOUT_HEAD';
  end if;
end;
/
