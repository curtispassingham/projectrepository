--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Table PRIV              
--------------------------------------

PROMPT Dropping Foreign Key on 'PRIV'
DECLARE
  L_table_priv_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_priv_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'PRIV';

  if (L_table_priv_exists != 0) then
      execute immediate 'DROP TABLE PRIV CASCADE CONSTRAINTS ';
  end if;
end;
/
