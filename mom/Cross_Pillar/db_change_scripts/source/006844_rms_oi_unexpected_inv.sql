--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_UNEXPECTED_INV'
ALTER TABLE RMS_OI_UNEXPECTED_INV ADD DEPT NUMBER (4,0) NULL
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.DEPT is 'Department number'
/

ALTER TABLE RMS_OI_UNEXPECTED_INV ADD DEPT_NAME VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.DEPT_NAME is 'Department name'
/

ALTER TABLE RMS_OI_UNEXPECTED_INV ADD CLASS NUMBER (4,0) NULL
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.CLASS is 'Class number'
/

ALTER TABLE RMS_OI_UNEXPECTED_INV ADD CLASS_NAME VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.CLASS_NAME is 'Class name'
/

ALTER TABLE RMS_OI_UNEXPECTED_INV ADD SUBCLASS NUMBER (4,0) NULL
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.SUBCLASS is 'Subclass number'
/

ALTER TABLE RMS_OI_UNEXPECTED_INV ADD SUBCLASS_NAME VARCHAR2 (120 ) NULL
/

COMMENT ON COLUMN RMS_OI_UNEXPECTED_INV.SUBCLASS_NAME is 'Subclass name'
/

