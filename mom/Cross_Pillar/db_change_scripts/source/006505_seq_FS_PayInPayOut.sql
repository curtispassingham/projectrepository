--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop Sequence         POS_PAID_INOUT_SEQUENCE      
--------------------------------------
PROMPT dropping Sequence 'POS_PAID_INOUT_SEQUENCE'
DECLARE
  L_sequence_exists number := 0;
BEGIN
   SELECT count(*) INTO L_sequence_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'POS_PAID_INOUT_SEQUENCE';

if (L_sequence_exists != 0) then
      execute immediate 'DROP SEQUENCE POS_PAID_INOUT_SEQUENCE';
  end if;
end;
/

