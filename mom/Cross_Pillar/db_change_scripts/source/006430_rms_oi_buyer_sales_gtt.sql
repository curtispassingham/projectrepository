--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_BUYER_SALES_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_BUYER_SALES_GTT'
CREATE GLOBAL TEMPORARY TABLE RMS_OI_BUYER_SALES_GTT
 (SESSION_ID NUMBER(15) NOT NULL,
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  AGG_DIFF_1 VARCHAR2(10 ),
  AGG_DIFF_2 VARCHAR2(10 ),
  AGG_DIFF_3 VARCHAR2(10 ),
  AGG_DIFF_4 VARCHAR2(10 ),
  AGG_DIFF_1_DESC VARCHAR2(120 ),
  AGG_DIFF_2_DESC VARCHAR2(120 ),
  AGG_DIFF_3_DESC VARCHAR2(120 ),
  AGG_DIFF_4_DESC VARCHAR2(120 ),
  TOTAL_RETAIL_PRIM NUMBER(20,4),
  TOTAL_COST_PRIM NUMBER(20,4),
  TOTAL_MARGIN_PRIM NUMBER(20,4),
  PRIM_CURRENCY_CODE VARCHAR2(3 ),
  SALES_UNITS NUMBER(12,4),
  STANDARD_UOM VARCHAR2(4 )
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE RMS_OI_BUYER_SALES_GTT is 'This is a global temporary table to support the dashboard Buyer Sales report process. It is used to temporarily hold the sales and margin data for an item that meets the filter criteria.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.SESSION_ID is 'Uniquely identifies a user session opening the report.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.ITEM is 'Transactional Item ID or Parent Item ID. For a tran-level item with parent and the parent item is aggregated by at least one differentiator, this holds the Parent Item ID. Otherwise, it holds the tran-level item ID.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.ITEM_DESC is 'Translated description of the ITEM.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_1 is 'Holds the diff_1 value of the tran-level item if the corresponding diff_1_aggregated_ind of its parent item is Y.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_2 is 'Holds the diff_2 value of the tran-level item if the corresponding diff_2_aggregated_ind of its parent item is Y.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_3 is 'Holds the diff_3 value of the tran-level item if the corresponding diff_3_aggregated_ind of its parent item is Y.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_4 is 'Holds the diff_4 value of the tran-level item if the corresponding diff_4_aggregated_ind of its parent item is Y.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_1_DESC is 'Holds the translated description of AGG_DIFF_1.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_2_DESC is 'Holds the translated description of AGG_DIFF_2.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_3_DESC is 'Holds the translated description of AGG_DIFF_3.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.AGG_DIFF_4_DESC is 'Holds the translated description of AGG_DIFF_4.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.TOTAL_RETAIL_PRIM is 'Holds the total_retail from tran_data converted to primary currency. It aggregates the data across all locations that meet the filter criteria. For parent/diff, it also aggregates the data by parent item and all differentiators that are aggregated.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.TOTAL_COST_PRIM is 'Holds the total_cost from tran_data converted to primary currency. It aggregates the data across all locations that meet the filter criteria. For parent/diff, it also aggregates the data by parent item and all differentiators that are aggregated.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.TOTAL_MARGIN_PRIM is 'Holds the total_retail - total_cost from tran_data converted to primary currency. It aggregates the data across all locations that meet the filter criteria. For parent/diff, it also aggregates the data by parent item and all differentiators that are aggregated.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.PRIM_CURRENCY_CODE is 'Holds the primary currency of the system.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.SALES_UNITS is 'Holds the sales quantity in item''''s standard unit of measure.'
/

COMMENT ON COLUMN RMS_OI_BUYER_SALES_GTT.STANDARD_UOM is 'Holds the item''''s standard unit of measure.'
/

