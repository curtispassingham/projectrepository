--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Tables
--------------------------------------
alter table SVC_CFA_EXT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_CFA_EXT modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ERROR_IMPACT modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ERROR_IMPACT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_FIF_GL_CROSS_REF modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_FIF_GL_CROSS_REF modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ROUNDING_RULE_DETAIL modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ROUNDING_RULE_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ROUNDING_RULE_HEAD modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ROUNDING_RULE_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_STORE_DATA modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_STORE_DATA modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ERROR_CODES modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_ERROR_CODES modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_REFERENCE modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_SA_REFERENCE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table VAT_DEPS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SEC_USER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_COST_SUSP_SUP_DETAIL_LOC modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_COST_SUSP_SUP_DETAIL_LOC modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_COST_SUSP_SUP_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_COST_SUSP_SUP_DETAIL modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_COST_SUSP_SUP_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_COST_SUSP_SUP_HEAD modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COST_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COST_DETAIL modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COUNTRY_L10N_EXT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COUNTRY_L10N_EXT modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_MASTER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_MASTER modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_MASTER modify NEXT_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_MASTER_CFA_EXT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_MASTER_CFA_EXT modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_COUNTRY_CFA_EXT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_COUNTRY_CFA_EXT modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/ 
alter table SVC_ITEM_SUPP_MANU_COUNTRY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_MANU_COUNTRY modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_UOM modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_UOM modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPPLIER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPPLIER modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_PACKITEM modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_PACKITEM modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_UDA_ITEM_DATE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_UDA_ITEM_DATE modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_UDA_ITEM_FF modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_UDA_ITEM_FF modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_PROCESS_TRACKER modify USER_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_COUNTRY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_COUNTRY modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_RPM_ITEM_ZONE_PRICE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_RPM_ITEM_ZONE_PRICE modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COST_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COST_HEAD modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COUNTRY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_COUNTRY modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPPLIER_CFA_EXT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPPLIER_CFA_EXT modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_XFORM_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_XFORM_HEAD modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_XFORM_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_XFORM_DETAIL modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_COUNTRY_DIM modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_ITEM_SUPP_COUNTRY_DIM modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_UDA_ITEM_LOV modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_UDA_ITEM_LOV modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_VAT_ITEM modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SVC_VAT_ITEM modify LAST_UPD_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table S9T_TMPL_COLS_DEF modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table S9T_TMPL_COLS_DEF modify LAST_UPDATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table S9T_TEMPLATE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table S9T_ERRORS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table S9T_ERRORS modify LAST_UPDATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ADDR modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_IMPORT_ATTR modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DEPS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DIFF_TYPE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table USER_ATTRIB modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DIFF_GROUP_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DOC modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DIVISION modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_LOC_TRAITS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table CLASS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table CHANNELS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table COND_TARIFF_TREATMENT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_CHRG_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table BUYER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table CURRENCY_RATES modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table COST_ZONE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table RELATED_ITEM_CHANGES_TEMP modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table FREIGHT_SIZE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_CHRG_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_LOC modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table PRICE_HIST modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_LOC_SOH modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SUPS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table UDA_VALUES modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DIFF_IDS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table PO_TYPE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table STOCK_LEDGER_INSERTS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table TSFZONE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPP_MANU_COUNTRY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SUBCLASS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table TSF_ENTITY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table WH modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table COUNTRY_ATTRIB modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table COST_ZONE_GROUP_LOC modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_TICKET modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table MERCHANT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table PARTNER_ORG_UNIT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table POS_MODS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table STORE_HIERARCHY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table UDA_ITEM_FF modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table WF_CUSTOMER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table FIF_GL_SETUP modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table GROUPS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_IMAGE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table COMPHEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPPLIER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_TEMP modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPP_UOM modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table NON_MERCH_CODE_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ORG_UNIT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_EXP_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_HTS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table AREA modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DEAL_COMP_TYPE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table CURRENCIES modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table SEC_GROUP modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DIFF_GROUP_DETAIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table DISTRICT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table REPL_ITEM_LOC_UPDATES modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table STORE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table UDA_ITEM_LOV modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table UDA_ITEM_DATE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table INV_ADJ modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_ATTRIBUTES modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_HTS_ASSESS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_MASTER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table CHAIN modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_EXP_HEAD modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table BANNER modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table COST_EVENT_NIL modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SEASONS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPP_COUNTRY_BRACKET_COST modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPP_COUNTRY modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPP_COUNTRY_DIM modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table OUTLOC modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table REGION modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table RTK_ROLE_PRIVS modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table STATE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table STORE_FORMAT modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table TIMELINE modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table UDA modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) 
/
alter table WF_CUSTOMER_GROUP modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/
alter table ITEM_SUPP_COUNTRY_LOC modify CREATE_ID default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))
/