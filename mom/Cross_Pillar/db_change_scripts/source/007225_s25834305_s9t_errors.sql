--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'S9T_ERRORS'
UPDATE S9T_ERRORS SET ERROR_TYPE='E' WHERE ERROR_TYPE IS NULL
/

ALTER TABLE S9T_ERRORS MODIFY ERROR_TYPE DEFAULT ON NULL 'E'
/
