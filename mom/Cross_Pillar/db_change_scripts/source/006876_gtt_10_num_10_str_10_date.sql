--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- Dropping Table:       GTT_10_NUM_10_STR_10_DATE
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Creating Table
--------------------------------------
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_TABLES
   WHERE TABLE_NAME = 'GTT_10_NUM_10_STR_10_DATE';

   if (L_table_exists = 0) then

      execute immediate 'CREATE GLOBAL TEMPORARY TABLE GTT_10_NUM_10_STR_10_DATE
         (
           NUMBER_1  NUMBER(20, 4) ,
           NUMBER_2  NUMBER(20, 4) ,
           NUMBER_3  NUMBER(20, 4) ,
           NUMBER_4  NUMBER(20, 4) ,
           NUMBER_5  NUMBER(20, 4) ,
           NUMBER_6  NUMBER(20, 4) ,
           NUMBER_7  NUMBER(20, 4) ,
           NUMBER_8  NUMBER(20, 4) ,
           NUMBER_9  NUMBER(20, 4) ,
           NUMBER_10 NUMBER(20, 4) ,
           VARCHAR2_1  VARCHAR2(255 BYTE) ,
           VARCHAR2_2  VARCHAR2(255 BYTE) ,
           VARCHAR2_3  VARCHAR2(255 BYTE) ,
           VARCHAR2_4  VARCHAR2(255 BYTE) ,
           VARCHAR2_5  VARCHAR2(255 BYTE) ,
           VARCHAR2_6  VARCHAR2(255 BYTE) ,
           VARCHAR2_7  VARCHAR2(255 BYTE) ,
           VARCHAR2_8  VARCHAR2(255 BYTE) ,
           VARCHAR2_9  VARCHAR2(255 BYTE) ,
           VARCHAR2_10 VARCHAR2(255 BYTE) ,
           DATE_1  DATE ,
           DATE_2  DATE ,
           DATE_3  DATE ,
           DATE_4  DATE ,
           DATE_5  DATE ,
           DATE_6  DATE ,
           DATE_7  DATE ,
           DATE_8  DATE ,
           DATE_9  DATE ,
           DATE_10 DATE
         )
         ON COMMIT DELETE ROWS';



      execute immediate q'!COMMENT ON TABLE GTT_10_NUM_10_STR_10_DATE IS 'A global temporary table with ten numeric values, string values, and date values.'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_1 IS 'numeric value 1'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_2 IS 'numeric value 2'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_3 IS 'numeric value 3'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_4 IS 'numeric value 4'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_5 IS 'numeric value 5'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_6 IS 'numeric value 6'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_7 IS 'numeric value 7'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_8 IS 'numeric value 8'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_9 IS 'numeric value 9'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.NUMBER_10 IS 'numeric value 10'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_1 IS 'string value 1'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_2 IS 'string value 2'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_3 IS 'string value 3'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_4 IS 'string value 4'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_5 IS 'string value 5'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_6 IS 'string value 6'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_7 IS 'string value 7'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_8 IS 'string value 8'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_9 IS 'string value 9'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_10 IS 'string value 10'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_1 IS 'date value 1'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_2 IS 'date value 2'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_3 IS 'date value 3'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_4 IS 'date value 4'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_5 IS 'date value 5'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_6 IS 'date value 6'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_7 IS 'date value 7'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_8 IS 'date value 8'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_9 IS 'date value 9'!';

      execute immediate q'!COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.DATE_10 IS 'date value 10'!';


  end if;
end;
/
