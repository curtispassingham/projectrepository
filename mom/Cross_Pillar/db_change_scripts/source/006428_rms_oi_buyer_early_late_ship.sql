--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_BUYER_EARLY_LATE_SHIP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_BUYER_EARLY_LATE_SHIP'
CREATE TABLE RMS_OI_BUYER_EARLY_LATE_SHIP
 (SESSION_ID NUMBER(15) NOT NULL,
  ORDER_NO NUMBER(12) NOT NULL,
  SUP_NAME VARCHAR2(240 ),
  NOT_BEFORE_DATE DATE,
  NOT_AFTER_DATE DATE,
  EST_ARR_DATE DATE,
  OTB_EOW_DATE DATE,
  SHIPMENT_ISSUE VARCHAR2(20 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_BUYER_EARLY_LATE_SHIP is 'This table is used to display early and late shipped orders in the the Buyer Dashboard. '
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.SESSION_ID is 'Uniquely identifies a user session opening the report.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.ORDER_NO is 'Holds the purchase order no that meets the filter criteria.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.SUP_NAME is 'Holds the translated name of the order supplier.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.NOT_BEFORE_DATE is 'Holds the not before date of the purchase order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.NOT_AFTER_DATE is 'Holds the not after date of the purchase order.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.EST_ARR_DATE is 'Holds the estmated arrival date of the order shipment.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.OTB_EOW_DATE is 'Holds the end of week date of the open to buy.'
/

COMMENT ON COLUMN RMS_OI_BUYER_EARLY_LATE_SHIP.SHIPMENT_ISSUE is 'Holds the issue that is flagged for the order shipment. For example, ASN not received yet, or early shipment where the estimated arrival date of the order shipment is before the not before date of the order.'
/

