--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW CREATED:               V_UNEXP_INV_OI_DEPT_OPTIONS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_UNEXP_INV_OI_DEPT_OPTIONS'
create or replace view V_UNEXP_INV_OI_DEPT_OPTIONS as 
SELECT ro.DEPT,DEPT_NAME,
IC_UNEXP_INV_DELETE_IND,
IC_UNEXP_INV_DISCONTINUE_IND,
IC_UNEXP_INV_INACTIVE_IND,
IC_UNEXP_INV_TOLERANCE_QTY,
IC_UNEXP_INV_WARN_COUNT,
IC_UNEXP_INV_CRITIAL_COUNT
FROM RMS_OI_DEPT_OPTIONS ro , DEPS dp
WHERE ro.dept=dp.dept and 
(
IC_UNEXP_INV_DELETE_IND IS NOT NULL OR
IC_UNEXP_INV_DISCONTINUE_IND IS NOT NULL OR
IC_UNEXP_INV_INACTIVE_IND IS NOT NULL OR
IC_UNEXP_INV_TOLERANCE_QTY IS NOT NULL OR
IC_UNEXP_INV_WARN_COUNT IS NOT NULL OR
IC_UNEXP_INV_CRITIAL_COUNT IS NOT NULL)
/

COMMENT ON TABLE V_UNEXP_INV_OI_DEPT_OPTIONS IS 'This view returns department level report option for inventory control unexpected inventory report Report.'
/

COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.DEPT IS 'Deprtment.'
/
COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.DEPT_NAME IS 'Department Name'
/
COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.IC_UNEXP_INV_CRITIAL_COUNT IS 'Controls the number of item/locs when exceeded causes the unexpected inventory report to be considered critical in the inventory control unexpected inventory report.'
/
COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.IC_UNEXP_INV_WARN_COUNT IS 'Controls the number of item/locs when exceeded causes the unexpected inventory report to be considered warning in the inventory control unexpected inventory report.'
/
COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.IC_UNEXP_INV_INACTIVE_IND IS 'Controls if inactive item/locs are included in the inventory control unexpected inventory report at Department level.'
/
COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.IC_UNEXP_INV_DISCONTINUE_IND IS 'Controls if discontinued item/locs are included in the inventory control unexpected inventory report at Department level.'
/
COMMENT ON COLUMN V_UNEXP_INV_OI_DEPT_OPTIONS.IC_UNEXP_INV_DELETE_IND IS 'Controls if deleted item/locs are included in the inventory control unexpected inventory report at Department level.'
/
