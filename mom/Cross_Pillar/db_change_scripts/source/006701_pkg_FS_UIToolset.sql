--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      PACKAGES to be DROPPED:    
                        PRINTER_SQL
						SCREEN_SQL  
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING PACKAGES
--------------------------------------
PROMPT DROPPING Package 'PRINTER_SQL'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'PRINTER_SQL';

  if (L_pkg_exists != 0) then
      execute immediate 'DROP PACKAGE PRINTER_SQL';
  end if;
end;
/

PROMPT DROPPING Package 'SCREEN_SQL'
DECLARE
  L_pkg_exists1 number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'SCREEN_SQL';

  if (L_pkg_exists1 != 0) then
      execute immediate 'DROP PACKAGE SCREEN_SQL';
  end if;
end;
/