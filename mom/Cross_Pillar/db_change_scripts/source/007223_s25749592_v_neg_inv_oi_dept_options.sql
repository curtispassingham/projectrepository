--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW CREATED:               V_NEG_INV_OI_DEPT_OPTIONS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_NEG_INV_OI_DEPT_OPTIONS'
create or replace view V_NEG_INV_OI_DEPT_OPTIONS
as
select ro.DEPT, dp.DEPT_NAME,
IC_NEG_INV_TOLERANCE_QTY 
from RMS_OI_DEPT_OPTIONS ro , DEPS dp
where ro.DEPT = dp.DEPT AND
IC_NEG_INV_TOLERANCE_QTY is not null
/

COMMENT ON TABLE V_NEG_INV_OI_DEPT_OPTIONS IS 'This view returns department level report option for Inventory Control Negative Inventory Report.'
/

COMMENT ON COLUMN V_NEG_INV_OI_DEPT_OPTIONS.DEPT IS 'Deprtment.'
/
COMMENT ON COLUMN V_NEG_INV_OI_DEPT_OPTIONS.DEPT_NAME IS 'Department Name'
/
COMMENT ON COLUMN V_NEG_INV_OI_DEPT_OPTIONS.IC_NEG_INV_TOLERANCE_QTY IS 'Defines a tolerance level of negative inventory for an item location to be shown in the Inventory Control Negative Inventory Report at Department level.'
/
