--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_LEAD_TIME_GTT'
ALTER TABLE RMS_OI_LEAD_TIME_GTT MODIFY PLAN_SUP_LEAD_TIME NUMBER (20,10)
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.PLAN_SUP_LEAD_TIME is 'The planned suppleir lead time.'
/

ALTER TABLE RMS_OI_LEAD_TIME_GTT MODIFY PLAN_WH_LEAD_TIME NUMBER (20,10)
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.PLAN_WH_LEAD_TIME is 'The planned wh lead time.'
/

ALTER TABLE RMS_OI_LEAD_TIME_GTT MODIFY PLAN_REVIEW_LEAD_TIME NUMBER (20,10)
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.PLAN_REVIEW_LEAD_TIME is 'The planned review lead time.'
/

ALTER TABLE RMS_OI_LEAD_TIME_GTT MODIFY ACTUAL_SUP_LEAD_TIME NUMBER (20,10)
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ACTUAL_SUP_LEAD_TIME is 'The actual suppleir lead time.'
/

ALTER TABLE RMS_OI_LEAD_TIME_GTT MODIFY ACTUAL_WH_LEAD_TIME NUMBER (20,10)
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ACTUAL_WH_LEAD_TIME is 'The actual wh lead time.'
/

ALTER TABLE RMS_OI_LEAD_TIME_GTT MODIFY ACTUAL_REVIEW_LEAD_TIME NUMBER (20,10)
/

COMMENT ON COLUMN RMS_OI_LEAD_TIME_GTT.ACTUAL_REVIEW_LEAD_TIME is 'The actual review lead time.'
/

