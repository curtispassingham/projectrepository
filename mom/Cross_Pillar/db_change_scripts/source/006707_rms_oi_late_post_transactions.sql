--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_LATE_POST_TRANSACTIONS'
ALTER TABLE RMS_OI_LATE_POST_TRANSACTIONS ADD LOC_TYPE VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_LATE_POST_TRANSACTIONS.LOC_TYPE is 'Location type'
/

