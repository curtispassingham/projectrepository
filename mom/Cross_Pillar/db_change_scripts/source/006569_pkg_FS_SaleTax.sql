--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Packages
-------------------------------------- 
PROMPT DROPPING Package 'TAX_VALIDATION_SQL'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_VALIDATION_SQL';

  if (L_pkg_exists != 0) then
      execute immediate 'drop package TAX_VALIDATION_SQL';
  end if;
end;
/

PROMPT DROPPING Package 'SALES_TAX_SQL'
DECLARE
  L_pkg_tax_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_tax_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'SALES_TAX_SQL';

  if (L_pkg_tax_exists != 0) then
      execute immediate 'drop package SALES_TAX_SQL';
  end if;
end;
/

PROMPT DROPPING Package 'GEOCODE_SQL'
DECLARE
  L_pkg_geo_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_geo_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'GEOCODE_SQL';

  if (L_pkg_geo_exists != 0) then
      execute immediate 'drop package GEOCODE_SQL';
  end if;
end;
/

PROMPT DROPPING Package 'TAX_VALIDATION_WRP'
DECLARE
  L_pkg_val_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_val_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_VALIDATION_WRP';

  if (L_pkg_val_exists != 0) then
      execute immediate 'drop package TAX_VALIDATION_WRP';
  end if;
end;
/

PROMPT DROPPING Package 'SALES_TAX_WRP'
DECLARE
  L_pkg_sales_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_sales_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'SALES_TAX_WRP';

  if (L_pkg_sales_exists != 0) then
      execute immediate 'drop package SALES_TAX_WRP';
  end if;
end;
/

PROMPT DROPPING Package 'GEOCODE_WRP'
DECLARE
  L_pkg__gecode_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg__gecode_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'GEOCODE_WRP';

  if (L_pkg__gecode_exists != 0) then
      execute immediate 'drop package GEOCODE_WRP';
  end if;
end;
/
