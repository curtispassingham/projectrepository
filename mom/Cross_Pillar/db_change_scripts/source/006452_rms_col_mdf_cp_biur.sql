--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Dropping Trigger: 		 RMS_COL_MDF_CP_BIUR
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Dropping Trigger               
--------------------------------------
DECLARE
  L_trigger_exists number := 0;
BEGIN
  SELECT count(*) INTO L_trigger_exists
    FROM ALL_TRIGGERS
   WHERE TRIGGER_NAME = 'RMS_COL_MDF_CP_BIUR';

  if (L_trigger_exists != 0) then
      execute immediate 'DROP TRIGGER RMS_COL_MDF_CP_BIUR';
  end if;
end;
/
