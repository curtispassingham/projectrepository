--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping FUNCTION
-------------------------------------- 
PROMPT DROPPING FUNCTION 'NEXT_PRODUCT_TAX_CODE_SEQ'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'NEXT_PRODUCT_TAX_CODE_SEQ'
   AND OBJECT_TYPE='FUNCTION';

  if (L_pkg_exists != 0) then
      execute immediate 'DROP FUNCTION NEXT_PRODUCT_TAX_CODE_SEQ';
  end if;
end;
/