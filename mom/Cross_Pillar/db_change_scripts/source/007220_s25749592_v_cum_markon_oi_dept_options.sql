--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW CREATED:               V_CUM_MARKON_OI_DEPT_OPTIONS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_CUM_MARKON_OI_DEPT_OPTIONS'
create or replace view V_CUM_MARKON_OI_DEPT_OPTIONS
as
select 
 ro.DEPT
,dp.DEPT_NAME
,FA_CUM_MARKON_MIN_VAR_PCT 
from RMS_OI_DEPT_OPTIONS ro , DEPS dp
where ro.DEPT = dp.DEPT AND
FA_CUM_MARKON_MIN_VAR_PCT is not null
/

COMMENT ON TABLE V_CUM_MARKON_OI_DEPT_OPTIONS IS 'This view returns department level report option for cumulative markon variance report.'
/

COMMENT ON COLUMN V_CUM_MARKON_OI_DEPT_OPTIONS.DEPT IS 'Deprtment.'
/
COMMENT ON COLUMN V_CUM_MARKON_OI_DEPT_OPTIONS.DEPT_NAME IS 'Department Name.'
/
COMMENT ON COLUMN V_CUM_MARKON_OI_DEPT_OPTIONS.FA_CUM_MARKON_MIN_VAR_PCT IS 'Defines the tolerance level outside of which if the cumulative mark on % variance falls, the  subclass/locations combinations will be shown in the financial analyst cumulative markon % variance report.'
/
