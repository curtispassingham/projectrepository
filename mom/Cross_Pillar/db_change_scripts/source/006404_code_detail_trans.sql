--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Dropping Table :                               CODE_DETAIL_TRANS
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT Copying data from 'CODE_DETAIL_TRANS' to 'CODE_DETAIL_TL' for non data integration langauge
MERGE INTO CODE_DETAIL_TL CDTL USING ( select cdt.code_type, 
                                              cdt.code, 
                                              cdt.code_desc,
                                              cdt.lang
                                         from code_detail_trans cdt,
                                              code_detail cd,
                                              system_config_options sco
                                        where cdt.code_type = cd.code_type
                                          and cdt.code = cd.code
                                          and cdt.lang <> sco.data_integration_lang) USE_THIS
ON (    cdtl.code_type = use_this.code_type 
    and cdtl.code = use_this.code 
    and cdtl.lang = use_this.lang)
WHEN MATCHED THEN 
    UPDATE  set cdtl.code_desc = use_this.code_desc, 
                cdtl.last_update_datetime = sysdate, 
                cdtl.last_update_id = user 
WHEN NOT MATCHED THEN 
    INSERT (lang, 
            code_type, 
            code, 
            code_desc, 
            create_datetime, 
            create_id, 
            last_update_datetime, 
            last_update_id)
    VALUES (use_this.lang, 
            use_this.code_type, 
            use_this.code, 
            use_this.code_desc, 
            sysdate, 
            user, 
            sysdate, 
            user)
/

commit
/

PROMPT Dropping table 'CODE_DETAIL_TRANS'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'CODE_DETAIL_TRANS';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE CODE_DETAIL_TRANS';
  end if;
end;
/
