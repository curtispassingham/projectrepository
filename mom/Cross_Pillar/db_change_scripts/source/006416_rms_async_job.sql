--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_ASYNC_JOB'
ALTER TABLE RMS_ASYNC_JOB ADD TASKFLOW_URL VARCHAR2 (255 ) NULL
/

COMMENT ON COLUMN RMS_ASYNC_JOB.TASKFLOW_URL is 'This field will contain the ADF taskflow url which will be use by the alerts framework to launch the screen when the user clicks on the notification link.'
/

ALTER TABLE RMS_ASYNC_JOB ADD NOTIFICATION_LAUNCH_IND VARCHAR2 (1 ) DEFAULT 'N' NOT NULL
/

COMMENT ON COLUMN RMS_ASYNC_JOB.NOTIFICATION_LAUNCH_IND is 'This field will determine if the notification will be launchable or not. If is is launchable, the notification will become a link  and launch the taskflow based on the taskflow_url.'
/


PROMPT ADDING CONSTRAINT 'CHK_RAJ_NOTIFCATION_LAUNCH_IND'
ALTER TABLE RMS_ASYNC_JOB ADD CONSTRAINT
 CHK_RAJ_NOTIFCATION_LAUNCH_IND CHECK (NOTIFICATION_LAUNCH_IND IN ('Y','N'))
/
