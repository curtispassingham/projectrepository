CREATE OR REPLACE FORCE VIEW V_RESTART_CONTROL_TL (PROGRAM_NAME, PROGRAM_DESC, LANG ) AS
SELECT  b.program_name,
        case when tl.lang is not null then tl.program_desc else b.program_desc end program_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  RESTART_CONTROL b,
        RESTART_CONTROL_TL tl
 WHERE  b.program_name = tl.program_name (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_RESTART_CONTROL_TL is 'This is the translation view for base table RESTART_CONTROL. This view fetches data in user langauge either from translation table RESTART_CONTROL_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_RESTART_CONTROL_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_RESTART_CONTROL_TL.PROGRAM_NAME is 'This field contains the batch program name.'
/

COMMENT ON COLUMN V_RESTART_CONTROL_TL.PROGRAM_DESC is 'This field contains a description of the batch program.'
/

