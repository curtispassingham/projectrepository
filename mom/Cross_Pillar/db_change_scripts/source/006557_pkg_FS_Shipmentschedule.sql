--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      PACKAGES to be DROPPED:    
                          STORE_SHIP_DATE_WRP
                          STORE_SHIP_SCHEDULE_WRP
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING PACKAGES
--------------------------------------
PROMPT DROPPING Package 'STORE_SHIP_DATE_WRP'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_SHIP_DATE_WRP';

  if (L_pkg_exists != 0) then
      execute immediate 'DROP PACKAGE STORE_SHIP_DATE_WRP';
  end if;
end;
/

PROMPT DROPPING Package 'STORE_SHIP_SCHEDULE_WRP'
DECLARE
  L_pkg_exists1 number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists1
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'STORE_SHIP_SCHEDULE_WRP';

  if (L_pkg_exists1 != 0) then
      execute immediate 'DROP PACKAGE STORE_SHIP_SCHEDULE_WRP';
  end if;
end;
/