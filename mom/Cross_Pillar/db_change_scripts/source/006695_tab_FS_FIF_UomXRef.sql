--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to rFIF_UOM_XREF this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       DROPPING Table FIF_UOM_XREF
-------------------------------------------
PROMPT DROPPING table FIF_UOM_XREF

DECLARE
  L_group_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_group_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'FIF_UOM_XREF';

  if (L_group_table_exists != 0) then
      execute immediate 'DROP TABLE FIF_UOM_XREF CASCADE CONSTRAINTS';
  end if;
end;
/