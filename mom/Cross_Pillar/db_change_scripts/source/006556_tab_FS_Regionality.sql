--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      TABLES to be DROPPED:    
                          REGIONALITY_MATRIX
                          REGIONALITY_TEMP
                          REGIONALITY_DEPT
                          REGIONALITY_SUP
                          REGIONALITY_SUP_DEPT
                          REGIONALITY_HEAD

*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TABLES
--------------------------------------
PROMPT DROPPING Table 'REGIONALITY_MATRIX'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REGIONALITY_MATRIX';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE REGIONALITY_MATRIX CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'REGIONALITY_TEMP'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REGIONALITY_TEMP';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE REGIONALITY_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'REGIONALITY_DEPT'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REGIONALITY_DEPT';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE REGIONALITY_DEPT CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'REGIONALITY_SUP'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REGIONALITY_SUP';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE REGIONALITY_SUP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'REGIONALITY_SUP_DEPT'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REGIONALITY_SUP_DEPT';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE REGIONALITY_SUP_DEPT CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT DROPPING Table 'REGIONALITY_HEAD'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'REGIONALITY_HEAD';

if (L_table_exists != 0) then
      execute immediate 'DROP TABLE REGIONALITY_HEAD CASCADE CONSTRAINTS';
  end if;
end;
/