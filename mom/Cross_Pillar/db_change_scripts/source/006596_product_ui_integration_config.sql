--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 PRODUCT_UI_INTEGRATION_CONFIG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'PRODUCT_UI_INTEGRATION_CONFIG'
CREATE TABLE PRODUCT_UI_INTEGRATION_CONFIG
 (APPLICATION_NAME VARCHAR2(30 ) NOT NULL,
  APPLICATION_URL VARCHAR2(255 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PRODUCT_UI_INTEGRATION_CONFIG is 'This table is used by the application ui to launch other applications. This table will be populated during application installation process. This table contains the URL of each of the integrated application to allow application launch from another application. '
/

COMMENT ON COLUMN PRODUCT_UI_INTEGRATION_CONFIG.APPLICATION_NAME is 'This contains the product name. Example RMS, RESA.ALLOCATION, REIM '
/

COMMENT ON COLUMN PRODUCT_UI_INTEGRATION_CONFIG.APPLICATION_URL is 'This contains application complete url including the port. Example ''http://msp10001.us.oracle.com:1001/Rms/faces/RmsLogin'''
/

