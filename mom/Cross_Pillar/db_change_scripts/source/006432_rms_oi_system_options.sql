--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_SYSTEM_OPTIONS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_SYSTEM_OPTIONS'
CREATE TABLE RMS_OI_SYSTEM_OPTIONS
 (IA_SHOW_ORD_ERR_PAST_NAD VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_SHOW_ORD_ERR_ORD_TO_CLOSE VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_SHOW_ORD_ERR_NEVER_APPRV VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_SHOW_ORD_ERR_ONCE_APPRV VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_SHOW_ORD_ERR_MISS_ORD_DATA VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_SHOW_ORD_ERR_MISS_ITEM_DATA VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_ORD_ERR_REF_ITEM_IND VARCHAR2(1 ) DEFAULT 'Y' NOT NULL,
  IA_ORD_ERR_FACTORY_IND VARCHAR2(1 ) DEFAULT 'Y' NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_SYSTEM_OPTIONS is 'This table holds the configurations related to RMS dashboards.'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_SHOW_ORD_ERR_PAST_NAD is 'Controls if the Past Not After Date Order Errors tile is displayed'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_SHOW_ORD_ERR_ORD_TO_CLOSE is 'Controls if the Orders to Close Order Errors tile is displayed'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_SHOW_ORD_ERR_NEVER_APPRV is 'Controls if the Never Approved Orders Order Errors tile is displayed'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_SHOW_ORD_ERR_ONCE_APPRV is 'Controls if the Once Approved Orders Order Errors tile is displayed'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_SHOW_ORD_ERR_MISS_ORD_DATA is 'Controls if the Missing Order Data Order Errors tile is displayed'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_SHOW_ORD_ERR_MISS_ITEM_DATA is 'Controls if the Missing Item Data Order Errors tile is displayed'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_ORD_ERR_REF_ITEM_IND is 'Controls if ref item logic is included in the Order Errors Missing Item Data Tile'
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.IA_ORD_ERR_FACTORY_IND is 'Controls if factory logic is included in teh Order Errors Missing Order Data Tile.'
/