--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ORD_ITEM_ERRORS'
ALTER TABLE RMS_OI_INV_ORD_ITEM_ERRORS ADD REF_ITEM_ACTION_IND VARCHAR2 (1 ) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ORD_ITEM_ERRORS.REF_ITEM_ACTION_IND is 'Indicates for rows with reference item errors, whether or not a primary refrerance item exists.  This will control the add reference item action is emabled.  Values will be Y/N.'
/

