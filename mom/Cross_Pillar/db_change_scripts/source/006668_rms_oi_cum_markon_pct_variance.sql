--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_CUM_MARKON_PCT_VARIANCE'
ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD HTD_GAFS_RETAIL NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.HTD_GAFS_RETAIL is 'Half-to-date Goods Available For Sale at retail, used in the retail method of accounting only. htd_gafs_retail = beginning of half stock at retail + half-to-date (purch_retail + markup_retail - RTV_retail + tsf_in_retail - tsf_out_retail))'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD HTD_GAFS_COST NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.HTD_GAFS_COST is 'Half-to-date Goods Available For Sale at cost, used in the retail method of accounting only. htd_gafs_cost = beginning of half stock at cost + half-to-date (purch_cost + freight_cost - RTV_cost + tsf_in_cost - tsf_out_cost))'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD CLS_STK_RETAIL NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CLS_STK_RETAIL is 'End of month stock on hand dollar values at retail for a subclass/location.  This is a calculated field. cls_stk_retail = opn_stk_retail + purch_retail + markup_retail - RTV _retail + tsf_in_retail - tsf_out_retail - net_sales_retail - perm_markdown_retail - prom_markdown_retail - clear_markdown_retail + markdown_can_retail - shrinkage_retail - empl_disc_retail'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD CLS_STK_COST NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CLS_STK_COST is 'End of month stock on hand dollar values at cost for a subclass/location. This is a calculated field. For retail method of accounting : cls_stk_cost = cls_stk_retail * ( 1 - cum_markon_pct/100) For cost method of accounting : cls_stk_cost = opn_stk_cost + purch_cost - RTV_cost + tsf_in_cost - tsf_out_cost'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD HTD_GAFS_RETAIL_PRIM_CUR NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.HTD_GAFS_RETAIL_PRIM_CUR is 'The HTD_GAFS_RETAIL coverted to the primary currency.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD HTD_GAFS_COST_PRIM_CUR NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.HTD_GAFS_COST_PRIM_CUR is 'The HTD_GAFS_COST coverted to the primary currency.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD CLS_STK_RETAIL_PRIM_CUR NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CLS_STK_RETAIL_PRIM_CUR is 'The CLS_STK_RETAIL coverted to the primary currency.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD CLS_STK_COST_PRIM_CUR NUMBER (20,4) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CLS_STK_COST_PRIM_CUR is 'The CLS_STK_COST coverted to the primary currency.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD EOM_DATE DATE  NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.EOM_DATE is 'The end of month date for the row.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD CURRENCY_CODE VARCHAR2 (3 ) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.CURRENCY_CODE is 'The currency for the location.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD SUBCLASS_EOM_DATE_LOC_CNT NUMBER (10,0) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.SUBCLASS_EOM_DATE_LOC_CNT is 'The count of locations that share the same dept/class/subclass and eom_date.'
/

ALTER TABLE RMS_OI_CUM_MARKON_PCT_VARIANCE ADD LOC_EOM_DATE_SUBCLASS_CNT NUMBER (10,0) NULL
/

COMMENT ON COLUMN RMS_OI_CUM_MARKON_PCT_VARIANCE.LOC_EOM_DATE_SUBCLASS_CNT is 'The count of dept/class/subclasses that share the same location and eom_date.'
/

