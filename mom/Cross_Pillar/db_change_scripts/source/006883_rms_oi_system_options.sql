--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD B_NUM_DAYS_NAD_EOW NUMBER (5) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.B_NUM_DAYS_NAD_EOW is 'Number of days between NAD and EOW date to determine if order qualifies as an issue'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD B_NUM_DAYS_EAD_OTB NUMBER (5) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.B_NUM_DAYS_EAD_OTB is 'Number of days between EAD and OTB date to determine if order qualifies as issue'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD B_NUM_DAYS_BOW_EAD NUMBER (5) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.B_NUM_DAYS_BOW_EAD is 'Number of days between beginning of week and EAD to determine if order qualifies as issue'
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD B_OTB_IND VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.B_OTB_IND is 'Indicator to turn on/off OTB report in a dashboard '
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN NUM_DAYS_NAD_P_EOW
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN NUM_DAYS_NAD_C_EOW
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN NUM_DAYS_EAD_OTB
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP COLUMN NUM_DAYS_BOW_EAD
/

