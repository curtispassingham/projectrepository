--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------
----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	CONSTRAINT MODIFIED:		        CHK_POS_STORE_POS_CONFIG_TYPE		
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------------------------
--       DROPPING CONSTRAINT CHK_POS_STORE_POS_CONFIG_TYPE
-------------------------------------------------------------
PROMPT DROPING CHECK CONSTRAINT ON 'POS_STORE'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_POS_STORE_POS_CONFIG_TYPE'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE POS_STORE DROP CONSTRAINT CHK_POS_STORE_POS_CONFIG_TYPE';
  end if;
end;
/
------------------------------------------------------------
--       CREATING CONSTRAINT CHK_POS_STORE_POS_CONFIG_TYPE
------------------------------------------------------------
PROMPT ADDING CONSTRAINT 'CHK_POS_STORE_POS_CONFIG_TYPE'
ALTER TABLE POS_STORE ADD CONSTRAINT
 CHK_POS_STORE_POS_CONFIG_TYPE CHECK (POS_CONFIG_TYPE IN ('COUP', 'PRES'))
/
