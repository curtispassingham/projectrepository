--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_INV_ANA_INV_GTT'
ALTER TABLE RMS_OI_INV_ANA_INV_GTT ADD FORECAST_SALES_HIST NUMBER (12,4) NULL
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.FORECAST_SALES_HIST is 'The forecast_sales history for the item location'
/

