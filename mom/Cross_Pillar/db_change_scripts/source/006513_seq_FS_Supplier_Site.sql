--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Sequence      
-------------------------------------- 
PROMPT DROPPING Sequence 'POS_SUPPLIER_PAYMENT_SEQUENCE'
DECLARE
  L_sequence_exists number := 0;
BEGIN
  SELECT count(*) INTO L_sequence_exists
    FROM USER_SEQUENCES
   WHERE SEQUENCE_NAME = 'POS_SUPPLIER_PAYMENT_SEQUENCE';

  if (L_sequence_exists != 0) then
      execute immediate 'drop sequence POS_SUPPLIER_PAYMENT_SEQUENCE';
  end if;
end;
/
