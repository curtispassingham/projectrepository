--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
/*--      PACKAGES to be DROPPED:    
                          ITEM_ATTRIB_DEFAULT_SQL
*/
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING PACKAGES
--------------------------------------
PROMPT DROPPING Package 'ITEM_ATTRIB_DEFAULT_SQL'
DECLARE
  L_pkg_exists number := 0;
BEGIN
  SELECT count(*) INTO L_pkg_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ITEM_ATTRIB_DEFAULT_SQL';

  if (L_pkg_exists != 0) then
      execute immediate 'DROP PACKAGE ITEM_ATTRIB_DEFAULT_SQL';
  end if;
end;
/