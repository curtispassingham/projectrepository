--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_WAC_VARIANCE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_WAC_VARIANCE'
CREATE TABLE RMS_OI_WAC_VARIANCE
 (SESSION_ID NUMBER(15),
  ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  LOC NUMBER(10),
  LOC_NAME VARCHAR2(150 ),
  UNIT_ELC NUMBER(20,4),
  AVG_COST NUMBER(20,4),
  VARIANCE_PCT NUMBER(20,10),
  INVENTORY NUMBER(20,4),
  CURRENCY_CODE VARCHAR2(3 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RMS_OI_WAC_VARIANCE is 'This table holds the information about item/locations having a variance between the unit cost and weighted average cost (WAC) that falls outside of a defined tolerance for the WAC Variance Report.'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.SESSION_ID is 'Session id'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.ITEM is 'Transactional Item ID'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.ITEM_DESC is 'Translated description of the ITEM.'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.LOC is 'Location'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.LOC_NAME is 'Location Name'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.UNIT_ELC is 'Unit Cost for the item/location'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.AVG_COST is 'Average Cost'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.VARIANCE_PCT is 'Variance Percentage'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.INVENTORY is 'Stock on hand * cost'
/

COMMENT ON COLUMN RMS_OI_WAC_VARIANCE.CURRENCY_CODE is 'Currency of the location'
/

PROMPT Creating Index 'RMS_OI_WAC_VARIANCE_I1'
CREATE INDEX RMS_OI_WAC_VARIANCE_I1 ON RMS_OI_WAC_VARIANCE
(SESSION_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
