--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ATTRIB_GROUP_LABELS'
ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY ACTION NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY PROCESS$STATUS DEFAULT 'N' NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY GROUP_ID NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.GROUP_ID is 'This column holds a generated ID that distinguishes the custom attribute group.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY LANG NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LANG is 'The attribute group labels language.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY LABEL NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LABEL is 'The attribute group label as displayed on the UI.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY CREATE_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.CREATE_ID is 'User who created the record.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY CREATE_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.CREATE_DATETIME is 'Date time when record was inserted.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY LAST_UPD_ID DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LAST_UPD_ID is 'User who last updated the record.'
/

ALTER TABLE SVC_ATTRIB_GROUP_LABELS MODIFY LAST_UPD_DATETIME DEFAULT sysdate NULL
/

COMMENT ON COLUMN SVC_ATTRIB_GROUP_LABELS.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/

