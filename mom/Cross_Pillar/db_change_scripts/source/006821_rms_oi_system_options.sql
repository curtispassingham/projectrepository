--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
CREATE TABLE DBC_RMS_OI_SYSTEM_OPTIONS AS SELECT * FROM RMS_OI_SYSTEM_OPTIONS
/

DELETE FROM RMS_OI_SYSTEM_OPTIONS
/

COMMIT
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS MODIFY FA_LATE_POST_ORG_HIER_LEVEL NUMBER (10)
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.FA_LATE_POST_ORG_HIER_LEVEL is 'Defines the level of organization hierarchy that would be used for chart display in the financial analyst late posted transactions report'
/

INSERT INTO RMS_OI_SYSTEM_OPTIONS SELECT * FROM DBC_RMS_OI_SYSTEM_OPTIONS
/

COMMIT
/

DROP TABLE DBC_RMS_OI_SYSTEM_OPTIONS
/
