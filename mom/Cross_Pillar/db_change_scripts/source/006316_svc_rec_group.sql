--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_REC_GROUP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_REC_GROUP'
CREATE TABLE SVC_REC_GROUP
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ) NOT NULL,
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N' NOT NULL,
  REC_GROUP_ID NUMBER(10) NOT NULL,
  REC_GROUP_NAME VARCHAR2(30 ) NOT NULL,
  QUERY_TYPE VARCHAR2(6 ) NOT NULL,
  TABLE_NAME VARCHAR2(30 ),
  COLUMN_1 VARCHAR2(30 ),
  COLUMN_2 VARCHAR2(30 ),
  WHERE_COL_1 VARCHAR2(30 ),
  WHERE_OPERATOR_1 VARCHAR2(6 ),
  WHERE_COND_1 VARCHAR2(120 ),
  WHERE_COL_2 VARCHAR2(30 ),
  WHERE_OPERATOR_2 VARCHAR2(6 ),
  WHERE_COND_2 VARCHAR2(120 ),
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  LAST_UPD_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPD_DATETIME DATE NOT NULL
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

COMMENT ON TABLE SVC_REC_GROUP is 'This is the staging table for CFAS record group information.It is used to temporarily hold data before it is uploaded/updated in CFA_REC_GROUP.'
/

COMMENT ON COLUMN SVC_REC_GROUP.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_REC_GROUP.CHUNK_ID is 'Uniquely identifies a chunk.The value will always be 1.'
/

COMMENT ON COLUMN SVC_REC_GROUP.ROW_SEQ is 'The rows sequence. Should be unique within a process-ID.'
/

COMMENT ON COLUMN SVC_REC_GROUP.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_REC_GROUP.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_REC_GROUP.REC_GROUP_ID is 'Unique identifier for the reference record group query.'
/

COMMENT ON COLUMN SVC_REC_GROUP.REC_GROUP_NAME is 'The name of the record group query. This is optional.'
/

COMMENT ON COLUMN SVC_REC_GROUP.QUERY_TYPE is 'Contains the query type value.  Valid values are Simple and Complex.  Simple queries can be created/maintained online.  Complex queries must be entered/maintained by a DBA and can be viewed online.'
/

COMMENT ON COLUMN SVC_REC_GROUP.TABLE_NAME is 'Contains the table value for simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.COLUMN_1 is 'Contains the first column to be selected in the query.  Generally, this should be a value column (ex. item).  this column will only contain a value for simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.COLUMN_2 is 'Contains the second column to be selected in the query.  Generally, this should be a description type column (ex. item_desc).  this column will only contain a value for simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.WHERE_COL_1 is 'Contains the first column used in the querys where clause.  This should contain a value for Simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.WHERE_OPERATOR_1 is 'Contains the operator for the first condition in the where clause.   Valid values are codes within code type CFWH.  This will contain a value for Simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.WHERE_COND_1 is 'Contains the condition value for the first condition statement in the where clause. It can be a constant value or bind variable.  This will contain a value for Simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.WHERE_COL_2 is 'Contains the column used in the second condition statment of querys where clause.  This should contain a value for Simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.WHERE_OPERATOR_2 is 'Contains the operator for the second condition in the where clause.   Valid values are codes within code type CFWH.  This will contain a value for Simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.WHERE_COND_2 is 'Contains the condition value for the second condition statement in the where clause. It can be a constant value or bind variable.  This will contain a value for Simple queries.'
/

COMMENT ON COLUMN SVC_REC_GROUP.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_REC_GROUP.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_REC_GROUP.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_REC_GROUP.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_REC_GROUP'
ALTER TABLE SVC_REC_GROUP
 ADD CONSTRAINT SVC_REC_GROUP_PK PRIMARY KEY
  (PROCESS_ID,ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_REC_GROUP'
ALTER TABLE SVC_REC_GROUP
 ADD CONSTRAINT SVC_REC_GROUP_UK UNIQUE
  (REC_GROUP_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

