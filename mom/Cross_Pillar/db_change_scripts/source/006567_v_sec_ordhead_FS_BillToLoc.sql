--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	VIEW UPDATED:				V_SEC_ORDHEAD
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_SEC_ORDHEAD'
CREATE OR REPLACE FORCE VIEW V_SEC_ORDHEAD
AS SELECT OH.ORDER_NO ORDER_NO,
          OH.ORDER_TYPE ORDER_TYPE,
          OH.DEPT DEPT,
          OH.BUYER BUYER,
          OH.SUPPLIER SUPPLIER,
          OH.SUPP_ADD_SEQ_NO SUPP_ADD_SEQ_NO,
          OH.LOC_TYPE LOC_TYPE,
          OH.LOCATION LOCATION,
          OH.PROMOTION PROMOTION,
          OH.QC_IND QC_IND,
          OH.WRITTEN_DATE WRITTEN_DATE,
          OH.NOT_BEFORE_DATE NOT_BEFORE_DATE,
          OH.NOT_AFTER_DATE NOT_AFTER_DATE,
          OH.OTB_EOW_DATE OTB_EOW_DATE,
          OH.EARLIEST_SHIP_DATE EARLIEST_SHIP_DATE,
          OH.LATEST_SHIP_DATE LATEST_SHIP_DATE,
          OH.CLOSE_DATE CLOSE_DATE,
          OH.TERMS TERMS,
          OH.FREIGHT_TERMS FREIGHT_TERMS ,
          OH.ORIG_IND ORIG_IND,
          OH.PAYMENT_METHOD PAYMENT_METHOD,
          OH.BACKHAUL_TYPE BACKHAUL_TYPE,
          OH.BACKHAUL_ALLOWANCE BACKHAUL_ALLOWANCE,
          OH.SHIP_METHOD SHIP_METHOD,
          OH.PURCHASE_TYPE PURCHASE_TYPE,
          OH.STATUS STATUS,
          OH.ORIG_APPROVAL_DATE ORIG_APPROVAL_DATE,
          OH.ORIG_APPROVAL_ID ORIG_APPROVAL_ID,
          OH.SHIP_PAY_METHOD SHIP_PAY_METHOD,
          OH.FOB_TRANS_RES FOB_TRANS_RES,
          OH.FOB_TRANS_RES_DESC FOB_TRANS_RES_DESC,
          OH.FOB_TITLE_PASS FOB_TITLE_PASS,
          OH.FOB_TITLE_PASS_DESC FOB_TITLE_PASS_DESC,
          OH.EDI_SENT_IND EDI_SENT_IND,
          OH.EDI_PO_IND EDI_PO_IND,
          OH.IMPORT_ORDER_IND IMPORT_ORDER_IND,
          OH.IMPORT_COUNTRY_ID IMPORT_COUNTRY_ID,
          OH.PO_ACK_RECVD_IND PO_ACK_RECVD_IND,
          OH.INCLUDE_ON_ORDER_IND INCLUDE_ON_ORDER_IND,
          OH.VENDOR_ORDER_NO VENDOR_ORDER_NO,
          OH.EXCHANGE_RATE EXCHANGE_RATE,
          OH.FACTORY FACTORY,
          OH.AGENT AGENT,
          OH.DISCHARGE_PORT DISCHARGE_PORT,
          OH.LADING_PORT LADING_PORT,
          OH.FREIGHT_CONTRACT_NO FREIGHT_CONTRACT_NO,
          OH.PO_TYPE PO_TYPE,
          OH.PRE_MARK_IND PRE_MARK_IND,
          OH.CURRENCY_CODE CURRENCY_CODE,
          OH.REJECT_CODE REJECT_CODE,
          OH.CONTRACT_NO CONTRACT_NO,
          OH.LAST_SENT_REV_NO LAST_SENT_REV_NO,
          OH.SPLIT_REF_ORDNO SPLIT_REF_ORDNO,
          OH.PICKUP_LOC PICKUP_LOC,
          OH.PICKUP_NO PICKUP_NO,
          OH.PICKUP_DATE PICKUP_DATE,
          OH.APP_DATETIME APP_DATETIME,
          OH.COMMENT_DESC COMMENT_DESC,
          OH.PARTNER_TYPE_1 PARTNER_TYPE_1,
          OH.PARTNER1 PARTNER1,
          OH.PARTNER_TYPE_2 PARTNER_TYPE_2,
          OH.PARTNER2 PARTNER2,
          OH.PARTNER_TYPE_3 PARTNER_TYPE_3,
          OH.PARTNER3 PARTNER3,
          OH.ITEM ITEM,
          OH.IMPORT_ID IMPORT_ID,
          OH.IMPORT_TYPE IMPORT_TYPE,
          OH.ROUTING_LOC_ID ROUTING_LOC_ID,
          OH.CLEARING_ZONE_ID CLEARING_ZONE_ID,
          OH.DELIVERY_SUPPLIER DELIVERY_SUPPLIER,
          OH.TRIANGULATION_IND TRIANGULATION_IND,
          OH.WF_ORDER_NO WF_ORDER_NO,
          OH.CREATE_ID CREATE_ID,
          OH.CREATE_DATETIME,
          OH.MASTER_PO_NO
     FROM ORDHEAD OH
    WHERE (DEPT IN (SELECT DEPT
                      FROM V_DEPS)
          OR DEPT IS NULL)
      AND SUPPLIER IN (SELECT SUPPLIER
                         FROM V_SUPS)
      AND (NOT EXISTS (SELECT 1
                         FROM ORD_INV_MGMT OIM
                        WHERE OIM.ORDER_NO = OH.ORDER_NO)
           OR EXISTS (SELECT 1
                        FROM ORD_INV_MGMT OIM
                       WHERE OIM.ORDER_NO  = OH.ORDER_NO
                         AND (POOL_SUPPLIER IN (SELECT SUPPLIER
                                                  FROM V_SUPS)
                              OR POOL_SUPPLIER IS NULL)))
      AND (LOCATION IN (SELECT STORE LOCATION
                          FROM V_STORE
                         UNION
                        SELECT WH LOCATION
                          FROM V_SEC_WH)
          OR LOCATION IS NULL)
      AND (EXISTS (SELECT 1
                     FROM ORDLOC OL
                    WHERE OL.ORDER_NO = OH.ORDER_NO
                      AND EXISTS (SELECT 1
                                    FROM V_ITEM_MASTER IM
                                   WHERE IM.ITEM = OL.ITEM)
                      AND EXISTS (SELECT 1
                                    FROM V_STORE
                                   WHERE STORE = LOCATION
                                   UNION
                                  SELECT 1
                                    FROM V_SEC_WH
                                   WHERE WH = LOCATION))
          OR EXISTS (SELECT 1
                       FROM ORDLOC_WKSHT OLW
                      WHERE OLW.ORDER_NO = OH.ORDER_NO
                        AND EXISTS (SELECT 1
                                      FROM V_ITEM_MASTER IM
                                     WHERE (IM.ITEM = OLW.ITEM
                                           AND OLW.ITEM_PARENT IS NULL)
                                        OR (IM.ITEM = OLW.ITEM_PARENT
                                           AND OLW.ITEM IS NULL)
                                        OR (IM.ITEM = OLW.ITEM
                                           AND IM.ITEM_PARENT = OLW.ITEM_PARENT))
                        AND (EXISTS (SELECT 1
                                       FROM V_STORE
                                      WHERE STORE = LOCATION
                                      UNION
                                     SELECT 1
                                       FROM V_SEC_WH
                                      WHERE WH = LOCATION)
                             OR LOCATION IS NULL))
          OR NOT EXISTS (SELECT 1
                           FROM ORDLOC OL
                          WHERE OL.ORDER_NO = OH.ORDER_NO
                          UNION
                         SELECT 1
                           FROM ORDLOC_WKSHT OLW
                          WHERE OLW.ORDER_NO = OH.ORDER_NO))
/

COMMENT ON TABLE V_SEC_ORDHEAD IS 'This view will fetch order header details using a security policy to filter user access.'
/