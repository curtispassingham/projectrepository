--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 TRANSLATE_METADATA
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'TRANSLATE_METADATA'
CREATE TABLE TRANSLATE_METADATA
 (BASE_TABLE VARCHAR2(30 ) NOT NULL,
  BASE_COLUMN VARCHAR2(30 ) NOT NULL,
  TRANSLATE_TABLE VARCHAR2(30 ) NOT NULL,
  TRANSLATE_COLUMN VARCHAR2(30 ) NOT NULL,
  VARIABLE_NAME VARCHAR2(30 ) NOT NULL,
  KEY VARCHAR2(1 ) NOT NULL,
  COLUMN_SEQUENCE NUMBER(2) NOT NULL,
  NULLABLE VARCHAR2(1 ) NOT NULL,
  DATA_TYPE VARCHAR2(10 ) NOT NULL,
  DATA_LENGTH NUMBER(6)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TRANSLATE_METADATA is 'This table is used by the application to dynamically render and process the global translation screen. This table contains the metadata of how the translation tables have been designed and the content of this table should be kept in sync with any change in structure of translation table. '
/

COMMENT ON COLUMN TRANSLATE_METADATA.BASE_TABLE is 'Contains the base system of record RMS table for this entity.'
/

COMMENT ON COLUMN TRANSLATE_METADATA.BASE_COLUMN is 'The base column name in translate table for this field. '
/

COMMENT ON COLUMN TRANSLATE_METADATA.TRANSLATE_TABLE is 'Contains the translate table name for this entity'
/

COMMENT ON COLUMN TRANSLATE_METADATA.TRANSLATE_COLUMN is 'Contain the column name in translate table'
/

COMMENT ON COLUMN TRANSLATE_METADATA.VARIABLE_NAME is 'The data shared between ADF and the DB layer is identified by this column. The signature of the database package will be having generic key variables and generic text variables. This field maps the variable being passed in the package to the column being referred in the translation tables. The column will contain values for primary key and values for text fields. Valid values are KEY_NUMBER_1 to KEY_NUMBER_4, KEY_VARCHAR2_5 to KEY_VARCHAR2_9, KEY_DATE_10  to KEY_DATE_11, TEXT_1 to TEXT_9.'
/

COMMENT ON COLUMN TRANSLATE_METADATA.KEY is 'Identifies if this field is part of the primary or unique key in the translate table.'
/

COMMENT ON COLUMN TRANSLATE_METADATA.COLUMN_SEQUENCE is 'The sequence of column in which the data has to be presented to the user'
/

COMMENT ON COLUMN TRANSLATE_METADATA.NULLABLE is 'Is this field nullable'
/

COMMENT ON COLUMN TRANSLATE_METADATA.DATA_TYPE is 'indicates the data type. Should be one of VARCHAR2 or DATE or NUMBER'
/

COMMENT ON COLUMN TRANSLATE_METADATA.DATA_LENGTH is 'Indicates the maximum length for data in this field. Can be null only in case of Date'
/


PROMPT Creating Primary Key on 'TRANSLATE_METADATA'
ALTER TABLE TRANSLATE_METADATA
 ADD CONSTRAINT PK_TRANSLATE_METADATA PRIMARY KEY
  (BASE_TABLE,
   BASE_COLUMN
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

ALTER TABLE TRANSLATE_METADATA ADD CONSTRAINT CHK_TL_METADATA_KEY CHECK (KEY IN ('Y', 'N'))
/

ALTER TABLE TRANSLATE_METADATA ADD CONSTRAINT CHK_TL_METADATA_NULLABLE CHECK (NULLABLE IN ('Y', 'N'))
/

ALTER TABLE TRANSLATE_METADATA ADD CONSTRAINT CHK_TL_METADATA_DATA_TYPE CHECK (DATA_TYPE IN ('VARCHAR2', 'DATE', 'NUMBER'))
/

ALTER TABLE TRANSLATE_METADATA ADD CONSTRAINT CHK_TL_METADATA_VARIABLE_NAME CHECK (VARIABLE_NAME IN ('KEY_NUMBER_1', 'KEY_NUMBER_2', 'KEY_NUMBER_3', 'KEY_NUMBER_4', 'KEY_VARCHAR2_5', 'KEY_VARCHAR2_6', 'KEY_VARCHAR2_7', 'KEY_VARCHAR2_8', 'KEY_VARCHAR2_9', 'KEY_DATE_10', 'KEY_DATE_11', 'TEXT_1', 'TEXT_2', 'TEXT_3', 'TEXT_4', 'TEXT_5', 'TEXT_6', 'TEXT_7', 'TEXT_8', 'TEXT_9'))
/

ALTER TABLE TRANSLATE_METADATA ADD CONSTRAINT CHK_TL_METADATA_DATA_LENGTH CHECK ((DATA_TYPE = 'DATE' AND DATA_LENGTH IS NULL) OR  (DATA_TYPE IN ('VARCHAR2', 'NUMBER') AND DATA_LENGTH IS NOT NULL))
/

ALTER TABLE TRANSLATE_METADATA ADD CONSTRAINT CHK_TL_METADATA_VARIABLE_KEY CHECK ((KEY = 'Y' AND VARIABLE_NAME IN ('KEY_NUMBER_1', 'KEY_NUMBER_2', 'KEY_NUMBER_3', 'KEY_NUMBER_4', 'KEY_VARCHAR2_5', 'KEY_VARCHAR2_6', 'KEY_VARCHAR2_7', 'KEY_VARCHAR2_8', 'KEY_VARCHAR2_9', 'KEY_DATE_10', 'KEY_DATE_11') ) OR (KEY = 'N' AND VARIABLE_NAME IN ('TEXT_1', 'TEXT_2', 'TEXT_3', 'TEXT_4', 'TEXT_5', 'TEXT_6', 'TEXT_7', 'TEXT_8', 'TEXT_9')))
/

