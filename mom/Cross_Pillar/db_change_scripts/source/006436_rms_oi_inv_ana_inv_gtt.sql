--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_INV_ANA_INV_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RMS_OI_INV_ANA_INV_GTT'
CREATE GLOBAL TEMPORARY TABLE RMS_OI_INV_ANA_INV_GTT
 (SESSION_ID NUMBER(15,0),
  ITEM VARCHAR2(25 ),
  ITEM_PARENT VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  ALC_ITEM_TYPE VARCHAR2(15 ),
  AGG_DIFF_1 VARCHAR2(10 ),
  AGG_DIFF_2 VARCHAR2(10 ),
  AGG_DIFF_3 VARCHAR2(10 ),
  AGG_DIFF_4 VARCHAR2(10 ),
  LOC NUMBER(10,0),
  GROUPING_LOC_ID VARCHAR2(150 ),
  GROUPING_LOC_NAME VARCHAR2(150 ),
  STOCK_ON_HAND NUMBER(12,4),
  IN_TRANSIT_QTY NUMBER(12,4),
  PACK_COMP_INTRAN NUMBER(12,4),
  PACK_COMP_SOH NUMBER(12,4),
  TSF_RESERVED_QTY NUMBER(12,4),
  PACK_COMP_RESV NUMBER(12,4),
  TSF_EXPECTED_QTY NUMBER(12,4),
  PACK_COMP_EXP NUMBER(12,4),
  RTV_QTY NUMBER(12,4),
  NON_SELLABLE_QTY NUMBER(12,4),
  PACK_COMP_NON_SELLABLE NUMBER(12,4),
  CUSTOMER_RESV NUMBER(12,4),
  PACK_COMP_CUST_RESV NUMBER(12,4),
  CUSTOMER_BACKORDER NUMBER(12,4),
  PACK_COMP_CUST_BACK NUMBER(12,4),
  FORECAST_SALES NUMBER(12,4),
  ON_ORDER_QTY NUMBER(12,4),
  CO_INBOUND NUMBER(12,4),
  PL_TSF_INBOUND NUMBER(12,4),
  ALLOC_INBOUND NUMBER(12,4),
  ALLOC_OUTBOUND NUMBER(12,4),
  SALES_HIST_QTY NUMBER(12,4),
  UNIT_RETAIL NUMBER(20,4),
  AVAIL_QTY NUMBER(12,4)
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE RMS_OI_INV_ANA_INV_GTT is 'This table holds data for the Inventory Analyst Order Errors Detail Report.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.SESSION_ID is 'The session_id for the data.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ITEM is 'The item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ITEM_PARENT is 'The item parent.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ITEM_DESC is 'The item description.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ALC_ITEM_TYPE is 'The alc_item_type of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.AGG_DIFF_1 is 'The aggregate diff 1 of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.AGG_DIFF_2 is 'The aggregate diff 2 of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.AGG_DIFF_3 is 'The aggregate diff 3 of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.AGG_DIFF_4 is 'The aggregate diff 4 of the item.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.LOC is 'The location.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.GROUPING_LOC_ID is 'The grouping location ID.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.GROUPING_LOC_NAME is 'The grouping location name.'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.STOCK_ON_HAND is 'The stock on hand for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.IN_TRANSIT_QTY is 'The in transit qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_INTRAN is 'The pack_comp_intran for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_SOH is 'The pack_comp_soh for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.TSF_RESERVED_QTY is 'The tsf_reserved_qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_RESV is 'The pack_comp_resv for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.TSF_EXPECTED_QTY is 'The tsf_expected_qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_EXP is 'The pack_comp_exp for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.RTV_QTY is 'The rtv_qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.NON_SELLABLE_QTY is 'The non_sellable_qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_NON_SELLABLE is 'The pack_comp_non_sellable for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.CUSTOMER_RESV is 'The customer_resv for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_CUST_RESV is 'The pack_comp_cust_resv for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.CUSTOMER_BACKORDER is 'The customer_backorder for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PACK_COMP_CUST_BACK is 'The pack_comp_cust_back for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.FORECAST_SALES is 'The forecast_sales for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ON_ORDER_QTY is 'The on_order_qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.CO_INBOUND is 'The co_inbound for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.PL_TSF_INBOUND is 'The pl_tsf_inbound for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ALLOC_INBOUND is 'The alloc_inbound for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.ALLOC_OUTBOUND is 'The alloc_outbound for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.SALES_HIST_QTY is 'The sales_hist_qty for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.UNIT_RETAIL is 'The unit_retail for the item location'
/

COMMENT ON COLUMN RMS_OI_INV_ANA_INV_GTT.AVAIL_QTY is 'The avail_qty for the item location'
/

