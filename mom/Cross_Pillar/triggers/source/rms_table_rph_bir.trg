--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          RMS_TABLE_RPH_BIR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'RMS_TABLE_RPH_BIR'
CREATE OR REPLACE TRIGGER RMS_TABLE_RPH_BIR
/* As SYS, grant the following:
       grant select on V_$SESSION to <table_owner>;
       grant select on V_$SESS_IO to <table_owner>;
       grant select on V_$STATNAME to <table_owner>;
       grant select on V_$SESSTAT to <table_owner>;
  */

  BEFORE INSERT ON RESTART_PROGRAM_HISTORY
  FOR EACH ROW
DECLARE
  v_block_gets            NUMBER;
  v_consistent_gets       NUMBER;
  v_physical_reads        NUMBER;
  v_block_changes         NUMBER;
  v_consistent_changes    NUMBER;
  v_lread                 NUMBER;
  v_lwrite                NUMBER;
  v_pread                 NUMBER;
  v_uga_max               NUMBER;
  v_pga_max               NUMBER;
  v_sqlnet_bytes_from_client NUMBER;
  v_sqlnet_bytes_to_client NUMBER;
  v_sqlnet_roundtrips     NUMBER;
  v_commits               NUMBER;
    
  CURSOR vsessio_cur IS
    SELECT s.block_gets,
           s.consistent_gets,
           s.physical_reads,
           s.block_changes,
           s.consistent_changes
      FROM v$session o, v$sess_io s
     WHERE s.sid = o.sid
       AND o.audsid = userenv('sessionid');

  CURSOR vsessort_cur IS
    SELECT SUM(DECODE(name,'session uga memory max', value )),
           SUM(DECODE(name,'session pga memory max', value )),
           SUM(DECODE(name,'bytes received via SQL*Net from client', value )),
           SUM(DECODE(name,'bytes sent via SQL*Net to client', value )),
           SUM(DECODE(name,'SQL*Net roundtrips to/from client', value )),
           SUM(DECODE(name,'user commits', value))
      FROM (SELECT n.name, ss.value
              FROM v$session s, v$statname n, v$sesstat ss
             WHERE s.audsid = USERENV('sessionid')
               AND n.name IN ('session pga memory max',
                              'session uga memory max',
                              'bytes received via SQL*Net from client',
                              'bytes sent via SQL*Net to client',
                              'SQL*Net roundtrips to/from client',
                              'user commits')
               AND ss.sid = s.sid
               AND ss.statistic# = n.statistic# );

BEGIN
  IF INSERTING THEN
    OPEN vsessio_cur;
    FETCH vsessio_cur INTO v_block_gets, v_consistent_gets,
      v_physical_reads, v_block_changes, v_consistent_changes;
    CLOSE vsessio_cur;
    :new.lread := v_block_gets + v_consistent_gets;
    :new.lwrite := v_block_changes + v_consistent_changes;
    :new.pread := v_physical_reads;

    OPEN vsessort_cur;
    FETCH vsessort_cur INTO v_uga_max, v_pga_max, v_sqlnet_bytes_from_client,
      v_sqlnet_bytes_to_client, v_sqlnet_roundtrips, v_commits;
    CLOSE vsessort_cur;
    :new.uga_max := v_uga_max;
    :new.pga_max := v_pga_max;
    :new.sqlnet_bytes_from_client := v_sqlnet_bytes_from_client;
    :new.sqlnet_bytes_to_client := v_sqlnet_bytes_to_client;
    :new.sqlnet_roundtrips := v_sqlnet_roundtrips;
    :new.commits := v_commits;
  END IF;
END;
/