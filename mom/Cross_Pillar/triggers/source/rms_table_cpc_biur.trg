--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          RMS_TABLE_CPC_BIUR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------

PROMPT Creating Trigger 'RMS_TABLE_CPC_BIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_CPC_BIUR
 BEFORE INSERT OR UPDATE 
 ON CUSTOM_PKG_CONFIG
 FOR EACH ROW
DECLARE
   L_user      VARCHAR2(30) := USER;
   L_sysdate   DATE         := SYSDATE;
BEGIN
   :new.USER_ID := L_user;
   :new.LAST_UPDATE_DATETIME := L_sysdate;
END RMS_TABLE_CPC_BIUR;
/
