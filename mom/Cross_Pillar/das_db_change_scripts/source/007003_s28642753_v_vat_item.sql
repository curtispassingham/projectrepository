--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying View               
--------------------------------------

PROMPT MODIFYING VIEW 'V_VAT_ITEM';

CREATE OR REPLACE VIEW V_VAT_ITEM ( ITEM, VAT_REGION, ACTIVE_DATE, VAT_TYPE, VAT_CODE, VAT_RATE, CREATE_DATE, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) AS
SELECT    VT.ITEM                  AS ITEM,
          VT.VAT_REGION            AS VAT_REGION,
          VT.ACTIVE_DATE           AS ACTIVE_DATE,
          VT.VAT_TYPE              AS VAT_TYPE,
          VT.VAT_CODE              AS VAT_CODE,
          VT.VAT_RATE              AS VAT_RATE,
          VT.CREATE_DATE           AS CREATE_DATE,
          VT.CREATE_ID             AS CREATE_ID,
          VT.CREATE_DATETIME       AS CREATE_DATETIME,
          VT.LAST_UPDATE_DATETIME  AS LAST_UPDATE_DATETIME,
          VT.LAST_UPDATE_ID        AS LAST_UPDATE_ID
     FROM VAT_ITEM VT
    WHERE EXISTS ( SELECT 'X'
                     FROM V_ITEMS VIT
                    WHERE VT.ITEM = VIT.IM_ITEM
                      AND ROWNUM=1)
/

