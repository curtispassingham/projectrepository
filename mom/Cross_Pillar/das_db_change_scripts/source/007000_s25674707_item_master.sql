--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_MASTER'

DECLARE
  L_index_exists number := 0;
BEGIN
  SELECT count(*) INTO L_index_exists
    FROM USER_INDEXES
   WHERE INDEX_NAME = 'ITEM_MASTER_I3'
     AND INDEX_TYPE = 'NORMAL';

  if (L_index_exists != 0) then
      execute immediate 'DROP INDEX ITEM_MASTER_I3';
  end if;
end;
/

PROMPT Creating Index 'ITEM_MASTER_I3'
CREATE INDEX ITEM_MASTER_I3 on ITEM_MASTER
  (DEPT,
   CLASS,
   SUBCLASS,
   ITEM,
   ITEM_LEVEL,
   TRAN_LEVEL
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

