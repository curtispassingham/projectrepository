
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
create or replace package ari_interface_sql AUTHID CURRENT_USER as

/*-------------------------
Takes a user name and returns a boolean indicating whether the user is an ARI user.
-------------------------*/

function get_is_ari_user(iov_error_message   in out   varchar2,
                          iob_is_ari_user     in out   boolean,
                          iv_username         in       varchar2)
return boolean;

/*-------------------------
Returns the icon name that should be displayed for a given user based on the user's current alerts.
-------------------------*/

function get_button_and_icon_name(iov_error_message   in out   varchar2,
                                  iov_icon_name       in out   varchar2,
                                  iov_button_name     in out   varchar2,
                                  iv_username         in       varchar2)
return boolean;

end;
/


