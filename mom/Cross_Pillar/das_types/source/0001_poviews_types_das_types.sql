--------------------------------------------------------
-- Copyright (c) 2013, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      TYPE ADDED: 					POVIEW_DEPARTMENTIDS 	
--							POVIEW_CLASS1IDS 		
--							POVIEW_SUBCLASSIDS 		
--							POVIEW_CHAINIDS 		
--							POVIEW_CHANNELIDS 		
--							POVIEW_AREAIDS 			
--							POVIEW_DISTRICTIDS 		
--							POVIEW_REGIONIDS 		
--							POVIEW_LOCATIONIDS 		
--							POVIEW_SUBCLASSCLASS1IDS 		
--							POVIEW_SUBCLASSDEPARTMENTIDS 	
--							POVIEW_CLASS1DEPARTMENTIDS 							
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------

CREATE OR REPLACE TYPE POVIEW_DEPARTMENTIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_CLASS1IDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_SUBCLASSIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_CHAINIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_CHANNELIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_AREAIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_DISTRICTIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_REGIONIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_LOCATIONIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_SUBCLASSCLASS1IDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_SUBCLASSDEPARTMENTIDS AS TABLE OF NUMBER(10)
/
CREATE OR REPLACE TYPE POVIEW_CLASS1DEPARTMENTIDS AS TABLE OF NUMBER(10)
/

