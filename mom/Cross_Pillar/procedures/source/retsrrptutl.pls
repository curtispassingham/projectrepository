------------------------------------------------------------------------------
-- Name:    UPDATE_RET_SER_RPT_URL
--
-- Purpose: This function is used update retail_service_report_url for sys_account value.
--
--          During Install, retail_service_report_url.sql script populates only RS_CODE, RS_NAME and RS_TYPE details,
--          later it is modified by Customer to load URLs details when they are ready.
--
--          This update function should be used while updating the:
--          1) sys_account,
--          2) keystore_passwd
--          3) private_key_alias_passwd.
--
-----------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION UPDATE_RET_SER_RPT_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                                  I_rs_code        IN      RETAIL_SERVICE_REPORT_URL.RS_CODE%TYPE,
                                                  I_sys_account    IN      VARCHAR2,
                                                  I_account_type   IN      VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN AUTHID CURRENT_USER IS

   L_program VARCHAR2(50) := 'UPDATE_RET_SER_RPT_URL';

BEGIN

   if I_rs_code is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_rs_code',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_sys_account is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_sys_account',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   /*The I_account_type will determine what encrypted column to update into retail_service_report_url.
     Valid values are 'SA' (system_account), 'KSP' (key store password) or 'PKP' (private key alias password).*/
   if NVL(I_account_type, 'SA') = 'SA' then

      update retail_service_report_url
         set sys_account = ENCRYPT_DATA(I_sys_account)
       where rs_code = I_rs_code;

   elsif I_account_type = 'KSP' then

      update retail_service_report_url
         set keystore_passwd = ENCRYPT_DATA(I_sys_account)
       where rs_code = I_rs_code;

   elsif I_account_type = 'PKP' then

      update retail_service_report_url
         set private_key_alias_passwd = ENCRYPT_DATA(I_sys_account)
       where rs_code = I_rs_code;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END;
/