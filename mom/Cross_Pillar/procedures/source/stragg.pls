create or replace function stragg
  ( input varchar2 )
  return varchar2 authid current_user
  deterministic
  parallel_enable
  aggregate using stragg_type
;
/
