SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------
-- Name:    GET_USER
-- Purpose: Retrieves the user-id from either RETAIL_CTX or
--          CLIENT_INFO or SESSION_USER
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_user
    RETURN VARCHAR2 AUTHID CURRENT_USER
IS
   pragma udf;
BEGIN
   RETURN NVL(sys_context('RETAIL_CTX', 'APP_USER_ID'), NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')));
END get_user;
/
