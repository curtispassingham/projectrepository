
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------
-- Name:    GET_VDATE
-- Purpose: This function returns the current vdate from table period
--          The return string is 'date' <default format = 'dd-MON-yy'>.
--          NO parameters are passed into this function.
--          If there is an error then the NULL is sent back out.
-------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_vdate RETURN DATE AUTHID CURRENT_USER AS 
   L_vdate       period.vdate%TYPE := null;
BEGIN
   L_vdate := DATES_SQL.GET_VDATE;
   
   return L_vdate;
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;   
END;
/


