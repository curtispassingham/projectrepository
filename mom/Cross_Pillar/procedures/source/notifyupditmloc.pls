CREATE OR REPLACE PROCEDURE NOTIFY_UPD_ITEM_LOC (context  raw,
                                                 reginfo  sys.aq$_reg_info,
                                                 descr    sys.aq$_descriptor,
                                                 payload  raw,
                                                 payloadl number) AUTHID CURRENT_USER as
   L_program               VARCHAR2(61) := 'NOTIFY_UPD_ITEM_LOC';
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   RMS_ASYNC_PROCESS_SQL.NOTIFY_UPDATE_ITEM_LOC(context,
                                                reginfo,
                                                descr,
                                                payload,
                                                payloadl);
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PROCEDURE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      raise PROGRAM_ERROR;
END;
/
