
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------
-- Name:    GET_USER_LANG
-- Purpose: Retrieves the language from the user_preferences table
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_USER_LANG RETURN NUMBER AUTHID CURRENT_USER IS

   O_user_lang lang.lang%TYPE  := NULL;

BEGIN
   O_user_lang := LANGUAGE_SQL.GET_USER_LANGUAGE;
   return O_user_lang;

EXCEPTION
   when OTHERS then
      return -999;

END GET_USER_LANG;
/
