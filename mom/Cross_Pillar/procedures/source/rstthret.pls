
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------------------
-- Name:    RESTART_THREAD_RETURN 
-- Purpose: This program will be the main "shell" for threading called by all
--   batch programs.  Algorithms for different values of total_threads
--   will be defined in different programs and called from this one.
--
--   Determines a thread value (for batch processing) of a given
--   number based on 1) the value of the number and 2) the algorithm
--   based on the total number of threads assigned to a process.
--
--   A batch program calls this function in the select statement of the
--   driving query to return a thread value when passed a unit value (will 
--   depend on the logical unit of work, LUW, for the query) and the total
--   number of threads available.  The purpose is to divide a query into
--   a separate, unique sets of data to be run as concurrant processes.
--
--   This procedure expects 2 input parameters:
--      unit_value    -  a numeric threading unit value (e.g. dept) 
--      total_threads -  total number of threads assigned to batch program
--			(fetched by batch from restart_control table)
--    
--   This procedure returns one integer value as the thread number associated
--   with the passed unit_value total_threads combination.
------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION RESTART_THREAD_RETURN(in_unit_value    NUMBER,
					         in_total_threads NUMBER) 
   RETURN NUMBER AUTHID CURRENT_USER IS
   ret_val NUMBER;
BEGIN
   
   ret_val := MOD(ABS(in_unit_value),in_total_threads) + 1;

   RETURN ret_val;
END;
/


