
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------
-- Name:    GET_PRIMARY_LANG
-- Purpose: Retrieve the primary language from the system_options table.
--------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_PRIMARY_LANG RETURN NUMBER AUTHID CURRENT_USER IS

   O_primary_lang lang.lang%TYPE  := NULL;

BEGIN
   O_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   return O_primary_lang;

EXCEPTION
   when OTHERS then
      return -999;

END GET_PRIMARY_LANG;
/
