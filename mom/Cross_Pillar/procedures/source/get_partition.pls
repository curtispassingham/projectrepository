-----------------------------------------------------------------------------------
-- Name: GET_PARTITION
-- Purpose: To get PARTITION_NAME, using HIGH_VALUE
--
--
--
-----------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_PARTITION(I_owner     IN  VARCHAR2,
                                         I_tablename IN  VARCHAR2,
                                         I_domain_id  IN  VARCHAR2)
RETURN VARCHAR2 AUTHID CURRENT_USER AS

L_error_msg     rtk_errors.rtk_text%type;
L_table_index   NUMBER := 0;

TYPE Partition_Record IS RECORD
(
   L_partition_name   ALL_TAB_PARTITIONS.TABLE_NAME%TYPE,
   L_high_value       VARCHAR2(4000)
);

TYPE T_partition_type IS TABLE OF Partition_Record INDEX BY BINARY_INTEGER;

T_partition T_partition_type;

cursor C_GET_PARTITION is
       select partition_name, high_value
         from all_tab_partitions
        where table_name = I_tablename
          and table_owner = I_owner;

BEGIN

   for p_rec in C_GET_PARTITION
   LOOP
      T_partition(L_table_index).L_partition_name := p_rec.partition_name;
      T_partition(L_table_index).L_high_value := p_rec.high_value;
      L_table_index := L_table_index + 1;
   END LOOP;
   
   L_table_index := 0;

   FOR L_table_index in T_partition.FIRST.. T_partition.LAST
   LOOP
      if T_partition(L_table_index).L_high_value = I_domain_id
      then
         return T_partition(L_table_index).L_partition_name;
      end if;
   END LOOP;
   return null;
   
EXCEPTION
   when OTHERS then
         l_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                           'GET_PARTITION',
                                            TO_CHAR(SQLCODE));
   
   return null;
END;
/