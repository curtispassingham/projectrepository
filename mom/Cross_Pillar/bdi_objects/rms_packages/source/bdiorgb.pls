CREATE OR REPLACE PACKAGE BODY BDI_ORG_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION STORE_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_control_id     IN OUT  NUMBER,
                  I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   --need to use the newer version of this package, the one that has a boolean return value to represent a failure/success.
   if STORE_FND_OUT_DATACTL.BEGINFULLSET_STORE_FND(L_control_id,
                                                   O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into store_out(store_id,
                         store_type,
                         store_name,
                         store_name_10,
                         store_name_abbr,
                         store_name_sec,
                         store_class_id,
                         store_class_desc,
                         manager,
                         open_date,
                         close_date,
                         acquire_date,
                         remodel_date,
                         fax_number,
                         phone_number,
                         email,
                         total_sq_feet,
                         selling_sq_feet,
                         linear_distance,
                         vat_region,
                         vat_incl_ind,
                         stock_holding_ind,
                         channel_id,
                         channel_name,
                         store_format_id,
                         store_format_name,
                         mall_name,
                         district,
                         transfer_zone,
                         transfer_zone_desc,
                         default_wh,
                         stop_order_days,
                         start_order_days,
                         currency_code,
                         store_lang_iso_code,
                         tran_no_generate,
                         duns_number,
                         sister_store,
                         tsf_entity_id,
                         org_unit_id,
                         auto_rcv,
                         remerch_ind,
                         wf_customer,
                         timezone,
                         customer_order_loc_ind)
      select s.store,
             s.store_type,
             s.store_name,
             s.store_name10,
             s.store_name3,
             s.store_name_secondary,
             s.store_class,
             cd.code_desc,
             s.store_mgr_name,
             s.store_open_date,
             s.store_close_date,
             s.acquired_date,
             s.remodel_date,
             s.fax_number,
             s.phone_number,
             s.email,
             s.total_square_ft,
             s.selling_square_ft,
             s.linear_distance,
             decode(vr.vat_calc_type,'E',null,s.vat_region),
             s.vat_include_ind,
             s.stockholding_ind,
             s.channel_id,
             c.channel_name,
             s.store_format,
             sf.format_name,
             s.mall_name,
             s.district,
             s.transfer_zone,
             tz.description,
             s.default_wh,
             s.stop_order_days,
             s.start_order_days,
             s.currency_code,
             l.iso_code,
             s.tran_no_generated,
             s.duns_number,
             s.sister_store,
             s.tsf_entity_id,
             s.org_unit_id,
             s.auto_rcv,
             s.remerch_ind,
             s.wf_customer_id,
             s.timezone_name,
             s.customer_order_loc_ind
        from store s,
             code_detail cd,
             channels c,
             store_format sf,
             lang l,
             vat_region vr,
             tsfzone tz
       where cd.code_type    = 'CSTR'
         and cd.code         = s.store_class
         and s.channel_id    = c.channel_id (+)
         and s.store_format  = sf.store_format (+)
         and s.lang          = l.lang
         and s.vat_region    = vr.vat_region(+)
         and s.transfer_zone = tz.transfer_zone(+);

   --need to use the newer version of this package, the one that has a boolean return value to represent a failure/succes
   if STORE_FND_OUT_DATACTL.ONSUCCENDSET_STORE_FND(I_job_context,
                                                   L_control_id,
                                                   O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_ORG_SQL.STORE_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END STORE_UP;
--------------------------------------------------------------------------------------------
FUNCTION STORE_ADDR_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_control_id     IN OUT  NUMBER,
                       I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if STOREADDR_FND_OUT_DATACTL.BEGINFULLSET_STOREADDR_FND(L_control_id,
                                                           O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into store_addr_out(store_id,
                              addr,
                              addr_type,
                              addr_type_desc,
                              primary_addr_ind,
                              add_1,
                              add_2,
                              add_3,
                              city,
                              county,
                              state,
                              country,
                              post_code,
                              jurisdiction_code,
                              contact_name,
                              contact_phone,
                              contact_fax,
                              contact_email)
      select a.key_value_1,
             a.addr_key,
             a.addr_type,
             vatt.type_desc,
             a.primary_addr_ind,
             a.add_1,
             a.add_2,
             a.add_3,
             a.city,
             a.county,
             a.state,
             a.country_id,
             a.post,
             a.jurisdiction_code,
             a.contact_name,
             a.contact_phone,
             a.contact_fax,
             a.contact_email
        from addr a,
             v_add_type_tl vatt
       where a.module in ('ST', 'WFST')
         and a.addr_type = vatt.address_type;

   --need to use the newer version of this package, the one that has a boolean return value to represent a failure/succes
   if STOREADDR_FND_OUT_DATACTL.ONSUCCENDSET_STOREADDR_FND(I_job_context,
                                                           L_control_id,
                                                           O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_ORG_SQL.STORE_ADDR_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END STORE_ADDR_UP;
--------------------------------------------------------------------------------------------
END BDI_ORG_SQL;
/


