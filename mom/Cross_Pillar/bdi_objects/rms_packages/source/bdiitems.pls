CREATE OR REPLACE PACKAGE BDI_ITEM_SQL AS
----------------------------------------------------------------------------
FUNCTION ITEM_MASTER_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_control_id     IN OUT  NUMBER,
                        I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
FUNCTION ITEM_LOC_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_control_id     IN OUT  NUMBER,
                     I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
FUNCTION REL_ITEM_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_control_id     IN OUT  NUMBER,
                     I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
END BDI_ITEM_SQL;
/


