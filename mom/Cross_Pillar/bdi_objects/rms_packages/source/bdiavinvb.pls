CREATE OR REPLACE PACKAGE BODY BDI_AV_INV_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION WH_AVAIL_INV_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_control_id     IN OUT  NUMBER,
                         I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if INVAVAILWH_TX_OUT_DATACTL.BEGINFULLSET_INVAVAILWH_TX(L_control_id,
                                                           O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into inv_avail_wh_out(item,
                                location,
                                loc_type,
                                avail_qty,
                                stock_on_hand,
                                standard_uom)
      select im.item,
             ils.loc,
             'W',
             GREATEST(ils.stock_on_hand - (  GREATEST(ils.tsf_reserved_qty,0)
                                           + GREATEST(ils.customer_resv,0)
                                           + GREATEST(ils.customer_backorder,0)
                                           + GREATEST(ils.non_sellable_qty,0)
                                           + GREATEST(ils.rtv_qty,0)
                                          ),0)   avail_qty,
             ils.stock_on_hand,
             im.standard_uom
        from item_master im,
             item_loc_soh ils,
             wh w
       where im.status          = 'A'
         and im.item_level      = im.tran_level
         and im.inventory_ind   = 'Y'
         and im.item            = ils.item
         and ils.loc_type       = 'W'
         and ils.loc            = w.wh
         and w.stockholding_ind = 'Y';

   if INVAVAILWH_TX_OUT_DATACTL.ONSUCCENDSET_INVAVAILWH_TX(I_job_context,
                                                           L_control_id,
                                                           O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;

   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_AV_INV_SQL.WH_AVAIL_INV_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END WH_AVAIL_INV_UP;
--------------------------------------------------------------------------------------------
FUNCTION ST_AVAIL_INV_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_control_id     IN OUT  NUMBER,
                         I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if INVAVAILSTORE_TX_OUT_DATACTL.BEGINFULLSET_INVAVAILSTORE_TX(L_control_id,
                                                                 O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;


   insert into inv_avail_store_out(item,
                                   location,
                                   loc_type,
                                   avail_qty,
                                   stock_on_hand,
                                   standard_uom)
      select im.item,
             ils.loc,
             'S',
             GREATEST(ils.stock_on_hand - (  GREATEST(ils.tsf_reserved_qty,0)
                                           + GREATEST(ils.customer_resv,0)
                                           + GREATEST(ils.customer_backorder,0)
                                           + GREATEST(ils.non_sellable_qty,0)
                                           + GREATEST(ils.rtv_qty,0)
                                          ),0)   avail_qty,
             ils.stock_on_hand,
             im.standard_uom
        from item_master im,
             item_loc_soh ils,
             store s
       where im.status        = 'A'
         and im.item_level    = im.tran_level
         and im.inventory_ind = 'Y'
         and im.item          = ils.item
         and im.pack_ind      = 'N'
         and ils.loc_type     = 'S'
         and ils.loc          = s.store
         and s.stockholding_ind = 'Y';

   if INVAVAILSTORE_TX_OUT_DATACTL.ONSUCCENDSET_INVAVAILSTORE_TX(I_job_context,
                                                                 L_control_id,
                                                                 O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_AV_INV_SQL.ST_AVAIL_INV_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END ST_AVAIL_INV_UP;
--------------------------------------------------------------------------------------------
END BDI_AV_INV_SQL;
/


