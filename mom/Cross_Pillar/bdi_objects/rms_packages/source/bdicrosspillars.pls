CREATE OR REPLACE PACKAGE BDI_CROSS_PILLAR_SQL AS
----------------------------------------------------------------------------
FUNCTION DIFF_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_control_id     IN OUT  NUMBER,
                 I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
FUNCTION DIFF_GROUP_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_control_id     IN OUT  NUMBER,
                       I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
END BDI_CROSS_PILLAR_SQL;
/


