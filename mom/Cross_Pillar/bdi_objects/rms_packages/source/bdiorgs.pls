CREATE OR REPLACE PACKAGE BDI_ORG_SQL AS
----------------------------------------------------------------------------
FUNCTION STORE_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_control_id     IN OUT  NUMBER,
                  I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
FUNCTION STORE_ADDR_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_control_id     IN OUT  NUMBER,
                       I_job_context    IN      VARCHAR2)

RETURN NUMBER;
----------------------------------------------------------------------------
END BDI_ORG_SQL;
/


