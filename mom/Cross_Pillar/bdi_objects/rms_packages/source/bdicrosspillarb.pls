CREATE OR REPLACE PACKAGE BODY BDI_CROSS_PILLAR_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION DIFF_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_control_id     IN OUT  NUMBER,
                 I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   --Call BDI package to update Control tables.
   if DIFF_FND_OUT_DATACTL.BEGINFULLSET_DIFF_FND(L_control_id,
                                                 O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into DIFF_OUT(diff_id,
                        diff_desc,
                        diff_type,
                        diff_type_desc,
                        industry_code,
                        industry_subgroup)
      select d.diff_id           DiffId,
             d.diff_desc         DiffDesc,
             dt.diff_type        DiffType,
             dt.diff_type_desc   DiffTypeDesc,
             d.industry_code     IndustryCode,
             d.industry_subgroup IndustrySubgroup
        from diff_ids d,
             diff_type dt
       where d.diff_type = dt.diff_type;

   --Call BDI package to update Control tables.
   if DIFF_FND_OUT_DATACTL.ONSUCCENDSET_DIFF_FND(I_job_context,
                                                 L_control_id,
                                                 O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_CROSS_PILLAR_SQL.DIFF_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END DIFF_UP;
-------------------------------------------------------------------------------------
FUNCTION DIFF_GROUP_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_control_id     IN OUT  NUMBER,
                       I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   --Call BDI package to update Control tables.
   if DIFFGRP_FND_OUT_DATACTL.BEGINFULLSET_DIFFGRP_FND(L_control_id,
                                                       O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   /*insert Diff Group*/
   insert into DIFF_GRP_OUT(diff_group_id,
                            diff_group_desc,
                            diff_type_id,
                            diff_type_desc)
      select dh.diff_group_id   diff_group_id,
             dh.diff_group_desc diff_group_desc,
             dh.diff_type       diff_type,
             dt.diff_type_desc  diff_type_desc
        from diff_group_head dh,
             diff_type dt
       where dh.diff_type = dt.diff_type;


   /*insert Diff Group Detail*/
   insert into DIFF_GRP_DTL_OUT(diff_group_id,
                                diff_id)
      select dd.diff_group_id,
             dd.diff_id
        from diff_group_detail dd;

   --Call BDI package to update Control tables.
   if DIFFGRP_FND_OUT_DATACTL.ONSUCCENDSET_DIFFGRP_FND(I_job_context,
                                                       L_control_id,
                                                       O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_CROSS_PILLAR_SQL.DIFF_GROUP_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END DIFF_GROUP_UP;
-------------------------------------------------------------------------------------
END BDI_CROSS_PILLAR_SQL;
/


