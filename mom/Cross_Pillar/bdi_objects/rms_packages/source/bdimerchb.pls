CREATE OR REPLACE PACKAGE BODY BDI_MERCH_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION MERCH_HIER_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_control_id     IN OUT  NUMBER,
                       I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if MERCHHIER_FND_OUT_DATACTL.BEGINFULLSET_MERCHHIER_FND(L_control_id,
                                                           O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into merch_hier_out(hierarchy_level,
                              hierarchy_node_id,
                              hierarchy_node_name,
                              parent_level,
                              parent_node_id,
                              grandparent_merch_display_id,
                              parent_merch_display_id,
                              merch_display_id,
                              purchase_type)
      select heirarchy_level,
             hierarchynodeid,
             heirarchy_name,
             parent_level,
             parent_id,
             grandparentmerchdisplayid,
             parentmerchdisplayid,
             merchdisplayid,
             purchase_type
   from (select 'DIVISION' heirarchy_level,
                d.division hierarchyNodeId,
                d.div_name heirarchy_name,
                'COMPANY'  parent_level,
                co.company parent_id,
                NULL       GrandparentMerchDisplayId,
                NULL       parentMerchDisplayId,
                d.division merchDisplayId,
                null       purchase_type
           from division d,
                comphead co
        union all
         select 'GROUP'       heirarchy_level,
                g.group_no    hierarchyNodeId,
                g.group_name  heirarchy_name,
                'DIVISION'    parent_level,
                g.division    parent_id,
                NULL          GrandparentMerchDisplayId,
                NULL          parentMerchDisplayId,
                g.group_no    merchDisplayId,
                null          purchase_type
           from groups g
        union all
         select 'DEPARTMENT'     heirarchy_level,
                dp.dept          hierarchyNodeId,
                dp.dept_name     heirarchy_name,
                'GROUP'          parent_level,
                dp.group_no      parent_id,
                NULL             GrandparentMerchDisplayId,
                NULL             parentMerchDisplayId,
                dp.dept          merchDisplayId,
                dp.purchase_type purchase_type
          from deps dp
        union all
         select 'CLASS'       heirarchy_level,
                c.class_id    hierarchyNodeId,
                c.class_name  heirarchy_name,
                'DEPARTMENT'  parent_level,
                c.dept        parent_id,
                NULL          GrandparentMerchDisplayId,
                c.dept        parentMerchDisplayId,
                c.class       merchDisplayId,
                null          purchase_type
           from class c
        union all
         select 'SUBCLASS'    heirarchy_level,
                s.subclass_id hierarchyNodeId,
                s.sub_name    heirarchy_name,
                'CLASS'       parent_level,
                c1.class_id   parent_id,
                s.dept        GrandparentMerchDisplayId,
                s.class       parentMerchDisplayId,
                s.subclass    merchDisplayId,
                null          purchase_type
           from subclass s,
                class c1
          where c1.class = s.class
            and c1.dept  = s.dept) merchhier;

   if MERCHHIER_FND_OUT_DATACTL.ONSUCCENDSET_MERCHHIER_FND(I_job_context,
                                                           L_control_id,
                                                           O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_MERCH_SQL.MERCH_HIER_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END MERCH_HIER_UP;
--------------------------------------------------------------------------------------------
END BDI_MERCH_SQL;
/


