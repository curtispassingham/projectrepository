CREATE OR REPLACE PACKAGE BODY BDI_ITEM_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION ITEM_MASTER_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_control_id     IN OUT  NUMBER,
                        I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if ITEMHDR_FND_OUT_DATACTL.BEGINFULLSET_ITEMHDR_FND(L_control_id,
                                                       O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into item_hdr_out(item,
                            item_parent,
                            item_grandparent,
                            pack_ind,
                            simple_pack_ind,
                            item_level,
                            tran_level,
                            inventory_ind,
                            diff_1_level,
                            diff_1_type,
                            diff_1,
                            diff_2_level,
                            diff_2_type,
                            diff_2,
                            diff_3_level,
                            diff_3_type,
                            diff_3,
                            diff_4_level,
                            diff_4_type,
                            diff_4,
                            dept,
                            class,
                            unique_class,
                            subclass,
                            unique_subclass,
                            status,
                            description,
                            secondary_item_desc,
                            short_desc,
                            brand_name,
                            merchandise_ind,
                            primary_ref_item_ind,
                            cost_zone_group_id,
                            standard_uom,
                            uom_conv_factor,
                            package_size,
                            package_uom,
                            store_order_multiple,
                            forecast_ind,
                            original_unit_retail,
                            currency_code,
                            mfg_rec_retail,
                            retail_label_type,
                            retail_label_value,
                            item_aggregate_ind,
                            diff_1_aggregate_ind,
                            diff_2_aggregate_ind,
                            diff_3_aggregate_ind,
                            diff_4_aggregate_ind,
                            item_number_type,
                            format_id,
                            prefix,
                            rec_handling_temp,
                            rec_handling_sens,
                            perishable_ind,
                            waste_type,
                            waste_pct,
                            default_waste_pct,
                            constant_dim_ind,
                            contains_inner_ind,
                            sellable_ind,
                            orderable_ind,
                            pack_type,
                            order_as_type,
                            item_service_level,
                            gift_wrap_ind,
                            ship_alone_ind,
                            item_form_ind,
                            catch_weight_ind,
                            catch_weight_type,
                            catch_weight_order_type,
                            catch_weight_sale_type,
                            catch_weight_uom,
                            deposit_item_type,
                            container_item,
                            deposit_in_price_per_uom,
                            soh_inquiry_at_pack_ind,
                            notional_pack_ind,
                            comments)
      select im.item,
             im.item_parent,
             im.item_grandparent,
             im.pack_ind,
             im.simple_pack_ind,
             im.item_level,
             im.tran_level,
             im.inventory_ind,
             decode( im.diff_1,NULL, im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP')),
             decode( im.diff_1,NULL, im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)),
             im.diff_1,
             decode( im.diff_2,NULL, im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP')),
             decode( im.diff_2,NULL, im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)),
             im.diff_2,
             decode( im.diff_3,NULL, im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP')),
             decode( im.diff_3,NULL, im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)),
             im.diff_3,
             decode( im.diff_4,NULL, im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP')),
             decode( im.diff_4,NULL, im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)),
             im.diff_4,
             im.dept,
             im.class,
             c.class_id,
             im.subclass,
             s.subclass_id,
             im.status,
             im.item_desc,
             im.item_desc_secondary,
             im.short_desc,
             im.brand_name,
             im.merchandise_ind,
             im.primary_ref_item_ind,
             im.cost_zone_group_id,
             im.standard_uom,
             im.uom_conv_factor,
             im.package_size,
             im.package_uom,
             im.store_ord_mult,
             im.forecast_ind,
             im.original_retail,
             so.currency_code,
             im.mfg_rec_retail,
             im.retail_label_type,
             im.retail_label_value,
             im.item_aggregate_ind,
             im.diff_1_aggregate_ind,
             im.diff_2_aggregate_ind,
             im.diff_3_aggregate_ind,
             im.diff_4_aggregate_ind,
             im.item_number_type,
             im.format_id,
             im.prefix,
             im.handling_temp,
             im.handling_sensitivity,
             im.perishable_ind,
             im.waste_type,
             im.waste_pct,
             im.default_waste_pct,
             im.const_dimen_ind,
             im.contains_inner_ind,
             im.sellable_ind,
             im.orderable_ind,
             im.pack_type,
             im.order_as_type,
             im.item_service_level,
             im.gift_wrap_ind,
             im.ship_alone_ind,
             im.item_xform_ind,
             im.catch_weight_ind,
             im.catch_weight_type,
             im.order_type,
             im.sale_type,
             im.catch_weight_uom,
             im.deposit_item_type,
             im.container_item,
             im.deposit_in_price_per_uom,
             im.soh_inquiry_at_pack_ind,
             im.notional_pack_ind,
             im.comments
       from  item_master im,
             class c,
             subclass s,
             diff_group_head dh1,
             diff_ids di1,
             diff_group_head dh2,
             diff_ids di2,
             diff_group_head dh3,
             diff_ids di3,
             diff_group_head dh4,
             diff_ids di4,
             system_options so
      where  im.status   = 'A'
        and  im.class    = c.class
        and  im.dept     = c.dept
        and  c.dept      = s.dept
        and  c.class     = s.class
        and  im.subclass = s.subclass
        and  im.diff_1   = dh1.diff_group_id(+)
        and  im.diff_1   = di1.diff_id(+)
        and  im.diff_2   = dh2.diff_group_id(+)
        and  im.diff_2   = di2.diff_id(+)
        and  im.diff_3   = dh3.diff_group_id(+)
        and  im.diff_3   = di3.diff_id(+)
        and  im.diff_4   = dh4.diff_group_id(+)
        and  im.diff_4   = di4.diff_id(+);

   if ITEMHDR_FND_OUT_DATACTL.ONSUCCENDSET_ITEMHDR_FND(I_job_context,
                                                       L_control_id,
                                                       O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_ITEM_SQL.ITEM_MASTER_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END ITEM_MASTER_UP;
--------------------------------------------------------------------------------------------
FUNCTION ITEM_LOC_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_control_id     IN OUT  NUMBER,
                     I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if ITEMLOC_FND_OUT_DATACTL.BEGINFULLSET_ITEMLOC_FND(L_control_id,
                                                       O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into item_loc_out(loc_type,
                            location,
                            item,
                            item_parent,
                            item_grandparent,
                            currency_code,
                            initial_unit_retail,
                            selling_unit_retail,
                            selling_uom,
                            taxable_ind,
                            local_item_desc,
                            local_short_desc,
                            ti,
                            hi,
                            store_order_multiple,
                            status,
                            daily_waste_pct,
                            measure_of_each,
                            measure_of_price,
                            uom_of_price,
                            primary_variant,
                            primary_cost_pack,
                            primary_supplier,
                            primary_origin_country,
                            receive_as_type,
                            inbound_handling_days,
                            source_method,
                            source_wh,
                            uin_type,
                            uin_label,
                            capture_time_in_proc,
                            ext_uin_ind,
                            intentionally_range_ind,
                            costing_location,
                            costing_loc_type,
                            launch_date,
                            qty_key_options,
                            manual_price_entry,
                            deposit_code,
                            food_stamp_ind,
                            wic_ind,
                            proportional_tare_pct,
                            fixed_tare_value,
                            fixed_tare_uom,
                            reward_eligible_ind,
                            natl_brand_comp_item,
                            return_policy,
                            stop_sale_ind,
                            elect_mtk_club,
                            report_code,
                            req_shelf_life_on_selection,
                            ib_shelf_life,
                            store_orderable_ind,
                            rack_size,
                            full_pallet_item,
                            in_store_market_basket,
                            storage_location,
                            alt_storage_location,
                            returnable_ind,
                            refundable_ind,
                            backorder_ind,
                            merchandise_ind)
      select il.loc_type,
             il.loc,
             il.item,
             il.item_parent,
             il.item_grandparent,
             loc.currency_code,
             il.unit_retail,
             il.selling_unit_retail,
             il.selling_uom,
             il.taxable_ind,
             il.local_item_desc,
             il.local_short_desc,
             il.ti,
             il.hi,
             il.store_ord_mult,
             il.status,
             il.daily_waste_pct,
             il.meas_of_each,
             il.meas_of_price,
             il.uom_of_price,
             il.primary_variant,
             il.primary_cost_pack,
             il.primary_supp,
             il.primary_cntry,
             il.receive_as_type,
             il.inbound_handling_days,
             il.source_method,
             il.source_wh,
             il.uin_type,
             il.uin_label,
             il.capture_time,
             il.ext_uin_ind,
             il.ranged_ind,
             il.costing_loc,
             il.costing_loc_type,
             ilt.launch_date,
             ilt.qty_key_options,
             ilt.manual_price_entry,
             ilt.deposit_code,
             ilt.food_stamp_ind,
             ilt.wic_ind,
             ilt.proportional_tare_pct,
             ilt.fixed_tare_value,
             ilt.fixed_tare_uom,
             ilt.reward_eligible_ind,
             ilt.natl_brand_comp_item,
             ilt.return_policy,
             ilt.stop_sale_ind,
             ilt.elect_mtk_clubs,
             ilt.report_code,
             ilt.req_shelf_life_on_selection,
             ilt.ib_shelf_life,
             ilt.store_reorderable_ind,
             ilt.rack_size,
             ilt.full_pallet_item,
             ilt.in_store_market_basket,
             ilt.storage_location,
             ilt.alt_storage_location,
             ilt.returnable_ind,
             ilt.refundable_ind,
             ilt.back_order_ind,
             im.merchandise_ind
        from item_loc il,
             item_loc_traits ilt,
             item_master im,
             (select store location,
                     currency_code currency_code
                from store
                union
              select wh location,
                     currency_code currency_code
                  from wh
                union
              select TO_NUMBER(partner_id) location,
                     currency_code
                from partner
               where partner_type = 'E') loc
       where il.item   = im.item
         and im.status = 'A'
         and  il.item  = im.item
         and  il.item  = ilt.item(+)
         and  il.loc   = ilt.loc(+)
         and  il.loc   = loc.location;

   if ITEMLOC_FND_OUT_DATACTL.ONSUCCENDSET_ITEMLOC_FND(I_job_context,
                                                       L_control_id,
                                                       O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_ITEM_SQL.ITEM_LOC_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END ITEM_LOC_UP;
--------------------------------------------------------------------------------------------
FUNCTION REL_ITEM_UP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_control_id     IN OUT  NUMBER,
                     I_job_context    IN      VARCHAR2)
   return NUMBER IS

   L_control_id NUMBER;

BEGIN

   if RELATEDITEM_FND_OUT_DATACTL.BEGINFULLSET_RELATEDITEM_FND(L_control_id,
                                                               O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   insert into related_item_out(relationship_id,
                                item,
                                relationship_name,
                                relationship_type,
                                mandatory_ind)
      select rh.relationship_id,
             rh.item,
             rh.relationship_name,
             rh.relationship_type,
             rh.mandatory_ind
        from related_item_head rh,
             item_master im
       where rh.item   = im.item
         and im.status = 'A';

   insert into related_item_dtl_out(relationship_id,
                                    related_item,
                                    priority,
                                    start_date,
                                    end_date)
      select rd.relationship_id,
             rd.related_item,
             rd.priority,
             rd.start_date,
             rd.end_date
        from related_item_detail rd,
             item_master im
       where rd.related_item = im.item
         and im.status       ='A';

   if RELATEDITEM_FND_OUT_DATACTL.ONSUCCENDSET_RELATEDITEM_FND(I_job_context,
                                                               L_control_id,
                                                               O_error_message) = 1 then
      ROLLBACK;
      return 1;
   end if;

   O_control_id := L_control_id;

   COMMIT;
   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'BDI_ITEM_SQL.REL_ITEM_UP',
                                              to_char(SQLCODE));
      ROLLBACK;
      return 1;
END REL_ITEM_UP;
--------------------------------------------------------------------------------------------
END BDI_ITEM_SQL;
/


