-- generate a script that drops BDI packages in the BDI_RMS_INT_SCHEMA environment and run the script
set pagesize 0
set feedback off
set trimspool on

prompt ;
prompt Start generating a script that drops BDI packages
prompt

spool drop_bdi_packages.lis

select 'DROP PACKAGE "'|| object_name || '";'
  from user_objects
 where OBJECT_TYPE = 'PACKAGE';

spool off
set feedback on
@drop_bdi_packages.lis

prompt
prompt The count from the SELECT below should be zero
prompt

select count(*)
  from user_objects
 where OBJECT_TYPE = 'PACKAGE';

prompt
prompt If the count is not zero, something is wrong ...
prompt

--exit
