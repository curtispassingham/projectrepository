CREATE OR REPLACE PACKAGE BODY bdi_OutDataControl AS

/******************************************************************************
* Internal datacontrol package used by BDI. The functions/procedures in this
* package are called from within the interface module outbound datacontrol 
* packages that the applications use to communicate the start and end 
* (success or error) of full/partial data load to outbound interface tables.
*******************************************************************************/
 
    FUNCTION beginDataSet (
        interfaceModule IN VARCHAR2,
        interfaceShortNames IN interfaceShortNamesType,
        dataSetType IN VARCHAR2) RETURN NUMBER IS
        
        interfaceModuleDataControlId NUMBER;
        interfaceDataControlId NUMBER;        
        interfaceSequencer VARCHAR2 (30);
        interfaceModuleSequencer VARCHAR2 (30);
        interfaceDataSetSequencer VARCHAR2(30);
        query_str VARCHAR2(500);
        beginSeqNumber NUMBER;
	queryFilter VARCHAR2(50);
        
        BEGIN
          interfaceModuleSequencer := 'BDI_DWN_IFCE_MOD_DATA_CTL_SEQ';
	  queryFilter := 'and 1=1'; 	  

	  EXECUTE IMMEDIATE 'SELECT ' || interfaceModuleSequencer || '.nextval' || ' FROM DUAL'
             INTO interfaceModuleDataControlId;
	  
          query_str := 'INSERT INTO BDI_DWNLDR_IFACE_MOD_DATA_CTL' || 
                 ' (DWNLDR_IFACE_MOD_DATA_CTL_ID, INTERFACE_MODULE, DATA_SET_TYPE) ' ||
                 ' VALUES (:interfaceModuleDataControlId, :interfaceModule, :dataSetType)';
          
          EXECUTE IMMEDIATE query_str 
                  USING interfaceModuleDataControlId, interfaceModule, dataSetType;                    

	  FOR i IN interfaceShortNames.FIRST .. interfaceShortNames.LAST LOOP
              interfaceSequencer := 'BDI_DWNLDR_IFACE_DATA_CTL_SEQ';
              interfaceDataSetSequencer := interfaceShortNames(i) || '_out_seq';
	      EXECUTE IMMEDIATE 'SELECT ' || interfaceSequencer || '.nextval' || ' FROM DUAL'
	          INTO interfaceDataControlId;
	      EXECUTE IMMEDIATE 'SELECT ' || interfaceDataSetSequencer || '.nextval' || ' FROM DUAL'
          	INTO beginSeqNumber;
              beginSeqNumber := beginSeqNumber + 1;

	      query_str := 'INSERT INTO BDI_DWNLDR_IFACE_DATA_CTL' || 
                 ' (DOWNLOADER_IFACE_DATA_CTL_ID, INTERFACE_SHORT_NAME, INTERFACE_DATA_BEGIN_SEQ_NUM, QUERY_FILTER, DWNLDR_IFACE_MOD_DATA_CTL_ID) ' ||
                 ' VALUES (:interfaceDataControlId, :interfaceShortName, :beginSeqNumber, :queryFilter, :interfaceModuleDataControlId)';
          
              EXECUTE IMMEDIATE query_str 
                  USING interfaceDataControlId, interfaceShortNames(i), beginSeqNumber, queryFilter, interfaceModuleDataControlId;            
          END LOOP;          
                
          RETURN interfaceModuleDataControlId;
    END beginDataSet;

    PROCEDURE saveDataSetJobInfo (
	interfaceModuleDataControlId IN NUMBER,
	jobContext IN VARCHAR2) IS

	position NUMBER;
	TYPE jobInfoTokenArrayType IS VARRAY(3) OF VARCHAR2(100); 
	jobInfoTokenArray jobInfoTokenArrayType;
	delimiter VARCHAR2(1);
	query_str VARCHAR2(500);
	tempJobContext VARCHAR2(200);

 	BEGIN
	  -- Split jobContext into jobName, jobInstanceId, and jobExecutionId using delimiter #
	  IF jobContext IS NULL THEN
	    RETURN;
	  END IF;

	  tempJobContext := jobContext;
	  delimiter := '#';
	  jobInfoTokenArray := jobInfoTokenArrayType();
          LOOP
	    position := instr(tempJobContext, delimiter);
	    exit when (nvl(position, 0) = 0);
	    jobInfoTokenArray.extend;
	    jobInfoTokenArray(jobInfoTokenArray.count) := substr(tempJobContext, 1, position - 1);
	    tempJobContext := substr(tempJobContext, position + length(delimiter));	    
	  END LOOP;

	  IF position = 0 AND tempJobContext IS NOT NULL THEN
      	    jobInfoTokenArray.extend;
            jobInfoTokenArray(jobInfoTokenArray.count) := tempJobContext;
    	  END IF;

	  IF jobInfoTokenArray.count = 3 THEN
	    query_str := 'INSERT INTO BDI_EXTRACTOR_EXE_DATA_SET' ||
			' (DWNLDR_IFACE_MOD_DATA_CTL_ID, JOB_NAME, JOB_INSTANCE_ID, JOB_EXECUTION_ID)' ||
			' VALUES(:dataSetid, :jobName, :jobInstanceId, :jobExecutionId)';

	    -- jobInfoTokenArray contains job name, instance id and execution id in that order
	    EXECUTE IMMEDIATE query_str 
                  USING interfaceModuleDataControlId, jobInfoTokenArray(1), jobInfoTokenArray(2), 
			jobInfoTokenArray(3);
	  END IF;

    END saveDataSetJobInfo;
    
    PROCEDURE onSuccessEndDataSet (
        interfaceModule IN VARCHAR2,
        interfaceShortNames IN interfaceShortNamesType, 
        interfaceModuleDataControlId IN NUMBER,
	jobContext IN VARCHAR2) IS
        
        interfaceDataSetSequencer VARCHAR2(30);
        query_str VARCHAR2(500);
        endSeqNumber NUMBER;
        
        BEGIN
        -- Update entries in DownloaderInterfaceModuleDataControl and DownloaderInterfaceDataControl tables
	  FOR i IN interfaceShortNames.FIRST .. interfaceShortNames.LAST LOOP
              interfaceDataSetSequencer := interfaceShortNames(i) || '_out_seq';          
              EXECUTE IMMEDIATE 'SELECT ' || interfaceDataSetSequencer || '.nextval' || ' FROM DUAL'
          	INTO endSeqNumber;
	      endSeqNumber := endSeqNumber - 1;

	      query_str := 'UPDATE BDI_DWNLDR_IFACE_DATA_CTL SET INTERFACE_DATA_END_SEQ_NUM = :endSeqNumber WHERE' || 
		' DWNLDR_IFACE_MOD_DATA_CTL_ID = :interfaceModuleDataControlId AND' ||
		' INTERFACE_SHORT_NAME = :interfaceShortName';

	      EXECUTE IMMEDIATE query_str USING endSeqNumber, interfaceModuleDataControlId, interfaceShortNames(i);
          END LOOP;    

	  query_str := 'UPDATE BDI_DWNLDR_IFACE_MOD_DATA_CTL' || 
          ' SET DATA_SET_READY_TIME = SYSDATE ' ||
          'WHERE DWNLDR_IFACE_MOD_DATA_CTL_ID = :interfaceModuleDataControlId';
          
          EXECUTE IMMEDIATE query_str USING interfaceModuleDataControlId;

	  -- Insert an entry in BDI_EXTRACTOR_EXE_DATASET table with data set id and job information
	  saveDataSetJobInfo(interfaceModuleDataControlId, jobContext);
    END onSuccessEndDataSet;
    
    PROCEDURE onErrorDiscardDataSet (
        interfaceModuleDataControlId IN NUMBER) IS        
        query_str VARCHAR2(500);
        
        BEGIN
          -- Delete entries in DownloaderInterfaceModuleDataControl and DownloaderInterfaceDataControl tables
          query_str := 'DELETE FROM BDI_DWNLDR_IFACE_DATA_CTL WHERE' || 
			' DWNLDR_IFACE_MOD_DATA_CTL_ID = :interfaceModuleDataControlId';           
          EXECUTE IMMEDIATE query_str USING interfaceModuleDataControlId;
          
          query_str := 'DELETE FROM BDI_DWNLDR_IFACE_MOD_DATA_CTL WHERE DWNLDR_IFACE_MOD_DATA_CTL_ID = :interfaceModuleDataControlId';           
          EXECUTE IMMEDIATE query_str USING interfaceModuleDataControlId;
    END onErrorDiscardDataSet;
    
END bdi_OutDataControl;
/
