/****************************************************************************************
* Interface Module : UdaValues_Fnd
* Outbound data control package
*
****************************************************************************************/

CREATE OR REPLACE PACKAGE BODY UdaValues_Fnd_Out_DataCtl AS
    
    interfaceShortNames bdi_OutDataControl.interfaceShortNamesType := 
        bdi_OutDataControl.interfaceShortNamesType('Uda_Values');

/****************************************************************************************
 *
 * Function    : beginFullSet_UdaValues_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in UdaValues_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_UdaValues_Fnd or onErrDiscardSet_UdaValues_Fnd
 *    
 ****************************************************************************************/

    FUNCTION beginFullSet_UdaValues_Fnd(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;	
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('UdaValues_Fnd', interfaceShortNames, 'FULL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'UdaValues_Fnd_Out_DataCtl.beginFullSet_UdaValues_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginFullSet_UdaValues_Fnd;

/****************************************************************************************
 *
 * Function    : beginPartialSet_UdaValues_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in UdaValues_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_UdaValues_Fnd or onErrDiscardSet_UdaValues_Fnd
 *    
 ****************************************************************************************/
    
    FUNCTION beginPartialSet_UdaValues_Fnd(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;      
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('UdaValues_Fnd', interfaceShortNames, 'PARTIAL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'UdaValues_Fnd_Out_DataCtl.beginPartialSet_UdaValues_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginPartialSet_UdaValues_Fnd;

/****************************************************************************************
 *
 * Function    	: onSuccEndSet_UdaValues_Fnd
 * Description 	: Function to be called by the application on successful completion
 *		  of data load to interface tables in UdaValues_Fnd module.
 *		  The function returns 0 on successful completion. In case of an error,
 *		  the function returns 1 with error message in the out parameter O_error_message.
 * Input        : I_job_context, I_datacontrol_id
 *  
 ****************************************************************************************/
    
    FUNCTION onSuccEndSet_UdaValues_Fnd(I_job_context IN VARCHAR2, I_datacontrol_id IN NUMBER,
						O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onSuccessEndDataSet('UdaValues_Fnd', interfaceShortNames, I_datacontrol_id, I_job_context);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'UdaValues_Fnd_Out_DataCtl.onSuccEndSet_UdaValues_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onSuccEndSet_UdaValues_Fnd;

/****************************************************************************************
 *
 * Function    : onErrDiscardSet_UdaValues_Fnd
 * Description : Function to be called by the application in case of any error
 *		 in data load to interface tables in UdaValues_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.
 * Input       : I_datacontrol_id
 *    
 ****************************************************************************************/
    
    FUNCTION onErrDiscardSet_UdaValues_Fnd(I_datacontrol_id IN NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onErrorDiscardDataSet(I_datacontrol_id);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'UdaValues_Fnd_Out_DataCtl.onErrDiscardSet_UdaValues_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onErrDiscardSet_UdaValues_Fnd;

END UdaValues_Fnd_Out_DataCtl;
/
