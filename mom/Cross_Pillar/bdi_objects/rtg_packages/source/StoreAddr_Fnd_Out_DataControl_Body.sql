/****************************************************************************************
* Interface Module : StoreAddr_Fnd
* Outbound data control package
*
****************************************************************************************/

CREATE OR REPLACE PACKAGE BODY StoreAddr_Fnd_Out_DataCtl AS
    
    interfaceShortNames bdi_OutDataControl.interfaceShortNamesType := 
        bdi_OutDataControl.interfaceShortNamesType('Store_Addr');

/****************************************************************************************
 *
 * Function    : beginFullSet_StoreAddr_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in StoreAddr_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_StoreAddr_Fnd or onErrDiscardSet_StoreAddr_Fnd
 *    
 ****************************************************************************************/

    FUNCTION beginFullSet_StoreAddr_Fnd(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;	
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('StoreAddr_Fnd', interfaceShortNames, 'FULL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'StoreAddr_Fnd_Out_DataCtl.beginFullSet_StoreAddr_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginFullSet_StoreAddr_Fnd;

/****************************************************************************************
 *
 * Function    : beginPartialSet_StoreAddr_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in StoreAddr_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_StoreAddr_Fnd or onErrDiscardSet_StoreAddr_Fnd
 *    
 ****************************************************************************************/
    
    FUNCTION beginPartialSet_StoreAddr_Fnd(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;      
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('StoreAddr_Fnd', interfaceShortNames, 'PARTIAL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'StoreAddr_Fnd_Out_DataCtl.beginPartialSet_StoreAddr_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginPartialSet_StoreAddr_Fnd;

/****************************************************************************************
 *
 * Function    	: onSuccEndSet_StoreAddr_Fnd
 * Description 	: Function to be called by the application on successful completion
 *		  of data load to interface tables in StoreAddr_Fnd module.
 *		  The function returns 0 on successful completion. In case of an error,
 *		  the function returns 1 with error message in the out parameter O_error_message.
 * Input        : I_job_context, I_datacontrol_id
 *  
 ****************************************************************************************/
    
    FUNCTION onSuccEndSet_StoreAddr_Fnd(I_job_context IN VARCHAR2, I_datacontrol_id IN NUMBER,
						O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onSuccessEndDataSet('StoreAddr_Fnd', interfaceShortNames, I_datacontrol_id, I_job_context);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'StoreAddr_Fnd_Out_DataCtl.onSuccEndSet_StoreAddr_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onSuccEndSet_StoreAddr_Fnd;

/****************************************************************************************
 *
 * Function    : onErrDiscardSet_StoreAddr_Fnd
 * Description : Function to be called by the application in case of any error
 *		 in data load to interface tables in StoreAddr_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.
 * Input       : I_datacontrol_id
 *    
 ****************************************************************************************/
    
    FUNCTION onErrDiscardSet_StoreAddr_Fnd(I_datacontrol_id IN NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onErrorDiscardDataSet(I_datacontrol_id);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'StoreAddr_Fnd_Out_DataCtl.onErrDiscardSet_StoreAddr_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onErrDiscardSet_StoreAddr_Fnd;

END StoreAddr_Fnd_Out_DataCtl;
/
