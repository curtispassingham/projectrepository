/****************************************************************************************
* Interface Module : ItSupManCtry_Fnd
* Outbound data control package
*
****************************************************************************************/

CREATE OR REPLACE PACKAGE BODY ItSupManCtry_Fnd_Out_DataCtl AS
    
    interfaceShortNames bdi_OutDataControl.interfaceShortNamesType := 
        bdi_OutDataControl.interfaceShortNamesType('Item_Sup_Man_Cty');

/****************************************************************************************
 *
 * Function    : beginFullSet_ItSupManCtry_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in ItSupManCtry_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_ItSupManCtry_Fnd or onErrDiscardSet_ItSupManCtry_F
 *    
 ****************************************************************************************/

    FUNCTION beginFullSet_ItSupManCtry_Fnd(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;	
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('ItSupManCtry_Fnd', interfaceShortNames, 'FULL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'ItSupManCtry_Fnd_Out_DataCtl.beginFullSet_ItSupManCtry_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginFullSet_ItSupManCtry_Fnd;

/****************************************************************************************
 *
 * Function    : beginPartialSet_ItSupManCtry_F
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in ItSupManCtry_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_ItSupManCtry_Fnd or onErrDiscardSet_ItSupManCtry_F
 *    
 ****************************************************************************************/
    
    FUNCTION beginPartialSet_ItSupManCtry_F(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;      
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('ItSupManCtry_Fnd', interfaceShortNames, 'PARTIAL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'ItSupManCtry_Fnd_Out_DataCtl.beginPartialSet_ItSupManCtry_F : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginPartialSet_ItSupManCtry_F;

/****************************************************************************************
 *
 * Function    	: onSuccEndSet_ItSupManCtry_Fnd
 * Description 	: Function to be called by the application on successful completion
 *		  of data load to interface tables in ItSupManCtry_Fnd module.
 *		  The function returns 0 on successful completion. In case of an error,
 *		  the function returns 1 with error message in the out parameter O_error_message.
 * Input        : I_job_context, I_datacontrol_id
 *  
 ****************************************************************************************/
    
    FUNCTION onSuccEndSet_ItSupManCtry_Fnd(I_job_context IN VARCHAR2, I_datacontrol_id IN NUMBER,
						O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onSuccessEndDataSet('ItSupManCtry_Fnd', interfaceShortNames, I_datacontrol_id, I_job_context);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'ItSupManCtry_Fnd_Out_DataCtl.onSuccEndSet_ItSupManCtry_Fnd : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onSuccEndSet_ItSupManCtry_Fnd;

/****************************************************************************************
 *
 * Function    : onErrDiscardSet_ItSupManCtry_F
 * Description : Function to be called by the application in case of any error
 *		 in data load to interface tables in ItSupManCtry_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.
 * Input       : I_datacontrol_id
 *    
 ****************************************************************************************/
    
    FUNCTION onErrDiscardSet_ItSupManCtry_F(I_datacontrol_id IN NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onErrorDiscardDataSet(I_datacontrol_id);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'ItSupManCtry_Fnd_Out_DataCtl.onErrDiscardSet_ItSupManCtry_F : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onErrDiscardSet_ItSupManCtry_F;

END ItSupManCtry_Fnd_Out_DataCtl;
/
