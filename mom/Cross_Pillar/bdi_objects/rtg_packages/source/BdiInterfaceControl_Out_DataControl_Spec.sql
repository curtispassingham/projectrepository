/******************************************************************************
* Internal datacontrol package used by BDI. The functions/procedures in this
* package are called from within the interface module outbound datacontrol 
* packages that the applications use to communicate the start and end 
* (success or error) of full/partial data load to interface tables.
* 
*******************************************************************************/

CREATE OR REPLACE PACKAGE bdi_OutDataControl AS
    
    TYPE interfaceShortNamesType IS TABLE OF VARCHAR2(30);
	
    FUNCTION beginDataSet (
        interfaceModule IN VARCHAR2,
        interfaceShortNames IN interfaceShortNamesType,
        dataSetType IN VARCHAR2) RETURN NUMBER;
    
    PROCEDURE onSuccessEndDataSet (
        interfaceModule IN VARCHAR2,
        interfaceShortNames IN interfaceShortNamesType, 
        interfaceModuleDataControlId IN NUMBER,
	jobContext IN VARCHAR2);
    
    PROCEDURE onErrorDiscardDataSet (
        interfaceModuleDataControlId IN NUMBER);
    
END bdi_OutDataControl;
/
