/****************************************************************************************
* Interface Module : ItemSupplier_Fnd
* Outbound data control package specification
*
****************************************************************************************/

CREATE OR REPLACE PACKAGE ItemSupplier_Fnd_Out_DataCtl AS

/****************************************************************************************
 *
 * Function    : beginFullSet_ItemSupplier_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in ItemSupplier_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_ItemSupplier_Fnd or onErrDiscardSet_ItemSupplier_F
 *    
 ****************************************************************************************/

    FUNCTION beginFullSet_ItemSupplier_Fnd(O_datacontrol_id OUT NUMBER, 
				O_error_message OUT VARCHAR2) RETURN NUMBER;

/****************************************************************************************
 *
 * Function    : beginPartialSet_ItemSupplier_F
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in ItemSupplier_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_ItemSupplier_Fnd or onErrDiscardSet_ItemSupplier_F
 *    
 ****************************************************************************************/
    
    FUNCTION beginPartialSet_ItemSupplier_F(O_datacontrol_id OUT NUMBER, 
				   O_error_message OUT VARCHAR2) RETURN NUMBER;

/****************************************************************************************
 *
 * Function    	: onSuccEndSet_ItemSupplier_Fnd
 * Description 	: Function to be called by the application on successful completion
 *		  of data load to interface tables in ItemSupplier_Fnd module.
 *		  The function returns 0 on successful completion. In case of an error,
 *		  the function returns 1 with error message in the out parameter O_error_message.
 * Input        : I_job_context, I_datacontrol_id
 *  
 ****************************************************************************************/
    
    FUNCTION onSuccEndSet_ItemSupplier_Fnd(I_job_context VARCHAR2, 
			       I_datacontrol_id IN NUMBER, 
			       O_error_message OUT VARCHAR2) RETURN NUMBER;

/****************************************************************************************
 *
 * Function    : onErrDiscardSet_ItemSupplier_F
 * Description : Function to be called by the application in case of any error
 *		 in data load to interface tables in ItemSupplier_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.
 * Input       : I_datacontrol_id
 *    
 ****************************************************************************************/
    
    FUNCTION onErrDiscardSet_ItemSupplier_F(I_datacontrol_id IN NUMBER, 
			          O_error_message OUT VARCHAR2) RETURN NUMBER;
    
END ItemSupplier_Fnd_Out_DataCtl;
/
