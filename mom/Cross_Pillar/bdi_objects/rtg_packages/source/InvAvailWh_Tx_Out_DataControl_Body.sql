/****************************************************************************************
* Interface Module : InvAvailWh_Tx
* Outbound data control package
*
****************************************************************************************/

CREATE OR REPLACE PACKAGE BODY InvAvailWh_Tx_Out_DataCtl AS
    
    interfaceShortNames bdi_OutDataControl.interfaceShortNamesType := 
        bdi_OutDataControl.interfaceShortNamesType('Inv_Avail_Wh');

/****************************************************************************************
 *
 * Function    : beginFullSet_InvAvailWh_Tx
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in InvAvailWh_Tx module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_InvAvailWh_Tx or onErrDiscardSet_InvAvailWh_Tx
 *    
 ****************************************************************************************/

    FUNCTION beginFullSet_InvAvailWh_Tx(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;	
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('InvAvailWh_Tx', interfaceShortNames, 'FULL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'InvAvailWh_Tx_Out_DataCtl.beginFullSet_InvAvailWh_Tx : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginFullSet_InvAvailWh_Tx;

/****************************************************************************************
 *
 * Function    : beginPartialSet_InvAvailWh_Tx
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in InvAvailWh_Tx module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_InvAvailWh_Tx or onErrDiscardSet_InvAvailWh_Tx
 *    
 ****************************************************************************************/
    
    FUNCTION beginPartialSet_InvAvailWh_Tx(O_datacontrol_id OUT NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        interfaceModuleDataControlId NUMBER;      
        BEGIN
          interfaceModuleDataControlId := 
            bdi_OutDataControl.beginDataSet('InvAvailWh_Tx', interfaceShortNames, 'PARTIAL');
          O_datacontrol_id := interfaceModuleDataControlId;
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'InvAvailWh_Tx_Out_DataCtl.beginPartialSet_InvAvailWh_Tx : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END beginPartialSet_InvAvailWh_Tx;

/****************************************************************************************
 *
 * Function    	: onSuccEndSet_InvAvailWh_Tx
 * Description 	: Function to be called by the application on successful completion
 *		  of data load to interface tables in InvAvailWh_Tx module.
 *		  The function returns 0 on successful completion. In case of an error,
 *		  the function returns 1 with error message in the out parameter O_error_message.
 * Input        : I_job_context, I_datacontrol_id
 *  
 ****************************************************************************************/
    
    FUNCTION onSuccEndSet_InvAvailWh_Tx(I_job_context IN VARCHAR2, I_datacontrol_id IN NUMBER,
						O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onSuccessEndDataSet('InvAvailWh_Tx', interfaceShortNames, I_datacontrol_id, I_job_context);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'InvAvailWh_Tx_Out_DataCtl.onSuccEndSet_InvAvailWh_Tx : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onSuccEndSet_InvAvailWh_Tx;

/****************************************************************************************
 *
 * Function    : onErrDiscardSet_InvAvailWh_Tx
 * Description : Function to be called by the application in case of any error
 *		 in data load to interface tables in InvAvailWh_Tx module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.
 * Input       : I_datacontrol_id
 *    
 ****************************************************************************************/
    
    FUNCTION onErrDiscardSet_InvAvailWh_Tx(I_datacontrol_id IN NUMBER, O_error_message OUT VARCHAR2)
    RETURN NUMBER IS
        BEGIN
          bdi_OutDataControl.onErrorDiscardDataSet(I_datacontrol_id);
	  RETURN 0;

	  EXCEPTION
	   WHEN OTHERS THEN
        	O_error_message := 'PACKAGE_ERROR: ' || 'InvAvailWh_Tx_Out_DataCtl.onErrDiscardSet_InvAvailWh_Tx : '  ||
                                     to_char(SQLCODE) || ' ' || SQLERRM;
   		RETURN 1;
    END onErrDiscardSet_InvAvailWh_Tx;

END InvAvailWh_Tx_Out_DataCtl;
/
