/****************************************************************************************
* Interface Module : CodeDetail_Fnd
* Outbound data control package specification
*
****************************************************************************************/

CREATE OR REPLACE PACKAGE CodeDetail_Fnd_Out_DataCtl AS

/****************************************************************************************
 *
 * Function    : beginFullSet_CodeDetail_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in CodeDetail_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_CodeDetail_Fnd or onErrDiscardSet_CodeDetail_Fnd
 *    
 ****************************************************************************************/

    FUNCTION beginFullSet_CodeDetail_Fnd(O_datacontrol_id OUT NUMBER, 
				O_error_message OUT VARCHAR2) RETURN NUMBER;

/****************************************************************************************
 *
 * Function    : beginPartialSet_CodeDetail_Fnd
 * Description : Function to be called by the application at the start of full data load
 *		 to interface tables in CodeDetail_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.		 
 *		 The out parameter O_datacontrol_id returns interfaceModuleDataControlId that 
 * 		 the application needs to pass as input when calling the procedure -  
 * 		 onSuccEndSet_CodeDetail_Fnd or onErrDiscardSet_CodeDetail_Fnd
 *    
 ****************************************************************************************/
    
    FUNCTION beginPartialSet_CodeDetail_Fnd(O_datacontrol_id OUT NUMBER, 
				   O_error_message OUT VARCHAR2) RETURN NUMBER;

/****************************************************************************************
 *
 * Function    	: onSuccEndSet_CodeDetail_Fnd
 * Description 	: Function to be called by the application on successful completion
 *		  of data load to interface tables in CodeDetail_Fnd module.
 *		  The function returns 0 on successful completion. In case of an error,
 *		  the function returns 1 with error message in the out parameter O_error_message.
 * Input        : I_job_context, I_datacontrol_id
 *  
 ****************************************************************************************/
    
    FUNCTION onSuccEndSet_CodeDetail_Fnd(I_job_context VARCHAR2, 
			       I_datacontrol_id IN NUMBER, 
			       O_error_message OUT VARCHAR2) RETURN NUMBER;

/****************************************************************************************
 *
 * Function    : onErrDiscardSet_CodeDetail_Fnd
 * Description : Function to be called by the application in case of any error
 *		 in data load to interface tables in CodeDetail_Fnd module.
 *		 The function returns 0 on successful completion. In case of an error,
 *		 the function returns 1 with error message in the out parameter O_error_message.
 * Input       : I_datacontrol_id
 *    
 ****************************************************************************************/
    
    FUNCTION onErrDiscardSet_CodeDetail_Fnd(I_datacontrol_id IN NUMBER, 
			          O_error_message OUT VARCHAR2) RETURN NUMBER;
    
END CodeDetail_Fnd_Out_DataCtl;
/
