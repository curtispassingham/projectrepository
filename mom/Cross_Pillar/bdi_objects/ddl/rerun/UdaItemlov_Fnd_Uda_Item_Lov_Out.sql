-- Interface Module: UdaItemlov_Fnd
-- Interface: Uda_Item_Lov_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE uda_item_lov_out_seq CACHE 10000;


-- Create BDI Outbound Table: UDA_ITEM_LOV_OUT
-- This table is used to integrate Uda Item LOV information.

CREATE TABLE UDA_ITEM_LOV_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT uda_item_lov_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- This field contains unique alphanumeric identifier for the item.
  item VARCHAR2(25) NOT NULL,
  -- This field contains a number uniquely identifying the User-Defined Attribute.
  uda_id NUMBER(5,0) NOT NULL,
  -- This field contains value of the Used Defined attribute for the item.
  uda_value NUMBER(5,0) NOT NULL
);

COMMENT ON TABLE UDA_ITEM_LOV_OUT IS 'This table is used to integrate Uda Item LOV information.';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.item IS 'This field contains unique alphanumeric identifier for the item.';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.uda_id IS 'This field contains a number uniquely identifying the User-Defined Attribute.';
COMMENT ON COLUMN UDA_ITEM_LOV_OUT.uda_value IS 'This field contains value of the Used Defined attribute for the item.';


-- Add BDI primary key constraint

ALTER TABLE UDA_ITEM_LOV_OUT ADD CONSTRAINT pk_uda_item_lov_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE UDA_ITEM_LOV_OUT ADD CONSTRAINT chk_type_uda_item_lov_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE UDA_ITEM_LOV_OUT ADD CONSTRAINT chk_actn_uda_item_lov_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
