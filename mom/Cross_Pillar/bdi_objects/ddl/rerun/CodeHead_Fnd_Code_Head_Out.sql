-- Interface Module: CodeHead_Fnd
-- Interface: Code_Head_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE code_head_out_seq CACHE 10000;


-- Create BDI Outbound Table: CODE_HEAD_OUT
-- This table is used to integrate Code Head information.

CREATE TABLE CODE_HEAD_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT code_head_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- This field contains the code type which will serve as a grouping mechanism for the multiple codes stored on the CODE_DETAIL table.
  code_type VARCHAR2(4) NOT NULL,
  -- This field will contain the description of the code type.
  code_type_desc VARCHAR2(120) NOT NULL
);

COMMENT ON TABLE CODE_HEAD_OUT IS 'This table is used to integrate Code Head information.';
COMMENT ON COLUMN CODE_HEAD_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN CODE_HEAD_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN CODE_HEAD_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN CODE_HEAD_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN CODE_HEAD_OUT.code_type IS 'This field contains the code type which will serve as a grouping mechanism for the multiple codes stored on the CODE_DETAIL table.';
COMMENT ON COLUMN CODE_HEAD_OUT.code_type_desc IS 'This field will contain the description of the code type.';


-- Add BDI primary key constraint

ALTER TABLE CODE_HEAD_OUT ADD CONSTRAINT pk_code_head_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE CODE_HEAD_OUT ADD CONSTRAINT chk_type_code_head_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE CODE_HEAD_OUT ADD CONSTRAINT chk_actn_code_head_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
