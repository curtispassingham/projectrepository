-- Interface Module: ItemSuppUom_Fnd
-- Interface: Item_Supp_Uom_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE item_supp_uom_out_seq CACHE 10000;


-- Create BDI Outbound Table: ITEM_SUPP_UOM_OUT
-- This table is used to integrate Item Supplier Unit of Measure information.

CREATE TABLE ITEM_SUPP_UOM_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT item_supp_uom_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- Alphanumeric value that identifies the item.
  item VARCHAR2(25) NOT NULL,
  -- The unique identifier for the supplier of the item.
  supplier NUMBER(10,0) NOT NULL,
  -- The unit of measure that the item is being measured in.
  uom VARCHAR2(4) NOT NULL,
  -- This field will store the equivalent value of the Item/Suppliers shipping carton in the associated unit of measure.
  value NUMBER(20,4) NOT NULL
);

COMMENT ON TABLE ITEM_SUPP_UOM_OUT IS 'This table is used to integrate Item Supplier Unit of Measure information.';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.item IS 'Alphanumeric value that identifies the item.';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.supplier IS 'The unique identifier for the supplier of the item.';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.uom IS 'The unit of measure that the item is being measured in.';
COMMENT ON COLUMN ITEM_SUPP_UOM_OUT.value IS 'This field will store the equivalent value of the Item/Suppliers shipping carton in the associated unit of measure.';


-- Add BDI primary key constraint

ALTER TABLE ITEM_SUPP_UOM_OUT ADD CONSTRAINT pk_item_supp_uom_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE ITEM_SUPP_UOM_OUT ADD CONSTRAINT chk_type_item_supp_uom_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE ITEM_SUPP_UOM_OUT ADD CONSTRAINT chk_actn_item_supp_uom_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
