-- Interface Module: RelatedItem_Fnd
-- Interface: Related_Item_Dtl_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE related_item_dtl_out_seq CACHE 10000;


-- Create BDI Outbound Table: RELATED_ITEM_DTL_OUT
-- This table is used to integrate related item details.  Related item details define the type of relationship and the related item. 

CREATE TABLE RELATED_ITEM_DTL_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT related_item_dtl_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- Unique identifier for each relationship header.  Data will always exist in this field.
  relationship_id NUMBER(20,0) NOT NULL,
  -- Item id of the related item.  This is the item that should be Cross Sold, Substituted, or Up Sold when the item on the parent record is sold.
  related_item VARCHAR2(25) NOT NULL,
  -- Applicable only in case of relationship type SUBS. In case of multiple related substitute items, this column could be used (optional) to define relative priority.
  priority NUMBER(4,0),
  -- From this date related item can be used on transactions.
  start_date TIMESTAMP,
  -- Till this date related item can be used on transactions. A value of null means that it is effective forever.
  end_date TIMESTAMP
);

COMMENT ON TABLE RELATED_ITEM_DTL_OUT IS 'This table is used to integrate related item details.  Related item details define the type of relationship and the related item. ';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.relationship_id IS 'Unique identifier for each relationship header.  Data will always exist in this field.';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.related_item IS 'Item id of the related item.  This is the item that should be Cross Sold, Substituted, or Up Sold when the item on the parent record is sold.';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.priority IS 'Applicable only in case of relationship type SUBS. In case of multiple related substitute items, this column could be used (optional) to define relative priority.';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.start_date IS 'From this date related item can be used on transactions.';
COMMENT ON COLUMN RELATED_ITEM_DTL_OUT.end_date IS 'Till this date related item can be used on transactions. A value of null means that it is effective forever.';


-- Add BDI primary key constraint

ALTER TABLE RELATED_ITEM_DTL_OUT ADD CONSTRAINT pk_related_item_dtl_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE RELATED_ITEM_DTL_OUT ADD CONSTRAINT chk_type_related_item_dtl_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE RELATED_ITEM_DTL_OUT ADD CONSTRAINT chk_actn_related_item_dtl_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
