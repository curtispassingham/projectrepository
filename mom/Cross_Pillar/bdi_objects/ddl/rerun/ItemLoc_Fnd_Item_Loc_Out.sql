-- Interface Module: ItemLoc_Fnd
-- Interface: Item_Loc_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE item_loc_out_seq CACHE 10000;


-- Create BDI Outbound Table: ITEM_LOC_OUT
-- This table is used to integrate item/location combinations.

CREATE TABLE ITEM_LOC_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT item_loc_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- Describes the type of location.  Valid values include S (store), W (warehouse) and E (external finisher).  Data will always be present in this field.
  loc_type VARCHAR2(1) NOT NULL,
  -- Numeric ID of location.  The intersection of location and item is a distinct entity.  Data will always be present in this field.
  location NUMBER(10,0) NOT NULL,
  -- ID of item.  The intersection of location and item is a distinct entity.  Data will always be present in this field.
  item VARCHAR2(25) NOT NULL,
  -- ID identifies the item/group at the level above the item. This value must exist as an item in another row on the item_master table.
  item_parent VARCHAR2(25),
  -- identifies the item/group two levels above the item. This value must exist as both an item and an item parent in another row on the item_master table.
  item_grandparent VARCHAR2(25),
  -- This field contains the currency code under which the store/wh operates.
  currency_code VARCHAR2(3),
  -- Contains the unit retail price in the standard unit of measure for the item/location combination. This field is stored in the local currency.
  initial_unit_retail NUMBER(20,4),
  -- Contains the unit retail price in the selling unit of measure for the item/location combination. This field is stored in the local currency.
  selling_unit_retail NUMBER(20,4),
  -- Contains the selling unit of measure for an items single-unit retail.
  selling_uom VARCHAR2(4),
  -- Indicates if item is taxable at the store.
  taxable_ind VARCHAR2(1),
  -- Contains the local description of the item.  This may be the same as the primary description of the item, a regional description of the item (e.g. jimmies vs sprinkles in the US or roll vs bap vs cob vs bun in the UK), or a value in a local language (e.g. Overlay dress true black knit at US stores vs Lagenkleid - Strick, tiefschwarz at stores in Germany).  The intent is that this string is appropriate to print description on signage/receipts at this location.
  local_item_desc VARCHAR2(250) NOT NULL,
  -- Contains the local short description of the item.
  local_short_desc VARCHAR2(120),
  -- Number of shipping units (cases) that make up one tier of a pallet.
  ti NUMBER(12,4),
  -- Number of tiers that make up a complete pallet (height).
  hi NUMBER(12,4),
  -- This column contains the multiple in which the item needs to be shipped from a warehouse to the location.
  store_order_multiple VARCHAR2(1),
  -- Current status of item at the store.
  status VARCHAR2(1),
  -- Average percentage lost from inventory on a daily basis due to natural wastage.
  daily_waste_pct NUMBER(12,4),
  -- Size of an each in terms of the uom_of_price. For example 12 oz. Used in ticketing.
  measure_of_each NUMBER(12,4),
  -- Size to be used on the ticket in terms of the uom_of_price.
  measure_of_price NUMBER(12,4),
  -- Unit of measure that will be used on the ticket for this item.
  uom_of_price VARCHAR2(4),
  -- This field is used to address sales of PLUs (i.e. above transaction level items) when inventory is tracked at a lower level (i.e. UPC). This field will only contain a value for items one level higher than the transaction level.
  primary_variant VARCHAR2(25),
  -- This field contains an item number that is a simple pack containing the item in the item column for this record.
  primary_cost_pack VARCHAR2(25),
  -- Numeric identifier of the supplier who will be considered the primary supplier for the specified item/loc.
  primary_supplier NUMBER(10,0),
  -- Contains the identifier of the origin country which will be considered the primary country for the specified item/location.
  primary_origin_country VARCHAR2(3),
  -- This column determines whether the stock on hand for a pack component item or the buyer pack itself will be updated when a buyer pack is received at a warehouse.
  receive_as_type VARCHAR2(2),
  -- This field indicates the number of inbound handling days for an item at a warehouse type location.
  inbound_handling_days NUMBER(2,0),
  -- This value will be used to specify how the adhoc PO/TSF creation process should source the item/location request.
  source_method VARCHAR2(1),
  -- This value will be used by the ad-hoc PO/Transfer creation process to determine which warehouse to fill the stores request from.
  source_wh NUMBER(10,0),
  -- This column will contain the unique identification number (UIN) used to identify the instances of the item at the location.
  uin_type VARCHAR2(6),
  -- This column will contain the label for the UIN when displayed in SIM.
  uin_label VARCHAR2(6),
  -- This column will indicate when the UIN should be captured for an item during transaction processing.
  capture_time_in_proc VARCHAR2(6),
  -- EXT_UIN_IND This Yes/No indicator indicates if UIN is being generated in the external system.
  ext_uin_ind VARCHAR2(1),
  -- This column determines if the location is ranged intentionally by the user for replenishment/selling or incidentally ranged by the RMS programs when item is not ranged to a specific location on the transaction.
  intentionally_range_ind VARCHAR2(1),
  -- Numeric identifier of the costing location for the franchise store. This field may contain a store or a warehouse.
  costing_location NUMBER(10,0),
  -- This field holds the type of costing location in the costing location field.
  costing_loc_type VARCHAR2(1),
  -- Holds the date that they item should first be sold at the location.
  launch_date TIMESTAMP,
  -- Determines whether the qty key on a POS should be used for this item at the location.
  qty_key_options VARCHAR2(6),
  -- Determines whether the price can/should be entered manually on a POS for this item at the location.
  manual_price_entry VARCHAR2(6),
  -- Indicates whether a deposit is associated with this item at the location.
  deposit_code VARCHAR2(6),
  -- Indicates whether the item is approved for food stamps at the location. This value will be downloaded to the POS.
  food_stamp_ind VARCHAR2(1),
  -- Indicates whether the item is approved for WIC at the location. This value will be downloaded to the POS.
  wic_ind VARCHAR2(1),
  -- Holds the value associated of the packaging in items sold by weight at the location.
  proportional_tare_pct NUMBER(12,4),
  -- Holds the value associated of the packaging in items sold by weight at the location.
  fixed_tare_value NUMBER(12,4),
  -- Holds the unit of measure value associated with the tare value. The only processing RMS does involving the fixed tare value and UOM is downloading it to the POS.
  fixed_tare_uom VARCHAR2(4),
  -- Holds whether the item is legally valid for various types of bonus point/award programs at the location.
  reward_eligible_ind VARCHAR2(1),
  -- Holds the nationally branded item to which you would like to compare the current item.
  natl_brand_comp_item VARCHAR2(25),
  -- Holds the return policy for the item at the location. Valid values for this field belong to the code_type RETP.
  return_policy VARCHAR2(6),
  -- Indicates that sale of the item should be stopped immediately at the location (i.e. in case of recall etc).
  stop_sale_ind VARCHAR2(1),
  -- Holds the code that represents the marketing clubs to which the item belongs at the location.
  elect_mtk_club VARCHAR2(6),
  -- Code to determine which reports the location should run.
  report_code VARCHAR2(6),
  -- Holds the required shelf life for an item on receipt in days.
  req_shelf_life_on_selection NUMBER(4,0),
  -- This column will hold the Investment Buyspecific shelf life for the item/location
  ib_shelf_life NUMBER(4,0),
  -- STORE_REORDERABLE_IND Indicates whether the store may re-order the item. This field is required to be either= Y - yes or N - no. The field will default to N. No RMS processing is based on the value in this field.
  store_orderable_ind VARCHAR2(1),
  -- Indicates the rack size that should be used for the item. This field is not required. Valid values for the field can be found and defined in the code_type RACK.
  rack_size VARCHAR2(6),
  -- Indicates whether a store must reorder an item in full pallets only.
  full_pallet_item VARCHAR2(1),
  -- Holds the in store market basket code for this item/location combination. Valid values for the field can be found in the code_type STMB.
  in_store_market_basket VARCHAR2(6),
  -- Holds the current storage location or bin number for the item at the location. No RMS processing is based on the value in this field.
  storage_location VARCHAR2(7),
  -- Holds the preferred alternate storage location or bin number for the item at the location.
  alt_storage_location VARCHAR2(7),
  -- This field will contain a value of Yes when the item can be returned to the location
  returnable_ind VARCHAR2(1),
  -- This field will contain a value of Yes when the item is refundable at the location.
  refundable_ind VARCHAR2(1),
  -- This field will contain a value of Yes when the item can be back ordered to the location
  backorder_ind VARCHAR2(1),
  -- Indicates if the item is a merchandise item (Y, N).
  merchandise_ind VARCHAR2(1)
);

COMMENT ON TABLE ITEM_LOC_OUT IS 'This table is used to integrate item/location combinations.';
COMMENT ON COLUMN ITEM_LOC_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN ITEM_LOC_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN ITEM_LOC_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN ITEM_LOC_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN ITEM_LOC_OUT.loc_type IS 'Describes the type of location.  Valid values include S (store), W (warehouse) and E (external finisher).  Data will always be present in this field.';
COMMENT ON COLUMN ITEM_LOC_OUT.location IS 'Numeric ID of location.  The intersection of location and item is a distinct entity.  Data will always be present in this field.';
COMMENT ON COLUMN ITEM_LOC_OUT.item IS 'ID of item.  The intersection of location and item is a distinct entity.  Data will always be present in this field.';
COMMENT ON COLUMN ITEM_LOC_OUT.item_parent IS 'ID identifies the item/group at the level above the item. This value must exist as an item in another row on the item_master table.';
COMMENT ON COLUMN ITEM_LOC_OUT.item_grandparent IS 'identifies the item/group two levels above the item. This value must exist as both an item and an item parent in another row on the item_master table.';
COMMENT ON COLUMN ITEM_LOC_OUT.currency_code IS 'This field contains the currency code under which the store/wh operates.';
COMMENT ON COLUMN ITEM_LOC_OUT.initial_unit_retail IS 'Contains the unit retail price in the standard unit of measure for the item/location combination. This field is stored in the local currency.';
COMMENT ON COLUMN ITEM_LOC_OUT.selling_unit_retail IS 'Contains the unit retail price in the selling unit of measure for the item/location combination. This field is stored in the local currency.';
COMMENT ON COLUMN ITEM_LOC_OUT.selling_uom IS 'Contains the selling unit of measure for an items single-unit retail.';
COMMENT ON COLUMN ITEM_LOC_OUT.taxable_ind IS 'Indicates if item is taxable at the store.';
COMMENT ON COLUMN ITEM_LOC_OUT.local_item_desc IS 'Contains the local description of the item.  This may be the same as the primary description of the item, a regional description of the item (e.g. jimmies vs sprinkles in the US or roll vs bap vs cob vs bun in the UK), or a value in a local language (e.g. Overlay dress true black knit at US stores vs Lagenkleid - Strick, tiefschwarz at stores in Germany).  The intent is that this string is appropriate to print description on signage/receipts at this location.';
COMMENT ON COLUMN ITEM_LOC_OUT.local_short_desc IS 'Contains the local short description of the item.';
COMMENT ON COLUMN ITEM_LOC_OUT.ti IS 'Number of shipping units (cases) that make up one tier of a pallet.';
COMMENT ON COLUMN ITEM_LOC_OUT.hi IS 'Number of tiers that make up a complete pallet (height).';
COMMENT ON COLUMN ITEM_LOC_OUT.store_order_multiple IS 'This column contains the multiple in which the item needs to be shipped from a warehouse to the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.status IS 'Current status of item at the store.';
COMMENT ON COLUMN ITEM_LOC_OUT.daily_waste_pct IS 'Average percentage lost from inventory on a daily basis due to natural wastage.';
COMMENT ON COLUMN ITEM_LOC_OUT.measure_of_each IS 'Size of an each in terms of the uom_of_price. For example 12 oz. Used in ticketing.';
COMMENT ON COLUMN ITEM_LOC_OUT.measure_of_price IS 'Size to be used on the ticket in terms of the uom_of_price.';
COMMENT ON COLUMN ITEM_LOC_OUT.uom_of_price IS 'Unit of measure that will be used on the ticket for this item.';
COMMENT ON COLUMN ITEM_LOC_OUT.primary_variant IS 'This field is used to address sales of PLUs (i.e. above transaction level items) when inventory is tracked at a lower level (i.e. UPC). This field will only contain a value for items one level higher than the transaction level.';
COMMENT ON COLUMN ITEM_LOC_OUT.primary_cost_pack IS 'This field contains an item number that is a simple pack containing the item in the item column for this record.';
COMMENT ON COLUMN ITEM_LOC_OUT.primary_supplier IS 'Numeric identifier of the supplier who will be considered the primary supplier for the specified item/loc.';
COMMENT ON COLUMN ITEM_LOC_OUT.primary_origin_country IS 'Contains the identifier of the origin country which will be considered the primary country for the specified item/location.';
COMMENT ON COLUMN ITEM_LOC_OUT.receive_as_type IS 'This column determines whether the stock on hand for a pack component item or the buyer pack itself will be updated when a buyer pack is received at a warehouse.';
COMMENT ON COLUMN ITEM_LOC_OUT.inbound_handling_days IS 'This field indicates the number of inbound handling days for an item at a warehouse type location.';
COMMENT ON COLUMN ITEM_LOC_OUT.source_method IS 'This value will be used to specify how the adhoc PO/TSF creation process should source the item/location request.';
COMMENT ON COLUMN ITEM_LOC_OUT.source_wh IS 'This value will be used by the ad-hoc PO/Transfer creation process to determine which warehouse to fill the stores request from.';
COMMENT ON COLUMN ITEM_LOC_OUT.uin_type IS 'This column will contain the unique identification number (UIN) used to identify the instances of the item at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.uin_label IS 'This column will contain the label for the UIN when displayed in SIM.';
COMMENT ON COLUMN ITEM_LOC_OUT.capture_time_in_proc IS 'This column will indicate when the UIN should be captured for an item during transaction processing.';
COMMENT ON COLUMN ITEM_LOC_OUT.ext_uin_ind IS 'EXT_UIN_IND This Yes/No indicator indicates if UIN is being generated in the external system.';
COMMENT ON COLUMN ITEM_LOC_OUT.intentionally_range_ind IS 'This column determines if the location is ranged intentionally by the user for replenishment/selling or incidentally ranged by the RMS programs when item is not ranged to a specific location on the transaction.';
COMMENT ON COLUMN ITEM_LOC_OUT.costing_location IS 'Numeric identifier of the costing location for the franchise store. This field may contain a store or a warehouse.';
COMMENT ON COLUMN ITEM_LOC_OUT.costing_loc_type IS 'This field holds the type of costing location in the costing location field.';
COMMENT ON COLUMN ITEM_LOC_OUT.launch_date IS 'Holds the date that they item should first be sold at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.qty_key_options IS 'Determines whether the qty key on a POS should be used for this item at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.manual_price_entry IS 'Determines whether the price can/should be entered manually on a POS for this item at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.deposit_code IS 'Indicates whether a deposit is associated with this item at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.food_stamp_ind IS 'Indicates whether the item is approved for food stamps at the location. This value will be downloaded to the POS.';
COMMENT ON COLUMN ITEM_LOC_OUT.wic_ind IS 'Indicates whether the item is approved for WIC at the location. This value will be downloaded to the POS.';
COMMENT ON COLUMN ITEM_LOC_OUT.proportional_tare_pct IS 'Holds the value associated of the packaging in items sold by weight at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.fixed_tare_value IS 'Holds the value associated of the packaging in items sold by weight at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.fixed_tare_uom IS 'Holds the unit of measure value associated with the tare value. The only processing RMS does involving the fixed tare value and UOM is downloading it to the POS.';
COMMENT ON COLUMN ITEM_LOC_OUT.reward_eligible_ind IS 'Holds whether the item is legally valid for various types of bonus point/award programs at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.natl_brand_comp_item IS 'Holds the nationally branded item to which you would like to compare the current item.';
COMMENT ON COLUMN ITEM_LOC_OUT.return_policy IS 'Holds the return policy for the item at the location. Valid values for this field belong to the code_type RETP.';
COMMENT ON COLUMN ITEM_LOC_OUT.stop_sale_ind IS 'Indicates that sale of the item should be stopped immediately at the location (i.e. in case of recall etc).';
COMMENT ON COLUMN ITEM_LOC_OUT.elect_mtk_club IS 'Holds the code that represents the marketing clubs to which the item belongs at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.report_code IS 'Code to determine which reports the location should run.';
COMMENT ON COLUMN ITEM_LOC_OUT.req_shelf_life_on_selection IS 'Holds the required shelf life for an item on receipt in days.';
COMMENT ON COLUMN ITEM_LOC_OUT.ib_shelf_life IS 'This column will hold the Investment Buyspecific shelf life for the item/location';
COMMENT ON COLUMN ITEM_LOC_OUT.store_orderable_ind IS 'STORE_REORDERABLE_IND Indicates whether the store may re-order the item. This field is required to be either= Y - yes or N - no. The field will default to N. No RMS processing is based on the value in this field.';
COMMENT ON COLUMN ITEM_LOC_OUT.rack_size IS 'Indicates the rack size that should be used for the item. This field is not required. Valid values for the field can be found and defined in the code_type RACK.';
COMMENT ON COLUMN ITEM_LOC_OUT.full_pallet_item IS 'Indicates whether a store must reorder an item in full pallets only.';
COMMENT ON COLUMN ITEM_LOC_OUT.in_store_market_basket IS 'Holds the in store market basket code for this item/location combination. Valid values for the field can be found in the code_type STMB.';
COMMENT ON COLUMN ITEM_LOC_OUT.storage_location IS 'Holds the current storage location or bin number for the item at the location. No RMS processing is based on the value in this field.';
COMMENT ON COLUMN ITEM_LOC_OUT.alt_storage_location IS 'Holds the preferred alternate storage location or bin number for the item at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.returnable_ind IS 'This field will contain a value of Yes when the item can be returned to the location';
COMMENT ON COLUMN ITEM_LOC_OUT.refundable_ind IS 'This field will contain a value of Yes when the item is refundable at the location.';
COMMENT ON COLUMN ITEM_LOC_OUT.backorder_ind IS 'This field will contain a value of Yes when the item can be back ordered to the location';
COMMENT ON COLUMN ITEM_LOC_OUT.merchandise_ind IS 'Indicates if the item is a merchandise item (Y, N).';


-- Add BDI primary key constraint

ALTER TABLE ITEM_LOC_OUT ADD CONSTRAINT pk_item_loc_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE ITEM_LOC_OUT ADD CONSTRAINT chk_type_item_loc_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE ITEM_LOC_OUT ADD CONSTRAINT chk_actn_item_loc_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
