-- Interface Module: DiffGrp_Fnd
-- Interface: Diff_Grp_Dtl_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE diff_grp_dtl_out_seq CACHE 10000;


-- Create BDI Outbound Table: DIFF_GRP_DTL_OUT
-- This table is used to integrate diff group details.  Diff groups are sets of related diffs (e.g. Fall 2017 Sweater Pallete).  The diff group details are the set of diffs that belong to the group.

CREATE TABLE DIFF_GRP_DTL_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT diff_grp_dtl_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- This field will hold a unique id for the differentiator group.  Data will always be present in this field.
  diff_group_id VARCHAR2(10) NOT NULL,
  -- This field will hold a unique id for the diff that is a member of this diff group.  Data will always be present in this field.
  diff_id VARCHAR2(10) NOT NULL
);

COMMENT ON TABLE DIFF_GRP_DTL_OUT IS 'This table is used to integrate diff group details.  Diff groups are sets of related diffs (e.g. Fall 2017 Sweater Pallete).  The diff group details are the set of diffs that belong to the group.';
COMMENT ON COLUMN DIFF_GRP_DTL_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN DIFF_GRP_DTL_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN DIFF_GRP_DTL_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN DIFF_GRP_DTL_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN DIFF_GRP_DTL_OUT.diff_group_id IS 'This field will hold a unique id for the differentiator group.  Data will always be present in this field.';
COMMENT ON COLUMN DIFF_GRP_DTL_OUT.diff_id IS 'This field will hold a unique id for the diff that is a member of this diff group.  Data will always be present in this field.';


-- Add BDI primary key constraint

ALTER TABLE DIFF_GRP_DTL_OUT ADD CONSTRAINT pk_diff_grp_dtl_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE DIFF_GRP_DTL_OUT ADD CONSTRAINT chk_type_diff_grp_dtl_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE DIFF_GRP_DTL_OUT ADD CONSTRAINT chk_actn_diff_grp_dtl_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
