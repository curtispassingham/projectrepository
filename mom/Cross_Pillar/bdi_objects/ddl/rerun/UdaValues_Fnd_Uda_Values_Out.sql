-- Interface Module: UdaValues_Fnd
-- Interface: Uda_Values_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE uda_values_out_seq CACHE 10000;


-- Create BDI Outbound Table: UDA_VALUES_OUT
-- This table is used to integrate Uda Values information.

CREATE TABLE UDA_VALUES_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT uda_values_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- This field contains a unique number identifying the User Defined Attribute.
  uda_id NUMBER(5,0) NOT NULL,
  -- This field contains a unique number identifying the User Defined Attribute value for the UDA. A UDA can have multiple values. For example, Color can be a UDA and it can have different values like Green, Red, Blue, etc.
  uda_value NUMBER(5,0) NOT NULL,
  -- This field contains a description of the UDA value.
  uda_value_desc VARCHAR2(250) NOT NULL
);

COMMENT ON TABLE UDA_VALUES_OUT IS 'This table is used to integrate Uda Values information.';
COMMENT ON COLUMN UDA_VALUES_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN UDA_VALUES_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN UDA_VALUES_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN UDA_VALUES_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN UDA_VALUES_OUT.uda_id IS 'This field contains a unique number identifying the User Defined Attribute.';
COMMENT ON COLUMN UDA_VALUES_OUT.uda_value IS 'This field contains a unique number identifying the User Defined Attribute value for the UDA. A UDA can have multiple values. For example, Color can be a UDA and it can have different values like Green, Red, Blue, etc.';
COMMENT ON COLUMN UDA_VALUES_OUT.uda_value_desc IS 'This field contains a description of the UDA value.';


-- Add BDI primary key constraint

ALTER TABLE UDA_VALUES_OUT ADD CONSTRAINT pk_uda_values_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE UDA_VALUES_OUT ADD CONSTRAINT chk_type_uda_values_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE UDA_VALUES_OUT ADD CONSTRAINT chk_actn_uda_values_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
