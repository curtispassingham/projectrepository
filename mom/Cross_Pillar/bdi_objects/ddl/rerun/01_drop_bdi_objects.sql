-- generate a script that drops BDI objects in the BDI_RMS_INT_SCHEMA environment and run the script
set pagesize 0
set feedback off
set trimspool on

prompt ;
prompt Start generating a script that drops BDI objects
prompt

spool drop_bdi_objects.lis

select 'DROP TABLE "'|| object_name || '";'
  from user_objects
 where OBJECT_NAME LIKE '%_OUT' AND OBJECT_TYPE = 'TABLE'
UNION ALL
select 'DROP SEQUENCE "'|| object_name || '";'
  from user_objects
 where OBJECT_NAME LIKE '%_OUT_SEQ';

spool off
set feedback on
@drop_bdi_objects.lis

prompt
prompt The count from the SELECT below should be zero
prompt

select count(*)
  from user_objects
 where ((OBJECT_NAME LIKE '%_OUT' AND OBJECT_TYPE = 'TABLE') or (OBJECT_NAME LIKE '%_OUT_SEQ'));

prompt
prompt If the count is not zero, something is wrong ...
prompt

--exit
