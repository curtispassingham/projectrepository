-- Interface Module: ItSupManCtry_Fnd
-- Interface: Item_Sup_Man_Cty_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE item_sup_man_cty_out_seq CACHE 10000;


-- Create BDI Outbound Table: ITEM_SUP_MAN_CTY_OUT
-- This table is used to integrate Item Supplier Manufacturing Country information.

CREATE TABLE ITEM_SUP_MAN_CTY_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT item_sup_man_cty_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- Alphanumeric value that identifies the item.
  item VARCHAR2(25) NOT NULL,
  -- The unique identifier for the supplier.
  supplier NUMBER(10,0) NOT NULL,
  -- The country where the item was manufactured.
  manu_country_id VARCHAR2(3) NOT NULL,
  -- This field indicates whether this country is the primary country of manufacture for the item/supplier. Each item/supplier combination must have one and only one primary country of manufacture.
  primary_manu_ctry_ind VARCHAR2(1) NOT NULL
);

COMMENT ON TABLE ITEM_SUP_MAN_CTY_OUT IS 'This table is used to integrate Item Supplier Manufacturing Country information.';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.item IS 'Alphanumeric value that identifies the item.';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.supplier IS 'The unique identifier for the supplier.';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.manu_country_id IS 'The country where the item was manufactured.';
COMMENT ON COLUMN ITEM_SUP_MAN_CTY_OUT.primary_manu_ctry_ind IS 'This field indicates whether this country is the primary country of manufacture for the item/supplier. Each item/supplier combination must have one and only one primary country of manufacture.';


-- Add BDI primary key constraint

ALTER TABLE ITEM_SUP_MAN_CTY_OUT ADD CONSTRAINT pk_item_sup_man_cty_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE ITEM_SUP_MAN_CTY_OUT ADD CONSTRAINT chk_type_item_sup_man_cty_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE ITEM_SUP_MAN_CTY_OUT ADD CONSTRAINT chk_actn_item_sup_man_cty_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
