-- Interface Module: ItSupCtryDim_Fnd
-- Interface: Item_Sup_Cty_Dim_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE item_sup_cty_dim_out_seq CACHE 10000;


-- Create BDI Outbound Table: ITEM_SUP_CTY_DIM_OUT
-- This table is used to integrate Item Supplier Country Dimension information.

CREATE TABLE ITEM_SUP_CTY_DIM_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT item_sup_cty_dim_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- Unique alphanumeric value that identifies the item.
  item VARCHAR2(25) NOT NULL,
  -- Unique identifier for the supplier.
  supplier NUMBER(10,0) NOT NULL,
  -- The country where the item was manufactured or significantly altered.
  origin_country VARCHAR2(3) NOT NULL,
  -- Specific object whose dimensions are specified in this record (e.g. case, pallet, each).  Valid values for this field are in the code type DIMO on the code_head and code_detail tables.
  dim_object VARCHAR2(6) NOT NULL,
  -- Describes the packaging (if any) being taken into consideration in the specified dimensions.  Valid values for this field are in the code type PCKT on the code_head and code_detail tables.
  presentation_method VARCHAR2(6),
  -- Length of dim_object measured in units specified in lwh_uom.
  length NUMBER(12,4),
  -- Width of dim_object measured in units specified in lwh_uom.
  width NUMBER(12,4),
  -- Height of dim_object measured in units specified in lwh_uom.
  height NUMBER(12,4),
  -- Unit of measurement for length, width, and height (e.g. inches, centimeters, feet).  Valid values for this field are contained in uom field on uom_class table where uom_class field = DIMEN.
  lwh_uom VARCHAR2(4),
  -- Weight of dim_object measured in units specified in weight_uom.
  weight NUMBER(12,4),
  -- Net weight of the dim_object (weight without packaging) measured in units specified in weight_uom.
  net_weight NUMBER(12,4),
  -- Unit of measurement for weight (e.g. pounds, kilograms).  Valid values for this field are contained in uom field on uom_class table where uom_class field = MASS.
  weight_uom VARCHAR2(4),
  -- Liquid volume, or capacity, of dim_object measured in units specified in volume_uom.  Liquid volumes are only convertible to other liquid volumes.
  liquid_volume NUMBER(12,4),
  -- Unit of measurement for liquid_volume (e.g. ounces, liters). Liquid volumes are only convertible to other liquid volumes.  Valid values for this field are contained in uom field on uom_class table where uom_class field = LVOL.
  liquid_volume_uom VARCHAR2(4),
  -- Statistical value of the dim_objects dimensions to be used for loading purposes.
  stat_cube NUMBER(12,4),
  -- Amount of weight to be subtracted for packaging materials.  Used to calculate the true net weight of the dim_object.
  tare_weight NUMBER(12,4),
  -- TARE_TYPE - Indicates if tare weight for this dim_object is wet or dry.  Valid values are found on the code_detail table with the code_type TARE and include:       W = Wet tare weight     D = Dry tare weight
  tare_type VARCHAR2(6)
);

COMMENT ON TABLE ITEM_SUP_CTY_DIM_OUT IS 'This table is used to integrate Item Supplier Country Dimension information.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.item IS 'Unique alphanumeric value that identifies the item.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.supplier IS 'Unique identifier for the supplier.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.origin_country IS 'The country where the item was manufactured or significantly altered.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.dim_object IS 'Specific object whose dimensions are specified in this record (e.g. case, pallet, each).  Valid values for this field are in the code type DIMO on the code_head and code_detail tables.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.presentation_method IS 'Describes the packaging (if any) being taken into consideration in the specified dimensions.  Valid values for this field are in the code type PCKT on the code_head and code_detail tables.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.length IS 'Length of dim_object measured in units specified in lwh_uom.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.width IS 'Width of dim_object measured in units specified in lwh_uom.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.height IS 'Height of dim_object measured in units specified in lwh_uom.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.lwh_uom IS 'Unit of measurement for length, width, and height (e.g. inches, centimeters, feet).  Valid values for this field are contained in uom field on uom_class table where uom_class field = DIMEN.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.weight IS 'Weight of dim_object measured in units specified in weight_uom.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.net_weight IS 'Net weight of the dim_object (weight without packaging) measured in units specified in weight_uom.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.weight_uom IS 'Unit of measurement for weight (e.g. pounds, kilograms).  Valid values for this field are contained in uom field on uom_class table where uom_class field = MASS.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.liquid_volume IS 'Liquid volume, or capacity, of dim_object measured in units specified in volume_uom.  Liquid volumes are only convertible to other liquid volumes.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.liquid_volume_uom IS 'Unit of measurement for liquid_volume (e.g. ounces, liters). Liquid volumes are only convertible to other liquid volumes.  Valid values for this field are contained in uom field on uom_class table where uom_class field = LVOL.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.stat_cube IS 'Statistical value of the dim_objects dimensions to be used for loading purposes.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.tare_weight IS 'Amount of weight to be subtracted for packaging materials.  Used to calculate the true net weight of the dim_object.';
COMMENT ON COLUMN ITEM_SUP_CTY_DIM_OUT.tare_type IS 'TARE_TYPE - Indicates if tare weight for this dim_object is wet or dry.  Valid values are found on the code_detail table with the code_type TARE and include:       W = Wet tare weight     D = Dry tare weight';


-- Add BDI primary key constraint

ALTER TABLE ITEM_SUP_CTY_DIM_OUT ADD CONSTRAINT pk_item_sup_cty_dim_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE ITEM_SUP_CTY_DIM_OUT ADD CONSTRAINT chk_type_item_sup_cty_dim_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE ITEM_SUP_CTY_DIM_OUT ADD CONSTRAINT chk_actn_item_sup_cty_dim_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
