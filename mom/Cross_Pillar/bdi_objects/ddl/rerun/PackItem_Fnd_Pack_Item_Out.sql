-- Interface Module: PackItem_Fnd
-- Interface: Pack_Item_Out

-- Create sequence for bdi_seq_id column

CREATE SEQUENCE pack_item_out_seq CACHE 10000;


-- Create BDI Outbound Table: PACK_ITEM_OUT
-- This table is used to integrate Pack Item information.

CREATE TABLE PACK_ITEM_OUT (
  -- bdi internal column
  bdi_seq_id NUMBER DEFAULT pack_item_out_seq.nextval NOT NULL,
  -- bdi internal column
  bdi_app_name VARCHAR2(50) DEFAULT sys_context('userenv', 'current_schema') NOT NULL,
  -- bdi internal column
  bdi_dataset_type VARCHAR2(20) DEFAULT 'FULL',
  -- bdi internal column
  bdi_dataset_action VARCHAR2(20) DEFAULT 'REPLACE',
  -- Alphanumeric value that uniquely identifies the pack for which details are held in this table.
  pack_no VARCHAR2(25) NOT NULL,
  -- Contains a sequence number used to uniquely identify a row in the PACKITEM table.
  seq_no NUMBER(4,0) NOT NULL,
  -- Alphanumeric value that identifies the component item within the pack. If pack item is created using pack template then the component items are stored in the PACKITEM_BREAKOUT table and this field is null.
  item VARCHAR2(25),
  -- This field contains the parent item (if any) associated  with the component item of the pack.
  item_parent VARCHAR2(25),
  -- Contains the pack template ID associated with the pack item.
  pack_tmpl_id NUMBER(8,0),
  -- Contains the quantity of component items within the pack. If the pack item is created using a pack template then the quantity specified here is 1 and the actual component item quantities in the pack are stored in PACKITEM_BREAKOUT table.
  pack_qty NUMBER(12,4) NOT NULL
);

COMMENT ON TABLE PACK_ITEM_OUT IS 'This table is used to integrate Pack Item information.';
COMMENT ON COLUMN PACK_ITEM_OUT.bdi_seq_id IS 'bdi internal column';
COMMENT ON COLUMN PACK_ITEM_OUT.bdi_app_name IS 'bdi internal column';
COMMENT ON COLUMN PACK_ITEM_OUT.bdi_dataset_type IS 'bdi internal column';
COMMENT ON COLUMN PACK_ITEM_OUT.bdi_dataset_action IS 'bdi internal column';
COMMENT ON COLUMN PACK_ITEM_OUT.pack_no IS 'Alphanumeric value that uniquely identifies the pack for which details are held in this table.';
COMMENT ON COLUMN PACK_ITEM_OUT.seq_no IS 'Contains a sequence number used to uniquely identify a row in the PACKITEM table.';
COMMENT ON COLUMN PACK_ITEM_OUT.item IS 'Alphanumeric value that identifies the component item within the pack. If pack item is created using pack template then the component items are stored in the PACKITEM_BREAKOUT table and this field is null.';
COMMENT ON COLUMN PACK_ITEM_OUT.item_parent IS 'This field contains the parent item (if any) associated  with the component item of the pack.';
COMMENT ON COLUMN PACK_ITEM_OUT.pack_tmpl_id IS 'Contains the pack template ID associated with the pack item.';
COMMENT ON COLUMN PACK_ITEM_OUT.pack_qty IS 'Contains the quantity of component items within the pack. If the pack item is created using a pack template then the quantity specified here is 1 and the actual component item quantities in the pack are stored in PACKITEM_BREAKOUT table.';


-- Add BDI primary key constraint

ALTER TABLE PACK_ITEM_OUT ADD CONSTRAINT pk_pack_item_out PRIMARY KEY (bdi_app_name, bdi_seq_id);


-- Add check constraint for bdi_dataset_type column

ALTER TABLE PACK_ITEM_OUT ADD CONSTRAINT chk_type_pack_item_out CHECK (bdi_dataset_type IN ('FULL', 'PARTIAL'));


-- Add check constraint for bdi_dataset_action column

ALTER TABLE PACK_ITEM_OUT ADD CONSTRAINT chk_actn_pack_item_out CHECK (bdi_dataset_action IN ('REPLACE', 'CREATE', 'UPDATE', 'DELETE'));
