DROP TYPE "RIB_StkRtnHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkRtnHdrDesc_REC";
/
DROP TYPE "RIB_StkRtnHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkRtnHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkRtnHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  return_id number(12),
  store_id number(10),
  external_id varchar2(128),
  authorization_code varchar2(12),
  context_type_id varchar2(15),
  context_value varchar2(25),
  status varchar2(10), -- status is enumeration field, valid values are [PENDING, REQUESTED, DISPATCHED, SUBMITTED, CANCELED, UNKNOWN] (all lower-case)
  ship_to_entity_id varchar2(128),
  status_date date,
  not_after_date date,
  number_of_line_items number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkRtnHdrDesc_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, authorization_code varchar2
, context_type_id varchar2
, context_value varchar2
, status varchar2
, ship_to_entity_id varchar2
, status_date date
, not_after_date date
, number_of_line_items number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkRtnHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkRtnHdrDesc') := "ns_name_StkRtnHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'return_id') := return_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_entity_id') := ship_to_entity_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status_date') := status_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_line_items') := number_of_line_items;
END appendNodeValues;
constructor function "RIB_StkRtnHdrDesc_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, authorization_code varchar2
, context_type_id varchar2
, context_value varchar2
, status varchar2
, ship_to_entity_id varchar2
, status_date date
, not_after_date date
, number_of_line_items number
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.store_id := store_id;
self.external_id := external_id;
self.authorization_code := authorization_code;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.status := status;
self.ship_to_entity_id := ship_to_entity_id;
self.status_date := status_date;
self.not_after_date := not_after_date;
self.number_of_line_items := number_of_line_items;
RETURN;
end;
END;
/
