DROP TYPE "RIB_PrcInqDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcInqDtl_REC";
/
DROP TYPE "RIB_PrcInqDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcInqDesc_REC";
/
DROP TYPE "RIB_PrcInqDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcInqDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcInqDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  status_code varchar2(10),
  item varchar2(25),
  location number(10),
  location_type varchar2(1),
  pricing_date date,
  selling_unit_retail number(20,4),
  selling_uom varchar2(4),
  multi_units number(12,4),
  multi_unit_retail number(20,4),
  multi_selling_uom varchar2(4),
  currency_code varchar2(3),
  price_type varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcInqDtl_REC"
(
  rib_oid number
, status_code varchar2
, item varchar2
, location number
, location_type varchar2
, pricing_date date
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
, currency_code varchar2
, price_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcInqDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcInqDesc') := "ns_name_PrcInqDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'status_code') := status_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'location_type') := location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_date') := pricing_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_retail') := selling_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_units') := multi_units;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_retail') := multi_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_selling_uom') := multi_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_type') := price_type;
END appendNodeValues;
constructor function "RIB_PrcInqDtl_REC"
(
  rib_oid number
, status_code varchar2
, item varchar2
, location number
, location_type varchar2
, pricing_date date
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
, currency_code varchar2
, price_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.status_code := status_code;
self.item := item;
self.location := location;
self.location_type := location_type;
self.pricing_date := pricing_date;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
self.multi_selling_uom := multi_selling_uom;
self.currency_code := currency_code;
self.price_type := price_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcInqDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcInqDtl_TBL" AS TABLE OF "RIB_PrcInqDtl_REC";
/
DROP TYPE "RIB_PrcInqDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcInqDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcInqDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  PrcInqDtl_TBL "RIB_PrcInqDtl_TBL",   -- Size of "RIB_PrcInqDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcInqDesc_REC"
(
  rib_oid number
, PrcInqDtl_TBL "RIB_PrcInqDtl_TBL"  -- Size of "RIB_PrcInqDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcInqDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcInqDesc') := "ns_name_PrcInqDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'PrcInqDtl_TBL.';
  FOR INDX IN PrcInqDtl_TBL.FIRST()..PrcInqDtl_TBL.LAST() LOOP
    PrcInqDtl_TBL(indx).appendNodeValues( i_prefix||indx||'PrcInqDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrcInqDesc_REC"
(
  rib_oid number
, PrcInqDtl_TBL "RIB_PrcInqDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.PrcInqDtl_TBL := PrcInqDtl_TBL;
RETURN;
end;
END;
/
