Rib Oracle Objects Installation Instructions:
-------------------------------------------------------------------------------

1) unzip the rib-public-payload-database-object-types.zip to install staging
   temporary directory. 

   unzip rib-public-payload-database-object-types.zip

2) Login in to your target database using SQLPLUS.

   E.g.
   sqlplus rmsuser/retek@orcllnx

3) In the SQLPLUS prompt type

   SQL>@InstallAndCompileAllRibOracleObjects.sql

4) When the above script ends it will print the number of "invalid" objects
   still there in the database after the initial compilation. If the count is
   0(zero) you are done. If the count of invalid objects is not 0(zero) run the
   CompileInvalidOracleObjects.sql script by typing the following in the SQLPLUS
   prompt. Repeat the CompileInvalidOracleObjects.sql script until the count
   becomes 0(zero).

   SQL>@CompileInvalidOracleObjects.sql
