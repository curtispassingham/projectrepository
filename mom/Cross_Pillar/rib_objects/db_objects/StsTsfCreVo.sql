DROP TYPE "RIB_StsTsfCreVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfCreVo_REC";
/
DROP TYPE "RIB_StsTsfCreVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfCreVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfCreVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  dest_loc_type varchar2(20), -- dest_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, UNKNOWN] (all lower-case)
  dest_loc_id number(10),
  not_after_date date,
  auth_code varchar2(12),
  context_type_id varchar2(15),
  context_value varchar2(25),
  partial_delivery_allowed varchar2(5), --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case) 
  use_available varchar2(5), --use_available is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfCreVo_REC"
(
  rib_oid number
, store_id number
, dest_loc_type varchar2
, dest_loc_id number
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfCreVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfCreVo') := "ns_name_StsTsfCreVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_type') := dest_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_id') := dest_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'auth_code') := auth_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'partial_delivery_allowed') := partial_delivery_allowed;
  rib_obj_util.g_RIB_element_values(i_prefix||'use_available') := use_available;
END appendNodeValues;
constructor function "RIB_StsTsfCreVo_REC"
(
  rib_oid number
, store_id number
, dest_loc_type varchar2
, dest_loc_id number
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, use_available varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.dest_loc_type := dest_loc_type;
self.dest_loc_id := dest_loc_id;
self.not_after_date := not_after_date;
self.auth_code := auth_code;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.use_available := use_available;
RETURN;
end;
END;
/
