@@TaxDetRBO.sql;
/
@@LineItemTaxRBO.sql;
/
@@NameValPairRBO.sql;
/
@@FreightRBO.sql;
/
DROP TYPE "RIB_FiscDocTaxRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscDocTaxRBO_REC";
/
DROP TYPE "RIB_SrcEcoClassCd_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcEcoClassCd_REC";
/
DROP TYPE "RIB_DstEcoClassCd_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DstEcoClassCd_REC";
/
DROP TYPE "RIB_SrcNameValPair_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcNameValPair_REC";
/
DROP TYPE "RIB_DstNameValPair_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DstNameValPair_REC";
/
DROP TYPE "RIB_SrcTaxContributor_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcTaxContributor_REC";
/
DROP TYPE "RIB_DstTaxContributor_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DstTaxContributor_REC";
/
DROP TYPE "RIB_SrcDiffTaxRegime_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcDiffTaxRegime_REC";
/
DROP TYPE "RIB_DstDiffTaxRegime_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DstDiffTaxRegime_REC";
/
DROP TYPE "RIB_Messages_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Messages_REC";
/
DROP TYPE "RIB_SrcEcoClassCd_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcEcoClassCd_TBL" AS TABLE OF "RIB_SrcEcoClassCd_REC";
/
DROP TYPE "RIB_SrcTaxContributor_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcTaxContributor_TBL" AS TABLE OF "RIB_SrcTaxContributor_REC";
/
DROP TYPE "RIB_SrcDiffTaxRegime_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcDiffTaxRegime_TBL" AS TABLE OF "RIB_SrcDiffTaxRegime_REC";
/
DROP TYPE "RIB_SrcNameValPair_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SrcNameValPair_TBL" AS TABLE OF "RIB_SrcNameValPair_REC";
/
DROP TYPE "RIB_DstEcoClassCd_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DstEcoClassCd_TBL" AS TABLE OF "RIB_DstEcoClassCd_REC";
/
DROP TYPE "RIB_DstDiffTaxRegime_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DstDiffTaxRegime_TBL" AS TABLE OF "RIB_DstDiffTaxRegime_REC";
/
DROP TYPE "RIB_DstTaxContributor_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DstTaxContributor_TBL" AS TABLE OF "RIB_DstTaxContributor_REC";
/
DROP TYPE "RIB_DstNameValPair_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DstNameValPair_TBL" AS TABLE OF "RIB_DstNameValPair_REC";
/
DROP TYPE "RIB_LineItemTaxRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LineItemTaxRBO_TBL" AS TABLE OF "RIB_LineItemTaxRBO_REC";
/
DROP TYPE "RIB_Messages_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_Messages_TBL" AS TABLE OF "RIB_Messages_REC";
/
DROP TYPE "RIB_TaxDetRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxDetRBO_TBL" AS TABLE OF "RIB_TaxDetRBO_REC";
/
DROP TYPE "RIB_FreightRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FreightRBO_TBL" AS TABLE OF "RIB_FreightRBO_REC";
/
DROP TYPE "RIB_FiscDocTaxRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscDocTaxRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  document_number varchar2(25),
  fiscal_document_date date,
  src_entity_code varchar2(250),
  src_entity_type varchar2(250),
  src_addr_city_id varchar2(10),
  src_federal_tax_reg_id varchar2(250),
  SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL",   -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
  SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL",   -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
  SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL",   -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
  src_is_income_range_eligible varchar2(1),
  src_is_distr_a_manufacturer varchar2(1),
  src_icms_simples_rate number(20,4),
  src_is_rural_producer varchar2(1),
  src_is_simples_contributor varchar2(1),
  SrcNameValPair_TBL "RIB_SrcNameValPair_TBL",   -- Size of "RIB_SrcNameValPair_TBL" is unbounded
  dst_entity_code varchar2(250),
  dst_entity_type varchar2(250),
  dst_addr_city_id varchar2(10),
  dst_federal_tax_reg_id varchar2(250),
  DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL",   -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
  DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL",   -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
  dst_is_income_range_eligible varchar2(1),
  dst_is_distr_a_manufacturer varchar2(1),
  dst_icms_simples_rate number(20,4),
  DstTaxContributor_TBL "RIB_DstTaxContributor_TBL",   -- Size of "RIB_DstTaxContributor_TBL" is unbounded
  DstNameValPair_TBL "RIB_DstNameValPair_TBL",   -- Size of "RIB_DstNameValPair_TBL" is unbounded
  disc_merch_cost number(20,4),
  total_cost number(20,4),
  total_services_cost number(20,4),
  calculation_status varchar2(1),
  LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL",   -- Size of "RIB_LineItemTaxRBO_TBL" is unbounded
  Messages_TBL "RIB_Messages_TBL",   -- Size of "RIB_Messages_TBL" is unbounded
  TaxDetRBO_TBL "RIB_TaxDetRBO_TBL",   -- Size of "RIB_TaxDetRBO_TBL" is unbounded
  FreightRBO_TBL "RIB_FreightRBO_TBL",   -- Size of "RIB_FreightRBO_TBL" is unbounded
  reduced_icms_tax number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"  -- Size of "RIB_LineItemTaxRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"  -- Size of "RIB_LineItemTaxRBO_TBL" is unbounded
, Messages_TBL "RIB_Messages_TBL"  -- Size of "RIB_Messages_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"  -- Size of "RIB_LineItemTaxRBO_TBL" is unbounded
, Messages_TBL "RIB_Messages_TBL"  -- Size of "RIB_Messages_TBL" is unbounded
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"  -- Size of "RIB_LineItemTaxRBO_TBL" is unbounded
, Messages_TBL "RIB_Messages_TBL"  -- Size of "RIB_Messages_TBL" is unbounded
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"  -- Size of "RIB_SrcEcoClassCd_TBL" is unbounded
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"  -- Size of "RIB_SrcTaxContributor_TBL" is unbounded
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"  -- Size of "RIB_SrcDiffTaxRegime_TBL" is unbounded
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"  -- Size of "RIB_SrcNameValPair_TBL" is unbounded
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"  -- Size of "RIB_DstEcoClassCd_TBL" is unbounded
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"  -- Size of "RIB_DstDiffTaxRegime_TBL" is unbounded
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"  -- Size of "RIB_DstTaxContributor_TBL" is unbounded
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"  -- Size of "RIB_DstNameValPair_TBL" is unbounded
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"  -- Size of "RIB_LineItemTaxRBO_TBL" is unbounded
, Messages_TBL "RIB_Messages_TBL"  -- Size of "RIB_Messages_TBL" is unbounded
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, reduced_icms_tax number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscDocTaxRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'document_number') := document_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_document_date') := fiscal_document_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_entity_code') := src_entity_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_entity_type') := src_entity_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_addr_city_id') := src_addr_city_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_federal_tax_reg_id') := src_federal_tax_reg_id;
  IF SrcEcoClassCd_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SrcEcoClassCd_TBL.';
    FOR INDX IN SrcEcoClassCd_TBL.FIRST()..SrcEcoClassCd_TBL.LAST() LOOP
      SrcEcoClassCd_TBL(indx).appendNodeValues( i_prefix||indx||'SrcEcoClassCd_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF SrcTaxContributor_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SrcTaxContributor_TBL.';
    FOR INDX IN SrcTaxContributor_TBL.FIRST()..SrcTaxContributor_TBL.LAST() LOOP
      SrcTaxContributor_TBL(indx).appendNodeValues( i_prefix||indx||'SrcTaxContributor_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF SrcDiffTaxRegime_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SrcDiffTaxRegime_TBL.';
    FOR INDX IN SrcDiffTaxRegime_TBL.FIRST()..SrcDiffTaxRegime_TBL.LAST() LOOP
      SrcDiffTaxRegime_TBL(indx).appendNodeValues( i_prefix||indx||'SrcDiffTaxRegime_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_is_income_range_eligible') := src_is_income_range_eligible;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_is_distr_a_manufacturer') := src_is_distr_a_manufacturer;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_icms_simples_rate') := src_icms_simples_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_is_rural_producer') := src_is_rural_producer;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_is_simples_contributor') := src_is_simples_contributor;
  IF SrcNameValPair_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SrcNameValPair_TBL.';
    FOR INDX IN SrcNameValPair_TBL.FIRST()..SrcNameValPair_TBL.LAST() LOOP
      SrcNameValPair_TBL(indx).appendNodeValues( i_prefix||indx||'SrcNameValPair_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_entity_code') := dst_entity_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_entity_type') := dst_entity_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_addr_city_id') := dst_addr_city_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_federal_tax_reg_id') := dst_federal_tax_reg_id;
  IF DstEcoClassCd_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DstEcoClassCd_TBL.';
    FOR INDX IN DstEcoClassCd_TBL.FIRST()..DstEcoClassCd_TBL.LAST() LOOP
      DstEcoClassCd_TBL(indx).appendNodeValues( i_prefix||indx||'DstEcoClassCd_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF DstDiffTaxRegime_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DstDiffTaxRegime_TBL.';
    FOR INDX IN DstDiffTaxRegime_TBL.FIRST()..DstDiffTaxRegime_TBL.LAST() LOOP
      DstDiffTaxRegime_TBL(indx).appendNodeValues( i_prefix||indx||'DstDiffTaxRegime_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_is_income_range_eligible') := dst_is_income_range_eligible;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_is_distr_a_manufacturer') := dst_is_distr_a_manufacturer;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_icms_simples_rate') := dst_icms_simples_rate;
  IF DstTaxContributor_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DstTaxContributor_TBL.';
    FOR INDX IN DstTaxContributor_TBL.FIRST()..DstTaxContributor_TBL.LAST() LOOP
      DstTaxContributor_TBL(indx).appendNodeValues( i_prefix||indx||'DstTaxContributor_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF DstNameValPair_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DstNameValPair_TBL.';
    FOR INDX IN DstNameValPair_TBL.FIRST()..DstNameValPair_TBL.LAST() LOOP
      DstNameValPair_TBL(indx).appendNodeValues( i_prefix||indx||'DstNameValPair_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'disc_merch_cost') := disc_merch_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cost') := total_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_services_cost') := total_services_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'calculation_status') := calculation_status;
  IF LineItemTaxRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LineItemTaxRBO_TBL.';
    FOR INDX IN LineItemTaxRBO_TBL.FIRST()..LineItemTaxRBO_TBL.LAST() LOOP
      LineItemTaxRBO_TBL(indx).appendNodeValues( i_prefix||indx||'LineItemTaxRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF Messages_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'Messages_TBL.';
    FOR INDX IN Messages_TBL.FIRST()..Messages_TBL.LAST() LOOP
      Messages_TBL(indx).appendNodeValues( i_prefix||indx||'Messages_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF TaxDetRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TaxDetRBO_TBL.';
    FOR INDX IN TaxDetRBO_TBL.FIRST()..TaxDetRBO_TBL.LAST() LOOP
      TaxDetRBO_TBL(indx).appendNodeValues( i_prefix||indx||'TaxDetRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF FreightRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FreightRBO_TBL.';
    FOR INDX IN FreightRBO_TBL.FIRST()..FreightRBO_TBL.LAST() LOOP
      FreightRBO_TBL(indx).appendNodeValues( i_prefix||indx||'FreightRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'reduced_icms_tax') := reduced_icms_tax;
END appendNodeValues;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
self.calculation_status := calculation_status;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
self.calculation_status := calculation_status;
self.LineItemTaxRBO_TBL := LineItemTaxRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"
, Messages_TBL "RIB_Messages_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
self.calculation_status := calculation_status;
self.LineItemTaxRBO_TBL := LineItemTaxRBO_TBL;
self.Messages_TBL := Messages_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"
, Messages_TBL "RIB_Messages_TBL"
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
self.calculation_status := calculation_status;
self.LineItemTaxRBO_TBL := LineItemTaxRBO_TBL;
self.Messages_TBL := Messages_TBL;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"
, Messages_TBL "RIB_Messages_TBL"
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
self.calculation_status := calculation_status;
self.LineItemTaxRBO_TBL := LineItemTaxRBO_TBL;
self.Messages_TBL := Messages_TBL;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscDocTaxRBO_REC"
(
  rib_oid number
, document_number varchar2
, fiscal_document_date date
, src_entity_code varchar2
, src_entity_type varchar2
, src_addr_city_id varchar2
, src_federal_tax_reg_id varchar2
, SrcEcoClassCd_TBL "RIB_SrcEcoClassCd_TBL"
, SrcTaxContributor_TBL "RIB_SrcTaxContributor_TBL"
, SrcDiffTaxRegime_TBL "RIB_SrcDiffTaxRegime_TBL"
, src_is_income_range_eligible varchar2
, src_is_distr_a_manufacturer varchar2
, src_icms_simples_rate number
, src_is_rural_producer varchar2
, src_is_simples_contributor varchar2
, SrcNameValPair_TBL "RIB_SrcNameValPair_TBL"
, dst_entity_code varchar2
, dst_entity_type varchar2
, dst_addr_city_id varchar2
, dst_federal_tax_reg_id varchar2
, DstEcoClassCd_TBL "RIB_DstEcoClassCd_TBL"
, DstDiffTaxRegime_TBL "RIB_DstDiffTaxRegime_TBL"
, dst_is_income_range_eligible varchar2
, dst_is_distr_a_manufacturer varchar2
, dst_icms_simples_rate number
, DstTaxContributor_TBL "RIB_DstTaxContributor_TBL"
, DstNameValPair_TBL "RIB_DstNameValPair_TBL"
, disc_merch_cost number
, total_cost number
, total_services_cost number
, calculation_status varchar2
, LineItemTaxRBO_TBL "RIB_LineItemTaxRBO_TBL"
, Messages_TBL "RIB_Messages_TBL"
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, reduced_icms_tax number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.src_entity_code := src_entity_code;
self.src_entity_type := src_entity_type;
self.src_addr_city_id := src_addr_city_id;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.SrcEcoClassCd_TBL := SrcEcoClassCd_TBL;
self.SrcTaxContributor_TBL := SrcTaxContributor_TBL;
self.SrcDiffTaxRegime_TBL := SrcDiffTaxRegime_TBL;
self.src_is_income_range_eligible := src_is_income_range_eligible;
self.src_is_distr_a_manufacturer := src_is_distr_a_manufacturer;
self.src_icms_simples_rate := src_icms_simples_rate;
self.src_is_rural_producer := src_is_rural_producer;
self.src_is_simples_contributor := src_is_simples_contributor;
self.SrcNameValPair_TBL := SrcNameValPair_TBL;
self.dst_entity_code := dst_entity_code;
self.dst_entity_type := dst_entity_type;
self.dst_addr_city_id := dst_addr_city_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.DstEcoClassCd_TBL := DstEcoClassCd_TBL;
self.DstDiffTaxRegime_TBL := DstDiffTaxRegime_TBL;
self.dst_is_income_range_eligible := dst_is_income_range_eligible;
self.dst_is_distr_a_manufacturer := dst_is_distr_a_manufacturer;
self.dst_icms_simples_rate := dst_icms_simples_rate;
self.DstTaxContributor_TBL := DstTaxContributor_TBL;
self.DstNameValPair_TBL := DstNameValPair_TBL;
self.disc_merch_cost := disc_merch_cost;
self.total_cost := total_cost;
self.total_services_cost := total_services_cost;
self.calculation_status := calculation_status;
self.LineItemTaxRBO_TBL := LineItemTaxRBO_TBL;
self.Messages_TBL := Messages_TBL;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.reduced_icms_tax := reduced_icms_tax;
RETURN;
end;
END;
/
DROP TYPE "RIB_SrcEcoClassCd_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SrcEcoClassCd_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SrcEcoClassCd_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SrcEcoClassCd_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_SrcEcoClassCd_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_DstEcoClassCd_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DstEcoClassCd_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DstEcoClassCd_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DstEcoClassCd_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_DstEcoClassCd_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_SrcNameValPair_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SrcNameValPair_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  NameValPairRBO "RIB_NameValPairRBO_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SrcNameValPair_REC"
(
  rib_oid number
, NameValPairRBO "RIB_NameValPairRBO_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SrcNameValPair_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'NameValPairRBO.';
  NameValPairRBO.appendNodeValues( i_prefix||'NameValPairRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_SrcNameValPair_REC"
(
  rib_oid number
, NameValPairRBO "RIB_NameValPairRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.NameValPairRBO := NameValPairRBO;
RETURN;
end;
END;
/
DROP TYPE "RIB_DstNameValPair_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DstNameValPair_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  NameValPairRBO "RIB_NameValPairRBO_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DstNameValPair_REC"
(
  rib_oid number
, NameValPairRBO "RIB_NameValPairRBO_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DstNameValPair_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'NameValPairRBO.';
  NameValPairRBO.appendNodeValues( i_prefix||'NameValPairRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_DstNameValPair_REC"
(
  rib_oid number
, NameValPairRBO "RIB_NameValPairRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.NameValPairRBO := NameValPairRBO;
RETURN;
end;
END;
/
DROP TYPE "RIB_SrcTaxContributor_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SrcTaxContributor_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SrcTaxContributor_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SrcTaxContributor_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_SrcTaxContributor_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_DstTaxContributor_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DstTaxContributor_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DstTaxContributor_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DstTaxContributor_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_DstTaxContributor_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_SrcDiffTaxRegime_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SrcDiffTaxRegime_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SrcDiffTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SrcDiffTaxRegime_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_SrcDiffTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_DstDiffTaxRegime_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DstDiffTaxRegime_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DstDiffTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DstDiffTaxRegime_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_DstDiffTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_Messages_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Messages_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(4000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Messages_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Messages_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocTaxRBO') := "ns_name_FiscDocTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_Messages_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
