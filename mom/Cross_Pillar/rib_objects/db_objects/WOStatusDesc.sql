DROP TYPE "RIB_WOStatusInvAdj_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOStatusInvAdj_REC";
/
DROP TYPE "RIB_WOStatusDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOStatusDesc_REC";
/
DROP TYPE "RIB_WOStatusInvAdj_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOStatusInvAdj_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOStatusDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_disposition varchar2(4),
  to_disposition varchar2(4),
  unit_qty number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOStatusInvAdj_REC"
(
  rib_oid number
, from_disposition varchar2
, to_disposition varchar2
, unit_qty number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOStatusInvAdj_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOStatusDesc') := "ns_name_WOStatusDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_disposition') := from_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
END appendNodeValues;
constructor function "RIB_WOStatusInvAdj_REC"
(
  rib_oid number
, from_disposition varchar2
, to_disposition varchar2
, unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_qty := unit_qty;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOStatusInvAdj_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOStatusInvAdj_TBL" AS TABLE OF "RIB_WOStatusInvAdj_REC";
/
DROP TYPE "RIB_WOStatusDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOStatusDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOStatusDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  wo_id varchar2(15),
  distro_nbr varchar2(12),
  distro_doc_type varchar2(1),
  distro_parent_nbr varchar2(12),
  distro_parent_type varchar2(1),
  item varchar2(25),
  wh number(10),
  loc_type varchar2(1),
  location number(10),
  seq_no number(4),
  wip_code varchar2(9),
  instructions varchar2(300),
  complete_date date,
  completed_qty number(12,4),
  completed_ind varchar2(1),
  WOStatusInvAdj_TBL "RIB_WOStatusInvAdj_TBL",   -- Size of "RIB_WOStatusInvAdj_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOStatusDesc_REC"
(
  rib_oid number
, wo_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, distro_parent_nbr varchar2
, distro_parent_type varchar2
, item varchar2
, wh number
, loc_type varchar2
, location number
, seq_no number
, wip_code varchar2
, instructions varchar2
, complete_date date
, completed_qty number
, completed_ind varchar2
) return self as result
,constructor function "RIB_WOStatusDesc_REC"
(
  rib_oid number
, wo_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, distro_parent_nbr varchar2
, distro_parent_type varchar2
, item varchar2
, wh number
, loc_type varchar2
, location number
, seq_no number
, wip_code varchar2
, instructions varchar2
, complete_date date
, completed_qty number
, completed_ind varchar2
, WOStatusInvAdj_TBL "RIB_WOStatusInvAdj_TBL"  -- Size of "RIB_WOStatusInvAdj_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOStatusDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOStatusDesc') := "ns_name_WOStatusDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'wo_id') := wo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_doc_type') := distro_doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_parent_nbr') := distro_parent_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_parent_type') := distro_parent_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh') := wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'wip_code') := wip_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'instructions') := instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'complete_date') := complete_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_qty') := completed_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_ind') := completed_ind;
  IF WOStatusInvAdj_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WOStatusInvAdj_TBL.';
    FOR INDX IN WOStatusInvAdj_TBL.FIRST()..WOStatusInvAdj_TBL.LAST() LOOP
      WOStatusInvAdj_TBL(indx).appendNodeValues( i_prefix||indx||'WOStatusInvAdj_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_WOStatusDesc_REC"
(
  rib_oid number
, wo_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, distro_parent_nbr varchar2
, distro_parent_type varchar2
, item varchar2
, wh number
, loc_type varchar2
, location number
, seq_no number
, wip_code varchar2
, instructions varchar2
, complete_date date
, completed_qty number
, completed_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.distro_parent_nbr := distro_parent_nbr;
self.distro_parent_type := distro_parent_type;
self.item := item;
self.wh := wh;
self.loc_type := loc_type;
self.location := location;
self.seq_no := seq_no;
self.wip_code := wip_code;
self.instructions := instructions;
self.complete_date := complete_date;
self.completed_qty := completed_qty;
self.completed_ind := completed_ind;
RETURN;
end;
constructor function "RIB_WOStatusDesc_REC"
(
  rib_oid number
, wo_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, distro_parent_nbr varchar2
, distro_parent_type varchar2
, item varchar2
, wh number
, loc_type varchar2
, location number
, seq_no number
, wip_code varchar2
, instructions varchar2
, complete_date date
, completed_qty number
, completed_ind varchar2
, WOStatusInvAdj_TBL "RIB_WOStatusInvAdj_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.distro_parent_nbr := distro_parent_nbr;
self.distro_parent_type := distro_parent_type;
self.item := item;
self.wh := wh;
self.loc_type := loc_type;
self.location := location;
self.seq_no := seq_no;
self.wip_code := wip_code;
self.instructions := instructions;
self.complete_date := complete_date;
self.completed_qty := completed_qty;
self.completed_ind := completed_ind;
self.WOStatusInvAdj_TBL := WOStatusInvAdj_TBL;
RETURN;
end;
END;
/
