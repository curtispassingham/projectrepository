DROP TYPE "RIB_StrVnsDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsDesc_REC";
/
DROP TYPE "RIB_StrVnsBol_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsBol_REC";
/
DROP TYPE "RIB_StrVnsBolCtn_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsBolCtn_REC";
/
DROP TYPE "RIB_StrVnsNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsNote_REC";
/
DROP TYPE "RIB_StrVnsNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsNote_TBL" AS TABLE OF "RIB_StrVnsNote_REC";
/
DROP TYPE "RIB_StrVnsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  store_id number(10),
  supplier_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, SHIPPED, CANCELED, UNKNOWN] (all lower-case)
  return_context varchar2(20), -- return_context is enumeration field, valid values are [INVALID_DELIVERY, SEASONAL, DAMAGED, NONE, UNKNOWN] (all lower-case)
  authorization_code varchar2(12),
  not_after_date date,
  doc_internal_id number(12),
  doc_external_id varchar2(128),
  create_date date,
  create_user varchar2(128),
  update_date date,
  update_user varchar2(128),
  submit_date date,
  submit_user varchar2(128),
  dispatch_date date,
  dispatch_user varchar2(128),
  StrVnsBol "RIB_StrVnsBol_REC",
  StrVnsNote_TBL "RIB_StrVnsNote_TBL",   -- Size of "RIB_StrVnsNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
) return self as result
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
) return self as result
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
) return self as result
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
) return self as result
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
, dispatch_user varchar2
) return self as result
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
, dispatch_user varchar2
, StrVnsBol "RIB_StrVnsBol_REC"
) return self as result
,constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
, dispatch_user varchar2
, StrVnsBol "RIB_StrVnsBol_REC"
, StrVnsNote_TBL "RIB_StrVnsNote_TBL"  -- Size of "RIB_StrVnsNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsDesc') := "ns_name_StrVnsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_context') := return_context;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_internal_id') := doc_internal_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_external_id') := doc_external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'submit_date') := submit_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'submit_user') := submit_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_date') := dispatch_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_user') := dispatch_user;
  l_new_pre :=i_prefix||'StrVnsBol.';
  StrVnsBol.appendNodeValues( i_prefix||'StrVnsBol');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF StrVnsNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVnsNote_TBL.';
    FOR INDX IN StrVnsNote_TBL.FIRST()..StrVnsNote_TBL.LAST() LOOP
      StrVnsNote_TBL(indx).appendNodeValues( i_prefix||indx||'StrVnsNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
RETURN;
end;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.submit_date := submit_date;
RETURN;
end;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.submit_date := submit_date;
self.submit_user := submit_user;
RETURN;
end;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.submit_date := submit_date;
self.submit_user := submit_user;
self.dispatch_date := dispatch_date;
RETURN;
end;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
, dispatch_user varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.submit_date := submit_date;
self.submit_user := submit_user;
self.dispatch_date := dispatch_date;
self.dispatch_user := dispatch_user;
RETURN;
end;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
, dispatch_user varchar2
, StrVnsBol "RIB_StrVnsBol_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.submit_date := submit_date;
self.submit_user := submit_user;
self.dispatch_date := dispatch_date;
self.dispatch_user := dispatch_user;
self.StrVnsBol := StrVnsBol;
RETURN;
end;
constructor function "RIB_StrVnsDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, status varchar2
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, doc_internal_id number
, doc_external_id varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, submit_date date
, submit_user varchar2
, dispatch_date date
, dispatch_user varchar2
, StrVnsBol "RIB_StrVnsBol_REC"
, StrVnsNote_TBL "RIB_StrVnsNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.doc_internal_id := doc_internal_id;
self.doc_external_id := doc_external_id;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.submit_date := submit_date;
self.submit_user := submit_user;
self.dispatch_date := dispatch_date;
self.dispatch_user := dispatch_user;
self.StrVnsBol := StrVnsBol;
self.StrVnsNote_TBL := StrVnsNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrVnsBolCtn_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsBolCtn_TBL" AS TABLE OF "RIB_StrVnsBolCtn_REC";
/
DROP TYPE "RIB_StrVnsBol_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsBol_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bol_id number(12),
  bill_of_lading_motive_id varchar2(120),
  bill_of_lading_tax_id varchar2(18),
  requested_pickup_date date,
  carrier_type varchar2(20), -- carrier_type is enumeration field, valid values are [SENDER, RECEIVER, THIRD_PARTY, UNKNOWN] (all lower-case)
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  carrier_name varchar2(128),
  carrier_address varchar2(2000),
  ship_to_address_type varchar2(10), -- ship_to_address_type is enumeration field, valid values are [BUSINESS, POSTAL, RETURNS, ORDER, INVOICE, REMITTANCE, BILLING, DELIVERY, UNKNOWN] (all lower-case)
  alternate_ship_to_address varchar2(2000),
  StrVnsBolCtn_TBL "RIB_StrVnsBolCtn_TBL",   -- Size of "RIB_StrVnsBolCtn_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsBol_REC"
(
  rib_oid number
, bol_id number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
) return self as result
,constructor function "RIB_StrVnsBol_REC"
(
  rib_oid number
, bol_id number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result
,constructor function "RIB_StrVnsBol_REC"
(
  rib_oid number
, bol_id number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
, StrVnsBolCtn_TBL "RIB_StrVnsBolCtn_TBL"  -- Size of "RIB_StrVnsBolCtn_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsBol_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsDesc') := "ns_name_StrVnsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_id') := bol_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_motive_id') := bill_of_lading_motive_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_tax_id') := bill_of_lading_tax_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pickup_date') := requested_pickup_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_type') := carrier_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_address') := carrier_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_address_type') := ship_to_address_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_ship_to_address') := alternate_ship_to_address;
  IF StrVnsBolCtn_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVnsBolCtn_TBL.';
    FOR INDX IN StrVnsBolCtn_TBL.FIRST()..StrVnsBolCtn_TBL.LAST() LOOP
      StrVnsBolCtn_TBL(indx).appendNodeValues( i_prefix||indx||'StrVnsBolCtn_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsBol_REC"
(
  rib_oid number
, bol_id number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bol_id := bol_id;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
RETURN;
end;
constructor function "RIB_StrVnsBol_REC"
(
  rib_oid number
, bol_id number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bol_id := bol_id;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
RETURN;
end;
constructor function "RIB_StrVnsBol_REC"
(
  rib_oid number
, bol_id number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
, StrVnsBolCtn_TBL "RIB_StrVnsBolCtn_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.bol_id := bol_id;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
self.StrVnsBolCtn_TBL := StrVnsBolCtn_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrVnsBolCtn_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsBolCtn_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  container_id number(15),
  container_label varchar2(128),
  package_weight number(12,4),
  package_weight_uom varchar2(4),
  tracking_number varchar2(128),
  package_type varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
) return self as result
,constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
) return self as result
,constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
) return self as result
,constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
) return self as result
,constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
) return self as result
,constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsBolCtn_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsDesc') := "ns_name_StrVnsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_label') := container_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight') := package_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight_uom') := package_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_type') := package_type;
END appendNodeValues;
constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.container_id := container_id;
RETURN;
end;
constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.container_id := container_id;
self.container_label := container_label;
RETURN;
end;
constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.container_id := container_id;
self.container_label := container_label;
self.package_weight := package_weight;
RETURN;
end;
constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.container_id := container_id;
self.container_label := container_label;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
RETURN;
end;
constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.container_id := container_id;
self.container_label := container_label;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
RETURN;
end;
constructor function "RIB_StrVnsBolCtn_REC"
(
  rib_oid number
, container_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.container_id := container_id;
self.container_label := container_label;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrVnsNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsDesc') := "ns_name_StrVnsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_StrVnsNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
