DROP TYPE "RIB_StrAdjTplDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjTplDesc_REC";
/
DROP TYPE "RIB_StrAdjTplItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjTplItm_REC";
/
DROP TYPE "RIB_StrAdjTplItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjTplItm_TBL" AS TABLE OF "RIB_StrAdjTplItm_REC";
/
DROP TYPE "RIB_StrAdjTplDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrAdjTplDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrAdjTplDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  template_id number(12),
  store_id number(10),
  description varchar2(250),
  status varchar2(20), -- status is enumeration field, valid values are [IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN, NO_VALUE] (all lower-case)
  comments varchar2(2000),
  create_date date,
  create_user_name varchar2(128),
  approve_date date,
  approve_user_name varchar2(128),
  StrAdjTplItm_TBL "RIB_StrAdjTplItm_TBL",   -- Size of "RIB_StrAdjTplItm_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
) return self as result
,constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
, approve_date date
) return self as result
,constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
, approve_date date
, approve_user_name varchar2
) return self as result
,constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
, approve_date date
, approve_user_name varchar2
, StrAdjTplItm_TBL "RIB_StrAdjTplItm_TBL"  -- Size of "RIB_StrAdjTplItm_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrAdjTplDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrAdjTplDesc') := "ns_name_StrAdjTplDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'template_id') := template_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'approve_date') := approve_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'approve_user_name') := approve_user_name;
  IF StrAdjTplItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrAdjTplItm_TBL.';
    FOR INDX IN StrAdjTplItm_TBL.FIRST()..StrAdjTplItm_TBL.LAST() LOOP
      StrAdjTplItm_TBL(indx).appendNodeValues( i_prefix||indx||'StrAdjTplItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.template_id := template_id;
self.store_id := store_id;
self.description := description;
self.status := status;
self.comments := comments;
self.create_date := create_date;
self.create_user_name := create_user_name;
RETURN;
end;
constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
, approve_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.template_id := template_id;
self.store_id := store_id;
self.description := description;
self.status := status;
self.comments := comments;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.approve_date := approve_date;
RETURN;
end;
constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
, approve_date date
, approve_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.template_id := template_id;
self.store_id := store_id;
self.description := description;
self.status := status;
self.comments := comments;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.approve_date := approve_date;
self.approve_user_name := approve_user_name;
RETURN;
end;
constructor function "RIB_StrAdjTplDesc_REC"
(
  rib_oid number
, template_id number
, store_id number
, description varchar2
, status varchar2
, comments varchar2
, create_date date
, create_user_name varchar2
, approve_date date
, approve_user_name varchar2
, StrAdjTplItm_TBL "RIB_StrAdjTplItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.template_id := template_id;
self.store_id := store_id;
self.description := description;
self.status := status;
self.comments := comments;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.approve_date := approve_date;
self.approve_user_name := approve_user_name;
self.StrAdjTplItm_TBL := StrAdjTplItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrAdjTplItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrAdjTplItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrAdjTplDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  reason_id number(12),
  quantity number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrAdjTplItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrAdjTplItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrAdjTplDesc') := "ns_name_StrAdjTplDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_StrAdjTplItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.quantity := quantity;
self.case_size := case_size;
RETURN;
end;
END;
/
