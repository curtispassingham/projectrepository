DROP TYPE "RIB_GiftListItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftListItemDesc_REC";
/
DROP TYPE "RIB_GiftListItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftListItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(40),
  xref_item_id "RIB_xref_item_id_TBL",   -- Size of "RIB_xref_item_id_TBL" is 10
  desired_quantity number(19,4),
  purchased_quantity number(19,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
) return self as result
,constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
, xref_item_id "RIB_xref_item_id_TBL"  -- Size of "RIB_xref_item_id_TBL" is 10
) return self as result
,constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
, xref_item_id "RIB_xref_item_id_TBL"  -- Size of "RIB_xref_item_id_TBL" is 10
, desired_quantity number
) return self as result
,constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
, xref_item_id "RIB_xref_item_id_TBL"  -- Size of "RIB_xref_item_id_TBL" is 10
, desired_quantity number
, purchased_quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftListItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListItemDesc') := "ns_name_GiftListItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  IF xref_item_id IS NOT NULL THEN
    FOR INDX IN xref_item_id.FIRST()..xref_item_id.LAST() LOOP
      l_new_pre :=i_prefix||indx||'xref_item_id'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'xref_item_id'||'.'):=xref_item_id(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'desired_quantity') := desired_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchased_quantity') := purchased_quantity;
END appendNodeValues;
constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
, xref_item_id "RIB_xref_item_id_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.xref_item_id := xref_item_id;
RETURN;
end;
constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
, xref_item_id "RIB_xref_item_id_TBL"
, desired_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.xref_item_id := xref_item_id;
self.desired_quantity := desired_quantity;
RETURN;
end;
constructor function "RIB_GiftListItemDesc_REC"
(
  rib_oid number
, item_id varchar2
, xref_item_id "RIB_xref_item_id_TBL"
, desired_quantity number
, purchased_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.xref_item_id := xref_item_id;
self.desired_quantity := desired_quantity;
self.purchased_quantity := purchased_quantity;
RETURN;
end;
END;
/
DROP TYPE "RIB_xref_item_id_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_xref_item_id_TBL" AS TABLE OF varchar2(40);
/
