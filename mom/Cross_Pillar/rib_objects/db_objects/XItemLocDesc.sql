DROP TYPE "RIB_XItemLocTrt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemLocTrt_REC";
/
DROP TYPE "RIB_XItemLocDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemLocDtl_REC";
/
DROP TYPE "RIB_XItemLocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemLocDesc_REC";
/
DROP TYPE "RIB_XItemLocTrt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemLocTrt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  launch_date date,
  qty_key_options varchar2(6),
  manual_price_entry varchar2(6),
  deposit_code varchar2(6),
  food_stamp_ind varchar2(1),
  wic_ind varchar2(1),
  proportional_tare_pct number(12,4),
  fixed_tare_value number(12,4),
  fixed_tare_uom varchar2(4),
  reward_eligible_ind varchar2(1),
  natl_brand_comp_item varchar2(25),
  return_policy varchar2(6),
  stop_sale_ind varchar2(1),
  elect_mkt_clubs varchar2(6),
  report_code varchar2(6),
  req_shelf_life_on_selection number(4),
  req_shelf_life_on_receipt number(4),
  ib_shelf_life number(4),
  store_reorderable_ind varchar2(1),
  rack_size varchar2(6),
  full_pallet_item varchar2(1),
  in_store_market_basket varchar2(6),
  storage_location varchar2(7),
  alt_storage_location varchar2(7),
  returnable_ind varchar2(1),
  refundable_ind varchar2(1),
  back_order_ind varchar2(1),
  create_datetime date,
  last_update_id varchar2(30),
  last_update_datetime date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
, create_datetime date
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
, create_datetime date
, last_update_id varchar2
) return self as result
,constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
, create_datetime date
, last_update_id varchar2
, last_update_datetime date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemLocTrt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemLocDesc') := "ns_name_XItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'launch_date') := launch_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_key_options') := qty_key_options;
  rib_obj_util.g_RIB_element_values(i_prefix||'manual_price_entry') := manual_price_entry;
  rib_obj_util.g_RIB_element_values(i_prefix||'deposit_code') := deposit_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'food_stamp_ind') := food_stamp_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'wic_ind') := wic_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'proportional_tare_pct') := proportional_tare_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'fixed_tare_value') := fixed_tare_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'fixed_tare_uom') := fixed_tare_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'reward_eligible_ind') := reward_eligible_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'natl_brand_comp_item') := natl_brand_comp_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_policy') := return_policy;
  rib_obj_util.g_RIB_element_values(i_prefix||'stop_sale_ind') := stop_sale_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'elect_mkt_clubs') := elect_mkt_clubs;
  rib_obj_util.g_RIB_element_values(i_prefix||'report_code') := report_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'req_shelf_life_on_selection') := req_shelf_life_on_selection;
  rib_obj_util.g_RIB_element_values(i_prefix||'req_shelf_life_on_receipt') := req_shelf_life_on_receipt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ib_shelf_life') := ib_shelf_life;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_reorderable_ind') := store_reorderable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'rack_size') := rack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'full_pallet_item') := full_pallet_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'in_store_market_basket') := in_store_market_basket;
  rib_obj_util.g_RIB_element_values(i_prefix||'storage_location') := storage_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'alt_storage_location') := alt_storage_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'returnable_ind') := returnable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'refundable_ind') := refundable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'back_order_ind') := back_order_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_id') := last_update_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_datetime') := last_update_datetime;
END appendNodeValues;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
self.returnable_ind := returnable_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
self.returnable_ind := returnable_ind;
self.refundable_ind := refundable_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
self.returnable_ind := returnable_ind;
self.refundable_ind := refundable_ind;
self.back_order_ind := back_order_ind;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
self.returnable_ind := returnable_ind;
self.refundable_ind := refundable_ind;
self.back_order_ind := back_order_ind;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
, create_datetime date
, last_update_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
self.returnable_ind := returnable_ind;
self.refundable_ind := refundable_ind;
self.back_order_ind := back_order_ind;
self.create_datetime := create_datetime;
self.last_update_id := last_update_id;
RETURN;
end;
constructor function "RIB_XItemLocTrt_REC"
(
  rib_oid number
, launch_date date
, qty_key_options varchar2
, manual_price_entry varchar2
, deposit_code varchar2
, food_stamp_ind varchar2
, wic_ind varchar2
, proportional_tare_pct number
, fixed_tare_value number
, fixed_tare_uom varchar2
, reward_eligible_ind varchar2
, natl_brand_comp_item varchar2
, return_policy varchar2
, stop_sale_ind varchar2
, elect_mkt_clubs varchar2
, report_code varchar2
, req_shelf_life_on_selection number
, req_shelf_life_on_receipt number
, ib_shelf_life number
, store_reorderable_ind varchar2
, rack_size varchar2
, full_pallet_item varchar2
, in_store_market_basket varchar2
, storage_location varchar2
, alt_storage_location varchar2
, returnable_ind varchar2
, refundable_ind varchar2
, back_order_ind varchar2
, create_datetime date
, last_update_id varchar2
, last_update_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.launch_date := launch_date;
self.qty_key_options := qty_key_options;
self.manual_price_entry := manual_price_entry;
self.deposit_code := deposit_code;
self.food_stamp_ind := food_stamp_ind;
self.wic_ind := wic_ind;
self.proportional_tare_pct := proportional_tare_pct;
self.fixed_tare_value := fixed_tare_value;
self.fixed_tare_uom := fixed_tare_uom;
self.reward_eligible_ind := reward_eligible_ind;
self.natl_brand_comp_item := natl_brand_comp_item;
self.return_policy := return_policy;
self.stop_sale_ind := stop_sale_ind;
self.elect_mkt_clubs := elect_mkt_clubs;
self.report_code := report_code;
self.req_shelf_life_on_selection := req_shelf_life_on_selection;
self.req_shelf_life_on_receipt := req_shelf_life_on_receipt;
self.ib_shelf_life := ib_shelf_life;
self.store_reorderable_ind := store_reorderable_ind;
self.rack_size := rack_size;
self.full_pallet_item := full_pallet_item;
self.in_store_market_basket := in_store_market_basket;
self.storage_location := storage_location;
self.alt_storage_location := alt_storage_location;
self.returnable_ind := returnable_ind;
self.refundable_ind := refundable_ind;
self.back_order_ind := back_order_ind;
self.create_datetime := create_datetime;
self.last_update_id := last_update_id;
self.last_update_datetime := last_update_datetime;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemLocDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemLocDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_value number(10),
  primary_supp number(10),
  primary_cntry varchar2(3),
  local_item_desc varchar2(250),
  status varchar2(1),
  store_ord_mult varchar2(1),
  receive_as_type varchar2(1),
  taxable_ind varchar2(1),
  ti number(12,4),
  hi number(12,4),
  daily_waste_pct number(12,4),
  local_short_desc varchar2(120),
  XItemLocTrt "RIB_XItemLocTrt_REC",
  uin_type varchar2(6),
  uin_label varchar2(6),
  capture_time varchar2(6),
  ext_uin_ind varchar2(1),
  source_method varchar2(1),
  source_wh number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
, source_method varchar2
) return self as result
,constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
, source_method varchar2
, source_wh number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemLocDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemLocDesc') := "ns_name_XItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_value') := hier_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supp') := primary_supp;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_cntry') := primary_cntry;
  rib_obj_util.g_RIB_element_values(i_prefix||'local_item_desc') := local_item_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_ord_mult') := store_ord_mult;
  rib_obj_util.g_RIB_element_values(i_prefix||'receive_as_type') := receive_as_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxable_ind') := taxable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ti') := ti;
  rib_obj_util.g_RIB_element_values(i_prefix||'hi') := hi;
  rib_obj_util.g_RIB_element_values(i_prefix||'daily_waste_pct') := daily_waste_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'local_short_desc') := local_short_desc;
  l_new_pre :=i_prefix||'XItemLocTrt.';
  XItemLocTrt.appendNodeValues( i_prefix||'XItemLocTrt');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_type') := uin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_label') := uin_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'capture_time') := capture_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_uin_ind') := ext_uin_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_method') := source_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_wh') := source_wh;
END appendNodeValues;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
self.uin_type := uin_type;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
self.uin_type := uin_type;
self.uin_label := uin_label;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.capture_time := capture_time;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.capture_time := capture_time;
self.ext_uin_ind := ext_uin_ind;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
, source_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.capture_time := capture_time;
self.ext_uin_ind := ext_uin_ind;
self.source_method := source_method;
RETURN;
end;
constructor function "RIB_XItemLocDtl_REC"
(
  rib_oid number
, hier_value number
, primary_supp number
, primary_cntry varchar2
, local_item_desc varchar2
, status varchar2
, store_ord_mult varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, ti number
, hi number
, daily_waste_pct number
, local_short_desc varchar2
, XItemLocTrt "RIB_XItemLocTrt_REC"
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
, source_method varchar2
, source_wh number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.local_item_desc := local_item_desc;
self.status := status;
self.store_ord_mult := store_ord_mult;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.ti := ti;
self.hi := hi;
self.daily_waste_pct := daily_waste_pct;
self.local_short_desc := local_short_desc;
self.XItemLocTrt := XItemLocTrt;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.capture_time := capture_time;
self.ext_uin_ind := ext_uin_ind;
self.source_method := source_method;
self.source_wh := source_wh;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemLocDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemLocDtl_TBL" AS TABLE OF "RIB_XItemLocDtl_REC";
/
DROP TYPE "RIB_XItemLocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemLocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  hier_level varchar2(2),
  XItemLocDtl_TBL "RIB_XItemLocDtl_TBL",   -- Size of "RIB_XItemLocDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemLocDesc_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemLocDtl_TBL "RIB_XItemLocDtl_TBL"  -- Size of "RIB_XItemLocDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemLocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemLocDesc') := "ns_name_XItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_level') := hier_level;
  l_new_pre :=i_prefix||'XItemLocDtl_TBL.';
  FOR INDX IN XItemLocDtl_TBL.FIRST()..XItemLocDtl_TBL.LAST() LOOP
    XItemLocDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XItemLocDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_XItemLocDesc_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemLocDtl_TBL "RIB_XItemLocDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemLocDtl_TBL := XItemLocDtl_TBL;
RETURN;
end;
END;
/
