@@EOfInSupplierRef.sql;
/
DROP TYPE "RIB_InSupplierRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierRef_REC";
/
DROP TYPE "RIB_InSupplierSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierSite_REC";
/
DROP TYPE "RIB_InSupplierSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierSiteAddr_REC";
/
DROP TYPE "RIB_InSupplierRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupplierRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupplierRef "RIB_EOfInSupplierRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupplierRef_REC"
(
  rib_oid number
, EOfInSupplierRef "RIB_EOfInSupplierRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupplierRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierRef') := "ns_name_InSupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'EOfInSupplierRef.';
  EOfInSupplierRef.appendNodeValues( i_prefix||'EOfInSupplierRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupplierRef_REC"
(
  rib_oid number
, EOfInSupplierRef "RIB_EOfInSupplierRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupplierRef := EOfInSupplierRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupplierSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupplierSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupplierSite "RIB_EOfInSupplierSite_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupplierSite_REC"
(
  rib_oid number
, EOfInSupplierSite "RIB_EOfInSupplierSite_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupplierSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierRef') := "ns_name_InSupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInSupplierSite.';
  EOfInSupplierSite.appendNodeValues( i_prefix||'EOfInSupplierSite');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupplierSite_REC"
(
  rib_oid number
, EOfInSupplierSite "RIB_EOfInSupplierSite_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupplierSite := EOfInSupplierSite;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupplierSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupplierSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupplierSiteAddr "RIB_EOfInSupplierSiteAddr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupplierSiteAddr_REC"
(
  rib_oid number
, EOfInSupplierSiteAddr "RIB_EOfInSupplierSiteAddr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupplierSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierRef') := "ns_name_InSupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInSupplierSiteAddr.';
  EOfInSupplierSiteAddr.appendNodeValues( i_prefix||'EOfInSupplierSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupplierSiteAddr_REC"
(
  rib_oid number
, EOfInSupplierSiteAddr "RIB_EOfInSupplierSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupplierSiteAddr := EOfInSupplierSiteAddr;
RETURN;
end;
END;
/
