@@SearchAreaDesc.sql;
/
DROP TYPE "RIB_InvAvailCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAvailCriVo_REC";
/
DROP TYPE "RIB_InvLocation_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvLocation_REC";
/
DROP TYPE "RIB_InvLocation_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InvLocation_TBL" AS TABLE OF "RIB_InvLocation_REC";
/
DROP TYPE "RIB_InvAvailCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAvailCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAvailCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  items "RIB_items_TBL",   -- Size of "RIB_items_TBL" is 999
  InvLocation_TBL "RIB_InvLocation_TBL",   -- Size of "RIB_InvLocation_TBL" is 999
  SearchAreaDesc "RIB_SearchAreaDesc_REC",
  store_pickup_ind varchar2(1), -- store_pickup_ind is enumeration field, valid values are [Y, N] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAvailCriVo_REC"
(
  rib_oid number
, items "RIB_items_TBL"  -- Size of "RIB_items_TBL" is 999
, InvLocation_TBL "RIB_InvLocation_TBL"  -- Size of "RIB_InvLocation_TBL" is 999
, SearchAreaDesc "RIB_SearchAreaDesc_REC"
, store_pickup_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAvailCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAvailCriVo') := "ns_name_InvAvailCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF items IS NOT NULL THEN
    FOR INDX IN items.FIRST()..items.LAST() LOOP
      l_new_pre :=i_prefix||indx||'items'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'items'||'.'):=items(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'InvLocation_TBL.';
  FOR INDX IN InvLocation_TBL.FIRST()..InvLocation_TBL.LAST() LOOP
    InvLocation_TBL(indx).appendNodeValues( i_prefix||indx||'InvLocation_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'SearchAreaDesc.';
  SearchAreaDesc.appendNodeValues( i_prefix||'SearchAreaDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_pickup_ind') := store_pickup_ind;
END appendNodeValues;
constructor function "RIB_InvAvailCriVo_REC"
(
  rib_oid number
, items "RIB_items_TBL"
, InvLocation_TBL "RIB_InvLocation_TBL"
, SearchAreaDesc "RIB_SearchAreaDesc_REC"
, store_pickup_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.items := items;
self.InvLocation_TBL := InvLocation_TBL;
self.SearchAreaDesc := SearchAreaDesc;
self.store_pickup_ind := store_pickup_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_items_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_items_TBL" AS TABLE OF varchar2(25);
/
DROP TYPE "RIB_InvLocation_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvLocation_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAvailCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1), -- loc_type is enumeration field, valid values are [S, W] (all lower-case)
  channel_id number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvLocation_REC"
(
  rib_oid number
, location number
, loc_type varchar2
) return self as result
,constructor function "RIB_InvLocation_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, channel_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvLocation_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAvailCriVo') := "ns_name_InvAvailCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
END appendNodeValues;
constructor function "RIB_InvLocation_REC"
(
  rib_oid number
, location number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
RETURN;
end;
constructor function "RIB_InvLocation_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, channel_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.channel_id := channel_id;
RETURN;
end;
END;
/
