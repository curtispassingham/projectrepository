@@EOfBrXItemDesc.sql;
/
@@NameValPairRBO.sql;
/
DROP TYPE "RIB_BrXItemVATDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemVATDesc_REC";
/
DROP TYPE "RIB_BrXISCLocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXISCLocDesc_REC";
/
DROP TYPE "RIB_BrXItemUDADtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemUDADtl_REC";
/
DROP TYPE "RIB_BrXItemImage_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemImage_REC";
/
DROP TYPE "RIB_BrXItemSeason_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemSeason_REC";
/
DROP TYPE "RIB_BrXIZPDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXIZPDesc_REC";
/
DROP TYPE "RIB_BrXISCDimDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXISCDimDesc_REC";
/
DROP TYPE "RIB_BrXItemBOMDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemBOMDesc_REC";
/
DROP TYPE "RIB_BrXItemSupCtyDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemSupCtyDesc_REC";
/
DROP TYPE "RIB_BrXItemSupDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemSupDesc_REC";
/
DROP TYPE "RIB_BrXItemCtryDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemCtryDesc_REC";
/
DROP TYPE "RIB_BrXItemCostDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemCostDesc_REC";
/
DROP TYPE "RIB_BrXItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemDesc_REC";
/
DROP TYPE "RIB_BrXItemVATDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemVATDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemVATDesc "RIB_EOfBrXItemVATDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemVATDesc_REC"
(
  rib_oid number
, EOfBrXItemVATDesc "RIB_EOfBrXItemVATDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemVATDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemVATDesc.';
  EOfBrXItemVATDesc.appendNodeValues( i_prefix||'EOfBrXItemVATDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemVATDesc_REC"
(
  rib_oid number
, EOfBrXItemVATDesc "RIB_EOfBrXItemVATDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemVATDesc := EOfBrXItemVATDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXISCLocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXISCLocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXISCLocDesc "RIB_EOfBrXISCLocDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXISCLocDesc_REC"
(
  rib_oid number
, EOfBrXISCLocDesc "RIB_EOfBrXISCLocDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXISCLocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXISCLocDesc.';
  EOfBrXISCLocDesc.appendNodeValues( i_prefix||'EOfBrXISCLocDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXISCLocDesc_REC"
(
  rib_oid number
, EOfBrXISCLocDesc "RIB_EOfBrXISCLocDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXISCLocDesc := EOfBrXISCLocDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemUDADtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemUDADtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemUDADtl "RIB_EOfBrXItemUDADtl_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemUDADtl_REC"
(
  rib_oid number
, EOfBrXItemUDADtl "RIB_EOfBrXItemUDADtl_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemUDADtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemUDADtl.';
  EOfBrXItemUDADtl.appendNodeValues( i_prefix||'EOfBrXItemUDADtl');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemUDADtl_REC"
(
  rib_oid number
, EOfBrXItemUDADtl "RIB_EOfBrXItemUDADtl_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemUDADtl := EOfBrXItemUDADtl;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemImage_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemImage_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemImage "RIB_EOfBrXItemImage_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemImage_REC"
(
  rib_oid number
, EOfBrXItemImage "RIB_EOfBrXItemImage_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemImage_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemImage.';
  EOfBrXItemImage.appendNodeValues( i_prefix||'EOfBrXItemImage');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemImage_REC"
(
  rib_oid number
, EOfBrXItemImage "RIB_EOfBrXItemImage_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemImage := EOfBrXItemImage;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemSeason_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemSeason_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemSeason "RIB_EOfBrXItemSeason_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemSeason_REC"
(
  rib_oid number
, EOfBrXItemSeason "RIB_EOfBrXItemSeason_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemSeason_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemSeason.';
  EOfBrXItemSeason.appendNodeValues( i_prefix||'EOfBrXItemSeason');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemSeason_REC"
(
  rib_oid number
, EOfBrXItemSeason "RIB_EOfBrXItemSeason_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemSeason := EOfBrXItemSeason;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXIZPDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXIZPDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXIZPDesc "RIB_EOfBrXIZPDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXIZPDesc_REC"
(
  rib_oid number
, EOfBrXIZPDesc "RIB_EOfBrXIZPDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXIZPDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXIZPDesc.';
  EOfBrXIZPDesc.appendNodeValues( i_prefix||'EOfBrXIZPDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXIZPDesc_REC"
(
  rib_oid number
, EOfBrXIZPDesc "RIB_EOfBrXIZPDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXIZPDesc := EOfBrXIZPDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXISCDimDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXISCDimDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXISCDimDesc "RIB_EOfBrXISCDimDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXISCDimDesc_REC"
(
  rib_oid number
, EOfBrXISCDimDesc "RIB_EOfBrXISCDimDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXISCDimDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXISCDimDesc.';
  EOfBrXISCDimDesc.appendNodeValues( i_prefix||'EOfBrXISCDimDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXISCDimDesc_REC"
(
  rib_oid number
, EOfBrXISCDimDesc "RIB_EOfBrXISCDimDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXISCDimDesc := EOfBrXISCDimDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemBOMDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemBOMDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemBOMDesc "RIB_EOfBrXItemBOMDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemBOMDesc_REC"
(
  rib_oid number
, EOfBrXItemBOMDesc "RIB_EOfBrXItemBOMDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemBOMDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemBOMDesc.';
  EOfBrXItemBOMDesc.appendNodeValues( i_prefix||'EOfBrXItemBOMDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemBOMDesc_REC"
(
  rib_oid number
, EOfBrXItemBOMDesc "RIB_EOfBrXItemBOMDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemBOMDesc := EOfBrXItemBOMDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemSupCtyDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemSupCtyDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemSupCtyDesc "RIB_EOfBrXItemSupCtyDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemSupCtyDesc_REC"
(
  rib_oid number
, EOfBrXItemSupCtyDesc "RIB_EOfBrXItemSupCtyDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemSupCtyDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemSupCtyDesc.';
  EOfBrXItemSupCtyDesc.appendNodeValues( i_prefix||'EOfBrXItemSupCtyDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemSupCtyDesc_REC"
(
  rib_oid number
, EOfBrXItemSupCtyDesc "RIB_EOfBrXItemSupCtyDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemSupCtyDesc := EOfBrXItemSupCtyDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  origin_code varchar2(6),
  EOfBrXItemSupDesc "RIB_EOfBrXItemSupDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemSupDesc_REC"
(
  rib_oid number
, origin_code varchar2
) return self as result
,constructor function "RIB_BrXItemSupDesc_REC"
(
  rib_oid number
, origin_code varchar2
, EOfBrXItemSupDesc "RIB_EOfBrXItemSupDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_code') := origin_code;
  l_new_pre :=i_prefix||'EOfBrXItemSupDesc.';
  EOfBrXItemSupDesc.appendNodeValues( i_prefix||'EOfBrXItemSupDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemSupDesc_REC"
(
  rib_oid number
, origin_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_code := origin_code;
RETURN;
end;
constructor function "RIB_BrXItemSupDesc_REC"
(
  rib_oid number
, origin_code varchar2
, EOfBrXItemSupDesc "RIB_EOfBrXItemSupDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_code := origin_code;
self.EOfBrXItemSupDesc := EOfBrXItemSupDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_BrXItemCtryDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemCtryDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  service_ind varchar2(1),
  origin_code varchar2(6),
  classification_id varchar2(25),
  ncm_char_code varchar2(25),
  ex_ipi varchar2(25),
  pauta_code varchar2(25),
  service_code varchar2(25),
  federal_service varchar2(25),
  state_of_manufacture varchar2(3),
  pharma_list_type varchar2(6),
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  EOfBrXItemCtryDesc "RIB_EOfBrXItemCtryDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, EOfBrXItemCtryDesc "RIB_EOfBrXItemCtryDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemCtryDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'service_ind') := service_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_code') := origin_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'classification_id') := classification_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ncm_char_code') := ncm_char_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ex_ipi') := ex_ipi;
  rib_obj_util.g_RIB_element_values(i_prefix||'pauta_code') := pauta_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'service_code') := service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'federal_service') := federal_service;
  rib_obj_util.g_RIB_element_values(i_prefix||'state_of_manufacture') := state_of_manufacture;
  rib_obj_util.g_RIB_element_values(i_prefix||'pharma_list_type') := pharma_list_type;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'EOfBrXItemCtryDesc.';
  EOfBrXItemCtryDesc.appendNodeValues( i_prefix||'EOfBrXItemCtryDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
self.service_code := service_code;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
self.service_code := service_code;
self.federal_service := federal_service;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
self.service_code := service_code;
self.federal_service := federal_service;
self.state_of_manufacture := state_of_manufacture;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
self.service_code := service_code;
self.federal_service := federal_service;
self.state_of_manufacture := state_of_manufacture;
self.pharma_list_type := pharma_list_type;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
self.service_code := service_code;
self.federal_service := federal_service;
self.state_of_manufacture := state_of_manufacture;
self.pharma_list_type := pharma_list_type;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
constructor function "RIB_BrXItemCtryDesc_REC"
(
  rib_oid number
, service_ind varchar2
, origin_code varchar2
, classification_id varchar2
, ncm_char_code varchar2
, ex_ipi varchar2
, pauta_code varchar2
, service_code varchar2
, federal_service varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, EOfBrXItemCtryDesc "RIB_EOfBrXItemCtryDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.service_ind := service_ind;
self.origin_code := origin_code;
self.classification_id := classification_id;
self.ncm_char_code := ncm_char_code;
self.ex_ipi := ex_ipi;
self.pauta_code := pauta_code;
self.service_code := service_code;
self.federal_service := federal_service;
self.state_of_manufacture := state_of_manufacture;
self.pharma_list_type := pharma_list_type;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.EOfBrXItemCtryDesc := EOfBrXItemCtryDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemCostDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemCostDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemCostDesc "RIB_EOfBrXItemCostDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemCostDesc_REC"
(
  rib_oid number
, EOfBrXItemCostDesc "RIB_EOfBrXItemCostDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemCostDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrXItemCostDesc.';
  EOfBrXItemCostDesc.appendNodeValues( i_prefix||'EOfBrXItemCostDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemCostDesc_REC"
(
  rib_oid number
, EOfBrXItemCostDesc "RIB_EOfBrXItemCostDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemCostDesc := EOfBrXItemCostDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrXItemDesc "RIB_EOfBrXItemDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXItemDesc_REC"
(
  rib_oid number
, EOfBrXItemDesc "RIB_EOfBrXItemDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXItemDesc') := "ns_name_BrXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'EOfBrXItemDesc.';
  EOfBrXItemDesc.appendNodeValues( i_prefix||'EOfBrXItemDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrXItemDesc_REC"
(
  rib_oid number
, EOfBrXItemDesc "RIB_EOfBrXItemDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrXItemDesc := EOfBrXItemDesc;
RETURN;
end;
END;
/
