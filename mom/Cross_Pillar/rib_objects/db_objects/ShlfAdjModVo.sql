DROP TYPE "RIB_ShlfAdjModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfAdjModVo_REC";
/
DROP TYPE "RIB_ShlfAdjModItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfAdjModItm_REC";
/
DROP TYPE "RIB_ShlfAdjModItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfAdjModItm_TBL" AS TABLE OF "RIB_ShlfAdjModItm_REC";
/
DROP TYPE "RIB_ShlfAdjModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfAdjModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfAdjModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  adjustment_id number(15),
  store_id number(10),
  user_name varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, COMPLETE, UNKNOWN] (all lower-case)
  type varchar2(20), -- type is enumeration field, valid values are [SHOP_FLOOR_ADJUST, BACK_ROOM_ADJUST, DISPLAY_LIST, ADHOC_REPLENISH, UNKNOWN] (all lower-case)
  ShlfAdjModItm_TBL "RIB_ShlfAdjModItm_TBL",   -- Size of "RIB_ShlfAdjModItm_TBL" is 999
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, user_name varchar2
, status varchar2
, type varchar2
) return self as result
,constructor function "RIB_ShlfAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, user_name varchar2
, status varchar2
, type varchar2
, ShlfAdjModItm_TBL "RIB_ShlfAdjModItm_TBL"  -- Size of "RIB_ShlfAdjModItm_TBL" is 999
) return self as result
,constructor function "RIB_ShlfAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, user_name varchar2
, status varchar2
, type varchar2
, ShlfAdjModItm_TBL "RIB_ShlfAdjModItm_TBL"  -- Size of "RIB_ShlfAdjModItm_TBL" is 999
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfAdjModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfAdjModVo') := "ns_name_ShlfAdjModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'adjustment_id') := adjustment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  IF ShlfAdjModItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ShlfAdjModItm_TBL.';
    FOR INDX IN ShlfAdjModItm_TBL.FIRST()..ShlfAdjModItm_TBL.LAST() LOOP
      ShlfAdjModItm_TBL(indx).appendNodeValues( i_prefix||indx||'ShlfAdjModItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ShlfAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, user_name varchar2
, status varchar2
, type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.user_name := user_name;
self.status := status;
self.type := type;
RETURN;
end;
constructor function "RIB_ShlfAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, user_name varchar2
, status varchar2
, type varchar2
, ShlfAdjModItm_TBL "RIB_ShlfAdjModItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.user_name := user_name;
self.status := status;
self.type := type;
self.ShlfAdjModItm_TBL := ShlfAdjModItm_TBL;
RETURN;
end;
constructor function "RIB_ShlfAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, user_name varchar2
, status varchar2
, type varchar2
, ShlfAdjModItm_TBL "RIB_ShlfAdjModItm_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.user_name := user_name;
self.status := status;
self.type := type;
self.ShlfAdjModItm_TBL := ShlfAdjModItm_TBL;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_ShlfAdjModItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfAdjModItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfAdjModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  requested_pick_amount number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfAdjModItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, requested_pick_amount number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfAdjModItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfAdjModVo') := "ns_name_ShlfAdjModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pick_amount') := requested_pick_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_ShlfAdjModItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, requested_pick_amount number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.requested_pick_amount := requested_pick_amount;
self.case_size := case_size;
RETURN;
end;
END;
/
