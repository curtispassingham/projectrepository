DROP TYPE "RIB_FiscalFDNRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscalFDNRBO_REC";
/
DROP TYPE "RIB_FiscalFDNRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscalFDNRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscalFDNRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  fiscal_code varchar2(30),
  fiscal_code_description varchar2(1000),
  fiscal_parent_code varchar2(30),
  fiscal_extended_parent_code varchar2(30),
  origin_state varchar2(30),
  destination_state varchar2(30),
  creation_date date,
  effective_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
) return self as result
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
) return self as result
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
) return self as result
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
) return self as result
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
, destination_state varchar2
) return self as result
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
, destination_state varchar2
, creation_date date
) return self as result
,constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
, destination_state varchar2
, creation_date date
, effective_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscalFDNRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscalFDNRBO') := "ns_name_FiscalFDNRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_code') := fiscal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_code_description') := fiscal_code_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_parent_code') := fiscal_parent_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_extended_parent_code') := fiscal_extended_parent_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_state') := origin_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_state') := destination_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'creation_date') := creation_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
END appendNodeValues;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
RETURN;
end;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
self.fiscal_parent_code := fiscal_parent_code;
RETURN;
end;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
self.fiscal_parent_code := fiscal_parent_code;
self.fiscal_extended_parent_code := fiscal_extended_parent_code;
RETURN;
end;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
self.fiscal_parent_code := fiscal_parent_code;
self.fiscal_extended_parent_code := fiscal_extended_parent_code;
self.origin_state := origin_state;
RETURN;
end;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
, destination_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
self.fiscal_parent_code := fiscal_parent_code;
self.fiscal_extended_parent_code := fiscal_extended_parent_code;
self.origin_state := origin_state;
self.destination_state := destination_state;
RETURN;
end;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
, destination_state varchar2
, creation_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
self.fiscal_parent_code := fiscal_parent_code;
self.fiscal_extended_parent_code := fiscal_extended_parent_code;
self.origin_state := origin_state;
self.destination_state := destination_state;
self.creation_date := creation_date;
RETURN;
end;
constructor function "RIB_FiscalFDNRBO_REC"
(
  rib_oid number
, fiscal_code varchar2
, fiscal_code_description varchar2
, fiscal_parent_code varchar2
, fiscal_extended_parent_code varchar2
, origin_state varchar2
, destination_state varchar2
, creation_date date
, effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.fiscal_code_description := fiscal_code_description;
self.fiscal_parent_code := fiscal_parent_code;
self.fiscal_extended_parent_code := fiscal_extended_parent_code;
self.origin_state := origin_state;
self.destination_state := destination_state;
self.creation_date := creation_date;
self.effective_date := effective_date;
RETURN;
end;
END;
/
