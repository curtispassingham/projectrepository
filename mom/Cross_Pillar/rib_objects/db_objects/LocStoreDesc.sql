DROP TYPE "RIB_LocStoreDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocStoreDesc_REC";
/
DROP TYPE "RIB_LocStoreDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocStoreDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocStoreDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  name varchar2(150),
  language varchar2(2),
  country varchar2(2),
  currency_code varchar2(40),
  transfer_zone varchar2(128),
  timezone varchar2(80),
  is_managed varchar2(5), --is_managed is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocStoreDesc_REC"
(
  rib_oid number
, store_id number
, name varchar2
, language varchar2
, country varchar2
, currency_code varchar2
, transfer_zone varchar2
, timezone varchar2
, is_managed varchar2  --is_managed is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocStoreDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocStoreDesc') := "ns_name_LocStoreDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'language') := language;
  rib_obj_util.g_RIB_element_values(i_prefix||'country') := country;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_zone') := transfer_zone;
  rib_obj_util.g_RIB_element_values(i_prefix||'timezone') := timezone;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_managed') := is_managed;
END appendNodeValues;
constructor function "RIB_LocStoreDesc_REC"
(
  rib_oid number
, store_id number
, name varchar2
, language varchar2
, country varchar2
, currency_code varchar2
, transfer_zone varchar2
, timezone varchar2
, is_managed varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.name := name;
self.language := language;
self.country := country;
self.currency_code := currency_code;
self.transfer_zone := transfer_zone;
self.timezone := timezone;
self.is_managed := is_managed;
RETURN;
end;
END;
/
