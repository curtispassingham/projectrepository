@@PhoneDesc.sql;
/
@@EmailDesc.sql;
/
DROP TYPE "RIB_ContactDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ContactDesc_REC";
/
DROP TYPE "RIB_Phones_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Phones_REC";
/
DROP TYPE "RIB_Emails_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Emails_REC";
/
DROP TYPE "RIB_ContactDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ContactDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ContactDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  contact_id number(22),
  first_name varchar2(120),
  last_name varchar2(120),
  middle_name varchar2(120),
  preferred_name varchar2(120),
  full_name varchar2(250),
  name_prefix varchar2(120),
  name_suffix varchar2(120),
  fax_number varchar2(120),
  company_name varchar2(250),
  Phones "RIB_Phones_REC",
  Emails "RIB_Emails_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
, company_name varchar2
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
, company_name varchar2
, Phones "RIB_Phones_REC"
) return self as result
,constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
, company_name varchar2
, Phones "RIB_Phones_REC"
, Emails "RIB_Emails_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ContactDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ContactDesc') := "ns_name_ContactDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_id') := contact_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'middle_name') := middle_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'preferred_name') := preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'full_name') := full_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'name_prefix') := name_prefix;
  rib_obj_util.g_RIB_element_values(i_prefix||'name_suffix') := name_suffix;
  rib_obj_util.g_RIB_element_values(i_prefix||'fax_number') := fax_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'company_name') := company_name;
  l_new_pre :=i_prefix||'Phones.';
  Phones.appendNodeValues( i_prefix||'Phones');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'Emails.';
  Emails.appendNodeValues( i_prefix||'Emails');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
self.name_prefix := name_prefix;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
self.name_prefix := name_prefix;
self.name_suffix := name_suffix;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
self.name_prefix := name_prefix;
self.name_suffix := name_suffix;
self.fax_number := fax_number;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
, company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
self.name_prefix := name_prefix;
self.name_suffix := name_suffix;
self.fax_number := fax_number;
self.company_name := company_name;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
, company_name varchar2
, Phones "RIB_Phones_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
self.name_prefix := name_prefix;
self.name_suffix := name_suffix;
self.fax_number := fax_number;
self.company_name := company_name;
self.Phones := Phones;
RETURN;
end;
constructor function "RIB_ContactDesc_REC"
(
  rib_oid number
, contact_id number
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, preferred_name varchar2
, full_name varchar2
, name_prefix varchar2
, name_suffix varchar2
, fax_number varchar2
, company_name varchar2
, Phones "RIB_Phones_REC"
, Emails "RIB_Emails_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.contact_id := contact_id;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.preferred_name := preferred_name;
self.full_name := full_name;
self.name_prefix := name_prefix;
self.name_suffix := name_suffix;
self.fax_number := fax_number;
self.company_name := company_name;
self.Phones := Phones;
self.Emails := Emails;
RETURN;
end;
END;
/
DROP TYPE "RIB_PhoneDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PhoneDesc_TBL" AS TABLE OF "RIB_PhoneDesc_REC";
/
DROP TYPE "RIB_Phones_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Phones_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ContactDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  PhoneDesc_TBL "RIB_PhoneDesc_TBL",   -- Size of "RIB_PhoneDesc_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Phones_REC"
(
  rib_oid number
, PhoneDesc_TBL "RIB_PhoneDesc_TBL"  -- Size of "RIB_PhoneDesc_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Phones_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ContactDesc') := "ns_name_ContactDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF PhoneDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PhoneDesc_TBL.';
    FOR INDX IN PhoneDesc_TBL.FIRST()..PhoneDesc_TBL.LAST() LOOP
      PhoneDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PhoneDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_Phones_REC"
(
  rib_oid number
, PhoneDesc_TBL "RIB_PhoneDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.PhoneDesc_TBL := PhoneDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_EmailDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_EmailDesc_TBL" AS TABLE OF "RIB_EmailDesc_REC";
/
DROP TYPE "RIB_Emails_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Emails_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ContactDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EmailDesc_TBL "RIB_EmailDesc_TBL",   -- Size of "RIB_EmailDesc_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Emails_REC"
(
  rib_oid number
, EmailDesc_TBL "RIB_EmailDesc_TBL"  -- Size of "RIB_EmailDesc_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Emails_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ContactDesc') := "ns_name_ContactDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF EmailDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'EmailDesc_TBL.';
    FOR INDX IN EmailDesc_TBL.FIRST()..EmailDesc_TBL.LAST() LOOP
      EmailDesc_TBL(indx).appendNodeValues( i_prefix||indx||'EmailDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_Emails_REC"
(
  rib_oid number
, EmailDesc_TBL "RIB_EmailDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EmailDesc_TBL := EmailDesc_TBL;
RETURN;
end;
END;
/
