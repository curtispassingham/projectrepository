@@PromoLineDesc.sql;
/
@@PrcOvdLineDesc.sql;
/
@@DiscntLineColDesc.sql;
/
@@TaxLineColDesc.sql;
/
DROP TYPE "RIB_CustOrdItmDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrdItmDesc_REC";
/
DROP TYPE "RIB_GiftCardItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftCardItem_REC";
/
DROP TYPE "RIB_AlterationItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AlterationItem_REC";
/
DROP TYPE "RIB_CustOrdItmDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrdItmDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdItmDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_item_no number(4),
  captured_line_item_no number(4),
  item_id varchar2(25),
  item_upc varchar2(25),
  item_description varchar2(250),
  quantity number(12,4),
  available_quantity number(12,4),
  completed_quantity number(12,4),
  cancelled_quantity number(12,4),
  returned_quantity number(12,4),
  unit_of_measure varchar2(4),
  currency_code varchar2(3),
  item_total number(20,4),
  unit_sell_price number(20,4),
  unit_regular_price number(20,4),
  completed_amount number(20,4),
  cancelled_amount number(20,4),
  returned_amount number(20,4),
  paid_amount number(20,4),
  discount_total number(20,4),
  completed_discount_amount number(20,4),
  cancelled_discount_amount number(20,4),
  returned_discount_amount number(20,4),
  tax_total number(20,4),
  completed_tax_amount number(20,4),
  cancelled_tax_amount number(20,4),
  returned_tax_amount number(20,4),
  inclusive_tax_total number(20,4),
  completed_inclusive_tax_amount number(20,4),
  cancelled_inclusive_tax_amount number(20,4),
  returned_inclusive_tax_amount number(20,4),
  PrcOvdLineDesc "RIB_PrcOvdLineDesc_REC",
  PromoLineDesc "RIB_PromoLineDesc_REC",
  DiscntLineColDesc "RIB_DiscntLineColDesc_REC",
  TaxLineColDesc "RIB_TaxLineColDesc_REC",
  department_id varchar2(14),
  restrictive_age number(22),
  discountable_flag varchar2(1), -- discountable_flag is enumeration field, valid values are [Y, N] (all lower-case)
  damage_discountable_flag varchar2(1), -- damage_discountable_flag is enumeration field, valid values are [Y, N] (all lower-case)
  employee_discountable_flag varchar2(1), -- employee_discountable_flag is enumeration field, valid values are [Y, N] (all lower-case)
  item_type varchar2(4), -- item_type is enumeration field, valid values are [STCK, SERV, CPON] (all lower-case)
  manufacturer_upc varchar2(14),
  merchandise_hierarchy_group_id varchar2(14),
  product_group varchar2(4), -- product_group is enumeration field, valid values are [GCRD, ALTR, GCRT] (all lower-case)
  restocking_fee_flag varchar2(1), -- restocking_fee_flag is enumeration field, valid values are [Y, N] (all lower-case)
  return_eligible_flag varchar2(1), -- return_eligible_flag is enumeration field, valid values are [Y, N] (all lower-case)
  serialized_item_flag varchar2(1), -- serialized_item_flag is enumeration field, valid values are [Y, N] (all lower-case)
  validate_serial_number_flag varchar2(1), -- validate_serial_number_flag is enumeration field, valid values are [Y, N] (all lower-case)
  allow_new_serial_number_flag varchar2(1), -- allow_new_serial_number_flag is enumeration field, valid values are [Y, N] (all lower-case)
  size_required_flag varchar2(1), -- size_required_flag is enumeration field, valid values are [Y, N] (all lower-case)
  tax_group_id number(22),
  taxable_flag varchar2(1), -- taxable_flag is enumeration field, valid values are [Y, N] (all lower-case)
  weight number(12,4),
  serial_number varchar2(40),
  size_code varchar2(10),
  gift_registry_id varchar2(14),
  gift_receipted_item_flag varchar2(1), -- gift_receipted_item_flag is enumeration field, valid values are [Y, N] (all lower-case)
  shipping_charge_flag varchar2(1), -- shipping_charge_flag is enumeration field, valid values are [Y, N] (all lower-case)
  entry_method varchar2(7), -- entry_method is enumeration field, valid values are [MANUAL, SCAN, MICR, SWIPE, ICC, WAVE, AUTO, ICCFLBK] (all lower-case)
  AlterationItem "RIB_AlterationItem_REC",
  GiftCardItem "RIB_GiftCardItem_REC",
  fulfillment_seq_no number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrdItmDesc_REC"
(
  rib_oid number
, line_item_no number
, captured_line_item_no number
, item_id varchar2
, item_upc varchar2
, item_description varchar2
, quantity number
, available_quantity number
, completed_quantity number
, cancelled_quantity number
, returned_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, item_total number
, unit_sell_price number
, unit_regular_price number
, completed_amount number
, cancelled_amount number
, returned_amount number
, paid_amount number
, discount_total number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, tax_total number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_total number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, PrcOvdLineDesc "RIB_PrcOvdLineDesc_REC"
, PromoLineDesc "RIB_PromoLineDesc_REC"
, DiscntLineColDesc "RIB_DiscntLineColDesc_REC"
, TaxLineColDesc "RIB_TaxLineColDesc_REC"
, department_id varchar2
, restrictive_age number
, discountable_flag varchar2
, damage_discountable_flag varchar2
, employee_discountable_flag varchar2
, item_type varchar2
, manufacturer_upc varchar2
, merchandise_hierarchy_group_id varchar2
, product_group varchar2
, restocking_fee_flag varchar2
, return_eligible_flag varchar2
, serialized_item_flag varchar2
, validate_serial_number_flag varchar2
, allow_new_serial_number_flag varchar2
, size_required_flag varchar2
, tax_group_id number
, taxable_flag varchar2
, weight number
, serial_number varchar2
, size_code varchar2
, gift_registry_id varchar2
, gift_receipted_item_flag varchar2
, shipping_charge_flag varchar2
, entry_method varchar2
, AlterationItem "RIB_AlterationItem_REC"
, GiftCardItem "RIB_GiftCardItem_REC"
, fulfillment_seq_no number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrdItmDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdItmDesc') := "ns_name_CustOrdItmDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_no') := line_item_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'captured_line_item_no') := captured_line_item_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_upc') := item_upc;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'available_quantity') := available_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_quantity') := completed_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_quantity') := cancelled_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_quantity') := returned_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_total') := item_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_sell_price') := unit_sell_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_regular_price') := unit_regular_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_amount') := completed_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_amount') := cancelled_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_amount') := returned_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'paid_amount') := paid_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_total') := discount_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_discount_amount') := completed_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_discount_amount') := cancelled_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_discount_amount') := returned_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_total') := tax_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_tax_amount') := completed_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_tax_amount') := cancelled_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_tax_amount') := returned_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'inclusive_tax_total') := inclusive_tax_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_inclusive_tax_amount') := completed_inclusive_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_inclusive_tax_amount') := cancelled_inclusive_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_inclusive_tax_amount') := returned_inclusive_tax_amount;
  l_new_pre :=i_prefix||'PrcOvdLineDesc.';
  PrcOvdLineDesc.appendNodeValues( i_prefix||'PrcOvdLineDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'PromoLineDesc.';
  PromoLineDesc.appendNodeValues( i_prefix||'PromoLineDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'DiscntLineColDesc.';
  DiscntLineColDesc.appendNodeValues( i_prefix||'DiscntLineColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'TaxLineColDesc.';
  TaxLineColDesc.appendNodeValues( i_prefix||'TaxLineColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'restrictive_age') := restrictive_age;
  rib_obj_util.g_RIB_element_values(i_prefix||'discountable_flag') := discountable_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'damage_discountable_flag') := damage_discountable_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'employee_discountable_flag') := employee_discountable_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_type') := item_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'manufacturer_upc') := manufacturer_upc;
  rib_obj_util.g_RIB_element_values(i_prefix||'merchandise_hierarchy_group_id') := merchandise_hierarchy_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group') := product_group;
  rib_obj_util.g_RIB_element_values(i_prefix||'restocking_fee_flag') := restocking_fee_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_eligible_flag') := return_eligible_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'serialized_item_flag') := serialized_item_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'validate_serial_number_flag') := validate_serial_number_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'allow_new_serial_number_flag') := allow_new_serial_number_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'size_required_flag') := size_required_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_group_id') := tax_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxable_flag') := taxable_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_number') := serial_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'size_code') := size_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_registry_id') := gift_registry_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_receipted_item_flag') := gift_receipted_item_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_charge_flag') := shipping_charge_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'entry_method') := entry_method;
  l_new_pre :=i_prefix||'AlterationItem.';
  AlterationItem.appendNodeValues( i_prefix||'AlterationItem');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftCardItem.';
  GiftCardItem.appendNodeValues( i_prefix||'GiftCardItem');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_seq_no') := fulfillment_seq_no;
END appendNodeValues;
constructor function "RIB_CustOrdItmDesc_REC"
(
  rib_oid number
, line_item_no number
, captured_line_item_no number
, item_id varchar2
, item_upc varchar2
, item_description varchar2
, quantity number
, available_quantity number
, completed_quantity number
, cancelled_quantity number
, returned_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, item_total number
, unit_sell_price number
, unit_regular_price number
, completed_amount number
, cancelled_amount number
, returned_amount number
, paid_amount number
, discount_total number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, tax_total number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_total number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, PrcOvdLineDesc "RIB_PrcOvdLineDesc_REC"
, PromoLineDesc "RIB_PromoLineDesc_REC"
, DiscntLineColDesc "RIB_DiscntLineColDesc_REC"
, TaxLineColDesc "RIB_TaxLineColDesc_REC"
, department_id varchar2
, restrictive_age number
, discountable_flag varchar2
, damage_discountable_flag varchar2
, employee_discountable_flag varchar2
, item_type varchar2
, manufacturer_upc varchar2
, merchandise_hierarchy_group_id varchar2
, product_group varchar2
, restocking_fee_flag varchar2
, return_eligible_flag varchar2
, serialized_item_flag varchar2
, validate_serial_number_flag varchar2
, allow_new_serial_number_flag varchar2
, size_required_flag varchar2
, tax_group_id number
, taxable_flag varchar2
, weight number
, serial_number varchar2
, size_code varchar2
, gift_registry_id varchar2
, gift_receipted_item_flag varchar2
, shipping_charge_flag varchar2
, entry_method varchar2
, AlterationItem "RIB_AlterationItem_REC"
, GiftCardItem "RIB_GiftCardItem_REC"
, fulfillment_seq_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.captured_line_item_no := captured_line_item_no;
self.item_id := item_id;
self.item_upc := item_upc;
self.item_description := item_description;
self.quantity := quantity;
self.available_quantity := available_quantity;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.returned_quantity := returned_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.item_total := item_total;
self.unit_sell_price := unit_sell_price;
self.unit_regular_price := unit_regular_price;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.paid_amount := paid_amount;
self.discount_total := discount_total;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.tax_total := tax_total;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_total := inclusive_tax_total;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.PrcOvdLineDesc := PrcOvdLineDesc;
self.PromoLineDesc := PromoLineDesc;
self.DiscntLineColDesc := DiscntLineColDesc;
self.TaxLineColDesc := TaxLineColDesc;
self.department_id := department_id;
self.restrictive_age := restrictive_age;
self.discountable_flag := discountable_flag;
self.damage_discountable_flag := damage_discountable_flag;
self.employee_discountable_flag := employee_discountable_flag;
self.item_type := item_type;
self.manufacturer_upc := manufacturer_upc;
self.merchandise_hierarchy_group_id := merchandise_hierarchy_group_id;
self.product_group := product_group;
self.restocking_fee_flag := restocking_fee_flag;
self.return_eligible_flag := return_eligible_flag;
self.serialized_item_flag := serialized_item_flag;
self.validate_serial_number_flag := validate_serial_number_flag;
self.allow_new_serial_number_flag := allow_new_serial_number_flag;
self.size_required_flag := size_required_flag;
self.tax_group_id := tax_group_id;
self.taxable_flag := taxable_flag;
self.weight := weight;
self.serial_number := serial_number;
self.size_code := size_code;
self.gift_registry_id := gift_registry_id;
self.gift_receipted_item_flag := gift_receipted_item_flag;
self.shipping_charge_flag := shipping_charge_flag;
self.entry_method := entry_method;
self.AlterationItem := AlterationItem;
self.GiftCardItem := GiftCardItem;
self.fulfillment_seq_no := fulfillment_seq_no;
RETURN;
end;
END;
/
DROP TYPE "RIB_GiftCardItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftCardItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdItmDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  card_number varchar2(20),
  request_type varchar2(6), -- request_type is enumeration field, valid values are [ISSUE, RELOAD] (all lower-case)
  authorization_code varchar2(20),
  authorization_datetime date,
  original_balance number(20,4),
  current_balance number(20,4),
  settlement_data varchar2(16),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftCardItem_REC"
(
  rib_oid number
, card_number varchar2
, request_type varchar2
, authorization_code varchar2
, authorization_datetime date
, original_balance number
, current_balance number
) return self as result
,constructor function "RIB_GiftCardItem_REC"
(
  rib_oid number
, card_number varchar2
, request_type varchar2
, authorization_code varchar2
, authorization_datetime date
, original_balance number
, current_balance number
, settlement_data varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftCardItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdItmDesc') := "ns_name_CustOrdItmDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'card_number') := card_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_type') := request_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_datetime') := authorization_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'original_balance') := original_balance;
  rib_obj_util.g_RIB_element_values(i_prefix||'current_balance') := current_balance;
  rib_obj_util.g_RIB_element_values(i_prefix||'settlement_data') := settlement_data;
END appendNodeValues;
constructor function "RIB_GiftCardItem_REC"
(
  rib_oid number
, card_number varchar2
, request_type varchar2
, authorization_code varchar2
, authorization_datetime date
, original_balance number
, current_balance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.request_type := request_type;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.original_balance := original_balance;
self.current_balance := current_balance;
RETURN;
end;
constructor function "RIB_GiftCardItem_REC"
(
  rib_oid number
, card_number varchar2
, request_type varchar2
, authorization_code varchar2
, authorization_datetime date
, original_balance number
, current_balance number
, settlement_data varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.request_type := request_type;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.original_balance := original_balance;
self.current_balance := current_balance;
self.settlement_data := settlement_data;
RETURN;
end;
END;
/
DROP TYPE "RIB_AlterationItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AlterationItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdItmDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  alteration_type varchar2(6), -- alteration_type is enumeration field, valid values are [COAT, DRESS, PANTS, SHIRT, SKIRT, REPAIR] (all lower-case)
  instruction "RIB_instruction_TBL",   -- Size of "RIB_instruction_TBL" is 6
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AlterationItem_REC"
(
  rib_oid number
, alteration_type varchar2
) return self as result
,constructor function "RIB_AlterationItem_REC"
(
  rib_oid number
, alteration_type varchar2
, instruction "RIB_instruction_TBL"  -- Size of "RIB_instruction_TBL" is 6
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AlterationItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdItmDesc') := "ns_name_CustOrdItmDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'alteration_type') := alteration_type;
  IF instruction IS NOT NULL THEN
    FOR INDX IN instruction.FIRST()..instruction.LAST() LOOP
      l_new_pre :=i_prefix||indx||'instruction'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'instruction'||'.'):=instruction(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_AlterationItem_REC"
(
  rib_oid number
, alteration_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alteration_type := alteration_type;
RETURN;
end;
constructor function "RIB_AlterationItem_REC"
(
  rib_oid number
, alteration_type varchar2
, instruction "RIB_instruction_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.alteration_type := alteration_type;
self.instruction := instruction;
RETURN;
end;
END;
/
DROP TYPE "RIB_instruction_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_instruction_TBL" AS TABLE OF varchar2(250);
/
