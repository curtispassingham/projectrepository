DROP TYPE "RIB_PromoLineDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PromoLineDesc_REC";
/
DROP TYPE "RIB_PromoLineDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PromoLineDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromoLineDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  currency_code varchar2(3),
  unit_discount_amount number(20,4),
  promotion_name varchar2(250),
  promotion_component_detail_id number(22),
  promotion_component_id number(22),
  promotion_id number(22),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
) return self as result
,constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
) return self as result
,constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
, promotion_component_detail_id number
) return self as result
,constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
, promotion_component_detail_id number
, promotion_component_id number
) return self as result
,constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
, promotion_component_detail_id number
, promotion_component_id number
, promotion_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PromoLineDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromoLineDesc') := "ns_name_PromoLineDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_discount_amount') := unit_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_name') := promotion_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_component_detail_id') := promotion_component_detail_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_component_id') := promotion_component_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
END appendNodeValues;
constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.currency_code := currency_code;
self.unit_discount_amount := unit_discount_amount;
RETURN;
end;
constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.currency_code := currency_code;
self.unit_discount_amount := unit_discount_amount;
self.promotion_name := promotion_name;
RETURN;
end;
constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
, promotion_component_detail_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.currency_code := currency_code;
self.unit_discount_amount := unit_discount_amount;
self.promotion_name := promotion_name;
self.promotion_component_detail_id := promotion_component_detail_id;
RETURN;
end;
constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
, promotion_component_detail_id number
, promotion_component_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.currency_code := currency_code;
self.unit_discount_amount := unit_discount_amount;
self.promotion_name := promotion_name;
self.promotion_component_detail_id := promotion_component_detail_id;
self.promotion_component_id := promotion_component_id;
RETURN;
end;
constructor function "RIB_PromoLineDesc_REC"
(
  rib_oid number
, currency_code varchar2
, unit_discount_amount number
, promotion_name varchar2
, promotion_component_detail_id number
, promotion_component_id number
, promotion_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.currency_code := currency_code;
self.unit_discount_amount := unit_discount_amount;
self.promotion_name := promotion_name;
self.promotion_component_detail_id := promotion_component_detail_id;
self.promotion_component_id := promotion_component_id;
self.promotion_id := promotion_id;
RETURN;
end;
END;
/
