DROP TYPE "RIB_ItmTktFrmDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmTktFrmDesc_REC";
/
DROP TYPE "RIB_ItmTktFrmDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmTktFrmDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktFrmDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  format_id number(10),
  store_id number(10),
  format_name varchar2(128),
  format_desc varchar2(128),
  template_url varchar2(128),
  ticket_type varchar2(20), -- ticket_type is enumeration field, valid values are [SHELF_LABEL, ITEM_TICKET, AGSN, UNKNOWN] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmTktFrmDesc_REC"
(
  rib_oid number
, format_id number
, store_id number
, format_name varchar2
, format_desc varchar2
, template_url varchar2
, ticket_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmTktFrmDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktFrmDesc') := "ns_name_ItmTktFrmDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'format_id') := format_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_name') := format_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_desc') := format_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'template_url') := template_url;
  rib_obj_util.g_RIB_element_values(i_prefix||'ticket_type') := ticket_type;
END appendNodeValues;
constructor function "RIB_ItmTktFrmDesc_REC"
(
  rib_oid number
, format_id number
, store_id number
, format_name varchar2
, format_desc varchar2
, template_url varchar2
, ticket_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.format_id := format_id;
self.store_id := store_id;
self.format_name := format_name;
self.format_desc := format_desc;
self.template_url := template_url;
self.ticket_type := ticket_type;
RETURN;
end;
END;
/
