@@CustomerRef.sql;
/
DROP TYPE "RIB_PurchHistCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PurchHistCriVo_REC";
/
DROP TYPE "RIB_PurchHistCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PurchHistCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PurchHistCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustomerRef "RIB_CustomerRef_REC",
  start_date date,
  end_date date,
  line_item_population number(3),
  max_record number(3),
  page_number number(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
,constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
) return self as result
,constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
) return self as result
,constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
, line_item_population number
) return self as result
,constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
, line_item_population number
, max_record number
) return self as result
,constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
, line_item_population number
, max_record number
, page_number number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PurchHistCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PurchHistCriVo') := "ns_name_PurchHistCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_population') := line_item_population;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_record') := max_record;
  rib_obj_util.g_RIB_element_values(i_prefix||'page_number') := page_number;
END appendNodeValues;
constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
RETURN;
end;
constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
, line_item_population number
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.start_date := start_date;
self.end_date := end_date;
self.line_item_population := line_item_population;
RETURN;
end;
constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
, line_item_population number
, max_record number
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.start_date := start_date;
self.end_date := end_date;
self.line_item_population := line_item_population;
self.max_record := max_record;
RETURN;
end;
constructor function "RIB_PurchHistCriVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, start_date date
, end_date date
, line_item_population number
, max_record number
, page_number number
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.start_date := start_date;
self.end_date := end_date;
self.line_item_population := line_item_population;
self.max_record := max_record;
self.page_number := page_number;
RETURN;
end;
END;
/
