DROP TYPE "RIB_FodHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodHdrDesc_REC";
/
DROP TYPE "RIB_FodHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  int_fulfill_order_delivery_id number(12),
  fulfill_order_id number(12),
  status varchar2(20), -- status is enumeration field, valid values are [IN_PROGRESS, SUBMITTED, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_date date,
  dispatch_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodHdrDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, fulfill_order_id number
, status varchar2
, create_date date
) return self as result
,constructor function "RIB_FodHdrDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, fulfill_order_id number
, status varchar2
, create_date date
, dispatch_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodHdrDesc') := "ns_name_FodHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'int_fulfill_order_delivery_id') := int_fulfill_order_delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_id') := fulfill_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_date') := dispatch_date;
END appendNodeValues;
constructor function "RIB_FodHdrDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, fulfill_order_id number
, status varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.fulfill_order_id := fulfill_order_id;
self.status := status;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_FodHdrDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, fulfill_order_id number
, status varchar2
, create_date date
, dispatch_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.fulfill_order_id := fulfill_order_id;
self.status := status;
self.create_date := create_date;
self.dispatch_date := dispatch_date;
RETURN;
end;
END;
/
