DROP TYPE "RIB_GiftRegUpdDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftRegUpdDesc_REC";
/
DROP TYPE "RIB_GiftRegUpdDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftRegUpdDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftRegUpdDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  banner_id number(4),
  registry_type varchar2(35),
  registry_nbr varchar2(30),
  bill_to_first_name varchar2(4000),
  bill_to_last_name varchar2(4000),
  bill_to_addr_line1 varchar2(4000),
  bill_to_addr_line2 varchar2(4000),
  bill_to_city varchar2(25),
  bill_to_state varchar2(4000),
  bill_to_postal_code varchar2(10),
  bill_to_day_phone varchar2(30),
  bill_to_evening_phone varchar2(30),
  purchased_sku varchar2(25),
  purchase_price number(18),
  trans_date date,
  qty_purchased number(12),
  order_no varchar2(12),
  order_type varchar2(9),
  order_source varchar2(1),
  order_line_nbr number(4),
  operation varchar2(4001),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftRegUpdDesc_REC"
(
  rib_oid number
, banner_id number
, registry_type varchar2
, registry_nbr varchar2
, bill_to_first_name varchar2
, bill_to_last_name varchar2
, bill_to_addr_line1 varchar2
, bill_to_addr_line2 varchar2
, bill_to_city varchar2
, bill_to_state varchar2
, bill_to_postal_code varchar2
, bill_to_day_phone varchar2
, bill_to_evening_phone varchar2
, purchased_sku varchar2
, purchase_price number
, trans_date date
, qty_purchased number
, order_no varchar2
, order_type varchar2
, order_source varchar2
, order_line_nbr number
, operation varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftRegUpdDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftRegUpdDesc') := "ns_name_GiftRegUpdDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'banner_id') := banner_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'registry_type') := registry_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'registry_nbr') := registry_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_first_name') := bill_to_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_last_name') := bill_to_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_addr_line1') := bill_to_addr_line1;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_addr_line2') := bill_to_addr_line2;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_city') := bill_to_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_state') := bill_to_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_postal_code') := bill_to_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_day_phone') := bill_to_day_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_evening_phone') := bill_to_evening_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchased_sku') := purchased_sku;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_price') := purchase_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'trans_date') := trans_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_purchased') := qty_purchased;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_source') := order_source;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_nbr') := order_line_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'operation') := operation;
END appendNodeValues;
constructor function "RIB_GiftRegUpdDesc_REC"
(
  rib_oid number
, banner_id number
, registry_type varchar2
, registry_nbr varchar2
, bill_to_first_name varchar2
, bill_to_last_name varchar2
, bill_to_addr_line1 varchar2
, bill_to_addr_line2 varchar2
, bill_to_city varchar2
, bill_to_state varchar2
, bill_to_postal_code varchar2
, bill_to_day_phone varchar2
, bill_to_evening_phone varchar2
, purchased_sku varchar2
, purchase_price number
, trans_date date
, qty_purchased number
, order_no varchar2
, order_type varchar2
, order_source varchar2
, order_line_nbr number
, operation varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.banner_id := banner_id;
self.registry_type := registry_type;
self.registry_nbr := registry_nbr;
self.bill_to_first_name := bill_to_first_name;
self.bill_to_last_name := bill_to_last_name;
self.bill_to_addr_line1 := bill_to_addr_line1;
self.bill_to_addr_line2 := bill_to_addr_line2;
self.bill_to_city := bill_to_city;
self.bill_to_state := bill_to_state;
self.bill_to_postal_code := bill_to_postal_code;
self.bill_to_day_phone := bill_to_day_phone;
self.bill_to_evening_phone := bill_to_evening_phone;
self.purchased_sku := purchased_sku;
self.purchase_price := purchase_price;
self.trans_date := trans_date;
self.qty_purchased := qty_purchased;
self.order_no := order_no;
self.order_type := order_type;
self.order_source := order_source;
self.order_line_nbr := order_line_nbr;
self.operation := operation;
RETURN;
end;
END;
/
