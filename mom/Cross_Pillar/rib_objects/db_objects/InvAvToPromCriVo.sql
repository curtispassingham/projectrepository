@@SearchAreaDesc.sql;
/
DROP TYPE "RIB_InvAvToPromCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAvToPromCriVo_REC";
/
DROP TYPE "RIB_InvAvLocation_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAvLocation_REC";
/
DROP TYPE "RIB_InvAvLocation_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAvLocation_TBL" AS TABLE OF "RIB_InvAvLocation_REC";
/
DROP TYPE "RIB_InvAvToPromCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAvToPromCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAvToPromCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  min_qty number(12,4),
  unit_of_measure varchar2(4),
  InvAvLocation_TBL "RIB_InvAvLocation_TBL",   -- Size of "RIB_InvAvLocation_TBL" is 999
  SearchAreaDesc "RIB_SearchAreaDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
) return self as result
,constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
, unit_of_measure varchar2
) return self as result
,constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
, unit_of_measure varchar2
, InvAvLocation_TBL "RIB_InvAvLocation_TBL"  -- Size of "RIB_InvAvLocation_TBL" is 999
) return self as result
,constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
, unit_of_measure varchar2
, InvAvLocation_TBL "RIB_InvAvLocation_TBL"  -- Size of "RIB_InvAvLocation_TBL" is 999
, SearchAreaDesc "RIB_SearchAreaDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAvToPromCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAvToPromCriVo') := "ns_name_InvAvToPromCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'min_qty') := min_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  IF InvAvLocation_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InvAvLocation_TBL.';
    FOR INDX IN InvAvLocation_TBL.FIRST()..InvAvLocation_TBL.LAST() LOOP
      InvAvLocation_TBL(indx).appendNodeValues( i_prefix||indx||'InvAvLocation_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'SearchAreaDesc.';
  SearchAreaDesc.appendNodeValues( i_prefix||'SearchAreaDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.min_qty := min_qty;
RETURN;
end;
constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.min_qty := min_qty;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
, unit_of_measure varchar2
, InvAvLocation_TBL "RIB_InvAvLocation_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.min_qty := min_qty;
self.unit_of_measure := unit_of_measure;
self.InvAvLocation_TBL := InvAvLocation_TBL;
RETURN;
end;
constructor function "RIB_InvAvToPromCriVo_REC"
(
  rib_oid number
, item varchar2
, min_qty number
, unit_of_measure varchar2
, InvAvLocation_TBL "RIB_InvAvLocation_TBL"
, SearchAreaDesc "RIB_SearchAreaDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.min_qty := min_qty;
self.unit_of_measure := unit_of_measure;
self.InvAvLocation_TBL := InvAvLocation_TBL;
self.SearchAreaDesc := SearchAreaDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_InvAvLocation_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAvLocation_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAvToPromCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1), -- loc_type is enumeration field, valid values are [S, W] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAvLocation_REC"
(
  rib_oid number
, location number
, loc_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAvLocation_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAvToPromCriVo') := "ns_name_InvAvToPromCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
END appendNodeValues;
constructor function "RIB_InvAvLocation_REC"
(
  rib_oid number
, location number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
RETURN;
end;
END;
/
