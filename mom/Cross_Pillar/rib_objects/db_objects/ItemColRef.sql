@@ItemRef.sql;
/
DROP TYPE "RIB_ItemColRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemColRef_REC";
/
DROP TYPE "RIB_ItemRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemRef_TBL" AS TABLE OF "RIB_ItemRef_REC";
/
DROP TYPE "RIB_ItemColRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemColRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemColRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  collection_size number(3),
  ItemRef_TBL "RIB_ItemRef_TBL",   -- Size of "RIB_ItemRef_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemColRef_REC"
(
  rib_oid number
, collection_size number
, ItemRef_TBL "RIB_ItemRef_TBL"  -- Size of "RIB_ItemRef_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemColRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemColRef') := "ns_name_ItemColRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'collection_size') := collection_size;
  l_new_pre :=i_prefix||'ItemRef_TBL.';
  FOR INDX IN ItemRef_TBL.FIRST()..ItemRef_TBL.LAST() LOOP
    ItemRef_TBL(indx).appendNodeValues( i_prefix||indx||'ItemRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItemColRef_REC"
(
  rib_oid number
, collection_size number
, ItemRef_TBL "RIB_ItemRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.collection_size := collection_size;
self.ItemRef_TBL := ItemRef_TBL;
RETURN;
end;
END;
/
