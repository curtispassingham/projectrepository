DROP TYPE "RIB_FulfilOrdDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdDtl_REC";
/
DROP TYPE "RIB_FulfilOrdDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FulfilOrdDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FulfilOrdDtl" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  ref_item varchar2(25),
  order_qty_suom number(12,4),
  standard_uom varchar2(4),
  transaction_uom varchar2(4),
  substitute_ind varchar2(1),
  unit_retail number(20,4),
  retail_curr varchar2(3),
  comments varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
) return self as result
,constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
, unit_retail number
) return self as result
,constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
, unit_retail number
, retail_curr varchar2
) return self as result
,constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
, unit_retail number
, retail_curr varchar2
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FulfilOrdDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FulfilOrdDtl') := "ns_name_FulfilOrdDtl";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_item') := ref_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_qty_suom') := order_qty_suom;
  rib_obj_util.g_RIB_element_values(i_prefix||'standard_uom') := standard_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_uom') := transaction_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'substitute_ind') := substitute_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_retail') := unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'retail_curr') := retail_curr;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.order_qty_suom := order_qty_suom;
self.standard_uom := standard_uom;
self.transaction_uom := transaction_uom;
self.substitute_ind := substitute_ind;
RETURN;
end;
constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
, unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.order_qty_suom := order_qty_suom;
self.standard_uom := standard_uom;
self.transaction_uom := transaction_uom;
self.substitute_ind := substitute_ind;
self.unit_retail := unit_retail;
RETURN;
end;
constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
, unit_retail number
, retail_curr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.order_qty_suom := order_qty_suom;
self.standard_uom := standard_uom;
self.transaction_uom := transaction_uom;
self.substitute_ind := substitute_ind;
self.unit_retail := unit_retail;
self.retail_curr := retail_curr;
RETURN;
end;
constructor function "RIB_FulfilOrdDtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, order_qty_suom number
, standard_uom varchar2
, transaction_uom varchar2
, substitute_ind varchar2
, unit_retail number
, retail_curr varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.order_qty_suom := order_qty_suom;
self.standard_uom := standard_uom;
self.transaction_uom := transaction_uom;
self.substitute_ind := substitute_ind;
self.unit_retail := unit_retail;
self.retail_curr := retail_curr;
self.comments := comments;
RETURN;
end;
END;
/
