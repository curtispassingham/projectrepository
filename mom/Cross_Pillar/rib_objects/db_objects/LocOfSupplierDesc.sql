@@InSupplierDesc.sql;
/
@@BrSupplierDesc.sql;
/
DROP TYPE "RIB_LocOfSupplierDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierDesc_REC";
/
DROP TYPE "RIB_LocOfSupAttr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupAttr_REC";
/
DROP TYPE "RIB_LocOfSupSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupSite_REC";
/
DROP TYPE "RIB_LocOfSupSiteOrgUnit_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupSiteOrgUnit_REC";
/
DROP TYPE "RIB_LocOfSupSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupSiteAddr_REC";
/
DROP TYPE "RIB_LocOfAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfAddr_REC";
/
DROP TYPE "RIB_InSupplierDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierDesc_TBL" AS TABLE OF "RIB_InSupplierDesc_REC";
/
DROP TYPE "RIB_BrSupplierDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupplierDesc_TBL" AS TABLE OF "RIB_BrSupplierDesc_REC";
/
DROP TYPE "RIB_LocOfSupplierDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupplierDesc_TBL "RIB_InSupplierDesc_TBL",   -- Size of "RIB_InSupplierDesc_TBL" is unbounded
  BrSupplierDesc_TBL "RIB_BrSupplierDesc_TBL",   -- Size of "RIB_BrSupplierDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupplierDesc_REC"
(
  rib_oid number
, InSupplierDesc_TBL "RIB_InSupplierDesc_TBL"  -- Size of "RIB_InSupplierDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupplierDesc_REC"
(
  rib_oid number
, InSupplierDesc_TBL "RIB_InSupplierDesc_TBL"  -- Size of "RIB_InSupplierDesc_TBL" is unbounded
, BrSupplierDesc_TBL "RIB_BrSupplierDesc_TBL"  -- Size of "RIB_BrSupplierDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupplierDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierDesc') := "ns_name_LocOfSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF InSupplierDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupplierDesc_TBL.';
    FOR INDX IN InSupplierDesc_TBL.FIRST()..InSupplierDesc_TBL.LAST() LOOP
      InSupplierDesc_TBL(indx).appendNodeValues( i_prefix||indx||'InSupplierDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupplierDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupplierDesc_TBL.';
    FOR INDX IN BrSupplierDesc_TBL.FIRST()..BrSupplierDesc_TBL.LAST() LOOP
      BrSupplierDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupplierDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupplierDesc_REC"
(
  rib_oid number
, InSupplierDesc_TBL "RIB_InSupplierDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierDesc_TBL := InSupplierDesc_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupplierDesc_REC"
(
  rib_oid number
, InSupplierDesc_TBL "RIB_InSupplierDesc_TBL"
, BrSupplierDesc_TBL "RIB_BrSupplierDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierDesc_TBL := InSupplierDesc_TBL;
self.BrSupplierDesc_TBL := BrSupplierDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupAttr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupAttr_TBL" AS TABLE OF "RIB_InSupAttr_REC";
/
DROP TYPE "RIB_BrSupAttr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupAttr_TBL" AS TABLE OF "RIB_BrSupAttr_REC";
/
DROP TYPE "RIB_LocOfSupAttr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupAttr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupAttr_TBL "RIB_InSupAttr_TBL",   -- Size of "RIB_InSupAttr_TBL" is unbounded
  BrSupAttr_TBL "RIB_BrSupAttr_TBL",   -- Size of "RIB_BrSupAttr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupAttr_REC"
(
  rib_oid number
, InSupAttr_TBL "RIB_InSupAttr_TBL"  -- Size of "RIB_InSupAttr_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupAttr_REC"
(
  rib_oid number
, InSupAttr_TBL "RIB_InSupAttr_TBL"  -- Size of "RIB_InSupAttr_TBL" is unbounded
, BrSupAttr_TBL "RIB_BrSupAttr_TBL"  -- Size of "RIB_BrSupAttr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupAttr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierDesc') := "ns_name_LocOfSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InSupAttr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupAttr_TBL.';
    FOR INDX IN InSupAttr_TBL.FIRST()..InSupAttr_TBL.LAST() LOOP
      InSupAttr_TBL(indx).appendNodeValues( i_prefix||indx||'InSupAttr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupAttr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupAttr_TBL.';
    FOR INDX IN BrSupAttr_TBL.FIRST()..BrSupAttr_TBL.LAST() LOOP
      BrSupAttr_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupAttr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupAttr_REC"
(
  rib_oid number
, InSupAttr_TBL "RIB_InSupAttr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupAttr_TBL := InSupAttr_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupAttr_REC"
(
  rib_oid number
, InSupAttr_TBL "RIB_InSupAttr_TBL"
, BrSupAttr_TBL "RIB_BrSupAttr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupAttr_TBL := InSupAttr_TBL;
self.BrSupAttr_TBL := BrSupAttr_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupSite_TBL" AS TABLE OF "RIB_InSupSite_REC";
/
DROP TYPE "RIB_BrSupSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupSite_TBL" AS TABLE OF "RIB_BrSupSite_REC";
/
DROP TYPE "RIB_LocOfSupSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupSite_TBL "RIB_InSupSite_TBL",   -- Size of "RIB_InSupSite_TBL" is unbounded
  BrSupSite_TBL "RIB_BrSupSite_TBL",   -- Size of "RIB_BrSupSite_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupSite_REC"
(
  rib_oid number
, InSupSite_TBL "RIB_InSupSite_TBL"  -- Size of "RIB_InSupSite_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupSite_REC"
(
  rib_oid number
, InSupSite_TBL "RIB_InSupSite_TBL"  -- Size of "RIB_InSupSite_TBL" is unbounded
, BrSupSite_TBL "RIB_BrSupSite_TBL"  -- Size of "RIB_BrSupSite_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierDesc') := "ns_name_LocOfSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InSupSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupSite_TBL.';
    FOR INDX IN InSupSite_TBL.FIRST()..InSupSite_TBL.LAST() LOOP
      InSupSite_TBL(indx).appendNodeValues( i_prefix||indx||'InSupSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupSite_TBL.';
    FOR INDX IN BrSupSite_TBL.FIRST()..BrSupSite_TBL.LAST() LOOP
      BrSupSite_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupSite_REC"
(
  rib_oid number
, InSupSite_TBL "RIB_InSupSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupSite_TBL := InSupSite_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupSite_REC"
(
  rib_oid number
, InSupSite_TBL "RIB_InSupSite_TBL"
, BrSupSite_TBL "RIB_BrSupSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupSite_TBL := InSupSite_TBL;
self.BrSupSite_TBL := BrSupSite_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupSiteOrgUnit_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupSiteOrgUnit_TBL" AS TABLE OF "RIB_InSupSiteOrgUnit_REC";
/
DROP TYPE "RIB_BrSupSiteOrgUnit_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupSiteOrgUnit_TBL" AS TABLE OF "RIB_BrSupSiteOrgUnit_REC";
/
DROP TYPE "RIB_LocOfSupSiteOrgUnit_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupSiteOrgUnit_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupSiteOrgUnit_TBL "RIB_InSupSiteOrgUnit_TBL",   -- Size of "RIB_InSupSiteOrgUnit_TBL" is unbounded
  BrSupSiteOrgUnit_TBL "RIB_BrSupSiteOrgUnit_TBL",   -- Size of "RIB_BrSupSiteOrgUnit_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupSiteOrgUnit_REC"
(
  rib_oid number
, InSupSiteOrgUnit_TBL "RIB_InSupSiteOrgUnit_TBL"  -- Size of "RIB_InSupSiteOrgUnit_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupSiteOrgUnit_REC"
(
  rib_oid number
, InSupSiteOrgUnit_TBL "RIB_InSupSiteOrgUnit_TBL"  -- Size of "RIB_InSupSiteOrgUnit_TBL" is unbounded
, BrSupSiteOrgUnit_TBL "RIB_BrSupSiteOrgUnit_TBL"  -- Size of "RIB_BrSupSiteOrgUnit_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupSiteOrgUnit_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierDesc') := "ns_name_LocOfSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InSupSiteOrgUnit_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupSiteOrgUnit_TBL.';
    FOR INDX IN InSupSiteOrgUnit_TBL.FIRST()..InSupSiteOrgUnit_TBL.LAST() LOOP
      InSupSiteOrgUnit_TBL(indx).appendNodeValues( i_prefix||indx||'InSupSiteOrgUnit_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupSiteOrgUnit_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupSiteOrgUnit_TBL.';
    FOR INDX IN BrSupSiteOrgUnit_TBL.FIRST()..BrSupSiteOrgUnit_TBL.LAST() LOOP
      BrSupSiteOrgUnit_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupSiteOrgUnit_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupSiteOrgUnit_REC"
(
  rib_oid number
, InSupSiteOrgUnit_TBL "RIB_InSupSiteOrgUnit_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupSiteOrgUnit_TBL := InSupSiteOrgUnit_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupSiteOrgUnit_REC"
(
  rib_oid number
, InSupSiteOrgUnit_TBL "RIB_InSupSiteOrgUnit_TBL"
, BrSupSiteOrgUnit_TBL "RIB_BrSupSiteOrgUnit_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupSiteOrgUnit_TBL := InSupSiteOrgUnit_TBL;
self.BrSupSiteOrgUnit_TBL := BrSupSiteOrgUnit_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupSiteAddr_TBL" AS TABLE OF "RIB_InSupSiteAddr_REC";
/
DROP TYPE "RIB_BrSupSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupSiteAddr_TBL" AS TABLE OF "RIB_BrSupSiteAddr_REC";
/
DROP TYPE "RIB_LocOfSupSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupSiteAddr_TBL "RIB_InSupSiteAddr_TBL",   -- Size of "RIB_InSupSiteAddr_TBL" is unbounded
  BrSupSiteAddr_TBL "RIB_BrSupSiteAddr_TBL",   -- Size of "RIB_BrSupSiteAddr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupSiteAddr_REC"
(
  rib_oid number
, InSupSiteAddr_TBL "RIB_InSupSiteAddr_TBL"  -- Size of "RIB_InSupSiteAddr_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupSiteAddr_REC"
(
  rib_oid number
, InSupSiteAddr_TBL "RIB_InSupSiteAddr_TBL"  -- Size of "RIB_InSupSiteAddr_TBL" is unbounded
, BrSupSiteAddr_TBL "RIB_BrSupSiteAddr_TBL"  -- Size of "RIB_BrSupSiteAddr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierDesc') := "ns_name_LocOfSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InSupSiteAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupSiteAddr_TBL.';
    FOR INDX IN InSupSiteAddr_TBL.FIRST()..InSupSiteAddr_TBL.LAST() LOOP
      InSupSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'InSupSiteAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupSiteAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupSiteAddr_TBL.';
    FOR INDX IN BrSupSiteAddr_TBL.FIRST()..BrSupSiteAddr_TBL.LAST() LOOP
      BrSupSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupSiteAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupSiteAddr_REC"
(
  rib_oid number
, InSupSiteAddr_TBL "RIB_InSupSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupSiteAddr_TBL := InSupSiteAddr_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupSiteAddr_REC"
(
  rib_oid number
, InSupSiteAddr_TBL "RIB_InSupSiteAddr_TBL"
, BrSupSiteAddr_TBL "RIB_BrSupSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupSiteAddr_TBL := InSupSiteAddr_TBL;
self.BrSupSiteAddr_TBL := BrSupSiteAddr_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InAddr_TBL" AS TABLE OF "RIB_InAddr_REC";
/
DROP TYPE "RIB_BrAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrAddr_TBL" AS TABLE OF "RIB_BrAddr_REC";
/
DROP TYPE "RIB_LocOfAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InAddr_TBL "RIB_InAddr_TBL",   -- Size of "RIB_InAddr_TBL" is unbounded
  BrAddr_TBL "RIB_BrAddr_TBL",   -- Size of "RIB_BrAddr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfAddr_REC"
(
  rib_oid number
, InAddr_TBL "RIB_InAddr_TBL"  -- Size of "RIB_InAddr_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfAddr_REC"
(
  rib_oid number
, InAddr_TBL "RIB_InAddr_TBL"  -- Size of "RIB_InAddr_TBL" is unbounded
, BrAddr_TBL "RIB_BrAddr_TBL"  -- Size of "RIB_BrAddr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierDesc') := "ns_name_LocOfSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InAddr_TBL.';
    FOR INDX IN InAddr_TBL.FIRST()..InAddr_TBL.LAST() LOOP
      InAddr_TBL(indx).appendNodeValues( i_prefix||indx||'InAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrAddr_TBL.';
    FOR INDX IN BrAddr_TBL.FIRST()..BrAddr_TBL.LAST() LOOP
      BrAddr_TBL(indx).appendNodeValues( i_prefix||indx||'BrAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfAddr_REC"
(
  rib_oid number
, InAddr_TBL "RIB_InAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InAddr_TBL := InAddr_TBL;
RETURN;
end;
constructor function "RIB_LocOfAddr_REC"
(
  rib_oid number
, InAddr_TBL "RIB_InAddr_TBL"
, BrAddr_TBL "RIB_BrAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InAddr_TBL := InAddr_TBL;
self.BrAddr_TBL := BrAddr_TBL;
RETURN;
end;
END;
/
