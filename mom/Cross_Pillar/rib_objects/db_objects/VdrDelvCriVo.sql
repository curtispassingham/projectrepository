DROP TYPE "RIB_VdrDelvCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCriVo_REC";
/
DROP TYPE "RIB_VdrDelvCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  from_date date,
  to_date date,
  purchase_order_ext_id varchar2(128),
  origin_type varchar2(20), -- origin_type is enumeration field, valid values are [ASN, DEXNEX, MANUAL, PO, NO_VALUE] (all lower-case)
  invoice_number varchar2(128),
  asn varchar2(128),
  item_id varchar2(25),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  any_customer_order varchar2(5), --any_customer_order is boolean field, valid values are true,false (all lower-case) 
  supplier_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, RECEIVED, CANCELED, REJECTED, NO_VALUE] (all lower-case)
  last_user varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, purchase_order_ext_id varchar2
, origin_type varchar2
, invoice_number varchar2
, asn varchar2
, item_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, any_customer_order varchar2  --any_customer_order is boolean field, valid values are true,false (all lower-case)
, supplier_id number
, status varchar2
) return self as result
,constructor function "RIB_VdrDelvCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, purchase_order_ext_id varchar2
, origin_type varchar2
, invoice_number varchar2
, asn varchar2
, item_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, any_customer_order varchar2  --any_customer_order is boolean field, valid values are true,false (all lower-case)
, supplier_id number
, status varchar2
, last_user varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCriVo') := "ns_name_VdrDelvCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_date') := from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_date') := to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_order_ext_id') := purchase_order_ext_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_type') := origin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_number') := invoice_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'any_customer_order') := any_customer_order;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_user') := last_user;
END appendNodeValues;
constructor function "RIB_VdrDelvCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, purchase_order_ext_id varchar2
, origin_type varchar2
, invoice_number varchar2
, asn varchar2
, item_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, any_customer_order varchar2
, supplier_id number
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.from_date := from_date;
self.to_date := to_date;
self.purchase_order_ext_id := purchase_order_ext_id;
self.origin_type := origin_type;
self.invoice_number := invoice_number;
self.asn := asn;
self.item_id := item_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.any_customer_order := any_customer_order;
self.supplier_id := supplier_id;
self.status := status;
RETURN;
end;
constructor function "RIB_VdrDelvCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, purchase_order_ext_id varchar2
, origin_type varchar2
, invoice_number varchar2
, asn varchar2
, item_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, any_customer_order varchar2
, supplier_id number
, status varchar2
, last_user varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.from_date := from_date;
self.to_date := to_date;
self.purchase_order_ext_id := purchase_order_ext_id;
self.origin_type := origin_type;
self.invoice_number := invoice_number;
self.asn := asn;
self.item_id := item_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.any_customer_order := any_customer_order;
self.supplier_id := supplier_id;
self.status := status;
self.last_user := last_user;
RETURN;
end;
END;
/
