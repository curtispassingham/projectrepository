DROP TYPE "RIB_XOrderDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XOrderDtl_REC";
/
DROP TYPE "RIB_XOrderDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XOrderDesc_REC";
/
DROP TYPE "RIB_XOrderDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XOrderDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XOrderDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  location number(10),
  unit_cost number(20,4),
  ref_item varchar2(25),
  origin_country_id varchar2(3),
  supp_pack_size number(12,4),
  qty_ordered number(12,4),
  location_type varchar2(1),
  cancel_ind varchar2(1),
  reinstate_ind varchar2(1),
  delivery_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
, cancel_ind varchar2
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
, cancel_ind varchar2
, reinstate_ind varchar2
) return self as result
,constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
, cancel_ind varchar2
, reinstate_ind varchar2
, delivery_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XOrderDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XOrderDesc') := "ns_name_XOrderDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_item') := ref_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_pack_size') := supp_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_ordered') := qty_ordered;
  rib_obj_util.g_RIB_element_values(i_prefix||'location_type') := location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancel_ind') := cancel_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'reinstate_ind') := reinstate_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
END appendNodeValues;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.qty_ordered := qty_ordered;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.qty_ordered := qty_ordered;
self.location_type := location_type;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
, cancel_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.qty_ordered := qty_ordered;
self.location_type := location_type;
self.cancel_ind := cancel_ind;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
, cancel_ind varchar2
, reinstate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.qty_ordered := qty_ordered;
self.location_type := location_type;
self.cancel_ind := cancel_ind;
self.reinstate_ind := reinstate_ind;
RETURN;
end;
constructor function "RIB_XOrderDtl_REC"
(
  rib_oid number
, item varchar2
, location number
, unit_cost number
, ref_item varchar2
, origin_country_id varchar2
, supp_pack_size number
, qty_ordered number
, location_type varchar2
, cancel_ind varchar2
, reinstate_ind varchar2
, delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.unit_cost := unit_cost;
self.ref_item := ref_item;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.qty_ordered := qty_ordered;
self.location_type := location_type;
self.cancel_ind := cancel_ind;
self.reinstate_ind := reinstate_ind;
self.delivery_date := delivery_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_XOrderDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XOrderDtl_TBL" AS TABLE OF "RIB_XOrderDtl_REC";
/
DROP TYPE "RIB_XOrderDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XOrderDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XOrderDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  order_no varchar2(12),
  supplier varchar2(10),
  currency_code varchar2(3),
  terms varchar2(15),
  not_before_date date,
  not_after_date date,
  otb_eow_date date,
  dept number(4),
  status varchar2(1),
  exchange_rate number(20,10),
  include_on_ord_ind varchar2(1),
  written_date date,
  XOrderDtl_TBL "RIB_XOrderDtl_TBL",   -- Size of "RIB_XOrderDtl_TBL" is unbounded
  orig_ind varchar2(1),
  edi_po_ind varchar2(1),
  pre_mark_ind varchar2(1),
  user_id varchar2(30),
  comment_desc varchar2(2000),
  attempt_rms_load varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
, orig_ind varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
, orig_ind varchar2
, edi_po_ind varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
, user_id varchar2
, comment_desc varchar2
) return self as result
,constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"  -- Size of "RIB_XOrderDtl_TBL" is unbounded
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
, user_id varchar2
, comment_desc varchar2
, attempt_rms_load varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XOrderDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XOrderDesc') := "ns_name_XOrderDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms') := terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_before_date') := not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'otb_eow_date') := otb_eow_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'exchange_rate') := exchange_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_on_ord_ind') := include_on_ord_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'written_date') := written_date;
  IF XOrderDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XOrderDtl_TBL.';
    FOR INDX IN XOrderDtl_TBL.FIRST()..XOrderDtl_TBL.LAST() LOOP
      XOrderDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XOrderDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'orig_ind') := orig_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_po_ind') := edi_po_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'pre_mark_ind') := pre_mark_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'comment_desc') := comment_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'attempt_rms_load') := attempt_rms_load;
END appendNodeValues;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
, orig_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
self.orig_ind := orig_ind;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
, orig_ind varchar2
, edi_po_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
self.orig_ind := orig_ind;
self.edi_po_ind := edi_po_ind;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
self.orig_ind := orig_ind;
self.edi_po_ind := edi_po_ind;
self.pre_mark_ind := pre_mark_ind;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
self.orig_ind := orig_ind;
self.edi_po_ind := edi_po_ind;
self.pre_mark_ind := pre_mark_ind;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
, user_id varchar2
, comment_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
self.orig_ind := orig_ind;
self.edi_po_ind := edi_po_ind;
self.pre_mark_ind := pre_mark_ind;
self.user_id := user_id;
self.comment_desc := comment_desc;
RETURN;
end;
constructor function "RIB_XOrderDesc_REC"
(
  rib_oid number
, order_no varchar2
, supplier varchar2
, currency_code varchar2
, terms varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, dept number
, status varchar2
, exchange_rate number
, include_on_ord_ind varchar2
, written_date date
, XOrderDtl_TBL "RIB_XOrderDtl_TBL"
, orig_ind varchar2
, edi_po_ind varchar2
, pre_mark_ind varchar2
, user_id varchar2
, comment_desc varchar2
, attempt_rms_load varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.currency_code := currency_code;
self.terms := terms;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.dept := dept;
self.status := status;
self.exchange_rate := exchange_rate;
self.include_on_ord_ind := include_on_ord_ind;
self.written_date := written_date;
self.XOrderDtl_TBL := XOrderDtl_TBL;
self.orig_ind := orig_ind;
self.edi_po_ind := edi_po_ind;
self.pre_mark_ind := pre_mark_ind;
self.user_id := user_id;
self.comment_desc := comment_desc;
self.attempt_rms_load := attempt_rms_load;
RETURN;
end;
END;
/
