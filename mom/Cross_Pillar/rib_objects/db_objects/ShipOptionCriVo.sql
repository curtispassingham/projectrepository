@@ShipItemColDesc.sql;
/
@@LocaleColDesc.sql;
/
@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_ShipOptionCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShipOptionCriVo_REC";
/
DROP TYPE "RIB_ShipOptionCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShipOptionCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipOptionCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ship_loc_type varchar2(1), -- ship_loc_type is enumeration field, valid values are [S, C] (all lower-case)
  ship_loc_id varchar2(5),
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  ShipItemColDesc "RIB_ShipItemColDesc_REC",
  LocaleColDesc "RIB_LocaleColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShipOptionCriVo_REC"
(
  rib_oid number
, ship_loc_type varchar2
, ship_loc_id varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, ShipItemColDesc "RIB_ShipItemColDesc_REC"
, LocaleColDesc "RIB_LocaleColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShipOptionCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipOptionCriVo') := "ns_name_ShipOptionCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_loc_type') := ship_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_loc_id') := ship_loc_id;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ShipItemColDesc.';
  ShipItemColDesc.appendNodeValues( i_prefix||'ShipItemColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LocaleColDesc.';
  LocaleColDesc.appendNodeValues( i_prefix||'LocaleColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ShipOptionCriVo_REC"
(
  rib_oid number
, ship_loc_type varchar2
, ship_loc_id varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, ShipItemColDesc "RIB_ShipItemColDesc_REC"
, LocaleColDesc "RIB_LocaleColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_loc_type := ship_loc_type;
self.ship_loc_id := ship_loc_id;
self.GeoAddrDesc := GeoAddrDesc;
self.ShipItemColDesc := ShipItemColDesc;
self.LocaleColDesc := LocaleColDesc;
RETURN;
end;
END;
/
