@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_TsfShipCtnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnDesc_REC";
/
DROP TYPE "RIB_TsfShipCtnSize_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnSize_REC";
/
DROP TYPE "RIB_TsfShipCtnItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnItm_REC";
/
DROP TYPE "RIB_TsfShipCtnItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnItm_TBL" AS TABLE OF "RIB_TsfShipCtnItm_REC";
/
DROP TYPE "RIB_TsfShipCtnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_ship_carton_id number(15),
  store_id number(10),
  external_id varchar2(128),
  tsf_ship_id number(15),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, SHIPPED, CANCELED, UNKNOWN, NO_VALUE] (all lower-case)
  destination_type varchar2(20), -- destination_type is enumeration field, valid values are [FINISHER, STORE, WAREHOUSE, UNKNOWN] (all lower-case)
  destination_name varchar2(128),
  hierarchy_level varchar2(20), -- hierarchy_level is enumeration field, valid values are [NONE, DEPT, CLASS, SUBCLASS] (all lower-case)
  TsfShipCtnSize "RIB_TsfShipCtnSize_REC",
  weight number(12,4),
  weight_uom varchar2(4),
  tracking_number varchar2(128),
  create_user varchar2(128),
  update_user varchar2(128),
  approval_user varchar2(128),
  create_date date,
  update_date date,
  approval_date date,
  use_available varchar2(5), --use_available is boolean field, valid values are true,false (all lower-case) 
  order_related varchar2(5), --order_related is boolean field, valid values are true,false (all lower-case) 
  TsfShipCtnItm_TBL "RIB_TsfShipCtnItm_TBL",   -- Size of "RIB_TsfShipCtnItm_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnDesc_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, store_id number
, external_id varchar2
, tsf_ship_id number
, status varchar2
, destination_type varchar2
, destination_name varchar2
, hierarchy_level varchar2
, TsfShipCtnSize "RIB_TsfShipCtnSize_REC"
, weight number
, weight_uom varchar2
, tracking_number varchar2
, create_user varchar2
, update_user varchar2
, approval_user varchar2
, create_date date
, update_date date
, approval_date date
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_TsfShipCtnDesc_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, store_id number
, external_id varchar2
, tsf_ship_id number
, status varchar2
, destination_type varchar2
, destination_name varchar2
, hierarchy_level varchar2
, TsfShipCtnSize "RIB_TsfShipCtnSize_REC"
, weight number
, weight_uom varchar2
, tracking_number varchar2
, create_user varchar2
, update_user varchar2
, approval_user varchar2
, create_date date
, update_date date
, approval_date date
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
, TsfShipCtnItm_TBL "RIB_TsfShipCtnItm_TBL"  -- Size of "RIB_TsfShipCtnItm_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnDesc') := "ns_name_TsfShipCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_ship_carton_id') := tsf_ship_carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_ship_id') := tsf_ship_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_type') := destination_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_name') := destination_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'hierarchy_level') := hierarchy_level;
  l_new_pre :=i_prefix||'TsfShipCtnSize.';
  TsfShipCtnSize.appendNodeValues( i_prefix||'TsfShipCtnSize');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'approval_user') := approval_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'approval_date') := approval_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'use_available') := use_available;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_related') := order_related;
  IF TsfShipCtnItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfShipCtnItm_TBL.';
    FOR INDX IN TsfShipCtnItm_TBL.FIRST()..TsfShipCtnItm_TBL.LAST() LOOP
      TsfShipCtnItm_TBL(indx).appendNodeValues( i_prefix||indx||'TsfShipCtnItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfShipCtnDesc_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, store_id number
, external_id varchar2
, tsf_ship_id number
, status varchar2
, destination_type varchar2
, destination_name varchar2
, hierarchy_level varchar2
, TsfShipCtnSize "RIB_TsfShipCtnSize_REC"
, weight number
, weight_uom varchar2
, tracking_number varchar2
, create_user varchar2
, update_user varchar2
, approval_user varchar2
, create_date date
, update_date date
, approval_date date
, use_available varchar2
, order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_ship_carton_id := tsf_ship_carton_id;
self.store_id := store_id;
self.external_id := external_id;
self.tsf_ship_id := tsf_ship_id;
self.status := status;
self.destination_type := destination_type;
self.destination_name := destination_name;
self.hierarchy_level := hierarchy_level;
self.TsfShipCtnSize := TsfShipCtnSize;
self.weight := weight;
self.weight_uom := weight_uom;
self.tracking_number := tracking_number;
self.create_user := create_user;
self.update_user := update_user;
self.approval_user := approval_user;
self.create_date := create_date;
self.update_date := update_date;
self.approval_date := approval_date;
self.use_available := use_available;
self.order_related := order_related;
RETURN;
end;
constructor function "RIB_TsfShipCtnDesc_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, store_id number
, external_id varchar2
, tsf_ship_id number
, status varchar2
, destination_type varchar2
, destination_name varchar2
, hierarchy_level varchar2
, TsfShipCtnSize "RIB_TsfShipCtnSize_REC"
, weight number
, weight_uom varchar2
, tracking_number varchar2
, create_user varchar2
, update_user varchar2
, approval_user varchar2
, create_date date
, update_date date
, approval_date date
, use_available varchar2
, order_related varchar2
, TsfShipCtnItm_TBL "RIB_TsfShipCtnItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_ship_carton_id := tsf_ship_carton_id;
self.store_id := store_id;
self.external_id := external_id;
self.tsf_ship_id := tsf_ship_id;
self.status := status;
self.destination_type := destination_type;
self.destination_name := destination_name;
self.hierarchy_level := hierarchy_level;
self.TsfShipCtnSize := TsfShipCtnSize;
self.weight := weight;
self.weight_uom := weight_uom;
self.tracking_number := tracking_number;
self.create_user := create_user;
self.update_user := update_user;
self.approval_user := approval_user;
self.create_date := create_date;
self.update_date := update_date;
self.approval_date := approval_date;
self.use_available := use_available;
self.order_related := order_related;
self.TsfShipCtnItm_TBL := TsfShipCtnItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfShipCtnSize_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnSize_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id number(15),
  store_id number(10),
  description varchar2(128),
  height number(20,4),
  width number(20,4),
  length number(20,4),
  unit_of_measure varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnSize_REC"
(
  rib_oid number
, id number
, store_id number
, description varchar2
, height number
, width number
, length number
, unit_of_measure varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnSize_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnDesc') := "ns_name_TsfShipCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'height') := height;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
END appendNodeValues;
constructor function "RIB_TsfShipCtnSize_REC"
(
  rib_oid number
, id number
, store_id number
, description varchar2
, height number
, width number
, length number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.store_id := store_id;
self.description := description;
self.height := height;
self.width := width;
self.length := length;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_TsfShipCtnItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_item_id number(15),
  item_id varchar2(25),
  description varchar2(400),
  case_size number(10,2),
  quantity number(10),
  shipment_reason_id number(15),
  transfer_id number(12),
  cust_ord_ext_id varchar2(128),
  ful_ord_ext_id varchar2(128),
  uin_col "RIB_uin_col_TBL",   -- Size of "RIB_uin_col_TBL" is 5000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
) return self as result
,constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
) return self as result
,constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
, ful_ord_ext_id varchar2
) return self as result
,constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
, ful_ord_ext_id varchar2
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
) return self as result
,constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
, ful_ord_ext_id varchar2
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnDesc') := "ns_name_TsfShipCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_id') := line_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_reason_id') := shipment_reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_id') := transfer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_ord_ext_id') := cust_ord_ext_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ful_ord_ext_id') := ful_ord_ext_id;
  IF uin_col IS NOT NULL THEN
    FOR INDX IN uin_col.FIRST()..uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'uin_col'||'.'):=uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
RETURN;
end;
constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.cust_ord_ext_id := cust_ord_ext_id;
RETURN;
end;
constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
, ful_ord_ext_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.cust_ord_ext_id := cust_ord_ext_id;
self.ful_ord_ext_id := ful_ord_ext_id;
RETURN;
end;
constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
, ful_ord_ext_id varchar2
, uin_col "RIB_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.cust_ord_ext_id := cust_ord_ext_id;
self.ful_ord_ext_id := ful_ord_ext_id;
self.uin_col := uin_col;
RETURN;
end;
constructor function "RIB_TsfShipCtnItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, cust_ord_ext_id varchar2
, ful_ord_ext_id varchar2
, uin_col "RIB_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.cust_ord_ext_id := cust_ord_ext_id;
self.ful_ord_ext_id := ful_ord_ext_id;
self.uin_col := uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_uin_col_TBL" AS TABLE OF varchar2(128);
/
