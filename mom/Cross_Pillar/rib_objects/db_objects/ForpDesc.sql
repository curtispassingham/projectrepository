DROP TYPE "RIB_ForpDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpDesc_REC";
/
DROP TYPE "RIB_ForpItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpItm_REC";
/
DROP TYPE "RIB_ForpItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpItm_TBL" AS TABLE OF "RIB_ForpItm_REC";
/
DROP TYPE "RIB_ForpDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ForpDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ForpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  reverse_pick_id number(12),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_date date,
  create_user_name varchar2(128),
  ForpItm_TBL "RIB_ForpItm_TBL",   -- Size of "RIB_ForpItm_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ForpDesc_REC"
(
  rib_oid number
, reverse_pick_id number
, status varchar2
, create_date date
, create_user_name varchar2
) return self as result
,constructor function "RIB_ForpDesc_REC"
(
  rib_oid number
, reverse_pick_id number
, status varchar2
, create_date date
, create_user_name varchar2
, ForpItm_TBL "RIB_ForpItm_TBL"  -- Size of "RIB_ForpItm_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ForpDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ForpDesc') := "ns_name_ForpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'reverse_pick_id') := reverse_pick_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  IF ForpItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ForpItm_TBL.';
    FOR INDX IN ForpItm_TBL.FIRST()..ForpItm_TBL.LAST() LOOP
      ForpItm_TBL(indx).appendNodeValues( i_prefix||indx||'ForpItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ForpDesc_REC"
(
  rib_oid number
, reverse_pick_id number
, status varchar2
, create_date date
, create_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.reverse_pick_id := reverse_pick_id;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
RETURN;
end;
constructor function "RIB_ForpDesc_REC"
(
  rib_oid number
, reverse_pick_id number
, status varchar2
, create_date date
, create_user_name varchar2
, ForpItm_TBL "RIB_ForpItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.reverse_pick_id := reverse_pick_id;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.ForpItm_TBL := ForpItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ForpItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ForpItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ForpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  fulfillment_order_line_id number(12),
  standard_uom varchar2(4),
  suggested_quantity number(20,4),
  unpicked_quantity number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ForpItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, fulfillment_order_line_id number
, standard_uom varchar2
, suggested_quantity number
, unpicked_quantity number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ForpItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ForpDesc') := "ns_name_ForpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_line_id') := fulfillment_order_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'standard_uom') := standard_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggested_quantity') := suggested_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unpicked_quantity') := unpicked_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_ForpItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, fulfillment_order_line_id number
, standard_uom varchar2
, suggested_quantity number
, unpicked_quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.fulfillment_order_line_id := fulfillment_order_line_id;
self.standard_uom := standard_uom;
self.suggested_quantity := suggested_quantity;
self.unpicked_quantity := unpicked_quantity;
self.case_size := case_size;
RETURN;
end;
END;
/
