DROP TYPE "RIB_ItmBasketModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBasketModVo_REC";
/
DROP TYPE "RIB_ItmBasketModItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBasketModItm_REC";
/
DROP TYPE "RIB_ItmBasketModItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBasketModItm_TBL" AS TABLE OF "RIB_ItmBasketModItm_REC";
/
DROP TYPE "RIB_ItmBasketModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmBasketModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmBasketModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_basket_id number(12),
  external_id varchar2(128),
  type_id number(2),
  store_id number(10),
  ItmBasketModItm_TBL "RIB_ItmBasketModItm_TBL",   -- Size of "RIB_ItmBasketModItm_TBL" is 5000
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmBasketModVo_REC"
(
  rib_oid number
, item_basket_id number
, external_id varchar2
, type_id number
, store_id number
) return self as result
,constructor function "RIB_ItmBasketModVo_REC"
(
  rib_oid number
, item_basket_id number
, external_id varchar2
, type_id number
, store_id number
, ItmBasketModItm_TBL "RIB_ItmBasketModItm_TBL"  -- Size of "RIB_ItmBasketModItm_TBL" is 5000
) return self as result
,constructor function "RIB_ItmBasketModVo_REC"
(
  rib_oid number
, item_basket_id number
, external_id varchar2
, type_id number
, store_id number
, ItmBasketModItm_TBL "RIB_ItmBasketModItm_TBL"  -- Size of "RIB_ItmBasketModItm_TBL" is 5000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmBasketModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmBasketModVo') := "ns_name_ItmBasketModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_basket_id') := item_basket_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'type_id') := type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  IF ItmBasketModItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItmBasketModItm_TBL.';
    FOR INDX IN ItmBasketModItm_TBL.FIRST()..ItmBasketModItm_TBL.LAST() LOOP
      ItmBasketModItm_TBL(indx).appendNodeValues( i_prefix||indx||'ItmBasketModItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ItmBasketModVo_REC"
(
  rib_oid number
, item_basket_id number
, external_id varchar2
, type_id number
, store_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_basket_id := item_basket_id;
self.external_id := external_id;
self.type_id := type_id;
self.store_id := store_id;
RETURN;
end;
constructor function "RIB_ItmBasketModVo_REC"
(
  rib_oid number
, item_basket_id number
, external_id varchar2
, type_id number
, store_id number
, ItmBasketModItm_TBL "RIB_ItmBasketModItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_basket_id := item_basket_id;
self.external_id := external_id;
self.type_id := type_id;
self.store_id := store_id;
self.ItmBasketModItm_TBL := ItmBasketModItm_TBL;
RETURN;
end;
constructor function "RIB_ItmBasketModVo_REC"
(
  rib_oid number
, item_basket_id number
, external_id varchar2
, type_id number
, store_id number
, ItmBasketModItm_TBL "RIB_ItmBasketModItm_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_basket_id := item_basket_id;
self.external_id := external_id;
self.type_id := type_id;
self.store_id := store_id;
self.ItmBasketModItm_TBL := ItmBasketModItm_TBL;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(10);
/
DROP TYPE "RIB_ItmBasketModItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmBasketModItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmBasketModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  uin varchar2(128),
  quantity number(12,4),
  case_size number(12,4),
  uom varchar2(40),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmBasketModItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, uin varchar2
, quantity number
, case_size number
, uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmBasketModItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmBasketModVo') := "ns_name_ItmBasketModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'uom') := uom;
END appendNodeValues;
constructor function "RIB_ItmBasketModItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, uin varchar2
, quantity number
, case_size number
, uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.uin := uin;
self.quantity := quantity;
self.case_size := case_size;
self.uom := uom;
RETURN;
end;
END;
/
