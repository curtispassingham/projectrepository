@@PrcChgExcDtl.sql;
/
DROP TYPE "RIB_PrcChgExcDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgExcDesc_REC";
/
DROP TYPE "RIB_PrcChgExcDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgExcDtl_TBL" AS TABLE OF "RIB_PrcChgExcDtl_REC";
/
DROP TYPE "RIB_PrcChgExcDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcChgExcDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgExcDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  status_code varchar2(10),
  loc number(10),
  loc_type varchar2(1),
  search_limit_no number(5),
  dept number(4),
  class number(4),
  subclass number(4),
  PrcChgExcDtl_TBL "RIB_PrcChgExcDtl_TBL",   -- Size of "RIB_PrcChgExcDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcChgExcDesc_REC"
(
  rib_oid number
, status_code varchar2
, loc number
, loc_type varchar2
, search_limit_no number
, dept number
, class number
, subclass number
, PrcChgExcDtl_TBL "RIB_PrcChgExcDtl_TBL"  -- Size of "RIB_PrcChgExcDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcChgExcDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgExcDesc') := "ns_name_PrcChgExcDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'status_code') := status_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'search_limit_no') := search_limit_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'class') := class;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass') := subclass;
  l_new_pre :=i_prefix||'PrcChgExcDtl_TBL.';
  FOR INDX IN PrcChgExcDtl_TBL.FIRST()..PrcChgExcDtl_TBL.LAST() LOOP
    PrcChgExcDtl_TBL(indx).appendNodeValues( i_prefix||indx||'PrcChgExcDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrcChgExcDesc_REC"
(
  rib_oid number
, status_code varchar2
, loc number
, loc_type varchar2
, search_limit_no number
, dept number
, class number
, subclass number
, PrcChgExcDtl_TBL "RIB_PrcChgExcDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.status_code := status_code;
self.loc := loc;
self.loc_type := loc_type;
self.search_limit_no := search_limit_no;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.PrcChgExcDtl_TBL := PrcChgExcDtl_TBL;
RETURN;
end;
END;
/
