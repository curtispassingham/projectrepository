DROP TYPE "RIB_StrVnsCtnHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnHdrDesc_REC";
/
DROP TYPE "RIB_StrVnsCtnHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsCtnHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  container_id number(15),
  store_id number(10),
  container_label varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  number_of_line_items number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsCtnHdrDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, number_of_line_items number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsCtnHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsCtnHdrDesc') := "ns_name_StrVnsCtnHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_label') := container_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_line_items') := number_of_line_items;
END appendNodeValues;
constructor function "RIB_StrVnsCtnHdrDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, number_of_line_items number
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.number_of_line_items := number_of_line_items;
RETURN;
end;
END;
/
