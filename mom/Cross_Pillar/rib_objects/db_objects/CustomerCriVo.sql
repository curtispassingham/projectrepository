@@CustomerRef.sql;
/
@@EmployeeRef.sql;
/
@@LoyAcctRef.sql;
/
DROP TYPE "RIB_CustomerCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerCriVo_REC";
/
DROP TYPE "RIB_CustomerInfoCriteria_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerInfoCriteria_REC";
/
DROP TYPE "RIB_BusinessInfoCriteria_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BusinessInfoCriteria_REC";
/
DROP TYPE "RIB_CustomerCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  search_type varchar2(23), -- search_type is enumeration field, valid values are [SEARCH_BY_CUSTOMER_ID, SEARCH_BY_EMPLOYEE_ID, SEARCH_BY_TAX_ID, SEARCH_BY_PHONE_NUMBER, SEARCH_BY_CUSTOMER_INFO, SEARCH_BY_BUSINESS_INFO, SEARCH_BY_EMAIL] (all lower-case)
  CustomerRef "RIB_CustomerRef_REC",
  EmployeeRef "RIB_EmployeeRef_REC",
  tax_id varchar2(1000),
  phone_number varchar2(30),
  email_address varchar2(30),
  LoyAcctRef "RIB_LoyAcctRef_REC",
  CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC",
  BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC",
  wildcard_search varchar2(5), --wildcard_search is boolean field, valid values are true,false (all lower-case) 
  ignore_case_search varchar2(5), --ignore_case_search is boolean field, valid values are true,false (all lower-case) 
  max_record number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
, wildcard_search varchar2  --wildcard_search is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
, wildcard_search varchar2  --wildcard_search is boolean field, valid values are true,false (all lower-case)
, ignore_case_search varchar2  --ignore_case_search is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
, wildcard_search varchar2  --wildcard_search is boolean field, valid values are true,false (all lower-case)
, ignore_case_search varchar2  --ignore_case_search is boolean field, valid values are true,false (all lower-case)
, max_record number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerCriVo') := "ns_name_CustomerCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'search_type') := search_type;
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'EmployeeRef.';
  EmployeeRef.appendNodeValues( i_prefix||'EmployeeRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_id') := tax_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'email_address') := email_address;
  l_new_pre :=i_prefix||'LoyAcctRef.';
  LoyAcctRef.appendNodeValues( i_prefix||'LoyAcctRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustomerInfoCriteria.';
  CustomerInfoCriteria.appendNodeValues( i_prefix||'CustomerInfoCriteria');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'BusinessInfoCriteria.';
  BusinessInfoCriteria.appendNodeValues( i_prefix||'BusinessInfoCriteria');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'wildcard_search') := wildcard_search;
  rib_obj_util.g_RIB_element_values(i_prefix||'ignore_case_search') := ignore_case_search;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_record') := max_record;
END appendNodeValues;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
self.LoyAcctRef := LoyAcctRef;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
self.LoyAcctRef := LoyAcctRef;
self.CustomerInfoCriteria := CustomerInfoCriteria;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
self.LoyAcctRef := LoyAcctRef;
self.CustomerInfoCriteria := CustomerInfoCriteria;
self.BusinessInfoCriteria := BusinessInfoCriteria;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
, wildcard_search varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
self.LoyAcctRef := LoyAcctRef;
self.CustomerInfoCriteria := CustomerInfoCriteria;
self.BusinessInfoCriteria := BusinessInfoCriteria;
self.wildcard_search := wildcard_search;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
, wildcard_search varchar2
, ignore_case_search varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
self.LoyAcctRef := LoyAcctRef;
self.CustomerInfoCriteria := CustomerInfoCriteria;
self.BusinessInfoCriteria := BusinessInfoCriteria;
self.wildcard_search := wildcard_search;
self.ignore_case_search := ignore_case_search;
RETURN;
end;
constructor function "RIB_CustomerCriVo_REC"
(
  rib_oid number
, search_type varchar2
, CustomerRef "RIB_CustomerRef_REC"
, EmployeeRef "RIB_EmployeeRef_REC"
, tax_id varchar2
, phone_number varchar2
, email_address varchar2
, LoyAcctRef "RIB_LoyAcctRef_REC"
, CustomerInfoCriteria "RIB_CustomerInfoCriteria_REC"
, BusinessInfoCriteria "RIB_BusinessInfoCriteria_REC"
, wildcard_search varchar2
, ignore_case_search varchar2
, max_record number
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.CustomerRef := CustomerRef;
self.EmployeeRef := EmployeeRef;
self.tax_id := tax_id;
self.phone_number := phone_number;
self.email_address := email_address;
self.LoyAcctRef := LoyAcctRef;
self.CustomerInfoCriteria := CustomerInfoCriteria;
self.BusinessInfoCriteria := BusinessInfoCriteria;
self.wildcard_search := wildcard_search;
self.ignore_case_search := ignore_case_search;
self.max_record := max_record;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustomerInfoCriteria_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerInfoCriteria_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  first_name varchar2(120),
  last_name varchar2(120),
  postal_code varchar2(30),
  address_1 varchar2(240),
  phone_number varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
) return self as result
,constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
) return self as result
,constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, postal_code varchar2
) return self as result
,constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, postal_code varchar2
, address_1 varchar2
) return self as result
,constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, postal_code varchar2
, address_1 varchar2
, phone_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerInfoCriteria_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerCriVo') := "ns_name_CustomerCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_1') := address_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
END appendNodeValues;
constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
RETURN;
end;
constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
RETURN;
end;
constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
self.postal_code := postal_code;
RETURN;
end;
constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, postal_code varchar2
, address_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
self.postal_code := postal_code;
self.address_1 := address_1;
RETURN;
end;
constructor function "RIB_CustomerInfoCriteria_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, postal_code varchar2
, address_1 varchar2
, phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
self.postal_code := postal_code;
self.address_1 := address_1;
self.phone_number := phone_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_BusinessInfoCriteria_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BusinessInfoCriteria_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  company_name varchar2(120),
  postal_code varchar2(30),
  address_1 varchar2(240),
  phone_number varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
) return self as result
,constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
, postal_code varchar2
) return self as result
,constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
, postal_code varchar2
, address_1 varchar2
) return self as result
,constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
, postal_code varchar2
, address_1 varchar2
, phone_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BusinessInfoCriteria_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerCriVo') := "ns_name_CustomerCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'company_name') := company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_1') := address_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
END appendNodeValues;
constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.company_name := company_name;
RETURN;
end;
constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
, postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.company_name := company_name;
self.postal_code := postal_code;
RETURN;
end;
constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
, postal_code varchar2
, address_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.company_name := company_name;
self.postal_code := postal_code;
self.address_1 := address_1;
RETURN;
end;
constructor function "RIB_BusinessInfoCriteria_REC"
(
  rib_oid number
, company_name varchar2
, postal_code varchar2
, address_1 varchar2
, phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.company_name := company_name;
self.postal_code := postal_code;
self.address_1 := address_1;
self.phone_number := phone_number;
RETURN;
end;
END;
/
