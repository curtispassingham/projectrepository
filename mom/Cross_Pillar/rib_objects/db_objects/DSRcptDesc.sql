DROP TYPE "RIB_DSRcptDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSRcptDesc_REC";
/
DROP TYPE "RIB_DSRcptDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSRcptDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSRcptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  trans_date date,
  item varchar2(25),
  store number(10),
  units number(12,4),
  unit_cost number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSRcptDesc_REC"
(
  rib_oid number
, trans_date date
, item varchar2
, store number
, units number
, unit_cost number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSRcptDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSRcptDesc') := "ns_name_DSRcptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'trans_date') := trans_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  rib_obj_util.g_RIB_element_values(i_prefix||'units') := units;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
END appendNodeValues;
constructor function "RIB_DSRcptDesc_REC"
(
  rib_oid number
, trans_date date
, item varchar2
, store number
, units number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.trans_date := trans_date;
self.item := item;
self.store := store;
self.units := units;
self.unit_cost := unit_cost;
RETURN;
end;
END;
/
