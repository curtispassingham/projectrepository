@@ExtOfSupplierColDesc.sql;
/
@@LocOfSupplierColDesc.sql;
/
@@SupplierDesc.sql;
/
DROP TYPE "RIB_SupplierColDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierColDesc_REC";
/
DROP TYPE "RIB_SupplierDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierDesc_TBL" AS TABLE OF "RIB_SupplierDesc_REC";
/
DROP TYPE "RIB_SupplierColDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupplierColDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  collection_size number(4),
  SupplierDesc_TBL "RIB_SupplierDesc_TBL",   -- Size of "RIB_SupplierDesc_TBL" is unbounded
  ExtOfSupplierColDesc "RIB_ExtOfSupplierColDesc_REC",
  LocOfSupplierColDesc "RIB_LocOfSupplierColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupplierColDesc_REC"
(
  rib_oid number
, collection_size number
, SupplierDesc_TBL "RIB_SupplierDesc_TBL"  -- Size of "RIB_SupplierDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_SupplierColDesc_REC"
(
  rib_oid number
, collection_size number
, SupplierDesc_TBL "RIB_SupplierDesc_TBL"  -- Size of "RIB_SupplierDesc_TBL" is unbounded
, ExtOfSupplierColDesc "RIB_ExtOfSupplierColDesc_REC"
) return self as result
,constructor function "RIB_SupplierColDesc_REC"
(
  rib_oid number
, collection_size number
, SupplierDesc_TBL "RIB_SupplierDesc_TBL"  -- Size of "RIB_SupplierDesc_TBL" is unbounded
, ExtOfSupplierColDesc "RIB_ExtOfSupplierColDesc_REC"
, LocOfSupplierColDesc "RIB_LocOfSupplierColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupplierColDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierColDesc') := "ns_name_SupplierColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'collection_size') := collection_size;
  l_new_pre :=i_prefix||'SupplierDesc_TBL.';
  FOR INDX IN SupplierDesc_TBL.FIRST()..SupplierDesc_TBL.LAST() LOOP
    SupplierDesc_TBL(indx).appendNodeValues( i_prefix||indx||'SupplierDesc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'ExtOfSupplierColDesc.';
  ExtOfSupplierColDesc.appendNodeValues( i_prefix||'ExtOfSupplierColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LocOfSupplierColDesc.';
  LocOfSupplierColDesc.appendNodeValues( i_prefix||'LocOfSupplierColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_SupplierColDesc_REC"
(
  rib_oid number
, collection_size number
, SupplierDesc_TBL "RIB_SupplierDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.collection_size := collection_size;
self.SupplierDesc_TBL := SupplierDesc_TBL;
RETURN;
end;
constructor function "RIB_SupplierColDesc_REC"
(
  rib_oid number
, collection_size number
, SupplierDesc_TBL "RIB_SupplierDesc_TBL"
, ExtOfSupplierColDesc "RIB_ExtOfSupplierColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.collection_size := collection_size;
self.SupplierDesc_TBL := SupplierDesc_TBL;
self.ExtOfSupplierColDesc := ExtOfSupplierColDesc;
RETURN;
end;
constructor function "RIB_SupplierColDesc_REC"
(
  rib_oid number
, collection_size number
, SupplierDesc_TBL "RIB_SupplierDesc_TBL"
, ExtOfSupplierColDesc "RIB_ExtOfSupplierColDesc_REC"
, LocOfSupplierColDesc "RIB_LocOfSupplierColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.collection_size := collection_size;
self.SupplierDesc_TBL := SupplierDesc_TBL;
self.ExtOfSupplierColDesc := ExtOfSupplierColDesc;
self.LocOfSupplierColDesc := LocOfSupplierColDesc;
RETURN;
end;
END;
/
