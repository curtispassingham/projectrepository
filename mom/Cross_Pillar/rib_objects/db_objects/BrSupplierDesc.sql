@@EOfBrSupplierDesc.sql;
/
@@NameValPairRBO.sql;
/
DROP TYPE "RIB_BrSupplierDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupplierDesc_REC";
/
DROP TYPE "RIB_BrSupAttr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupAttr_REC";
/
DROP TYPE "RIB_BrTributarySub_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrTributarySub_REC";
/
DROP TYPE "RIB_BrCnaeCodes_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrCnaeCodes_REC";
/
DROP TYPE "RIB_BrTaxRegime_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrTaxRegime_REC";
/
DROP TYPE "RIB_BrSupSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupSite_REC";
/
DROP TYPE "RIB_BrSupSiteOrgUnit_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupSiteOrgUnit_REC";
/
DROP TYPE "RIB_BrSupSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupSiteAddr_REC";
/
DROP TYPE "RIB_BrAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrAddr_REC";
/
DROP TYPE "RIB_BrSupplierDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrSupplierDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrSupplierDesc "RIB_EOfBrSupplierDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrSupplierDesc_REC"
(
  rib_oid number
, EOfBrSupplierDesc "RIB_EOfBrSupplierDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrSupplierDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'EOfBrSupplierDesc.';
  EOfBrSupplierDesc.appendNodeValues( i_prefix||'EOfBrSupplierDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrSupplierDesc_REC"
(
  rib_oid number
, EOfBrSupplierDesc "RIB_EOfBrSupplierDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrSupplierDesc := EOfBrSupplierDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrTributarySub_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrTributarySub_TBL" AS TABLE OF "RIB_BrTributarySub_REC";
/
DROP TYPE "RIB_BrCnaeCodes_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrCnaeCodes_TBL" AS TABLE OF "RIB_BrCnaeCodes_REC";
/
DROP TYPE "RIB_BrTaxRegime_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrTaxRegime_TBL" AS TABLE OF "RIB_BrTaxRegime_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_BrSupAttr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrSupAttr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  taxpayer_type varchar2(1),
  addr_1 varchar2(240),
  addr_2 varchar2(240),
  addr_3 varchar2(240),
  neighborhood varchar2(240),
  jurisdiction_code varchar2(10),
  state varchar2(5),
  country varchar2(3),
  postal_code varchar2(30),
  cpf varchar2(11),
  cnpj varchar2(14),
  nit varchar2(250),
  suframa varchar2(250),
  im varchar2(250),
  ie varchar2(250),
  iss_contrib_ind varchar2(1),
  simples_ind varchar2(1),
  st_contrib_ind varchar2(1),
  rural_prod_ind varchar2(1),
  ipi_ind varchar2(1),
  icms_contrib_ins varchar2(1),
  pis_contrib_ind varchar2(1),
  cofins_contrib_ind varchar2(1),
  is_income_range_eligible varchar2(1),
  is_distr_a_manufacturer varchar2(1),
  icms_simples_rate number(20,4),
  contrib_type varchar2(30),
  tax_exception_type varchar2(30),
  ie_exempt varchar2(30),
  BrTributarySub_TBL "RIB_BrTributarySub_TBL",   -- Size of "RIB_BrTributarySub_TBL" is unbounded
  BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL",   -- Size of "RIB_BrCnaeCodes_TBL" is unbounded
  BrTaxRegime_TBL "RIB_BrTaxRegime_TBL",   -- Size of "RIB_BrTaxRegime_TBL" is unbounded
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  EOfBrSupAttr "RIB_EOfBrSupAttr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"  -- Size of "RIB_BrTributarySub_TBL" is unbounded
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"  -- Size of "RIB_BrTributarySub_TBL" is unbounded
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"  -- Size of "RIB_BrCnaeCodes_TBL" is unbounded
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"  -- Size of "RIB_BrTributarySub_TBL" is unbounded
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"  -- Size of "RIB_BrCnaeCodes_TBL" is unbounded
, BrTaxRegime_TBL "RIB_BrTaxRegime_TBL"  -- Size of "RIB_BrTaxRegime_TBL" is unbounded
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"  -- Size of "RIB_BrTributarySub_TBL" is unbounded
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"  -- Size of "RIB_BrCnaeCodes_TBL" is unbounded
, BrTaxRegime_TBL "RIB_BrTaxRegime_TBL"  -- Size of "RIB_BrTaxRegime_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"  -- Size of "RIB_BrTributarySub_TBL" is unbounded
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"  -- Size of "RIB_BrCnaeCodes_TBL" is unbounded
, BrTaxRegime_TBL "RIB_BrTaxRegime_TBL"  -- Size of "RIB_BrTaxRegime_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, EOfBrSupAttr "RIB_EOfBrSupAttr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrSupAttr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'taxpayer_type') := taxpayer_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_1') := addr_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_2') := addr_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_3') := addr_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'neighborhood') := neighborhood;
  rib_obj_util.g_RIB_element_values(i_prefix||'jurisdiction_code') := jurisdiction_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country') := country;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'cpf') := cpf;
  rib_obj_util.g_RIB_element_values(i_prefix||'cnpj') := cnpj;
  rib_obj_util.g_RIB_element_values(i_prefix||'nit') := nit;
  rib_obj_util.g_RIB_element_values(i_prefix||'suframa') := suframa;
  rib_obj_util.g_RIB_element_values(i_prefix||'im') := im;
  rib_obj_util.g_RIB_element_values(i_prefix||'ie') := ie;
  rib_obj_util.g_RIB_element_values(i_prefix||'iss_contrib_ind') := iss_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'simples_ind') := simples_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'st_contrib_ind') := st_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'rural_prod_ind') := rural_prod_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_ind') := ipi_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_contrib_ins') := icms_contrib_ins;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_contrib_ind') := pis_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_contrib_ind') := cofins_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_income_range_eligible') := is_income_range_eligible;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_distr_a_manufacturer') := is_distr_a_manufacturer;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_simples_rate') := icms_simples_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'contrib_type') := contrib_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_exception_type') := tax_exception_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'ie_exempt') := ie_exempt;
  IF BrTributarySub_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrTributarySub_TBL.';
    FOR INDX IN BrTributarySub_TBL.FIRST()..BrTributarySub_TBL.LAST() LOOP
      BrTributarySub_TBL(indx).appendNodeValues( i_prefix||indx||'BrTributarySub_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrCnaeCodes_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrCnaeCodes_TBL.';
    FOR INDX IN BrCnaeCodes_TBL.FIRST()..BrCnaeCodes_TBL.LAST() LOOP
      BrCnaeCodes_TBL(indx).appendNodeValues( i_prefix||indx||'BrCnaeCodes_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrTaxRegime_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrTaxRegime_TBL.';
    FOR INDX IN BrTaxRegime_TBL.FIRST()..BrTaxRegime_TBL.LAST() LOOP
      BrTaxRegime_TBL(indx).appendNodeValues( i_prefix||indx||'BrTaxRegime_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'EOfBrSupAttr.';
  EOfBrSupAttr.appendNodeValues( i_prefix||'EOfBrSupAttr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
self.ie_exempt := ie_exempt;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
self.ie_exempt := ie_exempt;
self.BrTributarySub_TBL := BrTributarySub_TBL;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
self.ie_exempt := ie_exempt;
self.BrTributarySub_TBL := BrTributarySub_TBL;
self.BrCnaeCodes_TBL := BrCnaeCodes_TBL;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"
, BrTaxRegime_TBL "RIB_BrTaxRegime_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
self.ie_exempt := ie_exempt;
self.BrTributarySub_TBL := BrTributarySub_TBL;
self.BrCnaeCodes_TBL := BrCnaeCodes_TBL;
self.BrTaxRegime_TBL := BrTaxRegime_TBL;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"
, BrTaxRegime_TBL "RIB_BrTaxRegime_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
self.ie_exempt := ie_exempt;
self.BrTributarySub_TBL := BrTributarySub_TBL;
self.BrCnaeCodes_TBL := BrCnaeCodes_TBL;
self.BrTaxRegime_TBL := BrTaxRegime_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
constructor function "RIB_BrSupAttr_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, simples_ind varchar2
, st_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ins varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, contrib_type varchar2
, tax_exception_type varchar2
, ie_exempt varchar2
, BrTributarySub_TBL "RIB_BrTributarySub_TBL"
, BrCnaeCodes_TBL "RIB_BrCnaeCodes_TBL"
, BrTaxRegime_TBL "RIB_BrTaxRegime_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, EOfBrSupAttr "RIB_EOfBrSupAttr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.simples_ind := simples_ind;
self.st_contrib_ind := st_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ins := icms_contrib_ins;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.contrib_type := contrib_type;
self.tax_exception_type := tax_exception_type;
self.ie_exempt := ie_exempt;
self.BrTributarySub_TBL := BrTributarySub_TBL;
self.BrCnaeCodes_TBL := BrCnaeCodes_TBL;
self.BrTaxRegime_TBL := BrTaxRegime_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.EOfBrSupAttr := EOfBrSupAttr;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrTributarySub_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrTributarySub_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  country_id varchar2(3),
  state varchar2(3),
  ie_subs varchar2(250),
  EOfBrTributarySub "RIB_EOfBrTributarySub_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
) return self as result
,constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
, state varchar2
) return self as result
,constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
, state varchar2
, ie_subs varchar2
) return self as result
,constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
, state varchar2
, ie_subs varchar2
, EOfBrTributarySub "RIB_EOfBrTributarySub_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrTributarySub_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'ie_subs') := ie_subs;
  l_new_pre :=i_prefix||'EOfBrTributarySub.';
  EOfBrTributarySub.appendNodeValues( i_prefix||'EOfBrTributarySub');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
, state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
self.state := state;
RETURN;
end;
constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
, state varchar2
, ie_subs varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
self.state := state;
self.ie_subs := ie_subs;
RETURN;
end;
constructor function "RIB_BrTributarySub_REC"
(
  rib_oid number
, country_id varchar2
, state varchar2
, ie_subs varchar2
, EOfBrTributarySub "RIB_EOfBrTributarySub_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
self.state := state;
self.ie_subs := ie_subs;
self.EOfBrTributarySub := EOfBrTributarySub;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrCnaeCodes_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrCnaeCodes_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  module varchar2(4),
  country_id varchar2(3),
  cnae_code varchar2(9),
  primary_ind varchar2(1),
  EOfBrCnaeCodes "RIB_EOfBrCnaeCodes_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
) return self as result
,constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
, cnae_code varchar2
) return self as result
,constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
, cnae_code varchar2
, primary_ind varchar2
) return self as result
,constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
, cnae_code varchar2
, primary_ind varchar2
, EOfBrCnaeCodes "RIB_EOfBrCnaeCodes_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrCnaeCodes_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'module') := module;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'cnae_code') := cnae_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_ind') := primary_ind;
  l_new_pre :=i_prefix||'EOfBrCnaeCodes.';
  EOfBrCnaeCodes.appendNodeValues( i_prefix||'EOfBrCnaeCodes');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
, cnae_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.country_id := country_id;
self.cnae_code := cnae_code;
RETURN;
end;
constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
, cnae_code varchar2
, primary_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.country_id := country_id;
self.cnae_code := cnae_code;
self.primary_ind := primary_ind;
RETURN;
end;
constructor function "RIB_BrCnaeCodes_REC"
(
  rib_oid number
, module varchar2
, country_id varchar2
, cnae_code varchar2
, primary_ind varchar2
, EOfBrCnaeCodes "RIB_EOfBrCnaeCodes_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.country_id := country_id;
self.cnae_code := cnae_code;
self.primary_ind := primary_ind;
self.EOfBrCnaeCodes := EOfBrCnaeCodes;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrTaxRegime_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrTaxRegime_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tax_regime varchar2(30),
  EOfBrTaxRegimeb "RIB_EOfBrTaxRegimeb_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrTaxRegime_REC"
(
  rib_oid number
, tax_regime varchar2
) return self as result
,constructor function "RIB_BrTaxRegime_REC"
(
  rib_oid number
, tax_regime varchar2
, EOfBrTaxRegimeb "RIB_EOfBrTaxRegimeb_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrTaxRegime_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_regime') := tax_regime;
  l_new_pre :=i_prefix||'EOfBrTaxRegimeb.';
  EOfBrTaxRegimeb.appendNodeValues( i_prefix||'EOfBrTaxRegimeb');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrTaxRegime_REC"
(
  rib_oid number
, tax_regime varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_regime := tax_regime;
RETURN;
end;
constructor function "RIB_BrTaxRegime_REC"
(
  rib_oid number
, tax_regime varchar2
, EOfBrTaxRegimeb "RIB_EOfBrTaxRegimeb_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_regime := tax_regime;
self.EOfBrTaxRegimeb := EOfBrTaxRegimeb;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrSupSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrSupSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrSupSite "RIB_EOfBrSupSite_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrSupSite_REC"
(
  rib_oid number
, EOfBrSupSite "RIB_EOfBrSupSite_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrSupSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrSupSite.';
  EOfBrSupSite.appendNodeValues( i_prefix||'EOfBrSupSite');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrSupSite_REC"
(
  rib_oid number
, EOfBrSupSite "RIB_EOfBrSupSite_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrSupSite := EOfBrSupSite;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrSupSiteOrgUnit_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrSupSiteOrgUnit_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrSupSiteOrgUnit "RIB_EOfBrSupSiteOrgUnit_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrSupSiteOrgUnit_REC"
(
  rib_oid number
, EOfBrSupSiteOrgUnit "RIB_EOfBrSupSiteOrgUnit_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrSupSiteOrgUnit_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrSupSiteOrgUnit.';
  EOfBrSupSiteOrgUnit.appendNodeValues( i_prefix||'EOfBrSupSiteOrgUnit');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrSupSiteOrgUnit_REC"
(
  rib_oid number
, EOfBrSupSiteOrgUnit "RIB_EOfBrSupSiteOrgUnit_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrSupSiteOrgUnit := EOfBrSupSiteOrgUnit;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrSupSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrSupSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrSupSiteAddr "RIB_EOfBrSupSiteAddr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrSupSiteAddr_REC"
(
  rib_oid number
, EOfBrSupSiteAddr "RIB_EOfBrSupSiteAddr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrSupSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrSupSiteAddr.';
  EOfBrSupSiteAddr.appendNodeValues( i_prefix||'EOfBrSupSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrSupSiteAddr_REC"
(
  rib_oid number
, EOfBrSupSiteAddr "RIB_EOfBrSupSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrSupSiteAddr := EOfBrSupSiteAddr;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrAddr "RIB_EOfBrAddr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrAddr_REC"
(
  rib_oid number
, EOfBrAddr "RIB_EOfBrAddr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrSupplierDesc') := "ns_name_BrSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrAddr.';
  EOfBrAddr.appendNodeValues( i_prefix||'EOfBrAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrAddr_REC"
(
  rib_oid number
, EOfBrAddr "RIB_EOfBrAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrAddr := EOfBrAddr;
RETURN;
end;
END;
/
