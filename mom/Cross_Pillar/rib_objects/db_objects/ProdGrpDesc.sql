DROP TYPE "RIB_ProdGrpDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpDesc_REC";
/
DROP TYPE "RIB_ProdGrpHrchy_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpHrchy_REC";
/
DROP TYPE "RIB_ProdGrpItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpItm_REC";
/
DROP TYPE "RIB_ProdGrpHrchy_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpHrchy_TBL" AS TABLE OF "RIB_ProdGrpHrchy_REC";
/
DROP TYPE "RIB_ProdGrpItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpItm_TBL" AS TABLE OF "RIB_ProdGrpItm_REC";
/
DROP TYPE "RIB_ProdGrpDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProdGrpDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  product_group_id number(10),
  group_type varchar2(40), -- group_type is enumeration field, valid values are [STOCK_COUNT_UNIT, STOCK_COUNT_UNIT_AMOUNT, STOCK_COUNT_PROBLEM_LINE, STOCK_COUNT_WASTAGE, STOCK_COUNT_RMS_SYNC, SHELF_REPLENISHMENT, ITEM_REQUEST, AUTO_TICKET_PRINT, UNKNOWN, NO_VALUE] (all lower-case)
  description varchar2(250),
  store_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [ACTIVE, CANCELED] (all lower-case)
  uom_mode varchar2(20), -- uom_mode is enumeration field, valid values are [STANDARD, CASES, PREFERRED] (all lower-case)
  variance_count number(12),
  variance_percent number(12,2),
  variance_value number(12,2),
  counting_method varchar2(20), -- counting_method is enumeration field, valid values are [GUIDED, UNGUIDED, THIRD_PARTY, AUTO, UNKNOWN, NO_VALUE] (all lower-case)
  breakdown_type varchar2(20), -- breakdown_type is enumeration field, valid values are [DEPARTMENT, CLASS, SUBCLASS, LOCATION, NONE, UNKNOWN, NO_VALUE] (all lower-case)
  diff_type varchar2(20), -- diff_type is enumeration field, valid values are [DIFF1, DIFF2, DIFF3, DIFF4, UNKNOWN, NO_VALUE] (all lower-case)
  all_items varchar2(5), --all_items is boolean field, valid values are true,false (all lower-case) 
  recount_active varchar2(5), --recount_active is boolean field, valid values are true,false (all lower-case) 
  auto_authorize varchar2(5), --auto_authorize is boolean field, valid values are true,false (all lower-case) 
  pick_less_suggested varchar2(5), --pick_less_suggested is boolean field, valid values are true,false (all lower-case) 
  shelf_repl_less_suggested varchar2(5), --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case) 
  negative_available varchar2(5), --negative_available is boolean field, valid values are true,false (all lower-case) 
  uin_discrepancy varchar2(5), --uin_discrepancy is boolean field, valid values are true,false (all lower-case) 
  include_active_items varchar2(5), --include_active_items is boolean field, valid values are true,false (all lower-case) 
  include_inactive_items varchar2(5), --include_inactive_items is boolean field, valid values are true,false (all lower-case) 
  include_discontinued_items varchar2(5), --include_discontinued_items is boolean field, valid values are true,false (all lower-case) 
  include_deleted_items varchar2(5), --include_deleted_items is boolean field, valid values are true,false (all lower-case) 
  include_soh_zero varchar2(5), --include_soh_zero is boolean field, valid values are true,false (all lower-case) 
  include_soh_less_than_zero varchar2(5), --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case) 
  include_soh_greater_than_zero varchar2(5), --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case) 
  auto_replenishment varchar2(5), --auto_replenishment is boolean field, valid values are true,false (all lower-case) 
  refresh_ticket_quantity varchar2(5), --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case) 
  days_to_expire_request number(12),
  days_to_request_delivery number(12),
  ProdGrpHrchy_TBL "RIB_ProdGrpHrchy_TBL",   -- Size of "RIB_ProdGrpHrchy_TBL" is 1000
  ProdGrpItm_TBL "RIB_ProdGrpItm_TBL",   -- Size of "RIB_ProdGrpItm_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
) return self as result
,constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
) return self as result
,constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpHrchy_TBL "RIB_ProdGrpHrchy_TBL"  -- Size of "RIB_ProdGrpHrchy_TBL" is 1000
) return self as result
,constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpHrchy_TBL "RIB_ProdGrpHrchy_TBL"  -- Size of "RIB_ProdGrpHrchy_TBL" is 1000
, ProdGrpItm_TBL "RIB_ProdGrpItm_TBL"  -- Size of "RIB_ProdGrpItm_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProdGrpDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpDesc') := "ns_name_ProdGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_type') := group_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'uom_mode') := uom_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_count') := variance_count;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_percent') := variance_percent;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_value') := variance_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'counting_method') := counting_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'breakdown_type') := breakdown_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type') := diff_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'all_items') := all_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'recount_active') := recount_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_authorize') := auto_authorize;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_less_suggested') := pick_less_suggested;
  rib_obj_util.g_RIB_element_values(i_prefix||'shelf_repl_less_suggested') := shelf_repl_less_suggested;
  rib_obj_util.g_RIB_element_values(i_prefix||'negative_available') := negative_available;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_discrepancy') := uin_discrepancy;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_active_items') := include_active_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_inactive_items') := include_inactive_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_discontinued_items') := include_discontinued_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_deleted_items') := include_deleted_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_zero') := include_soh_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_less_than_zero') := include_soh_less_than_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_greater_than_zero') := include_soh_greater_than_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_replenishment') := auto_replenishment;
  rib_obj_util.g_RIB_element_values(i_prefix||'refresh_ticket_quantity') := refresh_ticket_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'days_to_expire_request') := days_to_expire_request;
  rib_obj_util.g_RIB_element_values(i_prefix||'days_to_request_delivery') := days_to_request_delivery;
  IF ProdGrpHrchy_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ProdGrpHrchy_TBL.';
    FOR INDX IN ProdGrpHrchy_TBL.FIRST()..ProdGrpHrchy_TBL.LAST() LOOP
      ProdGrpHrchy_TBL(indx).appendNodeValues( i_prefix||indx||'ProdGrpHrchy_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ProdGrpItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ProdGrpItm_TBL.';
    FOR INDX IN ProdGrpItm_TBL.FIRST()..ProdGrpItm_TBL.LAST() LOOP
      ProdGrpItm_TBL(indx).appendNodeValues( i_prefix||indx||'ProdGrpItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
RETURN;
end;
constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
RETURN;
end;
constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
RETURN;
end;
constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpHrchy_TBL "RIB_ProdGrpHrchy_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
self.ProdGrpHrchy_TBL := ProdGrpHrchy_TBL;
RETURN;
end;
constructor function "RIB_ProdGrpDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpHrchy_TBL "RIB_ProdGrpHrchy_TBL"
, ProdGrpItm_TBL "RIB_ProdGrpItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
self.ProdGrpHrchy_TBL := ProdGrpHrchy_TBL;
self.ProdGrpItm_TBL := ProdGrpItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ProdGrpHrchy_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProdGrpHrchy_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  department_id number(12),
  class_id number(10),
  subclass_id number(10),
  number_of_items number(5),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
) return self as result
,constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
) return self as result
,constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
) return self as result
,constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
, number_of_items number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProdGrpHrchy_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpDesc') := "ns_name_ProdGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_items') := number_of_items;
END appendNodeValues;
constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
RETURN;
end;
constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
RETURN;
end;
constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
RETURN;
end;
constructor function "RIB_ProdGrpHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
, number_of_items number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.number_of_items := number_of_items;
RETURN;
end;
END;
/
DROP TYPE "RIB_ProdGrpItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProdGrpItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  short_description varchar2(250),
  long_description varchar2(250),
  mdse_hierarchy_id varchar2(25),
  department_id number(10),
  deparment_name varchar2(100),
  class_id number(10),
  class_name varchar2(100),
  sub_class_id number(10),
  sub_class_name varchar2(100),
  status varchar2(20), -- status is enumeration field, valid values are [NONE, ACTIVE, DISCONTINUED, INACTIVE, DELETED, AUTO_STOCKABLE, NON_RANGED] (all lower-case)
  ranged varchar2(5), --ranged is boolean field, valid values are true,false (all lower-case) 
  sellable varchar2(5), --sellable is boolean field, valid values are true,false (all lower-case) 
  store_order_repl_typ varchar2(5), --store_order_repl_typ is boolean field, valid values are true,false (all lower-case) 
  type varchar2(30), -- type is enumeration field, valid values are [ITEM, CONSIGNMENT, CONCESSION, NON_INVENTORY, PACK_SIMPLE, PACK_COMPLEX, PACK_SIMPLE_BREAKABLE, PACK_COMPLEX_BREAKABLE] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProdGrpItm_REC"
(
  rib_oid number
, item_id varchar2
, short_description varchar2
, long_description varchar2
, mdse_hierarchy_id varchar2
, department_id number
, deparment_name varchar2
, class_id number
, class_name varchar2
, sub_class_id number
, sub_class_name varchar2
, status varchar2
, ranged varchar2  --ranged is boolean field, valid values are true,false (all lower-case)
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, store_order_repl_typ varchar2  --store_order_repl_typ is boolean field, valid values are true,false (all lower-case)
, type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProdGrpItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpDesc') := "ns_name_ProdGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'short_description') := short_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'long_description') := long_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'mdse_hierarchy_id') := mdse_hierarchy_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'deparment_name') := deparment_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_name') := class_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'sub_class_id') := sub_class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'sub_class_name') := sub_class_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'ranged') := ranged;
  rib_obj_util.g_RIB_element_values(i_prefix||'sellable') := sellable;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_order_repl_typ') := store_order_repl_typ;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
END appendNodeValues;
constructor function "RIB_ProdGrpItm_REC"
(
  rib_oid number
, item_id varchar2
, short_description varchar2
, long_description varchar2
, mdse_hierarchy_id varchar2
, department_id number
, deparment_name varchar2
, class_id number
, class_name varchar2
, sub_class_id number
, sub_class_name varchar2
, status varchar2
, ranged varchar2
, sellable varchar2
, store_order_repl_typ varchar2
, type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.short_description := short_description;
self.long_description := long_description;
self.mdse_hierarchy_id := mdse_hierarchy_id;
self.department_id := department_id;
self.deparment_name := deparment_name;
self.class_id := class_id;
self.class_name := class_name;
self.sub_class_id := sub_class_id;
self.sub_class_name := sub_class_name;
self.status := status;
self.ranged := ranged;
self.sellable := sellable;
self.store_order_repl_typ := store_order_repl_typ;
self.type := type;
RETURN;
end;
END;
/
