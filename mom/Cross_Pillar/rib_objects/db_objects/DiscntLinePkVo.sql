DROP TYPE "RIB_DiscntLinePkVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DiscntLinePkVo_REC";
/
DROP TYPE "RIB_DiscntLinePkVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DiscntLinePkVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DiscntLinePkVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_no number(4),
  currency_code varchar2(3),
  completed_discount_amount number(20,4),
  cancelled_discount_amount number(20,4),
  repriced_discount_amount number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DiscntLinePkVo_REC"
(
  rib_oid number
, line_no number
, currency_code varchar2
, completed_discount_amount number
, cancelled_discount_amount number
) return self as result
,constructor function "RIB_DiscntLinePkVo_REC"
(
  rib_oid number
, line_no number
, currency_code varchar2
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DiscntLinePkVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DiscntLinePkVo') := "ns_name_DiscntLinePkVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_no') := line_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_discount_amount') := completed_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_discount_amount') := cancelled_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_discount_amount') := repriced_discount_amount;
END appendNodeValues;
constructor function "RIB_DiscntLinePkVo_REC"
(
  rib_oid number
, line_no number
, currency_code varchar2
, completed_discount_amount number
, cancelled_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.currency_code := currency_code;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
RETURN;
end;
constructor function "RIB_DiscntLinePkVo_REC"
(
  rib_oid number
, line_no number
, currency_code varchar2
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.currency_code := currency_code;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
RETURN;
end;
END;
/
