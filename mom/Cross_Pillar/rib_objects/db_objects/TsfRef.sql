DROP TYPE "RIB_TsfDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDtlRef_REC";
/
DROP TYPE "RIB_TsfRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfRef_REC";
/
DROP TYPE "RIB_TsfDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDtlRef_REC"
(
  rib_oid number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfRef') := "ns_name_TsfRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_TsfDtlRef_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDtlRef_TBL" AS TABLE OF "RIB_TsfDtlRef_REC";
/
DROP TYPE "RIB_TsfRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_no number(12),
  doc_type varchar2(1),
  physical_from_loc number(10),
  from_loc varchar2(10),
  from_loc_type varchar2(1),
  from_store_type varchar2(1),
  from_stockholding_ind varchar2(1),
  physical_to_loc number(10),
  to_loc varchar2(10),
  to_loc_type varchar2(1),
  to_store_type varchar2(1),
  to_stockholding_ind varchar2(1),
  TsfDtlRef_TBL "RIB_TsfDtlRef_TBL",   -- Size of "RIB_TsfDtlRef_TBL" is unbounded
  tsf_parent_no number(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
) return self as result
,constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
) return self as result
,constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
) return self as result
,constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
) return self as result
,constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, TsfDtlRef_TBL "RIB_TsfDtlRef_TBL"  -- Size of "RIB_TsfDtlRef_TBL" is unbounded
) return self as result
,constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, TsfDtlRef_TBL "RIB_TsfDtlRef_TBL"  -- Size of "RIB_TsfDtlRef_TBL" is unbounded
, tsf_parent_no number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfRef') := "ns_name_TsfRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_no') := tsf_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_from_loc') := physical_from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc') := from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc_type') := from_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_store_type') := from_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_stockholding_ind') := from_stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_to_loc') := physical_to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_store_type') := to_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_stockholding_ind') := to_stockholding_ind;
  IF TsfDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDtlRef_TBL.';
    FOR INDX IN TsfDtlRef_TBL.FIRST()..TsfDtlRef_TBL.LAST() LOOP
      TsfDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_parent_no') := tsf_parent_no;
END appendNodeValues;
constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
RETURN;
end;
constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
RETURN;
end;
constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
RETURN;
end;
constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
RETURN;
end;
constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, TsfDtlRef_TBL "RIB_TsfDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.TsfDtlRef_TBL := TsfDtlRef_TBL;
RETURN;
end;
constructor function "RIB_TsfRef_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, physical_to_loc number
, to_loc varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, TsfDtlRef_TBL "RIB_TsfDtlRef_TBL"
, tsf_parent_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.TsfDtlRef_TBL := TsfDtlRef_TBL;
self.tsf_parent_no := tsf_parent_no;
RETURN;
end;
END;
/
