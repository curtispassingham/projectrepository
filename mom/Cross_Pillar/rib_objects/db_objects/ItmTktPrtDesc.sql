DROP TYPE "RIB_ItmTktPrtDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmTktPrtDesc_REC";
/
DROP TYPE "RIB_ItemTicket_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTicket_REC";
/
DROP TYPE "RIB_ItemTicketUda_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTicketUda_REC";
/
DROP TYPE "RIB_ItemTicketQrc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTicketQrc_REC";
/
DROP TYPE "RIB_ItemTicket_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTicket_TBL" AS TABLE OF "RIB_ItemTicket_REC";
/
DROP TYPE "RIB_ItmTktPrtDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmTktPrtDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktPrtDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  printer varchar2(128),
  printer_source varchar2(20), -- printer_source is enumeration field, valid values are [INTERNAL, EXTERNAL] (all lower-case)
  printer_uri varchar2(256),
  printer_route varchar2(40),
  format_name varchar2(128),
  format_type varchar2(20), -- format_type is enumeration field, valid values are [PDF, XML] (all lower-case)
  locale varchar2(128),
  copies number(2),
  ItemTicket_TBL "RIB_ItemTicket_TBL",   -- Size of "RIB_ItemTicket_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmTktPrtDesc_REC"
(
  rib_oid number
, store_id number
, printer varchar2
, printer_source varchar2
, printer_uri varchar2
, printer_route varchar2
, format_name varchar2
, format_type varchar2
, locale varchar2
, copies number
, ItemTicket_TBL "RIB_ItemTicket_TBL"  -- Size of "RIB_ItemTicket_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmTktPrtDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktPrtDesc') := "ns_name_ItmTktPrtDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer') := printer;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer_source') := printer_source;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer_uri') := printer_uri;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer_route') := printer_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_name') := format_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_type') := format_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'locale') := locale;
  rib_obj_util.g_RIB_element_values(i_prefix||'copies') := copies;
  l_new_pre :=i_prefix||'ItemTicket_TBL.';
  FOR INDX IN ItemTicket_TBL.FIRST()..ItemTicket_TBL.LAST() LOOP
    ItemTicket_TBL(indx).appendNodeValues( i_prefix||indx||'ItemTicket_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItmTktPrtDesc_REC"
(
  rib_oid number
, store_id number
, printer varchar2
, printer_source varchar2
, printer_uri varchar2
, printer_route varchar2
, format_name varchar2
, format_type varchar2
, locale varchar2
, copies number
, ItemTicket_TBL "RIB_ItemTicket_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.printer := printer;
self.printer_source := printer_source;
self.printer_uri := printer_uri;
self.printer_route := printer_route;
self.format_name := format_name;
self.format_type := format_type;
self.locale := locale;
self.copies := copies;
self.ItemTicket_TBL := ItemTicket_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemTicketUda_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTicketUda_TBL" AS TABLE OF "RIB_ItemTicketUda_REC";
/
DROP TYPE "RIB_ItemTicketQrc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTicketQrc_TBL" AS TABLE OF "RIB_ItemTicketQrc_REC";
/
DROP TYPE "RIB_ItemTicket_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemTicket_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktPrtDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ticket_id number(12),
  item_id varchar2(25),
  item_short_desc varchar2(255),
  item_long_desc varchar2(400),
  department_id number(12),
  department_name varchar2(360),
  class_id number(12),
  class_name varchar2(360),
  subclass_id number(12),
  subclass_name varchar2(360),
  diff_type_one varchar2(255),
  diff_desc_one varchar2(255),
  diff_type_two varchar2(255),
  diff_desc_two varchar2(255),
  diff_type_three varchar2(255),
  diff_desc_three varchar2(255),
  diff_type_four varchar2(255),
  diff_desc_four varchar2(255),
  template_url varchar2(128),
  quantity number(10),
  label_price number(12,4),
  label_price_type varchar2(20), -- label_price_type is enumeration field, valid values are [REGULAR, PROMOTIONAL, CLEARANCE] (all lower-case)
  label_price_uom varchar2(4),
  label_currency varchar2(3),
  override_price number(12,4),
  override_currency varchar2(3),
  multi_units number(12,4),
  multi_label_price number(12,4),
  multi_label_currency varchar2(3),
  multi_unit_change varchar2(5), --multi_unit_change is boolean field, valid values are true,false (all lower-case) 
  effective_date date,
  external_po_id varchar2(128),
  promotion_id number(10),
  country_manufacture varchar2(3),
  msr_price number(12,4),
  msr_currency varchar2(3),
  print_order number(10),
  uin varchar2(128),
  ItemTicketUda_TBL "RIB_ItemTicketUda_TBL",   -- Size of "RIB_ItemTicketUda_TBL" is 999
  ItemTicketQrc_TBL "RIB_ItemTicketQrc_TBL",   -- Size of "RIB_ItemTicketQrc_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
) return self as result
,constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
, uin varchar2
) return self as result
,constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
, uin varchar2
, ItemTicketUda_TBL "RIB_ItemTicketUda_TBL"  -- Size of "RIB_ItemTicketUda_TBL" is 999
) return self as result
,constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
, uin varchar2
, ItemTicketUda_TBL "RIB_ItemTicketUda_TBL"  -- Size of "RIB_ItemTicketUda_TBL" is 999
, ItemTicketQrc_TBL "RIB_ItemTicketQrc_TBL"  -- Size of "RIB_ItemTicketQrc_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemTicket_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktPrtDesc') := "ns_name_ItmTktPrtDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'ticket_id') := ticket_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_short_desc') := item_short_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_long_desc') := item_long_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_name') := department_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_name') := class_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_name') := subclass_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type_one') := diff_type_one;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_desc_one') := diff_desc_one;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type_two') := diff_type_two;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_desc_two') := diff_desc_two;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type_three') := diff_type_three;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_desc_three') := diff_desc_three;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type_four') := diff_type_four;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_desc_four') := diff_desc_four;
  rib_obj_util.g_RIB_element_values(i_prefix||'template_url') := template_url;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_price') := label_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_price_type') := label_price_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_price_uom') := label_price_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_currency') := label_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'override_price') := override_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'override_currency') := override_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_units') := multi_units;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_label_price') := multi_label_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_label_currency') := multi_label_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_change') := multi_unit_change;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_po_id') := external_po_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_manufacture') := country_manufacture;
  rib_obj_util.g_RIB_element_values(i_prefix||'msr_price') := msr_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'msr_currency') := msr_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'print_order') := print_order;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  IF ItemTicketUda_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemTicketUda_TBL.';
    FOR INDX IN ItemTicketUda_TBL.FIRST()..ItemTicketUda_TBL.LAST() LOOP
      ItemTicketUda_TBL(indx).appendNodeValues( i_prefix||indx||'ItemTicketUda_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemTicketQrc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemTicketQrc_TBL.';
    FOR INDX IN ItemTicketQrc_TBL.FIRST()..ItemTicketQrc_TBL.LAST() LOOP
      ItemTicketQrc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemTicketQrc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ticket_id := ticket_id;
self.item_id := item_id;
self.item_short_desc := item_short_desc;
self.item_long_desc := item_long_desc;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.diff_type_one := diff_type_one;
self.diff_desc_one := diff_desc_one;
self.diff_type_two := diff_type_two;
self.diff_desc_two := diff_desc_two;
self.diff_type_three := diff_type_three;
self.diff_desc_three := diff_desc_three;
self.diff_type_four := diff_type_four;
self.diff_desc_four := diff_desc_four;
self.template_url := template_url;
self.quantity := quantity;
self.label_price := label_price;
self.label_price_type := label_price_type;
self.label_price_uom := label_price_uom;
self.label_currency := label_currency;
self.override_price := override_price;
self.override_currency := override_currency;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.effective_date := effective_date;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.country_manufacture := country_manufacture;
self.msr_price := msr_price;
self.msr_currency := msr_currency;
self.print_order := print_order;
RETURN;
end;
constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
, uin varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ticket_id := ticket_id;
self.item_id := item_id;
self.item_short_desc := item_short_desc;
self.item_long_desc := item_long_desc;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.diff_type_one := diff_type_one;
self.diff_desc_one := diff_desc_one;
self.diff_type_two := diff_type_two;
self.diff_desc_two := diff_desc_two;
self.diff_type_three := diff_type_three;
self.diff_desc_three := diff_desc_three;
self.diff_type_four := diff_type_four;
self.diff_desc_four := diff_desc_four;
self.template_url := template_url;
self.quantity := quantity;
self.label_price := label_price;
self.label_price_type := label_price_type;
self.label_price_uom := label_price_uom;
self.label_currency := label_currency;
self.override_price := override_price;
self.override_currency := override_currency;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.effective_date := effective_date;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.country_manufacture := country_manufacture;
self.msr_price := msr_price;
self.msr_currency := msr_currency;
self.print_order := print_order;
self.uin := uin;
RETURN;
end;
constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
, uin varchar2
, ItemTicketUda_TBL "RIB_ItemTicketUda_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ticket_id := ticket_id;
self.item_id := item_id;
self.item_short_desc := item_short_desc;
self.item_long_desc := item_long_desc;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.diff_type_one := diff_type_one;
self.diff_desc_one := diff_desc_one;
self.diff_type_two := diff_type_two;
self.diff_desc_two := diff_desc_two;
self.diff_type_three := diff_type_three;
self.diff_desc_three := diff_desc_three;
self.diff_type_four := diff_type_four;
self.diff_desc_four := diff_desc_four;
self.template_url := template_url;
self.quantity := quantity;
self.label_price := label_price;
self.label_price_type := label_price_type;
self.label_price_uom := label_price_uom;
self.label_currency := label_currency;
self.override_price := override_price;
self.override_currency := override_currency;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.effective_date := effective_date;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.country_manufacture := country_manufacture;
self.msr_price := msr_price;
self.msr_currency := msr_currency;
self.print_order := print_order;
self.uin := uin;
self.ItemTicketUda_TBL := ItemTicketUda_TBL;
RETURN;
end;
constructor function "RIB_ItemTicket_REC"
(
  rib_oid number
, ticket_id number
, item_id varchar2
, item_short_desc varchar2
, item_long_desc varchar2
, department_id number
, department_name varchar2
, class_id number
, class_name varchar2
, subclass_id number
, subclass_name varchar2
, diff_type_one varchar2
, diff_desc_one varchar2
, diff_type_two varchar2
, diff_desc_two varchar2
, diff_type_three varchar2
, diff_desc_three varchar2
, diff_type_four varchar2
, diff_desc_four varchar2
, template_url varchar2
, quantity number
, label_price number
, label_price_type varchar2
, label_price_uom varchar2
, label_currency varchar2
, override_price number
, override_currency varchar2
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, effective_date date
, external_po_id varchar2
, promotion_id number
, country_manufacture varchar2
, msr_price number
, msr_currency varchar2
, print_order number
, uin varchar2
, ItemTicketUda_TBL "RIB_ItemTicketUda_TBL"
, ItemTicketQrc_TBL "RIB_ItemTicketQrc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ticket_id := ticket_id;
self.item_id := item_id;
self.item_short_desc := item_short_desc;
self.item_long_desc := item_long_desc;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.diff_type_one := diff_type_one;
self.diff_desc_one := diff_desc_one;
self.diff_type_two := diff_type_two;
self.diff_desc_two := diff_desc_two;
self.diff_type_three := diff_type_three;
self.diff_desc_three := diff_desc_three;
self.diff_type_four := diff_type_four;
self.diff_desc_four := diff_desc_four;
self.template_url := template_url;
self.quantity := quantity;
self.label_price := label_price;
self.label_price_type := label_price_type;
self.label_price_uom := label_price_uom;
self.label_currency := label_currency;
self.override_price := override_price;
self.override_currency := override_currency;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.effective_date := effective_date;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.country_manufacture := country_manufacture;
self.msr_price := msr_price;
self.msr_currency := msr_currency;
self.print_order := print_order;
self.uin := uin;
self.ItemTicketUda_TBL := ItemTicketUda_TBL;
self.ItemTicketQrc_TBL := ItemTicketQrc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemTicketUda_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemTicketUda_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktPrtDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  label varchar2(128),
  value varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemTicketUda_REC"
(
  rib_oid number
, label varchar2
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemTicketUda_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktPrtDesc') := "ns_name_ItmTktPrtDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'label') := label;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_ItemTicketUda_REC"
(
  rib_oid number
, label varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.label := label;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemTicketQrc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemTicketQrc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktPrtDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  name varchar2(120),
  url varchar2(1000),
  start_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemTicketQrc_REC"
(
  rib_oid number
, name varchar2
, url varchar2
) return self as result
,constructor function "RIB_ItemTicketQrc_REC"
(
  rib_oid number
, name varchar2
, url varchar2
, start_date date
) return self as result
,constructor function "RIB_ItemTicketQrc_REC"
(
  rib_oid number
, name varchar2
, url varchar2
, start_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemTicketQrc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktPrtDesc') := "ns_name_ItmTktPrtDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'url') := url;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_ItemTicketQrc_REC"
(
  rib_oid number
, name varchar2
, url varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
self.url := url;
RETURN;
end;
constructor function "RIB_ItemTicketQrc_REC"
(
  rib_oid number
, name varchar2
, url varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
self.url := url;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_ItemTicketQrc_REC"
(
  rib_oid number
, name varchar2
, url varchar2
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
self.url := url;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
END;
/
