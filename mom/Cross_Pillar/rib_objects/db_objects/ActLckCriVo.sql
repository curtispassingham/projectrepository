DROP TYPE "RIB_ActLckCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ActLckCriVo_REC";
/
DROP TYPE "RIB_ActLckCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ActLckCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ActLckCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  user_name varchar2(128),
  device_type varchar2(15), -- device_type is enumeration field, valid values are [PC, HH, SERVER, INT_SERVICE] (all lower-case)
  activity_id varchar2(128),
  activity_type varchar2(30), -- activity_type is enumeration field, valid values are [DIRECT_DELIVERY, DIRECT_DELIVERY_INVOICE, FULFILLMENT_ORDER, FULFILLMENT_ORDER_DELIVERY, FULFILLMENT_ORDER_PICK, FULFILLMENT_ORDER_REVERSE_PICK, INVENTORY_ADJUSTMENT, INVENTORY_ADJUSTMENT_REASON, INVENTORY_ADJUSTMENT_TEMPLATE, ITEM_BASKET, ITEM_REQUEST, ITEM_TICKET, SHELF_REPLENISHMENT, PRICE_CHANGE, PRODUCT_GROUP, PRODUCT_GROUP_SCHEDULE, RETURN, RETURN_REASON, STOCK_COUNT_CHILD, STORE_SEQUENCE, STORE_SEQUENCE_ALL, TRANSFER, WAREHOUSE_DELIVERY, UNKNOWN] (all lower-case)
  session_id varchar2(128),
  from_date date,
  to_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
) return self as result
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
) return self as result
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
) return self as result
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
) return self as result
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
, session_id varchar2
) return self as result
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
, session_id varchar2
, from_date date
) return self as result
,constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
, session_id varchar2
, from_date date
, to_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ActLckCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ActLckCriVo') := "ns_name_ActLckCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'device_type') := device_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'activity_id') := activity_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'activity_type') := activity_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'session_id') := session_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_date') := from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_date') := to_date;
END appendNodeValues;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
RETURN;
end;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
self.device_type := device_type;
RETURN;
end;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
self.device_type := device_type;
self.activity_id := activity_id;
RETURN;
end;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
self.device_type := device_type;
self.activity_id := activity_id;
self.activity_type := activity_type;
RETURN;
end;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
, session_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
self.device_type := device_type;
self.activity_id := activity_id;
self.activity_type := activity_type;
self.session_id := session_id;
RETURN;
end;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
, session_id varchar2
, from_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
self.device_type := device_type;
self.activity_id := activity_id;
self.activity_type := activity_type;
self.session_id := session_id;
self.from_date := from_date;
RETURN;
end;
constructor function "RIB_ActLckCriVo_REC"
(
  rib_oid number
, user_name varchar2
, device_type varchar2
, activity_id varchar2
, activity_type varchar2
, session_id varchar2
, from_date date
, to_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.user_name := user_name;
self.device_type := device_type;
self.activity_id := activity_id;
self.activity_type := activity_type;
self.session_id := session_id;
self.from_date := from_date;
self.to_date := to_date;
RETURN;
end;
END;
/
