DROP TYPE "RIB_StrItmByUdaCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmByUdaCriVo_REC";
/
DROP TYPE "RIB_StrItmByUdaCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmByUdaCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmByUdaCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  department_id number(12),
  class_id number(12),
  subclass_id number(12),
  include_non_ranged varchar2(5), --include_non_ranged is boolean field, valid values are true,false (all lower-case) 
  item_description varchar2(250),
  search_limit number(3),
  text_uda_id number(5),
  uda_text varchar2(120),
  value_uda_id number(5),
  uda_value varchar2(25),
  date_uda_id number(5),
  uda_date_from date,
  uda_date_to date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
, date_uda_id number
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
, date_uda_id number
, uda_date_from date
) return self as result
,constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
, date_uda_id number
, uda_date_from date
, uda_date_to date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmByUdaCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmByUdaCriVo') := "ns_name_StrItmByUdaCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_non_ranged') := include_non_ranged;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'search_limit') := search_limit;
  rib_obj_util.g_RIB_element_values(i_prefix||'text_uda_id') := text_uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_text') := uda_text;
  rib_obj_util.g_RIB_element_values(i_prefix||'value_uda_id') := value_uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_value') := uda_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'date_uda_id') := date_uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_date_from') := uda_date_from;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_date_to') := uda_date_to;
END appendNodeValues;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
self.uda_text := uda_text;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
self.uda_text := uda_text;
self.value_uda_id := value_uda_id;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
self.uda_text := uda_text;
self.value_uda_id := value_uda_id;
self.uda_value := uda_value;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
, date_uda_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
self.uda_text := uda_text;
self.value_uda_id := value_uda_id;
self.uda_value := uda_value;
self.date_uda_id := date_uda_id;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
, date_uda_id number
, uda_date_from date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
self.uda_text := uda_text;
self.value_uda_id := value_uda_id;
self.uda_value := uda_value;
self.date_uda_id := date_uda_id;
self.uda_date_from := uda_date_from;
RETURN;
end;
constructor function "RIB_StrItmByUdaCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, text_uda_id number
, uda_text varchar2
, value_uda_id number
, uda_value varchar2
, date_uda_id number
, uda_date_from date
, uda_date_to date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.text_uda_id := text_uda_id;
self.uda_text := uda_text;
self.value_uda_id := value_uda_id;
self.uda_value := uda_value;
self.date_uda_id := date_uda_id;
self.uda_date_from := uda_date_from;
self.uda_date_to := uda_date_to;
RETURN;
end;
END;
/
