DROP TYPE "RIB_StsTsfHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfHdrDesc_REC";
/
DROP TYPE "RIB_StsTsfHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_id number(15),
  ext_tsf_id varchar2(128),
  src_loc_type varchar2(20), -- src_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, UNKNOWN] (all lower-case)
  src_loc_id number(10),
  src_loc_name varchar2(150),
  dest_loc_type varchar2(20), -- dest_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, UNKNOWN] (all lower-case)
  dest_loc_id number(10),
  dest_loc_name varchar2(150),
  status varchar2(20), -- status is enumeration field, valid values are [NEW_REQUEST, REQUESTED, REQUEST_IN_PROGRESS, REJECTED, CANCELED_REQUEST, TRANSFER_IN_PROGRESS, APPROVED, IN_SHIPPING, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_date date,
  not_after_date date,
  number_of_line_items number(10),
  fulfillment_order_related varchar2(5), --fulfillment_order_related is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfHdrDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, src_loc_type varchar2
, src_loc_id number
, src_loc_name varchar2
, dest_loc_type varchar2
, dest_loc_id number
, dest_loc_name varchar2
, status varchar2
, create_date date
, not_after_date date
, number_of_line_items number
, fulfillment_order_related varchar2  --fulfillment_order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfHdrDesc') := "ns_name_StsTsfHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_id') := tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_tsf_id') := ext_tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_type') := src_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_id') := src_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_name') := src_loc_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_type') := dest_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_id') := dest_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_name') := dest_loc_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_line_items') := number_of_line_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_related') := fulfillment_order_related;
END appendNodeValues;
constructor function "RIB_StsTsfHdrDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, src_loc_type varchar2
, src_loc_id number
, src_loc_name varchar2
, dest_loc_type varchar2
, dest_loc_id number
, dest_loc_name varchar2
, status varchar2
, create_date date
, not_after_date date
, number_of_line_items number
, fulfillment_order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.ext_tsf_id := ext_tsf_id;
self.src_loc_type := src_loc_type;
self.src_loc_id := src_loc_id;
self.src_loc_name := src_loc_name;
self.dest_loc_type := dest_loc_type;
self.dest_loc_id := dest_loc_id;
self.dest_loc_name := dest_loc_name;
self.status := status;
self.create_date := create_date;
self.not_after_date := not_after_date;
self.number_of_line_items := number_of_line_items;
self.fulfillment_order_related := fulfillment_order_related;
RETURN;
end;
END;
/
