DROP TYPE "RIB_NotificationDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_NotificationDesc_REC";
/
DROP TYPE "RIB_NotificationDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_NotificationDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_NotificationDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location_id varchar2(10),
  notification_reference_id varchar2(50),
  notification varchar2(1000),
  notification_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_NotificationDesc_REC"
(
  rib_oid number
, location_id varchar2
, notification_reference_id varchar2
, notification varchar2
, notification_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_NotificationDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_NotificationDesc') := "ns_name_NotificationDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'location_id') := location_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'notification_reference_id') := notification_reference_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'notification') := notification;
  rib_obj_util.g_RIB_element_values(i_prefix||'notification_date') := notification_date;
END appendNodeValues;
constructor function "RIB_NotificationDesc_REC"
(
  rib_oid number
, location_id varchar2
, notification_reference_id varchar2
, notification varchar2
, notification_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.location_id := location_id;
self.notification_reference_id := notification_reference_id;
self.notification := notification;
self.notification_date := notification_date;
RETURN;
end;
END;
/
