@@RetItemIdentDesc.sql;
/
@@RetTendTypeDesc.sql;
/
@@RetStoreLangDesc.sql;
/
DROP TYPE "RIB_RetAuthResDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RetAuthResDesc_REC";
/
DROP TYPE "RIB_ReturnResponse_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReturnResponse_REC";
/
DROP TYPE "RIB_ReceiptMessageDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptMessageDesc_REC";
/
DROP TYPE "RIB_ItemReturnResponse_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemReturnResponse_REC";
/
DROP TYPE "RIB_RefundTenders_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RefundTenders_REC";
/
DROP TYPE "RIB_ResponseDescription_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ResponseDescription_REC";
/
DROP TYPE "RIB_ShortResponseDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShortResponseDesc_REC";
/
DROP TYPE "RIB_LongResponseDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LongResponseDesc_REC";
/
DROP TYPE "RIB_RetAuthResDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RetAuthResDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ReturnResponse "RIB_ReturnResponse_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RetAuthResDesc_REC"
(
  rib_oid number
, ReturnResponse "RIB_ReturnResponse_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RetAuthResDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'ReturnResponse.';
  ReturnResponse.appendNodeValues( i_prefix||'ReturnResponse');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_RetAuthResDesc_REC"
(
  rib_oid number
, ReturnResponse "RIB_ReturnResponse_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ReturnResponse := ReturnResponse;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemReturnResponse_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemReturnResponse_TBL" AS TABLE OF "RIB_ItemReturnResponse_REC";
/
DROP TYPE "RIB_ReturnResponse_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReturnResponse_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  return_ticket_id varchar2(30),
  response_approve_deny_code varchar2(25), -- response_approve_deny_code is enumeration field, valid values are [Authorization, Contingent Authorization, Denial, Mgr Overridable Denial] (all lower-case)
  avail_cust_service_override varchar2(5), --avail_cust_service_override is boolean field, valid values are true,false (all lower-case) 
  receipt_message_number number(8),
  ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC",
  ItemReturnResponse_TBL "RIB_ItemReturnResponse_TBL",   -- Size of "RIB_ItemReturnResponse_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReturnResponse_REC"
(
  rib_oid number
, return_ticket_id varchar2
, response_approve_deny_code varchar2
, avail_cust_service_override varchar2  --avail_cust_service_override is boolean field, valid values are true,false (all lower-case)
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, ItemReturnResponse_TBL "RIB_ItemReturnResponse_TBL"  -- Size of "RIB_ItemReturnResponse_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReturnResponse_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'return_ticket_id') := return_ticket_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'response_approve_deny_code') := response_approve_deny_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'avail_cust_service_override') := avail_cust_service_override;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_message_number') := receipt_message_number;
  l_new_pre :=i_prefix||'ReceiptMessageDesc.';
  ReceiptMessageDesc.appendNodeValues( i_prefix||'ReceiptMessageDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ItemReturnResponse_TBL.';
  FOR INDX IN ItemReturnResponse_TBL.FIRST()..ItemReturnResponse_TBL.LAST() LOOP
    ItemReturnResponse_TBL(indx).appendNodeValues( i_prefix||indx||'ItemReturnResponse_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ReturnResponse_REC"
(
  rib_oid number
, return_ticket_id varchar2
, response_approve_deny_code varchar2
, avail_cust_service_override varchar2
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, ItemReturnResponse_TBL "RIB_ItemReturnResponse_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_ticket_id := return_ticket_id;
self.response_approve_deny_code := response_approve_deny_code;
self.avail_cust_service_override := avail_cust_service_override;
self.receipt_message_number := receipt_message_number;
self.ReceiptMessageDesc := ReceiptMessageDesc;
self.ItemReturnResponse_TBL := ItemReturnResponse_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptMessageDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptMessageDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetStoreLangDesc "RIB_RetStoreLangDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptMessageDesc_REC"
(
  rib_oid number
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptMessageDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetStoreLangDesc.';
  RetStoreLangDesc.appendNodeValues( i_prefix||'RetStoreLangDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ReceiptMessageDesc_REC"
(
  rib_oid number
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetStoreLangDesc := RetStoreLangDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemReturnResponse_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemReturnResponse_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetItemIdentDesc "RIB_RetItemIdentDesc_REC",
  approved_quantity number(11),
  response_code number(8),
  approve_deny_code varchar2(25), -- approve_deny_code is enumeration field, valid values are [Authorization, Contingent Authorization, Denial, Mgr Overridable Denial] (all lower-case)
  ResponseDescription "RIB_ResponseDescription_REC",
  receipt_message_number number(8),
  ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC",
  RefundTenders "RIB_RefundTenders_REC",
  item_disposition_code varchar2(10),
  restocking_fee number(15),
  customer_info_required varchar2(5), --customer_info_required is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
) return self as result
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
) return self as result
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
) return self as result
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
) return self as result
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
, item_disposition_code varchar2
) return self as result
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
, item_disposition_code varchar2
, restocking_fee number
) return self as result
,constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
, item_disposition_code varchar2
, restocking_fee number
, customer_info_required varchar2  --customer_info_required is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemReturnResponse_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetItemIdentDesc.';
  RetItemIdentDesc.appendNodeValues( i_prefix||'RetItemIdentDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_quantity') := approved_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'response_code') := response_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'approve_deny_code') := approve_deny_code;
  l_new_pre :=i_prefix||'ResponseDescription.';
  ResponseDescription.appendNodeValues( i_prefix||'ResponseDescription');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_message_number') := receipt_message_number;
  l_new_pre :=i_prefix||'ReceiptMessageDesc.';
  ReceiptMessageDesc.appendNodeValues( i_prefix||'ReceiptMessageDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'RefundTenders.';
  RefundTenders.appendNodeValues( i_prefix||'RefundTenders');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_disposition_code') := item_disposition_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'restocking_fee') := restocking_fee;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_info_required') := customer_info_required;
END appendNodeValues;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
RETURN;
end;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
self.receipt_message_number := receipt_message_number;
RETURN;
end;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
self.receipt_message_number := receipt_message_number;
self.ReceiptMessageDesc := ReceiptMessageDesc;
RETURN;
end;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
self.receipt_message_number := receipt_message_number;
self.ReceiptMessageDesc := ReceiptMessageDesc;
self.RefundTenders := RefundTenders;
RETURN;
end;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
, item_disposition_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
self.receipt_message_number := receipt_message_number;
self.ReceiptMessageDesc := ReceiptMessageDesc;
self.RefundTenders := RefundTenders;
self.item_disposition_code := item_disposition_code;
RETURN;
end;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
, item_disposition_code varchar2
, restocking_fee number
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
self.receipt_message_number := receipt_message_number;
self.ReceiptMessageDesc := ReceiptMessageDesc;
self.RefundTenders := RefundTenders;
self.item_disposition_code := item_disposition_code;
self.restocking_fee := restocking_fee;
RETURN;
end;
constructor function "RIB_ItemReturnResponse_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, approved_quantity number
, response_code number
, approve_deny_code varchar2
, ResponseDescription "RIB_ResponseDescription_REC"
, receipt_message_number number
, ReceiptMessageDesc "RIB_ReceiptMessageDesc_REC"
, RefundTenders "RIB_RefundTenders_REC"
, item_disposition_code varchar2
, restocking_fee number
, customer_info_required varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.approved_quantity := approved_quantity;
self.response_code := response_code;
self.approve_deny_code := approve_deny_code;
self.ResponseDescription := ResponseDescription;
self.receipt_message_number := receipt_message_number;
self.ReceiptMessageDesc := ReceiptMessageDesc;
self.RefundTenders := RefundTenders;
self.item_disposition_code := item_disposition_code;
self.restocking_fee := restocking_fee;
self.customer_info_required := customer_info_required;
RETURN;
end;
END;
/
DROP TYPE "RIB_RetTendTypeDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RetTendTypeDesc_TBL" AS TABLE OF "RIB_RetTendTypeDesc_REC";
/
DROP TYPE "RIB_RefundTenders_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RefundTenders_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL",   -- Size of "RIB_RetTendTypeDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RefundTenders_REC"
(
  rib_oid number
, RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL"  -- Size of "RIB_RetTendTypeDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RefundTenders_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF RetTendTypeDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RetTendTypeDesc_TBL.';
    FOR INDX IN RetTendTypeDesc_TBL.FIRST()..RetTendTypeDesc_TBL.LAST() LOOP
      RetTendTypeDesc_TBL(indx).appendNodeValues( i_prefix||indx||'RetTendTypeDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_RefundTenders_REC"
(
  rib_oid number
, RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetTendTypeDesc_TBL := RetTendTypeDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ResponseDescription_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ResponseDescription_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ShortResponseDesc "RIB_ShortResponseDesc_REC",
  LongResponseDesc "RIB_LongResponseDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ResponseDescription_REC"
(
  rib_oid number
, ShortResponseDesc "RIB_ShortResponseDesc_REC"
, LongResponseDesc "RIB_LongResponseDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ResponseDescription_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ShortResponseDesc.';
  ShortResponseDesc.appendNodeValues( i_prefix||'ShortResponseDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LongResponseDesc.';
  LongResponseDesc.appendNodeValues( i_prefix||'LongResponseDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ResponseDescription_REC"
(
  rib_oid number
, ShortResponseDesc "RIB_ShortResponseDesc_REC"
, LongResponseDesc "RIB_LongResponseDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ShortResponseDesc := ShortResponseDesc;
self.LongResponseDesc := LongResponseDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShortResponseDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShortResponseDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetStoreLangDesc "RIB_RetStoreLangDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShortResponseDesc_REC"
(
  rib_oid number
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShortResponseDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetStoreLangDesc.';
  RetStoreLangDesc.appendNodeValues( i_prefix||'RetStoreLangDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ShortResponseDesc_REC"
(
  rib_oid number
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetStoreLangDesc := RetStoreLangDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_LongResponseDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LongResponseDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthResDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetStoreLangDesc "RIB_RetStoreLangDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LongResponseDesc_REC"
(
  rib_oid number
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LongResponseDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthResDesc') := "ns_name_RetAuthResDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetStoreLangDesc.';
  RetStoreLangDesc.appendNodeValues( i_prefix||'RetStoreLangDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_LongResponseDesc_REC"
(
  rib_oid number
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetStoreLangDesc := RetStoreLangDesc;
RETURN;
end;
END;
/
