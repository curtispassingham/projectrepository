DROP TYPE "RIB_PendRtrnRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnRef_REC";
/
DROP TYPE "RIB_PendRtrnRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PendRtrnRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PendRtrnRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  physical_wh number(10),
  rma_nbr varchar2(20),
  line_item_nbr number(3),
  reason_code varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PendRtrnRef_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, line_item_nbr number
) return self as result
,constructor function "RIB_PendRtrnRef_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, line_item_nbr number
, reason_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PendRtrnRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PendRtrnRef') := "ns_name_PendRtrnRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'rma_nbr') := rma_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_nbr') := line_item_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_code') := reason_code;
END appendNodeValues;
constructor function "RIB_PendRtrnRef_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, line_item_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_wh := physical_wh;
self.rma_nbr := rma_nbr;
self.line_item_nbr := line_item_nbr;
RETURN;
end;
constructor function "RIB_PendRtrnRef_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, line_item_nbr number
, reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_wh := physical_wh;
self.rma_nbr := rma_nbr;
self.line_item_nbr := line_item_nbr;
self.reason_code := reason_code;
RETURN;
end;
END;
/
