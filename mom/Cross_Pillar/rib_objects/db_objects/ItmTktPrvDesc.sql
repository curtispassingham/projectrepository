DROP TYPE "RIB_ItmTktPrvDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmTktPrvDesc_REC";
/
DROP TYPE "RIB_ItmTktPrvDoc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmTktPrvDoc_REC";
/
DROP TYPE "RIB_ItmTktPrvDoc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmTktPrvDoc_TBL" AS TABLE OF "RIB_ItmTktPrvDoc_REC";
/
DROP TYPE "RIB_ItmTktPrvDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmTktPrvDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktPrvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  content_type varchar2(20), -- content_type is enumeration field, valid values are [PDF, XML] (all lower-case)
  file_id varchar2(128),
  locale varchar2(12),
  ItmTktPrvDoc_TBL "RIB_ItmTktPrvDoc_TBL",   -- Size of "RIB_ItmTktPrvDoc_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmTktPrvDesc_REC"
(
  rib_oid number
, content_type varchar2
, file_id varchar2
, locale varchar2
, ItmTktPrvDoc_TBL "RIB_ItmTktPrvDoc_TBL"  -- Size of "RIB_ItmTktPrvDoc_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmTktPrvDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktPrvDesc') := "ns_name_ItmTktPrvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'content_type') := content_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'file_id') := file_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'locale') := locale;
  l_new_pre :=i_prefix||'ItmTktPrvDoc_TBL.';
  FOR INDX IN ItmTktPrvDoc_TBL.FIRST()..ItmTktPrvDoc_TBL.LAST() LOOP
    ItmTktPrvDoc_TBL(indx).appendNodeValues( i_prefix||indx||'ItmTktPrvDoc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItmTktPrvDesc_REC"
(
  rib_oid number
, content_type varchar2
, file_id varchar2
, locale varchar2
, ItmTktPrvDoc_TBL "RIB_ItmTktPrvDoc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.content_type := content_type;
self.file_id := file_id;
self.locale := locale;
self.ItmTktPrvDoc_TBL := ItmTktPrvDoc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItmTktPrvDoc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmTktPrvDoc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktPrvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  doc_order number(4),
  doc_content varchar2(3500),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmTktPrvDoc_REC"
(
  rib_oid number
, doc_order number
, doc_content varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmTktPrvDoc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktPrvDesc') := "ns_name_ItmTktPrvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_order') := doc_order;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_content') := doc_content;
END appendNodeValues;
constructor function "RIB_ItmTktPrvDoc_REC"
(
  rib_oid number
, doc_order number
, doc_content varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_order := doc_order;
self.doc_content := doc_content;
RETURN;
end;
END;
/
