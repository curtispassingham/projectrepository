DROP TYPE "RIB_XItmSupCtyMfrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItmSupCtyMfrDesc_REC";
/
DROP TYPE "RIB_XItmSupCtyMfrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItmSupCtyMfrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItmSupCtyMfrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  manufacturer_ctry_id varchar2(23),
  primary_manufacturer_ctry_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItmSupCtyMfrDesc_REC"
(
  rib_oid number
, manufacturer_ctry_id varchar2
) return self as result
,constructor function "RIB_XItmSupCtyMfrDesc_REC"
(
  rib_oid number
, manufacturer_ctry_id varchar2
, primary_manufacturer_ctry_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItmSupCtyMfrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItmSupCtyMfrDesc') := "ns_name_XItmSupCtyMfrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'manufacturer_ctry_id') := manufacturer_ctry_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_manufacturer_ctry_ind') := primary_manufacturer_ctry_ind;
END appendNodeValues;
constructor function "RIB_XItmSupCtyMfrDesc_REC"
(
  rib_oid number
, manufacturer_ctry_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.manufacturer_ctry_id := manufacturer_ctry_id;
RETURN;
end;
constructor function "RIB_XItmSupCtyMfrDesc_REC"
(
  rib_oid number
, manufacturer_ctry_id varchar2
, primary_manufacturer_ctry_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.manufacturer_ctry_id := manufacturer_ctry_id;
self.primary_manufacturer_ctry_ind := primary_manufacturer_ctry_ind;
RETURN;
end;
END;
/
