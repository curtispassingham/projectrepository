DROP TYPE "RIB_PrcZoneGrpStRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpStRef_REC";
/
DROP TYPE "RIB_PriceZoneRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PriceZoneRef_REC";
/
DROP TYPE "RIB_PrcZoneGrpRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpRef_REC";
/
DROP TYPE "RIB_PrcZoneGrpStRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpStRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcZoneGrpRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcZoneGrpStRef_REC"
(
  rib_oid number
, store number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcZoneGrpStRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcZoneGrpRef') := "ns_name_PrcZoneGrpRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
END appendNodeValues;
constructor function "RIB_PrcZoneGrpStRef_REC"
(
  rib_oid number
, store number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcZoneGrpStRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpStRef_TBL" AS TABLE OF "RIB_PrcZoneGrpStRef_REC";
/
DROP TYPE "RIB_PriceZoneRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PriceZoneRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcZoneGrpRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  zone_id number(10),
  PrcZoneGrpStRef_TBL "RIB_PrcZoneGrpStRef_TBL",   -- Size of "RIB_PrcZoneGrpStRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PriceZoneRef_REC"
(
  rib_oid number
, zone_id number
, PrcZoneGrpStRef_TBL "RIB_PrcZoneGrpStRef_TBL"  -- Size of "RIB_PrcZoneGrpStRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PriceZoneRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcZoneGrpRef') := "ns_name_PrcZoneGrpRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'zone_id') := zone_id;
  l_new_pre :=i_prefix||'PrcZoneGrpStRef_TBL.';
  FOR INDX IN PrcZoneGrpStRef_TBL.FIRST()..PrcZoneGrpStRef_TBL.LAST() LOOP
    PrcZoneGrpStRef_TBL(indx).appendNodeValues( i_prefix||indx||'PrcZoneGrpStRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PriceZoneRef_REC"
(
  rib_oid number
, zone_id number
, PrcZoneGrpStRef_TBL "RIB_PrcZoneGrpStRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.zone_id := zone_id;
self.PrcZoneGrpStRef_TBL := PrcZoneGrpStRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PriceZoneRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PriceZoneRef_TBL" AS TABLE OF "RIB_PriceZoneRef_REC";
/
DROP TYPE "RIB_PrcZoneGrpRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcZoneGrpRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  zone_group_id number(4),
  PriceZoneRef_TBL "RIB_PriceZoneRef_TBL",   -- Size of "RIB_PriceZoneRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcZoneGrpRef_REC"
(
  rib_oid number
, zone_group_id number
, PriceZoneRef_TBL "RIB_PriceZoneRef_TBL"  -- Size of "RIB_PriceZoneRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcZoneGrpRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcZoneGrpRef') := "ns_name_PrcZoneGrpRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'zone_group_id') := zone_group_id;
  l_new_pre :=i_prefix||'PriceZoneRef_TBL.';
  FOR INDX IN PriceZoneRef_TBL.FIRST()..PriceZoneRef_TBL.LAST() LOOP
    PriceZoneRef_TBL(indx).appendNodeValues( i_prefix||indx||'PriceZoneRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrcZoneGrpRef_REC"
(
  rib_oid number
, zone_group_id number
, PriceZoneRef_TBL "RIB_PriceZoneRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.zone_group_id := zone_group_id;
self.PriceZoneRef_TBL := PriceZoneRef_TBL;
RETURN;
end;
END;
/
