@@XStoreWT.sql;
/
@@XStoreLocTrt.sql;
/
@@AddrRef.sql;
/
DROP TYPE "RIB_XStoreRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XStoreRef_REC";
/
DROP TYPE "RIB_XStoreLocTrt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XStoreLocTrt_TBL" AS TABLE OF "RIB_XStoreLocTrt_REC";
/
DROP TYPE "RIB_XStoreWT_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XStoreWT_TBL" AS TABLE OF "RIB_XStoreWT_REC";
/
DROP TYPE "RIB_AddrRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrRef_TBL" AS TABLE OF "RIB_AddrRef_REC";
/
DROP TYPE "RIB_XStoreRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XStoreRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XStoreRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store number(10),
  XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL",   -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
  XStoreWT_TBL "RIB_XStoreWT_TBL",   -- Size of "RIB_XStoreWT_TBL" is unbounded
  AddrRef_TBL "RIB_AddrRef_TBL",   -- Size of "RIB_AddrRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
) return self as result
,constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
) return self as result
,constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
) return self as result
,constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrRef_TBL "RIB_AddrRef_TBL"  -- Size of "RIB_AddrRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XStoreRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XStoreRef') := "ns_name_XStoreRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  IF XStoreLocTrt_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XStoreLocTrt_TBL.';
    FOR INDX IN XStoreLocTrt_TBL.FIRST()..XStoreLocTrt_TBL.LAST() LOOP
      XStoreLocTrt_TBL(indx).appendNodeValues( i_prefix||indx||'XStoreLocTrt_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XStoreWT_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XStoreWT_TBL.';
    FOR INDX IN XStoreWT_TBL.FIRST()..XStoreWT_TBL.LAST() LOOP
      XStoreWT_TBL(indx).appendNodeValues( i_prefix||indx||'XStoreWT_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF AddrRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrRef_TBL.';
    FOR INDX IN AddrRef_TBL.FIRST()..AddrRef_TBL.LAST() LOOP
      AddrRef_TBL(indx).appendNodeValues( i_prefix||indx||'AddrRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
RETURN;
end;
constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
RETURN;
end;
constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
RETURN;
end;
constructor function "RIB_XStoreRef_REC"
(
  rib_oid number
, store number
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrRef_TBL "RIB_AddrRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrRef_TBL := AddrRef_TBL;
RETURN;
end;
END;
/
