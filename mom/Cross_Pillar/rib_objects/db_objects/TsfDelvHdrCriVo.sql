DROP TYPE "RIB_TsfDelvHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvHdrCriVo_REC";
/
DROP TYPE "RIB_TsfDelvHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  expected_from_date date,
  expected_to_date date,
  received_from_date date,
  received_to_date date,
  source_type varchar2(10), -- source_type is enumeration field, valid values are [WAREHOUSE, FINISHER, STORE, NO_VALUE] (all lower-case)
  source_id number(10),
  delivery_id number(15),
  asn varchar2(128),
  external_id varchar2(128),
  item_id varchar2(25),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, RECEIVED, CANCELED, NO_VALUE] (all lower-case)
  document_id number(15),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  order_related varchar2(5), --order_related is boolean field, valid values are true,false (all lower-case) 
  context_type_id varchar2(128),
  context_value varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
) return self as result
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
) return self as result
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
) return self as result
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
) return self as result
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
, context_type_id varchar2
) return self as result
,constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
, context_type_id varchar2
, context_value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvHdrCriVo') := "ns_name_TsfDelvHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_from_date') := expected_from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_to_date') := expected_to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_from_date') := received_from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_to_date') := received_to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_type') := source_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_id') := source_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_id') := document_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_related') := order_related;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
END appendNodeValues;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
RETURN;
end;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
self.document_id := document_id;
RETURN;
end;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
self.document_id := document_id;
self.customer_order_id := customer_order_id;
RETURN;
end;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
self.document_id := document_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
RETURN;
end;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
self.document_id := document_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.order_related := order_related;
RETURN;
end;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2
, context_type_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
self.document_id := document_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.order_related := order_related;
self.context_type_id := context_type_id;
RETURN;
end;
constructor function "RIB_TsfDelvHdrCriVo_REC"
(
  rib_oid number
, store_id number
, expected_from_date date
, expected_to_date date
, received_from_date date
, received_to_date date
, source_type varchar2
, source_id number
, delivery_id number
, asn varchar2
, external_id varchar2
, item_id varchar2
, status varchar2
, document_id number
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2
, context_type_id varchar2
, context_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.expected_from_date := expected_from_date;
self.expected_to_date := expected_to_date;
self.received_from_date := received_from_date;
self.received_to_date := received_to_date;
self.source_type := source_type;
self.source_id := source_id;
self.delivery_id := delivery_id;
self.asn := asn;
self.external_id := external_id;
self.item_id := item_id;
self.status := status;
self.document_id := document_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.order_related := order_related;
self.context_type_id := context_type_id;
self.context_value := context_value;
RETURN;
end;
END;
/
