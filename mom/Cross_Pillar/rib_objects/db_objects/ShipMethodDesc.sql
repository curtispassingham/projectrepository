@@LocaleTxtColDesc.sql;
/
DROP TYPE "RIB_ShipMethodDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShipMethodDesc_REC";
/
DROP TYPE "RIB_Carrier_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Carrier_REC";
/
DROP TYPE "RIB_CarrierService_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CarrierService_REC";
/
DROP TYPE "RIB_ShipMethodDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShipMethodDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipMethodDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(4),
  Carrier "RIB_Carrier_REC",
  CarrierService "RIB_CarrierService_REC",
  currency_code varchar2(3),
  shipping_charge number(20,4),
  est_delivery_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShipMethodDesc_REC"
(
  rib_oid number
, seq_no number
, Carrier "RIB_Carrier_REC"
, CarrierService "RIB_CarrierService_REC"
, currency_code varchar2
, shipping_charge number
, est_delivery_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShipMethodDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipMethodDesc') := "ns_name_ShipMethodDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  l_new_pre :=i_prefix||'Carrier.';
  Carrier.appendNodeValues( i_prefix||'Carrier');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CarrierService.';
  CarrierService.appendNodeValues( i_prefix||'CarrierService');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_charge') := shipping_charge;
  rib_obj_util.g_RIB_element_values(i_prefix||'est_delivery_date') := est_delivery_date;
END appendNodeValues;
constructor function "RIB_ShipMethodDesc_REC"
(
  rib_oid number
, seq_no number
, Carrier "RIB_Carrier_REC"
, CarrierService "RIB_CarrierService_REC"
, currency_code varchar2
, shipping_charge number
, est_delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.Carrier := Carrier;
self.CarrierService := CarrierService;
self.currency_code := currency_code;
self.shipping_charge := shipping_charge;
self.est_delivery_date := est_delivery_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_Carrier_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Carrier_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipMethodDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  code varchar2(4),
  LocaleTxtColDesc "RIB_LocaleTxtColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Carrier_REC"
(
  rib_oid number
, code varchar2
, LocaleTxtColDesc "RIB_LocaleTxtColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Carrier_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipMethodDesc') := "ns_name_ShipMethodDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'code') := code;
  l_new_pre :=i_prefix||'LocaleTxtColDesc.';
  LocaleTxtColDesc.appendNodeValues( i_prefix||'LocaleTxtColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_Carrier_REC"
(
  rib_oid number
, code varchar2
, LocaleTxtColDesc "RIB_LocaleTxtColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.code := code;
self.LocaleTxtColDesc := LocaleTxtColDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_CarrierService_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CarrierService_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipMethodDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  code varchar2(6),
  LocaleTxtColDesc "RIB_LocaleTxtColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CarrierService_REC"
(
  rib_oid number
, code varchar2
, LocaleTxtColDesc "RIB_LocaleTxtColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CarrierService_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipMethodDesc') := "ns_name_ShipMethodDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'code') := code;
  l_new_pre :=i_prefix||'LocaleTxtColDesc.';
  LocaleTxtColDesc.appendNodeValues( i_prefix||'LocaleTxtColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CarrierService_REC"
(
  rib_oid number
, code varchar2
, LocaleTxtColDesc "RIB_LocaleTxtColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.code := code;
self.LocaleTxtColDesc := LocaleTxtColDesc;
RETURN;
end;
END;
/
