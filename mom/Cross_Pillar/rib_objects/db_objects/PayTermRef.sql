@@ExtOfPayTermRef.sql;
/
@@LocOfPayTermRef.sql;
/
DROP TYPE "RIB_PayTermRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PayTermRef_REC";
/
DROP TYPE "RIB_TermsSeq_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TermsSeq_REC";
/
DROP TYPE "RIB_TermsSeq_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TermsSeq_TBL" AS TABLE OF "RIB_TermsSeq_REC";
/
DROP TYPE "RIB_LocOfPayTermRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermRef_TBL" AS TABLE OF "RIB_LocOfPayTermRef_REC";
/
DROP TYPE "RIB_PayTermRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PayTermRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PayTermRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  terms_xref_key varchar2(32),
  terms varchar2(15),
  TermsSeq_TBL "RIB_TermsSeq_TBL",   -- Size of "RIB_TermsSeq_TBL" is unbounded
  ExtOfPayTermRef "RIB_ExtOfPayTermRef_REC",
  LocOfPayTermRef_TBL "RIB_LocOfPayTermRef_TBL",   -- Size of "RIB_LocOfPayTermRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
) return self as result
,constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, TermsSeq_TBL "RIB_TermsSeq_TBL"  -- Size of "RIB_TermsSeq_TBL" is unbounded
) return self as result
,constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, TermsSeq_TBL "RIB_TermsSeq_TBL"  -- Size of "RIB_TermsSeq_TBL" is unbounded
, ExtOfPayTermRef "RIB_ExtOfPayTermRef_REC"
) return self as result
,constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, TermsSeq_TBL "RIB_TermsSeq_TBL"  -- Size of "RIB_TermsSeq_TBL" is unbounded
, ExtOfPayTermRef "RIB_ExtOfPayTermRef_REC"
, LocOfPayTermRef_TBL "RIB_LocOfPayTermRef_TBL"  -- Size of "RIB_LocOfPayTermRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PayTermRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PayTermRef') := "ns_name_PayTermRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'terms_xref_key') := terms_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms') := terms;
  IF TermsSeq_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TermsSeq_TBL.';
    FOR INDX IN TermsSeq_TBL.FIRST()..TermsSeq_TBL.LAST() LOOP
      TermsSeq_TBL(indx).appendNodeValues( i_prefix||indx||'TermsSeq_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ExtOfPayTermRef.';
  ExtOfPayTermRef.appendNodeValues( i_prefix||'ExtOfPayTermRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfPayTermRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfPayTermRef_TBL.';
    FOR INDX IN LocOfPayTermRef_TBL.FIRST()..LocOfPayTermRef_TBL.LAST() LOOP
      LocOfPayTermRef_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfPayTermRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
RETURN;
end;
constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, TermsSeq_TBL "RIB_TermsSeq_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.TermsSeq_TBL := TermsSeq_TBL;
RETURN;
end;
constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, TermsSeq_TBL "RIB_TermsSeq_TBL"
, ExtOfPayTermRef "RIB_ExtOfPayTermRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.TermsSeq_TBL := TermsSeq_TBL;
self.ExtOfPayTermRef := ExtOfPayTermRef;
RETURN;
end;
constructor function "RIB_PayTermRef_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, TermsSeq_TBL "RIB_TermsSeq_TBL"
, ExtOfPayTermRef "RIB_ExtOfPayTermRef_REC"
, LocOfPayTermRef_TBL "RIB_LocOfPayTermRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.TermsSeq_TBL := TermsSeq_TBL;
self.ExtOfPayTermRef := ExtOfPayTermRef;
self.LocOfPayTermRef_TBL := LocOfPayTermRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TermsSeq_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TermsSeq_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PayTermRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  detail_seq_no number(10),
  ExtOfTermsSeq "RIB_ExtOfTermsSeq_REC",
  LocOfTermsSeq "RIB_LocOfTermsSeq_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TermsSeq_REC"
(
  rib_oid number
, detail_seq_no number
) return self as result
,constructor function "RIB_TermsSeq_REC"
(
  rib_oid number
, detail_seq_no number
, ExtOfTermsSeq "RIB_ExtOfTermsSeq_REC"
) return self as result
,constructor function "RIB_TermsSeq_REC"
(
  rib_oid number
, detail_seq_no number
, ExtOfTermsSeq "RIB_ExtOfTermsSeq_REC"
, LocOfTermsSeq "RIB_LocOfTermsSeq_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TermsSeq_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PayTermRef') := "ns_name_PayTermRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'detail_seq_no') := detail_seq_no;
  l_new_pre :=i_prefix||'ExtOfTermsSeq.';
  ExtOfTermsSeq.appendNodeValues( i_prefix||'ExtOfTermsSeq');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LocOfTermsSeq.';
  LocOfTermsSeq.appendNodeValues( i_prefix||'LocOfTermsSeq');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_TermsSeq_REC"
(
  rib_oid number
, detail_seq_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.detail_seq_no := detail_seq_no;
RETURN;
end;
constructor function "RIB_TermsSeq_REC"
(
  rib_oid number
, detail_seq_no number
, ExtOfTermsSeq "RIB_ExtOfTermsSeq_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.detail_seq_no := detail_seq_no;
self.ExtOfTermsSeq := ExtOfTermsSeq;
RETURN;
end;
constructor function "RIB_TermsSeq_REC"
(
  rib_oid number
, detail_seq_no number
, ExtOfTermsSeq "RIB_ExtOfTermsSeq_REC"
, LocOfTermsSeq "RIB_LocOfTermsSeq_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.detail_seq_no := detail_seq_no;
self.ExtOfTermsSeq := ExtOfTermsSeq;
self.LocOfTermsSeq := LocOfTermsSeq;
RETURN;
end;
END;
/
