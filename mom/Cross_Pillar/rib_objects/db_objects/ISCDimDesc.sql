DROP TYPE "RIB_ISCDimDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ISCDimDesc_REC";
/
DROP TYPE "RIB_ISCDimDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ISCDimDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ISCDimDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  supplier varchar2(10),
  origin_country varchar2(3),
  dim_object varchar2(6),
  object_desc varchar2(40),
  presentation_method varchar2(6),
  method_desc varchar2(40),
  length number(12,4),
  width number(12,4),
  height number(12,4),
  lwh_uom varchar2(4),
  weight number(12,4),
  net_weight number(12,4),
  weight_uom varchar2(4),
  liquid_volume number(12,4),
  liquid_volume_uom varchar2(4),
  stat_cube number(12,4),
  tare_weight number(12,4),
  tare_type varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube number
, tare_weight number
) return self as result
,constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube number
, tare_weight number
, tare_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ISCDimDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ISCDimDesc') := "ns_name_ISCDimDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country') := origin_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'dim_object') := dim_object;
  rib_obj_util.g_RIB_element_values(i_prefix||'object_desc') := object_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'presentation_method') := presentation_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'method_desc') := method_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'height') := height;
  rib_obj_util.g_RIB_element_values(i_prefix||'lwh_uom') := lwh_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'net_weight') := net_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume') := liquid_volume;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume_uom') := liquid_volume_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'stat_cube') := stat_cube;
  rib_obj_util.g_RIB_element_values(i_prefix||'tare_weight') := tare_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'tare_type') := tare_type;
END appendNodeValues;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube number
, tare_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.tare_weight := tare_weight;
RETURN;
end;
constructor function "RIB_ISCDimDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country varchar2
, dim_object varchar2
, object_desc varchar2
, presentation_method varchar2
, method_desc varchar2
, length number
, width number
, height number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube number
, tare_weight number
, tare_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country := origin_country;
self.dim_object := dim_object;
self.object_desc := object_desc;
self.presentation_method := presentation_method;
self.method_desc := method_desc;
self.length := length;
self.width := width;
self.height := height;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
RETURN;
end;
END;
/
