DROP TYPE "RIB_CustOrdSubDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrdSubDesc_REC";
/
DROP TYPE "RIB_SubItemDetails_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SubItemDetails_REC";
/
DROP TYPE "RIB_SubItemDetails_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SubItemDetails_TBL" AS TABLE OF "RIB_SubItemDetails_REC";
/
DROP TYPE "RIB_CustOrdSubDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrdSubDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdSubDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_no varchar2(48),
  fulfill_order_no varchar2(48),
  loc_type varchar2(1), -- loc_type is enumeration field, valid values are [S] (all lower-case)
  loc_id number(10),
  item varchar2(25),
  item_qty number(12,4),
  qty_uom varchar2(4),
  SubItemDetails_TBL "RIB_SubItemDetails_TBL",   -- Size of "RIB_SubItemDetails_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrdSubDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, loc_type varchar2
, loc_id number
, item varchar2
, item_qty number
, qty_uom varchar2
, SubItemDetails_TBL "RIB_SubItemDetails_TBL"  -- Size of "RIB_SubItemDetails_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrdSubDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdSubDesc') := "ns_name_CustOrdSubDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_no') := customer_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_no') := fulfill_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_id') := loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_qty') := item_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_uom') := qty_uom;
  l_new_pre :=i_prefix||'SubItemDetails_TBL.';
  FOR INDX IN SubItemDetails_TBL.FIRST()..SubItemDetails_TBL.LAST() LOOP
    SubItemDetails_TBL(indx).appendNodeValues( i_prefix||indx||'SubItemDetails_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_CustOrdSubDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, loc_type varchar2
, loc_id number
, item varchar2
, item_qty number
, qty_uom varchar2
, SubItemDetails_TBL "RIB_SubItemDetails_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_no := customer_order_no;
self.fulfill_order_no := fulfill_order_no;
self.loc_type := loc_type;
self.loc_id := loc_id;
self.item := item;
self.item_qty := item_qty;
self.qty_uom := qty_uom;
self.SubItemDetails_TBL := SubItemDetails_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_SubItemDetails_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SubItemDetails_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdSubDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  sub_item varchar2(25),
  sub_item_qty number(12,4),
  sub_qty_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SubItemDetails_REC"
(
  rib_oid number
, sub_item varchar2
, sub_item_qty number
, sub_qty_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SubItemDetails_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdSubDesc') := "ns_name_CustOrdSubDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'sub_item') := sub_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'sub_item_qty') := sub_item_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'sub_qty_uom') := sub_qty_uom;
END appendNodeValues;
constructor function "RIB_SubItemDetails_REC"
(
  rib_oid number
, sub_item varchar2
, sub_item_qty number
, sub_qty_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sub_item := sub_item;
self.sub_item_qty := sub_item_qty;
self.sub_qty_uom := sub_qty_uom;
RETURN;
end;
END;
/
