DROP TYPE "RIB_TaxDetRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxDetRBO_REC";
/
DROP TYPE "RIB_TaxDetRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TaxDetRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TaxDetRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  legal_message varchar2(250),
  tax_amount number(20,4),
  tax_basis_amount number(20,4),
  modified_tax_basis_amount number(20,4),
  unit_tax_amount number(20,4),
  unit_tax_uom varchar2(25),
  tax_rate_type varchar2(1),
  tax_code varchar2(25),
  tax_rate number(20,10),
  recoverable_amt number(20,4),
  recovered_amt number(20,4),
  reg_pct_margin_value number(20,4),
  regulated_price number(20,4),
  icms_redu_basis_rate number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
, reg_pct_margin_value number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
, reg_pct_margin_value number
, regulated_price number
) return self as result
,constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
, reg_pct_margin_value number
, regulated_price number
, icms_redu_basis_rate number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TaxDetRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TaxDetRBO') := "ns_name_TaxDetRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'legal_message') := legal_message;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_amount') := tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_basis_amount') := tax_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'modified_tax_basis_amount') := modified_tax_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_tax_amount') := unit_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_tax_uom') := unit_tax_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rate_type') := tax_rate_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_code') := tax_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rate') := tax_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'recoverable_amt') := recoverable_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'recovered_amt') := recovered_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'reg_pct_margin_value') := reg_pct_margin_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'regulated_price') := regulated_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_redu_basis_rate') := icms_redu_basis_rate;
END appendNodeValues;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.recoverable_amt := recoverable_amt;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.recoverable_amt := recoverable_amt;
self.recovered_amt := recovered_amt;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
, reg_pct_margin_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.recoverable_amt := recoverable_amt;
self.recovered_amt := recovered_amt;
self.reg_pct_margin_value := reg_pct_margin_value;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
, reg_pct_margin_value number
, regulated_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.recoverable_amt := recoverable_amt;
self.recovered_amt := recovered_amt;
self.reg_pct_margin_value := reg_pct_margin_value;
self.regulated_price := regulated_price;
RETURN;
end;
constructor function "RIB_TaxDetRBO_REC"
(
  rib_oid number
, legal_message varchar2
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, unit_tax_amount number
, unit_tax_uom varchar2
, tax_rate_type varchar2
, tax_code varchar2
, tax_rate number
, recoverable_amt number
, recovered_amt number
, reg_pct_margin_value number
, regulated_price number
, icms_redu_basis_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.legal_message := legal_message;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.unit_tax_amount := unit_tax_amount;
self.unit_tax_uom := unit_tax_uom;
self.tax_rate_type := tax_rate_type;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.recoverable_amt := recoverable_amt;
self.recovered_amt := recovered_amt;
self.reg_pct_margin_value := reg_pct_margin_value;
self.regulated_price := regulated_price;
self.icms_redu_basis_rate := icms_redu_basis_rate;
RETURN;
end;
END;
/
