@@DiscntLinePkColVo.sql;
/
@@TaxLinePkColVo.sql;
/
@@CustOrdItmColDesc.sql;
/
DROP TYPE "RIB_CustOrdItmPkVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrdItmPkVo_REC";
/
DROP TYPE "RIB_NewPricePkItems_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_NewPricePkItems_REC";
/
DROP TYPE "RIB_CustOrdItmPkVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrdItmPkVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdItmPkVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_item_no number(4),
  fulfill_order_id varchar2(48),
  completed_quantity number(12,4),
  cancelled_quantity number(12,4),
  completed_repriced_quantity number(12,4),
  unit_of_measure varchar2(4),
  currency_code varchar2(3),
  completed_amount number(20,4),
  cancelled_amount number(20,4),
  repriced_amount number(20,4),
  completed_new_amount number(20,4),
  completed_discount_amount number(20,4),
  cancelled_discount_amount number(20,4),
  repriced_discount_amount number(20,4),
  completed_new_discount_amount number(20,4),
  completed_tax_amount number(20,4),
  cancelled_tax_amount number(20,4),
  repriced_tax_amount number(20,4),
  completed_new_tax_amount number(20,4),
  completed_inc_tax_amount number(20,4),
  cancelled_inc_tax_amount number(20,4),
  repriced_inc_tax_amount number(20,4),
  completed_new_inc_tax_amount number(20,4),
  paid_amount number(20,4),
  serial_number varchar2(40),
  DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC",
  TaxLinePkColVo "RIB_TaxLinePkColVo_REC",
  NewPricePkItems "RIB_NewPricePkItems_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
, DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC"
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
, DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC"
, TaxLinePkColVo "RIB_TaxLinePkColVo_REC"
) return self as result
,constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
, DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC"
, TaxLinePkColVo "RIB_TaxLinePkColVo_REC"
, NewPricePkItems "RIB_NewPricePkItems_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrdItmPkVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdItmPkVo') := "ns_name_CustOrdItmPkVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_no') := line_item_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_id') := fulfill_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_quantity') := completed_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_quantity') := cancelled_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_repriced_quantity') := completed_repriced_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_amount') := completed_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_amount') := cancelled_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_amount') := repriced_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_amount') := completed_new_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_discount_amount') := completed_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_discount_amount') := cancelled_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_discount_amount') := repriced_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_discount_amount') := completed_new_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_tax_amount') := completed_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_tax_amount') := cancelled_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_tax_amount') := repriced_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_tax_amount') := completed_new_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_inc_tax_amount') := completed_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_inc_tax_amount') := cancelled_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_inc_tax_amount') := repriced_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_inc_tax_amount') := completed_new_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'paid_amount') := paid_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_number') := serial_number;
  l_new_pre :=i_prefix||'DiscntLinePkColVo.';
  DiscntLinePkColVo.appendNodeValues( i_prefix||'DiscntLinePkColVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'TaxLinePkColVo.';
  TaxLinePkColVo.appendNodeValues( i_prefix||'TaxLinePkColVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'NewPricePkItems.';
  NewPricePkItems.appendNodeValues( i_prefix||'NewPricePkItems');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
self.serial_number := serial_number;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
, DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
self.serial_number := serial_number;
self.DiscntLinePkColVo := DiscntLinePkColVo;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
, DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC"
, TaxLinePkColVo "RIB_TaxLinePkColVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
self.serial_number := serial_number;
self.DiscntLinePkColVo := DiscntLinePkColVo;
self.TaxLinePkColVo := TaxLinePkColVo;
RETURN;
end;
constructor function "RIB_CustOrdItmPkVo_REC"
(
  rib_oid number
, line_item_no number
, fulfill_order_id varchar2
, completed_quantity number
, cancelled_quantity number
, completed_repriced_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, serial_number varchar2
, DiscntLinePkColVo "RIB_DiscntLinePkColVo_REC"
, TaxLinePkColVo "RIB_TaxLinePkColVo_REC"
, NewPricePkItems "RIB_NewPricePkItems_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_no := line_item_no;
self.fulfill_order_id := fulfill_order_id;
self.completed_quantity := completed_quantity;
self.cancelled_quantity := cancelled_quantity;
self.completed_repriced_quantity := completed_repriced_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
self.serial_number := serial_number;
self.DiscntLinePkColVo := DiscntLinePkColVo;
self.TaxLinePkColVo := TaxLinePkColVo;
self.NewPricePkItems := NewPricePkItems;
RETURN;
end;
END;
/
DROP TYPE "RIB_NewPricePkItems_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_NewPricePkItems_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdItmPkVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_NewPricePkItems_REC"
(
  rib_oid number
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_NewPricePkItems_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdItmPkVo') := "ns_name_CustOrdItmPkVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'CustOrdItmColDesc.';
  CustOrdItmColDesc.appendNodeValues( i_prefix||'CustOrdItmColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_NewPricePkItems_REC"
(
  rib_oid number
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustOrdItmColDesc := CustOrdItmColDesc;
RETURN;
end;
END;
/
