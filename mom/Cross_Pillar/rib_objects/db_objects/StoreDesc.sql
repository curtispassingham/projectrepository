@@AddrDesc.sql;
/
DROP TYPE "RIB_StoreDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StoreDesc_REC";
/
DROP TYPE "RIB_AddrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrDesc_TBL" AS TABLE OF "RIB_AddrDesc_REC";
/
DROP TYPE "RIB_StoreDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StoreDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StoreDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store number(10),
  store_type varchar2(1),
  store_name varchar2(150),
  store_name10 varchar2(10),
  store_name3 varchar2(3),
  store_add1 varchar2(240),
  store_add2 varchar2(240),
  store_city varchar2(120),
  county varchar2(250),
  state varchar2(3),
  country_id varchar2(3),
  store_pcode varchar2(30),
  store_class varchar2(1),
  store_mgr_name varchar2(120),
  store_open_date date,
  store_close_date date,
  acquired_date date,
  remodel_date date,
  fax_number varchar2(20),
  phone_number varchar2(20),
  email varchar2(100),
  total_square_ft number(8),
  selling_square_ft number(8),
  linear_distance number(8),
  stockholding_ind varchar2(1),
  channel_id number(4),
  store_format number(4),
  mall_name varchar2(120),
  district number(10),
  district_name varchar2(120),
  promo_zone number(4),
  promo_desc varchar2(20),
  transfer_zone number(4),
  description varchar2(1000),
  default_wh varchar2(10),
  stop_order_days number(3),
  start_order_days number(3),
  currency_code varchar2(3),
  lang number(6),
  integrated_pos_ind varchar2(1),
  orig_currency_code varchar2(3),
  duns_number varchar2(9),
  duns_loc varchar2(4),
  AddrDesc_TBL "RIB_AddrDesc_TBL",   -- Size of "RIB_AddrDesc_TBL" is unbounded
  pricing_loc number(10),
  pricing_loc_curr varchar2(3),
  org_unit_id number(15),
  timezone_name varchar2(64),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
, pricing_loc_curr varchar2
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
, pricing_loc_curr varchar2
, org_unit_id number
) return self as result
,constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
, pricing_loc_curr varchar2
, org_unit_id number
, timezone_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StoreDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StoreDesc') := "ns_name_StoreDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name') := store_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name10') := store_name10;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name3') := store_name3;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_add1') := store_add1;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_add2') := store_add2;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_city') := store_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'county') := county;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_pcode') := store_pcode;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_class') := store_class;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_mgr_name') := store_mgr_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_open_date') := store_open_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_close_date') := store_close_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'acquired_date') := acquired_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'remodel_date') := remodel_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'fax_number') := fax_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'email') := email;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_square_ft') := total_square_ft;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_square_ft') := selling_square_ft;
  rib_obj_util.g_RIB_element_values(i_prefix||'linear_distance') := linear_distance;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_format') := store_format;
  rib_obj_util.g_RIB_element_values(i_prefix||'mall_name') := mall_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'district') := district;
  rib_obj_util.g_RIB_element_values(i_prefix||'district_name') := district_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_zone') := promo_zone;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_desc') := promo_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_zone') := transfer_zone;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_wh') := default_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'stop_order_days') := stop_order_days;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_order_days') := start_order_days;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'integrated_pos_ind') := integrated_pos_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'orig_currency_code') := orig_currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_number') := duns_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_loc') := duns_loc;
  IF AddrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrDesc_TBL.';
    FOR INDX IN AddrDesc_TBL.FIRST()..AddrDesc_TBL.LAST() LOOP
      AddrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'AddrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_loc') := pricing_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_loc_curr') := pricing_loc_curr;
  rib_obj_util.g_RIB_element_values(i_prefix||'org_unit_id') := org_unit_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'timezone_name') := timezone_name;
END appendNodeValues;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.AddrDesc_TBL := AddrDesc_TBL;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
, pricing_loc_curr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
self.pricing_loc_curr := pricing_loc_curr;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
, pricing_loc_curr varchar2
, org_unit_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
self.pricing_loc_curr := pricing_loc_curr;
self.org_unit_id := org_unit_id;
RETURN;
end;
constructor function "RIB_StoreDesc_REC"
(
  rib_oid number
, store number
, store_type varchar2
, store_name varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_add1 varchar2
, store_add2 varchar2
, store_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, store_pcode varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, district_name varchar2
, promo_zone number
, promo_desc varchar2
, transfer_zone number
, description varchar2
, default_wh varchar2
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, integrated_pos_ind varchar2
, orig_currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
, pricing_loc_curr varchar2
, org_unit_id number
, timezone_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_type := store_type;
self.store_name := store_name;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_add1 := store_add1;
self.store_add2 := store_add2;
self.store_city := store_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.store_pcode := store_pcode;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.district_name := district_name;
self.promo_zone := promo_zone;
self.promo_desc := promo_desc;
self.transfer_zone := transfer_zone;
self.description := description;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.integrated_pos_ind := integrated_pos_ind;
self.orig_currency_code := orig_currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
self.pricing_loc_curr := pricing_loc_curr;
self.org_unit_id := org_unit_id;
self.timezone_name := timezone_name;
RETURN;
end;
END;
/
