DROP TYPE "RIB_Pair_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Pair_REC";
/
DROP TYPE "RIB_ReferenceId_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReferenceId_REC";
/
DROP TYPE "RIB_Pair_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Pair_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReferenceId" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  name varchar2(255),
  value varchar2(255),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Pair_REC"
(
  rib_oid number
, name varchar2
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Pair_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReferenceId') := "ns_name_ReferenceId";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_Pair_REC"
(
  rib_oid number
, name varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_Pair_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_Pair_TBL" AS TABLE OF "RIB_Pair_REC";
/
DROP TYPE "RIB_ReferenceId_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReferenceId_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReferenceId" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id varchar2(255),
  Pair_TBL "RIB_Pair_TBL",   -- Size of "RIB_Pair_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReferenceId_REC"
(
  rib_oid number
, id varchar2
) return self as result
,constructor function "RIB_ReferenceId_REC"
(
  rib_oid number
, id varchar2
, Pair_TBL "RIB_Pair_TBL"  -- Size of "RIB_Pair_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReferenceId_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReferenceId') := "ns_name_ReferenceId";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  IF Pair_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'Pair_TBL.';
    FOR INDX IN Pair_TBL.FIRST()..Pair_TBL.LAST() LOOP
      Pair_TBL(indx).appendNodeValues( i_prefix||indx||'Pair_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ReferenceId_REC"
(
  rib_oid number
, id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
RETURN;
end;
constructor function "RIB_ReferenceId_REC"
(
  rib_oid number
, id varchar2
, Pair_TBL "RIB_Pair_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.Pair_TBL := Pair_TBL;
RETURN;
end;
END;
/
