@@ContactDesc.sql;
/
@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_CustOrdFulDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrdFulDesc_REC";
/
DROP TYPE "RIB_DeliveryDestDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DeliveryDestDtl_REC";
/
DROP TYPE "RIB_BillingDestDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BillingDestDtl_REC";
/
DROP TYPE "RIB_CustOrdFulDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrdFulDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdFulDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(4),
  fulfill_order_id varchar2(48),
  fulfill_loc_type varchar2(1), -- fulfill_loc_type is enumeration field, valid values are [S, W] (all lower-case)
  fulfill_loc_id number(10),
  delivery_type varchar2(1), -- delivery_type is enumeration field, valid values are [C, S] (all lower-case)
  partial_delivery_ind varchar2(1), -- partial_delivery_ind is enumeration field, valid values are [Y, N] (all lower-case)
  ship_to_fulfill_loc_flag varchar2(1), -- ship_to_fulfill_loc_flag is enumeration field, valid values are [Y, N] (all lower-case)
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  consumer_delivery_date date,
  consumer_delivery_time date,
  comments varchar2(2000),
  DeliveryDestDtl "RIB_DeliveryDestDtl_REC",
  BillingDestDtl "RIB_BillingDestDtl_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrdFulDesc_REC"
(
  rib_oid number
, seq_no number
, fulfill_order_id varchar2
, fulfill_loc_type varchar2
, fulfill_loc_id number
, delivery_type varchar2
, partial_delivery_ind varchar2
, ship_to_fulfill_loc_flag varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, comments varchar2
, DeliveryDestDtl "RIB_DeliveryDestDtl_REC"
) return self as result
,constructor function "RIB_CustOrdFulDesc_REC"
(
  rib_oid number
, seq_no number
, fulfill_order_id varchar2
, fulfill_loc_type varchar2
, fulfill_loc_id number
, delivery_type varchar2
, partial_delivery_ind varchar2
, ship_to_fulfill_loc_flag varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, comments varchar2
, DeliveryDestDtl "RIB_DeliveryDestDtl_REC"
, BillingDestDtl "RIB_BillingDestDtl_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrdFulDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdFulDesc') := "ns_name_CustOrdFulDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_id') := fulfill_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_loc_type') := fulfill_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_loc_id') := fulfill_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_type') := delivery_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'partial_delivery_ind') := partial_delivery_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_fulfill_loc_flag') := ship_to_fulfill_loc_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_delivery_date') := consumer_delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_delivery_time') := consumer_delivery_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  l_new_pre :=i_prefix||'DeliveryDestDtl.';
  DeliveryDestDtl.appendNodeValues( i_prefix||'DeliveryDestDtl');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'BillingDestDtl.';
  BillingDestDtl.appendNodeValues( i_prefix||'BillingDestDtl');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CustOrdFulDesc_REC"
(
  rib_oid number
, seq_no number
, fulfill_order_id varchar2
, fulfill_loc_type varchar2
, fulfill_loc_id number
, delivery_type varchar2
, partial_delivery_ind varchar2
, ship_to_fulfill_loc_flag varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, comments varchar2
, DeliveryDestDtl "RIB_DeliveryDestDtl_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.fulfill_order_id := fulfill_order_id;
self.fulfill_loc_type := fulfill_loc_type;
self.fulfill_loc_id := fulfill_loc_id;
self.delivery_type := delivery_type;
self.partial_delivery_ind := partial_delivery_ind;
self.ship_to_fulfill_loc_flag := ship_to_fulfill_loc_flag;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.consumer_delivery_date := consumer_delivery_date;
self.consumer_delivery_time := consumer_delivery_time;
self.comments := comments;
self.DeliveryDestDtl := DeliveryDestDtl;
RETURN;
end;
constructor function "RIB_CustOrdFulDesc_REC"
(
  rib_oid number
, seq_no number
, fulfill_order_id varchar2
, fulfill_loc_type varchar2
, fulfill_loc_id number
, delivery_type varchar2
, partial_delivery_ind varchar2
, ship_to_fulfill_loc_flag varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, comments varchar2
, DeliveryDestDtl "RIB_DeliveryDestDtl_REC"
, BillingDestDtl "RIB_BillingDestDtl_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.fulfill_order_id := fulfill_order_id;
self.fulfill_loc_type := fulfill_loc_type;
self.fulfill_loc_id := fulfill_loc_id;
self.delivery_type := delivery_type;
self.partial_delivery_ind := partial_delivery_ind;
self.ship_to_fulfill_loc_flag := ship_to_fulfill_loc_flag;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.consumer_delivery_date := consumer_delivery_date;
self.consumer_delivery_time := consumer_delivery_time;
self.comments := comments;
self.DeliveryDestDtl := DeliveryDestDtl;
self.BillingDestDtl := BillingDestDtl;
RETURN;
end;
END;
/
DROP TYPE "RIB_DeliveryDestDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DeliveryDestDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdFulDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ContactDesc "RIB_ContactDesc_REC",
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DeliveryDestDtl_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
) return self as result
,constructor function "RIB_DeliveryDestDtl_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DeliveryDestDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdFulDesc') := "ns_name_CustOrdFulDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_DeliveryDestDtl_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
RETURN;
end;
constructor function "RIB_DeliveryDestDtl_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_BillingDestDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BillingDestDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdFulDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ContactDesc "RIB_ContactDesc_REC",
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BillingDestDtl_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BillingDestDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdFulDesc') := "ns_name_CustOrdFulDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BillingDestDtl_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
