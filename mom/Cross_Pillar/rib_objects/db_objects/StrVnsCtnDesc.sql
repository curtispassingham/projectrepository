@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_StrVnsCtnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnDesc_REC";
/
DROP TYPE "RIB_StrVnsCtnItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnItm_REC";
/
DROP TYPE "RIB_StrVnsCtnItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnItm_TBL" AS TABLE OF "RIB_StrVnsCtnItm_REC";
/
DROP TYPE "RIB_StrVnsCtnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  container_id number(15),
  store_id number(10),
  container_label varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  package_weight number(12,4),
  package_weight_uom varchar2(4),
  tracking_number varchar2(128),
  package_type varchar2(128),
  restriction_level varchar2(10), -- restriction_level is enumeration field, valid values are [DEPT, CLASS, SUBCLASS, NONE, UNKNOWN] (all lower-case)
  create_user_name varchar2(128),
  create_date date,
  update_user_name varchar2(128),
  update_date date,
  approved_user_name varchar2(128),
  approved_date date,
  StrVnsCtnItm_TBL "RIB_StrVnsCtnItm_TBL",   -- Size of "RIB_StrVnsCtnItm_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
) return self as result
,constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
) return self as result
,constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
) return self as result
,constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
) return self as result
,constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
) return self as result
,constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, StrVnsCtnItm_TBL "RIB_StrVnsCtnItm_TBL"  -- Size of "RIB_StrVnsCtnItm_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsCtnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsCtnDesc') := "ns_name_StrVnsCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_label') := container_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight') := package_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight_uom') := package_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_type') := package_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'restriction_level') := restriction_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user_name') := update_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_user_name') := approved_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_date') := approved_date;
  IF StrVnsCtnItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVnsCtnItm_TBL.';
    FOR INDX IN StrVnsCtnItm_TBL.FIRST()..StrVnsCtnItm_TBL.LAST() LOOP
      StrVnsCtnItm_TBL(indx).appendNodeValues( i_prefix||indx||'StrVnsCtnItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.create_user_name := create_user_name;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
RETURN;
end;
constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
RETURN;
end;
constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
RETURN;
end;
constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
RETURN;
end;
constructor function "RIB_StrVnsCtnDesc_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, status varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, StrVnsCtnItm_TBL "RIB_StrVnsCtnItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.status := status;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
self.StrVnsCtnItm_TBL := StrVnsCtnItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_StrVnsCtnItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  external_id number(15),
  item_id varchar2(25),
  description varchar2(400),
  reason_id number(15),
  unit_of_measure varchar2(40),
  case_size number(10,2),
  quantity number(20,4),
  uin_col "RIB_uin_col_TBL",   -- Size of "RIB_uin_col_TBL" is 5000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsCtnItm_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, description varchar2
, reason_id number
, unit_of_measure varchar2
, case_size number
, quantity number
) return self as result
,constructor function "RIB_StrVnsCtnItm_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, description varchar2
, reason_id number
, unit_of_measure varchar2
, case_size number
, quantity number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
) return self as result
,constructor function "RIB_StrVnsCtnItm_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, description varchar2
, reason_id number
, unit_of_measure varchar2
, case_size number
, quantity number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsCtnItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsCtnDesc') := "ns_name_StrVnsCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  IF uin_col IS NOT NULL THEN
    FOR INDX IN uin_col.FIRST()..uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'uin_col'||'.'):=uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsCtnItm_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, description varchar2
, reason_id number
, unit_of_measure varchar2
, case_size number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.unit_of_measure := unit_of_measure;
self.case_size := case_size;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_StrVnsCtnItm_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, description varchar2
, reason_id number
, unit_of_measure varchar2
, case_size number
, quantity number
, uin_col "RIB_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.unit_of_measure := unit_of_measure;
self.case_size := case_size;
self.quantity := quantity;
self.uin_col := uin_col;
RETURN;
end;
constructor function "RIB_StrVnsCtnItm_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, description varchar2
, reason_id number
, unit_of_measure varchar2
, case_size number
, quantity number
, uin_col "RIB_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.unit_of_measure := unit_of_measure;
self.case_size := case_size;
self.quantity := quantity;
self.uin_col := uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_uin_col_TBL" AS TABLE OF varchar2(128);
/
