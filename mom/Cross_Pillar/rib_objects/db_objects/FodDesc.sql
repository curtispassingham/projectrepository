@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_FodDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodDesc_REC";
/
DROP TYPE "RIB_FodBol_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodBol_REC";
/
DROP TYPE "RIB_FodItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodItm_REC";
/
DROP TYPE "RIB_FodItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FodItm_TBL" AS TABLE OF "RIB_FodItm_REC";
/
DROP TYPE "RIB_FodDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  int_fulfill_order_delivery_id number(12),
  ext_fulfill_order_delivery_id varchar2(25),
  fulfill_order_id number(12),
  store_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [IN_PROGRESS, SUBMITTED, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_date date,
  create_user_name varchar2(128),
  dispatch_date date,
  dispatch_user_name varchar2(128),
  FodBol "RIB_FodBol_REC",
  FodItm_TBL "RIB_FodItm_TBL",   -- Size of "RIB_FodItm_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, ext_fulfill_order_delivery_id varchar2
, fulfill_order_id number
, store_id number
, status varchar2
, create_date date
, create_user_name varchar2
, dispatch_date date
, dispatch_user_name varchar2
, FodBol "RIB_FodBol_REC"
) return self as result
,constructor function "RIB_FodDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, ext_fulfill_order_delivery_id varchar2
, fulfill_order_id number
, store_id number
, status varchar2
, create_date date
, create_user_name varchar2
, dispatch_date date
, dispatch_user_name varchar2
, FodBol "RIB_FodBol_REC"
, FodItm_TBL "RIB_FodItm_TBL"  -- Size of "RIB_FodItm_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodDesc') := "ns_name_FodDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'int_fulfill_order_delivery_id') := int_fulfill_order_delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_fulfill_order_delivery_id') := ext_fulfill_order_delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_id') := fulfill_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_date') := dispatch_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_user_name') := dispatch_user_name;
  l_new_pre :=i_prefix||'FodBol.';
  FodBol.appendNodeValues( i_prefix||'FodBol');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF FodItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FodItm_TBL.';
    FOR INDX IN FodItm_TBL.FIRST()..FodItm_TBL.LAST() LOOP
      FodItm_TBL(indx).appendNodeValues( i_prefix||indx||'FodItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FodDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, ext_fulfill_order_delivery_id varchar2
, fulfill_order_id number
, store_id number
, status varchar2
, create_date date
, create_user_name varchar2
, dispatch_date date
, dispatch_user_name varchar2
, FodBol "RIB_FodBol_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.ext_fulfill_order_delivery_id := ext_fulfill_order_delivery_id;
self.fulfill_order_id := fulfill_order_id;
self.store_id := store_id;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.dispatch_date := dispatch_date;
self.dispatch_user_name := dispatch_user_name;
self.FodBol := FodBol;
RETURN;
end;
constructor function "RIB_FodDesc_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, ext_fulfill_order_delivery_id varchar2
, fulfill_order_id number
, store_id number
, status varchar2
, create_date date
, create_user_name varchar2
, dispatch_date date
, dispatch_user_name varchar2
, FodBol "RIB_FodBol_REC"
, FodItm_TBL "RIB_FodItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.ext_fulfill_order_delivery_id := ext_fulfill_order_delivery_id;
self.fulfill_order_id := fulfill_order_id;
self.store_id := store_id;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.dispatch_date := dispatch_date;
self.dispatch_user_name := dispatch_user_name;
self.FodBol := FodBol;
self.FodItm_TBL := FodItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_FodBol_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodBol_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bill_of_lading_motive_id varchar2(120),
  requested_pickup_date date,
  carrier_role varchar2(20), -- carrier_role is enumeration field, valid values are [SENDER, RECEIVER, THIRD_PARTY] (all lower-case)
  carrier_code varchar2(25),
  carrier_service_code varchar2(25),
  carrier_name varchar2(240),
  carrier_address varchar2(2000),
  carton_type_id number(12),
  package_weight number(12,4),
  package_weight_uom varchar2(4),
  ship_to_address_type varchar2(10), -- ship_to_address_type is enumeration field, valid values are [BUSINESS, POSTAL, RETURNS, ORDER, INVOICE, REMITTANCE, NO_VALUE] (all lower-case)
  alternate_ship_to_address varchar2(2000),
  tracking_number varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, ship_to_address_type varchar2
) return self as result
,constructor function "RIB_FodBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result
,constructor function "RIB_FodBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
, tracking_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodBol_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodDesc') := "ns_name_FodDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_motive_id') := bill_of_lading_motive_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pickup_date') := requested_pickup_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_role') := carrier_role;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_address') := carrier_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_type_id') := carton_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight') := package_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight_uom') := package_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_address_type') := ship_to_address_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_ship_to_address') := alternate_ship_to_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
END appendNodeValues;
constructor function "RIB_FodBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, ship_to_address_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.ship_to_address_type := ship_to_address_type;
RETURN;
end;
constructor function "RIB_FodBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
RETURN;
end;
constructor function "RIB_FodBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
, tracking_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
self.tracking_number := tracking_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_FodItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  fulfill_order_delivery_line_id number(12),
  fulfill_order_line_id number(12),
  quantity number(20,4),
  case_size number(10,2),
  uin_col "RIB_uin_col_TBL",   -- Size of "RIB_uin_col_TBL" is unbounded
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodItm_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, fulfill_order_line_id number
, quantity number
, case_size number
) return self as result
,constructor function "RIB_FodItm_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, fulfill_order_line_id number
, quantity number
, case_size number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is unbounded
) return self as result
,constructor function "RIB_FodItm_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, fulfill_order_line_id number
, quantity number
, case_size number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is unbounded
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodDesc') := "ns_name_FodDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_delivery_line_id') := fulfill_order_delivery_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_line_id') := fulfill_order_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  IF uin_col IS NOT NULL THEN
    FOR INDX IN uin_col.FIRST()..uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'uin_col'||'.'):=uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FodItm_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, fulfill_order_line_id number
, quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.fulfill_order_line_id := fulfill_order_line_id;
self.quantity := quantity;
self.case_size := case_size;
RETURN;
end;
constructor function "RIB_FodItm_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, fulfill_order_line_id number
, quantity number
, case_size number
, uin_col "RIB_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.fulfill_order_line_id := fulfill_order_line_id;
self.quantity := quantity;
self.case_size := case_size;
self.uin_col := uin_col;
RETURN;
end;
constructor function "RIB_FodItm_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, fulfill_order_line_id number
, quantity number
, case_size number
, uin_col "RIB_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.fulfill_order_line_id := fulfill_order_line_id;
self.quantity := quantity;
self.case_size := case_size;
self.uin_col := uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_uin_col_TBL" AS TABLE OF varchar2(128);
/
