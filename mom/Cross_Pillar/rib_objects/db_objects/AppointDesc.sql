DROP TYPE "RIB_AppointDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AppointDtl_REC";
/
DROP TYPE "RIB_AppointDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AppointDesc_REC";
/
DROP TYPE "RIB_AppointDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AppointDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AppointDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  unit_qty number(12,4),
  po_nbr varchar2(12),
  document_type varchar2(1),
  asn_nbr varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AppointDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, po_nbr varchar2
, document_type varchar2
) return self as result
,constructor function "RIB_AppointDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, po_nbr varchar2
, document_type varchar2
, asn_nbr varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AppointDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AppointDesc') := "ns_name_AppointDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
END appendNodeValues;
constructor function "RIB_AppointDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, po_nbr varchar2
, document_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.po_nbr := po_nbr;
self.document_type := document_type;
RETURN;
end;
constructor function "RIB_AppointDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, po_nbr varchar2
, document_type varchar2
, asn_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.po_nbr := po_nbr;
self.document_type := document_type;
self.asn_nbr := asn_nbr;
RETURN;
end;
END;
/
DROP TYPE "RIB_AppointDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AppointDtl_TBL" AS TABLE OF "RIB_AppointDtl_REC";
/
DROP TYPE "RIB_AppointDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AppointDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AppointDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_location varchar2(10),
  to_location varchar2(10),
  appt_nbr number(9),
  appt_start_ts date,
  appt_action_status varchar2(2),
  AppointDtl_TBL "RIB_AppointDtl_TBL",   -- Size of "RIB_AppointDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AppointDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, appt_nbr number
, appt_start_ts date
, appt_action_status varchar2
) return self as result
,constructor function "RIB_AppointDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, appt_nbr number
, appt_start_ts date
, appt_action_status varchar2
, AppointDtl_TBL "RIB_AppointDtl_TBL"  -- Size of "RIB_AppointDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AppointDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AppointDesc') := "ns_name_AppointDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_nbr') := appt_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_start_ts') := appt_start_ts;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_action_status') := appt_action_status;
  IF AppointDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AppointDtl_TBL.';
    FOR INDX IN AppointDtl_TBL.FIRST()..AppointDtl_TBL.LAST() LOOP
      AppointDtl_TBL(indx).appendNodeValues( i_prefix||indx||'AppointDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_AppointDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, appt_nbr number
, appt_start_ts date
, appt_action_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.appt_nbr := appt_nbr;
self.appt_start_ts := appt_start_ts;
self.appt_action_status := appt_action_status;
RETURN;
end;
constructor function "RIB_AppointDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, appt_nbr number
, appt_start_ts date
, appt_action_status varchar2
, AppointDtl_TBL "RIB_AppointDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.appt_nbr := appt_nbr;
self.appt_start_ts := appt_start_ts;
self.appt_action_status := appt_action_status;
self.AppointDtl_TBL := AppointDtl_TBL;
RETURN;
end;
END;
/
