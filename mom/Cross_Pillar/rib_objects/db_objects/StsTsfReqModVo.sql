DROP TYPE "RIB_StsTsfReqModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfReqModVo_REC";
/
DROP TYPE "RIB_StsTsfReqItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfReqItmMod_REC";
/
DROP TYPE "RIB_StsTsfReqModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfReqModNote_REC";
/
DROP TYPE "RIB_StsTsfReqItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfReqItmMod_TBL" AS TABLE OF "RIB_StsTsfReqItmMod_REC";
/
DROP TYPE "RIB_StsTsfReqModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfReqModNote_TBL" AS TABLE OF "RIB_StsTsfReqModNote_REC";
/
DROP TYPE "RIB_StsTsfReqModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfReqModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfReqModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_id number(15),
  store_id number(10),
  not_after_date date,
  context_type_id varchar2(15),
  context_value varchar2(25),
  partial_delivery_allowed varchar2(5), --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case) 
  StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL",   -- Size of "RIB_StsTsfReqItmMod_TBL" is 5000
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 5000
  StsTsfReqModNote_TBL "RIB_StsTsfReqModNote_TBL",   -- Size of "RIB_StsTsfReqModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL"  -- Size of "RIB_StsTsfReqItmMod_TBL" is 5000
) return self as result
,constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL"  -- Size of "RIB_StsTsfReqItmMod_TBL" is 5000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
) return self as result
,constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL"  -- Size of "RIB_StsTsfReqItmMod_TBL" is 5000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
, StsTsfReqModNote_TBL "RIB_StsTsfReqModNote_TBL"  -- Size of "RIB_StsTsfReqModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfReqModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfReqModVo') := "ns_name_StsTsfReqModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_id') := tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'partial_delivery_allowed') := partial_delivery_allowed;
  IF StsTsfReqItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StsTsfReqItmMod_TBL.';
    FOR INDX IN StsTsfReqItmMod_TBL.FIRST()..StsTsfReqItmMod_TBL.LAST() LOOP
      StsTsfReqItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'StsTsfReqItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StsTsfReqModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StsTsfReqModNote_TBL.';
    FOR INDX IN StsTsfReqModNote_TBL.FIRST()..StsTsfReqModNote_TBL.LAST() LOOP
      StsTsfReqModNote_TBL(indx).appendNodeValues( i_prefix||indx||'StsTsfReqModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.not_after_date := not_after_date;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
RETURN;
end;
constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.not_after_date := not_after_date;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.StsTsfReqItmMod_TBL := StsTsfReqItmMod_TBL;
RETURN;
end;
constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.not_after_date := not_after_date;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.StsTsfReqItmMod_TBL := StsTsfReqItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
constructor function "RIB_StsTsfReqModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, not_after_date date
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, StsTsfReqItmMod_TBL "RIB_StsTsfReqItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
, StsTsfReqModNote_TBL "RIB_StsTsfReqModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.not_after_date := not_after_date;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.StsTsfReqItmMod_TBL := StsTsfReqItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
self.StsTsfReqModNote_TBL := StsTsfReqModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_StsTsfReqItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfReqItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfReqModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  quantity number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfReqItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, quantity number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfReqItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfReqModVo') := "ns_name_StsTsfReqModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_StsTsfReqItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.quantity := quantity;
self.case_size := case_size;
RETURN;
end;
END;
/
DROP TYPE "RIB_StsTsfReqModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfReqModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfReqModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfReqModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfReqModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfReqModVo') := "ns_name_StsTsfReqModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_StsTsfReqModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
