DROP TYPE "RIB_TsfShipCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCriVo_REC";
/
DROP TYPE "RIB_TsfShipCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  update_from_date date,
  update_to_date date,
  not_after_date date,
  destination_type varchar2(20), -- destination_type is enumeration field, valid values are [FINISHER, STORE, WAREHOUSE, NO_VALUE] (all lower-case)
  destination_id number(10),
  tsf_id number(15),
  asn varchar2(128),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  order_related varchar2(5), --order_related is boolean field, valid values are true,false (all lower-case) 
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, SHIPPED, CANCELED, ACTIVE, NO_VALUE] (all lower-case)
  context_type_id varchar2(128),
  context_value varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCriVo_REC"
(
  rib_oid number
, store_id number
, update_from_date date
, update_to_date date
, not_after_date date
, destination_type varchar2
, destination_id number
, tsf_id number
, asn varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
, status varchar2
) return self as result
,constructor function "RIB_TsfShipCriVo_REC"
(
  rib_oid number
, store_id number
, update_from_date date
, update_to_date date
, not_after_date date
, destination_type varchar2
, destination_id number
, tsf_id number
, asn varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
, status varchar2
, context_type_id varchar2
) return self as result
,constructor function "RIB_TsfShipCriVo_REC"
(
  rib_oid number
, store_id number
, update_from_date date
, update_to_date date
, not_after_date date
, destination_type varchar2
, destination_id number
, tsf_id number
, asn varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
, status varchar2
, context_type_id varchar2
, context_value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCriVo') := "ns_name_TsfShipCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_from_date') := update_from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_to_date') := update_to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_type') := destination_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_id') := destination_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_id') := tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_related') := order_related;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
END appendNodeValues;
constructor function "RIB_TsfShipCriVo_REC"
(
  rib_oid number
, store_id number
, update_from_date date
, update_to_date date
, not_after_date date
, destination_type varchar2
, destination_id number
, tsf_id number
, asn varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.update_from_date := update_from_date;
self.update_to_date := update_to_date;
self.not_after_date := not_after_date;
self.destination_type := destination_type;
self.destination_id := destination_id;
self.tsf_id := tsf_id;
self.asn := asn;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.order_related := order_related;
self.status := status;
RETURN;
end;
constructor function "RIB_TsfShipCriVo_REC"
(
  rib_oid number
, store_id number
, update_from_date date
, update_to_date date
, not_after_date date
, destination_type varchar2
, destination_id number
, tsf_id number
, asn varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2
, status varchar2
, context_type_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.update_from_date := update_from_date;
self.update_to_date := update_to_date;
self.not_after_date := not_after_date;
self.destination_type := destination_type;
self.destination_id := destination_id;
self.tsf_id := tsf_id;
self.asn := asn;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.order_related := order_related;
self.status := status;
self.context_type_id := context_type_id;
RETURN;
end;
constructor function "RIB_TsfShipCriVo_REC"
(
  rib_oid number
, store_id number
, update_from_date date
, update_to_date date
, not_after_date date
, destination_type varchar2
, destination_id number
, tsf_id number
, asn varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, order_related varchar2
, status varchar2
, context_type_id varchar2
, context_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.update_from_date := update_from_date;
self.update_to_date := update_to_date;
self.not_after_date := not_after_date;
self.destination_type := destination_type;
self.destination_id := destination_id;
self.tsf_id := tsf_id;
self.asn := asn;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.order_related := order_related;
self.status := status;
self.context_type_id := context_type_id;
self.context_value := context_value;
RETURN;
end;
END;
/
