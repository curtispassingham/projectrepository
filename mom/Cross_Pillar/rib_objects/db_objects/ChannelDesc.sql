DROP TYPE "RIB_ChannelDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ChannelDesc_REC";
/
DROP TYPE "RIB_ChannelDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ChannelDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ChannelDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  channel_id number(4),
  channel_name varchar2(120),
  channel_type varchar2(6),
  banner_id number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ChannelDesc_REC"
(
  rib_oid number
, channel_id number
, channel_name varchar2
, channel_type varchar2
, banner_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ChannelDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ChannelDesc') := "ns_name_ChannelDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_name') := channel_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_type') := channel_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'banner_id') := banner_id;
END appendNodeValues;
constructor function "RIB_ChannelDesc_REC"
(
  rib_oid number
, channel_id number
, channel_name varchar2
, channel_type varchar2
, banner_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.channel_id := channel_id;
self.channel_name := channel_name;
self.channel_type := channel_type;
self.banner_id := banner_id;
RETURN;
end;
END;
/
