@@InPayTermDesc.sql;
/
@@BrPayTermDesc.sql;
/
DROP TYPE "RIB_LocOfPayTermDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermDtl_REC";
/
DROP TYPE "RIB_LocOfPayTermDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermDesc_REC";
/
DROP TYPE "RIB_InPayTermDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InPayTermDtl_TBL" AS TABLE OF "RIB_InPayTermDtl_REC";
/
DROP TYPE "RIB_BrPayTermDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrPayTermDtl_TBL" AS TABLE OF "RIB_BrPayTermDtl_REC";
/
DROP TYPE "RIB_LocOfPayTermDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfPayTermDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InPayTermDtl_TBL "RIB_InPayTermDtl_TBL",   -- Size of "RIB_InPayTermDtl_TBL" is unbounded
  BrPayTermDtl_TBL "RIB_BrPayTermDtl_TBL",   -- Size of "RIB_BrPayTermDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfPayTermDtl_REC"
(
  rib_oid number
, InPayTermDtl_TBL "RIB_InPayTermDtl_TBL"  -- Size of "RIB_InPayTermDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfPayTermDtl_REC"
(
  rib_oid number
, InPayTermDtl_TBL "RIB_InPayTermDtl_TBL"  -- Size of "RIB_InPayTermDtl_TBL" is unbounded
, BrPayTermDtl_TBL "RIB_BrPayTermDtl_TBL"  -- Size of "RIB_BrPayTermDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfPayTermDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfPayTermDesc') := "ns_name_LocOfPayTermDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InPayTermDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InPayTermDtl_TBL.';
    FOR INDX IN InPayTermDtl_TBL.FIRST()..InPayTermDtl_TBL.LAST() LOOP
      InPayTermDtl_TBL(indx).appendNodeValues( i_prefix||indx||'InPayTermDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrPayTermDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrPayTermDtl_TBL.';
    FOR INDX IN BrPayTermDtl_TBL.FIRST()..BrPayTermDtl_TBL.LAST() LOOP
      BrPayTermDtl_TBL(indx).appendNodeValues( i_prefix||indx||'BrPayTermDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfPayTermDtl_REC"
(
  rib_oid number
, InPayTermDtl_TBL "RIB_InPayTermDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InPayTermDtl_TBL := InPayTermDtl_TBL;
RETURN;
end;
constructor function "RIB_LocOfPayTermDtl_REC"
(
  rib_oid number
, InPayTermDtl_TBL "RIB_InPayTermDtl_TBL"
, BrPayTermDtl_TBL "RIB_BrPayTermDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InPayTermDtl_TBL := InPayTermDtl_TBL;
self.BrPayTermDtl_TBL := BrPayTermDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InPayTermDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InPayTermDesc_TBL" AS TABLE OF "RIB_InPayTermDesc_REC";
/
DROP TYPE "RIB_BrPayTermDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrPayTermDesc_TBL" AS TABLE OF "RIB_BrPayTermDesc_REC";
/
DROP TYPE "RIB_LocOfPayTermDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfPayTermDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InPayTermDesc_TBL "RIB_InPayTermDesc_TBL",   -- Size of "RIB_InPayTermDesc_TBL" is unbounded
  BrPayTermDesc_TBL "RIB_BrPayTermDesc_TBL",   -- Size of "RIB_BrPayTermDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfPayTermDesc_REC"
(
  rib_oid number
, InPayTermDesc_TBL "RIB_InPayTermDesc_TBL"  -- Size of "RIB_InPayTermDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfPayTermDesc_REC"
(
  rib_oid number
, InPayTermDesc_TBL "RIB_InPayTermDesc_TBL"  -- Size of "RIB_InPayTermDesc_TBL" is unbounded
, BrPayTermDesc_TBL "RIB_BrPayTermDesc_TBL"  -- Size of "RIB_BrPayTermDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfPayTermDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfPayTermDesc') := "ns_name_LocOfPayTermDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF InPayTermDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InPayTermDesc_TBL.';
    FOR INDX IN InPayTermDesc_TBL.FIRST()..InPayTermDesc_TBL.LAST() LOOP
      InPayTermDesc_TBL(indx).appendNodeValues( i_prefix||indx||'InPayTermDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrPayTermDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrPayTermDesc_TBL.';
    FOR INDX IN BrPayTermDesc_TBL.FIRST()..BrPayTermDesc_TBL.LAST() LOOP
      BrPayTermDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrPayTermDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfPayTermDesc_REC"
(
  rib_oid number
, InPayTermDesc_TBL "RIB_InPayTermDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InPayTermDesc_TBL := InPayTermDesc_TBL;
RETURN;
end;
constructor function "RIB_LocOfPayTermDesc_REC"
(
  rib_oid number
, InPayTermDesc_TBL "RIB_InPayTermDesc_TBL"
, BrPayTermDesc_TBL "RIB_BrPayTermDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InPayTermDesc_TBL := InPayTermDesc_TBL;
self.BrPayTermDesc_TBL := BrPayTermDesc_TBL;
RETURN;
end;
END;
/
