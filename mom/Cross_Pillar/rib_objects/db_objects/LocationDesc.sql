DROP TYPE "RIB_LocationDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocationDesc_REC";
/
DROP TYPE "RIB_LocationDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocationDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocationDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dest_id varchar2(10),
  dest_type varchar2(20),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  description varchar2(1000),
  address1 varchar2(240),
  address2 varchar2(240),
  city varchar2(120),
  state varchar2(3),
  country_code varchar2(3),
  zip varchar2(30),
  contact_person varchar2(120),
  dest_fax varchar2(20),
  phone_nbr varchar2(20),
  currency_code varchar2(3),
  default_route varchar2(10),
  default_carrier_code varchar2(4),
  default_service_code varchar2(6),
  expedite_route varchar2(10),
  expedite_carrier_code varchar2(4),
  expedite_service_code varchar2(6),
  bol_upload_type varchar2(4),
  bol_print_type varchar2(4),
  lead_time number(3),
  distance_to_dest number(4),
  drop_trailers_accepted_flag varchar2(1),
  rcv_dock_available_flag varchar2(1),
  container_type varchar2(6),
  mld_default_route varchar2(10),
  unit_pick_container_type varchar2(6),
  dest_seq_nbr number(4),
  owning_dc varchar2(10),
  email_to varchar2(500),
  email_cc varchar2(500),
  email_bcc varchar2(500),
  org_unit_id number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
, email_cc varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
, email_cc varchar2
, email_bcc varchar2
) return self as result
,constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
, email_cc varchar2
, email_bcc varchar2
, org_unit_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocationDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocationDesc') := "ns_name_LocationDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_type') := dest_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'address1') := address1;
  rib_obj_util.g_RIB_element_values(i_prefix||'address2') := address2;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_code') := country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'zip') := zip;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_person') := contact_person;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_fax') := dest_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_nbr') := phone_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_route') := default_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_carrier_code') := default_carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_service_code') := default_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_route') := expedite_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_carrier_code') := expedite_carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_service_code') := expedite_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_upload_type') := bol_upload_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_print_type') := bol_print_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'lead_time') := lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'distance_to_dest') := distance_to_dest;
  rib_obj_util.g_RIB_element_values(i_prefix||'drop_trailers_accepted_flag') := drop_trailers_accepted_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'rcv_dock_available_flag') := rcv_dock_available_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_type') := container_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'mld_default_route') := mld_default_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_pick_container_type') := unit_pick_container_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_seq_nbr') := dest_seq_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'owning_dc') := owning_dc;
  rib_obj_util.g_RIB_element_values(i_prefix||'email_to') := email_to;
  rib_obj_util.g_RIB_element_values(i_prefix||'email_cc') := email_cc;
  rib_obj_util.g_RIB_element_values(i_prefix||'email_bcc') := email_bcc;
  rib_obj_util.g_RIB_element_values(i_prefix||'org_unit_id') := org_unit_id;
END appendNodeValues;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.email_to := email_to;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
, email_cc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.email_to := email_to;
self.email_cc := email_cc;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
, email_cc varchar2
, email_bcc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.email_to := email_to;
self.email_cc := email_cc;
self.email_bcc := email_bcc;
RETURN;
end;
constructor function "RIB_LocationDesc_REC"
(
  rib_oid number
, dest_id varchar2
, dest_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, description varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, zip varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, currency_code varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, email_to varchar2
, email_cc varchar2
, email_bcc varchar2
, org_unit_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.dest_type := dest_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.description := description;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.country_code := country_code;
self.zip := zip;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.currency_code := currency_code;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.email_to := email_to;
self.email_cc := email_cc;
self.email_bcc := email_bcc;
self.org_unit_id := org_unit_id;
RETURN;
end;
END;
/
