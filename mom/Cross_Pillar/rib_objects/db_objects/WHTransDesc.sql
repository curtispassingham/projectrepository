DROP TYPE "RIB_WHTran_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WHTran_REC";
/
DROP TYPE "RIB_WHTransDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WHTransDesc_REC";
/
DROP TYPE "RIB_WHTran_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WHTran_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WHTransDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_location varchar2(10),
  to_location varchar2(10),
  process_id varchar2(30),
  user_id varchar2(30),
  start_time date,
  end_time date,
  units number(12,4),
  cases number(12,4),
  pallets number(12,4),
  equipment_id varchar2(30),
  from_loc varchar2(12),
  to_loc varchar2(12),
  from_dest_id varchar2(10),
  from_dest_type varchar2(20),
  to_dest_id varchar2(10),
  to_dest_type varchar2(20),
  item_id varchar2(25),
  po_nbr varchar2(12),
  po_doc_type varchar2(1),
  appt_nbr number(9),
  appt_line number(4),
  asn_nbr varchar2(30),
  nsc_flag varchar2(1),
  conveyable_flag varchar2(1),
  appt_start_time date,
  appt_end_time date,
  trailer_id varchar2(12),
  bol_nbr varchar2(17),
  carrier varchar2(4),
  service varchar2(6),
  route varchar2(10),
  distro_nbr varchar2(12),
  distro_doc_type varchar2(1),
  wave_nbr number(3),
  pick_type varchar2(2),
  wip_code varchar2(9),
  lms_uda1 varchar2(250),
  lms_uda2 varchar2(250),
  lms_uda3 varchar2(250),
  lms_uda4 varchar2(250),
  lms_uda5 varchar2(250),
  lms_uda6 varchar2(250),
  lms_uda7 varchar2(250),
  lms_uda8 varchar2(250),
  lms_uda9 varchar2(250),
  lms_uda10 varchar2(250),
  trailer_close_date date,
  to_loc_type varchar2(1),
  equipment_class varchar2(30),
  pallet_cube number(12,4),
  pallet_weight number(12,4),
  case_cube number(12,4),
  case_weight number(12,4),
  unit_cube number(12,4),
  unit_weight number(12,4),
  container_type varchar2(6),
  zone varchar2(2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
, unit_weight number
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
, unit_weight number
, container_type varchar2
) return self as result
,constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
, unit_weight number
, container_type varchar2
, zone varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WHTran_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WHTransDesc') := "ns_name_WHTransDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'process_id') := process_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_time') := start_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_time') := end_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'units') := units;
  rib_obj_util.g_RIB_element_values(i_prefix||'cases') := cases;
  rib_obj_util.g_RIB_element_values(i_prefix||'pallets') := pallets;
  rib_obj_util.g_RIB_element_values(i_prefix||'equipment_id') := equipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc') := from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_dest_id') := from_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_dest_type') := from_dest_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_dest_id') := to_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_dest_type') := to_dest_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_doc_type') := po_doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_nbr') := appt_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_line') := appt_line;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'nsc_flag') := nsc_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'conveyable_flag') := conveyable_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_start_time') := appt_start_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_end_time') := appt_end_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'trailer_id') := trailer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_nbr') := bol_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier') := carrier;
  rib_obj_util.g_RIB_element_values(i_prefix||'service') := service;
  rib_obj_util.g_RIB_element_values(i_prefix||'route') := route;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_doc_type') := distro_doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'wave_nbr') := wave_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_type') := pick_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'wip_code') := wip_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda1') := lms_uda1;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda2') := lms_uda2;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda3') := lms_uda3;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda4') := lms_uda4;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda5') := lms_uda5;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda6') := lms_uda6;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda7') := lms_uda7;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda8') := lms_uda8;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda9') := lms_uda9;
  rib_obj_util.g_RIB_element_values(i_prefix||'lms_uda10') := lms_uda10;
  rib_obj_util.g_RIB_element_values(i_prefix||'trailer_close_date') := trailer_close_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'equipment_class') := equipment_class;
  rib_obj_util.g_RIB_element_values(i_prefix||'pallet_cube') := pallet_cube;
  rib_obj_util.g_RIB_element_values(i_prefix||'pallet_weight') := pallet_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_cube') := case_cube;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_weight') := case_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cube') := unit_cube;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_weight') := unit_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_type') := container_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'zone') := zone;
END appendNodeValues;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
self.case_cube := case_cube;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
self.case_cube := case_cube;
self.case_weight := case_weight;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
self.case_cube := case_cube;
self.case_weight := case_weight;
self.unit_cube := unit_cube;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
, unit_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
self.case_cube := case_cube;
self.case_weight := case_weight;
self.unit_cube := unit_cube;
self.unit_weight := unit_weight;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
, unit_weight number
, container_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
self.case_cube := case_cube;
self.case_weight := case_weight;
self.unit_cube := unit_cube;
self.unit_weight := unit_weight;
self.container_type := container_type;
RETURN;
end;
constructor function "RIB_WHTran_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, process_id varchar2
, user_id varchar2
, start_time date
, end_time date
, units number
, cases number
, pallets number
, equipment_id varchar2
, from_loc varchar2
, to_loc varchar2
, from_dest_id varchar2
, from_dest_type varchar2
, to_dest_id varchar2
, to_dest_type varchar2
, item_id varchar2
, po_nbr varchar2
, po_doc_type varchar2
, appt_nbr number
, appt_line number
, asn_nbr varchar2
, nsc_flag varchar2
, conveyable_flag varchar2
, appt_start_time date
, appt_end_time date
, trailer_id varchar2
, bol_nbr varchar2
, carrier varchar2
, service varchar2
, route varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, wave_nbr number
, pick_type varchar2
, wip_code varchar2
, lms_uda1 varchar2
, lms_uda2 varchar2
, lms_uda3 varchar2
, lms_uda4 varchar2
, lms_uda5 varchar2
, lms_uda6 varchar2
, lms_uda7 varchar2
, lms_uda8 varchar2
, lms_uda9 varchar2
, lms_uda10 varchar2
, trailer_close_date date
, to_loc_type varchar2
, equipment_class varchar2
, pallet_cube number
, pallet_weight number
, case_cube number
, case_weight number
, unit_cube number
, unit_weight number
, container_type varchar2
, zone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.process_id := process_id;
self.user_id := user_id;
self.start_time := start_time;
self.end_time := end_time;
self.units := units;
self.cases := cases;
self.pallets := pallets;
self.equipment_id := equipment_id;
self.from_loc := from_loc;
self.to_loc := to_loc;
self.from_dest_id := from_dest_id;
self.from_dest_type := from_dest_type;
self.to_dest_id := to_dest_id;
self.to_dest_type := to_dest_type;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.po_doc_type := po_doc_type;
self.appt_nbr := appt_nbr;
self.appt_line := appt_line;
self.asn_nbr := asn_nbr;
self.nsc_flag := nsc_flag;
self.conveyable_flag := conveyable_flag;
self.appt_start_time := appt_start_time;
self.appt_end_time := appt_end_time;
self.trailer_id := trailer_id;
self.bol_nbr := bol_nbr;
self.carrier := carrier;
self.service := service;
self.route := route;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.wave_nbr := wave_nbr;
self.pick_type := pick_type;
self.wip_code := wip_code;
self.lms_uda1 := lms_uda1;
self.lms_uda2 := lms_uda2;
self.lms_uda3 := lms_uda3;
self.lms_uda4 := lms_uda4;
self.lms_uda5 := lms_uda5;
self.lms_uda6 := lms_uda6;
self.lms_uda7 := lms_uda7;
self.lms_uda8 := lms_uda8;
self.lms_uda9 := lms_uda9;
self.lms_uda10 := lms_uda10;
self.trailer_close_date := trailer_close_date;
self.to_loc_type := to_loc_type;
self.equipment_class := equipment_class;
self.pallet_cube := pallet_cube;
self.pallet_weight := pallet_weight;
self.case_cube := case_cube;
self.case_weight := case_weight;
self.unit_cube := unit_cube;
self.unit_weight := unit_weight;
self.container_type := container_type;
self.zone := zone;
RETURN;
end;
END;
/
DROP TYPE "RIB_WHTran_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WHTran_TBL" AS TABLE OF "RIB_WHTran_REC";
/
DROP TYPE "RIB_WHTransDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WHTransDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WHTransDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  WHTran_TBL "RIB_WHTran_TBL",   -- Size of "RIB_WHTran_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WHTransDesc_REC"
(
  rib_oid number
, WHTran_TBL "RIB_WHTran_TBL"  -- Size of "RIB_WHTran_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WHTransDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WHTransDesc') := "ns_name_WHTransDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF WHTran_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WHTran_TBL.';
    FOR INDX IN WHTran_TBL.FIRST()..WHTran_TBL.LAST() LOOP
      WHTran_TBL(indx).appendNodeValues( i_prefix||indx||'WHTran_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_WHTransDesc_REC"
(
  rib_oid number
, WHTran_TBL "RIB_WHTran_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.WHTran_TBL := WHTran_TBL;
RETURN;
end;
END;
/
