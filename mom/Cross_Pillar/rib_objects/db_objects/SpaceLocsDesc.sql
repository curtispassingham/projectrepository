DROP TYPE "RIB_SpaceLocsDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SpaceLocsDesc_REC";
/
DROP TYPE "RIB_SpaceLocsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SpaceLocsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SpaceLocsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_location varchar2(10),
  to_location varchar2(20),
  locationid varchar2(12),
  locationtype varchar2(6),
  itemid varchar2(25),
  unitcapacity number(12,4),
  zone varchar2(2),
  locationstatus varchar2(11),
  putawaysequence number(8),
  picksequence number(8),
  description varchar2(60),
  containercapacity number(5),
  unitpiclocflag varchar2(1),
  casepicklocflag varchar2(1),
  length number(12,4),
  width number(12,4),
  height number(6,2),
  maxstdunits number(8),
  volumetype varchar2(4),
  unitcost number(9,4),
  zonedescription varchar2(30),
  pickpriority number(3),
  region varchar2(6),
  workarea varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SpaceLocsDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, locationid varchar2
, locationtype varchar2
, itemid varchar2
, unitcapacity number
, zone varchar2
, locationstatus varchar2
, putawaysequence number
, picksequence number
, description varchar2
, containercapacity number
, unitpiclocflag varchar2
, casepicklocflag varchar2
, length number
, width number
, height number
, maxstdunits number
, volumetype varchar2
, unitcost number
, zonedescription varchar2
, pickpriority number
, region varchar2
, workarea varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SpaceLocsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SpaceLocsDesc') := "ns_name_SpaceLocsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'locationid') := locationid;
  rib_obj_util.g_RIB_element_values(i_prefix||'locationtype') := locationtype;
  rib_obj_util.g_RIB_element_values(i_prefix||'itemid') := itemid;
  rib_obj_util.g_RIB_element_values(i_prefix||'unitcapacity') := unitcapacity;
  rib_obj_util.g_RIB_element_values(i_prefix||'zone') := zone;
  rib_obj_util.g_RIB_element_values(i_prefix||'locationstatus') := locationstatus;
  rib_obj_util.g_RIB_element_values(i_prefix||'putawaysequence') := putawaysequence;
  rib_obj_util.g_RIB_element_values(i_prefix||'picksequence') := picksequence;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'containercapacity') := containercapacity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unitpiclocflag') := unitpiclocflag;
  rib_obj_util.g_RIB_element_values(i_prefix||'casepicklocflag') := casepicklocflag;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'height') := height;
  rib_obj_util.g_RIB_element_values(i_prefix||'maxstdunits') := maxstdunits;
  rib_obj_util.g_RIB_element_values(i_prefix||'volumetype') := volumetype;
  rib_obj_util.g_RIB_element_values(i_prefix||'unitcost') := unitcost;
  rib_obj_util.g_RIB_element_values(i_prefix||'zonedescription') := zonedescription;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickpriority') := pickpriority;
  rib_obj_util.g_RIB_element_values(i_prefix||'region') := region;
  rib_obj_util.g_RIB_element_values(i_prefix||'workarea') := workarea;
END appendNodeValues;
constructor function "RIB_SpaceLocsDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, locationid varchar2
, locationtype varchar2
, itemid varchar2
, unitcapacity number
, zone varchar2
, locationstatus varchar2
, putawaysequence number
, picksequence number
, description varchar2
, containercapacity number
, unitpiclocflag varchar2
, casepicklocflag varchar2
, length number
, width number
, height number
, maxstdunits number
, volumetype varchar2
, unitcost number
, zonedescription varchar2
, pickpriority number
, region varchar2
, workarea varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.locationid := locationid;
self.locationtype := locationtype;
self.itemid := itemid;
self.unitcapacity := unitcapacity;
self.zone := zone;
self.locationstatus := locationstatus;
self.putawaysequence := putawaysequence;
self.picksequence := picksequence;
self.description := description;
self.containercapacity := containercapacity;
self.unitpiclocflag := unitpiclocflag;
self.casepicklocflag := casepicklocflag;
self.length := length;
self.width := width;
self.height := height;
self.maxstdunits := maxstdunits;
self.volumetype := volumetype;
self.unitcost := unitcost;
self.zonedescription := zonedescription;
self.pickpriority := pickpriority;
self.region := region;
self.workarea := workarea;
RETURN;
end;
END;
/
