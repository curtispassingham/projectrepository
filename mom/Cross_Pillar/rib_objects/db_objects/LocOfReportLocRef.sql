@@InReportLocRef.sql;
/
@@BrReportLocRef.sql;
/
DROP TYPE "RIB_LocOfReportLocRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfReportLocRef_REC";
/
DROP TYPE "RIB_InReportLocRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InReportLocRef_TBL" AS TABLE OF "RIB_InReportLocRef_REC";
/
DROP TYPE "RIB_BrReportLocRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrReportLocRef_TBL" AS TABLE OF "RIB_BrReportLocRef_REC";
/
DROP TYPE "RIB_LocOfReportLocRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfReportLocRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfReportLocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InReportLocRef_TBL "RIB_InReportLocRef_TBL",   -- Size of "RIB_InReportLocRef_TBL" is unbounded
  BrReportLocRef_TBL "RIB_BrReportLocRef_TBL",   -- Size of "RIB_BrReportLocRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfReportLocRef_REC"
(
  rib_oid number
, InReportLocRef_TBL "RIB_InReportLocRef_TBL"  -- Size of "RIB_InReportLocRef_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfReportLocRef_REC"
(
  rib_oid number
, InReportLocRef_TBL "RIB_InReportLocRef_TBL"  -- Size of "RIB_InReportLocRef_TBL" is unbounded
, BrReportLocRef_TBL "RIB_BrReportLocRef_TBL"  -- Size of "RIB_BrReportLocRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfReportLocRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfReportLocRef') := "ns_name_LocOfReportLocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF InReportLocRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InReportLocRef_TBL.';
    FOR INDX IN InReportLocRef_TBL.FIRST()..InReportLocRef_TBL.LAST() LOOP
      InReportLocRef_TBL(indx).appendNodeValues( i_prefix||indx||'InReportLocRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrReportLocRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrReportLocRef_TBL.';
    FOR INDX IN BrReportLocRef_TBL.FIRST()..BrReportLocRef_TBL.LAST() LOOP
      BrReportLocRef_TBL(indx).appendNodeValues( i_prefix||indx||'BrReportLocRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfReportLocRef_REC"
(
  rib_oid number
, InReportLocRef_TBL "RIB_InReportLocRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InReportLocRef_TBL := InReportLocRef_TBL;
RETURN;
end;
constructor function "RIB_LocOfReportLocRef_REC"
(
  rib_oid number
, InReportLocRef_TBL "RIB_InReportLocRef_TBL"
, BrReportLocRef_TBL "RIB_BrReportLocRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InReportLocRef_TBL := InReportLocRef_TBL;
self.BrReportLocRef_TBL := BrReportLocRef_TBL;
RETURN;
end;
END;
/
