DROP TYPE "RIB_ClrPrcChgDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDtlRef_REC";
/
DROP TYPE "RIB_ClrPrcChgRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgRef_REC";
/
DROP TYPE "RIB_ClrPrcChgDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ClrPrcChgRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  clearance_id number(15),
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ClrPrcChgDtlRef_REC"
(
  rib_oid number
, clearance_id number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ClrPrcChgDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ClrPrcChgRef') := "ns_name_ClrPrcChgRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'clearance_id') := clearance_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_ClrPrcChgDtlRef_REC"
(
  rib_oid number
, clearance_id number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.clearance_id := clearance_id;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_ClrPrcChgDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDtlRef_TBL" AS TABLE OF "RIB_ClrPrcChgDtlRef_REC";
/
DROP TYPE "RIB_ClrPrcChgRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ClrPrcChgRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1),
  ClrPrcChgDtlRef_TBL "RIB_ClrPrcChgDtlRef_TBL",   -- Size of "RIB_ClrPrcChgDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ClrPrcChgRef_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, ClrPrcChgDtlRef_TBL "RIB_ClrPrcChgDtlRef_TBL"  -- Size of "RIB_ClrPrcChgDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ClrPrcChgRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ClrPrcChgRef') := "ns_name_ClrPrcChgRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  l_new_pre :=i_prefix||'ClrPrcChgDtlRef_TBL.';
  FOR INDX IN ClrPrcChgDtlRef_TBL.FIRST()..ClrPrcChgDtlRef_TBL.LAST() LOOP
    ClrPrcChgDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'ClrPrcChgDtlRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ClrPrcChgRef_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, ClrPrcChgDtlRef_TBL "RIB_ClrPrcChgDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.ClrPrcChgDtlRef_TBL := ClrPrcChgDtlRef_TBL;
RETURN;
end;
END;
/
