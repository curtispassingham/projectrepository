DROP TYPE "RIB_StrInvCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvCriVo_REC";
/
DROP TYPE "RIB_StrInvCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrInvCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrInvCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id_col "RIB_item_id_col_TBL",   -- Size of "RIB_item_id_col_TBL" is 10
  store_id_col "RIB_store_id_col_TBL",   -- Size of "RIB_store_id_col_TBL" is 100
  uom_type varchar2(10), -- uom_type is enumeration field, valid values are [STANDARD, SELLING] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrInvCriVo_REC"
(
  rib_oid number
, item_id_col "RIB_item_id_col_TBL"  -- Size of "RIB_item_id_col_TBL" is 10
, store_id_col "RIB_store_id_col_TBL"  -- Size of "RIB_store_id_col_TBL" is 100
, uom_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrInvCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrInvCriVo') := "ns_name_StrInvCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF item_id_col IS NOT NULL THEN
    FOR INDX IN item_id_col.FIRST()..item_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'item_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'item_id_col'||'.'):=item_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF store_id_col IS NOT NULL THEN
    FOR INDX IN store_id_col.FIRST()..store_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'store_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'store_id_col'||'.'):=store_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'uom_type') := uom_type;
END appendNodeValues;
constructor function "RIB_StrInvCriVo_REC"
(
  rib_oid number
, item_id_col "RIB_item_id_col_TBL"
, store_id_col "RIB_store_id_col_TBL"
, uom_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id_col := item_id_col;
self.store_id_col := store_id_col;
self.uom_type := uom_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_item_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_item_id_col_TBL" AS TABLE OF varchar2(25);
/
DROP TYPE "RIB_store_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_store_id_col_TBL" AS TABLE OF number(10);
/
