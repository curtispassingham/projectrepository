DROP TYPE "RIB_StrShpRsnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrShpRsnDesc_REC";
/
DROP TYPE "RIB_StrShpRsnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrShpRsnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrShpRsnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  reason_id number(12),
  reason_code varchar2(6),
  description varchar2(120),
  location_type varchar2(20), -- location_type is enumeration field, valid values are [STORE, SUPPLIER, WAREHOUSE, FINISHER, CUSTOMER, UNKNOWN] (all lower-case)
  nonsellable_qty_type_id number(12),
  use_available varchar2(5), --use_available is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrShpRsnDesc_REC"
(
  rib_oid number
, reason_id number
, reason_code varchar2
, description varchar2
, location_type varchar2
, nonsellable_qty_type_id number
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrShpRsnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrShpRsnDesc') := "ns_name_StrShpRsnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_code') := reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'location_type') := location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsellable_qty_type_id') := nonsellable_qty_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'use_available') := use_available;
END appendNodeValues;
constructor function "RIB_StrShpRsnDesc_REC"
(
  rib_oid number
, reason_id number
, reason_code varchar2
, description varchar2
, location_type varchar2
, nonsellable_qty_type_id number
, use_available varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.reason_id := reason_id;
self.reason_code := reason_code;
self.description := description;
self.location_type := location_type;
self.nonsellable_qty_type_id := nonsellable_qty_type_id;
self.use_available := use_available;
RETURN;
end;
END;
/
