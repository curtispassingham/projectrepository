@@EOfBrXStoreDesc.sql;
/
DROP TYPE "RIB_BrXStoreDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXStoreDesc_REC";
/
DROP TYPE "RIB_EOfBrXStoreDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_EOfBrXStoreDesc_TBL" AS TABLE OF "RIB_EOfBrXStoreDesc_REC";
/
DROP TYPE "RIB_BrXStoreDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrXStoreDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrXStoreDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  taxpayer_type varchar2(1),
  addr_1 varchar2(240),
  addr_2 varchar2(240),
  addr_3 varchar2(240),
  neighborhood varchar2(240),
  jurisdiction_code varchar2(10),
  state varchar2(5),
  country varchar2(3),
  postal_code varchar2(30),
  cpf varchar2(11),
  cnpj varchar2(14),
  nit varchar2(250),
  suframa varchar2(250),
  im varchar2(250),
  ie varchar2(250),
  iss_contrib_ind varchar2(1),
  rural_prod_ind varchar2(1),
  ipi_ind varchar2(1),
  icms_contrib_ind varchar2(1),
  pis_contrib_ind varchar2(1),
  cofins_contrib_ind varchar2(1),
  match_opr_type varchar2(6),
  control_rec_st_ind varchar2(1),
  EOfBrXStoreDesc_TBL "RIB_EOfBrXStoreDesc_TBL",   -- Size of "RIB_EOfBrXStoreDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrXStoreDesc_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ind varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, match_opr_type varchar2
, control_rec_st_ind varchar2
) return self as result
,constructor function "RIB_BrXStoreDesc_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ind varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, match_opr_type varchar2
, control_rec_st_ind varchar2
, EOfBrXStoreDesc_TBL "RIB_EOfBrXStoreDesc_TBL"  -- Size of "RIB_EOfBrXStoreDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrXStoreDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrXStoreDesc') := "ns_name_BrXStoreDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'taxpayer_type') := taxpayer_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_1') := addr_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_2') := addr_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_3') := addr_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'neighborhood') := neighborhood;
  rib_obj_util.g_RIB_element_values(i_prefix||'jurisdiction_code') := jurisdiction_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country') := country;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'cpf') := cpf;
  rib_obj_util.g_RIB_element_values(i_prefix||'cnpj') := cnpj;
  rib_obj_util.g_RIB_element_values(i_prefix||'nit') := nit;
  rib_obj_util.g_RIB_element_values(i_prefix||'suframa') := suframa;
  rib_obj_util.g_RIB_element_values(i_prefix||'im') := im;
  rib_obj_util.g_RIB_element_values(i_prefix||'ie') := ie;
  rib_obj_util.g_RIB_element_values(i_prefix||'iss_contrib_ind') := iss_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'rural_prod_ind') := rural_prod_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_ind') := ipi_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_contrib_ind') := icms_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_contrib_ind') := pis_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_contrib_ind') := cofins_contrib_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'match_opr_type') := match_opr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'control_rec_st_ind') := control_rec_st_ind;
  IF EOfBrXStoreDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'EOfBrXStoreDesc_TBL.';
    FOR INDX IN EOfBrXStoreDesc_TBL.FIRST()..EOfBrXStoreDesc_TBL.LAST() LOOP
      EOfBrXStoreDesc_TBL(indx).appendNodeValues( i_prefix||indx||'EOfBrXStoreDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_BrXStoreDesc_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ind varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, match_opr_type varchar2
, control_rec_st_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ind := icms_contrib_ind;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.match_opr_type := match_opr_type;
self.control_rec_st_ind := control_rec_st_ind;
RETURN;
end;
constructor function "RIB_BrXStoreDesc_REC"
(
  rib_oid number
, taxpayer_type varchar2
, addr_1 varchar2
, addr_2 varchar2
, addr_3 varchar2
, neighborhood varchar2
, jurisdiction_code varchar2
, state varchar2
, country varchar2
, postal_code varchar2
, cpf varchar2
, cnpj varchar2
, nit varchar2
, suframa varchar2
, im varchar2
, ie varchar2
, iss_contrib_ind varchar2
, rural_prod_ind varchar2
, ipi_ind varchar2
, icms_contrib_ind varchar2
, pis_contrib_ind varchar2
, cofins_contrib_ind varchar2
, match_opr_type varchar2
, control_rec_st_ind varchar2
, EOfBrXStoreDesc_TBL "RIB_EOfBrXStoreDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.taxpayer_type := taxpayer_type;
self.addr_1 := addr_1;
self.addr_2 := addr_2;
self.addr_3 := addr_3;
self.neighborhood := neighborhood;
self.jurisdiction_code := jurisdiction_code;
self.state := state;
self.country := country;
self.postal_code := postal_code;
self.cpf := cpf;
self.cnpj := cnpj;
self.nit := nit;
self.suframa := suframa;
self.im := im;
self.ie := ie;
self.iss_contrib_ind := iss_contrib_ind;
self.rural_prod_ind := rural_prod_ind;
self.ipi_ind := ipi_ind;
self.icms_contrib_ind := icms_contrib_ind;
self.pis_contrib_ind := pis_contrib_ind;
self.cofins_contrib_ind := cofins_contrib_ind;
self.match_opr_type := match_opr_type;
self.control_rec_st_ind := control_rec_st_ind;
self.EOfBrXStoreDesc_TBL := EOfBrXStoreDesc_TBL;
RETURN;
end;
END;
/
