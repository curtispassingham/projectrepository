DROP TYPE "RIB_CustPurchItmHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustPurchItmHdrDesc_REC";
/
DROP TYPE "RIB_CustPurchItmHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustPurchItmHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustPurchItmHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  sequence_no number(4),
  item_id varchar2(25),
  item_description varchar2(250),
  quantity number(12,4),
  returned_quantity number(12,4),
  cancelled_quantity number(12,4),
  return_eligible_quantity number(12,4),
  unit_of_measure varchar2(4),
  currency_code varchar2(3),
  extended_sell_price number(20,4),
  discount_total number(20,4),
  last_modified_date date,
  delivery_date date,
  return_item_flag varchar2(5), --return_item_flag is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustPurchItmHdrDesc_REC"
(
  rib_oid number
, sequence_no number
, item_id varchar2
, item_description varchar2
, quantity number
, returned_quantity number
, cancelled_quantity number
, return_eligible_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, extended_sell_price number
, discount_total number
, last_modified_date date
, delivery_date date
, return_item_flag varchar2  --return_item_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustPurchItmHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustPurchItmHdrDesc') := "ns_name_CustPurchItmHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'sequence_no') := sequence_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_quantity') := returned_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_quantity') := cancelled_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_eligible_quantity') := return_eligible_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'extended_sell_price') := extended_sell_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_total') := discount_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_modified_date') := last_modified_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_item_flag') := return_item_flag;
END appendNodeValues;
constructor function "RIB_CustPurchItmHdrDesc_REC"
(
  rib_oid number
, sequence_no number
, item_id varchar2
, item_description varchar2
, quantity number
, returned_quantity number
, cancelled_quantity number
, return_eligible_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, extended_sell_price number
, discount_total number
, last_modified_date date
, delivery_date date
, return_item_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sequence_no := sequence_no;
self.item_id := item_id;
self.item_description := item_description;
self.quantity := quantity;
self.returned_quantity := returned_quantity;
self.cancelled_quantity := cancelled_quantity;
self.return_eligible_quantity := return_eligible_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.extended_sell_price := extended_sell_price;
self.discount_total := discount_total;
self.last_modified_date := last_modified_date;
self.delivery_date := delivery_date;
self.return_item_flag := return_item_flag;
RETURN;
end;
END;
/
