DROP TYPE "RIB_ShlfRplHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplHdrDesc_REC";
/
DROP TYPE "RIB_ShlfRplHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  replenishment_id number(15),
  store_id number(10),
  description varchar2(128),
  user_name varchar2(128),
  create_date date,
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETE, CANCELED, PENDING_ALTERED, UNKNOWN] (all lower-case)
  type varchar2(20), -- type is enumeration field, valid values are [CAPACITY, SALES, AD_HOC, DISPLAY, UNKNOWN] (all lower-case)
  replenishment_mode varchar2(20), -- replenishment_mode is enumeration field, valid values are [END_OF_DAY, WITHIN_DAY, UNKNOWN] (all lower-case)
  replenish_quantity number(20,4),
  actual_quantity number(20,4),
  number_of_line_items number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplHdrDesc_REC"
(
  rib_oid number
, replenishment_id number
, store_id number
, description varchar2
, user_name varchar2
, create_date date
, status varchar2
, type varchar2
, replenishment_mode varchar2
, replenish_quantity number
, actual_quantity number
, number_of_line_items number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplHdrDesc') := "ns_name_ShlfRplHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_id') := replenishment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_mode') := replenishment_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenish_quantity') := replenish_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'actual_quantity') := actual_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_line_items') := number_of_line_items;
END appendNodeValues;
constructor function "RIB_ShlfRplHdrDesc_REC"
(
  rib_oid number
, replenishment_id number
, store_id number
, description varchar2
, user_name varchar2
, create_date date
, status varchar2
, type varchar2
, replenishment_mode varchar2
, replenish_quantity number
, actual_quantity number
, number_of_line_items number
) return self as result is
begin
self.rib_oid := rib_oid;
self.replenishment_id := replenishment_id;
self.store_id := store_id;
self.description := description;
self.user_name := user_name;
self.create_date := create_date;
self.status := status;
self.type := type;
self.replenishment_mode := replenishment_mode;
self.replenish_quantity := replenish_quantity;
self.actual_quantity := actual_quantity;
self.number_of_line_items := number_of_line_items;
RETURN;
end;
END;
/
