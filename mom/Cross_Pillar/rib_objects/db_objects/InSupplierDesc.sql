@@EOfInSupplierDesc.sql;
/
DROP TYPE "RIB_InSupplierDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierDesc_REC";
/
DROP TYPE "RIB_InSupAttr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupAttr_REC";
/
DROP TYPE "RIB_InSupSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupSite_REC";
/
DROP TYPE "RIB_InSupSiteOrgUnit_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupSiteOrgUnit_REC";
/
DROP TYPE "RIB_InSupSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupSiteAddr_REC";
/
DROP TYPE "RIB_InAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InAddr_REC";
/
DROP TYPE "RIB_InSupplierDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupplierDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupplierDesc "RIB_EOfInSupplierDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupplierDesc_REC"
(
  rib_oid number
, EOfInSupplierDesc "RIB_EOfInSupplierDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupplierDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierDesc') := "ns_name_InSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'EOfInSupplierDesc.';
  EOfInSupplierDesc.appendNodeValues( i_prefix||'EOfInSupplierDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupplierDesc_REC"
(
  rib_oid number
, EOfInSupplierDesc "RIB_EOfInSupplierDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupplierDesc := EOfInSupplierDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupAttr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupAttr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupAttr "RIB_EOfInSupAttr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupAttr_REC"
(
  rib_oid number
, EOfInSupAttr "RIB_EOfInSupAttr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupAttr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierDesc') := "ns_name_InSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInSupAttr.';
  EOfInSupAttr.appendNodeValues( i_prefix||'EOfInSupAttr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupAttr_REC"
(
  rib_oid number
, EOfInSupAttr "RIB_EOfInSupAttr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupAttr := EOfInSupAttr;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupSite "RIB_EOfInSupSite_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupSite_REC"
(
  rib_oid number
, EOfInSupSite "RIB_EOfInSupSite_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierDesc') := "ns_name_InSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInSupSite.';
  EOfInSupSite.appendNodeValues( i_prefix||'EOfInSupSite');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupSite_REC"
(
  rib_oid number
, EOfInSupSite "RIB_EOfInSupSite_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupSite := EOfInSupSite;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupSiteOrgUnit_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupSiteOrgUnit_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupSiteOrgUnit "RIB_EOfInSupSiteOrgUnit_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupSiteOrgUnit_REC"
(
  rib_oid number
, EOfInSupSiteOrgUnit "RIB_EOfInSupSiteOrgUnit_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupSiteOrgUnit_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierDesc') := "ns_name_InSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInSupSiteOrgUnit.';
  EOfInSupSiteOrgUnit.appendNodeValues( i_prefix||'EOfInSupSiteOrgUnit');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupSiteOrgUnit_REC"
(
  rib_oid number
, EOfInSupSiteOrgUnit "RIB_EOfInSupSiteOrgUnit_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupSiteOrgUnit := EOfInSupSiteOrgUnit;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InSupSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInSupSiteAddr "RIB_EOfInSupSiteAddr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InSupSiteAddr_REC"
(
  rib_oid number
, EOfInSupSiteAddr "RIB_EOfInSupSiteAddr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InSupSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierDesc') := "ns_name_InSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInSupSiteAddr.';
  EOfInSupSiteAddr.appendNodeValues( i_prefix||'EOfInSupSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InSupSiteAddr_REC"
(
  rib_oid number
, EOfInSupSiteAddr "RIB_EOfInSupSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInSupSiteAddr := EOfInSupSiteAddr;
RETURN;
end;
END;
/
DROP TYPE "RIB_InAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InSupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfInAddr "RIB_EOfInAddr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InAddr_REC"
(
  rib_oid number
, EOfInAddr "RIB_EOfInAddr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InSupplierDesc') := "ns_name_InSupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfInAddr.';
  EOfInAddr.appendNodeValues( i_prefix||'EOfInAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InAddr_REC"
(
  rib_oid number
, EOfInAddr "RIB_EOfInAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfInAddr := EOfInAddr;
RETURN;
end;
END;
/
