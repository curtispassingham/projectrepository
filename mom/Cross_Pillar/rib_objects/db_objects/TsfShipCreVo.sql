DROP TYPE "RIB_TsfShipCreVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCreVo_REC";
/
DROP TYPE "RIB_TsfShipCreVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCreVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCreVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  destination_type varchar2(20), -- destination_type is enumeration field, valid values are [FINISHER, STORE, WAREHOUSE, UNKNOWN] (all lower-case)
  destination_id number(10),
  authorization_code varchar2(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCreVo_REC"
(
  rib_oid number
, store_id number
, destination_type varchar2
, destination_id number
) return self as result
,constructor function "RIB_TsfShipCreVo_REC"
(
  rib_oid number
, store_id number
, destination_type varchar2
, destination_id number
, authorization_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCreVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCreVo') := "ns_name_TsfShipCreVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_type') := destination_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_id') := destination_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
END appendNodeValues;
constructor function "RIB_TsfShipCreVo_REC"
(
  rib_oid number
, store_id number
, destination_type varchar2
, destination_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.destination_type := destination_type;
self.destination_id := destination_id;
RETURN;
end;
constructor function "RIB_TsfShipCreVo_REC"
(
  rib_oid number
, store_id number
, destination_type varchar2
, destination_id number
, authorization_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.destination_type := destination_type;
self.destination_id := destination_id;
self.authorization_code := authorization_code;
RETURN;
end;
END;
/
