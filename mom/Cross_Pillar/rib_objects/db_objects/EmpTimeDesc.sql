DROP TYPE "RIB_EmpTimeDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_EmpTimeDesc_REC";
/
DROP TYPE "RIB_EmpTimeDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_EmpTimeDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_EmpTimeDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  employee_id varchar2(10),
  clock_time date,
  punch_time date,
  to_location varchar2(10),
  from_location varchar2(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
) return self as result
,constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
) return self as result
,constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
, punch_time date
) return self as result
,constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
, punch_time date
, to_location varchar2
) return self as result
,constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
, punch_time date
, to_location varchar2
, from_location varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_EmpTimeDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_EmpTimeDesc') := "ns_name_EmpTimeDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'employee_id') := employee_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'clock_time') := clock_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'punch_time') := punch_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
END appendNodeValues;
constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.employee_id := employee_id;
RETURN;
end;
constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.employee_id := employee_id;
self.clock_time := clock_time;
RETURN;
end;
constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
, punch_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.employee_id := employee_id;
self.clock_time := clock_time;
self.punch_time := punch_time;
RETURN;
end;
constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
, punch_time date
, to_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.employee_id := employee_id;
self.clock_time := clock_time;
self.punch_time := punch_time;
self.to_location := to_location;
RETURN;
end;
constructor function "RIB_EmpTimeDesc_REC"
(
  rib_oid number
, employee_id varchar2
, clock_time date
, punch_time date
, to_location varchar2
, from_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.employee_id := employee_id;
self.clock_time := clock_time;
self.punch_time := punch_time;
self.to_location := to_location;
self.from_location := from_location;
RETURN;
end;
END;
/
