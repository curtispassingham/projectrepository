DROP TYPE "RIB_ProdGrpHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpHdrDesc_REC";
/
DROP TYPE "RIB_ProdGrpHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProdGrpHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  product_group_id number(10),
  group_type varchar2(40), -- group_type is enumeration field, valid values are [STOCK_COUNT_UNIT, STOCK_COUNT_UNIT_AMOUNT, STOCK_COUNT_PROBLEM_LINE, STOCK_COUNT_WASTAGE, STOCK_COUNT_RMS_SYNC, SHELF_REPLENISHMENT, ITEM_REQUEST, AUTO_TICKET_PRINT, UNKNOWN, NO_VALUE] (all lower-case)
  description varchar2(250),
  store_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProdGrpHdrDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
) return self as result
,constructor function "RIB_ProdGrpHdrDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProdGrpHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpHdrDesc') := "ns_name_ProdGrpHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_type') := group_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
END appendNodeValues;
constructor function "RIB_ProdGrpHdrDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
RETURN;
end;
constructor function "RIB_ProdGrpHdrDesc_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
RETURN;
end;
END;
/
