@@GeoAddrDesc.sql;
/
@@ShipMethodColDesc.sql;
/
DROP TYPE "RIB_ShipOptionDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShipOptionDesc_REC";
/
DROP TYPE "RIB_ItemAssignments_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemAssignments_REC";
/
DROP TYPE "RIB_ShipOptionDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShipOptionDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipOptionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(4),
  ship_loc_type varchar2(1), -- ship_loc_type is enumeration field, valid values are [S, C] (all lower-case)
  ship_loc_id varchar2(5),
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  ShipMethodColDesc "RIB_ShipMethodColDesc_REC",
  ItemAssignments "RIB_ItemAssignments_REC",
  est_ship_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShipOptionDesc_REC"
(
  rib_oid number
, seq_no number
, ship_loc_type varchar2
, ship_loc_id varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, ShipMethodColDesc "RIB_ShipMethodColDesc_REC"
, ItemAssignments "RIB_ItemAssignments_REC"
, est_ship_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShipOptionDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipOptionDesc') := "ns_name_ShipOptionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_loc_type') := ship_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_loc_id') := ship_loc_id;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ShipMethodColDesc.';
  ShipMethodColDesc.appendNodeValues( i_prefix||'ShipMethodColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ItemAssignments.';
  ItemAssignments.appendNodeValues( i_prefix||'ItemAssignments');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'est_ship_date') := est_ship_date;
END appendNodeValues;
constructor function "RIB_ShipOptionDesc_REC"
(
  rib_oid number
, seq_no number
, ship_loc_type varchar2
, ship_loc_id varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, ShipMethodColDesc "RIB_ShipMethodColDesc_REC"
, ItemAssignments "RIB_ItemAssignments_REC"
, est_ship_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.ship_loc_type := ship_loc_type;
self.ship_loc_id := ship_loc_id;
self.GeoAddrDesc := GeoAddrDesc;
self.ShipMethodColDesc := ShipMethodColDesc;
self.ItemAssignments := ItemAssignments;
self.est_ship_date := est_ship_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemAssignments_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemAssignments_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipOptionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no "RIB_seq_no_TBL",   -- Size of "RIB_seq_no_TBL" is unbounded
  collection_size number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemAssignments_REC"
(
  rib_oid number
, seq_no "RIB_seq_no_TBL"  -- Size of "RIB_seq_no_TBL" is unbounded
, collection_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemAssignments_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipOptionDesc') := "ns_name_ShipOptionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF seq_no IS NOT NULL THEN
    FOR INDX IN seq_no.FIRST()..seq_no.LAST() LOOP
      l_new_pre :=i_prefix||indx||'seq_no'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'seq_no'||'.'):=seq_no(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'collection_size') := collection_size;
END appendNodeValues;
constructor function "RIB_ItemAssignments_REC"
(
  rib_oid number
, seq_no "RIB_seq_no_TBL"
, collection_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.collection_size := collection_size;
RETURN;
end;
END;
/
DROP TYPE "RIB_seq_no_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_seq_no_TBL" AS TABLE OF number(4);
/
