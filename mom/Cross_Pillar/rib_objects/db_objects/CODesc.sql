DROP TYPE "RIB_COHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_COHdrDesc_REC";
/
DROP TYPE "RIB_CODtlDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CODtlDesc_REC";
/
DROP TYPE "RIB_CODesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CODesc_REC";
/
DROP TYPE "RIB_COHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_COHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CODesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ship_request_id number(10),
  document_type varchar2(1),
  physical_warehouse_id number(10),
  creation_date date,
  order_number varchar2(20),
  base_code varchar2(4),
  carrier_code varchar2(4),
  carrier_service_type_code varchar2(6),
  route varchar2(10),
  ship_to_full_name varchar2(30),
  ship_to_street_address varchar2(30),
  ship_to_address_line_2 varchar2(30),
  ship_to_address_line_3 varchar2(30),
  ship_to_city varchar2(120),
  ship_to_state_province varchar2(3),
  ship_to_postal_code varchar2(30),
  ship_to_country_code varchar2(3),
  bill_to_full_name varchar2(30),
  bill_to_street_address varchar2(30),
  bill_to_address_line_2 varchar2(30),
  bill_to_address_line_3 varchar2(30),
  bill_to_city varchar2(25),
  bill_to_state_province varchar2(3),
  bill_to_postal_code varchar2(10),
  bill_to_country_code varchar2(3),
  order_total number(12),
  taxable_total number(12),
  pick_not_before_date date,
  pick_not_after_date date,
  calculated_shipping_amount number(12),
  order_type varchar2(9),
  pick_complete varchar2(1),
  consumer_direct_flag varchar2(1),
  rollback_allocation varchar2(1),
  cartonization_flag varchar2(1),
  order_line_cost varchar2(1),
  break_by_distro varchar2(1),
  message varchar2(300),
  chute_type varchar2(6),
  bill_address4 varchar2(240),
  bill_address5 varchar2(240),
  ship_address4 varchar2(240),
  ship_address5 varchar2(240),
  amount1 number(16),
  amount2 number(16),
  amount3 number(16),
  amount4 number(16),
  amount5 number(16),
  amount6 number(16),
  po_nbr varchar2(12),
  event_code varchar2(6),
  event_description varchar2(1000),
  direct_ship_ind varchar2(1),
  gift_certificate_flag varchar2(1),
  ship_to_day_phone varchar2(30),
  ship_to_evening_phone varchar2(30),
  shipping_instructions varchar2(500),
  picking_instructions varchar2(500),
  physical_warehouse_id_type varchar2(4000),
  underpayment_amt number(18),
  banner_id number(4),
  banner_name varchar2(120),
  accom_total number(18),
  promotions_total number(18),
  sh_standard_amt number(18),
  sh_rush_amt number(18),
  sh_additional_amt number(18),
  vas_total number(18),
  tax_amt number(18),
  ret_merch_amt number(18),
  ret_vas_amt number(18),
  ret_sh_amt number(18),
  ret_tax_amt number(18),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_COHdrDesc_REC"
(
  rib_oid number
, ship_request_id number
, document_type varchar2
, physical_warehouse_id number
, creation_date date
, order_number varchar2
, base_code varchar2
, carrier_code varchar2
, carrier_service_type_code varchar2
, route varchar2
, ship_to_full_name varchar2
, ship_to_street_address varchar2
, ship_to_address_line_2 varchar2
, ship_to_address_line_3 varchar2
, ship_to_city varchar2
, ship_to_state_province varchar2
, ship_to_postal_code varchar2
, ship_to_country_code varchar2
, bill_to_full_name varchar2
, bill_to_street_address varchar2
, bill_to_address_line_2 varchar2
, bill_to_address_line_3 varchar2
, bill_to_city varchar2
, bill_to_state_province varchar2
, bill_to_postal_code varchar2
, bill_to_country_code varchar2
, order_total number
, taxable_total number
, pick_not_before_date date
, pick_not_after_date date
, calculated_shipping_amount number
, order_type varchar2
, pick_complete varchar2
, consumer_direct_flag varchar2
, rollback_allocation varchar2
, cartonization_flag varchar2
, order_line_cost varchar2
, break_by_distro varchar2
, message varchar2
, chute_type varchar2
, bill_address4 varchar2
, bill_address5 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, po_nbr varchar2
, event_code varchar2
, event_description varchar2
, direct_ship_ind varchar2
, gift_certificate_flag varchar2
, ship_to_day_phone varchar2
, ship_to_evening_phone varchar2
, shipping_instructions varchar2
, picking_instructions varchar2
, physical_warehouse_id_type varchar2
, underpayment_amt number
, banner_id number
, banner_name varchar2
, accom_total number
, promotions_total number
, sh_standard_amt number
, sh_rush_amt number
, sh_additional_amt number
, vas_total number
, tax_amt number
, ret_merch_amt number
, ret_vas_amt number
, ret_sh_amt number
, ret_tax_amt number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_COHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CODesc') := "ns_name_CODesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_request_id') := ship_request_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_warehouse_id') := physical_warehouse_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'creation_date') := creation_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_number') := order_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'base_code') := base_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_type_code') := carrier_service_type_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'route') := route;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_full_name') := ship_to_full_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_street_address') := ship_to_street_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_address_line_2') := ship_to_address_line_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_address_line_3') := ship_to_address_line_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_city') := ship_to_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_state_province') := ship_to_state_province;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_postal_code') := ship_to_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_country_code') := ship_to_country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_full_name') := bill_to_full_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_street_address') := bill_to_street_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_address_line_2') := bill_to_address_line_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_address_line_3') := bill_to_address_line_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_city') := bill_to_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_state_province') := bill_to_state_province;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_postal_code') := bill_to_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_to_country_code') := bill_to_country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_total') := order_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxable_total') := taxable_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_not_before_date') := pick_not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_not_after_date') := pick_not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'calculated_shipping_amount') := calculated_shipping_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_complete') := pick_complete;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_direct_flag') := consumer_direct_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'rollback_allocation') := rollback_allocation;
  rib_obj_util.g_RIB_element_values(i_prefix||'cartonization_flag') := cartonization_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_cost') := order_line_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'break_by_distro') := break_by_distro;
  rib_obj_util.g_RIB_element_values(i_prefix||'message') := message;
  rib_obj_util.g_RIB_element_values(i_prefix||'chute_type') := chute_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_address4') := bill_address4;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_address5') := bill_address5;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address4') := ship_address4;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address5') := ship_address5;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount1') := amount1;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount2') := amount2;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount3') := amount3;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount4') := amount4;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount5') := amount5;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount6') := amount6;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_code') := event_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_description') := event_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'direct_ship_ind') := direct_ship_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_certificate_flag') := gift_certificate_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_day_phone') := ship_to_day_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_evening_phone') := ship_to_evening_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_instructions') := shipping_instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'picking_instructions') := picking_instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_warehouse_id_type') := physical_warehouse_id_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'underpayment_amt') := underpayment_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'banner_id') := banner_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'banner_name') := banner_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'accom_total') := accom_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotions_total') := promotions_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'sh_standard_amt') := sh_standard_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'sh_rush_amt') := sh_rush_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'sh_additional_amt') := sh_additional_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'vas_total') := vas_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_amt') := tax_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_merch_amt') := ret_merch_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_vas_amt') := ret_vas_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_sh_amt') := ret_sh_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_tax_amt') := ret_tax_amt;
END appendNodeValues;
constructor function "RIB_COHdrDesc_REC"
(
  rib_oid number
, ship_request_id number
, document_type varchar2
, physical_warehouse_id number
, creation_date date
, order_number varchar2
, base_code varchar2
, carrier_code varchar2
, carrier_service_type_code varchar2
, route varchar2
, ship_to_full_name varchar2
, ship_to_street_address varchar2
, ship_to_address_line_2 varchar2
, ship_to_address_line_3 varchar2
, ship_to_city varchar2
, ship_to_state_province varchar2
, ship_to_postal_code varchar2
, ship_to_country_code varchar2
, bill_to_full_name varchar2
, bill_to_street_address varchar2
, bill_to_address_line_2 varchar2
, bill_to_address_line_3 varchar2
, bill_to_city varchar2
, bill_to_state_province varchar2
, bill_to_postal_code varchar2
, bill_to_country_code varchar2
, order_total number
, taxable_total number
, pick_not_before_date date
, pick_not_after_date date
, calculated_shipping_amount number
, order_type varchar2
, pick_complete varchar2
, consumer_direct_flag varchar2
, rollback_allocation varchar2
, cartonization_flag varchar2
, order_line_cost varchar2
, break_by_distro varchar2
, message varchar2
, chute_type varchar2
, bill_address4 varchar2
, bill_address5 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, po_nbr varchar2
, event_code varchar2
, event_description varchar2
, direct_ship_ind varchar2
, gift_certificate_flag varchar2
, ship_to_day_phone varchar2
, ship_to_evening_phone varchar2
, shipping_instructions varchar2
, picking_instructions varchar2
, physical_warehouse_id_type varchar2
, underpayment_amt number
, banner_id number
, banner_name varchar2
, accom_total number
, promotions_total number
, sh_standard_amt number
, sh_rush_amt number
, sh_additional_amt number
, vas_total number
, tax_amt number
, ret_merch_amt number
, ret_vas_amt number
, ret_sh_amt number
, ret_tax_amt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.document_type := document_type;
self.physical_warehouse_id := physical_warehouse_id;
self.creation_date := creation_date;
self.order_number := order_number;
self.base_code := base_code;
self.carrier_code := carrier_code;
self.carrier_service_type_code := carrier_service_type_code;
self.route := route;
self.ship_to_full_name := ship_to_full_name;
self.ship_to_street_address := ship_to_street_address;
self.ship_to_address_line_2 := ship_to_address_line_2;
self.ship_to_address_line_3 := ship_to_address_line_3;
self.ship_to_city := ship_to_city;
self.ship_to_state_province := ship_to_state_province;
self.ship_to_postal_code := ship_to_postal_code;
self.ship_to_country_code := ship_to_country_code;
self.bill_to_full_name := bill_to_full_name;
self.bill_to_street_address := bill_to_street_address;
self.bill_to_address_line_2 := bill_to_address_line_2;
self.bill_to_address_line_3 := bill_to_address_line_3;
self.bill_to_city := bill_to_city;
self.bill_to_state_province := bill_to_state_province;
self.bill_to_postal_code := bill_to_postal_code;
self.bill_to_country_code := bill_to_country_code;
self.order_total := order_total;
self.taxable_total := taxable_total;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.calculated_shipping_amount := calculated_shipping_amount;
self.order_type := order_type;
self.pick_complete := pick_complete;
self.consumer_direct_flag := consumer_direct_flag;
self.rollback_allocation := rollback_allocation;
self.cartonization_flag := cartonization_flag;
self.order_line_cost := order_line_cost;
self.break_by_distro := break_by_distro;
self.message := message;
self.chute_type := chute_type;
self.bill_address4 := bill_address4;
self.bill_address5 := bill_address5;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.po_nbr := po_nbr;
self.event_code := event_code;
self.event_description := event_description;
self.direct_ship_ind := direct_ship_ind;
self.gift_certificate_flag := gift_certificate_flag;
self.ship_to_day_phone := ship_to_day_phone;
self.ship_to_evening_phone := ship_to_evening_phone;
self.shipping_instructions := shipping_instructions;
self.picking_instructions := picking_instructions;
self.physical_warehouse_id_type := physical_warehouse_id_type;
self.underpayment_amt := underpayment_amt;
self.banner_id := banner_id;
self.banner_name := banner_name;
self.accom_total := accom_total;
self.promotions_total := promotions_total;
self.sh_standard_amt := sh_standard_amt;
self.sh_rush_amt := sh_rush_amt;
self.sh_additional_amt := sh_additional_amt;
self.vas_total := vas_total;
self.tax_amt := tax_amt;
self.ret_merch_amt := ret_merch_amt;
self.ret_vas_amt := ret_vas_amt;
self.ret_sh_amt := ret_sh_amt;
self.ret_tax_amt := ret_tax_amt;
RETURN;
end;
END;
/
DROP TYPE "RIB_CODtlDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CODtlDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CODesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ship_request_id number(10),
  rdm_dest_id number(10),
  item_number varchar2(25),
  requested_quantity number(9),
  unit_selling_price number(20),
  unit_tax_percent number(12),
  item_status varchar2(1),
  order_line_nbr number(4),
  amount1 number(16),
  amount2 number(16),
  amount3 number(16),
  amount4 number(16),
  amount5 number(16),
  amount6 number(16),
  picking_instructions varchar2(500),
  voucher_to varchar2(55),
  voucher_from varchar2(55),
  voucher_message varchar2(100),
  estimated_ship_date date,
  selling_item_number varchar2(4000),
  selling_item_description varchar2(4000),
  special_hdlg_ind varchar2(4000),
  total_line_merch_amt number(18),
  tax_line_amt number(18),
  sh_additional_line_amt number(18),
  vas_line_amt number(18),
  prom_line_amt number(18),
  accom_line_amt number(18),
  estimated_delivery_date date,
  gift_ind number(1),
  directship_ind number(1),
  selling_sku_desc varchar2(100),
  gift_services_line_amt number(18),
  order_line_type varchar2(1),
  order_line_cancel_reason_id number(6),
  event_id number(10),
  event_desc varchar2(32),
  process_together varchar2(1),
  wrap_together varchar2(1),
  work_order_count number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
) return self as result
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
) return self as result
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
) return self as result
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
) return self as result
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
, process_together varchar2
) return self as result
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
, process_together varchar2
, wrap_together varchar2
) return self as result
,constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
, process_together varchar2
, wrap_together varchar2
, work_order_count number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CODtlDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CODesc') := "ns_name_CODesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_request_id') := ship_request_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'rdm_dest_id') := rdm_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_number') := item_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_quantity') := requested_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_selling_price') := unit_selling_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_tax_percent') := unit_tax_percent;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_status') := item_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_nbr') := order_line_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount1') := amount1;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount2') := amount2;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount3') := amount3;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount4') := amount4;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount5') := amount5;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount6') := amount6;
  rib_obj_util.g_RIB_element_values(i_prefix||'picking_instructions') := picking_instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'voucher_to') := voucher_to;
  rib_obj_util.g_RIB_element_values(i_prefix||'voucher_from') := voucher_from;
  rib_obj_util.g_RIB_element_values(i_prefix||'voucher_message') := voucher_message;
  rib_obj_util.g_RIB_element_values(i_prefix||'estimated_ship_date') := estimated_ship_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_item_number') := selling_item_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_item_description') := selling_item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'special_hdlg_ind') := special_hdlg_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_line_merch_amt') := total_line_merch_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_line_amt') := tax_line_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'sh_additional_line_amt') := sh_additional_line_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'vas_line_amt') := vas_line_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'prom_line_amt') := prom_line_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'accom_line_amt') := accom_line_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'estimated_delivery_date') := estimated_delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_ind') := gift_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'directship_ind') := directship_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_sku_desc') := selling_sku_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_services_line_amt') := gift_services_line_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_type') := order_line_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_cancel_reason_id') := order_line_cancel_reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_id') := event_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_desc') := event_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'process_together') := process_together;
  rib_obj_util.g_RIB_element_values(i_prefix||'wrap_together') := wrap_together;
  rib_obj_util.g_RIB_element_values(i_prefix||'work_order_count') := work_order_count;
END appendNodeValues;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
RETURN;
end;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
self.order_line_cancel_reason_id := order_line_cancel_reason_id;
RETURN;
end;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
self.order_line_cancel_reason_id := order_line_cancel_reason_id;
self.event_id := event_id;
RETURN;
end;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
self.order_line_cancel_reason_id := order_line_cancel_reason_id;
self.event_id := event_id;
self.event_desc := event_desc;
RETURN;
end;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
, process_together varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
self.order_line_cancel_reason_id := order_line_cancel_reason_id;
self.event_id := event_id;
self.event_desc := event_desc;
self.process_together := process_together;
RETURN;
end;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
, process_together varchar2
, wrap_together varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
self.order_line_cancel_reason_id := order_line_cancel_reason_id;
self.event_id := event_id;
self.event_desc := event_desc;
self.process_together := process_together;
self.wrap_together := wrap_together;
RETURN;
end;
constructor function "RIB_CODtlDesc_REC"
(
  rib_oid number
, ship_request_id number
, rdm_dest_id number
, item_number varchar2
, requested_quantity number
, unit_selling_price number
, unit_tax_percent number
, item_status varchar2
, order_line_nbr number
, amount1 number
, amount2 number
, amount3 number
, amount4 number
, amount5 number
, amount6 number
, picking_instructions varchar2
, voucher_to varchar2
, voucher_from varchar2
, voucher_message varchar2
, estimated_ship_date date
, selling_item_number varchar2
, selling_item_description varchar2
, special_hdlg_ind varchar2
, total_line_merch_amt number
, tax_line_amt number
, sh_additional_line_amt number
, vas_line_amt number
, prom_line_amt number
, accom_line_amt number
, estimated_delivery_date date
, gift_ind number
, directship_ind number
, selling_sku_desc varchar2
, gift_services_line_amt number
, order_line_type varchar2
, order_line_cancel_reason_id number
, event_id number
, event_desc varchar2
, process_together varchar2
, wrap_together varchar2
, work_order_count number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ship_request_id := ship_request_id;
self.rdm_dest_id := rdm_dest_id;
self.item_number := item_number;
self.requested_quantity := requested_quantity;
self.unit_selling_price := unit_selling_price;
self.unit_tax_percent := unit_tax_percent;
self.item_status := item_status;
self.order_line_nbr := order_line_nbr;
self.amount1 := amount1;
self.amount2 := amount2;
self.amount3 := amount3;
self.amount4 := amount4;
self.amount5 := amount5;
self.amount6 := amount6;
self.picking_instructions := picking_instructions;
self.voucher_to := voucher_to;
self.voucher_from := voucher_from;
self.voucher_message := voucher_message;
self.estimated_ship_date := estimated_ship_date;
self.selling_item_number := selling_item_number;
self.selling_item_description := selling_item_description;
self.special_hdlg_ind := special_hdlg_ind;
self.total_line_merch_amt := total_line_merch_amt;
self.tax_line_amt := tax_line_amt;
self.sh_additional_line_amt := sh_additional_line_amt;
self.vas_line_amt := vas_line_amt;
self.prom_line_amt := prom_line_amt;
self.accom_line_amt := accom_line_amt;
self.estimated_delivery_date := estimated_delivery_date;
self.gift_ind := gift_ind;
self.directship_ind := directship_ind;
self.selling_sku_desc := selling_sku_desc;
self.gift_services_line_amt := gift_services_line_amt;
self.order_line_type := order_line_type;
self.order_line_cancel_reason_id := order_line_cancel_reason_id;
self.event_id := event_id;
self.event_desc := event_desc;
self.process_together := process_together;
self.wrap_together := wrap_together;
self.work_order_count := work_order_count;
RETURN;
end;
END;
/
DROP TYPE "RIB_CODtlDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CODtlDesc_TBL" AS TABLE OF "RIB_CODtlDesc_REC";
/
DROP TYPE "RIB_CODesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CODesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CODesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  COHdrDesc "RIB_COHdrDesc_REC",
  CODtlDesc_TBL "RIB_CODtlDesc_TBL",   -- Size of "RIB_CODtlDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CODesc_REC"
(
  rib_oid number
, COHdrDesc "RIB_COHdrDesc_REC"
, CODtlDesc_TBL "RIB_CODtlDesc_TBL"  -- Size of "RIB_CODtlDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CODesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CODesc') := "ns_name_CODesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'COHdrDesc.';
  COHdrDesc.appendNodeValues( i_prefix||'COHdrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CODtlDesc_TBL.';
  FOR INDX IN CODtlDesc_TBL.FIRST()..CODtlDesc_TBL.LAST() LOOP
    CODtlDesc_TBL(indx).appendNodeValues( i_prefix||indx||'CODtlDesc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_CODesc_REC"
(
  rib_oid number
, COHdrDesc "RIB_COHdrDesc_REC"
, CODtlDesc_TBL "RIB_CODtlDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.COHdrDesc := COHdrDesc;
self.CODtlDesc_TBL := CODtlDesc_TBL;
RETURN;
end;
END;
/
