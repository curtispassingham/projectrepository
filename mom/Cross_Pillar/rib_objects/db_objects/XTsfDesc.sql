DROP TYPE "RIB_XTsfDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XTsfDtl_REC";
/
DROP TYPE "RIB_XTsfDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XTsfDesc_REC";
/
DROP TYPE "RIB_XTsfDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XTsfDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  tsf_qty number(12,4),
  supp_pack_size number(12,4),
  inv_status number(2),
  unit_cost number(20,4),
  adjustment_type varchar2(6),
  adjustment_value number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
) return self as result
,constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
) return self as result
,constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
) return self as result
,constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
, unit_cost number
) return self as result
,constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
, unit_cost number
, adjustment_type varchar2
) return self as result
,constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
, unit_cost number
, adjustment_type varchar2
, adjustment_value number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XTsfDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XTsfDesc') := "ns_name_XTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_qty') := tsf_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_pack_size') := supp_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_status') := inv_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'adjustment_type') := adjustment_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'adjustment_value') := adjustment_value;
END appendNodeValues;
constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
RETURN;
end;
constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.supp_pack_size := supp_pack_size;
RETURN;
end;
constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.supp_pack_size := supp_pack_size;
self.inv_status := inv_status;
RETURN;
end;
constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.supp_pack_size := supp_pack_size;
self.inv_status := inv_status;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
, unit_cost number
, adjustment_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.supp_pack_size := supp_pack_size;
self.inv_status := inv_status;
self.unit_cost := unit_cost;
self.adjustment_type := adjustment_type;
RETURN;
end;
constructor function "RIB_XTsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, supp_pack_size number
, inv_status number
, unit_cost number
, adjustment_type varchar2
, adjustment_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.supp_pack_size := supp_pack_size;
self.inv_status := inv_status;
self.unit_cost := unit_cost;
self.adjustment_type := adjustment_type;
self.adjustment_value := adjustment_value;
RETURN;
end;
END;
/
DROP TYPE "RIB_XTsfDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XTsfDtl_TBL" AS TABLE OF "RIB_XTsfDtl_REC";
/
DROP TYPE "RIB_XTsfDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XTsfDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_no number(12),
  from_loc_type varchar2(1),
  from_loc varchar2(10),
  to_loc_type varchar2(1),
  to_loc varchar2(10),
  delivery_date date,
  dept number(4),
  routing_code varchar2(1),
  freight_code varchar2(1),
  tsf_type varchar2(6),
  XTsfDtl_TBL "RIB_XTsfDtl_TBL",   -- Size of "RIB_XTsfDtl_TBL" is unbounded
  status varchar2(1),
  user_id varchar2(30),
  comment_desc varchar2(2000),
  context_type varchar2(6),
  context_value varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"  -- Size of "RIB_XTsfDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"  -- Size of "RIB_XTsfDtl_TBL" is unbounded
, status varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"  -- Size of "RIB_XTsfDtl_TBL" is unbounded
, status varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"  -- Size of "RIB_XTsfDtl_TBL" is unbounded
, status varchar2
, user_id varchar2
, comment_desc varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"  -- Size of "RIB_XTsfDtl_TBL" is unbounded
, status varchar2
, user_id varchar2
, comment_desc varchar2
, context_type varchar2
) return self as result
,constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"  -- Size of "RIB_XTsfDtl_TBL" is unbounded
, status varchar2
, user_id varchar2
, comment_desc varchar2
, context_type varchar2
, context_value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XTsfDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XTsfDesc') := "ns_name_XTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_no') := tsf_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc_type') := from_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc') := from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'routing_code') := routing_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_code') := freight_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_type') := tsf_type;
  IF XTsfDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XTsfDtl_TBL.';
    FOR INDX IN XTsfDtl_TBL.FIRST()..XTsfDtl_TBL.LAST() LOOP
      XTsfDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XTsfDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'comment_desc') := comment_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type') := context_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
END appendNodeValues;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
self.XTsfDtl_TBL := XTsfDtl_TBL;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
self.XTsfDtl_TBL := XTsfDtl_TBL;
self.status := status;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"
, status varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
self.XTsfDtl_TBL := XTsfDtl_TBL;
self.status := status;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"
, status varchar2
, user_id varchar2
, comment_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
self.XTsfDtl_TBL := XTsfDtl_TBL;
self.status := status;
self.user_id := user_id;
self.comment_desc := comment_desc;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"
, status varchar2
, user_id varchar2
, comment_desc varchar2
, context_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
self.XTsfDtl_TBL := XTsfDtl_TBL;
self.status := status;
self.user_id := user_id;
self.comment_desc := comment_desc;
self.context_type := context_type;
RETURN;
end;
constructor function "RIB_XTsfDesc_REC"
(
  rib_oid number
, tsf_no number
, from_loc_type varchar2
, from_loc varchar2
, to_loc_type varchar2
, to_loc varchar2
, delivery_date date
, dept number
, routing_code varchar2
, freight_code varchar2
, tsf_type varchar2
, XTsfDtl_TBL "RIB_XTsfDtl_TBL"
, status varchar2
, user_id varchar2
, comment_desc varchar2
, context_type varchar2
, context_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.from_loc_type := from_loc_type;
self.from_loc := from_loc;
self.to_loc_type := to_loc_type;
self.to_loc := to_loc;
self.delivery_date := delivery_date;
self.dept := dept;
self.routing_code := routing_code;
self.freight_code := freight_code;
self.tsf_type := tsf_type;
self.XTsfDtl_TBL := XTsfDtl_TBL;
self.status := status;
self.user_id := user_id;
self.comment_desc := comment_desc;
self.context_type := context_type;
self.context_value := context_value;
RETURN;
end;
END;
/
