DROP TYPE "RIB_UDADesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_UDADesc_REC";
/
DROP TYPE "RIB_UDADesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_UDADesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_UDADesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uda_id number(5),
  uda_desc varchar2(120),
  module varchar2(20),
  display_type varchar2(2),
  data_type varchar2(12),
  data_length number(3),
  single_value_ind varchar2(1),
  host_ind varchar2(1),
  carton_group varchar2(1),
  combinability varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
) return self as result
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
) return self as result
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
) return self as result
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
) return self as result
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
, host_ind varchar2
) return self as result
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
, host_ind varchar2
, carton_group varchar2
) return self as result
,constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
, host_ind varchar2
, carton_group varchar2
, combinability varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_UDADesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_UDADesc') := "ns_name_UDADesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_id') := uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_desc') := uda_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'module') := module;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_type') := display_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'data_type') := data_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'data_length') := data_length;
  rib_obj_util.g_RIB_element_values(i_prefix||'single_value_ind') := single_value_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'host_ind') := host_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_group') := carton_group;
  rib_obj_util.g_RIB_element_values(i_prefix||'combinability') := combinability;
END appendNodeValues;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
RETURN;
end;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
self.data_type := data_type;
RETURN;
end;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
self.data_type := data_type;
self.data_length := data_length;
RETURN;
end;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
self.data_type := data_type;
self.data_length := data_length;
self.single_value_ind := single_value_ind;
RETURN;
end;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
, host_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
self.data_type := data_type;
self.data_length := data_length;
self.single_value_ind := single_value_ind;
self.host_ind := host_ind;
RETURN;
end;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
, host_ind varchar2
, carton_group varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
self.data_type := data_type;
self.data_length := data_length;
self.single_value_ind := single_value_ind;
self.host_ind := host_ind;
self.carton_group := carton_group;
RETURN;
end;
constructor function "RIB_UDADesc_REC"
(
  rib_oid number
, uda_id number
, uda_desc varchar2
, module varchar2
, display_type varchar2
, data_type varchar2
, data_length number
, single_value_ind varchar2
, host_ind varchar2
, carton_group varchar2
, combinability varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_desc := uda_desc;
self.module := module;
self.display_type := display_type;
self.data_type := data_type;
self.data_length := data_length;
self.single_value_ind := single_value_ind;
self.host_ind := host_ind;
self.carton_group := carton_group;
self.combinability := combinability;
RETURN;
end;
END;
/
