DROP TYPE "RIB_ItPrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItPrDesc_REC";
/
DROP TYPE "RIB_ItPrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItPrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItPrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_price_id number(12),
  external_id number(15),
  store_id number(10),
  item_id varchar2(25),
  effective_date date,
  end_date date,
  price_type varchar2(20), -- price_type is enumeration field, valid values are [CLEARANCE, PROMOTIONAL, PERMANENT, NO_VALUE, UNKNOWN] (all lower-case)
  selling_unit_price_cur varchar2(3),
  selling_unit_price_value number(12,4),
  status varchar2(20), -- status is enumeration field, valid values are [APPROVED, PENDING, ACTIVE, COMPLETED, REJECTED, TICKET_LIST, NEW, DEFAULT, DELETED, NO_VALUE, UNKNOWN] (all lower-case)
  promotion_id number(10),
  promotion_component_id number(10),
  selling_uom varchar2(4),
  multi_units number(12,4),
  multi_unit_price_currency_code varchar2(3),
  multi_unit_price_value number(12,4),
  multi_unit_uom varchar2(4),
  multi_unit_change varchar2(5), --multi_unit_change is boolean field, valid values are true,false (all lower-case) 
  selling_unit_change varchar2(5), --selling_unit_change is boolean field, valid values are true,false (all lower-case) 
  promotion_name varchar2(160),
  promotion_description varchar2(640),
  promotion_comp_name varchar2(160),
  reset_clearance_id number(15),
  new_price_cur varchar2(3),
  new_price_value number(12,4),
  new_selling_uom varchar2(4),
  suggest_retail_cur varchar2(3),
  suggest_retail_value number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, selling_unit_change varchar2  --selling_unit_change is boolean field, valid values are true,false (all lower-case)
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
) return self as result
,constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, selling_unit_change varchar2  --selling_unit_change is boolean field, valid values are true,false (all lower-case)
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
) return self as result
,constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, selling_unit_change varchar2  --selling_unit_change is boolean field, valid values are true,false (all lower-case)
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, suggest_retail_cur varchar2
) return self as result
,constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, selling_unit_change varchar2  --selling_unit_change is boolean field, valid values are true,false (all lower-case)
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, suggest_retail_cur varchar2
, suggest_retail_value number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItPrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItPrDesc') := "ns_name_ItPrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_price_id') := item_price_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_type') := price_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_price_cur') := selling_unit_price_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_price_value') := selling_unit_price_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_component_id') := promotion_component_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_units') := multi_units;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_price_currency_code') := multi_unit_price_currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_price_value') := multi_unit_price_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_uom') := multi_unit_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_change') := multi_unit_change;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_change') := selling_unit_change;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_name') := promotion_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_description') := promotion_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_comp_name') := promotion_comp_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'reset_clearance_id') := reset_clearance_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_price_cur') := new_price_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_price_value') := new_price_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_selling_uom') := new_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggest_retail_cur') := suggest_retail_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggest_retail_value') := suggest_retail_value;
END appendNodeValues;
constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2
, selling_unit_change varchar2
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_price_id := item_price_id;
self.external_id := external_id;
self.store_id := store_id;
self.item_id := item_id;
self.effective_date := effective_date;
self.end_date := end_date;
self.price_type := price_type;
self.selling_unit_price_cur := selling_unit_price_cur;
self.selling_unit_price_value := selling_unit_price_value;
self.status := status;
self.promotion_id := promotion_id;
self.promotion_component_id := promotion_component_id;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_price_currency_code := multi_unit_price_currency_code;
self.multi_unit_price_value := multi_unit_price_value;
self.multi_unit_uom := multi_unit_uom;
self.multi_unit_change := multi_unit_change;
self.selling_unit_change := selling_unit_change;
self.promotion_name := promotion_name;
self.promotion_description := promotion_description;
self.promotion_comp_name := promotion_comp_name;
self.reset_clearance_id := reset_clearance_id;
self.new_price_cur := new_price_cur;
self.new_price_value := new_price_value;
RETURN;
end;
constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2
, selling_unit_change varchar2
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_price_id := item_price_id;
self.external_id := external_id;
self.store_id := store_id;
self.item_id := item_id;
self.effective_date := effective_date;
self.end_date := end_date;
self.price_type := price_type;
self.selling_unit_price_cur := selling_unit_price_cur;
self.selling_unit_price_value := selling_unit_price_value;
self.status := status;
self.promotion_id := promotion_id;
self.promotion_component_id := promotion_component_id;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_price_currency_code := multi_unit_price_currency_code;
self.multi_unit_price_value := multi_unit_price_value;
self.multi_unit_uom := multi_unit_uom;
self.multi_unit_change := multi_unit_change;
self.selling_unit_change := selling_unit_change;
self.promotion_name := promotion_name;
self.promotion_description := promotion_description;
self.promotion_comp_name := promotion_comp_name;
self.reset_clearance_id := reset_clearance_id;
self.new_price_cur := new_price_cur;
self.new_price_value := new_price_value;
self.new_selling_uom := new_selling_uom;
RETURN;
end;
constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2
, selling_unit_change varchar2
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, suggest_retail_cur varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_price_id := item_price_id;
self.external_id := external_id;
self.store_id := store_id;
self.item_id := item_id;
self.effective_date := effective_date;
self.end_date := end_date;
self.price_type := price_type;
self.selling_unit_price_cur := selling_unit_price_cur;
self.selling_unit_price_value := selling_unit_price_value;
self.status := status;
self.promotion_id := promotion_id;
self.promotion_component_id := promotion_component_id;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_price_currency_code := multi_unit_price_currency_code;
self.multi_unit_price_value := multi_unit_price_value;
self.multi_unit_uom := multi_unit_uom;
self.multi_unit_change := multi_unit_change;
self.selling_unit_change := selling_unit_change;
self.promotion_name := promotion_name;
self.promotion_description := promotion_description;
self.promotion_comp_name := promotion_comp_name;
self.reset_clearance_id := reset_clearance_id;
self.new_price_cur := new_price_cur;
self.new_price_value := new_price_value;
self.new_selling_uom := new_selling_uom;
self.suggest_retail_cur := suggest_retail_cur;
RETURN;
end;
constructor function "RIB_ItPrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, store_id number
, item_id varchar2
, effective_date date
, end_date date
, price_type varchar2
, selling_unit_price_cur varchar2
, selling_unit_price_value number
, status varchar2
, promotion_id number
, promotion_component_id number
, selling_uom varchar2
, multi_units number
, multi_unit_price_currency_code varchar2
, multi_unit_price_value number
, multi_unit_uom varchar2
, multi_unit_change varchar2
, selling_unit_change varchar2
, promotion_name varchar2
, promotion_description varchar2
, promotion_comp_name varchar2
, reset_clearance_id number
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, suggest_retail_cur varchar2
, suggest_retail_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_price_id := item_price_id;
self.external_id := external_id;
self.store_id := store_id;
self.item_id := item_id;
self.effective_date := effective_date;
self.end_date := end_date;
self.price_type := price_type;
self.selling_unit_price_cur := selling_unit_price_cur;
self.selling_unit_price_value := selling_unit_price_value;
self.status := status;
self.promotion_id := promotion_id;
self.promotion_component_id := promotion_component_id;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_price_currency_code := multi_unit_price_currency_code;
self.multi_unit_price_value := multi_unit_price_value;
self.multi_unit_uom := multi_unit_uom;
self.multi_unit_change := multi_unit_change;
self.selling_unit_change := selling_unit_change;
self.promotion_name := promotion_name;
self.promotion_description := promotion_description;
self.promotion_comp_name := promotion_comp_name;
self.reset_clearance_id := reset_clearance_id;
self.new_price_cur := new_price_cur;
self.new_price_value := new_price_value;
self.new_selling_uom := new_selling_uom;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
RETURN;
end;
END;
/
