DROP TYPE "RIB_FopCreBinDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopCreBinDesc_REC";
/
DROP TYPE "RIB_FopCreBinDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopCreBinDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopCreBinDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  number_of_bins number(3),
  bin_id_col "RIB_bin_id_col_TBL",   -- Size of "RIB_bin_id_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopCreBinDesc_REC"
(
  rib_oid number
, store_id number
, number_of_bins number
) return self as result
,constructor function "RIB_FopCreBinDesc_REC"
(
  rib_oid number
, store_id number
, number_of_bins number
, bin_id_col "RIB_bin_id_col_TBL"  -- Size of "RIB_bin_id_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopCreBinDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopCreBinDesc') := "ns_name_FopCreBinDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_bins') := number_of_bins;
  IF bin_id_col IS NOT NULL THEN
    FOR INDX IN bin_id_col.FIRST()..bin_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'bin_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'bin_id_col'||'.'):=bin_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FopCreBinDesc_REC"
(
  rib_oid number
, store_id number
, number_of_bins number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.number_of_bins := number_of_bins;
RETURN;
end;
constructor function "RIB_FopCreBinDesc_REC"
(
  rib_oid number
, store_id number
, number_of_bins number
, bin_id_col "RIB_bin_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.number_of_bins := number_of_bins;
self.bin_id_col := bin_id_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_bin_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_bin_id_col_TBL" AS TABLE OF varchar2(128);
/
