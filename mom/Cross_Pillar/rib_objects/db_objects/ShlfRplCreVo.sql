DROP TYPE "RIB_ShlfRplCreVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplCreVo_REC";
/
DROP TYPE "RIB_ShlfRplCreVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplCreVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplCreVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  type varchar2(20), -- type is enumeration field, valid values are [CAPACITY, SALES, AD_HOC, DISPLAY, UNKNOWN] (all lower-case)
  replenishment_mode varchar2(20), -- replenishment_mode is enumeration field, valid values are [END_OF_DAY, WITHIN_DAY, UNKNOWN] (all lower-case)
  product_group_id number(12),
  shelf_adjust_id number(15),
  user_name varchar2(128),
  department_id number(12),
  class_id number(12),
  subclass_id number(12),
  replenishment_quantity number(20,4),
  check_available_soh varchar2(5), --check_available_soh is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplCreVo_REC"
(
  rib_oid number
, store_id number
, type varchar2
, replenishment_mode varchar2
, product_group_id number
, shelf_adjust_id number
, user_name varchar2
, department_id number
, class_id number
, subclass_id number
, replenishment_quantity number
, check_available_soh varchar2  --check_available_soh is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplCreVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplCreVo') := "ns_name_ShlfRplCreVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_mode') := replenishment_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'shelf_adjust_id') := shelf_adjust_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_quantity') := replenishment_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'check_available_soh') := check_available_soh;
END appendNodeValues;
constructor function "RIB_ShlfRplCreVo_REC"
(
  rib_oid number
, store_id number
, type varchar2
, replenishment_mode varchar2
, product_group_id number
, shelf_adjust_id number
, user_name varchar2
, department_id number
, class_id number
, subclass_id number
, replenishment_quantity number
, check_available_soh varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.type := type;
self.replenishment_mode := replenishment_mode;
self.product_group_id := product_group_id;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.replenishment_quantity := replenishment_quantity;
self.check_available_soh := check_available_soh;
RETURN;
end;
END;
/
