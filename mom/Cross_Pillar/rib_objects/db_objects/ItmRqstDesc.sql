DROP TYPE "RIB_ItmRqstDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmRqstDesc_REC";
/
DROP TYPE "RIB_ItmRqstItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmRqstItm_REC";
/
DROP TYPE "RIB_ItmRqstItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmRqstItm_TBL" AS TABLE OF "RIB_ItmRqstItm_REC";
/
DROP TYPE "RIB_ItmRqstDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmRqstDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmRqstDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  request_id number(12),
  store_id number(10),
  user_name varchar2(128),
  comments varchar2(2000),
  delivery_date date,
  expiration_date date,
  process_date date,
  create_date date,
  status varchar2(10), -- status is enumeration field, valid values are [PENDING, COMPLETED, CANCELED, UNKNOWN, NO_VALUE] (all lower-case)
  timeslot_id varchar2(15),
  schedule_description varchar2(250),
  ItmRqstItm_TBL "RIB_ItmRqstItm_TBL",   -- Size of "RIB_ItmRqstItm_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmRqstDesc_REC"
(
  rib_oid number
, request_id number
, store_id number
, user_name varchar2
, comments varchar2
, delivery_date date
, expiration_date date
, process_date date
, create_date date
, status varchar2
, timeslot_id varchar2
, schedule_description varchar2
, ItmRqstItm_TBL "RIB_ItmRqstItm_TBL"  -- Size of "RIB_ItmRqstItm_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmRqstDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmRqstDesc') := "ns_name_ItmRqstDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'request_id') := request_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration_date') := expiration_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'process_date') := process_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'timeslot_id') := timeslot_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_description') := schedule_description;
  l_new_pre :=i_prefix||'ItmRqstItm_TBL.';
  FOR INDX IN ItmRqstItm_TBL.FIRST()..ItmRqstItm_TBL.LAST() LOOP
    ItmRqstItm_TBL(indx).appendNodeValues( i_prefix||indx||'ItmRqstItm_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItmRqstDesc_REC"
(
  rib_oid number
, request_id number
, store_id number
, user_name varchar2
, comments varchar2
, delivery_date date
, expiration_date date
, process_date date
, create_date date
, status varchar2
, timeslot_id varchar2
, schedule_description varchar2
, ItmRqstItm_TBL "RIB_ItmRqstItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.request_id := request_id;
self.store_id := store_id;
self.user_name := user_name;
self.comments := comments;
self.delivery_date := delivery_date;
self.expiration_date := expiration_date;
self.process_date := process_date;
self.create_date := create_date;
self.status := status;
self.timeslot_id := timeslot_id;
self.schedule_description := schedule_description;
self.ItmRqstItm_TBL := ItmRqstItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItmRqstItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmRqstItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmRqstDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  timeslot_id varchar2(15),
  quantity number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmRqstItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, timeslot_id varchar2
, quantity number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmRqstItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmRqstDesc') := "ns_name_ItmRqstDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'timeslot_id') := timeslot_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_ItmRqstItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, timeslot_id varchar2
, quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.timeslot_id := timeslot_id;
self.quantity := quantity;
self.case_size := case_size;
RETURN;
end;
END;
/
