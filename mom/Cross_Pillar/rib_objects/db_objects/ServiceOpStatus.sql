DROP TYPE "RIB_ServiceOpStatus_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ServiceOpStatus_REC";
/
DROP TYPE "RIB_SuccessStatus_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SuccessStatus_REC";
/
DROP TYPE "RIB_FailStatus_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FailStatus_REC";
/
DROP TYPE "RIB_BusinessProblemDetail_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BusinessProblemDetail_REC";
/
DROP TYPE "RIB_ProblemDetailEntry_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProblemDetailEntry_REC";
/
DROP TYPE "RIB_SuccessStatus_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SuccessStatus_TBL" AS TABLE OF "RIB_SuccessStatus_REC";
/
DROP TYPE "RIB_FailStatus_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FailStatus_TBL" AS TABLE OF "RIB_FailStatus_REC";
/
DROP TYPE "RIB_problemDescription_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_problemDescription_TBL" AS TABLE OF varchar2(500);
/
DROP TYPE "RIB_ServiceOpStatus_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ServiceOpStatus_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ServiceOpStatus" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_services" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_integration" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  SuccessStatus_TBL "RIB_SuccessStatus_TBL",
  FailStatus_TBL "RIB_FailStatus_TBL",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ServiceOpStatus_REC"
(
  rib_oid number
, SuccessStatus_TBL "RIB_SuccessStatus_TBL"
) return self as result
,constructor function "RIB_ServiceOpStatus_REC"
(
  rib_oid number
, SuccessStatus_TBL "RIB_SuccessStatus_TBL"
, FailStatus_TBL "RIB_FailStatus_TBL"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ServiceOpStatus_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpStatus') := "ns_name_ServiceOpStatus";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF SuccessStatus_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SuccessStatus_TBL.';
    FOR INDX IN SuccessStatus_TBL.FIRST()..SuccessStatus_TBL.LAST() LOOP
      SuccessStatus_TBL(indx).appendNodeValues( i_prefix||indx||'SuccessStatus_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF FailStatus_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FailStatus_TBL.';
    FOR INDX IN FailStatus_TBL.FIRST()..FailStatus_TBL.LAST() LOOP
      FailStatus_TBL(indx).appendNodeValues( i_prefix||indx||'FailStatus_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ServiceOpStatus_REC"
(
  rib_oid number
, SuccessStatus_TBL "RIB_SuccessStatus_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.SuccessStatus_TBL := SuccessStatus_TBL;
RETURN;
end;
constructor function "RIB_ServiceOpStatus_REC"
(
  rib_oid number
, SuccessStatus_TBL "RIB_SuccessStatus_TBL"
, FailStatus_TBL "RIB_FailStatus_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.SuccessStatus_TBL := SuccessStatus_TBL;
self.FailStatus_TBL := FailStatus_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_SuccessStatus_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SuccessStatus_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ServiceOpStatus" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_services" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_integration" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shortSuccessMessage varchar2(255),
  successDescription varchar2(500),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SuccessStatus_REC"
(
  rib_oid number
, shortSuccessMessage varchar2
) return self as result
,constructor function "RIB_SuccessStatus_REC"
(
  rib_oid number
, shortSuccessMessage varchar2
, successDescription varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SuccessStatus_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpStatus') := "ns_name_ServiceOpStatus";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'shortSuccessMessage') := shortSuccessMessage;
  rib_obj_util.g_RIB_element_values(i_prefix||'successDescription') := successDescription;
END appendNodeValues;
constructor function "RIB_SuccessStatus_REC"
(
  rib_oid number
, shortSuccessMessage varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shortSuccessMessage := shortSuccessMessage;
RETURN;
end;
constructor function "RIB_SuccessStatus_REC"
(
  rib_oid number
, shortSuccessMessage varchar2
, successDescription varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shortSuccessMessage := shortSuccessMessage;
self.successDescription := successDescription;
RETURN;
end;
END;
/
DROP TYPE "RIB_BusinessProblemDetail_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BusinessProblemDetail_TBL" AS TABLE OF "RIB_BusinessProblemDetail_REC";
/
DROP TYPE "RIB_FailStatus_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FailStatus_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ServiceOpStatus" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_services" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_integration" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  errorType varchar2(255),
  shortErrorMessage varchar2(255),
  errorDescription varchar2(500),
  BusinessProblemDetail_TBL "RIB_BusinessProblemDetail_TBL",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FailStatus_REC"
(
  rib_oid number
, errorType varchar2
, shortErrorMessage varchar2
) return self as result
,constructor function "RIB_FailStatus_REC"
(
  rib_oid number
, errorType varchar2
, shortErrorMessage varchar2
, errorDescription varchar2
) return self as result
,constructor function "RIB_FailStatus_REC"
(
  rib_oid number
, errorType varchar2
, shortErrorMessage varchar2
, errorDescription varchar2
, BusinessProblemDetail_TBL "RIB_BusinessProblemDetail_TBL"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FailStatus_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpStatus') := "ns_name_ServiceOpStatus";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'errorType') := errorType;
  rib_obj_util.g_RIB_element_values(i_prefix||'shortErrorMessage') := shortErrorMessage;
  rib_obj_util.g_RIB_element_values(i_prefix||'errorDescription') := errorDescription;
  IF BusinessProblemDetail_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BusinessProblemDetail_TBL.';
    FOR INDX IN BusinessProblemDetail_TBL.FIRST()..BusinessProblemDetail_TBL.LAST() LOOP
      BusinessProblemDetail_TBL(indx).appendNodeValues( i_prefix||indx||'BusinessProblemDetail_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FailStatus_REC"
(
  rib_oid number
, errorType varchar2
, shortErrorMessage varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.errorType := errorType;
self.shortErrorMessage := shortErrorMessage;
RETURN;
end;
constructor function "RIB_FailStatus_REC"
(
  rib_oid number
, errorType varchar2
, shortErrorMessage varchar2
, errorDescription varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.errorType := errorType;
self.shortErrorMessage := shortErrorMessage;
self.errorDescription := errorDescription;
RETURN;
end;
constructor function "RIB_FailStatus_REC"
(
  rib_oid number
, errorType varchar2
, shortErrorMessage varchar2
, errorDescription varchar2
, BusinessProblemDetail_TBL "RIB_BusinessProblemDetail_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.errorType := errorType;
self.shortErrorMessage := shortErrorMessage;
self.errorDescription := errorDescription;
self.BusinessProblemDetail_TBL := BusinessProblemDetail_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ProblemDetailEntry_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ProblemDetailEntry_TBL" AS TABLE OF "RIB_ProblemDetailEntry_REC";
/
DROP TYPE "RIB_BusinessProblemDetail_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BusinessProblemDetail_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ServiceOpStatus" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_services" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_integration" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  problemDescription_TBL "RIB_problemDescription_TBL",
  ProblemDetailEntry_TBL "RIB_ProblemDetailEntry_TBL",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BusinessProblemDetail_REC"
(
  rib_oid number
, problemDescription_TBL "RIB_problemDescription_TBL"
) return self as result
,constructor function "RIB_BusinessProblemDetail_REC"
(
  rib_oid number
, problemDescription_TBL "RIB_problemDescription_TBL"
, ProblemDetailEntry_TBL "RIB_ProblemDetailEntry_TBL"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BusinessProblemDetail_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpStatus') := "ns_name_ServiceOpStatus";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";

  IF problemDescription_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'problemDescription_TBL.';   
  END IF;  

  IF ProblemDetailEntry_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ProblemDetailEntry_TBL.';
    FOR INDX IN ProblemDetailEntry_TBL.FIRST()..ProblemDetailEntry_TBL.LAST() LOOP
      ProblemDetailEntry_TBL(indx).appendNodeValues( i_prefix||indx||'ProblemDetailEntry_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_BusinessProblemDetail_REC"
(
  rib_oid number
, problemDescription_TBL "RIB_problemDescription_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.problemDescription_TBL := problemDescription_TBL;
RETURN;
end;
constructor function "RIB_BusinessProblemDetail_REC"
(
  rib_oid number
, problemDescription_TBL "RIB_problemDescription_TBL"
, ProblemDetailEntry_TBL "RIB_ProblemDetailEntry_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.problemDescription_TBL := problemDescription_TBL;
self.ProblemDetailEntry_TBL := ProblemDetailEntry_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ProblemDetailEntry_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProblemDetailEntry_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ServiceOpStatus" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_services" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_integration" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  name varchar2(255),
  value varchar2(255),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProblemDetailEntry_REC"
(
  rib_oid number
, name varchar2
) return self as result
,constructor function "RIB_ProblemDetailEntry_REC"
(
  rib_oid number
, name varchar2
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProblemDetailEntry_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpStatus') := "ns_name_ServiceOpStatus";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_ProblemDetailEntry_REC"
(
  rib_oid number
, name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
RETURN;
end;
constructor function "RIB_ProblemDetailEntry_REC"
(
  rib_oid number
, name varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
self.value := value;
RETURN;
end;
END;
/
