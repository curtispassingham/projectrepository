@@InPayTermRef.sql;
/
@@BrPayTermRef.sql;
/
DROP TYPE "RIB_LocOfPayTermRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermRef_REC";
/
DROP TYPE "RIB_LocOfTermsSeq_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfTermsSeq_REC";
/
DROP TYPE "RIB_InPayTermRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InPayTermRef_TBL" AS TABLE OF "RIB_InPayTermRef_REC";
/
DROP TYPE "RIB_BrPayTermRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrPayTermRef_TBL" AS TABLE OF "RIB_BrPayTermRef_REC";
/
DROP TYPE "RIB_LocOfPayTermRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfPayTermRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InPayTermRef_TBL "RIB_InPayTermRef_TBL",   -- Size of "RIB_InPayTermRef_TBL" is unbounded
  BrPayTermRef_TBL "RIB_BrPayTermRef_TBL",   -- Size of "RIB_BrPayTermRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfPayTermRef_REC"
(
  rib_oid number
, InPayTermRef_TBL "RIB_InPayTermRef_TBL"  -- Size of "RIB_InPayTermRef_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfPayTermRef_REC"
(
  rib_oid number
, InPayTermRef_TBL "RIB_InPayTermRef_TBL"  -- Size of "RIB_InPayTermRef_TBL" is unbounded
, BrPayTermRef_TBL "RIB_BrPayTermRef_TBL"  -- Size of "RIB_BrPayTermRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfPayTermRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfPayTermRef') := "ns_name_LocOfPayTermRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF InPayTermRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InPayTermRef_TBL.';
    FOR INDX IN InPayTermRef_TBL.FIRST()..InPayTermRef_TBL.LAST() LOOP
      InPayTermRef_TBL(indx).appendNodeValues( i_prefix||indx||'InPayTermRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrPayTermRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrPayTermRef_TBL.';
    FOR INDX IN BrPayTermRef_TBL.FIRST()..BrPayTermRef_TBL.LAST() LOOP
      BrPayTermRef_TBL(indx).appendNodeValues( i_prefix||indx||'BrPayTermRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfPayTermRef_REC"
(
  rib_oid number
, InPayTermRef_TBL "RIB_InPayTermRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InPayTermRef_TBL := InPayTermRef_TBL;
RETURN;
end;
constructor function "RIB_LocOfPayTermRef_REC"
(
  rib_oid number
, InPayTermRef_TBL "RIB_InPayTermRef_TBL"
, BrPayTermRef_TBL "RIB_BrPayTermRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InPayTermRef_TBL := InPayTermRef_TBL;
self.BrPayTermRef_TBL := BrPayTermRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InTermsSeq_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InTermsSeq_TBL" AS TABLE OF "RIB_InTermsSeq_REC";
/
DROP TYPE "RIB_BrTermsSeq_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrTermsSeq_TBL" AS TABLE OF "RIB_BrTermsSeq_REC";
/
DROP TYPE "RIB_LocOfTermsSeq_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfTermsSeq_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfPayTermRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InTermsSeq_TBL "RIB_InTermsSeq_TBL",   -- Size of "RIB_InTermsSeq_TBL" is unbounded
  BrTermsSeq_TBL "RIB_BrTermsSeq_TBL",   -- Size of "RIB_BrTermsSeq_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfTermsSeq_REC"
(
  rib_oid number
, InTermsSeq_TBL "RIB_InTermsSeq_TBL"  -- Size of "RIB_InTermsSeq_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfTermsSeq_REC"
(
  rib_oid number
, InTermsSeq_TBL "RIB_InTermsSeq_TBL"  -- Size of "RIB_InTermsSeq_TBL" is unbounded
, BrTermsSeq_TBL "RIB_BrTermsSeq_TBL"  -- Size of "RIB_BrTermsSeq_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfTermsSeq_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfPayTermRef') := "ns_name_LocOfPayTermRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InTermsSeq_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InTermsSeq_TBL.';
    FOR INDX IN InTermsSeq_TBL.FIRST()..InTermsSeq_TBL.LAST() LOOP
      InTermsSeq_TBL(indx).appendNodeValues( i_prefix||indx||'InTermsSeq_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrTermsSeq_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrTermsSeq_TBL.';
    FOR INDX IN BrTermsSeq_TBL.FIRST()..BrTermsSeq_TBL.LAST() LOOP
      BrTermsSeq_TBL(indx).appendNodeValues( i_prefix||indx||'BrTermsSeq_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfTermsSeq_REC"
(
  rib_oid number
, InTermsSeq_TBL "RIB_InTermsSeq_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InTermsSeq_TBL := InTermsSeq_TBL;
RETURN;
end;
constructor function "RIB_LocOfTermsSeq_REC"
(
  rib_oid number
, InTermsSeq_TBL "RIB_InTermsSeq_TBL"
, BrTermsSeq_TBL "RIB_BrTermsSeq_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InTermsSeq_TBL := InTermsSeq_TBL;
self.BrTermsSeq_TBL := BrTermsSeq_TBL;
RETURN;
end;
END;
/
