@@ExtOfPayTermDesc.sql;
/
@@LocOfPayTermDesc.sql;
/
DROP TYPE "RIB_PayTermDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PayTermDtl_REC";
/
DROP TYPE "RIB_PayTermDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PayTermDesc_REC";
/
DROP TYPE "RIB_LocOfPayTermDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermDtl_TBL" AS TABLE OF "RIB_LocOfPayTermDtl_REC";
/
DROP TYPE "RIB_PayTermDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PayTermDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PayTermDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  due_days number(3),
  due_max_amount number(12,4),
  due_dom number(2),
  discdays number(3),
  percent number(12,4),
  disc_dom number(2),
  disc_mm_fwd number(3),
  fixed_date date,
  enabled_flag varchar2(1),
  start_date_active date,
  end_date_active date,
  terms_seq number(10),
  due_mm_fwd number(3),
  cutoff_day number(2),
  ExtOfPayTermDtl "RIB_ExtOfPayTermDtl_REC",
  LocOfPayTermDtl_TBL "RIB_LocOfPayTermDtl_TBL",   -- Size of "RIB_LocOfPayTermDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PayTermDtl_REC"
(
  rib_oid number
, due_days number
, due_max_amount number
, due_dom number
, discdays number
, percent number
, disc_dom number
, disc_mm_fwd number
, fixed_date date
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, terms_seq number
, due_mm_fwd number
, cutoff_day number
) return self as result
,constructor function "RIB_PayTermDtl_REC"
(
  rib_oid number
, due_days number
, due_max_amount number
, due_dom number
, discdays number
, percent number
, disc_dom number
, disc_mm_fwd number
, fixed_date date
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, terms_seq number
, due_mm_fwd number
, cutoff_day number
, ExtOfPayTermDtl "RIB_ExtOfPayTermDtl_REC"
) return self as result
,constructor function "RIB_PayTermDtl_REC"
(
  rib_oid number
, due_days number
, due_max_amount number
, due_dom number
, discdays number
, percent number
, disc_dom number
, disc_mm_fwd number
, fixed_date date
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, terms_seq number
, due_mm_fwd number
, cutoff_day number
, ExtOfPayTermDtl "RIB_ExtOfPayTermDtl_REC"
, LocOfPayTermDtl_TBL "RIB_LocOfPayTermDtl_TBL"  -- Size of "RIB_LocOfPayTermDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PayTermDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PayTermDesc') := "ns_name_PayTermDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'due_days') := due_days;
  rib_obj_util.g_RIB_element_values(i_prefix||'due_max_amount') := due_max_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'due_dom') := due_dom;
  rib_obj_util.g_RIB_element_values(i_prefix||'discdays') := discdays;
  rib_obj_util.g_RIB_element_values(i_prefix||'percent') := percent;
  rib_obj_util.g_RIB_element_values(i_prefix||'disc_dom') := disc_dom;
  rib_obj_util.g_RIB_element_values(i_prefix||'disc_mm_fwd') := disc_mm_fwd;
  rib_obj_util.g_RIB_element_values(i_prefix||'fixed_date') := fixed_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'enabled_flag') := enabled_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date_active') := start_date_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date_active') := end_date_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms_seq') := terms_seq;
  rib_obj_util.g_RIB_element_values(i_prefix||'due_mm_fwd') := due_mm_fwd;
  rib_obj_util.g_RIB_element_values(i_prefix||'cutoff_day') := cutoff_day;
  l_new_pre :=i_prefix||'ExtOfPayTermDtl.';
  ExtOfPayTermDtl.appendNodeValues( i_prefix||'ExtOfPayTermDtl');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfPayTermDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfPayTermDtl_TBL.';
    FOR INDX IN LocOfPayTermDtl_TBL.FIRST()..LocOfPayTermDtl_TBL.LAST() LOOP
      LocOfPayTermDtl_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfPayTermDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PayTermDtl_REC"
(
  rib_oid number
, due_days number
, due_max_amount number
, due_dom number
, discdays number
, percent number
, disc_dom number
, disc_mm_fwd number
, fixed_date date
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, terms_seq number
, due_mm_fwd number
, cutoff_day number
) return self as result is
begin
self.rib_oid := rib_oid;
self.due_days := due_days;
self.due_max_amount := due_max_amount;
self.due_dom := due_dom;
self.discdays := discdays;
self.percent := percent;
self.disc_dom := disc_dom;
self.disc_mm_fwd := disc_mm_fwd;
self.fixed_date := fixed_date;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.terms_seq := terms_seq;
self.due_mm_fwd := due_mm_fwd;
self.cutoff_day := cutoff_day;
RETURN;
end;
constructor function "RIB_PayTermDtl_REC"
(
  rib_oid number
, due_days number
, due_max_amount number
, due_dom number
, discdays number
, percent number
, disc_dom number
, disc_mm_fwd number
, fixed_date date
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, terms_seq number
, due_mm_fwd number
, cutoff_day number
, ExtOfPayTermDtl "RIB_ExtOfPayTermDtl_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.due_days := due_days;
self.due_max_amount := due_max_amount;
self.due_dom := due_dom;
self.discdays := discdays;
self.percent := percent;
self.disc_dom := disc_dom;
self.disc_mm_fwd := disc_mm_fwd;
self.fixed_date := fixed_date;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.terms_seq := terms_seq;
self.due_mm_fwd := due_mm_fwd;
self.cutoff_day := cutoff_day;
self.ExtOfPayTermDtl := ExtOfPayTermDtl;
RETURN;
end;
constructor function "RIB_PayTermDtl_REC"
(
  rib_oid number
, due_days number
, due_max_amount number
, due_dom number
, discdays number
, percent number
, disc_dom number
, disc_mm_fwd number
, fixed_date date
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, terms_seq number
, due_mm_fwd number
, cutoff_day number
, ExtOfPayTermDtl "RIB_ExtOfPayTermDtl_REC"
, LocOfPayTermDtl_TBL "RIB_LocOfPayTermDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.due_days := due_days;
self.due_max_amount := due_max_amount;
self.due_dom := due_dom;
self.discdays := discdays;
self.percent := percent;
self.disc_dom := disc_dom;
self.disc_mm_fwd := disc_mm_fwd;
self.fixed_date := fixed_date;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.terms_seq := terms_seq;
self.due_mm_fwd := due_mm_fwd;
self.cutoff_day := cutoff_day;
self.ExtOfPayTermDtl := ExtOfPayTermDtl;
self.LocOfPayTermDtl_TBL := LocOfPayTermDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PayTermDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PayTermDtl_TBL" AS TABLE OF "RIB_PayTermDtl_REC";
/
DROP TYPE "RIB_LocOfPayTermDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfPayTermDesc_TBL" AS TABLE OF "RIB_LocOfPayTermDesc_REC";
/
DROP TYPE "RIB_PayTermDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PayTermDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PayTermDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  terms_xref_key varchar2(32),
  terms varchar2(15),
  terms_code varchar2(50),
  terms_desc varchar2(240),
  due_days number(3),
  enabled_flag varchar2(1),
  start_date_active date,
  end_date_active date,
  discdays number(3),
  percent number(12,4),
  PayTermDtl_TBL "RIB_PayTermDtl_TBL",   -- Size of "RIB_PayTermDtl_TBL" is unbounded
  rank number(10),
  ExtOfPayTermDesc "RIB_ExtOfPayTermDesc_REC",
  LocOfPayTermDesc_TBL "RIB_LocOfPayTermDesc_TBL",   -- Size of "RIB_LocOfPayTermDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"  -- Size of "RIB_PayTermDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"  -- Size of "RIB_PayTermDtl_TBL" is unbounded
, rank number
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"  -- Size of "RIB_PayTermDtl_TBL" is unbounded
, rank number
, ExtOfPayTermDesc "RIB_ExtOfPayTermDesc_REC"
) return self as result
,constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"  -- Size of "RIB_PayTermDtl_TBL" is unbounded
, rank number
, ExtOfPayTermDesc "RIB_ExtOfPayTermDesc_REC"
, LocOfPayTermDesc_TBL "RIB_LocOfPayTermDesc_TBL"  -- Size of "RIB_LocOfPayTermDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PayTermDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PayTermDesc') := "ns_name_PayTermDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'terms_xref_key') := terms_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms') := terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms_code') := terms_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms_desc') := terms_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'due_days') := due_days;
  rib_obj_util.g_RIB_element_values(i_prefix||'enabled_flag') := enabled_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date_active') := start_date_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date_active') := end_date_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'discdays') := discdays;
  rib_obj_util.g_RIB_element_values(i_prefix||'percent') := percent;
  IF PayTermDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PayTermDtl_TBL.';
    FOR INDX IN PayTermDtl_TBL.FIRST()..PayTermDtl_TBL.LAST() LOOP
      PayTermDtl_TBL(indx).appendNodeValues( i_prefix||indx||'PayTermDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'rank') := rank;
  l_new_pre :=i_prefix||'ExtOfPayTermDesc.';
  ExtOfPayTermDesc.appendNodeValues( i_prefix||'ExtOfPayTermDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfPayTermDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfPayTermDesc_TBL.';
    FOR INDX IN LocOfPayTermDesc_TBL.FIRST()..LocOfPayTermDesc_TBL.LAST() LOOP
      LocOfPayTermDesc_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfPayTermDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.discdays := discdays;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.discdays := discdays;
self.percent := percent;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.discdays := discdays;
self.percent := percent;
self.PayTermDtl_TBL := PayTermDtl_TBL;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"
, rank number
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.discdays := discdays;
self.percent := percent;
self.PayTermDtl_TBL := PayTermDtl_TBL;
self.rank := rank;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"
, rank number
, ExtOfPayTermDesc "RIB_ExtOfPayTermDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.discdays := discdays;
self.percent := percent;
self.PayTermDtl_TBL := PayTermDtl_TBL;
self.rank := rank;
self.ExtOfPayTermDesc := ExtOfPayTermDesc;
RETURN;
end;
constructor function "RIB_PayTermDesc_REC"
(
  rib_oid number
, terms_xref_key varchar2
, terms varchar2
, terms_code varchar2
, terms_desc varchar2
, due_days number
, enabled_flag varchar2
, start_date_active date
, end_date_active date
, discdays number
, percent number
, PayTermDtl_TBL "RIB_PayTermDtl_TBL"
, rank number
, ExtOfPayTermDesc "RIB_ExtOfPayTermDesc_REC"
, LocOfPayTermDesc_TBL "RIB_LocOfPayTermDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.terms_xref_key := terms_xref_key;
self.terms := terms;
self.terms_code := terms_code;
self.terms_desc := terms_desc;
self.due_days := due_days;
self.enabled_flag := enabled_flag;
self.start_date_active := start_date_active;
self.end_date_active := end_date_active;
self.discdays := discdays;
self.percent := percent;
self.PayTermDtl_TBL := PayTermDtl_TBL;
self.rank := rank;
self.ExtOfPayTermDesc := ExtOfPayTermDesc;
self.LocOfPayTermDesc_TBL := LocOfPayTermDesc_TBL;
RETURN;
end;
END;
/
