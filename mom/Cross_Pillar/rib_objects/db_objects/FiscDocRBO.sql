@@NameValPairRBO.sql;
/
@@InformTaxRBO.sql;
/
@@FiscEntityRBO.sql;
/
@@LineItemRBO.sql;
/
@@FreightRBO.sql;
/
DROP TYPE "RIB_FiscDocRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscDocRBO_REC";
/
DROP TYPE "RIB_ToEntity_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ToEntity_REC";
/
DROP TYPE "RIB_FromEntity_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FromEntity_REC";
/
DROP TYPE "RIB_ToEntity_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ToEntity_TBL" AS TABLE OF "RIB_ToEntity_REC";
/
DROP TYPE "RIB_FromEntity_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FromEntity_TBL" AS TABLE OF "RIB_FromEntity_REC";
/
DROP TYPE "RIB_LineItemRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LineItemRBO_TBL" AS TABLE OF "RIB_LineItemRBO_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_InformTaxRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InformTaxRBO_TBL" AS TABLE OF "RIB_InformTaxRBO_REC";
/
DROP TYPE "RIB_FreightRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FreightRBO_TBL" AS TABLE OF "RIB_FreightRBO_REC";
/
DROP TYPE "RIB_FiscDocRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscDocRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ToEntity_TBL "RIB_ToEntity_TBL",   -- Size of "RIB_ToEntity_TBL" is unbounded
  FromEntity_TBL "RIB_FromEntity_TBL",   -- Size of "RIB_FromEntity_TBL" is unbounded
  LineItemRBO_TBL "RIB_LineItemRBO_TBL",   -- Size of "RIB_LineItemRBO_TBL" is unbounded
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  due_date date,
  fiscal_document_date date,
  document_type varchar2(2),
  gross_weight number(12,4),
  net_weight number(12,4),
  operation_type varchar2(1),
  freight number(20,4),
  insurance number(20,4),
  discount number(20,4),
  commision number(20,4),
  freight_type varchar2(1),
  other_expenses number(20,4),
  total_cost number(20,4),
  tax_amount number(20,4),
  tax_basis_amount number(20,4),
  tax_code varchar2(25),
  receipt_date date,
  transaction_type varchar2(1),
  is_supplier_issuer varchar2(1),
  no_history_tracked varchar2(1),
  process_inconclusive_rules varchar2(1),
  approximation_mode varchar2(1),
  decimal_precision varchar2(1),
  calculation_status varchar2(1),
  enable_log varchar2(1),
  calc_process_type varchar2(10),
  nature_of_operation varchar2(25),
  ignore_tax_calc_list varchar2(500),
  document_series varchar2(25),
  document_number varchar2(25),
  InformTaxRBO_TBL "RIB_InformTaxRBO_TBL",   -- Size of "RIB_InformTaxRBO_TBL" is unbounded
  FreightRBO_TBL "RIB_FreightRBO_TBL",   -- Size of "RIB_FreightRBO_TBL" is unbounded
  tsf_purchase_type varchar2(1),
  non_taxable_expenses number(20,4),
  end_consumer_ind varchar2(1),
  presence_ind number(4),
  st_control_method varchar2(250),
  total_icms_fcp_addr_state number(13,2),
  total_icms_difal_sender_state number(13,2),
  total_icms_difal_addr_state number(13,2),
  legal_message varchar2(250),
  rma_reason_code varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
, total_icms_difal_addr_state number
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
, total_icms_difal_addr_state number
, legal_message varchar2
) return self as result
,constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"  -- Size of "RIB_ToEntity_TBL" is unbounded
, FromEntity_TBL "RIB_FromEntity_TBL"  -- Size of "RIB_FromEntity_TBL" is unbounded
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"  -- Size of "RIB_LineItemRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, FreightRBO_TBL "RIB_FreightRBO_TBL"  -- Size of "RIB_FreightRBO_TBL" is unbounded
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
, total_icms_difal_addr_state number
, legal_message varchar2
, rma_reason_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscDocRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocRBO') := "ns_name_FiscDocRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF ToEntity_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ToEntity_TBL.';
    FOR INDX IN ToEntity_TBL.FIRST()..ToEntity_TBL.LAST() LOOP
      ToEntity_TBL(indx).appendNodeValues( i_prefix||indx||'ToEntity_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF FromEntity_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FromEntity_TBL.';
    FOR INDX IN FromEntity_TBL.FIRST()..FromEntity_TBL.LAST() LOOP
      FromEntity_TBL(indx).appendNodeValues( i_prefix||indx||'FromEntity_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF LineItemRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LineItemRBO_TBL.';
    FOR INDX IN LineItemRBO_TBL.FIRST()..LineItemRBO_TBL.LAST() LOOP
      LineItemRBO_TBL(indx).appendNodeValues( i_prefix||indx||'LineItemRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'due_date') := due_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_document_date') := fiscal_document_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'gross_weight') := gross_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'net_weight') := net_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'operation_type') := operation_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight') := freight;
  rib_obj_util.g_RIB_element_values(i_prefix||'insurance') := insurance;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount') := discount;
  rib_obj_util.g_RIB_element_values(i_prefix||'commision') := commision;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_type') := freight_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'other_expenses') := other_expenses;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cost') := total_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_amount') := tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_basis_amount') := tax_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_code') := tax_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_date') := receipt_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_type') := transaction_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_supplier_issuer') := is_supplier_issuer;
  rib_obj_util.g_RIB_element_values(i_prefix||'no_history_tracked') := no_history_tracked;
  rib_obj_util.g_RIB_element_values(i_prefix||'process_inconclusive_rules') := process_inconclusive_rules;
  rib_obj_util.g_RIB_element_values(i_prefix||'approximation_mode') := approximation_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'decimal_precision') := decimal_precision;
  rib_obj_util.g_RIB_element_values(i_prefix||'calculation_status') := calculation_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'enable_log') := enable_log;
  rib_obj_util.g_RIB_element_values(i_prefix||'calc_process_type') := calc_process_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'nature_of_operation') := nature_of_operation;
  rib_obj_util.g_RIB_element_values(i_prefix||'ignore_tax_calc_list') := ignore_tax_calc_list;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_series') := document_series;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_number') := document_number;
  IF InformTaxRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InformTaxRBO_TBL.';
    FOR INDX IN InformTaxRBO_TBL.FIRST()..InformTaxRBO_TBL.LAST() LOOP
      InformTaxRBO_TBL(indx).appendNodeValues( i_prefix||indx||'InformTaxRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF FreightRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FreightRBO_TBL.';
    FOR INDX IN FreightRBO_TBL.FIRST()..FreightRBO_TBL.LAST() LOOP
      FreightRBO_TBL(indx).appendNodeValues( i_prefix||indx||'FreightRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_purchase_type') := tsf_purchase_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'non_taxable_expenses') := non_taxable_expenses;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_consumer_ind') := end_consumer_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'presence_ind') := presence_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'st_control_method') := st_control_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_icms_fcp_addr_state') := total_icms_fcp_addr_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_icms_difal_sender_state') := total_icms_difal_sender_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_icms_difal_addr_state') := total_icms_difal_addr_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'legal_message') := legal_message;
  rib_obj_util.g_RIB_element_values(i_prefix||'rma_reason_code') := rma_reason_code;
END appendNodeValues;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
self.st_control_method := st_control_method;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
self.st_control_method := st_control_method;
self.total_icms_fcp_addr_state := total_icms_fcp_addr_state;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
self.st_control_method := st_control_method;
self.total_icms_fcp_addr_state := total_icms_fcp_addr_state;
self.total_icms_difal_sender_state := total_icms_difal_sender_state;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
, total_icms_difal_addr_state number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
self.st_control_method := st_control_method;
self.total_icms_fcp_addr_state := total_icms_fcp_addr_state;
self.total_icms_difal_sender_state := total_icms_difal_sender_state;
self.total_icms_difal_addr_state := total_icms_difal_addr_state;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
, total_icms_difal_addr_state number
, legal_message varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
self.st_control_method := st_control_method;
self.total_icms_fcp_addr_state := total_icms_fcp_addr_state;
self.total_icms_difal_sender_state := total_icms_difal_sender_state;
self.total_icms_difal_addr_state := total_icms_difal_addr_state;
self.legal_message := legal_message;
RETURN;
end;
constructor function "RIB_FiscDocRBO_REC"
(
  rib_oid number
, ToEntity_TBL "RIB_ToEntity_TBL"
, FromEntity_TBL "RIB_FromEntity_TBL"
, LineItemRBO_TBL "RIB_LineItemRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, due_date date
, fiscal_document_date date
, document_type varchar2
, gross_weight number
, net_weight number
, operation_type varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, total_cost number
, tax_amount number
, tax_basis_amount number
, tax_code varchar2
, receipt_date date
, transaction_type varchar2
, is_supplier_issuer varchar2
, no_history_tracked varchar2
, process_inconclusive_rules varchar2
, approximation_mode varchar2
, decimal_precision varchar2
, calculation_status varchar2
, enable_log varchar2
, calc_process_type varchar2
, nature_of_operation varchar2
, ignore_tax_calc_list varchar2
, document_series varchar2
, document_number varchar2
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, FreightRBO_TBL "RIB_FreightRBO_TBL"
, tsf_purchase_type varchar2
, non_taxable_expenses number
, end_consumer_ind varchar2
, presence_ind number
, st_control_method varchar2
, total_icms_fcp_addr_state number
, total_icms_difal_sender_state number
, total_icms_difal_addr_state number
, legal_message varchar2
, rma_reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ToEntity_TBL := ToEntity_TBL;
self.FromEntity_TBL := FromEntity_TBL;
self.LineItemRBO_TBL := LineItemRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.due_date := due_date;
self.fiscal_document_date := fiscal_document_date;
self.document_type := document_type;
self.gross_weight := gross_weight;
self.net_weight := net_weight;
self.operation_type := operation_type;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.total_cost := total_cost;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.tax_code := tax_code;
self.receipt_date := receipt_date;
self.transaction_type := transaction_type;
self.is_supplier_issuer := is_supplier_issuer;
self.no_history_tracked := no_history_tracked;
self.process_inconclusive_rules := process_inconclusive_rules;
self.approximation_mode := approximation_mode;
self.decimal_precision := decimal_precision;
self.calculation_status := calculation_status;
self.enable_log := enable_log;
self.calc_process_type := calc_process_type;
self.nature_of_operation := nature_of_operation;
self.ignore_tax_calc_list := ignore_tax_calc_list;
self.document_series := document_series;
self.document_number := document_number;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.FreightRBO_TBL := FreightRBO_TBL;
self.tsf_purchase_type := tsf_purchase_type;
self.non_taxable_expenses := non_taxable_expenses;
self.end_consumer_ind := end_consumer_ind;
self.presence_ind := presence_ind;
self.st_control_method := st_control_method;
self.total_icms_fcp_addr_state := total_icms_fcp_addr_state;
self.total_icms_difal_sender_state := total_icms_difal_sender_state;
self.total_icms_difal_addr_state := total_icms_difal_addr_state;
self.legal_message := legal_message;
self.rma_reason_code := rma_reason_code;
RETURN;
end;
END;
/
DROP TYPE "RIB_ToEntity_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ToEntity_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  FiscEntityRBO "RIB_FiscEntityRBO_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ToEntity_REC"
(
  rib_oid number
, FiscEntityRBO "RIB_FiscEntityRBO_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ToEntity_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocRBO') := "ns_name_FiscDocRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'FiscEntityRBO.';
  FiscEntityRBO.appendNodeValues( i_prefix||'FiscEntityRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ToEntity_REC"
(
  rib_oid number
, FiscEntityRBO "RIB_FiscEntityRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscEntityRBO := FiscEntityRBO;
RETURN;
end;
END;
/
DROP TYPE "RIB_FromEntity_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FromEntity_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  FiscEntityRBO "RIB_FiscEntityRBO_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FromEntity_REC"
(
  rib_oid number
, FiscEntityRBO "RIB_FiscEntityRBO_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FromEntity_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocRBO') := "ns_name_FiscDocRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'FiscEntityRBO.';
  FiscEntityRBO.appendNodeValues( i_prefix||'FiscEntityRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_FromEntity_REC"
(
  rib_oid number
, FiscEntityRBO "RIB_FiscEntityRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscEntityRBO := FiscEntityRBO;
RETURN;
end;
END;
/
