DROP TYPE "RIB_MoneyType_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_MoneyType_REC";
/
DROP TYPE "RIB_NewUnitRetail_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_NewUnitRetail_REC";
/
DROP TYPE "RIB_BasisUnitRetail_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BasisUnitRetail_REC";
/
DROP TYPE "RIB_CurrentUnitRetail_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CurrentUnitRetail_REC";
/
DROP TYPE "RIB_PrcChgExcDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgExcDtl_REC";
/
DROP TYPE "RIB_MoneyType_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_MoneyType_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgExcDtl" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  decimal_value number(20),
  currency_code varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_MoneyType_REC"
(
  rib_oid number
, decimal_value number
, currency_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_MoneyType_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgExcDtl') := "ns_name_PrcChgExcDtl";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'decimal_value') := decimal_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
END appendNodeValues;
constructor function "RIB_MoneyType_REC"
(
  rib_oid number
, decimal_value number
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.decimal_value := decimal_value;
self.currency_code := currency_code;
RETURN;
end;
END;
/
DROP TYPE "RIB_NewUnitRetail_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_NewUnitRetail_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgExcDtl" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  MoneyType "RIB_MoneyType_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_NewUnitRetail_REC"
(
  rib_oid number
, MoneyType "RIB_MoneyType_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_NewUnitRetail_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgExcDtl') := "ns_name_PrcChgExcDtl";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'MoneyType.';
  MoneyType.appendNodeValues( i_prefix||'MoneyType');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_NewUnitRetail_REC"
(
  rib_oid number
, MoneyType "RIB_MoneyType_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.MoneyType := MoneyType;
RETURN;
end;
END;
/
DROP TYPE "RIB_BasisUnitRetail_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BasisUnitRetail_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgExcDtl" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  MoneyType "RIB_MoneyType_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BasisUnitRetail_REC"
(
  rib_oid number
, MoneyType "RIB_MoneyType_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BasisUnitRetail_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgExcDtl') := "ns_name_PrcChgExcDtl";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'MoneyType.';
  MoneyType.appendNodeValues( i_prefix||'MoneyType');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BasisUnitRetail_REC"
(
  rib_oid number
, MoneyType "RIB_MoneyType_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.MoneyType := MoneyType;
RETURN;
end;
END;
/
DROP TYPE "RIB_CurrentUnitRetail_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CurrentUnitRetail_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgExcDtl" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  MoneyType "RIB_MoneyType_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CurrentUnitRetail_REC"
(
  rib_oid number
, MoneyType "RIB_MoneyType_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CurrentUnitRetail_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgExcDtl') := "ns_name_PrcChgExcDtl";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'MoneyType.';
  MoneyType.appendNodeValues( i_prefix||'MoneyType');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CurrentUnitRetail_REC"
(
  rib_oid number
, MoneyType "RIB_MoneyType_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.MoneyType := MoneyType;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcChgExcDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcChgExcDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgExcDtl" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  price_change_type varchar2(6),
  NewUnitRetail "RIB_NewUnitRetail_REC",
  new_selling_uom varchar2(3),
  from_effective_date date,
  to_effective_date date,
  from_end_date date,
  to_end_date date,
  status varchar2(30),
  CurrentUnitRetail "RIB_CurrentUnitRetail_REC",
  current_selling_uom varchar2(4),
  BasisUnitRetail "RIB_BasisUnitRetail_REC",
  basis_uom varchar2(4),
  promotion_id number(10),
  promotion_comp_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
, basis_uom varchar2
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
, basis_uom varchar2
, promotion_id number
) return self as result
,constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
, basis_uom varchar2
, promotion_id number
, promotion_comp_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcChgExcDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgExcDtl') := "ns_name_PrcChgExcDtl";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_change_type') := price_change_type;
  l_new_pre :=i_prefix||'NewUnitRetail.';
  NewUnitRetail.appendNodeValues( i_prefix||'NewUnitRetail');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_selling_uom') := new_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_effective_date') := from_effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_effective_date') := to_effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_end_date') := from_end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_end_date') := to_end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  l_new_pre :=i_prefix||'CurrentUnitRetail.';
  CurrentUnitRetail.appendNodeValues( i_prefix||'CurrentUnitRetail');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'current_selling_uom') := current_selling_uom;
  l_new_pre :=i_prefix||'BasisUnitRetail.';
  BasisUnitRetail.appendNodeValues( i_prefix||'BasisUnitRetail');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'basis_uom') := basis_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_comp_id') := promotion_comp_id;
END appendNodeValues;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
self.CurrentUnitRetail := CurrentUnitRetail;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
self.CurrentUnitRetail := CurrentUnitRetail;
self.current_selling_uom := current_selling_uom;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
self.CurrentUnitRetail := CurrentUnitRetail;
self.current_selling_uom := current_selling_uom;
self.BasisUnitRetail := BasisUnitRetail;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
, basis_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
self.CurrentUnitRetail := CurrentUnitRetail;
self.current_selling_uom := current_selling_uom;
self.BasisUnitRetail := BasisUnitRetail;
self.basis_uom := basis_uom;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
, basis_uom varchar2
, promotion_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
self.CurrentUnitRetail := CurrentUnitRetail;
self.current_selling_uom := current_selling_uom;
self.BasisUnitRetail := BasisUnitRetail;
self.basis_uom := basis_uom;
self.promotion_id := promotion_id;
RETURN;
end;
constructor function "RIB_PrcChgExcDtl_REC"
(
  rib_oid number
, item varchar2
, price_change_type varchar2
, NewUnitRetail "RIB_NewUnitRetail_REC"
, new_selling_uom varchar2
, from_effective_date date
, to_effective_date date
, from_end_date date
, to_end_date date
, status varchar2
, CurrentUnitRetail "RIB_CurrentUnitRetail_REC"
, current_selling_uom varchar2
, BasisUnitRetail "RIB_BasisUnitRetail_REC"
, basis_uom varchar2
, promotion_id number
, promotion_comp_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.price_change_type := price_change_type;
self.NewUnitRetail := NewUnitRetail;
self.new_selling_uom := new_selling_uom;
self.from_effective_date := from_effective_date;
self.to_effective_date := to_effective_date;
self.from_end_date := from_end_date;
self.to_end_date := to_end_date;
self.status := status;
self.CurrentUnitRetail := CurrentUnitRetail;
self.current_selling_uom := current_selling_uom;
self.BasisUnitRetail := BasisUnitRetail;
self.basis_uom := basis_uom;
self.promotion_id := promotion_id;
self.promotion_comp_id := promotion_comp_id;
RETURN;
end;
END;
/
