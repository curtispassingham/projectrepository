DROP TYPE "RIB_ListGroupRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ListGroupRef_REC";
/
DROP TYPE "RIB_ListDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ListDtlRef_REC";
/
DROP TYPE "RIB_ItemListRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemListRef_REC";
/
DROP TYPE "RIB_LocationListRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocationListRef_REC";
/
DROP TYPE "RIB_PromotionRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PromotionRef_REC";
/
DROP TYPE "RIB_ListDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ListDtlRef_TBL" AS TABLE OF "RIB_ListDtlRef_REC";
/
DROP TYPE "RIB_ListGroupRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ListGroupRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  listgroup_id number(10),
  ListDtlRef_TBL "RIB_ListDtlRef_TBL",   -- Size of "RIB_ListDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ListGroupRef_REC"
(
  rib_oid number
, listgroup_id number
, ListDtlRef_TBL "RIB_ListDtlRef_TBL"  -- Size of "RIB_ListDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ListGroupRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionRef') := "ns_name_PromotionRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'listgroup_id') := listgroup_id;
  l_new_pre :=i_prefix||'ListDtlRef_TBL.';
  FOR INDX IN ListDtlRef_TBL.FIRST()..ListDtlRef_TBL.LAST() LOOP
    ListDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'ListDtlRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ListGroupRef_REC"
(
  rib_oid number
, listgroup_id number
, ListDtlRef_TBL "RIB_ListDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.listgroup_id := listgroup_id;
self.ListDtlRef_TBL := ListDtlRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemListRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemListRef_TBL" AS TABLE OF "RIB_ItemListRef_REC";
/
DROP TYPE "RIB_ListDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ListDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  listdtl_id number(10),
  ItemListRef_TBL "RIB_ItemListRef_TBL",   -- Size of "RIB_ItemListRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ListDtlRef_REC"
(
  rib_oid number
, listdtl_id number
, ItemListRef_TBL "RIB_ItemListRef_TBL"  -- Size of "RIB_ItemListRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ListDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionRef') := "ns_name_PromotionRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'listdtl_id') := listdtl_id;
  l_new_pre :=i_prefix||'ItemListRef_TBL.';
  FOR INDX IN ItemListRef_TBL.FIRST()..ItemListRef_TBL.LAST() LOOP
    ItemListRef_TBL(indx).appendNodeValues( i_prefix||indx||'ItemListRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ListDtlRef_REC"
(
  rib_oid number
, listdtl_id number
, ItemListRef_TBL "RIB_ItemListRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.listdtl_id := listdtl_id;
self.ItemListRef_TBL := ItemListRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemListRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemListRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_num varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemListRef_REC"
(
  rib_oid number
, item_num varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemListRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionRef') := "ns_name_PromotionRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_num') := item_num;
END appendNodeValues;
constructor function "RIB_ItemListRef_REC"
(
  rib_oid number
, item_num varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_num := item_num;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocationListRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocationListRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocationListRef_REC"
(
  rib_oid number
, location number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocationListRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionRef') := "ns_name_PromotionRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
END appendNodeValues;
constructor function "RIB_LocationListRef_REC"
(
  rib_oid number
, location number
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocationListRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocationListRef_TBL" AS TABLE OF "RIB_LocationListRef_REC";
/
DROP TYPE "RIB_ListGroupRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ListGroupRef_TBL" AS TABLE OF "RIB_ListGroupRef_REC";
/
DROP TYPE "RIB_PromotionRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PromotionRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  promo_id number(10),
  promo_comp_id number(10),
  promo_dtl_id number(10),
  LocationListRef_TBL "RIB_LocationListRef_TBL",   -- Size of "RIB_LocationListRef_TBL" is unbounded
  ListGroupRef_TBL "RIB_ListGroupRef_TBL",   -- Size of "RIB_ListGroupRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PromotionRef_REC"
(
  rib_oid number
, promo_id number
, promo_comp_id number
, promo_dtl_id number
, LocationListRef_TBL "RIB_LocationListRef_TBL"  -- Size of "RIB_LocationListRef_TBL" is unbounded
, ListGroupRef_TBL "RIB_ListGroupRef_TBL"  -- Size of "RIB_ListGroupRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PromotionRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionRef') := "ns_name_PromotionRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_id') := promo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_id') := promo_comp_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_dtl_id') := promo_dtl_id;
  l_new_pre :=i_prefix||'LocationListRef_TBL.';
  FOR INDX IN LocationListRef_TBL.FIRST()..LocationListRef_TBL.LAST() LOOP
    LocationListRef_TBL(indx).appendNodeValues( i_prefix||indx||'LocationListRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'ListGroupRef_TBL.';
  FOR INDX IN ListGroupRef_TBL.FIRST()..ListGroupRef_TBL.LAST() LOOP
    ListGroupRef_TBL(indx).appendNodeValues( i_prefix||indx||'ListGroupRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PromotionRef_REC"
(
  rib_oid number
, promo_id number
, promo_comp_id number
, promo_dtl_id number
, LocationListRef_TBL "RIB_LocationListRef_TBL"
, ListGroupRef_TBL "RIB_ListGroupRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_comp_id := promo_comp_id;
self.promo_dtl_id := promo_dtl_id;
self.LocationListRef_TBL := LocationListRef_TBL;
self.ListGroupRef_TBL := ListGroupRef_TBL;
RETURN;
end;
END;
/
