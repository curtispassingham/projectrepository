DROP TYPE "RIB_StrItmImgDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmImgDesc_REC";
/
DROP TYPE "RIB_StrItmImgDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmImgDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmImgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  store_id number(10),
  url varchar2(1000),
  type varchar2(20), -- type is enumeration field, valid values are [IMAGE, QR_CODE, UNKNOWN] (all lower-case)
  name varchar2(120),
  start_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmImgDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, url varchar2
, type varchar2
, name varchar2
) return self as result
,constructor function "RIB_StrItmImgDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, url varchar2
, type varchar2
, name varchar2
, start_date date
) return self as result
,constructor function "RIB_StrItmImgDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, url varchar2
, type varchar2
, name varchar2
, start_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmImgDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmImgDesc') := "ns_name_StrItmImgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'url') := url;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_StrItmImgDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, url varchar2
, type varchar2
, name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.url := url;
self.type := type;
self.name := name;
RETURN;
end;
constructor function "RIB_StrItmImgDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, url varchar2
, type varchar2
, name varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.url := url;
self.type := type;
self.name := name;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_StrItmImgDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, url varchar2
, type varchar2
, name varchar2
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.url := url;
self.type := type;
self.name := name;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
END;
/
