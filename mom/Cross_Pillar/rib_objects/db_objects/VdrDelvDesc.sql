DROP TYPE "RIB_VdrDelvDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvDesc_REC";
/
DROP TYPE "RIB_VdrDelvDescNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvDescNote_REC";
/
DROP TYPE "RIB_VdrDelvDescNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvDescNote_TBL" AS TABLE OF "RIB_VdrDelvDescNote_REC";
/
DROP TYPE "RIB_VdrDelvDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(12),
  store_id number(10),
  supplier_id number(10),
  supplier_name varchar2(240),
  origin_type varchar2(20), -- origin_type is enumeration field, valid values are [ASN, DEXNEX, MANUAL, PO, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, RECEIVED, CANCELED, REJECTED, UNKNOWN] (all lower-case)
  asn varchar2(128),
  purchase_order_ext_id varchar2(128),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  invoice_number varchar2(128),
  invoice_date date,
  invoice_cost number(12,4),
  carrier_type varchar2(20), -- carrier_type is enumeration field, valid values are [CORPORATE, THIRD_PARTY, NO_VALUE] (all lower-case)
  carrier_name varchar2(128),
  carrier_code varchar2(4),
  license_plate varchar2(128),
  source_address varchar2(1000),
  freight_id varchar2(128),
  bol_external_id varchar2(128),
  currency_code varchar2(3),
  country_code varchar2(3),
  expected_arrival_date date,
  create_date date,
  update_date date,
  received_date date,
  create_user varchar2(128),
  update_user varchar2(128),
  received_user varchar2(128),
  total_cartons number(10),
  total_items number(10),
  total_documents number(10),
  VdrDelvDescNote_TBL "RIB_VdrDelvDescNote_TBL",   -- Size of "RIB_VdrDelvDescNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, supplier_id number
, supplier_name varchar2
, origin_type varchar2
, status varchar2
, asn varchar2
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_type varchar2
, carrier_name varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
, currency_code varchar2
, country_code varchar2
, expected_arrival_date date
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, total_cartons number
, total_items number
, total_documents number
) return self as result
,constructor function "RIB_VdrDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, supplier_id number
, supplier_name varchar2
, origin_type varchar2
, status varchar2
, asn varchar2
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_type varchar2
, carrier_name varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
, currency_code varchar2
, country_code varchar2
, expected_arrival_date date
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, total_cartons number
, total_items number
, total_documents number
, VdrDelvDescNote_TBL "RIB_VdrDelvDescNote_TBL"  -- Size of "RIB_VdrDelvDescNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvDesc') := "ns_name_VdrDelvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_name') := supplier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_type') := origin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_order_ext_id') := purchase_order_ext_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_number') := invoice_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_date') := invoice_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_cost') := invoice_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_type') := carrier_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'license_plate') := license_plate;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_address') := source_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_id') := freight_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_external_id') := bol_external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_code') := country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_arrival_date') := expected_arrival_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_date') := received_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_user') := received_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cartons') := total_cartons;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_items') := total_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_documents') := total_documents;
  IF VdrDelvDescNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDelvDescNote_TBL.';
    FOR INDX IN VdrDelvDescNote_TBL.FIRST()..VdrDelvDescNote_TBL.LAST() LOOP
      VdrDelvDescNote_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDelvDescNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VdrDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, supplier_id number
, supplier_name varchar2
, origin_type varchar2
, status varchar2
, asn varchar2
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_type varchar2
, carrier_name varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
, currency_code varchar2
, country_code varchar2
, expected_arrival_date date
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, total_cartons number
, total_items number
, total_documents number
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.supplier_name := supplier_name;
self.origin_type := origin_type;
self.status := status;
self.asn := asn;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_type := carrier_type;
self.carrier_name := carrier_name;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
self.source_address := source_address;
self.freight_id := freight_id;
self.bol_external_id := bol_external_id;
self.currency_code := currency_code;
self.country_code := country_code;
self.expected_arrival_date := expected_arrival_date;
self.create_date := create_date;
self.update_date := update_date;
self.received_date := received_date;
self.create_user := create_user;
self.update_user := update_user;
self.received_user := received_user;
self.total_cartons := total_cartons;
self.total_items := total_items;
self.total_documents := total_documents;
RETURN;
end;
constructor function "RIB_VdrDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, supplier_id number
, supplier_name varchar2
, origin_type varchar2
, status varchar2
, asn varchar2
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_type varchar2
, carrier_name varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
, currency_code varchar2
, country_code varchar2
, expected_arrival_date date
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, total_cartons number
, total_items number
, total_documents number
, VdrDelvDescNote_TBL "RIB_VdrDelvDescNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.supplier_name := supplier_name;
self.origin_type := origin_type;
self.status := status;
self.asn := asn;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_type := carrier_type;
self.carrier_name := carrier_name;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
self.source_address := source_address;
self.freight_id := freight_id;
self.bol_external_id := bol_external_id;
self.currency_code := currency_code;
self.country_code := country_code;
self.expected_arrival_date := expected_arrival_date;
self.create_date := create_date;
self.update_date := update_date;
self.received_date := received_date;
self.create_user := create_user;
self.update_user := update_user;
self.received_user := received_user;
self.total_cartons := total_cartons;
self.total_items := total_items;
self.total_documents := total_documents;
self.VdrDelvDescNote_TBL := VdrDelvDescNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_VdrDelvDescNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvDescNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvDescNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvDesc') := "ns_name_VdrDelvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_VdrDelvDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
