DROP TYPE "RIB_ISCDimWHRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ISCDimWHRef_REC";
/
DROP TYPE "RIB_ISCDimWHRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ISCDimWHRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ISCDimWHRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  itemid varchar2(25),
  from_location varchar2(10),
  to_location varchar2(10),
  vendornbr varchar2(4000),
  origincountryid varchar2(4000),
  dimobject varchar2(4000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
) return self as result
,constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
) return self as result
,constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
) return self as result
,constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
, vendornbr varchar2
) return self as result
,constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
, vendornbr varchar2
, origincountryid varchar2
) return self as result
,constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
, vendornbr varchar2
, origincountryid varchar2
, dimobject varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ISCDimWHRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ISCDimWHRef') := "ns_name_ISCDimWHRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'itemid') := itemid;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'vendornbr') := vendornbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'origincountryid') := origincountryid;
  rib_obj_util.g_RIB_element_values(i_prefix||'dimobject') := dimobject;
END appendNodeValues;
constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.itemid := itemid;
RETURN;
end;
constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.itemid := itemid;
self.from_location := from_location;
RETURN;
end;
constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.itemid := itemid;
self.from_location := from_location;
self.to_location := to_location;
RETURN;
end;
constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
, vendornbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.itemid := itemid;
self.from_location := from_location;
self.to_location := to_location;
self.vendornbr := vendornbr;
RETURN;
end;
constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
, vendornbr varchar2
, origincountryid varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.itemid := itemid;
self.from_location := from_location;
self.to_location := to_location;
self.vendornbr := vendornbr;
self.origincountryid := origincountryid;
RETURN;
end;
constructor function "RIB_ISCDimWHRef_REC"
(
  rib_oid number
, itemid varchar2
, from_location varchar2
, to_location varchar2
, vendornbr varchar2
, origincountryid varchar2
, dimobject varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.itemid := itemid;
self.from_location := from_location;
self.to_location := to_location;
self.vendornbr := vendornbr;
self.origincountryid := origincountryid;
self.dimobject := dimobject;
RETURN;
end;
END;
/
