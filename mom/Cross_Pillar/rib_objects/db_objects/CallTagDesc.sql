DROP TYPE "RIB_CallTagDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CallTagDtl_REC";
/
DROP TYPE "RIB_CallTagDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CallTagDesc_REC";
/
DROP TYPE "RIB_CallTagDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CallTagDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CallTagDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_number varchar2(25),
  item_desc varchar2(250),
  quantity number(12),
  merchandise_value number(18),
  return_line_nbr number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CallTagDtl_REC"
(
  rib_oid number
, item_number varchar2
, item_desc varchar2
, quantity number
, merchandise_value number
, return_line_nbr number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CallTagDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CallTagDesc') := "ns_name_CallTagDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_number') := item_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_desc') := item_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'merchandise_value') := merchandise_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_line_nbr') := return_line_nbr;
END appendNodeValues;
constructor function "RIB_CallTagDtl_REC"
(
  rib_oid number
, item_number varchar2
, item_desc varchar2
, quantity number
, merchandise_value number
, return_line_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_number := item_number;
self.item_desc := item_desc;
self.quantity := quantity;
self.merchandise_value := merchandise_value;
self.return_line_nbr := return_line_nbr;
RETURN;
end;
END;
/
DROP TYPE "RIB_CallTagDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CallTagDtl_TBL" AS TABLE OF "RIB_CallTagDtl_REC";
/
DROP TYPE "RIB_CallTagDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CallTagDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CallTagDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  call_tag_id number(12),
  carrier_id number(12),
  cust_name varchar2(40),
  cust_day_phone_nbr varchar2(30),
  cust_day_phone_ext varchar2(10),
  cust_eve_phone_nbr varchar2(30),
  cust_eve_phone_ext varchar2(10),
  cust_email_addr varchar2(100),
  cust_comment varchar2(1000),
  cust_order_display_nbr varchar2(30),
  number_of_boxes number(6),
  pickup_addr_line1 varchar2(30),
  pickup_addr_line2 varchar2(30),
  pickup_addr_line3 varchar2(30),
  pickup_addr_country_code varchar2(3),
  pickup_addr_postal_code varchar2(10),
  pickup_addr_city varchar2(25),
  pickup_addr_state varchar2(3),
  return_addr_attention varchar2(30),
  return_addr_line1 varchar2(30),
  return_addr_line2 varchar2(30),
  return_addr_line3 varchar2(30),
  return_addr_country_code varchar2(3),
  return_addr_postal_code varchar2(10),
  return_addr_city varchar2(25),
  return_addr_state varchar2(3),
  return_reason_desc varchar2(1000),
  created_by number(20),
  create_date date,
  CallTagDtl_TBL "RIB_CallTagDtl_TBL",   -- Size of "RIB_CallTagDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CallTagDesc_REC"
(
  rib_oid number
, call_tag_id number
, carrier_id number
, cust_name varchar2
, cust_day_phone_nbr varchar2
, cust_day_phone_ext varchar2
, cust_eve_phone_nbr varchar2
, cust_eve_phone_ext varchar2
, cust_email_addr varchar2
, cust_comment varchar2
, cust_order_display_nbr varchar2
, number_of_boxes number
, pickup_addr_line1 varchar2
, pickup_addr_line2 varchar2
, pickup_addr_line3 varchar2
, pickup_addr_country_code varchar2
, pickup_addr_postal_code varchar2
, pickup_addr_city varchar2
, pickup_addr_state varchar2
, return_addr_attention varchar2
, return_addr_line1 varchar2
, return_addr_line2 varchar2
, return_addr_line3 varchar2
, return_addr_country_code varchar2
, return_addr_postal_code varchar2
, return_addr_city varchar2
, return_addr_state varchar2
, return_reason_desc varchar2
, created_by number
, create_date date
, CallTagDtl_TBL "RIB_CallTagDtl_TBL"  -- Size of "RIB_CallTagDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CallTagDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CallTagDesc') := "ns_name_CallTagDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'call_tag_id') := call_tag_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_id') := carrier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_name') := cust_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_day_phone_nbr') := cust_day_phone_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_day_phone_ext') := cust_day_phone_ext;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_eve_phone_nbr') := cust_eve_phone_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_eve_phone_ext') := cust_eve_phone_ext;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_email_addr') := cust_email_addr;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_comment') := cust_comment;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_display_nbr') := cust_order_display_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_boxes') := number_of_boxes;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_line1') := pickup_addr_line1;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_line2') := pickup_addr_line2;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_line3') := pickup_addr_line3;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_country_code') := pickup_addr_country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_postal_code') := pickup_addr_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_city') := pickup_addr_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_addr_state') := pickup_addr_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_attention') := return_addr_attention;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_line1') := return_addr_line1;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_line2') := return_addr_line2;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_line3') := return_addr_line3;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_country_code') := return_addr_country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_postal_code') := return_addr_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_city') := return_addr_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_addr_state') := return_addr_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_reason_desc') := return_reason_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'created_by') := created_by;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  l_new_pre :=i_prefix||'CallTagDtl_TBL.';
  FOR INDX IN CallTagDtl_TBL.FIRST()..CallTagDtl_TBL.LAST() LOOP
    CallTagDtl_TBL(indx).appendNodeValues( i_prefix||indx||'CallTagDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_CallTagDesc_REC"
(
  rib_oid number
, call_tag_id number
, carrier_id number
, cust_name varchar2
, cust_day_phone_nbr varchar2
, cust_day_phone_ext varchar2
, cust_eve_phone_nbr varchar2
, cust_eve_phone_ext varchar2
, cust_email_addr varchar2
, cust_comment varchar2
, cust_order_display_nbr varchar2
, number_of_boxes number
, pickup_addr_line1 varchar2
, pickup_addr_line2 varchar2
, pickup_addr_line3 varchar2
, pickup_addr_country_code varchar2
, pickup_addr_postal_code varchar2
, pickup_addr_city varchar2
, pickup_addr_state varchar2
, return_addr_attention varchar2
, return_addr_line1 varchar2
, return_addr_line2 varchar2
, return_addr_line3 varchar2
, return_addr_country_code varchar2
, return_addr_postal_code varchar2
, return_addr_city varchar2
, return_addr_state varchar2
, return_reason_desc varchar2
, created_by number
, create_date date
, CallTagDtl_TBL "RIB_CallTagDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.call_tag_id := call_tag_id;
self.carrier_id := carrier_id;
self.cust_name := cust_name;
self.cust_day_phone_nbr := cust_day_phone_nbr;
self.cust_day_phone_ext := cust_day_phone_ext;
self.cust_eve_phone_nbr := cust_eve_phone_nbr;
self.cust_eve_phone_ext := cust_eve_phone_ext;
self.cust_email_addr := cust_email_addr;
self.cust_comment := cust_comment;
self.cust_order_display_nbr := cust_order_display_nbr;
self.number_of_boxes := number_of_boxes;
self.pickup_addr_line1 := pickup_addr_line1;
self.pickup_addr_line2 := pickup_addr_line2;
self.pickup_addr_line3 := pickup_addr_line3;
self.pickup_addr_country_code := pickup_addr_country_code;
self.pickup_addr_postal_code := pickup_addr_postal_code;
self.pickup_addr_city := pickup_addr_city;
self.pickup_addr_state := pickup_addr_state;
self.return_addr_attention := return_addr_attention;
self.return_addr_line1 := return_addr_line1;
self.return_addr_line2 := return_addr_line2;
self.return_addr_line3 := return_addr_line3;
self.return_addr_country_code := return_addr_country_code;
self.return_addr_postal_code := return_addr_postal_code;
self.return_addr_city := return_addr_city;
self.return_addr_state := return_addr_state;
self.return_reason_desc := return_reason_desc;
self.created_by := created_by;
self.create_date := create_date;
self.CallTagDtl_TBL := CallTagDtl_TBL;
RETURN;
end;
END;
/
