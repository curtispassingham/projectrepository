DROP TYPE "RIB_StsTsfHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfHdrCriVo_REC";
/
DROP TYPE "RIB_StsTsfHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  tsf_id number(15),
  ext_tsf_id varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW_REQUEST, REQUESTED, REQUEST_IN_PROGRESS, REJECTED, CANCELED_REQUEST, TRANSFER_IN_PROGRESS, APPROVED, IN_SHIPPING, COMPLETED, CANCELED, ACTIVE, NO_VALUE] (all lower-case)
  src_loc_type varchar2(20), -- src_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, NO_VALUE] (all lower-case)
  src_loc_id number(10),
  dest_loc_type varchar2(20), -- dest_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, NO_VALUE] (all lower-case)
  dest_loc_id number(10),
  from_update_date date,
  to_update_date date,
  item_id varchar2(250),
  update_user varchar2(250),
  context_type_id varchar2(15),
  context_value varchar2(25),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  all_customer_orders varchar2(5), --all_customer_orders is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfHdrCriVo_REC"
(
  rib_oid number
, store_id number
, tsf_id number
, ext_tsf_id varchar2
, status varchar2
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, from_update_date date
, to_update_date date
, item_id varchar2
, update_user varchar2
, context_type_id varchar2
, context_value varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, all_customer_orders varchar2  --all_customer_orders is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfHdrCriVo') := "ns_name_StsTsfHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_id') := tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_tsf_id') := ext_tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_type') := src_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_id') := src_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_type') := dest_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_id') := dest_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_update_date') := from_update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_update_date') := to_update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'all_customer_orders') := all_customer_orders;
END appendNodeValues;
constructor function "RIB_StsTsfHdrCriVo_REC"
(
  rib_oid number
, store_id number
, tsf_id number
, ext_tsf_id varchar2
, status varchar2
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, from_update_date date
, to_update_date date
, item_id varchar2
, update_user varchar2
, context_type_id varchar2
, context_value varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, all_customer_orders varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.tsf_id := tsf_id;
self.ext_tsf_id := ext_tsf_id;
self.status := status;
self.src_loc_type := src_loc_type;
self.src_loc_id := src_loc_id;
self.dest_loc_type := dest_loc_type;
self.dest_loc_id := dest_loc_id;
self.from_update_date := from_update_date;
self.to_update_date := to_update_date;
self.item_id := item_id;
self.update_user := update_user;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.all_customer_orders := all_customer_orders;
RETURN;
end;
END;
/
