DROP TYPE "RIB_InvReqItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvReqItem_REC";
/
DROP TYPE "RIB_InvReqDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvReqDesc_REC";
/
DROP TYPE "RIB_InvReqItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvReqItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvReqDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  qty_rqst number(12,4),
  uop varchar2(6),
  need_date date,
  delivery_slot_id varchar2(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvReqItem_REC"
(
  rib_oid number
, item varchar2
, qty_rqst number
, uop varchar2
, need_date date
) return self as result
,constructor function "RIB_InvReqItem_REC"
(
  rib_oid number
, item varchar2
, qty_rqst number
, uop varchar2
, need_date date
, delivery_slot_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvReqItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvReqDesc') := "ns_name_InvReqDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_rqst') := qty_rqst;
  rib_obj_util.g_RIB_element_values(i_prefix||'uop') := uop;
  rib_obj_util.g_RIB_element_values(i_prefix||'need_date') := need_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_slot_id') := delivery_slot_id;
END appendNodeValues;
constructor function "RIB_InvReqItem_REC"
(
  rib_oid number
, item varchar2
, qty_rqst number
, uop varchar2
, need_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_rqst := qty_rqst;
self.uop := uop;
self.need_date := need_date;
RETURN;
end;
constructor function "RIB_InvReqItem_REC"
(
  rib_oid number
, item varchar2
, qty_rqst number
, uop varchar2
, need_date date
, delivery_slot_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_rqst := qty_rqst;
self.uop := uop;
self.need_date := need_date;
self.delivery_slot_id := delivery_slot_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_InvReqItem_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InvReqItem_TBL" AS TABLE OF "RIB_InvReqItem_REC";
/
DROP TYPE "RIB_InvReqDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvReqDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvReqDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  request_id number(10),
  store number(10),
  request_type varchar2(2),
  InvReqItem_TBL "RIB_InvReqItem_TBL",   -- Size of "RIB_InvReqItem_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvReqDesc_REC"
(
  rib_oid number
, request_id number
, store number
, request_type varchar2
, InvReqItem_TBL "RIB_InvReqItem_TBL"  -- Size of "RIB_InvReqItem_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvReqDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvReqDesc') := "ns_name_InvReqDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'request_id') := request_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_type') := request_type;
  l_new_pre :=i_prefix||'InvReqItem_TBL.';
  FOR INDX IN InvReqItem_TBL.FIRST()..InvReqItem_TBL.LAST() LOOP
    InvReqItem_TBL(indx).appendNodeValues( i_prefix||indx||'InvReqItem_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_InvReqDesc_REC"
(
  rib_oid number
, request_id number
, store number
, request_type varchar2
, InvReqItem_TBL "RIB_InvReqItem_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.request_id := request_id;
self.store := store;
self.request_type := request_type;
self.InvReqItem_TBL := InvReqItem_TBL;
RETURN;
end;
END;
/
