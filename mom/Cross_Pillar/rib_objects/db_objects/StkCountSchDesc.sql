DROP TYPE "RIB_StkCountSchLoc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCountSchLoc_REC";
/
DROP TYPE "RIB_StkCountSchProd_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCountSchProd_REC";
/
DROP TYPE "RIB_StkCountSchDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCountSchDesc_REC";
/
DROP TYPE "RIB_StkCountSchLoc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCountSchLoc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCountSchDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCountSchLoc_REC"
(
  rib_oid number
, location number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCountSchLoc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCountSchDesc') := "ns_name_StkCountSchDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
END appendNodeValues;
constructor function "RIB_StkCountSchLoc_REC"
(
  rib_oid number
, location number
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
RETURN;
end;
END;
/
DROP TYPE "RIB_StkCountSchProd_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCountSchProd_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCountSchDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dept number(4),
  class number(4),
  subclass number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCountSchProd_REC"
(
  rib_oid number
, dept number
) return self as result
,constructor function "RIB_StkCountSchProd_REC"
(
  rib_oid number
, dept number
, class number
) return self as result
,constructor function "RIB_StkCountSchProd_REC"
(
  rib_oid number
, dept number
, class number
, subclass number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCountSchProd_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCountSchDesc') := "ns_name_StkCountSchDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'class') := class;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass') := subclass;
END appendNodeValues;
constructor function "RIB_StkCountSchProd_REC"
(
  rib_oid number
, dept number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dept := dept;
RETURN;
end;
constructor function "RIB_StkCountSchProd_REC"
(
  rib_oid number
, dept number
, class number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dept := dept;
self.class := class;
RETURN;
end;
constructor function "RIB_StkCountSchProd_REC"
(
  rib_oid number
, dept number
, class number
, subclass number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dept := dept;
self.class := class;
self.subclass := subclass;
RETURN;
end;
END;
/
DROP TYPE "RIB_StkCountSchProd_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCountSchProd_TBL" AS TABLE OF "RIB_StkCountSchProd_REC";
/
DROP TYPE "RIB_StkCountSchLoc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCountSchLoc_TBL" AS TABLE OF "RIB_StkCountSchLoc_REC";
/
DROP TYPE "RIB_StkCountSchDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCountSchDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCountSchDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  cycle_count number(8),
  cycle_count_desc varchar2(250),
  location_type varchar2(1),
  stocktake_date date,
  stocktake_type varchar2(1),
  StkCountSchProd_TBL "RIB_StkCountSchProd_TBL",   -- Size of "RIB_StkCountSchProd_TBL" is unbounded
  StkCountSchLoc_TBL "RIB_StkCountSchLoc_TBL",   -- Size of "RIB_StkCountSchLoc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCountSchDesc_REC"
(
  rib_oid number
, cycle_count number
, cycle_count_desc varchar2
, location_type varchar2
, stocktake_date date
, stocktake_type varchar2
) return self as result
,constructor function "RIB_StkCountSchDesc_REC"
(
  rib_oid number
, cycle_count number
, cycle_count_desc varchar2
, location_type varchar2
, stocktake_date date
, stocktake_type varchar2
, StkCountSchProd_TBL "RIB_StkCountSchProd_TBL"  -- Size of "RIB_StkCountSchProd_TBL" is unbounded
) return self as result
,constructor function "RIB_StkCountSchDesc_REC"
(
  rib_oid number
, cycle_count number
, cycle_count_desc varchar2
, location_type varchar2
, stocktake_date date
, stocktake_type varchar2
, StkCountSchProd_TBL "RIB_StkCountSchProd_TBL"  -- Size of "RIB_StkCountSchProd_TBL" is unbounded
, StkCountSchLoc_TBL "RIB_StkCountSchLoc_TBL"  -- Size of "RIB_StkCountSchLoc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCountSchDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCountSchDesc') := "ns_name_StkCountSchDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'cycle_count') := cycle_count;
  rib_obj_util.g_RIB_element_values(i_prefix||'cycle_count_desc') := cycle_count_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'location_type') := location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stocktake_date') := stocktake_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'stocktake_type') := stocktake_type;
  IF StkCountSchProd_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StkCountSchProd_TBL.';
    FOR INDX IN StkCountSchProd_TBL.FIRST()..StkCountSchProd_TBL.LAST() LOOP
      StkCountSchProd_TBL(indx).appendNodeValues( i_prefix||indx||'StkCountSchProd_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StkCountSchLoc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StkCountSchLoc_TBL.';
    FOR INDX IN StkCountSchLoc_TBL.FIRST()..StkCountSchLoc_TBL.LAST() LOOP
      StkCountSchLoc_TBL(indx).appendNodeValues( i_prefix||indx||'StkCountSchLoc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StkCountSchDesc_REC"
(
  rib_oid number
, cycle_count number
, cycle_count_desc varchar2
, location_type varchar2
, stocktake_date date
, stocktake_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cycle_count := cycle_count;
self.cycle_count_desc := cycle_count_desc;
self.location_type := location_type;
self.stocktake_date := stocktake_date;
self.stocktake_type := stocktake_type;
RETURN;
end;
constructor function "RIB_StkCountSchDesc_REC"
(
  rib_oid number
, cycle_count number
, cycle_count_desc varchar2
, location_type varchar2
, stocktake_date date
, stocktake_type varchar2
, StkCountSchProd_TBL "RIB_StkCountSchProd_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.cycle_count := cycle_count;
self.cycle_count_desc := cycle_count_desc;
self.location_type := location_type;
self.stocktake_date := stocktake_date;
self.stocktake_type := stocktake_type;
self.StkCountSchProd_TBL := StkCountSchProd_TBL;
RETURN;
end;
constructor function "RIB_StkCountSchDesc_REC"
(
  rib_oid number
, cycle_count number
, cycle_count_desc varchar2
, location_type varchar2
, stocktake_date date
, stocktake_type varchar2
, StkCountSchProd_TBL "RIB_StkCountSchProd_TBL"
, StkCountSchLoc_TBL "RIB_StkCountSchLoc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.cycle_count := cycle_count;
self.cycle_count_desc := cycle_count_desc;
self.location_type := location_type;
self.stocktake_date := stocktake_date;
self.stocktake_type := stocktake_type;
self.StkCountSchProd_TBL := StkCountSchProd_TBL;
self.StkCountSchLoc_TBL := StkCountSchLoc_TBL;
RETURN;
end;
END;
/
