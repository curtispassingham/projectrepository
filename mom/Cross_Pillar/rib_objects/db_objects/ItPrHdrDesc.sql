DROP TYPE "RIB_ItPrHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItPrHdrDesc_REC";
/
DROP TYPE "RIB_ItPrHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItPrHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItPrHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_price_id number(12),
  external_id number(15),
  effective_date date,
  end_date date,
  item_id varchar2(25),
  current_price_cur varchar2(3),
  current_price_value number(12,4),
  current_selling_uom varchar2(4),
  new_price_cur varchar2(3),
  new_price_value number(12,4),
  new_selling_uom varchar2(4),
  multi_unit_price_cur varchar2(3),
  multi_unit_price_value number(12,4),
  multi_unit_quantity number(12,4),
  multi_unit_selling_uom varchar2(4),
  multi_unit_price_change varchar2(5), --multi_unit_price_change is boolean field, valid values are true,false (all lower-case) 
  suggest_retail_cur varchar2(3),
  suggest_retail_value number(12,4),
  status varchar2(20), -- status is enumeration field, valid values are [APPROVED, PENDING, ACTIVE, COMPLETED, REJECTED, TICKET_LIST, NEW, DEFAULT, DELETED, NO_VALUE, UNKNOWN] (all lower-case)
  price_type varchar2(20), -- price_type is enumeration field, valid values are [CLEARANCE, PROMOTIONAL, PERMANENT, NO_VALUE, UNKNOWN] (all lower-case)
  promotion_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItPrHdrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, effective_date date
, end_date date
, item_id varchar2
, current_price_cur varchar2
, current_price_value number
, current_selling_uom varchar2
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, multi_unit_price_cur varchar2
, multi_unit_price_value number
, multi_unit_quantity number
, multi_unit_selling_uom varchar2
, multi_unit_price_change varchar2  --multi_unit_price_change is boolean field, valid values are true,false (all lower-case)
, suggest_retail_cur varchar2
, suggest_retail_value number
, status varchar2
, price_type varchar2
) return self as result
,constructor function "RIB_ItPrHdrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, effective_date date
, end_date date
, item_id varchar2
, current_price_cur varchar2
, current_price_value number
, current_selling_uom varchar2
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, multi_unit_price_cur varchar2
, multi_unit_price_value number
, multi_unit_quantity number
, multi_unit_selling_uom varchar2
, multi_unit_price_change varchar2  --multi_unit_price_change is boolean field, valid values are true,false (all lower-case)
, suggest_retail_cur varchar2
, suggest_retail_value number
, status varchar2
, price_type varchar2
, promotion_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItPrHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItPrHdrDesc') := "ns_name_ItPrHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_price_id') := item_price_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'current_price_cur') := current_price_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'current_price_value') := current_price_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'current_selling_uom') := current_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_price_cur') := new_price_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_price_value') := new_price_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_selling_uom') := new_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_price_cur') := multi_unit_price_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_price_value') := multi_unit_price_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_quantity') := multi_unit_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_selling_uom') := multi_unit_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_price_change') := multi_unit_price_change;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggest_retail_cur') := suggest_retail_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggest_retail_value') := suggest_retail_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_type') := price_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
END appendNodeValues;
constructor function "RIB_ItPrHdrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, effective_date date
, end_date date
, item_id varchar2
, current_price_cur varchar2
, current_price_value number
, current_selling_uom varchar2
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, multi_unit_price_cur varchar2
, multi_unit_price_value number
, multi_unit_quantity number
, multi_unit_selling_uom varchar2
, multi_unit_price_change varchar2
, suggest_retail_cur varchar2
, suggest_retail_value number
, status varchar2
, price_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_price_id := item_price_id;
self.external_id := external_id;
self.effective_date := effective_date;
self.end_date := end_date;
self.item_id := item_id;
self.current_price_cur := current_price_cur;
self.current_price_value := current_price_value;
self.current_selling_uom := current_selling_uom;
self.new_price_cur := new_price_cur;
self.new_price_value := new_price_value;
self.new_selling_uom := new_selling_uom;
self.multi_unit_price_cur := multi_unit_price_cur;
self.multi_unit_price_value := multi_unit_price_value;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.multi_unit_price_change := multi_unit_price_change;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.status := status;
self.price_type := price_type;
RETURN;
end;
constructor function "RIB_ItPrHdrDesc_REC"
(
  rib_oid number
, item_price_id number
, external_id number
, effective_date date
, end_date date
, item_id varchar2
, current_price_cur varchar2
, current_price_value number
, current_selling_uom varchar2
, new_price_cur varchar2
, new_price_value number
, new_selling_uom varchar2
, multi_unit_price_cur varchar2
, multi_unit_price_value number
, multi_unit_quantity number
, multi_unit_selling_uom varchar2
, multi_unit_price_change varchar2
, suggest_retail_cur varchar2
, suggest_retail_value number
, status varchar2
, price_type varchar2
, promotion_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_price_id := item_price_id;
self.external_id := external_id;
self.effective_date := effective_date;
self.end_date := end_date;
self.item_id := item_id;
self.current_price_cur := current_price_cur;
self.current_price_value := current_price_value;
self.current_selling_uom := current_selling_uom;
self.new_price_cur := new_price_cur;
self.new_price_value := new_price_value;
self.new_selling_uom := new_selling_uom;
self.multi_unit_price_cur := multi_unit_price_cur;
self.multi_unit_price_value := multi_unit_price_value;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.multi_unit_price_change := multi_unit_price_change;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.status := status;
self.price_type := price_type;
self.promotion_id := promotion_id;
RETURN;
end;
END;
/
