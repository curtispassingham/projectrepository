DROP TYPE "RIB_XCostChgHrDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XCostChgHrDtl_REC";
/
DROP TYPE "RIB_XCostChgDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XCostChgDesc_REC";
/
DROP TYPE "RIB_XCostChgHrDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XCostChgHrDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XCostChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_value number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XCostChgHrDtl_REC"
(
  rib_oid number
, hier_value number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XCostChgHrDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XCostChgDesc') := "ns_name_XCostChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_value') := hier_value;
END appendNodeValues;
constructor function "RIB_XCostChgHrDtl_REC"
(
  rib_oid number
, hier_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
RETURN;
end;
END;
/
DROP TYPE "RIB_XCostChgHrDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XCostChgHrDtl_TBL" AS TABLE OF "RIB_XCostChgHrDtl_REC";
/
DROP TYPE "RIB_XCostChgDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XCostChgDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XCostChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  supplier number(10),
  origin_country_id varchar2(3),
  diff_id varchar2(10),
  unit_cost number(20,4),
  recalc_ord_ind varchar2(1),
  currency_code varchar2(3),
  hier_level varchar2(2),
  XCostChgHrDtl_TBL "RIB_XCostChgHrDtl_TBL",   -- Size of "RIB_XCostChgHrDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XCostChgDesc_REC"
(
  rib_oid number
, item varchar2
, supplier number
, origin_country_id varchar2
, diff_id varchar2
, unit_cost number
, recalc_ord_ind varchar2
, currency_code varchar2
) return self as result
,constructor function "RIB_XCostChgDesc_REC"
(
  rib_oid number
, item varchar2
, supplier number
, origin_country_id varchar2
, diff_id varchar2
, unit_cost number
, recalc_ord_ind varchar2
, currency_code varchar2
, hier_level varchar2
) return self as result
,constructor function "RIB_XCostChgDesc_REC"
(
  rib_oid number
, item varchar2
, supplier number
, origin_country_id varchar2
, diff_id varchar2
, unit_cost number
, recalc_ord_ind varchar2
, currency_code varchar2
, hier_level varchar2
, XCostChgHrDtl_TBL "RIB_XCostChgHrDtl_TBL"  -- Size of "RIB_XCostChgHrDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XCostChgDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XCostChgDesc') := "ns_name_XCostChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_id') := diff_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'recalc_ord_ind') := recalc_ord_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_level') := hier_level;
  IF XCostChgHrDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XCostChgHrDtl_TBL.';
    FOR INDX IN XCostChgHrDtl_TBL.FIRST()..XCostChgHrDtl_TBL.LAST() LOOP
      XCostChgHrDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XCostChgHrDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XCostChgDesc_REC"
(
  rib_oid number
, item varchar2
, supplier number
, origin_country_id varchar2
, diff_id varchar2
, unit_cost number
, recalc_ord_ind varchar2
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.diff_id := diff_id;
self.unit_cost := unit_cost;
self.recalc_ord_ind := recalc_ord_ind;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_XCostChgDesc_REC"
(
  rib_oid number
, item varchar2
, supplier number
, origin_country_id varchar2
, diff_id varchar2
, unit_cost number
, recalc_ord_ind varchar2
, currency_code varchar2
, hier_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.diff_id := diff_id;
self.unit_cost := unit_cost;
self.recalc_ord_ind := recalc_ord_ind;
self.currency_code := currency_code;
self.hier_level := hier_level;
RETURN;
end;
constructor function "RIB_XCostChgDesc_REC"
(
  rib_oid number
, item varchar2
, supplier number
, origin_country_id varchar2
, diff_id varchar2
, unit_cost number
, recalc_ord_ind varchar2
, currency_code varchar2
, hier_level varchar2
, XCostChgHrDtl_TBL "RIB_XCostChgHrDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.diff_id := diff_id;
self.unit_cost := unit_cost;
self.recalc_ord_ind := recalc_ord_ind;
self.currency_code := currency_code;
self.hier_level := hier_level;
self.XCostChgHrDtl_TBL := XCostChgHrDtl_TBL;
RETURN;
end;
END;
/
