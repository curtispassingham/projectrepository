DROP TYPE "RIB_TaxLineRtVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxLineRtVo_REC";
/
DROP TYPE "RIB_TaxLineRtVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TaxLineRtVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TaxLineRtVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_no number(4),
  currency_code varchar2(3),
  returned_tax_amount number(20,4),
  inclusive_tax_flag varchar2(1), -- inclusive_tax_flag is enumeration field, valid values are [Y, N] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TaxLineRtVo_REC"
(
  rib_oid number
, line_no number
, currency_code varchar2
, returned_tax_amount number
, inclusive_tax_flag varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TaxLineRtVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TaxLineRtVo') := "ns_name_TaxLineRtVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_no') := line_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_tax_amount') := returned_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'inclusive_tax_flag') := inclusive_tax_flag;
END appendNodeValues;
constructor function "RIB_TaxLineRtVo_REC"
(
  rib_oid number
, line_no number
, currency_code varchar2
, returned_tax_amount number
, inclusive_tax_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.currency_code := currency_code;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
RETURN;
end;
END;
/
