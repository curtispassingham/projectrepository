DROP TYPE "RIB_StrVrnHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnHdrCriVo_REC";
/
DROP TYPE "RIB_StrVrnHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVrnHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVrnHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  item_id varchar2(25),
  return_id number(12),
  external_id varchar2(128),
  authorization_number varchar2(12),
  reason_id number(12),
  status varchar2(20), -- status is enumeration field, valid values are [REQUESTED, REQUEST_IN_PROGRESS, RTV_IN_PROGRESS, APPROVED, IN_SHIPPING, COMPLETED, REJECTED, CANCELED_REQUEST, CANCELED_RTV, ACTIVE, NO_VALUE] (all lower-case)
  supplier_id varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVrnHdrCriVo_REC"
(
  rib_oid number
, store_id number
, item_id varchar2
, return_id number
, external_id varchar2
, authorization_number varchar2
, reason_id number
, status varchar2
) return self as result
,constructor function "RIB_StrVrnHdrCriVo_REC"
(
  rib_oid number
, store_id number
, item_id varchar2
, return_id number
, external_id varchar2
, authorization_number varchar2
, reason_id number
, status varchar2
, supplier_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVrnHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVrnHdrCriVo') := "ns_name_StrVrnHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_id') := return_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_number') := authorization_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
END appendNodeValues;
constructor function "RIB_StrVrnHdrCriVo_REC"
(
  rib_oid number
, store_id number
, item_id varchar2
, return_id number
, external_id varchar2
, authorization_number varchar2
, reason_id number
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.item_id := item_id;
self.return_id := return_id;
self.external_id := external_id;
self.authorization_number := authorization_number;
self.reason_id := reason_id;
self.status := status;
RETURN;
end;
constructor function "RIB_StrVrnHdrCriVo_REC"
(
  rib_oid number
, store_id number
, item_id varchar2
, return_id number
, external_id varchar2
, authorization_number varchar2
, reason_id number
, status varchar2
, supplier_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.item_id := item_id;
self.return_id := return_id;
self.external_id := external_id;
self.authorization_number := authorization_number;
self.reason_id := reason_id;
self.status := status;
self.supplier_id := supplier_id;
RETURN;
end;
END;
/
