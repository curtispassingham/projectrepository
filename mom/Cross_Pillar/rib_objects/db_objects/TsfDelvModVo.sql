DROP TYPE "RIB_TsfDelvModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvModVo_REC";
/
DROP TYPE "RIB_TsfDelvModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(15),
  store_id number(10),
  carrier_name varchar2(128),
  carrier_type varchar2(20), -- carrier_type is enumeration field, valid values are [CORPORATE, THIRD_PARTY, UNKNOWN] (all lower-case)
  carrier_code varchar2(4),
  source_address varchar2(1000),
  license_plate varchar2(128),
  freight_id varchar2(128),
  external_bol varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
) return self as result
,constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
) return self as result
,constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
) return self as result
,constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
) return self as result
,constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
) return self as result
,constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
, external_bol varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvModVo') := "ns_name_TsfDelvModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_type') := carrier_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_address') := source_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'license_plate') := license_plate;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_id') := freight_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_bol') := external_bol;
END appendNodeValues;
constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
RETURN;
end;
constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
RETURN;
end;
constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.source_address := source_address;
RETURN;
end;
constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.source_address := source_address;
self.license_plate := license_plate;
RETURN;
end;
constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.source_address := source_address;
self.license_plate := license_plate;
self.freight_id := freight_id;
RETURN;
end;
constructor function "RIB_TsfDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
, external_bol varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.source_address := source_address;
self.license_plate := license_plate;
self.freight_id := freight_id;
self.external_bol := external_bol;
RETURN;
end;
END;
/
