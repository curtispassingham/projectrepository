DROP TYPE "RIB_WOOutDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutDtlRef_REC";
/
DROP TYPE "RIB_WOOutRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutRef_REC";
/
DROP TYPE "RIB_WOOutDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dest_id varchar2(10),
  item_id varchar2(25),
  wip_seq_nbr number(7),
  order_line_nbr number(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutDtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
) return self as result
,constructor function "RIB_WOOutDtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
) return self as result
,constructor function "RIB_WOOutDtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, order_line_nbr number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutRef') := "ns_name_WOOutRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'wip_seq_nbr') := wip_seq_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_nbr') := order_line_nbr;
END appendNodeValues;
constructor function "RIB_WOOutDtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_WOOutDtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.item_id := item_id;
self.wip_seq_nbr := wip_seq_nbr;
RETURN;
end;
constructor function "RIB_WOOutDtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, order_line_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.item_id := item_id;
self.wip_seq_nbr := wip_seq_nbr;
self.order_line_nbr := order_line_nbr;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutDtlRef_TBL" AS TABLE OF "RIB_WOOutDtlRef_REC";
/
DROP TYPE "RIB_WOOutRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  wo_id number(15),
  dc_dest_id varchar2(10),
  distro_nbr varchar2(12),
  WOOutDtlRef_TBL "RIB_WOOutDtlRef_TBL",   -- Size of "RIB_WOOutDtlRef_TBL" is unbounded
  distro_parent_nbr varchar2(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutRef_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
) return self as result
,constructor function "RIB_WOOutRef_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtlRef_TBL "RIB_WOOutDtlRef_TBL"  -- Size of "RIB_WOOutDtlRef_TBL" is unbounded
) return self as result
,constructor function "RIB_WOOutRef_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtlRef_TBL "RIB_WOOutDtlRef_TBL"  -- Size of "RIB_WOOutDtlRef_TBL" is unbounded
, distro_parent_nbr varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutRef') := "ns_name_WOOutRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'wo_id') := wo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  IF WOOutDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WOOutDtlRef_TBL.';
    FOR INDX IN WOOutDtlRef_TBL.FIRST()..WOOutDtlRef_TBL.LAST() LOOP
      WOOutDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_parent_nbr') := distro_parent_nbr;
END appendNodeValues;
constructor function "RIB_WOOutRef_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
RETURN;
end;
constructor function "RIB_WOOutRef_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtlRef_TBL "RIB_WOOutDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtlRef_TBL := WOOutDtlRef_TBL;
RETURN;
end;
constructor function "RIB_WOOutRef_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtlRef_TBL "RIB_WOOutDtlRef_TBL"
, distro_parent_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtlRef_TBL := WOOutDtlRef_TBL;
self.distro_parent_nbr := distro_parent_nbr;
RETURN;
end;
END;
/
