@@GiftListItemColDesc.sql;
/
@@GeoAddrDesc.sql;
/
@@CustomerRef.sql;
/
DROP TYPE "RIB_GiftListDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftListDesc_REC";
/
DROP TYPE "RIB_GeoAddrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_GeoAddrDesc_TBL" AS TABLE OF "RIB_GeoAddrDesc_REC";
/
DROP TYPE "RIB_GiftListDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftListDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id varchar2(40),
  CustomerRef "RIB_CustomerRef_REC",
  name varchar2(64),
  event_type varchar2(12), -- event_type is enumeration field, valid values are [ANNIVERSARY, BABYSHOWER, BAPTISM, BARMITZVAH, BIRTHDAY, BRIDALSHOWER, CHANUKAH, CHRISTMAS, GRADUATION, NONEVENT, OTHER, ROSHHASHANAH, VALENTINES, WEDDING, YOMKIPPUR] (all lower-case)
  event_date date,
  published_flag varchar2(5), --published_flag is boolean field, valid values are true,false (all lower-case) 
  public_flag varchar2(5), --public_flag is boolean field, valid values are true,false (all lower-case) 
  location_created varchar2(40),
  comments varchar2(254),
  instructions varchar2(254),
  image_data varchar2(254),
  create_date date,
  update_date date,
  expiration_date date,
  create_user_id varchar2(40),
  update_user_id varchar2(40),
  GeoAddrDesc_TBL "RIB_GeoAddrDesc_TBL",   -- Size of "RIB_GeoAddrDesc_TBL" is 10
  GiftListItemColDesc "RIB_GiftListItemColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
, update_user_id varchar2
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
, update_user_id varchar2
, GeoAddrDesc_TBL "RIB_GeoAddrDesc_TBL"  -- Size of "RIB_GeoAddrDesc_TBL" is 10
) return self as result
,constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
, update_user_id varchar2
, GeoAddrDesc_TBL "RIB_GeoAddrDesc_TBL"  -- Size of "RIB_GeoAddrDesc_TBL" is 10
, GiftListItemColDesc "RIB_GiftListItemColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftListDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListDesc') := "ns_name_GiftListDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_type') := event_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_date') := event_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'published_flag') := published_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'public_flag') := public_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'location_created') := location_created;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'instructions') := instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'image_data') := image_data;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration_date') := expiration_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_id') := create_user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user_id') := update_user_id;
  IF GeoAddrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'GeoAddrDesc_TBL.';
    FOR INDX IN GeoAddrDesc_TBL.FIRST()..GeoAddrDesc_TBL.LAST() LOOP
      GeoAddrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'GeoAddrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'GiftListItemColDesc.';
  GiftListItemColDesc.appendNodeValues( i_prefix||'GiftListItemColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
self.update_date := update_date;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
self.update_date := update_date;
self.expiration_date := expiration_date;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
self.update_date := update_date;
self.expiration_date := expiration_date;
self.create_user_id := create_user_id;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
, update_user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
self.update_date := update_date;
self.expiration_date := expiration_date;
self.create_user_id := create_user_id;
self.update_user_id := update_user_id;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
, update_user_id varchar2
, GeoAddrDesc_TBL "RIB_GeoAddrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
self.update_date := update_date;
self.expiration_date := expiration_date;
self.create_user_id := create_user_id;
self.update_user_id := update_user_id;
self.GeoAddrDesc_TBL := GeoAddrDesc_TBL;
RETURN;
end;
constructor function "RIB_GiftListDesc_REC"
(
  rib_oid number
, id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location_created varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, create_date date
, update_date date
, expiration_date date
, create_user_id varchar2
, update_user_id varchar2
, GeoAddrDesc_TBL "RIB_GeoAddrDesc_TBL"
, GiftListItemColDesc "RIB_GiftListItemColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.CustomerRef := CustomerRef;
self.name := name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location_created := location_created;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.create_date := create_date;
self.update_date := update_date;
self.expiration_date := expiration_date;
self.create_user_id := create_user_id;
self.update_user_id := update_user_id;
self.GeoAddrDesc_TBL := GeoAddrDesc_TBL;
self.GiftListItemColDesc := GiftListItemColDesc;
RETURN;
end;
END;
/
