@@ItemColRef.sql;
/
DROP TYPE "RIB_TgtItemColDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TgtItemColDesc_REC";
/
DROP TYPE "RIB_TargetedItems_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TargetedItems_REC";
/
DROP TYPE "RIB_CrossSells_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CrossSells_REC";
/
DROP TYPE "RIB_UpSells_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_UpSells_REC";
/
DROP TYPE "RIB_TgtItemColDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TgtItemColDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TgtItemColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  TargetedItems "RIB_TargetedItems_REC",
  CrossSells "RIB_CrossSells_REC",
  UpSells "RIB_UpSells_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TgtItemColDesc_REC"
(
  rib_oid number
, TargetedItems "RIB_TargetedItems_REC"
) return self as result
,constructor function "RIB_TgtItemColDesc_REC"
(
  rib_oid number
, TargetedItems "RIB_TargetedItems_REC"
, CrossSells "RIB_CrossSells_REC"
) return self as result
,constructor function "RIB_TgtItemColDesc_REC"
(
  rib_oid number
, TargetedItems "RIB_TargetedItems_REC"
, CrossSells "RIB_CrossSells_REC"
, UpSells "RIB_UpSells_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TgtItemColDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TgtItemColDesc') := "ns_name_TgtItemColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'TargetedItems.';
  TargetedItems.appendNodeValues( i_prefix||'TargetedItems');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CrossSells.';
  CrossSells.appendNodeValues( i_prefix||'CrossSells');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'UpSells.';
  UpSells.appendNodeValues( i_prefix||'UpSells');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_TgtItemColDesc_REC"
(
  rib_oid number
, TargetedItems "RIB_TargetedItems_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.TargetedItems := TargetedItems;
RETURN;
end;
constructor function "RIB_TgtItemColDesc_REC"
(
  rib_oid number
, TargetedItems "RIB_TargetedItems_REC"
, CrossSells "RIB_CrossSells_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.TargetedItems := TargetedItems;
self.CrossSells := CrossSells;
RETURN;
end;
constructor function "RIB_TgtItemColDesc_REC"
(
  rib_oid number
, TargetedItems "RIB_TargetedItems_REC"
, CrossSells "RIB_CrossSells_REC"
, UpSells "RIB_UpSells_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.TargetedItems := TargetedItems;
self.CrossSells := CrossSells;
self.UpSells := UpSells;
RETURN;
end;
END;
/
DROP TYPE "RIB_TargetedItems_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TargetedItems_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TgtItemColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ItemColRef "RIB_ItemColRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TargetedItems_REC"
(
  rib_oid number
, ItemColRef "RIB_ItemColRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TargetedItems_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TgtItemColDesc') := "ns_name_TgtItemColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ItemColRef.';
  ItemColRef.appendNodeValues( i_prefix||'ItemColRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_TargetedItems_REC"
(
  rib_oid number
, ItemColRef "RIB_ItemColRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemColRef := ItemColRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_CrossSells_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CrossSells_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TgtItemColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ItemColRef "RIB_ItemColRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CrossSells_REC"
(
  rib_oid number
, ItemColRef "RIB_ItemColRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CrossSells_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TgtItemColDesc') := "ns_name_TgtItemColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ItemColRef.';
  ItemColRef.appendNodeValues( i_prefix||'ItemColRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CrossSells_REC"
(
  rib_oid number
, ItemColRef "RIB_ItemColRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemColRef := ItemColRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_UpSells_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_UpSells_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TgtItemColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ItemColRef "RIB_ItemColRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_UpSells_REC"
(
  rib_oid number
, ItemColRef "RIB_ItemColRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_UpSells_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TgtItemColDesc') := "ns_name_TgtItemColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ItemColRef.';
  ItemColRef.appendNodeValues( i_prefix||'ItemColRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_UpSells_REC"
(
  rib_oid number
, ItemColRef "RIB_ItemColRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemColRef := ItemColRef;
RETURN;
end;
END;
/
