DROP TYPE "RIB_StrItmRelDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmRelDesc_REC";
/
DROP TYPE "RIB_StrItmRelDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmRelDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmRelDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  short_description varchar2(120),
  long_description varchar2(250),
  unit_of_measure varchar2(25),
  available_soh_qty number(12,4),
  diff_type1 varchar2(10),
  diff_description1 varchar2(255),
  diff_type2 varchar2(10),
  diff_description2 varchar2(255),
  diff_type3 varchar2(10),
  diff_description3 varchar2(255),
  diff_type4 varchar2(10),
  diff_description4 varchar2(255),
  effective_date date,
  end_date date,
  relationship_type varchar2(6),
  mandatory_ind varchar2(5), --mandatory_ind is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmRelDesc_REC"
(
  rib_oid number
, item_id varchar2
, short_description varchar2
, long_description varchar2
, unit_of_measure varchar2
, available_soh_qty number
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, effective_date date
, end_date date
, relationship_type varchar2
, mandatory_ind varchar2  --mandatory_ind is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmRelDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmRelDesc') := "ns_name_StrItmRelDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'short_description') := short_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'long_description') := long_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'available_soh_qty') := available_soh_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type1') := diff_type1;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description1') := diff_description1;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type2') := diff_type2;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description2') := diff_description2;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type3') := diff_type3;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description3') := diff_description3;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type4') := diff_type4;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description4') := diff_description4;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'relationship_type') := relationship_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'mandatory_ind') := mandatory_ind;
END appendNodeValues;
constructor function "RIB_StrItmRelDesc_REC"
(
  rib_oid number
, item_id varchar2
, short_description varchar2
, long_description varchar2
, unit_of_measure varchar2
, available_soh_qty number
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, effective_date date
, end_date date
, relationship_type varchar2
, mandatory_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.short_description := short_description;
self.long_description := long_description;
self.unit_of_measure := unit_of_measure;
self.available_soh_qty := available_soh_qty;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.effective_date := effective_date;
self.end_date := end_date;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
RETURN;
end;
END;
/
