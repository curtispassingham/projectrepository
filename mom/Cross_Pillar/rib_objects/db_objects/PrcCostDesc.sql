DROP TYPE "RIB_PrcCostDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcCostDesc_REC";
/
DROP TYPE "RIB_PrcCostDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcCostDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcCostDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  source_location number(10),
  source_location_country varchar2(3),
  fulfill_location number(10),
  loc_type varchar2(1), -- loc_type is enumeration field, valid values are [S, W] (all lower-case)
  channel_id number(4),
  active_date date,
  pricing_cost number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcCostDesc_REC"
(
  rib_oid number
, item varchar2
, source_location number
, source_location_country varchar2
, fulfill_location number
, loc_type varchar2
, channel_id number
, active_date date
, pricing_cost number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcCostDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcCostDesc') := "ns_name_PrcCostDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_location') := source_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_location_country') := source_location_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_location') := fulfill_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'active_date') := active_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_cost') := pricing_cost;
END appendNodeValues;
constructor function "RIB_PrcCostDesc_REC"
(
  rib_oid number
, item varchar2
, source_location number
, source_location_country varchar2
, fulfill_location number
, loc_type varchar2
, channel_id number
, active_date date
, pricing_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.source_location := source_location;
self.source_location_country := source_location_country;
self.fulfill_location := fulfill_location;
self.loc_type := loc_type;
self.channel_id := channel_id;
self.active_date := active_date;
self.pricing_cost := pricing_cost;
RETURN;
end;
END;
/
