DROP TYPE "RIB_FopDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopDesc_REC";
/
DROP TYPE "RIB_FopItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopItm_REC";
/
DROP TYPE "RIB_FopItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FopItm_TBL" AS TABLE OF "RIB_FopItm_REC";
/
DROP TYPE "RIB_FopDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  pick_id number(12),
  pick_type varchar2(20), -- pick_type is enumeration field, valid values are [ORDER, BIN, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_date date,
  create_user_name varchar2(128),
  complete_date date,
  complete_user_name varchar2(128),
  FopItm_TBL "RIB_FopItm_TBL",   -- Size of "RIB_FopItm_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
) return self as result
,constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
, complete_date date
) return self as result
,constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
, complete_date date
, complete_user_name varchar2
) return self as result
,constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
, complete_date date
, complete_user_name varchar2
, FopItm_TBL "RIB_FopItm_TBL"  -- Size of "RIB_FopItm_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopDesc') := "ns_name_FopDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_id') := pick_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_type') := pick_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'complete_date') := complete_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'complete_user_name') := complete_user_name;
  IF FopItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FopItm_TBL.';
    FOR INDX IN FopItm_TBL.FIRST()..FopItm_TBL.LAST() LOOP
      FopItm_TBL(indx).appendNodeValues( i_prefix||indx||'FopItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
self.pick_type := pick_type;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
RETURN;
end;
constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
, complete_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
self.pick_type := pick_type;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.complete_date := complete_date;
RETURN;
end;
constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
, complete_date date
, complete_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
self.pick_type := pick_type;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.complete_date := complete_date;
self.complete_user_name := complete_user_name;
RETURN;
end;
constructor function "RIB_FopDesc_REC"
(
  rib_oid number
, pick_id number
, pick_type varchar2
, status varchar2
, create_date date
, create_user_name varchar2
, complete_date date
, complete_user_name varchar2
, FopItm_TBL "RIB_FopItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
self.pick_type := pick_type;
self.status := status;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.complete_date := complete_date;
self.complete_user_name := complete_user_name;
self.FopItm_TBL := FopItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_FopItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  bin_id varchar2(128),
  fulfillment_order_id number(12),
  fulfillment_order_line_id number(12),
  substitute_line_id number(12),
  preferred_uom varchar2(4),
  standard_uom varchar2(4),
  suggested_quantity number(20,4),
  picked_quantity number(20,4),
  case_size number(10,2),
  pick_area varchar2(255),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, bin_id varchar2
, fulfillment_order_id number
, fulfillment_order_line_id number
, substitute_line_id number
, preferred_uom varchar2
, standard_uom varchar2
, suggested_quantity number
, picked_quantity number
, case_size number
, pick_area varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopDesc') := "ns_name_FopDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bin_id') := bin_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_line_id') := fulfillment_order_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'substitute_line_id') := substitute_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'preferred_uom') := preferred_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'standard_uom') := standard_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggested_quantity') := suggested_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'picked_quantity') := picked_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_area') := pick_area;
END appendNodeValues;
constructor function "RIB_FopItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, bin_id varchar2
, fulfillment_order_id number
, fulfillment_order_line_id number
, substitute_line_id number
, preferred_uom varchar2
, standard_uom varchar2
, suggested_quantity number
, picked_quantity number
, case_size number
, pick_area varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.bin_id := bin_id;
self.fulfillment_order_id := fulfillment_order_id;
self.fulfillment_order_line_id := fulfillment_order_line_id;
self.substitute_line_id := substitute_line_id;
self.preferred_uom := preferred_uom;
self.standard_uom := standard_uom;
self.suggested_quantity := suggested_quantity;
self.picked_quantity := picked_quantity;
self.case_size := case_size;
self.pick_area := pick_area;
RETURN;
end;
END;
/
