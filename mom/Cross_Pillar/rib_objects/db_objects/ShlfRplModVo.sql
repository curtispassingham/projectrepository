DROP TYPE "RIB_ShlfRplModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplModVo_REC";
/
DROP TYPE "RIB_ShlfRplItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplItmMod_REC";
/
DROP TYPE "RIB_ShlfRplSubMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplSubMod_REC";
/
DROP TYPE "RIB_ShlfRplItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplItmMod_TBL" AS TABLE OF "RIB_ShlfRplItmMod_REC";
/
DROP TYPE "RIB_ShlfRplModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  replenishment_id varchar2(15),
  store_id number(10),
  ShlfRplItmMod_TBL "RIB_ShlfRplItmMod_TBL",   -- Size of "RIB_ShlfRplItmMod_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplModVo_REC"
(
  rib_oid number
, replenishment_id varchar2
, store_id number
, ShlfRplItmMod_TBL "RIB_ShlfRplItmMod_TBL"  -- Size of "RIB_ShlfRplItmMod_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplModVo') := "ns_name_ShlfRplModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_id') := replenishment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  l_new_pre :=i_prefix||'ShlfRplItmMod_TBL.';
  FOR INDX IN ShlfRplItmMod_TBL.FIRST()..ShlfRplItmMod_TBL.LAST() LOOP
    ShlfRplItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'ShlfRplItmMod_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ShlfRplModVo_REC"
(
  rib_oid number
, replenishment_id varchar2
, store_id number
, ShlfRplItmMod_TBL "RIB_ShlfRplItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.replenishment_id := replenishment_id;
self.store_id := store_id;
self.ShlfRplItmMod_TBL := ShlfRplItmMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShlfRplSubMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplSubMod_TBL" AS TABLE OF "RIB_ShlfRplSubMod_REC";
/
DROP TYPE "RIB_ShlfRplItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  actual_pick_amount number(20,4),
  pick_from_area varchar2(20), -- pick_from_area is enumeration field, valid values are [BACKROOM, DELIVERY_BAY, UNKNOWN] (all lower-case)
  ShlfRplSubMod_TBL "RIB_ShlfRplSubMod_TBL",   -- Size of "RIB_ShlfRplSubMod_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplItmMod_REC"
(
  rib_oid number
, line_id number
, actual_pick_amount number
, pick_from_area varchar2
) return self as result
,constructor function "RIB_ShlfRplItmMod_REC"
(
  rib_oid number
, line_id number
, actual_pick_amount number
, pick_from_area varchar2
, ShlfRplSubMod_TBL "RIB_ShlfRplSubMod_TBL"  -- Size of "RIB_ShlfRplSubMod_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplModVo') := "ns_name_ShlfRplModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'actual_pick_amount') := actual_pick_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_from_area') := pick_from_area;
  IF ShlfRplSubMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ShlfRplSubMod_TBL.';
    FOR INDX IN ShlfRplSubMod_TBL.FIRST()..ShlfRplSubMod_TBL.LAST() LOOP
      ShlfRplSubMod_TBL(indx).appendNodeValues( i_prefix||indx||'ShlfRplSubMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ShlfRplItmMod_REC"
(
  rib_oid number
, line_id number
, actual_pick_amount number
, pick_from_area varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.actual_pick_amount := actual_pick_amount;
self.pick_from_area := pick_from_area;
RETURN;
end;
constructor function "RIB_ShlfRplItmMod_REC"
(
  rib_oid number
, line_id number
, actual_pick_amount number
, pick_from_area varchar2
, ShlfRplSubMod_TBL "RIB_ShlfRplSubMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.actual_pick_amount := actual_pick_amount;
self.pick_from_area := pick_from_area;
self.ShlfRplSubMod_TBL := ShlfRplSubMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShlfRplSubMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplSubMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  actual_pick_amount number(20,4),
  pick_from_area varchar2(20), -- pick_from_area is enumeration field, valid values are [BACKROOM, DELIVERY_BAY, UNKNOWN] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplSubMod_REC"
(
  rib_oid number
, item_id varchar2
, actual_pick_amount number
, pick_from_area varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplSubMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplModVo') := "ns_name_ShlfRplModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'actual_pick_amount') := actual_pick_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_from_area') := pick_from_area;
END appendNodeValues;
constructor function "RIB_ShlfRplSubMod_REC"
(
  rib_oid number
, item_id varchar2
, actual_pick_amount number
, pick_from_area varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.actual_pick_amount := actual_pick_amount;
self.pick_from_area := pick_from_area;
RETURN;
end;
END;
/
