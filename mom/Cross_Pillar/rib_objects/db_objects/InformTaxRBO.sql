@@NameValPairRBO.sql;
/
DROP TYPE "RIB_InformTaxRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InformTaxRBO_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_InformTaxRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InformTaxRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InformTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tax_amount number(20,4),
  tax_basis_amount number(20,4),
  modified_tax_basis_amount number(20,4),
  tax_code varchar2(25),
  tax_rate number(20,10),
  tax_exception_type varchar2(25),
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
) return self as result
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
) return self as result
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
) return self as result
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
) return self as result
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
, tax_rate number
) return self as result
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
, tax_rate number
, tax_exception_type varchar2
) return self as result
,constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
, tax_rate number
, tax_exception_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InformTaxRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InformTaxRBO') := "ns_name_InformTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_amount') := tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_basis_amount') := tax_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'modified_tax_basis_amount') := modified_tax_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_code') := tax_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rate') := tax_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_exception_type') := tax_exception_type;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
RETURN;
end;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
RETURN;
end;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
RETURN;
end;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.tax_code := tax_code;
RETURN;
end;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
, tax_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
RETURN;
end;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
, tax_rate number
, tax_exception_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.tax_exception_type := tax_exception_type;
RETURN;
end;
constructor function "RIB_InformTaxRBO_REC"
(
  rib_oid number
, tax_amount number
, tax_basis_amount number
, modified_tax_basis_amount number
, tax_code varchar2
, tax_rate number
, tax_exception_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tax_amount := tax_amount;
self.tax_basis_amount := tax_basis_amount;
self.modified_tax_basis_amount := modified_tax_basis_amount;
self.tax_code := tax_code;
self.tax_rate := tax_rate;
self.tax_exception_type := tax_exception_type;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
END;
/
