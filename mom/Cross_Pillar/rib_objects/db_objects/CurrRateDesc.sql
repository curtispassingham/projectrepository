DROP TYPE "RIB_CurrRateDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CurrRateDesc_REC";
/
DROP TYPE "RIB_CurrRateDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CurrRateDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CurrRateDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_currency varchar2(3),
  to_currency varchar2(3),
  conversion_date date,
  conversion_rate number(20,10),
  user_conversion_type varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CurrRateDesc_REC"
(
  rib_oid number
, from_currency varchar2
, to_currency varchar2
, conversion_date date
, conversion_rate number
, user_conversion_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CurrRateDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CurrRateDesc') := "ns_name_CurrRateDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_currency') := from_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_currency') := to_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'conversion_date') := conversion_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'conversion_rate') := conversion_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_conversion_type') := user_conversion_type;
END appendNodeValues;
constructor function "RIB_CurrRateDesc_REC"
(
  rib_oid number
, from_currency varchar2
, to_currency varchar2
, conversion_date date
, conversion_rate number
, user_conversion_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_currency := from_currency;
self.to_currency := to_currency;
self.conversion_date := conversion_date;
self.conversion_rate := conversion_rate;
self.user_conversion_type := user_conversion_type;
RETURN;
end;
END;
/
