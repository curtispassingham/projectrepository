DROP TYPE "RIB_SOStatusDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SOStatusDtl_REC";
/
DROP TYPE "RIB_SOStatusDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SOStatusDesc_REC";
/
DROP TYPE "RIB_SOStatusDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SOStatusDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SOStatusDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dest_id varchar2(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  item_id varchar2(25),
  original_item_id varchar2(25),
  order_line_nbr number(3),
  unit_qty number(12,4),
  status varchar2(2),
  user_id varchar2(30),
  updated_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SOStatusDtl_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
, original_item_id varchar2
, order_line_nbr number
, unit_qty number
, status varchar2
) return self as result
,constructor function "RIB_SOStatusDtl_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
, original_item_id varchar2
, order_line_nbr number
, unit_qty number
, status varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_SOStatusDtl_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
, original_item_id varchar2
, order_line_nbr number
, unit_qty number
, status varchar2
, user_id varchar2
, updated_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SOStatusDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SOStatusDesc') := "ns_name_SOStatusDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'original_item_id') := original_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_nbr') := order_line_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'updated_date') := updated_date;
END appendNodeValues;
constructor function "RIB_SOStatusDtl_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
, original_item_id varchar2
, order_line_nbr number
, unit_qty number
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.item_id := item_id;
self.original_item_id := original_item_id;
self.order_line_nbr := order_line_nbr;
self.unit_qty := unit_qty;
self.status := status;
RETURN;
end;
constructor function "RIB_SOStatusDtl_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
, original_item_id varchar2
, order_line_nbr number
, unit_qty number
, status varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.item_id := item_id;
self.original_item_id := original_item_id;
self.order_line_nbr := order_line_nbr;
self.unit_qty := unit_qty;
self.status := status;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_SOStatusDtl_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
, original_item_id varchar2
, order_line_nbr number
, unit_qty number
, status varchar2
, user_id varchar2
, updated_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.item_id := item_id;
self.original_item_id := original_item_id;
self.order_line_nbr := order_line_nbr;
self.unit_qty := unit_qty;
self.status := status;
self.user_id := user_id;
self.updated_date := updated_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_SOStatusDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SOStatusDtl_TBL" AS TABLE OF "RIB_SOStatusDtl_REC";
/
DROP TYPE "RIB_SOStatusDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SOStatusDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SOStatusDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dc_dest_id varchar2(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  distro_nbr varchar2(12),
  distro_document_type varchar2(1),
  SOStatusDtl_TBL "RIB_SOStatusDtl_TBL",   -- Size of "RIB_SOStatusDtl_TBL" is unbounded
  context_type varchar2(6),
  context_value varchar2(25),
  inventory_type varchar2(6),
  cust_order_nbr varchar2(48),
  fulfill_order_nbr varchar2(48),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"  -- Size of "RIB_SOStatusDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"  -- Size of "RIB_SOStatusDtl_TBL" is unbounded
, context_type varchar2
) return self as result
,constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"  -- Size of "RIB_SOStatusDtl_TBL" is unbounded
, context_type varchar2
, context_value varchar2
) return self as result
,constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"  -- Size of "RIB_SOStatusDtl_TBL" is unbounded
, context_type varchar2
, context_value varchar2
, inventory_type varchar2
) return self as result
,constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"  -- Size of "RIB_SOStatusDtl_TBL" is unbounded
, context_type varchar2
, context_value varchar2
, inventory_type varchar2
, cust_order_nbr varchar2
) return self as result
,constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"  -- Size of "RIB_SOStatusDtl_TBL" is unbounded
, context_type varchar2
, context_value varchar2
, inventory_type varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SOStatusDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SOStatusDesc') := "ns_name_SOStatusDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_document_type') := distro_document_type;
  l_new_pre :=i_prefix||'SOStatusDtl_TBL.';
  FOR INDX IN SOStatusDtl_TBL.FIRST()..SOStatusDtl_TBL.LAST() LOOP
    SOStatusDtl_TBL(indx).appendNodeValues( i_prefix||indx||'SOStatusDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type') := context_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'inventory_type') := inventory_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_nbr') := fulfill_order_nbr;
END appendNodeValues;
constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.distro_nbr := distro_nbr;
self.distro_document_type := distro_document_type;
self.SOStatusDtl_TBL := SOStatusDtl_TBL;
RETURN;
end;
constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"
, context_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.distro_nbr := distro_nbr;
self.distro_document_type := distro_document_type;
self.SOStatusDtl_TBL := SOStatusDtl_TBL;
self.context_type := context_type;
RETURN;
end;
constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"
, context_type varchar2
, context_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.distro_nbr := distro_nbr;
self.distro_document_type := distro_document_type;
self.SOStatusDtl_TBL := SOStatusDtl_TBL;
self.context_type := context_type;
self.context_value := context_value;
RETURN;
end;
constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"
, context_type varchar2
, context_value varchar2
, inventory_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.distro_nbr := distro_nbr;
self.distro_document_type := distro_document_type;
self.SOStatusDtl_TBL := SOStatusDtl_TBL;
self.context_type := context_type;
self.context_value := context_value;
self.inventory_type := inventory_type;
RETURN;
end;
constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"
, context_type varchar2
, context_value varchar2
, inventory_type varchar2
, cust_order_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.distro_nbr := distro_nbr;
self.distro_document_type := distro_document_type;
self.SOStatusDtl_TBL := SOStatusDtl_TBL;
self.context_type := context_type;
self.context_value := context_value;
self.inventory_type := inventory_type;
self.cust_order_nbr := cust_order_nbr;
RETURN;
end;
constructor function "RIB_SOStatusDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, distro_nbr varchar2
, distro_document_type varchar2
, SOStatusDtl_TBL "RIB_SOStatusDtl_TBL"
, context_type varchar2
, context_value varchar2
, inventory_type varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.distro_nbr := distro_nbr;
self.distro_document_type := distro_document_type;
self.SOStatusDtl_TBL := SOStatusDtl_TBL;
self.context_type := context_type;
self.context_value := context_value;
self.inventory_type := inventory_type;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
RETURN;
end;
END;
/
