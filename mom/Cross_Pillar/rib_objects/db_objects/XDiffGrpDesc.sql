DROP TYPE "RIB_XDiffGrpDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XDiffGrpDtl_REC";
/
DROP TYPE "RIB_XDiffGrpDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XDiffGrpDesc_REC";
/
DROP TYPE "RIB_XDiffGrpDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XDiffGrpDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XDiffGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  diff_id varchar2(10),
  display_seq number(4),
  create_datetime date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XDiffGrpDtl_REC"
(
  rib_oid number
, diff_id varchar2
) return self as result
,constructor function "RIB_XDiffGrpDtl_REC"
(
  rib_oid number
, diff_id varchar2
, display_seq number
) return self as result
,constructor function "RIB_XDiffGrpDtl_REC"
(
  rib_oid number
, diff_id varchar2
, display_seq number
, create_datetime date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XDiffGrpDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XDiffGrpDesc') := "ns_name_XDiffGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_id') := diff_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_seq') := display_seq;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
END appendNodeValues;
constructor function "RIB_XDiffGrpDtl_REC"
(
  rib_oid number
, diff_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_id := diff_id;
RETURN;
end;
constructor function "RIB_XDiffGrpDtl_REC"
(
  rib_oid number
, diff_id varchar2
, display_seq number
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_id := diff_id;
self.display_seq := display_seq;
RETURN;
end;
constructor function "RIB_XDiffGrpDtl_REC"
(
  rib_oid number
, diff_id varchar2
, display_seq number
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_id := diff_id;
self.display_seq := display_seq;
self.create_datetime := create_datetime;
RETURN;
end;
END;
/
DROP TYPE "RIB_XDiffGrpDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XDiffGrpDtl_TBL" AS TABLE OF "RIB_XDiffGrpDtl_REC";
/
DROP TYPE "RIB_XDiffGrpDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XDiffGrpDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XDiffGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  diff_group_id varchar2(10),
  diff_group_type varchar2(6),
  diff_group_desc varchar2(120),
  create_datetime date,
  XDiffGrpDtl_TBL "RIB_XDiffGrpDtl_TBL",   -- Size of "RIB_XDiffGrpDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XDiffGrpDesc_REC"
(
  rib_oid number
, diff_group_id varchar2
, diff_group_type varchar2
, diff_group_desc varchar2
) return self as result
,constructor function "RIB_XDiffGrpDesc_REC"
(
  rib_oid number
, diff_group_id varchar2
, diff_group_type varchar2
, diff_group_desc varchar2
, create_datetime date
) return self as result
,constructor function "RIB_XDiffGrpDesc_REC"
(
  rib_oid number
, diff_group_id varchar2
, diff_group_type varchar2
, diff_group_desc varchar2
, create_datetime date
, XDiffGrpDtl_TBL "RIB_XDiffGrpDtl_TBL"  -- Size of "RIB_XDiffGrpDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XDiffGrpDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XDiffGrpDesc') := "ns_name_XDiffGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_group_id') := diff_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_group_type') := diff_group_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_group_desc') := diff_group_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  IF XDiffGrpDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XDiffGrpDtl_TBL.';
    FOR INDX IN XDiffGrpDtl_TBL.FIRST()..XDiffGrpDtl_TBL.LAST() LOOP
      XDiffGrpDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XDiffGrpDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XDiffGrpDesc_REC"
(
  rib_oid number
, diff_group_id varchar2
, diff_group_type varchar2
, diff_group_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_group_id := diff_group_id;
self.diff_group_type := diff_group_type;
self.diff_group_desc := diff_group_desc;
RETURN;
end;
constructor function "RIB_XDiffGrpDesc_REC"
(
  rib_oid number
, diff_group_id varchar2
, diff_group_type varchar2
, diff_group_desc varchar2
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_group_id := diff_group_id;
self.diff_group_type := diff_group_type;
self.diff_group_desc := diff_group_desc;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_XDiffGrpDesc_REC"
(
  rib_oid number
, diff_group_id varchar2
, diff_group_type varchar2
, diff_group_desc varchar2
, create_datetime date
, XDiffGrpDtl_TBL "RIB_XDiffGrpDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_group_id := diff_group_id;
self.diff_group_type := diff_group_type;
self.diff_group_desc := diff_group_desc;
self.create_datetime := create_datetime;
self.XDiffGrpDtl_TBL := XDiffGrpDtl_TBL;
RETURN;
end;
END;
/
