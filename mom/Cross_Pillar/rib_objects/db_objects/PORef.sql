DROP TYPE "RIB_PODtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PODtlRef_REC";
/
DROP TYPE "RIB_PORef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PORef_REC";
/
DROP TYPE "RIB_PODtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PODtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PORef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  physical_location_type varchar2(1),
  physical_location number(10),
  physical_store_type varchar2(1),
  physical_stockholding_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PODtlRef_REC"
(
  rib_oid number
, item varchar2
, physical_location_type varchar2
, physical_location number
) return self as result
,constructor function "RIB_PODtlRef_REC"
(
  rib_oid number
, item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
) return self as result
,constructor function "RIB_PODtlRef_REC"
(
  rib_oid number
, item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PODtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PORef') := "ns_name_PORef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_location_type') := physical_location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_location') := physical_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_store_type') := physical_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_stockholding_ind') := physical_stockholding_ind;
END appendNodeValues;
constructor function "RIB_PODtlRef_REC"
(
  rib_oid number
, item varchar2
, physical_location_type varchar2
, physical_location number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
RETURN;
end;
constructor function "RIB_PODtlRef_REC"
(
  rib_oid number
, item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
RETURN;
end;
constructor function "RIB_PODtlRef_REC"
(
  rib_oid number
, item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_PODtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PODtlRef_TBL" AS TABLE OF "RIB_PODtlRef_REC";
/
DROP TYPE "RIB_PORef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PORef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PORef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  doc_type varchar2(1),
  order_no varchar2(12),
  PODtlRef_TBL "RIB_PODtlRef_TBL",   -- Size of "RIB_PODtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PORef_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
) return self as result
,constructor function "RIB_PORef_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, PODtlRef_TBL "RIB_PODtlRef_TBL"  -- Size of "RIB_PODtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PORef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PORef') := "ns_name_PORef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  IF PODtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PODtlRef_TBL.';
    FOR INDX IN PODtlRef_TBL.FIRST()..PODtlRef_TBL.LAST() LOOP
      PODtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'PODtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PORef_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
RETURN;
end;
constructor function "RIB_PORef_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, PODtlRef_TBL "RIB_PODtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.PODtlRef_TBL := PODtlRef_TBL;
RETURN;
end;
END;
/
