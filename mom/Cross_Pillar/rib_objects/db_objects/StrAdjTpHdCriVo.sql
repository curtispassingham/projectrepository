DROP TYPE "RIB_StrAdjTpHdCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjTpHdCriVo_REC";
/
DROP TYPE "RIB_StrAdjTpHdCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrAdjTpHdCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrAdjTpHdCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  from_date date,
  to_date date,
  item_id varchar2(25),
  template_id number(12),
  description varchar2(250),
  reason_id number(12),
  status varchar2(20), -- status is enumeration field, valid values are [IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN, NO_VALUE] (all lower-case)
  user_name varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrAdjTpHdCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, item_id varchar2
, template_id number
, description varchar2
, reason_id number
, status varchar2
) return self as result
,constructor function "RIB_StrAdjTpHdCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, item_id varchar2
, template_id number
, description varchar2
, reason_id number
, status varchar2
, user_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrAdjTpHdCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrAdjTpHdCriVo') := "ns_name_StrAdjTpHdCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_date') := from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_date') := to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'template_id') := template_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
END appendNodeValues;
constructor function "RIB_StrAdjTpHdCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, item_id varchar2
, template_id number
, description varchar2
, reason_id number
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.from_date := from_date;
self.to_date := to_date;
self.item_id := item_id;
self.template_id := template_id;
self.description := description;
self.reason_id := reason_id;
self.status := status;
RETURN;
end;
constructor function "RIB_StrAdjTpHdCriVo_REC"
(
  rib_oid number
, store_id number
, from_date date
, to_date date
, item_id varchar2
, template_id number
, description varchar2
, reason_id number
, status varchar2
, user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.from_date := from_date;
self.to_date := to_date;
self.item_id := item_id;
self.template_id := template_id;
self.description := description;
self.reason_id := reason_id;
self.status := status;
self.user_name := user_name;
RETURN;
end;
END;
/
