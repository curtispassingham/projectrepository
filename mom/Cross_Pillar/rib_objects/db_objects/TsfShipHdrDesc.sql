DROP TYPE "RIB_TsfShipHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipHdrDesc_REC";
/
DROP TYPE "RIB_TsfShipHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  asn varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, SHIPPED, CANCELED, UNKNOWN] (all lower-case)
  destination_type varchar2(20), -- destination_type is enumeration field, valid values are [FINISHER, STORE, WAREHOUSE, UNKNOWN] (all lower-case)
  destination_name varchar2(250),
  last_update_date date,
  dispatch_date date,
  num_of_containers number(10),
  order_related varchar2(5), --order_related is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipHdrDesc_REC"
(
  rib_oid number
, shipment_id number
, asn varchar2
, status varchar2
, destination_type varchar2
, destination_name varchar2
, last_update_date date
, dispatch_date date
, num_of_containers number
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipHdrDesc') := "ns_name_TsfShipHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_type') := destination_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_name') := destination_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_date') := last_update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_date') := dispatch_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'num_of_containers') := num_of_containers;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_related') := order_related;
END appendNodeValues;
constructor function "RIB_TsfShipHdrDesc_REC"
(
  rib_oid number
, shipment_id number
, asn varchar2
, status varchar2
, destination_type varchar2
, destination_name varchar2
, last_update_date date
, dispatch_date date
, num_of_containers number
, order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.asn := asn;
self.status := status;
self.destination_type := destination_type;
self.destination_name := destination_name;
self.last_update_date := last_update_date;
self.dispatch_date := dispatch_date;
self.num_of_containers := num_of_containers;
self.order_related := order_related;
RETURN;
end;
END;
/
