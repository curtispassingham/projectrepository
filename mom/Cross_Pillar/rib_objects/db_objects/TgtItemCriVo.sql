@@CustomerRef.sql;
/
@@ItemColRef.sql;
/
@@LocationRef.sql;
/
DROP TYPE "RIB_TgtItemCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TgtItemCriVo_REC";
/
DROP TYPE "RIB_TgtItemCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TgtItemCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TgtItemCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  LocationRef "RIB_LocationRef_REC",
  CustomerRef "RIB_CustomerRef_REC",
  ItemColRef "RIB_ItemColRef_REC",
  max_targeted number(3),
  max_crosssell number(3),
  max_upsell number(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
) return self as result
,constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
,constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
) return self as result
,constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
, max_targeted number
) return self as result
,constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
, max_targeted number
, max_crosssell number
) return self as result
,constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
, max_targeted number
, max_crosssell number
, max_upsell number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TgtItemCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TgtItemCriVo') := "ns_name_TgtItemCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'LocationRef.';
  LocationRef.appendNodeValues( i_prefix||'LocationRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ItemColRef.';
  ItemColRef.appendNodeValues( i_prefix||'ItemColRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_targeted') := max_targeted;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_crosssell') := max_crosssell;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_upsell') := max_upsell;
END appendNodeValues;
constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.LocationRef := LocationRef;
RETURN;
end;
constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.LocationRef := LocationRef;
self.CustomerRef := CustomerRef;
RETURN;
end;
constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.LocationRef := LocationRef;
self.CustomerRef := CustomerRef;
self.ItemColRef := ItemColRef;
RETURN;
end;
constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
, max_targeted number
) return self as result is
begin
self.rib_oid := rib_oid;
self.LocationRef := LocationRef;
self.CustomerRef := CustomerRef;
self.ItemColRef := ItemColRef;
self.max_targeted := max_targeted;
RETURN;
end;
constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
, max_targeted number
, max_crosssell number
) return self as result is
begin
self.rib_oid := rib_oid;
self.LocationRef := LocationRef;
self.CustomerRef := CustomerRef;
self.ItemColRef := ItemColRef;
self.max_targeted := max_targeted;
self.max_crosssell := max_crosssell;
RETURN;
end;
constructor function "RIB_TgtItemCriVo_REC"
(
  rib_oid number
, LocationRef "RIB_LocationRef_REC"
, CustomerRef "RIB_CustomerRef_REC"
, ItemColRef "RIB_ItemColRef_REC"
, max_targeted number
, max_crosssell number
, max_upsell number
) return self as result is
begin
self.rib_oid := rib_oid;
self.LocationRef := LocationRef;
self.CustomerRef := CustomerRef;
self.ItemColRef := ItemColRef;
self.max_targeted := max_targeted;
self.max_crosssell := max_crosssell;
self.max_upsell := max_upsell;
RETURN;
end;
END;
/
