@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_StrAdjModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjModVo_REC";
/
DROP TYPE "RIB_StrAdjItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjItmMod_REC";
/
DROP TYPE "RIB_StrAdjItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrAdjItmMod_TBL" AS TABLE OF "RIB_StrAdjItmMod_REC";
/
DROP TYPE "RIB_StrAdjModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrAdjModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrAdjModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  adjustment_id number(12),
  store_id number(10),
  reference_id number(12),
  template_id number(12),
  comments varchar2(2000),
  publish varchar2(5), --publish is boolean field, valid values are true,false (all lower-case) 
  StrAdjItmMod_TBL "RIB_StrAdjItmMod_TBL",   -- Size of "RIB_StrAdjItmMod_TBL" is 1000
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
) return self as result
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
) return self as result
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
) return self as result
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
) return self as result
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
, publish varchar2  --publish is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
, publish varchar2  --publish is boolean field, valid values are true,false (all lower-case)
, StrAdjItmMod_TBL "RIB_StrAdjItmMod_TBL"  -- Size of "RIB_StrAdjItmMod_TBL" is 1000
) return self as result
,constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
, publish varchar2  --publish is boolean field, valid values are true,false (all lower-case)
, StrAdjItmMod_TBL "RIB_StrAdjItmMod_TBL"  -- Size of "RIB_StrAdjItmMod_TBL" is 1000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrAdjModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrAdjModVo') := "ns_name_StrAdjModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'adjustment_id') := adjustment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reference_id') := reference_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'template_id') := template_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'publish') := publish;
  IF StrAdjItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrAdjItmMod_TBL.';
    FOR INDX IN StrAdjItmMod_TBL.FIRST()..StrAdjItmMod_TBL.LAST() LOOP
      StrAdjItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'StrAdjItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
RETURN;
end;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.reference_id := reference_id;
RETURN;
end;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.reference_id := reference_id;
self.template_id := template_id;
RETURN;
end;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.reference_id := reference_id;
self.template_id := template_id;
self.comments := comments;
RETURN;
end;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
, publish varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.reference_id := reference_id;
self.template_id := template_id;
self.comments := comments;
self.publish := publish;
RETURN;
end;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
, publish varchar2
, StrAdjItmMod_TBL "RIB_StrAdjItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.reference_id := reference_id;
self.template_id := template_id;
self.comments := comments;
self.publish := publish;
self.StrAdjItmMod_TBL := StrAdjItmMod_TBL;
RETURN;
end;
constructor function "RIB_StrAdjModVo_REC"
(
  rib_oid number
, adjustment_id number
, store_id number
, reference_id number
, template_id number
, comments varchar2
, publish varchar2
, StrAdjItmMod_TBL "RIB_StrAdjItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.adjustment_id := adjustment_id;
self.store_id := store_id;
self.reference_id := reference_id;
self.template_id := template_id;
self.comments := comments;
self.publish := publish;
self.StrAdjItmMod_TBL := StrAdjItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_StrAdjItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrAdjItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrAdjModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  reason_id number(12),
  quantity number(20,4),
  case_size number(10,2),
  added_uin_col "RIB_added_uin_col_TBL",   -- Size of "RIB_added_uin_col_TBL" is 100
  removed_uin_col "RIB_removed_uin_col_TBL",   -- Size of "RIB_removed_uin_col_TBL" is 1000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  removed_ext_att_col "RIB_removed_ext_att_col_TBL",   -- Size of "RIB_removed_ext_att_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
) return self as result
,constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
) return self as result
,constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
) return self as result
,constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
) return self as result
,constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"  -- Size of "RIB_removed_ext_att_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrAdjItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrAdjModVo') := "ns_name_StrAdjModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  IF added_uin_col IS NOT NULL THEN
    FOR INDX IN added_uin_col.FIRST()..added_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'added_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'added_uin_col'||'.'):=added_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_uin_col IS NOT NULL THEN
    FOR INDX IN removed_uin_col.FIRST()..removed_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_uin_col'||'.'):=removed_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_ext_att_col IS NOT NULL THEN
    FOR INDX IN removed_ext_att_col.FIRST()..removed_ext_att_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_ext_att_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_ext_att_col'||'.'):=removed_ext_att_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.quantity := quantity;
self.case_size := case_size;
RETURN;
end;
constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.quantity := quantity;
self.case_size := case_size;
self.added_uin_col := added_uin_col;
RETURN;
end;
constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.quantity := quantity;
self.case_size := case_size;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
RETURN;
end;
constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.quantity := quantity;
self.case_size := case_size;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
constructor function "RIB_StrAdjItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, quantity number
, case_size number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.quantity := quantity;
self.case_size := case_size;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.removed_ext_att_col := removed_ext_att_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_added_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_added_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_ext_att_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_ext_att_col_TBL" AS TABLE OF varchar2(128);
/
