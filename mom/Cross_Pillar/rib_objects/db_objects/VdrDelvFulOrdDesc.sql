DROP TYPE "RIB_VdrDelvFulOrdDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvFulOrdDesc_REC";
/
DROP TYPE "RIB_VdrDelvFulOrdDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvFulOrdDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvFulOrdDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  carton_id number(12),
  external_id varchar2(128),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  number_of_line_items number(10),
  comments varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvFulOrdDesc_REC"
(
  rib_oid number
, carton_id number
, external_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, status varchar2
, number_of_line_items number
) return self as result
,constructor function "RIB_VdrDelvFulOrdDesc_REC"
(
  rib_oid number
, carton_id number
, external_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, status varchar2
, number_of_line_items number
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvFulOrdDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvFulOrdDesc') := "ns_name_VdrDelvFulOrdDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_line_items') := number_of_line_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_VdrDelvFulOrdDesc_REC"
(
  rib_oid number
, carton_id number
, external_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, status varchar2
, number_of_line_items number
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.external_id := external_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.status := status;
self.number_of_line_items := number_of_line_items;
RETURN;
end;
constructor function "RIB_VdrDelvFulOrdDesc_REC"
(
  rib_oid number
, carton_id number
, external_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, status varchar2
, number_of_line_items number
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.external_id := external_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.status := status;
self.number_of_line_items := number_of_line_items;
self.comments := comments;
RETURN;
end;
END;
/
