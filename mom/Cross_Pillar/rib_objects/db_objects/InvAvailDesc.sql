@@DistanceDesc.sql;
/
DROP TYPE "RIB_InvAvailDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAvailDesc_REC";
/
DROP TYPE "RIB_InvAvailDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAvailDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAvailDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  location number(10),
  loc_type varchar2(1), -- loc_type is enumeration field, valid values are [S, W] (all lower-case)
  channel_id number(4),
  available_qty number(12,4),
  unit_of_measure varchar2(4),
  available_date date,
  pack_calculate_ind varchar2(1), -- pack_calculate_ind is enumeration field, valid values are [Y, N] (all lower-case)
  DistanceDesc "RIB_DistanceDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
) return self as result
,constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
, available_date date
) return self as result
,constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
, available_date date
, pack_calculate_ind varchar2
) return self as result
,constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
, available_date date
, pack_calculate_ind varchar2
, DistanceDesc "RIB_DistanceDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAvailDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAvailDesc') := "ns_name_InvAvailDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'available_qty') := available_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'available_date') := available_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_calculate_ind') := pack_calculate_ind;
  l_new_pre :=i_prefix||'DistanceDesc.';
  DistanceDesc.appendNodeValues( i_prefix||'DistanceDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.loc_type := loc_type;
self.channel_id := channel_id;
self.available_qty := available_qty;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
, available_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.loc_type := loc_type;
self.channel_id := channel_id;
self.available_qty := available_qty;
self.unit_of_measure := unit_of_measure;
self.available_date := available_date;
RETURN;
end;
constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
, available_date date
, pack_calculate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.loc_type := loc_type;
self.channel_id := channel_id;
self.available_qty := available_qty;
self.unit_of_measure := unit_of_measure;
self.available_date := available_date;
self.pack_calculate_ind := pack_calculate_ind;
RETURN;
end;
constructor function "RIB_InvAvailDesc_REC"
(
  rib_oid number
, item varchar2
, location number
, loc_type varchar2
, channel_id number
, available_qty number
, unit_of_measure varchar2
, available_date date
, pack_calculate_ind varchar2
, DistanceDesc "RIB_DistanceDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.location := location;
self.loc_type := loc_type;
self.channel_id := channel_id;
self.available_qty := available_qty;
self.unit_of_measure := unit_of_measure;
self.available_date := available_date;
self.pack_calculate_ind := pack_calculate_ind;
self.DistanceDesc := DistanceDesc;
RETURN;
end;
END;
/
