@@ItemRef.sql;
/
DROP TYPE "RIB_PurchItmHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PurchItmHdrDesc_REC";
/
DROP TYPE "RIB_PurchItmHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PurchItmHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PurchItmHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  sequence_number number(4),
  ItemRef "RIB_ItemRef_REC",
  item_description varchar2(250),
  quantity number(12,4),
  currency_code varchar2(3),
  extended_sell_price number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PurchItmHdrDesc_REC"
(
  rib_oid number
, sequence_number number
, ItemRef "RIB_ItemRef_REC"
, item_description varchar2
, quantity number
, currency_code varchar2
, extended_sell_price number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PurchItmHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PurchItmHdrDesc') := "ns_name_PurchItmHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'sequence_number') := sequence_number;
  l_new_pre :=i_prefix||'ItemRef.';
  ItemRef.appendNodeValues( i_prefix||'ItemRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'extended_sell_price') := extended_sell_price;
END appendNodeValues;
constructor function "RIB_PurchItmHdrDesc_REC"
(
  rib_oid number
, sequence_number number
, ItemRef "RIB_ItemRef_REC"
, item_description varchar2
, quantity number
, currency_code varchar2
, extended_sell_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sequence_number := sequence_number;
self.ItemRef := ItemRef;
self.item_description := item_description;
self.quantity := quantity;
self.currency_code := currency_code;
self.extended_sell_price := extended_sell_price;
RETURN;
end;
END;
/
