DROP TYPE "RIB_AppointDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AppointDtlRef_REC";
/
DROP TYPE "RIB_AppointRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AppointRef_REC";
/
DROP TYPE "RIB_AppointDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AppointDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AppointRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  po_nbr varchar2(12),
  asn_nbr varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AppointDtlRef_REC"
(
  rib_oid number
, item_id varchar2
, po_nbr varchar2
) return self as result
,constructor function "RIB_AppointDtlRef_REC"
(
  rib_oid number
, item_id varchar2
, po_nbr varchar2
, asn_nbr varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AppointDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AppointRef') := "ns_name_AppointRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
END appendNodeValues;
constructor function "RIB_AppointDtlRef_REC"
(
  rib_oid number
, item_id varchar2
, po_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.po_nbr := po_nbr;
RETURN;
end;
constructor function "RIB_AppointDtlRef_REC"
(
  rib_oid number
, item_id varchar2
, po_nbr varchar2
, asn_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.po_nbr := po_nbr;
self.asn_nbr := asn_nbr;
RETURN;
end;
END;
/
DROP TYPE "RIB_AppointDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AppointDtlRef_TBL" AS TABLE OF "RIB_AppointDtlRef_REC";
/
DROP TYPE "RIB_AppointRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AppointRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AppointRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  appt_nbr number(9),
  from_location varchar2(10),
  AppointDtlRef_TBL "RIB_AppointDtlRef_TBL",   -- Size of "RIB_AppointDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AppointRef_REC"
(
  rib_oid number
, appt_nbr number
, from_location varchar2
) return self as result
,constructor function "RIB_AppointRef_REC"
(
  rib_oid number
, appt_nbr number
, from_location varchar2
, AppointDtlRef_TBL "RIB_AppointDtlRef_TBL"  -- Size of "RIB_AppointDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AppointRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AppointRef') := "ns_name_AppointRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_nbr') := appt_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  IF AppointDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AppointDtlRef_TBL.';
    FOR INDX IN AppointDtlRef_TBL.FIRST()..AppointDtlRef_TBL.LAST() LOOP
      AppointDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'AppointDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_AppointRef_REC"
(
  rib_oid number
, appt_nbr number
, from_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.appt_nbr := appt_nbr;
self.from_location := from_location;
RETURN;
end;
constructor function "RIB_AppointRef_REC"
(
  rib_oid number
, appt_nbr number
, from_location varchar2
, AppointDtlRef_TBL "RIB_AppointDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.appt_nbr := appt_nbr;
self.from_location := from_location;
self.AppointDtlRef_TBL := AppointDtlRef_TBL;
RETURN;
end;
END;
/
