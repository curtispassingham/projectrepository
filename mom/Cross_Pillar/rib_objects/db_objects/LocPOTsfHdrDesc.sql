DROP TYPE "RIB_LocPOTsfHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfHdrDesc_REC";
/
DROP TYPE "RIB_LocPOTsfHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocPOTsfHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocPOTsfHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  order_id number(12),
  source_type varchar2(1),
  source_id number(10),
  create_date date,
  order_status varchar2(6),
  not_before_date date,
  not_after_date date,
  user_id varchar2(30),
  quantity number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocPOTsfHdrDesc_REC"
(
  rib_oid number
, order_id number
, source_type varchar2
, source_id number
, create_date date
, order_status varchar2
, not_before_date date
, not_after_date date
) return self as result
,constructor function "RIB_LocPOTsfHdrDesc_REC"
(
  rib_oid number
, order_id number
, source_type varchar2
, source_id number
, create_date date
, order_status varchar2
, not_before_date date
, not_after_date date
, user_id varchar2
) return self as result
,constructor function "RIB_LocPOTsfHdrDesc_REC"
(
  rib_oid number
, order_id number
, source_type varchar2
, source_id number
, create_date date
, order_status varchar2
, not_before_date date
, not_after_date date
, user_id varchar2
, quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocPOTsfHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocPOTsfHdrDesc') := "ns_name_LocPOTsfHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'order_id') := order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_type') := source_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_id') := source_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_status') := order_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_before_date') := not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
END appendNodeValues;
constructor function "RIB_LocPOTsfHdrDesc_REC"
(
  rib_oid number
, order_id number
, source_type varchar2
, source_id number
, create_date date
, order_status varchar2
, not_before_date date
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.create_date := create_date;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrDesc_REC"
(
  rib_oid number
, order_id number
, source_type varchar2
, source_id number
, create_date date
, order_status varchar2
, not_before_date date
, not_after_date date
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.create_date := create_date;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrDesc_REC"
(
  rib_oid number
, order_id number
, source_type varchar2
, source_id number
, create_date date
, order_status varchar2
, not_before_date date
, not_after_date date
, user_id varchar2
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.create_date := create_date;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.user_id := user_id;
self.quantity := quantity;
RETURN;
end;
END;
/
