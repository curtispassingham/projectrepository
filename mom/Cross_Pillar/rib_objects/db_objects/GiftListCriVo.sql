DROP TYPE "RIB_GiftListCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftListCriVo_REC";
/
DROP TYPE "RIB_GiftListCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftListCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  search_type varchar2(23), -- search_type is enumeration field, valid values are [SEARCH_BY_GIFTLIST_ID, SEARCH_BY_CUSTOMER_ID, SEARCH_BY_GIFTLIST_INFO] (all lower-case)
  fetch_giftitems_flag varchar2(5), --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case) 
  gift_list_id varchar2(40),
  owner_id varchar2(40),
  first_name varchar2(120),
  last_name varchar2(120),
  gift_list_name varchar2(254),
  event_type varchar2(40),
  event_date date,
  published_flag varchar2(5), --published_flag is boolean field, valid values are true,false (all lower-case) 
  public_flag varchar2(5), --public_flag is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
, event_date date
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2  --fetch_giftitems_flag is boolean field, valid values are true,false (all lower-case)
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftListCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCriVo') := "ns_name_GiftListCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'search_type') := search_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'fetch_giftitems_flag') := fetch_giftitems_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_list_id') := gift_list_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'owner_id') := owner_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_list_name') := gift_list_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_type') := event_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_date') := event_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'published_flag') := published_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'public_flag') := public_flag;
END appendNodeValues;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
self.last_name := last_name;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
self.last_name := last_name;
self.gift_list_name := gift_list_name;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
self.last_name := last_name;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
, event_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
self.last_name := last_name;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
self.last_name := last_name;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
RETURN;
end;
constructor function "RIB_GiftListCriVo_REC"
(
  rib_oid number
, search_type varchar2
, fetch_giftitems_flag varchar2
, gift_list_id varchar2
, owner_id varchar2
, first_name varchar2
, last_name varchar2
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.fetch_giftitems_flag := fetch_giftitems_flag;
self.gift_list_id := gift_list_id;
self.owner_id := owner_id;
self.first_name := first_name;
self.last_name := last_name;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
RETURN;
end;
END;
/
