@@RetItemIdentDesc.sql;
/
@@RetTendTypeDesc.sql;
/
@@RetMsgExtDesc.sql;
/
@@RetStoreLangDesc.sql;
/
@@TransIdDesc.sql;
/
@@RetAuthDesc.sql;
/
DROP TYPE "RIB_RetResultDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RetResultDesc_REC";
/
DROP TYPE "RIB_ReturnResult_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReturnResult_REC";
/
DROP TYPE "RIB_OfflineRequest_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OfflineRequest_REC";
/
DROP TYPE "RIB_ItemReturnResult_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemReturnResult_REC";
/
DROP TYPE "RIB_OverrideInfo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OverrideInfo_REC";
/
DROP TYPE "RIB_ReturnTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReturnTender_REC";
/
DROP TYPE "RIB_ReturnTransactionID_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReturnTransactionID_REC";
/
DROP TYPE "RIB_OriginalTransactionID_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OriginalTransactionID_REC";
/
DROP TYPE "RIB_RetResultDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RetResultDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ReturnResult "RIB_ReturnResult_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RetResultDesc_REC"
(
  rib_oid number
, ReturnResult "RIB_ReturnResult_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RetResultDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'ReturnResult.';
  ReturnResult.appendNodeValues( i_prefix||'ReturnResult');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_RetResultDesc_REC"
(
  rib_oid number
, ReturnResult "RIB_ReturnResult_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ReturnResult := ReturnResult;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemReturnResult_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemReturnResult_TBL" AS TABLE OF "RIB_ItemReturnResult_REC";
/
DROP TYPE "RIB_ReturnResult_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReturnResult_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  return_ticket_id varchar2(30),
  offline_date date,
  OfflineRequest "RIB_OfflineRequest_REC",
  ReturnTransactionID "RIB_ReturnTransactionID_REC",
  currency_iso_code varchar2(3),
  ItemReturnResult_TBL "RIB_ItemReturnResult_TBL",   -- Size of "RIB_ItemReturnResult_TBL" is unbounded
  return_voided varchar2(5), --return_voided is boolean field, valid values are true,false (all lower-case) 
  RetMsgExtDesc "RIB_RetMsgExtDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
) return self as result
,constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
, ItemReturnResult_TBL "RIB_ItemReturnResult_TBL"  -- Size of "RIB_ItemReturnResult_TBL" is unbounded
) return self as result
,constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
, ItemReturnResult_TBL "RIB_ItemReturnResult_TBL"  -- Size of "RIB_ItemReturnResult_TBL" is unbounded
, return_voided varchar2  --return_voided is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
, ItemReturnResult_TBL "RIB_ItemReturnResult_TBL"  -- Size of "RIB_ItemReturnResult_TBL" is unbounded
, return_voided varchar2  --return_voided is boolean field, valid values are true,false (all lower-case)
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReturnResult_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'return_ticket_id') := return_ticket_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'offline_date') := offline_date;
  l_new_pre :=i_prefix||'OfflineRequest.';
  OfflineRequest.appendNodeValues( i_prefix||'OfflineRequest');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ReturnTransactionID.';
  ReturnTransactionID.appendNodeValues( i_prefix||'ReturnTransactionID');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_iso_code') := currency_iso_code;
  IF ItemReturnResult_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemReturnResult_TBL.';
    FOR INDX IN ItemReturnResult_TBL.FIRST()..ItemReturnResult_TBL.LAST() LOOP
      ItemReturnResult_TBL(indx).appendNodeValues( i_prefix||indx||'ItemReturnResult_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_voided') := return_voided;
  l_new_pre :=i_prefix||'RetMsgExtDesc.';
  RetMsgExtDesc.appendNodeValues( i_prefix||'RetMsgExtDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_ticket_id := return_ticket_id;
self.offline_date := offline_date;
self.OfflineRequest := OfflineRequest;
self.ReturnTransactionID := ReturnTransactionID;
self.currency_iso_code := currency_iso_code;
RETURN;
end;
constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
, ItemReturnResult_TBL "RIB_ItemReturnResult_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_ticket_id := return_ticket_id;
self.offline_date := offline_date;
self.OfflineRequest := OfflineRequest;
self.ReturnTransactionID := ReturnTransactionID;
self.currency_iso_code := currency_iso_code;
self.ItemReturnResult_TBL := ItemReturnResult_TBL;
RETURN;
end;
constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
, ItemReturnResult_TBL "RIB_ItemReturnResult_TBL"
, return_voided varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_ticket_id := return_ticket_id;
self.offline_date := offline_date;
self.OfflineRequest := OfflineRequest;
self.ReturnTransactionID := ReturnTransactionID;
self.currency_iso_code := currency_iso_code;
self.ItemReturnResult_TBL := ItemReturnResult_TBL;
self.return_voided := return_voided;
RETURN;
end;
constructor function "RIB_ReturnResult_REC"
(
  rib_oid number
, return_ticket_id varchar2
, offline_date date
, OfflineRequest "RIB_OfflineRequest_REC"
, ReturnTransactionID "RIB_ReturnTransactionID_REC"
, currency_iso_code varchar2
, ItemReturnResult_TBL "RIB_ItemReturnResult_TBL"
, return_voided varchar2
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_ticket_id := return_ticket_id;
self.offline_date := offline_date;
self.OfflineRequest := OfflineRequest;
self.ReturnTransactionID := ReturnTransactionID;
self.currency_iso_code := currency_iso_code;
self.ItemReturnResult_TBL := ItemReturnResult_TBL;
self.return_voided := return_voided;
self.RetMsgExtDesc := RetMsgExtDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_OfflineRequest_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OfflineRequest_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetAuthDesc "RIB_RetAuthDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OfflineRequest_REC"
(
  rib_oid number
, RetAuthDesc "RIB_RetAuthDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OfflineRequest_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetAuthDesc.';
  RetAuthDesc.appendNodeValues( i_prefix||'RetAuthDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_OfflineRequest_REC"
(
  rib_oid number
, RetAuthDesc "RIB_RetAuthDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetAuthDesc := RetAuthDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemReturnResult_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemReturnResult_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetItemIdentDesc "RIB_RetItemIdentDesc_REC",
  quantity_returned number(11),
  final_result_code varchar2(11), -- final_result_code is enumeration field, valid values are [Authorized, Denial] (all lower-case)
  OverrideInfo "RIB_OverrideInfo_REC",
  OriginalTransactionID "RIB_OriginalTransactionID_REC",
  ReturnTender "RIB_ReturnTender_REC",
  RetMsgExtDesc "RIB_RetMsgExtDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
) return self as result
,constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
) return self as result
,constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
) return self as result
,constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
, OriginalTransactionID "RIB_OriginalTransactionID_REC"
) return self as result
,constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
, OriginalTransactionID "RIB_OriginalTransactionID_REC"
, ReturnTender "RIB_ReturnTender_REC"
) return self as result
,constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
, OriginalTransactionID "RIB_OriginalTransactionID_REC"
, ReturnTender "RIB_ReturnTender_REC"
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemReturnResult_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetItemIdentDesc.';
  RetItemIdentDesc.appendNodeValues( i_prefix||'RetItemIdentDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_returned') := quantity_returned;
  rib_obj_util.g_RIB_element_values(i_prefix||'final_result_code') := final_result_code;
  l_new_pre :=i_prefix||'OverrideInfo.';
  OverrideInfo.appendNodeValues( i_prefix||'OverrideInfo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'OriginalTransactionID.';
  OriginalTransactionID.appendNodeValues( i_prefix||'OriginalTransactionID');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ReturnTender.';
  ReturnTender.appendNodeValues( i_prefix||'ReturnTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'RetMsgExtDesc.';
  RetMsgExtDesc.appendNodeValues( i_prefix||'RetMsgExtDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.quantity_returned := quantity_returned;
RETURN;
end;
constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.quantity_returned := quantity_returned;
self.final_result_code := final_result_code;
RETURN;
end;
constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.quantity_returned := quantity_returned;
self.final_result_code := final_result_code;
self.OverrideInfo := OverrideInfo;
RETURN;
end;
constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
, OriginalTransactionID "RIB_OriginalTransactionID_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.quantity_returned := quantity_returned;
self.final_result_code := final_result_code;
self.OverrideInfo := OverrideInfo;
self.OriginalTransactionID := OriginalTransactionID;
RETURN;
end;
constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
, OriginalTransactionID "RIB_OriginalTransactionID_REC"
, ReturnTender "RIB_ReturnTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.quantity_returned := quantity_returned;
self.final_result_code := final_result_code;
self.OverrideInfo := OverrideInfo;
self.OriginalTransactionID := OriginalTransactionID;
self.ReturnTender := ReturnTender;
RETURN;
end;
constructor function "RIB_ItemReturnResult_REC"
(
  rib_oid number
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, quantity_returned number
, final_result_code varchar2
, OverrideInfo "RIB_OverrideInfo_REC"
, OriginalTransactionID "RIB_OriginalTransactionID_REC"
, ReturnTender "RIB_ReturnTender_REC"
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetItemIdentDesc := RetItemIdentDesc;
self.quantity_returned := quantity_returned;
self.final_result_code := final_result_code;
self.OverrideInfo := OverrideInfo;
self.OriginalTransactionID := OriginalTransactionID;
self.ReturnTender := ReturnTender;
self.RetMsgExtDesc := RetMsgExtDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_OverrideInfo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OverrideInfo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  manager_id varchar2(14),
  override_obtained varchar2(5), --override_obtained is boolean field, valid values are true,false (all lower-case) 
  tender_override varchar2(5), --tender_override is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OverrideInfo_REC"
(
  rib_oid number
, manager_id varchar2
, override_obtained varchar2  --override_obtained is boolean field, valid values are true,false (all lower-case)
, tender_override varchar2  --tender_override is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OverrideInfo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'manager_id') := manager_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'override_obtained') := override_obtained;
  rib_obj_util.g_RIB_element_values(i_prefix||'tender_override') := tender_override;
END appendNodeValues;
constructor function "RIB_OverrideInfo_REC"
(
  rib_oid number
, manager_id varchar2
, override_obtained varchar2
, tender_override varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.manager_id := manager_id;
self.override_obtained := override_obtained;
self.tender_override := tender_override;
RETURN;
end;
END;
/
DROP TYPE "RIB_RetTendTypeDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RetTendTypeDesc_TBL" AS TABLE OF "RIB_RetTendTypeDesc_REC";
/
DROP TYPE "RIB_ReturnTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReturnTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL",   -- Size of "RIB_RetTendTypeDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReturnTender_REC"
(
  rib_oid number
, RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL"  -- Size of "RIB_RetTendTypeDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReturnTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'RetTendTypeDesc_TBL.';
  FOR INDX IN RetTendTypeDesc_TBL.FIRST()..RetTendTypeDesc_TBL.LAST() LOOP
    RetTendTypeDesc_TBL(indx).appendNodeValues( i_prefix||indx||'RetTendTypeDesc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ReturnTender_REC"
(
  rib_oid number
, RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetTendTypeDesc_TBL := RetTendTypeDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReturnTransactionID_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReturnTransactionID_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  TransIdDesc "RIB_TransIdDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReturnTransactionID_REC"
(
  rib_oid number
, TransIdDesc "RIB_TransIdDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReturnTransactionID_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'TransIdDesc.';
  TransIdDesc.appendNodeValues( i_prefix||'TransIdDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ReturnTransactionID_REC"
(
  rib_oid number
, TransIdDesc "RIB_TransIdDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.TransIdDesc := TransIdDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_OriginalTransactionID_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OriginalTransactionID_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetResultDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  TransIdDesc "RIB_TransIdDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OriginalTransactionID_REC"
(
  rib_oid number
, TransIdDesc "RIB_TransIdDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OriginalTransactionID_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetResultDesc') := "ns_name_RetResultDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'TransIdDesc.';
  TransIdDesc.appendNodeValues( i_prefix||'TransIdDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_OriginalTransactionID_REC"
(
  rib_oid number
, TransIdDesc "RIB_TransIdDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.TransIdDesc := TransIdDesc;
RETURN;
end;
END;
/
