@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_StrVrnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnDesc_REC";
/
DROP TYPE "RIB_StrVrnItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnItm_REC";
/
DROP TYPE "RIB_StrVrnItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnItm_TBL" AS TABLE OF "RIB_StrVrnItm_REC";
/
DROP TYPE "RIB_StrVrnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVrnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  return_id number(15),
  external_id varchar2(128),
  store_id number(10),
  supplier_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [REQUESTED, REQUEST_IN_PROGRESS, RTV_IN_PROGRESS, APPROVED, IN_SHIPPING, COMPLETED, REJECTED, CANCELED_REQUEST, CANCELED_RTV, UNKNOWN] (all lower-case)
  context varchar2(20), -- context is enumeration field, valid values are [INVALID_DELIVERY, SEASONAL, DAMAGED, NONE, UNKNOWN] (all lower-case)
  authorization_number varchar2(12),
  not_after_date date,
  create_user_name varchar2(128),
  create_date date,
  update_user_name varchar2(128),
  update_date date,
  approved_user_name varchar2(128),
  approved_date date,
  closed_user_name varchar2(128),
  closed_user_date date,
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  StrVrnItm_TBL "RIB_StrVrnItm_TBL",   -- Size of "RIB_StrVrnItm_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
, closed_user_date date
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
, closed_user_date date
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
,constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
, closed_user_date date
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, StrVrnItm_TBL "RIB_StrVrnItm_TBL"  -- Size of "RIB_StrVrnItm_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVrnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVrnDesc') := "ns_name_StrVrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'return_id') := return_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'context') := context;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_number') := authorization_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user_name') := update_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_user_name') := approved_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_date') := approved_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'closed_user_name') := closed_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'closed_user_date') := closed_user_date;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF StrVrnItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVrnItm_TBL.';
    FOR INDX IN StrVrnItm_TBL.FIRST()..StrVrnItm_TBL.LAST() LOOP
      StrVrnItm_TBL(indx).appendNodeValues( i_prefix||indx||'StrVrnItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
self.closed_user_name := closed_user_name;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
, closed_user_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
self.closed_user_name := closed_user_name;
self.closed_user_date := closed_user_date;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
, closed_user_date date
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
self.closed_user_name := closed_user_name;
self.closed_user_date := closed_user_date;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
constructor function "RIB_StrVrnDesc_REC"
(
  rib_oid number
, return_id number
, external_id varchar2
, store_id number
, supplier_id number
, status varchar2
, context varchar2
, authorization_number varchar2
, not_after_date date
, create_user_name varchar2
, create_date date
, update_user_name varchar2
, update_date date
, approved_user_name varchar2
, approved_date date
, closed_user_name varchar2
, closed_user_date date
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, StrVrnItm_TBL "RIB_StrVrnItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.external_id := external_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.status := status;
self.context := context;
self.authorization_number := authorization_number;
self.not_after_date := not_after_date;
self.create_user_name := create_user_name;
self.create_date := create_date;
self.update_user_name := update_user_name;
self.update_date := update_date;
self.approved_user_name := approved_user_name;
self.approved_date := approved_date;
self.closed_user_name := closed_user_name;
self.closed_user_date := closed_user_date;
self.GeoAddrDesc := GeoAddrDesc;
self.StrVrnItm_TBL := StrVrnItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrVrnItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVrnItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  item_id varchar2(25),
  description varchar2(400),
  reason_id number(15),
  case_size number(10,2),
  quantity_requested number(20,4),
  quantity_approved number(20,4),
  quantity_in_shipping number(20,4),
  quantity_shipped number(20,4),
  external_sequence number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
) return self as result
,constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
) return self as result
,constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
) return self as result
,constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
, quantity_in_shipping number
) return self as result
,constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
, quantity_in_shipping number
, quantity_shipped number
) return self as result
,constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
, quantity_in_shipping number
, quantity_shipped number
, external_sequence number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVrnItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVrnDesc') := "ns_name_StrVrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_requested') := quantity_requested;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_approved') := quantity_approved;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_in_shipping') := quantity_in_shipping;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_shipped') := quantity_shipped;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_sequence') := external_sequence;
END appendNodeValues;
constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.case_size := case_size;
RETURN;
end;
constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_requested := quantity_requested;
RETURN;
end;
constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_requested := quantity_requested;
self.quantity_approved := quantity_approved;
RETURN;
end;
constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
, quantity_in_shipping number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_requested := quantity_requested;
self.quantity_approved := quantity_approved;
self.quantity_in_shipping := quantity_in_shipping;
RETURN;
end;
constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
, quantity_in_shipping number
, quantity_shipped number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_requested := quantity_requested;
self.quantity_approved := quantity_approved;
self.quantity_in_shipping := quantity_in_shipping;
self.quantity_shipped := quantity_shipped;
RETURN;
end;
constructor function "RIB_StrVrnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, reason_id number
, case_size number
, quantity_requested number
, quantity_approved number
, quantity_in_shipping number
, quantity_shipped number
, external_sequence number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_requested := quantity_requested;
self.quantity_approved := quantity_approved;
self.quantity_in_shipping := quantity_in_shipping;
self.quantity_shipped := quantity_shipped;
self.external_sequence := external_sequence;
RETURN;
end;
END;
/
