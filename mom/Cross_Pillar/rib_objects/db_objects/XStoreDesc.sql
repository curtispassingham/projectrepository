@@XStoreWT.sql;
/
@@XStoreLocTrt.sql;
/
@@AddrDesc.sql;
/
DROP TYPE "RIB_XStoreDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XStoreDesc_REC";
/
DROP TYPE "RIB_XStoreLocTrt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XStoreLocTrt_TBL" AS TABLE OF "RIB_XStoreLocTrt_REC";
/
DROP TYPE "RIB_XStoreWT_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XStoreWT_TBL" AS TABLE OF "RIB_XStoreWT_REC";
/
DROP TYPE "RIB_AddrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrDesc_TBL" AS TABLE OF "RIB_AddrDesc_REC";
/
DROP TYPE "RIB_XStoreDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XStoreDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XStoreDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store number(10),
  store_name varchar2(150),
  store_type varchar2(1),
  store_name10 varchar2(10),
  store_name3 varchar2(3),
  store_class varchar2(1),
  store_mgr_name varchar2(120),
  store_open_date date,
  store_close_date date,
  acquired_date date,
  remodel_date date,
  fax_number varchar2(20),
  phone_number varchar2(20),
  email varchar2(100),
  total_square_ft number(8),
  selling_square_ft number(8),
  linear_distance number(8),
  stockholding_ind varchar2(1),
  channel_id number(4),
  store_format number(4),
  mall_name varchar2(120),
  district number(10),
  promo_zone number(4),
  transfer_zone number(4),
  default_wh number(10),
  stop_order_days number(3),
  start_order_days number(3),
  currency_code varchar2(3),
  lang number(6),
  iso_code varchar2(6),
  integrated_pos_ind varchar2(1),
  duns_number varchar2(9),
  duns_loc varchar2(4),
  copy_dlvry_ind varchar2(1),
  copy_activity_ind varchar2(1),
  price_store number(10),
  cost_location number(10),
  vat_include_ind varchar2(1),
  vat_region number(4),
  like_store number(10),
  copy_repl_ind varchar2(1),
  transfer_entity number(10),
  sister_store number(10),
  tran_no_generated varchar2(6),
  county varchar2(250),
  XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL",   -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
  XStoreWT_TBL "RIB_XStoreWT_TBL",   -- Size of "RIB_XStoreWT_TBL" is unbounded
  AddrDesc_TBL "RIB_AddrDesc_TBL",   -- Size of "RIB_AddrDesc_TBL" is unbounded
  timezone_name varchar2(64),
  wf_customer_id number(10),
  org_unit_id number(15),
  store_name_secondary varchar2(150),
  customer_order_loc_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, timezone_name varchar2
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, timezone_name varchar2
, wf_customer_id number
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, timezone_name varchar2
, wf_customer_id number
, org_unit_id number
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, timezone_name varchar2
, wf_customer_id number
, org_unit_id number
, store_name_secondary varchar2
) return self as result
,constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"  -- Size of "RIB_XStoreLocTrt_TBL" is unbounded
, XStoreWT_TBL "RIB_XStoreWT_TBL"  -- Size of "RIB_XStoreWT_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, timezone_name varchar2
, wf_customer_id number
, org_unit_id number
, store_name_secondary varchar2
, customer_order_loc_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XStoreDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XStoreDesc') := "ns_name_XStoreDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name') := store_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name10') := store_name10;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name3') := store_name3;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_class') := store_class;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_mgr_name') := store_mgr_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_open_date') := store_open_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_close_date') := store_close_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'acquired_date') := acquired_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'remodel_date') := remodel_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'fax_number') := fax_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'email') := email;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_square_ft') := total_square_ft;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_square_ft') := selling_square_ft;
  rib_obj_util.g_RIB_element_values(i_prefix||'linear_distance') := linear_distance;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_format') := store_format;
  rib_obj_util.g_RIB_element_values(i_prefix||'mall_name') := mall_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'district') := district;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_zone') := promo_zone;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_zone') := transfer_zone;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_wh') := default_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'stop_order_days') := stop_order_days;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_order_days') := start_order_days;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'iso_code') := iso_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'integrated_pos_ind') := integrated_pos_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_number') := duns_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_loc') := duns_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'copy_dlvry_ind') := copy_dlvry_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'copy_activity_ind') := copy_activity_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_store') := price_store;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_location') := cost_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_include_ind') := vat_include_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_region') := vat_region;
  rib_obj_util.g_RIB_element_values(i_prefix||'like_store') := like_store;
  rib_obj_util.g_RIB_element_values(i_prefix||'copy_repl_ind') := copy_repl_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_entity') := transfer_entity;
  rib_obj_util.g_RIB_element_values(i_prefix||'sister_store') := sister_store;
  rib_obj_util.g_RIB_element_values(i_prefix||'tran_no_generated') := tran_no_generated;
  rib_obj_util.g_RIB_element_values(i_prefix||'county') := county;
  IF XStoreLocTrt_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XStoreLocTrt_TBL.';
    FOR INDX IN XStoreLocTrt_TBL.FIRST()..XStoreLocTrt_TBL.LAST() LOOP
      XStoreLocTrt_TBL(indx).appendNodeValues( i_prefix||indx||'XStoreLocTrt_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XStoreWT_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XStoreWT_TBL.';
    FOR INDX IN XStoreWT_TBL.FIRST()..XStoreWT_TBL.LAST() LOOP
      XStoreWT_TBL(indx).appendNodeValues( i_prefix||indx||'XStoreWT_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF AddrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrDesc_TBL.';
    FOR INDX IN AddrDesc_TBL.FIRST()..AddrDesc_TBL.LAST() LOOP
      AddrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'AddrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'timezone_name') := timezone_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'wf_customer_id') := wf_customer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'org_unit_id') := org_unit_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name_secondary') := store_name_secondary;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_loc_ind') := customer_order_loc_ind;
END appendNodeValues;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, timezone_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
self.timezone_name := timezone_name;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, timezone_name varchar2
, wf_customer_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
self.timezone_name := timezone_name;
self.wf_customer_id := wf_customer_id;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, timezone_name varchar2
, wf_customer_id number
, org_unit_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
self.timezone_name := timezone_name;
self.wf_customer_id := wf_customer_id;
self.org_unit_id := org_unit_id;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, timezone_name varchar2
, wf_customer_id number
, org_unit_id number
, store_name_secondary varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
self.timezone_name := timezone_name;
self.wf_customer_id := wf_customer_id;
self.org_unit_id := org_unit_id;
self.store_name_secondary := store_name_secondary;
RETURN;
end;
constructor function "RIB_XStoreDesc_REC"
(
  rib_oid number
, store number
, store_name varchar2
, store_type varchar2
, store_name10 varchar2
, store_name3 varchar2
, store_class varchar2
, store_mgr_name varchar2
, store_open_date date
, store_close_date date
, acquired_date date
, remodel_date date
, fax_number varchar2
, phone_number varchar2
, email varchar2
, total_square_ft number
, selling_square_ft number
, linear_distance number
, stockholding_ind varchar2
, channel_id number
, store_format number
, mall_name varchar2
, district number
, promo_zone number
, transfer_zone number
, default_wh number
, stop_order_days number
, start_order_days number
, currency_code varchar2
, lang number
, iso_code varchar2
, integrated_pos_ind varchar2
, duns_number varchar2
, duns_loc varchar2
, copy_dlvry_ind varchar2
, copy_activity_ind varchar2
, price_store number
, cost_location number
, vat_include_ind varchar2
, vat_region number
, like_store number
, copy_repl_ind varchar2
, transfer_entity number
, sister_store number
, tran_no_generated varchar2
, county varchar2
, XStoreLocTrt_TBL "RIB_XStoreLocTrt_TBL"
, XStoreWT_TBL "RIB_XStoreWT_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, timezone_name varchar2
, wf_customer_id number
, org_unit_id number
, store_name_secondary varchar2
, customer_order_loc_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
self.store_name := store_name;
self.store_type := store_type;
self.store_name10 := store_name10;
self.store_name3 := store_name3;
self.store_class := store_class;
self.store_mgr_name := store_mgr_name;
self.store_open_date := store_open_date;
self.store_close_date := store_close_date;
self.acquired_date := acquired_date;
self.remodel_date := remodel_date;
self.fax_number := fax_number;
self.phone_number := phone_number;
self.email := email;
self.total_square_ft := total_square_ft;
self.selling_square_ft := selling_square_ft;
self.linear_distance := linear_distance;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.store_format := store_format;
self.mall_name := mall_name;
self.district := district;
self.promo_zone := promo_zone;
self.transfer_zone := transfer_zone;
self.default_wh := default_wh;
self.stop_order_days := stop_order_days;
self.start_order_days := start_order_days;
self.currency_code := currency_code;
self.lang := lang;
self.iso_code := iso_code;
self.integrated_pos_ind := integrated_pos_ind;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.copy_dlvry_ind := copy_dlvry_ind;
self.copy_activity_ind := copy_activity_ind;
self.price_store := price_store;
self.cost_location := cost_location;
self.vat_include_ind := vat_include_ind;
self.vat_region := vat_region;
self.like_store := like_store;
self.copy_repl_ind := copy_repl_ind;
self.transfer_entity := transfer_entity;
self.sister_store := sister_store;
self.tran_no_generated := tran_no_generated;
self.county := county;
self.XStoreLocTrt_TBL := XStoreLocTrt_TBL;
self.XStoreWT_TBL := XStoreWT_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
self.timezone_name := timezone_name;
self.wf_customer_id := wf_customer_id;
self.org_unit_id := org_unit_id;
self.store_name_secondary := store_name_secondary;
self.customer_order_loc_ind := customer_order_loc_ind;
RETURN;
end;
END;
/
