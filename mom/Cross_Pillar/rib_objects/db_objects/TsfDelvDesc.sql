DROP TYPE "RIB_TsfDelvDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvDesc_REC";
/
DROP TYPE "RIB_TsfDelvDescNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvDescNote_REC";
/
DROP TYPE "RIB_TsfDelvDescNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvDescNote_TBL" AS TABLE OF "RIB_TsfDelvDescNote_REC";
/
DROP TYPE "RIB_TsfDelvDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(15),
  store_id number(10),
  asn varchar2(128),
  source_type varchar2(10), -- source_type is enumeration field, valid values are [WAREHOUSE, FINISHER, STORE, UNKNOWN] (all lower-case)
  source_name varchar2(250),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, RECEIVED, CANCELED, UNKNOWN] (all lower-case)
  carrier_name varchar2(128),
  carrier_type varchar2(20), -- carrier_type is enumeration field, valid values are [CORPORATE, THIRD_PARTY, UNKNOWN] (all lower-case)
  carrier_code varchar2(4),
  source_address varchar2(1000),
  license_plate varchar2(128),
  freight_id varchar2(128),
  external_bol varchar2(128),
  create_date date,
  update_date date,
  expected_date date,
  received_date date,
  create_user varchar2(128),
  update_user varchar2(128),
  received_user varchar2(128),
  adhoc_document_id number(15),
  num_of_cartons number(10),
  total_skus number(10),
  total_documents number(10),
  TsfDelvDescNote_TBL "RIB_TsfDelvDescNote_TBL",   -- Size of "RIB_TsfDelvDescNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, asn varchar2
, source_type varchar2
, source_name varchar2
, status varchar2
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
, external_bol varchar2
, create_date date
, update_date date
, expected_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adhoc_document_id number
, num_of_cartons number
, total_skus number
, total_documents number
) return self as result
,constructor function "RIB_TsfDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, asn varchar2
, source_type varchar2
, source_name varchar2
, status varchar2
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
, external_bol varchar2
, create_date date
, update_date date
, expected_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adhoc_document_id number
, num_of_cartons number
, total_skus number
, total_documents number
, TsfDelvDescNote_TBL "RIB_TsfDelvDescNote_TBL"  -- Size of "RIB_TsfDelvDescNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvDesc') := "ns_name_TsfDelvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_type') := source_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_name') := source_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_type') := carrier_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_address') := source_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'license_plate') := license_plate;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_id') := freight_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_bol') := external_bol;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_date') := expected_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_date') := received_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_user') := received_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'adhoc_document_id') := adhoc_document_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'num_of_cartons') := num_of_cartons;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_skus') := total_skus;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_documents') := total_documents;
  IF TsfDelvDescNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDelvDescNote_TBL.';
    FOR INDX IN TsfDelvDescNote_TBL.FIRST()..TsfDelvDescNote_TBL.LAST() LOOP
      TsfDelvDescNote_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDelvDescNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, asn varchar2
, source_type varchar2
, source_name varchar2
, status varchar2
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
, external_bol varchar2
, create_date date
, update_date date
, expected_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adhoc_document_id number
, num_of_cartons number
, total_skus number
, total_documents number
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.asn := asn;
self.source_type := source_type;
self.source_name := source_name;
self.status := status;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.source_address := source_address;
self.license_plate := license_plate;
self.freight_id := freight_id;
self.external_bol := external_bol;
self.create_date := create_date;
self.update_date := update_date;
self.expected_date := expected_date;
self.received_date := received_date;
self.create_user := create_user;
self.update_user := update_user;
self.received_user := received_user;
self.adhoc_document_id := adhoc_document_id;
self.num_of_cartons := num_of_cartons;
self.total_skus := total_skus;
self.total_documents := total_documents;
RETURN;
end;
constructor function "RIB_TsfDelvDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, asn varchar2
, source_type varchar2
, source_name varchar2
, status varchar2
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, source_address varchar2
, license_plate varchar2
, freight_id varchar2
, external_bol varchar2
, create_date date
, update_date date
, expected_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adhoc_document_id number
, num_of_cartons number
, total_skus number
, total_documents number
, TsfDelvDescNote_TBL "RIB_TsfDelvDescNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.asn := asn;
self.source_type := source_type;
self.source_name := source_name;
self.status := status;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.source_address := source_address;
self.license_plate := license_plate;
self.freight_id := freight_id;
self.external_bol := external_bol;
self.create_date := create_date;
self.update_date := update_date;
self.expected_date := expected_date;
self.received_date := received_date;
self.create_user := create_user;
self.update_user := update_user;
self.received_user := received_user;
self.adhoc_document_id := adhoc_document_id;
self.num_of_cartons := num_of_cartons;
self.total_skus := total_skus;
self.total_documents := total_documents;
self.TsfDelvDescNote_TBL := TsfDelvDescNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDelvDescNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvDescNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvDescNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvDesc') := "ns_name_TsfDelvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_TsfDelvDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
