DROP TYPE "RIB_LocPOTsfItmSlsDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfItmSlsDesc_REC";
/
DROP TYPE "RIB_LocPOTsfItmSlsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocPOTsfItmSlsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocPOTsfItmSlsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  eow_date date,
  quantity number(12,4),
  sales_value number(20,4),
  sales_type varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocPOTsfItmSlsDesc_REC"
(
  rib_oid number
, eow_date date
, quantity number
, sales_value number
, sales_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocPOTsfItmSlsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocPOTsfItmSlsDesc') := "ns_name_LocPOTsfItmSlsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'eow_date') := eow_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'sales_value') := sales_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'sales_type') := sales_type;
END appendNodeValues;
constructor function "RIB_LocPOTsfItmSlsDesc_REC"
(
  rib_oid number
, eow_date date
, quantity number
, sales_value number
, sales_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.eow_date := eow_date;
self.quantity := quantity;
self.sales_value := sales_value;
self.sales_type := sales_type;
RETURN;
end;
END;
/
