DROP TYPE "RIB_TsfShipDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipDesc_REC";
/
DROP TYPE "RIB_TsfShipDescNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipDescNote_REC";
/
DROP TYPE "RIB_TsfShipDescNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipDescNote_TBL" AS TABLE OF "RIB_TsfShipDescNote_REC";
/
DROP TYPE "RIB_TsfShipDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  store_id number(10),
  store_name varchar2(128),
  bol_id number(12),
  asn varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, SHIPPED, CANCELED, UNKNOWN] (all lower-case)
  destination_type varchar2(20), -- destination_type is enumeration field, valid values are [FINISHER, STORE, WAREHOUSE, UNKNOWN] (all lower-case)
  destination_name varchar2(250),
  not_after_date date,
  authorization_code varchar2(12),
  adhoc_doc_id number(15),
  create_user varchar2(128),
  update_user varchar2(128),
  submit_user varchar2(128),
  dispatch_user varchar2(128),
  create_date date,
  last_update_date date,
  submit_date date,
  dispatch_date date,
  num_of_cartons number(12),
  skus number(12),
  num_of_docs number(12),
  TsfShipDescNote_TBL "RIB_TsfShipDescNote_TBL",   -- Size of "RIB_TsfShipDescNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, store_name varchar2
, bol_id number
, asn varchar2
, status varchar2
, destination_type varchar2
, destination_name varchar2
, not_after_date date
, authorization_code varchar2
, adhoc_doc_id number
, create_user varchar2
, update_user varchar2
, submit_user varchar2
, dispatch_user varchar2
, create_date date
, last_update_date date
, submit_date date
, dispatch_date date
, num_of_cartons number
, skus number
, num_of_docs number
) return self as result
,constructor function "RIB_TsfShipDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, store_name varchar2
, bol_id number
, asn varchar2
, status varchar2
, destination_type varchar2
, destination_name varchar2
, not_after_date date
, authorization_code varchar2
, adhoc_doc_id number
, create_user varchar2
, update_user varchar2
, submit_user varchar2
, dispatch_user varchar2
, create_date date
, last_update_date date
, submit_date date
, dispatch_date date
, num_of_cartons number
, skus number
, num_of_docs number
, TsfShipDescNote_TBL "RIB_TsfShipDescNote_TBL"  -- Size of "RIB_TsfShipDescNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipDesc') := "ns_name_TsfShipDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_name') := store_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_id') := bol_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_type') := destination_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'destination_name') := destination_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'adhoc_doc_id') := adhoc_doc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'submit_user') := submit_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_user') := dispatch_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_date') := last_update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'submit_date') := submit_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'dispatch_date') := dispatch_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'num_of_cartons') := num_of_cartons;
  rib_obj_util.g_RIB_element_values(i_prefix||'skus') := skus;
  rib_obj_util.g_RIB_element_values(i_prefix||'num_of_docs') := num_of_docs;
  IF TsfShipDescNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfShipDescNote_TBL.';
    FOR INDX IN TsfShipDescNote_TBL.FIRST()..TsfShipDescNote_TBL.LAST() LOOP
      TsfShipDescNote_TBL(indx).appendNodeValues( i_prefix||indx||'TsfShipDescNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfShipDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, store_name varchar2
, bol_id number
, asn varchar2
, status varchar2
, destination_type varchar2
, destination_name varchar2
, not_after_date date
, authorization_code varchar2
, adhoc_doc_id number
, create_user varchar2
, update_user varchar2
, submit_user varchar2
, dispatch_user varchar2
, create_date date
, last_update_date date
, submit_date date
, dispatch_date date
, num_of_cartons number
, skus number
, num_of_docs number
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.store_name := store_name;
self.bol_id := bol_id;
self.asn := asn;
self.status := status;
self.destination_type := destination_type;
self.destination_name := destination_name;
self.not_after_date := not_after_date;
self.authorization_code := authorization_code;
self.adhoc_doc_id := adhoc_doc_id;
self.create_user := create_user;
self.update_user := update_user;
self.submit_user := submit_user;
self.dispatch_user := dispatch_user;
self.create_date := create_date;
self.last_update_date := last_update_date;
self.submit_date := submit_date;
self.dispatch_date := dispatch_date;
self.num_of_cartons := num_of_cartons;
self.skus := skus;
self.num_of_docs := num_of_docs;
RETURN;
end;
constructor function "RIB_TsfShipDesc_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, store_name varchar2
, bol_id number
, asn varchar2
, status varchar2
, destination_type varchar2
, destination_name varchar2
, not_after_date date
, authorization_code varchar2
, adhoc_doc_id number
, create_user varchar2
, update_user varchar2
, submit_user varchar2
, dispatch_user varchar2
, create_date date
, last_update_date date
, submit_date date
, dispatch_date date
, num_of_cartons number
, skus number
, num_of_docs number
, TsfShipDescNote_TBL "RIB_TsfShipDescNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.store_name := store_name;
self.bol_id := bol_id;
self.asn := asn;
self.status := status;
self.destination_type := destination_type;
self.destination_name := destination_name;
self.not_after_date := not_after_date;
self.authorization_code := authorization_code;
self.adhoc_doc_id := adhoc_doc_id;
self.create_user := create_user;
self.update_user := update_user;
self.submit_user := submit_user;
self.dispatch_user := dispatch_user;
self.create_date := create_date;
self.last_update_date := last_update_date;
self.submit_date := submit_date;
self.dispatch_date := dispatch_date;
self.num_of_cartons := num_of_cartons;
self.skus := skus;
self.num_of_docs := num_of_docs;
self.TsfShipDescNote_TBL := TsfShipDescNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfShipDescNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipDescNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipDescNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipDesc') := "ns_name_TsfShipDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_TsfShipDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
