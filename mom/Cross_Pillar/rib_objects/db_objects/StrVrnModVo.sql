@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_StrVrnModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnModVo_REC";
/
DROP TYPE "RIB_StrVrnItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnItmMod_REC";
/
DROP TYPE "RIB_StrVrnItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVrnItmMod_TBL" AS TABLE OF "RIB_StrVrnItmMod_REC";
/
DROP TYPE "RIB_StrVrnModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVrnModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVrnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  return_id number(12),
  store_id number(10),
  external_id varchar2(128),
  supplier_id number(10),
  not_after_date date,
  authorization_number varchar2(12),
  context varchar2(20), -- context is enumeration field, valid values are [INVALID_DELIVERY, SEASONAL, DAMAGED, NONE, UNKNOWN] (all lower-case)
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 5000
  StrVrnItmMod_TBL "RIB_StrVrnItmMod_TBL",   -- Size of "RIB_StrVrnItmMod_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
) return self as result
,constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
,constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
) return self as result
,constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
, StrVrnItmMod_TBL "RIB_StrVrnItmMod_TBL"  -- Size of "RIB_StrVrnItmMod_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVrnModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVrnModVo') := "ns_name_StrVrnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'return_id') := return_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_number') := authorization_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'context') := context;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrVrnItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVrnItmMod_TBL.';
    FOR INDX IN StrVrnItmMod_TBL.FIRST()..StrVrnItmMod_TBL.LAST() LOOP
      StrVrnItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'StrVrnItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.store_id := store_id;
self.external_id := external_id;
self.supplier_id := supplier_id;
self.not_after_date := not_after_date;
self.authorization_number := authorization_number;
self.context := context;
RETURN;
end;
constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.store_id := store_id;
self.external_id := external_id;
self.supplier_id := supplier_id;
self.not_after_date := not_after_date;
self.authorization_number := authorization_number;
self.context := context;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.store_id := store_id;
self.external_id := external_id;
self.supplier_id := supplier_id;
self.not_after_date := not_after_date;
self.authorization_number := authorization_number;
self.context := context;
self.GeoAddrDesc := GeoAddrDesc;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
constructor function "RIB_StrVrnModVo_REC"
(
  rib_oid number
, return_id number
, store_id number
, external_id varchar2
, supplier_id number
, not_after_date date
, authorization_number varchar2
, context varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
, StrVrnItmMod_TBL "RIB_StrVrnItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.return_id := return_id;
self.store_id := store_id;
self.external_id := external_id;
self.supplier_id := supplier_id;
self.not_after_date := not_after_date;
self.authorization_number := authorization_number;
self.context := context;
self.GeoAddrDesc := GeoAddrDesc;
self.removed_line_id_col := removed_line_id_col;
self.StrVrnItmMod_TBL := StrVrnItmMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_StrVrnItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVrnItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVrnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  item_id varchar2(25),
  reason_id number(15),
  case_size number(10,2),
  quantity_approved number(20,4),
  external_sequence number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVrnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity_approved number
) return self as result
,constructor function "RIB_StrVrnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity_approved number
, external_sequence number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVrnItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVrnModVo') := "ns_name_StrVrnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_approved') := quantity_approved;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_sequence') := external_sequence;
END appendNodeValues;
constructor function "RIB_StrVrnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity_approved number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_approved := quantity_approved;
RETURN;
end;
constructor function "RIB_StrVrnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity_approved number
, external_sequence number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity_approved := quantity_approved;
self.external_sequence := external_sequence;
RETURN;
end;
END;
/
