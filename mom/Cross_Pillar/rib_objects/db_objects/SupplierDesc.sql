@@ExtOfSupplierDesc.sql;
/
@@LocOfSupplierDesc.sql;
/
DROP TYPE "RIB_SupplierDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierDesc_REC";
/
DROP TYPE "RIB_SupAttr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupAttr_REC";
/
DROP TYPE "RIB_SupSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupSite_REC";
/
DROP TYPE "RIB_SupSiteOrgUnit_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupSiteOrgUnit_REC";
/
DROP TYPE "RIB_SupSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupSiteAddr_REC";
/
DROP TYPE "RIB_Addr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Addr_REC";
/
DROP TYPE "RIB_SupSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SupSite_TBL" AS TABLE OF "RIB_SupSite_REC";
/
DROP TYPE "RIB_LocOfSupplierDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierDesc_TBL" AS TABLE OF "RIB_LocOfSupplierDesc_REC";
/
DROP TYPE "RIB_SupplierDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupplierDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  sup_xref_key varchar2(32),
  supplier_id number(10),
  SupAttr "RIB_SupAttr_REC",
  SupSite_TBL "RIB_SupSite_TBL",   -- Size of "RIB_SupSite_TBL" is unbounded
  ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC",
  LocOfSupplierDesc_TBL "RIB_LocOfSupplierDesc_TBL",   -- Size of "RIB_LocOfSupplierDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
) return self as result
,constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
, SupSite_TBL "RIB_SupSite_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
) return self as result
,constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
, SupSite_TBL "RIB_SupSite_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
) return self as result
,constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
, SupSite_TBL "RIB_SupSite_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
, LocOfSupplierDesc_TBL "RIB_LocOfSupplierDesc_TBL"  -- Size of "RIB_LocOfSupplierDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupplierDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_xref_key') := sup_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  l_new_pre :=i_prefix||'SupAttr.';
  SupAttr.appendNodeValues( i_prefix||'SupAttr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF SupSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SupSite_TBL.';
    FOR INDX IN SupSite_TBL.FIRST()..SupSite_TBL.LAST() LOOP
      SupSite_TBL(indx).appendNodeValues( i_prefix||indx||'SupSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ExtOfSupplierDesc.';
  ExtOfSupplierDesc.appendNodeValues( i_prefix||'ExtOfSupplierDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupplierDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupplierDesc_TBL.';
    FOR INDX IN LocOfSupplierDesc_TBL.FIRST()..LocOfSupplierDesc_TBL.LAST() LOOP
      LocOfSupplierDesc_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupplierDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupAttr := SupAttr;
RETURN;
end;
constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
, SupSite_TBL "RIB_SupSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupAttr := SupAttr;
self.SupSite_TBL := SupSite_TBL;
RETURN;
end;
constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
, SupSite_TBL "RIB_SupSite_TBL"
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupAttr := SupAttr;
self.SupSite_TBL := SupSite_TBL;
self.ExtOfSupplierDesc := ExtOfSupplierDesc;
RETURN;
end;
constructor function "RIB_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupAttr "RIB_SupAttr_REC"
, SupSite_TBL "RIB_SupSite_TBL"
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
, LocOfSupplierDesc_TBL "RIB_LocOfSupplierDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupAttr := SupAttr;
self.SupSite_TBL := SupSite_TBL;
self.ExtOfSupplierDesc := ExtOfSupplierDesc;
self.LocOfSupplierDesc_TBL := LocOfSupplierDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocOfSupAttr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupAttr_TBL" AS TABLE OF "RIB_LocOfSupAttr_REC";
/
DROP TYPE "RIB_SupAttr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupAttr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  sup_name varchar2(240),
  sup_name_secondary varchar2(240),
  contact_name varchar2(120),
  contact_phone varchar2(20),
  contact_fax varchar2(20),
  contact_pager varchar2(20),
  sup_status varchar2(1),
  qc_ind varchar2(1),
  qc_pct number(12,4),
  qc_freq varchar2(2),
  vc_ind varchar2(1),
  vc_pct number(12,4),
  vc_freq number(2),
  currency_code varchar2(3),
  lang number(6),
  terms varchar2(15),
  freight_terms varchar2(30),
  ret_allow_ind varchar2(1),
  ret_auth_req varchar2(1),
  ret_min_dol_amt number(20,4),
  ret_courier varchar2(250),
  handling_pct number(12,4),
  edi_po_ind varchar2(1),
  edi_po_chg varchar2(1),
  edi_po_confirm varchar2(1),
  edi_asn varchar2(1),
  edi_sales_rpt_freq varchar2(1),
  edi_supp_available_ind varchar2(1),
  edi_contract_ind varchar2(1),
  edi_invc_ind varchar2(1),
  edi_channel_ind number(4),
  cost_chg_pct_var number(12,4),
  cost_chg_amt_var number(20,4),
  replen_approval_ind varchar2(1),
  ship_method varchar2(6),
  payment_method varchar2(6),
  contact_telex varchar2(20),
  contact_email varchar2(100),
  settlement_code varchar2(1),
  pre_mark_ind varchar2(1),
  auto_appr_invc_ind varchar2(1),
  dbt_memo_code varchar2(1),
  freight_charge_ind varchar2(1),
  auto_appr_dbt_memo_ind varchar2(1),
  prepay_invc_ind varchar2(1),
  backorder_ind varchar2(1),
  vat_region number(4),
  inv_mgmt_lvl varchar2(6),
  service_perf_req_ind varchar2(1),
  invc_pay_loc varchar2(6),
  invc_receive_loc varchar2(6),
  addinvc_gross_net varchar2(6),
  delivery_policy varchar2(6),
  comment_desc varchar2(2000),
  default_item_lead_time number(4),
  duns_number varchar2(9),
  duns_loc varchar2(4),
  bracket_costing_ind varchar2(1),
  vmi_order_status varchar2(6),
  dsd_ind varchar2(1),
  scale_aip_orders varchar2(1),
  sup_qty_level varchar2(6),
  ExtOfSupAttr "RIB_ExtOfSupAttr_REC",
  LocOfSupAttr_TBL "RIB_LocOfSupAttr_TBL",   -- Size of "RIB_LocOfSupAttr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
, sup_qty_level varchar2
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
, sup_qty_level varchar2
, ExtOfSupAttr "RIB_ExtOfSupAttr_REC"
) return self as result
,constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
, sup_qty_level varchar2
, ExtOfSupAttr "RIB_ExtOfSupAttr_REC"
, LocOfSupAttr_TBL "RIB_LocOfSupAttr_TBL"  -- Size of "RIB_LocOfSupAttr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupAttr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_name') := sup_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_name_secondary') := sup_name_secondary;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_name') := contact_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_phone') := contact_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_fax') := contact_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_pager') := contact_pager;
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_status') := sup_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'qc_ind') := qc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'qc_pct') := qc_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'qc_freq') := qc_freq;
  rib_obj_util.g_RIB_element_values(i_prefix||'vc_ind') := vc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'vc_pct') := vc_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'vc_freq') := vc_freq;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms') := terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_terms') := freight_terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_allow_ind') := ret_allow_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_auth_req') := ret_auth_req;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_min_dol_amt') := ret_min_dol_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_courier') := ret_courier;
  rib_obj_util.g_RIB_element_values(i_prefix||'handling_pct') := handling_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_po_ind') := edi_po_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_po_chg') := edi_po_chg;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_po_confirm') := edi_po_confirm;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_asn') := edi_asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_sales_rpt_freq') := edi_sales_rpt_freq;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_supp_available_ind') := edi_supp_available_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_contract_ind') := edi_contract_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_invc_ind') := edi_invc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'edi_channel_ind') := edi_channel_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_chg_pct_var') := cost_chg_pct_var;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_chg_amt_var') := cost_chg_amt_var;
  rib_obj_util.g_RIB_element_values(i_prefix||'replen_approval_ind') := replen_approval_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_method') := ship_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_method') := payment_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_telex') := contact_telex;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_email') := contact_email;
  rib_obj_util.g_RIB_element_values(i_prefix||'settlement_code') := settlement_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'pre_mark_ind') := pre_mark_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_appr_invc_ind') := auto_appr_invc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'dbt_memo_code') := dbt_memo_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_charge_ind') := freight_charge_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_appr_dbt_memo_ind') := auto_appr_dbt_memo_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'prepay_invc_ind') := prepay_invc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'backorder_ind') := backorder_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_region') := vat_region;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_mgmt_lvl') := inv_mgmt_lvl;
  rib_obj_util.g_RIB_element_values(i_prefix||'service_perf_req_ind') := service_perf_req_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_pay_loc') := invc_pay_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_receive_loc') := invc_receive_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'addinvc_gross_net') := addinvc_gross_net;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_policy') := delivery_policy;
  rib_obj_util.g_RIB_element_values(i_prefix||'comment_desc') := comment_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_item_lead_time') := default_item_lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_number') := duns_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_loc') := duns_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'bracket_costing_ind') := bracket_costing_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'vmi_order_status') := vmi_order_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'dsd_ind') := dsd_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'scale_aip_orders') := scale_aip_orders;
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_qty_level') := sup_qty_level;
  l_new_pre :=i_prefix||'ExtOfSupAttr.';
  ExtOfSupAttr.appendNodeValues( i_prefix||'ExtOfSupAttr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupAttr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupAttr_TBL.';
    FOR INDX IN LocOfSupAttr_TBL.FIRST()..LocOfSupAttr_TBL.LAST() LOOP
      LocOfSupAttr_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupAttr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
self.vmi_order_status := vmi_order_status;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
self.vmi_order_status := vmi_order_status;
self.dsd_ind := dsd_ind;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
self.vmi_order_status := vmi_order_status;
self.dsd_ind := dsd_ind;
self.scale_aip_orders := scale_aip_orders;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
, sup_qty_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
self.vmi_order_status := vmi_order_status;
self.dsd_ind := dsd_ind;
self.scale_aip_orders := scale_aip_orders;
self.sup_qty_level := sup_qty_level;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
, sup_qty_level varchar2
, ExtOfSupAttr "RIB_ExtOfSupAttr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
self.vmi_order_status := vmi_order_status;
self.dsd_ind := dsd_ind;
self.scale_aip_orders := scale_aip_orders;
self.sup_qty_level := sup_qty_level;
self.ExtOfSupAttr := ExtOfSupAttr;
RETURN;
end;
constructor function "RIB_SupAttr_REC"
(
  rib_oid number
, sup_name varchar2
, sup_name_secondary varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_pager varchar2
, sup_status varchar2
, qc_ind varchar2
, qc_pct number
, qc_freq varchar2
, vc_ind varchar2
, vc_pct number
, vc_freq number
, currency_code varchar2
, lang number
, terms varchar2
, freight_terms varchar2
, ret_allow_ind varchar2
, ret_auth_req varchar2
, ret_min_dol_amt number
, ret_courier varchar2
, handling_pct number
, edi_po_ind varchar2
, edi_po_chg varchar2
, edi_po_confirm varchar2
, edi_asn varchar2
, edi_sales_rpt_freq varchar2
, edi_supp_available_ind varchar2
, edi_contract_ind varchar2
, edi_invc_ind varchar2
, edi_channel_ind number
, cost_chg_pct_var number
, cost_chg_amt_var number
, replen_approval_ind varchar2
, ship_method varchar2
, payment_method varchar2
, contact_telex varchar2
, contact_email varchar2
, settlement_code varchar2
, pre_mark_ind varchar2
, auto_appr_invc_ind varchar2
, dbt_memo_code varchar2
, freight_charge_ind varchar2
, auto_appr_dbt_memo_ind varchar2
, prepay_invc_ind varchar2
, backorder_ind varchar2
, vat_region number
, inv_mgmt_lvl varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, addinvc_gross_net varchar2
, delivery_policy varchar2
, comment_desc varchar2
, default_item_lead_time number
, duns_number varchar2
, duns_loc varchar2
, bracket_costing_ind varchar2
, vmi_order_status varchar2
, dsd_ind varchar2
, scale_aip_orders varchar2
, sup_qty_level varchar2
, ExtOfSupAttr "RIB_ExtOfSupAttr_REC"
, LocOfSupAttr_TBL "RIB_LocOfSupAttr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_name := sup_name;
self.sup_name_secondary := sup_name_secondary;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_pager := contact_pager;
self.sup_status := sup_status;
self.qc_ind := qc_ind;
self.qc_pct := qc_pct;
self.qc_freq := qc_freq;
self.vc_ind := vc_ind;
self.vc_pct := vc_pct;
self.vc_freq := vc_freq;
self.currency_code := currency_code;
self.lang := lang;
self.terms := terms;
self.freight_terms := freight_terms;
self.ret_allow_ind := ret_allow_ind;
self.ret_auth_req := ret_auth_req;
self.ret_min_dol_amt := ret_min_dol_amt;
self.ret_courier := ret_courier;
self.handling_pct := handling_pct;
self.edi_po_ind := edi_po_ind;
self.edi_po_chg := edi_po_chg;
self.edi_po_confirm := edi_po_confirm;
self.edi_asn := edi_asn;
self.edi_sales_rpt_freq := edi_sales_rpt_freq;
self.edi_supp_available_ind := edi_supp_available_ind;
self.edi_contract_ind := edi_contract_ind;
self.edi_invc_ind := edi_invc_ind;
self.edi_channel_ind := edi_channel_ind;
self.cost_chg_pct_var := cost_chg_pct_var;
self.cost_chg_amt_var := cost_chg_amt_var;
self.replen_approval_ind := replen_approval_ind;
self.ship_method := ship_method;
self.payment_method := payment_method;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.settlement_code := settlement_code;
self.pre_mark_ind := pre_mark_ind;
self.auto_appr_invc_ind := auto_appr_invc_ind;
self.dbt_memo_code := dbt_memo_code;
self.freight_charge_ind := freight_charge_ind;
self.auto_appr_dbt_memo_ind := auto_appr_dbt_memo_ind;
self.prepay_invc_ind := prepay_invc_ind;
self.backorder_ind := backorder_ind;
self.vat_region := vat_region;
self.inv_mgmt_lvl := inv_mgmt_lvl;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.addinvc_gross_net := addinvc_gross_net;
self.delivery_policy := delivery_policy;
self.comment_desc := comment_desc;
self.default_item_lead_time := default_item_lead_time;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.bracket_costing_ind := bracket_costing_ind;
self.vmi_order_status := vmi_order_status;
self.dsd_ind := dsd_ind;
self.scale_aip_orders := scale_aip_orders;
self.sup_qty_level := sup_qty_level;
self.ExtOfSupAttr := ExtOfSupAttr;
self.LocOfSupAttr_TBL := LocOfSupAttr_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_SupSiteOrgUnit_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SupSiteOrgUnit_TBL" AS TABLE OF "RIB_SupSiteOrgUnit_REC";
/
DROP TYPE "RIB_SupSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SupSiteAddr_TBL" AS TABLE OF "RIB_SupSiteAddr_REC";
/
DROP TYPE "RIB_LocOfSupSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupSite_TBL" AS TABLE OF "RIB_LocOfSupSite_REC";
/
DROP TYPE "RIB_SupSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supsite_xref_key varchar2(32),
  supplier_site_id number(10),
  SupAttr "RIB_SupAttr_REC",
  SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL",   -- Size of "RIB_SupSiteOrgUnit_TBL" is unbounded
  SupSiteAddr_TBL "RIB_SupSiteAddr_TBL",   -- Size of "RIB_SupSiteAddr_TBL" is unbounded
  ExtOfSupSite "RIB_ExtOfSupSite_REC",
  LocOfSupSite_TBL "RIB_LocOfSupSite_TBL",   -- Size of "RIB_LocOfSupSite_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupAttr "RIB_SupAttr_REC"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"  -- Size of "RIB_SupSiteOrgUnit_TBL" is unbounded
, SupSiteAddr_TBL "RIB_SupSiteAddr_TBL"  -- Size of "RIB_SupSiteAddr_TBL" is unbounded
) return self as result
,constructor function "RIB_SupSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupAttr "RIB_SupAttr_REC"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"  -- Size of "RIB_SupSiteOrgUnit_TBL" is unbounded
, SupSiteAddr_TBL "RIB_SupSiteAddr_TBL"  -- Size of "RIB_SupSiteAddr_TBL" is unbounded
, ExtOfSupSite "RIB_ExtOfSupSite_REC"
) return self as result
,constructor function "RIB_SupSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupAttr "RIB_SupAttr_REC"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"  -- Size of "RIB_SupSiteOrgUnit_TBL" is unbounded
, SupSiteAddr_TBL "RIB_SupSiteAddr_TBL"  -- Size of "RIB_SupSiteAddr_TBL" is unbounded
, ExtOfSupSite "RIB_ExtOfSupSite_REC"
, LocOfSupSite_TBL "RIB_LocOfSupSite_TBL"  -- Size of "RIB_LocOfSupSite_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supsite_xref_key') := supsite_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_site_id') := supplier_site_id;
  l_new_pre :=i_prefix||'SupAttr.';
  SupAttr.appendNodeValues( i_prefix||'SupAttr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'SupSiteOrgUnit_TBL.';
  FOR INDX IN SupSiteOrgUnit_TBL.FIRST()..SupSiteOrgUnit_TBL.LAST() LOOP
    SupSiteOrgUnit_TBL(indx).appendNodeValues( i_prefix||indx||'SupSiteOrgUnit_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'SupSiteAddr_TBL.';
  FOR INDX IN SupSiteAddr_TBL.FIRST()..SupSiteAddr_TBL.LAST() LOOP
    SupSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'SupSiteAddr_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'ExtOfSupSite.';
  ExtOfSupSite.appendNodeValues( i_prefix||'ExtOfSupSite');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupSite_TBL.';
    FOR INDX IN LocOfSupSite_TBL.FIRST()..LocOfSupSite_TBL.LAST() LOOP
      LocOfSupSite_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SupSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupAttr "RIB_SupAttr_REC"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupSiteAddr_TBL "RIB_SupSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supsite_xref_key := supsite_xref_key;
self.supplier_site_id := supplier_site_id;
self.SupAttr := SupAttr;
self.SupSiteOrgUnit_TBL := SupSiteOrgUnit_TBL;
self.SupSiteAddr_TBL := SupSiteAddr_TBL;
RETURN;
end;
constructor function "RIB_SupSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupAttr "RIB_SupAttr_REC"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupSiteAddr_TBL "RIB_SupSiteAddr_TBL"
, ExtOfSupSite "RIB_ExtOfSupSite_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supsite_xref_key := supsite_xref_key;
self.supplier_site_id := supplier_site_id;
self.SupAttr := SupAttr;
self.SupSiteOrgUnit_TBL := SupSiteOrgUnit_TBL;
self.SupSiteAddr_TBL := SupSiteAddr_TBL;
self.ExtOfSupSite := ExtOfSupSite;
RETURN;
end;
constructor function "RIB_SupSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupAttr "RIB_SupAttr_REC"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupSiteAddr_TBL "RIB_SupSiteAddr_TBL"
, ExtOfSupSite "RIB_ExtOfSupSite_REC"
, LocOfSupSite_TBL "RIB_LocOfSupSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supsite_xref_key := supsite_xref_key;
self.supplier_site_id := supplier_site_id;
self.SupAttr := SupAttr;
self.SupSiteOrgUnit_TBL := SupSiteOrgUnit_TBL;
self.SupSiteAddr_TBL := SupSiteAddr_TBL;
self.ExtOfSupSite := ExtOfSupSite;
self.LocOfSupSite_TBL := LocOfSupSite_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocOfSupSiteOrgUnit_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupSiteOrgUnit_TBL" AS TABLE OF "RIB_LocOfSupSiteOrgUnit_REC";
/
DROP TYPE "RIB_SupSiteOrgUnit_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupSiteOrgUnit_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  org_unit_id number(15),
  primary_pay_site varchar2(1),
  ExtOfSupSiteOrgUnit "RIB_ExtOfSupSiteOrgUnit_REC",
  LocOfSupSiteOrgUnit_TBL "RIB_LocOfSupSiteOrgUnit_TBL",   -- Size of "RIB_LocOfSupSiteOrgUnit_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
) return self as result
,constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
, primary_pay_site varchar2
) return self as result
,constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
, primary_pay_site varchar2
, ExtOfSupSiteOrgUnit "RIB_ExtOfSupSiteOrgUnit_REC"
) return self as result
,constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
, primary_pay_site varchar2
, ExtOfSupSiteOrgUnit "RIB_ExtOfSupSiteOrgUnit_REC"
, LocOfSupSiteOrgUnit_TBL "RIB_LocOfSupSiteOrgUnit_TBL"  -- Size of "RIB_LocOfSupSiteOrgUnit_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupSiteOrgUnit_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'org_unit_id') := org_unit_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_pay_site') := primary_pay_site;
  l_new_pre :=i_prefix||'ExtOfSupSiteOrgUnit.';
  ExtOfSupSiteOrgUnit.appendNodeValues( i_prefix||'ExtOfSupSiteOrgUnit');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupSiteOrgUnit_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupSiteOrgUnit_TBL.';
    FOR INDX IN LocOfSupSiteOrgUnit_TBL.FIRST()..LocOfSupSiteOrgUnit_TBL.LAST() LOOP
      LocOfSupSiteOrgUnit_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupSiteOrgUnit_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.org_unit_id := org_unit_id;
RETURN;
end;
constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
, primary_pay_site varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.org_unit_id := org_unit_id;
self.primary_pay_site := primary_pay_site;
RETURN;
end;
constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
, primary_pay_site varchar2
, ExtOfSupSiteOrgUnit "RIB_ExtOfSupSiteOrgUnit_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.org_unit_id := org_unit_id;
self.primary_pay_site := primary_pay_site;
self.ExtOfSupSiteOrgUnit := ExtOfSupSiteOrgUnit;
RETURN;
end;
constructor function "RIB_SupSiteOrgUnit_REC"
(
  rib_oid number
, org_unit_id number
, primary_pay_site varchar2
, ExtOfSupSiteOrgUnit "RIB_ExtOfSupSiteOrgUnit_REC"
, LocOfSupSiteOrgUnit_TBL "RIB_LocOfSupSiteOrgUnit_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.org_unit_id := org_unit_id;
self.primary_pay_site := primary_pay_site;
self.ExtOfSupSiteOrgUnit := ExtOfSupSiteOrgUnit;
self.LocOfSupSiteOrgUnit_TBL := LocOfSupSiteOrgUnit_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocOfSupSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupSiteAddr_TBL" AS TABLE OF "RIB_LocOfSupSiteAddr_REC";
/
DROP TYPE "RIB_SupSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  addr_xref_key varchar2(32),
  addr_key number(11),
  Addr "RIB_Addr_REC",
  ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC",
  LocOfSupSiteAddr_TBL "RIB_LocOfSupSiteAddr_TBL",   -- Size of "RIB_LocOfSupSiteAddr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
) return self as result
,constructor function "RIB_SupSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
) return self as result
,constructor function "RIB_SupSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
, LocOfSupSiteAddr_TBL "RIB_LocOfSupSiteAddr_TBL"  -- Size of "RIB_LocOfSupSiteAddr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_xref_key') := addr_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_key') := addr_key;
  l_new_pre :=i_prefix||'Addr.';
  Addr.appendNodeValues( i_prefix||'Addr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ExtOfSupSiteAddr.';
  ExtOfSupSiteAddr.appendNodeValues( i_prefix||'ExtOfSupSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupSiteAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupSiteAddr_TBL.';
    FOR INDX IN LocOfSupSiteAddr_TBL.FIRST()..LocOfSupSiteAddr_TBL.LAST() LOOP
      LocOfSupSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupSiteAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SupSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.Addr := Addr;
RETURN;
end;
constructor function "RIB_SupSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.Addr := Addr;
self.ExtOfSupSiteAddr := ExtOfSupSiteAddr;
RETURN;
end;
constructor function "RIB_SupSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
, LocOfSupSiteAddr_TBL "RIB_LocOfSupSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.Addr := Addr;
self.ExtOfSupSiteAddr := ExtOfSupSiteAddr;
self.LocOfSupSiteAddr_TBL := LocOfSupSiteAddr_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocOfAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfAddr_TBL" AS TABLE OF "RIB_LocOfAddr_REC";
/
DROP TYPE "RIB_Addr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Addr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  addr_type varchar2(2),
  primary_addr_ind varchar2(1),
  add_1 varchar2(240),
  add_2 varchar2(240),
  add_3 varchar2(240),
  city varchar2(120),
  state varchar2(3),
  country_id varchar2(3),
  post varchar2(30),
  contact_name varchar2(120),
  contact_phone varchar2(20),
  contact_fax varchar2(20),
  contact_email varchar2(100),
  jurisdiction_code varchar2(10),
  ExtOfAddr "RIB_ExtOfAddr_REC",
  LocOfAddr_TBL "RIB_LocOfAddr_TBL",   -- Size of "RIB_LocOfAddr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
, jurisdiction_code varchar2
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
, jurisdiction_code varchar2
, ExtOfAddr "RIB_ExtOfAddr_REC"
) return self as result
,constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
, jurisdiction_code varchar2
, ExtOfAddr "RIB_ExtOfAddr_REC"
, LocOfAddr_TBL "RIB_LocOfAddr_TBL"  -- Size of "RIB_LocOfAddr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Addr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_type') := addr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_addr_ind') := primary_addr_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_1') := add_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_2') := add_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_3') := add_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'post') := post;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_name') := contact_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_phone') := contact_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_fax') := contact_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_email') := contact_email;
  rib_obj_util.g_RIB_element_values(i_prefix||'jurisdiction_code') := jurisdiction_code;
  l_new_pre :=i_prefix||'ExtOfAddr.';
  ExtOfAddr.appendNodeValues( i_prefix||'ExtOfAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfAddr_TBL.';
    FOR INDX IN LocOfAddr_TBL.FIRST()..LocOfAddr_TBL.LAST() LOOP
      LocOfAddr_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
, jurisdiction_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
self.jurisdiction_code := jurisdiction_code;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
, jurisdiction_code varchar2
, ExtOfAddr "RIB_ExtOfAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
self.jurisdiction_code := jurisdiction_code;
self.ExtOfAddr := ExtOfAddr;
RETURN;
end;
constructor function "RIB_Addr_REC"
(
  rib_oid number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_email varchar2
, jurisdiction_code varchar2
, ExtOfAddr "RIB_ExtOfAddr_REC"
, LocOfAddr_TBL "RIB_LocOfAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
self.jurisdiction_code := jurisdiction_code;
self.ExtOfAddr := ExtOfAddr;
self.LocOfAddr_TBL := LocOfAddr_TBL;
RETURN;
end;
END;
/
