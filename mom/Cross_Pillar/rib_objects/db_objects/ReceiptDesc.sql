DROP TYPE "RIB_ReceiptOverageUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptOverageUin_REC";
/
DROP TYPE "RIB_ReceiptOverageDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptOverageDtl_REC";
/
DROP TYPE "RIB_ReceiptOverage_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptOverage_REC";
/
DROP TYPE "RIB_ReceiptCartonDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptCartonDtl_REC";
/
DROP TYPE "RIB_ReceiptDtlUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptDtlUin_REC";
/
DROP TYPE "RIB_ReceiptDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptDtl_REC";
/
DROP TYPE "RIB_Receipt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Receipt_REC";
/
DROP TYPE "RIB_ReceiptDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptDesc_REC";
/
DROP TYPE "RIB_ReceiptOverageUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptOverageUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  status number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptOverageUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptOverageUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
END appendNodeValues;
constructor function "RIB_ReceiptOverageUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.status := status;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptOverageUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptOverageUin_TBL" AS TABLE OF "RIB_ReceiptOverageUin_REC";
/
DROP TYPE "RIB_ReceiptOverageDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptOverageDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  qty_received number(12,4),
  reason_code number(4),
  ReceiptOverageUin_TBL "RIB_ReceiptOverageUin_TBL",   -- Size of "RIB_ReceiptOverageUin_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
) return self as result
,constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
, qty_received number
) return self as result
,constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
, qty_received number
, reason_code number
) return self as result
,constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
, qty_received number
, reason_code number
, ReceiptOverageUin_TBL "RIB_ReceiptOverageUin_TBL"  -- Size of "RIB_ReceiptOverageUin_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptOverageDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_received') := qty_received;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_code') := reason_code;
  IF ReceiptOverageUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ReceiptOverageUin_TBL.';
    FOR INDX IN ReceiptOverageUin_TBL.FIRST()..ReceiptOverageUin_TBL.LAST() LOOP
      ReceiptOverageUin_TBL(indx).appendNodeValues( i_prefix||indx||'ReceiptOverageUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
, qty_received number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.qty_received := qty_received;
RETURN;
end;
constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
, qty_received number
, reason_code number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.qty_received := qty_received;
self.reason_code := reason_code;
RETURN;
end;
constructor function "RIB_ReceiptOverageDtl_REC"
(
  rib_oid number
, item_id varchar2
, qty_received number
, reason_code number
, ReceiptOverageUin_TBL "RIB_ReceiptOverageUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.qty_received := qty_received;
self.reason_code := reason_code;
self.ReceiptOverageUin_TBL := ReceiptOverageUin_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptOverageDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptOverageDtl_TBL" AS TABLE OF "RIB_ReceiptOverageDtl_REC";
/
DROP TYPE "RIB_ReceiptOverage_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptOverage_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  po_nbr varchar2(12),
  document_type varchar2(1),
  asn_nbr varchar2(30),
  ReceiptOverageDtl_TBL "RIB_ReceiptOverageDtl_TBL",   -- Size of "RIB_ReceiptOverageDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
) return self as result
,constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
, document_type varchar2
) return self as result
,constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
, document_type varchar2
, asn_nbr varchar2
) return self as result
,constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
, document_type varchar2
, asn_nbr varchar2
, ReceiptOverageDtl_TBL "RIB_ReceiptOverageDtl_TBL"  -- Size of "RIB_ReceiptOverageDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptOverage_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
  IF ReceiptOverageDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ReceiptOverageDtl_TBL.';
    FOR INDX IN ReceiptOverageDtl_TBL.FIRST()..ReceiptOverageDtl_TBL.LAST() LOOP
      ReceiptOverageDtl_TBL(indx).appendNodeValues( i_prefix||indx||'ReceiptOverageDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
RETURN;
end;
constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
, document_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.document_type := document_type;
RETURN;
end;
constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
, document_type varchar2
, asn_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.document_type := document_type;
self.asn_nbr := asn_nbr;
RETURN;
end;
constructor function "RIB_ReceiptOverage_REC"
(
  rib_oid number
, po_nbr varchar2
, document_type varchar2
, asn_nbr varchar2
, ReceiptOverageDtl_TBL "RIB_ReceiptOverageDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.document_type := document_type;
self.asn_nbr := asn_nbr;
self.ReceiptOverageDtl_TBL := ReceiptOverageDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptCartonDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptCartonDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  carton_status_ind varchar2(1),
  container_id varchar2(20),
  dest_id varchar2(10),
  receipt_xactn_type varchar2(1),
  receipt_date date,
  receipt_nbr varchar2(17),
  user_id varchar2(30),
  to_disposition varchar2(4),
  weight number(12,4),
  weight_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
) return self as result
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
) return self as result
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
) return self as result
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
, to_disposition varchar2
) return self as result
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
, to_disposition varchar2
, weight number
) return self as result
,constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
, to_disposition varchar2
, weight number
, weight_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptCartonDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_status_ind') := carton_status_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_xactn_type') := receipt_xactn_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_date') := receipt_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_nbr') := receipt_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
END appendNodeValues;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
RETURN;
end;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
RETURN;
end;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
RETURN;
end;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
, to_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.user_id := user_id;
self.to_disposition := to_disposition;
RETURN;
end;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
, to_disposition varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.user_id := user_id;
self.to_disposition := to_disposition;
self.weight := weight;
RETURN;
end;
constructor function "RIB_ReceiptCartonDtl_REC"
(
  rib_oid number
, carton_status_ind varchar2
, container_id varchar2
, dest_id varchar2
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, user_id varchar2
, to_disposition varchar2
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_status_ind := carton_status_ind;
self.container_id := container_id;
self.dest_id := dest_id;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.user_id := user_id;
self.to_disposition := to_disposition;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptDtlUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptDtlUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  status number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptDtlUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptDtlUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
END appendNodeValues;
constructor function "RIB_ReceiptDtlUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.status := status;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptDtlUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptDtlUin_TBL" AS TABLE OF "RIB_ReceiptDtlUin_REC";
/
DROP TYPE "RIB_ReceiptDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  unit_qty number(12,4),
  receipt_xactn_type varchar2(1),
  receipt_date date,
  receipt_nbr varchar2(17),
  dest_id varchar2(10),
  container_id varchar2(20),
  distro_nbr varchar2(12),
  distro_doc_type varchar2(1),
  to_disposition varchar2(4),
  from_disposition varchar2(4),
  to_wip varchar2(9),
  from_wip varchar2(9),
  to_trouble varchar2(9),
  from_trouble varchar2(9),
  user_id varchar2(30),
  dummy_carton_ind varchar2(1),
  tampered_carton_ind varchar2(1),
  unit_cost number(20,4),
  shipped_qty number(12,4),
  weight number(12,4),
  weight_uom varchar2(4),
  gross_cost number(20,4),
  ReceiptDtlUin_TBL "RIB_ReceiptDtlUin_TBL",   -- Size of "RIB_ReceiptDtlUin_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
, weight_uom varchar2
, gross_cost number
) return self as result
,constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
, weight_uom varchar2
, gross_cost number
, ReceiptDtlUin_TBL "RIB_ReceiptDtlUin_TBL"  -- Size of "RIB_ReceiptDtlUin_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_xactn_type') := receipt_xactn_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_date') := receipt_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_nbr') := receipt_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_doc_type') := distro_doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_disposition') := from_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_wip') := to_wip;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_wip') := from_wip;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_trouble') := to_trouble;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_trouble') := from_trouble;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dummy_carton_ind') := dummy_carton_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'tampered_carton_ind') := tampered_carton_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipped_qty') := shipped_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'gross_cost') := gross_cost;
  IF ReceiptDtlUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ReceiptDtlUin_TBL.';
    FOR INDX IN ReceiptDtlUin_TBL.FIRST()..ReceiptDtlUin_TBL.LAST() LOOP
      ReceiptDtlUin_TBL(indx).appendNodeValues( i_prefix||indx||'ReceiptDtlUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
self.unit_cost := unit_cost;
self.shipped_qty := shipped_qty;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
self.unit_cost := unit_cost;
self.shipped_qty := shipped_qty;
self.weight := weight;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
self.unit_cost := unit_cost;
self.shipped_qty := shipped_qty;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
, weight_uom varchar2
, gross_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
self.unit_cost := unit_cost;
self.shipped_qty := shipped_qty;
self.weight := weight;
self.weight_uom := weight_uom;
self.gross_cost := gross_cost;
RETURN;
end;
constructor function "RIB_ReceiptDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, receipt_xactn_type varchar2
, receipt_date date
, receipt_nbr varchar2
, dest_id varchar2
, container_id varchar2
, distro_nbr varchar2
, distro_doc_type varchar2
, to_disposition varchar2
, from_disposition varchar2
, to_wip varchar2
, from_wip varchar2
, to_trouble varchar2
, from_trouble varchar2
, user_id varchar2
, dummy_carton_ind varchar2
, tampered_carton_ind varchar2
, unit_cost number
, shipped_qty number
, weight number
, weight_uom varchar2
, gross_cost number
, ReceiptDtlUin_TBL "RIB_ReceiptDtlUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.receipt_xactn_type := receipt_xactn_type;
self.receipt_date := receipt_date;
self.receipt_nbr := receipt_nbr;
self.dest_id := dest_id;
self.container_id := container_id;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.to_disposition := to_disposition;
self.from_disposition := from_disposition;
self.to_wip := to_wip;
self.from_wip := from_wip;
self.to_trouble := to_trouble;
self.from_trouble := from_trouble;
self.user_id := user_id;
self.dummy_carton_ind := dummy_carton_ind;
self.tampered_carton_ind := tampered_carton_ind;
self.unit_cost := unit_cost;
self.shipped_qty := shipped_qty;
self.weight := weight;
self.weight_uom := weight_uom;
self.gross_cost := gross_cost;
self.ReceiptDtlUin_TBL := ReceiptDtlUin_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ReceiptDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptDtl_TBL" AS TABLE OF "RIB_ReceiptDtl_REC";
/
DROP TYPE "RIB_ReceiptCartonDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptCartonDtl_TBL" AS TABLE OF "RIB_ReceiptCartonDtl_REC";
/
DROP TYPE "RIB_Receipt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Receipt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dc_dest_id varchar2(10),
  po_nbr varchar2(12),
  cust_order_nbr varchar2(48),
  fulfill_order_nbr varchar2(48),
  document_type varchar2(1),
  ref_doc_no number(10),
  asn_nbr varchar2(30),
  ReceiptDtl_TBL "RIB_ReceiptDtl_TBL",   -- Size of "RIB_ReceiptDtl_TBL" is unbounded
  ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL",   -- Size of "RIB_ReceiptCartonDtl_TBL" is unbounded
  receipt_type varchar2(2),
  from_loc varchar2(10),
  from_loc_type varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"  -- Size of "RIB_ReceiptDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"  -- Size of "RIB_ReceiptDtl_TBL" is unbounded
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"  -- Size of "RIB_ReceiptCartonDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"  -- Size of "RIB_ReceiptDtl_TBL" is unbounded
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"  -- Size of "RIB_ReceiptCartonDtl_TBL" is unbounded
, receipt_type varchar2
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"  -- Size of "RIB_ReceiptDtl_TBL" is unbounded
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"  -- Size of "RIB_ReceiptCartonDtl_TBL" is unbounded
, receipt_type varchar2
, from_loc varchar2
) return self as result
,constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"  -- Size of "RIB_ReceiptDtl_TBL" is unbounded
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"  -- Size of "RIB_ReceiptCartonDtl_TBL" is unbounded
, receipt_type varchar2
, from_loc varchar2
, from_loc_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Receipt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_nbr') := fulfill_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_doc_no') := ref_doc_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
  IF ReceiptDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ReceiptDtl_TBL.';
    FOR INDX IN ReceiptDtl_TBL.FIRST()..ReceiptDtl_TBL.LAST() LOOP
      ReceiptDtl_TBL(indx).appendNodeValues( i_prefix||indx||'ReceiptDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ReceiptCartonDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ReceiptCartonDtl_TBL.';
    FOR INDX IN ReceiptCartonDtl_TBL.FIRST()..ReceiptCartonDtl_TBL.LAST() LOOP
      ReceiptCartonDtl_TBL(indx).appendNodeValues( i_prefix||indx||'ReceiptCartonDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_type') := receipt_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc') := from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc_type') := from_loc_type;
END appendNodeValues;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
self.asn_nbr := asn_nbr;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
self.asn_nbr := asn_nbr;
self.ReceiptDtl_TBL := ReceiptDtl_TBL;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
self.asn_nbr := asn_nbr;
self.ReceiptDtl_TBL := ReceiptDtl_TBL;
self.ReceiptCartonDtl_TBL := ReceiptCartonDtl_TBL;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"
, receipt_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
self.asn_nbr := asn_nbr;
self.ReceiptDtl_TBL := ReceiptDtl_TBL;
self.ReceiptCartonDtl_TBL := ReceiptCartonDtl_TBL;
self.receipt_type := receipt_type;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"
, receipt_type varchar2
, from_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
self.asn_nbr := asn_nbr;
self.ReceiptDtl_TBL := ReceiptDtl_TBL;
self.ReceiptCartonDtl_TBL := ReceiptCartonDtl_TBL;
self.receipt_type := receipt_type;
self.from_loc := from_loc;
RETURN;
end;
constructor function "RIB_Receipt_REC"
(
  rib_oid number
, dc_dest_id varchar2
, po_nbr varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, document_type varchar2
, ref_doc_no number
, asn_nbr varchar2
, ReceiptDtl_TBL "RIB_ReceiptDtl_TBL"
, ReceiptCartonDtl_TBL "RIB_ReceiptCartonDtl_TBL"
, receipt_type varchar2
, from_loc varchar2
, from_loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.po_nbr := po_nbr;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.document_type := document_type;
self.ref_doc_no := ref_doc_no;
self.asn_nbr := asn_nbr;
self.ReceiptDtl_TBL := ReceiptDtl_TBL;
self.ReceiptCartonDtl_TBL := ReceiptCartonDtl_TBL;
self.receipt_type := receipt_type;
self.from_loc := from_loc;
self.from_loc_type := from_loc_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_Receipt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_Receipt_TBL" AS TABLE OF "RIB_Receipt_REC";
/
DROP TYPE "RIB_ReceiptOverage_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ReceiptOverage_TBL" AS TABLE OF "RIB_ReceiptOverage_REC";
/
DROP TYPE "RIB_ReceiptDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReceiptDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  schedule_nbr number(8),
  appt_nbr number(9),
  Receipt_TBL "RIB_Receipt_TBL",   -- Size of "RIB_Receipt_TBL" is unbounded
  ReceiptOverage_TBL "RIB_ReceiptOverage_TBL",   -- Size of "RIB_ReceiptOverage_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReceiptDesc_REC"
(
  rib_oid number
, schedule_nbr number
, appt_nbr number
, Receipt_TBL "RIB_Receipt_TBL"  -- Size of "RIB_Receipt_TBL" is unbounded
) return self as result
,constructor function "RIB_ReceiptDesc_REC"
(
  rib_oid number
, schedule_nbr number
, appt_nbr number
, Receipt_TBL "RIB_Receipt_TBL"  -- Size of "RIB_Receipt_TBL" is unbounded
, ReceiptOverage_TBL "RIB_ReceiptOverage_TBL"  -- Size of "RIB_ReceiptOverage_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReceiptDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReceiptDesc') := "ns_name_ReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_nbr') := schedule_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'appt_nbr') := appt_nbr;
  l_new_pre :=i_prefix||'Receipt_TBL.';
  FOR INDX IN Receipt_TBL.FIRST()..Receipt_TBL.LAST() LOOP
    Receipt_TBL(indx).appendNodeValues( i_prefix||indx||'Receipt_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  IF ReceiptOverage_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ReceiptOverage_TBL.';
    FOR INDX IN ReceiptOverage_TBL.FIRST()..ReceiptOverage_TBL.LAST() LOOP
      ReceiptOverage_TBL(indx).appendNodeValues( i_prefix||indx||'ReceiptOverage_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ReceiptDesc_REC"
(
  rib_oid number
, schedule_nbr number
, appt_nbr number
, Receipt_TBL "RIB_Receipt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.appt_nbr := appt_nbr;
self.Receipt_TBL := Receipt_TBL;
RETURN;
end;
constructor function "RIB_ReceiptDesc_REC"
(
  rib_oid number
, schedule_nbr number
, appt_nbr number
, Receipt_TBL "RIB_Receipt_TBL"
, ReceiptOverage_TBL "RIB_ReceiptOverage_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.appt_nbr := appt_nbr;
self.Receipt_TBL := Receipt_TBL;
self.ReceiptOverage_TBL := ReceiptOverage_TBL;
RETURN;
end;
END;
/
