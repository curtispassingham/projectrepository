DROP TYPE "RIB_StkCntHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntHdrCriVo_REC";
/
DROP TYPE "RIB_StkCntHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCntHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCntHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  schedule_date date,
  count_phase varchar2(20), -- count_phase is enumeration field, valid values are [FUTURE, COUNT, RECOUNT, AUTHORIZE, UNKNOWN] (all lower-case)
  display_status varchar2(20), -- display_status is enumeration field, valid values are [NONE, NEW, ACTIVE, IN_PROGRESS, PENDING, PROCESSING, AUTHORIZED, COMPLETED, CANCELED, VIEW_ONLY] (all lower-case)
  department_id number(12),
  class_id number(12),
  subclass_id number(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
) return self as result
,constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
, department_id number
) return self as result
,constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
, department_id number
, class_id number
) return self as result
,constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
, department_id number
, class_id number
, subclass_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCntHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCntHdrCriVo') := "ns_name_StkCntHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_date') := schedule_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_phase') := count_phase;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_status') := display_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
END appendNodeValues;
constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.schedule_date := schedule_date;
self.count_phase := count_phase;
self.display_status := display_status;
RETURN;
end;
constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
, department_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.schedule_date := schedule_date;
self.count_phase := count_phase;
self.display_status := display_status;
self.department_id := department_id;
RETURN;
end;
constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
, department_id number
, class_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.schedule_date := schedule_date;
self.count_phase := count_phase;
self.display_status := display_status;
self.department_id := department_id;
self.class_id := class_id;
RETURN;
end;
constructor function "RIB_StkCntHdrCriVo_REC"
(
  rib_oid number
, store_id number
, schedule_date date
, count_phase varchar2
, display_status varchar2
, department_id number
, class_id number
, subclass_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.schedule_date := schedule_date;
self.count_phase := count_phase;
self.display_status := display_status;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
RETURN;
end;
END;
/
