DROP TYPE "RIB_PendRtrnDtlRsnCodeDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlRsnCodeDesc_REC";
/
DROP TYPE "RIB_PendRtrnDtlActCodeDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlActCodeDesc_REC";
/
DROP TYPE "RIB_PendRtrnDtlDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlDesc_REC";
/
DROP TYPE "RIB_PendRtrnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDesc_REC";
/
DROP TYPE "RIB_PendRtrnDtlRsnCodeDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlRsnCodeDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PendRtrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  reason_code varchar2(6),
  description varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PendRtrnDtlRsnCodeDesc_REC"
(
  rib_oid number
, reason_code varchar2
) return self as result
,constructor function "RIB_PendRtrnDtlRsnCodeDesc_REC"
(
  rib_oid number
, reason_code varchar2
, description varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PendRtrnDtlRsnCodeDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PendRtrnDesc') := "ns_name_PendRtrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_code') := reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
END appendNodeValues;
constructor function "RIB_PendRtrnDtlRsnCodeDesc_REC"
(
  rib_oid number
, reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.reason_code := reason_code;
RETURN;
end;
constructor function "RIB_PendRtrnDtlRsnCodeDesc_REC"
(
  rib_oid number
, reason_code varchar2
, description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.reason_code := reason_code;
self.description := description;
RETURN;
end;
END;
/
DROP TYPE "RIB_PendRtrnDtlActCodeDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlActCodeDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PendRtrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  action_code varchar2(6),
  unit_qty number(12),
  disposition_code varchar2(6),
  sellable varchar2(1),
  comments varchar2(1000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
) return self as result
,constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
) return self as result
,constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
, disposition_code varchar2
) return self as result
,constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
, disposition_code varchar2
, sellable varchar2
) return self as result
,constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
, disposition_code varchar2
, sellable varchar2
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PendRtrnDtlActCodeDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PendRtrnDesc') := "ns_name_PendRtrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'action_code') := action_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'disposition_code') := disposition_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'sellable') := sellable;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.action_code := action_code;
RETURN;
end;
constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.action_code := action_code;
self.unit_qty := unit_qty;
RETURN;
end;
constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
, disposition_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.action_code := action_code;
self.unit_qty := unit_qty;
self.disposition_code := disposition_code;
RETURN;
end;
constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
, disposition_code varchar2
, sellable varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.action_code := action_code;
self.unit_qty := unit_qty;
self.disposition_code := disposition_code;
self.sellable := sellable;
RETURN;
end;
constructor function "RIB_PendRtrnDtlActCodeDesc_REC"
(
  rib_oid number
, action_code varchar2
, unit_qty number
, disposition_code varchar2
, sellable varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.action_code := action_code;
self.unit_qty := unit_qty;
self.disposition_code := disposition_code;
self.sellable := sellable;
self.comments := comments;
RETURN;
end;
END;
/
DROP TYPE "RIB_PendRtrnDtlRsnCodeDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlRsnCodeDesc_TBL" AS TABLE OF "RIB_PendRtrnDtlRsnCodeDesc_REC";
/
DROP TYPE "RIB_PendRtrnDtlActCodeDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlActCodeDesc_TBL" AS TABLE OF "RIB_PendRtrnDtlActCodeDesc_REC";
/
DROP TYPE "RIB_PendRtrnDtlDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PendRtrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  line_item_nbr number(3),
  expected_unit_qty number(12),
  PendRtrnDtlRsnCodeDesc_TBL "RIB_PendRtrnDtlRsnCodeDesc_TBL",   -- Size of "RIB_PendRtrnDtlRsnCodeDesc_TBL" is unbounded
  PendRtrnDtlActCodeDesc_TBL "RIB_PendRtrnDtlActCodeDesc_TBL",   -- Size of "RIB_PendRtrnDtlActCodeDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
) return self as result
,constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
, expected_unit_qty number
) return self as result
,constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
, expected_unit_qty number
, PendRtrnDtlRsnCodeDesc_TBL "RIB_PendRtrnDtlRsnCodeDesc_TBL"  -- Size of "RIB_PendRtrnDtlRsnCodeDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
, expected_unit_qty number
, PendRtrnDtlRsnCodeDesc_TBL "RIB_PendRtrnDtlRsnCodeDesc_TBL"  -- Size of "RIB_PendRtrnDtlRsnCodeDesc_TBL" is unbounded
, PendRtrnDtlActCodeDesc_TBL "RIB_PendRtrnDtlActCodeDesc_TBL"  -- Size of "RIB_PendRtrnDtlActCodeDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PendRtrnDtlDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PendRtrnDesc') := "ns_name_PendRtrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_nbr') := line_item_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_unit_qty') := expected_unit_qty;
  IF PendRtrnDtlRsnCodeDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PendRtrnDtlRsnCodeDesc_TBL.';
    FOR INDX IN PendRtrnDtlRsnCodeDesc_TBL.FIRST()..PendRtrnDtlRsnCodeDesc_TBL.LAST() LOOP
      PendRtrnDtlRsnCodeDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PendRtrnDtlRsnCodeDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF PendRtrnDtlActCodeDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PendRtrnDtlActCodeDesc_TBL.';
    FOR INDX IN PendRtrnDtlActCodeDesc_TBL.FIRST()..PendRtrnDtlActCodeDesc_TBL.LAST() LOOP
      PendRtrnDtlActCodeDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PendRtrnDtlActCodeDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.line_item_nbr := line_item_nbr;
RETURN;
end;
constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
, expected_unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.line_item_nbr := line_item_nbr;
self.expected_unit_qty := expected_unit_qty;
RETURN;
end;
constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
, expected_unit_qty number
, PendRtrnDtlRsnCodeDesc_TBL "RIB_PendRtrnDtlRsnCodeDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.line_item_nbr := line_item_nbr;
self.expected_unit_qty := expected_unit_qty;
self.PendRtrnDtlRsnCodeDesc_TBL := PendRtrnDtlRsnCodeDesc_TBL;
RETURN;
end;
constructor function "RIB_PendRtrnDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, line_item_nbr number
, expected_unit_qty number
, PendRtrnDtlRsnCodeDesc_TBL "RIB_PendRtrnDtlRsnCodeDesc_TBL"
, PendRtrnDtlActCodeDesc_TBL "RIB_PendRtrnDtlActCodeDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.line_item_nbr := line_item_nbr;
self.expected_unit_qty := expected_unit_qty;
self.PendRtrnDtlRsnCodeDesc_TBL := PendRtrnDtlRsnCodeDesc_TBL;
self.PendRtrnDtlActCodeDesc_TBL := PendRtrnDtlActCodeDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PendRtrnDtlDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnDtlDesc_TBL" AS TABLE OF "RIB_PendRtrnDtlDesc_REC";
/
DROP TYPE "RIB_PendRtrnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PendRtrnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PendRtrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  physical_wh number(10),
  rma_nbr varchar2(20),
  cust_order_nbr varchar2(48),
  special_instructions varchar2(250),
  expected_receipt date,
  PendRtrnDtlDesc_TBL "RIB_PendRtrnDtlDesc_TBL",   -- Size of "RIB_PendRtrnDtlDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PendRtrnDesc_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, cust_order_nbr varchar2
, special_instructions varchar2
, expected_receipt date
, PendRtrnDtlDesc_TBL "RIB_PendRtrnDtlDesc_TBL"  -- Size of "RIB_PendRtrnDtlDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PendRtrnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PendRtrnDesc') := "ns_name_PendRtrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'rma_nbr') := rma_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'special_instructions') := special_instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_receipt') := expected_receipt;
  l_new_pre :=i_prefix||'PendRtrnDtlDesc_TBL.';
  FOR INDX IN PendRtrnDtlDesc_TBL.FIRST()..PendRtrnDtlDesc_TBL.LAST() LOOP
    PendRtrnDtlDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PendRtrnDtlDesc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PendRtrnDesc_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, cust_order_nbr varchar2
, special_instructions varchar2
, expected_receipt date
, PendRtrnDtlDesc_TBL "RIB_PendRtrnDtlDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_wh := physical_wh;
self.rma_nbr := rma_nbr;
self.cust_order_nbr := cust_order_nbr;
self.special_instructions := special_instructions;
self.expected_receipt := expected_receipt;
self.PendRtrnDtlDesc_TBL := PendRtrnDtlDesc_TBL;
RETURN;
end;
END;
/
