DROP TYPE "RIB_ShlfRplDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplDesc_REC";
/
DROP TYPE "RIB_ShlfRplItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplItm_REC";
/
DROP TYPE "RIB_ShlfRplItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplItm_TBL" AS TABLE OF "RIB_ShlfRplItm_REC";
/
DROP TYPE "RIB_ShlfRplDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  replenishment_id varchar2(15),
  store_id number(10),
  product_group_id number(12),
  department_id number(12),
  class_id number(12),
  subclass_id number(12),
  shelf_adjust_id number(15),
  description varchar2(128),
  replenish_quantity number(20,4),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETE, CANCELED, PENDING_ALTERED, UNKNOWN] (all lower-case)
  type varchar2(20), -- type is enumeration field, valid values are [CAPACITY, SALES, AD_HOC, DISPLAY, UNKNOWN] (all lower-case)
  replenishment_mode varchar2(20), -- replenishment_mode is enumeration field, valid values are [END_OF_DAY, WITHIN_DAY, UNKNOWN] (all lower-case)
  user_name varchar2(128),
  create_date date,
  update_date date,
  ShlfRplItm_TBL "RIB_ShlfRplItm_TBL",   -- Size of "RIB_ShlfRplItm_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplDesc_REC"
(
  rib_oid number
, replenishment_id varchar2
, store_id number
, product_group_id number
, department_id number
, class_id number
, subclass_id number
, shelf_adjust_id number
, description varchar2
, replenish_quantity number
, status varchar2
, type varchar2
, replenishment_mode varchar2
, user_name varchar2
, create_date date
, update_date date
) return self as result
,constructor function "RIB_ShlfRplDesc_REC"
(
  rib_oid number
, replenishment_id varchar2
, store_id number
, product_group_id number
, department_id number
, class_id number
, subclass_id number
, shelf_adjust_id number
, description varchar2
, replenish_quantity number
, status varchar2
, type varchar2
, replenishment_mode varchar2
, user_name varchar2
, create_date date
, update_date date
, ShlfRplItm_TBL "RIB_ShlfRplItm_TBL"  -- Size of "RIB_ShlfRplItm_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplDesc') := "ns_name_ShlfRplDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_id') := replenishment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'shelf_adjust_id') := shelf_adjust_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenish_quantity') := replenish_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_mode') := replenishment_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  IF ShlfRplItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ShlfRplItm_TBL.';
    FOR INDX IN ShlfRplItm_TBL.FIRST()..ShlfRplItm_TBL.LAST() LOOP
      ShlfRplItm_TBL(indx).appendNodeValues( i_prefix||indx||'ShlfRplItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ShlfRplDesc_REC"
(
  rib_oid number
, replenishment_id varchar2
, store_id number
, product_group_id number
, department_id number
, class_id number
, subclass_id number
, shelf_adjust_id number
, description varchar2
, replenish_quantity number
, status varchar2
, type varchar2
, replenishment_mode varchar2
, user_name varchar2
, create_date date
, update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.replenishment_id := replenishment_id;
self.store_id := store_id;
self.product_group_id := product_group_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.shelf_adjust_id := shelf_adjust_id;
self.description := description;
self.replenish_quantity := replenish_quantity;
self.status := status;
self.type := type;
self.replenishment_mode := replenishment_mode;
self.user_name := user_name;
self.create_date := create_date;
self.update_date := update_date;
RETURN;
end;
constructor function "RIB_ShlfRplDesc_REC"
(
  rib_oid number
, replenishment_id varchar2
, store_id number
, product_group_id number
, department_id number
, class_id number
, subclass_id number
, shelf_adjust_id number
, description varchar2
, replenish_quantity number
, status varchar2
, type varchar2
, replenishment_mode varchar2
, user_name varchar2
, create_date date
, update_date date
, ShlfRplItm_TBL "RIB_ShlfRplItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.replenishment_id := replenishment_id;
self.store_id := store_id;
self.product_group_id := product_group_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.shelf_adjust_id := shelf_adjust_id;
self.description := description;
self.replenish_quantity := replenish_quantity;
self.status := status;
self.type := type;
self.replenishment_mode := replenishment_mode;
self.user_name := user_name;
self.create_date := create_date;
self.update_date := update_date;
self.ShlfRplItm_TBL := ShlfRplItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShlfRplItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  pick_from_area varchar2(20), -- pick_from_area is enumeration field, valid values are [BACKROOM, DELIVERY_BAY, UNKNOWN] (all lower-case)
  requested_pick_amount number(20,4),
  actual_pick_amount number(20,4),
  case_size number(10,2),
  substitute_item_id varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, pick_from_area varchar2
, requested_pick_amount number
, actual_pick_amount number
, case_size number
) return self as result
,constructor function "RIB_ShlfRplItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, pick_from_area varchar2
, requested_pick_amount number
, actual_pick_amount number
, case_size number
, substitute_item_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplDesc') := "ns_name_ShlfRplDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_from_area') := pick_from_area;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pick_amount') := requested_pick_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'actual_pick_amount') := actual_pick_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'substitute_item_id') := substitute_item_id;
END appendNodeValues;
constructor function "RIB_ShlfRplItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, pick_from_area varchar2
, requested_pick_amount number
, actual_pick_amount number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.pick_from_area := pick_from_area;
self.requested_pick_amount := requested_pick_amount;
self.actual_pick_amount := actual_pick_amount;
self.case_size := case_size;
RETURN;
end;
constructor function "RIB_ShlfRplItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, pick_from_area varchar2
, requested_pick_amount number
, actual_pick_amount number
, case_size number
, substitute_item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.pick_from_area := pick_from_area;
self.requested_pick_amount := requested_pick_amount;
self.actual_pick_amount := actual_pick_amount;
self.case_size := case_size;
self.substitute_item_id := substitute_item_id;
RETURN;
end;
END;
/
