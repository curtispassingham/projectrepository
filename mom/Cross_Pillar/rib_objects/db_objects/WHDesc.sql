@@AddrDesc.sql;
/
DROP TYPE "RIB_WHDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WHDesc_REC";
/
DROP TYPE "RIB_AddrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrDesc_TBL" AS TABLE OF "RIB_AddrDesc_REC";
/
DROP TYPE "RIB_WHDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WHDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WHDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  wh number(10),
  wh_name varchar2(150),
  wh_add1 varchar2(240),
  wh_add2 varchar2(240),
  wh_city varchar2(120),
  county varchar2(250),
  state varchar2(3),
  country_id varchar2(3),
  wh_pcode varchar2(30),
  email varchar2(100),
  stockholding_ind varchar2(1),
  channel_id number(4),
  currency_code varchar2(3),
  duns_number varchar2(9),
  duns_loc varchar2(4),
  physical_wh number(10),
  break_pack_ind varchar2(1),
  redist_wh_ind varchar2(1),
  delivery_policy varchar2(6),
  contact_person varchar2(120),
  dest_fax varchar2(20),
  phone_nbr varchar2(20),
  default_route varchar2(10),
  default_carrier_code varchar2(4),
  default_service_code varchar2(6),
  expedite_route varchar2(10),
  expedite_carrier_code varchar2(4),
  expedite_service_code varchar2(6),
  bol_upload_type varchar2(4),
  bol_print_type varchar2(4),
  lead_time number(4),
  distance_to_dest number(4),
  drop_trailers_accepted_flag varchar2(1),
  rcv_dock_available_flag varchar2(1),
  container_type varchar2(6),
  mld_default_route varchar2(10),
  unit_pick_container_type varchar2(6),
  dest_seq_nbr number(4),
  owning_dc varchar2(10),
  AddrDesc_TBL "RIB_AddrDesc_TBL",   -- Size of "RIB_AddrDesc_TBL" is unbounded
  pricing_loc number(10),
  pricing_loc_curr varchar2(3),
  org_unit_id number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
, pricing_loc_curr varchar2
) return self as result
,constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
, pricing_loc number
, pricing_loc_curr varchar2
, org_unit_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WHDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WHDesc') := "ns_name_WHDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'wh') := wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh_name') := wh_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh_add1') := wh_add1;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh_add2') := wh_add2;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh_city') := wh_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'county') := county;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh_pcode') := wh_pcode;
  rib_obj_util.g_RIB_element_values(i_prefix||'email') := email;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'channel_id') := channel_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_number') := duns_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'duns_loc') := duns_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'break_pack_ind') := break_pack_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'redist_wh_ind') := redist_wh_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_policy') := delivery_policy;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_person') := contact_person;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_fax') := dest_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_nbr') := phone_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_route') := default_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_carrier_code') := default_carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_service_code') := default_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_route') := expedite_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_carrier_code') := expedite_carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_service_code') := expedite_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_upload_type') := bol_upload_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_print_type') := bol_print_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'lead_time') := lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'distance_to_dest') := distance_to_dest;
  rib_obj_util.g_RIB_element_values(i_prefix||'drop_trailers_accepted_flag') := drop_trailers_accepted_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'rcv_dock_available_flag') := rcv_dock_available_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_type') := container_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'mld_default_route') := mld_default_route;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_pick_container_type') := unit_pick_container_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_seq_nbr') := dest_seq_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'owning_dc') := owning_dc;
  IF AddrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrDesc_TBL.';
    FOR INDX IN AddrDesc_TBL.FIRST()..AddrDesc_TBL.LAST() LOOP
      AddrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'AddrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_loc') := pricing_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_loc_curr') := pricing_loc_curr;
  rib_obj_util.g_RIB_element_values(i_prefix||'org_unit_id') := org_unit_id;
END appendNodeValues;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.AddrDesc_TBL := AddrDesc_TBL;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
, pricing_loc_curr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
self.pricing_loc_curr := pricing_loc_curr;
RETURN;
end;
constructor function "RIB_WHDesc_REC"
(
  rib_oid number
, wh number
, wh_name varchar2
, wh_add1 varchar2
, wh_add2 varchar2
, wh_city varchar2
, county varchar2
, state varchar2
, country_id varchar2
, wh_pcode varchar2
, email varchar2
, stockholding_ind varchar2
, channel_id number
, currency_code varchar2
, duns_number varchar2
, duns_loc varchar2
, physical_wh number
, break_pack_ind varchar2
, redist_wh_ind varchar2
, delivery_policy varchar2
, contact_person varchar2
, dest_fax varchar2
, phone_nbr varchar2
, default_route varchar2
, default_carrier_code varchar2
, default_service_code varchar2
, expedite_route varchar2
, expedite_carrier_code varchar2
, expedite_service_code varchar2
, bol_upload_type varchar2
, bol_print_type varchar2
, lead_time number
, distance_to_dest number
, drop_trailers_accepted_flag varchar2
, rcv_dock_available_flag varchar2
, container_type varchar2
, mld_default_route varchar2
, unit_pick_container_type varchar2
, dest_seq_nbr number
, owning_dc varchar2
, AddrDesc_TBL "RIB_AddrDesc_TBL"
, pricing_loc number
, pricing_loc_curr varchar2
, org_unit_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.wh_name := wh_name;
self.wh_add1 := wh_add1;
self.wh_add2 := wh_add2;
self.wh_city := wh_city;
self.county := county;
self.state := state;
self.country_id := country_id;
self.wh_pcode := wh_pcode;
self.email := email;
self.stockholding_ind := stockholding_ind;
self.channel_id := channel_id;
self.currency_code := currency_code;
self.duns_number := duns_number;
self.duns_loc := duns_loc;
self.physical_wh := physical_wh;
self.break_pack_ind := break_pack_ind;
self.redist_wh_ind := redist_wh_ind;
self.delivery_policy := delivery_policy;
self.contact_person := contact_person;
self.dest_fax := dest_fax;
self.phone_nbr := phone_nbr;
self.default_route := default_route;
self.default_carrier_code := default_carrier_code;
self.default_service_code := default_service_code;
self.expedite_route := expedite_route;
self.expedite_carrier_code := expedite_carrier_code;
self.expedite_service_code := expedite_service_code;
self.bol_upload_type := bol_upload_type;
self.bol_print_type := bol_print_type;
self.lead_time := lead_time;
self.distance_to_dest := distance_to_dest;
self.drop_trailers_accepted_flag := drop_trailers_accepted_flag;
self.rcv_dock_available_flag := rcv_dock_available_flag;
self.container_type := container_type;
self.mld_default_route := mld_default_route;
self.unit_pick_container_type := unit_pick_container_type;
self.dest_seq_nbr := dest_seq_nbr;
self.owning_dc := owning_dc;
self.AddrDesc_TBL := AddrDesc_TBL;
self.pricing_loc := pricing_loc;
self.pricing_loc_curr := pricing_loc_curr;
self.org_unit_id := org_unit_id;
RETURN;
end;
END;
/
