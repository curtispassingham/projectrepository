DROP TYPE "RIB_ItemUPCDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemUPCDesc_REC";
/
DROP TYPE "RIB_ItemUPCDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemUPCDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemUPCDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  item_parent varchar2(25),
  primary_ref_item_ind varchar2(1),
  format_id varchar2(1),
  prefix number(2),
  item_number_type varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
) return self as result
,constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
) return self as result
,constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
) return self as result
,constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
, format_id varchar2
) return self as result
,constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
, format_id varchar2
, prefix number
) return self as result
,constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
, format_id varchar2
, prefix number
, item_number_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemUPCDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemUPCDesc') := "ns_name_ItemUPCDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_parent') := item_parent;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_ref_item_ind') := primary_ref_item_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_id') := format_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'prefix') := prefix;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_number_type') := item_number_type;
END appendNodeValues;
constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_parent := item_parent;
RETURN;
end;
constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_parent := item_parent;
self.primary_ref_item_ind := primary_ref_item_ind;
RETURN;
end;
constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
, format_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_parent := item_parent;
self.primary_ref_item_ind := primary_ref_item_ind;
self.format_id := format_id;
RETURN;
end;
constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
, format_id varchar2
, prefix number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_parent := item_parent;
self.primary_ref_item_ind := primary_ref_item_ind;
self.format_id := format_id;
self.prefix := prefix;
RETURN;
end;
constructor function "RIB_ItemUPCDesc_REC"
(
  rib_oid number
, item_id varchar2
, item_parent varchar2
, primary_ref_item_ind varchar2
, format_id varchar2
, prefix number
, item_number_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_parent := item_parent;
self.primary_ref_item_ind := primary_ref_item_ind;
self.format_id := format_id;
self.prefix := prefix;
self.item_number_type := item_number_type;
RETURN;
end;
END;
/
