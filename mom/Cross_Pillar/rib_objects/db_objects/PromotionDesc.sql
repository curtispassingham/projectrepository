DROP TYPE "RIB_ListGroup_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ListGroup_REC";
/
DROP TYPE "RIB_ListDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ListDtl_REC";
/
DROP TYPE "RIB_LocationList_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocationList_REC";
/
DROP TYPE "RIB_PromotionDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PromotionDesc_REC";
/
DROP TYPE "RIB_DiscountLadder_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DiscountLadder_REC";
/
DROP TYPE "RIB_ItemList_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemList_REC";
/
DROP TYPE "RIB_ItemLocSimpleRet_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocSimpleRet_REC";
/
DROP TYPE "RIB_CreditDetail_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CreditDetail_REC";
/
DROP TYPE "RIB_ListDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ListDtl_TBL" AS TABLE OF "RIB_ListDtl_REC";
/
DROP TYPE "RIB_ListGroup_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ListGroup_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  listgroup_id number(10),
  ListDtl_TBL "RIB_ListDtl_TBL",   -- Size of "RIB_ListDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ListGroup_REC"
(
  rib_oid number
, listgroup_id number
, ListDtl_TBL "RIB_ListDtl_TBL"  -- Size of "RIB_ListDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ListGroup_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'listgroup_id') := listgroup_id;
  l_new_pre :=i_prefix||'ListDtl_TBL.';
  FOR INDX IN ListDtl_TBL.FIRST()..ListDtl_TBL.LAST() LOOP
    ListDtl_TBL(indx).appendNodeValues( i_prefix||indx||'ListDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ListGroup_REC"
(
  rib_oid number
, listgroup_id number
, ListDtl_TBL "RIB_ListDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.listgroup_id := listgroup_id;
self.ListDtl_TBL := ListDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_DiscountLadder_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DiscountLadder_TBL" AS TABLE OF "RIB_DiscountLadder_REC";
/
DROP TYPE "RIB_ItemList_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemList_TBL" AS TABLE OF "RIB_ItemList_REC";
/
DROP TYPE "RIB_ListDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ListDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  listdtl_id number(10),
  reward_application number(1),
  description varchar2(160),
  DiscountLadder_TBL "RIB_DiscountLadder_TBL",   -- Size of "RIB_DiscountLadder_TBL" is unbounded
  ItemList_TBL "RIB_ItemList_TBL",   -- Size of "RIB_ItemList_TBL" is unbounded
  price_range_min number(20,4),
  price_range_max number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ListDtl_REC"
(
  rib_oid number
, listdtl_id number
, reward_application number
, description varchar2
, DiscountLadder_TBL "RIB_DiscountLadder_TBL"  -- Size of "RIB_DiscountLadder_TBL" is unbounded
, ItemList_TBL "RIB_ItemList_TBL"  -- Size of "RIB_ItemList_TBL" is unbounded
) return self as result
,constructor function "RIB_ListDtl_REC"
(
  rib_oid number
, listdtl_id number
, reward_application number
, description varchar2
, DiscountLadder_TBL "RIB_DiscountLadder_TBL"  -- Size of "RIB_DiscountLadder_TBL" is unbounded
, ItemList_TBL "RIB_ItemList_TBL"  -- Size of "RIB_ItemList_TBL" is unbounded
, price_range_min number
) return self as result
,constructor function "RIB_ListDtl_REC"
(
  rib_oid number
, listdtl_id number
, reward_application number
, description varchar2
, DiscountLadder_TBL "RIB_DiscountLadder_TBL"  -- Size of "RIB_DiscountLadder_TBL" is unbounded
, ItemList_TBL "RIB_ItemList_TBL"  -- Size of "RIB_ItemList_TBL" is unbounded
, price_range_min number
, price_range_max number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ListDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'listdtl_id') := listdtl_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reward_application') := reward_application;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  IF DiscountLadder_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DiscountLadder_TBL.';
    FOR INDX IN DiscountLadder_TBL.FIRST()..DiscountLadder_TBL.LAST() LOOP
      DiscountLadder_TBL(indx).appendNodeValues( i_prefix||indx||'DiscountLadder_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ItemList_TBL.';
  FOR INDX IN ItemList_TBL.FIRST()..ItemList_TBL.LAST() LOOP
    ItemList_TBL(indx).appendNodeValues( i_prefix||indx||'ItemList_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_range_min') := price_range_min;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_range_max') := price_range_max;
END appendNodeValues;
constructor function "RIB_ListDtl_REC"
(
  rib_oid number
, listdtl_id number
, reward_application number
, description varchar2
, DiscountLadder_TBL "RIB_DiscountLadder_TBL"
, ItemList_TBL "RIB_ItemList_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.listdtl_id := listdtl_id;
self.reward_application := reward_application;
self.description := description;
self.DiscountLadder_TBL := DiscountLadder_TBL;
self.ItemList_TBL := ItemList_TBL;
RETURN;
end;
constructor function "RIB_ListDtl_REC"
(
  rib_oid number
, listdtl_id number
, reward_application number
, description varchar2
, DiscountLadder_TBL "RIB_DiscountLadder_TBL"
, ItemList_TBL "RIB_ItemList_TBL"
, price_range_min number
) return self as result is
begin
self.rib_oid := rib_oid;
self.listdtl_id := listdtl_id;
self.reward_application := reward_application;
self.description := description;
self.DiscountLadder_TBL := DiscountLadder_TBL;
self.ItemList_TBL := ItemList_TBL;
self.price_range_min := price_range_min;
RETURN;
end;
constructor function "RIB_ListDtl_REC"
(
  rib_oid number
, listdtl_id number
, reward_application number
, description varchar2
, DiscountLadder_TBL "RIB_DiscountLadder_TBL"
, ItemList_TBL "RIB_ItemList_TBL"
, price_range_min number
, price_range_max number
) return self as result is
begin
self.rib_oid := rib_oid;
self.listdtl_id := listdtl_id;
self.reward_application := reward_application;
self.description := description;
self.DiscountLadder_TBL := DiscountLadder_TBL;
self.ItemList_TBL := ItemList_TBL;
self.price_range_min := price_range_min;
self.price_range_max := price_range_max;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocationList_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocationList_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocationList_REC"
(
  rib_oid number
, location number
, loc_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocationList_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
END appendNodeValues;
constructor function "RIB_LocationList_REC"
(
  rib_oid number
, location number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocationList_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocationList_TBL" AS TABLE OF "RIB_LocationList_REC";
/
DROP TYPE "RIB_ListGroup_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ListGroup_TBL" AS TABLE OF "RIB_ListGroup_REC";
/
DROP TYPE "RIB_ItemLocSimpleRet_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocSimpleRet_TBL" AS TABLE OF "RIB_ItemLocSimpleRet_REC";
/
DROP TYPE "RIB_CreditDetail_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CreditDetail_TBL" AS TABLE OF "RIB_CreditDetail_REC";
/
DROP TYPE "RIB_PromotionDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PromotionDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  promo_id number(10),
  promo_name varchar2(160),
  promo_description varchar2(640),
  promo_comp_id number(10),
  promo_comp_desc varchar2(160),
  promo_comp_type number(2),
  promo_dtl_id number(10),
  apply_order number(6),
  start_date date,
  end_date date,
  apply_to_code number(1),
  discount_limit number(3),
  exception_parent_id number(10),
  LocationList_TBL "RIB_LocationList_TBL",   -- Size of "RIB_LocationList_TBL" is unbounded
  ListGroup_TBL "RIB_ListGroup_TBL",   -- Size of "RIB_ListGroup_TBL" is unbounded
  ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL",   -- Size of "RIB_ItemLocSimpleRet_TBL" is unbounded
  customer_type_id number(10),
  CreditDetail_TBL "RIB_CreditDetail_TBL",   -- Size of "RIB_CreditDetail_TBL" is unbounded
  threshold_qual_type number(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"  -- Size of "RIB_LocationList_TBL" is unbounded
, ListGroup_TBL "RIB_ListGroup_TBL"  -- Size of "RIB_ListGroup_TBL" is unbounded
) return self as result
,constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"  -- Size of "RIB_LocationList_TBL" is unbounded
, ListGroup_TBL "RIB_ListGroup_TBL"  -- Size of "RIB_ListGroup_TBL" is unbounded
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"  -- Size of "RIB_ItemLocSimpleRet_TBL" is unbounded
) return self as result
,constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"  -- Size of "RIB_LocationList_TBL" is unbounded
, ListGroup_TBL "RIB_ListGroup_TBL"  -- Size of "RIB_ListGroup_TBL" is unbounded
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"  -- Size of "RIB_ItemLocSimpleRet_TBL" is unbounded
, customer_type_id number
) return self as result
,constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"  -- Size of "RIB_LocationList_TBL" is unbounded
, ListGroup_TBL "RIB_ListGroup_TBL"  -- Size of "RIB_ListGroup_TBL" is unbounded
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"  -- Size of "RIB_ItemLocSimpleRet_TBL" is unbounded
, customer_type_id number
, CreditDetail_TBL "RIB_CreditDetail_TBL"  -- Size of "RIB_CreditDetail_TBL" is unbounded
) return self as result
,constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"  -- Size of "RIB_LocationList_TBL" is unbounded
, ListGroup_TBL "RIB_ListGroup_TBL"  -- Size of "RIB_ListGroup_TBL" is unbounded
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"  -- Size of "RIB_ItemLocSimpleRet_TBL" is unbounded
, customer_type_id number
, CreditDetail_TBL "RIB_CreditDetail_TBL"  -- Size of "RIB_CreditDetail_TBL" is unbounded
, threshold_qual_type number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PromotionDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_id') := promo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_name') := promo_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_description') := promo_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_id') := promo_comp_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_desc') := promo_comp_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_type') := promo_comp_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_dtl_id') := promo_dtl_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'apply_order') := apply_order;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'apply_to_code') := apply_to_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_limit') := discount_limit;
  rib_obj_util.g_RIB_element_values(i_prefix||'exception_parent_id') := exception_parent_id;
  l_new_pre :=i_prefix||'LocationList_TBL.';
  FOR INDX IN LocationList_TBL.FIRST()..LocationList_TBL.LAST() LOOP
    LocationList_TBL(indx).appendNodeValues( i_prefix||indx||'LocationList_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'ListGroup_TBL.';
  FOR INDX IN ListGroup_TBL.FIRST()..ListGroup_TBL.LAST() LOOP
    ListGroup_TBL(indx).appendNodeValues( i_prefix||indx||'ListGroup_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  IF ItemLocSimpleRet_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemLocSimpleRet_TBL.';
    FOR INDX IN ItemLocSimpleRet_TBL.FIRST()..ItemLocSimpleRet_TBL.LAST() LOOP
      ItemLocSimpleRet_TBL(indx).appendNodeValues( i_prefix||indx||'ItemLocSimpleRet_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_type_id') := customer_type_id;
  IF CreditDetail_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CreditDetail_TBL.';
    FOR INDX IN CreditDetail_TBL.FIRST()..CreditDetail_TBL.LAST() LOOP
      CreditDetail_TBL(indx).appendNodeValues( i_prefix||indx||'CreditDetail_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'threshold_qual_type') := threshold_qual_type;
END appendNodeValues;
constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"
, ListGroup_TBL "RIB_ListGroup_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_type := promo_comp_type;
self.promo_dtl_id := promo_dtl_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.apply_to_code := apply_to_code;
self.discount_limit := discount_limit;
self.exception_parent_id := exception_parent_id;
self.LocationList_TBL := LocationList_TBL;
self.ListGroup_TBL := ListGroup_TBL;
RETURN;
end;
constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"
, ListGroup_TBL "RIB_ListGroup_TBL"
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_type := promo_comp_type;
self.promo_dtl_id := promo_dtl_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.apply_to_code := apply_to_code;
self.discount_limit := discount_limit;
self.exception_parent_id := exception_parent_id;
self.LocationList_TBL := LocationList_TBL;
self.ListGroup_TBL := ListGroup_TBL;
self.ItemLocSimpleRet_TBL := ItemLocSimpleRet_TBL;
RETURN;
end;
constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"
, ListGroup_TBL "RIB_ListGroup_TBL"
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"
, customer_type_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_type := promo_comp_type;
self.promo_dtl_id := promo_dtl_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.apply_to_code := apply_to_code;
self.discount_limit := discount_limit;
self.exception_parent_id := exception_parent_id;
self.LocationList_TBL := LocationList_TBL;
self.ListGroup_TBL := ListGroup_TBL;
self.ItemLocSimpleRet_TBL := ItemLocSimpleRet_TBL;
self.customer_type_id := customer_type_id;
RETURN;
end;
constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"
, ListGroup_TBL "RIB_ListGroup_TBL"
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"
, customer_type_id number
, CreditDetail_TBL "RIB_CreditDetail_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_type := promo_comp_type;
self.promo_dtl_id := promo_dtl_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.apply_to_code := apply_to_code;
self.discount_limit := discount_limit;
self.exception_parent_id := exception_parent_id;
self.LocationList_TBL := LocationList_TBL;
self.ListGroup_TBL := ListGroup_TBL;
self.ItemLocSimpleRet_TBL := ItemLocSimpleRet_TBL;
self.customer_type_id := customer_type_id;
self.CreditDetail_TBL := CreditDetail_TBL;
RETURN;
end;
constructor function "RIB_PromotionDesc_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_type number
, promo_dtl_id number
, apply_order number
, start_date date
, end_date date
, apply_to_code number
, discount_limit number
, exception_parent_id number
, LocationList_TBL "RIB_LocationList_TBL"
, ListGroup_TBL "RIB_ListGroup_TBL"
, ItemLocSimpleRet_TBL "RIB_ItemLocSimpleRet_TBL"
, customer_type_id number
, CreditDetail_TBL "RIB_CreditDetail_TBL"
, threshold_qual_type number
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_type := promo_comp_type;
self.promo_dtl_id := promo_dtl_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.apply_to_code := apply_to_code;
self.discount_limit := discount_limit;
self.exception_parent_id := exception_parent_id;
self.LocationList_TBL := LocationList_TBL;
self.ListGroup_TBL := ListGroup_TBL;
self.ItemLocSimpleRet_TBL := ItemLocSimpleRet_TBL;
self.customer_type_id := customer_type_id;
self.CreditDetail_TBL := CreditDetail_TBL;
self.threshold_qual_type := threshold_qual_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_DiscountLadder_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DiscountLadder_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  chg_type number(2),
  chg_amt number(20,4),
  chg_currency varchar2(3),
  chg_percent number(20,4),
  chg_selling_uom varchar2(4),
  qual_type number(2),
  qual_value number(20,4),
  chg_duration number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
, qual_type number
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
, qual_type number
, qual_value number
) return self as result
,constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
, qual_type number
, qual_value number
, chg_duration number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DiscountLadder_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'chg_type') := chg_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'chg_amt') := chg_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'chg_currency') := chg_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'chg_percent') := chg_percent;
  rib_obj_util.g_RIB_element_values(i_prefix||'chg_selling_uom') := chg_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'qual_type') := qual_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'qual_value') := qual_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'chg_duration') := chg_duration;
END appendNodeValues;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
self.chg_currency := chg_currency;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
self.chg_currency := chg_currency;
self.chg_percent := chg_percent;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
self.chg_currency := chg_currency;
self.chg_percent := chg_percent;
self.chg_selling_uom := chg_selling_uom;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
, qual_type number
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
self.chg_currency := chg_currency;
self.chg_percent := chg_percent;
self.chg_selling_uom := chg_selling_uom;
self.qual_type := qual_type;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
, qual_type number
, qual_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
self.chg_currency := chg_currency;
self.chg_percent := chg_percent;
self.chg_selling_uom := chg_selling_uom;
self.qual_type := qual_type;
self.qual_value := qual_value;
RETURN;
end;
constructor function "RIB_DiscountLadder_REC"
(
  rib_oid number
, chg_type number
, chg_amt number
, chg_currency varchar2
, chg_percent number
, chg_selling_uom varchar2
, qual_type number
, qual_value number
, chg_duration number
) return self as result is
begin
self.rib_oid := rib_oid;
self.chg_type := chg_type;
self.chg_amt := chg_amt;
self.chg_currency := chg_currency;
self.chg_percent := chg_percent;
self.chg_selling_uom := chg_selling_uom;
self.qual_type := qual_type;
self.qual_value := qual_value;
self.chg_duration := chg_duration;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemList_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemList_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_num varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemList_REC"
(
  rib_oid number
, item_num varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemList_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_num') := item_num;
END appendNodeValues;
constructor function "RIB_ItemList_REC"
(
  rib_oid number
, item_num varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_num := item_num;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemLocSimpleRet_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocSimpleRet_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_num varchar2(25),
  location number(10),
  selling_retail number(20,4),
  selling_uom varchar2(4),
  effective_date date,
  selling_currency varchar2(3),
  ref_promo_dtl_id number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocSimpleRet_REC"
(
  rib_oid number
, item_num varchar2
, location number
, selling_retail number
, selling_uom varchar2
, effective_date date
, selling_currency varchar2
) return self as result
,constructor function "RIB_ItemLocSimpleRet_REC"
(
  rib_oid number
, item_num varchar2
, location number
, selling_retail number
, selling_uom varchar2
, effective_date date
, selling_currency varchar2
, ref_promo_dtl_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocSimpleRet_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_num') := item_num;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_retail') := selling_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_currency') := selling_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_promo_dtl_id') := ref_promo_dtl_id;
END appendNodeValues;
constructor function "RIB_ItemLocSimpleRet_REC"
(
  rib_oid number
, item_num varchar2
, location number
, selling_retail number
, selling_uom varchar2
, effective_date date
, selling_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_num := item_num;
self.location := location;
self.selling_retail := selling_retail;
self.selling_uom := selling_uom;
self.effective_date := effective_date;
self.selling_currency := selling_currency;
RETURN;
end;
constructor function "RIB_ItemLocSimpleRet_REC"
(
  rib_oid number
, item_num varchar2
, location number
, selling_retail number
, selling_uom varchar2
, effective_date date
, selling_currency varchar2
, ref_promo_dtl_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_num := item_num;
self.location := location;
self.selling_retail := selling_retail;
self.selling_uom := selling_uom;
self.effective_date := effective_date;
self.selling_currency := selling_currency;
self.ref_promo_dtl_id := ref_promo_dtl_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_CreditDetail_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CreditDetail_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PromotionDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  credit_detail_id number(10),
  credit_type varchar2(40),
  bin_number_from number(10),
  bin_number_to number(10),
  commision_rate number(10),
  comments varchar2(160),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CreditDetail_REC"
(
  rib_oid number
, credit_detail_id number
, credit_type varchar2
, bin_number_from number
, bin_number_to number
, commision_rate number
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CreditDetail_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PromotionDesc') := "ns_name_PromotionDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'credit_detail_id') := credit_detail_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'credit_type') := credit_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'bin_number_from') := bin_number_from;
  rib_obj_util.g_RIB_element_values(i_prefix||'bin_number_to') := bin_number_to;
  rib_obj_util.g_RIB_element_values(i_prefix||'commision_rate') := commision_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_CreditDetail_REC"
(
  rib_oid number
, credit_detail_id number
, credit_type varchar2
, bin_number_from number
, bin_number_to number
, commision_rate number
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.credit_detail_id := credit_detail_id;
self.credit_type := credit_type;
self.bin_number_from := bin_number_from;
self.bin_number_to := bin_number_to;
self.commision_rate := commision_rate;
self.comments := comments;
RETURN;
end;
END;
/
