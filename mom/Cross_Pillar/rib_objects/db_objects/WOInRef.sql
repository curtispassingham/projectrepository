DROP TYPE "RIB_WOInDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOInDtlRef_REC";
/
DROP TYPE "RIB_WOInRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOInRef_REC";
/
DROP TYPE "RIB_WOInDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOInDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOInRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  wh number(10),
  item varchar2(25),
  loc_type varchar2(1),
  location number(10),
  seq_no number(4),
  wip_code varchar2(9),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOInDtlRef_REC"
(
  rib_oid number
, wh number
, item varchar2
, loc_type varchar2
, location number
, seq_no number
, wip_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOInDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOInRef') := "ns_name_WOInRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'wh') := wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'wip_code') := wip_code;
END appendNodeValues;
constructor function "RIB_WOInDtlRef_REC"
(
  rib_oid number
, wh number
, item varchar2
, loc_type varchar2
, location number
, seq_no number
, wip_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wh := wh;
self.item := item;
self.loc_type := loc_type;
self.location := location;
self.seq_no := seq_no;
self.wip_code := wip_code;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOInDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOInDtlRef_TBL" AS TABLE OF "RIB_WOInDtlRef_REC";
/
DROP TYPE "RIB_WOInRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOInRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOInRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  wo_id number(10),
  order_no varchar2(12),
  WOInDtlRef_TBL "RIB_WOInDtlRef_TBL",   -- Size of "RIB_WOInDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOInRef_REC"
(
  rib_oid number
, wo_id number
, order_no varchar2
) return self as result
,constructor function "RIB_WOInRef_REC"
(
  rib_oid number
, wo_id number
, order_no varchar2
, WOInDtlRef_TBL "RIB_WOInDtlRef_TBL"  -- Size of "RIB_WOInDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOInRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOInRef') := "ns_name_WOInRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'wo_id') := wo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  IF WOInDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WOInDtlRef_TBL.';
    FOR INDX IN WOInDtlRef_TBL.FIRST()..WOInDtlRef_TBL.LAST() LOOP
      WOInDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'WOInDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_WOInRef_REC"
(
  rib_oid number
, wo_id number
, order_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.order_no := order_no;
RETURN;
end;
constructor function "RIB_WOInRef_REC"
(
  rib_oid number
, wo_id number
, order_no varchar2
, WOInDtlRef_TBL "RIB_WOInDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.order_no := order_no;
self.WOInDtlRef_TBL := WOInDtlRef_TBL;
RETURN;
end;
END;
/
