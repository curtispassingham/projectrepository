@@CustomerRef.sql;
/
DROP TYPE "RIB_LoyAcctDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LoyAcctDesc_REC";
/
DROP TYPE "RIB_ProgramLevel_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProgramLevel_REC";
/
DROP TYPE "RIB_Awards_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Awards_REC";
/
DROP TYPE "RIB_Coupon_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Coupon_REC";
/
DROP TYPE "RIB_PointsBalance_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PointsBalance_REC";
/
DROP TYPE "RIB_LoyaltyMember_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LoyaltyMember_REC";
/
DROP TYPE "RIB_LoyaltyCard_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LoyaltyCard_REC";
/
DROP TYPE "RIB_LoyaltyMember_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LoyaltyMember_TBL" AS TABLE OF "RIB_LoyaltyMember_REC";
/
DROP TYPE "RIB_LoyaltyCard_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LoyaltyCard_TBL" AS TABLE OF "RIB_LoyaltyCard_REC";
/
DROP TYPE "RIB_LoyAcctDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LoyAcctDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  account_id varchar2(32),
  program_id varchar2(32),
  start_date date,
  referred_by_member_number varchar2(32),
  ProgramLevel "RIB_ProgramLevel_REC",
  PointsBalance "RIB_PointsBalance_REC",
  Awards "RIB_Awards_REC",
  LoyaltyMember_TBL "RIB_LoyaltyMember_TBL",   -- Size of "RIB_LoyaltyMember_TBL" is 99
  LoyaltyCard_TBL "RIB_LoyaltyCard_TBL",   -- Size of "RIB_LoyaltyCard_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
, Awards "RIB_Awards_REC"
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
, Awards "RIB_Awards_REC"
, LoyaltyMember_TBL "RIB_LoyaltyMember_TBL"  -- Size of "RIB_LoyaltyMember_TBL" is 99
) return self as result
,constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
, Awards "RIB_Awards_REC"
, LoyaltyMember_TBL "RIB_LoyaltyMember_TBL"  -- Size of "RIB_LoyaltyMember_TBL" is 99
, LoyaltyCard_TBL "RIB_LoyaltyCard_TBL"  -- Size of "RIB_LoyaltyCard_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LoyAcctDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'account_id') := account_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'program_id') := program_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'referred_by_member_number') := referred_by_member_number;
  l_new_pre :=i_prefix||'ProgramLevel.';
  ProgramLevel.appendNodeValues( i_prefix||'ProgramLevel');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'PointsBalance.';
  PointsBalance.appendNodeValues( i_prefix||'PointsBalance');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'Awards.';
  Awards.appendNodeValues( i_prefix||'Awards');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LoyaltyMember_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LoyaltyMember_TBL.';
    FOR INDX IN LoyaltyMember_TBL.FIRST()..LoyaltyMember_TBL.LAST() LOOP
      LoyaltyMember_TBL(indx).appendNodeValues( i_prefix||indx||'LoyaltyMember_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF LoyaltyCard_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LoyaltyCard_TBL.';
    FOR INDX IN LoyaltyCard_TBL.FIRST()..LoyaltyCard_TBL.LAST() LOOP
      LoyaltyCard_TBL(indx).appendNodeValues( i_prefix||indx||'LoyaltyCard_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
self.referred_by_member_number := referred_by_member_number;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
self.referred_by_member_number := referred_by_member_number;
self.ProgramLevel := ProgramLevel;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
self.referred_by_member_number := referred_by_member_number;
self.ProgramLevel := ProgramLevel;
self.PointsBalance := PointsBalance;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
, Awards "RIB_Awards_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
self.referred_by_member_number := referred_by_member_number;
self.ProgramLevel := ProgramLevel;
self.PointsBalance := PointsBalance;
self.Awards := Awards;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
, Awards "RIB_Awards_REC"
, LoyaltyMember_TBL "RIB_LoyaltyMember_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
self.referred_by_member_number := referred_by_member_number;
self.ProgramLevel := ProgramLevel;
self.PointsBalance := PointsBalance;
self.Awards := Awards;
self.LoyaltyMember_TBL := LoyaltyMember_TBL;
RETURN;
end;
constructor function "RIB_LoyAcctDesc_REC"
(
  rib_oid number
, account_id varchar2
, program_id varchar2
, start_date date
, referred_by_member_number varchar2
, ProgramLevel "RIB_ProgramLevel_REC"
, PointsBalance "RIB_PointsBalance_REC"
, Awards "RIB_Awards_REC"
, LoyaltyMember_TBL "RIB_LoyaltyMember_TBL"
, LoyaltyCard_TBL "RIB_LoyaltyCard_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.account_id := account_id;
self.program_id := program_id;
self.start_date := start_date;
self.referred_by_member_number := referred_by_member_number;
self.ProgramLevel := ProgramLevel;
self.PointsBalance := PointsBalance;
self.Awards := Awards;
self.LoyaltyMember_TBL := LoyaltyMember_TBL;
self.LoyaltyCard_TBL := LoyaltyCard_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ProgramLevel_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProgramLevel_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  level_id number(2),
  level_name varchar2(32),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProgramLevel_REC"
(
  rib_oid number
, level_id number
) return self as result
,constructor function "RIB_ProgramLevel_REC"
(
  rib_oid number
, level_id number
, level_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProgramLevel_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'level_id') := level_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'level_name') := level_name;
END appendNodeValues;
constructor function "RIB_ProgramLevel_REC"
(
  rib_oid number
, level_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.level_id := level_id;
RETURN;
end;
constructor function "RIB_ProgramLevel_REC"
(
  rib_oid number
, level_id number
, level_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.level_id := level_id;
self.level_name := level_name;
RETURN;
end;
END;
/
DROP TYPE "RIB_Coupon_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_Coupon_TBL" AS TABLE OF "RIB_Coupon_REC";
/
DROP TYPE "RIB_Awards_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Awards_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  Coupon_TBL "RIB_Coupon_TBL",   -- Size of "RIB_Coupon_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Awards_REC"
(
  rib_oid number
, Coupon_TBL "RIB_Coupon_TBL"  -- Size of "RIB_Coupon_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Awards_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF Coupon_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'Coupon_TBL.';
    FOR INDX IN Coupon_TBL.FIRST()..Coupon_TBL.LAST() LOOP
      Coupon_TBL(indx).appendNodeValues( i_prefix||indx||'Coupon_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_Awards_REC"
(
  rib_oid number
, Coupon_TBL "RIB_Coupon_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.Coupon_TBL := Coupon_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_Coupon_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Coupon_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id varchar2(32),
  amount number(20,4),
  currency_code varchar2(3),
  expiration_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Coupon_REC"
(
  rib_oid number
, id varchar2
, amount number
) return self as result
,constructor function "RIB_Coupon_REC"
(
  rib_oid number
, id varchar2
, amount number
, currency_code varchar2
) return self as result
,constructor function "RIB_Coupon_REC"
(
  rib_oid number
, id varchar2
, amount number
, currency_code varchar2
, expiration_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Coupon_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount') := amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration_date') := expiration_date;
END appendNodeValues;
constructor function "RIB_Coupon_REC"
(
  rib_oid number
, id varchar2
, amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.amount := amount;
RETURN;
end;
constructor function "RIB_Coupon_REC"
(
  rib_oid number
, id varchar2
, amount number
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.amount := amount;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_Coupon_REC"
(
  rib_oid number
, id varchar2
, amount number
, currency_code varchar2
, expiration_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.amount := amount;
self.currency_code := currency_code;
self.expiration_date := expiration_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_PointsBalance_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PointsBalance_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  earned number(9),
  escrow number(9),
  bonus number(9),
  ytd number(9),
  ltd number(9),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
) return self as result
,constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
) return self as result
,constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
) return self as result
,constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
, ytd number
) return self as result
,constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
, ytd number
, ltd number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PointsBalance_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'earned') := earned;
  rib_obj_util.g_RIB_element_values(i_prefix||'escrow') := escrow;
  rib_obj_util.g_RIB_element_values(i_prefix||'bonus') := bonus;
  rib_obj_util.g_RIB_element_values(i_prefix||'ytd') := ytd;
  rib_obj_util.g_RIB_element_values(i_prefix||'ltd') := ltd;
END appendNodeValues;
constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
RETURN;
end;
constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
self.escrow := escrow;
RETURN;
end;
constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
self.escrow := escrow;
self.bonus := bonus;
RETURN;
end;
constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
, ytd number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
self.escrow := escrow;
self.bonus := bonus;
self.ytd := ytd;
RETURN;
end;
constructor function "RIB_PointsBalance_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
, ytd number
, ltd number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
self.escrow := escrow;
self.bonus := bonus;
self.ytd := ytd;
self.ltd := ltd;
RETURN;
end;
END;
/
DROP TYPE "RIB_LoyaltyMember_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LoyaltyMember_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustomerRef "RIB_CustomerRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LoyaltyMember_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LoyaltyMember_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_LoyaltyMember_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_LoyaltyCard_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LoyaltyCard_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyAcctDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  card_prefix varchar2(5),
  card_series_seq varchar2(2),
  card_number varchar2(32),
  serial_number varchar2(32),
  auth_data varchar2(64),
  LoyaltyMember "RIB_LoyaltyMember_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
) return self as result
,constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
) return self as result
,constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
) return self as result
,constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
, serial_number varchar2
) return self as result
,constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
, serial_number varchar2
, auth_data varchar2
) return self as result
,constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
, serial_number varchar2
, auth_data varchar2
, LoyaltyMember "RIB_LoyaltyMember_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LoyaltyCard_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyAcctDesc') := "ns_name_LoyAcctDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'card_prefix') := card_prefix;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_series_seq') := card_series_seq;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_number') := card_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_number') := serial_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'auth_data') := auth_data;
  l_new_pre :=i_prefix||'LoyaltyMember.';
  LoyaltyMember.appendNodeValues( i_prefix||'LoyaltyMember');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_prefix := card_prefix;
RETURN;
end;
constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_prefix := card_prefix;
self.card_series_seq := card_series_seq;
RETURN;
end;
constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_prefix := card_prefix;
self.card_series_seq := card_series_seq;
self.card_number := card_number;
RETURN;
end;
constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
, serial_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_prefix := card_prefix;
self.card_series_seq := card_series_seq;
self.card_number := card_number;
self.serial_number := serial_number;
RETURN;
end;
constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
, serial_number varchar2
, auth_data varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_prefix := card_prefix;
self.card_series_seq := card_series_seq;
self.card_number := card_number;
self.serial_number := serial_number;
self.auth_data := auth_data;
RETURN;
end;
constructor function "RIB_LoyaltyCard_REC"
(
  rib_oid number
, card_prefix varchar2
, card_series_seq varchar2
, card_number varchar2
, serial_number varchar2
, auth_data varchar2
, LoyaltyMember "RIB_LoyaltyMember_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_prefix := card_prefix;
self.card_series_seq := card_series_seq;
self.card_number := card_number;
self.serial_number := serial_number;
self.auth_data := auth_data;
self.LoyaltyMember := LoyaltyMember;
RETURN;
end;
END;
/
