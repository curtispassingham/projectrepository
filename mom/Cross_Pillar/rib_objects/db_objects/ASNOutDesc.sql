DROP TYPE "RIB_ASNOutItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutItem_REC";
/
DROP TYPE "RIB_ASNOutUIN_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutUIN_REC";
/
DROP TYPE "RIB_ASNOutCtn_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutCtn_REC";
/
DROP TYPE "RIB_ASNOutDistro_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutDistro_REC";
/
DROP TYPE "RIB_ASNOutDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutDesc_REC";
/
DROP TYPE "RIB_ASNOutUIN_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutUIN_TBL" AS TABLE OF "RIB_ASNOutUIN_REC";
/
DROP TYPE "RIB_ASNOutItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ASNOutItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ASNOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  unit_qty number(12,4),
  gross_cost number(20,4),
  priority_level number(1),
  order_line_nbr number(4),
  lot_nbr varchar2(20),
  final_location varchar2(10),
  from_disposition varchar2(4),
  to_disposition varchar2(4),
  voucher_number varchar2(16),
  voucher_expiration_date date,
  container_qty number(6),
  comments varchar2(2000),
  unit_cost number(20,4),
  base_cost number(20,4),
  weight number(12,4),
  weight_uom varchar2(4),
  ASNOutUIN_TBL "RIB_ASNOutUIN_TBL",   -- Size of "RIB_ASNOutUIN_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
, weight number
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
, weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
, weight number
, weight_uom varchar2
, ASNOutUIN_TBL "RIB_ASNOutUIN_TBL"  -- Size of "RIB_ASNOutUIN_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ASNOutItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ASNOutDesc') := "ns_name_ASNOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'gross_cost') := gross_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority_level') := priority_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_nbr') := order_line_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'lot_nbr') := lot_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'final_location') := final_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_disposition') := from_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'voucher_number') := voucher_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'voucher_expiration_date') := voucher_expiration_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_qty') := container_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'base_cost') := base_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  IF ASNOutUIN_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ASNOutUIN_TBL.';
    FOR INDX IN ASNOutUIN_TBL.FIRST()..ASNOutUIN_TBL.LAST() LOOP
      ASNOutUIN_TBL(indx).appendNodeValues( i_prefix||indx||'ASNOutUIN_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
self.comments := comments;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
self.comments := comments;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
self.comments := comments;
self.unit_cost := unit_cost;
self.base_cost := base_cost;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
self.comments := comments;
self.unit_cost := unit_cost;
self.base_cost := base_cost;
self.weight := weight;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
self.comments := comments;
self.unit_cost := unit_cost;
self.base_cost := base_cost;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_ASNOutItem_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, gross_cost number
, priority_level number
, order_line_nbr number
, lot_nbr varchar2
, final_location varchar2
, from_disposition varchar2
, to_disposition varchar2
, voucher_number varchar2
, voucher_expiration_date date
, container_qty number
, comments varchar2
, unit_cost number
, base_cost number
, weight number
, weight_uom varchar2
, ASNOutUIN_TBL "RIB_ASNOutUIN_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.gross_cost := gross_cost;
self.priority_level := priority_level;
self.order_line_nbr := order_line_nbr;
self.lot_nbr := lot_nbr;
self.final_location := final_location;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.voucher_number := voucher_number;
self.voucher_expiration_date := voucher_expiration_date;
self.container_qty := container_qty;
self.comments := comments;
self.unit_cost := unit_cost;
self.base_cost := base_cost;
self.weight := weight;
self.weight_uom := weight_uom;
self.ASNOutUIN_TBL := ASNOutUIN_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ASNOutUIN_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ASNOutUIN_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ASNOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ASNOutUIN_REC"
(
  rib_oid number
, uin varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ASNOutUIN_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ASNOutDesc') := "ns_name_ASNOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
END appendNodeValues;
constructor function "RIB_ASNOutUIN_REC"
(
  rib_oid number
, uin varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
RETURN;
end;
END;
/
DROP TYPE "RIB_ASNOutItem_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutItem_TBL" AS TABLE OF "RIB_ASNOutItem_REC";
/
DROP TYPE "RIB_ASNOutCtn_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ASNOutCtn_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ASNOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  final_location varchar2(10),
  container_id varchar2(20),
  container_weight number(12,4),
  container_length number(12,4),
  container_width number(12,4),
  container_height number(12,4),
  container_cube number(12,2),
  expedite_flag varchar2(1),
  in_store_date date,
  tracking_nbr varchar2(120),
  freight_charge number(20,4),
  master_container_id varchar2(20),
  ASNOutItem_TBL "RIB_ASNOutItem_TBL",   -- Size of "RIB_ASNOutItem_TBL" is unbounded
  comments varchar2(2000),
  weight number(12,4),
  weight_uom varchar2(4),
  carrier_shipment_nbr varchar2(120),
  original_item_id varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"  -- Size of "RIB_ASNOutItem_TBL" is unbounded
) return self as result
,constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"  -- Size of "RIB_ASNOutItem_TBL" is unbounded
, comments varchar2
) return self as result
,constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"  -- Size of "RIB_ASNOutItem_TBL" is unbounded
, comments varchar2
, weight number
) return self as result
,constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"  -- Size of "RIB_ASNOutItem_TBL" is unbounded
, comments varchar2
, weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"  -- Size of "RIB_ASNOutItem_TBL" is unbounded
, comments varchar2
, weight number
, weight_uom varchar2
, carrier_shipment_nbr varchar2
) return self as result
,constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"  -- Size of "RIB_ASNOutItem_TBL" is unbounded
, comments varchar2
, weight number
, weight_uom varchar2
, carrier_shipment_nbr varchar2
, original_item_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ASNOutCtn_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ASNOutDesc') := "ns_name_ASNOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'final_location') := final_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_weight') := container_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_length') := container_length;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_width') := container_width;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_height') := container_height;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_cube') := container_cube;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_flag') := expedite_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'in_store_date') := in_store_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_nbr') := tracking_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_charge') := freight_charge;
  rib_obj_util.g_RIB_element_values(i_prefix||'master_container_id') := master_container_id;
  l_new_pre :=i_prefix||'ASNOutItem_TBL.';
  FOR INDX IN ASNOutItem_TBL.FIRST()..ASNOutItem_TBL.LAST() LOOP
    ASNOutItem_TBL(indx).appendNodeValues( i_prefix||indx||'ASNOutItem_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_shipment_nbr') := carrier_shipment_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'original_item_id') := original_item_id;
END appendNodeValues;
constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.final_location := final_location;
self.container_id := container_id;
self.container_weight := container_weight;
self.container_length := container_length;
self.container_width := container_width;
self.container_height := container_height;
self.container_cube := container_cube;
self.expedite_flag := expedite_flag;
self.in_store_date := in_store_date;
self.tracking_nbr := tracking_nbr;
self.freight_charge := freight_charge;
self.master_container_id := master_container_id;
self.ASNOutItem_TBL := ASNOutItem_TBL;
RETURN;
end;
constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.final_location := final_location;
self.container_id := container_id;
self.container_weight := container_weight;
self.container_length := container_length;
self.container_width := container_width;
self.container_height := container_height;
self.container_cube := container_cube;
self.expedite_flag := expedite_flag;
self.in_store_date := in_store_date;
self.tracking_nbr := tracking_nbr;
self.freight_charge := freight_charge;
self.master_container_id := master_container_id;
self.ASNOutItem_TBL := ASNOutItem_TBL;
self.comments := comments;
RETURN;
end;
constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"
, comments varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.final_location := final_location;
self.container_id := container_id;
self.container_weight := container_weight;
self.container_length := container_length;
self.container_width := container_width;
self.container_height := container_height;
self.container_cube := container_cube;
self.expedite_flag := expedite_flag;
self.in_store_date := in_store_date;
self.tracking_nbr := tracking_nbr;
self.freight_charge := freight_charge;
self.master_container_id := master_container_id;
self.ASNOutItem_TBL := ASNOutItem_TBL;
self.comments := comments;
self.weight := weight;
RETURN;
end;
constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"
, comments varchar2
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.final_location := final_location;
self.container_id := container_id;
self.container_weight := container_weight;
self.container_length := container_length;
self.container_width := container_width;
self.container_height := container_height;
self.container_cube := container_cube;
self.expedite_flag := expedite_flag;
self.in_store_date := in_store_date;
self.tracking_nbr := tracking_nbr;
self.freight_charge := freight_charge;
self.master_container_id := master_container_id;
self.ASNOutItem_TBL := ASNOutItem_TBL;
self.comments := comments;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"
, comments varchar2
, weight number
, weight_uom varchar2
, carrier_shipment_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.final_location := final_location;
self.container_id := container_id;
self.container_weight := container_weight;
self.container_length := container_length;
self.container_width := container_width;
self.container_height := container_height;
self.container_cube := container_cube;
self.expedite_flag := expedite_flag;
self.in_store_date := in_store_date;
self.tracking_nbr := tracking_nbr;
self.freight_charge := freight_charge;
self.master_container_id := master_container_id;
self.ASNOutItem_TBL := ASNOutItem_TBL;
self.comments := comments;
self.weight := weight;
self.weight_uom := weight_uom;
self.carrier_shipment_nbr := carrier_shipment_nbr;
RETURN;
end;
constructor function "RIB_ASNOutCtn_REC"
(
  rib_oid number
, final_location varchar2
, container_id varchar2
, container_weight number
, container_length number
, container_width number
, container_height number
, container_cube number
, expedite_flag varchar2
, in_store_date date
, tracking_nbr varchar2
, freight_charge number
, master_container_id varchar2
, ASNOutItem_TBL "RIB_ASNOutItem_TBL"
, comments varchar2
, weight number
, weight_uom varchar2
, carrier_shipment_nbr varchar2
, original_item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.final_location := final_location;
self.container_id := container_id;
self.container_weight := container_weight;
self.container_length := container_length;
self.container_width := container_width;
self.container_height := container_height;
self.container_cube := container_cube;
self.expedite_flag := expedite_flag;
self.in_store_date := in_store_date;
self.tracking_nbr := tracking_nbr;
self.freight_charge := freight_charge;
self.master_container_id := master_container_id;
self.ASNOutItem_TBL := ASNOutItem_TBL;
self.comments := comments;
self.weight := weight;
self.weight_uom := weight_uom;
self.carrier_shipment_nbr := carrier_shipment_nbr;
self.original_item_id := original_item_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_ASNOutCtn_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutCtn_TBL" AS TABLE OF "RIB_ASNOutCtn_REC";
/
DROP TYPE "RIB_ASNOutDistro_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ASNOutDistro_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ASNOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  distro_nbr varchar2(12),
  distro_doc_type varchar2(1),
  cust_order_nbr varchar2(48),
  fulfill_order_nbr varchar2(48),
  consumer_direct varchar2(1),
  ASNOutCtn_TBL "RIB_ASNOutCtn_TBL",   -- Size of "RIB_ASNOutCtn_TBL" is unbounded
  comments varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ASNOutDistro_REC"
(
  rib_oid number
, distro_nbr varchar2
, distro_doc_type varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, consumer_direct varchar2
, ASNOutCtn_TBL "RIB_ASNOutCtn_TBL"  -- Size of "RIB_ASNOutCtn_TBL" is unbounded
) return self as result
,constructor function "RIB_ASNOutDistro_REC"
(
  rib_oid number
, distro_nbr varchar2
, distro_doc_type varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, consumer_direct varchar2
, ASNOutCtn_TBL "RIB_ASNOutCtn_TBL"  -- Size of "RIB_ASNOutCtn_TBL" is unbounded
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ASNOutDistro_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ASNOutDesc') := "ns_name_ASNOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_doc_type') := distro_doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_nbr') := fulfill_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_direct') := consumer_direct;
  l_new_pre :=i_prefix||'ASNOutCtn_TBL.';
  FOR INDX IN ASNOutCtn_TBL.FIRST()..ASNOutCtn_TBL.LAST() LOOP
    ASNOutCtn_TBL(indx).appendNodeValues( i_prefix||indx||'ASNOutCtn_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_ASNOutDistro_REC"
(
  rib_oid number
, distro_nbr varchar2
, distro_doc_type varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, consumer_direct varchar2
, ASNOutCtn_TBL "RIB_ASNOutCtn_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.consumer_direct := consumer_direct;
self.ASNOutCtn_TBL := ASNOutCtn_TBL;
RETURN;
end;
constructor function "RIB_ASNOutDistro_REC"
(
  rib_oid number
, distro_nbr varchar2
, distro_doc_type varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, consumer_direct varchar2
, ASNOutCtn_TBL "RIB_ASNOutCtn_TBL"
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.distro_doc_type := distro_doc_type;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.consumer_direct := consumer_direct;
self.ASNOutCtn_TBL := ASNOutCtn_TBL;
self.comments := comments;
RETURN;
end;
END;
/
DROP TYPE "RIB_ASNOutDistro_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ASNOutDistro_TBL" AS TABLE OF "RIB_ASNOutDistro_REC";
/
DROP TYPE "RIB_ASNOutDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ASNOutDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ASNOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  schedule_nbr number(8),
  auto_receive varchar2(1),
  to_location varchar2(10),
  to_loc_type varchar2(1),
  to_store_type varchar2(1),
  to_stockholding_ind varchar2(1),
  from_location varchar2(10),
  from_loc_type varchar2(1),
  from_store_type varchar2(1),
  from_stockholding_ind varchar2(1),
  asn_nbr varchar2(30),
  asn_type varchar2(1),
  container_qty number(6),
  bol_nbr varchar2(17),
  shipment_date date,
  est_arr_date date,
  ship_address1 varchar2(240),
  ship_address2 varchar2(240),
  ship_address3 varchar2(240),
  ship_address4 varchar2(240),
  ship_address5 varchar2(240),
  ship_city varchar2(120),
  ship_state varchar2(3),
  ship_zip varchar2(30),
  ship_country_id varchar2(3),
  trailer_nbr varchar2(12),
  seal_nbr varchar2(12),
  transshipment_nbr varchar2(30),
  ASNOutDistro_TBL "RIB_ASNOutDistro_TBL",   -- Size of "RIB_ASNOutDistro_TBL" is unbounded
  comments varchar2(2000),
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"  -- Size of "RIB_ASNOutDistro_TBL" is unbounded
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"  -- Size of "RIB_ASNOutDistro_TBL" is unbounded
, comments varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"  -- Size of "RIB_ASNOutDistro_TBL" is unbounded
, comments varchar2
, carrier_code varchar2
) return self as result
,constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"  -- Size of "RIB_ASNOutDistro_TBL" is unbounded
, comments varchar2
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ASNOutDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ASNOutDesc') := "ns_name_ASNOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_nbr') := schedule_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_receive') := auto_receive;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_store_type') := to_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_stockholding_ind') := to_stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc_type') := from_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_store_type') := from_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_stockholding_ind') := from_stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_type') := asn_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_qty') := container_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_nbr') := bol_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_date') := shipment_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'est_arr_date') := est_arr_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address1') := ship_address1;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address2') := ship_address2;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address3') := ship_address3;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address4') := ship_address4;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address5') := ship_address5;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_city') := ship_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_state') := ship_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_zip') := ship_zip;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_country_id') := ship_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'trailer_nbr') := trailer_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'seal_nbr') := seal_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'transshipment_nbr') := transshipment_nbr;
  IF ASNOutDistro_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ASNOutDistro_TBL.';
    FOR INDX IN ASNOutDistro_TBL.FIRST()..ASNOutDistro_TBL.LAST() LOOP
      ASNOutDistro_TBL(indx).appendNodeValues( i_prefix||indx||'ASNOutDistro_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
END appendNodeValues;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
self.seal_nbr := seal_nbr;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
self.seal_nbr := seal_nbr;
self.transshipment_nbr := transshipment_nbr;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
self.seal_nbr := seal_nbr;
self.transshipment_nbr := transshipment_nbr;
self.ASNOutDistro_TBL := ASNOutDistro_TBL;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
self.seal_nbr := seal_nbr;
self.transshipment_nbr := transshipment_nbr;
self.ASNOutDistro_TBL := ASNOutDistro_TBL;
self.comments := comments;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"
, comments varchar2
, carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
self.seal_nbr := seal_nbr;
self.transshipment_nbr := transshipment_nbr;
self.ASNOutDistro_TBL := ASNOutDistro_TBL;
self.comments := comments;
self.carrier_code := carrier_code;
RETURN;
end;
constructor function "RIB_ASNOutDesc_REC"
(
  rib_oid number
, schedule_nbr number
, auto_receive varchar2
, to_location varchar2
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, from_location varchar2
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, asn_nbr varchar2
, asn_type varchar2
, container_qty number
, bol_nbr varchar2
, shipment_date date
, est_arr_date date
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, ship_address4 varchar2
, ship_address5 varchar2
, ship_city varchar2
, ship_state varchar2
, ship_zip varchar2
, ship_country_id varchar2
, trailer_nbr varchar2
, seal_nbr varchar2
, transshipment_nbr varchar2
, ASNOutDistro_TBL "RIB_ASNOutDistro_TBL"
, comments varchar2
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.auto_receive := auto_receive;
self.to_location := to_location;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.from_location := from_location;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.asn_nbr := asn_nbr;
self.asn_type := asn_type;
self.container_qty := container_qty;
self.bol_nbr := bol_nbr;
self.shipment_date := shipment_date;
self.est_arr_date := est_arr_date;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.ship_address4 := ship_address4;
self.ship_address5 := ship_address5;
self.ship_city := ship_city;
self.ship_state := ship_state;
self.ship_zip := ship_zip;
self.ship_country_id := ship_country_id;
self.trailer_nbr := trailer_nbr;
self.seal_nbr := seal_nbr;
self.transshipment_nbr := transshipment_nbr;
self.ASNOutDistro_TBL := ASNOutDistro_TBL;
self.comments := comments;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
RETURN;
end;
END;
/
