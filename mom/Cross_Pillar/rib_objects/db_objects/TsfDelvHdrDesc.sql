DROP TYPE "RIB_TsfDelvHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvHdrDesc_REC";
/
DROP TYPE "RIB_TsfDelvHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(15),
  store_id number(10),
  source_type varchar2(10), -- source_type is enumeration field, valid values are [WAREHOUSE, FINISHER, STORE, UNKNOWN] (all lower-case)
  source_name varchar2(250),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, RECEIVED, CANCELED, UNKNOWN] (all lower-case)
  asn varchar2(128),
  expected_date date,
  received_date date,
  received_user varchar2(128),
  num_of_cartons number(10),
  total_skus number(10),
  cartons_missing varchar2(5), --cartons_missing is boolean field, valid values are true,false (all lower-case) 
  customer_order_related varchar2(5), --customer_order_related is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvHdrDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, source_type varchar2
, source_name varchar2
, status varchar2
, asn varchar2
, expected_date date
, received_date date
, received_user varchar2
, num_of_cartons number
, total_skus number
, cartons_missing varchar2  --cartons_missing is boolean field, valid values are true,false (all lower-case)
, customer_order_related varchar2  --customer_order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvHdrDesc') := "ns_name_TsfDelvHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_type') := source_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_name') := source_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_date') := expected_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_date') := received_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_user') := received_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'num_of_cartons') := num_of_cartons;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_skus') := total_skus;
  rib_obj_util.g_RIB_element_values(i_prefix||'cartons_missing') := cartons_missing;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_related') := customer_order_related;
END appendNodeValues;
constructor function "RIB_TsfDelvHdrDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, source_type varchar2
, source_name varchar2
, status varchar2
, asn varchar2
, expected_date date
, received_date date
, received_user varchar2
, num_of_cartons number
, total_skus number
, cartons_missing varchar2
, customer_order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.source_type := source_type;
self.source_name := source_name;
self.status := status;
self.asn := asn;
self.expected_date := expected_date;
self.received_date := received_date;
self.received_user := received_user;
self.num_of_cartons := num_of_cartons;
self.total_skus := total_skus;
self.cartons_missing := cartons_missing;
self.customer_order_related := customer_order_related;
RETURN;
end;
END;
/
