@@CustOrdItmPkColVo.sql;
/
@@CustOrdDelDesc.sql;
/
@@PaymentColDesc.sql;
/
DROP TYPE "RIB_CustOrderPicVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrderPicVo_REC";
/
DROP TYPE "RIB_CustOrderPicVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrderPicVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderPicVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_id varchar2(48),
  currency_code varchar2(3),
  completed_amount number(20,4),
  cancelled_amount number(20,4),
  repriced_amount number(20,4),
  completed_new_amount number(20,4),
  completed_discount_amount number(20,4),
  cancelled_discount_amount number(20,4),
  repriced_discount_amount number(20,4),
  completed_new_discount_amount number(20,4),
  completed_tax_amount number(20,4),
  cancelled_tax_amount number(20,4),
  repriced_tax_amount number(20,4),
  completed_new_tax_amount number(20,4),
  completed_inc_tax_amount number(20,4),
  cancelled_inc_tax_amount number(20,4),
  repriced_inc_tax_amount number(20,4),
  completed_new_inc_tax_amount number(20,4),
  paid_amount number(20,4),
  rounding_adjustment number(20,4),
  refund_amount_offset_by_sale number(20,4),
  update_timestamp date,
  CustOrdItmPkColVo "RIB_CustOrdItmPkColVo_REC",
  CustOrdDelDesc "RIB_CustOrdDelDesc_REC",
  PaymentColDesc "RIB_PaymentColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrderPicVo_REC"
(
  rib_oid number
, customer_order_id varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, update_timestamp date
, CustOrdItmPkColVo "RIB_CustOrdItmPkColVo_REC"
, CustOrdDelDesc "RIB_CustOrdDelDesc_REC"
) return self as result
,constructor function "RIB_CustOrderPicVo_REC"
(
  rib_oid number
, customer_order_id varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, update_timestamp date
, CustOrdItmPkColVo "RIB_CustOrdItmPkColVo_REC"
, CustOrdDelDesc "RIB_CustOrdDelDesc_REC"
, PaymentColDesc "RIB_PaymentColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrderPicVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderPicVo') := "ns_name_CustOrderPicVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_amount') := completed_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_amount') := cancelled_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_amount') := repriced_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_amount') := completed_new_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_discount_amount') := completed_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_discount_amount') := cancelled_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_discount_amount') := repriced_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_discount_amount') := completed_new_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_tax_amount') := completed_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_tax_amount') := cancelled_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_tax_amount') := repriced_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_tax_amount') := completed_new_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_inc_tax_amount') := completed_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_inc_tax_amount') := cancelled_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'repriced_inc_tax_amount') := repriced_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_new_inc_tax_amount') := completed_new_inc_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'paid_amount') := paid_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'rounding_adjustment') := rounding_adjustment;
  rib_obj_util.g_RIB_element_values(i_prefix||'refund_amount_offset_by_sale') := refund_amount_offset_by_sale;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_timestamp') := update_timestamp;
  l_new_pre :=i_prefix||'CustOrdItmPkColVo.';
  CustOrdItmPkColVo.appendNodeValues( i_prefix||'CustOrdItmPkColVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustOrdDelDesc.';
  CustOrdDelDesc.appendNodeValues( i_prefix||'CustOrdDelDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'PaymentColDesc.';
  PaymentColDesc.appendNodeValues( i_prefix||'PaymentColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CustOrderPicVo_REC"
(
  rib_oid number
, customer_order_id varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, update_timestamp date
, CustOrdItmPkColVo "RIB_CustOrdItmPkColVo_REC"
, CustOrdDelDesc "RIB_CustOrdDelDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.update_timestamp := update_timestamp;
self.CustOrdItmPkColVo := CustOrdItmPkColVo;
self.CustOrdDelDesc := CustOrdDelDesc;
RETURN;
end;
constructor function "RIB_CustOrderPicVo_REC"
(
  rib_oid number
, customer_order_id varchar2
, currency_code varchar2
, completed_amount number
, cancelled_amount number
, repriced_amount number
, completed_new_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, repriced_discount_amount number
, completed_new_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, repriced_tax_amount number
, completed_new_tax_amount number
, completed_inc_tax_amount number
, cancelled_inc_tax_amount number
, repriced_inc_tax_amount number
, completed_new_inc_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, update_timestamp date
, CustOrdItmPkColVo "RIB_CustOrdItmPkColVo_REC"
, CustOrdDelDesc "RIB_CustOrdDelDesc_REC"
, PaymentColDesc "RIB_PaymentColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.currency_code := currency_code;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.repriced_amount := repriced_amount;
self.completed_new_amount := completed_new_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.repriced_discount_amount := repriced_discount_amount;
self.completed_new_discount_amount := completed_new_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.repriced_tax_amount := repriced_tax_amount;
self.completed_new_tax_amount := completed_new_tax_amount;
self.completed_inc_tax_amount := completed_inc_tax_amount;
self.cancelled_inc_tax_amount := cancelled_inc_tax_amount;
self.repriced_inc_tax_amount := repriced_inc_tax_amount;
self.completed_new_inc_tax_amount := completed_new_inc_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.update_timestamp := update_timestamp;
self.CustOrdItmPkColVo := CustOrdItmPkColVo;
self.CustOrdDelDesc := CustOrdDelDesc;
self.PaymentColDesc := PaymentColDesc;
RETURN;
end;
END;
/
