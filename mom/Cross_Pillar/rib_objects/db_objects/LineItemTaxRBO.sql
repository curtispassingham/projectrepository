@@TaxDetRBO.sql;
/
@@InconclRuleRBO.sql;
/
@@PrdItemTaxRBO.sql;
/
@@SvcItemTaxRBO.sql;
/
@@NameValPairRBO.sql;
/
DROP TYPE "RIB_LineItemTaxRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LineItemTaxRBO_REC";
/
DROP TYPE "RIB_TaxDetRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxDetRBO_TBL" AS TABLE OF "RIB_TaxDetRBO_REC";
/
DROP TYPE "RIB_InconclRuleRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InconclRuleRBO_TBL" AS TABLE OF "RIB_InconclRuleRBO_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_LineItemTaxRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LineItemTaxRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LineItemTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  document_line_id varchar2(25),
  item_id varchar2(25),
  pack_item_id varchar2(25),
  item_tran_code varchar2(25),
  taxed_quantity number(12,4),
  taxed_quantity_uom varchar2(4),
  total_cost number(20,4),
  deduced_fiscal_code_opr varchar2(12),
  icms_cst_code varchar2(6),
  pis_cst_code varchar2(6),
  cofins_cst_code varchar2(6),
  deduce_icms_cst_code varchar2(1),
  deduce_pis_cst_code varchar2(1),
  deduce_cofins_cst_code varchar2(1),
  recoverable_icmsst number(20,4),
  total_cost_with_icms number(20,4),
  unit_cost_with_icms number(20,4),
  recoverable_base_icmsst number(20,4),
  unit_cost number(20,4),
  dim_object varchar2(6),
  length number(12,4),
  width number(12,4),
  lwh_uom varchar2(4),
  weight number(12,4),
  net_weight number(12,4),
  weight_uom varchar2(4),
  liquid_volume number(12,4),
  liquid_volume_uom varchar2(4),
  TaxDetRBO_TBL "RIB_TaxDetRBO_TBL",   -- Size of "RIB_TaxDetRBO_TBL" is unbounded
  InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL",   -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
  PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC",
  SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC",
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  ipi_cst_code varchar2(6),
  ipi_clenq varchar2(25),
  ent_pis_cst_code varchar2(6),
  ent_cofins_cst_code varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, ipi_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, ipi_cst_code varchar2
, ipi_clenq varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, ipi_cst_code varchar2
, ipi_clenq varchar2
, ent_pis_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"  -- Size of "RIB_TaxDetRBO_TBL" is unbounded
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"  -- Size of "RIB_InconclRuleRBO_TBL" is unbounded
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, ipi_cst_code varchar2
, ipi_clenq varchar2
, ent_pis_cst_code varchar2
, ent_cofins_cst_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LineItemTaxRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LineItemTaxRBO') := "ns_name_LineItemTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'document_line_id') := document_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_item_id') := pack_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_tran_code') := item_tran_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxed_quantity') := taxed_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxed_quantity_uom') := taxed_quantity_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cost') := total_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduced_fiscal_code_opr') := deduced_fiscal_code_opr;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_cst_code') := icms_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_cst_code') := pis_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_cst_code') := cofins_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_icms_cst_code') := deduce_icms_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_pis_cst_code') := deduce_pis_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_cofins_cst_code') := deduce_cofins_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'recoverable_icmsst') := recoverable_icmsst;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cost_with_icms') := total_cost_with_icms;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost_with_icms') := unit_cost_with_icms;
  rib_obj_util.g_RIB_element_values(i_prefix||'recoverable_base_icmsst') := recoverable_base_icmsst;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'dim_object') := dim_object;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'lwh_uom') := lwh_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'net_weight') := net_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume') := liquid_volume;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume_uom') := liquid_volume_uom;
  IF TaxDetRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TaxDetRBO_TBL.';
    FOR INDX IN TaxDetRBO_TBL.FIRST()..TaxDetRBO_TBL.LAST() LOOP
      TaxDetRBO_TBL(indx).appendNodeValues( i_prefix||indx||'TaxDetRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF InconclRuleRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InconclRuleRBO_TBL.';
    FOR INDX IN InconclRuleRBO_TBL.FIRST()..InconclRuleRBO_TBL.LAST() LOOP
      InconclRuleRBO_TBL(indx).appendNodeValues( i_prefix||indx||'InconclRuleRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'PrdItemTaxRBO.';
  PrdItemTaxRBO.appendNodeValues( i_prefix||'PrdItemTaxRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'SvcItemTaxRBO.';
  SvcItemTaxRBO.appendNodeValues( i_prefix||'SvcItemTaxRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_cst_code') := ipi_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_clenq') := ipi_clenq;
  rib_obj_util.g_RIB_element_values(i_prefix||'ent_pis_cst_code') := ent_pis_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ent_cofins_cst_code') := ent_cofins_cst_code;
END appendNodeValues;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
self.SvcItemTaxRBO := SvcItemTaxRBO;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
self.SvcItemTaxRBO := SvcItemTaxRBO;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, ipi_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
self.SvcItemTaxRBO := SvcItemTaxRBO;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.ipi_cst_code := ipi_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, ipi_cst_code varchar2
, ipi_clenq varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
self.SvcItemTaxRBO := SvcItemTaxRBO;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, ipi_cst_code varchar2
, ipi_clenq varchar2
, ent_pis_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
self.SvcItemTaxRBO := SvcItemTaxRBO;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.ent_pis_cst_code := ent_pis_cst_code;
RETURN;
end;
constructor function "RIB_LineItemTaxRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, pack_item_id varchar2
, item_tran_code varchar2
, taxed_quantity number
, taxed_quantity_uom varchar2
, total_cost number
, deduced_fiscal_code_opr varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, total_cost_with_icms number
, unit_cost_with_icms number
, recoverable_base_icmsst number
, unit_cost number
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, TaxDetRBO_TBL "RIB_TaxDetRBO_TBL"
, InconclRuleRBO_TBL "RIB_InconclRuleRBO_TBL"
, PrdItemTaxRBO "RIB_PrdItemTaxRBO_REC"
, SvcItemTaxRBO "RIB_SvcItemTaxRBO_REC"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, ipi_cst_code varchar2
, ipi_clenq varchar2
, ent_pis_cst_code varchar2
, ent_cofins_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.pack_item_id := pack_item_id;
self.item_tran_code := item_tran_code;
self.taxed_quantity := taxed_quantity;
self.taxed_quantity_uom := taxed_quantity_uom;
self.total_cost := total_cost;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.total_cost_with_icms := total_cost_with_icms;
self.unit_cost_with_icms := unit_cost_with_icms;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.unit_cost := unit_cost;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.TaxDetRBO_TBL := TaxDetRBO_TBL;
self.InconclRuleRBO_TBL := InconclRuleRBO_TBL;
self.PrdItemTaxRBO := PrdItemTaxRBO;
self.SvcItemTaxRBO := SvcItemTaxRBO;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.ent_pis_cst_code := ent_pis_cst_code;
self.ent_cofins_cst_code := ent_cofins_cst_code;
RETURN;
end;
END;
/
