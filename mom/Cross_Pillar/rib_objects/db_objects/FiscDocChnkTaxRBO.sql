@@FiscDocTaxRBO.sql;
/
DROP TYPE "RIB_FiscDocChnkTaxRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscDocChnkTaxRBO_REC";
/
DROP TYPE "RIB_FiscDocTaxRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscDocTaxRBO_TBL" AS TABLE OF "RIB_FiscDocTaxRBO_REC";
/
DROP TYPE "RIB_FiscDocChnkTaxRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscDocChnkTaxRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocChnkTaxRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  chunk_id varchar2(2),
  FiscDocTaxRBO_TBL "RIB_FiscDocTaxRBO_TBL",   -- Size of "RIB_FiscDocTaxRBO_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscDocChnkTaxRBO_REC"
(
  rib_oid number
, chunk_id varchar2
, FiscDocTaxRBO_TBL "RIB_FiscDocTaxRBO_TBL"  -- Size of "RIB_FiscDocTaxRBO_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscDocChnkTaxRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocChnkTaxRBO') := "ns_name_FiscDocChnkTaxRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'chunk_id') := chunk_id;
  l_new_pre :=i_prefix||'FiscDocTaxRBO_TBL.';
  FOR INDX IN FiscDocTaxRBO_TBL.FIRST()..FiscDocTaxRBO_TBL.LAST() LOOP
    FiscDocTaxRBO_TBL(indx).appendNodeValues( i_prefix||indx||'FiscDocTaxRBO_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_FiscDocChnkTaxRBO_REC"
(
  rib_oid number
, chunk_id varchar2
, FiscDocTaxRBO_TBL "RIB_FiscDocTaxRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.chunk_id := chunk_id;
self.FiscDocTaxRBO_TBL := FiscDocTaxRBO_TBL;
RETURN;
end;
END;
/
