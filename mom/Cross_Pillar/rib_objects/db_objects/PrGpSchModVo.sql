DROP TYPE "RIB_PrGpSchModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrGpSchModVo_REC";
/
DROP TYPE "RIB_ScheduleMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ScheduleMod_REC";
/
DROP TYPE "RIB_DayOfWeekMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DayOfWeekMod_REC";
/
DROP TYPE "RIB_PrGpSchModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrGpSchModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrGpSchModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  schedule_id number(8),
  description varchar2(250),
  status varchar2(20), -- status is enumeration field, valid values are [OPEN, CLOSED, UNKNOWN, NO_VALUE] (all lower-case)
  group_id number(12),
  group_description varchar2(100),
  group_type varchar2(24), -- group_type is enumeration field, valid values are [STOCK_COUNT_UNIT, STOCK_COUNT_UNIT_AMOUNT, STOCK_COUNT_PROBLEM_LINE, STOCK_COUNT_WASTAGE, STOCK_COUNT_RMS_SYNC, SHELF_REPLENISHMENT, ITEM_REQUEST, AUTO_TICKET_PRINT, UNKNOWN, NO_VALUE] (all lower-case)
  store_id_col "RIB_store_id_col_TBL",   -- Size of "RIB_store_id_col_TBL" is 1000
  ScheduleMod "RIB_ScheduleMod_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrGpSchModVo_REC"
(
  rib_oid number
, schedule_id number
, description varchar2
, status varchar2
, group_id number
, group_description varchar2
, group_type varchar2
) return self as result
,constructor function "RIB_PrGpSchModVo_REC"
(
  rib_oid number
, schedule_id number
, description varchar2
, status varchar2
, group_id number
, group_description varchar2
, group_type varchar2
, store_id_col "RIB_store_id_col_TBL"  -- Size of "RIB_store_id_col_TBL" is 1000
) return self as result
,constructor function "RIB_PrGpSchModVo_REC"
(
  rib_oid number
, schedule_id number
, description varchar2
, status varchar2
, group_id number
, group_description varchar2
, group_type varchar2
, store_id_col "RIB_store_id_col_TBL"  -- Size of "RIB_store_id_col_TBL" is 1000
, ScheduleMod "RIB_ScheduleMod_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrGpSchModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrGpSchModVo') := "ns_name_PrGpSchModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_id') := schedule_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_id') := group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_description') := group_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_type') := group_type;
  IF store_id_col IS NOT NULL THEN
    FOR INDX IN store_id_col.FIRST()..store_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'store_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'store_id_col'||'.'):=store_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ScheduleMod.';
  ScheduleMod.appendNodeValues( i_prefix||'ScheduleMod');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_PrGpSchModVo_REC"
(
  rib_oid number
, schedule_id number
, description varchar2
, status varchar2
, group_id number
, group_description varchar2
, group_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_id := schedule_id;
self.description := description;
self.status := status;
self.group_id := group_id;
self.group_description := group_description;
self.group_type := group_type;
RETURN;
end;
constructor function "RIB_PrGpSchModVo_REC"
(
  rib_oid number
, schedule_id number
, description varchar2
, status varchar2
, group_id number
, group_description varchar2
, group_type varchar2
, store_id_col "RIB_store_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_id := schedule_id;
self.description := description;
self.status := status;
self.group_id := group_id;
self.group_description := group_description;
self.group_type := group_type;
self.store_id_col := store_id_col;
RETURN;
end;
constructor function "RIB_PrGpSchModVo_REC"
(
  rib_oid number
, schedule_id number
, description varchar2
, status varchar2
, group_id number
, group_description varchar2
, group_type varchar2
, store_id_col "RIB_store_id_col_TBL"
, ScheduleMod "RIB_ScheduleMod_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_id := schedule_id;
self.description := description;
self.status := status;
self.group_id := group_id;
self.group_description := group_description;
self.group_type := group_type;
self.store_id_col := store_id_col;
self.ScheduleMod := ScheduleMod;
RETURN;
end;
END;
/
DROP TYPE "RIB_store_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_store_id_col_TBL" AS TABLE OF number(10);
/
DROP TYPE "RIB_DayOfWeekMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DayOfWeekMod_TBL" AS TABLE OF "RIB_DayOfWeekMod_REC";
/
DROP TYPE "RIB_ScheduleMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ScheduleMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrGpSchModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  schedule_type varchar2(20), -- schedule_type is enumeration field, valid values are [DAILY, DAILY_BY_WEEKDAY, WEEKLY, MONTHLY_BY_DAY, MONTHLY_BY_WEEK, YEARLY_BY_DAY, YEARLY_BY_WEEK, UNKNOWN, NO_VALUE] (all lower-case)
  start_date date,
  end_date date,
  daily_frequency number(6),
  weekly_frequency number(4),
  monthly_frequency number(5),
  DayOfWeekMod_TBL "RIB_DayOfWeekMod_TBL",   -- Size of "RIB_DayOfWeekMod_TBL" is 7
  day_of_month number(1),
  week_of_month number(1),
  month_of_year varchar2(20), -- month_of_year is enumeration field, valid values are [JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER, UNKNOWN, NO_VALUE] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ScheduleMod_REC"
(
  rib_oid number
, schedule_type varchar2
, start_date date
, end_date date
, daily_frequency number
, weekly_frequency number
, monthly_frequency number
, DayOfWeekMod_TBL "RIB_DayOfWeekMod_TBL"  -- Size of "RIB_DayOfWeekMod_TBL" is 7
, day_of_month number
, week_of_month number
, month_of_year varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ScheduleMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrGpSchModVo') := "ns_name_PrGpSchModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_type') := schedule_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'daily_frequency') := daily_frequency;
  rib_obj_util.g_RIB_element_values(i_prefix||'weekly_frequency') := weekly_frequency;
  rib_obj_util.g_RIB_element_values(i_prefix||'monthly_frequency') := monthly_frequency;
  IF DayOfWeekMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DayOfWeekMod_TBL.';
    FOR INDX IN DayOfWeekMod_TBL.FIRST()..DayOfWeekMod_TBL.LAST() LOOP
      DayOfWeekMod_TBL(indx).appendNodeValues( i_prefix||indx||'DayOfWeekMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'day_of_month') := day_of_month;
  rib_obj_util.g_RIB_element_values(i_prefix||'week_of_month') := week_of_month;
  rib_obj_util.g_RIB_element_values(i_prefix||'month_of_year') := month_of_year;
END appendNodeValues;
constructor function "RIB_ScheduleMod_REC"
(
  rib_oid number
, schedule_type varchar2
, start_date date
, end_date date
, daily_frequency number
, weekly_frequency number
, monthly_frequency number
, DayOfWeekMod_TBL "RIB_DayOfWeekMod_TBL"
, day_of_month number
, week_of_month number
, month_of_year varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_type := schedule_type;
self.start_date := start_date;
self.end_date := end_date;
self.daily_frequency := daily_frequency;
self.weekly_frequency := weekly_frequency;
self.monthly_frequency := monthly_frequency;
self.DayOfWeekMod_TBL := DayOfWeekMod_TBL;
self.day_of_month := day_of_month;
self.week_of_month := week_of_month;
self.month_of_year := month_of_year;
RETURN;
end;
END;
/
DROP TYPE "RIB_DayOfWeekMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DayOfWeekMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrGpSchModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  day_of_week varchar2(20), -- day_of_week is enumeration field, valid values are [SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, UNKNOWN, NO_VALUE] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DayOfWeekMod_REC"
(
  rib_oid number
, day_of_week varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DayOfWeekMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrGpSchModVo') := "ns_name_PrGpSchModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'day_of_week') := day_of_week;
END appendNodeValues;
constructor function "RIB_DayOfWeekMod_REC"
(
  rib_oid number
, day_of_week varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.day_of_week := day_of_week;
RETURN;
end;
END;
/
