DROP TYPE "RIB_LocPOTsfDealsDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDealsDesc_REC";
/
DROP TYPE "RIB_LocPOTsfDealsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDealsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocPOTsfDealsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  deal_id number(10),
  deal_type varchar2(10),
  active_date date,
  close_date date,
  deal_class varchar2(6),
  limit_type varchar2(6),
  value_type varchar2(6),
  lower_limit number(20,4),
  upper_limit number(20,4),
  value number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocPOTsfDealsDesc_REC"
(
  rib_oid number
, deal_id number
, deal_type varchar2
, active_date date
, close_date date
, deal_class varchar2
, limit_type varchar2
, value_type varchar2
, lower_limit number
, upper_limit number
, value number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocPOTsfDealsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocPOTsfDealsDesc') := "ns_name_LocPOTsfDealsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'deal_id') := deal_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'deal_type') := deal_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'active_date') := active_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'close_date') := close_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'deal_class') := deal_class;
  rib_obj_util.g_RIB_element_values(i_prefix||'limit_type') := limit_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'value_type') := value_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'lower_limit') := lower_limit;
  rib_obj_util.g_RIB_element_values(i_prefix||'upper_limit') := upper_limit;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_LocPOTsfDealsDesc_REC"
(
  rib_oid number
, deal_id number
, deal_type varchar2
, active_date date
, close_date date
, deal_class varchar2
, limit_type varchar2
, value_type varchar2
, lower_limit number
, upper_limit number
, value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.deal_id := deal_id;
self.deal_type := deal_type;
self.active_date := active_date;
self.close_date := close_date;
self.deal_class := deal_class;
self.limit_type := limit_type;
self.value_type := value_type;
self.lower_limit := lower_limit;
self.upper_limit := upper_limit;
self.value := value;
RETURN;
end;
END;
/
