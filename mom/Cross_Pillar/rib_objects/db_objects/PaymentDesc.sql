@@ContactDesc.sql;
/
@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_PaymentDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PaymentDesc_REC";
/
DROP TYPE "RIB_CreditDebitTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CreditDebitTender_REC";
/
DROP TYPE "RIB_CheckTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CheckTender_REC";
/
DROP TYPE "RIB_CouponTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CouponTender_REC";
/
DROP TYPE "RIB_GiftCardTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftCardTender_REC";
/
DROP TYPE "RIB_GiftCertTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftCertTender_REC";
/
DROP TYPE "RIB_MailCheckTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_MailCheckTender_REC";
/
DROP TYPE "RIB_PurchaseOrdTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PurchaseOrdTender_REC";
/
DROP TYPE "RIB_StoreCreditTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StoreCreditTender_REC";
/
DROP TYPE "RIB_TravelCheckTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TravelCheckTender_REC";
/
DROP TYPE "RIB_PaymentDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PaymentDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(4),
  payment_type varchar2(13), -- payment_type is enumeration field, valid values are [CASH, CREDIT, CHECK, TRAVCHECK, GIFTCERT, MAILCHECK, DEBIT, COUPON, GIFTCARD, STORECREDIT, MALLCERT, PURCHASEORDER, MONEYORDER, ECHECK] (all lower-case)
  currency_code varchar2(3),
  amount number(20,4),
  alternate_currency_code varchar2(3),
  alternate_amount number(20,4),
  CreditDebitTender "RIB_CreditDebitTender_REC",
  CheckTender "RIB_CheckTender_REC",
  CouponTender "RIB_CouponTender_REC",
  GiftCardTender "RIB_GiftCardTender_REC",
  GiftCertTender "RIB_GiftCertTender_REC",
  MailCheckTender "RIB_MailCheckTender_REC",
  PurchaseOrdTender "RIB_PurchaseOrdTender_REC",
  StoreCreditTender "RIB_StoreCreditTender_REC",
  TravelCheckTender "RIB_TravelCheckTender_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
, PurchaseOrdTender "RIB_PurchaseOrdTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
, PurchaseOrdTender "RIB_PurchaseOrdTender_REC"
, StoreCreditTender "RIB_StoreCreditTender_REC"
) return self as result
,constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
, PurchaseOrdTender "RIB_PurchaseOrdTender_REC"
, StoreCreditTender "RIB_StoreCreditTender_REC"
, TravelCheckTender "RIB_TravelCheckTender_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PaymentDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_type') := payment_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount') := amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_currency_code') := alternate_currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_amount') := alternate_amount;
  l_new_pre :=i_prefix||'CreditDebitTender.';
  CreditDebitTender.appendNodeValues( i_prefix||'CreditDebitTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CheckTender.';
  CheckTender.appendNodeValues( i_prefix||'CheckTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CouponTender.';
  CouponTender.appendNodeValues( i_prefix||'CouponTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftCardTender.';
  GiftCardTender.appendNodeValues( i_prefix||'GiftCardTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftCertTender.';
  GiftCertTender.appendNodeValues( i_prefix||'GiftCertTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'MailCheckTender.';
  MailCheckTender.appendNodeValues( i_prefix||'MailCheckTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'PurchaseOrdTender.';
  PurchaseOrdTender.appendNodeValues( i_prefix||'PurchaseOrdTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'StoreCreditTender.';
  StoreCreditTender.appendNodeValues( i_prefix||'StoreCreditTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'TravelCheckTender.';
  TravelCheckTender.appendNodeValues( i_prefix||'TravelCheckTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
self.GiftCardTender := GiftCardTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
self.GiftCardTender := GiftCardTender;
self.GiftCertTender := GiftCertTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
self.GiftCardTender := GiftCardTender;
self.GiftCertTender := GiftCertTender;
self.MailCheckTender := MailCheckTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
, PurchaseOrdTender "RIB_PurchaseOrdTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
self.GiftCardTender := GiftCardTender;
self.GiftCertTender := GiftCertTender;
self.MailCheckTender := MailCheckTender;
self.PurchaseOrdTender := PurchaseOrdTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
, PurchaseOrdTender "RIB_PurchaseOrdTender_REC"
, StoreCreditTender "RIB_StoreCreditTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
self.GiftCardTender := GiftCardTender;
self.GiftCertTender := GiftCertTender;
self.MailCheckTender := MailCheckTender;
self.PurchaseOrdTender := PurchaseOrdTender;
self.StoreCreditTender := StoreCreditTender;
RETURN;
end;
constructor function "RIB_PaymentDesc_REC"
(
  rib_oid number
, seq_no number
, payment_type varchar2
, currency_code varchar2
, amount number
, alternate_currency_code varchar2
, alternate_amount number
, CreditDebitTender "RIB_CreditDebitTender_REC"
, CheckTender "RIB_CheckTender_REC"
, CouponTender "RIB_CouponTender_REC"
, GiftCardTender "RIB_GiftCardTender_REC"
, GiftCertTender "RIB_GiftCertTender_REC"
, MailCheckTender "RIB_MailCheckTender_REC"
, PurchaseOrdTender "RIB_PurchaseOrdTender_REC"
, StoreCreditTender "RIB_StoreCreditTender_REC"
, TravelCheckTender "RIB_TravelCheckTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.payment_type := payment_type;
self.currency_code := currency_code;
self.amount := amount;
self.alternate_currency_code := alternate_currency_code;
self.alternate_amount := alternate_amount;
self.CreditDebitTender := CreditDebitTender;
self.CheckTender := CheckTender;
self.CouponTender := CouponTender;
self.GiftCardTender := GiftCardTender;
self.GiftCertTender := GiftCertTender;
self.MailCheckTender := MailCheckTender;
self.PurchaseOrdTender := PurchaseOrdTender;
self.StoreCreditTender := StoreCreditTender;
self.TravelCheckTender := TravelCheckTender;
RETURN;
end;
END;
/
DROP TYPE "RIB_CreditDebitTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CreditDebitTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  masked_account_number varchar2(20),
  card_token varchar2(100),
  card_type varchar2(9), -- card_type is enumeration field, valid values are [VISA, MASTER, AMEX, DISCOVER, DINERS, HOUSECARD, JCB, GIFTCARD] (all lower-case)
  card_expiration_month varchar2(20),
  card_expiration_year varchar2(20),
  entry_method varchar2(7), -- entry_method is enumeration field, valid values are [MANUAL, SCAN, MICR, SWIPE, ICC, WAVE, AUTO, ICCFLBK] (all lower-case)
  authorization_code varchar2(40),
  authorization_datetime date,
  authorization_method varchar2(4), -- authorization_method is enumeration field, valid values are [AUTO, SYST, MANU] (all lower-case)
  personal_id_country varchar2(3),
  personal_id_state varchar2(3),
  personal_id_expiration_date date,
  prepaid_balance number(20,4),
  account_apr varchar2(8),
  account_apr_type varchar2(1),
  promotion_apr varchar2(8),
  promotion_apr_type varchar2(1),
  promotion_description varchar2(60),
  promotion_duration varchar2(40),
  settlement_data varchar2(16),
  signature_data "RIB_signature_data_TBL",   -- Size of "RIB_signature_data_TBL" is unbounded
  additional_security_info varchar2(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
, settlement_data varchar2
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
, settlement_data varchar2
, signature_data "RIB_signature_data_TBL"  -- Size of "RIB_signature_data_TBL" is unbounded
) return self as result
,constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
, settlement_data varchar2
, signature_data "RIB_signature_data_TBL"  -- Size of "RIB_signature_data_TBL" is unbounded
, additional_security_info varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CreditDebitTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'masked_account_number') := masked_account_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_token') := card_token;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_type') := card_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_expiration_month') := card_expiration_month;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_expiration_year') := card_expiration_year;
  rib_obj_util.g_RIB_element_values(i_prefix||'entry_method') := entry_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_datetime') := authorization_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_method') := authorization_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_country') := personal_id_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_state') := personal_id_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_expiration_date') := personal_id_expiration_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'prepaid_balance') := prepaid_balance;
  rib_obj_util.g_RIB_element_values(i_prefix||'account_apr') := account_apr;
  rib_obj_util.g_RIB_element_values(i_prefix||'account_apr_type') := account_apr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_apr') := promotion_apr;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_apr_type') := promotion_apr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_description') := promotion_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_duration') := promotion_duration;
  rib_obj_util.g_RIB_element_values(i_prefix||'settlement_data') := settlement_data;
  IF signature_data IS NOT NULL THEN
    FOR INDX IN signature_data.FIRST()..signature_data.LAST() LOOP
      l_new_pre :=i_prefix||indx||'signature_data'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'signature_data'||'.'):=signature_data(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'additional_security_info') := additional_security_info;
END appendNodeValues;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
self.promotion_apr_type := promotion_apr_type;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
self.promotion_apr_type := promotion_apr_type;
self.promotion_description := promotion_description;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
self.promotion_apr_type := promotion_apr_type;
self.promotion_description := promotion_description;
self.promotion_duration := promotion_duration;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
, settlement_data varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
self.promotion_apr_type := promotion_apr_type;
self.promotion_description := promotion_description;
self.promotion_duration := promotion_duration;
self.settlement_data := settlement_data;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
, settlement_data varchar2
, signature_data "RIB_signature_data_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
self.promotion_apr_type := promotion_apr_type;
self.promotion_description := promotion_description;
self.promotion_duration := promotion_duration;
self.settlement_data := settlement_data;
self.signature_data := signature_data;
RETURN;
end;
constructor function "RIB_CreditDebitTender_REC"
(
  rib_oid number
, masked_account_number varchar2
, card_token varchar2
, card_type varchar2
, card_expiration_month varchar2
, card_expiration_year varchar2
, entry_method varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, personal_id_country varchar2
, personal_id_state varchar2
, personal_id_expiration_date date
, prepaid_balance number
, account_apr varchar2
, account_apr_type varchar2
, promotion_apr varchar2
, promotion_apr_type varchar2
, promotion_description varchar2
, promotion_duration varchar2
, settlement_data varchar2
, signature_data "RIB_signature_data_TBL"
, additional_security_info varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.masked_account_number := masked_account_number;
self.card_token := card_token;
self.card_type := card_type;
self.card_expiration_month := card_expiration_month;
self.card_expiration_year := card_expiration_year;
self.entry_method := entry_method;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.personal_id_country := personal_id_country;
self.personal_id_state := personal_id_state;
self.personal_id_expiration_date := personal_id_expiration_date;
self.prepaid_balance := prepaid_balance;
self.account_apr := account_apr;
self.account_apr_type := account_apr_type;
self.promotion_apr := promotion_apr;
self.promotion_apr_type := promotion_apr_type;
self.promotion_description := promotion_description;
self.promotion_duration := promotion_duration;
self.settlement_data := settlement_data;
self.signature_data := signature_data;
self.additional_security_info := additional_security_info;
RETURN;
end;
END;
/
DROP TYPE "RIB_signature_data_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_signature_data_TBL" AS TABLE OF varchar2(5000);
/
DROP TYPE "RIB_CheckTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CheckTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bank_id varchar2(20),
  account_number varchar2(17),
  micr_number varchar2(80),
  check_number varchar2(10),
  authorization_code varchar2(20),
  authorization_method varchar2(4), -- authorization_method is enumeration field, valid values are [AUTO, SYST, MANU] (all lower-case)
  personal_id_number varchar2(20),
  personal_id_issuer varchar2(20),
  personal_id_issuer_co_code varchar2(22),
  personal_id_issuer_state_code varchar2(5),
  personal_id_swiped_flag varchar2(1), -- personal_id_swiped_flag is enumeration field, valid values are [Y, N] (all lower-case)
  customer_phone_number varchar2(30),
  entry_method varchar2(7), -- entry_method is enumeration field, valid values are [MANUAL, SCAN, MICR, SWIPE, ICC, WAVE, AUTO, ICCFLBK] (all lower-case)
  echeck_conversion_code varchar2(9), -- echeck_conversion_code is enumeration field, valid values are [CONV, VERIFY, GUARENTEE] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
, customer_phone_number varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
, customer_phone_number varchar2
, entry_method varchar2
) return self as result
,constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
, customer_phone_number varchar2
, entry_method varchar2
, echeck_conversion_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CheckTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'bank_id') := bank_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'account_number') := account_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'micr_number') := micr_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'check_number') := check_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_method') := authorization_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_number') := personal_id_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_issuer') := personal_id_issuer;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_issuer_co_code') := personal_id_issuer_co_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_issuer_state_code') := personal_id_issuer_state_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_swiped_flag') := personal_id_swiped_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_phone_number') := customer_phone_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'entry_method') := entry_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'echeck_conversion_code') := echeck_conversion_code;
END appendNodeValues;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
self.personal_id_issuer_co_code := personal_id_issuer_co_code;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
self.personal_id_issuer_co_code := personal_id_issuer_co_code;
self.personal_id_issuer_state_code := personal_id_issuer_state_code;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
self.personal_id_issuer_co_code := personal_id_issuer_co_code;
self.personal_id_issuer_state_code := personal_id_issuer_state_code;
self.personal_id_swiped_flag := personal_id_swiped_flag;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
, customer_phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
self.personal_id_issuer_co_code := personal_id_issuer_co_code;
self.personal_id_issuer_state_code := personal_id_issuer_state_code;
self.personal_id_swiped_flag := personal_id_swiped_flag;
self.customer_phone_number := customer_phone_number;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
, customer_phone_number varchar2
, entry_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
self.personal_id_issuer_co_code := personal_id_issuer_co_code;
self.personal_id_issuer_state_code := personal_id_issuer_state_code;
self.personal_id_swiped_flag := personal_id_swiped_flag;
self.customer_phone_number := customer_phone_number;
self.entry_method := entry_method;
RETURN;
end;
constructor function "RIB_CheckTender_REC"
(
  rib_oid number
, bank_id varchar2
, account_number varchar2
, micr_number varchar2
, check_number varchar2
, authorization_code varchar2
, authorization_method varchar2
, personal_id_number varchar2
, personal_id_issuer varchar2
, personal_id_issuer_co_code varchar2
, personal_id_issuer_state_code varchar2
, personal_id_swiped_flag varchar2
, customer_phone_number varchar2
, entry_method varchar2
, echeck_conversion_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bank_id := bank_id;
self.account_number := account_number;
self.micr_number := micr_number;
self.check_number := check_number;
self.authorization_code := authorization_code;
self.authorization_method := authorization_method;
self.personal_id_number := personal_id_number;
self.personal_id_issuer := personal_id_issuer;
self.personal_id_issuer_co_code := personal_id_issuer_co_code;
self.personal_id_issuer_state_code := personal_id_issuer_state_code;
self.personal_id_swiped_flag := personal_id_swiped_flag;
self.customer_phone_number := customer_phone_number;
self.entry_method := entry_method;
self.echeck_conversion_code := echeck_conversion_code;
RETURN;
end;
END;
/
DROP TYPE "RIB_CouponTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CouponTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  coupon_type varchar2(12), -- coupon_type is enumeration field, valid values are [LOYALTY, MANUFACTURER, STORE, ELECTRONIC] (all lower-case)
  entry_method varchar2(7), -- entry_method is enumeration field, valid values are [MANUAL, SCAN, MICR, SWIPE, ICC, WAVE, AUTO, ICCFLBK] (all lower-case)
  coupon_number varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CouponTender_REC"
(
  rib_oid number
, coupon_type varchar2
, entry_method varchar2
, coupon_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CouponTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'coupon_type') := coupon_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'entry_method') := entry_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'coupon_number') := coupon_number;
END appendNodeValues;
constructor function "RIB_CouponTender_REC"
(
  rib_oid number
, coupon_type varchar2
, entry_method varchar2
, coupon_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.coupon_type := coupon_type;
self.entry_method := entry_method;
self.coupon_number := coupon_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_GiftCardTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftCardTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  card_number varchar2(20),
  authorization_code varchar2(20),
  authorization_datetime date,
  authorization_method varchar2(4), -- authorization_method is enumeration field, valid values are [AUTO, SYST, MANU] (all lower-case)
  credit_flag varchar2(1), -- credit_flag is enumeration field, valid values are [Y, N] (all lower-case)
  entry_method varchar2(7), -- entry_method is enumeration field, valid values are [MANUAL, SCAN, MICR, SWIPE, ICC, WAVE, AUTO, ICCFLBK] (all lower-case)
  original_balance number(20,4),
  remaining_balance number(20,4),
  settlement_data varchar2(16),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
) return self as result
,constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
) return self as result
,constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
, original_balance number
) return self as result
,constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
, original_balance number
, remaining_balance number
) return self as result
,constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
, original_balance number
, remaining_balance number
, settlement_data varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftCardTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'card_number') := card_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_datetime') := authorization_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_method') := authorization_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'credit_flag') := credit_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'entry_method') := entry_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'original_balance') := original_balance;
  rib_obj_util.g_RIB_element_values(i_prefix||'remaining_balance') := remaining_balance;
  rib_obj_util.g_RIB_element_values(i_prefix||'settlement_data') := settlement_data;
END appendNodeValues;
constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.credit_flag := credit_flag;
RETURN;
end;
constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.credit_flag := credit_flag;
self.entry_method := entry_method;
RETURN;
end;
constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
, original_balance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.credit_flag := credit_flag;
self.entry_method := entry_method;
self.original_balance := original_balance;
RETURN;
end;
constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
, original_balance number
, remaining_balance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.credit_flag := credit_flag;
self.entry_method := entry_method;
self.original_balance := original_balance;
self.remaining_balance := remaining_balance;
RETURN;
end;
constructor function "RIB_GiftCardTender_REC"
(
  rib_oid number
, card_number varchar2
, authorization_code varchar2
, authorization_datetime date
, authorization_method varchar2
, credit_flag varchar2
, entry_method varchar2
, original_balance number
, remaining_balance number
, settlement_data varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_number := card_number;
self.authorization_code := authorization_code;
self.authorization_datetime := authorization_datetime;
self.authorization_method := authorization_method;
self.credit_flag := credit_flag;
self.entry_method := entry_method;
self.original_balance := original_balance;
self.remaining_balance := remaining_balance;
self.settlement_data := settlement_data;
RETURN;
end;
END;
/
DROP TYPE "RIB_GiftCertTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftCertTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  certificate_type varchar2(7), -- certificate_type is enumeration field, valid values are [STORE, CORP, FOREIGN] (all lower-case)
  issue_location_type varchar2(1), -- issue_location_type is enumeration field, valid values are [S] (all lower-case)
  issue_location_id number(10),
  serial_number varchar2(40),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftCertTender_REC"
(
  rib_oid number
, certificate_type varchar2
, issue_location_type varchar2
, issue_location_id number
, serial_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftCertTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'certificate_type') := certificate_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'issue_location_type') := issue_location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'issue_location_id') := issue_location_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_number') := serial_number;
END appendNodeValues;
constructor function "RIB_GiftCertTender_REC"
(
  rib_oid number
, certificate_type varchar2
, issue_location_type varchar2
, issue_location_id number
, serial_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.certificate_type := certificate_type;
self.issue_location_type := issue_location_type;
self.issue_location_id := issue_location_id;
self.serial_number := serial_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_MailCheckTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_MailCheckTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ContactDesc "RIB_ContactDesc_REC",
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  personal_id_type varchar2(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_MailCheckTender_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
,constructor function "RIB_MailCheckTender_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, personal_id_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_MailCheckTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_type') := personal_id_type;
END appendNodeValues;
constructor function "RIB_MailCheckTender_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
constructor function "RIB_MailCheckTender_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, personal_id_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.GeoAddrDesc := GeoAddrDesc;
self.personal_id_type := personal_id_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_PurchaseOrdTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PurchaseOrdTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  agent_name varchar2(120),
  purchase_order_number varchar2(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PurchaseOrdTender_REC"
(
  rib_oid number
, agent_name varchar2
, purchase_order_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PurchaseOrdTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'agent_name') := agent_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_order_number') := purchase_order_number;
END appendNodeValues;
constructor function "RIB_PurchaseOrdTender_REC"
(
  rib_oid number
, agent_name varchar2
, purchase_order_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.agent_name := agent_name;
self.purchase_order_number := purchase_order_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_StoreCreditTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StoreCreditTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  certificate_type varchar2(7), -- certificate_type is enumeration field, valid values are [STORE, CORP, FOREIGN] (all lower-case)
  first_name varchar2(120),
  last_name varchar2(120),
  personal_id_type varchar2(20),
  state varchar2(6), -- state is enumeration field, valid values are [ISSUE, REDEEM] (all lower-case)
  store_credit_id varchar2(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StoreCreditTender_REC"
(
  rib_oid number
, certificate_type varchar2
, first_name varchar2
, last_name varchar2
, personal_id_type varchar2
, state varchar2
, store_credit_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StoreCreditTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'certificate_type') := certificate_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'personal_id_type') := personal_id_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_credit_id') := store_credit_id;
END appendNodeValues;
constructor function "RIB_StoreCreditTender_REC"
(
  rib_oid number
, certificate_type varchar2
, first_name varchar2
, last_name varchar2
, personal_id_type varchar2
, state varchar2
, store_credit_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.certificate_type := certificate_type;
self.first_name := first_name;
self.last_name := last_name;
self.personal_id_type := personal_id_type;
self.state := state;
self.store_credit_id := store_credit_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_TravelCheckTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TravelCheckTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PaymentDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  check_count number(22),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TravelCheckTender_REC"
(
  rib_oid number
, check_count number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TravelCheckTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PaymentDesc') := "ns_name_PaymentDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'check_count') := check_count;
END appendNodeValues;
constructor function "RIB_TravelCheckTender_REC"
(
  rib_oid number
, check_count number
) return self as result is
begin
self.rib_oid := rib_oid;
self.check_count := check_count;
RETURN;
end;
END;
/
