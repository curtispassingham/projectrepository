DROP TYPE "RIB_LocPOTsfHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfHdrCriVo_REC";
/
DROP TYPE "RIB_LocPOTsfHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocPOTsfHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocPOTsfHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  order_id number(12),
  item varchar2(25),
  loc number(10),
  loc_type varchar2(1),
  source_type varchar2(1),
  source_id number(10),
  order_status varchar2(6),
  not_before_date date,
  not_after_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
) return self as result
,constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
) return self as result
,constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
) return self as result
,constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
, order_status varchar2
) return self as result
,constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
, order_status varchar2
, not_before_date date
) return self as result
,constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
, order_status varchar2
, not_before_date date
, not_after_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocPOTsfHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocPOTsfHdrCriVo') := "ns_name_LocPOTsfHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'order_id') := order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_type') := source_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_id') := source_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_status') := order_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_before_date') := not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
END appendNodeValues;
constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.item := item;
self.loc := loc;
self.loc_type := loc_type;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.item := item;
self.loc := loc;
self.loc_type := loc_type;
self.source_type := source_type;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.item := item;
self.loc := loc;
self.loc_type := loc_type;
self.source_type := source_type;
self.source_id := source_id;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
, order_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.item := item;
self.loc := loc;
self.loc_type := loc_type;
self.source_type := source_type;
self.source_id := source_id;
self.order_status := order_status;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
, order_status varchar2
, not_before_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.item := item;
self.loc := loc;
self.loc_type := loc_type;
self.source_type := source_type;
self.source_id := source_id;
self.order_status := order_status;
self.not_before_date := not_before_date;
RETURN;
end;
constructor function "RIB_LocPOTsfHdrCriVo_REC"
(
  rib_oid number
, order_id number
, item varchar2
, loc number
, loc_type varchar2
, source_type varchar2
, source_id number
, order_status varchar2
, not_before_date date
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_id := order_id;
self.item := item;
self.loc := loc;
self.loc_type := loc_type;
self.source_type := source_type;
self.source_id := source_id;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
RETURN;
end;
END;
/
