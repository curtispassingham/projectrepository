DROP TYPE "RIB_ItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemSupDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  supplier varchar2(10),
  primary_supp_ind varchar2(3),
  vpn varchar2(30),
  supp_label varchar2(15),
  consignment_rate number(12,4),
  supp_diff_1 varchar2(120),
  supp_diff_2 varchar2(120),
  supp_diff_3 varchar2(120),
  supp_diff_4 varchar2(120),
  pallet_name varchar2(6),
  case_name varchar2(6),
  inner_name varchar2(6),
  supp_discontinue_date date,
  direct_ship_ind varchar2(1),
  ExtOfItemSupDesc "RIB_ExtOfItemSupDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, supp_discontinue_date date
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, supp_discontinue_date date
, direct_ship_ind varchar2
) return self as result
,constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, supp_discontinue_date date
, direct_ship_ind varchar2
, ExtOfItemSupDesc "RIB_ExtOfItemSupDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemSupDesc') := "ns_name_ItemSupDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supp_ind') := primary_supp_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'vpn') := vpn;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_label') := supp_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'consignment_rate') := consignment_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_1') := supp_diff_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_2') := supp_diff_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_3') := supp_diff_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_4') := supp_diff_4;
  rib_obj_util.g_RIB_element_values(i_prefix||'pallet_name') := pallet_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_name') := case_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'inner_name') := inner_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_discontinue_date') := supp_discontinue_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'direct_ship_ind') := direct_ship_ind;
  l_new_pre :=i_prefix||'ExtOfItemSupDesc.';
  ExtOfItemSupDesc.appendNodeValues( i_prefix||'ExtOfItemSupDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.pallet_name := pallet_name;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.pallet_name := pallet_name;
self.case_name := case_name;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, supp_discontinue_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.supp_discontinue_date := supp_discontinue_date;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, supp_discontinue_date date
, direct_ship_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
RETURN;
end;
constructor function "RIB_ItemSupDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, consignment_rate number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, supp_discontinue_date date
, direct_ship_ind varchar2
, ExtOfItemSupDesc "RIB_ExtOfItemSupDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.consignment_rate := consignment_rate;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.ExtOfItemSupDesc := ExtOfItemSupDesc;
RETURN;
end;
END;
/
