DROP TYPE "RIB_RcvUnitAdjUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjUin_REC";
/
DROP TYPE "RIB_RcvUnitAdjDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjDtl_REC";
/
DROP TYPE "RIB_RcvUnitAdjDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjDesc_REC";
/
DROP TYPE "RIB_RcvUnitAdjUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RcvUnitAdjDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  status number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RcvUnitAdjUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RcvUnitAdjUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RcvUnitAdjDesc') := "ns_name_RcvUnitAdjDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
END appendNodeValues;
constructor function "RIB_RcvUnitAdjUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.status := status;
RETURN;
end;
END;
/
DROP TYPE "RIB_RcvUnitAdjUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjUin_TBL" AS TABLE OF "RIB_RcvUnitAdjUin_REC";
/
DROP TYPE "RIB_RcvUnitAdjDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RcvUnitAdjDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  po_nbr varchar2(12),
  asn_nbr varchar2(30),
  item varchar2(25),
  container_id varchar2(20),
  unit_qty number(12,4),
  from_disposition varchar2(4),
  to_disposition varchar2(4),
  RcvUnitAdjUin_TBL "RIB_RcvUnitAdjUin_TBL",   -- Size of "RIB_RcvUnitAdjUin_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
) return self as result
,constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
, from_disposition varchar2
) return self as result
,constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
, from_disposition varchar2
, to_disposition varchar2
) return self as result
,constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
, from_disposition varchar2
, to_disposition varchar2
, RcvUnitAdjUin_TBL "RIB_RcvUnitAdjUin_TBL"  -- Size of "RIB_RcvUnitAdjUin_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RcvUnitAdjDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RcvUnitAdjDesc') := "ns_name_RcvUnitAdjDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn_nbr') := asn_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_disposition') := from_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  IF RcvUnitAdjUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RcvUnitAdjUin_TBL.';
    FOR INDX IN RcvUnitAdjUin_TBL.FIRST()..RcvUnitAdjUin_TBL.LAST() LOOP
      RcvUnitAdjUin_TBL(indx).appendNodeValues( i_prefix||indx||'RcvUnitAdjUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.asn_nbr := asn_nbr;
self.item := item;
self.container_id := container_id;
self.unit_qty := unit_qty;
RETURN;
end;
constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
, from_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.asn_nbr := asn_nbr;
self.item := item;
self.container_id := container_id;
self.unit_qty := unit_qty;
self.from_disposition := from_disposition;
RETURN;
end;
constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
, from_disposition varchar2
, to_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.asn_nbr := asn_nbr;
self.item := item;
self.container_id := container_id;
self.unit_qty := unit_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
RETURN;
end;
constructor function "RIB_RcvUnitAdjDtl_REC"
(
  rib_oid number
, po_nbr varchar2
, asn_nbr varchar2
, item varchar2
, container_id varchar2
, unit_qty number
, from_disposition varchar2
, to_disposition varchar2
, RcvUnitAdjUin_TBL "RIB_RcvUnitAdjUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.asn_nbr := asn_nbr;
self.item := item;
self.container_id := container_id;
self.unit_qty := unit_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.RcvUnitAdjUin_TBL := RcvUnitAdjUin_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_RcvUnitAdjDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjDtl_TBL" AS TABLE OF "RIB_RcvUnitAdjDtl_REC";
/
DROP TYPE "RIB_RcvUnitAdjDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RcvUnitAdjDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RcvUnitAdjDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  loc number(10),
  loc_type varchar2(1),
  RcvUnitAdjDtl_TBL "RIB_RcvUnitAdjDtl_TBL",   -- Size of "RIB_RcvUnitAdjDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RcvUnitAdjDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, RcvUnitAdjDtl_TBL "RIB_RcvUnitAdjDtl_TBL"  -- Size of "RIB_RcvUnitAdjDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RcvUnitAdjDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RcvUnitAdjDesc') := "ns_name_RcvUnitAdjDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  l_new_pre :=i_prefix||'RcvUnitAdjDtl_TBL.';
  FOR INDX IN RcvUnitAdjDtl_TBL.FIRST()..RcvUnitAdjDtl_TBL.LAST() LOOP
    RcvUnitAdjDtl_TBL(indx).appendNodeValues( i_prefix||indx||'RcvUnitAdjDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_RcvUnitAdjDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, RcvUnitAdjDtl_TBL "RIB_RcvUnitAdjDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.RcvUnitAdjDtl_TBL := RcvUnitAdjDtl_TBL;
RETURN;
end;
END;
/
