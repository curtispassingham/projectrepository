@@NameValPairRBO.sql;
/
@@InformTaxRBO.sql;
/
@@PrdItemRBO.sql;
/
@@SvcItemRBO.sql;
/
DROP TYPE "RIB_LineItemRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LineItemRBO_REC";
/
DROP TYPE "RIB_InformTaxRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InformTaxRBO_TBL" AS TABLE OF "RIB_InformTaxRBO_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_LineItemRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LineItemRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LineItemRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  document_line_id varchar2(25),
  item_id varchar2(25),
  item_tran_code varchar2(25),
  item_type varchar2(1),
  quantity number(20,4),
  unit_of_measure varchar2(25),
  quantity_in_eaches number(20,4),
  origin_doc_date date,
  pack_item_id varchar2(25),
  total_cost number(20,4),
  unit_cost number(20,4),
  src_taxpayer_type varchar2(25),
  orig_fiscal_doc_number varchar2(25),
  orig_fiscal_doc_series varchar2(25),
  dim_object varchar2(6),
  length number(20,4),
  width number(20,4),
  lwh_uom varchar2(4),
  weight number(20,4),
  net_weight number(20,4),
  weight_uom varchar2(4),
  liquid_volume number(20,4),
  liquid_volume_uom varchar2(4),
  freight number(20,4),
  insurance number(20,4),
  discount number(20,4),
  commision number(20,4),
  freight_type varchar2(1),
  other_expenses number(20,4),
  origin_fiscal_code_opr varchar2(12),
  deduced_fiscal_code_opr varchar2(12),
  deduce_cfop_code varchar2(1),
  icms_cst_code varchar2(6),
  pis_cst_code varchar2(6),
  cofins_cst_code varchar2(6),
  deduce_icms_cst_code varchar2(1),
  deduce_pis_cst_code varchar2(1),
  deduce_cofins_cst_code varchar2(1),
  recoverable_icmsst number(20,4),
  item_cost_contains_cofins varchar2(1),
  recoverable_base_icmsst number(20,4),
  item_cost_contains_pis varchar2(1),
  item_cost_contains_icms varchar2(1),
  ipi_cst_code varchar2(6),
  ipi_clenq varchar2(25),
  imported_item_first_exit varchar2(1),
  imported_item_retail_src number(20,4),
  imported_item_retail_dest number(20,4),
  non_taxable_expenses number(20,4),
  PrdItemRBO "RIB_PrdItemRBO_REC",
  SvcItemRBO "RIB_SvcItemRBO_REC",
  InformTaxRBO_TBL "RIB_InformTaxRBO_TBL",   -- Size of "RIB_InformTaxRBO_TBL" is unbounded
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
, SvcItemRBO "RIB_SvcItemRBO_REC"
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
, SvcItemRBO "RIB_SvcItemRBO_REC"
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
, SvcItemRBO "RIB_SvcItemRBO_REC"
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"  -- Size of "RIB_InformTaxRBO_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LineItemRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LineItemRBO') := "ns_name_LineItemRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'document_line_id') := document_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_tran_code') := item_tran_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_type') := item_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_in_eaches') := quantity_in_eaches;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_doc_date') := origin_doc_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_item_id') := pack_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cost') := total_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_taxpayer_type') := src_taxpayer_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'orig_fiscal_doc_number') := orig_fiscal_doc_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'orig_fiscal_doc_series') := orig_fiscal_doc_series;
  rib_obj_util.g_RIB_element_values(i_prefix||'dim_object') := dim_object;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'lwh_uom') := lwh_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'net_weight') := net_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume') := liquid_volume;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume_uom') := liquid_volume_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight') := freight;
  rib_obj_util.g_RIB_element_values(i_prefix||'insurance') := insurance;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount') := discount;
  rib_obj_util.g_RIB_element_values(i_prefix||'commision') := commision;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_type') := freight_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'other_expenses') := other_expenses;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_fiscal_code_opr') := origin_fiscal_code_opr;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduced_fiscal_code_opr') := deduced_fiscal_code_opr;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_cfop_code') := deduce_cfop_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_cst_code') := icms_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_cst_code') := pis_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_cst_code') := cofins_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_icms_cst_code') := deduce_icms_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_pis_cst_code') := deduce_pis_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'deduce_cofins_cst_code') := deduce_cofins_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'recoverable_icmsst') := recoverable_icmsst;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_cost_contains_cofins') := item_cost_contains_cofins;
  rib_obj_util.g_RIB_element_values(i_prefix||'recoverable_base_icmsst') := recoverable_base_icmsst;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_cost_contains_pis') := item_cost_contains_pis;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_cost_contains_icms') := item_cost_contains_icms;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_cst_code') := ipi_cst_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_clenq') := ipi_clenq;
  rib_obj_util.g_RIB_element_values(i_prefix||'imported_item_first_exit') := imported_item_first_exit;
  rib_obj_util.g_RIB_element_values(i_prefix||'imported_item_retail_src') := imported_item_retail_src;
  rib_obj_util.g_RIB_element_values(i_prefix||'imported_item_retail_dest') := imported_item_retail_dest;
  rib_obj_util.g_RIB_element_values(i_prefix||'non_taxable_expenses') := non_taxable_expenses;
  l_new_pre :=i_prefix||'PrdItemRBO.';
  PrdItemRBO.appendNodeValues( i_prefix||'PrdItemRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'SvcItemRBO.';
  SvcItemRBO.appendNodeValues( i_prefix||'SvcItemRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF InformTaxRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InformTaxRBO_TBL.';
    FOR INDX IN InformTaxRBO_TBL.FIRST()..InformTaxRBO_TBL.LAST() LOOP
      InformTaxRBO_TBL(indx).appendNodeValues( i_prefix||indx||'InformTaxRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
self.imported_item_retail_dest := imported_item_retail_dest;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
self.imported_item_retail_dest := imported_item_retail_dest;
self.non_taxable_expenses := non_taxable_expenses;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
self.imported_item_retail_dest := imported_item_retail_dest;
self.non_taxable_expenses := non_taxable_expenses;
self.PrdItemRBO := PrdItemRBO;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
, SvcItemRBO "RIB_SvcItemRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
self.imported_item_retail_dest := imported_item_retail_dest;
self.non_taxable_expenses := non_taxable_expenses;
self.PrdItemRBO := PrdItemRBO;
self.SvcItemRBO := SvcItemRBO;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
, SvcItemRBO "RIB_SvcItemRBO_REC"
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
self.imported_item_retail_dest := imported_item_retail_dest;
self.non_taxable_expenses := non_taxable_expenses;
self.PrdItemRBO := PrdItemRBO;
self.SvcItemRBO := SvcItemRBO;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
RETURN;
end;
constructor function "RIB_LineItemRBO_REC"
(
  rib_oid number
, document_line_id varchar2
, item_id varchar2
, item_tran_code varchar2
, item_type varchar2
, quantity number
, unit_of_measure varchar2
, quantity_in_eaches number
, origin_doc_date date
, pack_item_id varchar2
, total_cost number
, unit_cost number
, src_taxpayer_type varchar2
, orig_fiscal_doc_number varchar2
, orig_fiscal_doc_series varchar2
, dim_object varchar2
, length number
, width number
, lwh_uom varchar2
, weight number
, net_weight number
, weight_uom varchar2
, liquid_volume number
, liquid_volume_uom varchar2
, freight number
, insurance number
, discount number
, commision number
, freight_type varchar2
, other_expenses number
, origin_fiscal_code_opr varchar2
, deduced_fiscal_code_opr varchar2
, deduce_cfop_code varchar2
, icms_cst_code varchar2
, pis_cst_code varchar2
, cofins_cst_code varchar2
, deduce_icms_cst_code varchar2
, deduce_pis_cst_code varchar2
, deduce_cofins_cst_code varchar2
, recoverable_icmsst number
, item_cost_contains_cofins varchar2
, recoverable_base_icmsst number
, item_cost_contains_pis varchar2
, item_cost_contains_icms varchar2
, ipi_cst_code varchar2
, ipi_clenq varchar2
, imported_item_first_exit varchar2
, imported_item_retail_src number
, imported_item_retail_dest number
, non_taxable_expenses number
, PrdItemRBO "RIB_PrdItemRBO_REC"
, SvcItemRBO "RIB_SvcItemRBO_REC"
, InformTaxRBO_TBL "RIB_InformTaxRBO_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.document_line_id := document_line_id;
self.item_id := item_id;
self.item_tran_code := item_tran_code;
self.item_type := item_type;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.quantity_in_eaches := quantity_in_eaches;
self.origin_doc_date := origin_doc_date;
self.pack_item_id := pack_item_id;
self.total_cost := total_cost;
self.unit_cost := unit_cost;
self.src_taxpayer_type := src_taxpayer_type;
self.orig_fiscal_doc_number := orig_fiscal_doc_number;
self.orig_fiscal_doc_series := orig_fiscal_doc_series;
self.dim_object := dim_object;
self.length := length;
self.width := width;
self.lwh_uom := lwh_uom;
self.weight := weight;
self.net_weight := net_weight;
self.weight_uom := weight_uom;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.freight := freight;
self.insurance := insurance;
self.discount := discount;
self.commision := commision;
self.freight_type := freight_type;
self.other_expenses := other_expenses;
self.origin_fiscal_code_opr := origin_fiscal_code_opr;
self.deduced_fiscal_code_opr := deduced_fiscal_code_opr;
self.deduce_cfop_code := deduce_cfop_code;
self.icms_cst_code := icms_cst_code;
self.pis_cst_code := pis_cst_code;
self.cofins_cst_code := cofins_cst_code;
self.deduce_icms_cst_code := deduce_icms_cst_code;
self.deduce_pis_cst_code := deduce_pis_cst_code;
self.deduce_cofins_cst_code := deduce_cofins_cst_code;
self.recoverable_icmsst := recoverable_icmsst;
self.item_cost_contains_cofins := item_cost_contains_cofins;
self.recoverable_base_icmsst := recoverable_base_icmsst;
self.item_cost_contains_pis := item_cost_contains_pis;
self.item_cost_contains_icms := item_cost_contains_icms;
self.ipi_cst_code := ipi_cst_code;
self.ipi_clenq := ipi_clenq;
self.imported_item_first_exit := imported_item_first_exit;
self.imported_item_retail_src := imported_item_retail_src;
self.imported_item_retail_dest := imported_item_retail_dest;
self.non_taxable_expenses := non_taxable_expenses;
self.PrdItemRBO := PrdItemRBO;
self.SvcItemRBO := SvcItemRBO;
self.InformTaxRBO_TBL := InformTaxRBO_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
END;
/
