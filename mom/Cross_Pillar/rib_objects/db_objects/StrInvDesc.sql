DROP TYPE "RIB_StrInvDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvDesc_REC";
/
DROP TYPE "RIB_StrInvNslQty_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvNslQty_REC";
/
DROP TYPE "RIB_StrInvNslQty_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvNslQty_TBL" AS TABLE OF "RIB_StrInvNslQty_REC";
/
DROP TYPE "RIB_StrInvDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrInvDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrInvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  store_id number(10),
  is_ranged varchar2(5), --is_ranged is boolean field, valid values are true,false (all lower-case) 
  is_estimated varchar2(5), --is_estimated is boolean field, valid values are true,false (all lower-case) 
  uom_code varchar2(4),
  case_size number(10,2),
  stock_on_hand_qty number(20,4),
  back_room_qty number(20,4),
  shop_floor_qty number(20,4),
  delivery_bay_qty number(20,4),
  available_qty number(20,4),
  unavailable_qty number(20,4),
  nonsell_total_qty number(20,4),
  in_transit_qty number(20,4),
  cust_reserved_qty number(20,4),
  transfer_reserved_qty number(20,4),
  vendor_return_qty number(20,4),
  StrInvNslQty_TBL "RIB_StrInvNslQty_TBL",   -- Size of "RIB_StrInvNslQty_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrInvDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, is_ranged varchar2  --is_ranged is boolean field, valid values are true,false (all lower-case)
, is_estimated varchar2  --is_estimated is boolean field, valid values are true,false (all lower-case)
, uom_code varchar2
, case_size number
, stock_on_hand_qty number
, back_room_qty number
, shop_floor_qty number
, delivery_bay_qty number
, available_qty number
, unavailable_qty number
, nonsell_total_qty number
, in_transit_qty number
, cust_reserved_qty number
, transfer_reserved_qty number
, vendor_return_qty number
) return self as result
,constructor function "RIB_StrInvDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, is_ranged varchar2  --is_ranged is boolean field, valid values are true,false (all lower-case)
, is_estimated varchar2  --is_estimated is boolean field, valid values are true,false (all lower-case)
, uom_code varchar2
, case_size number
, stock_on_hand_qty number
, back_room_qty number
, shop_floor_qty number
, delivery_bay_qty number
, available_qty number
, unavailable_qty number
, nonsell_total_qty number
, in_transit_qty number
, cust_reserved_qty number
, transfer_reserved_qty number
, vendor_return_qty number
, StrInvNslQty_TBL "RIB_StrInvNslQty_TBL"  -- Size of "RIB_StrInvNslQty_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrInvDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrInvDesc') := "ns_name_StrInvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_ranged') := is_ranged;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_estimated') := is_estimated;
  rib_obj_util.g_RIB_element_values(i_prefix||'uom_code') := uom_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_on_hand_qty') := stock_on_hand_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'back_room_qty') := back_room_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'shop_floor_qty') := shop_floor_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_bay_qty') := delivery_bay_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'available_qty') := available_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'unavailable_qty') := unavailable_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsell_total_qty') := nonsell_total_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'in_transit_qty') := in_transit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_reserved_qty') := cust_reserved_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_reserved_qty') := transfer_reserved_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'vendor_return_qty') := vendor_return_qty;
  IF StrInvNslQty_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvNslQty_TBL.';
    FOR INDX IN StrInvNslQty_TBL.FIRST()..StrInvNslQty_TBL.LAST() LOOP
      StrInvNslQty_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvNslQty_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrInvDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, is_ranged varchar2
, is_estimated varchar2
, uom_code varchar2
, case_size number
, stock_on_hand_qty number
, back_room_qty number
, shop_floor_qty number
, delivery_bay_qty number
, available_qty number
, unavailable_qty number
, nonsell_total_qty number
, in_transit_qty number
, cust_reserved_qty number
, transfer_reserved_qty number
, vendor_return_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.is_ranged := is_ranged;
self.is_estimated := is_estimated;
self.uom_code := uom_code;
self.case_size := case_size;
self.stock_on_hand_qty := stock_on_hand_qty;
self.back_room_qty := back_room_qty;
self.shop_floor_qty := shop_floor_qty;
self.delivery_bay_qty := delivery_bay_qty;
self.available_qty := available_qty;
self.unavailable_qty := unavailable_qty;
self.nonsell_total_qty := nonsell_total_qty;
self.in_transit_qty := in_transit_qty;
self.cust_reserved_qty := cust_reserved_qty;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
RETURN;
end;
constructor function "RIB_StrInvDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, is_ranged varchar2
, is_estimated varchar2
, uom_code varchar2
, case_size number
, stock_on_hand_qty number
, back_room_qty number
, shop_floor_qty number
, delivery_bay_qty number
, available_qty number
, unavailable_qty number
, nonsell_total_qty number
, in_transit_qty number
, cust_reserved_qty number
, transfer_reserved_qty number
, vendor_return_qty number
, StrInvNslQty_TBL "RIB_StrInvNslQty_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.is_ranged := is_ranged;
self.is_estimated := is_estimated;
self.uom_code := uom_code;
self.case_size := case_size;
self.stock_on_hand_qty := stock_on_hand_qty;
self.back_room_qty := back_room_qty;
self.shop_floor_qty := shop_floor_qty;
self.delivery_bay_qty := delivery_bay_qty;
self.available_qty := available_qty;
self.unavailable_qty := unavailable_qty;
self.nonsell_total_qty := nonsell_total_qty;
self.in_transit_qty := in_transit_qty;
self.cust_reserved_qty := cust_reserved_qty;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.StrInvNslQty_TBL := StrInvNslQty_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvNslQty_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrInvNslQty_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrInvDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  nonsell_qty_type_id number(12),
  quantity number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrInvNslQty_REC"
(
  rib_oid number
, nonsell_qty_type_id number
, quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrInvNslQty_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrInvDesc') := "ns_name_StrInvDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsell_qty_type_id') := nonsell_qty_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
END appendNodeValues;
constructor function "RIB_StrInvNslQty_REC"
(
  rib_oid number
, nonsell_qty_type_id number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.nonsell_qty_type_id := nonsell_qty_type_id;
self.quantity := quantity;
RETURN;
end;
END;
/
