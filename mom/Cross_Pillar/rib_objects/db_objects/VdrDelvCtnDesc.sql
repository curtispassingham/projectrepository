@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_VdrDelvCtnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnDesc_REC";
/
DROP TYPE "RIB_VdrDelvCtnItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItm_REC";
/
DROP TYPE "RIB_VdrDelvCtnItmUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItmUin_REC";
/
DROP TYPE "RIB_VdrDelvCtnItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItm_TBL" AS TABLE OF "RIB_VdrDelvCtnItm_REC";
/
DROP TYPE "RIB_VdrDelvCtnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  carton_id number(12),
  store_id number(10),
  external_id varchar2(128),
  reference_id varchar2(128),
  delivery_id number(12),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, RECEIVED, DAMAGED, MISSING, CANCELED, UNKNOWN] (all lower-case)
  customer_order_related varchar2(20), -- customer_order_related is enumeration field, valid values are [YES, MIX, NO] (all lower-case)
  damaged_reason varchar2(128),
  serial_code number(18),
  tracking_number varchar2(128),
  create_date date,
  create_user varchar2(128),
  update_date date,
  update_user varchar2(128),
  received_date date,
  received_user varchar2(128),
  adjusted varchar2(5), --adjusted is boolean field, valid values are true,false (all lower-case) 
  external_created varchar2(5), --external_created is boolean field, valid values are true,false (all lower-case) 
  quality_control varchar2(5), --quality_control is boolean field, valid values are true,false (all lower-case) 
  damage_remaining varchar2(5), --damage_remaining is boolean field, valid values are true,false (all lower-case) 
  receive_shopfloor varchar2(5), --receive_shopfloor is boolean field, valid values are true,false (all lower-case) 
  VdrDelvCtnItm_TBL "RIB_VdrDelvCtnItm_TBL",   -- Size of "RIB_VdrDelvCtnItm_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnDesc_REC"
(
  rib_oid number
, carton_id number
, store_id number
, external_id varchar2
, reference_id varchar2
, delivery_id number
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, received_date date
, received_user varchar2
, adjusted varchar2  --adjusted is boolean field, valid values are true,false (all lower-case)
, external_created varchar2  --external_created is boolean field, valid values are true,false (all lower-case)
, quality_control varchar2  --quality_control is boolean field, valid values are true,false (all lower-case)
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_VdrDelvCtnDesc_REC"
(
  rib_oid number
, carton_id number
, store_id number
, external_id varchar2
, reference_id varchar2
, delivery_id number
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, received_date date
, received_user varchar2
, adjusted varchar2  --adjusted is boolean field, valid values are true,false (all lower-case)
, external_created varchar2  --external_created is boolean field, valid values are true,false (all lower-case)
, quality_control varchar2  --quality_control is boolean field, valid values are true,false (all lower-case)
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, VdrDelvCtnItm_TBL "RIB_VdrDelvCtnItm_TBL"  -- Size of "RIB_VdrDelvCtnItm_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnDesc') := "ns_name_VdrDelvCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reference_id') := reference_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_related') := customer_order_related;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged_reason') := damaged_reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_code') := serial_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_date') := received_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_user') := received_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'adjusted') := adjusted;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_created') := external_created;
  rib_obj_util.g_RIB_element_values(i_prefix||'quality_control') := quality_control;
  rib_obj_util.g_RIB_element_values(i_prefix||'damage_remaining') := damage_remaining;
  rib_obj_util.g_RIB_element_values(i_prefix||'receive_shopfloor') := receive_shopfloor;
  IF VdrDelvCtnItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDelvCtnItm_TBL.';
    FOR INDX IN VdrDelvCtnItm_TBL.FIRST()..VdrDelvCtnItm_TBL.LAST() LOOP
      VdrDelvCtnItm_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDelvCtnItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnDesc_REC"
(
  rib_oid number
, carton_id number
, store_id number
, external_id varchar2
, reference_id varchar2
, delivery_id number
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, received_date date
, received_user varchar2
, adjusted varchar2
, external_created varchar2
, quality_control varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.store_id := store_id;
self.external_id := external_id;
self.reference_id := reference_id;
self.delivery_id := delivery_id;
self.status := status;
self.customer_order_related := customer_order_related;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.received_date := received_date;
self.received_user := received_user;
self.adjusted := adjusted;
self.external_created := external_created;
self.quality_control := quality_control;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
RETURN;
end;
constructor function "RIB_VdrDelvCtnDesc_REC"
(
  rib_oid number
, carton_id number
, store_id number
, external_id varchar2
, reference_id varchar2
, delivery_id number
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, create_user varchar2
, update_date date
, update_user varchar2
, received_date date
, received_user varchar2
, adjusted varchar2
, external_created varchar2
, quality_control varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, VdrDelvCtnItm_TBL "RIB_VdrDelvCtnItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.store_id := store_id;
self.external_id := external_id;
self.reference_id := reference_id;
self.delivery_id := delivery_id;
self.status := status;
self.customer_order_related := customer_order_related;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.create_date := create_date;
self.create_user := create_user;
self.update_date := update_date;
self.update_user := update_user;
self.received_date := received_date;
self.received_user := received_user;
self.adjusted := adjusted;
self.external_created := external_created;
self.quality_control := quality_control;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.VdrDelvCtnItm_TBL := VdrDelvCtnItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_VdrDelvCtnItmUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItmUin_TBL" AS TABLE OF "RIB_VdrDelvCtnItmUin_REC";
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_VdrDelvCtnItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  item_id varchar2(25),
  primary_vpn varchar2(256),
  case_size number(10,2),
  unit_cost number(12,4),
  unit_cost_override number(12,4),
  quantity_expected number(20,4),
  quantity_remaining number(20,4),
  quantity_received number(20,4),
  quantity_damaged number(20,4),
  quantity_received_overage number(20,4),
  quantity_damaged_overage number(20,4),
  purchase_order_id number(12),
  purchase_order_ext_id varchar2(128),
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  preferred_uom varchar2(4),
  VdrDelvCtnItmUin_TBL "RIB_VdrDelvCtnItmUin_TBL",   -- Size of "RIB_VdrDelvCtnItmUin_TBL" is 1000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, preferred_uom varchar2
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, preferred_uom varchar2
, VdrDelvCtnItmUin_TBL "RIB_VdrDelvCtnItmUin_TBL"  -- Size of "RIB_VdrDelvCtnItmUin_TBL" is 1000
) return self as result
,constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, preferred_uom varchar2
, VdrDelvCtnItmUin_TBL "RIB_VdrDelvCtnItmUin_TBL"  -- Size of "RIB_VdrDelvCtnItmUin_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnDesc') := "ns_name_VdrDelvCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_vpn') := primary_vpn;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost_override') := unit_cost_override;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_expected') := quantity_expected;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_remaining') := quantity_remaining;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_received') := quantity_received;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_damaged') := quantity_damaged;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_received_overage') := quantity_received_overage;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_damaged_overage') := quantity_damaged_overage;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_order_id') := purchase_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_order_ext_id') := purchase_order_ext_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'preferred_uom') := preferred_uom;
  IF VdrDelvCtnItmUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDelvCtnItmUin_TBL.';
    FOR INDX IN VdrDelvCtnItmUin_TBL.FIRST()..VdrDelvCtnItmUin_TBL.LAST() LOOP
      VdrDelvCtnItmUin_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDelvCtnItmUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
self.purchase_order_ext_id := purchase_order_ext_id;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, preferred_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.preferred_uom := preferred_uom;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, preferred_uom varchar2
, VdrDelvCtnItmUin_TBL "RIB_VdrDelvCtnItmUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.preferred_uom := preferred_uom;
self.VdrDelvCtnItmUin_TBL := VdrDelvCtnItmUin_TBL;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, primary_vpn varchar2
, case_size number
, unit_cost number
, unit_cost_override number
, quantity_expected number
, quantity_remaining number
, quantity_received number
, quantity_damaged number
, quantity_received_overage number
, quantity_damaged_overage number
, purchase_order_id number
, purchase_order_ext_id varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, preferred_uom varchar2
, VdrDelvCtnItmUin_TBL "RIB_VdrDelvCtnItmUin_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.primary_vpn := primary_vpn;
self.case_size := case_size;
self.unit_cost := unit_cost;
self.unit_cost_override := unit_cost_override;
self.quantity_expected := quantity_expected;
self.quantity_remaining := quantity_remaining;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.quantity_received_overage := quantity_received_overage;
self.quantity_damaged_overage := quantity_damaged_overage;
self.purchase_order_id := purchase_order_id;
self.purchase_order_ext_id := purchase_order_ext_id;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.preferred_uom := preferred_uom;
self.VdrDelvCtnItmUin_TBL := VdrDelvCtnItmUin_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_VdrDelvCtnItmUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItmUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  shipped varchar2(5), --shipped is boolean field, valid values are true,false (all lower-case) 
  received varchar2(5), --received is boolean field, valid values are true,false (all lower-case) 
  damaged varchar2(5), --damaged is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnItmUin_REC"
(
  rib_oid number
, uin varchar2
, shipped varchar2  --shipped is boolean field, valid values are true,false (all lower-case)
, received varchar2  --received is boolean field, valid values are true,false (all lower-case)
, damaged varchar2  --damaged is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnItmUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnDesc') := "ns_name_VdrDelvCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipped') := shipped;
  rib_obj_util.g_RIB_element_values(i_prefix||'received') := received;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged') := damaged;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnItmUin_REC"
(
  rib_oid number
, uin varchar2
, shipped varchar2
, received varchar2
, damaged varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.shipped := shipped;
self.received := received;
self.damaged := damaged;
RETURN;
end;
END;
/
