@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_TsfDelvCtnModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnModVo_REC";
/
DROP TYPE "RIB_TsfDelvCtnItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnItmMod_REC";
/
DROP TYPE "RIB_TsfDelvUinMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvUinMod_REC";
/
DROP TYPE "RIB_TsfDelvCtnModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnModNote_REC";
/
DROP TYPE "RIB_TsfDelvCtnItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnItmMod_TBL" AS TABLE OF "RIB_TsfDelvCtnItmMod_REC";
/
DROP TYPE "RIB_TsfDelvCtnModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnModNote_TBL" AS TABLE OF "RIB_TsfDelvCtnModNote_REC";
/
DROP TYPE "RIB_TsfDelvCtnModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(15),
  store_id number(10),
  carton_id number(15),
  external_id varchar2(128),
  reference_id varchar2(128),
  damaged_reason varchar2(128),
  serial_code number(18),
  tracking_number varchar2(128),
  damage_remaining varchar2(5), --damage_remaining is boolean field, valid values are true,false (all lower-case) 
  receive_shopfloor varchar2(5), --receive_shopfloor is boolean field, valid values are true,false (all lower-case) 
  TsfDelvCtnItmMod_TBL "RIB_TsfDelvCtnItmMod_TBL",   -- Size of "RIB_TsfDelvCtnItmMod_TBL" is 5000
  TsfDelvCtnModNote_TBL "RIB_TsfDelvCtnModNote_TBL",   -- Size of "RIB_TsfDelvCtnModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvCtnModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, external_id varchar2
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_TsfDelvCtnModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, external_id varchar2
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, TsfDelvCtnItmMod_TBL "RIB_TsfDelvCtnItmMod_TBL"  -- Size of "RIB_TsfDelvCtnItmMod_TBL" is 5000
) return self as result
,constructor function "RIB_TsfDelvCtnModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, external_id varchar2
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, TsfDelvCtnItmMod_TBL "RIB_TsfDelvCtnItmMod_TBL"  -- Size of "RIB_TsfDelvCtnItmMod_TBL" is 5000
, TsfDelvCtnModNote_TBL "RIB_TsfDelvCtnModNote_TBL"  -- Size of "RIB_TsfDelvCtnModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvCtnModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnModVo') := "ns_name_TsfDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reference_id') := reference_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged_reason') := damaged_reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_code') := serial_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'damage_remaining') := damage_remaining;
  rib_obj_util.g_RIB_element_values(i_prefix||'receive_shopfloor') := receive_shopfloor;
  IF TsfDelvCtnItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDelvCtnItmMod_TBL.';
    FOR INDX IN TsfDelvCtnItmMod_TBL.FIRST()..TsfDelvCtnItmMod_TBL.LAST() LOOP
      TsfDelvCtnItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDelvCtnItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF TsfDelvCtnModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDelvCtnModNote_TBL.';
    FOR INDX IN TsfDelvCtnModNote_TBL.FIRST()..TsfDelvCtnModNote_TBL.LAST() LOOP
      TsfDelvCtnModNote_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDelvCtnModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfDelvCtnModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, external_id varchar2
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carton_id := carton_id;
self.external_id := external_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
RETURN;
end;
constructor function "RIB_TsfDelvCtnModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, external_id varchar2
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, TsfDelvCtnItmMod_TBL "RIB_TsfDelvCtnItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carton_id := carton_id;
self.external_id := external_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.TsfDelvCtnItmMod_TBL := TsfDelvCtnItmMod_TBL;
RETURN;
end;
constructor function "RIB_TsfDelvCtnModVo_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, external_id varchar2
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, TsfDelvCtnItmMod_TBL "RIB_TsfDelvCtnItmMod_TBL"
, TsfDelvCtnModNote_TBL "RIB_TsfDelvCtnModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carton_id := carton_id;
self.external_id := external_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.TsfDelvCtnItmMod_TBL := TsfDelvCtnItmMod_TBL;
self.TsfDelvCtnModNote_TBL := TsfDelvCtnModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDelvUinMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvUinMod_TBL" AS TABLE OF "RIB_TsfDelvUinMod_REC";
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_TsfDelvCtnItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  item_id varchar2(25),
  case_size number(10,2),
  quantity_expected number(20,4),
  quantity_received number(20,4),
  quantity_damaged number(20,4),
  document_type varchar2(20), -- document_type is enumeration field, valid values are [TRANSFER, ALLOCATION, UNKNOWN] (all lower-case)
  document_id number(15),
  document_date date,
  TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL",   -- Size of "RIB_TsfDelvUinMod_TBL" is 5000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  removed_ext_att_col "RIB_removed_ext_att_col_TBL",   -- Size of "RIB_removed_ext_att_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
) return self as result
,constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL"  -- Size of "RIB_TsfDelvUinMod_TBL" is 5000
) return self as result
,constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL"  -- Size of "RIB_TsfDelvUinMod_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
) return self as result
,constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL"  -- Size of "RIB_TsfDelvUinMod_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"  -- Size of "RIB_removed_ext_att_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvCtnItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnModVo') := "ns_name_TsfDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_expected') := quantity_expected;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_received') := quantity_received;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_damaged') := quantity_damaged;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_id') := document_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_date') := document_date;
  IF TsfDelvUinMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDelvUinMod_TBL.';
    FOR INDX IN TsfDelvUinMod_TBL.FIRST()..TsfDelvUinMod_TBL.LAST() LOOP
      TsfDelvUinMod_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDelvUinMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_ext_att_col IS NOT NULL THEN
    FOR INDX IN removed_ext_att_col.FIRST()..removed_ext_att_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_ext_att_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_ext_att_col'||'.'):=removed_ext_att_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
RETURN;
end;
constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
self.TsfDelvUinMod_TBL := TsfDelvUinMod_TBL;
RETURN;
end;
constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
self.TsfDelvUinMod_TBL := TsfDelvUinMod_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
constructor function "RIB_TsfDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, TsfDelvUinMod_TBL "RIB_TsfDelvUinMod_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
self.TsfDelvUinMod_TBL := TsfDelvUinMod_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.removed_ext_att_col := removed_ext_att_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_ext_att_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_ext_att_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_TsfDelvUinMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvUinMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  shipped varchar2(5), --shipped is boolean field, valid values are true,false (all lower-case) 
  received varchar2(5), --received is boolean field, valid values are true,false (all lower-case) 
  damaged varchar2(5), --damaged is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvUinMod_REC"
(
  rib_oid number
, uin varchar2
, shipped varchar2  --shipped is boolean field, valid values are true,false (all lower-case)
, received varchar2  --received is boolean field, valid values are true,false (all lower-case)
, damaged varchar2  --damaged is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvUinMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnModVo') := "ns_name_TsfDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipped') := shipped;
  rib_obj_util.g_RIB_element_values(i_prefix||'received') := received;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged') := damaged;
END appendNodeValues;
constructor function "RIB_TsfDelvUinMod_REC"
(
  rib_oid number
, uin varchar2
, shipped varchar2
, received varchar2
, damaged varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.shipped := shipped;
self.received := received;
self.damaged := damaged;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDelvCtnModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvCtnModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvCtnModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnModVo') := "ns_name_TsfDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_TsfDelvCtnModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
