DROP TYPE "RIB_DSDDeals_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDeals_REC";
/
DROP TYPE "RIB_DSDDealsDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDealsDesc_REC";
/
DROP TYPE "RIB_DSDDeals_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDDeals_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDDealsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  order_no number(12),
  supplier varchar2(10),
  store number(10),
  dept number(4),
  currency_code varchar2(3),
  paid_ind varchar2(1),
  ext_ref_no varchar2(30),
  proof_of_delivery_no varchar2(30),
  payment_ref_no varchar2(16),
  payment_date date,
  deals_ind varchar2(1),
  shipment number(12),
  invc_id number(10),
  invc_ind varchar2(1),
  receipt_date date,
  qty_sum number(12,4),
  cost_sum number(20,4),
  ext_receipt_no varchar2(17),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDDeals_REC"
(
  rib_oid number
, order_no number
, supplier varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, deals_ind varchar2
, shipment number
, invc_id number
, invc_ind varchar2
, receipt_date date
, qty_sum number
, cost_sum number
, ext_receipt_no varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDDeals_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDDealsDesc') := "ns_name_DSDDealsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'paid_ind') := paid_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_ref_no') := ext_ref_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'proof_of_delivery_no') := proof_of_delivery_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_ref_no') := payment_ref_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_date') := payment_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'deals_ind') := deals_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment') := shipment;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_id') := invc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_ind') := invc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_date') := receipt_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_sum') := qty_sum;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_sum') := cost_sum;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_receipt_no') := ext_receipt_no;
END appendNodeValues;
constructor function "RIB_DSDDeals_REC"
(
  rib_oid number
, order_no number
, supplier varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, deals_ind varchar2
, shipment number
, invc_id number
, invc_ind varchar2
, receipt_date date
, qty_sum number
, cost_sum number
, ext_receipt_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.order_no := order_no;
self.supplier := supplier;
self.store := store;
self.dept := dept;
self.currency_code := currency_code;
self.paid_ind := paid_ind;
self.ext_ref_no := ext_ref_no;
self.proof_of_delivery_no := proof_of_delivery_no;
self.payment_ref_no := payment_ref_no;
self.payment_date := payment_date;
self.deals_ind := deals_ind;
self.shipment := shipment;
self.invc_id := invc_id;
self.invc_ind := invc_ind;
self.receipt_date := receipt_date;
self.qty_sum := qty_sum;
self.cost_sum := cost_sum;
self.ext_receipt_no := ext_receipt_no;
RETURN;
end;
END;
/
DROP TYPE "RIB_DSDDeals_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDeals_TBL" AS TABLE OF "RIB_DSDDeals_REC";
/
DROP TYPE "RIB_DSDDealsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDDealsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDDealsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  DSDDeals_TBL "RIB_DSDDeals_TBL",   -- Size of "RIB_DSDDeals_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDDealsDesc_REC"
(
  rib_oid number
, DSDDeals_TBL "RIB_DSDDeals_TBL"  -- Size of "RIB_DSDDeals_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDDealsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDDealsDesc') := "ns_name_DSDDealsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'DSDDeals_TBL.';
  FOR INDX IN DSDDeals_TBL.FIRST()..DSDDeals_TBL.LAST() LOOP
    DSDDeals_TBL(indx).appendNodeValues( i_prefix||indx||'DSDDeals_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_DSDDealsDesc_REC"
(
  rib_oid number
, DSDDeals_TBL "RIB_DSDDeals_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.DSDDeals_TBL := DSDDeals_TBL;
RETURN;
end;
END;
/
