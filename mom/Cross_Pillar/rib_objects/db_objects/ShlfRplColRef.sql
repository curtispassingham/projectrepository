@@ShlfRplRef.sql;
/
DROP TYPE "RIB_ShlfRplColRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplColRef_REC";
/
DROP TYPE "RIB_ShlfRplRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplRef_TBL" AS TABLE OF "RIB_ShlfRplRef_REC";
/
DROP TYPE "RIB_ShlfRplColRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplColRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplColRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ShlfRplRef_TBL "RIB_ShlfRplRef_TBL",   -- Size of "RIB_ShlfRplRef_TBL" is unbounded
  collection_size number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplColRef_REC"
(
  rib_oid number
, ShlfRplRef_TBL "RIB_ShlfRplRef_TBL"  -- Size of "RIB_ShlfRplRef_TBL" is unbounded
, collection_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplColRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplColRef') := "ns_name_ShlfRplColRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF ShlfRplRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ShlfRplRef_TBL.';
    FOR INDX IN ShlfRplRef_TBL.FIRST()..ShlfRplRef_TBL.LAST() LOOP
      ShlfRplRef_TBL(indx).appendNodeValues( i_prefix||indx||'ShlfRplRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'collection_size') := collection_size;
END appendNodeValues;
constructor function "RIB_ShlfRplColRef_REC"
(
  rib_oid number
, ShlfRplRef_TBL "RIB_ShlfRplRef_TBL"
, collection_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.ShlfRplRef_TBL := ShlfRplRef_TBL;
self.collection_size := collection_size;
RETURN;
end;
END;
/
