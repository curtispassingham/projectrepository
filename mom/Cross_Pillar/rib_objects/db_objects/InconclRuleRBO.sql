DROP TYPE "RIB_InconclRuleRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InconclRuleRBO_REC";
/
DROP TYPE "RIB_InconclRuleRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InconclRuleRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InconclRuleRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  status varchar2(250),
  tax_rule_code varchar2(250),
  tax_rule_description varchar2(250),
  tax_rule_id varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
) return self as result
,constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
, tax_rule_code varchar2
) return self as result
,constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
, tax_rule_code varchar2
, tax_rule_description varchar2
) return self as result
,constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
, tax_rule_code varchar2
, tax_rule_description varchar2
, tax_rule_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InconclRuleRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InconclRuleRBO') := "ns_name_InconclRuleRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rule_code') := tax_rule_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rule_description') := tax_rule_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rule_id') := tax_rule_id;
END appendNodeValues;
constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.status := status;
RETURN;
end;
constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
, tax_rule_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.status := status;
self.tax_rule_code := tax_rule_code;
RETURN;
end;
constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
, tax_rule_code varchar2
, tax_rule_description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.status := status;
self.tax_rule_code := tax_rule_code;
self.tax_rule_description := tax_rule_description;
RETURN;
end;
constructor function "RIB_InconclRuleRBO_REC"
(
  rib_oid number
, status varchar2
, tax_rule_code varchar2
, tax_rule_description varchar2
, tax_rule_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.status := status;
self.tax_rule_code := tax_rule_code;
self.tax_rule_description := tax_rule_description;
self.tax_rule_id := tax_rule_id;
RETURN;
end;
END;
/
