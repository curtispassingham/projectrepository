@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_TsfDelvCtnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnDesc_REC";
/
DROP TYPE "RIB_TsfDelvCtnItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnItm_REC";
/
DROP TYPE "RIB_TsfDelvUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvUin_REC";
/
DROP TYPE "RIB_TsfDelvCtnItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnItm_TBL" AS TABLE OF "RIB_TsfDelvCtnItm_REC";
/
DROP TYPE "RIB_TsfDelvCtnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(15),
  container_id number(15),
  store_id number(10),
  external_id varchar2(128),
  misdirected_carton_id number(15),
  reference_id varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [IN_PROGRESS, DAMAGED, RECEIVED, MISSING, UNKNOWN] (all lower-case)
  customer_order_related varchar2(20), -- customer_order_related is enumeration field, valid values are [YES, NO, MIX, UNKNOWN] (all lower-case)
  damaged_reason varchar2(128),
  serial_code number(18),
  tracking_number varchar2(128),
  create_date date,
  update_date date,
  received_date date,
  create_user varchar2(128),
  update_user varchar2(128),
  received_user varchar2(128),
  adjusted varchar2(5), --adjusted is boolean field, valid values are true,false (all lower-case) 
  external_create varchar2(5), --external_create is boolean field, valid values are true,false (all lower-case) 
  damage_remaining varchar2(5), --damage_remaining is boolean field, valid values are true,false (all lower-case) 
  receive_shopfloor varchar2(5), --receive_shopfloor is boolean field, valid values are true,false (all lower-case) 
  TsfDelvCtnItm_TBL "RIB_TsfDelvCtnItm_TBL",   -- Size of "RIB_TsfDelvCtnItm_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvCtnDesc_REC"
(
  rib_oid number
, delivery_id number
, container_id number
, store_id number
, external_id varchar2
, misdirected_carton_id number
, reference_id varchar2
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adjusted varchar2  --adjusted is boolean field, valid values are true,false (all lower-case)
, external_create varchar2  --external_create is boolean field, valid values are true,false (all lower-case)
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_TsfDelvCtnDesc_REC"
(
  rib_oid number
, delivery_id number
, container_id number
, store_id number
, external_id varchar2
, misdirected_carton_id number
, reference_id varchar2
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adjusted varchar2  --adjusted is boolean field, valid values are true,false (all lower-case)
, external_create varchar2  --external_create is boolean field, valid values are true,false (all lower-case)
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, TsfDelvCtnItm_TBL "RIB_TsfDelvCtnItm_TBL"  -- Size of "RIB_TsfDelvCtnItm_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvCtnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnDesc') := "ns_name_TsfDelvCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'misdirected_carton_id') := misdirected_carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reference_id') := reference_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_related') := customer_order_related;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged_reason') := damaged_reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_code') := serial_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_date') := received_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_user') := received_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'adjusted') := adjusted;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_create') := external_create;
  rib_obj_util.g_RIB_element_values(i_prefix||'damage_remaining') := damage_remaining;
  rib_obj_util.g_RIB_element_values(i_prefix||'receive_shopfloor') := receive_shopfloor;
  IF TsfDelvCtnItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDelvCtnItm_TBL.';
    FOR INDX IN TsfDelvCtnItm_TBL.FIRST()..TsfDelvCtnItm_TBL.LAST() LOOP
      TsfDelvCtnItm_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDelvCtnItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfDelvCtnDesc_REC"
(
  rib_oid number
, delivery_id number
, container_id number
, store_id number
, external_id varchar2
, misdirected_carton_id number
, reference_id varchar2
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adjusted varchar2
, external_create varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.container_id := container_id;
self.store_id := store_id;
self.external_id := external_id;
self.misdirected_carton_id := misdirected_carton_id;
self.reference_id := reference_id;
self.status := status;
self.customer_order_related := customer_order_related;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.create_date := create_date;
self.update_date := update_date;
self.received_date := received_date;
self.create_user := create_user;
self.update_user := update_user;
self.received_user := received_user;
self.adjusted := adjusted;
self.external_create := external_create;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
RETURN;
end;
constructor function "RIB_TsfDelvCtnDesc_REC"
(
  rib_oid number
, delivery_id number
, container_id number
, store_id number
, external_id varchar2
, misdirected_carton_id number
, reference_id varchar2
, status varchar2
, customer_order_related varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, create_date date
, update_date date
, received_date date
, create_user varchar2
, update_user varchar2
, received_user varchar2
, adjusted varchar2
, external_create varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, TsfDelvCtnItm_TBL "RIB_TsfDelvCtnItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.container_id := container_id;
self.store_id := store_id;
self.external_id := external_id;
self.misdirected_carton_id := misdirected_carton_id;
self.reference_id := reference_id;
self.status := status;
self.customer_order_related := customer_order_related;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.create_date := create_date;
self.update_date := update_date;
self.received_date := received_date;
self.create_user := create_user;
self.update_user := update_user;
self.received_user := received_user;
self.adjusted := adjusted;
self.external_create := external_create;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.TsfDelvCtnItm_TBL := TsfDelvCtnItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDelvUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDelvUin_TBL" AS TABLE OF "RIB_TsfDelvUin_REC";
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_TsfDelvCtnItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvCtnItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  item_id varchar2(25),
  description varchar2(400),
  case_size number(10,2),
  quantity_expected number(20,4),
  quantity_received number(20,4),
  quantity_damaged number(20,4),
  document_type varchar2(20), -- document_type is enumeration field, valid values are [TRANSFER, ALLOCATION, UNKNOWN] (all lower-case)
  document_id number(15),
  document_date date,
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  use_available varchar2(5), --use_available is boolean field, valid values are true,false (all lower-case) 
  TsfDelvUin_TBL "RIB_TsfDelvUin_TBL",   -- Size of "RIB_TsfDelvUin_TBL" is 5000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, customer_order_id varchar2
, fulfillment_order_id varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_TsfDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, customer_order_id varchar2
, fulfillment_order_id varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, TsfDelvUin_TBL "RIB_TsfDelvUin_TBL"  -- Size of "RIB_TsfDelvUin_TBL" is 5000
) return self as result
,constructor function "RIB_TsfDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, customer_order_id varchar2
, fulfillment_order_id varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, TsfDelvUin_TBL "RIB_TsfDelvUin_TBL"  -- Size of "RIB_TsfDelvUin_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvCtnItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnDesc') := "ns_name_TsfDelvCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_expected') := quantity_expected;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_received') := quantity_received;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_damaged') := quantity_damaged;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_id') := document_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_date') := document_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'use_available') := use_available;
  IF TsfDelvUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDelvUin_TBL.';
    FOR INDX IN TsfDelvUin_TBL.FIRST()..TsfDelvUin_TBL.LAST() LOOP
      TsfDelvUin_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDelvUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, customer_order_id varchar2
, fulfillment_order_id varchar2
, use_available varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.use_available := use_available;
RETURN;
end;
constructor function "RIB_TsfDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, customer_order_id varchar2
, fulfillment_order_id varchar2
, use_available varchar2
, TsfDelvUin_TBL "RIB_TsfDelvUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.use_available := use_available;
self.TsfDelvUin_TBL := TsfDelvUin_TBL;
RETURN;
end;
constructor function "RIB_TsfDelvCtnItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, case_size number
, quantity_expected number
, quantity_received number
, quantity_damaged number
, document_type varchar2
, document_id number
, document_date date
, customer_order_id varchar2
, fulfillment_order_id varchar2
, use_available varchar2
, TsfDelvUin_TBL "RIB_TsfDelvUin_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.case_size := case_size;
self.quantity_expected := quantity_expected;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.document_type := document_type;
self.document_id := document_id;
self.document_date := document_date;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.use_available := use_available;
self.TsfDelvUin_TBL := TsfDelvUin_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDelvUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDelvUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDelvCtnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  shipped varchar2(5), --shipped is boolean field, valid values are true,false (all lower-case) 
  received varchar2(5), --received is boolean field, valid values are true,false (all lower-case) 
  damaged varchar2(5), --damaged is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDelvUin_REC"
(
  rib_oid number
, uin varchar2
, shipped varchar2  --shipped is boolean field, valid values are true,false (all lower-case)
, received varchar2  --received is boolean field, valid values are true,false (all lower-case)
, damaged varchar2  --damaged is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDelvUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDelvCtnDesc') := "ns_name_TsfDelvCtnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipped') := shipped;
  rib_obj_util.g_RIB_element_values(i_prefix||'received') := received;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged') := damaged;
END appendNodeValues;
constructor function "RIB_TsfDelvUin_REC"
(
  rib_oid number
, uin varchar2
, shipped varchar2
, received varchar2
, damaged varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.shipped := shipped;
self.received := received;
self.damaged := damaged;
RETURN;
end;
END;
/
