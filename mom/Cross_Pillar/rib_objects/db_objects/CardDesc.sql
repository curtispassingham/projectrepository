@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_CardDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CardDesc_REC";
/
DROP TYPE "RIB_CardDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CardDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CardDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  card_id varchar2(14),
  card_number varchar2(40),
  masked_card_number varchar2(40),
  card_type varchar2(8), -- card_type is enumeration field, valid values are [CREDIT, DEBIT, GIFTCARD, LOYALTY, OTHER] (all lower-case)
  card_status varchar2(8), -- card_status is enumeration field, valid values are [ACTIVE, INACTIVE, OTHER] (all lower-case)
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  activation_date date,
  expiration_month number(2),
  expiration_year number(2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
) return self as result
,constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
) return self as result
,constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
,constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, activation_date date
) return self as result
,constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, activation_date date
, expiration_month number
) return self as result
,constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, activation_date date
, expiration_month number
, expiration_year number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CardDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CardDesc') := "ns_name_CardDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'card_id') := card_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_number') := card_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'masked_card_number') := masked_card_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_type') := card_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'card_status') := card_status;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'activation_date') := activation_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration_month') := expiration_month;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration_year') := expiration_year;
END appendNodeValues;
constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_id := card_id;
self.card_number := card_number;
self.masked_card_number := masked_card_number;
self.card_type := card_type;
RETURN;
end;
constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_id := card_id;
self.card_number := card_number;
self.masked_card_number := masked_card_number;
self.card_type := card_type;
self.card_status := card_status;
RETURN;
end;
constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_id := card_id;
self.card_number := card_number;
self.masked_card_number := masked_card_number;
self.card_type := card_type;
self.card_status := card_status;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, activation_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_id := card_id;
self.card_number := card_number;
self.masked_card_number := masked_card_number;
self.card_type := card_type;
self.card_status := card_status;
self.GeoAddrDesc := GeoAddrDesc;
self.activation_date := activation_date;
RETURN;
end;
constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, activation_date date
, expiration_month number
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_id := card_id;
self.card_number := card_number;
self.masked_card_number := masked_card_number;
self.card_type := card_type;
self.card_status := card_status;
self.GeoAddrDesc := GeoAddrDesc;
self.activation_date := activation_date;
self.expiration_month := expiration_month;
RETURN;
end;
constructor function "RIB_CardDesc_REC"
(
  rib_oid number
, card_id varchar2
, card_number varchar2
, masked_card_number varchar2
, card_type varchar2
, card_status varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, activation_date date
, expiration_month number
, expiration_year number
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_id := card_id;
self.card_number := card_number;
self.masked_card_number := masked_card_number;
self.card_type := card_type;
self.card_status := card_status;
self.GeoAddrDesc := GeoAddrDesc;
self.activation_date := activation_date;
self.expiration_month := expiration_month;
self.expiration_year := expiration_year;
RETURN;
end;
END;
/
