@@NameValPairRBO.sql;
/
@@NameValPairRBO.sql;
/
@@AddrRBO.sql;
/
DROP TYPE "RIB_FiscEntityRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscEntityRBO_REC";
/
DROP TYPE "RIB_TaxContributor_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxContributor_REC";
/
DROP TYPE "RIB_EcoClassCd_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_EcoClassCd_REC";
/
DROP TYPE "RIB_DiffTaxRegime_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DiffTaxRegime_REC";
/
DROP TYPE "RIB_EcoClassCd_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_EcoClassCd_TBL" AS TABLE OF "RIB_EcoClassCd_REC";
/
DROP TYPE "RIB_DiffTaxRegime_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DiffTaxRegime_TBL" AS TABLE OF "RIB_DiffTaxRegime_REC";
/
DROP TYPE "RIB_TaxContributor_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxContributor_TBL" AS TABLE OF "RIB_TaxContributor_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_FiscEntityRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscEntityRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscEntityRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  AddrRBO "RIB_AddrRBO_REC",
  EcoClassCd_TBL "RIB_EcoClassCd_TBL",   -- Size of "RIB_EcoClassCd_TBL" is unbounded
  DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL",   -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
  entity_code varchar2(250),
  entity_type varchar2(250),
  legal_name varchar2(500),
  fiscal_type varchar2(250),
  is_simples_contributor varchar2(1),
  federal_tax_reg_id varchar2(250),
  is_rural_producer varchar2(1),
  is_income_range_eligible varchar2(1),
  is_distr_a_manufacturer varchar2(1),
  icms_simples_rate number(20,4),
  TaxContributor_TBL "RIB_TaxContributor_TBL",   -- Size of "RIB_TaxContributor_TBL" is unbounded
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  suframa varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, TaxContributor_TBL "RIB_TaxContributor_TBL"  -- Size of "RIB_TaxContributor_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, TaxContributor_TBL "RIB_TaxContributor_TBL"  -- Size of "RIB_TaxContributor_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"  -- Size of "RIB_EcoClassCd_TBL" is unbounded
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"  -- Size of "RIB_DiffTaxRegime_TBL" is unbounded
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, TaxContributor_TBL "RIB_TaxContributor_TBL"  -- Size of "RIB_TaxContributor_TBL" is unbounded
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
, suframa varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscEntityRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscEntityRBO') := "ns_name_FiscEntityRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'AddrRBO.';
  AddrRBO.appendNodeValues( i_prefix||'AddrRBO');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF EcoClassCd_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'EcoClassCd_TBL.';
    FOR INDX IN EcoClassCd_TBL.FIRST()..EcoClassCd_TBL.LAST() LOOP
      EcoClassCd_TBL(indx).appendNodeValues( i_prefix||indx||'EcoClassCd_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF DiffTaxRegime_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DiffTaxRegime_TBL.';
    FOR INDX IN DiffTaxRegime_TBL.FIRST()..DiffTaxRegime_TBL.LAST() LOOP
      DiffTaxRegime_TBL(indx).appendNodeValues( i_prefix||indx||'DiffTaxRegime_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'entity_code') := entity_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'entity_type') := entity_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'legal_name') := legal_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_type') := fiscal_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_simples_contributor') := is_simples_contributor;
  rib_obj_util.g_RIB_element_values(i_prefix||'federal_tax_reg_id') := federal_tax_reg_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_rural_producer') := is_rural_producer;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_income_range_eligible') := is_income_range_eligible;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_distr_a_manufacturer') := is_distr_a_manufacturer;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_simples_rate') := icms_simples_rate;
  IF TaxContributor_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TaxContributor_TBL.';
    FOR INDX IN TaxContributor_TBL.FIRST()..TaxContributor_TBL.LAST() LOOP
      TaxContributor_TBL(indx).appendNodeValues( i_prefix||indx||'TaxContributor_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'suframa') := suframa;
END appendNodeValues;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
self.is_income_range_eligible := is_income_range_eligible;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, TaxContributor_TBL "RIB_TaxContributor_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.TaxContributor_TBL := TaxContributor_TBL;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, TaxContributor_TBL "RIB_TaxContributor_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.TaxContributor_TBL := TaxContributor_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscEntityRBO_REC"
(
  rib_oid number
, AddrRBO "RIB_AddrRBO_REC"
, EcoClassCd_TBL "RIB_EcoClassCd_TBL"
, DiffTaxRegime_TBL "RIB_DiffTaxRegime_TBL"
, entity_code varchar2
, entity_type varchar2
, legal_name varchar2
, fiscal_type varchar2
, is_simples_contributor varchar2
, federal_tax_reg_id varchar2
, is_rural_producer varchar2
, is_income_range_eligible varchar2
, is_distr_a_manufacturer varchar2
, icms_simples_rate number
, TaxContributor_TBL "RIB_TaxContributor_TBL"
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
, suframa varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrRBO := AddrRBO;
self.EcoClassCd_TBL := EcoClassCd_TBL;
self.DiffTaxRegime_TBL := DiffTaxRegime_TBL;
self.entity_code := entity_code;
self.entity_type := entity_type;
self.legal_name := legal_name;
self.fiscal_type := fiscal_type;
self.is_simples_contributor := is_simples_contributor;
self.federal_tax_reg_id := federal_tax_reg_id;
self.is_rural_producer := is_rural_producer;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distr_a_manufacturer := is_distr_a_manufacturer;
self.icms_simples_rate := icms_simples_rate;
self.TaxContributor_TBL := TaxContributor_TBL;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
self.suframa := suframa;
RETURN;
end;
END;
/
DROP TYPE "RIB_TaxContributor_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TaxContributor_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscEntityRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TaxContributor_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TaxContributor_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscEntityRBO') := "ns_name_FiscEntityRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_TaxContributor_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_EcoClassCd_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_EcoClassCd_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscEntityRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_EcoClassCd_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_EcoClassCd_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscEntityRBO') := "ns_name_FiscEntityRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_EcoClassCd_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_DiffTaxRegime_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DiffTaxRegime_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscEntityRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DiffTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DiffTaxRegime_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscEntityRBO') := "ns_name_FiscEntityRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_DiffTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
