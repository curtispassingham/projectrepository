DROP TYPE "RIB_FreightRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FreightRBO_REC";
/
DROP TYPE "RIB_SourceTaxRegime_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SourceTaxRegime_REC";
/
DROP TYPE "RIB_DestTaxRegime_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DestTaxRegime_REC";
/
DROP TYPE "RIB_SourceTaxRegime_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SourceTaxRegime_TBL" AS TABLE OF "RIB_SourceTaxRegime_REC";
/
DROP TYPE "RIB_DestTaxRegime_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DestTaxRegime_TBL" AS TABLE OF "RIB_DestTaxRegime_REC";
/
DROP TYPE "RIB_FreightRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FreightRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FreightRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_code varchar2(25),
  document_number varchar2(25),
  item_value_percentage number(12,4),
  pis_taxable_basis_amount number(20,4),
  pis_rec_value number(20,4),
  cofins_taxable_basis_amount number(20,4),
  cofins_rec_value number(20,4),
  item_utilization varchar2(5),
  fiscal_classification_code varchar2(25),
  SourceTaxRegime_TBL "RIB_SourceTaxRegime_TBL",   -- Size of "RIB_SourceTaxRegime_TBL" is unbounded
  DestTaxRegime_TBL "RIB_DestTaxRegime_TBL",   -- Size of "RIB_DestTaxRegime_TBL" is unbounded
  nature_of_operation varchar2(25),
  source_country_id varchar2(25),
  dest_country_id varchar2(25),
  item_tran_code varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FreightRBO_REC"
(
  rib_oid number
, item_code varchar2
, document_number varchar2
, item_value_percentage number
, pis_taxable_basis_amount number
, pis_rec_value number
, cofins_taxable_basis_amount number
, cofins_rec_value number
, item_utilization varchar2
, fiscal_classification_code varchar2
, SourceTaxRegime_TBL "RIB_SourceTaxRegime_TBL"  -- Size of "RIB_SourceTaxRegime_TBL" is unbounded
, DestTaxRegime_TBL "RIB_DestTaxRegime_TBL"  -- Size of "RIB_DestTaxRegime_TBL" is unbounded
, nature_of_operation varchar2
, source_country_id varchar2
, dest_country_id varchar2
, item_tran_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FreightRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FreightRBO') := "ns_name_FreightRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_code') := item_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_number') := document_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_value_percentage') := item_value_percentage;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_taxable_basis_amount') := pis_taxable_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_rec_value') := pis_rec_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_taxable_basis_amount') := cofins_taxable_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_rec_value') := cofins_rec_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_utilization') := item_utilization;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_classification_code') := fiscal_classification_code;
  IF SourceTaxRegime_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SourceTaxRegime_TBL.';
    FOR INDX IN SourceTaxRegime_TBL.FIRST()..SourceTaxRegime_TBL.LAST() LOOP
      SourceTaxRegime_TBL(indx).appendNodeValues( i_prefix||indx||'SourceTaxRegime_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF DestTaxRegime_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DestTaxRegime_TBL.';
    FOR INDX IN DestTaxRegime_TBL.FIRST()..DestTaxRegime_TBL.LAST() LOOP
      DestTaxRegime_TBL(indx).appendNodeValues( i_prefix||indx||'DestTaxRegime_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'nature_of_operation') := nature_of_operation;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_country_id') := source_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_country_id') := dest_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_tran_code') := item_tran_code;
END appendNodeValues;
constructor function "RIB_FreightRBO_REC"
(
  rib_oid number
, item_code varchar2
, document_number varchar2
, item_value_percentage number
, pis_taxable_basis_amount number
, pis_rec_value number
, cofins_taxable_basis_amount number
, cofins_rec_value number
, item_utilization varchar2
, fiscal_classification_code varchar2
, SourceTaxRegime_TBL "RIB_SourceTaxRegime_TBL"
, DestTaxRegime_TBL "RIB_DestTaxRegime_TBL"
, nature_of_operation varchar2
, source_country_id varchar2
, dest_country_id varchar2
, item_tran_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.document_number := document_number;
self.item_value_percentage := item_value_percentage;
self.pis_taxable_basis_amount := pis_taxable_basis_amount;
self.pis_rec_value := pis_rec_value;
self.cofins_taxable_basis_amount := cofins_taxable_basis_amount;
self.cofins_rec_value := cofins_rec_value;
self.item_utilization := item_utilization;
self.fiscal_classification_code := fiscal_classification_code;
self.SourceTaxRegime_TBL := SourceTaxRegime_TBL;
self.DestTaxRegime_TBL := DestTaxRegime_TBL;
self.nature_of_operation := nature_of_operation;
self.source_country_id := source_country_id;
self.dest_country_id := dest_country_id;
self.item_tran_code := item_tran_code;
RETURN;
end;
END;
/
DROP TYPE "RIB_SourceTaxRegime_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SourceTaxRegime_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FreightRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SourceTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SourceTaxRegime_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FreightRBO') := "ns_name_FreightRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_SourceTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_DestTaxRegime_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DestTaxRegime_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FreightRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  value varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DestTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DestTaxRegime_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FreightRBO') := "ns_name_FreightRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_DestTaxRegime_REC"
(
  rib_oid number
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.value := value;
RETURN;
end;
END;
/
